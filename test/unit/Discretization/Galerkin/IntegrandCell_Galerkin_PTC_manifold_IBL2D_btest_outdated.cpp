// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_PTC_manifold_IBL2D_btest
// testing of cell element residual integrands for Galerkin pseudo-time continuation (PTC): IBL2D

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_PTC_manifold.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Tuple/ElementTuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "pde/IBL/PDEIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "tools/Tuple.h"

using namespace std;
using namespace SANS;


namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarData2DDANCt VarDataType;
typedef VarTypeDANCt VarType;
typedef PDEIBL<PhysDim,VarType> PDEClass;
typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
typedef PDEClass::template ArrayParam<Real> ArrayParam;
typedef PDEClass::template VectorX<Real> VectorX;
typedef DLA::VectorS<PhysDim::D,VectorX> VectorVectorX;

typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldClass;
typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
typedef ElementSequence<ArrayQ,TopoD1,Line> ElementQFieldSeqClass;
typedef Element<Real,TopoD1,Line> ElementTFieldClass;
typedef Element<ArrayParam,TopoD1,Line> ElementParamFieldClass;
typedef Element<VectorX,TopoD1,Line> ElementVFieldClass;

typedef typename ElementXFieldClass::BaseType::RefCoordType RefCoordType;

typedef MakeTuple<ElementTuple, ElementParamFieldClass,
                                ElementXFieldClass>::type ElementIBLParamType;

typedef ElementTuple<ElementIBLParamType,ElementIBLParamType,TupleClass<1>> ElementParamPastAndCurrentType;
typedef ElementTuple<ElementParamPastAndCurrentType,ElementTFieldClass,TupleClass<1>> ElementParamPTCType;

typedef IntegrandCell_Galerkin_PTC_manifold<NDPDEClass> IntegrandClass;
typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,ElementParamPTCType> BasisWeightedClass;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class IntegrandCell_Galerkin_PTC_manifold<NDPDEClass>::BasisWeighted<Real,TopoD1,Line,ElementParamPTCType>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_PTC_manifold_IBL2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Phys2D_Line_X1Q1_test )
{
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);

  // field element order
  const int order = 1;

  // ----------------------- ELEMENT PARAMETERS ----------------------- //
  // Geometry (line grid)
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  xfldElem.DOF(0) = { 1.200, 2.200 };
  xfldElem.DOF(1) = { 2.000, 2.800 };

  // Parameter: edge velocity field
  ElementVFieldClass vfldElem(order, BasisFunctionCategory_Hierarchical);

  const Real q1x_L = 3.,  q1z_L = 4.;
  const Real q1x_R = 5.,  q1z_R = 2.;

  VectorX q1_L = { q1x_L, q1z_L };
  VectorX q1_R = { q1x_R, q1z_R };

  vfldElem.DOF(0) = q1_L;
  vfldElem.DOF(1) = q1_R;

  // Parameter: edge velocity gradient fields
  VectorVectorX gradvL, gradvR;
  std::vector<VectorX> gradphiL(vfldElem.nDOF()), gradphiR(vfldElem.nDOF());

  xfldElem.evalBasisGradient( 0.0, xfldElem, gradphiL.data(), gradphiL.size() );
  xfldElem.evalBasisGradient( 1.0, xfldElem, gradphiR.data(), gradphiR.size() );

  vfldElem.evalFromBasis( gradphiL.data(), gradphiL.size(), gradvL );
  vfldElem.evalFromBasis( gradphiR.data(), gradphiR.size(), gradvR );

  VectorX q1x_XL = { gradvL[0][0], gradvL[1][0] };
  VectorX q1x_XR = { gradvR[0][0], gradvR[1][0] };

  // Parameter: stagnation state field
  const Real p0i_L = 1.30e+5,  T0i_L = 290.;
  const Real p0i_R = 1.40e+5,  T0i_R = 350.;

  // Parameter field
  ElementParamFieldClass paramElem(order, BasisFunctionCategory_Hierarchical);

  paramElem.DOF(0) = {q1x_L, q1z_L, gradvL[0][0], gradvL[1][0], gradvL[0][1], gradvL[1][1], p0i_L, T0i_L};
  paramElem.DOF(1) = {q1x_R, q1z_R, gradvR[0][0], gradvR[1][0], gradvR[0][1], gradvR[1][1], p0i_R, T0i_R};

  // Solution
  ElementQFieldClass qfldElem_t0(order, BasisFunctionCategory_Hierarchical); // current timestep

  BOOST_CHECK_EQUAL( 1, qfldElem_t0.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem_t0.nDOF() );

  // State vector solution at line cell left node
  Real delta_L = 2e-2;
  Real A_L     = 1.2;

  // State vector solution at line cell right node
  Real delta_R = 2.2e-2;
  Real A_R     = 1.3;

  // Line solution
  qfldElem_t0.DOF(0) = pde.setDOFFrom( VarDataType(delta_L,A_L) );
  qfldElem_t0.DOF(1) = pde.setDOFFrom( VarDataType(delta_R,A_R) );


  // inverse timestep field element
  ElementTFieldClass dtifldElem(1, BasisFunctionCategory_Hierarchical);
  dtifldElem.DOF(0) = 1./13.;
  dtifldElem.DOF(1) = 1./12.;

  //Explicitly create a paramater instance.
  //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior
  ElementIBLParamType paramElemIBLpast = (paramElem, xfldElem);
  ElementIBLParamType paramElemIBL = (paramElem, xfldElem);
  ElementParamPastAndCurrentType paramElemPastAndCurrent = (paramElemIBLpast, paramElemIBL);
  ElementParamPTCType paramElemPTC = (paramElemPastAndCurrent, dtifldElem);

  // ----------------------------- TEST INTEGRAND & ELEMENTAL INTEGRAL (RESIDUAL) ----------------------------- //
  const Real small_tol = 8e-13;
  const Real close_tol = 8e-13;

  const int nBasis = qfldElem_t0.nDOF();  // number of basis functions

  RefCoordType sRef;
  IntegrandClass fcnint( pde, {0} );

  // Test the element integral of the functor: quadrature order = 39
  const int quadratureorder = 39;
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralPDE(quadratureorder, nBasis);

  //////////////////////////////////
  // Case 1: du/dt = 0; Set previous timestep equal to current timestep
  //////////////////////////////////
  {
    ElementQFieldSeqClass qfldElemPastVec(qfldElem_t0.basis(), 1);
    qfldElemPastVec[0] = qfldElem_t0;

    // --- Test integrands
    BasisWeightedClass fcnPDE = fcnint.integrand( paramElemPTC, qfldElem_t0, qfldElemPastVec );

    BOOST_CHECK( pde.N == fcnPDE.nEqn() );
    BOOST_CHECK_EQUAL( order+1, fcnPDE.nDOF() );
    BOOST_CHECK( fcnPDE.needsEvaluation() == true );

    std::vector<ArrayQ> integrandPDETrue(nBasis);
    std::vector<ArrayQ> integrandPDE(nBasis);

    // Test
    sRef = 0.;
    fcnPDE( sRef, integrandPDE.data(), nBasis ); // IntegrandCell Functor operator()

    // PDE residual integrands
    integrandPDETrue[0] = { 0, 0 };
    integrandPDETrue[1] = { 0, 0 };

    for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
    {
      for (int n = 0; n < pde.N; n++)
      {
        SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
      }
    }

    // Test
    sRef = 0.6;
    fcnPDE( sRef, integrandPDE.data(), nBasis ); // IntegrandCell Functor operator()

    // PDE residual integrands: (source) + (advective flux)
    integrandPDETrue[0] = { 0, 0 };
    integrandPDETrue[1] = { 0, 0 };

    for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
    {
      for (int n = 0; n < pde.N; n++)
      {
        SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
      }
    }

    // Test
    sRef = 1.0;
    fcnPDE( sRef, integrandPDE.data(), nBasis ); // IntegrandCell Functor operator()

    // PDE residual integrands: (source) + (advective flux)
    integrandPDETrue[0] = { 0, 0 };
    integrandPDETrue[1] = { 0, 0 };

    for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
    {
      for (int n = 0; n < pde.N; n++)
      {
        SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
      }
    }

    // --- Test integrals
    std::vector<ArrayQ> rsdPDETrue(nBasis);
    std::vector<ArrayQ> rsdElemPDE(nBasis);

    rsdPDETrue[0] = 0.0;
    rsdPDETrue[1] = 0.0;

    integralPDE( fcnPDE, xfldElem, rsdElemPDE.data(), nBasis );

    for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
    {
      for (int n = 0; n < pde.N; n++)
      {
        SANS_CHECK_CLOSE( rsdPDETrue[ibasis][n], rsdElemPDE[ibasis][n], small_tol, close_tol );
      }
    }
  }

  //////////////////////////////////
  // Case 1: du/dt = 0; Set previous timestep equal to current timestep
  //////////////////////////////////
  {
    ElementQFieldClass qfldElem_tm1(order, BasisFunctionCategory_Hierarchical); // current timestep

    BOOST_CHECK_EQUAL( 1, qfldElem_tm1.order() );
    BOOST_CHECK_EQUAL( 2, qfldElem_tm1.nDOF() );

    qfldElem_tm1.DOF(0) = pde.setDOFFrom( VarDataType(3.7e-2, 0.8) );
    qfldElem_tm1.DOF(1) = pde.setDOFFrom( VarDataType(1.2e-2, 3.0) );

    ElementQFieldSeqClass qfldElemPastVec(qfldElem_tm1.basis(), 1);

    qfldElemPastVec[0] = qfldElem_tm1;

    BasisWeightedClass fcnPDE = fcnint.integrand( paramElemPTC, qfldElem_t0, qfldElemPastVec );

    BOOST_CHECK( pde.N == fcnPDE.nEqn() );
    BOOST_CHECK_EQUAL( order+1, fcnPDE.nDOF() );
    BOOST_CHECK( fcnPDE.needsEvaluation() == true );

    RefCoordType sRef;
    
    const int nBasis = qfldElem_t0.nDOF();  // number of basis functions

    std::vector<ArrayQ> integrandPDETrue(nBasis);
    std::vector<ArrayQ> integrandPDE(nBasis);

    // Test
    sRef = 0.;
    fcnPDE( sRef, integrandPDE.data(), nBasis ); // IntegrandCell Functor operator()

    integrandPDETrue[0] = { -3.8768217660508559e-03, -2.6322486894222667e-02 }; // Basis function 1
    integrandPDETrue[1] = { -0.0000000000000000e+00, -0.0000000000000000e+00 }; // Basis function 2

    for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
    {
      for (int n = 0; n < pde.N; n++)
      {
        SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
      }
    }

    // Test
    sRef = 0.6;
    fcnPDE( sRef, integrandPDE.data(), nBasis ); // IntegrandCell Functor operator()

    integrandPDETrue[0] = { 1.6191720846831393e-04, 7.9669536175885599e-04 }; // Basis function 1
    integrandPDETrue[1] = { 2.4287581270247084e-04, 1.1950430426382838e-03 }; // Basis function 2

    for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
    {
      for (int n = 0; n < pde.N; n++)
      {
        SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
      }
    }

    // Test
    sRef = 1.0;
    fcnPDE( sRef, integrandPDE.data(), nBasis ); // IntegrandCell Functor operator()

    integrandPDETrue[0] = { 0.0000000000000000e+00, 0.0000000000000000e+00 }; // Basis function 1
    integrandPDETrue[1] = { 2.5948930183215405e-03, 1.8912695198185690e-02 }; // Basis function 2

    for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
    {
      for (int n = 0; n < pde.N; n++)
      {
        SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
      }
    }

    // ----------------------------- TEST INTEGRAND JACOBIAN----------------------------- //
    {
      const Real small_tol_jac = 1e-12;
      const Real close_tol_jac = 5e-6;

      sRef = 0.66;

      const std::vector<Real> fdstepsize = {2e-3, 1e-3};
      const ArrayQ DOFscale = {2E-2, 1.0};
      const Real rateMin = 1.9, rateMax = 2.1;

      typedef DLA::VectorD<ArrayQ> IntegrandType;
      typedef DLA::MatrixD<MatrixQ> IntegrandJacobianType;

      std::vector<IntegrandJacobianType> diffJac;

      // accumulate the jacobian via Surreal
      IntegrandJacobianType mtxPDEElem(nBasis, nBasis);
      mtxPDEElem = 0;

      const Real dJ = 1.0; // grid jacobian. Assume physical and reference coordinates cooincide
      fcnPDE(dJ, sRef, mtxPDEElem);

      for (size_t jstep = 0; jstep < fdstepsize.size(); ++jstep )
      {
        IntegrandType integrandm(nBasis), integrandp(nBasis);
        IntegrandJacobianType mtxPDEElemFD(nBasis, nBasis);

        // compute jacobians via central differencing
        for (int j = 0; j < nBasis; j++)
        {
          for (int n = 0; n < NDPDEClass::N; n++)
          {
            const Real step_size = DOFscale[n] * fdstepsize[jstep];

            qfldElem_t0.DOF(j)[n] -= step_size;
            fcnPDE(sRef, &integrandm[0], integrandm.m());

            qfldElem_t0.DOF(j)[n] += 2*step_size;
            fcnPDE(sRef, &integrandp[0], integrandp.m());

            qfldElem_t0.DOF(j)[n] -= step_size; // reset

            for (int i = 0; i < nBasis; i++)
              for (int m = 0; m < NDPDEClass::N; m++)
                mtxPDEElemFD(i,j)(m,n) = (integrandp[i][m] - integrandm[i][m])/(2.0*step_size);
          }
        }

        // compare jacobians directly
        for (int i = 0; i < nBasis; i++)
          for (int j = 0; j < nBasis; j++)
            for (int m = 0; m < PDEClass::N; m++)
              for (int n = 0; n < PDEClass::N; n++)
                SANS_CHECK_CLOSE( mtxPDEElemFD(i,j)(m,n), mtxPDEElem(i,j)(m,n), small_tol_jac, close_tol_jac );

        // save differences
        diffJac.push_back(mtxPDEElemFD - mtxPDEElem);
      }

      // jacobian ping test
      for (int i = 0; i < nBasis; i++)
      {
        for (int j = 0; j < nBasis; j++)
        {
          for (int m = 0; m < PDEClass::N; m++)
          {
            for (int n = 0; n < PDEClass::N; n++)
            {
              std::vector<Real> err_vec(fdstepsize.size());
              err_vec[0] = fabs( diffJac.at(0)(i,j)(m,n) );
              err_vec[1] = fabs( diffJac.at(1)(i,j)(m,n) );

              const Real rateLog = log(err_vec[1]/err_vec[0])/log(fdstepsize[1]/fdstepsize[0]);

              BOOST_CHECK_MESSAGE( ( (rateLog >= rateMin && rateLog <= rateMax)
                                  || (err_vec[0] < small_tol_jac)
                                  || (err_vec[1] < small_tol_jac) ),
                                   "Ping test fails at (i,j,m,n) = (" << i << ", " << j << ", " << m << ", " << n
                                   << "): rate = " << rateLog << ", jac = " << mtxPDEElem(i,j)(m,n)
                                   << "err_vec = (" << err_vec[0] << ", " << err_vec[1] << ")\n");
            }
          }
        }
      }
    }

    // --- Test integrals
    std::vector<ArrayQ> rsdPDETrue(nBasis);
    std::vector<ArrayQ> rsdElemPDE(nBasis);

    rsdPDETrue[0] = { -7.1427856650350594e-04, -4.9788728569355482e-03 }; // Basis function 1
    rsdPDETrue[1] = { 3.5722940544884754e-04, 2.3333609076900291e-03 }; // Basis function 2

    integralPDE( fcnPDE, xfldElem, rsdElemPDE.data(), nBasis );

    for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
    {
      for (int n = 0; n < pde.N; n++)
      {
        SANS_CHECK_CLOSE( rsdPDETrue[ibasis][n], rsdElemPDE[ibasis][n], small_tol, close_tol );
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
