// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_LinearScalar_Galerkin_AD_btest
// testing of boundary integrands: Advection-Diffusion on Triangles

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None > PDEAdvectionDiffusion1D;
typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_None> PDEAdvectionDiffusion3D;

typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass3D;

typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> > BCClass1D;
typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> > BCClass2D;
typedef BCNDConvertSpace<PhysD3, BCAdvectionDiffusion<PhysD3,BCTypeLinearRobin_sansLG> > BCClass3D;

typedef NDVectorCategory<boost::mpl::vector1<BCClass1D>, BCClass1D::Category> NDBCVecCat1D;
typedef NDVectorCategory<boost::mpl::vector1<BCClass2D>, BCClass2D::Category> NDBCVecCat2D;
typedef NDVectorCategory<boost::mpl::vector1<BCClass3D>, BCClass3D::Category> NDBCVecCat3D;

typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldArea_Triangle;
typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldArea_Quad;
typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldVolume_Tet;
typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldVolume_Hex;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, Galerkin>
               ::BasisWeighted<Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::BasisWeighted<Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::BasisWeighted<Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
               ::BasisWeighted<Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
               ::BasisWeighted<Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin_AD_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_BCTypeLinearRobin_sansLG_Line_X1Q1_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xfldTrace(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xfldTrace.order() );
  BOOST_CHECK_EQUAL( 1, xfldTrace.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xfldTrace.DOF(0) = {x2};
  xfldTrace.normalSignL() = 1;

  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // integrand functor

  StabilizationNitsche stab(order);
  IntegrandClass fcnint( pde, bc, {0}, stab );

  BasisWeightedClass fcn = fcnint.integrand( xfldTrace, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  ArrayQ integrandPDETrue[2], integrandPDE[2] = {0,0};

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrandPDE, 2 );

  //PDE residual integrands: (advective) + (viscous) + (Lagrange)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 15644387./6500000. );  // Basis function 1
  integrandPDETrue[1] = ( 33./10. ) + ( -2123./1000. ) + ( -12696787./6500000. );  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = 0;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, ArrayQ> integral(quadratureorder, nIntegrandL);

  ArrayQ rsdPDETrue[2], rsdPDE[2] = {0,0};

  // cell integration for canonical element
  integral( fcn, xfldTrace, rsdPDE, nIntegrandL );

  //PDE residuals: (advective) + (viscous) + (Lagrange)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 15644387./6500000. ) ; // Basis function 1
  rsdPDETrue[1] = ( 33./10. ) + ( -2123./1000. ) + ( -12696787./6500000. ) ; // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDE[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_BCTypeLinearRobin_sansLG_Triangle_X1Q1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // integrand functor

  StabilizationNitsche stab(order);
  IntegrandClass fcnint( pde, bc, {0}, stab );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  ArrayQ integrandPDETrue[3], integrandPDE[3] = {0,0,0};

  // Test at sRef={0}, {s,t}={1, 0}
  sRef = {0};
  fcn( sRef, integrandPDE, 3 );

  //PDE residual integrands: (advective) + (viscous) + (Lagrange)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( (4161*(14823+500*sqrt(2)))/6.5e6 ); // Basis function 1
  integrandPDETrue[1] = ( 39/(10.*sqrt(2)) ) + ( -4941/(500.*sqrt(2)) ) + ( (-14359062+5061625*sqrt(2))/1.625e6 ); // Basis function 2
  integrandPDETrue[2] = ( 0 ) + ( 0 ) + ( (-297*(14823+500*sqrt(2)))/1.3e6 ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  // Test at sRef={1}, {s,t}={0, 1}
  sRef = {1};
  fcn( sRef, integrandPDE, 3 );

  //PDE residual integrands: (advective) + (viscous) + (Lagrange)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( (4161*(3+14823/(500.*sqrt(2))))/(6500.*sqrt(2)) ); // Basis function 1
  integrandPDETrue[1] = ( 0 ) + ( 0 ) + ( (-669*(3+14823/(500.*sqrt(2))))/(1625.*sqrt(2)) ); // Basis function 2
  integrandPDETrue[2] = ( (13*sqrt(2))/5. ) + ( -4941/(500.*sqrt(2)) ) + ( (3*(-2452137+1203800*sqrt(2)))/1.3e6 ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef = {1./2.};
  fcn( sRef, integrandPDE, 3 );

  //PDE residual integrands: (advective) + (viscous) + (Lagrange)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( (4161*(14823+1000*sqrt(2)))/6.5e6 ); // Basis function 1
  integrandPDETrue[1] = ( 91/(40.*sqrt(2)) ) + ( -4941/(1000.*sqrt(2)) ) + ( (-23900649+3895625*sqrt(2))/3.25e6 ); // Basis function 2
  integrandPDETrue[2] = ( 91/(40.*sqrt(2)) ) + ( -4941/(1000.*sqrt(2)) ) + ( (-6029421+1796450*sqrt(2))/1.3e6 ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrandL);

  ArrayQ rsdPDE[3] = {0,0,0};
  ArrayQ rsdPDETrue[3];

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDE, nIntegrandL );

  //PDE residuals: (advective) + (viscous) + (Lagrange)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( (4161*(2+14823/(500.*sqrt(2))))/6500. ); // Basis function 1
  rsdPDETrue[1] = ( 13./6. ) + ( -4941./1000. ) + ( (-72076947+11849375*sqrt(2))/(4.875e6*sqrt(2)) ); // Basis function 2
  rsdPDETrue[2] = ( 143./60. ) + ( -4941./1000. ) + ( (-17938263+5324350*sqrt(2))/(1.95e6*sqrt(2)) ); // Basis function 3

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDE[2], small_tol, close_tol );
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_sansLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.348;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc , source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  Element<Real,TopoD2,Triangle> efldElem( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElem( qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());
    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
    }

    StabilizationNitsche stab(qorder);
    IntegrandClass fcnint( pde, bc, {0}, stab );

    BasisWeightedClass fcnB = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrand = qfldElem.nDOF();

    Real rsdElemW[1]={0};
    std::vector<Real> rsdElemB(nIntegrand,0); // trickery to get round the POD warning

    int quadratureorder = -1;
    GalerkinWeightedIntegral<TopoD1, Line, Real> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xedge, rsdElemB.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD1, Line, Real> integralW(quadratureorder, efldElem.nDOF() );

    for (int i = 0; i < wfldElem.nDOF(); i++) // testing the wfld
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemW[0] = 0;
      integralW( fcnW, xedge, rsdElemW, 1 );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdElemW[0], rsdElemB[i], small_tol, close_tol );

      // reset to 0
      wfldElem.DOF(i) = 0;
    }
  }

}
#endif



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
