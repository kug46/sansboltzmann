// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianFunctionalBoundaryTrace_HSM2D_btest
// testing of 2-D functional boundary-integral jacobian: PDEHSM2D

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>
#include <limits>

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldArea_CG_Cell.h"

#include "pde/HSM/BCHSM2D.h"
#include "pde/HSM/PDEHSM2D.h"
#include "pde/HSM/OutputHSM2D.h"

#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Output_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_Galerkin_HSM2D.h"
#include "Surreal/SurrealS.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianFunctionalBoundaryTrace_HSM2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_P1_Linear_test )
{
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::VectorX<Real> VectorX;
  typedef SurrealS<6> SurrealClass;

  typedef OutputHSM2D_Displacement<VarTypeLambda> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> IntegrandClass;

  typedef BCHSM2D<BCTypeForces, PDEHSM2D<VarTypeLambda>> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> BCIntegrandBoundaryTrace;

  typedef typename IntegrandClass::template ArrayJ<Real> ArrayJ;
  typedef typename IntegrandClass::template MatrixJ<Real> MatrixJ;

  // pde class
  VectorX g = { 0.023, -9.81 };
  NDPDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  // grid: single triangle, P1 (aka X1)
  //XField2D_1Triangle_X1_1Group xfld;
  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> varfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // State vector solution at first cell corner {0,0}
  Real   rx_00 = -1;
  Real   ry_00 = -1;
  Real lam3_00 = 3./5.;
  Real  f11_00 = 3;
  Real  f22_00 = -2./5.;
  Real  f12_00 = 1./2.;

  // State vector solution at second cell corner {1,0}
  Real  rx_10 = 1;
  Real  ry_10 = -1./2.;
  Real lam3_10 = 5./13.;
  Real  f11_10 = 1;
  Real  f22_10 = -1./2.;
  Real  f12_10 = 2;

  // State vector solution at third cell corner {0,1}
  Real   rx_01 = -1./2.;
  Real   ry_01 = 2;
  Real lam3_01 = 7./25.;
  Real  f11_01 = -5;
  Real  f22_01 = 9;
  Real  f12_01 = 2;

  // solution data
  for (int i = 0; i < varfld.nDOF(); i++)
  {
    switch (i % 3)
    {
    case 0:
      varfld.DOF(i) = pde.setDOFFrom( PositionLambdaForces2D(rx_00, ry_00, lam3_00, f11_00, f22_00, f12_00) );
      break;
    case 1:
      varfld.DOF(i) = pde.setDOFFrom( PositionLambdaForces2D(rx_10, ry_10, lam3_10, f11_10, f22_10, f12_10) );
      break;
    case 2:
      varfld.DOF(i) = pde.setDOFFrom( PositionLambdaForces2D(rx_01, ry_01, lam3_01, f11_01, f22_01, f12_01) );
    }
  }

  // quadrature rule (quadratic: basis is linear, solution is linear)
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 2);

  // integrand over 2 of the 3 boundaries
  NDOutputClass outputFcn(pde);
  IntegrandClass fcnint( outputFcn, {0,1} );

  StabilizationNitsche stab(qorder);
  NDBCClass bc(pde, 2.0, 3.0);
  BCIntegrandBoundaryTrace bcintegrand( pde, bc, {0,1}, stab );

  // Via Surreals

  DLA::VectorD<MatrixJ> jacFunctional( varfld.nDOF() );
  jacFunctional=0;

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      JacobianFunctionalBoundaryTrace_Galerkin<SurrealClass>( fcnint, jacFunctional ),
      xfld, varfld, quadratureOrder.data(), quadratureOrder.size() );

  // Via FD
  std::vector<MatrixJ> fdFunctional( varfld.nDOF() );
  ArrayJ functional0=0, functional1;

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      FunctionalBoundaryTrace_Galerkin( fcnint, functional0 ),
      xfld, varfld, quadratureOrder.data(), quadratureOrder.size() );

  Real eps = sqrt(std::numeric_limits<Real>::epsilon());

  for (int i = 0; i < varfld.nDOF(); i++)
  {
    for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
    {
      varfld.DOF(i)[n] += eps;

      functional1 = 0;
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          FunctionalBoundaryTrace_Galerkin( fcnint, functional1 ),
          xfld, varfld, quadratureOrder.data(), quadratureOrder.size() );

      varfld.DOF(i)[n] -= eps;

      // functional is a double
      fdFunctional[i][n] = (functional1 - functional0)/eps;
    }
  }

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-5;
  for (int i = 0; i < varfld.nDOF(); i++)
  {
    //std::cout << "i   = " << i << std::endl;
    //std::cout << "fd  = " << fdFunctional[i];
    //std::cout << "jac = " << jacFunctional[i] << std::endl;
    for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
      SANS_CHECK_CLOSE( fdFunctional[i][n], jacFunctional[i][n], small_tol, close_tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
