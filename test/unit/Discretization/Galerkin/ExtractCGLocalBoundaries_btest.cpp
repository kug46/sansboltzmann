// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LocalSolve_Split_Galerkin_Triangle_AD_btest
// testing of 2-D DG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "Discretization/Galerkin/ExtractCGLocalBoundaries.h"
#include "Discretization/Galerkin/FieldBundle_Galerkin.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_Cell.h"

#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Field/Field_NodalView.h"
#include "Field/ProjectSoln/ProjectSolnCell_CGtoDG.h"

#include "Field/Local/Field_Local.h"
#include "Field/Local/XField_LocalPatch.h"

#include "Field/tools/for_each_CellGroup.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

#include "tools/linspace.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ExtractCGLocalBoundaries_test_suite )


//----------------------------------------------------------------------------//
template <class PhysDim, class ArrayQ>
class CheckInteriorTraceCoordinates:
    public GroupFunctorInteriorTraceType<CheckInteriorTraceCoordinates<PhysDim,ArrayQ>>
{
public:

  explicit CheckInteriorTraceCoordinates( const Real small_tol, const Real close_tol,
                                          const std::vector<int>& interiorTraceGroups ) :
    small_tol(small_tol), close_tol(close_tol), interiorTraceGroups_(interiorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL, class TopologyR>
  void
  apply( const typename Field<PhysDim,  typename TopologyL::TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& fldCellL,
         const typename Field<PhysDim,  typename TopologyR::TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& fldCellR,
         const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    typedef typename TopologyL::TopoDim TopoDim;
    // Cell Group Types
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> FieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> FieldCellGroupTypeR;
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename FieldCellGroupTypeL::template ElementType<>  ElementFieldCellClassL;
    typedef typename FieldCellGroupTypeR::template ElementType<>  ElementFieldCellClassR;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    // Construct the elements
    ElementFieldCellClassL fldElemL( fldCellL.basis() );
    ElementFieldCellClassR fldElemR( fldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // a line of points
    const int npts = 5;
    std::vector<Real> oneDPts = linspace(0.1,0.9,npts);

    // raster scanning over the points to fill
    std::vector<DLA::VectorS<TopologyTrace::TopoDim::D,Real>> traceTestPts(pow(npts,PhysDim::D));

    for (int d = 0; d < PhysDim::D; d++)
      for (std::size_t i = 0; i < traceTestPts.size(); i++)
        traceTestPts[i][d] = oneDPts[static_cast<int>( floor( i / pow(npts,d) ) ) % npts];

    // vectors for storing the coordinates
    DLA::VectorS<TopoDim::D,Real> sCellL, sCellR;
    DLA::VectorS<TopologyTrace::TopoDim::D,Real> sRefTrace;

    // state vectors for the evaluation
    ArrayQ qL, qR;


    // loop over elements
    for (int elem = 0; elem < xfldTrace.nElem();elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      int elemR = xfldTrace.getElementRight( elem );
      CanonicalTraceToCell canonicalEdgeL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell canonicalEdgeR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      fldCellL.getElement( fldElemL, elemL );
      fldCellR.getElement( fldElemR, elemR );

      for (std::size_t i = 0; i < traceTestPts.size(); i++)
      {
        // extract the test point
        sRefTrace = traceTestPts[i];

        // left/right reference-element coords
        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalEdgeL, sRefTrace, sCellL );
        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::eval( canonicalEdgeR, sRefTrace, sCellR );

        // element coordinates from L/R elements
        fldElemL.eval( sCellL, qL );
        fldElemR.eval( sCellR, qR );

        // check if the left and right evaluations match in the trace
        for (int m = 0; m < ArrayQ::M; m++)
          SANS_CHECK_CLOSE( index(qL,m), index(qR,m), small_tol, close_tol );

      }
    }
  }
protected:
  const Real small_tol;
  const Real close_tol;

  const std::vector<int> interiorTraceGroups_;
};



#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ExtractCGLocalBoundaries_1D_test )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Line Topology;
  const int M = 2;
  typedef DLA::VectorS<M,Real> ArrayQ;

  BOOST_CHECK_EQUAL(1,1);

  XField1D xfld( 3 );

  // edge to extract local grids for
  std::array<int,2> edge{{1,2}};

  // bits for constructing the patch
  XField_CellToTrace<PhysDim,TopoDim> connectivity(xfld);
  Field_NodalView nodalView(xfld,{0});

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const BasisFunctionCategory basis = BasisFunctionCategory_Lagrange;

  // Global field bundle
  int order = 3;
  FieldBundle_Galerkin<PhysDim,TopoDim,ArrayQ> flds(xfld, order, basis, basis, {} );

  for (int dof = 0; dof < flds.qfld.nDOF(); dof++)
    for (int ind = 0; ind < M; ind++)
      flds.qfld.DOF(dof)[ind] = (dof-1)*M + ind;

  // local patch
  XField_LocalPatch<PhysDim,Topology> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalView);
  std::vector<int> localInteriorTraceGroups = linspace(0,xfld_local.nInteriorTraceGroups()-1);

  // local Field bundles -- SPLIT GROUP construction
  std::vector<int> active_BGroup_list{};
  FieldBundle_Galerkin_Local<PhysDim,TopoDim,ArrayQ> flds_local(xfld_local,flds,order,active_BGroup_list);

  Field_CG_Cell<PhysDim,TopoDim,Real> cfld(xfld_local,order,flds.basis_cell);
  std::map<int,int> group1_to_group0 = constructDOFMap(flds_local.qfld,cfld,1,0);

  BOOST_CHECK( group1_to_group0.size() > 0 );

  const Real tol = 1e-12;
  for (const auto& keyVal : group1_to_group0 )
    for ( int ind = 0; ind < M; ind++)
      SANS_CHECK_CLOSE( flds_local.qfld.DOF(keyVal.first)[ind], flds_local.qfld.DOF(keyVal.second)[ind], tol, tol );

  // make a local dg field and fill the dofs with exactly those of the CG field -> now can track where they came from
  Field_DG_Cell<PhysDim,TopoDim,ArrayQ> dfld( xfld_local, order, basis );
  // for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_CGtoDG<PhysDim,ArrayQ>({1}), (flds_local.qfld,dfld) );
  flds_local.qfld.projectTo(dfld);

  std::map<int,int> DGgroup1_to_CGgroup0 = constructDGtoCGDOFMap( dfld, flds_local.qfld, group1_to_group0, 1 );

  BOOST_CHECK( DGgroup1_to_CGgroup0.size() > 0 );
  //std::cout << "DGgroup1_to_CGgroup0.size() = " << DGgroup1_to_CGgroup0.size() <<std::endl;
  for (const auto& keyVal : DGgroup1_to_CGgroup0 )
    for ( int ind = 0; ind < M; ind++)
      SANS_CHECK_CLOSE( dfld.DOF(keyVal.first)[ind], flds_local.qfld.DOF(keyVal.second)[ind], tol, tol );

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ExtractCGLocalBoundaries_2D_test )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef Triangle Topology;
  const int M = 2;
  typedef DLA::VectorS<M,Real> ArrayQ;

  BOOST_CHECK_EQUAL(1,1);

  // XField2D_Box_Triangle_X1 xfld( 3, 3, 0, 3, 0, 3 );
  XField2D_4Triangle_X1_1Group xfld;

  // edge to extract local grids for
  // std::array<int,2> edge{{6,9}};
  std::array<int,2> edge{{0,1}};

  // bits for constructing the patch
  XField_CellToTrace<PhysDim,TopoDim> connectivity(xfld);
  Field_NodalView nodalView(xfld,{0});

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const BasisFunctionCategory basis = BasisFunctionCategory_Hierarchical;

  // Global field bundle
  int order = 3;
  FieldBundle_Galerkin<PhysDim,TopoDim,ArrayQ> flds(xfld, order, basis, basis, {} );

  for (int dof = 0; dof < flds.qfld.nDOF(); dof++)
    for (int ind = 0; ind < M; ind++)
      // flds.qfld.DOF(dof)[ind] = dof%3 * (2*ind - 1);
      flds.qfld.DOF(dof)[ind] = dof;

  // local patch

  XField_LocalPatchConstructor<PhysDim,Topology> xfld_construct(comm_local,connectivity,edge,SpaceType::Continuous,&nodalView);
  XField_LocalPatch<PhysDim,Topology> xfld_local(xfld_construct);

  // XField_LocalPatch<PhysDim,Topology> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalView);
  std::vector<int> localInteriorTraceGroups = linspace(0,xfld_local.nInteriorTraceGroups()-1);

  // local Field bundles -- SPLIT GROUP construction

  std::vector<int> active_BGroup_list;
  FieldBundle_Galerkin_Local<PhysDim,TopoDim,ArrayQ> flds_local(xfld_local,flds,order,active_BGroup_list);

  Field_CG_Cell<PhysDim,TopoDim,Real> cfld(xfld_local,order,flds.basis_cell);
  std::map<int,int> group1_to_group0 = constructDOFMap(flds_local.qfld,cfld,1,0);

  BOOST_CHECK( group1_to_group0.size() > 0 );

  const Real tol = 1e-10;
  for (const auto& keyVal : group1_to_group0 )
    for ( int ind = 0; ind < M; ind++)
      SANS_CHECK_CLOSE( flds_local.qfld.DOF(keyVal.first)[ind], flds_local.qfld.DOF(keyVal.second)[ind], tol, tol );

  // make a local dg field and fill the dofs with exactly those of the CG field -> now can track where they came from
  Field_DG_Cell<PhysDim,TopoDim,ArrayQ> dfld( xfld_local, order, basis );
  // for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_CGtoDG<PhysDim,ArrayQ>({1}), (flds_local.qfld,dfld) );
  flds_local.qfld.projectTo(dfld);

  std::map<int,int> DGgroup1_to_CGgroup0 = constructDGtoCGDOFMap( dfld, flds_local.qfld, group1_to_group0, 1 );

  BOOST_CHECK( DGgroup1_to_CGgroup0.size() > 0 );
  for (const auto& keyVal : DGgroup1_to_CGgroup0 )
    for ( int ind = 0; ind < M; ind++)
    {
      SANS_CHECK_CLOSE( dfld.DOF(keyVal.first)[ind], flds_local.qfld.DOF(keyVal.second)[ind], tol, tol );
    }

}
#endif



#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ExtractCGLocalBoundaries_3D_test )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef Tet Topology;
  const int M = 2;
  typedef DLA::VectorS<M,Real> ArrayQ;

  // XField2D_Box_Triangle_X1 xfld( 3, 3, 0, 3, 0, 3 );
  XField3D_6Tet_X1_1Group xfld;

  // edge to extract local grids for
  // std::array<int,2> edge{{6,9}};
  std::array<int,2> edge{{0,1}};

  // bits for constructing the patch
  XField_CellToTrace<PhysDim,TopoDim> connectivity(xfld);
  Field_NodalView nodalView(xfld,{0});

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const BasisFunctionCategory basis = BasisFunctionCategory_Hierarchical;

  // Global field bundle
  int order = 2;
  FieldBundle_Galerkin<PhysDim,TopoDim,ArrayQ> flds(xfld, order, basis, basis, {} );

  for (int dof = 0; dof < flds.qfld.nDOF(); dof++)
    for (int ind = 0; ind < M; ind++)
      // flds.qfld.DOF(dof)[ind] = dof%3 * (2*ind - 1);
      flds.qfld.DOF(dof)[ind] = dof;

  // local patch

  XField_LocalPatchConstructor<PhysDim,Topology> xfld_construct(comm_local,connectivity,edge,SpaceType::Continuous,&nodalView);
  XField_LocalPatch<PhysDim,Topology> xfld_local(xfld_construct);

  std::vector<int> localInteriorTraceGroups = linspace(0,xfld_local.nInteriorTraceGroups()-1);

  // local Field bundles -- SPLIT GROUP construction

  std::vector<int> active_BGroup_list{};
  FieldBundle_Galerkin_Local<PhysDim,TopoDim,ArrayQ> flds_local(xfld_local,flds,order,active_BGroup_list);

  Field_CG_Cell<PhysDim,TopoDim,Real> cfld(xfld_local,order,flds.basis_cell);
  std::map<int,int> group1_to_group0 = constructDOFMap(flds_local.qfld,cfld,1,0);

  BOOST_CHECK( group1_to_group0.size() > 0 );

  const Real tol = 1e-12;
  for (const auto& keyVal : group1_to_group0 )
    for ( int ind = 0; ind < M; ind++)
      SANS_CHECK_CLOSE( flds_local.qfld.DOF(keyVal.first)[ind], flds_local.qfld.DOF(keyVal.second)[ind], tol, tol );

  // make a local dg field and fill the dofs with exactly those of the CG field -> now can track where they came from
  Field_DG_Cell<PhysDim,TopoDim,ArrayQ> dfld( xfld_local, order, basis );
  // for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_CGtoDG<PhysDim,ArrayQ>({1}), (flds_local.qfld,dfld) );
  flds_local.qfld.projectTo(dfld);

  std::map<int,int> DGgroup1_to_CGgroup0 = constructDGtoCGDOFMap( dfld, flds_local.qfld, group1_to_group0, 1 );

  BOOST_CHECK( DGgroup1_to_CGgroup0.size() > 0 );
  for (const auto& keyVal : DGgroup1_to_CGgroup0 )
    for ( int ind = 0; ind < M; ind++)
      SANS_CHECK_CLOSE( dfld.DOF(keyVal.first)[ind], flds_local.qfld.DOF(keyVal.second)[ind], tol, tol );

}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
