// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <ostream>
#include <string>
#include <fstream>

#include "tools/SANSnumerics.h"     // Real

#include "Discretization/Galerkin/AlgebraicEquationSet_Project.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/Tuple/FieldTuple.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/FunctionNDConvertSpace2D.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Discretization/Galerkin/IntegrandCell_ProjectFunction.h"
#include "Discretization/Galerkin/IntegrandCell_Project.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace SANS
{

typedef FunctionNDConvertSpace<PhysD2, Real> Function2DND;
typedef IntegrandCell_ProjectFunction<Function2DND,IntegrandCell_ProjFcn_detail::FcnX> IntegrandCellFunctionClass;
template class AlgebraicEquationSet_Project< XField<PhysD2, TopoD2>, IntegrandCellFunctionClass, TopoD2, AlgEqSetTraits_Sparse >;
template class AlgebraicEquationSet_Project< XField<PhysD2, TopoD2>, IntegrandCellFunctionClass, TopoD2, AlgEqSetTraits_Dense >;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None> PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
typedef IntegrandCell_Project<PDEClass2D> IntegrandCellProjectClass;
typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;
template class AlgebraicEquationSet_Project< ParamFieldTupleType, IntegrandCellProjectClass, TopoD2, AlgEqSetTraits_Sparse >;
template class AlgebraicEquationSet_Project< ParamFieldTupleType, IntegrandCellProjectClass, TopoD2, AlgEqSetTraits_Dense >;

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Project_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseSolve )
{
  typedef AlgebraicEquationSet_Project< XField<PhysD2, TopoD2>,
      IntegrandCellFunctionClass, TopoD2, AlgEqSetTraits_Sparse > ProjectionEquationSetClass;

  typedef ProjectionEquationSetClass::ArrayQ ArrayQ;

  typedef ProjectionEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef ProjectionEquationSetClass::SystemVector SystemVectorClass;

  // Function

  Real a = 3.14;
  Real b = 0.25;
  Real c = -1.25;
  ScalarFunction2D_Linear fcnLiner(a, b, c);

  Function2DND fcnND(fcnLiner);

  // grid: HierarchicalP1 (aka X1)

  int ii = 3;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );


  // solution: HierarchicalP1 (aka Q1)

  int order = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  qfld = 0;

  QuadratureOrder quadratureOrder( xfld, 2 );
  IntegrandCellFunctionClass fcnCell( fcnND, {0} );
  const std::array<Real,1> tol_projection = {{1e-15}};
  ProjectionEquationSetClass ProjEqSet(xfld, qfld, fcnCell, quadratureOrder, tol_projection );

  SystemVectorClass q(ProjEqSet.vectorStateSize());
  SystemVectorClass rhs(ProjEqSet.vectorEqSize());

  ProjEqSet.fillSystemVector(q);

  rhs = 0;
  ProjEqSet.residual(rhs);

  SLA::UMFPACK<SystemMatrixClass> solver(ProjEqSet);
  SystemVectorClass dq(ProjEqSet.vectorStateSize());
  solver.solve(rhs, dq);

  q -= dq;

  ProjEqSet.setSolutionField(q);

  // Field Cell Types
  typedef typename XField<PhysD2, TopoD2        >::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename Field< PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::ElementType<> ElementQFieldClass;

  typedef typename ElementXFieldClass::VectorX VectorX;

  const XFieldCellGroupType& xfldCell = xfld.getCellGroup<Triangle>(0);

  // solution - p - For directly evaluating the functional
  QFieldCellGroupType& qfldCell = qfld.getCellGroup<Triangle>(0);

  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // use quadrature rule used to check the projection
  Quadrature<TopoD2, Triangle> quadrature( 1 );
  typedef QuadraturePoint<TopoD2> QuadPoint;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  for (int elem = 0; elem < xfldCell.nElem(); elem++)
  {
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    // loop over quadrature points
    const int nquad = quadrature.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      QuadPoint Ref = quadrature.coordinates_cache( iquad );

      ArrayQ q;
      VectorX X;
      qfldElem.eval( Ref, q );
      xfldElem.eval( Ref, X );

      ArrayQ q0 = fcnND(X);
      for (int n = 0; n < DLA::VectorSize<ArrayQ>::M; n++)
        SANS_CHECK_CLOSE( DLA::index(q0,n), DLA::index(q,n), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectSparseSolve )
{

  typedef AlgebraicEquationSet_Project< ParamFieldTupleType,
      IntegrandCellProjectClass, TopoD2, AlgEqSetTraits_Sparse > ProjectionEquationSetClass;
  typedef ProjectionEquationSetClass::ArrayQ ArrayQ;

  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass2D;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass_Triangle2D;

  typedef MakeTuple< ElementTuple, ElementQFieldClass2D,
                                   ElementXFieldClass_Triangle2D >::type ElementParam2D;

  typedef IntegrandCellProjectClass::BasisWeighted<TopoD2,Triangle, ElementParam2D> BasisWeightedClass;


  typedef ProjectionEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef ProjectionEquationSetClass::SystemVector SystemVectorClass;

  // grid: HierarchicalP1 (aka X1)

  int ii = 3;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  // solution: HierarchicalP1 (aka Q1)

  int order = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qFromfld(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qTofld(xfld, order, BasisFunctionCategory_Hierarchical);

  qFromfld = 0;  qTofld = 0;

  for (int i = 0; i < qFromfld.nDOF(); i++)
    qFromfld.DOF(i) = (i+1)*pow(-1,i);

  QuadratureOrder quadratureOrder( xfld, 2 );
  IntegrandCellProjectClass fcnCell( {0} );

  const std::array<Real,1> tol_projection = {{1e-10}};

  ParamFieldTupleType paramfld = (qFromfld,xfld);

  ProjectionEquationSetClass ProjEqSet(paramfld, qTofld, fcnCell, quadratureOrder, tol_projection );

  SystemVectorClass q(ProjEqSet.vectorStateSize());
  SystemVectorClass rhs(ProjEqSet.vectorEqSize());

  ProjEqSet.fillSystemVector(q);

  rhs = 0;
  ProjEqSet.residual(rhs);

  SLA::UMFPACK<SystemMatrixClass> solver(ProjEqSet);
  solver.solve(rhs, q); // Goes directly, no q -= dq necessary

  ProjEqSet.setSolutionField(q);

  rhs = 0;
  ProjEqSet.residual(rhs);
  std::vector<std::vector<Real>> rsdNrm( ProjEqSet.residualNorm(rhs) );

  bool residConverged = ProjEqSet.convergedResidual(rsdNrm);

  BOOST_CHECK_EQUAL( residConverged, true );

  // Field Cell Types
  typedef typename XField<PhysD2, TopoD2        >::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename Field< PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle> QFieldCellGroupType;

  typedef typename QFieldCellGroupType::ElementType<> ElementQFieldClass;
  typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;

  const XFieldCellGroupType& xfldCell = xfld.getCellGroup<Triangle>(0);

  // solution - p - For directly evaluating the functional
  QFieldCellGroupType& qFromfldCell = qFromfld.getCellGroup<Triangle>(0);
  QFieldCellGroupType& qTofldCell = qTofld.getCellGroup<Triangle>(0);

  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qFromfldElem( qFromfldCell.basis() );
  ElementQFieldClass qTofldElem( qTofldCell.basis() );

  // use quadrature rule used to check the projection
  Quadrature<TopoD2, Triangle> quadrature( order );
  int nIntegrand = qTofldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2,Triangle,ArrayQ> integral( order, nIntegrand );

  std::vector<ArrayQ> rsdElem(qTofldElem.nDOF());

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  // Loop over elements, check that their galerkin weighted integrals are now 0
  for (int elem = 0; elem < xfldCell.nElem(); elem++)
  {
    xfldCell.getElement( xfldElem, elem );
    qFromfldCell.getElement( qFromfldElem, elem );
    qTofldCell.getElement( qTofldElem, elem );

    ElementParam2D elemParam = (qFromfldElem, xfldElem);
    BasisWeightedClass fcn = fcnCell.integrand( elemParam, qTofldElem );

    integral(fcn, xfldElem, rsdElem.data(), nIntegrand );

    // loop over integrands, check all are 0
    for (int i = 0; i < nIntegrand; i++)
      SANS_CHECK_CLOSE( 0, rsdElem[i], small_tol, close_tol );

  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
