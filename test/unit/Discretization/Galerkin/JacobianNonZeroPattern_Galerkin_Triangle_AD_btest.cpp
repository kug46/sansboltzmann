// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianNonZeroPattern_CG_Triangle_AD_btest
// testing computing the non-zero pattern Jacobian of CG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_mitLG_Galerkin.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_mitLG_Galerkin.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianNonZeroPattern_CG_Triangle_AD_test_suite )


template<class MatrixQ>
void checkDenseSparseEquality(DLA::MatrixD<MatrixQ>& djac, const SLA::SparseMatrix_CRS<MatrixQ>& sjac )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const int *col_ind = sjac.get_col_ind();
  const int *row_ptr = sjac.get_row_ptr();

  for (int i = 0; i < sjac.m(); i++)
    for (int j = row_ptr[i]; j < row_ptr[i+1]; j++)
    {
      SANS_CHECK_CLOSE( djac(i,col_ind[j]), sjac[j], small_tol, close_tol );

      // Zero out the non-zero entry so the next loop can look for any non-zero values missed
      djac(i,col_ind[j]) = 0;
    }

  // Check that all non-zero values have been cleared
  for (int i = 0; i < djac.m(); i++)
    for (int j = 0; j < djac.n(); j++)
      BOOST_CHECK_EQUAL(djac(i,j), 0);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( X1Q1LG1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> SolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCAdvectionDiffusion<PhysD2,BCType> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat1D;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat1D, Galerkin> IntegrandBCClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandCellClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC
  std::shared_ptr<SolutionExact> solnExact( new SolutionExact );

  BCClass bc( solnExact, visc );

  // integrands
  IntegrandCellClass fcnCell( pde, {0} );
  IntegrandBCClass fcnBC( pde, bc, {0,1,2,3} );

  // grid: HierarchicalP1 (aka X1)

  int ii = 3;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );


  // solution: HierarchicalP1 (aka Q1)

  int order = 1;

  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  int nDOFPDE = qfld.nDOF();
  qfld = 0;

  // Lagrange multiplier: Legendre P1

#if 0
  order = 0;
  QField2D_DG_BoundaryEdge<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#elif 1
  order = 1;
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#else
  order = 1;
  QField2D_CG_BoundaryEdge_Independent<PDEClass> lgfld( xfld, order );
#endif

  int nDOFBC = lgfld.nDOF();
  lgfld = 0;

#if 0
  cout << "btest: dumping lgfld" << endl;  lgfld.dump(2);
#endif

  // quadrature rule
  int quadratureOrder[4] = {-1, -1, -1, -1};    // max
  int quadratureOrderMin[4] = {0, 0, 0, 0};     // min

  // dense linear system setup

  typedef DLA::MatrixD<MatrixQ>  DenseMatrixClass;

  DenseMatrixClass djacPDE_q(nDOFPDE, nDOFPDE);
  DenseMatrixClass djacPDE_lg(nDOFPDE, nDOFBC);
  DenseMatrixClass djacBC_q(nDOFBC, nDOFPDE);
  DenseMatrixClass djacBC_lg(nDOFBC, nDOFBC);

  djacPDE_q = 0;
  djacPDE_lg = 0;
  djacBC_q = 0;
  djacBC_lg = 0;

  // dense jacobian

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnCell, djacPDE_q), xfld, qfld, quadratureOrder, 1 );

   IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcnBC, djacPDE_q, djacPDE_lg, djacBC_q, djacBC_lg),
      xfld, qfld, lgfld, quadratureOrder, 4 );


  // sparse linear system setup

  typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;

  // jacobian nonzero pattern

  typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

  NonZeroPatternClass nzPDE_q(nDOFPDE, nDOFPDE);
  NonZeroPatternClass nzPDE_lg(nDOFPDE, nDOFBC);
  NonZeroPatternClass nzBC_q(nDOFBC, nDOFPDE);
  NonZeroPatternClass nzBC_lg(nDOFBC, nDOFBC);

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnCell, nzPDE_q), xfld, qfld, quadratureOrderMin, 1 );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
     JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcnBC, nzPDE_q, nzPDE_lg, nzBC_q, nzBC_lg),
     xfld, qfld, lgfld, quadratureOrderMin, 4 );


  // sparse jacobian

  SparseMatrixClass jacPDE_q(nzPDE_q);
  SparseMatrixClass jacPDE_lg(nzPDE_lg);
  SparseMatrixClass jacBC_q(nzBC_q);
  SparseMatrixClass jacBC_lg(nzBC_lg);

  jacPDE_q = 0;
  jacPDE_lg = 0;
  jacBC_q = 0;
  jacBC_lg = 0;

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnCell, jacPDE_q), xfld, qfld, quadratureOrder, 1 );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
     JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcnBC, jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg),
     xfld, qfld, lgfld, quadratureOrder, 4 );


  BOOST_TEST_CHECKPOINT( "PDE_q" );
  checkDenseSparseEquality(djacPDE_q, jacPDE_q);

  BOOST_TEST_CHECKPOINT( "PDE_lg" );
  checkDenseSparseEquality(djacPDE_lg, jacPDE_lg);

  BOOST_TEST_CHECKPOINT( "BC_q" );
  checkDenseSparseEquality(djacBC_q, jacBC_q);

  BOOST_TEST_CHECKPOINT( "BC_lg" );
  checkDenseSparseEquality(djacBC_lg, jacBC_lg);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
