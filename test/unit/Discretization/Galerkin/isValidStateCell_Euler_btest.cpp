// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// isValidStateCell_Euler_btest
// testing of cell physicality check for Galerkin with Euler

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

//#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
//#include "Field/FieldVolume_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"
//#include "Field/FieldVolume_CG_Cell.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/isValidState/isValidStateCell.h"
#include "Discretization/IntegrateCellGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( isValidStateCell_AD_test_suite )

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidStateCell_1D_Galerkin_Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 1;
  bool isValidState = true;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  IntegrateCellGroups<TopoD1>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );


}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidStateCell_2D_Galerkin_1Triangle_X1Q1 )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  Real rho, u, v, t;

  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 1;
  bool isValidState = true;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, true );

  // Now for a non-physical density
  rho = -1.137;
  qDataPrim[0] = rho;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  isValidState = true;
  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical temperature
  rho = 1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[3] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  isValidState = true;
  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical density and temperature
  rho = -1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[3] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  isValidState = true;
  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidStateCell_2D_Galerkin_1Triangle_X1Q2 )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  Real rho, u, v, t;

  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P2 (aka Q2)
  int qorder = 2;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution field variable
  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  int nodeMap[3] = {0};
  int edgeMap[3] = {0};
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle>& qfldCells = qfld.getCellGroup<Triangle>(0);
  qfldCells.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );
  BOOST_CHECK_EQUAL( 5, nodeMap[2] );

  qfldCells.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 1, edgeMap[1] );
  BOOST_CHECK_EQUAL( 0, edgeMap[2] );

  // solution data
  qfld.DOF(nodeMap[0]) =  q;
  qfld.DOF(nodeMap[1]) =  q;
  qfld.DOF(nodeMap[2]) =  q;
  qfld.DOF(edgeMap[0]) =  q;
  qfld.DOF(edgeMap[1]) =  q;
  qfld.DOF(edgeMap[2]) =  q;

  // quadrature rule: basis grad is linear, flux is quadratic)
  int quadratureOrder = 3;
  bool isValidState = true;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, true );

  // Now for a non-physical density
  rho = -1.137;
  qDataPrim[0] = rho;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(nodeMap[0]) =  q;
  qfld.DOF(nodeMap[1]) =  q;
  qfld.DOF(nodeMap[2]) =  q;
  qfld.DOF(edgeMap[0]) =  q;
  qfld.DOF(edgeMap[1]) =  q;
  qfld.DOF(edgeMap[2]) =  q;
  isValidState = true;
  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical temperature
  rho = 1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[3] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(nodeMap[0]) =  q;
  qfld.DOF(nodeMap[1]) =  q;
  qfld.DOF(nodeMap[2]) =  q;
  qfld.DOF(edgeMap[0]) =  q;
  qfld.DOF(edgeMap[1]) =  q;
  qfld.DOF(edgeMap[2]) =  q;
  isValidState = true;
  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical density and temperature
  rho = -1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[3] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(nodeMap[0]) =  q;
  qfld.DOF(nodeMap[1]) =  q;
  qfld.DOF(nodeMap[2]) =  q;
  qfld.DOF(edgeMap[0]) =  q;
  qfld.DOF(edgeMap[1]) =  q;
  qfld.DOF(edgeMap[2]) =  q;
  isValidState = true;
  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidStateCell_2D_Galerkin_1Quad_X1Q1 )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  Real rho, u, v, t;

  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Quad_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;

  // quadrature rule (quadratic: basis grad is linear, flux is linear)
  int quadratureOrder = 2;
  bool isValidState = true;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );

  BOOST_CHECK_EQUAL( isValidState, true );

  // Now for a non-physical density
  rho = -1.137;
  qDataPrim[0] = rho;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;
  isValidState = true;
  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical temperature
  rho = 1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[3] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;
  isValidState = true;
  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical density and temperature
  rho = -1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[3] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;
  isValidState = true;
  IntegrateCellGroups<TopoD2>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidStateCell_3D_Galerkin_1Tet_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3,  PDEAdvectionDiffusion3D > PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single tetrahedron, P1 (aka X1)
  XField3D_1Tet_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single tetrahedron, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  qfld.DOF(3) = 6;

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 1;
  bool isValidState = true;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  IntegrateCellGroups<TopoD3>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsd1, rsdPDEElem[0], tol );
  BOOST_CHECK_CLOSE( rsd2, rsdPDEElem[1], tol );
  BOOST_CHECK_CLOSE( rsd3, rsdPDEElem[2], tol );
  BOOST_CHECK_CLOSE( rsd4, rsdPDEElem[3], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidStateCell_3D_Galerkin_1Hex_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3,  PDEAdvectionDiffusion3D > PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single hexahedron, P1 (aka X1)
  XField3D_1Hex_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 8, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 8, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  qfld.DOF(3) = 6;

  qfld.DOF(4) = 8;
  qfld.DOF(5) = 2;
  qfld.DOF(6) = 9;
  qfld.DOF(7) = 7;

  // quadrature rule (quadratic: basis grad is linear, flux is linear)
  int quadratureOrder = 2;
  bool isValidState = true;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  IntegrateCellGroups<TopoD3>::integrate( isValidStateCell(pde, fcnint.cellGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsd1, rsdPDEElem[0], tol );
  BOOST_CHECK_CLOSE( rsd2, rsdPDEElem[1], tol );
  BOOST_CHECK_CLOSE( rsd3, rsdPDEElem[2], tol );
  BOOST_CHECK_CLOSE( rsd4, rsdPDEElem[3], tol );

  BOOST_CHECK_CLOSE( rsd5, rsdPDEElem[4], tol );
  BOOST_CHECK_CLOSE( rsd6, rsdPDEElem[5], tol );
  BOOST_CHECK_CLOSE( rsd7, rsdPDEElem[6], tol );
  BOOST_CHECK_CLOSE( rsd8, rsdPDEElem[7], tol );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
