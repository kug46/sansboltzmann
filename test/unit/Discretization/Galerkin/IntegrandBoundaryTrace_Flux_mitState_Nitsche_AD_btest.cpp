// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_Flux_mitState_Galerkin_AD_btest
// testing of boundary integrands: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None> PDEAdvectionDiffusion2D;
//typedef PDEAdvectionDiffusion<PhysD3,
//                              AdvectiveFlux3D_Uniform,
//                              ViscousFlux3D_Uniform,
//                              Source3D_None> PDEAdvectionDiffusion3D;

typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
//typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass3D;

typedef BCTypeFunction_mitState<AdvectiveFlux1D_Uniform,ViscousFlux1D_Uniform> BCTypeFunction1D;
typedef BCTypeFunction_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCTypeFunction2D;

typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeFunction1D> > BCClass1D;
typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeFunction2D> > BCClass2D;
//typedef BCNDConvertSpace<PhysD3, BCAdvectionDiffusion<PhysD3,BCTypeFunction3D> > BCClass3D;

typedef NDVectorCategory<boost::mpl::vector1<BCClass1D>, BCClass1D::Category> NDBCVecCat1D;
typedef NDVectorCategory<boost::mpl::vector1<BCClass2D>, BCClass2D::Category> NDBCVecCat2D;
//typedef NDVectorCategory<boost::mpl::vector1<BCClass3D>, BCClass3D::Category> NDBCVecCat3D;

typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldArea_Triangle;
typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldArea_Quad;
//typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldVolume_Tet;
//typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldVolume_Hex;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, Galerkin>
               ::BasisWeighted<Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::BasisWeighted<Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::BasisWeighted<Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::BasisWeighted<Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::BasisWeighted<Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::FieldWeighted<Real, Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::FieldWeighted<Real, Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_Flux_mitState_Galerkin_AD_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Line_X1Q1R1_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeFunction1D> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 2.4;
  BCClass::Function_ptr uexact(new ScalarFunction1D_Const( uB ));
  bool upwind = true;
  BCClass bc(uexact, adv, visc, BCClass::ParamsType::params.SolutionBCType.Dirichlet, upwind);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() = 1;

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // line solution (left)
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // integrand functor
  StabilizationNitsche stab(order);
  IntegrandClass fcnint( pde, bc, {0}, stab );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  BasisWeightedClass fcnPDE = fcnint.integrand( xnode, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 2, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrandPDETrue[2];
  ArrayQ integrandPDE[2];

  sRef = 0;
  fcnPDE( sRef, integrandPDE, 2 );

  Real gradphi1 = -1;
  Real gradphi2 = 1;
  const Real lengthL = fabs(x2 - x1);
  const Real Cb = 2.0;

  //PDE residual integrands (left): (advective) + (viscous) + (DC term) + (Nitsche Param)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + (-gradphi1*kxx*(3.-uB)) + ( 0 ) ;  // Basis function 1
  integrandPDETrue[1] = ( 33./10. ) + ( -2123./1000. )  + (-gradphi2*kxx*(3.-uB)) + (4*Cb*kxx*lengthL)*(3.-uB);  // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 0;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, ArrayQ> integralPDE(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[2] = {0,0};
  Real rsdPDETrue[2];

  // cell integration for canonical element
  integralPDE( fcnPDE, xnode, rsdPDEElem, nIntegrand );

  //PDE residuals (left): (advective) + (viscous) + (DC term) + (Nitsche Param)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + (-gradphi1*kxx*(3.-uB)) + ( 0 );  // Basis function 1
  rsdPDETrue[1] = ( 33./10. ) + ( -2123./1000. )  + (-gradphi2*kxx*(3.-uB)) + (4*Cb*kxx*lengthL)*(3.-uB);  // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEElem[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Line_X1Q1R1W2S2_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeFunction1D> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> FieldWeightedClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 12./5.;
  BCClass::Function_ptr uexact(new ScalarFunction1D_Const( uB ));
  bool upwind = true;
  BCClass bc(uexact, adv, visc, BCClass::ParamsType::params.SolutionBCType.Dirichlet, upwind);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() = 1;

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // line solution (left)
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // weight
  ElementQFieldCell wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 3, wfldElem.nDOF() );

  // line solution (left)
  wfldElem.DOF(0) = 3;
  wfldElem.DOF(1) = 4;
  wfldElem.DOF(2) = 5;

  Element<Real,TopoD1,Line> efldElem( 0, BasisFunctionCategory_Legendre);

  // integrand functor
  StabilizationNitsche stab(order);
  IntegrandClass fcnint( pde, bc, {0}, stab );

  FieldWeightedClass fcn = fcnint.integrand( xnode,
                                             CanonicalTraceToCell(0, 1),
                                             xfldElem,
                                             qfldElem,
                                             wfldElem,
                                             efldElem);

  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrand[1]={0};
  Real integrandPDETrue;

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrand, 1 );

  const Real Cb = 2.0;
  const Real lengthL = fabs(x2-x1);
  Real wx = 0;
  Real wend = wfldElem.eval( 1.0 );
  wfldElem.evalDerivative(1.0, wx );

  //PDE residual integrands (left): (advective) + (viscous) + (DC term) + (Nitsche Param)
//  integrandPDETrue = ( 66./5. ) + ( -2123./250. );  // Basis function
  integrandPDETrue = ( 66./5. ) + ( -2123./250. )  + (-wx*kxx*(3.-uB)) + wend*(4*Cb*kxx*lengthL)*(3.-uB);  // Basis function
  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 0;
  GalerkinWeightedIntegral<TopoD0, Node, Real> integral(quadratureorder,1);

  Real rsdElem[1]={0};
  Real rsdPDETrue;

  // cell integration for canonical element
  integral( fcn, xnode, rsdElem, 1 );

  //PDE residuals (left): (advective) + (viscous) + (DC term) + (Nitsche Param)
//  rsdPDETrue = ( 66./5. ) + ( -2123./250. ); // Basis function
  rsdPDETrue = ( 66./5. ) + ( -2123./250. )  + (-wx*kxx*(3.-uB)) + wend*(4*Cb*kxx*lengthL)*(3.-uB);  // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem[0], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeFunction1D> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> FieldWeightedClass;

  // PDE
  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 12./5.;
  BCClass::Function_ptr uexact(new ScalarFunction1D_Const( uB ));
  bool upwind = true;
  BCClass bc(uexact, adv, visc, BCClass::ParamsType::params.SolutionBCType.Dirichlet, upwind);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() = 1;

  Element<Real,TopoD1,Line> efldElem(0, BasisFunctionCategory_Legendre );
  for ( int qorder = 2; qorder < 3; qorder++ )
  {
    //solution
    ElementQFieldCell qfldElem(qorder, BasisFunctionCategory_Hierarchical );
    ElementQFieldCell wfldElem(qorder, BasisFunctionCategory_Hierarchical );

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElem.nDOF() );

    BOOST_CHECK_EQUAL( qfldElem.nDOF(), wfldElem.nDOF() );
    // line solution
    for ( int dof = 0; dof < qfldElem.nDOF(); dof++ )
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
    }

    // integrand functor
    StabilizationNitsche stab(qorder);
    IntegrandClass fcnint( pde, bc, {0}, stab );

    BasisWeightedClass fcnPDEB = fcnint.integrand( xnode,
                                                   CanonicalTraceToCell(0, 1),
                                                   xfldElem,
                                                   qfldElem );

    FieldWeightedClass fcnW = fcnint.integrand( xnode,
                                                CanonicalTraceToCell(0, 1),
                                                xfldElem,
                                                qfldElem,
                                                wfldElem,
                                                efldElem);

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    const int nDOF = 3; // want to be qorder+1, but can't
    int nIntegrand = qfldElem.nDOF();
    int quadratureorder = 0;

    GalerkinWeightedIntegral<TopoD0, Node, ArrayQ> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD0, Node, Real> integralW(quadratureorder, efldElem.nDOF());

    ArrayQ rsdPDEElemB[nDOF] = {0,0};
    Real rsdElemW[1] = {0};

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xnode, rsdPDEElemB, nIntegrand );

    for ( int i = 0; i < wfldElem.nDOF(); i++ )
    {
      // set just one of the elements to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical Element
      rsdElemW[0] = 0;
      integralW( fcnW, xnode, rsdElemW, 1 );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemW[0], rsdPDEElemB[i], small_tol, close_tol );

      // reset to 0
      wfldElem.DOF(i) = 0;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Triangle_X1Q1R1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeFunction2D> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 12./5.;
  BCClass::Function_ptr uexact(new ScalarFunction2D_Const( uB ));
  bool upwind = true;
  BCClass bc(uexact, adv, visc, BCClass::ParamsType::params.SolutionBCType.Dirichlet, upwind);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // integrand functor
  StabilizationNitsche stab(order);
  IntegrandClass fcnint( pde, bc, {0}, stab );

  BasisWeightedClass fcnPDE = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 3, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;
  RefCoordTraceType sRef;
  Real integrandPDETrue[3];
  ArrayQ integrandPDE[3];

  Real Cb = 1.5;

  //See Galerkin_Stabilized_AD.nb for the following:
  sRef = 0;
  fcnPDE( sRef, integrandPDE, 3 );

  //PDE residual integrands (left): (advective) + (viscous) + (DC term) + (Nitsche Param)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + (12483./(5000.*sqrt(2))) + Cb*(0);   // Basis function 1
  integrandPDETrue[1] = ( 39/(10.*sqrt(2)) ) + ( -4941./(500.*sqrt(2)) ) + (-2007./(1250.*sqrt(2))) + Cb*(12483./(625.*sqrt(2))); // Basis function 2
  integrandPDETrue[2] = ( 0 ) + ( 0 )  + (-891./(1000.*sqrt(2))) + Cb*(0);

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );


  sRef = 1;
  fcnPDE( sRef, integrandPDE, 3 );

  //PDE residual integrands (left): (advective) + (viscous) + (DC term) + (Nitsche Param)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + (4161./(625.*sqrt(2.))) + Cb*(0);
  integrandPDETrue[1] = ( 0 ) + ( 0 ) + (-1338.*sqrt(2)/625) + Cb*(0);
  integrandPDETrue[2] = ( (13*sqrt(2))/5. ) + (-4941./(500.*sqrt(2))) + (-297./(125.*sqrt(2))) + Cb*(16644.*sqrt(2)/625.);

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );


  sRef = 0.5;
  fcnPDE( sRef, integrandPDE, 3 );

  //PDE residual integrands (left): (advective) + (viscous) + (DC term) + (Nitsche Param)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + (45771./(10000.*sqrt(2.))) + Cb*(0);
  integrandPDETrue[1] = ( 91./(40.*sqrt(2)) ) + ( -4941./(1000.*sqrt(2)) ) + (-7359./(2500.*sqrt(2))) + Cb*(45771./(2500.*sqrt(2)));
  integrandPDETrue[2] = ( 91./(40.*sqrt(2)) ) + ( -4941./(1000.*sqrt(2)) ) + (-3267./(2000.*sqrt(2))) + Cb*(45771./(2500.*sqrt(2)));

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );


  // test the trace element integral of the functor
  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralPDE(quadratureorder, nIntegrandL);

  ArrayQ rsdPDEElem[3] = {0,0,0};
  Real rsdPDETrue[3];

  // cell integration for canonical element
  integralPDE( fcnPDE, xedge, rsdPDEElem, nIntegrandL );

  //PDE residuals (left): (advective) + (viscous)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + (45771./10000.) + Cb*(0);
  rsdPDETrue[1] = ( 13./6. ) + ( -4941./1000. ) + (-7359./2500.) + Cb*(9709./625.);   // Basis function 2
  rsdPDETrue[2] = ( 143./60. ) + ( -4941./1000. ) + (-3267./2000.) + Cb*(26353./1250.);   // Basis function 3

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDEElem[2], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Triangle_X1Q1R1W2S2_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeFunction2D> > BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle, ElementXFieldCell> FieldWeightedClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 12./5.;
  BCClass::Function_ptr uexact(new ScalarFunction2D_Const( uB ));
  bool upwind = true;
  BCClass bc(uexact, adv, visc, BCClass::ParamsType::params.SolutionBCType.Dirichlet, upwind);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // weight
  ElementQFieldCell wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution (left)
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  Element<Real,TopoD2,Triangle> efldElem(0, BasisFunctionCategory_Legendre);

  // integrand functor
  StabilizationNitsche stab(order);
  IntegrandClass fcnint( pde, bc, {0}, stab );

  FieldWeightedClass fcn = fcnint.integrand( xedge,
                                             CanonicalTraceToCell(0, 1),
                                             xfldElem,
                                             qfldElem,
                                             wfldElem,
                                             efldElem);

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;

  Real Cb = 1.5;

  Real integrandPDETrue;
  Real integrand[1]={0};

  // Test at sRef={0}, {s,t}={1, 0}
  sRef = {0};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands (left): (advective) + (viscous)
  integrandPDETrue = ( (39*sqrt(2))/5. ) + ( -4941/(125.*sqrt(2)) )
                     + (-31203./(1000.*sqrt(2))) + Cb*((24966*sqrt(2))/625.);  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );


  // Test at sRef={1}, {s,t}={0, 1}
  sRef = {1};
  fcn( sRef, integrand,1 );

  //PDE residual integrands (left): (advective) + (viscous)
  integrandPDETrue = ( (39*sqrt(2))/5. ) + (-14823./(500.*sqrt(2)))
                      + (21687./(625.*sqrt(2))) + Cb*((49932*sqrt(2))/625.);  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );


  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef = {1./2.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands (left): (advective) + (viscous)
  integrandPDETrue = ( 1001./(40.*sqrt(2)) ) + ( -54351./(1000.*sqrt(2)) )
                     + (-166749./(10000.*sqrt(2))) + Cb*(503481./(2500.*sqrt(2)));  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );


  // Test at sRef={1/5}, {s,t}={4/5, 1/5}
  sRef = {1./5.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands (left): (advective) + (viscous)
  integrandPDETrue = ( (6604*sqrt(2))/625. ) + ( -627507/(12500.*sqrt(2)) )
                     + (-186333./(6250.*sqrt(2))) + Cb*(1056894.*sqrt(2)/15625.);  // Weight function

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = -1;
  GalerkinWeightedIntegral<TopoD1, Line, Real> integral(quadratureorder,efldElem.nDOF());

  Real rsdElem[1]={0};
  Real rsdPDETrue;

  // cell integration for canonical element
  integral( fcn, xedge, rsdElem, 1 );

  //PDE residuals (left): (advective) + (viscous)
  rsdPDETrue = ( 1313./60. ) + ( -47763./1000. ) + (-105339./10000.) + Cb*(217759./1250.);  // Weight function

  SANS_CHECK_CLOSE( rsdPDETrue, rsdElem[0], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeFunction2D> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> FieldWeightedClass;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // BC
  Real uB = 12./5.;
  BCClass::Function_ptr uexact(new ScalarFunction2D_Const( uB ));
  bool upwind = true;
  BCClass bc(uexact, adv, visc, BCClass::ParamsType::params.SolutionBCType.Dirichlet, upwind);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  Element<Real,TopoD2,Triangle> efldElem( 0, BasisFunctionCategory_Legendre );

  for ( int qorder = 2; qorder < 3; qorder++ )
  {
    //solution
    ElementQFieldCell qfldElem(qorder, BasisFunctionCategory_Hierarchical );
    ElementQFieldCell wfldElem(qorder, BasisFunctionCategory_Hierarchical );

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    BOOST_CHECK_EQUAL( qfldElem.nDOF(), wfldElem.nDOF() );
    // line solution
    for ( int dof = 0; dof < qfldElem.nDOF(); dof++ )
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
    }

    // integrand functor
    StabilizationNitsche stab(qorder);
    IntegrandClass fcnint( pde, bc, {0}, stab );

    BasisWeightedClass fcnPDEB = fcnint.integrand( xedge,
                                                   CanonicalTraceToCell(0, 1),
                                                   xfldElem,
                                                   qfldElem );

    FieldWeightedClass fcnW = fcnint.integrand( xedge,
                                                CanonicalTraceToCell(0, 1),
                                                xfldElem,
                                                qfldElem,
                                                wfldElem,
                                                efldElem);

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    const int nDOF = 6; // want to be qorder+1, but can't
    int nIntegrand = qfldElem.nDOF();
    int quadratureorder = 4;

    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralPDEB(quadratureorder, nIntegrand);
    GalerkinWeightedIntegral<TopoD1, Line, Real> integralW(quadratureorder, 1);

    ArrayQ rsdPDEElemB[nDOF] = {0,0};
    Real rsdElemW[1] = {0};

    // cell integration for canonical element
    integralPDEB( fcnPDEB, xedge, rsdPDEElemB, nIntegrand );

    for ( int i = 0; i < wfldElem.nDOF(); i++ )
    {
      // set just one of the elements to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical Element
      rsdElemW[0] = 0;
      integralW( fcnW, xedge, rsdElemW, 1 );

      // test the two integrands are the same
      SANS_CHECK_CLOSE ( rsdElemW[0], rsdPDEElemB[i], small_tol, close_tol );

      // reset to 0
      wfldElem.DOF(i) = 0;
    }
  }
}




//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
