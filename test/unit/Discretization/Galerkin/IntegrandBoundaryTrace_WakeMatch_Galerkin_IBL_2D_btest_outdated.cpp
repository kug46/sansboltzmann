// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// test of IntegrandBoundaryTrace_WakeMatch_Galerkin_IBL of IBL2D

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_WakeMatch_Galerkin_IBL.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Tuple/ElementTuple.h"

#include "pde/IBL/PDEIBL2D.h"
#include "pde/IBL/BCIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

using namespace std;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarTypeDANCt VarType;
typedef VarInterpret2D<VarType> VarInterpType;
typedef VarData2DDANCt VarDataType;

typedef PDEIBL<PhysDim,VarType> PDEClass;
typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef PDEClass::template ArrayParam<Real> ArrayParam;
typedef PDEClass::template VectorX<Real> VectorX;
typedef DLA::VectorS<PhysDim::D,VectorX> VectorVectorX;

typedef BCIBL2D<BCTypeWakeMatch,VarType> BCClass;
typedef BCNDConvertSpace<PhysDim, BCClass> NDBCClass;
typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

typedef BCIBL2DParams<BCTypeWakeMatch> BCParamsType;

typedef ElementXField<PhysDim,TopoD1,Line> ElementXFieldClass;
typedef ElementXField<PhysDim,TopoD0,Node> ElementXFieldTraceClass;
typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
typedef Element<ArrayQ,TopoD0,Node> ElementQHubTraceClass;
typedef Element<ArrayParam,TopoD1,Line> ElementParamFieldClass;
typedef Element<VectorX,TopoD1,Line> ElementVFieldClass;

typedef ElementXFieldTraceClass::RefCoordType RefCoordTraceType;

typedef MakeTuple<ElementTuple, ElementParamFieldClass,
                                ElementXFieldClass>::type ElementParamType;

typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin_manifold> IntegrandClass;
typedef typename IntegrandClass::template BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParamType> BasisWeightedPDEClass;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin_manifold>::
BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParamType>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_WakeMatch_Galerkin_IBL_2D_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Line_X1Q1_TrailingEdge_test )
{
  // static tests
  BOOST_CHECK( PDEClass::N == IntegrandClass::N );

  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);

  // BC

  PyDict bcArgs;
  bcArgs[NDBCClass::ParamsType::params.matchingType] = NDBCClass::ParamsType::params.matchingType.trailingEdge;

  BCParamsType BCParamIBL2D;
  BCParamIBL2D.checkInputs(bcArgs);

  NDBCClass bc(pde,bcArgs);

  // ----------------------- FIELD ELEMENTS ----------------------- //
  const int order = 1;

  // Grid
  VectorX X1 = { 0.000, 0.000 };
  VectorX X2 = { 0.600, 0.800 };

  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);
  xfldElem.DOF(0) = X1;
  xfldElem.DOF(1) = X2;

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTraceClass xnode(0, BasisFunctionCategory_Legendre);
  xnode.DOF(0) = X2;
  xnode.normalSignL() = 1;

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // Parameter: edge velocity field
  const Real qxi_L_0 = 4.000, qzi_L_0 = 2.000, p0_L_0 = 1.100e+05, T0_L_0 = 290.00;
  const Real qxi_L_1 = 3.000, qzi_L_1 = 4.000, p0_L_1 = 1.000e+05, T0_L_1 = 350.00;

  ElementQFieldClass vfldElem(order, BasisFunctionCategory_Hierarchical);

  vfldElem.DOF(0) = { qxi_L_0, qzi_L_0 };
  vfldElem.DOF(1) = { qxi_L_1, qzi_L_1 };

  // Parameter: edge velocity gradient fields
  std::vector<VectorX> gradphi0(vfldElem.nDOF()), gradphi1(vfldElem.nDOF());
  xfldElem.evalBasisGradient( 0.0, xfldElem, gradphi0.data(), gradphi0.size() );
  xfldElem.evalBasisGradient( 1.0, xfldElem, gradphi1.data(), gradphi1.size() );

  VectorVectorX gradv0, gradv1; // grad(v) at node 0 and 1 of element
  vfldElem.evalFromBasis( gradphi0.data(), gradphi0.size(), gradv0 );
  vfldElem.evalFromBasis( gradphi1.data(), gradphi1.size(), gradv1 );

  // Parameter field
  ElementParamFieldClass paramElem(order, BasisFunctionCategory_Hierarchical);

  paramElem.DOF(0) = {qxi_L_0, qzi_L_0, gradv0[0][0], gradv0[1][0], gradv0[0][1], gradv0[1][1], p0_L_0, T0_L_0};
  paramElem.DOF(1) = {qxi_L_1, qzi_L_1, gradv1[0][0], gradv1[1][0], gradv1[0][1], gradv1[1][1], p0_L_1, T0_L_1};

  // Solution
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // State vector at L element basis fcn 0
  const Real delta_0 = 0.1200;
  const Real A_0     = 2.9000;

  // State vector at L element basis fcn 1
  const Real delta_1 = 0.0800;
  const Real A_1     = 3.3000;

  // line solution (left)
  qfldElem.DOF(0) = pde.setDOFFrom( VarDataType(delta_0,A_0) );
  qfldElem.DOF(1) = pde.setDOFFrom( VarDataType(delta_1,A_1) );

  // hubtrace DOFs are dummy here
  ElementQHubTraceClass hbfldElem(0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( 0, hbfldElem.order() );
  BOOST_CHECK_EQUAL( 1, hbfldElem.nDOF() );

  // integrand functor
  const std::vector<int> boundaryGroups = {0};
  IntegrandClass integrandBoundaryTrace(pde, bc, boundaryGroups);

  BOOST_CHECK(boundaryGroups.size() == integrandBoundaryTrace.nBoundaryGroups());

  for (std::size_t i = 0; i < boundaryGroups.size(); ++i)
  {
    BOOST_CHECK_EQUAL(boundaryGroups[i], integrandBoundaryTrace.boundaryGroup(i));
    BOOST_CHECK_EQUAL(boundaryGroups[i], integrandBoundaryTrace.getBoundaryGroups()[i]);
  }

  //Explicitly create a paramater instance.
  //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior
  ElementParamType tupleElem = (paramElem, xfldElem);
  BasisWeightedPDEClass integrandBasisWeighted =
      integrandBoundaryTrace.integrand(xnode, CanonicalTraceToCell(0,0), tupleElem, qfldElem, hbfldElem);

  BOOST_CHECK( true == integrandBasisWeighted.needsEvaluation() );
  BOOST_CHECK( qfldElem.nDOF() == integrandBasisWeighted.nDOFCell() );
  BOOST_CHECK( hbfldElem.nDOF() == integrandBasisWeighted.nDOFTrace() );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const int nBasis_cell = 2;
  const int nBasis_trace = 1;

  RefCoordTraceType sRefTrace;

  ArrayQ integrandCellTrue[nBasis_cell];
  ArrayQ integrandCell[nBasis_cell];

  ArrayQ integrandTraceTrue[nBasis_trace];
  ArrayQ integrandTrace[nBasis_trace];

  sRefTrace = 0;
  integrandBasisWeighted(sRefTrace, integrandCell, nBasis_cell, integrandTrace, nBasis_trace);

  integrandCellTrue[0] = 0; // Basis function 1
  integrandCellTrue[1] = { 2.1686909160164361e-01,
                           1.7360439378074395e+00 }; // Basis function 2

  integrandTraceTrue[0] = { 2.0182805577715117e-02 + fabs(xnode.DOF(0)[1]),
                            8.7191289321164912e-03 }; // Basis function 1

  for (int ibasis = 0; ibasis < nBasis_cell; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandCellTrue[ibasis][n], integrandCell[ibasis][n], small_tol, close_tol );
    }
  }

  for (int ibasis = 0; ibasis < nBasis_trace; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandTraceTrue[ibasis][n], integrandTrace[ibasis][n], small_tol, close_tol );
    }
  }

  // test the trace element integral of the functor
  const int quadratureorder = 0;
  const int nIntegrand_cell = qfldElem.nDOF();
  const int nIntegrand_trace = hbfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, ArrayQ, ArrayQ> integralBC(quadratureorder, nIntegrand_cell, nIntegrand_trace);

  ArrayQ rsdPDEElem[nBasis_cell] = {0,0};
  ArrayQ rsdPDETrue[nBasis_cell] = {0,0};
  rsdPDETrue[0] = integrandCellTrue[0];
  rsdPDETrue[1] = integrandCellTrue[1];

  ArrayQ rsdHTElem[nBasis_trace] = {0};
  ArrayQ rsdHTTrue[nBasis_trace] = {0};
  rsdHTTrue[0] = integrandTraceTrue[0];

  // cell integration for canonical element
  integralBC(integrandBasisWeighted, xnode, rsdPDEElem, nIntegrand_cell, rsdHTElem, nIntegrand_trace);

  // check
  for (int ibasis = 0; ibasis < nBasis_cell; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( rsdPDETrue[ibasis][n], rsdPDEElem[ibasis][n], small_tol, close_tol );
    }
  }

  for ( int nbasis = 0; nbasis < nBasis_trace; nbasis++ )
  {
    for ( int ieqn = 0; ieqn < pde.N; ieqn++ )
    {
      SANS_CHECK_CLOSE( rsdHTTrue[nbasis][ieqn], rsdHTTrue[nbasis][ieqn], small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Line_X1Q1_WakeInflow_test )
{
  // PDE
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarWake);

  // BC
  PyDict bcArgs;
  bcArgs[NDBCClass::ParamsType::params.matchingType] = NDBCClass::ParamsType::params.matchingType.wakeInflow;

  BCParamsType BCParamIBL2D;
  BCParamIBL2D.checkInputs(bcArgs);

  NDBCClass bc(pde,bcArgs);

  // ----------------------- FIELD ELEMENTS ----------------------- //
  const int order = 1;

  // Grid
  VectorX X1 = { 0.000, 0.000 };
  VectorX X2 = { 0.600, 0.800 };

  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);
  xfldElem.DOF(0) = X1;
  xfldElem.DOF(1) = X2;

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTraceClass xnode(0, BasisFunctionCategory_Legendre);
  xnode.DOF(0) = X1;
  xnode.normalSignL() = -1;

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // Parameter: edge velocity field
  const Real qxi_L_0 = 4.000, qzi_L_0 = 2.000, p0_L_0 = 1.100e+05, T0_L_0 = 290.00;
  const Real qxi_L_1 = 3.000, qzi_L_1 = 4.000, p0_L_1 = 1.000e+05, T0_L_1 = 350.00;

  ElementQFieldClass vfldElem(order, BasisFunctionCategory_Hierarchical);

  vfldElem.DOF(0) = { qxi_L_0, qzi_L_0 };
  vfldElem.DOF(1) = { qxi_L_1, qzi_L_1 };

  // Parameter: edge velocity gradient fields
  std::vector<VectorX> gradphi0(vfldElem.nDOF()), gradphi1(vfldElem.nDOF());
  xfldElem.evalBasisGradient( 0.0, xfldElem, gradphi0.data(), gradphi0.size() );
  xfldElem.evalBasisGradient( 1.0, xfldElem, gradphi1.data(), gradphi1.size() );

  VectorVectorX gradv0, gradv1; // grad(v) at node 0 and 1 of element
  vfldElem.evalFromBasis( gradphi0.data(), gradphi0.size(), gradv0 );
  vfldElem.evalFromBasis( gradphi1.data(), gradphi1.size(), gradv1 );

  // Parameter field
  ElementParamFieldClass paramElem(order, BasisFunctionCategory_Hierarchical);

  paramElem.DOF(0) = {qxi_L_0, qzi_L_0, gradv0[0][0], gradv0[1][0], gradv0[0][1], gradv0[1][1], p0_L_0, T0_L_0};
  paramElem.DOF(1) = {qxi_L_1, qzi_L_1, gradv1[0][0], gradv1[1][0], gradv1[0][1], gradv1[1][1], p0_L_1, T0_L_1};

  // Solution
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // State vector at L element basis fcn 0
  const Real delta_0 = 0.1200;
  const Real A_0     = 2.9000;

  // State vector at L element basis fcn 1
  const Real delta_1 = 0.0800;
  const Real A_1     = 3.3000;

  // line solution (left)
  qfldElem.DOF(0) = pde.setDOFFrom( VarDataType(delta_0,A_0) );
  qfldElem.DOF(1) = pde.setDOFFrom( VarDataType(delta_1,A_1) );

  ElementQHubTraceClass hbfldElem(0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( 0, hbfldElem.order() );
  BOOST_CHECK_EQUAL( 1, hbfldElem.nDOF() );

  // State vector at hubtrace
  hbfldElem.DOF(0) = {-0.66, -0.77};

  // integrand functor
  IntegrandClass integrandBoundaryTrace(pde, bc, {0});

  //Explicitly create a paramater instance.
  //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior
  ElementParamType tupleElem = (paramElem, xfldElem);
  BasisWeightedPDEClass integrandBasisWeighted =
      integrandBoundaryTrace.integrand(xnode, CanonicalTraceToCell(1,0), tupleElem, qfldElem, hbfldElem);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const int nBasis_cell = 2;
  const int nBasis_trace = 1;

  RefCoordTraceType sRefTrace;

  ArrayQ integrandCellTrue[nBasis_cell];
  ArrayQ integrandCell[nBasis_cell];

  ArrayQ integrandTraceTrue[nBasis_trace];
  ArrayQ integrandTrace[nBasis_trace];

  sRefTrace = 0;
  integrandBasisWeighted(sRefTrace, integrandCell, nBasis_cell, integrandTrace, nBasis_trace);

#if IsMatchWakeUnknowns // computing wake thicknesses using wake unknowns --> strong Dirichlet BC and correct
  integrandCellTrue[0] = -hbfldElem.DOF(0); // Basis function 1
  integrandCellTrue[1] = 0; // Basis function 2
#else // computing wake thicknesses using hub trace unknowns --> still imposing Dirichlet BC weakly which does not work
  integrandCellTrue[0] = { -2.9247063778654186e-01,
                           -2.3443256781635280e+00 }; // Basis function 1
  integrandCellTrue[1] = 0; // Basis function 2
#endif

#if IsMatchWakeUnknowns // computing wake thicknesses using wake unknowns --> strong Dirichlet BC and correct
  integrandTraceTrue[0] = { -3.0599999999999995e-02,
                            -1.6666071428571433e-02 }; // Basis function 1
#else // computing wake thicknesses using hub trace unknowns --> still imposing Dirichlet BC weakly which does not work
    integrandTraceTrue[0] = { -2.4999999999999994e-02,
                              -1.3839285714285715e-02 }; // Basis function 1
#endif

  for (int ibasis = 0; ibasis < nBasis_cell; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandCellTrue[ibasis][n], integrandCell[ibasis][n], small_tol, close_tol );
    }
  }

  for (int ibasis = 0; ibasis < nBasis_trace; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandTraceTrue[ibasis][n], integrandTrace[ibasis][n], small_tol, close_tol );
    }
  }

  // test the trace element integral of the functor
  const int quadratureorder = 0;
  const int nIntegrand_cell = qfldElem.nDOF();
  const int nIntegrand_trace = hbfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, ArrayQ, ArrayQ> integralBC(quadratureorder, nIntegrand_cell, nIntegrand_trace);

  ArrayQ rsdPDEElem[nBasis_cell] = {0,0};
  ArrayQ rsdPDETrue[nBasis_cell] = {0,0};
  rsdPDETrue[0] = integrandCellTrue[0];
  rsdPDETrue[1] = integrandCellTrue[1];

  ArrayQ rsdHTElem[nBasis_trace] = {0};
  ArrayQ rsdHTTrue[nBasis_trace];
  rsdHTTrue[0] = integrandTraceTrue[0];

  // cell integration for canonical element
  integralBC(integrandBasisWeighted, xnode, rsdPDEElem, nIntegrand_cell, rsdHTElem, nIntegrand_trace);

  // check
  for (int ibasis = 0; ibasis < nBasis_cell; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( rsdPDETrue[ibasis][n], rsdPDEElem[ibasis][n], small_tol, close_tol );
    }
  }

  for ( int nbasis = 0; nbasis < nBasis_trace; nbasis++ )
  {
    for ( int ieqn = 0; ieqn < pde.N; ieqn++ )
    {
      SANS_CHECK_CLOSE( rsdHTTrue[nbasis][ieqn], rsdHTTrue[nbasis][ieqn], small_tol, close_tol );
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
