// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualCell_Galerkin_IRS_AD_btest
// testing of cell pseudot-time continuation residual functions for Galerkin with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
//#include "Field/FieldVolume_DG_Cell.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_IRS.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin_BDF.h"

#include "Discretization/IntegrateCellGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_Galerkin_IRS_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_1D_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin_IRS<PDEClass> IntegrandClass;
  typedef Field_DG_Cell<PhysD1, TopoD1, Real> TField_DG_CellType;
  typedef Field_DG_Cell<PhysD1, TopoD1, Real> HField_DG_CellType;
  typedef Field_DG_Cell<PhysD1, TopoD1, ArrayQ> QField_DG_CellType;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QFieldSequenceType;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single line, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // inverse timestep field
  TField_DG_CellType dtifld(xfld, 1, BasisFunctionCategory_Hierarchical);

  dtifld.DOF(0) = 1./13.;
  dtifld.DOF(1) = 1./12.;

  // Element size field
  HField_DG_CellType hfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  hfld.DOF(0) = 1.;
  hfld.DOF(1) = 1.;

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  QField_DG_CellType qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  QFieldSequenceType qfldspast(1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Hierarchical);

  // previous solution data
  qfldspast[0].DOF(0) = 2;
  qfldspast[0].DOF(1) = 1;

  // quadrature rule: quadratic (aux var defn has linear basis & linear solution)
  int quadratureOrder = 2;
  BOOST_CHECK_EQUAL( 2, quadratureOrder );

  // PDE residuals: (time)
  Real rsd[2];
  rsd[0] =  0.5 * (4./13. + 4./12.);
  rsd[1] = -0.5 * (4./13. + 4./12.);


  const Real tol = 1e-12;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  // base interface
  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsd[0], rsdPDEGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsd[1], rsdPDEGlobal[1], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_2D_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin_IRS<PDEClass> IntegrandClass;
  typedef Field_DG_Cell<PhysD2, TopoD2, Real> TField_DG_CellType;
  typedef Field_DG_Cell<PhysD2, TopoD2, Real> HField_DG_CellType;
  typedef FieldSequence<PhysD2, TopoD2, ArrayQ> QFieldSequenceType;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // inverse timestep field
  TField_DG_CellType dtifld(xfld, 1, BasisFunctionCategory_Hierarchical);

  dtifld.DOF(0) = 1./13.;
  dtifld.DOF(1) = 1./12.;
  dtifld.DOF(2) = 1./14.;

  // Element size field
  HField_DG_CellType hfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  hfld.DOF(0) = 1.;
  hfld.DOF(1) = 1.;
  hfld.DOF(2) = 1.;

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  int BDFweights = 2;

  QFieldSequenceType qfldspast(BDFweights-1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Hierarchical);

  // previous solution data
  qfldspast[0].DOF(0) = 2;
  qfldspast[0].DOF(1) = 1;
  qfldspast[0].DOF(2) = 3;

  // quadrature rule: cubic (dti has linear basis & linear solution & linear test function)
  int quadratureOrder = 3;

  // PDE residuals: (time)
  Real rsd[3];
  rsd[0] =  1./3. * (5./13. + 5./12. + 5./14.) * 0.5;
  rsd[1] = -1./3. * (3./13. + 3./12. + 3./14.) * 0.5;
  rsd[2] = -1./3. * (2./13. + 2./12. + 2./14.) * 0.5;

  const Real tol = 1e-12;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsd[0], rsdPDEGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsd[1], rsdPDEGlobal[1], tol );
  BOOST_CHECK_CLOSE( rsd[2], rsdPDEGlobal[2], tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_2D_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin_IRS<PDEClass> IntegrandClass;
  typedef Field_DG_Cell<PhysD2, TopoD2, Real> TField_DG_CellType;
  typedef Field_DG_Cell<PhysD2, TopoD2, Real> HField_DG_CellType;
  typedef FieldSequence<PhysD2, TopoD2, ArrayQ> QFieldSequenceType;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Quad_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // inverse timestep field
  TField_DG_CellType dtifld(xfld, 1, BasisFunctionCategory_Hierarchical);

  dtifld.DOF(0) = 1./13.;
  dtifld.DOF(1) = 1./12.;
  dtifld.DOF(2) = 1./10.;
  dtifld.DOF(3) = 1./14.;

  // Element size field
  HField_DG_CellType hfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  hfld.DOF(0) = 1.;
  hfld.DOF(1) = 1.;
  hfld.DOF(2) = 1.;
  hfld.DOF(3) = 1.;

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 5;
  qfld.DOF(3) = 4;

  int BDFweights = 2;

  QFieldSequenceType qfldspast(BDFweights-1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Hierarchical);

  // previous solution data
  qfldspast[0].DOF(0) = 2;
  qfldspast[0].DOF(1) = 1;
  qfldspast[0].DOF(2) = 4;
  qfldspast[0].DOF(3) = 3;

  // quadrature rule: cubic (dti has linear basis & linear solution & linear test function)
  int quadratureOrder = 3;

  // PDE residuals: (time)
  Real rsd[4];
  rsd[0] = 7867./65520.;   // Basis function 1
  rsd[1] = -5437./65520.;   // Basis function 2
  rsd[2] = -2621./65520.;   // Basis function 3
  rsd[3] = 191./65520.;   // Basis function 4

  const Real tol = 1e-12;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsd[0], rsdPDEGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsd[1], rsdPDEGlobal[1], tol );
  BOOST_CHECK_CLOSE( rsd[2], rsdPDEGlobal[2], tol );
  BOOST_CHECK_CLOSE( rsd[3], rsdPDEGlobal[3], tol );

}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
