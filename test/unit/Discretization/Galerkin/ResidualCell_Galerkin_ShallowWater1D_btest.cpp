// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualCell_Galerkin_ShallowWater1D_btest.cpp
// testing of cell residual functions for Galerkin: ShallowWater1D

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"

#include "pde/ShallowWater/PDEShallowWater1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/Tuple/FieldTuple.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/IntegrateCellGroups.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//#if PDEShallowWater_sansgH2fluxAdvective1D 0  // does not incorporate grad H in advective flux

//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_Galerkin_ShallowWater1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_1D_Galerkin_1Line_X1_test )
{
  typedef PhysD1 PhysDim;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDENDConvertSpace<PhysDim,PDEShallowWater<PhysDim,VarTypeHVelocity1D,SolutionClass> > NDPDEClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandClass;

  // define pde object
  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 18.5;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  NDPDEClass pde(q0,solnArgs,PDEShallowWater<PhysDim,VarTypeHVelocity1D,SolutionClass>::ShallowWater_ResidInterp_Raw);

  // static tests: check dimensions
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 2 );

  // check hasFlux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

#if PDEShallowWater_sansgH2fluxAdvective1D
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );
#else
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
#endif

  // grid: single line, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int order = 1;

  // ----------------------- PARAMETER FIELD ----------------------- //

  // Solution variable CG field
  Field_CG_Cell<PhysDim, TopoD1, ArrayQ> varfld(xfld, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, varfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, varfld.nElem() );

  // State vector solution at line cell first node {0}
  Real x_L = 0.0;
  Real HL  = 1 + x_L + pow(x_L,2) + pow(x_L,3) + pow(x_L,4) + pow(x_L,5);
  Real uL = q0/HL;
  VarTypeHVelocity1D vardataL( HL, uL );

  // State vector solution at line cell second node {1}
  Real x_R = 1.0;
  Real HR  = 1 + x_R + pow(x_R,2) + pow(x_R,3) + pow(x_R,4) + pow(x_R,5);
  Real uR = q0/HR;
  VarTypeHVelocity1D vardataR( HR, uR );

  // Line solution
  varfld.DOF(0) = pde.setDOFFrom( vardataL );
  varfld.DOF(1) = pde.setDOFFrom( vardataR );

  // ----------------------------- TEST RESIDUAL (INTEGRAL) ----------------------------- //
  const Real small_tol_res = 5e-11;
  const Real close_tol_res = 5e-11;
  SLA::SparseVector<ArrayQ> residual(2);

  // Test the element integral of the functor: quadrature order = 39
  int quadratureOrder = 39;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // grid integration (just one element)
  residual = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin(fcnint, residual),
                                          xfld, varfld,
                                          &quadratureOrder, 1 );
  Real HRes;
  Real uRes;
  ArrayQ residualTrue[2];

  // PDE residuals: (source) + (advective flux)
  // True values obtained from: residual_script.m

#if PDEShallowWater_sansgH2fluxAdvective1D
  // Basis function 1
  HRes = ( 0.0000000000000000e+00 ) + ( 3.1347222222222225e+01 );
  uRes = ( 2.6295449001042687e+02 ) + ( 3.3828877314814827e+02 );
  residualTrue[0] = { HRes, uRes };

  // Basis function 2
  HRes = ( 0.0000000000000000e+00 ) + ( -3.1347222222222225e+01 );
  uRes = ( 1.2511791437105610e+02 ) + ( -3.3828877314814827e+02 );
  residualTrue[1] = { HRes, uRes };
#else
  // Basis function 1
  HRes = ( 0.0000000000000000e+00 ) + ( 3.1347222222222225e+01 );
  uRes = ( 1.9755449001042683e+02 ) + ( 4.0859377314814827e+02 );
  residualTrue[0] = { HRes, uRes };

  // Basis function 2
  HRes = ( 0.0000000000000000e+00 ) + ( -3.1347222222222225e+01 );
  uRes = ( 1.8842914371056114e+01 ) + ( -4.0859377314814827e+02 );
  residualTrue[1] = { HRes, uRes };
#endif

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( residualTrue[0][n], residual[0][n], small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[1][n], residual[1][n], small_tol_res, close_tol_res );
  }

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
