// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianBoundaryTrace_Galerkin_AD_btest - mitLG
// testing of trace-integral jacobian: advection-diffusion

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/Galerkin/JacobianBoundaryTrace_mitLG_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_mitLG_Galerkin.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianBoundaryTrace_Galerkin_Dirichlet_mitLG_AD_test_suite )


typedef boost::mpl::list< SurrealS<1>, SurrealS<2> > Surreals;
//typedef boost::mpl::list< SurrealS<1> > Surreals;

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin1D_1Line_X1Q1, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::template MatrixQ<Real> MatrixQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;

  // PDE

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real uB = 3;

  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 2 );

  ArrayQ qDOF[2];

  // triangle solution data (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, 0, BasisFunctionCategory_Legendre);

  BOOST_REQUIRE_EQUAL( lgfld.nDOF(), 2 );

  ArrayQ lgDOF[2];
  for (int i = 0; i < 2; i++)
    lgDOF[i] = 0;

  // Lagrange multiplier DOF data
  lgDOF[0] = lgfld.DOF(0) =  5;
  lgDOF[1] = lgfld.DOF(1) = -7;

  // integrand
  IntegrandClass fcn( pde, bc, {1} );

  // quadrature rule
  int quadratureOrder[2] = {0,0};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(2), rsdPDEGlobal1(2);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(2), rsdBCGlobal1(2);
  Real jacPDE_q[2][2], jacPDE_lg[2][2];
  Real jacBC_q[2][2], jacBC_lg[2][2];

  rsdPDEGlobal0 = 0;
  rsdBCGlobal0 = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal0, rsdBCGlobal0),
                                                      xfld, qfld, lgfld, quadratureOrder, 2 );

  // jacobian wrt solution
  for (int j = 0; j < 2; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal1, rsdBCGlobal1),
                                                        xfld, qfld, lgfld, quadratureOrder, 2 );

    for (int i = 0; i < 2; i++)
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 2; i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 2; i++)
      qfld.DOF(i) = qDOF[i];
  }


  // jacobian wrt Lagrange multiplier
  for (int j = 0; j < 2; j++)
  {
    lgfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal1, rsdBCGlobal1),
                                                        xfld, qfld, lgfld, quadratureOrder, 2 );

    for (int i = 0; i < 2; i++)
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 2; i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 2; i++)
      lgfld.DOF(i) = lgDOF[i];
  }

#if 0
  cout << "FD: jacPDE_q  = ";  dump( jacPDE_q, 2, 2 );  cout << endl;
  cout << "FD: jacPDE_lg = ";  dump( jacPDE_lg, 2, 2 );  cout << endl;
  cout << "FD: jacBC_q   = ";  dump( jacBC_q, 2, 2 );  cout << endl;
  cout << "FD: jacBC_lg  = ";  dump( jacBC_lg, 2, 2 );  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(2,2);
  DLA::MatrixD<MatrixQ> mtxGlobPDE_lg(2,2);
  DLA::MatrixD<MatrixQ> mtxGlobBC_q(2,2);
  DLA::MatrixD<MatrixQ> mtxGlobBC_lg(2,2);

  mtxGlobPDE_q = 0;
  mtxGlobPDE_lg = 0;
  mtxGlobBC_q = 0;
  mtxGlobBC_lg = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
      JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcn, mtxGlobPDE_q, mtxGlobPDE_lg, mtxGlobBC_q, mtxGlobBC_lg),
      xfld, qfld, lgfld, quadratureOrder, 2 );

#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
  cout << "Surreal: mtxGlobPDE_lg = " << endl;  mtxGlobPDE_lg.dump(2);
  cout << "Surreal: mtxGlobBC_q   = " << endl;  mtxGlobBC_q.dump(2);
  cout << "Surreal: mtxGlobBC_lg  = " << endl;  mtxGlobBC_lg.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;
  for (int i = 0; i < 2; i++)
  {
    for (int j = 0; j < 2; j++)
       SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxGlobPDE_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 2; j++)
       SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxGlobPDE_lg(i,j), small_tol, close_tol );
  }

  for (int i = 0; i < 2; i++)
  {
    for (int j = 0; j < 2; j++)
       SANS_CHECK_CLOSE( jacBC_q[i][j], mtxGlobBC_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 2; j++)
       SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxGlobBC_lg(i,j), small_tol, close_tol );
  }

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_1Triangle_X1Q1, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real uB = 3;

  BCClass bc(pde, uB, true);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 3 );

  ArrayQ qDOF[3];

  // triangle solution data (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;
  qDOF[2] = qfld.DOF(2) = 4;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( lgfld.nDOF(), 6 );

  ArrayQ lgDOF[6];
  for (int i = 0; i < 6; i++)
    lgDOF[i] = 0;

  // Lagrange multiplier DOF data
  lgDOF[2] = lgfld.DOF(2) =  5;
  lgDOF[3] = lgfld.DOF(3) = -7;

  // integrand
  IntegrandClass fcn( pde, bc, {1} );

  // quadrature rule
  int quadratureOrder[3] = {2,2,2};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(3), rsdPDEGlobal1(3);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(6), rsdBCGlobal1(6);
  Real jacPDE_q[3][3], jacPDE_lg[3][6];
  Real jacBC_q[6][3], jacBC_lg[6][6];

  rsdPDEGlobal0 = 0;
  rsdBCGlobal0 = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal0, rsdBCGlobal0),
                                                      xfld, qfld, lgfld, quadratureOrder, 3 );

  // jacobian wrt solution
  for (int j = 0; j < 3; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal1, rsdBCGlobal1),
                                                        xfld, qfld, lgfld, quadratureOrder, 3 );

    for (int i = 0; i < 3; i++)
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 6; i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 3; i++)
      qfld.DOF(i) = qDOF[i];
  }


  // jacobian wrt Lagrange multiplier
  for (int j = 0; j < 6; j++)
  {
    lgfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal1, rsdBCGlobal1),
                                                        xfld, qfld, lgfld, quadratureOrder, 3 );

    for (int i = 0; i < 3; i++)
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 6; i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 6; i++)
      lgfld.DOF(i) = lgDOF[i];
  }

#if 0
  cout << "FD: jacPDE_q  = ";  dump( jacPDE_q, 3, 3 );  cout << endl;
  cout << "FD: jacPDE_lg = ";  dump( jacPDE_lg, 3, 6 );  cout << endl;
  cout << "FD: jacBC_q   = ";  dump( jacBC_q, 6, 3 );  cout << endl;
  cout << "FD: jacBC_lg  = ";  dump( jacBC_lg, 6, 6 );  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(3,3);
  DLA::MatrixD<MatrixQ> mtxGlobPDE_lg(3,6);
  DLA::MatrixD<MatrixQ> mtxGlobBC_q(6,3);
  DLA::MatrixD<MatrixQ> mtxGlobBC_lg(6,6);

  mtxGlobPDE_q = 0;
  mtxGlobPDE_lg = 0;
  mtxGlobBC_q = 0;
  mtxGlobBC_lg = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcn, mtxGlobPDE_q, mtxGlobPDE_lg, mtxGlobBC_q, mtxGlobBC_lg),
      xfld, qfld, lgfld, quadratureOrder, 3 );


#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
  cout << "Surreal: mtxGlobPDE_lg = " << endl;  mtxGlobPDE_lg.dump(2);
  cout << "Surreal: mtxGlobBC_q   = " << endl;  mtxGlobBC_q.dump(2);
  cout << "Surreal: mtxGlobBC_lg  = " << endl;  mtxGlobBC_lg.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;
  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < 3; j++)
       SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxGlobPDE_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 6; j++)
       SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxGlobPDE_lg(i,j), small_tol, close_tol );
  }

  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 3; j++)
       SANS_CHECK_CLOSE( jacBC_q[i][j], mtxGlobBC_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 6; j++)
       SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxGlobBC_lg(i,j), small_tol, close_tol );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_1Quad_X1Q1, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real uB = 3;

  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Quad_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 4 );

  ArrayQ qDOF[4];

  // triangle solution data (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;
  qDOF[2] = qfld.DOF(2) = 4;
  qDOF[3] = qfld.DOF(3) = 6;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( lgfld.nDOF(), 2*4 );

  ArrayQ lgDOF[2*4];
  for (int i = 0; i < 2*4; i++)
    lgDOF[i] = 0;

  // Lagrange multiplier DOF data
  lgDOF[0] = lgfld.DOF(0) =  5;
  lgDOF[1] = lgfld.DOF(1) = -7;

  // integrand
  IntegrandClass fcn( pde, bc, {0} );

  // quadrature rule
  int quadratureOrder[4] = {2,2,2,2};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(4), rsdPDEGlobal1(4);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(2*4), rsdBCGlobal1(2*4);
  Real jacPDE_q[4][4], jacPDE_lg[4][2*4];
  Real jacBC_q[2*4][4], jacBC_lg[2*4][2*4];

  rsdPDEGlobal0 = 0;
  rsdBCGlobal0 = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal0, rsdBCGlobal0),
                                                      xfld, qfld, lgfld, quadratureOrder, 4 );

  // jacobian wrt solution
  for (int j = 0; j < 4; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal1, rsdBCGlobal1),
                                                        xfld, qfld, lgfld, quadratureOrder, 4 );

    for (int i = 0; i < 4; i++)
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 2*4; i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 4; i++)
      qfld.DOF(i) = qDOF[i];
  }


  // jacobian wrt Lagrange multiplier
  for (int j = 0; j < 2*4; j++)
  {
    lgfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal1, rsdBCGlobal1),
                                                        xfld, qfld, lgfld, quadratureOrder, 4 );

    for (int i = 0; i < 4; i++)
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 2*4; i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 2*4; i++)
      lgfld.DOF(i) = lgDOF[i];
  }

#if 0
  cout << "FD: jacPDE_q  = ";  dump( jacPDE_q, 3, 3 );  cout << endl;
  cout << "FD: jacPDE_lg = ";  dump( jacPDE_lg, 3, 6 );  cout << endl;
  cout << "FD: jacBC_q   = ";  dump( jacBC_q, 6, 3 );  cout << endl;
  cout << "FD: jacBC_lg  = ";  dump( jacBC_lg, 6, 6 );  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(4,4);
  DLA::MatrixD<MatrixQ> mtxGlobPDE_lg(4,2*4);
  DLA::MatrixD<MatrixQ> mtxGlobBC_q(2*4,4);
  DLA::MatrixD<MatrixQ> mtxGlobBC_lg(2*4,2*4);

  mtxGlobPDE_q = 0;
  mtxGlobPDE_lg = 0;
  mtxGlobBC_q = 0;
  mtxGlobBC_lg = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
      JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcn, mtxGlobPDE_q, mtxGlobPDE_lg, mtxGlobBC_q, mtxGlobBC_lg),
      xfld, qfld, lgfld, quadratureOrder, 4 );

#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
  cout << "Surreal: mtxGlobPDE_lg = " << endl;  mtxGlobPDE_lg.dump(2);
  cout << "Surreal: mtxGlobBC_q   = " << endl;  mtxGlobBC_q.dump(2);
  cout << "Surreal: mtxGlobBC_lg  = " << endl;  mtxGlobBC_lg.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
       SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxGlobPDE_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 2*4; j++)
       SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxGlobPDE_lg(i,j), small_tol, close_tol );
  }

  for (int i = 0; i < 2*4; i++)
  {
    for (int j = 0; j < 4; j++)
       SANS_CHECK_CLOSE( jacBC_q[i][j], mtxGlobBC_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 2*4; j++)
       SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxGlobBC_lg(i,j), small_tol, close_tol );
  }

}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin3D_1Tet_X1Q1, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion3D::MatrixQ<Real> MatrixQ;

  typedef BCAdvectionDiffusion<PhysD3,BCTypeDirichlet_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real uB = 3;

  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField3D_1Tet_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 4 );

  ArrayQ qDOF[4];

  // triangle solution data (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;
  qDOF[2] = qfld.DOF(2) = 4;
  qDOF[3] = qfld.DOF(3) = 7;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( lgfld.nDOF(), 12 );

  ArrayQ lgDOF[12];
  for (int i = 0; i < 12; i++)
    lgDOF[i] = 0;

  // Lagrange multiplier DOF data
  lgDOF[0] = lgfld.DOF(0) =  5;
  lgDOF[1] = lgfld.DOF(1) = -7;
  lgDOF[2] = lgfld.DOF(2) =  9;

  // integrand
  IntegrandClass fcn( pde, bc, {0} );

  // quadrature rule
  int quadratureOrder[4] = {1,1,1,1};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(4), rsdPDEGlobal1(4);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(12), rsdBCGlobal1(12);
  Real jacPDE_q[4][4], jacPDE_lg[4][12];
  Real jacBC_q[12][4], jacBC_lg[12][12];

  rsdPDEGlobal0 = 0;
  rsdBCGlobal0 = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal0, rsdBCGlobal0),
                                                      xfld, qfld, lgfld, quadratureOrder, 4 );

  // jacobian wrt solution
  for (int j = 0; j < 4; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal1, rsdBCGlobal1),
                                                        xfld, qfld, lgfld, quadratureOrder, 4 );

    for (int i = 0; i < 4; i++)
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 12; i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 4; i++)
      qfld.DOF(i) = qDOF[i];
  }


  // jacobian wrt Lagrange multiplier
  for (int j = 0; j < 12; j++)
  {
    lgfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal1, rsdBCGlobal1),
                                                        xfld, qfld, lgfld, quadratureOrder, 4 );

    for (int i = 0; i < 4; i++)
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 12; i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 12; i++)
      lgfld.DOF(i) = lgDOF[i];
  }

#if 0
  cout << "FD: jacPDE_q  = ";  dump( jacPDE_q, 3, 3 );  cout << endl;
  cout << "FD: jacPDE_lg = ";  dump( jacPDE_lg, 3, 6 );  cout << endl;
  cout << "FD: jacBC_q   = ";  dump( jacBC_q, 6, 3 );  cout << endl;
  cout << "FD: jacBC_lg  = ";  dump( jacBC_lg, 6, 6 );  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(4,4);
  DLA::MatrixD<MatrixQ> mtxGlobPDE_lg(4,12);
  DLA::MatrixD<MatrixQ> mtxGlobBC_q(12,4);
  DLA::MatrixD<MatrixQ> mtxGlobBC_lg(12,12);

  mtxGlobPDE_q = 0;
  mtxGlobPDE_lg = 0;
  mtxGlobBC_q = 0;
  mtxGlobBC_lg = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate(
      JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcn, mtxGlobPDE_q, mtxGlobPDE_lg, mtxGlobBC_q, mtxGlobBC_lg),
      xfld, qfld, lgfld, quadratureOrder, 4 );

#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
  cout << "Surreal: mtxGlobPDE_lg = " << endl;  mtxGlobPDE_lg.dump(2);
  cout << "Surreal: mtxGlobBC_q   = " << endl;  mtxGlobBC_q.dump(2);
  cout << "Surreal: mtxGlobBC_lg  = " << endl;  mtxGlobBC_lg.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
       SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxGlobPDE_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 12; j++)
       SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxGlobPDE_lg(i,j), small_tol, close_tol );
  }

  for (int i = 0; i < 12; i++)
  {
    for (int j = 0; j < 4; j++)
       SANS_CHECK_CLOSE( jacBC_q[i][j], mtxGlobBC_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 12; j++)
       SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxGlobBC_lg(i,j), small_tol, close_tol );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin3D_1Hex_X1Q1, SurrealClass, Surreals )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion3D::MatrixQ<Real> MatrixQ;

  typedef BCAdvectionDiffusion<PhysD3,BCTypeDirichlet_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real uB = 3;

  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField3D_1Hex_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 8 );

  ArrayQ qDOF[8];

  // triangle solution data (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;
  qDOF[2] = qfld.DOF(2) = 4;
  qDOF[3] = qfld.DOF(3) = 7;

  qDOF[4] = qfld.DOF(4) = 8;
  qDOF[5] = qfld.DOF(5) = 2;
  qDOF[6] = qfld.DOF(6) = 9;
  qDOF[7] = qfld.DOF(7) = 7;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( lgfld.nDOF(), 6*4 );

  ArrayQ lgDOF[6*4];
  for (int i = 0; i < 6*4; i++)
    lgDOF[i] = 0;

  // Lagrange multiplier DOF data
  lgDOF[0] = lgfld.DOF(0) =  5;
  lgDOF[1] = lgfld.DOF(1) = -7;
  lgDOF[2] = lgfld.DOF(2) =  9;
  lgDOF[3] = lgfld.DOF(3) =  3;

  // integrand
  IntegrandClass fcn( pde, bc, {0} );

  // quadrature rule
  int quadratureOrder[6] = {2,2,2,2,2,2};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(8), rsdPDEGlobal1(8);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(6*4), rsdBCGlobal1(6*4);
  Real jacPDE_q[8][8], jacPDE_lg[8][6*4];
  Real jacBC_q[6*4][8], jacBC_lg[6*4][6*4];

  rsdPDEGlobal0 = 0;
  rsdBCGlobal0 = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal0, rsdBCGlobal0),
                                                      xfld, qfld, lgfld, quadratureOrder, 6 );

  // jacobian wrt solution
  for (int j = 0; j < 8; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal1, rsdBCGlobal1),
                                                        xfld, qfld, lgfld, quadratureOrder, 6 );

    for (int i = 0; i < 8; i++)
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 6*4; i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 8; i++)
      qfld.DOF(i) = qDOF[i];
  }


  // jacobian wrt Lagrange multiplier
  for (int j = 0; j < 6*4; j++)
  {
    lgfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate( ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal1, rsdBCGlobal1),
                                                        xfld, qfld, lgfld, quadratureOrder, 6 );

    for (int i = 0; i < 8; i++)
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 6*4; i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 6*4; i++)
      lgfld.DOF(i) = lgDOF[i];
  }

#if 0
  cout << "FD: jacPDE_q  = ";  dump( jacPDE_q, 8, 8 );  cout << endl;
  cout << "FD: jacPDE_lg = ";  dump( jacPDE_lg, 8, 6*4 );  cout << endl;
  cout << "FD: jacBC_q   = ";  dump( jacBC_q, 6*4, 8 );  cout << endl;
  cout << "FD: jacBC_lg  = ";  dump( jacBC_lg, 6*4, 6*4 );  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(8,8);
  DLA::MatrixD<MatrixQ> mtxGlobPDE_lg(8,6*4);
  DLA::MatrixD<MatrixQ> mtxGlobBC_q(6*4,8);
  DLA::MatrixD<MatrixQ> mtxGlobBC_lg(6*4,6*4);

  mtxGlobPDE_q = 0;
  mtxGlobPDE_lg = 0;
  mtxGlobBC_q = 0;
  mtxGlobBC_lg = 0;

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD3>::integrate(
      JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcn, mtxGlobPDE_q, mtxGlobPDE_lg, mtxGlobBC_q, mtxGlobBC_lg),
      xfld, qfld, lgfld, quadratureOrder, 6 );

#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
  cout << "Surreal: mtxGlobPDE_lg = " << endl;  mtxGlobPDE_lg.dump(2);
  cout << "Surreal: mtxGlobBC_q   = " << endl;  mtxGlobBC_q.dump(2);
  cout << "Surreal: mtxGlobBC_lg  = " << endl;  mtxGlobBC_lg.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 6e-11;
  for (int i = 0; i < 8; i++)
  {
    for (int j = 0; j < 8; j++)
       SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxGlobPDE_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 6*4; j++)
       SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxGlobPDE_lg(i,j), small_tol, close_tol );
  }

  for (int i = 0; i < 6*4; i++)
  {
    for (int j = 0; j < 8; j++)
       SANS_CHECK_CLOSE( jacBC_q[i][j], mtxGlobBC_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 6*4; j++)
       SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxGlobBC_lg(i,j), small_tol, close_tol );
  }

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
