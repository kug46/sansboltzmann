// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_DGBR2_manifold_IBL2D_btest
// testing of element boundary trace residual integrands for DGBR2_manifold: IBL2D

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Galerkin_manifold.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Tuple/ElementTuple.h"

#include "pde/IBL/BCIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

using namespace std;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarTypeDANCt VarType;
typedef VarInterpret2D<VarType> VarInterpType;
typedef VarData2DDANCt VarDataType;

typedef PDEIBL<PhysD2,VarType> PDEClass;
typedef PDENDConvertSpace<PhysD2,PDEClass> NDPDEClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef PDEClass::template ArrayParam<Real> ArrayParam;
typedef PDEClass::template VectorX<Real> VectorX;
typedef DLA::VectorS<PhysDim::D,VectorX> VectorVectorX;

typedef BCIBL2D<BCTypeFullState,VarType> BCClass;
typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

typedef BCIBL2DParams<BCTypeFullState> BCParamsType;

typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldClass;
typedef ElementXField<PhysDim,TopoD0,Node> ElementXFieldTraceClass;
typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
typedef Element<ArrayParam,TopoD1,Line> ElementParamFieldClass;
typedef Element<VectorX,TopoD1,Line> ElementVFieldClass;

typedef MakeTuple<ElementTuple,ElementParamFieldClass,ElementXFieldClass>::type ElementParamType;

typedef ElementXFieldTraceClass::RefCoordType RefCoordTraceType;

typedef IntegrandBoundaryTrace<NDPDEClass,NDBCVecCat,Galerkin_manifold> IntegrandClass;
typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParamType> BasisWeightedPDEClass;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class IntegrandBoundaryTrace<NDPDEClass,NDBCVecCat,Galerkin_manifold>::
BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementParamType>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE(IntegrandBoundaryTrace_Dirichlet_mitState_Galerkin_manifold_IBL2D_btest_suite)

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(BasisWeighted_Line_X1Q1_UseUpwind_test)
{
  // static tests
  BOOST_CHECK( PDEClass::N == IntegrandClass::N );

  // PDE
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);
  VarInterpType varInterpret;

  // BC
  PyDict bcArgs;
  bcArgs[NDBCClass::ParamsType::params.isFullState] = true; // using upwinding
  bcArgs[NDBCClass::ParamsType::params.delta] = 0.12;
  bcArgs[NDBCClass::ParamsType::params.A] = 2.9;

  BCParamsType BCParamIBL2D;
  BCParamIBL2D.checkInputs(bcArgs);

  NDBCClass bc(pde, bcArgs);

  // ----------------------- ELEMENTAL FIELDS ----------------------- //
  const int order = 1;

  // Grid element
  VectorX X1 = { 0.000, 0.000 };
  VectorX X2 = { 0.800, 0.600 };

  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL(1, xfldElem.order());
  BOOST_CHECK_EQUAL(2, xfldElem.nDOF());

  xfldElem.DOF(0) = X1;
  xfldElem.DOF(1) = X2;

  ElementXFieldTraceClass xnode(0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL(0, xnode.order());
  BOOST_CHECK_EQUAL(1, xnode.nDOF());

  xnode.DOF(0) = X1;
  xnode.normalSignL() = -1;
  CanonicalTraceToCell canonicalTrace = CanonicalTraceToCell(1,0);

  // Parameter element
  const Real qxi_0 = 4.000, qzi_0 = 2.000, p0_0 = 1.100e+05, T0_0 = 290.00; // DOFs associated with basis function 0
  const Real qxi_1 = 3.000, qzi_1 = 4.000, p0_1 = 1.000e+05, T0_1 = 350.00; // DOFs associated with basis function 1

  ElementQFieldClass vfldElem(order, BasisFunctionCategory_Hierarchical);

  vfldElem.DOF(0) = {qxi_0, qzi_0};
  vfldElem.DOF(1) = {qxi_1, qzi_1};

  std::vector<VectorX> gradphi0(vfldElem.nDOF()), gradphi1(vfldElem.nDOF());
  xfldElem.evalBasisGradient(0.0, xfldElem, gradphi0.data(), gradphi0.size());
  xfldElem.evalBasisGradient(1.0, xfldElem, gradphi1.data(), gradphi1.size());

  VectorVectorX gradv0, gradv1;
  vfldElem.evalFromBasis(gradphi0.data(), gradphi0.size(), gradv0);
  vfldElem.evalFromBasis(gradphi1.data(), gradphi1.size(), gradv1);

  ElementParamFieldClass paramElem(order, BasisFunctionCategory_Hierarchical);

  paramElem.DOF(0) = {qxi_0, qzi_0, gradv0[0][0], gradv0[1][0], gradv0[0][1], gradv0[1][1], p0_0, T0_0};
  paramElem.DOF(1) = {qxi_1, qzi_1, gradv1[0][0], gradv1[1][0], gradv1[0][1], gradv1[1][1], p0_1, T0_1};

  // Solution element
  const Real delta_0 = 0.1000; // DOFs associated with basis function 0
  const Real A_0     = 3.0000;

  const Real delta_1 = 0.0800; // DOFs associated with basis function 1
  const Real A_1     = 3.3000;

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL(1, qfldElem.order());
  BOOST_CHECK_EQUAL(2, qfldElem.nDOF());

  qfldElem.DOF(0) = pde.setDOFFrom(VarDataType(delta_0,A_0));
  qfldElem.DOF(1) = pde.setDOFFrom(VarDataType(delta_1,A_1));

  // tuple element
  //Explicitly create a paramater instance.
  //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior
  ElementParamType tupleElem = (paramElem, xfldElem);

  // integrand functor
  StabilizationNitsche stab(order);
  const std::vector<int> boundaryGroups = {0};
  IntegrandClass integrandBoundaryTrace(pde,bc,boundaryGroups, stab);

  BOOST_CHECK(boundaryGroups.size() == integrandBoundaryTrace.nBoundaryGroups());

  for (std::size_t i=0; i<boundaryGroups.size(); ++i)
  {
    BOOST_CHECK_EQUAL(boundaryGroups[i], integrandBoundaryTrace.boundaryGroup(i));
    BOOST_CHECK_EQUAL(boundaryGroups[i], integrandBoundaryTrace.getBoundaryGroups()[i]);
  }

  BasisWeightedPDEClass integrandBasisWeighted =
      integrandBoundaryTrace.integrand(xnode,canonicalTrace,tupleElem,qfldElem);

  BOOST_CHECK( true == integrandBasisWeighted.needsEvaluation() );
  BOOST_CHECK( qfldElem.nDOF() == integrandBasisWeighted.nDOF() );

  const int nBasis = 2;

  ArrayQ integrandPDETrue[nBasis];
  // Basis function 0
#if ISIBLLFFLUX
  Real eIntegrand    = -3.8722568507209865e-01;
  Real starIntegrand = -2.7422838951631681e+00;
#else
  Real eIntegrand    = -3.4311367095203066e-01;
  Real starIntegrand = -2.4834614652222307e+00;
#endif

  integrandPDETrue[0] = { eIntegrand, starIntegrand };

  // Basis function 1
  eIntegrand    = -0.0000000000000000e+00;
  starIntegrand = -0.0000000000000000e+00;
  integrandPDETrue[1] = { eIntegrand, starIntegrand };

  ArrayQ integrandPDE[nBasis];

  RefCoordTraceType sRefTrace = 0;
  integrandBasisWeighted(sRefTrace,integrandPDE,nBasis);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  for (int ibasis = 0; ibasis < nBasis; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE(integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol);
    }
  }

  // test the trace element integral of the functor
  const int quadratureorder = 0;
  const int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0,Node,ArrayQ> integralPDE(quadratureorder,nIntegrand);

  ArrayQ rsdPDETrue[nBasis];
  rsdPDETrue[0] = integrandPDETrue[0];
  rsdPDETrue[1] = integrandPDETrue[1];

  // cell integration for canonical element
  ArrayQ rsdPDE[nBasis];
  integralPDE(integrandBasisWeighted,xnode,rsdPDE,nIntegrand);

  // check
  for (int ibasis = 0; ibasis < nBasis; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE(rsdPDETrue[ibasis][n], rsdPDE[ibasis][n], small_tol, close_tol);
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(BasisWeighted_Line_X1Q1_NotUseUpwind_test)
{
  // PDE
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);
  VarInterpType varInterpret;

  // BC
  PyDict bcArgs;
  bcArgs[NDBCClass::ParamsType::params.isFullState] = false; // whether to use upwinding
  bcArgs[NDBCClass::ParamsType::params.delta] = 0.12;
  bcArgs[NDBCClass::ParamsType::params.A] = 2.9;

  BCParamsType BCParamIBL2D;
  BCParamIBL2D.checkInputs(bcArgs);

  NDBCClass bc(pde,bcArgs);

  // ----------------------- ELEMENTAL FIELDS ----------------------- //
  const int order = 1;

  // Grid element
  VectorX X1 = { 0.000, 0.000 };
  VectorX X2 = { 0.800, 0.600 };

  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL(1, xfldElem.order());
  BOOST_CHECK_EQUAL(2, xfldElem.nDOF());

  xfldElem.DOF(0) = X1;
  xfldElem.DOF(1) = X2;

  ElementXFieldTraceClass xnode(0, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL(0, xnode.order());
  BOOST_CHECK_EQUAL(1, xnode.nDOF());

  xnode.DOF(0) = X1;
  xnode.normalSignL() = -1;
  CanonicalTraceToCell canonicalTrace = CanonicalTraceToCell(1,0);

  // Parameter element
  const Real qxi_0 = 4.000, qzi_0 = 2.000, p0_0 = 1.100e+05, T0_0 = 290.00; // DOFs associated with basis function 0
  const Real qxi_1 = 3.000, qzi_1 = 4.000, p0_1 = 1.000e+05, T0_1 = 350.00; // DOFs associated with basis function 1

  ElementQFieldClass vfldElem(order, BasisFunctionCategory_Hierarchical);

  vfldElem.DOF(0) = {qxi_0, qzi_0};
  vfldElem.DOF(1) = {qxi_1, qzi_1};

  std::vector<VectorX> gradphi0(vfldElem.nDOF()), gradphi1(vfldElem.nDOF());
  xfldElem.evalBasisGradient(0.0, xfldElem, gradphi0.data(), gradphi0.size());
  xfldElem.evalBasisGradient(1.0, xfldElem, gradphi1.data(), gradphi1.size());

  VectorVectorX gradv0, gradv1;
  vfldElem.evalFromBasis(gradphi0.data(), gradphi0.size(), gradv0);
  vfldElem.evalFromBasis(gradphi1.data(), gradphi1.size(), gradv1);

  ElementParamFieldClass paramElem(order, BasisFunctionCategory_Hierarchical);

  paramElem.DOF(0) = {qxi_0, qzi_0, gradv0[0][0], gradv0[1][0], gradv0[0][1], gradv0[1][1], p0_0, T0_0};
  paramElem.DOF(1) = {qxi_1, qzi_1, gradv1[0][0], gradv1[1][0], gradv1[0][1], gradv1[1][1], p0_1, T0_1};

  // Solution element
  const Real delta_0 = 0.1000; // DOFs associated with basis function 0
  const Real A_0     = 3.0000;

  const Real delta_1 = 0.0800; // DOFs associated with basis function 1
  const Real A_1     = 3.3000;

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL(1, qfldElem.order());
  BOOST_CHECK_EQUAL(2, qfldElem.nDOF());

  qfldElem.DOF(0) = pde.setDOFFrom(VarDataType(delta_0,A_0));
  qfldElem.DOF(1) = pde.setDOFFrom(VarDataType(delta_1,A_1));

  // tuple element
  //Explicitly create a paramater instance.
  //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior
  ElementParamType tupleElem = (paramElem, xfldElem);

  // integrand functor
  const std::vector<int> boundaryGroups = {0};
  IntegrandClass integrandBoundaryTrace(pde,bc,boundaryGroups);

  BOOST_CHECK(boundaryGroups.size() == integrandBoundaryTrace.nBoundaryGroups());

  for (std::size_t i=0; i<boundaryGroups.size(); ++i)
  {
    BOOST_CHECK_EQUAL(boundaryGroups[i], integrandBoundaryTrace.boundaryGroup(i));
    BOOST_CHECK_EQUAL(boundaryGroups[i], integrandBoundaryTrace.getBoundaryGroups()[i]);
  }

  BasisWeightedPDEClass integrandBasisWeighted =
      integrandBoundaryTrace.integrand(xnode,canonicalTrace,tupleElem,qfldElem);

  BOOST_CHECK_EQUAL(2, integrandBasisWeighted.nDOF());
  BOOST_CHECK(integrandBasisWeighted.needsEvaluation() == true);

  const int nBasis = 2;

  ArrayQ integrandPDETrue[nBasis];
  // Basis function 0
  Real eIntegrand    = -3.4311367095203066e-01;
  Real starIntegrand = -2.4834614652222307e+00;
  integrandPDETrue[0] = { eIntegrand, starIntegrand };

  // Basis function 1
  eIntegrand    = -0.0000000000000000e+00;
  starIntegrand = -0.0000000000000000e+00;
  integrandPDETrue[1] = { eIntegrand, starIntegrand };

  ArrayQ integrandPDE[nBasis];

  RefCoordTraceType sRefTrace = 0;
  integrandBasisWeighted(sRefTrace,integrandPDE,nBasis);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  for (int ibasis = 0; ibasis < nBasis; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE(integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol);
    }
  }

  // test the trace element integral of the functor
  const int quadratureorder = 0;
  const int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0,Node,ArrayQ> integralPDE(quadratureorder,nIntegrand);

  ArrayQ rsdPDETrue[nBasis];
  rsdPDETrue[0] = integrandPDETrue[0];
  rsdPDETrue[1] = integrandPDETrue[1];

  // cell integration for canonical element
  ArrayQ rsdPDE[nBasis];
  integralPDE(integrandBasisWeighted,xnode,rsdPDE,nIntegrand);

  // check
  for (int ibasis = 0; ibasis < nBasis; ibasis++)
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE(rsdPDETrue[ibasis][n], rsdPDE[ibasis][n], small_tol, close_tol);
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
