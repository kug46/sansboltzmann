// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_CR_btest
// testing of cell element residual integrands for Galerkin: Cauchy-Riemann

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"

#include "SANS_btest.h"

#include "Surreal/SurrealS.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/CauchyRiemann/PDECauchyRiemann2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementXFieldArea.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDE2DClassPlusTime;
template class IntegrandCell_Galerkin< PDE2DClassPlusTime >::BasisWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2,TopoD2, Triangle> >;

template class IntegrandCell_Galerkin< PDE2DClassPlusTime >::BasisWeighted<Real, TopoD2, Quad, ElementXField<PhysD2,TopoD2, Quad> >;
}

using namespace SANS;



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_CR_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_test )
{
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDECauchyRiemann2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle, ElementXField<PhysD2,TopoD2, Triangle> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  PDEClass pde;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = {1, 2};
  qfldElem.DOF(1) = {3, 4};
  qfldElem.DOF(2) = {5, 6};

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 2, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  ArrayQ integrandTrue[3];
  ArrayQ integrand[3];

  sRef = {0, 0};
  fcn( sRef, integrand, 3 );

  integrandTrue[0] = { 3, -1};
  integrandTrue[1] = {-1,  2};
  integrandTrue[2] = {-2, -1};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][k], integrand[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][k], integrand[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][k], integrand[2][k], small_tol, close_tol );
  }


  sRef = {1, 0};
  fcn( sRef, integrand, 3 );

  integrandTrue[0] = { 7, -1};
  integrandTrue[1] = {-3,  4};
  integrandTrue[2] = {-4, -3};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][k], integrand[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][k], integrand[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][k], integrand[2][k], small_tol, close_tol );
  }


  sRef = {0, 1};
  fcn( sRef, integrand, 3 );

  integrandTrue[0] = { 11, -1};
  integrandTrue[1] = { -5,  6};
  integrandTrue[2] = { -6, -5};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][k], integrand[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][k], integrand[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][k], integrand[2][k], small_tol, close_tol );
  }


  sRef = {1./3., 1./3.};
  fcn( sRef, integrand, 3 );

  integrandTrue[0] = { 7, -1};
  integrandTrue[1] = {-3,  4};
  integrandTrue[2] = {-4, -3};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][k], integrand[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][k], integrand[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][k], integrand[2][k], small_tol, close_tol );
  }


  // test the element integral of the functor

  int quadratureorder = 1;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[3] = {0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  ArrayQ rsd1 = { 3.5, -0.5};
  ArrayQ rsd2 = {-1.5,  2.0};
  ArrayQ rsd3 = {-2.0, -1.5};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( rsd1[k], rsdPDEElem[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2[k], rsdPDEElem[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3[k], rsdPDEElem[2][k], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_Surreal_test )
{
  typedef SurrealS<2> SurrealClass;
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDECauchyRiemann2D::template ArrayQ<SurrealClass> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<SurrealClass,TopoD2,Triangle, ElementXField<PhysD2,TopoD2, Triangle> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  PDEClass pde;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = {1, 2};
  qfldElem.DOF(1) = {3, 4};
  qfldElem.DOF(2) = {5, 6};

  // jacobians wrt qfldElem.DOF(0)
  qfldElem.DOF(0)[0].deriv(0) = 1;
  qfldElem.DOF(0)[1].deriv(1) = 1;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 2, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  ArrayQ integrand[3];

  {
  sRef = {0, 0};
  fcn( sRef, integrand, 3 );

  Real integrand1Val[2] = { 3, -1};
  Real integrand2Val[2] = {-1,  2};
  Real integrand3Val[2] = {-2, -1};

  Real integrand1Deriv0[2] = { 1,  1};
  Real integrand2Deriv0[2] = {-1,  0};
  Real integrand3Deriv0[2] = { 0, -1};
  Real integrand1Deriv1[2] = { 1, -1};
  Real integrand2Deriv1[2] = { 0,  1};
  Real integrand3Deriv1[2] = {-1,  0};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrand1Val[k], integrand[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Val[k], integrand[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Val[k], integrand[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1Deriv0[k], integrand[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Deriv0[k], integrand[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Deriv0[k], integrand[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1Deriv1[k], integrand[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Deriv1[k], integrand[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Deriv1[k], integrand[2][k].deriv(1), small_tol, close_tol );
  }
  }

  {
  sRef = {1, 0};
  fcn( sRef, integrand, 3 );

  Real integrand1Val[2] = { 7, -1};
  Real integrand2Val[2] = {-3,  4};
  Real integrand3Val[2] = {-4, -3};

  Real integrand1Deriv0[2] = {0, 0};
  Real integrand2Deriv0[2] = {0, 0};
  Real integrand3Deriv0[2] = {0, 0};
  Real integrand1Deriv1[2] = {0, 0};
  Real integrand2Deriv1[2] = {0, 0};
  Real integrand3Deriv1[2] = {0, 0};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrand1Val[k], integrand[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Val[k], integrand[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Val[k], integrand[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1Deriv0[k], integrand[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Deriv0[k], integrand[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Deriv0[k], integrand[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1Deriv1[k], integrand[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Deriv1[k], integrand[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Deriv1[k], integrand[2][k].deriv(1), small_tol, close_tol );
  }
  }

  {
  sRef = {0, 1};
  fcn( sRef, integrand, 3 );

  Real integrand1Val[2] = { 11, -1};
  Real integrand2Val[2] = { -5,  6};
  Real integrand3Val[2] = { -6, -5};

  Real integrand1Deriv0[2] = {0, 0};
  Real integrand2Deriv0[2] = {0, 0};
  Real integrand3Deriv0[2] = {0, 0};
  Real integrand1Deriv1[2] = {0, 0};
  Real integrand2Deriv1[2] = {0, 0};
  Real integrand3Deriv1[2] = {0, 0};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrand1Val[k], integrand[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Val[k], integrand[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Val[k], integrand[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1Deriv0[k], integrand[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Deriv0[k], integrand[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Deriv0[k], integrand[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1Deriv1[k], integrand[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Deriv1[k], integrand[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Deriv1[k], integrand[2][k].deriv(1), small_tol, close_tol );
  }
  }


  {
  sRef = {1./3., 1./3.};
  fcn( sRef, integrand, 3 );

  Real integrand1Val[2] = { 7, -1};
  Real integrand2Val[2] = {-3,  4};
  Real integrand3Val[2] = {-4, -3};

  Real thrd = 1./3.;
  Real integrand1Deriv0[2] = { thrd,  thrd};
  Real integrand2Deriv0[2] = {-thrd,     0};
  Real integrand3Deriv0[2] = {    0, -thrd};
  Real integrand1Deriv1[2] = { thrd, -thrd};
  Real integrand2Deriv1[2] = {    0,  thrd};
  Real integrand3Deriv1[2] = {-thrd,     0};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrand1Val[k], integrand[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Val[k], integrand[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Val[k], integrand[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1Deriv0[k], integrand[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Deriv0[k], integrand[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Deriv0[k], integrand[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1Deriv1[k], integrand[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2Deriv1[k], integrand[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3Deriv1[k], integrand[2][k].deriv(1), small_tol, close_tol );
  }
  }


  // test the element integral of the functor

  int quadratureorder = 1;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[3] = {0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  Real rsd1Data[2] = { 3.5, -0.5};
  Real rsd2Data[2] = {-1.5,  2.0};
  Real rsd3Data[2] = {-2.0, -1.5};

  Real sixth = 1./6.;
  Real rsd1Deriv0[2] = { sixth,  sixth};
  Real rsd2Deriv0[2] = {-sixth,      0};
  Real rsd3Deriv0[2] = {     0, -sixth};
  Real rsd1Deriv1[2] = { sixth, -sixth};
  Real rsd2Deriv1[2] = {     0,  sixth};
  Real rsd3Deriv1[2] = {-sixth,      0};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( rsd1Data[k], rsdPDEElem[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2Data[k], rsdPDEElem[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3Data[k], rsdPDEElem[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( rsd1Deriv0[k], rsdPDEElem[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2Deriv0[k], rsdPDEElem[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3Deriv0[k], rsdPDEElem[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( rsd1Deriv1[k], rsdPDEElem[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2Deriv1[k], rsdPDEElem[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3Deriv1[k], rsdPDEElem[2][k].deriv(1), small_tol, close_tol );
  }
}


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Quad_test )
{
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDECauchyRiemann2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Quad, ElementXField<PhysD2,TopoD2, Quad> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  // Quad grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 1;  y3 = 1;
  x4 = 0;  y4 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};
  xfldElem.DOF(3) = {x4, y4};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // Quad solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 2e-13;
  const Real close_tol = 2e-13;
  RefCoordType sRef;
  Real integrandTrue[4];
  ArrayQ integrand[4];

  sRef = {0, 0};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 1.2) + (-1644./125.);    // (advective) + (viscous)
  integrandTrue[1] = (-1)   + ( 7011./1000.);
  integrandTrue[2] = ( 0)   + ( 0 );
  integrandTrue[3] = (-0.2) + ( 6141./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {1, 0};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 3.  ) + (-4799./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-2.4 ) + ( 1343./500.);
  integrandTrue[2] = (-0.6 ) + ( 2113./1000.);
  integrandTrue[3] = ( 0   ) + ( 0 );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {1, 1};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = (  0   ) + ( 0 );    // (advective) + (viscous)
  integrandTrue[1] = (  0.8 ) + ( 99./1000. );
  integrandTrue[2] = ( -4.8 ) + ( -474./125. );
  integrandTrue[3] = (  4.  ) + ( 3693./1000. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {0, 1};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 1.2 ) + ( -3929./1000. );    // (advective) + (viscous)
  integrandTrue[1] = ( 0 )   + ( 0 );
  integrandTrue[2] = ( -6. ) + ( -1481./1000. );
  integrandTrue[3] = ( 4.8 ) + ( 541./100. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {1./2., 1./2.};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 2.1) + ( -117./50. );    // (advective) + (viscous)
  integrandTrue[1] = (-1.4) + ( -681./1000. );
  integrandTrue[2] = (-2.1) + ( 117./50. );
  integrandTrue[3] = ( 1.4) + ( 681./1000. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 2;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Quad, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[4];

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  Real rsd1 = ( 37./20.) + (-203./60.);   // (advective) + (viscous)
  Real rsd2 = (-23./20.) + (1087./3000.);
  Real rsd3 = (-47./20.) + ( 389./300.);
  Real rsd4 = ( 33./20.) + (5173./3000.);

  SANS_CHECK_CLOSE( rsd1, rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2, rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3, rsdPDEElem[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4, rsdPDEElem[3], small_tol, close_tol );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
