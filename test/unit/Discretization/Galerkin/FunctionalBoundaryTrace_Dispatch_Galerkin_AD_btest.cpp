// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FunctionalCell_Galerkin_btest
// testing of 2-D functional area-integral with Galerkin

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pde/BCParameters.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Output_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/QuadratureOrder.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( FunctionalBoundaryTrace_Dispatch_Galerkin_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SingleTriangle_BoundaryFlux )
{
  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  Real u = 11./10;
  Real v = 2./10;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2123./1000;
  Real kxy = 553./1000;
  Real kyx = 789./1000;
  Real kyy = 1007./1000;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Source2D_None source;

  NDPDEClass pde( adv, visc, source );

  PyDict BCDirichlet_mitLG,BCDirichlet_sansLG,BCDirichlet_mitState;
  BCDirichlet_mitLG[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCDirichlet_mitLG[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.A] = 1.4;
  BCDirichlet_mitLG[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.B] = 0.6;
  BCDirichlet_mitLG[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.bcdata] = 1.1;

  BCDirichlet_sansLG[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichlet_sansLG[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_sansLG>::params.A] = 1.3;
  BCDirichlet_sansLG[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_sansLG>::params.B] = 0.3;
  BCDirichlet_sansLG[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_sansLG>::params.bcdata] = 1.2;

  BCDirichlet_mitState[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet_mitState[BCAdvectionDiffusionParams<PhysD2,BCTypeDirichlet_mitStateParam>::params.qB] = 1.2;

  PyDict PyBCList;
  PyBCList["BCDirichlet_mitLG"]  = BCDirichlet_mitLG;
  PyBCList["BCDirichlet_sansLG"] = BCDirichlet_sansLG;
  PyBCList["BCDirichlet_mitState"] = BCDirichlet_mitState;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["BCDirichlet_mitLG"] = {0};
  BCBoundaryGroups["BCDirichlet_sansLG"] = {1};
  BCBoundaryGroups["BCDirichlet_mitState"] = {2};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Create the dispatcher inputs

  std::map< std::string, std::shared_ptr<BCBase> > BCs(BCParams::template createBCs<BCNDConvertSpace>(pde, PyBCList) );


  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  int order = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

//  output_Tecplot(qfld,"tmp/qfld.plt");

  QuadratureOrder quadratureOrder( xfld, 4 );
  std::vector<Real> tol = {1e-12, 2e-12};

  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

  lgfld = 2;

  typedef OutputAdvectionDiffusion2D_WeightedResidual OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;

  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvertSpace,
                                                   BCVector, Galerkin> IntegrateBoundaryTrace_DispatchClass;


  StabilizationNitsche stab(order);
  IntegrateBoundaryTrace_DispatchClass dispatcher(pde, PyBCList, BCs, BCBoundaryGroups, stab);

  NDOutputClass outputFcn(1.);
  {
    Real outputJ = 0;
    // Bottom Boundary - mitLG
    OutputIntegrandClass outputIntegrand( outputFcn, {0} );

    dispatcher.dispatch(
        FunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin(outputIntegrand, xfld, qfld, lgfld,
                                                             quadratureOrder.boundaryTraceOrders.data(),
                                                             quadratureOrder.boundaryTraceOrders.size(), outputJ ),
        FunctionalBoundaryTrace_Dispatch_Galerkin(outputIntegrand, xfld, qfld,
                                                  quadratureOrder.boundaryTraceOrders.data(),
                                                  quadratureOrder.boundaryTraceOrders.size(), outputJ ) );

    const Real small_tol = 1e-10, close_tol = 1e-10;
    // Exact output from Mathematica Galerkin_AlgEqSet_OutputFunctional.nb
    Real outputJTrue = 12723./5000.;
    SANS_CHECK_CLOSE( outputJTrue, outputJ, small_tol, close_tol);
  }

  {
    Real outputJ = 0;
    // Diagonal Boundary - sansLG
    OutputIntegrandClass outputIntegrand( outputFcn, {1} );

    dispatcher.dispatch(
        FunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin(outputIntegrand, xfld, qfld, lgfld,
                                                             quadratureOrder.boundaryTraceOrders.data(),
                                                             quadratureOrder.boundaryTraceOrders.size(), outputJ ),
        FunctionalBoundaryTrace_Dispatch_Galerkin(outputIntegrand, xfld, qfld,
                                                  quadratureOrder.boundaryTraceOrders.data(),
                                                  quadratureOrder.boundaryTraceOrders.size(), outputJ ) );

    const Real small_tol = 1e-10, close_tol = 1e-10;
    // Exact output from Mathematica Galerkin_AlgEqSet_OutputFunctional.nb
    Real outputJTrue = -(828882./sqrt(2)+3828565.)/445000.;
    SANS_CHECK_CLOSE( outputJTrue, outputJ, small_tol, close_tol);
  }

  {
    Real outputJ = 0;
    OutputIntegrandClass outputIntegrand( outputFcn, {2} );

    dispatcher.dispatch(
        FunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin(outputIntegrand, xfld, qfld, lgfld,
                                                             quadratureOrder.boundaryTraceOrders.data(),
                                                             quadratureOrder.boundaryTraceOrders.size(), outputJ ),
        FunctionalBoundaryTrace_Dispatch_Galerkin(outputIntegrand, xfld, qfld,
                                                  quadratureOrder.boundaryTraceOrders.data(),
                                                  quadratureOrder.boundaryTraceOrders.size(), outputJ ) );

    Real Cb = 1.5;
    const Real small_tol = 1e-10, close_tol = 1e-10;
    // Exact output from Mathematica Galerkin_AlgEqSet_OutputFunctional.nb
    Real outputJTrue = 917./200. + Cb*27599./1250.;
//    Real outputJTrue = 2787.625; //exact output with Nitsche param
    SANS_CHECK_CLOSE( outputJTrue, outputJ, small_tol, close_tol);
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
