// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualCell_Galerkin_BDF_AD_btest
// testing of cell BDF residual functions for Galerkin with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
//#include "Field/FieldVolume_DG_Cell.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_BDF.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin_BDF.h"

#include "Discretization/IntegrateCellGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_Galerkin_BDF_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_1D_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin_BDF<PDEClass> IntegrandClass;
  typedef Field_DG_Cell<PhysD1, TopoD1, ArrayQ> QField_DG_CellType;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QFieldSequenceType;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single line, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  QField_DG_CellType qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 4;

  int BDFweights = 2;

  QFieldSequenceType qfldspast(BDFweights-1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Hierarchical);

  // previous solution data
  qfldspast[0] = 0;

  // quadrature rule: quadratic (aux var defn has linear basis & linear solution)
  int quadratureOrder = 2;
  BOOST_CHECK_EQUAL( 2, quadratureOrder );

  // PDE residuals: (time)
  Real rsdPDE1 = 1./13.;
  Real rsdPDE2 = 3./26.;


  const Real tol = 1e-12;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  // base interface
  rsdPDEGlobal = 0;

  // Time integration parameters
  Real dt = 13.;
  std::vector<Real> weights {1., -1.}; //BDF1 weights

  // integrand
  IntegrandClass fcnint( pde, {0}, dt, weights );

  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal),
                                          xfld, (qfld, qfldspast), &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsdPDE1, rsdPDEGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsdPDE2, rsdPDEGlobal[1], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_2D_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef FieldSequence<PhysD2, TopoD2, ArrayQ> QFieldSequenceType;
  typedef IntegrandCell_Galerkin_BDF<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

   // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  int BDFweights = 2;

  QFieldSequenceType qfldspast(BDFweights-1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Hierarchical);

  // previous solution data
  qfldspast[0] = 0;

  // quadrature rule: quadratic (aux var defn has linear basis & linear solution)
  int quadratureOrder = 2;

  // PDE residuals: (advective) + (viscous)
  Real rsdPDE1 = 3./104.;
  Real rsdPDE2 = 11./312.;
  Real rsdPDE3 = 1./26.;

  const Real tol = 1e-12;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  rsdPDEGlobal = 0;

  // Time integration parameters
  Real dt = 13.;
  std::vector<Real> weights {1., -1.}; //BDF1 weights

  // integrand
  IntegrandClass fcnint( pde, {0}, dt, weights );

  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal),
                                          xfld, (qfld, qfldspast), &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsdPDE1, rsdPDEGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsdPDE2, rsdPDEGlobal[1], tol );
  BOOST_CHECK_CLOSE( rsdPDE3, rsdPDEGlobal[2], tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
