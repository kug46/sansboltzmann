// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_Galerkin_AD_btest
// testing of cell-integral jacobian: advection-diffusion on triangles

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/CauchyRiemann/PDECauchyRiemann2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_Galerkin_CR_test_suite )

typedef boost::mpl::list< SurrealS<1>, SurrealS<2> > Surreals;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_1Triangle_X1Q1_Surreal, SurrealClass, Surreals )
{
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  PDEClass pde;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );
  const int N = pde.N;

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  ArrayQ qDOF[3];

  // solution data
  qDOF[0] = qfld.DOF(0) = {1,  7};
  qDOF[1] = qfld.DOF(1) = {3, -2};
  qDOF[2] = qfld.DOF(2) = {4,  9};

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // quadrature rule (linear: basis is linear, solution is linear)
  int quadratureOrder = 2;

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdGlobal0(3), rsdGlobal1(3);
  Real jac[3*pde.N][3*pde.N];

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal0), xfld, qfld, &quadratureOrder, 1 );

#if 0
  cout << "rsdGlobal0 = ";
  for (int i = 0; i < 3; i++)
    cout << rsdGlobal0[i] << " ";
  cout << endl;
#endif

  for (int j = 0; j < 3; j++)
  {
    for (int n = 0; n < N; n++)
    {
      qfld.DOF(j)(n) += 1;

      rsdGlobal1 = 0;
      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal1), xfld, qfld, &quadratureOrder, 1 );

      for (int i = 0; i < 3; i++)
        qfld.DOF(i) = qDOF[i];

      for (int i = 0; i < 3; i++)
        for (int m = 0; m < N; m++)
          jac[i*N + m][j*N + n] = rsdGlobal1[i](m) - rsdGlobal0[i](m);
    }
  }

#if 0
  cout << "FD: jacobian = {";
  for (int i = 0; i < 3*N; i++)
  {
    cout << "{";
    for (int j = 0; j < 3*N; j++)
    {
      cout << jac[i][j];
      if (j < 2) cout << ", ";
    }
    cout << "}";
    if (i < 2) cout << ", ";
  }
  cout << "}" << endl;
#endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxGlob(3,3);

  mtxGlob = 0;
  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnint, mtxGlob), xfld, qfld, &quadratureOrder, 1 );

#if 0
  cout << "Surreal: jacobian = " << endl;
  mtxGlob.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < 3; i++)
    for (int m = 0; m < N; m++)
      for (int j = 0; j < 3; j++)
        for (int n = 0; n < N; n++)
          SANS_CHECK_CLOSE( jac[i*N + m][j*N + n], mtxGlob(i,j)(m,n), small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_1Quad_X1Q1_Surreal, SurrealClass, Surreals )
{
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  PDEClass pde;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );
  const int N = 2;

  // grid: single quad, P1 (aka X1)
  XField2D_1Quad_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  ArrayQ qDOF[4];

  // solution data
  qDOF[0] = qfld.DOF(0) = {1,  7};
  qDOF[1] = qfld.DOF(1) = {3, -2};
  qDOF[2] = qfld.DOF(2) = {4,  9};
  qDOF[3] = qfld.DOF(3) = {6,  5};

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // quadrature rule (linear: basis is linear, solution is linear)
  int quadratureOrder = 2;

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdGlobal0(4), rsdGlobal1(4);
  Real jac[4*N][4*N];

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal0), xfld, qfld, &quadratureOrder, 1 );

#if 0
  cout << "rsdGlobal0 = ";
  for (int i = 0; i < 3; i++)
    cout << rsdGlobal0[i] << " ";
  cout << endl;
#endif

  for (int j = 0; j < 4; j++)
  {
    for (int n = 0; n < N; n++)
    {
      qfld.DOF(j)(n) += 1;

      rsdGlobal1 = 0;
      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal1), xfld, qfld, &quadratureOrder, 1 );

      for (int i = 0; i < 4; i++)
        qfld.DOF(i) = qDOF[i];

      for (int i = 0; i < 4; i++)
        for (int m = 0; m < N; m++)
          jac[i*N + m][j*N + n] = rsdGlobal1[i](m) - rsdGlobal0[i](m);
    }
  }

#if 0
  cout << "FD: jacobian = {";
  for (int i = 0; i < 4*N; i++)
  {
    cout << "{";
    for (int j = 0; j < 4*N; j++)
    {
      cout << jac[i][j];
      if (j < 3) cout << ", ";
    }
    cout << "}";
    if (i < 3) cout << ", ";
  }
  cout << "}" << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlob(4,4);

  mtxGlob = 0;
  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnint, mtxGlob), xfld, qfld, &quadratureOrder, 1 );

#if 0
  cout << "Surreal: jacobian = " << endl;
  mtxGlob.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;
  for (int i = 0; i < 4; i++)
    for (int m = 0; m < N; m++)
      for (int j = 0; j < 4; j++)
        for (int n = 0; n < N; n++)
          SANS_CHECK_CLOSE( jac[i*N + m][j*N + n], mtxGlob(i,j)(m,n), small_tol, close_tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
