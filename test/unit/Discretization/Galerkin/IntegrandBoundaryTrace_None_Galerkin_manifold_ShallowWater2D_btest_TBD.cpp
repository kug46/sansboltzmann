// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_DGBR2_None_LittleR_manifold_ShallowWater2D_btest
//   testing of boundary integrands: 2D shallow water

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/ShallowWater/PDEShallowWater2D_manifold.h"
#include "pde/ShallowWater/BCShallowWater2D_manifold.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Discretization/DG/IntegrandBoundaryTrace_None_DGBR2_LittleR_manifold.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_None_DGBR2_LittleR_manifold_ShallowWater2D_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_X1Q1R1_test )
{
  typedef PhysD2 PhysDim;
  typedef ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta SolutionClass;
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef PDEShallowWater_manifold<PhysDim,VarType,SolutionClass> PDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass::VectorX VectorX;

  typedef BCNDConvertSpace<PhysDim, BCNone<PhysD2,PDEClass::N> > BCNDClass;

  typedef IntegrandBoundaryTrace<NDPDEClass, BCNDClass, BCNDClass::Category, Galerkin_manifold> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line> BasisWeightedClass;

  typedef ElementXField<PhysDim,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysDim,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<VectorArrayQ,TopoD1,Line> ElementRFieldClass;

  // PDE
  const int p = 5;     // solution H polynomial degree
  const Real q0 = 2.1; // inflow H*V
  const Real g = 9.81; // gravitational acceleration [m/s^2]
  const Real a = 5, b = 1.6; // major and minor semi-axes

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.a] = a;
  solnArgs[SolutionClass::ParamsType::params.b] = b;

  SolutionClass::ParamsType::checkInputs(solnArgs);

  NDPDEClass pde( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // BC
  const ArrayQ bcdata = { 0.3, 1.0 };
  PDEClass pdebc( solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );
  BCNDClass bc( pdebc, bcdata );

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 2 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  VectorX X1, X2;
  X1 = { a*cos(0.05), b*sin(0.05) };
  X2 = { a*cos(0.1), b*sin(0.1) };

  xfldElem.DOF(0) = X1;
  xfldElem.DOF(1) = X2;

  xnode.DOF(0) = X2;
  xnode.normalSignL() = 1;

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // State vector solution at line cell first node {0} (left node of first element)
  const Real HL = 3.4, vsL = 3.1;
  VarType vardataL( HL, vsL);

  // State vector solution at line cell first node {1} (right node of first element)
  const Real HR = 1.2, vsR = 0.2;
  VarType vardataR( HR, vsR);

  // Solution field
  qfldElem.DOF(0) = pde.setDOFFrom( vardataL );
  qfldElem.DOF(1) = pde.setDOFFrom( vardataR );

  // lifting operators
  ElementRFieldClass rfldElem(order, BasisFunctionCategory_Hierarchical);

//  rfldElem.DOF(0) = -1;  rfldElem.DOF(1) = 4;

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0}, disc );

  BasisWeightedClass fcn = fcnint.integrand( xnode, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, rfldElem );

  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrandPDETrue[2];
  Real integrandLOxTrue[2];
  BasisWeightedClass::IntegrandCellType integrand[2];

  sRef = 0;
  fcn( sRef, integrand, 2 );

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  // TODO: not actually implemented
  integrandPDETrue[0] = (0)          + (0)            + (0);
  integrandPDETrue[1] = (33./10.)    + (-2123./1000.) + (0);

  integrandLOxTrue[0] = 0;
  integrandLOxTrue[1] = 0;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDETrue[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrand[0].PDE[0], integrand[0].PDE[0], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandLOxTrue[0], integrandLOxTrue[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrand[0].Lift[0][0], integrand[0].Lift[0][0], small_tol, close_tol );

//  // test the trace element integral of the functor
//  int quadratureorder = 0;
//  int nIntegrand = qfldElem.nDOF();
//  GalerkinWeightedIntegral<TopoD0, Node, BasisWeightedClass::IntegrandCellType> integral(quadratureorder, nIntegrand);
//
//  BasisWeightedClass::IntegrandCellType rsdPDEElem[2] = {0,0};
//  Real rsdPDETrue[2];
//  Real rsdLOxTrue[2];
//
//  // cell integration for canonical element
//  integral( fcn, xnode, rsdPDEElem, nIntegrand );
//
//  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
//  rsdPDETrue[0] = (0)          + (0)            + (0);
//  rsdPDETrue[1] = (33./10.)    + (-2123./1000.) + (0);
//
//  // Lifting operator residuals
//  rsdLOxTrue[0] = 0;
//  rsdLOxTrue[1] = 0;
//
//  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEElem[0].PDE, small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEElem[1].PDE, small_tol, close_tol );
//
//  SANS_CHECK_CLOSE( rsdLOxTrue[0], rsdPDEElem[0].Lift[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsdLOxTrue[1], rsdPDEElem[1].Lift[0], small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
