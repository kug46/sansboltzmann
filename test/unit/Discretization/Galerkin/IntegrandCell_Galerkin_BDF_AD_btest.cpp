// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_BDF_AD_btest
// testing of residual integrands for Galerkin BDF: Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_BDF.h"

#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"

#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Field/Element/GalerkinWeightedIntegral.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
template class IntegrandCell_Galerkin_BDF<PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D>>
::BasisWeighted<Real, TopoD1, Line, ElementXField<PhysD1,TopoD1, Line>>;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
template class IntegrandCell_Galerkin_BDF<PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D>>
::BasisWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2,TopoD2, Triangle>>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_BDF_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_1D_test )
{
    typedef PDEAdvectionDiffusion<PhysD1,
                                  AdvectiveFlux1D_Uniform,
                                  ViscousFlux1D_Uniform,
                                  Source1D_None> PDEAdvectionDiffusion1D;
    typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
    typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

    typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;

    typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
    typedef ElementSequence<ArrayQ,TopoD1,Line> ElementQFieldSeqCell;

    typedef IntegrandCell_Galerkin_BDF<PDEClass> IntegrandClass;
    typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,ElementXFieldCell> BasisWeightedClass;

    Real u = 1.1;
    AdvectiveFlux1D_Uniform adv(u);

    Real kxx = 2.123;
    ViscousFlux1D_Uniform visc(kxx);

    Source1D_None source;

    GlobalTime time(0);

    PDEClass pde( time, adv, visc, source );

    // static tests
    BOOST_CHECK( pde.D == 1 );
    BOOST_CHECK( pde.N == 1 );

    // flux components
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == true );
    BOOST_CHECK( pde.hasSource() == false );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

    // grid

    int order = 1;
    ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( 1, xfldElem.order() );
    BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

    // line grid
    Real x1, x2;

    x1 = 0;
    x2 = 1;

    xfldElem.DOF(0) = {x1};
    xfldElem.DOF(1) = {x2};

    // timestep
    Real dt = 13.;

    // solution at two timesteps

    ElementQFieldCell qfldElem_t0(order, BasisFunctionCategory_Hierarchical); // current timestep
    ElementQFieldCell qfldElem_tm1(order, BasisFunctionCategory_Hierarchical); // previous timestep

    std::vector<Real> weights = {1., -1.}; //BDF1 weights

    BOOST_CHECK_EQUAL( 1, qfldElem_t0.order() );
    BOOST_CHECK_EQUAL( 2, qfldElem_t0.nDOF() );

    // line solution current
    qfldElem_t0.DOF(0) = 1;
    qfldElem_t0.DOF(1) = 4;

    // integrand
    IntegrandClass fcnint( pde, {0}, dt, weights );

    //////////////////////////////////
    // Case 1: du/dt = 0; Set previous timestep equal to current timestep
    //////////////////////////////////
    ElementQFieldSeqCell qfldElemPastVec(qfldElem_t0.basis(), 1);

    qfldElemPastVec[0] = qfldElem_t0;

    // integrand functor
    BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem_t0, qfldElemPastVec);

    BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
    BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
    BOOST_CHECK( fcn.needsEvaluation() == true );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-12;
    Real sRef;
    Real integrandPDETrue[2];
    ArrayQ integrand[2];

    // x=0
    sRef = 0;
    fcn( {sRef}, integrand, 2 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );

    // x=1
    sRef = 1;
    fcn( {sRef}, integrand, 2 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );

    // x=0.5
    sRef = 1./2.;
    fcn( {sRef}, integrand, 2 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );


    // test the element integral of the functor

    int quadratureorder = 3;
    int nIntegrand = qfldElem_t0.nDOF();
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrand);
    Real rsd[2];

    ArrayQ rsdPDEElem[2] = {0,0};

    // cell integration for canonical element
    integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

    //PDE residual: (time)
    rsd[0] = 0;   // Basis function 1
    rsd[1] = 0;   // Basis function 2

    SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );

    //////////////////////////////////
    // Case 2: BDF1 for non-zero nonzero du/dt; Change solution at t=n-1;
    //////////////////////////////////

    qfldElem_tm1.DOF(0) = 0;
    qfldElem_tm1.DOF(1) = 0;
    qfldElemPastVec[0] = qfldElem_tm1;
    BOOST_CHECK_EQUAL(qfldElemPastVec.nElem(), 1); //to get around cppcheck

    //x=0
    sRef = 0;
    fcn( {sRef}, integrand, 2 );

    integrandPDETrue[0] = 1./13.;
    integrandPDETrue[1] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );

    //x=1
    sRef = 1;
    fcn( {sRef}, integrand, 2 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 4./13.;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );

    //x=0.5
    sRef = 1./3.;
    fcn( {sRef}, integrand, 2 );

    integrandPDETrue[0] = 4./39.;
    integrandPDETrue[1] = 2./39.;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );


    // test the element integral of the functor

    rsdPDEElem[0] = 0;
    rsdPDEElem[1] = 0;

    // cell integration for canonical element
    integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

    //PDE residual: (time)
    rsd[0] = 1./13.;
    rsd[1] = 3./26.;

    SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );

    //////////////////////////////////
    //Case 3, TEST BDF2
    //////////////////////////////////
    //Switch to BDF2 weights: {1.5, -2., 0.5}
    std::vector<Real> weights2 = {1.5, -2., 0.5};
    BOOST_CHECK_EQUAL(weights2.size(), 3); //to get around cppcheck

    IntegrandClass fcnint2( pde, {0}, dt, weights2 );

    ElementQFieldCell qfldElem_tm2(order, BasisFunctionCategory_Hierarchical); // previous timestep

    qfldElem_tm2.DOF(0) = -1./2.;
    qfldElem_tm2.DOF(1) = -2.;

    ElementQFieldSeqCell qfldElemPastVec2(qfldElem_t0.basis(), 2);
    BOOST_CHECK_EQUAL(qfldElemPastVec2.nElem(), 2);

    qfldElemPastVec2[0] = qfldElem_tm1;
    qfldElemPastVec2[1] = qfldElem_tm2;

    // integrand functor
    BasisWeightedClass fcn2 = fcnint2.integrand( xfldElem, qfldElem_t0, qfldElemPastVec2);

    //x=0
    sRef = 0;
    fcn2( {sRef}, integrand, 2 );

    integrandPDETrue[0] = 5./52.;
    integrandPDETrue[1] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );

    //x=1
    sRef = 1;
    fcn2( {sRef}, integrand, 2 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 5./13.;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );

    //x=0.5
    sRef = 1./3.;
    fcn2( {sRef}, integrand, 2 );

    integrandPDETrue[0] = 5./39.;
    integrandPDETrue[1] = 5./78.;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_2D_test )
{
    typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
    typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

    typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;

    typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
    typedef ElementSequence<ArrayQ,TopoD2,Triangle> ElementQFieldSeqCell;

    typedef IntegrandCell_Galerkin_BDF<PDEClass> IntegrandClass;
    typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;

    Real u = 1;
    Real v = 0.2;
    AdvectiveFlux2D_Uniform adv(u, v);

    Real kxx = 2.123;
    Real kxy = 0.553;
    Real kyy = 1.007;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    GlobalTime time(0);
    PDEClass pde( time, adv, visc, source );

    // static tests
    BOOST_CHECK( pde.D == 2 );
    BOOST_CHECK( pde.N == 1 );

    // flux components
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == true );
    BOOST_CHECK( pde.hasSource() == false );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

    // grid

    int order = 1;
    ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( 1, xfldElem.order() );
    BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

    // timestep + BDF1 weights
    Real dt = 13;
    std::vector <Real> weights = {1., -1.};

    // triangle grid
    Real x1, x2, x3, y1, y2, y3;

    x1 = 0;  y1 = 0;
    x2 = 1;  y2 = 0;
    x3 = 0;  y3 = 1;

    xfldElem.DOF(0) = {x1, y1};
    xfldElem.DOF(1) = {x2, y2};
    xfldElem.DOF(2) = {x3, y3};

    // solution

    ElementQFieldCell qfldElem_t0(order, BasisFunctionCategory_Hierarchical); // current timestep
    ElementQFieldCell qfldElem_tm1(order, BasisFunctionCategory_Hierarchical); // previous timestep

    BOOST_CHECK_EQUAL( 1, qfldElem_t0.order() );
    BOOST_CHECK_EQUAL( 3, qfldElem_t0.nDOF() );

    // triangle solution
    qfldElem_t0.DOF(0) = 1;
    qfldElem_t0.DOF(1) = 3;
    qfldElem_t0.DOF(2) = 4;

    // integrand
    IntegrandClass fcnint( pde, {0}, dt, weights  );

    //////////////////////////////////
    // Case 1: BDF1, du/dt = 0; Set previous timestep equal to current timestep
    //////////////////////////////////
    ElementQFieldSeqCell qfldElemPastVec(qfldElem_t0.basis(), 1);
    qfldElemPastVec[0] = qfldElem_t0;

    // integrand functor
    BasisWeightedClass fcn = fcnint.integrand(xfldElem, qfldElem_t0, qfldElemPastVec);

    BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
    BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
    BOOST_CHECK( fcn.needsEvaluation() == true );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-12;
    Real sRef, tRef;
    Real integrandPDETrue[3];
    ArrayQ integrand[3];

    //sref =0; tref = 0;
    sRef = 0;  tRef = 0;
    fcn( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    //sref =1; tref = 0;
    sRef = 1;  tRef = 0;
    fcn( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    //sref =0; tref = 1;
    sRef = 0;  tRef = 1;
    fcn( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    //sref = 0.5; tref = 0.5;
    sRef = 0.5;  tRef = 0.5;
    fcn( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );


    // test the element integral of the functor

    int quadratureorder = 3;
    int nIntegrand = qfldElem_t0.nDOF();
    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);
    Real rsd[3];

    ArrayQ rsdPDEElem[3] = {0,0,0};

    // cell integration for canonical element
    integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

    //PDE residual: (time)
    rsd[0] = 0;   // Basis function 1
    rsd[1] = 0;   // Basis function 2
    rsd[2] = 0;   // Basis function 3

    SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );

    //////////////////////////////////
    // Case 2: BDF1, du/dt nonzero; Set previous timestep equal to zero
    //////////////////////////////////
    qfldElem_tm1.DOF(0) = 0;
    qfldElem_tm1.DOF(1) = 0;
    qfldElem_tm1.DOF(2) = 0;

    qfldElemPastVec[0] = qfldElem_tm1;
    BOOST_CHECK_EQUAL(qfldElemPastVec.nElem(), 1);

    //sref =0; tref = 0;
    sRef = 0;  tRef = 0;
    fcn( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 1./13;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    //sref =1; tref = 0;
    sRef = 1;  tRef = 0;
    fcn( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 3./13;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    //sref =0; tref = 1;
    sRef = 0;  tRef = 1;
    fcn( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 4./13;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    //sref = 0.5; tref = 0.5;
    sRef = 1./3.;  tRef = 2./3.;
    fcn( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 11./117;
    integrandPDETrue[2] = 22./117;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );


    // test the element integral of the functor

    rsdPDEElem[0] = 0;
    rsdPDEElem[1] = 0;
    rsdPDEElem[2] = 0;

    // cell integration for canonical element
    integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

    //PDE residual: (time)
    rsd[0] = 3./104.;
    rsd[1] = 11./312.;
    rsd[2] = 1./26.;

    SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );

    //////////////////////////////////
    // Case 3: BDF2, du/dt nonzero; constant slope
    //////////////////////////////////
    std::vector<Real> weights2 = {1.5, -2., 0.5};
    BOOST_CHECK_EQUAL(weights2.size(), 3); //to get around cppcheck

    IntegrandClass fcnint2( pde, {0}, dt, weights2 );

    ElementQFieldCell qfldElem_tm2(order, BasisFunctionCategory_Hierarchical); // previous timestep
    qfldElem_tm2.DOF(0) = -1./3.;
    qfldElem_tm2.DOF(1) = -4./9.;
    qfldElem_tm2.DOF(2) = -5.;

    ElementQFieldSeqCell qfldElemPastVec2(qfldElem_t0.basis(), 2);
    BOOST_CHECK_EQUAL(qfldElemPastVec2.nElem(), 2);

    qfldElemPastVec2[0] = qfldElem_tm1;
    qfldElemPastVec2[1] = qfldElem_tm2;

    // integrand functor
    BasisWeightedClass fcn2 = fcnint2.integrand( xfldElem, qfldElem_t0, qfldElemPastVec2);

    //sref =0; tref = 0;
    sRef = 0;  tRef = 0;
    fcn2( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 4./39;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    //sref =1; tref = 0;
    sRef = 1;  tRef = 0;
    fcn2( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 77./234;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    //sref =0; tref = 1;
    sRef = 0;  tRef = 1;
    fcn2( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 7./26;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    //sref = 0.5; tref = 0.5;
    sRef = 1./3;  tRef = 2./3;
    fcn2( {sRef, tRef}, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 203./2106;
    integrandPDETrue[2] = 203./1053;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
