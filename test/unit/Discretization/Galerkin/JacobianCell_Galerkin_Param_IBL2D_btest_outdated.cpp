// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_Galerkin_Param_IBL2D_btest
// testing of 2-D line-integral jacobian: IBL

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "pde/IBL/SetVelDofCell_IBL2D.h"
#include "pde/IBL/PDEIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_manifold.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin_Param.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField2D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (err_vec[1] > small_tol){ \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.9 && rate <= 4.0, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_Galerkin_Param_IBL2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin2D_1Line_X1Q1Param1_Surreal2 )
{
#if 1
  typedef PhysD2 PhysDim;
  typedef VarData2DDANCt VarDataType;
  typedef VarTypeDANCt VarType;
  typedef PDEIBL<PhysDim,VarType> PDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template ArrayParam<Real> ArrayParam;
  typedef PDEClass::template MatrixParam<Real> MatrixParam;
  typedef NDPDEClass::VectorX VectorX;
  typedef PDEClass::template ArrayS<Real> ArrayS;

  typedef SurrealS<PDEClass::Nparam> SurrealClass;

  typedef IntegrandCell_Galerkin_manifold<NDPDEClass> IntegrandClass;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );
  BOOST_CHECK( pde.Nparam == 8 );

  // ----------------------- SET UP FIELDS ----------------------- //
  const int order = 1;
  const int nBasis = order + 1;
  const int nelem = 1;

  // grid: one line (order X1)
  VectorX X1 = {0, 0};
  VectorX X2 = {0.8, 0.6};

  XField2D_1Line_X1_1Group xfld(X1,X2);

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, xfld.nElem() );

  // EIF edge velocity DG field
  Field_DG_Cell<PhysDim, TopoD1, VectorX> vfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis*nelem, vfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, vfld.nElem() );

  std::vector<Real> dataVel = {6, -2};

  Field_DG_Cell<PhysDim, TopoD1, VectorX> vxXfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis*nelem, vxXfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, vxXfld.nElem() );

  Field_DG_Cell<PhysDim, TopoD1, VectorX> vzXfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis*nelem, vzXfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, vzXfld.nElem() );

  for_each_CellGroup<TopoD1>::apply( SetVelocityDofCell_IBL2D(dataVel, {0}), (vfld, vxXfld, vzXfld, xfld) );

  // EIF stagnation state DG field
  Field_DG_Cell<PhysDim, TopoD1, ArrayS> stagfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis*nelem, stagfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, stagfld.nElem() );

  Real p0i_L0 = 1.20e+5, T0i_L0 = 200.;
  Real p0i_L1 = 1.30e+5, T0i_L1 = 350.;

  ArrayQ stag_L0 = { p0i_L0, T0i_L0 }, stag_L1 = { p0i_L1, T0i_L1 };

  stagfld.DOF(0) = stag_L0;
  stagfld.DOF(1) = stag_L1;

  // parameter field
  Field_DG_Cell<PhysDim, TopoD1, ArrayParam> paramfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, paramfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      paramfld.nElem() );

  paramfld.DOF(0) = {vfld.DOF(0)[0], vfld.DOF(0)[1],
                     vxXfld.DOF(0)[0], vxXfld.DOF(0)[1],
                     vzXfld.DOF(0)[0], vzXfld.DOF(0)[1],
                     stagfld.DOF(0)[0], stagfld.DOF(0)[1]};

  paramfld.DOF(1) = {vfld.DOF(1)[0], vfld.DOF(1)[1],
                     vxXfld.DOF(1)[0], vxXfld.DOF(1)[1],
                     vzXfld.DOF(1)[0], vzXfld.DOF(1)[1],
                     stagfld.DOF(1)[0], stagfld.DOF(1)[1]};

  // Solution variable DG field
  Field_DG_Cell<PhysDim, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( nBasis*nelem, qfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, qfld.nElem() );

  Real delta_L0 = 2e-2, A_L0 = 1.2;
  Real delta_L1 = 5e-2, A_L1 = 0.2;

  // Line solution
  qfld.DOF(0) = pde.setDOFFrom( VarDataType(delta_L0,A_L0) );
  qfld.DOF(1) = pde.setDOFFrom( VarDataType(delta_L1,A_L1) );

  // ----------------------- INTEGRATE ----------------------- //
  IntegrandClass fcnint( pde, {0} );
  const int nCellGroup = 1;

  const int quadratureOrder = 39;

  const int nDOF = order + 1;
  const int paramDOF = order + 1;

  const int nPDE = pde.N;
  const int nArrayP = pde.Nparam;

  const int m_jac = nPDE*nDOF;         // number of rows in jac
  const int n_jac = nArrayP*paramDOF;  // number of columns in jac

  // jacobian via FD w/ residual operator
  const int nk = 2;              // number of FD steps
  Real delta[nk] = {1e-1, 1e-2}; // FD step sizes

  Real jac[m_jac][n_jac][nk];

  SLA::SparseVector<ArrayQ> rsdGlobalm1(nDOF), rsdGlobalp1(nDOF);

  for (int kk = 0; kk < nk; kk++)
  {
    for (int j = 0; j < paramDOF; j++)
    {
      for (int n = 0; n < nArrayP; n++)
      {
        // residual(param - d param)
        paramfld.DOF(j)[n] -= delta[kk];
        rsdGlobalm1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobalm1),
                                                (paramfld, xfld), qfld, &quadratureOrder, nCellGroup);

        // residual(param + d param)
        paramfld.DOF(j)[n] += 2.0*delta[kk];
        rsdGlobalp1 = 0;
        IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobalp1),
                                                (paramfld, xfld), qfld, &quadratureOrder, nCellGroup);

        BOOST_REQUIRE(rsdGlobalm1.m()*nPDE == m_jac);
        BOOST_REQUIRE(rsdGlobalp1.m()*nPDE == m_jac);

        const int slot_column = j*nArrayP + n;
        for (int i = 0; i < nDOF; i++)
        {
          for (int m = 0; m < nPDE; m++)
          {
            const int slot_row = i*nPDE + m;
            jac[slot_row][slot_column][kk] = (rsdGlobalp1[i][m] - rsdGlobalm1[i][m]) / (2.0*delta[kk]);
          }
        }

        // reset param
        paramfld.DOF(j)[n] -= delta[kk];
      }
    }
  }

  // jacobian via Surreal
  DLA::MatrixD<MatrixParam> mtxGlob(nDOF,paramDOF);
  mtxGlob = 0;
  const int iParam = 0; // parameter index in tuple
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_Galerkin_Param<SurrealClass, iParam>(fcnint, mtxGlob),
                                          (paramfld, xfld), qfld, &quadratureOrder, nCellGroup);

  // ping test
  Real err[nk];
  const Real small_tol = 1e-10;
  for (int i = 0; i < nDOF; i++)
  {
    for (int m = 0; m < nPDE; m++)
    {
      const int slot_row = i*nPDE + m;
      for (int j = 0; j < paramDOF; j++)
      {
        for (int n = 0; n < nArrayP; n++)
        {
          const int slot_column = j*nArrayP + n;
          for (int k = 0; k < nk; k++)
          {
            err[k] = (jac[slot_row][slot_column][k] - DLA::index(mtxGlob(i,j),m,n));
          }
          SANS_CHECK_PING_ORDER(err, delta, small_tol);
        }
      }
    }
  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
