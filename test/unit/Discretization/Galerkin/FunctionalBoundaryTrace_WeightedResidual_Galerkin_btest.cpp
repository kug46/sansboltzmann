// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FunctionalBoundaryTrace_Galerkin_btest
// testing of boundary output integrand routines for : DGBR2 NS

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler2D.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_WeightedResidual_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_WeightedResidual_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( FunctionalBoundaryTrace_WeightedResidual_Galerkin_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionalTest_2D_P1_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // WALL BC INTEGRAND
  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> BCIntegrandBoundaryTrace;

  int qorder = 1;
  StabilizationNitsche stab(qorder);
  NDBCClass bc(pde);
  BCIntegrandBoundaryTrace bcintegrand( pde, bc, {1}, stab );

  // DRAG INTEGRAND
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> IntegrandOutputClass;

  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass output_integrand( outputFcn, {1} );

  //GRID

  XField2D_1Triangle_X1_1Group xfld;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution: P1

  // triangle solution
  qfld.DOF(0) = {1.0, 0.0, 0.0, 1.0};
  qfld.DOF(1) = {1.0, 0.0, 0.0, 1.0};
  qfld.DOF(2) = {1.0, 0.0, 0.0, 1.0};

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  ArrayJ Drag = 0;
  int quadratureOrder[3] = {-1, -1, -1};    // max
  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      FunctionalBoundaryTrace_WeightedResidual_Galerkin( output_integrand, bcintegrand, Drag ),
      xfld, qfld, quadratureOrder, 3 );

  SANS_CHECK_CLOSE( Drag, 1.0, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FunctionalTest_Parallel_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  const Real gamma = 1.4;
  const Real R     = 287.04;        // J/(kg K)
  const Real muRef = 1.789e-5;      // kg/(m s)
//  const Real tSutherland = 110;     // K
//  const Real tRef = 300.0;
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // WALL BC INTEGRAND
  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> BCIntegrandBoundaryTrace;

  int qorder = 2;
  NDBCClass bc(pde);
  StabilizationNitsche stab(qorder);
  BCIntegrandBoundaryTrace bcintegrand( pde, bc, {0,1}, stab );

  // DRAG INTEGRAND
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> IntegrandOutputClass;

  NDOutputClass outputFcn(pde, 0.4, 0.2);
  IntegrandOutputClass output_integrand( outputFcn, {XField2D_Box_Triangle_Lagrange_X1::iBottom,
                                                     XField2D_Box_Triangle_Lagrange_X1::iRight} );

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  //GRID

  int ii = 5;
  int jj = 4;
  XField2D_Box_Triangle_Lagrange_X1 xfld_parallel(world, ii, jj);
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld_parallel(xfld_parallel, qorder, BasisFunctionCategory_Hierarchical);

  XField2D_Box_Triangle_Lagrange_X1 xfld_serial(comm, ii, jj);
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld_serial(xfld_serial, qorder, BasisFunctionCategory_Hierarchical);

  // solution: P1
  ArrayQ q = {1.0, 0.1, 0.2, 1.0};
  qfld_parallel = q;
  qfld_serial = q;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int quadratureOrder[4] = {-1, -1, -1, -1};    // max

  ArrayJ Drag_parallel = 0;
  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      FunctionalBoundaryTrace_WeightedResidual_Galerkin( output_integrand, bcintegrand, Drag_parallel ),
      xfld_parallel, qfld_parallel, quadratureOrder, 4 );

#ifdef SANS_MPI
  Drag_parallel = boost::mpi::all_reduce( *xfld_parallel.comm(), Drag_parallel, std::plus<Real>() );
#endif

  ArrayJ Drag_serial = 0;
  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      FunctionalBoundaryTrace_WeightedResidual_Galerkin( output_integrand, bcintegrand, Drag_serial ),
      xfld_serial, qfld_serial, quadratureOrder, 4 );

  SANS_CHECK_CLOSE( Drag_serial, Drag_parallel, small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
