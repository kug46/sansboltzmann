// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_Dirichlet_Galerkin_Euler_btest
// testing of boundary integrands: Euler

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
//#include "pde/NDConvert/NDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
//#include "pde/NDConvert/NDConvertSpace3D.h"
//#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_Dirichlet_mitState_Galerkin_Euler_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_X1Q1_Symmetry_mitState_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCEuler2D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;

  // PDE

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // BC

  NDBCClass bc(pde);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = pde.setDOFFrom( DensityVelocityPressure2D<Real>(0.967, 0.25, 0.32, 1.07) );
  qfldElem.DOF(1) = pde.setDOFFrom( DensityVelocityPressure2D<Real>(1.03, 0.23, 0.32, 1.08) );
  qfldElem.DOF(2) = pde.setDOFFrom( DensityVelocityPressure2D<Real>(1.1, 0.21, 0.36, 1.05) );

  // integrand functor

  StabilizationNitsche stab(order);
  IntegrandClass fcnint( pde, bc, {0}, stab );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;
  RefCoordTraceType sRef;
  ArrayQ integrandPDE1, integrandPDE2, integrandPDE3;
  ArrayQ integrandPDE[3];

  sRef = 0;
  fcn( sRef, integrandPDE, 3 );

  // PDE residuals: (boundary flux) + (BC term)
  integrandPDE1 =  (0)                - (0);
  integrandPDE2 = { 0 ,
                     27./(25.*sqrt(2)),
                     27./(25.*sqrt(2)),
                   0};
  integrandPDE3 =  (0)                - (0);

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandPDE1[n], integrandPDE[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE2[n], integrandPDE[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE3[n], integrandPDE[2][n], small_tol, close_tol );
  }

  sRef = 1;
  fcn( sRef, integrandPDE, 3 );

  // PDE residuals: (boundary flux) + (BC term)
  integrandPDE1 =  (0)                - (0);
  integrandPDE2 =  (0)                - (0);
  integrandPDE3= { 0 ,
                     21./(20.*sqrt(2)),
                     21./(20.*sqrt(2)),
                   0};


  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandPDE1[n], integrandPDE[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE2[n], integrandPDE[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE3[n], integrandPDE[2][n], small_tol, close_tol );
  }


  sRef = 0.5;
  fcn( sRef, integrandPDE, 3 );

  // PDE residuals: (boundary flux) + (BC term)
  integrandPDE1 =  (0)                 - (0);
  integrandPDE2 = {(   0. ),
                   ( 213./(400.*sqrt(2)) ),
                   ( 213./(400.*sqrt(2)) ),
                   0};
  integrandPDE3 = {0,
                   ( 213./(400.*sqrt(2)) ),
                   ( 213./(400.*sqrt(2)) ),
                   0};

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandPDE1[n], integrandPDE[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE2[n], integrandPDE[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE3[n], integrandPDE[2][n], small_tol, close_tol );
  }


  // test the trace element integral of the functor
//
//  int quadratureorder = -2;    // integrand is quintic
//  int nIntegrandL = qfldElem.nDOF();
//  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrandL);
//
//  ArrayQ rsdPDEElemL[3] = {0,0,0};
//
//  // cell integration for canonical element
//  integral( fcn, xedge, rsdPDEElemL, nIntegrandL );
//
//  // PDE residuals (left): (advective) + (BC term)
//  ArrayQ rsd1PDE =  (0)          - (0);
//  ArrayQ rsd2PDE = {(   83/6. ) + (   2*sqrt(2) - 83/6.        ),
//                    ( 3649/60.) + (  13*sqrt(2) - 1079/12.     ),
//                    ( 6683/60.) + ( 101*sqrt(2)/5. - 8429/60.  ),
//                    ( 5562/5. ) + (5758*sqrt(2)/25. - 95617/60.)};
//  ArrayQ rsd3PDE = {(   14    ) + (  11*sqrt(2)/5. - 14        ),
//                    ( 1467/20.) + ( 143*sqrt(2)/10. - 91       ),
//                    ( 2119/20.) + ( 193*sqrt(2)/10. - 618/5.   ),
//                    (71141/60.) + (6277*sqrt(2)/25. - 19181/12.)};
//
//  for (int n = 0; n < pde.N; n++)
//  {
//    SANS_CHECK_CLOSE( rsd1PDE[n], rsdPDEElemL[0][n], small_tol, close_tol );
//    SANS_CHECK_CLOSE( rsd2PDE[n], rsdPDEElemL[1][n], small_tol, close_tol );
//    SANS_CHECK_CLOSE( rsd3PDE[n], rsdPDEElemL[2][n], small_tol, close_tol );
//  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
