// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_Stabilized_AD_btest
// testing of cell element residual integrands for Galerkin: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized_Corrected.h"

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h" // Basis Weighted
#include "Field/Element/ElementIntegral.h" // Field Weighted

#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_UniformGrad> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDE1DClassPlusTime;
template class IntegrandCell_Galerkin_Stabilized_Corrected< PDE1DClassPlusTime >::
  FieldWeighted<Real, TopoD1, Line, ElementXField<PhysD1,TopoD1, Line> >;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_UniformGrad > PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDE2DClassPlusTime;
template class IntegrandCell_Galerkin_Stabilized_Corrected< PDE2DClassPlusTime >::
  FieldWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2,TopoD2, Triangle> >;

}

using namespace SANS;



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_Stabilized_Corrected_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field_Weight_Correction_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_Stabilized_Corrected<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam > FieldWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3, b = 1.4;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  ElementQFieldClass wfldElem(2,BasisFunctionCategory_Legendre); //true p solution
  wfldElem.DOF(0) = 0;
  wfldElem.DOF(1) = 1.0;
  wfldElem.DOF(2) = 2.0;

  ElementQFieldClass qfldElem(1,BasisFunctionCategory_Hierarchical);
  qfldElem.DOF(0) = 2.0;
  qfldElem.DOF(1) = 3.0;

  Element<Real,TopoD1,Line> efldElem(1, BasisFunctionCategory_Hierarchical);

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  {

    efldElem.DOF(0) = 0;
    efldElem.DOF(1) = 0;

    StabilizationMatrix tau(StabilizationType::SUPG, TauType::Glasby, order);
    IntegrandClass fcnint( pde, {0}, tau );

    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 1e-11;
    std::vector<Real> rsdPDEElemW(efldElem.nDOF(), 0);
    std::vector<Real> rsdPDEElemTrue(efldElem.nDOF(), 0);

    //FROM MATHEMATICA
    rsdPDEElemTrue[0] = ( -2.2798272147706493 ) ;   // Basis function 1
    rsdPDEElemTrue[1] = ( 7.167916183844982 ) ;   // Basis function 2

    int quadratureorder = 6;

    GalerkinWeightedIntegral<TopoD1, Line, Real> integralW(quadratureorder, efldElem.nDOF() );

    integralW( fcnW, xfldElem, rsdPDEElemW.data(), rsdPDEElemW.size() );

    for (int i = 0; i < efldElem.nDOF(); i++)
      SANS_CHECK_CLOSE( rsdPDEElemW[i], rsdPDEElemTrue[i], small_tol, close_tol );
  }

  {
    StabilizationMatrix tau(StabilizationType::GLS, TauType::Glasby, 1);
    IntegrandClass fcnint( pde, {0}, tau );

    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    RefCoordType sRef;
    Real integrandTrue[2] = {0, 0};
    ArrayQ integrand[2] = {0, 0};

    // Test at {0}
    sRef = 0;
    fcnW( sRef, integrand, 2 );

    //PDE residual integrands:
    integrandTrue[0] =  ( -134.80073722469227268 );   // Basis function 1
    integrandTrue[1] =  ( 0 );   // Basis function 2

    SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );

    std::vector<Real> rsdPDEElemW(efldElem.nDOF(), 0);
    std::vector<Real> rsdPDEElemTrue(efldElem.nDOF(), 0);

    //FROM MATHEMATICA
    rsdPDEElemTrue[0] = ( -57.04921856384168 ) ;   // Basis function 1
    rsdPDEElemTrue[1] = ( -39.137741143278966 ) ;   // Basis function 2

    int quadratureorder = 6;
    GalerkinWeightedIntegral<TopoD1, Line, Real> integralW(quadratureorder, efldElem.nDOF() );

    integralW( fcnW, xfldElem, rsdPDEElemW.data(), rsdPDEElemW.size() );

    for (int i = 0; i < efldElem.nDOF(); i++)
      SANS_CHECK_CLOSE( rsdPDEElemW[i], rsdPDEElemTrue[i], small_tol, close_tol );
  }

}
#endif


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_FieldWeighted_2D_Triangle_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef IntegrandCell_Galerkin_Stabilized<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle, ElementXField<PhysD2,TopoD2, Triangle> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 11./10.;
  Real v = 2./10.;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2123./1000.;
  Real kxy = 553./1000.;
  Real kyx = 478./1000.;
  Real kyy = 1007./1000.;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 35./100., b = 182./100., c = 249./100.;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // weighting
  ElementQFieldClass wfldElem(order+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  // estimate
  ElementQFieldClass efldElem(1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, efldElem.order() );
  BOOST_CHECK_EQUAL( 3, efldElem.nDOF() );

  efldElem.DOF(0) = 0;
  efldElem.DOF(1) = 0;
  efldElem.DOF(2) = 0;

  StabilizationMatrix tau(StabilizationType::Adjoint, TauType::Constant, 1, 10.0 );

  // integrand
  IntegrandClass fcnint( pde, {0}, tau );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandTrue[3];
  ArrayQ integrand[3];

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( -9 ) + ( 115091/1000. ) + ( -573/25. ) + ( 15566929/2500. );   // Basis function 1
  integrandTrue[1] = ( 11/5. ) + ( -1181/100. ) + ( 0 ) + ( 0 );   // Basis function 2
  integrandTrue[2] = ( 2/5. ) + ( -3977/500. ) + ( 0 ) + ( 0 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( 78/5. ) + ( -4941/125. ) + ( 0 ) + ( 0 );   // Basis function 1
  integrandTrue[1] = ( -282/5. ) + ( 150279/1000. ) + ( 1216/25. ) + ( 5064521/625. );   // Basis function 2
  integrandTrue[2] = ( -12/5. ) + ( 3977/250. ) + ( 0 ) + ( 0 );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( 78/5. ) + ( -14823/500. ) + ( 0 ) + ( 0 );   // Basis function 1
  integrandTrue[1] = ( -66/5. ) + ( 3543/200. ) + ( 0 ) + ( 0 );   // Basis function 2
  integrandTrue[2] = ( 76/5. ) + ( -21813/500. ) + ( 3753/100. ) + ( -21374291/2500. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {1/3, 1/3}
  sRef = {1/3., 1/3.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( 388/45. ) + ( -179441/9000. ) + ( 25291/1620. ) + ( 132436673/202500. );   // Basis function 1
  integrandTrue[1] = ( -244/15. ) + ( 5182/125. ) + ( 25291/1620. ) + ( 132436673/202500. );   // Basis function 2
  integrandTrue[2] = ( -104/15. ) + ( 38203/1125. ) + ( 25291/1620. ) + ( 132436673/202500. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {1/5, 2/3}
  sRef = {1/5., 2/3.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( 120173/5625. ) + ( -1089937/22500. ) + ( 45346/5625. ) + ( -2909317/6750. );   // Basis function 1
  integrandTrue[1] = ( -106471/5625. ) + ( 6576113/225000. ) + ( 22673/1875. ) + ( -2909317/4500. );   // Basis function 2
  integrandTrue[2] = ( -29002/5625. ) + ( 1137293/56250. ) + ( 45346/1125. ) + ( -2909317/1350. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {3/4, 1/7}
  sRef = {3/4., 1/7.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous) + (source) + (stabilization)
  integrandTrue[0] = ( 323859/27440. ) + ( -177966/6125. ) + ( 5701023/1097600. ) + ( 4648330569/7840000. );   // Basis function 1
  integrandTrue[1] = ( -1007493/27440. ) + ( 18922473/196000. ) + ( 5701023/156800. ) + ( 4648330569/1120000. );   // Basis function 2
  integrandTrue[2] = ( -1927/280. ) + ( 5837583/196000. ) + ( 1900341/274400. ) + ( 1549443523/1960000. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = 5;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[3] = {0,0,0};
  Real rsd[3];
  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  //PDE residual: (advection) + (diffusion) + (source) + (stabilization)
  rsd[0] = (2617/600.) + (-11127/2000.) + (84923/18000.) + (182126281/360000.);   // Basis function 1
  rsd[1] = (-1063/120.) + (66161/3000.) + (125681/18000.) + (209491591/360000.);   // Basis function 2
  rsd[2] = (-691/300.) + (1406/125.) + (153671/18000.) + (-20044597/180000.);   // Basis function 3

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, 10*close_tol );
  SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, 10*close_tol );

}
#endif



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
