// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin_Euler_btest
// testing of boundary integrands: Euler

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
//#include "pde/NDConvert/NDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
//#include "pde/NDConvert/NDConvertSpace3D.h"
//#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin_Euler_test_suite )

#if 0   // disabled: issues with how to expand arrays/matrices to full size for mit-Lagrange formulation
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_X1Q1LG1_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCEuler2D<BCTypeSymmetry, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle> BasisWeightedClass;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  // PDE

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas);

  // BC

  ArrayQ bcdata = {1.2, 0.5, 0.1, 30.0}; // TODO: Replace with numbers from the wisdom of Steve

  NDBCClass bc(pde, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 4 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = pde.setDOFFrom( DensityVelocityPressure2D(2, 6, 5, 23) );
  qfldElem.DOF(1) = pde.setDOFFrom( DensityVelocityPressure2D(3, 2, 7, 42) );
  qfldElem.DOF(2) = pde.setDOFFrom( DensityVelocityPressure2D(4, 3, 4, 87) );

  // Lagrange multiplier
  ElementQFieldTrace lgedge(order, BasisFunctionCategory_Hierarchical);

  lgedge.DOF(0) = ArrayQ( {3, 8, 2, 26} );
  lgedge.DOF(1) = ArrayQ( {2, 3, 6, 42} );

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, lgedge );

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  ArrayQ integrandPDE1, integrandPDE2, integrandPDE3;
  ArrayQ integrandBC1, integrandBC2;
  ArrayQ integrandPDE[3];
  ArrayQ integrandBC[2];

  sRef = 0;
  fcn( sRef, integrandPDE, 3, integrandBC, 2 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 =  (0)                  + (0);
  integrandPDE2 = {( 27./sqrt(2)      ) + (0),
                   ( 48.*sqrt(2)      ) + (0),
                   (231./sqrt(2)      ) + (0),
                   (4077./(2.*sqrt(2))) + (0)};
  integrandPDE3 =  (0)                  + (0);

  // BC residuals
  integrandBC1 = {7 + 9./sqrt(2), 0};
  integrandBC2 = 0;

#if 0
  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandPDE1[n], integrandPDE[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE2[n], integrandPDE[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE3[n], integrandPDE[2][n], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandBC1[n], integrandBC[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandBC2[n], integrandBC[1][n], small_tol, close_tol );
  }
#else
    SANS_CHECK_CLOSE( integrandPDE1[0], integrandPDE[0][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE2[0], integrandPDE[1][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE3[0], integrandPDE[2][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE1[1], integrandPDE[0][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE2[1], integrandPDE[1][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE3[1], integrandPDE[2][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE1[2], integrandPDE[0][2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE2[2], integrandPDE[1][2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE3[2], integrandPDE[2][2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE1[3], integrandPDE[0][3], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE2[3], integrandPDE[1][3], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE3[3], integrandPDE[2][3], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandBC1[0], integrandBC[0][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandBC2[0], integrandBC[1][0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandBC1[1], integrandBC[0][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandBC2[1], integrandBC[1][1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandBC1[2], integrandBC[0][2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandBC2[2], integrandBC[1][2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandBC1[3], integrandBC[0][3], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandBC2[3], integrandBC[1][3], small_tol, close_tol );
#endif

  sRef = 1;
  fcn( sRef, integrandPDE, 3, integrandBC, 2 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                 + (0);
  integrandPDE2 = (0)                 + (0);
  integrandPDE3 = {14.*sqrt(2) + (0), 171./sqrt(2) + (0), 199./sqrt(2) + (0), 4963./(2.*sqrt(2)) + (0)};

  // BC residuals
  integrandBC1 = 0;
  integrandBC2 = 0;

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandPDE1[n], integrandPDE[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE2[n], integrandPDE[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE3[n], integrandPDE[2][n], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandBC1[n], integrandBC[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandBC2[n], integrandBC[1][n], small_tol, close_tol );
  }


  sRef = 0.5;
  fcn( sRef, integrandPDE, 3, integrandBC, 2 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                 + (0);
  integrandPDE2 = {7.*sqrt(2) + (0), 269./(4.*sqrt(2)) + (0), 437./(4.*sqrt(2)) + (0), 2317./(2.*sqrt(2)) + (0)};
  integrandPDE3 = {7.*sqrt(2) + (0), 269./(4.*sqrt(2)) + (0), 437./(4.*sqrt(2)) + (0), 2317./(2.*sqrt(2)) + (0)};

  // BC residuals
  integrandBC1 = 0;
  integrandBC2 = 0;

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandPDE1[n], integrandPDE[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE2[n], integrandPDE[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDE3[n], integrandPDE[2][n], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandBC1[n], integrandBC[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandBC2[n], integrandBC[1][n], small_tol, close_tol );
  }


  // test the trace element integral of the functor

  int quadratureorder = 4;
  int nIntegrandL = qfldElem.nDOF();
  int nIntegrandR = lgedge.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[3] = {0,0,0};
  ArrayQ rsdBCElemR[2] = {0,0};

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDEElemL, nIntegrandL, rsdBCElemR, nIntegrandR );

  // PDE residuals (left): (advective) + (Lagrange)
  ArrayQ rsd1PDE = (0)      + (0);
  ArrayQ rsd2PDE = {83./6. + (0), 3649./60. + (0), 6683./60. + (0),  5562./5.  + (0)};
  ArrayQ rsd3PDE = {14     + (0), 1467./20. + (0), 2119./20. + (0), 71141./60. + (0)};

  // BC residuals:
  ArrayQ rsd1BC = 0;
  ArrayQ rsd2BC = 0;

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( rsd1PDE[n], rsdPDEElemL[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2PDE[n], rsdPDEElemL[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3PDE[n], rsdPDEElemL[2][n], small_tol, close_tol );

    SANS_CHECK_CLOSE( rsd1BC[n], rsdBCElemR[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2BC[n], rsdBCElemR[1][n], small_tol, close_tol );
  }
}
#endif  // disabled: issues with how to expand arrays/matrices to full size for mit-Lagrange formulation

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
