// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing of cell residual functions for Galerkin (specifically for IBL)

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_manifold.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "pde/IBL/SetVelDofCell_IBL2D.h"
#include "pde/IBL/PDEIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "unit/UnitGrids/XField2D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEIBL<PhysD2,VarTypeDANCt> PDEIBL2D;
typedef PDENDConvertSpace<PhysD2,PDEIBL2D> NDPDEClassIBL2D;

template class IntegrandCell_Galerkin_manifold<NDPDEClassIBL2D>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_Galerkin_IBL2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_Galerkin_IBL2D_Line_X1Q1R1_IncompressibleLaminarWallProfile_test )
{
#if 1
  // type definition
  typedef PhysD2 PhysDim;
  typedef VarData2DDANCt VarDataType;
  typedef VarTypeDANCt VarType;
  typedef PDEIBL<PhysDim,VarType> PDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template ArrayParam<Real> ArrayParam;
  typedef NDPDEClass::VectorX VectorX;
  typedef PDEClass::template ArrayS<Real> ArrayS;

  typedef IntegrandCell_Galerkin_manifold<NDPDEClass> IntegrandClass;

  const int nBasis = 2;

  // ----------------------- FIELDS ----------------------- //
  // solution: single line, P1 (aka Q1)
  const int order = 1;

  // grid: single line, P1 (aka X1)
  VectorX X1, X2;
  Real x_L = 1.2, x_R = x_L + 0.8;
  Real z_L = 2.2, z_R = z_L + 0.6;

  X1 = { x_L, z_L };
  X2 = { x_R, z_R };

  XField2D_1Line_X1_1Group xfld( X1, X2 );

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // EIF edge velocity DG field
  Field_DG_Cell<PhysDim, TopoD1, VectorX> vfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, vfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      vfld.nElem() );

  std::vector<Real> dataVel = {6, 2};

  Field_DG_Cell<PhysDim, TopoD1, VectorX> vxXfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, vxXfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      vxXfld.nElem() );

  Field_DG_Cell<PhysDim, TopoD1, VectorX> vzXfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, vzXfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      vzXfld.nElem() );

  for_each_CellGroup<TopoD1>::apply( SetVelocityDofCell_IBL2D(dataVel, {0}), (vfld, vxXfld, vzXfld, xfld) );

  // EIF stagnation state DG field
  Field_DG_Cell<PhysDim, TopoD1, ArrayS> stagfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, stagfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      stagfld.nElem() );

  Real p0i_L = 1.30e+5,  T0i_L = 290.;
  Real p0i_R = 1.40e+5,  T0i_R = 350.;

  ArrayQ stag_L = { p0i_L, T0i_L };
  ArrayQ stag_R = { p0i_R, T0i_R };

  stagfld.DOF(0) = stag_L;
  stagfld.DOF(1) = stag_R;

  // parameter field
  Field_DG_Cell<PhysDim, TopoD1, ArrayParam> paramfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis, paramfld.nDOF() );
  BOOST_CHECK_EQUAL( 1,      paramfld.nElem() );

  paramfld.DOF(0) = {vfld.DOF(0)[0], vfld.DOF(0)[1],
                     vxXfld.DOF(0)[0], vxXfld.DOF(0)[1],
                     vzXfld.DOF(0)[0], vzXfld.DOF(0)[1],
                     stagfld.DOF(0)[0], stagfld.DOF(0)[1]};

  paramfld.DOF(1) = {vfld.DOF(1)[0], vfld.DOF(1)[1],
                     vxXfld.DOF(1)[0], vxXfld.DOF(1)[1],
                     vzXfld.DOF(1)[0], vzXfld.DOF(1)[1],
                     stagfld.DOF(1)[0], stagfld.DOF(1)[1]};

  // Solution variable DG field
  Field_DG_Cell<PhysDim, TopoD1, ArrayS> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( nBasis, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // State vector solution at line cell first node {0}
  Real delta_L = 2e-2;
  Real A_L     = 1.2;

  // State vector solution at line cell second node {1}
  Real delta_R = 2.2e-2;
  Real A_R     = 1.3;

  // Line solution
  qfld.DOF(0) = pde.setDOFFrom( VarDataType(delta_L,A_L) );
  qfld.DOF(1) = pde.setDOFFrom( VarDataType(delta_R,A_R) );

  // ----------------------------- TEST RESIDUAL (INTEGRAL) ----------------------------- //
  const Real small_tol_res = 5e-13;
  const Real close_tol_res = 5e-13;

  // Test the element integral of the functor: quadrature order = 39
  int quadratureOrder = 39;

  SLA::SparseVector<ArrayQ> rsdPDEGlobal(qfld.nDOF());

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // grid integration (just one element)
  const int nCellGroup = 1;

  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin(fcnint, rsdPDEGlobal),
                                          (paramfld, xfld), qfld, &quadratureOrder, nCellGroup );

  Real eRes;
  Real starRes;

  ArrayQ rsdPDETrue[nBasis];

  // PDE residuals: (source) + (advective flux)
  // Basis function 1
  eRes = ( -1.0160079240343214e-01 ) + ( 6.4774236872871135e-02 );
  starRes = ( -2.7242315945861288e-02 ) + ( 4.6313795440318911e-01 );
  rsdPDETrue[0] = { eRes, starRes };

  // Basis function 2
  eRes = ( -7.1776476686346499e-02 ) + ( -6.4774236872871135e-02 );
  starRes = ( -1.4066661472156960e-02 ) + ( -4.6313795440318911e-01 );
  rsdPDETrue[1] = { eRes, starRes };

  for ( int ibasis = 0; ibasis < pde.D; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( rsdPDETrue[ibasis][n], rsdPDEGlobal[ibasis][n], small_tol_res, close_tol_res );
    }
  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
