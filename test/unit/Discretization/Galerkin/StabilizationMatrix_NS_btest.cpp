// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_Stabilized_NS_btest
// testing of cell residual integrands for Stabilized CG; NS

#include <boost/test/unit_test.hpp>

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

#include "Field/Element/GalerkinWeightedIntegral.h" // Basis Weighted
#include "Field/Element/ElementIntegral.h" // Field Weighted

#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Discretization/Galerkin/Stabilization_Galerkin.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE(StabilizationMatrix_Galerkin_NS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( StabilizationMatrix_NS_2D_Triangle_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  //  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass::MatrixQ<Real> MatrixQ;
  typedef NDPDEClass::TensorMatrixQ<Real> TensorMatrixQ;
  typedef NDPDEClass::VectorX VectorX;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;

  typedef ElementXFieldClass::RefCoordType RefCoordType;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

  const Real muRef = 0.1;      // kg/(m s) // FOR RE = 10
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );


  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution

  // set
  ArrayQ q1 = 0;
  ArrayQ q2 = 0;
  ArrayQ q3 = 0;
  pde.setDOFFrom( q1, DensityVelocityPressure2D<Real>(967./1000.,  1./4., 32./100., 107./100) );
  pde.setDOFFrom( q2, DensityVelocityPressure2D<Real>(103./100.,  23./100., 32./100., 108./100) );
  pde.setDOFFrom( q3, DensityVelocityPressure2D<Real>(11./10.,  21./100., 36./100., 105./100) );

  qfldElem.DOF(0) = q1;
  qfldElem.DOF(1) = q2;
  qfldElem.DOF(2) = q3;


  VectorArrayQ gradq = 0;       // gradient

  const int nDOF = 3;
  const int nNodes = 3;
  RefCoordType sRef;
  Real phi[nDOF];
  VectorX gradphi[nDOF];
  sRef = {0, 0};

  xfldElem.evalBasis(sRef, phi, nDOF);
  xfldElem.evalBasisGradient( sRef, qfldElem, gradphi, nDOF );
  qfldElem.evalFromBasis( gradphi, nDOF, gradq );

  VectorX X = {0, 0};

  TensorMatrixQ K = 0.0;
  pde.diffusionViscous( X, q1, gradq, K );

  MatrixQ S = 0.0;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  {
    ArrayQ strongPDE = q1;
    //for unstabilized, do nothing!
    StabilizationMatrix tau(StabilizationType::Unstabilized, TauType::Constant, order);

    tau.solve<Real,Real,Real>(pde, X, q1, K, S, phi, gradphi, order, nNodes, nDOF, strongPDE);

    for (int i=0; i< nDOF; i++)
      SANS_CHECK_CLOSE( strongPDE[i], q1[i], small_tol, close_tol );


  }


  {
    ArrayQ strongPDE = q1;
    //for constant, scale by cb!
    Real cb = 2.0;
    StabilizationMatrix tau(StabilizationType::Adjoint, TauType::Constant, order, cb);

    tau.solve<Real,Real,Real>(pde, X, q1, K, S, phi, gradphi, order, nNodes, nDOF, strongPDE);

    for (int i=0; i< nDOF; i++)
      SANS_CHECK_CLOSE( strongPDE[i], 2.0*q1[i], small_tol, close_tol );


  }

  {
    ArrayQ strongPDE = q1;
    //for glasby, do the glasby thing.
    StabilizationMatrix tau(StabilizationType::Adjoint, TauType::Glasby, order);

    tau.solve<Real,Real,Real>(pde, X, q1, K, S, phi, gradphi, order, nNodes, nDOF, strongPDE);


    MatrixQ tauinv = 0;

    for (int k = 0; k < nDOF; k++)
      pde.jacobianFluxAdvectiveAbsoluteValue(X, q1, gradphi[k], tauinv);

    for (int k = 0; k<nDOF; k++)
      for ( int i = 0; i < 2; i++ )
        for ( int j = 0; j <2; j++ )
          tauinv += gradphi[k][i]*K(i,j)*gradphi[k][j];

    q1 = SANS::DLA::InverseLU::Solve(tauinv, q1);

    for (int i=0; i< nDOF; i++)
      SANS_CHECK_CLOSE( strongPDE[i], q1[i], small_tol, close_tol );
  }


//
//  // integrand
//  IntegrandClass fcnint( pde, {0}, tau );
//
//  // integrand functor
//  BasisWeightedClass fcn = fcnint.integrand(xfldElem, qfldElem );
//
//  BOOST_CHECK_EQUAL( 4, fcn.nEqn() );
//  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
//  BOOST_CHECK( fcn.needsEvaluation() == true );
//
//  const Real small_tol = 1e-13;
//  const Real close_tol = 1e-12;
//  RefCoordType sRef;
//  Real integrandPDETrue[3][4];
//  ArrayQ integrand[3];
//
//  // Test at {0, 0}
//  sRef = {0, 0};
//  fcn( sRef, integrand, 3 );
//
//  //PDE residual integrands: (advective) + (viscous)
//  integrandPDETrue[0][0] = ( 55119./100000. ) + ( 0 ) + ( -0.020325397232862994130325804872809 );
//  integrandPDETrue[0][1] = ( 483119./400000. ) + ( 7./750. ) + ( -0.015433540006838033088023676336516 );
//  integrandPDETrue[0][2] = ( 194747./156250. ) + ( -1./375. ) + ( -0.017082407246312071165873373752227 );
//  integrandPDETrue[0][3] = ( 4360191231./2000000000. ) + ( 8102388629./70131675000. ) + ( 0.01360363645641147910395505831584 );
//  integrandPDETrue[1][0] = ( -967./4000. ) + ( 0 ) + ( 0.0087202425655800719060133155375513 );
//  integrandPDETrue[1][1] = ( -18087./16000. ) + ( -2./375. ) + ( 0.01395002749317493002114402178632 );
//  integrandPDETrue[1][2] = ( -967./12500. ) + ( -1./250. ) + ( 0.0014835125136631030668796545501956 );
//  integrandPDETrue[1][3] = ( -76494583./80000000. ) + ( -858143104./26299378125. ) + ( -0.0053377137498818520879866008483537 );
//  integrandPDETrue[2][0] = ( -967./3125. ) + ( 0 ) + ( 0.011605154667282922224312489335258 );
//  integrandPDETrue[2][1] = ( -967./12500. ) + ( -1./250. ) + ( 0.0014835125136631030668796545501956 );
//  integrandPDETrue[2][2] = ( -365319./312500. ) + ( 1./150. ) + ( 0.015598894732648968098993719202031 );
//  integrandPDETrue[2][3] = ( -76494583./62500000. ) + ( -3488404211./42079005000. ) + ( -0.008265922706529627015968457467486 );
//
//  for (int i=0; i< fcn.nDOF(); i++)
//      for (int j=0; j< fcn.nEqn(); j++)
//        SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrand[i][j], small_tol, close_tol );
//
//  // Test at {1, 0}
//  sRef = {1, 0};
//  fcn( sRef, integrand, 3 );
//
//  //PDE residual integrands: (advective) + (viscous)
//  integrandPDETrue[0][0] = ( 1133./2000. ) + ( 0 ) + ( -0.019828301085087955738304269466092 );
//  integrandPDETrue[0][1] = ( 242059./200000. ) + ( 7./750. ) + ( -0.014778514019757395216476414280114 );
//  integrandPDETrue[0][2] = ( 7883./6250. ) + ( -1./375. ) + ( -0.017050394504466745140415110550812 );
//  integrandPDETrue[0][3] = ( 84919549./40000000. ) + ( 61469461./596756250. ) + ( 0.012267050615622179150920164277678 );
//  integrandPDETrue[1][0] = ( -2369./10000. ) + ( 0 ) + ( 0.0076925710848002255312852480004041 );
//  integrandPDETrue[1][1] = ( -1134487./1000000. ) + ( -2./375. ) + ( 0.013493904416965800912350747574824 );
//  integrandPDETrue[1][2] = ( -2369./31250. ) + ( -1./250. ) + ( 0.0012846096027915943041256667052904 );
//  integrandPDETrue[1][3] = ( -177559057./200000000. ) + ( -69136601./2387025000. ) + ( -0.0063560400744888879739944185094462 );
//  integrandPDETrue[2][0] = ( -206./625. ) + ( 0 ) + ( 0.012135730000287730207019021465688 );
//  integrandPDETrue[2][1] = ( -2369./31250. ) + ( -1./250. ) + ( 0.0012846096027915943041256667052904 );
//  integrandPDETrue[2][2] = ( -18523./15625. ) + ( 1./150. ) + ( 0.015765784901675150836289443845521 );
//  integrandPDETrue[2][3] = ( -7719959./6250000. ) + ( -176741243./2387025000. ) + ( -0.0059110105411332911769257457682313 );
//
//  for (int i=0; i< fcn.nDOF(); i++)
//      for (int j=0; j< fcn.nEqn(); j++)
//        SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrand[i][j], small_tol, close_tol );
//
//  // Test at {0, 1}
//  sRef = {0, 1};
//  fcn( sRef, integrand, 3 );
//
//  //PDE residual integrands: (advective) + (viscous)
//  integrandPDETrue[0][0] = ( 627./1000. ) + ( 0 ) + ( -0.022151290622751690872826329312999 );
//  integrandPDETrue[0][1] = ( 118167./100000. ) + ( 7./750. ) + ( -0.014032450803811696735881725024096 );
//  integrandPDETrue[0][2] = ( 31893./25000. ) + ( -1./375. ) + ( -0.018868576678419463578955664624106 );
//  integrandPDETrue[0][3] = ( 42984099./20000000. ) + ( 95939./1089000. ) + ( 0.0093577911124171503922689708795252 );
//  integrandPDETrue[1][0] = ( -231./1000. ) + ( 0 ) + ( 0.0062197880448817067078138974023496 );
//  integrandPDETrue[1][1] = ( -109851./100000. ) + ( -2./375. ) + ( 0.012770409531466889881890116322005 );
//  integrandPDETrue[1][2] = ( -2079./25000. ) + ( -1./250. ) + ( 0.0012620412723448068539916087020908 );
//  integrandPDETrue[1][3] = ( -15836247./20000000. ) + ( -5383193./217800000. ) + ( -0.0096072261968890130095360296728621 );
//  integrandPDETrue[2][0] = ( -99./250. ) + ( 0 ) + ( 0.015931502577869984165012431910649 );
//  integrandPDETrue[2][1] = ( -2079./25000. ) + ( -1./250. ) + ( 0.0012620412723448068539916087020908 );
//  integrandPDETrue[2][2] = ( -14907./12500. ) + ( 1./150. ) + ( 0.017606535406074656724964055922015 );
//  integrandPDETrue[2][3] = ( -6786963./5000000. ) + ( -13804607./217800000. ) + ( 0.00024943508447186261726705879333685 );
//
//  for (int i=0; i< fcn.nDOF(); i++)
//      for (int j=0; j< fcn.nEqn(); j++)
//        SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrand[i][j], small_tol, close_tol );
//
//  // Test at {1/3, 1/3}
//  sRef = {1./3., 1./3.};
//  fcn( sRef, integrand, 3 );
//
//  //PDE residual integrands: (advective) + (viscous)
//  integrandPDETrue[0][0] = ( 523393./900000. ) + ( 0 ) + ( -0.02073954395968269536573588904461 );
//  integrandPDETrue[0][1] = ( 108038039./90000000. ) + ( 7./750. ) + ( -0.014776176244836022782857650435956 );
//  integrandPDETrue[0][2] = ( 3403393./2700000. ) + ( -1./375. ) + ( -0.017638397065036436563281835508205 );
//  integrandPDETrue[0][3] = ( 348429804073./162000000000. ) + ( 218676399997./2158067025000. ) + ( 0.011666988087258823740579695610592 );
//  integrandPDETrue[1][0] = ( -71231./300000. ) + ( 0 ) + ( 0.0075817127737732241458531962721579 );
//  integrandPDETrue[1][1] = ( -11212771./10000000. ) + ( -2./375. ) + ( 0.013403569061139620666987538974498 );
//  integrandPDETrue[1][2] = ( -71231./900000. ) + ( -1./250. ) + ( 0.0013726071836964021158701114614574 );
//  integrandPDETrue[1][3] = ( -47419440791./54000000000. ) + ( -10252103389./359677837500. ) + ( -0.0071133487662221415540620938523535 );
//  integrandPDETrue[2][0] = ( -3097./9000. ) + ( 0 ) + ( 0.013157831185909471219882692772452 );
//  integrandPDETrue[2][1] = ( -71231./900000. ) + ( -1./250. ) + ( 0.0013726071836964021158701114614574 );
//  integrandPDETrue[2][2] = ( -31897./27000. ) + ( 1./150. ) + ( 0.016265789881340034447411724046748 );
//  integrandPDETrue[2][3] = ( -2061714817./1620000000. ) + ( -8271777877./113582475000. ) + ( -0.0045536393210366821865176017582388 );
//
//
//  for (int i=0; i< fcn.nDOF(); i++)
//      for (int j=0; j< fcn.nEqn(); j++)
//        SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrand[i][j], small_tol, close_tol );
//


//
//
//  // test the element integral of the functor
//
//  int quadratureorder = -1;
//  int nIntegrand = qfldElem.nDOF();
//  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);
//
//  Real rsdPDETrue[3][4];
//  ArrayQ rsdElem[3];
//
//  // cell integration for canonical element
//  integral( fcn, xfldElem, rsdElem, nIntegrand );
////
//
//  //PDE residual: (advection) + (diffusion)
//  rsdPDETrue[0][0] = ( 348931./1200000. ) + ( 0 );
//  rsdPDETrue[0][1] = ( 180044713./300000000. ) + ( 307./180000. );
//  rsdPDETrue[0][2] = ( 189100171./300000000. ) + ( -989./180000. );
//  rsdPDETrue[0][3] = ( 387142357567./360000000000. ) + ( -0.00249983511875611 );
//  rsdPDETrue[1][0] = ( -47443./400000. ) + ( 0 );
//  rsdPDETrue[1][1] = ( -84093299./150000000. ) + ( -239./90000. );
//  rsdPDETrue[1][2] = ( -790541./20000000. ) + ( 19./20000. );
//  rsdPDETrue[1][3] = ( -52695175427./120000000000. ) + ( -0.00539847735443355 );
//  rsdPDETrue[2][0] = ( -103301./600000. ) + ( 0 );
//  rsdPDETrue[2][1] = ( -790541./20000000. ) + ( 19./20000. );
//  rsdPDETrue[2][2] = ( -22155257./37500000. ) + ( 409./90000. );
//  rsdPDETrue[2][3] = ( -114528415643./180000000000. ) + ( 0.00789831247304454 );
//
//  const Real small_tol_rsd = 1e-11;
//  const Real close_tol_rsd = 1e-10;
//
//  for (int i=0; i< fcn.nDOF(); i++)
//    for (int j=0; j< fcn.nEqn(); j++)
//    {
//      //      SANS_CHECK_CLOSE( rsdPDETrue[i][j], rsdElem[i][j], small_tol_rsd, close_tol_rsd );
//    }

}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
