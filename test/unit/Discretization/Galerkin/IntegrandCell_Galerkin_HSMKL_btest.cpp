// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_HSMKL_btest
// testing of cell element residual integrands for Galerkin: PDEHSMKL

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"

#include "Surreal/SurrealS.h"

#include "pde/HSM/PDEHSMKL.h"
#include "pde/HSM/ComplianceMatrix.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_HSMKL.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Tuple/ElementTuple.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_HSMKL_test_suite )

template<class T_, class Tc_, class Tm_, class Tq_, class Tr_>
struct SurrealCombo
{
  typedef T_ T;
  typedef Tc_ Tc;
  typedef Tm_ Tm;
  typedef Tq_ Tq;
  typedef Tr_ Tr;
};

typedef boost::mpl::list< SurrealCombo<Real,Real,Real,Real,Real> //,
                          //SurrealCombo<SurrealS< PDEHSMKL<VarTypeLambda>::N >,Real,Real,Real,Real>,
                          //SurrealCombo<Real,SurrealS<1>,SurrealS<1>,Real,Real>
                        > TTypes;

template<int N, class T>
T SurrealValue(const SurrealS<N,T>& s) { return s.value(); }

Real SurrealValue(const Real& s) { return s; }

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Triangle_2D_test, Combo, TTypes )
{
  typedef typename Combo::T T;
  typedef typename Combo::Tc Tc;
  typedef typename Combo::Tm Tm;
  typedef typename Combo::Tq Tq;
  typedef typename Combo::Tr Tr;
  typedef typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type Ts;

  typedef typename PDEHSMKL<VarTypeLambda>::template VectorX<Real> VectorX_R;
  typedef typename PDEHSMKL<VarTypeLambda>::template VectorX<Tq> VectorX_Tq;
  typedef typename PDEHSMKL<VarTypeLambda>::template VectorE<Tr> VectorE;

  typedef typename PDEHSMKL<VarTypeLambda>::template MatrixX<Real> MatrixX_R;

  typedef PDEHSMKL<VarTypeLambda> PDEClass;

  typedef typename PDEClass::template ArrayQ<T> ArrayQ;
  typedef typename PDEClass::template ArrayQ<Ts> ArrayQS;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ_R;
  typedef typename PDEClass::template ComplianceMatrix<Tc> ComplianceMatrix;

  typedef ElementXField<PhysD2    , TopoD2, Triangle> ElementXFieldClass; // r0 grid
  typedef Element<ArrayQ          , TopoD2, Triangle> ElementQFieldClass; // State variable field

  // Parameter types
  typedef Element<MatrixX_R       , TopoD2, Triangle> ElementE0FieldClass;          // E0 field
  typedef Element<ComplianceMatrix, TopoD2, Triangle> ElementAinvFieldClass;        // Ainv field
  typedef Element<Tm              , TopoD2, Triangle> ElementMassFieldClass;        // mu field
  typedef Element<VectorE         , TopoD2, Triangle> ElementFollowForceFieldClass; // qe field
  typedef Element<VectorX_Tq      , TopoD2, Triangle> ElementGlobForceFieldClass;   // qx field
  typedef Element<VectorX_Tq      , TopoD2, Triangle> ElementAccelFieldClass;       // a field

  typedef typename MakeTuple<ElementTuple, ElementE0FieldClass,
                                           ElementAinvFieldClass,
                                           ElementMassFieldClass,
                                           ElementFollowForceFieldClass,
                                           ElementGlobForceFieldClass,
                                           ElementAccelFieldClass,
                                           ElementXFieldClass>::type ElementParam;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef typename IntegrandClass::template BasisWeighted<T,TopoD2,Triangle, ElementParam > BasisWeightedClass;

  VectorX_R g = { 0.023, -9.81 };
  PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 6 );

  int order = 1;

  // Refernce basis vectors
  ElementE0FieldClass e0fldElem(order, BasisFunctionCategory_Lagrange);

  MatrixX_R e0_00 = {{ 1, 0 },
                     { 0, 1 }};
  MatrixX_R e0_10 = {{ 3./5., -4./5. },
                     { 4./5.,  3./5. }};
  MatrixX_R e0_01 = -e0_10;

  e0fldElem.DOF(0) = e0_00;
  e0fldElem.DOF(1) = e0_10;
  e0fldElem.DOF(2) = e0_01;

  // Extensional compliance matrix
  ElementAinvFieldClass AinvfldElem(order, BasisFunctionCategory_Lagrange);

  // Unrealistic low values for E, but better for testing
  Real E_00 = 70e4, nu_00 = 1./3.,  t_00 = 1./100.;
  Real E_10 = 80e4, nu_10 = 1./4.,  t_10 = 1./200.;
  Real E_01 = 90e2, nu_01 = 5./11., t_01 = 1./400.;

  ComplianceMatrix AijInv_00 = calcComplianceMatrix( E_00, nu_00, t_00 );
  ComplianceMatrix AijInv_10 = calcComplianceMatrix( E_10, nu_10, t_10 );
  ComplianceMatrix AijInv_01 = calcComplianceMatrix( E_01, nu_01, t_01 );

  AinvfldElem.DOF(0) = AijInv_00;
  AinvfldElem.DOF(1) = AijInv_10;
  AinvfldElem.DOF(2) = AijInv_01;

  // Lumped shell mass
  ElementMassFieldClass mufldElem(order, BasisFunctionCategory_Lagrange);

  Real mu_00 = 0.0123;
  Real mu_10 = 3.923;
  Real mu_01 = 0.946;

  mufldElem.DOF(0) = mu_00;
  mufldElem.DOF(1) = mu_10;
  mufldElem.DOF(2) = mu_01;

  // Following force
  ElementFollowForceFieldClass qefldElem(order, BasisFunctionCategory_Lagrange);

  VectorE qe_00 = { -3   ,  4./7. };
  VectorE qe_10 = { 2./5., -2./3. };
  VectorE qe_01 = { 1./2.,  1./4. };

  qefldElem.DOF(0) = qe_00;
  qefldElem.DOF(1) = qe_10;
  qefldElem.DOF(2) = qe_01;

  // Global forcing
  ElementGlobForceFieldClass qxfldElem(order, BasisFunctionCategory_Lagrange);

  VectorX_Tq qx_00 = {  23.  , 0.0123 };
  VectorX_Tq qx_10 = { 4.935, -2.77 };
  VectorX_Tq qx_01 = { -9.54, -0.32 };

  qxfldElem.DOF(0) = qx_00;
  qxfldElem.DOF(1) = qx_10;
  qxfldElem.DOF(2) = qx_01;

  // Accelerations
  ElementAccelFieldClass afldElem(order, BasisFunctionCategory_Lagrange);

  VectorX_Tq a_00 = { 3.245, -1./3. };
  VectorX_Tq a_10 = { 1.43 , 8.79 };
  VectorX_Tq a_01 = { 0.034, 23.12 };

  afldElem.DOF(0) = a_00;
  afldElem.DOF(1) = a_10;
  afldElem.DOF(2) = a_01;

  // Undeformed geometry (triangle grid)
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_REQUIRE_EQUAL( 3, xfldElem.nDOF() );

  Real x0_00 = 0, y0_00 = 0;
  Real x0_10 = 1, y0_10 = 0;
  Real x0_01 = 0, y0_01 = 1;

  xfldElem.DOF(0) = {x0_00, y0_00};
  xfldElem.DOF(1) = {x0_10, y0_10};
  xfldElem.DOF(2) = {x0_01, y0_01};

  ElementQFieldClass varfldElem(order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 1, varfldElem.order() );
  BOOST_REQUIRE_EQUAL( 3, varfldElem.nDOF() );

  // State vector solution at first cell corner {0,0}
  Real   rx_00 = -1;
  Real   ry_00 = -1;
  Real lam3_00 = 3./5.;
  Real  f11_00 = 3;
  Real  f22_00 = -2./5.;
  Real  f12_00 = 1./2.;

  // State vector solution at second cell corner {1,0}
  Real  rx_10 = 1;
  Real  ry_10 = -1./2.;
  Real lam3_10 = 5./13.;
  Real  f11_10 = 1;
  Real  f22_10 = -1./2.;
  Real  f12_10 = 2;

  // State vector solution at third cell corner {0,1}
  Real   rx_01 = -1./2.;
  Real   ry_01 = 2;
  Real lam3_01 = 7./25.;
  Real  f11_01 = -5;
  Real  f22_01 = 9;
  Real  f12_01 = 2;

  // Triangle solution
  varfldElem.DOF(0) = pde.setDOFFrom( PositionLambdaForces2D(rx_00, ry_00, lam3_00, f11_00, f22_00, f12_00) );
  varfldElem.DOF(1) = pde.setDOFFrom( PositionLambdaForces2D(rx_10, ry_10, lam3_10, f11_10, f22_10, f12_10) );
  varfldElem.DOF(2) = pde.setDOFFrom( PositionLambdaForces2D(rx_01, ry_01, lam3_01, f11_01, f22_01, f12_01) );

  // Integrand
  IntegrandClass fcnint( pde, {0} );

  // Explicitly create a paramater instance.
  // If it was created in the argument below, the temporary variable in the
  // function argument would go out of scope and result in undefined behavior
  ElementParam params = ( e0fldElem, AinvfldElem, mufldElem, qefldElem, qxfldElem, afldElem, xfldElem );

  BasisWeightedClass fcn = fcnint.integrand( params, varfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 6, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  typedef ElementXFieldClass::RefCoordType RefCoordType;

  const Real small_tol = 8e-13;
  const Real close_tol = 8e-13;
  RefCoordType sRef;
  Real eps11Integrand;
  Real eps22Integrand;
  Real eps12Integrand;
  Real f1Integrand;
  Real f2Integrand;
  Real e3Integrand;
  ArrayQ_R integrandTrue[3];
  ArrayQS integrand[3];

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (source) + (flux)

  // Basis function 1
  eps11Integrand = ( 4.640623501067877 ) + ( 0. );
  eps22Integrand = ( 1.1091288798845045 ) + ( 0. );
  eps12Integrand = ( -0.07750840683285665 ) + ( 0. );
  f1Integrand    = ( 5.222690706518728 ) + ( 3.5983498555864237 );
  f2Integrand    = ( -20.86631364417231 ) + ( 0.8750709528181704 );
  e3Integrand    = ( -5.597762187096939 ) + ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( -0.3329858167442386 );
  f2Integrand    = ( 0. ) + ( -0.7627175679361079 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( -2.1963312129518204 );
  f2Integrand    = ( 0. ) + ( -2.035561730928931 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
  }


  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (source) + (flux)

  // Basis function 1
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 2.5025369957239674 );
  f2Integrand    = ( 0. ) + ( 1.949762767725383 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 4.083653902743402 ) + ( 0. );
  eps22Integrand = ( 1.6662523472565989 ) + ( 0. );
  eps12Integrand = ( 1.2891330379518051 ) + ( 0. );
  f1Integrand    = ( -52.701957841449314 ) + ( 0.6727196416924307 );
  f2Integrand    = ( -54.67350188897776 ) + ( -1.7846832884812884 );
  e3Integrand    = ( -0.16550439201356681 ) + ( 0. );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( -1.8716252728045335 );
  f2Integrand    = ( 0. ) + ( -1.4523679567162646 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
  }


  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (source) + (flux)

  // Basis function 1
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( -1.6759826049027442 );
  f2Integrand    = ( 0. ) + ( 8.231426163588333 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 5.824109380910017 );
  f2Integrand    = ( 0. ) + ( 1.9182903549869517 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 3.859562898915146 ) + ( 0. );
  eps22Integrand = ( 1.7934674041151575 ) + ( 0. );
  eps12Integrand = ( 1.540435681273592 ) + ( 0. );
  f1Integrand    = ( -24.309005455755695 ) + ( 0.9614207675775859 );
  f2Integrand    = ( -21.341582605949018 ) + ( -8.687668394962513 );
  e3Integrand    = ( -11.894951929449663 ) + ( 0. );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
  }


  // Test at {1/3, 1/3}
  sRef = {1/3., 1/3.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (source) + (flux)

  // Basis function 1
  eps11Integrand = ( 1.5421653659549688 ) + ( 0. );
  eps22Integrand = ( 0.3636889600431071 ) + ( 0. );
  eps12Integrand = ( -0.07812895028678775 ) + ( 0. );
  f1Integrand    = ( -10.859769291316105 ) + ( 1.4749680821358826 );
  f2Integrand    = ( -5.682124741149076 ) + ( 3.6854199613772956 );
  e3Integrand    = ( -1.865920729032313 ) + ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 1.3959685856660438 ) + ( 0. );
  eps22Integrand = ( 0.5098857403320323 ) + ( 0. );
  eps12Integrand = ( 0.39625760623216505 ) + ( 0. );
  f1Integrand    = ( -7.494988848590126 ) + ( 2.0546144019527364 );
  f2Integrand    = ( -9.697745758376037 ) + ( -0.20970350047681485 );
  e3Integrand    = ( -0.055168130671188934 ) + ( 0. );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 1.1967194914051955 ) + ( 0. );
  eps22Integrand = ( 0.7091348345928805 ) + ( 0. );
  eps12Integrand = ( 0.5420987855603021 ) + ( 0. );
  f1Integrand    = ( -5.317235887500068 ) + ( -1.0355119060595892 );
  f2Integrand    = ( -11.04301286566633 ) + ( -4.05853269420257 );
  e3Integrand    = ( -3.9649839764832207 ) + ( 0. );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
  }

  // Test the element integral of the functor
  /*
   Quad order 8
   Error: < 3e-8%

   Quad order 9
   Error: < 3e-12%

   USE QUAD ORDER 9: No increase in accuracy increasing past quad order 9
   */
  int quadratureorder = 9;
  const Real small_tol_res = 3e-12;
  const Real close_tol_res = 3e-12;
  int nIntegrand = varfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQS> integral(quadratureorder, nIntegrand);

  Real eps11Res;
  Real eps22Res;
  Real eps12Res;
  Real f1Res;
  Real f2Res;
  Real e3Res;
  ArrayQ_R residualTrue[3];
  ArrayQS residual[3] = {0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, residual, nIntegrand );

  //PDE residuals: (source) + (flux)

  // Basis function 1
  eps11Res = ( 0.7716713247776081 ) + ( 0 );
  eps22Res = ( 0.18259706334468623 ) + ( 0 );
  eps12Res = ( -0.032527873308914386 ) + ( 0 );
  f1Res    = ( -3.8548013714719254 ) + ( 0.7374840410679413 );
  f2Res    = ( -3.0002265131047494 ) + ( 1.8427099806886478 );
  e3Res    = ( -0.9329603645161565 ) + ( 0 );
  residualTrue[0] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 2
  eps11Res = ( 0.693640465572408 ) + ( 0 );
  eps22Res = ( 0.2606343337602038 ) + ( 0 );
  eps12Res = ( 0.2023104789183872 ) + ( 0 );
  f1Res    = ( -5.006535728281685 ) + ( 1.0273072009763682 );
  f2Res    = ( -5.9147172380984205 ) + ( -0.10485175023840755 );
  e3Res    = ( -0.02758406533559447 ) + ( 0 );
  residualTrue[1] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 3
  eps11Res = ( 0.6095849300650793 ) + ( 0 );
  eps22Res = ( 0.3406533714771285 ) + ( 0 );
  eps12Res = ( 0.26747186463817957 ) + ( 0 );
  f1Res    = ( -3.0068386851356794 ) + ( -0.5177559530297948 );
  f2Res    = ( -5.030362433206083 ) + ( -2.029266347101285 );
  e3Res    = ( -1.9824919882416105 ) + ( 0 );
  residualTrue[2] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( residualTrue[0][n], SurrealValue(residual[0][n]), small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[1][n], SurrealValue(residual[1][n]), small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[2][n], SurrealValue(residual[2][n]), small_tol_res, close_tol_res );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Quad_2D_test, Combo, TTypes )
{
  typedef typename Combo::T T;
  typedef typename Combo::Tc Tc;
  typedef typename Combo::Tm Tm;
  typedef typename Combo::Tq Tq;
  typedef typename Combo::Tr Tr;
  typedef typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type Ts;

  typedef typename PDEHSMKL<VarTypeLambda>::template VectorX<Real> VectorX_R;
  typedef typename PDEHSMKL<VarTypeLambda>::template VectorX<Tq> VectorX_Tq;
  typedef typename PDEHSMKL<VarTypeLambda>::template VectorE<Tr> VectorE;

  typedef typename PDEHSMKL<VarTypeLambda>::template MatrixX<Real> MatrixX_R;

  typedef PDEHSMKL<VarTypeLambda> PDEClass;

  typedef typename PDEClass::template ArrayQ<T> ArrayQ;
  typedef typename PDEClass::template ArrayQ<Ts> ArrayQS;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ_R;
  typedef typename PDEClass::template ComplianceMatrix<Tc> ComplianceMatrix;

  typedef ElementXField<PhysD2    , TopoD2, Quad> ElementXFieldClass; // r0 grid
  typedef Element<ArrayQ          , TopoD2, Quad> ElementQFieldClass; // State variable field

  // Parameter types
  typedef Element<MatrixX_R       , TopoD2, Quad> ElementE0FieldClass;          // E0 field
  typedef Element<ComplianceMatrix, TopoD2, Quad> ElementAinvFieldClass;        // Ainv field
  typedef Element<Tm              , TopoD2, Quad> ElementMassFieldClass;        // mu field
  typedef Element<VectorE         , TopoD2, Quad> ElementFollowForceFieldClass; // qe field
  typedef Element<VectorX_Tq      , TopoD2, Quad> ElementGlobForceFieldClass;   // qx field
  typedef Element<VectorX_Tq      , TopoD2, Quad> ElementAccelFieldClass;       // a field

  typedef typename MakeTuple<ElementTuple, ElementE0FieldClass,
                                           ElementAinvFieldClass,
                                           ElementMassFieldClass,
                                           ElementFollowForceFieldClass,
                                           ElementGlobForceFieldClass,
                                           ElementAccelFieldClass,
                                           ElementXFieldClass>::type ElementParam;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef typename IntegrandClass::template BasisWeighted<T,TopoD2,Quad, ElementParam > BasisWeightedClass;

  VectorX_R g = { 0.023, -9.81 };
  PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);


  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 6 );

  int order = 1;

  // Refernce basis vectors
  ElementE0FieldClass e0fldElem(order, BasisFunctionCategory_Hierarchical);

  MatrixX_R e0_00 = {{ 1, 0 },
                     { 0, 1 }};
  MatrixX_R e0_10 = {{ 3./5., -4./5. },
                     { 4./5.,  3./5. }};
  MatrixX_R e0_11 = -e0_00;
  MatrixX_R e0_01 = -e0_10;

  e0fldElem.DOF(0) = e0_00;
  e0fldElem.DOF(1) = e0_10;
  e0fldElem.DOF(2) = e0_11;
  e0fldElem.DOF(3) = e0_01;

  // Extensional compliance matrix
  ElementAinvFieldClass AinvfldElem(order, BasisFunctionCategory_Hierarchical);

  Real E_00 = 70e4, nu_00 = 1./3.,  t_00 = 1./100.;
  Real E_10 = 80e4, nu_10 = 1./4.,  t_10 = 1./200.;
  Real E_11 = 90e4, nu_11 = 1./5.,  t_11 = 1./300.;
  Real E_01 = 90e2, nu_01 = 5./11., t_01 = 1./400.;

  ComplianceMatrix AijInv_00 = calcComplianceMatrix( E_00, nu_00, t_00 );
  ComplianceMatrix AijInv_10 = calcComplianceMatrix( E_10, nu_10, t_10 );
  ComplianceMatrix AijInv_11 = calcComplianceMatrix( E_11, nu_11, t_11 );
  ComplianceMatrix AijInv_01 = calcComplianceMatrix( E_01, nu_01, t_01 );

  AinvfldElem.DOF(0) = AijInv_00;
  AinvfldElem.DOF(1) = AijInv_10;
  AinvfldElem.DOF(2) = AijInv_11;
  AinvfldElem.DOF(3) = AijInv_01;

  // Lumped shell mass
  ElementMassFieldClass mufldElem(order, BasisFunctionCategory_Hierarchical);

  Real mu_00 = 0.0123;
  Real mu_10 = 3.923;
  Real mu_11 = 9.13;
  Real mu_01 = 0.946;

  mufldElem.DOF(0) = mu_00;
  mufldElem.DOF(1) = mu_10;
  mufldElem.DOF(2) = mu_11;
  mufldElem.DOF(3) = mu_01;

  // Following force
  ElementFollowForceFieldClass qefldElem(order, BasisFunctionCategory_Hierarchical);

  VectorE qe_00 = { -3   ,  4./7. };
  VectorE qe_10 = { 2./5., -2./3. };
  VectorE qe_11 = {  1   ,  -7    };
  VectorE qe_01 = { 1./2.,  1./4. };

  qefldElem.DOF(0) = qe_00;
  qefldElem.DOF(1) = qe_10;
  qefldElem.DOF(2) = qe_11;
  qefldElem.DOF(3) = qe_01;

  // Global forcing
  ElementGlobForceFieldClass qxfldElem(order, BasisFunctionCategory_Hierarchical);

  VectorX_Tq qx_00 = {  23. , 0.0123 };
  VectorX_Tq qx_10 = { 4.935, -2.77 };
  VectorX_Tq qx_11 = {  7.35, 1.43 };
  VectorX_Tq qx_01 = { -9.54, -0.32 };

  qxfldElem.DOF(0) = qx_00;
  qxfldElem.DOF(1) = qx_10;
  qxfldElem.DOF(2) = qx_11;
  qxfldElem.DOF(3) = qx_01;

  // Accelerations
  ElementAccelFieldClass afldElem(order, BasisFunctionCategory_Hierarchical);

  VectorX_Tq a_00 = { 3.245, -1./3. };
  VectorX_Tq a_10 = { 1.43 , 8.79 };
  VectorX_Tq a_11 = { -0.874 , 1.3 };
  VectorX_Tq a_01 = { 0.034, 23.12 };

  afldElem.DOF(0) = a_00;
  afldElem.DOF(1) = a_10;
  afldElem.DOF(2) = a_11;
  afldElem.DOF(3) = a_01;

  // Undeformed geometry (quad grid)
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  Real x0_00 = 0, y0_00 = 0;
  Real x0_10 = 1, y0_10 = 0;
  Real x0_11 = 1, y0_11 = 1;
  Real x0_01 = 0, y0_01 = 1;

  xfldElem.DOF(0) = {x0_00, y0_00};
  xfldElem.DOF(1) = {x0_10, y0_10};
  xfldElem.DOF(2) = {x0_11, y0_11};
  xfldElem.DOF(3) = {x0_01, y0_01};

  ElementQFieldClass varfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, varfldElem.order() );
  BOOST_CHECK_EQUAL( 4, varfldElem.nDOF() );

  // State vector solution at first cell corner {0,0}
  Real   rx_00 = -1;
  Real   ry_00 = -1;
  Real lam3_00 = 3./5.;
  Real  f11_00 = 3;
  Real  f22_00 = -2./5.;
  Real  f12_00 = 1./2.;

  // State vector solution at second cell corner {1,0}
  Real   rx_10 = 1;
  Real   ry_10 = -1./2.;
  Real lam3_10 = 5./13.;
  Real  f11_10 = 1;
  Real  f22_10 = -1./2.;
  Real  f12_10 = 2;

  // State vector solution at third cell corner {1,1}
  Real   rx_11 = 2;
  Real   ry_11 = 3;
  Real lam3_11 = 8./17.;
  Real  f11_11 = 2;
  Real  f22_11 = 3;
  Real  f12_11 = -1./3.;

  // State vector solution at third cell corner {0,1}
  Real   rx_01 = -1./2.;
  Real   ry_01 = 2;
  Real lam3_01 = 7./25.;
  Real  f11_01 = -5;
  Real  f22_01 = 9;
  Real  f12_01 = 2;

  // Quad solution
  varfldElem.DOF(0) = pde.setDOFFrom( PositionLambdaForces2D(rx_00, ry_00, lam3_00, f11_00, f22_00, f12_00) );
  varfldElem.DOF(1) = pde.setDOFFrom( PositionLambdaForces2D(rx_10, ry_10, lam3_10, f11_10, f22_10, f12_10) );
  varfldElem.DOF(2) = pde.setDOFFrom( PositionLambdaForces2D(rx_11, ry_11, lam3_11, f11_11, f22_11, f12_11) );
  varfldElem.DOF(3) = pde.setDOFFrom( PositionLambdaForces2D(rx_01, ry_01, lam3_01, f11_01, f22_01, f12_01) );

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // Explicitly create a paramater instance.
  // If it was created in the argument below, the temporary variable in the
  // function argument would go out of scope and result in undefined behavior
  ElementParam params = ( e0fldElem, AinvfldElem, mufldElem, qefldElem, qxfldElem, afldElem, xfldElem );

  BasisWeightedClass fcn = fcnint.integrand( params, varfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 6, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  typedef ElementXFieldClass::RefCoordType RefCoordType;

  const Real small_tol = 8e-13;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real eps11Integrand;
  Real eps22Integrand;
  Real eps12Integrand;
  Real f1Integrand;
  Real f2Integrand;
  Real e3Integrand;
  ArrayQ_R integrandTrue[4];
  ArrayQS integrand[4];

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (source) + (flux)

  // Basis function 1
  eps11Integrand = ( 4.640623501067878 ) + ( 0. );
  eps22Integrand = ( 1.1091288798845045 ) + ( 0. );
  eps12Integrand = ( -0.07750840683285731 ) + ( 0. );
  f1Integrand    = ( 5.222690706518722 ) + ( 3.598349855586423 );
  f2Integrand    = ( -20.86631364417231 ) + ( 0.8750709528181714 );
  e3Integrand    = ( -1.399440546774235 ) + ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( -0.33298581674423733 );
  f2Integrand    = ( 0. ) + ( -0.7627175679361076 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 0. );
  f2Integrand    = ( 0. ) + ( 0. );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 4
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( -2.1963312129518195 );
  f2Integrand    = ( 0. ) + ( -2.035561730928933 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[3] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[3][n], SurrealValue(integrand[3][n]), small_tol, close_tol );
  }


  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (source) + (flux)

  // Basis function 1
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 0.13396678112665417 );
  f2Integrand    = ( 0. ) + ( 1.9025506716137004 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 5.6759980463338415 ) + ( 0. );
  eps22Integrand = ( 2.0739082036661594 ) + ( 0. );
  eps12Integrand = ( 2.3088160355319887 ) + ( 0. );
  f1Integrand    = ( -52.701957841449314 ) + ( 2.805194327047736 );
  f2Integrand    = ( -54.67350188897776 ) + ( -0.7527406738870397 );
  e3Integrand    = ( -0.21720637408511445 ) + ( 0. );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( -2.27759367578769 );
  f2Integrand    = ( 0. ) + ( -0.6518595642534837 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 4
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 0. );
  f2Integrand    = ( 0. ) + ( 0. );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[3] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[3][n], SurrealValue(integrand[3][n]), small_tol, close_tol );
  }


  // Test at {1, 1}
  sRef = {1, 1};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (source) + (flux)

  // Basis function 1
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 0. );
  f2Integrand    = ( 0. ) + ( 0. );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 1.1431280610385437 );
  f2Integrand    = ( 0. ) + ( 1.7180187363548183 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 7.939917029843768 ) + ( 0. );
  eps22Integrand = ( 1.3087496368228981 ) + ( 0. );
  eps12Integrand = ( 0.5082976226264764 ) + ( 0. );
  f1Integrand    = ( -70.67831058491228 ) + ( -2.8673158704060966 );
  f2Integrand    = ( -78.44619856321927 ) + ( 1.1239379021828415 );
  e3Integrand    = ( 1.9521262736984895 ) + ( 0. );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 4
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 2.318288845726564 );
  f2Integrand    = ( 0. ) + ( -1.894578820635887 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[3] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[3][n], SurrealValue(integrand[3][n]), small_tol, close_tol );
  }


  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (source) + (flux)

  // Basis function 1
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 4.417084442323903 );
  f2Integrand    = ( 0. ) + ( 7.542498065133335 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 0. );
  f2Integrand    = ( 0. ) + ( 0. );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0. ) + ( 0. );
  eps22Integrand = ( 0. ) + ( 0. );
  eps12Integrand = ( 0. ) + ( 0. );
  f1Integrand    = ( 0. ) + ( 6.066444320125144 );
  f2Integrand    = ( 0. ) + ( 0.8934994558819782 );
  e3Integrand    = ( 0. ) + ( 0. );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 4
  eps11Integrand = ( 5.723912620527121 ) + ( 0. );
  eps22Integrand = ( 1.429117682503182 ) + ( 0. );
  eps12Integrand = ( 1.246582490133337 ) + ( 0. );
  f1Integrand    = ( -24.309005455755695 ) + ( -4.3372271833312634 );
  f2Integrand    = ( -21.341582605949014 ) + ( -11.77383395422363 );
  e3Integrand    = ( -2.8694862773308083 ) + ( 0. );
  integrandTrue[3] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[3][n], SurrealValue(integrand[3][n]), small_tol, close_tol );
  }


  // Test at {1/2, 1/2}
  sRef = {1/2., 1/2.};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (source) + (flux)

  // Basis function 1
  eps11Integrand = ( 1.5208802948144706 ) + ( 0. );
  eps22Integrand = ( 0.31670443022611383 ) + ( 0. );
  eps12Integrand = ( -0.17745967557075437 ) + ( 0. );
  f1Integrand    = ( -11.633740884913061 ) + ( 0.8636312905659234 );
  f2Integrand    = ( -6.8051728688021855 ) + ( 1.1544859596692918 );
  e3Integrand    = ( -0.358787742427236 ) + ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 1.4455791040083368 ) + ( 0. );
  eps22Integrand = ( 0.3920056210322478 ) + ( 0. );
  eps12Integrand = ( 0.3413173650143878 ) + ( 0. );
  f1Integrand    = ( -7.7293030451509015 ) + ( 1.3750635764093595 );
  f2Integrand    = ( -11.041384840127623 ) + ( 1.8360629454612525 );
  e3Integrand    = ( -0.01927163023452523 ) + ( 0. );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 1.5298118744168456 ) + ( 0. );
  eps22Integrand = ( 0.3077728506237389 ) + ( 0. );
  eps12Integrand = ( 0.1437253309255344 ) + ( 0. );
  f1Integrand    = ( -9.504502029518582 ) + ( -0.5393821361115869 );
  f2Integrand    = ( -9.556084235839293 ) + ( -1.337072828368668 );
  e3Integrand    = ( 0.49131630332315357 ) + ( 0. );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 4
  eps11Integrand = ( 1.261429314003451 ) + ( 0. );
  eps22Integrand = ( 0.5761554110371335 ) + ( 0. );
  eps12Integrand = ( 0.5259294003440804 ) + ( 0. );
  f1Integrand    = ( -5.267355997182122 ) + ( -0.9637107294791487 );
  f2Integrand    = ( -12.406017312196877 ) + ( -2.0816312374554515 );
  e3Integrand    = ( -0.7523621013242813 ) + ( 0. );
  integrandTrue[3] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[3][n], SurrealValue(integrand[3][n]), small_tol, close_tol );
  }


  // Test the element integral of the functor with a matching Gauss-quadrature scheme in Mathematica
  //
  // Quad order 3
  // Error: < 0.15%
  //
  // Quad order 4+
  // Error: < 0.00131 %
  //
  // USE QUAD ORDER 4: No increase in accuracy increasing past quad order 4

  int quadratureorder = 4;
  const Real small_tol_res = 0.00131; // Errors are so high because SANS quadrature scheme does not
  const Real close_tol_res = 0.00131; // match well with Mathematica quadrature scheme
  int nIntegrand = varfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Quad, ArrayQS> integral(quadratureorder, nIntegrand);

  Real eps11Res;
  Real eps22Res;
  Real eps12Res;
  Real f1Res;
  Real f2Res;
  Real e3Res;
  ArrayQ_R residualTrue[4];
  ArrayQS residual[4] = {0,0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, residual, nIntegrand );

  //PDE residuals: (source) + (flux)

  // Basis function 1
  eps11Res = ( 1.3977045336798344 ) + ( 0 );
  eps22Res = ( 0.30517183566840994 ) + ( 0 );
  eps12Res = ( -0.1287432487098044 ) + ( 0 );
  f1Res    = ( -7.595344343539347 ) + ( 1.2548709502970306 );
  f2Res    = ( -5.604292107208924 ) + ( 1.6296672805766288 );
  e3Res    = ( -0.35623753204462505 ) + ( 0 );
  residualTrue[0] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 2
  eps11Res = ( 1.4358429804696238 ) + ( 0 );
  eps22Res = ( 0.4363651694822763 ) + ( 0 );
  eps12Res = ( 0.4242603492100673 ) + ( 0 );
  f1Res    = ( -9.01472101016328 ) + ( 1.21798709855141 );
  f2Res    = ( -12.412342722003247 ) + ( 1.2409220048518077 );
  e3Res    = ( -0.030669949372949184 ) + ( 0 );
  residualTrue[1] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 3
  eps11Res = ( 1.680865953030704 ) + ( 0 );
  eps22Res = ( 0.3135866067937304 ) + ( 0 );
  eps12Res = ( 0.133594520395779 ) + ( 0 );
  f1Res    = ( -12.663611510147241 ) + ( -0.2827935262467783 );
  f2Res    = ( -12.329236464598367 ) + ( -0.7775837360948342 );
  e3Res    = ( 0.49000025517676843 ) + ( 0 );
  residualTrue[2] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 4
  eps11Res = ( 1.315449097755295 ) + ( 0 );
  eps22Res = ( 0.50701938994913 ) + ( 0 );
  eps12Res = ( 0.4578178200307009 ) + ( 0 );
  f1Res    = ( -4.916025551189345 ) + ( -0.9937462821991424 );
  f2Res    = ( -10.423555572006808 ) + ( -2.696418700452672 );
  e3Res    = ( -0.7403576985700515 ) + ( 0 );
  residualTrue[3] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( residualTrue[0][n], SurrealValue(residual[0][n]), small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[1][n], SurrealValue(residual[1][n]), small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[2][n], SurrealValue(residual[2][n]), small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[3][n], SurrealValue(residual[3][n]), small_tol_res, close_tol_res );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
