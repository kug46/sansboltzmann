// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_PTC_AD_btest
// testing of residual integrands for Galerkin pseudo-time continuation (PTC): Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_PTC.h"

#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"

#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Field/Tuple/ElementTuple.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_PTC_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_1D_test )
{
    typedef PDEAdvectionDiffusion<PhysD1,
                                  AdvectiveFlux1D_Uniform,
                                  ViscousFlux1D_Uniform,
                                  Source1D_None> PDEAdvectionDiffusion1D;
    typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
    typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

    typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;

    typedef Element<Real,TopoD1,Line> CellTFieldClass;
    typedef Element<ArrayQ,TopoD1,Line> CellQFieldClass;
    typedef ElementSequence<ArrayQ,TopoD1,Line> CellQFieldSeqClass;

    typedef MakeTuple<ElementTuple, ElementXFieldClass, CellTFieldClass>::type ElementParam;

    typedef IntegrandCell_Galerkin_PTC<PDEClass> IntegrandClass;
    typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,ElementParam> BasisWeightedClass;

    typedef ElementXFieldClass::RefCoordType RefCoordType;

    Real u = 1.1;
    AdvectiveFlux1D_Uniform adv(u);

    Real kxx = 2.123;
    ViscousFlux1D_Uniform visc(kxx);

    Source1D_None source;

    GlobalTime time(0);

    PDEClass pde( time, adv, visc, source );

    // static tests
    BOOST_CHECK( pde.D == 1 );
    BOOST_CHECK( pde.N == 1 );

    // flux components
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == true );
    BOOST_CHECK( pde.hasSource() == false );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

    // grid

    int order = 1;
    ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( 1, xfldElem.order() );
    BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

    // line grid
    Real x1, x2;

    x1 = 0;
    x2 = 1;

    xfldElem.DOF(0) = {x1};
    xfldElem.DOF(1) = {x2};

    // inverse timestep field element
    CellTFieldClass dtifldElem(1, BasisFunctionCategory_Hierarchical);

    dtifldElem.DOF(0) = 1./13.;
    dtifldElem.DOF(1) = 1./12.;

    // solution at two timesteps

    CellQFieldClass qfldElem_t0(order, BasisFunctionCategory_Hierarchical); // current timestep

    BOOST_CHECK_EQUAL( 1, qfldElem_t0.order() );
    BOOST_CHECK_EQUAL( 2, qfldElem_t0.nDOF() );

    // line solution current
    qfldElem_t0.DOF(0) = 1;
    qfldElem_t0.DOF(1) = 4;

    // integrand
    IntegrandClass fcnint( pde, {0} );

    //////////////////////////////////
    // Case 1: du/dt = 0; Set previous timestep equal to current timestep
    //////////////////////////////////
    CellQFieldSeqClass qfldElemPastVec(qfldElem_t0.basis(), 1);

    qfldElemPastVec[0] = qfldElem_t0;

    // create the params tuple (only needed in unit tests)
    ElementParam params = (xfldElem, dtifldElem);

    // integrand functor
    BasisWeightedClass fcn = fcnint.integrand( params, qfldElem_t0, qfldElemPastVec);
    BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
    BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
    BOOST_CHECK( fcn.needsEvaluation() == true );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-12;
    RefCoordType sRef;
    Real integrandPDETrue[2];
    ArrayQ integrand[2];

    // x=0
    sRef = 0;
    fcn( sRef, integrand, 2 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );

    // x=1
    sRef = 1;
    fcn( sRef, integrand, 2 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );

    // x=0.5
    sRef = 1./2.;
    fcn( sRef, integrand, 2 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );


    // test the element integral of the functor

    int quadratureorder = 3;
    int nIntegrand = qfldElem_t0.nDOF();
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrand);
    Real rsd[2];

    ArrayQ rsdPDEElem[2] = {0,0};

    // cell integration for canonical element
    integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

    //PDE residual: (time)
    rsd[0] = 0;   // Basis function 1
    rsd[1] = 0;   // Basis function 2

    SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );

    //////////////////////////////////
    // Case 2: non-zero du/dt; Change solution at t=n-1;
    //////////////////////////////////
    qfldElemPastVec[0].DOF(0) = 2;
    qfldElemPastVec[0].DOF(1) = 1;

    // Test at {0}
    sRef = {0};
    fcn( sRef, integrand, 2 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = -1./13.;   // Basis function 1
    integrandPDETrue[1] = 0;   // Basis function 2

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );

    // Test at {1}
    sRef = {1};
    fcn( sRef, integrand, 2 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = 0;   // Basis function 1
    integrandPDETrue[1] = 1./4.;   // Basis function 2

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );

    // Test at {1/2}
    sRef = {1./2.};
    fcn( sRef, integrand, 2 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = 25./624.;   // Basis function 1
    integrandPDETrue[1] = 25./624.;   // Basis function 2

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );


    // test the element integral of the functor

    rsdPDEElem[0] = 0;
    rsdPDEElem[1] = 0;

    // cell integration for canonical element
    integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

    //PDE residual: (time)
    rsd[0] = 1./72.;   // Basis function 1
    rsd[1] = 8./117.;   // Basis function 2

    SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_2D_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
    typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
    typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

    typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;

    typedef Element<Real,TopoD2,Triangle> CellTFieldClass;
    typedef Element<ArrayQ,TopoD2,Triangle> CellQFieldClass;
    typedef ElementSequence<ArrayQ,TopoD2,Triangle> CellQFieldSeqClass;

    typedef MakeTuple<ElementTuple, ElementXFieldClass, CellTFieldClass>::type ElementParam;

    typedef IntegrandCell_Galerkin_PTC<PDEClass> IntegrandClass;
    typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,ElementParam> BasisWeightedClass;

    typedef ElementXFieldClass::RefCoordType RefCoordType;

    Real u = 1;
    Real v = 0.2;
    AdvectiveFlux2D_Uniform adv(u, v);

    Real kxx = 2.123;
    Real kxy = 0.553;
    Real kyy = 1.007;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    GlobalTime time(0);
    PDEClass pde(time, adv, visc, source);

    // static tests
    BOOST_CHECK( pde.D == 2 );
    BOOST_CHECK( pde.N == 1 );

    // flux components
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == true );
    BOOST_CHECK( pde.hasSource() == false );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

    // grid

    int order = 1;
    ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( 1, xfldElem.order() );
    BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

    // triangle grid
    Real x1, x2, x3, y1, y2, y3;

    x1 = 0;  y1 = 0;
    x2 = 1;  y2 = 0;
    x3 = 0;  y3 = 1;

    xfldElem.DOF(0) = {x1, y1};
    xfldElem.DOF(1) = {x2, y2};
    xfldElem.DOF(2) = {x3, y3};

    // inverse timestep field element
    CellTFieldClass dtifldElem(1, BasisFunctionCategory_Hierarchical);

    dtifldElem.DOF(0) = 1./13.;
    dtifldElem.DOF(1) = 1./12.;
    dtifldElem.DOF(2) = 1./14.;

    // solution

    CellQFieldClass qfldElem_t0(order, BasisFunctionCategory_Hierarchical); // current timestep

    BOOST_CHECK_EQUAL( 1, qfldElem_t0.order() );
    BOOST_CHECK_EQUAL( 3, qfldElem_t0.nDOF() );

    // triangle solution
    qfldElem_t0.DOF(0) = 1;
    qfldElem_t0.DOF(1) = 3;
    qfldElem_t0.DOF(2) = 4;

    // integrand
    IntegrandClass fcnint( pde, {0}  );

    //////////////////////////////////
    // Case 1: du/dt = 0; Set previous timestep equal to current timestep
    //////////////////////////////////
    CellQFieldSeqClass qfldElemPastVec(qfldElem_t0.basis(), 1);
    qfldElemPastVec[0] = qfldElem_t0;

    // create the params tuple (only needed in unit tests)
    ElementParam params = (xfldElem, dtifldElem);

    // integrand functor
    BasisWeightedClass fcn = fcnint.integrand(params, qfldElem_t0, qfldElemPastVec);

    BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
    BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
    BOOST_CHECK( fcn.needsEvaluation() == true );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-12;
    RefCoordType sRef;
    Real integrandPDETrue[3];
    ArrayQ integrand[3];

    sRef = {0,  0};
    fcn( sRef, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    sRef = {1,  0};
    fcn( sRef, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    sRef = {0,  1};
    fcn( sRef, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    sRef = {1./3.,  1./3.};
    fcn( sRef, integrand, 3 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );


    // test the element integral of the functor

    int quadratureorder = 3;
    int nIntegrand = qfldElem_t0.nDOF();
    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);
    Real rsd[3];

    ArrayQ rsdPDEElem[3] = {0,0,0};

    // cell integration for canonical element
    integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

    //PDE residual: (time)
    rsd[0] = 0;   // Basis function 1
    rsd[1] = 0;   // Basis function 2
    rsd[2] = 0;   // Basis function 3

    SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );


    //////////////////////////////////
    // Case 2: du/dt nonzero
    //////////////////////////////////
    qfldElemPastVec[0].DOF(0) = 2;
    qfldElemPastVec[0].DOF(1) = 1;
    qfldElemPastVec[0].DOF(2) = 3;

    // Test at {0, 0}
    sRef = {0, 0};
    fcn( sRef, integrand, 3 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = -1./13.;   // Basis function 1
    integrandPDETrue[1] = 0;   // Basis function 2
    integrandPDETrue[2] = 0;   // Basis function 3

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    // Test at {1, 0}
    sRef = {1, 0};
    fcn( sRef, integrand, 3 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = 0;   // Basis function 1
    integrandPDETrue[1] = 1./6.;   // Basis function 2
    integrandPDETrue[2] = 0;   // Basis function 3

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    // Test at {0, 1}
    sRef = {0, 1};
    fcn( sRef, integrand, 3 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = 0;   // Basis function 1
    integrandPDETrue[1] = 0;   // Basis function 2
    integrandPDETrue[2] = 1./14.;   // Basis function 3

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );

    // Test at {1/3, 1/3}
    sRef = {1./3., 1./3.};
    fcn( sRef, integrand, 3 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = 253./14742.;   // Basis function 1
    integrandPDETrue[1] = 253./14742.;   // Basis function 2
    integrandPDETrue[2] = 253./14742.;   // Basis function 3

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );


    // test the element integral of the functor

    rsdPDEElem[0] = 0;
    rsdPDEElem[1] = 0;
    rsdPDEElem[2] = 0;

    // cell integration for canonical element
    integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

    //PDE residual: (time)
    rsd[0] = 11./3360.;   // Basis function 1
    rsd[1] = 289./21840.;   // Basis function 2
    rsd[2] = 1247./131040.;   // Basis function 3

    SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_2D_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
    typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
    typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

    typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldClass;

    typedef Element<Real,TopoD2,Quad> CellTFieldClass;
    typedef Element<ArrayQ,TopoD2,Quad> CellQFieldClass;
    typedef ElementSequence<ArrayQ,TopoD2,Quad> CellQFieldSeqClass;

    typedef MakeTuple<ElementTuple, ElementXFieldClass, CellTFieldClass>::type ElementParam;

    typedef IntegrandCell_Galerkin_PTC<PDEClass> IntegrandClass;
    typedef IntegrandClass::BasisWeighted<Real,TopoD2,Quad,ElementParam> BasisWeightedClass;

    typedef ElementXFieldClass::RefCoordType RefCoordType;

    Real u = 1;
    Real v = 0.2;
    AdvectiveFlux2D_Uniform adv(u, v);

    Real kxx = 2.123;
    Real kxy = 0.553;
    Real kyy = 1.007;
    ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

    Source2D_None source;

    GlobalTime time(0);
    PDEClass pde( time, adv, visc, source );

    // static tests
    BOOST_CHECK( pde.D == 2 );
    BOOST_CHECK( pde.N == 1 );

    // flux components
    BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
    BOOST_CHECK( pde.hasFluxAdvective() == true );
    BOOST_CHECK( pde.hasFluxViscous() == true );
    BOOST_CHECK( pde.hasSource() == false );
    BOOST_CHECK( pde.hasForcingFunction() == false );

    BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

    // grid

    int order = 1;
    ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( 1, xfldElem.order() );
    BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

    // quad grid that matches reference element
    xfldElem.DOF(0) = {0, 0};
    xfldElem.DOF(1) = {1, 0};
    xfldElem.DOF(2) = {1, 1};
    xfldElem.DOF(3) = {0, 1};

    // inverse timestep field element
    CellTFieldClass dtifldElem(1, BasisFunctionCategory_Hierarchical);

    dtifldElem.DOF(0) = 1./13.;
    dtifldElem.DOF(1) = 1./12.;
    dtifldElem.DOF(2) = 1./10.;
    dtifldElem.DOF(3) = 1./14.;

    // solution

    CellQFieldClass qfldElem_t0(order, BasisFunctionCategory_Hierarchical); // current timestep

    BOOST_CHECK_EQUAL( 1, qfldElem_t0.order() );
    BOOST_CHECK_EQUAL( 4, qfldElem_t0.nDOF() );

    // triangle solution
    qfldElem_t0.DOF(0) = 1;
    qfldElem_t0.DOF(1) = 3;
    qfldElem_t0.DOF(2) = 5;
    qfldElem_t0.DOF(3) = 4;

    // integrand
    IntegrandClass fcnint( pde, {0}  );

    //////////////////////////////////
    // Case 1: du/dt = 0; Set previous timestep equal to current timestep
    //////////////////////////////////
    CellQFieldSeqClass qfldElemPastVec(qfldElem_t0.basis(), 1);
    qfldElemPastVec[0] = qfldElem_t0;

    // create the params tuple (only needed in unit tests)
    ElementParam params = (xfldElem, dtifldElem);

    // integrand functor
    BasisWeightedClass fcn = fcnint.integrand(params, qfldElem_t0, qfldElemPastVec);

    BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
    BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
    BOOST_CHECK( fcn.needsEvaluation() == true );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-12;
    RefCoordType sRef;
    Real integrandPDETrue[4];
    ArrayQ integrand[4];

    sRef = {0,  0};
    fcn( sRef, integrand, 4 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;
    integrandPDETrue[3] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[3], integrand[3], small_tol, close_tol );

    sRef = {1,  0};
    fcn( sRef, integrand, 4 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;
    integrandPDETrue[3] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[3], integrand[3], small_tol, close_tol );

    sRef = {0,  1};
    fcn( sRef, integrand, 4 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;
    integrandPDETrue[3] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[3], integrand[3], small_tol, close_tol );

    sRef = {1./2.,  1./2.};
    fcn( sRef, integrand, 4 );

    integrandPDETrue[0] = 0;
    integrandPDETrue[1] = 0;
    integrandPDETrue[2] = 0;
    integrandPDETrue[3] = 0;

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[3], integrand[3], small_tol, close_tol );

    // test the element integral of the functor

    int quadratureorder = 3;
    int nIntegrand = qfldElem_t0.nDOF();
    GalerkinWeightedIntegral<TopoD2, Quad, ArrayQ> integral(quadratureorder, nIntegrand);
    Real rsd[4];

    ArrayQ rsdPDEElem[4] = {0,0,0,0};

    // cell integration for canonical element
    integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

    //PDE residual: (time)
    rsd[0] = 0;   // Basis function 1
    rsd[1] = 0;   // Basis function 2
    rsd[2] = 0;   // Basis function 3
    rsd[3] = 0;   // Basis function 4

    SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[3], rsdPDEElem[3], small_tol, close_tol );


    //////////////////////////////////
    // Case 2: du/dt nonzero
    //////////////////////////////////
    qfldElemPastVec[0].DOF(0) = 2;
    qfldElemPastVec[0].DOF(1) = 1;
    qfldElemPastVec[0].DOF(2) = 4;
    qfldElemPastVec[0].DOF(3) = 3;

    // Test at {0, 0}
    sRef = {0, 0};
    fcn( sRef, integrand, 4 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = -1./13.;   // Basis function 1
    integrandPDETrue[1] = 0;   // Basis function 2
    integrandPDETrue[2] = 0;   // Basis function 3
    integrandPDETrue[3] = 0;   // Basis function 4

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[3], integrand[3], small_tol, close_tol );

    // Test at {1, 0}
    sRef = {1, 0};
    fcn( sRef, integrand, 4 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = 0;   // Basis function 1
    integrandPDETrue[1] = 1./6.;   // Basis function 2
    integrandPDETrue[2] = 0;   // Basis function 3
    integrandPDETrue[3] = 0;   // Basis function 4

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[3], integrand[3], small_tol, close_tol );

    // Test at {1, 1}
    sRef = {1, 1};
    fcn( sRef, integrand, 4 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = 0;   // Basis function 1
    integrandPDETrue[1] = 0;   // Basis function 2
    integrandPDETrue[2] = 1./10.;   // Basis function 3
    integrandPDETrue[3] = 0;   // Basis function 4

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[3], integrand[3], small_tol, close_tol );

    // Test at {0, 1}
    sRef = {0, 1};
    fcn( sRef, integrand, 4 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = 0;   // Basis function 1
    integrandPDETrue[1] = 0;   // Basis function 2
    integrandPDETrue[2] = 0;   // Basis function 3
    integrandPDETrue[3] = 1./14.;   // Basis function 4

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[3], integrand[3], small_tol, close_tol );

    // Test at {1/2, 1/2}
    sRef = {1./2., 1./2.};
    fcn( sRef, integrand, 4 );

    //PDE residual integrands: (time)
    integrandPDETrue[0] = 1811./116480.;   // Basis function 1
    integrandPDETrue[1] = 1811./116480.;   // Basis function 2
    integrandPDETrue[2] = 1811./116480.;   // Basis function 3
    integrandPDETrue[3] = 1811./116480.;   // Basis function 4

    SANS_CHECK_CLOSE( integrandPDETrue[0], integrand[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[1], integrand[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[2], integrand[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandPDETrue[3], integrand[3], small_tol, close_tol );


    // test the element integral of the functor

    rsdPDEElem[0] = 0;
    rsdPDEElem[1] = 0;
    rsdPDEElem[2] = 0;
    rsdPDEElem[3] = 0;

    // cell integration for canonical element
    integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

    //PDE residual: (time)
    rsd[0] = 5503./786240.;   // Basis function 1
    rsd[1] = 16829./786240.;   // Basis function 2
    rsd[2] = 827./37440.;   // Basis function 3
    rsd[3] = 1193./87360.;   // Basis function 4

    SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd[3], rsdPDEElem[3], small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
