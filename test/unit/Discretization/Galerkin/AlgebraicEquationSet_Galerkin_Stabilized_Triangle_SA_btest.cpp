// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_Galerkin_Stabilized_Triangle_AD_btest
// testing AlgebraicEquationSet_Galerkin_Stabilized

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <string>
#include <fstream>

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"

#include "Field/DistanceFunction/DistanceFunction.h"
#include "Field/ProjectSoln/ProjectSolnCell_Lagrange.h"
#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#ifdef SANS_PETSC
#include "LinearAlgebra/SparseLinAlg/PETSc/PETScSolver.h"
#endif

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Galerkin_Stabilized_Triangle_SA_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Serial_Parallel_Equivalency )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCRANSSA2DVector< TraitsSizeRANSSA, TraitsModelRANSSAClass > BCVector;

  typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                   AlgEqSetTraits_Dense, ParamFieldTupleType> PrimalEquationSet;
  typedef PrimalEquationSet::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  // Size of the state vector
  const int N = ArrayQ::M;

  typedef PrimalEquationSet::SystemMatrix DenseSystemMatrixClass;
  typedef PrimalEquationSet::SystemVector DenseSystemVectorClass;
  typedef PrimalEquationSet::SystemNonZeroPattern DenseSystemNonZeroPattern;

  const Real small_tol = 5e-10;
  const Real close_tol = 1e-9;
  const Real rsd_close_tol = 1e-4;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm_global = world.split(world.rank());

  // PDE

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.2;
  const Real Reynolds = 1e4;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale

  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // BC
  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aSpec = atan(vRef/uRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;
  BCIn["nt"] = ntRef; // TODO: not happy about this

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {XField2D_Box_Triangle_Lagrange_X1::iTop};
  BCBoundaryGroups["BCNoSlip"] = {XField2D_Box_Triangle_Lagrange_X1::iBottom};
  BCBoundaryGroups["BCOut"] = {XField2D_Box_Triangle_Lagrange_X1::iRight};
  BCBoundaryGroups["BCIn"] = {XField2D_Box_Triangle_Lagrange_X1::iLeft};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> mitLG_bcgroups = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  SolnNDConvertSpace<PhysD2, SolutionFunction_RANSSA2D_Wake<TraitsSizeRANSSA, TraitsModelRANSSAClass>>
    solnWake( gas, 0.9, rhoRef, uRef, pRef, ntRef );

  int ii = 4;
  int jj = 4;

  // grid
  XField2D_Box_Triangle_Lagrange_X1 xfld_global( comm_global, ii, jj ); // complete system on all processors

  // distance function
  Field_CG_Cell<PhysD2, TopoD2, Real> distfld_global(xfld_global, 1, BasisFunctionCategory_Lagrange);
  DistanceFunction(distfld_global, BCBoundaryGroups.at("BCNoSlip"), false);


  for (int order = 1; order <= BasisFunctionArea_Triangle_LagrangePMax; order++)
  {
    // solution
    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld_global(xfld_global, order, BasisFunctionCategory_Lagrange);

    // This avoids jacobians about 0 == fabs(uy - vx)
    for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Lagrange(solnWake, {0}), (xfld_global, qfld_global) );

    // Lagrange multiplier
    Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_global( xfld_global, order, BasisFunctionCategory_Lagrange, mitLG_bcgroups );
    lgfld_global = pde.setDOFFrom( SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntRef}) );


    QuadratureOrder quadratureOrder( xfld_global, 2*order + 1 );
    std::vector<Real> tol = {1e-12, 1e-12};
    const StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby, order);

    ParamFieldTupleType paramfld_global = (distfld_global, xfld_global);

    PrimalEquationSet PrimalEqSet_global(paramfld_global, qfld_global, lgfld_global, pde, stab, quadratureOrder,
                                         ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups);

    // residual

    DenseSystemVectorClass q_global(PrimalEqSet_global.vectorStateSize());

    DenseSystemVectorClass rsd_global(PrimalEqSet_global.vectorEqSize());

    PrimalEqSet_global.fillSystemVector(q_global);

    rsd_global = 0;
    PrimalEqSet_global.residual(q_global, rsd_global);

    // residual norm

    std::vector<std::vector<Real>> rsdNrm_global( PrimalEqSet_global.residualNorm(rsd_global) );

    // jacobian nonzero pattern

    DenseSystemNonZeroPattern nz_global(PrimalEqSet_global.matrixSize());

    PrimalEqSet_global.jacobian(q_global, nz_global);

    // jacobian

    DenseSystemMatrixClass jac_global(nz_global);

    jac_global = 0;
    PrimalEqSet_global.jacobian(q_global, jac_global);

    // range of splits
    for (int comm_size = 1; comm_size <= world.size(); comm_size++)
    {
      int color = world.rank() < comm_size ? 0 : 1;
      mpi::communicator comm_local = world.split(color);

      if (color == 1) continue;

      // grid
      XField2D_Box_Triangle_Lagrange_X1 xfld_local ( comm_local , ii, jj ); // partitioned system

      // distance function
      Field_CG_Cell<PhysD2, TopoD2, Real> distfld_local (xfld_local , 1, BasisFunctionCategory_Lagrange);
      DistanceFunction(distfld_local, BCBoundaryGroups.at("BCNoSlip"), false);

      // solution
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld_local(xfld_local , order, BasisFunctionCategory_Lagrange);

      // This avoids jacobians about 0 == fabs(uy - vx)
      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Lagrange(solnWake, {0}), (xfld_local, qfld_local) );

      // Lagrange multiplier
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_local( xfld_local, order, BasisFunctionCategory_Lagrange, mitLG_bcgroups );
      lgfld_local = pde.setDOFFrom( SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntRef}) );

      ParamFieldTupleType paramfld_local  = (distfld_local , xfld_local );

      PrimalEquationSet PrimalEqSet_local (paramfld_local, qfld_local, lgfld_local, pde, stab, quadratureOrder,
                                           ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups);

      DenseSystemVectorClass q_local(PrimalEqSet_local.vectorStateSize());

      DenseSystemVectorClass rsd_local(PrimalEqSet_local.vectorEqSize());

      PrimalEqSet_local.fillSystemVector(q_local);

      rsd_local = 0;
      PrimalEqSet_local.residual(q_local, rsd_local);

      BOOST_REQUIRE_EQUAL( rsd_local[0].m(), qfld_local.nDOFpossessed() );

      BOOST_REQUIRE_EQUAL(rsd_global.m(), 2);
      BOOST_REQUIRE_EQUAL(rsd_local.m(), 2);

      for ( int i = 0; i < rsd_local[0].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          SANS_CHECK_CLOSE( rsd_global[0][qfld_local.local2nativeDOFmap(i)][ib], rsd_local[0][i][ib], small_tol, rsd_close_tol );

      for ( int i = 0; i < rsd_local[1].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          SANS_CHECK_CLOSE( rsd_global[1][lgfld_local.local2nativeDOFmap(i)][ib], rsd_local[1][i][ib], small_tol, rsd_close_tol );

      // residual norm

      std::vector<std::vector<Real>> rsdNrm_local( PrimalEqSet_local.residualNorm(rsd_local) );

      BOOST_REQUIRE_EQUAL( rsdNrm_global.size(), rsdNrm_local.size() );

      for ( std::size_t i = 0; i < rsdNrm_global.size(); i++ )
      {
        BOOST_REQUIRE_EQUAL( rsdNrm_global[i].size(), rsdNrm_local[i].size() );
        for ( std::size_t j = 0; j < rsdNrm_global[i].size(); j++ )
          SANS_CHECK_CLOSE( rsdNrm_global[i][j], rsdNrm_local[i][j], small_tol, rsd_close_tol );
      }

      // jacobian nonzero pattern

      DenseSystemNonZeroPattern nz_local(PrimalEqSet_local.matrixSize());

      PrimalEqSet_local.jacobian(q_local, nz_local);

      // jacobian

      DenseSystemMatrixClass jac_local(nz_local);

      jac_local = 0;
      PrimalEqSet_local.jacobian(q_local, jac_local);

      BOOST_REQUIRE_EQUAL(jac_global.m(), 2);
      BOOST_REQUIRE_EQUAL(jac_global.n(), 2);

      BOOST_REQUIRE_EQUAL(jac_local.m(), 2);
      BOOST_REQUIRE_EQUAL(jac_local.n(), 2);

#if __clang_analyzer__
      return; // clang thinks we are accessing zero size memory below... sigh...
#endif

      // jacobian nonzero pattern
      //
      //           q  lg
      //   PDE     X   X
      //   BC      X   0

      const DenseSystemMatrixClass::node_type& PDE_q_global = jac_global(0,0);
      const DenseSystemMatrixClass::node_type& PDE_q_local = jac_local(0,0);

      for ( int i = 0; i < PDE_q_local.m(); i++ )
        for ( int j = 0; j < PDE_q_local.n(); j++ )
        {
          int in = qfld_local.local2nativeDOFmap(i);
          int jn = qfld_local.local2nativeDOFmap(j);
          for ( int ib = 0; ib < N; ib++ )
            for ( int jb = 0; jb < N; jb++ )
              SANS_CHECK_CLOSE( PDE_q_global(in,jn)(ib,jb), PDE_q_local(i,j)(ib,jb), small_tol, close_tol );
        }

      const DenseSystemMatrixClass::node_type& PDE_lg_global = jac_global(0,1);
      const DenseSystemMatrixClass::node_type& PDE_lg_local = jac_local(0,1);

      for ( int i = 0; i < PDE_lg_local.m(); i++ )
        for ( int j = 0; j < PDE_lg_local.n(); j++ )
        {
          int in = qfld_local.local2nativeDOFmap(i);
          int jn = lgfld_local.local2nativeDOFmap(j);
          for ( int ib = 0; ib < N; ib++ )
            for ( int jb = 0; jb < N; jb++ )
              SANS_CHECK_CLOSE( PDE_lg_global(in,jn)(ib,jb), PDE_lg_local(i,j)(ib,jb), small_tol, close_tol );
        }


      const DenseSystemMatrixClass::node_type& BC_q_global = jac_global(1,0);
      const DenseSystemMatrixClass::node_type& BC_q_local = jac_local(1,0);

      for ( int i = 0; i < BC_q_local.m(); i++ )
        for ( int j = 0; j < BC_q_local.n(); j++ )
        {
          int in = lgfld_local.local2nativeDOFmap(i);
          int jn = qfld_local.local2nativeDOFmap(j);
          for ( int ib = 0; ib < N; ib++ )
            for ( int jb = 0; jb < N; jb++ )
              SANS_CHECK_CLOSE( BC_q_global(in,jn)(ib,jb), BC_q_local(i,j)(ib,jb), small_tol, close_tol );
        }

      const DenseSystemMatrixClass::node_type& BC_lg_global = jac_global(1,1);
      const DenseSystemMatrixClass::node_type& BC_lg_local = jac_local(1,1);

      for ( int i = 0; i < BC_lg_local.m(); i++ )
        for ( int j = 0; j < BC_lg_local.n(); j++ )
        {
          int in = lgfld_local.local2nativeDOFmap(i);
          int jn = lgfld_local.local2nativeDOFmap(j);
          for ( int ib = 0; ib < N; ib++ )
            for ( int jb = 0; jb < N; jb++ )
              SANS_CHECK_CLOSE( BC_lg_global(in,jn)(ib,jb), BC_lg_local(i,j)(ib,jb), small_tol, close_tol );
        }

    } //order
  }
}


#ifdef SANS_PETSC
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Serial_Parallel_Solve_Equivalency )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCRANSSA2DVector< TraitsSizeRANSSA, TraitsModelRANSSAClass > BCVector;

  typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                   AlgEqSetTraits_Sparse, ParamFieldTupleType> PrimalEquationSet;
  typedef PrimalEquationSet::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  // Size of the state vector
  const int N = ArrayQ::M;

  typedef PrimalEquationSet::SystemMatrix SparseSystemMatrixClass;
  typedef PrimalEquationSet::SystemVector SparseSystemVectorClass;

  const Real small_tol = 1e-10;
  const Real close_tol = 5e-3;
#ifdef __INTEL_COMPILER
  const Real rsd_close_tol = 1e-3;
#else
  const Real rsd_close_tol = 1e-4;
#endif
  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm_global = world.split(world.rank());

  // PDE

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.2;
  const Real Reynolds = 1e4;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale

  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // BC
  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aSpec = atan(vRef/uRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;
  BCIn["nt"] = ntRef; // TODO: not happy about this

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {XField2D_Box_Triangle_Lagrange_X1::iTop};
  BCBoundaryGroups["BCNoSlip"] = {XField2D_Box_Triangle_Lagrange_X1::iBottom};
  BCBoundaryGroups["BCOut"] = {XField2D_Box_Triangle_Lagrange_X1::iRight};
  BCBoundaryGroups["BCIn"] = {XField2D_Box_Triangle_Lagrange_X1::iLeft};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> mitLG_bcgroups = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  SolnNDConvertSpace<PhysD2, SolutionFunction_RANSSA2D_Wake<TraitsSizeRANSSA, TraitsModelRANSSAClass>>
    solnWake( gas, 0.9, rhoRef, uRef, pRef, ntRef );

  // compute the global jacobian

  // grid

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_Lagrange_X1 xfld_global( comm_global, ii, jj ); // complete system on all processors

  // distance function
  Field_CG_Cell<PhysD2, TopoD2, Real> distfld_global(xfld_global, 1, BasisFunctionCategory_Lagrange);
  DistanceFunction(distfld_global, BCBoundaryGroups.at("BCNoSlip"), false);

  for (int order = 1; order <= 3; order++)
  {
    // solution:
    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld_global(xfld_global, order, BasisFunctionCategory_Lagrange);

    // This avoids jacobians about 0 == fabs(uy - vx)
    for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Lagrange(solnWake, {0}), (xfld_global, qfld_global) );

    // Lagrange multiplier
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_global( xfld_global, order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
    lgfld_global = 0;

    ParamFieldTupleType paramfld_global = (distfld_global, xfld_global);

    QuadratureOrder quadratureOrder( xfld_global, 2*order + 1 );
    std::vector<Real> tol = {1e-12, 1e-12};
    const StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby, order);

    PrimalEquationSet PrimalEqSet_global(paramfld_global, qfld_global, lgfld_global, pde, stab, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

    // residual

    SparseSystemVectorClass q_global(PrimalEqSet_global.vectorStateSize());

    SparseSystemVectorClass rsd_global(PrimalEqSet_global.vectorEqSize());

    SparseSystemVectorClass dq_global(PrimalEqSet_global.vectorStateSize());

    PrimalEqSet_global.fillSystemVector(q_global);

    rsd_global = 0;
    PrimalEqSet_global.residual(q_global, rsd_global);

    PyDict PreconditionerLU;
    PreconditionerLU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
    PreconditionerLU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;

    PyDict PreconditionerDict;
    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerLU;

    PyDict PETScDict;
    PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
    PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-9;
    PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;

    SLA::PETScSolver<SparseSystemMatrixClass> Solver_global(PETScDict, PrimalEqSet_global);

    //Solve the linear system.
    dq_global = 0;
    Solver_global.solve(rsd_global, dq_global);

    for (int comm_size = 1; comm_size <= world.size(); comm_size++)
    {
      int color = world.rank() < comm_size ? 0 : 1;
      mpi::communicator comm_local = world.split(color);

      if (color == 1) continue;

      // grid
      XField2D_Box_Triangle_Lagrange_X1 xfld_local( comm_local , ii, jj ); // partitioned system

      // distance function
      Field_CG_Cell<PhysD2, TopoD2, Real> distfld_local(xfld_local , 1, BasisFunctionCategory_Lagrange);
      DistanceFunction(distfld_local , BCBoundaryGroups.at("BCNoSlip"), false);

      // solution
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld_local(xfld_local , order, BasisFunctionCategory_Lagrange);

      // This avoids jacobians about 0 == fabs(uy - vx)
      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Lagrange(solnWake, {0}), (xfld_local, qfld_local ) );

      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_local ( xfld_local, order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
      lgfld_local  = 0;

      ParamFieldTupleType paramfld_local  = (distfld_local , xfld_local );

      PrimalEquationSet PrimalEqSet_local (paramfld_local, qfld_local, lgfld_local, pde, stab, quadratureOrder,
                                           ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

      SparseSystemVectorClass q_local (PrimalEqSet_local.vectorStateSize());

      SparseSystemVectorClass rsd_local (PrimalEqSet_local.vectorEqSize());

      SparseSystemVectorClass dq_local (PrimalEqSet_local.vectorStateSize());

      PrimalEqSet_local.fillSystemVector(q_local);

      rsd_local = 0;
      PrimalEqSet_local.residual(q_local, rsd_local);

      BOOST_REQUIRE_EQUAL( rsd_local[0].m(), qfld_local.nDOFpossessed() );

      BOOST_REQUIRE_EQUAL(rsd_global.m(), 2);
      BOOST_REQUIRE_EQUAL(rsd_local.m(), 2);

      for ( int i = 0; i < rsd_local[0].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          SANS_CHECK_CLOSE( rsd_global[0][qfld_local.local2nativeDOFmap(i)][ib], rsd_local[0][i][ib], small_tol, rsd_close_tol );

      for ( int i = 0; i < rsd_local[1].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          SANS_CHECK_CLOSE( rsd_global[1][lgfld_local.local2nativeDOFmap(i)][ib], rsd_local[1][i][ib], small_tol, rsd_close_tol );

      SLA::PETScSolver<SparseSystemMatrixClass> Solver_local(PETScDict, PrimalEqSet_local);

      //Solve the linear system.
      dq_local = 0;
      Solver_local.solve(rsd_local, dq_local);

      BOOST_REQUIRE_EQUAL(dq_global.m(), 2);
      BOOST_REQUIRE_EQUAL(dq_local.m(), 2);

      for ( int i = 0; i < dq_local[0].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          SANS_CHECK_CLOSE( dq_global[0][qfld_local.local2nativeDOFmap(i)][ib], dq_local[0][i][ib], small_tol, close_tol );

      for ( int i = 0; i < dq_local[1].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          SANS_CHECK_CLOSE( dq_global[1][lgfld_local.local2nativeDOFmap(i)][ib], dq_local[1][i][ib], small_tol, close_tol );
    }
  }
}
#endif //SANS_PETSC

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
