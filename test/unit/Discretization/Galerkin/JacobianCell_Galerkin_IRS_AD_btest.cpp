// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_Galerkin_IRS_AD_btest
// testing of Galerkin pseudo-time continuation cell-integral jacobian: advection-diffusions

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
//#include "Field/FieldVolume_DG_Cell.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_IRS.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin_BDF.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin_BDF.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace           // local definition
{

//----------------------------------------------------------------------------//
// jacobian dumps
template <int N>
void
dump( Real jac[][N], int m, int n )
{
  cout << "{";
  for (int i = 0; i < m; i++)
  {
    cout << "{";
    for (int j = 0; j < n; j++)
    {
      cout << jac[i][j];
      if (j < n-1) cout << ", ";
    }
    cout << "}";
    if (i < m-1) cout << ", ";
  }
  cout << "}" << std::endl;
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_Galerkin_IRS_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_P1_1D_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::MatrixQ<Real> MatrixQ;
  typedef Field_DG_Cell<PhysD1, TopoD1, Real> TField_DG_CellType;
  typedef Field_DG_Cell<PhysD1, TopoD1, Real> HField_DG_CellType;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QFieldSequenceType;

  typedef IntegrandCell_Galerkin_IRS<PDEClass> IntegrandClass;

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  GlobalTime time(0);

  Source1D_None source;

  PDEClass pde(time, adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: line
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // inverse timestep field
  TField_DG_CellType dtifld(xfld, 1, BasisFunctionCategory_Hierarchical);

  dtifld.DOF(0) = 1./13.;
  dtifld.DOF(1) = 1./12.;

  // element size field
  HField_DG_CellType hfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  hfld.DOF(0) = 1.;
  hfld.DOF(1) = 1.;

  // solution: single line, P1 (aka Q1)
  int qorder = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  // solution data
  qfld.DOF(0) = 1;

  QFieldSequenceType qfldspast(1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Legendre);

  // previous solution data
  qfldspast[0].DOF(0) = 2;

  // quadrature rule (quadratic: basis is linear, solution is const, time step is linear)
  int quadratureOrder = 2;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(1), rsdPDEGlobal1(1);
  Real jacPDE_q[1][1];

  rsdPDEGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal0),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );


  qfld.DOF(0) += 1;
  rsdPDEGlobal1 = 0;
  //rsdAuGlobal1 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal1),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

  jacPDE_q[0][0] = rsdPDEGlobal1[0] - rsdPDEGlobal0[0];

  qfld.DOF(0) -= 1;

#if 0
  std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,2);
#endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(1,1);

  mtxPDEGlob_q = 0;
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_Galerkin_BDF(fcnint, mtxPDEGlob_q),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  SANS_CHECK_CLOSE( jacPDE_q[0][0], mtxPDEGlob_q(0,0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_P2_1D_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::MatrixQ<Real> MatrixQ;
  typedef Field_DG_Cell<PhysD1, TopoD1, Real> TField_DG_CellType;
  typedef Field_DG_Cell<PhysD1, TopoD1, Real> HField_DG_CellType;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QFieldSequenceType;

  typedef IntegrandCell_Galerkin_IRS<PDEClass> IntegrandClass;

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  GlobalTime time(0);

  Source1D_None source;

  PDEClass pde( time, adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // inverse timestep field
  TField_DG_CellType dtifld(xfld, 1, BasisFunctionCategory_Hierarchical);

  dtifld.DOF(0) = 1./13.;
  dtifld.DOF(1) = 1./12.;

  // element size field
  HField_DG_CellType hfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  hfld.DOF(0) = 1.;
  hfld.DOF(1) = 1.;

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;


  QFieldSequenceType qfldspast(1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Legendre);

  qfldspast[0] = 0;

  // quadrature rule (cubic: basis is linear, solution is linea, time step is linear)
  int quadratureOrder = 3;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(2), rsdPDEGlobal1(2);
  Real jacPDE_q[2][2];

  rsdPDEGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal0),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

  for (int j = 0; j < 2; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal1),
                                            (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < 2; i++)
    {
      jacPDE_q[i][j]   = rsdPDEGlobal1[i]  - rsdPDEGlobal0[i];
    }
  }

#if 0
  std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,3,6);
#endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(2,2);

  mtxPDEGlob_q = 0;

  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_Galerkin_BDF(fcnint, mtxPDEGlob_q),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

#if 0
  std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-12;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_P1_2D_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::MatrixQ<Real> MatrixQ;
  typedef Field_DG_Cell<PhysD2, TopoD2, Real> TField_DG_CellType;
  typedef Field_DG_Cell<PhysD2, TopoD2, Real> HField_DG_CellType;
  typedef FieldSequence<PhysD2, TopoD2, ArrayQ> QFieldSequenceType;

  typedef IntegrandCell_Galerkin_IRS<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  GlobalTime time(0);
  PDEClass pde( time, adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // inverse timestep field
  TField_DG_CellType dtifld(xfld, 1, BasisFunctionCategory_Hierarchical);

  dtifld.DOF(0) = 1./13.;
  dtifld.DOF(1) = 1./12.;
  dtifld.DOF(2) = 1./14.;

  // element size field
  HField_DG_CellType hfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  hfld.DOF(0) = 1.;
  hfld.DOF(1) = 1.;
  hfld.DOF(2) = 1.;

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;


  QFieldSequenceType qfldspast(1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Legendre);

  // previous solution data
  qfldspast[0].DOF(0) = 2;
  qfldspast[0].DOF(1) = 1;
  qfldspast[0].DOF(2) = 3;

  // quadrature rule (cubic: basis is linear, solution is linear, time step is linear)
  int quadratureOrder = 3;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // jacobian via FD w/ residual operator; assumes scalar PDE
  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(3), rsdPDEGlobal1(3);
  Real jacPDE_q[3][3];

  rsdPDEGlobal0 = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal0),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

  for (int j = 0; j < 3; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal1),
                                            (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < 3; i++)
      jacPDE_q[i][j]     = rsdPDEGlobal1[i]    - rsdPDEGlobal0[i];
  }

#if 0
  std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,3,3);
#endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(3,3);
  mtxPDEGlob_q = 0;

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin_BDF(fcnint, mtxPDEGlob_q),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

#if 0
  std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-11;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );
    }

}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_P1_2D_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::MatrixQ<Real> MatrixQ;
  typedef Field_DG_Cell<PhysD2, TopoD2, Real> TField_DG_CellType;
  typedef Field_DG_Cell<PhysD2, TopoD2, Real> HField_DG_CellType;
  typedef FieldSequence<PhysD2, TopoD2, ArrayQ> QFieldSequenceType;

  typedef IntegrandCell_Galerkin_IRS<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  GlobalTime time(0);
  PDEClass pde( time, adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // grid: single quad, P1 (aka X1)
  XField2D_1Quad_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // inverse timestep field
  TField_DG_CellType dtifld(xfld, 1, BasisFunctionCategory_Hierarchical);

  dtifld.DOF(0) = 1./13.;
  dtifld.DOF(1) = 1./12.;
  dtifld.DOF(2) = 1./10.;
  dtifld.DOF(3) = 1./14.;

  // element size field
  HField_DG_CellType hfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  hfld.DOF(0) = 1.;
  hfld.DOF(1) = 1.;
  hfld.DOF(2) = 1.;
  hfld.DOF(3) = 1.;

  // solution: single quad, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 5;
  qfld.DOF(3) = 4;

  QFieldSequenceType qfldspast(1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Hierarchical);

  // previous solution data
  qfldspast[0].DOF(0) = 2;
  qfldspast[0].DOF(1) = 1;
  qfldspast[0].DOF(2) = 4;
  qfldspast[0].DOF(3) = 3;

  // quadrature rule (cubic: basis is linear, solution is linear, time step is linear)
  int quadratureOrder = 3;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // jacobian via FD w/ residual operator; assumes scalar PDE
  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(4), rsdPDEGlobal1(4);
  Real jacPDE_q[4][4];

  rsdPDEGlobal0 = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal0),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

  for (int j = 0; j < 4; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal1),
                                            (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < 4; i++)
      jacPDE_q[i][j]     = rsdPDEGlobal1[i]    - rsdPDEGlobal0[i];
  }

#if 0
  std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,4,4);
#endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(4,4);
  mtxPDEGlob_q = 0;

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin_BDF(fcnint, mtxPDEGlob_q),
                                          (xfld, hfld, dtifld), (qfld, qfldspast), &quadratureOrder, 1 );

#if 0
  std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
