// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_AD_btest
// testing of cell element residual integrands for Galerkin: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h" // Basis Weighted
#include "Field/Element/ElementIntegral.h" // Field Weighted

#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDE1DClassPlusTime;
template class IntegrandCell_Galerkin< PDE1DClassPlusTime >::BasisWeighted<Real, TopoD1, Line, ElementXField<PhysD1,TopoD1, Line> >;
//template class IntegrandCell_Galerkin< PDE1DClassPlusTime >::FieldWeighted<Real, TopoD1, Line, ElementXField<PhysD1,TopoD1, Line> >;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDE2DClassPlusTime;
template class IntegrandCell_Galerkin< PDE2DClassPlusTime >::BasisWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2,TopoD2, Triangle> >;
//template class IntegrandCell_Galerkin< PDE2DClassPlusTime >::FieldWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2,TopoD2, Triangle> >;

template class IntegrandCell_Galerkin< PDE2DClassPlusTime >::BasisWeighted<Real, TopoD2, Quad, ElementXField<PhysD2,TopoD2, Quad> >;

typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_None> PDEAdvectionDiffusion3D;
typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDE3DClassPlusTime;
template class IntegrandCell_Galerkin< PDE3DClassPlusTime >::BasisWeighted<Real, TopoD3, Tet, ElementXField<PhysD3,TopoD3, Tet> >;

template class IntegrandCell_Galerkin< PDE3DClassPlusTime >::BasisWeighted<Real, TopoD3, Hex, ElementXField<PhysD3,TopoD3, Hex> >;
}

using namespace SANS;



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line, ElementXField<PhysD1,TopoD1, Line> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandTrue[2];
  ArrayQ integrand[2];

  // Test at {0}
  sRef = {0};
  fcn( sRef, integrand, 2 );

  //PDE residual integrands:
  integrandTrue[0] = ( 11./5. ) + ( -2123./1000. );   // Basis function 1
  integrandTrue[1] = ( -11./5. ) + ( 2123./1000. );   // Basis function 2

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );

  // Test at {1}
  sRef = {1};
  fcn( sRef, integrand, 2 );

  //PDE residual integrands:
  integrandTrue[0] = ( 33./10. ) + ( -2123./1000. );   // Basis function 1
  integrandTrue[1] = ( -33./10. ) + ( 2123./1000. );   // Basis function 2

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );

  // Test at {1/2}
  sRef = {1./2.};
  fcn( sRef, integrand, 2 );

  //PDE residual integrands:
  integrandTrue[0] = ( 11./4. ) + ( -2123./1000. );   // Basis function 1
  integrandTrue[1] = ( -11./4. ) + ( 2123./1000. );   // Basis function 2

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );


  // test the element integral of the functor
  int quadratureorder = 1;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[2] = {0,0};
  Real rsd[2];

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  //PDE residual:
  rsd[0] = ( 11./4. ) + ( -2123./1000. ) ;   // Basis function 1
  rsd[1] = ( -11./4. ) + ( 2123./1000. ) ;   // Basis function 2

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_1D_Line_Jacobian_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::template MatrixQ<Real> MatrixQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line, ElementXField<PhysD1,TopoD1, Line> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;


  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3, b = 1.4;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  IntegrandClass fcnint( pde, {0} );

  for (int qorder = 1; qorder <= 4; qorder++)
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;
    RefCoordType sRef = Line::centerRef;
    DLA::VectorD<ArrayQ> integrand0(qfldElem.nDOF()), integrand1(qfldElem.nDOF());
    DLA::MatrixD<MatrixQ> mtxPDEElemTrue(qfldElem.nDOF(), qfldElem.nDOF());
    DLA::MatrixD<MatrixQ> mtxPDEElem(qfldElem.nDOF(), qfldElem.nDOF());

    // compute jacobians via finite differenec (exact for linear PDE)
    fcn(sRef, &integrand0[0], integrand0.m());

    for (int i = 0; i < qfldElem.nDOF(); i++)
    {
      qfldElem.DOF(i) += 1;
      fcn(sRef, &integrand1[0], integrand1.m());
      qfldElem.DOF(i) -= 1;

      for (int j = 0; j < qfldElem.nDOF(); j++)
        mtxPDEElemTrue(j,i) = integrand1[j] - integrand0[j];
    }

    mtxPDEElem = 0;

    // accumulate the jacobian via Surreal
    fcn(1., sRef, mtxPDEElem);

    for (int i = 0; i < qfldElem.nDOF(); i++)
      for (int j = 0; j < qfldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxPDEElemTrue(i,j), mtxPDEElem(i,j), small_tol, close_tol );
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_WeightVary_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  Element<Real,TopoD1,Line> efldElem( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass wfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, wfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElem.nDOF() );

    // line weighting
    for (int dof = 0; dof < wfldElem.nDOF(); dof ++ )
      wfldElem.DOF(dof) = 0;

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    Real rsdPDEElemW[1] = {0};
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

    int quadratureorder = 1;
    int nIntegrand = qfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD1, Line, Real> integralW(quadratureorder, efldElem.nDOF() );

    for (int i = 0; i < wfldElem.nDOF(); i++)
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdPDEElemW[0] = 0;
      integralW( fcnW, xfldElem, rsdPDEElemW, 1 );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdPDEElemW[0], rsdPDEElemB[i], small_tol, close_tol );

      // reset to zero
      wfldElem.DOF(i) = 0;
    }
  }

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_EstimateVary_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  ElementQFieldClass wfldElem( 0, BasisFunctionCategory_Legendre );
  wfldElem.DOF(0) = 1;

  for (int qorder = 1; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass efldElem(qorder, BasisFunctionCategory_Hierarchical);

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);
    std::vector<Real> rsdPDEElemW(efldElem.nDOF(), 0);

    int quadratureorder = 1;
    int nIntegrand = qfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD1, Line, Real> integralW(quadratureorder, efldElem.nDOF() );

    integralW( fcnW, xfldElem, rsdPDEElemW.data(), rsdPDEElemW.size() );

    // efld same order as qfld with a wfld of 1 gives the basis weighted function
    for (int i = 0; i < efldElem.nDOF(); i++)
      SANS_CHECK_CLOSE( rsdPDEElemW[i], rsdPDEElemB[i], small_tol, close_tol );

  }

}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle, ElementXField<PhysD2,TopoD2, Triangle> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandTrue[3];
  ArrayQ integrand[3];

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 13./10. ) + ( -4941./500. );   // Basis function 1
  integrandTrue[1] = ( -11./10. ) + ( 1181./200. );   // Basis function 2
  integrandTrue[2] = ( -1./5. ) + ( 3977./1000. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 39./10. ) + ( -4941./500. );   // Basis function 1
  integrandTrue[1] = ( -33./10. ) + ( 1181./200. );   // Basis function 2
  integrandTrue[2] = ( -3./5. ) + ( 3977./1000. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 26./5. ) + ( -4941./500. );   // Basis function 1
  integrandTrue[1] = ( -22./5. ) + ( 1181./200. );   // Basis function 2
  integrandTrue[2] = ( -4./5. ) + ( 3977./1000. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // Test at {1/3, 1/3}
  sRef = {1./3., 1./3.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 52./15. ) + ( -4941./500. );   // Basis function 1
  integrandTrue[1] = ( -44./15. ) + ( 1181./200. );   // Basis function 2
  integrandTrue[2] = ( -8./15. ) + ( 3977./1000. );   // Basis function 3

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = 1;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[3] = {0,0,0};
  Real rsd[3];
  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  //PDE residual: (advection) + (diffusion)
  rsd[0] = (26./15.) + (-4941./1000.);   // Basis function 1
  rsd[1] = (-22./15.) + (1181./400.);   // Basis function 2
  rsd[2] = (-4./15.) + (3977./2000.);   // Basis function 3

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_Jacobian_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::template MatrixQ<Real> MatrixQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle, ElementXField<PhysD2,TopoD2, Triangle> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0.1;  y1 = -0.2;
  x2 = 1.0;  y2 =  0.1;
  x3 = 0.0;  y3 =  1.1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // integrand
  IntegrandClass fcnint( pde, {0} );

  for (int qorder = 1; qorder <= 4; qorder++)
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    const int nDOF = qfldElem.nDOF();

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;
    RefCoordType sRef = Triangle::centerRef;
    DLA::VectorD<ArrayQ> integrand0(nDOF), integrand1(nDOF);
    DLA::MatrixD<MatrixQ> mtxPDEElemTrue(nDOF, nDOF);
    DLA::MatrixD<MatrixQ> mtxPDEElem(nDOF, nDOF);

    // compute jacobians via finite differenec (exact for linear PDE)
    fcn(sRef, &integrand0[0], integrand0.m());

    for (int i = 0; i < nDOF; i++)
    {
      qfldElem.DOF(i) += 1;
      fcn(sRef, &integrand1[0], integrand1.m());
      qfldElem.DOF(i) -= 1;

      for (int j = 0; j < nDOF; j++)
        mtxPDEElemTrue(j,i) = integrand1[j] - integrand0[j];
    }

    mtxPDEElem = 0;

    // accumulate the jacobian via Surreal
    fcn(1., sRef, mtxPDEElem);

    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        SANS_CHECK_CLOSE( mtxPDEElemTrue(i,j), mtxPDEElem(i,j), small_tol, close_tol );
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_WeightVary_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  Element<Real,TopoD2,Triangle> efldElem( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass wfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    // line weighting
    for (int dof = 0; dof < wfldElem.nDOF(); dof ++ )
      wfldElem.DOF(dof) = 0;

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    Real rsdPDEElemW[1] = {0};
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

    int quadratureorder = qorder+1;
    int nIntegrand = qfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD2, Triangle, Real> integralW(quadratureorder, efldElem.nDOF() );

    for (int i = 0; i < wfldElem.nDOF(); i++)
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdPDEElemW[0] = 0;
      integralW( fcnW, xfldElem, rsdPDEElemW, 1 );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdPDEElemW[0], rsdPDEElemB[i], small_tol, close_tol );

      // reset to zero
      wfldElem.DOF(i) = 0;
    }
  }

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_EstimateVary_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  Element<Real,TopoD2,Triangle> wfldElem( 0, BasisFunctionCategory_Legendre );
  wfldElem.DOF(0) = 1;

  for (int qorder = 1; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass efldElem(qorder, BasisFunctionCategory_Hierarchical);

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);
    std::vector<Real> rsdPDEElemW(efldElem.nDOF(), 0);

    int quadratureorder = qorder+1;
    int nIntegrand = qfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD2, Triangle, Real> integralW(quadratureorder, efldElem.nDOF() );

    integralW( fcnW, xfldElem, rsdPDEElemW.data(), rsdPDEElemW.size() );

    for (int i = 0; i < efldElem.nDOF(); i++)
      SANS_CHECK_CLOSE( rsdPDEElemW[i], rsdPDEElemB[i], small_tol, close_tol );

  }

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Quad_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Quad, ElementXField<PhysD2,TopoD2, Quad> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  // Quad grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 1;  y3 = 1;
  x4 = 0;  y4 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};
  xfldElem.DOF(3) = {x4, y4};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // Quad solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 6;
  qfldElem.DOF(3) = 4;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 5e-13;
  const Real close_tol = 5e-13;
  RefCoordType sRef;
  Real integrandTrue[4];
  ArrayQ integrand[4];

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 13./10. ) + ( -1254./125. );   // Basis function 1
  integrandTrue[1] = ( -11./10. ) + ( 1181./200. );   // Basis function 2
  integrandTrue[2] = ( 0 ) + ( 0 );   // Basis function 3
  integrandTrue[3] = ( -1./5. ) + ( 4127./1000. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 33./10. ) + ( -1181./200. );   // Basis function 1
  integrandTrue[1] = ( -27./10. ) + ( 889./500. );   // Basis function 2
  integrandTrue[2] = ( -3./5. ) + ( 4127./1000. );   // Basis function 3
  integrandTrue[3] = ( 0 ) + ( 0 );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {1, 1}
  sRef = {1, 1};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 0 ) + ( 0 );   // Basis function 1
  integrandTrue[1] = ( 6./5. ) + ( -4127./1000. );   // Basis function 2
  integrandTrue[2] = ( -39./5. ) + ( 1254./125. );   // Basis function 3
  integrandTrue[3] = ( 33./5. ) + ( -1181./200. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 4./5. ) + ( -4127./1000. );   // Basis function 1
  integrandTrue[1] = ( 0 ) + ( 0 );   // Basis function 2
  integrandTrue[2] = ( -22./5. ) + ( 1181./200. );   // Basis function 3
  integrandTrue[3] = ( 18./5. ) + ( -889./500. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // Test at {1/2, 1/2}
  sRef = {1./2., 1./2.};
  fcn( sRef, integrand, 4 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue[0] = ( 91./40. ) + ( -627./125. );   // Basis function 1
  integrandTrue[1] = ( -63./40. ) + ( 889./1000. );   // Basis function 2
  integrandTrue[2] = ( -91./40. ) + ( 627./125. );   // Basis function 3
  integrandTrue[3] = ( 63./40. ) + ( -889./1000. );   // Basis function 4

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  // test the element integral of the functor

  int quadratureorder = 2;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Quad, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[4] = {0,0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  Real rsd[4];

  //PDE residual: (advection) + (diffusion)
  rsd[0] = (59./30.) + (-627./125.);   // Basis function 1
  rsd[1] = (-19./15.) + (889./1000.);   // Basis function 2
  rsd[2] = (-31./12.) + (627./125.);   // Basis function 3
  rsd[3] = (113./60.) + (-889./1000.);   // Basis function 4

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[3], rsdPDEElem[3], small_tol, close_tol );


}


//----------------------------------------------------------------------------//
#if 0 // Needs more basis functions on Quad
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Quad_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Quad,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Quad,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
//    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass wfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, wfldElem.order() );
//    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    // line weighting
    for (int dof = 0; dof < wfldElem.nDOF(); dof ++ )
      wfldElem.DOF(dof) = 0;

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    ArrayQ rsdPDEElemW=0;
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

    int quadratureorder = 1;
    int nIntegrand = qfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD2, Quad, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

    ElementIntegral<TopoD2, Quad, ArrayQ> integralW(quadratureorder);

    for (int i = 0; i < wfldElem.nDOF(); i++)
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdPDEElemW = 0;
      integralW( fcnW, xfldElem, rsdPDEElemW );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdPDEElemW, rsdPDEElemB[i], small_tol, close_tol );

      // reset to zero
      wfldElem.DOF(i) = 0;
    }
  }

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_3D_Tet_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3,  PDEAdvectionDiffusion3D > PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD3,Tet> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD3,Tet, ElementXField<PhysD3,TopoD3, Tet> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  // tetrahdral grid element
  Real x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;

  x1 = 0;  y1 = 0;  z1 = 0;
  x2 = 1;  y2 = 0;  z2 = 0;
  x3 = 0;  y3 = 1;  z3 = 0;
  x4 = 0;  y4 = 0;  z4 = 1;

  xfldElem.DOF(0) = {x1, y1, z1};
  xfldElem.DOF(1) = {x2, y2, z2};
  xfldElem.DOF(2) = {x3, y3, z3};
  xfldElem.DOF(3) = {x4, y4, z4};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // tet solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandTrue[4];
  ArrayQ integrand[4];

  sRef = {0, 0, 0};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 9./5.)   + (-26823./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-11./10.) + ( 228./25.);
  integrandTrue[2] = (-1./5.)   + ( 994./125.);
  integrandTrue[3] = (-1./2.)   + ( 9751./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {1, 0, 0};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 27./5.)  + (-26823./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-33./10.) + ( 228./25.);
  integrandTrue[2] = (-3./5.)   + ( 994./125.);
  integrandTrue[3] = (-3./2.)   + ( 9751./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {0, 1, 0};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 36./5.) + (-26823./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-22./5.) + ( 228./25.);
  integrandTrue[2] = (-4./5.)  + ( 994./125.);
  integrandTrue[3] = (-2.)     + ( 9751./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {0, 0, 1};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 54./5.) + (-26823./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-33./5.) + ( 228./25.);
  integrandTrue[2] = (-6./5.)  + ( 994./125.);
  integrandTrue[3] = (-3.)     + ( 9751./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  sRef = {1./4., 1./4., 1./4.};
  fcn( sRef, integrand, 4 );

  integrandTrue[0] = ( 63./10.) + (-26823./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-77./20.) + ( 228./25.);
  integrandTrue[2] = (-7./10.)  + ( 994./125.);
  integrandTrue[3] = (-7./4.)   + ( 9751./1000.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 1;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD3, Tet, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[4] = {0,0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  Real rsd1 = ( 21./20.)  + (-8941./2000.);   // (advective) + (viscous)
  Real rsd2 = (-77./120.) + (38./25.);
  Real rsd3 = (-7./60.)   + (497./375.);
  Real rsd4 = (-7./24.)   + (9751./6000.);

  SANS_CHECK_CLOSE( rsd1, rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2, rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3, rsdPDEElem[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4, rsdPDEElem[3], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_3D_Hex_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3,  PDEAdvectionDiffusion3D > PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD3,Hex> ElementQFieldClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD3,Hex, ElementXField<PhysD3,TopoD3, Hex> > BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 8, xfldElem.nDOF() );

  // hexahedral grid element

  xfldElem.DOF(0) = {0, 0, 0};
  xfldElem.DOF(1) = {1, 0, 0};
  xfldElem.DOF(2) = {1, 1, 0};
  xfldElem.DOF(3) = {0, 1, 0};

  xfldElem.DOF(4) = {0, 0, 1};
  xfldElem.DOF(5) = {1, 0, 1};
  xfldElem.DOF(6) = {1, 1, 1};
  xfldElem.DOF(7) = {0, 1, 1};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 8, qfldElem.nDOF() );

  // hex solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  qfldElem.DOF(4) = 8;
  qfldElem.DOF(5) = 2;
  qfldElem.DOF(6) = 9;
  qfldElem.DOF(7) = 7;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 8, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;
  RefCoordType sRef;
  Real integrandTrue[8];
  ArrayQ integrand[8];

  sRef = {0, 0, 0};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 9./5.)   + (-36757./1000.);    // (advective) + (viscous)
  integrandTrue[1] = (-11./10.) + ( 1439./125.);
  integrandTrue[2] = ( 0.)      + ( 0.);
  integrandTrue[3] = (-1./5.)   + ( 1437./125. );

  integrandTrue[4] = (-1./2.) + ( 13749./1000. );
  integrandTrue[5] = ( 0. )   + ( 0.);
  integrandTrue[6] = ( 0. )   + ( 0.);
  integrandTrue[7] = ( 0. )   + ( 0.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );

  sRef = {1, 0, 0};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 33./10. ) + (-1039./250.);    // (advective) + (viscous)
  integrandTrue[1] = (-6./5.   ) + ( 1991./1000.);
  integrandTrue[2] = ( -3./5.  ) + ( 337./250.);
  integrandTrue[3] = ( 0.      ) + ( 0. );

  integrandTrue[4] = ( 0.    ) + ( 0. );
  integrandTrue[5] = (-3./2. ) + ( 817./1000. );
  integrandTrue[6] = ( 0.    ) + ( 0.);
  integrandTrue[7] = ( 0.    ) + ( 0.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {1, 1, 0};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 0.      ) + ( 0. );          // (advective) + (viscous)
  integrandTrue[1] = ( 4./5.   ) + (-1863./500.);
  integrandTrue[2] = ( -16./5. ) + (-2401./1000.);
  integrandTrue[3] = ( 22./5.  ) + ( 239./500. );

  integrandTrue[4] = ( 0. ) + ( 0. );
  integrandTrue[5] = ( 0. ) + ( 0. );
  integrandTrue[6] = (-2. ) + ( 5649./1000. );
  integrandTrue[7] = ( 0. ) + ( 0.);

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {0, 1, 0};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 6./5.   ) + (-2347./500.);    // (advective) + (viscous)
  integrandTrue[1] = ( 0.      ) + ( 0.);
  integrandTrue[2] = ( -33./5. ) + (-419./500.);
  integrandTrue[3] = ( 42./5.  ) + ( 1759./1000. );

  integrandTrue[4] = ( 0. ) + ( 0. );
  integrandTrue[5] = ( 0. ) + ( 0. );
  integrandTrue[6] = ( 0. ) + ( 0. );
  integrandTrue[7] = (-3. ) + ( 3773./1000. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {0, 0, 1};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 4. ) + (-803./200.);    // (advective) + (viscous)
  integrandTrue[1] = ( 0. ) + ( 0. );
  integrandTrue[2] = ( 0. ) + ( 0. );
  integrandTrue[3] = ( 0. ) + ( 0. );

  integrandTrue[4] = ( 32./5. ) + ( 471./40. );
  integrandTrue[5] = (-44./5. ) + (-879./100. );
  integrandTrue[6] = ( 0.     ) + ( 0. );
  integrandTrue[7] = (-8./5.  ) + ( 103./100. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {1, 0, 1};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 0. ) + ( 0. );    // (advective) + (viscous)
  integrandTrue[1] = ( 1. ) + (-263./1000. );
  integrandTrue[2] = ( 0. ) + ( 0. );
  integrandTrue[3] = ( 0. ) + ( 0. );

  integrandTrue[4] = ( 11./5. ) + ( 951./100. );
  integrandTrue[5] = (-14./5. ) + (-12213./1000. );
  integrandTrue[6] = (-2./5.  ) + ( 1483./500. );
  integrandTrue[7] = ( 0.     ) + ( 0. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {1, 1, 1};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 0.    ) + ( 0. );    // (advective) + (viscous)
  integrandTrue[1] = ( 0.    ) + ( 0. );
  integrandTrue[2] = ( 9./2. ) + ( -12811./1000. );
  integrandTrue[3] = ( 0.    ) + ( 0. );

  integrandTrue[4] = ( 0.      ) + ( 0. );
  integrandTrue[5] = ( 9./5.   ) + (-599./50. );
  integrandTrue[6] = (-81./5.  ) + ( 36123./1000. );
  integrandTrue[7] = ( 99./10. ) + ( -2833/250. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {0, 1, 1};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 0.    ) + ( 0. );    // (advective) + (viscous)
  integrandTrue[1] = ( 0.    ) + ( 0. );
  integrandTrue[2] = ( 0.    ) + ( 0. );
  integrandTrue[3] = ( 7./2. ) + (-351./200. );

  integrandTrue[4] = ( 7./5.   ) + ( -108./125. );
  integrandTrue[5] = ( 0.      ) + ( 0. );
  integrandTrue[6] = (-77./10. ) + ( 542./125. );
  integrandTrue[7] = ( 14./5.  ) + ( -1717./1000. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  sRef = {1./2., 1./2., 1./2.};
  fcn( sRef, integrand, 8 );

  integrandTrue[0] = ( 9/4.  ) + ( -5791./2000. );    // (advective) + (viscous)
  integrandTrue[1] = (-1./2. ) + ( -2163./1000. );
  integrandTrue[2] = (-1.    ) + ( 437./2000. );
  integrandTrue[3] = ( 7./4. ) + (-257./500. );

  integrandTrue[4] = ( 1.    ) + ( -437./2000. );
  integrandTrue[5] = (-7./4. ) + ( 257./500. );
  integrandTrue[6] = (-9./4. ) + ( 5791./2000. );
  integrandTrue[7] = ( 1./2. ) + ( 2163./1000. );

  SANS_CHECK_CLOSE( integrandTrue[0], integrand[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[1], integrand[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[2], integrand[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[3], integrand[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandTrue[4], integrand[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[5], integrand[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[6], integrand[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue[7], integrand[7], small_tol, close_tol );


  // test the element integral of the functor

  int quadratureorder = 2;
  int nIntegrand = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD3, Hex, ArrayQ> integral(quadratureorder, nIntegrand);

  ArrayQ rsdPDEElem[8] = {0,0,0,0, 0,0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );

  Real rsd1 = ( 461./240. ) + (-39247./12000.);   // (advective) + (viscous)
  Real rsd2 = (-83./240.  ) + (-851./480.);
  Real rsd3 = (-221./240. ) + (25./96.);
  Real rsd4 = ( 443./240. ) + (-6851./12000.);

  Real rsd5 = ( 257./240. ) + (2203/4000.);
  Real rsd6 = (-79./48.   ) + (-1081./4000.);
  Real rsd7 = (-207./80.  ) + (12991./4000.);
  Real rsd8 = ( 53./80.   ) + (7303./4000.);

  SANS_CHECK_CLOSE( rsd1, rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2, rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3, rsdPDEElem[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4, rsdPDEElem[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd5, rsdPDEElem[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd6, rsdPDEElem[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd7, rsdPDEElem[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd8, rsdPDEElem[7], small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
