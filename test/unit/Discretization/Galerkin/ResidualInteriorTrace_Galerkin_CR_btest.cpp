// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualInteriorTrace_Triangle_AD_btest
// testing of interior trace residual functions with Cauchy-Riemann


#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/CauchyRiemann/PDECauchyRiemann2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualInteriorTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualInteriorTrace_CR_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_Triangle_X1Q1_1Group )
{
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;

  PDEClass pde;

  // grid: P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qfld.nDOF() == 6 );

  // triangle solution data (left)
  qfld.DOF(0) = {1, 2};
  qfld.DOF(1) = {3, 4};
  qfld.DOF(2) = {5, 6};

  // triangle solution data (right)
  qfld.DOF(3) = {3, 7};
  qfld.DOF(4) = {4, 2};
  qfld.DOF(5) = {8, 9};

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureOrder = 2;

  // PDE residuals (left)
  Real rsd1L[2] = {0, 0};
  Real rsd2L[2] = {65/12. - 0.25*3*sqrt(2), -0.25 - 1./sqrt(2)};
  Real rsd3L[2] = {29/6. - 0.25*sqrt(2), 0.25*sqrt(2)};

  // PDE residuals (right)
  Real rsd1R[2] = {0, 0};
  Real rsd2R[2] = {-29/6. + 0.25*sqrt(2), -0.25*sqrt(2)};
  Real rsd3R[2] = {-65/12. + 0.25*3*sqrt(2), 0.25 + 1./sqrt(2)};

  // PDE global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(6);

  // general interface
  rsdPDEGlobal = 0;

  IntegrateInteriorTraceGroups<TopoD2>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdPDEGlobal),
                                                   xfld, qfld, &quadratureOrder, 1);

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( rsd1L[k], rsdPDEGlobal[0](k), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2L[k], rsdPDEGlobal[1](k), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3L[k], rsdPDEGlobal[2](k), small_tol, close_tol );

    SANS_CHECK_CLOSE( rsd1R[k], rsdPDEGlobal[3](k), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2R[k], rsdPDEGlobal[4](k), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3R[k], rsdPDEGlobal[5](k), small_tol, close_tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
