// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_StrongForm_AD_btest
// testing of cell element residual integrands for Galerkin: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h" // Basis Weighted
#include "Field/Element/ElementIntegral.h" // Field Weighted

#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_StrongForm.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDE1DClassPlusTime;
template class IntegrandCell_Galerkin_StrongForm< PDE1DClassPlusTime >::BasisWeighted<Real, TopoD1, Line, ElementXField<PhysD1,TopoD1, Line> >;
template class IntegrandCell_Galerkin_StrongForm< PDE1DClassPlusTime >::FieldWeighted<Real, TopoD1, Line, ElementXField<PhysD1,TopoD1, Line> >;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDE2DClassPlusTime;
template class IntegrandCell_Galerkin_StrongForm< PDE2DClassPlusTime >::
  BasisWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2,TopoD2, Triangle> >;
template class IntegrandCell_Galerkin_StrongForm< PDE2DClassPlusTime >::
  FieldWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2,TopoD2, Triangle> >;

//template class IntegrandCell_Galerkin_StrongForm< PDE2DClassPlusTime >::BasisWeighted<Real, TopoD2, Quad, ElementXField<PhysD2,TopoD2, Quad> >;
//
//typedef PDEAdvectionDiffusion<PhysD3,
//                              AdvectiveFlux3D_Uniform,
//                              ViscousFlux3D_Uniform,
//                              Source3D_None> PDEAdvectionDiffusion3D;
//typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDE3DClassPlusTime;
//template class IntegrandCell_Galerkin_StrongForm< PDE3DClassPlusTime >::BasisWeighted<Real, TopoD3, Tet, ElementXField<PhysD3,TopoD3, Tet> >;
//
//template class IntegrandCell_Galerkin_StrongForm< PDE3DClassPlusTime >::BasisWeighted<Real, TopoD3, Hex, ElementXField<PhysD3,TopoD3, Hex> >;
}

using namespace SANS;



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_StrongForm_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Strong_FW_1D_P2P3_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_StrongForm<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  int qorder = 2;
  ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // line solution
  qfldElem.DOF(0) =  5;
  qfldElem.DOF(1) = -4;
  qfldElem.DOF(2) =  3;

  ElementQFieldClass wfldElem(qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, wfldElem.order() );
  BOOST_CHECK_EQUAL( 4, wfldElem.nDOF() );

  // line weighting
  wfldElem.DOF(0) = 3;
  wfldElem.DOF(1) = 4;
  wfldElem.DOF(2) = 5;
  wfldElem.DOF(3) = 6;

  StabilizationMatrix tau( StabilizationType::Unstabilized, TauType::Glasby,qorder+1);
  Element<Real,TopoD1,Line> efldElem( 0, BasisFunctionCategory_Legendre );

  // integrand
  IntegrandClass fcnint( pde, {0}, tau );

  FieldWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandTrue=0;
  Real integrand[1]={0};

  // Test at {0}
  sRef = {0.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = ( 99./10. ) + ( 19107./125. );   // Weight function

  SANS_CHECK_CLOSE( integrandTrue, integrand[0], small_tol, close_tol );

  // Test at {1}
  sRef = {1.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = ( -462./5. ) + ( 25476./125. );   // Weight function

  SANS_CHECK_CLOSE( integrandTrue, integrand[0], small_tol, close_tol );

  // Test at {1/2}
  sRef = {1./2.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = ( -1683./20. ) + ( 108273./250. );   // Weight function

  SANS_CHECK_CLOSE( integrandTrue, integrand[0], small_tol, close_tol );

  // Test at {1/5}
  sRef = {1./5.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = ( (-792.*(50.+27.*sqrt(3.)))/3125. ) + ( (101904.*(50.+27.*sqrt(3.)))/15625. );   // Weight function

  SANS_CHECK_CLOSE( integrandTrue, integrand[0], small_tol, close_tol );

  // test the element integral of the functor
  int quadratureorder = 5;
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder,1);

  Real rsdTrue,rsd[1] = {0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsd, 1 );

  //PDE residual: (advective) + (viscous)
  rsdTrue = ( (11.*(-635.+144.*sqrt(3.)))/100. ) + ( 87043./250. );

  SANS_CHECK_CLOSE( rsdTrue, rsd[0], small_tol, close_tol );

}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_StrongForm<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  Element<Real, TopoD1, Line> efldElem( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass wfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, wfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElem.nDOF() );

    // line weighting
    for (int dof = 0; dof < wfldElem.nDOF(); dof ++ )
      wfldElem.DOF(dof) = 0;

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());

    StabilizationMatrix tau( StabilizationType::Unstabilized, TauType::Glasby, qorder+1);

    // integrand
    IntegrandClass fcnint( pde, {0}, tau );


    BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    Real rsdPDEElemW[1]={0};
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

    int quadratureorder = 1;
    int nIntegrand = qfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralW(quadratureorder,1);

    for (int i = 0; i < wfldElem.nDOF(); i++)
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdPDEElemW[0] = 0;
      integralW( fcnW, xfldElem, rsdPDEElemW, 1 );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdPDEElemW[0], rsdPDEElemB[i], small_tol, close_tol );

      // reset to zero
      wfldElem.DOF(i) = 0;
    }
  }

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Strong_FW_2D_Triangle_P2P3_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_StrongForm<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<ArrayQ,TopoD2,Triangle,ElementParam> FieldWeightedClass;


  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.321;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  int qorder = 2;
  ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qfldElem.order() );
  BOOST_CHECK_EQUAL( 6, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) =  1;
  qfldElem.DOF(1) =  3;
  qfldElem.DOF(2) =  4;
  qfldElem.DOF(3) =  2;
  qfldElem.DOF(4) =  3;
  qfldElem.DOF(5) =  4;

  ElementQFieldClass wfldElem(qorder+1, BasisFunctionCategory_Hierarchical);

  // triangle solution
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) = -4;
  wfldElem.DOF(5) =  4;
  wfldElem.DOF(6) = -3;
  wfldElem.DOF(7) = -1;
  wfldElem.DOF(8) = -2;
  wfldElem.DOF(9) = -1;

  BOOST_CHECK_EQUAL(  3, wfldElem.order() );
  BOOST_CHECK_EQUAL( 10, wfldElem.nDOF() );

  Element<Real,TopoD2,Triangle> efldElem( 0, BasisFunctionCategory_Legendre );

  // integrand
  StabilizationMatrix tau( StabilizationType::Unstabilized, TauType::Glasby, qorder+1);

  // integrand
  IntegrandClass fcnint( pde, {0}, tau );

  FieldWeightedClass fcn = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 6, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandTrue=0;
  Real integrand[1]={0};

  // Test at {0, 0}
  sRef = {0., 0.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = ( -228./5. ) + ( -6088./25. );   // Weight Function

  SANS_CHECK_CLOSE( integrandTrue, integrand[0], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0., 1.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = ( -12. ) + ( 9132./25. );   // Weight Function

  SANS_CHECK_CLOSE( integrandTrue, integrand[0], small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1., 0.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = ( -328./5. ) + ( 12176./25. );   // Weight Function

  SANS_CHECK_CLOSE( integrandTrue, integrand[0], small_tol, close_tol );

  // Test at {1/3, 1/3}
  sRef = {1./3., 1./3.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = ( 104./45. ) + ( 79144./225. );   // Weight Function

  SANS_CHECK_CLOSE( integrandTrue, integrand[0], small_tol, close_tol );

  // Test at {1/5, 1/2}
  sRef = {1./5., 1./2.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = ( (39.*(1925.+54.*sqrt(3.)))/12500. ) + ( (761.*(1925.+54.*sqrt(3.)))/3125. );   // Weight Function

  SANS_CHECK_CLOSE( integrandTrue, integrand[0], small_tol, close_tol );

  // test the element integral of the functor

  // quadrature rule (for linear solution: basis grad is const, flux is linear)
  int quadratureorder = 5;
  GalerkinWeightedIntegral<TopoD2, Triangle, Real> integral(quadratureorder,1);

  Real rsd,rsdPDEElem[1] = {0};

  // cell integration for canonical element
  integral( fcn, xfldElem, rsdPDEElem,1 );

  //PDE residual: (advection) + (diffusion)
  rsd = ((-174.+43.*sqrt(3.))/50.) + (131653./750.);   // Weight Function

  SANS_CHECK_CLOSE( rsd, rsdPDEElem[0], small_tol, close_tol );
}


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_StrongForm<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,ElementParam > BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam > FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  Element<Real,TopoD2,Triangle> efldElem( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    ElementQFieldClass wfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    // line weighting
    for (int dof = 0; dof < wfldElem.nDOF(); dof ++ )
      wfldElem.DOF(dof) = 0;

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());

    StabilizationMatrix tau( StabilizationType::Unstabilized, TauType::Glasby, qorder+1);

    // integrand
    IntegrandClass fcnint( pde, {0}, tau );

    BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    Real rsdPDEElemW[1]={0};
    std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

    int quadratureorder = 1;
    int nIntegrand = qfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD2, Triangle, Real> integralW(quadratureorder, 1);

    for (int i = 0; i < wfldElem.nDOF(); i++)
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdPDEElemW[0] = 0;
      integralW( fcnW, xfldElem, rsdPDEElemW, 1 );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdPDEElemW[0], rsdPDEElemB[i], small_tol, close_tol );

      // reset to zero
      wfldElem.DOF(i) = 0;
    }
  }

}
#endif



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
