// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandInteriorTrace_Galerkin_StrongForm_Triangle_AD_btest
// testing of trace element residual integrands for Galerkin: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_StrongForm.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h" // Basis Weighted
#include "Field/Element/ElementIntegral.h" // Field Weighted

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"


using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDE1DClassPlusTime;

typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
template class IntegrandInteriorTrace_Galerkin_StrongForm< PDE1DClassPlusTime >::
BasisWeighted<Real, TopoD0, Node, TopoD1, Line, Line,ElementXFieldLine,ElementXFieldLine>;
template class IntegrandInteriorTrace_Galerkin_StrongForm< PDE1DClassPlusTime >::
FieldWeighted<Real, TopoD0, Node, TopoD1, Line, Line,ElementXFieldLine,ElementXFieldLine>;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDE2DClassPlusTime;

typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTriangle;
template class IntegrandInteriorTrace_Galerkin_StrongForm< PDE2DClassPlusTime >::
BasisWeighted<Real, TopoD1, Line, TopoD2, Triangle, Triangle,ElementXFieldTriangle,ElementXFieldTriangle>;

template class IntegrandInteriorTrace_Galerkin_StrongForm< PDE2DClassPlusTime >::
FieldWeighted<Real, TopoD1, Line, TopoD2, Triangle, Triangle,ElementXFieldTriangle,ElementXFieldTriangle>;

typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldQuad;
template class IntegrandInteriorTrace_Galerkin_StrongForm< PDE2DClassPlusTime >::
BasisWeighted<Real, TopoD1, Line, TopoD2, Quad, Quad,ElementXFieldQuad,ElementXFieldQuad>;

typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_None> PDEAdvectionDiffusion3D;
typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDE3DClassPlusTime;
typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldTet;
template class IntegrandInteriorTrace_Galerkin_StrongForm< PDE3DClassPlusTime >::
BasisWeighted<Real, TopoD2, Triangle, TopoD3, Tet, Tet,ElementXFieldTet,ElementXFieldTet>;

typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldHex;
template class IntegrandInteriorTrace_Galerkin_StrongForm< PDE3DClassPlusTime >::
BasisWeighted<Real, TopoD2, Quad, TopoD3, Hex, Hex,ElementXFieldHex,ElementXFieldHex>;
}

using namespace SANS;



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandInteriorTrace_Galerkin_StrongForm_AD_test_suite )

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Galerkin_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParam,ElementParam> BasisWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemR.nDOF() );

  // line solution (left)
  qfldElemL.DOF(0) = 2;
  qfldElemL.DOF(1) = 3;

  // line solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 4;

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                             xfldElemL, qfldElemL,
                                             xfldElemR, qfldElemR );

  BOOST_CHECK( fcnint.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandLTrue[2];
  Real integrandRTrue[2];
  ArrayQ integrandL[2];
  ArrayQ integrandR[2];

  sRef = 0;
  fcn( sRef, integrandL, 2, integrandR, 2 );

  // PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = ( 0)        + (0)           + (-2123./500.);
  integrandLTrue[1] = ( 33./10.)  + (2123./1000.) + ( 2123./500.);

  // PDE residuals (right)
  integrandRTrue[0] = (-33./10.)  + (-2123./1000.) + (-2123./500.);
  integrandRTrue[1] = ( 0)        + ( 0)           + ( 2123./500.);

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = 0;
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[2] = {0,0};
  ArrayQ rsdPDEElemR[2] = {0,0};

  // cell integration for canonical element
  integral( fcn, xnode, rsdPDEElemL, nIntegrandL, rsdPDEElemR, nIntegrandR );

  // PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  Real rsd1L = ( 0)        + (0)           + (-2123./500.);
  Real rsd2L = ( 33./10.)  + (2123./1000.) + ( 2123./500.);

  // PDE residuals (right): (advective) + (viscous) + (dual-consistent)
  Real rsd1R = (-33./10.)  + (-2123./1000.) + (-2123./500.);
  Real rsd2R = ( 0)        + ( 0)           + ( 2123./500.);

  SANS_CHECK_CLOSE( rsd1L, rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2L, rsdPDEElemL[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1R, rsdPDEElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2R, rsdPDEElemR[1], small_tol, close_tol );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Galerkin_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<ArrayQ, TopoD0, Node, TopoD1, Line, Line,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  // solution
  int qorder = 1;
  ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemR.nDOF() );

  // line solution (left)
  qfldElemL.DOF(0) =  5;
  qfldElemL.DOF(1) = -4;

  // line solution (right)
  qfldElemR.DOF(0) = -4;
  qfldElemR.DOF(1) =  2;


  ElementQFieldCell wfldElemL(qorder+1,BasisFunctionCategory_Hierarchical);
  ElementQFieldCell wfldElemR(qorder+1,BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, wfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 2, wfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, wfldElemR.nDOF() );

  // line solution (left)
  wfldElemL.DOF(0) =  3;
  wfldElemL.DOF(1) =  4;
  wfldElemL.DOF(2) =  5;

  // line solution (right)
  wfldElemR.DOF(0) =  4;
  wfldElemR.DOF(1) = -4;
  wfldElemR.DOF(2) = -1;

  Element<Real,TopoD1,Line> efldElemL(0, BasisFunctionCategory_Legendre);
  Element<Real,TopoD1,Line> efldElemR(0, BasisFunctionCategory_Legendre);

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  FieldWeightedClass fcn = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                             xfldElemL, qfldElemL, wfldElemL, efldElemL,
                                             xfldElemR, qfldElemR, wfldElemR, efldElemR );

  BOOST_CHECK( fcnint.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandLTrue, integrandL[1]={0};
  Real integrandRTrue, integrandR[1]={0};

  // Test at sRef={0}, s={1}
  sRef = {0.};
  fcn( sRef, integrandL, 1, integrandR, 1 );

  //PDE residual integrands: (viscous)
  integrandLTrue = ( -6369./100. );   // Single Valued
  integrandRTrue = ( -6369./100. );   // Single Valued

  SANS_CHECK_CLOSE( integrandLTrue, integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue, integrandR[0], small_tol, close_tol );

  // test the trace element integral of the functor

  int quadratureorder =0;
  GalerkinWeightedIntegral<TopoD0, Node, Real, Real> integral(quadratureorder,1,1);

  Real rsdLTrue=0,rsdL[1]={0};
  Real rsdRTrue=0,rsdR[1]={0};

  // Trace integration for canonical element
  integral( fcn, xnode, rsdL, 1, rsdR, 1 );

  //PDE residual: (viscous)
  rsdLTrue = ( -6369./100. );   // Basis function
  rsdRTrue = ( -6369./100. );   // Basis function

  SANS_CHECK_CLOSE( rsdLTrue, rsdL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdRTrue, rsdR[0], small_tol, close_tol );


}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParam,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  Element<Real,TopoD1,Line> efldElemL( 0, BasisFunctionCategory_Legendre );
  Element<Real,TopoD1,Line> efldElemR( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qorder,   wfldElemL.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElemR.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElemR.nDOF() );

    BOOST_REQUIRE_EQUAL( wfldElemL.nDOF(), qfldElemL.nDOF() );
    BOOST_REQUIRE_EQUAL( wfldElemR.nDOF(), qfldElemR.nDOF() );
    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
      wfldElemL.DOF(dof) = 0;
      wfldElemR.DOF(dof) = 0;
    }

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                                xfldElemL, qfldElemL,
                                                xfldElemR, qfldElemR );
    FieldWeightedClass fcnW = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                                xfldElemL, qfldElemL, wfldElemL, efldElemL,
                                                xfldElemR, qfldElemR, wfldElemR, efldElemR );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    RefCoordTraceType sRef;
    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandR = qfldElemR.nDOF();
    Real rsdElemWL[1], rsdElemWR[1];
    std::vector<ArrayQ> rsdElemBL(nIntegrandL, 0);
    std::vector<ArrayQ> rsdElemBR(nIntegrandR, 0);

    int quadratureorder = 0;
    GalerkinWeightedIntegral<TopoD0, Node, ArrayQ, ArrayQ> integralB(quadratureorder, nIntegrandL, nIntegrandR);
    GalerkinWeightedIntegral<TopoD0, Node, Real, Real> integralW(quadratureorder,1,1);

    // cell integration for canonical element
    integralB( fcnB, xnode, rsdElemBL.data(), nIntegrandL, rsdElemBR.data(), nIntegrandR );
    for (int i = 0; i < wfldElemL.nDOF(); i++)
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1;
      wfldElemR.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL[0] = 0; rsdElemWR[0] = 0;
      integralW( fcnW, xnode, rsdElemWL, 1, rsdElemWR, 1 );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdElemWL[0], rsdElemBL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( rsdElemWR[0], rsdElemBR[i], small_tol, close_tol );

      // reset to zero
      wfldElemL.DOF(i) = 0;
      wfldElemR.DOF(i) = 0;
    }
  }

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Galerkin_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 4;

  // triangle solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 2;
  qfldElemR.DOF(2) = 9;

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                             xfldElemL, qfldElemL,
                                             xfldElemR, qfldElemR );

  BOOST_CHECK( fcnint.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;
  RefCoordTraceType sRef;
  Real integrandLTrue[3];
  Real integrandRTrue[3];
  ArrayQ integrandL[3];
  ArrayQ integrandR[3];

  sRef = 0;
  fcn( sRef, integrandL, 3, integrandR,3 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = ( 0 ) + ( 0 ) + ( -3177/(250.*sqrt(2)) );  // Basis function 1
  integrandLTrue[1] = ( 39/(10.*sqrt(2)) ) + ( -5073/(500.*sqrt(2)) ) + ( 2007/(250.*sqrt(2)) );  // Basis function 2
  integrandLTrue[2] = ( 0 ) + ( 0 ) + ( 117/(25.*sqrt(2)) );  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent)
  integrandRTrue[0] = ( 0 ) + ( 0 ) + ( 3177/(250.*sqrt(2)) );  // Basis function 1
  integrandRTrue[1] = ( 0 ) + ( 0 ) + ( -2007/(250.*sqrt(2)) );  // Basis function 2
  integrandRTrue[2] = ( -39/(10.*sqrt(2)) ) + ( 5073/(500.*sqrt(2)) ) + ( -117/(25.*sqrt(2)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[2], integrandL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[2], integrandR[2], small_tol, close_tol );


  sRef = 1;
  fcn( sRef, integrandL, 3, integrandR,3 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = ( 0 ) + ( 0 ) + ( 1059/(250.*sqrt(2)) );  // Basis function 1
  integrandLTrue[1] = ( 0 ) + ( 0 ) + ( -669/(250.*sqrt(2)) );  // Basis function 2
  integrandLTrue[2] = ( (13*sqrt(2))/5. ) + ( -5073/(500.*sqrt(2)) ) + ( -39/(25.*sqrt(2)) );  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent)
  integrandRTrue[0] = ( 0 ) + ( 0 ) + ( -1059/(250.*sqrt(2)) );  // Basis function 1
  integrandRTrue[1] = ( (-13*sqrt(2))/5. ) + ( 5073/(500.*sqrt(2)) ) + ( 669/(250.*sqrt(2)) );  // Basis function 2
  integrandRTrue[2] = ( 0 ) + ( 0 ) + ( 39/(25.*sqrt(2)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[2], integrandL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[2], integrandR[2], small_tol, close_tol );


  sRef = 0.5;
  fcn( sRef, integrandL, 3, integrandR,3 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = ( 0 ) + ( 0 ) + ( -1059/(250.*sqrt(2)) );  // Basis function 1
  integrandLTrue[1] = ( 91/(40.*sqrt(2)) ) + ( -5073/(1000.*sqrt(2)) ) + ( 669/(250.*sqrt(2)) );  // Basis function 2
  integrandLTrue[2] = ( 91/(40.*sqrt(2)) ) + ( -5073/(1000.*sqrt(2)) ) + ( 39/(25.*sqrt(2)) );  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent)
  integrandRTrue[0] = ( 0 ) + ( 0 ) + ( 1059/(250.*sqrt(2)) );  // Basis function 1
  integrandRTrue[1] = ( -91/(40.*sqrt(2)) ) + ( 5073/(1000.*sqrt(2)) ) + ( -669/(250.*sqrt(2)) );  // Basis function 2
  integrandRTrue[2] = ( -91/(40.*sqrt(2)) ) + ( 5073/(1000.*sqrt(2)) ) + ( -39/(25.*sqrt(2)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[2], integrandL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[2], integrandR[2], small_tol, close_tol );


  // test the trace element integral of the functor

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureorder = 2;

  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[3] = {0,0,0};
  ArrayQ rsdPDEElemR[3] = {0,0,0};
  Real rsdLTrue[3];
  Real rsdRTrue[3];

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDEElemL, nIntegrandL, rsdPDEElemR, nIntegrandR );

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  rsdLTrue[0] = ( 0 ) + ( 0 ) + ( -1059./250. ) ;   // Basis function 1
  rsdLTrue[1] = ( 13./6. ) + ( -5073./1000. ) + ( 669./250. ) ;   // Basis function 2
  rsdLTrue[2] = ( 143./60. ) + ( -5073./1000. ) + ( 39./25. ) ;   // Basis function 3

  //PDE residuals (right): (advective) + (viscous) + (dual-consistent)
  rsdRTrue[0] = ( 0 ) + ( 0 ) + ( 1059./250. ) ;   // Basis function 1
  rsdRTrue[1] = ( -143./60. ) + ( 5073./1000. ) + ( -669./250. ) ;   // Basis function 2
  rsdRTrue[2] = ( -13./6. ) + ( 5073./1000. ) + ( -39./25. ) ;   // Basis function 3

  SANS_CHECK_CLOSE( rsdLTrue[0], rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLTrue[1], rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLTrue[2], rsdPDEElemL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdRTrue[0], rsdPDEElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdRTrue[1], rsdPDEElemR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdRTrue[2], rsdPDEElemR[2], small_tol, close_tol );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Galerkin_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<ArrayQ, TopoD1, Line,
                                          TopoD2, Triangle, Triangle, ElementParam, ElementParam> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.321;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  int qorder = 1;
  ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) =  1;
  qfldElemL.DOF(1) =  3;
  qfldElemL.DOF(2) =  4;

  // triangle solution (right)
  qfldElemR.DOF(0) = -4;
  qfldElemR.DOF(1) =  4;
  qfldElemR.DOF(2) =  3;

  ElementQFieldCell wfldElemL(qorder+1, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell wfldElemR(qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElemL.order() );
  BOOST_CHECK_EQUAL( 6, wfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 2, wfldElemR.order() );
  BOOST_CHECK_EQUAL( 6, wfldElemR.nDOF() );

  // triangle solution (left)
  wfldElemL.DOF(0) = -2;
  wfldElemL.DOF(1) =  4;
  wfldElemL.DOF(2) =  3;
  wfldElemL.DOF(3) =  2;
  wfldElemL.DOF(4) =  4;
  wfldElemL.DOF(5) = -1;

  // triangle solution (right)
  wfldElemR.DOF(0) =  4;
  wfldElemR.DOF(1) =  3;
  wfldElemR.DOF(2) =  4;
  wfldElemR.DOF(3) =  2;
  wfldElemR.DOF(4) =  3;
  wfldElemR.DOF(5) = -2;

  Element<Real,TopoD2,Triangle> efldElemL(0,BasisFunctionCategory_Legendre);
  Element<Real,TopoD2,Triangle> efldElemR(0,BasisFunctionCategory_Legendre);

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  FieldWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                       xfldElemL, qfldElemL, wfldElemL, efldElemL,
                                       xfldElemR, qfldElemR, wfldElemR, efldElemR );

  BOOST_CHECK( fcnint.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandTrue;
  Real integrandL[1], integrandR[1];

  // Test at sRef={0}, {s,t}={1, 0}
  sRef = {0.};
  fcn( sRef, integrandL, 1, integrandR, 1 );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 91.*sqrt(2.)/2. );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue, integrandR[0], small_tol, close_tol );

  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef = {1./2.};
  fcn( sRef, integrandL, 1, integrandR, 1 );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 1001./(8.*sqrt(2.)) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue, integrandR[0], small_tol, close_tol );

  // Test at sRef={1}, {s,t}={0, 1}
  sRef = {1.};
  fcn( sRef, integrandL, 1, integrandR, 1 );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 273./(4.*sqrt(2.)) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue, integrandR[0], small_tol, close_tol );

  // Test at sRef={1/5}, {s,t}={4/5, 1/5}
  sRef = {1./5.};
  fcn( sRef, integrandL, 1, integrandR, 1 );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 11557./(100.*sqrt(2.)) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue, integrandR[0], small_tol, close_tol );

  // Test at sRef={1/8}, {s,t}={7/8, 1/8}
  sRef = {1./8.};
  fcn( sRef, integrandL, 1, integrandR, 1 );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 1729./(16.*sqrt(2.)) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue, integrandR[0], small_tol, close_tol );

  // Test at sRef={6/7}, {s,t}={1/7, 6/7}
  sRef = {6./7.};
  fcn( sRef, integrandL, 1, integrandR, 1 );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 1313./(14.*sqrt(2.)) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandTrue, integrandR[0], small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder =3;
  GalerkinWeightedIntegral<TopoD1, Line, Real, Real > integral(quadratureorder,1,1);

  Real rsdTrue,rsdL[1]={0},rsdR[1]={0};

  // Trace integration for canonical element
  integral( fcn, xedge, rsdL, 1, rsdR, 1 );

  //PDE residual: (viscous)
  rsdTrue = ( 2639./24. );   // Basis function

  SANS_CHECK_CLOSE( rsdTrue, rsdL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsdR[0], small_tol, close_tol );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin_StrongForm<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 3.1;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  Element<Real,TopoD2,Triangle> efldElemL( 0, BasisFunctionCategory_Legendre );
  Element<Real,TopoD2,Triangle> efldElemR( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qorder,   wfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElemR.nDOF() );

    BOOST_REQUIRE_EQUAL( wfldElemL.nDOF(), qfldElemL.nDOF() );
    BOOST_REQUIRE_EQUAL( wfldElemR.nDOF(), qfldElemR.nDOF() );
    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
      wfldElemL.DOF(dof) = 0;
      wfldElemR.DOF(dof) = 0;
    }

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                                xfldElemL, qfldElemL,
                                                xfldElemR, qfldElemR );
    FieldWeightedClass fcnW = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                                xfldElemL, qfldElemL, wfldElemL, efldElemL,
                                                xfldElemR, qfldElemR, wfldElemR, efldElemR );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    RefCoordTraceType sRef;
    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandR = qfldElemR.nDOF();
    Real rsdElemWL[1], rsdElemWR[1];
    std::vector<ArrayQ> rsdElemBL(nIntegrandL, 0);
    std::vector<ArrayQ> rsdElemBR(nIntegrandR, 0);

    int quadratureorder = 0;
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralB(quadratureorder, nIntegrandL, nIntegrandR);
    GalerkinWeightedIntegral<TopoD1, Line, Real, Real> integralW(quadratureorder,1,1);

    // cell integration for canonical element
    integralB( fcnB, xedge, rsdElemBL.data(), nIntegrandL, rsdElemBR.data(), nIntegrandR );
    for (int i = 0; i < wfldElemL.nDOF(); i++)
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1;
      wfldElemR.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL[0] = 0; rsdElemWR[0] = 0;
      integralW( fcnW, xedge, rsdElemWL, 1, rsdElemWR, 1 );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdElemWL[0], rsdElemBL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( rsdElemWR[0], rsdElemBR[i], small_tol, close_tol );

      // reset to zero
      wfldElemL.DOF(i) = 0;
      wfldElemR.DOF(i) = 0;
    }
  }

}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
