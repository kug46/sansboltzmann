// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualCell_Galerkin_AD_btest
// testing of cell residual functions for Galerkin with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/IntegrateCellGroups.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_Galerkin_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_1D_Galerkin_Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 1;

  Real rsd[2];
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  SLA::SparseVector<ArrayQ> rsdPDEElem(2);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  rsdPDEElem = 0;

  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin(fcnint, rsdPDEElem), xfld, qfld, &quadratureOrder, 1 );

  //PDE residual:
  rsd[0] = ( 11./4. ) + ( -2123./1000. ) ;   // Basis function 1
  rsd[1] = ( -11./4. ) + ( 2123./1000. ) ;   // Basis function 2

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );


}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_2D_Galerkin_1Triangle_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 1;

  Real rsd[3];
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  SLA::SparseVector<ArrayQ> rsdPDEElem(3);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  rsdPDEElem = 0;

  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdPDEElem), xfld, qfld, &quadratureOrder, 1 );

  //PDE residual: (advection) + (diffusion)
  rsd[0] = (26./15.) + (-627./125.);   // Basis function 1
  rsd[1] = (-22./15.) + (1181./400.);   // Basis function 2
  rsd[2] = (-4./15.) + (4127./2000.);   // Basis function 3

  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_2D_Galerkin_1Triangle_X1Q2 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.0;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P2 (aka Q2)
  int qorder = 2;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution field variable
  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  int nodeMap[3] = {0};
  int edgeMap[3] = {0};
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle>& qfldCells = qfld.getCellGroup<Triangle>(0);
  qfldCells.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );
  BOOST_CHECK_EQUAL( 5, nodeMap[2] );

  qfldCells.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 1, edgeMap[1] );
  BOOST_CHECK_EQUAL( 0, edgeMap[2] );

  // solution data
  qfld.DOF(nodeMap[0]) =  1;
  qfld.DOF(nodeMap[1]) =  3;
  qfld.DOF(nodeMap[2]) =  4;
  qfld.DOF(edgeMap[0]) = -7;
  qfld.DOF(edgeMap[1]) =  9;
  qfld.DOF(edgeMap[2]) = -5;

  // quadrature rule: basis grad is linear, flux is quadratic)
  int quadratureOrder = 3;

  // expected residuals: (advective) + (viscous)
  Real rsd1 = (1)         + (3201./125.);
  Real rsd2 = (-5./6.)    + (-122581./6000.);
  Real rsd3 = (-1./6.)    + (-31067./6000.);
  Real rsd4 = (-11./6.)   + (-37049./750.);
  Real rsd5 = (287./150.) + (92323./1500.);
  Real rsd6 = (-11./6.)   + (-9163./1500.);

  const Real tol = 1e-13;
  SLA::SparseVector<ArrayQ> rsdPDEElem(6);

  rsdPDEElem = 0;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdPDEElem), xfld, qfld, &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsd1, rsdPDEElem[nodeMap[0]], tol );
  BOOST_CHECK_CLOSE( rsd2, rsdPDEElem[nodeMap[1]], tol );
  BOOST_CHECK_CLOSE( rsd3, rsdPDEElem[nodeMap[2]], tol );
  BOOST_CHECK_CLOSE( rsd4, rsdPDEElem[edgeMap[0]], tol );
  BOOST_CHECK_CLOSE( rsd5, rsdPDEElem[edgeMap[1]], tol );
  BOOST_CHECK_CLOSE( rsd6, rsdPDEElem[edgeMap[2]], tol );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_2D_Galerkin_1Quad_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Quad_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 6;
  qfld.DOF(3) = 4;

  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // quadrature rule (quadratic: basis grad is linear, flux is linear)
  int quadratureOrder = 2;

  Real rsd[4];

  const Real small_tol = 5e-13;
  const Real close_tol = 5e-13;
  SLA::SparseVector<ArrayQ> rsdPDEElem(4);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  rsdPDEElem = 0;

  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdPDEElem), xfld, qfld, &quadratureOrder, 1 );

  //PDE residual: (advection) + (diffusion)
  rsd[0] = (59./30.) + (-627./125.);   // Basis function 1
  rsd[1] = (-19./15.) + (889./1000.);   // Basis function 2
  rsd[2] = (-31./12.) + (627./125.);   // Basis function 3
  rsd[3] = (113./60.) + (-889./1000.);   // Basis function 4


  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd[3], rsdPDEElem[3], small_tol, close_tol );


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_3D_Galerkin_1Tet_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3,  PDEAdvectionDiffusion3D > PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single tetrahedron, P1 (aka X1)
  XField3D_1Tet_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single tetrahedron, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  qfld.DOF(3) = 6;

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 1;

  Real rsd1 = ( 21./20.)  + (-8941./2000.);   // (advective) + (viscous)
  Real rsd2 = (-77./120.) + (38./25.);
  Real rsd3 = (-7./60.)   + (497./375.);
  Real rsd4 = (-7./24.)   + (9751./6000.);

  const Real tol = 1e-13;
  SLA::SparseVector<ArrayQ> rsdPDEElem(4);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  rsdPDEElem = 0;

  IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnint, rsdPDEElem), xfld, qfld, &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsd1, rsdPDEElem[0], tol );
  BOOST_CHECK_CLOSE( rsd2, rsdPDEElem[1], tol );
  BOOST_CHECK_CLOSE( rsd3, rsdPDEElem[2], tol );
  BOOST_CHECK_CLOSE( rsd4, rsdPDEElem[3], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_3D_Galerkin_1Hex_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3,  PDEAdvectionDiffusion3D > PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single hexahedron, P1 (aka X1)
  XField3D_1Hex_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 8, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 8, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  qfld.DOF(3) = 6;

  qfld.DOF(4) = 8;
  qfld.DOF(5) = 2;
  qfld.DOF(6) = 9;
  qfld.DOF(7) = 7;

  // quadrature rule (quadratic: basis grad is linear, flux is linear)
  int quadratureOrder = 2;

  Real rsd1 = ( 461./240. ) + (-39247./12000.);   // (advective) + (viscous)
  Real rsd2 = (-83./240.  ) + (-851./480.);
  Real rsd3 = (-221./240. ) + (25./96.);
  Real rsd4 = ( 443./240. ) + (-6851./12000.);

  Real rsd5 = ( 257./240. ) + (2203/4000.);
  Real rsd6 = (-79./48.   ) + (-1081./4000.);
  Real rsd7 = (-207./80.  ) + (12991./4000.);
  Real rsd8 = ( 53./80.   ) + (7303./4000.);

  const Real tol = 1e-13;
  SLA::SparseVector<ArrayQ> rsdPDEElem(8);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  rsdPDEElem = 0;

  IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnint, rsdPDEElem), xfld, qfld, &quadratureOrder, 1 );

  BOOST_CHECK_CLOSE( rsd1, rsdPDEElem[0], tol );
  BOOST_CHECK_CLOSE( rsd2, rsdPDEElem[1], tol );
  BOOST_CHECK_CLOSE( rsd3, rsdPDEElem[2], tol );
  BOOST_CHECK_CLOSE( rsd4, rsdPDEElem[3], tol );

  BOOST_CHECK_CLOSE( rsd5, rsdPDEElem[4], tol );
  BOOST_CHECK_CLOSE( rsd6, rsdPDEElem[5], tol );
  BOOST_CHECK_CLOSE( rsd7, rsdPDEElem[6], tol );
  BOOST_CHECK_CLOSE( rsd8, rsdPDEElem[7], tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
