// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_Galerkin_NS_btest
// testing of cell-integral jacobian: Navier-Stokes on triangles

#include <cmath>
#include <ostream>
#include <string>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"

#include "pde/NS/TraitsNavierStokes.h"

#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_Galerkin_Stabilized_NS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin2D_1Triangle_X1Q1 )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::MatrixQ<Real> MatrixQ;

  typedef IntegrandCell_Galerkin_Stabilized<NDPDEClass> IntegrandClass;

  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const Real Mach = 0.3;
  const Real Reynolds = 10;
  const Real Prandtl = 0.72;

  // Sutherland viscosity
  const Real tSuth = 198.6/540;     // R/R

  const Real rhoRef = 1;            // density scale
  const Real tRef = 1;              // temperature

  const Real lRef = 1;              // length scale

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;
  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  const int N = pde.N;

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // set nodal DOFs
  ArrayQ q1 = 0;
  ArrayQ q2 = 0;
  ArrayQ q3 = 0;
  pde.setDOFFrom( q1, DensityVelocityPressure2D<Real>(967./1000.,  1./4., 32./100., 107./100) );
  pde.setDOFFrom( q2, DensityVelocityPressure2D<Real>(103./100.,  23./100., 32./100., 108./100) );
  pde.setDOFFrom( q3, DensityVelocityPressure2D<Real>(11./10.,  21./100., 36./100., 105./100) );

  const int nstab = (int)StabilizationType::nStabilizationType;
  StabilizationType stabs[nstab];
  for (int i = 0; i < nstab; i++ )
    stabs[i] = (StabilizationType)i;

  const int ntau = (int)TauType::nTauType;
  TauType taus[ntau];
  for (int i = 0; i < ntau; i++ )
    taus[i] = (TauType)i;

  for (int qorder = 2; qorder <= 2; qorder++) // making sure the cubic basis works too
  {
    // solution: single triangle
    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

    const int nDOF = qfld.nDOF();

    // set high-order DOFs to zero
    qfld = 0;

    // nodal solution data
    qfld.DOF(0) = q1;
    qfld.DOF(1) = q2;
    qfld.DOF(2) = q3;

    // quadrature rule
    int quadratureOrder = (3*qorder+1);

    // jacobian via FD w/ residual operator
    SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
    std::vector<SLA::SparseVector<ArrayQ>> errors;
    DLA::MatrixD<MatrixQ> mtxGlob(nDOF, nDOF);

    errors.emplace_back(nDOF);
    errors.emplace_back(nDOF);

    for (int istab = 0; istab < nstab; istab++)
      for (int itau = 0; itau < ntau; itau++)
      {
        // skip invalid combinations
        if (!StabilizationMatrix::checkStabTau(stabs[istab], taus[itau]))
          continue;

        //don't do jacobian test for adjoint/AGLS - not using surreals consistently for nonlinear
        if (istab == (int)StabilizationType::Adjoint ||istab == (int)StabilizationType::AGLSAdjoint)
          continue;

        StabilizationMatrix tau( stabs[istab], taus[itau], qorder);

        // integrand
        IntegrandClass fcnint( pde, {0}, tau );

        // jacobian via Surreal
        mtxGlob = 0;
        IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnint, mtxGlob), xfld, qfld, &quadratureOrder, 1 );

        Real eps[2] = {1.0e-2, 1.0e-3};
        Real rate_range[2] = {1.8, 2.2};

        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < N; n++)
          {
            for (int k = 0; k < 2; k++)
            {
              // use central difference
              qfld.DOF(j)(n) -= eps[k];

              rsdGlobal0 = 0;
              IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal0), xfld, qfld, &quadratureOrder, 1 );

              qfld.DOF(j)(n) += 2*eps[k];

              rsdGlobal1 = 0;
              IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal1), xfld, qfld, &quadratureOrder, 1 );

              // reset the finite difference
              qfld.DOF(j)(n) -= eps[k];

              // compute the errors via finite difference
              for (int i = 0; i < nDOF; i++)
                for (int m = 0; m < N; m++)
                  errors[k][i][m] = fabs( mtxGlob(i,j)(m,n) - (rsdGlobal1[i](m) - rsdGlobal0[i](m))/(2*eps[k]) );
            }

            for (int i = 0; i < nDOF; i++)
              for (int m = 0; m < N; m++)
              {
                Real err_vec[2] = { errors[0][i][m], errors[1][i][m] };

                if (err_vec[0] == 0 || err_vec[1] == 0) continue;
                Real rate = log(err_vec[1]/err_vec[0])/log(eps[1]/eps[0]);

                BOOST_CHECK_MESSAGE( (rate >= rate_range[0] && rate <= rate_range[1]) || (err_vec[0] < 1e-8) || (err_vec[1] < 1e-8),
                                     "Rate check failed at col = " << j << ": m = " << m <<
                                     " n = " << n << ": rate = " << rate <<
                                     ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" );
              }
          }
        }
      }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin3D_1Tet_X1Q1 )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::MatrixQ<Real> MatrixQ;

  typedef IntegrandCell_Galerkin_Stabilized<NDPDEClass> IntegrandClass;

  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const Real Mach = 0.3;
  const Real Reynolds = 10;
  const Real Prandtl = 0.72;

  // Sutherland viscosity
  const Real tSuth = 198.6/540;     // R/R

  const Real rhoRef = 1;            // density scale
  const Real tRef = 1;              // temperature

  const Real lRef = 1;              // length scale

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;
  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  const int N = pde.N;

  // grid: single triangle, P1 (aka X1)
  XField3D_1Tet_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // set solution DOFs
  ArrayQ q0 = 0;
  pde.setDOFFrom( q0, DensityVelocityPressure3D<Real>(967./1000.,  1./4., 32./100., 48./100., 107./100) );

  const int nstab = (int)StabilizationType::nStabilizationType;
  StabilizationType stabs[nstab];
  for (int i = 0; i < nstab; i++ )
    stabs[i] = (StabilizationType)i;

  const int ntau = (int)TauType::nTauType;
  TauType taus[ntau];
  for (int i = 0; i < ntau; i++ )
    taus[i] = (TauType)i;

  // use smaller orders due to long run times
  for (int qorder = 1; qorder <= 2; qorder++)
  {
    // solution: single triangle
    Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Lagrange);

    const int nDOF = qfld.nDOF();

    // set uniform DOFs
    qfld = q0;

    // quadrature rule
    int quadratureOrder = (3*qorder+1);

    // jacobian via FD w/ residual operator; assumes scalar PDE
    // topology-specific single group interface
    SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
    std::vector<SLA::SparseVector<ArrayQ>> errors;
    DLA::MatrixD<MatrixQ> mtxGlob(nDOF, nDOF);

    errors.emplace_back(nDOF);
    errors.emplace_back(nDOF);

    for (int istab = 0; istab < nstab; istab++)
      for (int itau = 0; itau < ntau; itau++)
      {
        // skip invalid combinations
        if (!StabilizationMatrix::checkStabTau(stabs[istab], taus[itau]))
          continue;

        //don't do jacobian test for adjoint/AGLS - not using surreals consistently for nonlinear
        if (istab == (int)StabilizationType::Adjoint ||istab == (int)StabilizationType::AGLSAdjoint)
          continue;

        StabilizationMatrix tau( stabs[istab], taus[itau], qorder);


        // integrand
        IntegrandClass fcnint( pde, {0}, tau );

        // jacobian via Surreal
        mtxGlob = 0;
        IntegrateCellGroups<TopoD3>::integrate( JacobianCell_Galerkin(fcnint, mtxGlob), xfld, qfld, &quadratureOrder, 1 );

        Real eps[2] = {1.0e-2, 1.0e-3};
        Real rate_range[2] = {1.8, 2.2};

        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < N; n++)
          {
            for (int k = 0; k < 2; k++)
            {
              // use central difference
              qfld.DOF(j)(n) -= eps[k];

              rsdGlobal0 = 0;
              IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal0), xfld, qfld, &quadratureOrder, 1 );

              qfld.DOF(j)(n) += 2*eps[k];

              rsdGlobal1 = 0;
              IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal1), xfld, qfld, &quadratureOrder, 1 );

              // reset the finite difference
              qfld.DOF(j)(n) -= eps[k];

              for (int i = 0; i < nDOF; i++)
                for (int m = 0; m < N; m++)
                  errors[k][i][m] = fabs( mtxGlob(i,j)(m,n) - (rsdGlobal1[i](m) - rsdGlobal0[i](m))/(2*eps[k]) );
            }

            for (int i = 0; i < nDOF; i++)
              for (int m = 0; m < N; m++)
              {
                Real err_vec[2] = { errors[0][i][m], errors[1][i][m] };

                if (err_vec[0] == 0 || err_vec[1] == 0) continue;
                Real rate = log(err_vec[1]/err_vec[0])/log(eps[1]/eps[0]);

                BOOST_CHECK_MESSAGE( (rate >= rate_range[0] && rate <= rate_range[1]) || (err_vec[0] < 1e-8) || (err_vec[1] < 1e-8),
                                     "Rate check failed at col = " << j << ": m = " << m <<
                                     " n = " << n << ": rate = " << rate <<
                                     ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" );
              }
          }
        }
      }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
