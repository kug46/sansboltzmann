// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LocalSolve_Split_Galerkin_Triangle_AD_btest
// testing of 2-D DG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/Field_NodalView.h"
#include "Field/Local/Field_Local.h"
#include "Field/Local/XField_LocalPatch.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Local_Galerkin_Stabilized_Euler_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_Local_Galerkin_Stabilized_BoundaryEdge_Unsplit_P1_AD_test )
{
  typedef QTypePrimitiveRhoPressure QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;
  typedef BCParameters<BCVector> BCParams;

  typedef Field_CG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_CG_Cell;
  typedef Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_CG_Trace;

  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> AlgEqnSet;
  typedef AlgEqnSet::BCParams BCParams;
  typedef AlgEqnSet::SystemMatrix SystemMatrixClass;
  typedef AlgEqnSet::SystemVector SystemVectorClass;

  typedef AlgebraicEquationSet_Local_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                         XField<PhysD2, TopoD2>> AlgEqnSet_Local;

  typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  int jj = pow(2,1), ii = 3*jj;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);

  const int order = 1;
  // PDE
  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // Initial Condition
  //solution at inlet is (1, 0.1, 0, 1);
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(1, 0.1, 0, 1) );

  // BC
  // Create a BC dictionary
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;

  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = 0.0;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = 3.505;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = 0;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = 1.0;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2};
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {3};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);


  const StabilizationMatrix stab( StabilizationType::Adjoint, TauType::Glasby, order);

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 10;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // Output functional
  Real sExact = 0.0;
  NDOutputClass outEntropyError( pde, sExact );
  OutputIntegrandClass outputIntegrand( outEntropyError, {0} );

  //Solution
  QField2D_CG_Cell qfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld = q0;

  //Lagrange multipliers
  LGField2D_CG_Trace lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  BOOST_CHECK_EQUAL( lgfld.nDOF() , 0 );

  //-------------SOLVE GLOBAL PROBLEM----------------

  QuadratureOrder quadratureOrder( xfld, 2*order + 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  AlgEqnSet primal(xfld,qfld,lgfld,pde,stab,quadratureOrder,ResidualNorm_L2,tol,{0}, PyBCList, BCBoundaryGroups );
  SystemVectorClass q(primal.vectorStateSize()), qinit(primal.vectorStateSize());
  SystemVectorClass rsd(primal.vectorEqSize());

  primal.fillSystemVector(qinit);
  q = qinit;

  rsd = 0;

  std::vector<std::vector<Real>> rsdNorm( primal.residualNorm(rsd) );

  NewtonSolver<SystemMatrixClass> Solver( primal, NewtonSolverDict );

  BOOST_REQUIRE(Solver.solve(qinit,q).converged);
  primal.setSolutionField(q);

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

//  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // Left most of bottom edge
  std::array<int,2> edge{{0,1}};

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_constructor(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_constructor);

  //Find the mapping between the boundary conditions of local problem and the global problem
  std::vector<int> boundaryTraceGroups = xfld_local.getReSolveBoundaryTraceGroups();

  PyDict BCList_local = PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  for (std::size_t local_group = 0; local_group < boundaryTraceGroups.size(); local_group++)
  {
    //Get the global boundary-trace group index for the current local boundary-trace group
    int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[local_group],0}).first;

    BOOST_REQUIRE(globalBTraceGroup >= 0); //Check for non-negative index

    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it)
    {
      BCBoundaryGroups_local.insert(std::pair<std::string, std::vector<int>>(it->first,{}) );

      const std::vector<int>& grouplist = it->second;
      bool found = false;

      for (int j = 0; j < (int) grouplist.size(); j++)
      {
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the BC associated with the globalBTraceGroup we want, so add it to the local BTraceGroup map
          BCBoundaryGroups_local[it->first].push_back(local_group);
          BCList_local[it->first] = PyBCList[it->first];
          found = true;
          break;
        }
      }

      if (found) break;
    }
  } //loop over local btracegroups

//  BCBoundaryGroups_local["BCOut"] = {};
//  std::cout<< "local boundary conditions" << std::endl;
//  for (auto it = BCBoundaryGroups_local.begin(); it != BCBoundaryGroups_local.end(); ++it)
//  {
//    std::cout << it->first << " = ";
//    for (auto itt = it->second.begin(); itt != it->second.end(); ++itt)
//      std::cout << *itt;
//    std::cout<< std::endl;
//  }

  std::vector<int> active_local_boundaries = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(BCList_local, BCBoundaryGroups_local);

  Field_Local<QField2D_CG_Cell> qfld_local(SANS::split(xfld_local.getReSolveCellGroups()),
                                           xfld_local, qfld, order, BasisFunctionCategory_Hierarchical);
  Field_Local<LGField2D_CG_Trace> lgfld_local(xfld_local, lgfld, order, BasisFunctionCategory_Hierarchical,
                                              {active_local_boundaries} );

  std::unique_ptr<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>> up_resfld_local =
    SANS::make_unique<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>>(xfld_local,order,BasisFunctionCategory_Hierarchical);

  *up_resfld_local = 0;

//  output_Tecplot(qfld_local, "tmp/q_local.plt");

  BOOST_CHECK_EQUAL( lgfld_local.nDOF(), 0 );//no lagrange multipliers, there shouldn't be any dofs

//  std::cout<< "lgfld_local.nElem() = " << lgfld_local.nElem() << std::endl;

  int nMainBTraces = 0;
  std::vector<int> MainBTraceGroup_list;

  for (int i=0; i < lgfld_local.nBoundaryTraceGroups(); i++)
  {
    MainBTraceGroup_list.push_back(i);
    nMainBTraces += lgfld_local.getBoundaryTraceGroup<Line>(i).nElem();
  }

//  std::cout<<"nMainBTraces:"<<nMainBTraces<<std::endl;

//  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;
//
//  // Define the BoundaryGroups for each boundary condition
//  BCBoundaryGroups_local["BCName"] = MainBTraceGroup_list;

  QuadratureOrder quadratureOrder_local( xfld_local, -1 );
  std::vector<Real> eqnsettol = {1e-12, 1e-12};

  std::vector<int> localCellGroups = {0};
  std::vector<int> localInteriorTraceGroups = findInteriorTraceGroup(xfld_local,0,1);

  {
    AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, lgfld_local, up_resfld_local,
                                      pde, stab, quadratureOrder_local, ResidualNorm_L2, eqnsettol,
                                      localCellGroups, localInteriorTraceGroups, PyBCList, BCBoundaryGroups_local);

    //------------------------------------------------------------------------------------------

    // Local residual
    SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
    PrimalEqSet_Local.fillSystemVector(q_local);

    const bool validState = PrimalEqSet_Local.isValidStateSystemVector(q_local);

    BOOST_CHECK_EQUAL(validState, true );

    BOOST_REQUIRE_EQUAL( q_local.m(), 2 );
    BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::iq].m(), 3 );
    BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::ilg].m(), 0 );

    SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
    rsd_local = 0;
    PrimalEqSet_Local.residual(q_local, rsd_local);

    std::vector<std::vector<Real>> rsdNrm( PrimalEqSet_Local.residualNorm(rsd_local) );

    #if 0 //Print local residual vector
      cout <<endl << "Solution residual:" <<endl;
      for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

      cout <<endl << "Lagrange multiplier residual:" <<endl;
      for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
    #endif

    #ifdef __clang_analyzer__
      return; // clang analyzer complains about the matrix size below
    #endif

    //Jacobian
    SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
    SystemMatrixClass_Local jac_local(nz_local);

    BOOST_REQUIRE_EQUAL( jac_local.m(), 2 );
    BOOST_REQUIRE_EQUAL( jac_local.n(), 2 );
    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).m(), 3 );
    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).n(), 3 );

    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).m(), 3 );
    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).n(), 0 );

    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).m(), 0 );
    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).n(), 3 );

    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).m(), 0 );
    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).n(), 0 );

    jac_local = 0;
    PrimalEqSet_Local.jacobian(jac_local);

    BOOST_CHECK_EQUAL( xfld.nDOF(), 21 );
    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 8 );

    const std::array<std::vector<int>,2>& freeDOFs = PrimalEqSet_Local.getFreeDOFs();

    // Check that the right dofs are frozen

    // PDE DOFs
    std::vector<int> true_frozeDOFs = {3,4,5,6,7};
    std::vector<int> true_freeDOFs  =  {0,1,2};

    BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[0].size() );

    for ( auto it = true_frozeDOFs.begin(); it != true_frozeDOFs.end(); ++it)
      BOOST_CHECK( std::find(freeDOFs[0].begin(),freeDOFs[0].end(),*it) == (freeDOFs[0]).end() );

    for ( auto it = true_freeDOFs.begin(); it != true_freeDOFs.end(); ++it)
      BOOST_CHECK( std::find(freeDOFs[0].begin(),freeDOFs[0].end(),*it) != (freeDOFs[0]).end() );

    // BC DOFs
    true_frozeDOFs = {};
    true_freeDOFs = {};

    BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[1].size());

    for ( auto it = true_frozeDOFs.begin(); it != true_frozeDOFs.end(); ++it)
      BOOST_CHECK( std::find(freeDOFs[1].begin(),freeDOFs[1].end(),*it) == (freeDOFs[1]).end() );

    for ( auto it = true_freeDOFs.begin(); it != true_freeDOFs.end(); ++it)
      BOOST_CHECK( std::find(freeDOFs[1].begin(),freeDOFs[1].end(),*it) != (freeDOFs[1]).end() );



    SystemVectorClass_Local sln_local(rsd_local.size());
    // Flattening solve
    sln_local = 0;
    sln_local = DLA::InverseLUP::Solve(jac_local, rsd_local);

    //Update local solution fields
    q_local -= 0.5*sln_local;
    PrimalEqSet_Local.setSolutionField(q_local);

    //Re-compute residual and check if it's zero
    rsd_local = 0;
    PrimalEqSet_Local.residual(rsd_local);
    std::vector<std::vector<Real>> rsdNrm_local1( PrimalEqSet_Local.residualNorm(rsd_local) );

    BOOST_CHECK(rsdNrm_local1[0][0] <= rsdNrm[0][0]);
    BOOST_CHECK(rsdNrm_local1[0][1] <= rsdNrm[0][1]);
    BOOST_CHECK(rsdNrm_local1[0][2] <= rsdNrm[0][2]);
    BOOST_CHECK(rsdNrm_local1[0][3] <= rsdNrm[0][3]);
  }

}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
