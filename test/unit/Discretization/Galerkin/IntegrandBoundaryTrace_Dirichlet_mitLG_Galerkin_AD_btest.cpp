// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_Dirichlet_Galerkin_AD_btest
// testing of boundary integrands: Advection-Diffusion on Triangles

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
#if 0
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None > PDEAdvectionDiffusion1D;
#endif
typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
#if 0
typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_None> PDEAdvectionDiffusion3D;
#endif

//typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
//typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass3D;

//typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitLG>> BCClass1D;
typedef BCNDConvertSpace<PhysD2, BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitLG>> BCClass2D;
//typedef BCNDConvertSpace<PhysD3, BCAdvectionDiffusion<PhysD3,BCTypeDirichlet_mitLG>> BCClass3D;

//typedef NDVectorCategory<boost::mpl::vector1<BCClass1D>, BCClass1D::Category> NDBCVecCat1D;
typedef NDVectorCategory<boost::mpl::vector1<BCClass2D>, BCClass2D::Category> NDBCVecCat2D;
//typedef NDVectorCategory<boost::mpl::vector1<BCClass3D>, BCClass3D::Category> NDBCVecCat3D;

typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldArea_Triangle;
typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldArea_Quad;
typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldVolume_Tet;
typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldVolume_Hex;

// 1D
//template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, Galerkin>
//               ::BasisWeighted<Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::BasisWeighted<Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::BasisWeighted<Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::BasisWeighted<Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::BasisWeighted<Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;

// 1D
//template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, Galerkin>
//               ::FieldWeighted<Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
//template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
//               ::FieldWeighted<Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
//template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
//               ::FieldWeighted<Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::FieldWeighted<Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
//template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
//               ::FieldWeighted<Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin_AD_btest_suite )

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Line_BCTypeLinearRobin_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD1,BCTypeDirichlet_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD0,Node> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real A = 2;
  Real B = 3;
  Real bcdata = 5;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xfldTrace(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xfldTrace.order() );
  BOOST_CHECK_EQUAL( 1, xfldTrace.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xfldTrace.DOF(0) = {x2};
  xfldTrace.normalSignL() = 1;

  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // Lagrange multiplier
  ElementQFieldTrace lgfldElem(0, BasisFunctionCategory_Legendre);

  lgfldElem.DOF(0) =  7;

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xfldTrace, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, lgfldElem );

  BOOST_CHECK_EQUAL( 2, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 1, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  ArrayQ integrandPDETrue[2], integrandPDE[2] = {0,0};
  ArrayQ integrandBCTrue[1], integrandBC[1] = {0};

  // Test at sRef={0}, s={1}
  sRef = {0};
  fcn( sRef, integrandPDE, 2, integrandBC,1 );

  //PDE residual integrands: (advective) + (viscous) + (Lagrange)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( -274650387./6500000. );  // Basis function 1
  integrandPDETrue[1] = ( 33./10. ) + ( -2123./1000. ) + ( 503202287./6500000. );  // Basis function 2

  //BC residual integrands:
  integrandBCTrue[0] = ( 7369./1000. );  // Basis function 1

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBCTrue[0], integrandBC[0], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = 0;
  int nIntegrandL = qfldElem.nDOF();
  int nIntegrandR = lgfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDETrue[2], rsdPDE[2] = {0,0};
  ArrayQ rsdBCTrue[1], rsdBC[1] = {0};

  // cell integration for canonical element
  integral( fcn, xfldTrace, rsdPDE, nIntegrandL, rsdBC, nIntegrandR );

  //PDE residuals: (advective) + (viscous) + (Lagrange)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( -274650387./6500000. ) ; // Basis function 1
  rsdPDETrue[1] = ( 33./10. ) + ( -2123./1000. ) + ( 503202287./6500000. ) ; // Basis function 2

  //BC residuals:
  rsdBCTrue[0] = ( 7369./1000. ) ; // Basis function 1

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDE[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdBCTrue[0], rsdBC[0], small_tol, close_tol );
}
#endif
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Triangle_BCTypeLinearRobin_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeDirichlet_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;
  typedef IntegrandClass::BasisWeightedFlux<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedFluxClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123, kxy = 0.553;
  Real kyx = 0.478, kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real uB = 5;

  BCClass bc(pde, uB);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // Lagrange multiplier
  ElementQFieldTrace lgedge(order, BasisFunctionCategory_Hierarchical);

  lgedge.DOF(0) =  7;
  lgedge.DOF(1) = -9;

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, lgedge );
  BasisWeightedFluxClass fcnFlux = fcnint.integrandFlux( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 3, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  ArrayQ integrandPDETrue[3], integrandPDE[3], integrandPDEflux[3];
  ArrayQ integrandBCTrue[2], integrandBC[2];

  // Test at sRef={0}, {s,t}={1, 0}
  sRef=0;
  fcn( sRef, integrandPDE, 3, integrandBC, 2 );
  fcnFlux( sRef, integrandPDEflux, 3 );

  //PDE residual integrands: (advective) + (viscous) + (Lagrange)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( 39/(10.*sqrt(2)) ) + ( -4941/(500.*sqrt(2)) ) + ( 7 ); // Basis function 2
  integrandPDETrue[2] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 3

  //BC residual integrands:
  integrandBCTrue[0] = ( -2 ); // Basis function 1
  integrandBCTrue[1] = ( 0 ); // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0] + integrandPDEflux[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1] + integrandPDEflux[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2] + integrandPDEflux[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBCTrue[0], integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue[1], integrandBC[1], small_tol, close_tol );

  // Test at sRef={1}, {s,t}={0, 1}
  sRef=1;
  fcn( sRef, integrandPDE, 3, integrandBC, 2 );
  fcnFlux( sRef, integrandPDEflux, 3 );

  //PDE residual integrands: (advective) + (viscous) + (Lagrange)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 2
  integrandPDETrue[2] = ( (13*sqrt(2))/5. ) + ( -4941/(500.*sqrt(2)) ) + ( -9 ); // Basis function 3

  //BC residual integrands:
  integrandBCTrue[0] = ( 0 ); // Basis function 1
  integrandBCTrue[1] = ( -1 ); // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0]+integrandPDEflux[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1]+integrandPDEflux[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2]+integrandPDEflux[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBCTrue[0], integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue[1], integrandBC[1], small_tol, close_tol );

  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef=1/2.;
  fcn( sRef, integrandPDE, 3, integrandBC, 2 );
  fcnFlux( sRef, integrandPDEflux, 3 );

  //PDE residual integrands: (advective) + (viscous) + (Lagrange)
  integrandPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( 91/(40.*sqrt(2)) ) + ( -4941/(1000.*sqrt(2)) ) + ( -1./2. ); // Basis function 2
  integrandPDETrue[2] = ( 91/(40.*sqrt(2)) ) + ( -4941/(1000.*sqrt(2)) ) + ( -1./2. ); // Basis function 3

  //BC residual integrands:
  integrandBCTrue[0] = ( -3./4. ); // Basis function 1
  integrandBCTrue[1] = ( -3./4. ); // Basis function 2

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0]+integrandPDEflux[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1]+integrandPDEflux[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2]+integrandPDEflux[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBCTrue[0], integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue[1], integrandBC[1], small_tol, close_tol );

  // test the trace element integral of the functor

  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  int nIntegrandR = lgedge.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralflux(quadratureorder, nIntegrandL);

  ArrayQ rsdPDE[3] = {0,0,0}, rsdPDEflux[3] = {0,0,0};
  ArrayQ rsdBC[2] = {0,0};
  ArrayQ rsdPDETrue[3];
  ArrayQ rsdBCTrue[2];

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDE, nIntegrandL, rsdBC, nIntegrandR );
  integralflux( fcnFlux, xedge, rsdPDEflux, nIntegrandL );

  //PDE residuals: (advective) + (viscous) + (Lagrange)
  rsdPDETrue[0] = ( 0 ) + ( 0 ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 13./6. ) + ( -4941./1000. ) + ( 5/(3.*sqrt(2)) ); // Basis function 2
  rsdPDETrue[2] = ( 143./60. ) + ( -4941./1000. ) + ( -11/(3.*sqrt(2)) ); // Basis function 3

  //BC residuals:
  rsdBCTrue[0] = ( -5/(3.*sqrt(2)) ); // Basis function 1
  rsdBCTrue[1] = ( (-2*sqrt(2))/3. ); // Basis function 2

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDE[0]+rsdPDEflux[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDE[1]+rsdPDEflux[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDE[2]+rsdPDEflux[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdBCTrue[0], rsdBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue[1], rsdBC[1], small_tol, close_tol );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Triangle_BCTypeLinearRobin_X1Q1LG1W2MU2_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;

  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> FieldWeightedClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.321;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};


  // solution
  int qorder = 1;
  ElementQFieldCell qfldElem(qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // weight
  ElementQFieldCell wfldElem(qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution (left)
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  // Lagrange multiplier
  ElementQFieldTrace lgedge(qorder, BasisFunctionCategory_Hierarchical);

  lgedge.DOF(0) =  2;
  lgedge.DOF(1) = -3;

  // Lagrange multiplier
  ElementQFieldTrace muedge(qorder+1, BasisFunctionCategory_Hierarchical);

  muedge.DOF(0) = -3;
  muedge.DOF(1) =  2;
  muedge.DOF(2) = -1;

  // integrand FieldWeighted

  IntegrandClass fcnint( pde, bc, {0} );

  FieldWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1),
                                             xfldElem,
                                             qfldElem, wfldElem,
                                             lgedge, muedge );

  BOOST_CHECK_EQUAL( 3, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDE=0,integrandPDETrue;
  Real integrandBC=0, integrandBCTrue;

  // Test at sRef={0}, {s,t}={1, 0}
  sRef = {0.};
  fcn( sRef, integrandPDE, integrandBC );

  //PDE residual integrands: (advective) + (viscous) + (lagrange)
  integrandPDETrue = ( (39.*sqrt(2.))/5. ) + ( -5487./(125.*sqrt(2.)) )
                   + ( (179084683.+69305000.*sqrt(2.))/1250000. );  // Single Value

  //BC residual integrands: (lagrange)
  integrandBCTrue = ( -16461./(250.*sqrt(2.)) ) ;  // Single Value

  SANS_CHECK_CLOSE( integrandPDETrue, integrandPDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue,  integrandBC,  small_tol, close_tol );

  // Test at sRef={1}, {s,t}={0, 1}
  sRef = {1.};
  fcn( sRef, integrandPDE, integrandBC );

  //PDE residual integrands: (advective) + (viscous) + (lagrange)
  integrandPDETrue = ( (39.*sqrt(2.))/5. ) + ( -16461./(500.*sqrt(2.)) )
                   + ( (-77788071.+130994750.*sqrt(2.))/1250000. );  // Single Value

  //BC residual integrands: (lagrange)
  integrandBCTrue = ( 2.+5487./(125.*sqrt(2.)) ) ;  // Single Value

  SANS_CHECK_CLOSE( integrandPDETrue, integrandPDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue,  integrandBC,  small_tol, close_tol );

  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef = {1./2.};
  fcn( sRef, integrandPDE, integrandBC );

  //PDE residual integrands: (advective) + (viscous) + (lagrange)
  integrandPDETrue = ( 1001./(40.*sqrt(2.)) ) + ( -60357./(1000.*sqrt(2.)) )
                   + ( (51343906.-48210875.*sqrt(2.))/1250000. );  // Single Value

  //BC residual integrands: (lagrange)
  integrandBCTrue = ( (-3.*(250.+5487.*sqrt(2.)))/1000. ) ;  // Single Value

  SANS_CHECK_CLOSE( integrandPDETrue, integrandPDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue,  integrandBC,  small_tol, close_tol );

  // Test at sRef={1/5}, {s,t}={4/5, 1/5}
  sRef = {1./5.};
  fcn( sRef, integrandPDE, integrandBC );

  //PDE residual integrands: (advective) + (viscous) + (lagrange)
  integrandPDETrue = ( (6604.*sqrt(2.))/625. ) + ( -696849./(12500.*sqrt(2.)) )
                   + ( (653736581.-49691650.*sqrt(2.))/6250000. );  // Single Value

  //BC residual integrands: (lagrange)
  integrandBCTrue = ( (-33.*(100.+5487.*sqrt(2.)))/6250. ) ;  // Single Value

  SANS_CHECK_CLOSE( integrandPDETrue, integrandPDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue,  integrandBC,  small_tol, close_tol );

  // Test at sRef={1/8}, {s,t}={7/8, 1/8}
  sRef = {1./8.};
  fcn( sRef, integrandPDE, integrandBC );

  //PDE residual integrands: (advective) + (viscous) + (lagrange)
  integrandPDETrue = ( 1235./(64.*sqrt(2.)) ) + ( -104253./(2000.*sqrt(2.)) )
                   + ( (59797903.+5995075.*sqrt(2.))/500000. );  // Single Value

  //BC residual integrands: (lagrange)
  integrandBCTrue = ( (-9.*(125.+10974.*sqrt(2.)))/3200. ) ;  // Single Value

  SANS_CHECK_CLOSE( integrandPDETrue, integrandPDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBCTrue,  integrandBC,  small_tol, close_tol );

  // test the trace element integral of the FieldWeighted

  int quadratureorder = 3;
  ElementIntegral<TopoD1, Line, ArrayQ, ArrayQ> integral(quadratureorder);

  Real rsdPDE=0,rsdPDETrue;
  Real rsdBC=0, rsdBCTrue;

  // Trace integration for canonical element
  integral( fcn, xedge, rsdPDE, rsdBC );

  //PDE residual: (advective) + (viscous) + (lagrange)
  rsdPDETrue = ( 1313./60. ) + ( -53041./1000. )+ ( (153336118.+3728125.*sqrt(2.))/(1875000.*sqrt(2.)) );   // Basis function

  //BC residual:  (lagrange)
  rsdBCTrue = ( -(500.+38409.*sqrt(2.))/(1500.*sqrt(2.)) );   // Basis function

  SANS_CHECK_CLOSE( rsdPDETrue, rsdPDE, small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdBCTrue,  rsdBC,  small_tol, close_tol );
}


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.348;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc , source );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElem( qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());
    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
    }

    ElementQFieldTrace lgedge( qorder, BasisFunctionCategory_Hierarchical );
    ElementQFieldTrace muedge( qorder, BasisFunctionCategory_Hierarchical );

    BOOST_CHECK_EQUAL( qorder, lgedge.order() );
    BOOST_CHECK_EQUAL( qorder, muedge.order() );
    BOOST_CHECK_EQUAL( qorder+1, lgedge.nDOF() );
    BOOST_CHECK_EQUAL( qorder+1, muedge.nDOF() );

    BOOST_REQUIRE_EQUAL( lgedge.nDOF(), muedge.nDOF() );
    for (int dof = 0; dof< lgedge.nDOF(); dof++ )
    {
      lgedge.DOF(dof) = (dof+1)*pow(-1,dof);
      muedge.DOF(dof) = 0;
    }

    IntegrandClass fcnint( pde, bc, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, lgedge );
    FieldWeightedClass fcnW = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, wfldElem, lgedge, muedge );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrandCell = qfldElem.nDOF();
    const int nIntegrandTrace = lgedge.nDOF();
    FieldWeightedClass::IntegrandCellType rsdPDEElemW=0;
    FieldWeightedClass::IntegrandTraceType rsdBCElemW=0;
    std::vector<BasisWeightedClass::IntegrandCellType<Real>> rsdPDEElemB(nIntegrandCell,0); // trickery to get round the POD warning
    std::vector<BasisWeightedClass::IntegrandTraceType<Real>> rsdBCElemB(nIntegrandTrace,0); // trickery to get round the POD warning

    int quadratureorder = -1;
    GalerkinWeightedIntegral<TopoD1, Line, BasisWeightedClass::IntegrandCellType<Real>,
                                           BasisWeightedClass::IntegrandTraceType<Real>> integralB(quadratureorder, nIntegrandCell, nIntegrandTrace);

    // cell integration for canonical element
    integralB( fcnB, xedge, rsdPDEElemB.data(), nIntegrandCell, rsdBCElemB.data(), nIntegrandTrace );

    ElementIntegral<TopoD1, Line, FieldWeightedClass::IntegrandCellType,
                                  FieldWeightedClass::IntegrandTraceType> integralW(quadratureorder);

    for (int i = 0; i < wfldElem.nDOF(); i++) // testing the wfld
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdPDEElemW = 0; rsdBCElemW = 0;
      integralW( fcnW, xedge, rsdPDEElemW, rsdBCElemW );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdPDEElemW, rsdPDEElemB[i], small_tol, close_tol );

      // reset to 0
      wfldElem.DOF(i) = 0;
    }
    for (int i = 0; i < muedge.nDOF(); i++) // testing the mufld
    {
      // set just one of the weights to one
      muedge.DOF(i) = 1;

      // cell integration for canonical element
      rsdPDEElemW = 0; rsdBCElemW = 0;
      integralW( fcnW, xedge, rsdPDEElemW, rsdBCElemW );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdBCElemW, rsdBCElemB[i], small_tol, close_tol );

      // reset to 0
      muedge.DOF(i) = 0;
    }
  }

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Triangle_BCTypeFlux_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion< PhysD2, BCTypeFlux<PDEAdvectionDiffusion2D> > BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real bcdata = 3;
  ArrayQ bcdataVec = bcdata;

  BCClass bc(pde, bcdataVec);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // grid coordinates
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // Lagrange multiplier

  order = 1;
  ElementQFieldTrace lgedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, lgedge.order() );
  BOOST_CHECK_EQUAL( 2, lgedge.nDOF() );

  lgedge.DOF(0) =  7;
  lgedge.DOF(1) = -9;

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, lgedge );

  BOOST_CHECK_EQUAL( 3, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDE1, integrandPDE2, integrandPDE3;
  Real integrandBC1, integrandBC2;
  ArrayQ integrandPDE[3] = {0,0,0};
  ArrayQ integrandBC[2] = {0,0};

  sRef = 0;
  fcn( sRef, integrandPDE, 3, integrandBC, 2 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                 + ( 6969279./(134375*sqrt(2)));
  integrandPDE2 = (-402*sqrt(2)/125.) + (-4402689./(134375*sqrt(2)));
  integrandPDE3 = (0)                 + (-256659*sqrt(2)/26875.);

  // BC residuals
  integrandBC1 = -3 - 402*sqrt(2)/125.;
  integrandBC2 = 0;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );


  sRef = 1;
  fcn( sRef, integrandPDE, 3, integrandBC, 2 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                 + (-3614367./(268750*sqrt(2)));
  integrandPDE2 = (0)                 + ( 2283297./(268750*sqrt(2)));
  integrandPDE3 = (-327*sqrt(2)/125.) + (  133107./(26875*sqrt(2)));

  // BC residuals
  integrandBC1 = 0;
  integrandBC2 = -3 - 327*sqrt(2)/125.;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );


  sRef = 0.5;
  fcn( sRef, integrandPDE, 3, integrandBC, 2 );

  // PDE residuals: (boundary flux) + (Lagrange)
  integrandPDE1 = (0)                 + (10324191./(537500*sqrt(2)));
  integrandPDE2 = (-729*sqrt(2)/500.) + (-6522081./(537500*sqrt(2)));
  integrandPDE3 = (-729*sqrt(2)/500.) + (-3802110./(537500*sqrt(2)));

  // BC residuals
  integrandBC1 = -3*(250 + 243*sqrt(2))/500.;
  integrandBC2 = -3*(250 + 243*sqrt(2))/500.;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  int nIntegrandR = lgedge.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[3] = {0,0,0};
  ArrayQ rsdBCElemR[2] = {0,0};

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDEElemL, nIntegrandL, rsdBCElemR, nIntegrandR );

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  Real rsd1PDE = (0)      + (0)          + (10324191./537500.);
  Real rsd2PDE = (2)      + (-627./125.) + (-6522081./537500.);
  Real rsd3PDE = (11./5.) + (-627./125.) + (-380211./53750.);

  // BC residuals:
  Real rsd1BC = (-754. - 375.*sqrt(2))/250.;
  Real rsd2BC = (-704. - 375.*sqrt(2))/250.;

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3PDE, rsdPDEElemL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1BC, rsdBCElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2BC, rsdBCElemR[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Quad_BCTypeLinearRobin_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Quad,ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 1;  y3 = 1;
  x4 = 0;  y4 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};
  xfldElem.DOF(3) = {x4, y4};

  xedge.DOF(0) = {x1, y1};
  xedge.DOF(1) = {x2, y2};


  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // quad solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  // Lagrange multiplier
  ElementQFieldTrace lgedge(order, BasisFunctionCategory_Hierarchical);

  lgedge.DOF(0) =  7;
  lgedge.DOF(1) = -9;

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, lgedge );

  BOOST_CHECK_EQUAL( 4, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDE1, integrandPDE2, integrandPDE3, integrandPDE4;
  Real integrandBC1, integrandBC2;
  ArrayQ integrandPDE[4];
  ArrayQ integrandBC[2];

  sRef = 0;
  fcn( sRef, integrandPDE, 4, integrandBC, 2 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (-1./5.) + (6141./1000.) + (2497887./125000.);
  integrandPDE2 = (0)      + (0)           + (-14853027./2500000.);
  integrandPDE3 = (0)      + (0)           + (0);
  integrandPDE4 = (0)      + (0)           + (-27047013./2500000.);

  // BC residuals
  integrandBC1 = -7141./500.;
  integrandBC2 = 0;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );


  sRef = 1;
  fcn( sRef, integrandPDE, 4, integrandBC, 2 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (0)      + (0)           + (-29371489/2500000.);
  integrandPDE2 = (-3./5.) + (2113./1000.) + (-20023601./1250000.);
  integrandPDE3 = (0)      + (0)           + (53484791./2500000.);
  integrandPDE4 = (0)      + (0)           + (0);

  // BC residuals
  integrandBC1 = 0;
  integrandBC2 = -2113./500.;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );


  sRef = 1./2.;
  fcn( sRef, integrandPDE, 4, integrandBC, 2 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (-1./5.) + (4127./2000.) + (-31675451./5000000.);
  integrandPDE2 = (-1./5.) + (4127./2000.) + (-2638527./5000000.);
  integrandPDE3 = (0)      + (0)           + (13218889./5000000.);
  integrandPDE4 = (0)      + (0)           + (13218889./5000000.);

  // BC residuals
  integrandBC1 = -4627./1000.;
  integrandBC2 = -4627./1000.;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  int nIntegrandR = lgedge.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[4] = {0,0,0,0};
  ArrayQ rsdBCElemR[2] = {0,0};

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDEElemL, nIntegrandL, rsdBCElemR, nIntegrandR );

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  Real rsd1PDE = (-1./6.)  + (2879./1200.)  + (-42764651./15000000.);
  Real rsd2PDE = (-7./30.) + (10367./6000.) + (-60177283./15000000.);
  Real rsd3PDE = (0)       + (0)            + ( 79922569./15000000.);
  Real rsd4PDE = (0)       + (0)            + (-121847./3000000.);

  // BC residuals:
  Real rsd1BC = -1093./200.;
  Real rsd2BC = -3789./1000.;

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3PDE, rsdPDEElemL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4PDE, rsdPDEElemL[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1BC, rsdBCElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2BC, rsdBCElemR[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Tet_BCTypeLinearRobin_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD3,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD3,TopoD2,Triangle> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD3,Tet> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,TopoD3,Tet,ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kxy, kyy, kyz,
                                  kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  ElementXFieldTrace xtri(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xtri.order() );
  BOOST_CHECK_EQUAL( 3, xtri.nDOF() );

  // tetrahdral grid element
  Real x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;

  x1 = 0;  y1 = 0;  z1 = 0;
  x2 = 1;  y2 = 0;  z2 = 0;
  x3 = 0;  y3 = 1;  z3 = 0;
  x4 = 0;  y4 = 0;  z4 = 1;

  xfldElem.DOF(0) = {x1, y1, z1};
  xfldElem.DOF(1) = {x2, y2, z2};
  xfldElem.DOF(2) = {x3, y3, z3};
  xfldElem.DOF(3) = {x4, y4, z4};

  xtri.DOF(0) = {x2, y2, z2};
  xtri.DOF(1) = {x3, y3, z3};
  xtri.DOF(2) = {x4, y4, z4};


  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  // Lagrange multiplier
  ElementQFieldTrace ltri(order, BasisFunctionCategory_Hierarchical);

  ltri.DOF(0) =  7;
  ltri.DOF(1) = -9;
  ltri.DOF(2) =  5;

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xtri, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, ltri );

  BOOST_CHECK_EQUAL( 4, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDE1, integrandPDE2, integrandPDE3, integrandPDE4;
  Real integrandBC1, integrandBC2, integrandBC3;
  ArrayQ integrandPDE[4];
  ArrayQ integrandBC[3];

  sRef = {0, 0};
  fcn( sRef, integrandPDE, 4, integrandBC, 3 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (0)             + (0)                    + (-37042563./1250000. - 40049.*sqrt(3)/1250.           );
  integrandPDE2 = (9.*sqrt(3)/5.) + (-8941.*sqrt(3)/1000.) + ( 60268979./2500000. +         12949./(200.*sqrt(3))  );
  integrandPDE3 = (0)             + (0)                    + (   831513./100000.  +   899.*sqrt(3)/100.            );
  integrandPDE4 = (0)             + (0)                    + ( 11811061./1250000. +         38309./(1250.*sqrt(3)) );

  // BC residuals
  integrandBC1 = 8941.*sqrt(3)/500.;
  integrandBC2 = 0;
  integrandBC3 = 0;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC3, integrandBC[2], small_tol, close_tol );


  sRef = {1,0};
  fcn( sRef, integrandPDE, 4, integrandBC, 3 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (0)              + (0)                    + (-37042563./1250000. +  73193.*sqrt(3)/1250.           );
  integrandPDE2 = (0)              + (0)                    + ( 29675179./2500000. -         175907./(sqrt(3)*2500.) );
  integrandPDE3 = (12.*sqrt(3)/5.) + (-8941.*sqrt(3)/1000.) + (    83053./20000.   - 136809.*sqrt(3)/5000.           );
  integrandPDE4 = (0)              + (0)                    + ( 11811061./1250000. -          70013./(sqrt(3)*1250.) );

  // BC residuals
  integrandBC1 = 0;
  integrandBC2 = 1 + 8941.*sqrt(3)/500.;
  integrandBC3 = 0;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC3, integrandBC[2], small_tol, close_tol );

  sRef = {0,1};
  fcn( sRef, integrandPDE, 4, integrandBC, 3 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (0)               + (0)                    + (-37042563./1250000. - 17953.*sqrt(3)/1250.           );
  integrandPDE2 = (0)               + (0)                    + ( 29675179./2500000. +         43147./(2500.*sqrt(3)) );
  integrandPDE3 = (0)               + (0)                    + (   831513./100000.  +   403.*sqrt(3)/100.            );
  integrandPDE4 = (18.*sqrt(3)/5.)  + (-8941.*sqrt(3)/1000.) + ( 23107961./1250000. +         28463./(1000.*sqrt(3)) );

  // BC residuals
  integrandBC1 = 0;
  integrandBC2 = 0;
  integrandBC3 = 3 + 8941.*sqrt(3)/500.;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC3, integrandBC[2], small_tol, close_tol );

  sRef = {1./3., 1./3.};
  fcn( sRef, integrandPDE, 4, integrandBC, 3 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (0)                + (0)                      + ( -37042563./1250000.  + 15191./(1250.*sqrt(3)) );
  integrandPDE2 = (13./(5.*sqrt(3))) + (-8941./(1000.*sqrt(3))) + ( 309858011./22500000. - 11879./(3000.*sqrt(3)) );
  integrandPDE3 = (13./(5.*sqrt(3))) + (-8941./(1000.*sqrt(3))) + (   9194873./900000.   +  8941./(5000.*sqrt(3)) - 143.*sqrt(3)/100.);
  integrandPDE4 = (13./(5.*sqrt(3))) + (-8941./(1000.*sqrt(3))) + ( 127690249./11250000. - 44501./(15000.*sqrt(3)) );

  // BC residuals
  integrandBC1 = 4./9. + 8941./(500.*sqrt(3));
  integrandBC2 = 4./9. + 8941./(500.*sqrt(3));
  integrandBC3 = 4./9. + 8941./(500.*sqrt(3));

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC3, integrandBC[2], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  int nIntegrandR = ltri.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[4] = {0,0,0,0};
  ArrayQ rsdBCElemR[3] = {0,0,0};

  // cell integration for canonical element
  integral( fcn, xtri, rsdPDEElemL, nIntegrandL, rsdBCElemR, nIntegrandR );

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  Real rsd1PDE = (0)       + (0)            + ( 15191./2500. - 37042563*sqrt(3)/2500000.);
  Real rsd2PDE = (6./5.)   + (-8941./2000.) + ( -5999./6000. + 35789779*sqrt(3)/5000000.);
  Real rsd3PDE = (51./40.) + (-8941./2000.) + (-27309./10000. + 2818291/(sqrt(3)*200000.));
  Real rsd4PDE = (57./40.) + (-8941./2000.) + (-29501./30000. + 43605083/(sqrt(3)*2500000.) );

  // BC residuals:
  Real rsd1BC = 8941/1000. + 1./(2.*sqrt(3));
  Real rsd2BC = 8941/1000. + 5./(8.*sqrt(3));
  Real rsd3BC = 8941/1000. + 7./(8.*sqrt(3));

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3PDE, rsdPDEElemL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4PDE, rsdPDEElemL[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1BC, rsdBCElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2BC, rsdBCElemR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3BC, rsdBCElemR[2], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Hex_BCTypeLinearRobin_X1Q1LG1_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion<PhysD3,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD3,TopoD2,Quad> ElementXFieldTrace;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD3,Hex> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Quad,TopoD3,Hex,ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kxy, kyy, kyz,
                                  kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 8, xfldElem.nDOF() );

  ElementXFieldTrace xquad(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xquad.order() );
  BOOST_CHECK_EQUAL( 4, xquad.nDOF() );

  // hexahdral grid element
  xfldElem.DOF(0) = {0, 0, 0};
  xfldElem.DOF(1) = {1, 0, 0};
  xfldElem.DOF(2) = {1, 1, 0};
  xfldElem.DOF(3) = {0, 1, 0};

  xfldElem.DOF(4) = {0, 0, 1};
  xfldElem.DOF(5) = {1, 0, 1};
  xfldElem.DOF(6) = {1, 1, 1};
  xfldElem.DOF(7) = {0, 1, 1};

  xquad.DOF(0) = {0, 0, 0};
  xquad.DOF(1) = {0, 1, 0};
  xquad.DOF(2) = {1, 1, 0};
  xquad.DOF(3) = {1, 0, 0};


  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 8, qfldElem.nDOF() );

  // hex solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  qfldElem.DOF(4) = 8;
  qfldElem.DOF(5) = 2;
  qfldElem.DOF(6) = 9;
  qfldElem.DOF(7) = 7;

  // Lagrange multiplier
  ElementQFieldTrace lquad(order, BasisFunctionCategory_Hierarchical);

  lquad.DOF(0) =  7;
  lquad.DOF(1) = -9;
  lquad.DOF(2) =  5;
  lquad.DOF(3) =  3;

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xquad, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, lquad );

  BOOST_CHECK_EQUAL( 8, fcn.nDOFCell() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOFTrace() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 2e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDE1, integrandPDE2, integrandPDE3, integrandPDE4;
  Real integrandPDE5, integrandPDE6, integrandPDE7, integrandPDE8;
  Real integrandBC1, integrandBC2, integrandBC3, integrandBC4;
  ArrayQ integrandPDE[8];
  ArrayQ integrandBC[4];

  sRef = {0, 0};
  fcn( sRef, integrandPDE, 8, integrandBC, 4 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (-1./2.) + (13749./1000.) + (25430571./1250000.  );
  integrandPDE2 = (0)      + (0)            + (-12378393./2500000. );
  integrandPDE3 = (0)      + (0)            + (0);
  integrandPDE4 = (0)      + (0)            + (-2945403./500000.   );

  integrandPDE5 = (0)      + (0)            + (-11877867./1250000. );
  integrandPDE6 = (0)      + (0)            + ( 0 );
  integrandPDE7 = (0)      + (0)            + ( 0 );
  integrandPDE8 = (0)      + (0)            + ( 0 );

  // BC residuals
  integrandBC1 = -14749./500.;
  integrandBC2 = 0;
  integrandBC3 = 0;
  integrandBC4 = 0;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDE5, integrandPDE[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE6, integrandPDE[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE7, integrandPDE[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE8, integrandPDE[7], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC3, integrandBC[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC4, integrandBC[3], small_tol, close_tol );


  sRef = {1, 0};
  fcn( sRef, integrandPDE, 8, integrandBC, 4 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (0)  + (0)           + (-9298269./500000. );
  integrandPDE2 = (0)  + (0)           + ( 0 );
  integrandPDE3 = (0)  + (0)           + ( 39077039./2500000. );
  integrandPDE4 = (-3) + (3773./1000.) + (-8447447./312500. );

  integrandPDE5 = (0)  + (0)            + ( 0 );
  integrandPDE6 = (0)  + (0)            + ( 0 );
  integrandPDE7 = (0)  + (0)            + ( 0 );
  integrandPDE8 = (0)  + (0)            + ( 37496941./1250000. );

  // BC residuals
  integrandBC1 = 0;
  integrandBC2 = -2273./500.;
  integrandBC3 = 0;
  integrandBC4 = 0;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDE5, integrandPDE[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE6, integrandPDE[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE7, integrandPDE[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE8, integrandPDE[7], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC3, integrandBC[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC4, integrandBC[3], small_tol, close_tol );


  sRef = {1, 1};
  fcn( sRef, integrandPDE, 8, integrandBC, 4 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (0)  + (0)           + ( 0 );
  integrandPDE2 = (0)  + (0)           + (1736703./500000. );
  integrandPDE3 = (-2) + (5649./1000.) + (-987537./1250000. );
  integrandPDE4 = (0)  + (0)           + ( 7298693./2500000. );

  integrandPDE5 = (0)  + (0)           + ( 0 );
  integrandPDE6 = (0)  + (0)           + ( 0 );
  integrandPDE7 = (0)  + (0)           + ( -7003567./1250000. );
  integrandPDE8 = (0)  + (0)           + ( 0 );

  // BC residuals
  integrandBC1 = 0;
  integrandBC2 = 0;
  integrandBC3 = -5149./500.;
  integrandBC4 = 0;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDE5, integrandPDE[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE6, integrandPDE[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE7, integrandPDE[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE8, integrandPDE[7], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC3, integrandBC[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC4, integrandBC[3], small_tol, close_tol );


  sRef = {0, 1};
  fcn( sRef, integrandPDE, 8, integrandBC, 4 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (0)      + (0)          + ( 5261669./2500000. );
  integrandPDE2 = (-3./2.) + (817./1000.) + ( 2774037./625000. );
  integrandPDE3 = (0)      + (0)          + (-1251999./500000. );
  integrandPDE4 = (0)      + (0)          + ( 0 );

  integrandPDE5 = (0)  + (0)           + ( 0 );
  integrandPDE6 = (0)  + (0)           + (-5048911./1250000. );
  integrandPDE7 = (0)  + (0)           + ( 0 );
  integrandPDE8 = (0)  + (0)           + ( 0 );

  // BC residuals
  integrandBC1 = 0;
  integrandBC2 = 0;
  integrandBC3 = 0;
  integrandBC4 = -817./500.;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDE5, integrandPDE[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE6, integrandPDE[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE7, integrandPDE[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE8, integrandPDE[7], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC3, integrandBC[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC4, integrandBC[3], small_tol, close_tol );


  sRef = {1./2., 1./2.};
  fcn( sRef, integrandPDE, 8, integrandBC, 4 );

  // PDE residuals: (inviscid) + (viscous) + (Lagrange)
  integrandPDE1 = (-7./16.) + (5997./4000.) + (-445257./200000. );
  integrandPDE2 = (-7./16.) + (5997./4000.) + (-4062283./5000000. );
  integrandPDE3 = (-7./16.) + (5997./4000.) + ( 4348127./5000000. );
  integrandPDE4 = (-7./16.) + (5997./4000.) + (-544203./1000000. );

  integrandPDE5 = (0)  + (0)            + ( 3391649./5000000. );
  integrandPDE6 = (0)  + (0)            + ( 3391649./5000000. );
  integrandPDE7 = (0)  + (0)            + ( 3391649./5000000. );
  integrandPDE8 = (0)  + (0)            + ( 3391649./5000000. );

  // BC residuals
  integrandBC1 = -5747./2000.;
  integrandBC2 = -5747./2000.;
  integrandBC3 = -5747./2000.;
  integrandBC4 = -5747./2000.;

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE3, integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE4, integrandPDE[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandPDE5, integrandPDE[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE6, integrandPDE[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE7, integrandPDE[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE8, integrandPDE[7], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandBC1, integrandBC[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC2, integrandBC[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC3, integrandBC[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandBC4, integrandBC[3], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  int nIntegrandR = lquad.nDOF();
  GalerkinWeightedIntegral<TopoD2, Quad, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[8] = {0,0,0,0, 0,0,0,0};
  ArrayQ rsdBCElemR[4] = {0,0,0,0};

  // cell integration for canonical element
  integral( fcn, xquad, rsdPDEElemL, nIntegrandL, rsdBCElemR, nIntegrandR );

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  Real rsd1PDE = (-13./36.) + (931./480.)   + (-78006083./45000000.);
  Real rsd2PDE = (-7./18.)  + (5093./4000.) + (12392939./45000000.);
  Real rsd3PDE = (-35./72.) + (607./480.)   + (9337397./9000000.);
  Real rsd4PDE = (-37./72.) + (3647./2400.) + (-20634641./9000000.);

  Real rsd5PDE = (0)        + (0)           + (415241./1800000.);
  Real rsd6PDE = (0)        + (0)           + (-20461571./45000000.);
  Real rsd7PDE = (0)        + (0)           + (1000157./1800000.);
  Real rsd8PDE = (0)        + (0)           + (21435197./9000000.);


  // BC residuals:
  Real rsd1BC = -2813./720.;
  Real rsd2BC = -9941./3600.;
  Real rsd3BC = -1661./720.;
  Real rsd4BC = -45337./18000.;

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3PDE, rsdPDEElemL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4PDE, rsdPDEElemL[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd5PDE, rsdPDEElemL[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd6PDE, rsdPDEElemL[5], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd7PDE, rsdPDEElemL[6], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd8PDE, rsdPDEElemL[7], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1BC, rsdBCElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2BC, rsdBCElemR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3BC, rsdBCElemR[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd4BC, rsdBCElemR[3], small_tol, close_tol );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
