// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianTranspose_CG_Triangle_AD_btest
// testing computing the transposed Jacobian of CG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_mitLG_Galerkin.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_mitLG_Galerkin.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianTranspose_CG_Triangle_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( X1Q1LG1 )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> SolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCAdvectionDiffusion<PhysD2,BCType> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;


  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC
  std::shared_ptr<SolutionExact> solnExact( new SolutionExact );

  BCClass bc( solnExact, visc );

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandBCClass;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandCellClass;

  // integrands
  IntegrandCellClass fcnCell( pde, {0} );
  IntegrandBCClass fcnBC( pde, bc, {0,1,2,3} );

  // grid: HierarchicalP1 (aka X1)

  int ii = 3;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );


  // solution: HierarchicalP1 (aka Q1)

  int order = 1;

  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  int nDOFPDE = qfld.nDOF();
  qfld = 0;

  // Lagrange multiplier: Legendre P1

#if 0
  order = 0;
  QField2D_DG_BoundaryEdge<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#elif 1
  order = 1;
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#else
  order = 1;
  QField2D_CG_BoundaryEdge_Independent<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#endif

  int nDOFBC = lgfld.nDOF();
  lgfld = 0;

#if 0
  cout << "btest: dumping lgfld" << endl;  lgfld.dump(2);
#endif

  // quadrature rule
  int quadratureOrder[4] = {-1, -1, -1, -1};    // max
  int quadratureOrderMin[4] = {0, 0, 0, 0};     // min

  // linear system setup

  typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
  typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;

  // jacobian nonzero pattern

  typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

  DLA::MatrixD<NonZeroPatternClass> nz =
      {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFBC} },
       { { nDOFBC, nDOFPDE}, { nDOFBC, nDOFBC} }};

  NonZeroPatternClass& nzPDE_q  = nz(0,0);
  NonZeroPatternClass& nzPDE_lg = nz(0,1);
  NonZeroPatternClass& nzBC_q   = nz(1,0);
  NonZeroPatternClass& nzBC_lg  = nz(1,1);

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnCell, nzPDE_q), xfld, qfld, quadratureOrderMin, 1 );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
     JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcnBC, nzPDE_q, nzPDE_lg, nzBC_q, nzBC_lg),
     xfld, qfld, lgfld, quadratureOrderMin, 4 );

  // jacobian

  SystemMatrixClass jac(nz);

  SparseMatrixClass& jacPDE_q  = jac(0,0);
  SparseMatrixClass& jacPDE_lg = jac(0,1);
  SparseMatrixClass& jacBC_q   = jac(1,0);
  SparseMatrixClass& jacBC_lg  = jac(1,1);

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnCell, jacPDE_q), xfld, qfld, quadratureOrder, 1 );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
     JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcnBC, jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg),
     xfld, qfld, lgfld, quadratureOrder, 4 );

  // jacobian transpose

  SystemMatrixClass jacT(nz);

  auto jacTPDE_q  = Transpose(jacT)(0,0);
  auto jacTPDE_lg = Transpose(jacT)(0,1);
  auto jacTBC_q   = Transpose(jacT)(1,0);
  auto jacTBC_lg  = Transpose(jacT)(1,1);

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnCell, jacTPDE_q), xfld, qfld, quadratureOrder, 1 );

  IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
     JacobianBoundaryTrace_mitLG_Galerkin<SurrealClass>(fcnBC, jacTPDE_q, jacTPDE_lg, jacTBC_q, jacTBC_lg),
     xfld, qfld, lgfld, quadratureOrder, 4 );

/*
  std::fstream jacfile("tmp/jacobian.mtx", std::ios::out);
  WriteMatrixMarketFile( jac, jacfile );

  std::fstream jacTfile("tmp/jacobianT.mtx", std::ios::out);
  WriteMatrixMarketFile( jacT, jacTfile );
*/

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;

  //Check that jacT is a transpose of jac
  for ( int i = 0; i < jac.m(); i++ )
    for ( int j = 0; j < jac.n(); j++ )
    {
      SparseMatrixClass& jacij = jac(i,j);
      SparseMatrixClass& jacTji = jacT(j,i);

      BOOST_REQUIRE_EQUAL(jacij.m(), jacTji.n());
      BOOST_REQUIRE_EQUAL(jacij.n(), jacTji.m());

      for ( int ib = 0; ib < jacij.m(); ib++ )
        for ( int jb = 0; jb < jacij.n(); jb++ )
        {
          //Only compare non-zero entries
          if ( !jacij.isNonZero(ib,jb) )
          {
            //Make sure the transpose is also a zero entry
            BOOST_CHECK( !jacTji.isNonZero(jb,ib) );
            continue;
          }
          //Check tha the numbers are the same
          SANS_CHECK_CLOSE( jacij(ib,jb), jacTji(jb,ib), small_tol, close_tol );
        }
    }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
