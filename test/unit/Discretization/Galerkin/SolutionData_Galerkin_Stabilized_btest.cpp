// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// SolutionData_Galerkin_Stabilized_btest
// testing of the SolutionData_Galerkin_Stabilized class

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "SANS_btest.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"


using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolutionData_Galerkin_Stabilized_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SolutionData_Galerkin_Stabilized_test )
{
  typedef AdvectiveFlux2D_Uniform AdvectiveFluxType;
  typedef ViscousFlux2D_Uniform ViscousFluxType;
  typedef Source2D_UniformGrad SourceType;

  typedef PDEAdvectionDiffusion<PhysD2, AdvectiveFluxType, ViscousFluxType, SourceType > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  int ii = 3;
  int jj = 3;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  int order = 1;

  // PDE
  Real a = 0.5;
  Real b = 1.0;
  Real nu = 0.0;
  AdvectiveFluxType adv( a, b );
  ViscousFluxType visc( nu, 0, 0, nu );
  SourceType source(0.0, 0.0, 0.0);

  NDPDEClass pde( adv, visc, source );

  StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, order+1);

  SolutionClass globalSol(xfld, pde, order, order+1,
                          BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical, {}, stab);

  BOOST_CHECK_EQUAL( &globalSol.pde, &pde);
  BOOST_CHECK_EQUAL( globalSol.primal.order, order);

  BOOST_CHECK_EQUAL( globalSol.primal.basis_cell, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( globalSol.primal.basis_trace, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( &globalSol.primal.qfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.primal.lgfld.getXField(), &xfld);

  BOOST_CHECK_EQUAL( &globalSol.adjoint.qfld.getXField(), &xfld);
  BOOST_CHECK_EQUAL( &globalSol.adjoint.lgfld.getXField(), &xfld);

  BOOST_CHECK_EQUAL( globalSol.primal.qfld.nDOF(), (ii+1)*(jj+1));
  BOOST_CHECK_EQUAL( globalSol.primal.lgfld.nDOF(), 0);

  BOOST_CHECK_EQUAL( globalSol.adjoint.qfld.nDOF(), 7*7 );
  BOOST_CHECK_EQUAL( globalSol.adjoint.lgfld.nDOF(), 0);

  globalSol.setSolution(0.2);

  for (int i = 0; i < globalSol.primal.qfld.nDOF(); i++)
    SANS_CHECK_CLOSE( globalSol.primal.qfld.DOF(i), 0.2, small_tol, close_tol);

  SolutionClass globalSolFrom(xfld, pde, order, order+1,
                              BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical, {}, stab );

  globalSolFrom.setSolution(0.5);

  globalSol.setSolution(globalSolFrom);

  for (int i = 0; i < globalSol.primal.qfld.nDOF(); i++)
    SANS_CHECK_CLOSE( globalSol.primal.qfld.DOF(i), 0.5, small_tol, close_tol);

  //Create a lifted quantity field
  globalSol.createLiftedQuantityField(1, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( &globalSol.pliftedQuantityfld->getXField(), &xfld);
  BOOST_CHECK_EQUAL( globalSol.pliftedQuantityfld->nDOF(), (ii+1)*(jj+1));
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
