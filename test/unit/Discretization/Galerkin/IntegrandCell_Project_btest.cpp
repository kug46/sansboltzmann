// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Project_btest
// testing of cell element residual integrands for Galerkin: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandCell_Project.h"

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

//#include "pde/NDConvert/FunctionNDConvertSpace1D.h"
//#include "pde/NDConvert/FunctionNDConvertSpace2D.h"

#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"
//#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
//#include "Field/Element/ElementXFieldVolume.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{

typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef PDEClass1D::template ArrayQ<Real> ArrayQ1D;
typedef PDEClass1D::template MatrixQ<Real> MatrixQ1D;
typedef Element<ArrayQ1D,TopoD1,Line> ElementQFieldClass1D;
typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass_Line1D;

typedef MakeTuple< ElementTuple, ElementQFieldClass1D,
                                 ElementXFieldClass_Line1D >::type ElementParam1D;

typedef IntegrandCell_Project< PDEClass1D > IntegrandClass1D;
typedef IntegrandClass1D::BasisWeighted<TopoD1,Line,ElementParam1D> BasisWeightedClass1D;
typedef ElementXFieldClass_Line1D::RefCoordType RefCoordType1D;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None> PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
typedef PDEClass2D::template ArrayQ<Real> ArrayQ2D;
typedef PDEClass2D::template MatrixQ<Real> MatrixQ2D;
typedef Element<ArrayQ2D,TopoD2,Triangle> ElementQFieldClass2D;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass_Triangle2D;

typedef MakeTuple< ElementTuple, ElementQFieldClass2D,
                                 ElementXFieldClass_Triangle2D >::type ElementParam2D;

typedef IntegrandCell_Project< PDEClass2D > IntegrandClass2D;
typedef IntegrandClass2D::BasisWeighted<TopoD2,Triangle,ElementParam2D> BasisWeightedClass2D;
typedef ElementXFieldClass_Triangle2D::RefCoordType RefCoordType2D;

}

using namespace SANS;



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Project_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_1D_Line_P2P2_test )
{


  int order = 1;
  ElementXFieldClass_Line1D xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  order = 2;
  ElementQFieldClass1D qFromfldElem(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass1D qTofldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qFromfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qFromfldElem.nDOF() );

  // solution
  qFromfldElem.DOF(0) = 2;
  qFromfldElem.DOF(1) = 3;
  qFromfldElem.DOF(2) = 4;

  qTofldElem.DOF(0) = 5;
  qTofldElem.DOF(1) = 6;
  qTofldElem.DOF(2) = 7;

  // integrand
  IntegrandClass1D fcnint( {0} );

  ElementParam1D tupleElem = (qFromfldElem, xfldElem);
  BasisWeightedClass1D fcn = fcnint.integrand( tupleElem, qTofldElem );

  BOOST_CHECK_EQUAL( qTofldElem.nDOF(), fcn.nDOF() );

  // basis value
  std::vector<Real> phi(qTofldElem.nDOF());

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  std::vector<RefCoordType1D> sRef = { {0}, {1}, {1/2}, {1/4} };
  Real integrandTrue;
  const int neqn = 3;
  ArrayQ1D integrand[neqn];
  ArrayQ1D qTo, qFrom;

  // Test at {0}
  for (std::size_t i = 0; i < sRef.size(); i++)
  {
    fcn( sRef[i], integrand, neqn );

    //residual integrands:
    qTofldElem.evalBasis( sRef[i], phi.data(), phi.size() );
    qTofldElem.evalFromBasis( phi.data(), phi.size(), qTo );
    qFromfldElem.eval( sRef[i], qFrom);

    for (int j = 0; j < neqn; j++)
    {
      integrandTrue = phi[j]*(qFrom - qTo);
      SANS_CHECK_CLOSE( integrandTrue, integrand[j], small_tol, close_tol );
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_1D_Line_P2P1_test )
{
  int order = 1;
  ElementXFieldClass_Line1D xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  order = 2;
  ElementQFieldClass1D qFromfldElem(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass1D qTofldElem(order-1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qFromfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qFromfldElem.nDOF() );

  // solution
  qFromfldElem.DOF(0) = 3;
  qFromfldElem.DOF(1) = 2;
  qFromfldElem.DOF(2) = 1;

  qTofldElem.DOF(0) = 4;
  qTofldElem.DOF(1) = 5;

  // integrand
  IntegrandClass1D fcnint( {0} );

  ElementParam1D tupleElem = (qFromfldElem, xfldElem);
  BasisWeightedClass1D fcn = fcnint.integrand( tupleElem, qTofldElem );

  BOOST_CHECK_EQUAL( qTofldElem.nDOF(), fcn.nDOF() );

  // basis value
  std::vector<Real> phi(qTofldElem.nDOF());

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  std::vector<RefCoordType1D> sRef = { {0}, {1}, {1/2}, {1/4} };
  Real integrandTrue;
  const int neqn = 2;
  ArrayQ1D integrand[neqn];
  ArrayQ1D qTo, qFrom;

  // Test at {0}
  for (std::size_t i = 0; i < sRef.size(); i++)
  {
    fcn( sRef[i], integrand, neqn );

    //residual integrands:
    qTofldElem.evalBasis( sRef[i], phi.data(), phi.size() );
    qTofldElem.evalFromBasis( phi.data(), phi.size(), qTo );
    qFromfldElem.eval( sRef[i], qFrom);

    for (int j = 0; j < neqn; j++)
    {
      integrandTrue = phi[j]*(qFrom - qTo);
      SANS_CHECK_CLOSE( integrandTrue, integrand[j], small_tol, close_tol );
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_1D_Line_P2P2_Jacobian_test )
{
  int order = 1;
  ElementXFieldClass_Line1D xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  // integrand
  IntegrandClass1D fcnint( {0} );

  for (int qorder = 1; qorder <= 4; qorder++)
  {
    ElementQFieldClass1D qTofldElem  (qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass1D qFromfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qTofldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qTofldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qTofldElem.nDOF();dof++)
      qTofldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    BOOST_CHECK_EQUAL( qorder, qFromfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qFromfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qFromfldElem.nDOF();dof++)
      qFromfldElem.DOF(dof) = -(dof+1)*pow(-1,dof+1);

    ElementParam1D tupleElem = (qFromfldElem, xfldElem);
    BasisWeightedClass1D fcn = fcnint.integrand( tupleElem, qTofldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;
    RefCoordType1D sRef = Line::centerRef;
    DLA::VectorD<ArrayQ1D> integrand0(qTofldElem.nDOF()), integrand1(qTofldElem.nDOF());
    DLA::MatrixD<MatrixQ1D> mtxMassTrue(qTofldElem.nDOF(), qTofldElem.nDOF());
    DLA::MatrixD<MatrixQ1D> mtxMass(qTofldElem.nDOF(), qTofldElem.nDOF());

    // compute jacobians via finite differenec (exact for linear PDE)
    fcn(sRef, &integrand0[0], integrand0.m());

    // The ordering is reversed because project system RHS
    // is (qFrom - qTo), so it returns the solution in one shot.
    for (int i = 0; i < qTofldElem.nDOF(); i++)
    {
      qTofldElem.DOF(i) += 1;
      fcn(sRef, &integrand1[0], integrand1.m());
      qTofldElem.DOF(i) -= 1;

      for (int j = 0; j < qTofldElem.nDOF(); j++)
        mtxMassTrue(j,i) = integrand0[j] - integrand1[j];
    }

    mtxMass = 0;

    // accumulate the jacobian via Surreal
    fcn(1., sRef, mtxMass);

    for (int i = 0; i < qTofldElem.nDOF(); i++)
      for (int j = 0; j < qTofldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxMassTrue(i,j), mtxMass(i,j), small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_1D_Line_P2P1_Jacobian_test )
{
  int order = 1;
  ElementXFieldClass_Line1D xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  // integrand
  IntegrandClass1D fcnint( {0} );

  for (int qorder = 2; qorder <= 5; qorder++)
  {
    ElementQFieldClass1D qTofldElem  (qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass1D qFromfldElem(qorder-1, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qTofldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qTofldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qTofldElem.nDOF();dof++)
      qTofldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    BOOST_CHECK_EQUAL( qorder-1, qFromfldElem.order() );
    BOOST_CHECK_EQUAL( qorder, qFromfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qFromfldElem.nDOF();dof++)
      qFromfldElem.DOF(dof) = -(dof+1)*pow(-1,dof+1);

    ElementParam1D tupleElem = (qFromfldElem, xfldElem);
    BasisWeightedClass1D fcn = fcnint.integrand( tupleElem, qTofldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;
    RefCoordType1D sRef = Line::centerRef;
    DLA::VectorD<ArrayQ1D> integrand0(qTofldElem.nDOF()), integrand1(qTofldElem.nDOF());
    DLA::MatrixD<MatrixQ1D> mtxMassTrue(qTofldElem.nDOF(), qTofldElem.nDOF());
    DLA::MatrixD<MatrixQ1D> mtxMass(qTofldElem.nDOF(), qTofldElem.nDOF());

    // compute jacobians via finite differenec (exact for linear PDE)
    fcn(sRef, &integrand0[0], integrand0.m());

    for (int i = 0; i < qTofldElem.nDOF(); i++)
    {
      qTofldElem.DOF(i) += 1;
      fcn(sRef, &integrand1[0], integrand1.m());
      qTofldElem.DOF(i) -= 1;

      for (int j = 0; j < qTofldElem.nDOF(); j++)
        mtxMassTrue(j,i) = integrand0[j] - integrand1[j];
    }

    mtxMass = 0;

    // accumulate the jacobian via Surreal
    fcn(1., sRef, mtxMass);

    for (int i = 0; i < qTofldElem.nDOF(); i++)
      for (int j = 0; j < qTofldElem.nDOF(); j++)
        SANS_CHECK_CLOSE( mtxMassTrue(i,j), mtxMass(i,j), small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_P2P2_test )
{
  int order = 1;
  ElementXFieldClass_Triangle2D xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  order++;
  ElementQFieldClass2D qFromfldElem(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass2D qTofldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qFromfldElem.order() );
  BOOST_CHECK_EQUAL( 6, qFromfldElem.nDOF() );

  // triangle solution
  for (int i = 0; i < qFromfldElem.nDOF(); i++)
    qFromfldElem.DOF(i) = i+1;

  BOOST_CHECK_EQUAL( 2, qTofldElem.order() );
  BOOST_CHECK_EQUAL( 6, qTofldElem.nDOF() );

  // triangle solution
  for (int i = 0; i < qTofldElem.nDOF(); i++)
    qTofldElem.DOF(i) = i+3;

  // integrand
  IntegrandClass2D fcnint( {0} );

  ElementParam2D tupleElem = (qFromfldElem, xfldElem);
  BasisWeightedClass2D fcn = fcnint.integrand( tupleElem, qTofldElem );

  BOOST_CHECK_EQUAL( qTofldElem.nDOF(), fcn.nDOF() );

  // basis value
  std::vector<Real> phi(qTofldElem.nDOF());

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  std::vector<RefCoordType2D> sRef = { {0,0}, {1,0}, {0,1}, {1/2,1/2} };
  Real integrandTrue;
  const int neqn = 6;
  ArrayQ2D integrand[neqn];
  ArrayQ2D qTo, qFrom;

  for (std::size_t i = 0; i < sRef.size(); i++)
  {
    fcn( sRef[i], integrand, neqn );

    //residual integrands:
    qTofldElem.evalBasis( sRef[i], phi.data(), phi.size() );
    qTofldElem.evalFromBasis( phi.data(), phi.size(), qTo );
    qFromfldElem.eval( sRef[i], qFrom);

    for (int j = 0; j < neqn; j++)
    {
      integrandTrue = phi[j]*(qFrom - qTo);
      SANS_CHECK_CLOSE( integrandTrue, integrand[j], small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_P2P1_test )
{

  int order = 1;
  ElementXFieldClass_Triangle2D xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  order++;
  ElementQFieldClass2D qFromfldElem(order, BasisFunctionCategory_Hierarchical);
  order--;
  ElementQFieldClass2D qTofldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qFromfldElem.order() );
  BOOST_CHECK_EQUAL( 6, qFromfldElem.nDOF() );

  // triangle solution
  for (int i = 0; i < qFromfldElem.nDOF(); i++)
    qFromfldElem.DOF(i) = i+1;

  BOOST_CHECK_EQUAL( 1, qTofldElem.order() );
  BOOST_CHECK_EQUAL( 3, qTofldElem.nDOF() );

  // triangle solution
  for (int i = 0; i < qTofldElem.nDOF(); i++)
    qTofldElem.DOF(i) = i+3;

  // integrand
  IntegrandClass2D fcnint( {0} );

  ElementParam2D tupleElem = (qFromfldElem, xfldElem);
  BasisWeightedClass2D fcn = fcnint.integrand( tupleElem, qTofldElem );

  BOOST_CHECK_EQUAL( qTofldElem.nDOF(), fcn.nDOF() );

  // basis value
  std::vector<Real> phi(qTofldElem.nDOF());

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  std::vector<RefCoordType2D> sRef = { {0,0}, {1,0}, {0,1}, {1/2,1/2} };
  Real integrandTrue;
  const int neqn = 3;
  ArrayQ2D integrand[neqn];
  ArrayQ2D qTo, qFrom;

  for (std::size_t i = 0; i < sRef.size(); i++)
  {
    fcn( sRef[i], integrand, neqn );

    //residual integrands:
    qTofldElem.evalBasis( sRef[i], phi.data(), phi.size() );
    qTofldElem.evalFromBasis( phi.data(), phi.size(), qTo );
    qFromfldElem.eval( sRef[i], qFrom);

    for (int j = 0; j < neqn; j++)
    {
      integrandTrue = phi[j]*(qFrom - qTo);
      SANS_CHECK_CLOSE( integrandTrue, integrand[j], small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_P2P2_Jacobian_test )
{
  int order = 1;
  ElementXFieldClass_Triangle2D xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0.1;  y1 = -0.2;
  x2 = 1.0;  y2 =  0.1;
  x3 = 0.0;  y3 =  1.1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // integrand
  IntegrandClass2D fcnint( {0} );

  for (int qorder = 1; qorder <= 4; qorder++)
  {
    ElementQFieldClass2D qTofldElem  (qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass2D qFromfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qTofldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qTofldElem.nDOF() );

    const int nDOF = qTofldElem.nDOF();

    // line solution
    for (int dof = 0; dof < qTofldElem.nDOF();dof++)
      qTofldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    BOOST_CHECK_EQUAL( qorder, qFromfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qFromfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qFromfldElem.nDOF();dof++)
      qFromfldElem.DOF(dof) = -(dof+1)*pow(-1,dof+1);

    ElementParam2D tupleElem = (qFromfldElem, xfldElem);
    BasisWeightedClass2D fcn = fcnint.integrand( tupleElem, qTofldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;
    RefCoordType2D sRef = Triangle::centerRef;
    DLA::VectorD<ArrayQ2D> integrand0(nDOF), integrand1(nDOF);
    DLA::MatrixD<MatrixQ2D> mtxMassTrue(nDOF, nDOF);
    DLA::MatrixD<MatrixQ2D> mtxMass(nDOF, nDOF);

    // compute jacobians via finite differenec (exact for linear PDE)
    fcn(sRef, &integrand0[0], integrand0.m());

    for (int i = 0; i < nDOF; i++)
    {
      qTofldElem.DOF(i) += 1;
      fcn(sRef, &integrand1[0], integrand1.m());
      qTofldElem.DOF(i) -= 1;

      for (int j = 0; j < nDOF; j++)
        mtxMassTrue(j,i) = integrand0[j] - integrand1[j];
    }

    mtxMass = 0;

    // accumulate the jacobian via Surreal
    fcn(1., sRef, mtxMass);

    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        SANS_CHECK_CLOSE( mtxMassTrue(i,j), mtxMass(i,j), small_tol, close_tol );
  }
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_P2P1_Jacobian_test )
{
  int order = 1;
  ElementXFieldClass_Triangle2D xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0.1;  y1 = -0.2;
  x2 = 1.0;  y2 =  0.1;
  x3 = 0.0;  y3 =  1.1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // integrand
  IntegrandClass2D fcnint( {0} );

  for (int qorder = 2; qorder <= 5; qorder++)
  {
    ElementQFieldClass2D qTofldElem  (qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldClass2D qFromfldElem(qorder-1, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qTofldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qTofldElem.nDOF() );

    const int nDOF = qTofldElem.nDOF();

    // line solution
    for (int dof = 0; dof < qTofldElem.nDOF();dof++)
      qTofldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    BOOST_CHECK_EQUAL( qorder-1, qFromfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder)*(qorder+1)/2, qFromfldElem.nDOF() );

    // line solution
    for (int dof = 0; dof < qFromfldElem.nDOF();dof++)
      qFromfldElem.DOF(dof) = -(dof+1)*pow(-1,dof+1);

    ElementParam2D tupleElem = (qFromfldElem, xfldElem);
    BasisWeightedClass2D fcn = fcnint.integrand( tupleElem, qTofldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;
    RefCoordType2D sRef = Triangle::centerRef;
    DLA::VectorD<ArrayQ2D> integrand0(nDOF), integrand1(nDOF);
    DLA::MatrixD<MatrixQ2D> mtxMassTrue(nDOF, nDOF);
    DLA::MatrixD<MatrixQ2D> mtxMass(nDOF, nDOF);

    // compute jacobians via finite differenec (exact for linear PDE)
    fcn(sRef, &integrand0[0], integrand0.m());

    for (int i = 0; i < nDOF; i++)
    {
      qTofldElem.DOF(i) += 1;
      fcn(sRef, &integrand1[0], integrand1.m());
      qTofldElem.DOF(i) -= 1;

      for (int j = 0; j < nDOF; j++)
        mtxMassTrue(j,i) = integrand0[j] - integrand1[j];
    }

    mtxMass = 0;

    // accumulate the jacobian via Surreal
    fcn(1., sRef, mtxMass);

    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        SANS_CHECK_CLOSE( mtxMassTrue(i,j), mtxMass(i,j), small_tol, close_tol );
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
