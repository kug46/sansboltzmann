// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_AD_btest
// testing of cell element residual integrands for Galerkin: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "Field/Element/GalerkinWeightedIntegral.h" // Basis Weighted
#include "Field/Element/ElementIntegral.h" // Field Weighted

#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_Output_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_Output_1D_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEClass;
//  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Const> SolutionExact;

  PyDict solnArgs;
  solnArgs[SolutionExact::ParamsType::params.a0] = 1.0;

  SolutionExact solnExact( solnArgs );

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;
  typedef ErrorIntegrandClass::Functor<Real,TopoD1,Line, ElementXField<PhysD1,TopoD1, Line> > FunctorClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  // line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = x1;
  xfldElem.DOF(1) = x2;

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // integrands
  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});
  FunctorClass fcn = errorIntegrand.integrand(xfldElem, qfldElem);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandTrue;
  ArrayQ integrand;

  // Test at {0}
  sRef = {0};
  fcn( sRef, integrand );


  //PDE residual integrands:
  integrandTrue = 1.0;
  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // Test at {1}
  sRef = {1};
  fcn( sRef, integrand );

  //PDE residual integrands:
  integrandTrue = 4.0;

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // Test at {1/2}
  sRef = {1./2.};
  fcn( sRef, integrand );
  //PDE residual integrands:
  integrandTrue = 2.25;

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );


//  // test the element integral of the functor
//
//  int quadratureorder = 1;
//  int nIntegrand = qfldElem.nDOF();
//  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrand);
//
//  ArrayQ rsdPDEElem[2] = {0,0};
//  Real rsd[2];
//
//  // cell integration for canonical element
//  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );
//
//  //PDE residual:
//  rsd[0] = ( 11./4. ) + ( -2123./1000. ) ;   // Basis function 1
//  rsd[1] = ( -11./4. ) + ( 2123./1000. ) ;   // Basis function 2
//
//  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None> PDEClass;
//  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Const> SolutionExact;

  PyDict solnArgs;
  solnArgs[SolutionExact::ParamsType::params.a0] = 1.0;

  SolutionExact solnExact( solnArgs );

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;
  typedef ErrorIntegrandClass::Functor<Real,TopoD2,Triangle, ElementXField<PhysD2,TopoD2, Triangle> > FunctorClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // integrands
  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});
  FunctorClass fcn = errorIntegrand.integrand(xfldElem, qfldElem);

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordType sRef;
  Real integrandTrue;
  ArrayQ integrand;

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand );

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = 0.0;   // Basis function 1

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand);

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = 4.0;   // Basis function 1

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand);

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = 9.0;   // Basis function 1

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // Test at {1/3, 1/3}
  sRef = {1./3., 1./3.};
  fcn( sRef, integrand);

  //PDE residual integrands: (advective) + (viscous)
  integrandTrue = 25./9.;   // Basis function 1

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // test the element integral of the functor

//  int quadratureorder = 1;
//  int nIntegrand = qfldElem.nDOF();
//  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);
//
//  ArrayQ rsdPDEElem[3] = {0,0,0};
//  Real rsd[3];
//  // cell integration for canonical element
//  integral( fcn, xfldElem, rsdPDEElem, nIntegrand );
//
//  //PDE residual: (advection) + (diffusion)
//  rsd[0] = (26./15.) + (-4941./1000.);   // Basis function 1
//  rsd[1] = (-22./15.) + (1181./400.);   // Basis function 2
//  rsd[2] = (-4./15.) + (3977./2000.);   // Basis function 3
//
//  SANS_CHECK_CLOSE( rsd[0], rsdPDEElem[0], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsd[1], rsdPDEElem[1], small_tol, close_tol );
//  SANS_CHECK_CLOSE( rsd[2], rsdPDEElem[2], small_tol, close_tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
