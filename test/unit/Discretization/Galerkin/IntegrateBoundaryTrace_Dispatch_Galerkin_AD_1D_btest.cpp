// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_Dispatch_AD_1D_btest
//
// test of 1-D advection diffusion BC IntegrateBoundaryTrace_Dispatch

#include <boost/mpl/size.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/equal.hpp>
#include <boost/mpl/copy_if.hpp>
#include <boost/mpl/contains.hpp>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/call_with_derived.h"
#include "tools/add_decorator.h"

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"

#include "Discretization/Galerkin/ResidualBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

using namespace std;

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_Dispatch_AD_1D_test_suite )

// Define a BCVector here for testing
typedef boost::mpl::vector3< BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N>,
                             BCAdvectionDiffusion<PhysD1, BCTypeLinearRobin_mitLG>,
                             BCAdvectionDiffusion<PhysD1, BCTypeLinearRobin_sansLG>
                           > BCVectorTest;

// Check that the correct BC is extracted with the Category
static_assert(boost::mpl::equal<BCVectorByCategory<BCVectorTest, BCCategory::None>::type,
              NDVectorCategory<boost::mpl::vector1<
                BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N>
                >, BCCategory::None> >::value, "These should match");

// Create a vector of BCVectors to tests the meta function
typedef GroupBCVectorByCategory<BCVectorTest>::type BCVectors;

static_assert( boost::mpl::size<BCVectors>::value == 3, "Shold be 3 vectors in the vector" );

typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1,  PDEAdvectionDiffusion1D > NDPDEClass;

// Create the boundary trace dispatch for the Galerkin integrand
typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvertSpace, BCVectorTest, Galerkin> IntegrateBoundaryTrace_DispatchClass;

// Check that some expected types are created properly by the dispatch class

// Seprated BC vector
static_assert( boost::mpl::equal< IntegrateBoundaryTrace_DispatchClass::BCVectormitFT,
                                  boost::mpl::vector< BCAdvectionDiffusion<PhysD1, BCTypeLinearRobin_mitLG> >
                                >::value, "These should match" );

static_assert( boost::mpl::equal< IntegrateBoundaryTrace_DispatchClass::BCVectorsansFT,
                                  boost::mpl::vector< BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N>,
                                                      BCAdvectionDiffusion<PhysD1, BCTypeLinearRobin_sansLG> >
                                >::value, "These should match" );

// Decorated BC vectors
static_assert( boost::mpl::equal< IntegrateBoundaryTrace_DispatchClass::BCNDVectormitFT,
                                  boost::mpl::vector< BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1, BCTypeLinearRobin_mitLG> > >
                                >::value, "These should match" );

static_assert( boost::mpl::equal< IntegrateBoundaryTrace_DispatchClass::BCNDVectorsansFT,
                                  boost::mpl::vector< BCNDConvertSpace<PhysD1, BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N> >,
                                                      BCNDConvertSpace<PhysD1, BCAdvectionDiffusion<PhysD1, BCTypeLinearRobin_sansLG> > >
                                >::value, "These should match" );

// Manually construct all the vectors.
// Must use push_back here so boost::mpl::equal works
typedef BCNDConvertSpace<PhysD1,BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_mitLG>> NDBCLinRobin_mitLG;
typedef NDVectorCategory<boost::mpl::push_back<boost::mpl::vector0<>, NDBCLinRobin_mitLG>::type,
                         BCCategory::LinearScalar_mitLG> NDBCVector_LinScalar_mitLG;
typedef boost::mpl::push_back<boost::mpl::vector0<>,NDBCVector_LinScalar_mitLG>::type BCNDVectorsmitFTcat;

typedef BCNDConvertSpace<PhysD1,BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N>> NDBCNone;
typedef NDVectorCategory<boost::mpl::push_back<boost::mpl::vector0<>, NDBCNone>::type,
                         BCCategory::None> NDBCVector_None;

typedef BCNDConvertSpace<PhysD1,BCAdvectionDiffusion<PhysD1,BCTypeLinearRobin_sansLG>> NDBCLinRobin_sansLG;
typedef NDVectorCategory<boost::mpl::push_back<boost::mpl::vector0<>, NDBCLinRobin_sansLG>::type,
                         BCCategory::LinearScalar_sansLG> NDBCVector_LinScalar_sansLG;

typedef boost::mpl::push_back<boost::mpl::vector0<>,NDBCVector_LinScalar_sansLG>::type BCNDVectorssansFTcat0;
typedef boost::mpl::push_back<BCNDVectorssansFTcat0,NDBCVector_None>::type BCNDVectorssansFTcat;

typedef typename boost::mpl::transform< BCNDVectorssansFTcat,
    IntegrateBoundaryTrace_DispatchClass::def_IntegrandBoundaryTraceOp >::type IntegrandBoundaryTracesansFTVector;

// BC integrands
static_assert( boost::mpl::equal< IntegrateBoundaryTrace_DispatchClass::BCNDVectorsmitFTcat,
                                  BCNDVectorsmitFTcat
                                >::value, "These should match" );

static_assert( boost::mpl::equal< IntegrateBoundaryTrace_DispatchClass::IntegrandBoundaryTracemitFTVector,
                                  boost::mpl::push_back<boost::mpl::vector0<>,
  IntegrandBoundaryTrace<NDPDEClass, NDBCVector_LinScalar_mitLG, Galerkin> >::type
                                >::value, "These should match" );


static_assert( boost::mpl::equal< IntegrateBoundaryTrace_DispatchClass::BCNDVectorssansFTcat,
                                  BCNDVectorssansFTcat
                                >::value, "These should match" );

static_assert( boost::mpl::equal< IntegrateBoundaryTrace_DispatchClass::IntegrandBoundaryTracesansFTVector,
                                  IntegrandBoundaryTracesansFTVector
                                >::value, "These should match" );

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( None )
{
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef BCParameters<BCVectorTest> BCParams;

#if 0 // FOR_DEBUGGING
#if 0
  std::cout << "BCVectorByCategory<BCVectorTest, BCCategory::None>::type" << std::endl;
  std::cout << demangle(typeid(BCVectorByCategory<BCVectorTest, BCCategory::None>::type).name()) << std::endl;

  std::cout << "BCVectorHasCategory<BCVectorTest>::apply<BCCategory::None>::type" << std::endl;
  std::cout << demangle(typeid(BCVectorHasCategory<BCVectorTest>::apply<BCCategory::None>::type).name()) << std::endl;

  std::cout << "boost::mpl::size<BCVectors>::value = " << boost::mpl::size<BCVectors>::value << std::endl;
  std::cout << demangle(typeid(BCVectors).name()).c_str() << std::endl;

  std::cout << "boost::mpl::begin<BCVectors>" << std::endl;
  std::cout << demangle(typeid(boost::mpl::deref<boost::mpl::begin<BCVectors>>::type).name()) << std::endl;
#endif
#if 1
  std::cout << "BCNDVectorssansFTcat" << std::endl;
  std::cout << demangle(typeid( BCNDVectorssansFTcat ).name()) << std::endl;
#endif
  std::cout << "IntegrateBoundaryTrace_DispatchClass::BCNDVectorssansFTcat" << std::endl;
  std::cout << demangle(typeid( IntegrateBoundaryTrace_DispatchClass::BCNDVectorssansFTcat).name()) << std::endl;

#if 0
  std::cout << "IntegrateBoundaryTrace_DispatchClass::IntegrandBoundaryTracesansFTVector" << std::endl;
  std::cout << demangle(typeid( IntegrateBoundaryTrace_DispatchClass::IntegrandBoundaryTracesansFTVector).name()) << std::endl;

  std::cout << demangle(typeid( boost::mpl::vector1<
    IntegrandBoundaryTrace<NDPDEClass, NDBCVector_LinScalar_sansLG, Galerkin> >).name()) << std::endl;


  std::cout << "IntegrateBoundaryTrace_DispatchClass::IntegrandBoundaryTracemitFTVector" << std::endl;
  std::cout << demangle(typeid( IntegrateBoundaryTrace_DispatchClass::IntegrandBoundaryTracemitFTVector).name()) << std::endl;
#endif
#endif

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  NDPDEClass pde( adv, visc, source );

  // Create a BC dictionary
  PyDict BC0;
  BC0[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  // Create a list of BC dictionaries where the key is the 'name' of the BC
  PyDict PyBCList;
  PyBCList["BC0"] = BC0;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BC0"] = {1};

  // Create a vector of BC types
  std::map< std::string, std::shared_ptr<BCBase> > BCs = BCParams::createBCs<BCNDConvertSpace>(pde, PyBCList);

  // Create a vector of IntegrandBoundaryTrace types
  int qorder = 1;
  StabilizationNitsche stab(qorder);
  IntegrateBoundaryTrace_DispatchClass BCdispatch(pde, PyBCList, BCs, BCBoundaryGroups, stab);

  // grid: P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // line solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, 0, BasisFunctionCategory_Legendre);

  // Lagrange multiplier DOF data
  lgfld.DOF(0) =  5;
  lgfld.DOF(1) = -7;

  // quadrature rule
  int quadratureorder[2] = {0,0};

  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  Real rsd1PDE = (0)          + (0)            + (0.);
  Real rsd2PDE = (33./10.)    + (-2123./1000.)*0 + (0.);

  // PDE/BC global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(2);
  SLA::SparseVector<ArrayQ> rsdBCGlobal(2);

  rsdPDEGlobal = 0;
  rsdBCGlobal = 0;

  // Compute the BC residuals using the dispatch class
  BCdispatch.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin( xfld, qfld, lgfld, quadratureorder, 2, rsdPDEGlobal, rsdBCGlobal ),
      ResidualBoundaryTrace_Dispatch_Galerkin( xfld, qfld, quadratureorder, 2, rsdPDEGlobal ) );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEGlobal[1], small_tol, close_tol );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
