// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
  // ResidualCell_Galerkin_CR_btest
// testing of cell residual functions for Galerkin with Cauchy-Riemann

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/CauchyRiemann/PDECauchyRiemann2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/IntegrateCellGroups.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_Galerkin_CR_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_2D_Galerkin_1Triangle_X1Q1 )
{
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  PDEClass pde;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = {1, 2};
  qfld.DOF(1) = {3, 4};
  qfld.DOF(2) = {5, 6};

  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 1;

  Real rsd1[2] = { 3.5, -0.5};
  Real rsd2[2] = {-1.5,  2.0};
  Real rsd3[2] = {-2.0, -1.5};

  const Real tol = 1e-13;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(3);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  rsdPDEGlobal = 0;

  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdPDEGlobal), xfld, qfld, &quadratureOrder, 1 );

  for (int k = 0; k < 2; k++)
  {
    BOOST_CHECK_CLOSE( rsd1[k], rsdPDEGlobal[0][k], tol );
    BOOST_CHECK_CLOSE( rsd2[k], rsdPDEGlobal[1][k], tol );
    BOOST_CHECK_CLOSE( rsd3[k], rsdPDEGlobal[2][k], tol );
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_2D_Galerkin_1Triangle_X1Q2 )
{
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P2 (aka Q2)
  int qorder = 2;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution field variable
  BOOST_CHECK_EQUAL( 6, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  int nodeMap[3] = {0};
  int edgeMap[3] = {0};
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle>& qfldCells = qfld.getCellGroup<Triangle>(0);
  qfldCells.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );
  BOOST_CHECK_EQUAL( 5, nodeMap[2] );

  qfldCells.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );
  BOOST_CHECK_EQUAL( 2, edgeMap[1] );
  BOOST_CHECK_EQUAL( 0, edgeMap[2] );

  // solution data
  qfld.DOF(nodeMap[0]) =  1;
  qfld.DOF(nodeMap[1]) =  3;
  qfld.DOF(nodeMap[2]) =  4;
  qfld.DOF(edgeMap[0]) = -7;
  qfld.DOF(edgeMap[1]) =  9;
  qfld.DOF(edgeMap[2]) = -5;

  // quadrature rule: basis grad is linear, flux is quadratic)
  int quadratureOrder = 3;

  // expected residuals: (advective) + (viscous)
  Real rsd1 = (1)         + (3201./125.);
  Real rsd2 = (-5./6.)    + (-122581./6000.);
  Real rsd3 = (-1./6.)    + (-31067./6000.);
  Real rsd4 = (-11./6.)   + (-37049./750.);
  Real rsd5 = (287./150.) + (92323./1500.);
  Real rsd6 = (-11./6.)   + (-9163./1500.);

  const Real tol = 1e-13;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(6);

  rsdPDEGlobal = 0;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  ResidualCell_Galerkin<TopoD2>::integrate( fcnint, xfld, qfld, &quadratureOrder, 1, rsdPDEGlobal );

  BOOST_CHECK_CLOSE( rsd1, rsdPDEGlobal[nodeMap[0]], tol );
  BOOST_CHECK_CLOSE( rsd2, rsdPDEGlobal[nodeMap[1]], tol );
  BOOST_CHECK_CLOSE( rsd3, rsdPDEGlobal[nodeMap[2]], tol );
  BOOST_CHECK_CLOSE( rsd4, rsdPDEGlobal[edgeMap[0]], tol );
  BOOST_CHECK_CLOSE( rsd5, rsdPDEGlobal[edgeMap[1]], tol );
  BOOST_CHECK_CLOSE( rsd6, rsdPDEGlobal[edgeMap[2]], tol );

}
#endif
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ResidualCell_2D_Galerkin_1Quad_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDECauchyRiemann2D;
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Quad_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;
  qfld.DOF(3) = 6;

  BOOST_CHECK_EQUAL( 4, qfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld.nElem() );

  // quadrature rule (quadratic: basis grad is linear, flux is linear)
  int quadratureOrder = 2;

  Real rsd1 = ( 37./20.) + (-203./60.);   // (advective) + (viscous)
  Real rsd2 = (-23./20.) + (1087./3000.);
  Real rsd3 = (-47./20.) + ( 389./300.);
  Real rsd4 = ( 33./20.) + (5173./3000.);

  const Real tol = 1e-13;
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(4);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  rsdPDEGlobal = 0;

  ResidualCell_Galerkin<TopoD2>::integrate( fcnint, xfld, qfld, &quadratureOrder, 1, rsdPDEGlobal );

  BOOST_CHECK_CLOSE( rsd1, rsdPDEGlobal[0], tol );
  BOOST_CHECK_CLOSE( rsd2, rsdPDEGlobal[1], tol );
  BOOST_CHECK_CLOSE( rsd3, rsdPDEGlobal[2], tol );
  BOOST_CHECK_CLOSE( rsd4, rsdPDEGlobal[3], tol );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
