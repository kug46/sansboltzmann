// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Jacobian2DLine_Triangle_Galerkin_AD_btest
// testing of 2-D line-integral jacobian: advection-diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin.h"
#include "Discretization/Galerkin/ResidualInteriorTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_2Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Hex_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianInteriorTrace_Galerkin_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin1D_1Line_X1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::MatrixQ<Real> MatrixQ;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.1, b = 5.6;
  Source1D_UniformGrad source(a,b);

  PDEClass pde( adv, visc, source );

  // grid: 2 lines @ P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // sequence of solution orders
  for (int qorder = 0; qorder <= 3; qorder++)
  {
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int nDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < nDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)nDOF));

    // quadrature rule (0 for a node)
    int quadratureOrder = 0;

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
    DLA::MatrixD<MatrixQ> jacTrue(nDOF,nDOF);

    rsdGlobal0 = 0;
    IntegrateInteriorTraceGroups<TopoD1>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal0),
                                                     xfld, qfld, &quadratureOrder, 1);

    for (int j = 0; j < nDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdGlobal1 = 0;
      IntegrateInteriorTraceGroups<TopoD1>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal1),
                                                       xfld, qfld, &quadratureOrder, 1);
      qfld.DOF(j) -= 1;

      for (int i = 0; i < nDOF; i++)
        jacTrue(i,j) = rsdGlobal1[i] - rsdGlobal0[i];
    }

    // jacobian via Surreal

    DLA::MatrixD<MatrixQ> mtxGlob(nDOF,nDOF);

    mtxGlob = 0;
    IntegrateInteriorTraceGroups<TopoD1>::integrate( JacobianInteriorTrace_Galerkin(fcnint, mtxGlob),
                                                     xfld, qfld, &quadratureOrder, 1);

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        SANS_CHECK_CLOSE( jacTrue(i,j), mtxGlob(i,j), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin2D_Box_UnionJack_Triangle_X1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::MatrixQ<Real> MatrixQ;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 2.1, b = 5.6, c = -0.34;
  Source2D_UniformGrad source(a,b,c);

  PDEClass pde( adv, visc, source );

  // grid: 2 triangles @ P1 (aka X1)
  XField2D_Box_UnionJack_Triangle_X1 xfld(2,2);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // sequence of solution orders
  for (int qorder = 0; qorder <= 3; qorder++)
  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int nDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < nDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)nDOF));

    // quadrature rule (2*P for basis and flux polynomials)
    std::vector<int> quadratureOrder = {2*qorder, 2*qorder, 2*qorder};

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
    DLA::MatrixD<MatrixQ> jacTrue(nDOF,nDOF);

    rsdGlobal0 = 0;
    IntegrateInteriorTraceGroups<TopoD2>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal0),
                                                     xfld, qfld, quadratureOrder.data(), quadratureOrder.size());

    for (int j = 0; j < nDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdGlobal1 = 0;
      IntegrateInteriorTraceGroups<TopoD2>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal1),
                                                       xfld, qfld, quadratureOrder.data(), quadratureOrder.size());
      qfld.DOF(j) -= 1;

      for (int i = 0; i < nDOF; i++)
        jacTrue(i,j) = rsdGlobal1[i] - rsdGlobal0[i];
    }

    // jacobian via Surreal
    DLA::MatrixD<MatrixQ> mtxGlob(nDOF,nDOF);

    mtxGlob = 0;
    IntegrateInteriorTraceGroups<TopoD2>::integrate( JacobianInteriorTrace_Galerkin(fcnint, mtxGlob),
                                                     xfld, qfld, quadratureOrder.data(), quadratureOrder.size());

    const Real small_tol = 1e-12;
    const Real close_tol = 5e-11;
    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        SANS_CHECK_CLOSE( jacTrue(i,j), mtxGlob(i,j), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin2D_2Quad_X1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::MatrixQ<Real> MatrixQ;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 2.1, b = 5.6, c = -0.34;
  Source2D_UniformGrad source(a,b,c);

  PDEClass pde( adv, visc, source );

  // grid: 2 quad @ P1 (aka X1)
  XField2D_2Quad_X1_1Group xfld;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // sequence of solution orders
  for (int qorder = 0; qorder <= MIN(3,BasisFunctionArea_Quad_LegendrePMax); qorder++)
  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int nDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < nDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)nDOF));

    // quadrature rule
    int quadratureOrder = 2*qorder;

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
    DLA::MatrixD<MatrixQ> jacTrue(nDOF,nDOF);

    rsdGlobal0 = 0;
    IntegrateInteriorTraceGroups<TopoD2>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal0),
                                                     xfld, qfld, &quadratureOrder, 1);

    for (int j = 0; j < nDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdGlobal1 = 0;
      IntegrateInteriorTraceGroups<TopoD2>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal1),
                                                       xfld, qfld, &quadratureOrder, 1);
      qfld.DOF(j) -= 1;

      for (int i = 0; i < nDOF; i++)
        jacTrue(i,j) = rsdGlobal1[i] - rsdGlobal0[i];
    }

    // jacobian via Surreal
    DLA::MatrixD<MatrixQ> mtxGlob(nDOF,nDOF);

    mtxGlob = 0;
    IntegrateInteriorTraceGroups<TopoD2>::integrate( JacobianInteriorTrace_Galerkin(fcnint, mtxGlob),
                                                     xfld, qfld, &quadratureOrder, 1);

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-11;
    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        SANS_CHECK_CLOSE( jacTrue(i,j), mtxGlob(i,j), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin2D_2Tet_X1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion3D::MatrixQ<Real> MatrixQ;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                                 kyx, kyy, kyz,
                                 kzx, kzy, kzz);

  Real a = 2.1, b = 5.6, c = -0.34, d = 0.67;
  Source3D_UniformGrad source(a,b,c,d);

  PDEClass pde( adv, visc, source );

  // grid: 2 tetrahedron @ P1 (aka X1)
  XField3D_2Tet_X1_1Group xfld;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // sequence of solution orders
  for (int qorder = 0; qorder <= MIN(3,BasisFunctionVolume_Tet_LegendrePMax); qorder++)
  {
    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int nDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < nDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)nDOF));

    // quadrature rule
    int quadratureOrder = 2*qorder;

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
    DLA::MatrixD<MatrixQ> jacTrue(nDOF,nDOF);

    rsdGlobal0 = 0;
    IntegrateInteriorTraceGroups<TopoD3>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal0),
                                                     xfld, qfld, &quadratureOrder, 1);

    for (int j = 0; j < nDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdGlobal1 = 0;
      IntegrateInteriorTraceGroups<TopoD3>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal1),
                                                       xfld, qfld, &quadratureOrder, 1);
      qfld.DOF(j) -= 1;

      for (int i = 0; i < nDOF; i++)
        jacTrue(i,j) = rsdGlobal1[i] - rsdGlobal0[i];
    }

    // jacobian via Surreal
    DLA::MatrixD<MatrixQ> mtxGlob(nDOF,nDOF);

    mtxGlob = 0;
    IntegrateInteriorTraceGroups<TopoD3>::integrate( JacobianInteriorTrace_Galerkin(fcnint, mtxGlob),
                                                     xfld, qfld, &quadratureOrder, 1);

    const Real small_tol = 1e-12;
    const Real close_tol = 5e-9;
    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        SANS_CHECK_CLOSE( jacTrue(i,j), mtxGlob(i,j), small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin2D_2Hex_X1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion3D::MatrixQ<Real> MatrixQ;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.7;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123; Real kxy = 0.553; Real kxz = 0.760;
  Real kyx = 0.343; Real kyy = 1.007; Real kyz = 0.870;
  Real kzx = 0.690; Real kzy = 0.365; Real kzz = 1.460;

  ViscousFlux3D_Uniform visc(kxx, kxy, kxz,
                                 kyx, kyy, kyz,
                                 kzx, kzy, kzz);

  Real a = 2.1, b = 5.6, c = -0.34, d = 0.67;
  Source3D_UniformGrad source(a,b,c,d);

  PDEClass pde( adv, visc, source );

  // grid: 2 tetrahedron @ P1 (aka X1)
  XField3D_2Hex_X1_1Group xfld;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // sequence of solution orders
  for (int qorder = 0; qorder <= MIN(3,BasisFunctionVolume_Hex_LegendrePMax); qorder++)
  {
    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

    const int nDOF = qfld.nDOF();

    // solution data
    for (int i = 0; i < nDOF; i++)
      qfld.DOF(i) = sin(PI*i/((Real)nDOF));

    // quadrature rule
    int quadratureOrder = 2*qorder;

    // jacobian via FD w/ residual operator; assumes scalar PDE

    SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
    DLA::MatrixD<MatrixQ> jacTrue(nDOF,nDOF);

    rsdGlobal0 = 0;
    IntegrateInteriorTraceGroups<TopoD3>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal0),
                                                     xfld, qfld, &quadratureOrder, 1);

    for (int j = 0; j < nDOF; j++)
    {
      qfld.DOF(j) += 1;

      rsdGlobal1 = 0;
      IntegrateInteriorTraceGroups<TopoD3>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobal1),
                                                       xfld, qfld, &quadratureOrder, 1);
      qfld.DOF(j) -= 1;

      for (int i = 0; i < nDOF; i++)
        jacTrue(i,j) = rsdGlobal1[i] - rsdGlobal0[i];
    }

    // jacobian via Surreal
    DLA::MatrixD<MatrixQ> mtxGlob(nDOF,nDOF);

    mtxGlob = 0;
    IntegrateInteriorTraceGroups<TopoD3>::integrate( JacobianInteriorTrace_Galerkin(fcnint, mtxGlob),
                                                     xfld, qfld, &quadratureOrder, 1);

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-10;
    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        SANS_CHECK_CLOSE( jacTrue(i,j), mtxGlob(i,j), small_tol, close_tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
