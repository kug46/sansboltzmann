// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FunctionalCell_Galerkin_btest
// testing of 2-D functional area-integral with Galerkin

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/list.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/tools/for_each_CellGroup.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/OutputCell_WeightedSolution.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include "MPI/serialize_DenseLinAlg_MatrixS.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( FunctionalCell_Galerkin_WeightedSolution_test_suite )

template<class PhysDim_>
struct DummyPDEReal
{
  typedef PhysDim_ PhysDim;

  template<class T>
  using ArrayQ = T;

  template<class T>
  using MatrixQ = T;
};

template<class PhysDim_>
struct DummyPDEVector
{
  typedef PhysDim_ PhysDim;

  template<class T>
  using ArrayQ = DLA::VectorS<2,T>;

  template<class T>
  using MatrixQ = DLA::MatrixS<2,2,T>;
};


typedef boost::mpl::list< DummyPDEReal<PhysD1>,
                          DummyPDEVector<PhysD1> >  DummyPDEs_PhysD1;

typedef boost::mpl::list< DummyPDEReal<PhysD2>,
                          DummyPDEVector<PhysD2> >  DummyPDEs_PhysD2;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Line_P0_Const_test, DummyPDE, DummyPDEs_PhysD1 )
{
  typedef ScalarFunction1D_Const WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  typedef IntegrandCell_Galerkin_Output<NDOutputClass> IntegrandClass;

  Real a0 = 1.4;
  WeightFcn weightFcn(a0);

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P0 (aka Q0)
  int qorder = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  BOOST_CHECK_EQUAL( 1, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayJ functional = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_Galerkin( integrand, functional ), xfld, qfld, &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
    SANS_CHECK_CLOSE( a0, DLA::index(functional,m), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Line_P1_Const_test, DummyPDE, DummyPDEs_PhysD1 )
{
  typedef ScalarFunction1D_Const WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  typedef IntegrandCell_Galerkin_Output<NDOutputClass> IntegrandClass;

  Real a0=1.4;
  WeightFcn weightFcn(a0);

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( 2, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0; // phi_0 = 1-x;
  qfld.DOF(1) = 2.0; // phi_1 = x;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  ArrayJ functional = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_Galerkin( integrand, functional ), xfld, qfld, &quadratureOrder, 1 );

  for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
    SANS_CHECK_CLOSE( a0*(3./2), DLA::index(functional,m), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Triangle_P0_Linear_test, DummyPDE, DummyPDEs_PhysD2 )
{
  typedef ScalarFunction2D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  typedef IntegrandCell_Galerkin_Output<NDOutputClass> IntegrandClass;

  Real a0=1.4,a1=0.0,a2=0.0;
  WeightFcn weightFcn(a0,a1,a2);

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P0 (aka Q0)
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayJ functional = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_Galerkin( integrand, functional ), xfld, qfld, &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
    SANS_CHECK_CLOSE( a0/2, DLA::index(functional,m), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Triangle_P1_Linear_test, DummyPDE, DummyPDEs_PhysD2 )
{
  typedef ScalarFunction2D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDE,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  typedef IntegrandCell_Galerkin_Output<NDOutputClass> IntegrandClass;

  Real a0=1.4,a1=0.0,a2=0.0;
  WeightFcn weightFcn(a0,a1,a2);

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 3, qfld.nDOF() );

  // solution data
  qfld.DOF(0) = 1.0;
  qfld.DOF(1) = 2.0;
  qfld.DOF(2) = 3.0;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 2;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayJ functional = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_Galerkin( integrand, functional ), xfld, qfld, &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
    SANS_CHECK_CLOSE( a0/2, DLA::index(functional,m), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_P1_Linear_Parallel_test )
{
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> NDSolution;
  typedef ScalarFunction2D_Linear WeightFcn; // Change this to change weighting type
  typedef OutputCell_WeightedSolution<DummyPDEReal<PhysD2>,WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;

  typedef typename NDOutputClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDOutputClass::template ArrayJ<Real> ArrayJ;

  typedef IntegrandCell_Galerkin_Output<NDOutputClass> IntegrandClass;

  // global communicator
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  Real a0=1.4,a1=0.0,a2=0.0;
  WeightFcn weightFcn(a0,a1,a2);

  int ii = 5;
  int jj = 4;

  // grid
  XField2D_Box_Triangle_Lagrange_X1 xfld_global( comm, ii, jj ); // complete system on all processors
  XField2D_Box_Triangle_Lagrange_X1 xfld_local( world, ii, jj ); // partitioned system

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_global(xfld_global, qorder, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_local (xfld_local , qorder, BasisFunctionCategory_Legendre);

  // solution data
  NDSolution soln;
  for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(soln, {0}), (xfld_global, qfld_global) );
  for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(soln, {0}), (xfld_local , qfld_local ) );

  // quadrature rule: sine function...
  int quadratureOrder = 3*qorder+1;

  // integrand
  NDOutputClass fcnOutput(weightFcn);
  IntegrandClass integrand( fcnOutput, {0} );

  ArrayJ functional_global = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_Galerkin( integrand, functional_global ), xfld_global, qfld_global, &quadratureOrder, 1 );

  ArrayJ functional_local = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_Galerkin( integrand, functional_local ), xfld_local, qfld_local, &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int m = 0; m < DLA::VectorSize<ArrayJ>::M; m++)
    SANS_CHECK_CLOSE( DLA::index(functional_global,m), DLA::index(functional_local,m), small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
