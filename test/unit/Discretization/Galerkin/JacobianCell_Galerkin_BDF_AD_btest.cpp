// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_Galerkin_BDF_AD_btest
// testing of Galerkin BDF cell-integral jacobian: advection-diffusions

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
//#include "Field/FieldVolume_DG_Cell.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_BDF.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin_BDF.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin_BDF.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace           // local definition
{

//----------------------------------------------------------------------------//
// jacobian dumps
template <int N>
void
dump( Real jac[][N], int m, int n )
{
  cout << "{";
  for (int i = 0; i < m; i++)
  {
    cout << "{";
    for (int j = 0; j < n; j++)
    {
      cout << jac[i][j];
      if (j < n-1) cout << ", ";
    }
    cout << "}";
    if (i < m-1) cout << ", ";
  }
  cout << "}" << std::endl;
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_Galerkin_BDF_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_BDF_1D_1Line_X1Q0_Surreal1 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::MatrixQ<Real> MatrixQ;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QFieldSequenceType;

  typedef IntegrandCell_Galerkin_BDF<PDEClass> IntegrandClass;

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  GlobalTime time(0);

  Source1D_None source;

  PDEClass pde( time, adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: line
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single line, P0 (aka Q0)
  int qorder = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  // solution data
  qfld.DOF(0) = 1;


  int BDFweights = 2;

  QFieldSequenceType qfldspast(BDFweights-1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Legendre);

  qfldspast[0].DOF(0) = 0;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // Time integration parameters
  Real dt = 13.;
  std::vector<Real> weights {1., -1.}; //BDF1 weights

  // integrand
  IntegrandClass fcnint( pde, {0}, dt, weights );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(1), rsdPDEGlobal1(1);
  Real jacPDE_q[1][1];

  rsdPDEGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal0),
                                          xfld, (qfld, qfldspast), &quadratureOrder, 1 );

  qfld.DOF(0) += 1;
  rsdPDEGlobal1 = 0;
  //rsdAuGlobal1 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal1),
                                          xfld, (qfld, qfldspast), &quadratureOrder, 1 );

  jacPDE_q[0][0] = rsdPDEGlobal1[0] - rsdPDEGlobal0[0];

  qfld.DOF(0) -= 1;

#if 0
  std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,1,2);
#endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(1,1);

  mtxPDEGlob_q = 0;
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_Galerkin_BDF(fcnint, mtxPDEGlob_q),
                                          xfld, (qfld, qfldspast), &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  SANS_CHECK_CLOSE( jacPDE_q[0][0], mtxPDEGlob_q(0,0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_BDF_1D_1Line_X1Q1_Surreal )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::MatrixQ<Real> MatrixQ;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QFieldSequenceType;

  typedef IntegrandCell_Galerkin_BDF<PDEClass> IntegrandClass;

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  GlobalTime time(0);

  Source1D_None source;

  PDEClass pde( time, adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 2, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;


  int BDFweights = 2;

  QFieldSequenceType qfldspast(BDFweights-1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Legendre);

  qfldspast[0] = 0;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 2;

  // Temporal discretization parameters
  Real dt = 13.;
  std::vector<Real> weights {1., -1.}; //BDF1 weights

  // integrand
  IntegrandClass fcnint( pde, {0}, dt, weights );

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(2), rsdPDEGlobal1(2);
  Real jacPDE_q[2][2];

  rsdPDEGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal0),
                                          xfld, (qfld, qfldspast), &quadratureOrder, 1 );

  for (int j = 0; j < 2; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal1),
                                            xfld, (qfld, qfldspast), &quadratureOrder, 1 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < 2; i++)
    {
      jacPDE_q[i][j]   = rsdPDEGlobal1[i]  - rsdPDEGlobal0[i];
    }
  }

#if 0
  std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,3,6);
#endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(2,2);

  mtxPDEGlob_q = 0;

  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_Galerkin_BDF(fcnint, mtxPDEGlob_q),
                                          xfld, (qfld, qfldspast), &quadratureOrder, 1 );

#if 0
  std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 2; j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_BDF_Galerkin_1Triangle_X1Q1_Surreal )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::MatrixQ<Real> MatrixQ;
  typedef FieldSequence<PhysD2, TopoD2, ArrayQ> QFieldSequenceType;

  typedef IntegrandCell_Galerkin_BDF<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  GlobalTime time(0);
  PDEClass pde( time, adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  // solution data
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  int BDFweights = 3;

  QFieldSequenceType qfldspast(BDFweights-1, FieldConstructor<Field_DG_Cell>(), xfld, qorder, BasisFunctionCategory_Legendre);

  qfldspast[0].DOF(0) = 0;
  qfldspast[0].DOF(1) = 0;
  qfldspast[0].DOF(2) = 0;

  qfldspast[1].DOF(0) = -1./3.;
  qfldspast[1].DOF(1) = -4./9.;
  qfldspast[1].DOF(2) = -5.;

  // quadrature rule (linear: basis is linear, solution is const)
  int quadratureOrder = 1;

  // Temporal discretization
  Real dt = 13.;
  std::vector<Real> weights {1.5, -2., 0.5}; //BDF2 weights

  // integrand
  IntegrandClass fcnint( pde, {0}, dt, weights );

  // jacobian via FD w/ residual operator; assumes scalar PDE
  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(3), rsdPDEGlobal1(3);
  Real jacPDE_q[3][3];

  rsdPDEGlobal0 = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal0),
                                          xfld, (qfld, qfldspast), &quadratureOrder, 1 );

  for (int j = 0; j < 3; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin_BDF(fcnint, rsdPDEGlobal1),
                                            xfld, (qfld, qfldspast), &quadratureOrder, 1 );

    qfld.DOF(j) -= 1;

    for (int i = 0; i < 3; i++)
      jacPDE_q[i][j]     = rsdPDEGlobal1[i]    - rsdPDEGlobal0[i];
  }

#if 0
  std::cout << "jacPDE_q = " << std::endl; dump(jacPDE_q,3,6);
#endif

  // jacobian via Surreal

  DLA::MatrixD<MatrixQ> mtxPDEGlob_q(3,3);
  mtxPDEGlob_q = 0;

  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin_BDF(fcnint, mtxPDEGlob_q),
                                          xfld, (qfld, qfldspast), &quadratureOrder, 1 );

#if 0
  std::cout << "mtxPDEGlob_q " << std::endl; mtxPDEGlob_q.dump();
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxPDEGlob_q(i,j), small_tol, close_tol );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
