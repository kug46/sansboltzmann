// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianBoundaryTrace_Galerkin_WakeCut_LIP_btest
// testing of trace-integral jacobian: LIP wake cut

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"
#include "pde/FullPotential/BCLinearizedIncompressiblePotential3D.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Galerkin_WakeCut_LIP.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Galerkin_WakeCut_LIP.h"

#include "Discretization/Galerkin/JacobianBoundaryTrace_WakeCut_CG_Potential_Drela.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_WakeCut_CG_Potential_Drela.h"

#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "Surreal/SurrealS.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group_WakeCut.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

using namespace std;
using namespace SANS;


namespace           // local definition
{

//----------------------------------------------------------------------------//
// jacobian dumps
template <int N>
void
dump( Real jac[][N], int m, int n )
{
  cout << endl << "{";
  for (int i = 0; i < m; i++)
  {
    cout << "{";
    for (int j = 0; j < n; j++)
    {
      cout << jac[i][j];
      if (j < n-1) cout << ", ";
    }
    cout << "}";
    if (i < m-1) cout << ","  << endl << " ";
  }
  cout << "}";
}

void
dump( Real jac[][3], int m, int n )
{
  cout << "{";
  for (int i = 0; i < m; i++)
  {
    cout << "{";
    for (int j = 0; j < n; j++)
    {
      cout << jac[i][j];
      if (j < n-1) cout << ", ";
    }
    cout << "}";
    if (i < m-1) cout << ","  << endl << " ";
  }
  cout << "}";
}

}                   // local definition


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianBoundaryTrace_Galerkin_WakeCut_LIP_test_suite )


typedef boost::mpl::list< SurrealS<1>, SurrealS<2> > Surreals;

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin2D_1Triangle_X1Q1, SurrealClass, Surreals )
{

  typedef PDEAdvectionDiffusion2D PDEClassRaw;
  typedef PDENDConvertSpace<PhysD2, PDEClassRaw> PDEClass;
  typedef PDEClassRaw::template ArrayQ<Real> ArrayQ;
  typedef PDEClassRaw::template MatrixQ<Real> MatrixQ;
  typedef PDEClassRaw::template ArrayQ<SurrealClass> ArrayQSurreal;

  typedef BCAdvectionDiffusion<PhysD2,BCTypeLinearRobin_mitLG> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;

  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);


  PDEClass pde(adv, visc);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  Real A = 1;
  Real B = 2;
  Real bcdata = 3;

  BCClass bc(A, B, bcdata);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 1 );

  // grid: P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  ArrayQ qDOF[3];

  // triangle solution data (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;
  qDOF[2] = qfld.DOF(2) = 4;

  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  ArrayQ lgDOF[6];
  for (int i = 0; i < 6; i++)
    lgDOF[i] = 0;

  // Lagrange multiplier DOF data
  lgDOF[2] = lgfld.DOF(2) =  5;
  lgDOF[3] = lgfld.DOF(3) = -7;

  // integrand
  IntegrandClass fcn( pde, bc, {1} );

  // quadrature rule
  int quadratureOrder[3] = {2,2,2};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(3), rsdPDEGlobal1(3);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(6), rsdBCGlobal1(6);
  Real jacPDE_q[3][3], jacPDE_lg[3][6];
  Real jacBC_q[6][3], jacBC_lg[6][6];

  rsdPDEGlobal0 = 0;
  rsdBCGlobal0 = 0;

  ResidualBoundaryTrace_Galerkin_LG<TopoD2>::integrate( fcn, qfld, lgfld, quadratureOrder, 3, rsdPDEGlobal0, rsdBCGlobal0 );

  // jacobian wrt solution
  for (int j = 0; j < 3; j++)
  {
    qfld.DOF(j)(0) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    ResidualBoundaryTrace_Galerkin_LG<TopoD2>::integrate( fcn, qfld, lgfld, quadratureOrder, 3, rsdPDEGlobal1, rsdBCGlobal1 );

    for (int i = 0; i < 3; i++)
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 6; i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 3; i++)
      qfld.DOF(i) = qDOF[i];
  }


  // jacobian wrt Lagrange multiplier
  for (int j = 0; j < 6; j++)
  {
    lgfld.DOF(j)(0) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    ResidualBoundaryTrace_Galerkin_LG<TopoD2>::integrate( fcn, qfld, lgfld, quadratureOrder, 3, rsdPDEGlobal1, rsdBCGlobal1 );

    for (int i = 0; i < 3; i++)
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 6; i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 6; i++)
      lgfld.DOF(i) = lgDOF[i];
  }

#if 0
  cout << "FD: jacPDE_q  = ";  dump( jacPDE_q, 3, 3 );  cout << endl;
  cout << "FD: jacPDE_lg = ";  dump( jacPDE_lg, 3, 6 );  cout << endl;
  cout << "FD: jacBC_q   = ";  dump( jacBC_q, 6, 3 );  cout << endl;
  cout << "FD: jacBC_lg  = ";  dump( jacBC_lg, 6, 6 );  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(3,3);
  DLA::MatrixD<MatrixQ> mtxGlobPDE_lg(3,6);
  DLA::MatrixD<MatrixQ> mtxGlobBC_q(6,3);
  DLA::MatrixD<MatrixQ> mtxGlobBC_lg(6,6);

  mtxGlobPDE_q = 0;
  mtxGlobPDE_lg = 0;
  mtxGlobBC_q = 0;
  mtxGlobBC_lg = 0;

  JacobianBoundaryTrace_Galerkin<SurrealClass,TopoD2>::integrate(
      fcn, qfld, lgfld, quadratureOrder, 3,
      mtxGlobPDE_q, mtxGlobPDE_lg, mtxGlobBC_q, mtxGlobBC_lg );

#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
  cout << "Surreal: mtxGlobPDE_lg = " << endl;  mtxGlobPDE_lg.dump(2);
  cout << "Surreal: mtxGlobBC_q   = " << endl;  mtxGlobBC_q.dump(2);
  cout << "Surreal: mtxGlobBC_lg  = " << endl;  mtxGlobBC_lg.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;
  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < 3; j++)
       SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxGlobPDE_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 6; j++)
       SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxGlobPDE_lg(i,j), small_tol, close_tol );
  }

  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 3; j++)
       SANS_CHECK_CLOSE( jacBC_q[i][j], mtxGlobBC_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 6; j++)
       SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxGlobBC_lg(i,j), small_tol, close_tol );
  }

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin3D_1Tet_X1Q1, SurrealClass, Surreals )
{

  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  typedef IntegrandTrace_Galerkin_WakeCut_LIP<PDEClass> IntegrandClass;

  // PDE

  Real U = 1.1;
  Real V = 0.2;
  Real W = 0.5;

  PDEClass pde( U, V, W );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // grid: P1 (aka X1)
  XField3D_2Tet_X1_1Group_WakeCut xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 6 );

  ArrayQ qDOF[6];

  // triangle solution data (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;
  qDOF[2] = qfld.DOF(2) = 4;
  qDOF[3] = qfld.DOF(3) = 7;
  qDOF[4] = qfld.DOF(4) = 9;
  qDOF[5] = qfld.DOF(5) = 12;

  // Lagrange multiplier
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld(xfld, qorder);

  BOOST_REQUIRE_EQUAL( lgfld.nDOF(), 6 );

  ArrayQ lgDOF[6];
  for (int i = 0; i < 6; i++)
    lgDOF[i] = 0;

  // Lagrange multiplier DOF data
  lgDOF[0] = lgfld.DOF(0) =  5;
  lgDOF[1] = lgfld.DOF(1) = -7;
  lgDOF[2] = lgfld.DOF(2) =  9;
  lgDOF[3] = lgfld.DOF(3) = -2;
  lgDOF[4] = lgfld.DOF(4) =  3;
  lgDOF[5] = lgfld.DOF(5) =  2;

  // integrand
  IntegrandClass fcn( pde, {0} );

  // quadrature rule
  int quadratureOrder[7] = {1,1,1,1,1,1,1};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(6), rsdPDEGlobal1(6);
  SLA::SparseVector<ArrayQ> rsdBCGlobal0(6), rsdBCGlobal1(6);
  Real jacPDE_q[6][6], jacPDE_lg[6][6];
  Real jacBC_q[6][6], jacBC_lg[6][6];

  rsdPDEGlobal0 = 0;
  rsdBCGlobal0 = 0;

  ResidualBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::integrate( fcn, qfld, lgfld, quadratureOrder, 7, rsdPDEGlobal0, rsdBCGlobal0 );

  // jacobian wrt solution
  for (int j = 0; j < 6; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    ResidualBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::integrate( fcn, qfld, lgfld, quadratureOrder, 7, rsdPDEGlobal1, rsdBCGlobal1 );

    for (int i = 0; i < 6; i++)
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 6; i++)
      jacBC_q[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 6; i++)
      qfld.DOF(i) = qDOF[i];
  }


  // jacobian wrt Lagrange multiplier
  for (int j = 0; j < 6; j++)
  {
    lgfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;
    rsdBCGlobal1 = 0;

    ResidualBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::integrate( fcn, qfld, lgfld, quadratureOrder, 7, rsdPDEGlobal1, rsdBCGlobal1 );

    for (int i = 0; i < 6; i++)
      jacPDE_lg[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];
    for (int i = 0; i < 6; i++)
      jacBC_lg[i][j] = rsdBCGlobal1[i] - rsdBCGlobal0[i];

    for (int i = 0; i < 6; i++)
      lgfld.DOF(i) = lgDOF[i];
  }

#if 0
  cout << "FD: jacPDE_q  = ";  dump( jacPDE_q, 5, 5 );  cout << endl;
  cout << "FD: jacPDE_lg = ";  dump( jacPDE_lg, 5, 5 );  cout << endl;
  cout << "FD: jacBC_q   = ";  dump( jacBC_q, 5, 5 );  cout << endl;
  cout << "FD: jacBC_lg  = ";  dump( jacBC_lg, 5, 5 );  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(6,6);
  DLA::MatrixD<MatrixQ> mtxGlobPDE_lg(6,6);
  DLA::MatrixD<MatrixQ> mtxGlobBC_q(6,6);
  DLA::MatrixD<MatrixQ> mtxGlobBC_lg(6,6);

  mtxGlobPDE_q = 0;
  mtxGlobPDE_lg = 0;
  mtxGlobBC_q = 0;
  mtxGlobBC_lg = 0;

  JacobianBoundaryTrace_Galerkin_WakeCut_LIP<SurrealClass,TopoD3>::integrate(
      fcn, qfld, lgfld, quadratureOrder, 7,
      mtxGlobPDE_q, mtxGlobPDE_lg, mtxGlobBC_q, mtxGlobBC_lg );

#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
  cout << "Surreal: mtxGlobPDE_lg = " << endl;  mtxGlobPDE_lg.dump(2);
  cout << "Surreal: mtxGlobBC_q   = " << endl;  mtxGlobBC_q.dump(2);
  cout << "Surreal: mtxGlobBC_lg  = " << endl;  mtxGlobBC_lg.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;
  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 6; j++)
       SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxGlobPDE_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 6; j++)
       SANS_CHECK_CLOSE( jacPDE_lg[i][j], mtxGlobPDE_lg(i,j), small_tol, close_tol );
  }

  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 6; j++)
       SANS_CHECK_CLOSE( jacBC_q[i][j], mtxGlobBC_q(i,j), small_tol, close_tol );
    for (int j = 0; j < 6; j++)
       SANS_CHECK_CLOSE( jacBC_lg[i][j], mtxGlobBC_lg(i,j), small_tol, close_tol );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin3D_1Tet_X1Q1_sansLG, SurrealClass, Surreals )
{

  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  typedef IntegrandTrace_Galerkin_WakeCut_sansLG<PDEClass> IntegrandClass;

  // PDE

  Real U = 1.1;
  Real V = 0.2;
  Real W = 0.5;

  PDEClass pde( U, V, W );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // grid: P1 (aka X1)
  XField3D_2Tet_X1_1Group_WakeCut xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( qfld.nDOF(), 6 );

  ArrayQ qDOF[6];

  // triangle solution data (left)
  qDOF[0] = qfld.DOF(0) = 1;
  qDOF[1] = qfld.DOF(1) = 3;
  qDOF[2] = qfld.DOF(2) = 4;
  qDOF[3] = qfld.DOF(3) = 7;
  qDOF[4] = qfld.DOF(4) = 9;
  qDOF[5] = qfld.DOF(5) = 12;

  // integrand
  IntegrandClass fcn( pde, {0}, 0, {} );

  // quadrature rule
  int quadratureOrder[7] = {1,1,1,1,1,1,1};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(6), rsdPDEGlobal1(6);
  Real jacPDE_q[6][6];

  rsdPDEGlobal0 = 0;

  ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcn, qfld, quadratureOrder, 7, rsdPDEGlobal0 );

  // jacobian wrt solution
  for (int j = 0; j < 6; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;

    ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcn, qfld, quadratureOrder, 7, rsdPDEGlobal1 );

    for (int i = 0; i < 6; i++)
      jacPDE_q[i][j] = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

    for (int i = 0; i < 6; i++)
      qfld.DOF(i) = qDOF[i];
  }


#if 0
  cout << "FD: jacPDE_q  = ";  dump( jacPDE_q, 6, 6 );  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(6,6);

  mtxGlobPDE_q = 0;

  JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate(
      fcn, qfld, quadratureOrder, 7,
      mtxGlobPDE_q );

#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;
  for (int i = 0; i < 6; i++)
  {
    for (int j = 0; j < 6; j++)
       SANS_CHECK_CLOSE( jacPDE_q[i][j], mtxGlobPDE_q(i,j), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( avoid_compiler_warnings )
{
  Real jac1[2][2] = { {0, 0}, {0, 0} };
  Real jac2[3][3] = { {0, 0, 0}, {0, 0, 0}, {0, 0, 0} };

  cout.setstate( std::ios_base::badbit );
  dump( jac1, 2, 2 );
  dump( jac2, 3, 3 );
  cout.clear();
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
