// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LinearizedIncompressiblePotential3D_Elliptic_btest
// testing of LIP wake with an imposed elliptic potential field

#include <complex>

//#define LG_WAKE

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"
#include "pde/FullPotential/BCLinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/LinearizedIncompressiblePotential3D_Trefftz.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "OutputFunctional/FunctionalBoundaryTrace_mitLG.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
//#include "Discretization/Galerkin/ResidualBoundaryTrace_Galerkin_LG.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Galerkin_PDE.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
//#include "Discretization/Galerkin/JacobianBoundaryTrace_Galerkin.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Galerkin.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"

#ifdef LG_WAKE
#include "Discretization/Galerkin/ResidualBoundaryTrace_Galerkin_WakeCut_LIP.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Galerkin_WakeCut_LIP.h"
#else
#include "Discretization/Galerkin/ResidualBoundaryTrace_WakeCut_CG_Potential_Drela.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_WakeCut_CG_Potential_Drela.h"
#include "Discretization/Galerkin/IntegrandFunctor_Galerkin_WakeCut_sansLG.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Galerkin_WakeCut_Potential_Drela.h"
#endif

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/Direct/MKL_PARDISO/MKL_PARDISOSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGADS/extrude.h"
#include "Meshing/EGADS/solidBoolean.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "Meshing/TetGen/EGTetGen.h"

using namespace std;
using namespace SANS;
using namespace EGADS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

// Un-named namespace makes these functions private to this file
namespace
{


EGModel<3> makeWakedBox(const EGContext& context, const int nSpan,
                        const std::vector<double>& wakeParams)
{
  //Create the farfield box that is larger than the wake
  // defined on x > 0, y in [-1,1], z = 0
  std::vector<double> Box = {0.00, -2.0, -1.,
                             10.00,  4.0,  2.};
  EGBody<3> box = context.makeSolidBody( BOX, Box );


  std::vector< EGFace<3> > boxfaces = box.getFaces();
  boxfaces[0].addAttribute("Inflow", 1);
  boxfaces[1].addAttribute("Outflow", 1);
  for ( std::size_t face = 2; face < boxfaces.size(); face++ )
  {
    //Set lateral faces to Lateral
    boxfaces[face].addAttribute("Lateral", 1);
  }

  EGNode<3> nodeL(context, {-0.1,-1, 0});
  EGNode<3> nodeR(context, {-0.1, 1, 0});

  EGEdge<3> wakeedge(nodeL, nodeR);

  Real len = 50;
  DLA::VectorS<3,Real> dir = {1, 0., 0};

  //Nodes outside the box
  EGNode<3> outL(context, {-0.1, -20, 0});
  EGNode<3> outR(context, {-0.1,  20, 0});

  //Edges that extend from the wing tips outside the box
  EGEdge<3> edgeTipL (outL, nodeL);
  EGEdge<3> edgeTipR (nodeR, outR);

  EGLoop<3>::edge_vector wakeEdges;

  wakeEdges = {(edgeTipL,1),(wakeedge,1),(edgeTipR,1)};

  // Surface used to create the wake and cut the box
  EGBody<3> wakecutter = extrude( EGLoop<3>(context, wakeEdges, OPEN), len, dir );

  EGBody<3> boxscribed( EGIntersection<3>(box, wakecutter) );

  EGBody<3> wakelong = extrude( EGLoop<3>((wakeedge,1), OPEN), len, dir );

  EGBody<3> wake = intersect( EGModel<3>(wakelong), box ).getBodies()[0].clone();

  //const int nSpan = std::max(2./wakeParams[0],2.);
  std::vector<double> rSpan(nSpan-2);
  for ( int i = 1; i < nSpan-1; i++ )
    //rSpan[i-1] = i/Real(nSpan-1);
    rSpan[i-1] = 1-0.5*(1+cos(PI*i/Real(nSpan-1)));

  //Set the tess resolution on the wake
  std::vector< EGFace<3> > wakefaces = wake.getFaces();
  for ( auto face = wakefaces.begin(); face != wakefaces.end(); face++ )
  {
    face->addAttribute(".tParams", wakeParams);
    face->addAttribute("Wake", 1);
  }

  typedef EGNode<3>::CartCoord CartCoord;
  std::vector< EGEdge<3> > WakeEdges = wake.getEdges();
  for ( auto edge = WakeEdges.begin(); edge != WakeEdges.end(); edge++ )
  {
    std::vector<EGNode<3>> nodes = edge->getNodes();

    if (nodes.size() == 1 ) continue;

    if ( ((CartCoord)nodes[0])[0] > Box[3]-1e-6 &&
         ((CartCoord)nodes[1])[0] > Box[3]-1e-6 )
    {
      edge->addAttribute("Trefftz", 1); // Mark the edge that is used for Treffetz plane integrals
    }
    else if ( ((CartCoord)nodes[0])[0] < Box[0]+1e-6 &&
              ((CartCoord)nodes[1])[0] < Box[0]+1e-6 )
    {
      edge->addAttribute("Kutta", 2); // Mark the edge that is used for Kutta condition integrals
    }

    if ( fabs( ((CartCoord)nodes[0])[0] - ((CartCoord)nodes[1])[0] ) < 0.1 )
    {
      edge->addAttribute(".rPos", rSpan); //Span
    }
  }


  return EGModel<3>({wake, boxscribed});
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( LinearizedIncompressiblePotential_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EllipticPotential_test )
{
  return; //Don't do any testing because memcheck takes too long

  typedef SurrealS<1> SurrealClass;

  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D> PDEClass;
  typedef PDELinearizedIncompressiblePotential3D::template ArrayQ<Real> ArrayQ;
  typedef PDELinearizedIncompressiblePotential3D::template MatrixQ<Real> MatrixQ;

  typedef BCNDConvertSpace<PhysD3, BC<PDELinearizedIncompressiblePotential3D,BCTypeFunction<SolutionDummy> > > BCSolution;
  typedef BCNDConvertSpace<PhysD3, BC<PDELinearizedIncompressiblePotential3D,BCTypeNeumann> > BCNeumann;
  typedef BCNDConvertSpace<PhysD3, BC<PDELinearizedIncompressiblePotential3D,BCTypeWall> > BCWall;

  typedef IntegrandBoundary_Galerkin_PDE<PDEClass, BCSolution> IntegrandBCSolution;
  typedef IntegrandBoundary_Galerkin_PDE<PDEClass, BCNeumann> IntegrandBCNeumann;
  typedef IntegrandBoundary_Galerkin_PDE<PDEClass, BCWall> IntegrandBCWall;

  typedef IntegrandTrace_Galerkin_WakeCut_Potential_Drela<PDEClass> IntegrandWake;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandCellClass;


  EGContext context((CreateContext()));

  // Refining the grid any more in the spanwise direction gives the incorrect integral
  // as the derivate of the elliptic potential field is singular at y = -1 and y = 1
  const int nSpan = 11;
  std::vector<double> wakeParams = {0.2, 0.001,15.};
  std::vector<double> EGADSParams = {0.2, 0.001, 15.0};
  // TetGen mesh parameters
  Real maxRadiusEdgeRatio = 1.075;
  Real minDihedralAngle = 0;

  EGModel<3> model = makeWakedBox(context, nSpan, wakeParams);

  EGTetGen xfld(model, EGADSParams, maxRadiusEdgeRatio, minDihedralAngle);


  std::vector<int> WallFaces, InflowFaces, OutflowFaces, WakeFaces, LGFaces;

  {
    int BCFace = 0;
    std::vector< EGBody<3> > bodies = model.getBodies();
    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
    {
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        if ( faces[i].hasAttribute("Wall") )
        {
          WallFaces.push_back(BCFace);
          LGFaces.push_back(BCFace);
          BCFace++;
        }
        else if ( faces[i].hasAttribute("Inflow") || faces[i].hasAttribute("Lateral") )
        {
          InflowFaces.push_back(BCFace);
          LGFaces.push_back(BCFace);
          BCFace++;
        }
        else if ( faces[i].hasAttribute("Outflow") )
        {
          //InflowFaces.push_back(BCFace);
          OutflowFaces.push_back(BCFace);
          LGFaces.push_back(BCFace);
          BCFace++;
        }
      }
    }

    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ ) //TODO: This should first go over solid bodies then sheet bodies...
    {
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        if ( faces[i].hasAttribute("Wake") )
        {
          WakeFaces.push_back(BCFace);
          BCFace++;
        }
      }
    }
  }

  // PDE
  Real U = 1, V = 0, W = 0;

  PDEClass pde( U, V, W );

  // BC
  EllipticPotential phi;
  Real outdata = 0.;

  BCSolution bcInflow( &phi );
  BCNeumann bcOutflow( outdata );
  BCWall bcwall( pde );

  // integrands
  IntegrandCellClass fcnCell( pde, {0} );
  IntegrandBCSolution fcnBCInflow( pde, bcInflow, InflowFaces );
  IntegrandBCNeumann fcnBCOutflow( pde, bcOutflow, OutflowFaces );
  IntegrandBCWall fcnBCwall( pde, bcwall, WallFaces );
  IntegrandWake fcnWake(pde, WakeFaces, xfld.dupPointOffset_, xfld.KuttaPoints_);

  int order = 1;

  // solution: Hierarchical, C0
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld( xfld, order, BasisFunctionCategory_Hierarchical );
  qfld = 0;
  const int nDOFPDE = qfld.nDOF();

  std::cout << "nDOFPDE = " << nDOFPDE << std::endl;

  EGTetGen::FieldCellGroupType<Tet>& xCellGroup = xfld.getCellGroup<Tet>(0);
  EGTetGen::FieldCellGroupType<Tet>::ElementType<> xCellElem( xCellGroup.basis() );

  Field_CG_Cell<PhysD3, TopoD3, ArrayQ>::FieldCellGroupType<Tet>& qCellGroup = qfld.getCellGroup<Tet>(0);
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ>::FieldCellGroupType<Tet>::ElementType<> qCellElem( qCellGroup.basis() );


  for (int elem = 0; elem < xCellGroup.nElem(); elem++ )
  {
    xCellGroup.getElement(xCellElem, elem);
    qCellGroup.getElement(qCellElem, elem);

    for (int n = 0; n < xCellElem.nDOF(); n++)
      qCellElem.DOF(n) = phi(xCellElem.DOF(n));

    qCellGroup.setElement(qCellElem, elem);
  }

  if ( WakeFaces.size() > 0 )
  {
    EGTetGen::FieldTraceGroupType<Triangle>& xTraceGroup = xfld.getBoundaryTraceGroup<Triangle>(WakeFaces[0]);

    // Traverse all elements on the wake, and perturb them so the solution from either
    // side of the wake is property initialized
    for (int elem = 0; elem < xTraceGroup.nElem(); elem++ )
    {
      int elemL = xTraceGroup.getElementLeft(elem);

      xCellGroup.getElement(xCellElem, elemL);
      qCellGroup.getElement(qCellElem, elemL);

      for (int n = 0; n < xCellElem.nDOF(); n++)
      {
        // Nudge the element so it's clearly above z = 0
        xCellElem.DOF(n)[2] += 1e-12;
        qCellElem.DOF(n) = phi(xCellElem.DOF(n));
      }

      qCellGroup.setElement(qCellElem, elemL);


      int elemR = xTraceGroup.getElementRight(elem);

      xCellGroup.getElement(xCellElem, elemR);
      qCellGroup.getElement(qCellElem, elemR);

      for (int n = 0; n < xCellElem.nDOF(); n++)
      {
        // Nudge the element so it's clearly below z = 0
        xCellElem.DOF(n)[2] -= 1e-12;
        qCellElem.DOF(n) = phi(xCellElem.DOF(n));
      }

      qCellGroup.setElement(qCellElem, elemR);
    }
  }
#if 0
  string filename = "tmp/EllipticPotential_LIP.dat";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot_LIP( pde, qfld, filename );
#endif



  // quadrature rule
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 1);    // max
  std::vector<int> quadratureOrderMin(xfld.nBoundaryTraceGroups(), 0);     // min



  // linear system setup

  typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
  typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
  typedef SparseMatrixClass               SystemMatrixClass;
  typedef SparseVectorClass               SystemVectorClass;

  typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

  NonZeroPatternClass nzPDE_q(nDOFPDE, nDOFPDE);

  JacobianCell_Galerkin<SurrealClass,TopoD3>::integrate( fcnCell, xfld, qfld, &quadratureOrderMin[0], 1, nzPDE_q );

  JacobianBoundaryTrace_Galerkin_PDE<SurrealClass,TopoD3>::integrate( fcnBCInflow, qfld, &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                      nzPDE_q );
  JacobianBoundaryTrace_Galerkin_PDE<SurrealClass,TopoD3>::integrate( fcnBCOutflow, qfld, &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                  nzPDE_q );
  JacobianBoundaryTrace_Galerkin_PDE<SurrealClass,TopoD3>::integrate( fcnBCwall, qfld, &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                  nzPDE_q );

  JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate( fcnWake, qfld, &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                               nzPDE_q );

  SystemVectorClass sln(nDOFPDE);
  SystemVectorClass rsd(nDOFPDE);

  // Solution and residual vector and jacobian matrix
  SystemMatrixClass jacPDE_q(nzPDE_q);

  rsd = 0;
  jacPDE_q = 0;

  ResidualCell_Galerkin<TopoD3>::integrate( fcnCell, xfld, qfld, &quadratureOrder[0], 1, rsd );

  JacobianCell_Galerkin<SurrealClass,TopoD3>::integrate( fcnCell, xfld, qfld, &quadratureOrder[0], 1, jacPDE_q );

  JacobianBoundaryTrace_Galerkin_PDE<SurrealClass,TopoD3>::integrate( fcnBCInflow, qfld, &quadratureOrder[0], quadratureOrder.size(),
                                                                  jacPDE_q );
  JacobianBoundaryTrace_Galerkin_PDE<SurrealClass,TopoD3>::integrate( fcnBCOutflow, qfld, &quadratureOrder[0], quadratureOrder.size(),
                                                                  jacPDE_q );
  JacobianBoundaryTrace_Galerkin_PDE<SurrealClass,TopoD3>::integrate( fcnBCwall, qfld, &quadratureOrder[0], quadratureOrder.size(),
                                                                  jacPDE_q );

  ResidualBoundaryTrace_Galerkin_PDE<TopoD3>::integrate( fcnBCInflow, qfld, &quadratureOrder[0], quadratureOrder.size(), rsd );
  ResidualBoundaryTrace_Galerkin_PDE<TopoD3>::integrate( fcnBCOutflow, qfld, &quadratureOrder[0], quadratureOrder.size(), rsd );
  ResidualBoundaryTrace_Galerkin_PDE<TopoD3>::integrate( fcnBCwall, qfld, &quadratureOrder[0], quadratureOrder.size(), rsd );

  Real rsdPDEnrm = 0;
  for (int n = 0; n < nDOFPDE; n++)
    rsdPDEnrm += pow(rsd[n],2);

  JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate( fcnWake, qfld, &quadratureOrder[0], quadratureOrder.size(),
                                                                               jacPDE_q );

  ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfld,
                                                                  &quadratureOrder[0], quadratureOrder.size(), rsd );


  //BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

  std::cout << "rsdPDEnrm = " << rsdPDEnrm << std::endl;


  std::cout << "Writing tmp/EllipticPotential_LIP.mtx" << std::endl;
  SLA::WriteMatrixMarketFile(jacPDE_q, "tmp/EllipticPotential_LIP.mtx");

//#ifdef INTEL_MKL
//    SLA::MKL_PARDISO<SystemMatrixClass> solver;
//#else
   SLA::UMFPACK<SystemMatrixClass> solver;
//#endif

  sln = solver.inverse(jacPDE_q)*rsd;

  // updated solution

  for (int k = 0; k < nDOFPDE; k++)
    qfld.DOF(k) -= sln[k];


  rsd = 0;
  ResidualCell_Galerkin<TopoD3>::integrate( fcnCell, xfld, qfld, &quadratureOrder[0], 1, rsd );

  ResidualBoundaryTrace_Galerkin_PDE<TopoD3>::integrate( fcnBCInflow, qfld, &quadratureOrder[0], quadratureOrder.size(), rsd );
  ResidualBoundaryTrace_Galerkin_PDE<TopoD3>::integrate( fcnBCOutflow, qfld, &quadratureOrder[0], quadratureOrder.size(), rsd );
  ResidualBoundaryTrace_Galerkin_PDE<TopoD3>::integrate( fcnBCwall, qfld, &quadratureOrder[0], quadratureOrder.size(), rsd );

  ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfld,
                                                                  &quadratureOrder[0], quadratureOrder.size(), rsd );

  rsdPDEnrm = 0;
  for (int n = 0; n < nDOFPDE; n++)
    rsdPDEnrm += pow(rsd[n],2);

  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

#if 1
  Field_CG_Cell<PhysD3, TopoD3, DLA::VectorS<1,Real> > rsdfld(xfld, order, BasisFunctionCategory_Hierarchical);
  for (int k = 0; k < nDOFPDE; k++)
    rsdfld.DOF(k) = rsd[k];
  output_Tecplot( rsdfld, "tmp/EllipticPotential_LIP_rsd.dat" );
#endif

#if 1
  // Tecplot dump
  output_Tecplot_LIP( pde, qfld, "tmp/EllipticPotential_LIP_Solve.dat" );
#endif

}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EllipticPotential_test )
{
  //return; //Don't do any testing because memcheck takes too long

  typedef SurrealS<1> SurrealClass;

  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D> PDEClass;
  typedef PDELinearizedIncompressiblePotential3D::template ArrayQ<Real> ArrayQ;
  typedef PDELinearizedIncompressiblePotential3D::template MatrixQ<Real> MatrixQ;

  typedef BCNDConvertSpace<PhysD3, BC<PDELinearizedIncompressiblePotential3D,BCTypeFunction<SolutionDummy> > > BCSolution;
  typedef BCNDConvertSpace<PhysD3, BC<PDELinearizedIncompressiblePotential3D,BCTypeNeumann> > BCNeumann;
  typedef BCNDConvertSpace<PhysD3, BC<PDELinearizedIncompressiblePotential3D,BCTypeWall> > BCWall;

  typedef IntegrandBoundaryTrace_LinearScalar_Galerkin<PDEClass, BCSolution> IntegrandBCSolution;
  typedef IntegrandBoundaryTrace_LinearScalar_Galerkin<PDEClass, BCNeumann> IntegrandBCNeumann;
  typedef IntegrandBoundaryTrace_LinearScalar_Galerkin<PDEClass, BCWall> IntegrandBCWall;
#ifdef LG_WAKE
  typedef IntegrandTrace_Galerkin_WakeCut_LIP<PDEClass> IntegrandWake;
#else
  //typedef IntegrandTrace_Galerkin_WakeCut_sansLG<PDEClass> IntegrandWake;
  typedef IntegrandTrace_Galerkin_WakeCut_Potential_Drela<PDEClass> IntegrandWake;
#endif
  typedef IntegrandCell_Galerkin<PDEClass> IntegrandCellClass;


  EGContext context((CreateContext()));

  // Refining the grid any more in the spanwise direction gives the incorrect integral
  // as the derivate of the elliptic potential field is singular at y = -1 and y = 1
  const int nSpan = 11;
  std::vector<double> wakeParams = {0.2*2.5, 0.001,15.};
  std::vector<double> EGADSParams = {0.2*2.5, 0.001, 15.0};
  // TetGen mesh parameters
  Real maxRadiusEdgeRatio = 1.5; //1.075;
  Real minDihedralAngle = 0;

  EGModel<3> model = makeWakedBox(context, nSpan, wakeParams);

  EGTetGen xfld(model, EGADSParams, maxRadiusEdgeRatio, minDihedralAngle);


  std::vector<int> WallFaces, InflowFaces, OutflowFaces, WakeFaces, LGFaces;

  {
    int BCFace = 0;
    std::vector< EGBody<3> > bodies = model.getBodies();
    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
    {
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        if ( faces[i].hasAttribute("Wall") )
        {
          WallFaces.push_back(BCFace);
          LGFaces.push_back(BCFace);
          BCFace++;
        }
        else if ( faces[i].hasAttribute("Inflow") || faces[i].hasAttribute("Lateral") )
        {
          InflowFaces.push_back(BCFace);
          LGFaces.push_back(BCFace);
          BCFace++;
        }
        else if ( faces[i].hasAttribute("Outflow") )
        {
          //InflowFaces.push_back(BCFace);
          OutflowFaces.push_back(BCFace);
          LGFaces.push_back(BCFace);
          BCFace++;
        }
      }
    }

    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ ) //TODO: This should first go over solid bodies then sheet bodies...
    {
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        if ( faces[i].hasAttribute("Wake") )
        {
          WakeFaces.push_back(BCFace);
          BCFace++;
        }
      }
    }
  }

  //BOOST_REQUIRE_EQUAL( WakeFaces.size(), 1 );


  // PDE
  Real U = 1, V = 0, W = 0;

  PDEClass pde( U, V, W );

  // BC
  EllipticPotential phi;
  Real outdata = 0.;

  BCSolution bcInflow( &phi );
  BCNeumann bcOutflow( outdata );
  BCWall bcwall( pde );

  // integrands
  IntegrandCellClass fcnCell( pde );
  IntegrandBCSolution fcnBCInflow( pde, bcInflow, InflowFaces );
  IntegrandBCNeumann fcnBCOutflow( pde, bcOutflow, OutflowFaces );
  IntegrandBCWall fcnBCwall( pde, bcwall, WallFaces );
  IntegrandWake fcnWake(pde, WakeFaces);

  int order = 1;

  // solution: Hierarchical, C0
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld( xfld, order, BasisFunctionCategory_Hierarchical );
  qfld = 0;
  const int nDOFPDE = qfld.nDOF();

#ifdef LG_WAKE
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order );
#else
  //Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, LGFaces );
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, 0, BasisFunctionCategory_Legendre, LGFaces );
#endif
  lgfld = 0;
  const int nDOFBC = lgfld.nDOF();

  std::cout << "nDOFPDE = " << nDOFPDE << " nDOFBC = " <<  nDOFBC << " Total = " << nDOFPDE+nDOFBC << std::endl;

  EGTetGen::FieldCellGroupType<Tet>& xCellGroup = xfld.getCellGroup<Tet>(0);
  EGTetGen::FieldCellGroupType<Tet>::ElementType<> xCellElem( xCellGroup.basis() );

  Field_CG_Cell<PhysD3, TopoD3, ArrayQ>::FieldCellGroupType<Tet>& qCellGroup = qfld.getCellGroup<Tet>(0);
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ>::FieldCellGroupType<Tet>::ElementType<> qCellElem( qCellGroup.basis() );


  for (int elem = 0; elem < xCellGroup.nElem(); elem++ )
  {
    xCellGroup.getElement(xCellElem, elem);
    qCellGroup.getElement(qCellElem, elem);

    for (int n = 0; n < xCellElem.nDOF(); n++)
      qCellElem.DOF(n) = phi(xCellElem.DOF(n));

    qCellGroup.setElement(qCellElem, elem);
  }

  if ( WakeFaces.size() > 0 )
  {
    EGTetGen::FieldTraceGroupType<Triangle>& xTraceGroup = xfld.getBoundaryTraceGroup<Triangle>(WakeFaces[0]);
#ifdef LG_WAKE
    EGTetGen::FieldTraceGroupType<Triangle>::ElementType<> xTraceElem( xTraceGroup.basis() );
    Field<PhysD3, TopoD3, ArrayQ>::FieldTraceGroupType<Triangle>& lgWakeGroup = lgfld.getBoundaryTraceGroup<Triangle>(WakeFaces[0]);
    Field<PhysD3, TopoD3, ArrayQ>::FieldTraceGroupType<Triangle>::ElementType<> lgTraceElem(lgWakeGroup.basis());
#endif

    // Traverse all elements on the wake, and perturb them so the solution from either
    // side of the wake is property initialized
    for (int elem = 0; elem < xTraceGroup.nElem(); elem++ )
    {
      int elemL = xTraceGroup.getElementLeft(elem);
#ifdef LG_WAKE
      xTraceGroup.getElement(xTraceElem, elem);
      lgWakeGroup.getElement(lgTraceElem, elem);
      for (int n = 0; n < xTraceElem.nDOF(); n++)
      {
        // Nudge the element so it's clearly above and below z = 0
        xTraceElem.DOF(n)[2] += 1e-12;
        lgTraceElem.DOF(n) = 0.5*phi(xTraceElem.DOF(n));
        xTraceElem.DOF(n)[2] -= 2*1e-12;
        lgTraceElem.DOF(n) += 0.5*phi(xTraceElem.DOF(n));
      }
      lgWakeGroup.setElement(lgTraceElem, elem);
#endif

      xCellGroup.getElement(xCellElem, elemL);
      qCellGroup.getElement(qCellElem, elemL);

      for (int n = 0; n < xCellElem.nDOF(); n++)
      {
        // Nudge the element so it's clearly above z = 0
        xCellElem.DOF(n)[2] += 1e-12;
        qCellElem.DOF(n) = phi(xCellElem.DOF(n));
      }

      qCellGroup.setElement(qCellElem, elemL);


      int elemR = xTraceGroup.getElementRight(elem);

      xCellGroup.getElement(xCellElem, elemR);
      qCellGroup.getElement(qCellElem, elemR);

      for (int n = 0; n < xCellElem.nDOF(); n++)
      {
        // Nudge the element so it's clearly below z = 0
        xCellElem.DOF(n)[2] -= 1e-12;
        qCellElem.DOF(n) = phi(xCellElem.DOF(n));
      }

      qCellGroup.setElement(qCellElem, elemR);
    }
  }
#if 0
  string filename = "tmp/EllipticPotential_LIP.dat";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot_LIP( pde, qfld, filename );
#endif



  // quadrature rule
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 1);    // max
  std::vector<int> quadratureOrderMin(xfld.nBoundaryTraceGroups(), 0);     // min



  // linear system setup

  typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
  typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
  typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
  typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;

  typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

  DLA::MatrixD<NonZeroPatternClass> nz = {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFBC} },
                                          { { nDOFBC, nDOFPDE}, { nDOFBC, nDOFBC} }};

  NonZeroPatternClass& nzPDE_q  = nz(0,0);
  NonZeroPatternClass& nzPDE_lg = nz(0,1);
  NonZeroPatternClass& nzBC_q   = nz(1,0);
  NonZeroPatternClass& nzBC_lg  = nz(1,1);

  JacobianCell_Galerkin<SurrealClass,TopoD3>::integrate( fcnCell, qfld, &quadratureOrderMin[0], 1, nzPDE_q );

  JacobianBoundaryTrace_Galerkin<SurrealClass,TopoD3>::integrate( fcnBCInflow, qfld, lgfld, &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                  nzPDE_q, nzPDE_lg, nzBC_q, nzBC_lg );
  JacobianBoundaryTrace_Galerkin<SurrealClass,TopoD3>::integrate( fcnBCOutflow, qfld, lgfld, &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                  nzPDE_q, nzPDE_lg, nzBC_q, nzBC_lg );
  JacobianBoundaryTrace_Galerkin<SurrealClass,TopoD3>::integrate( fcnBCwall, qfld, lgfld, &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                  nzPDE_q, nzPDE_lg, nzBC_q, nzBC_lg );
#ifdef LG_WAKE
  JacobianBoundaryTrace_Galerkin_WakeCut_LIP<SurrealClass,TopoD3>::integrate( fcnWake, qfld, lgfld,
                                                                              &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                              nzPDE_q, nzPDE_lg, nzBC_q, nzBC_lg );
#else
  JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate( fcnWake, qfld, &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                               nzPDE_q );
#endif

  SystemVectorClass sln = {{nDOFPDE},{nDOFBC}};
  SystemVectorClass rsd = {{nDOFPDE},{nDOFBC}};

  // Solution and residual vector and jacobian matrix
  SystemMatrixClass jac(nz);

  rsd = 0;
  jac = 0;

  SparseMatrixClass& jacPDE_q  = jac(0,0);
  SparseMatrixClass& jacPDE_lg = jac(0,1);
  SparseMatrixClass& jacBC_q   = jac(1,0);
  SparseMatrixClass& jacBC_lg  = jac(1,1);

  ResidualCell_Galerkin<TopoD3>::integrate( fcnCell, qfld, &quadratureOrder[0], 1, rsd(0) );

  JacobianCell_Galerkin<SurrealClass,TopoD3>::integrate( fcnCell, qfld, &quadratureOrder[0], 1, jacPDE_q );

  JacobianBoundaryTrace_Galerkin<SurrealClass,TopoD3>::integrate( fcnBCInflow, qfld, lgfld, &quadratureOrder[0], quadratureOrder.size(),
                                                                  jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg );
  JacobianBoundaryTrace_Galerkin<SurrealClass,TopoD3>::integrate( fcnBCOutflow, qfld, lgfld, &quadratureOrder[0], quadratureOrder.size(),
                                                                  jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg );
  JacobianBoundaryTrace_Galerkin<SurrealClass,TopoD3>::integrate( fcnBCwall, qfld, lgfld, &quadratureOrder[0], quadratureOrder.size(),
                                                                  jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg );

  ResidualBoundaryTrace_Galerkin_LG<TopoD3>::integrate( fcnBCInflow, qfld, lgfld, &quadratureOrder[0], quadratureOrder.size(), rsd[0], rsd[1] );
  ResidualBoundaryTrace_Galerkin_LG<TopoD3>::integrate( fcnBCOutflow, qfld, lgfld, &quadratureOrder[0], quadratureOrder.size(), rsd[0], rsd[1] );
  ResidualBoundaryTrace_Galerkin_LG<TopoD3>::integrate( fcnBCwall, qfld, lgfld, &quadratureOrder[0], quadratureOrder.size(), rsd[0], rsd[1] );

#ifdef LG_WAKE
  JacobianBoundaryTrace_Galerkin_WakeCut_LIP<SurrealClass,TopoD3>::integrate( fcnWake, qfld, lgfld, &quadratureOrder[0], quadratureOrder.size(),
                                                                              jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg );
#else
  JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate( fcnWake, qfld, &quadratureOrder[0], quadratureOrder.size(),
                                                                               jacPDE_q );
#endif


#ifdef LG_WAKE
  ResidualBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::integrate( fcnWake, qfld, lgfld,
                                                                 &quadratureOrder[0], quadratureOrder.size(), rsd[0], rsd[1] );
#else
  ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfld,
                                                                  &quadratureOrder[0], quadratureOrder.size(), rsd[0] );
#endif

  Real rsdPDEnrm = 0;
  for (int n = 0; n < nDOFPDE; n++)
    rsdPDEnrm += pow(rsd[0][n],2);

  //BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

  Real rsdBCnrm = 0;
  for (int n = 0; n < nDOFBC; n++)
    rsdBCnrm += pow(rsd[1][n],2);

  //BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

  std::cout << "rsdPDEnrm = " << rsdPDEnrm << " rsdBCnrm = " << rsdBCnrm << std::endl;


  std::cout << "Writing tmp/EllipticPotential_LIP.mtx" << std::endl;
  SLA::WriteMatrixMarketFile(jac, "tmp/EllipticPotential_LIP.mtx");

//#ifdef INTEL_MKL
//    SLA::MKL_PARDISO<SystemMatrixClass> solver;
//#else
   SLA::UMFPACK<SystemMatrixClass> solver;
//#endif

  sln = solver.inverse(jac)*rsd;

  // updated solution

  for (int k = 0; k < nDOFPDE; k++)
    qfld.DOF(k) -= sln[0][k];

  for (int k = 0; k < nDOFBC; k++)
    lgfld.DOF(k) -= sln[1][k];

  Field_CG_Cell<PhysD3, TopoD3, DLA::VectorS<1,Real> > rsdfld(xfld, order);

  for (int k = 0; k < nDOFPDE; k++)
    rsdfld.DOF(k) = rsd[0][k];


  rsd = 0;
  ResidualCell_Galerkin<TopoD3>::integrate( fcnCell, qfld, &quadratureOrder[0], 1, rsd(0) );

  ResidualBoundaryTrace_Galerkin_LG<TopoD3>::integrate( fcnBCInflow, qfld, lgfld, &quadratureOrder[0], quadratureOrder.size(), rsd[0], rsd[1] );
  ResidualBoundaryTrace_Galerkin_LG<TopoD3>::integrate( fcnBCOutflow, qfld, lgfld, &quadratureOrder[0], quadratureOrder.size(), rsd[0], rsd[1] );
  ResidualBoundaryTrace_Galerkin_LG<TopoD3>::integrate( fcnBCwall, qfld, lgfld, &quadratureOrder[0], quadratureOrder.size(), rsd[0], rsd[1] );

#ifdef LG_WAKE
  ResidualBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::integrate( fcnWake, qfld, lgfld,
                                                                 &quadratureOrder[0], quadratureOrder.size(), rsd[0], rsd[1] );
#else
  ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfld,
                                                                  &quadratureOrder[0], quadratureOrder.size(), rsd[0] );
#endif

  rsdPDEnrm = 0;
  for (int n = 0; n < nDOFPDE; n++)
    rsdPDEnrm += pow(rsd[0][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

  rsdBCnrm = 0;
  for (int n = 0; n < nDOFBC; n++)
    rsdBCnrm += pow(rsd[1][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

#if 1
  // Tecplot dump
  //output_Tecplot( rsdfld, "tmp/EllipticPotential_LIP_rsd.dat" );
  output_Tecplot_LIP( pde, qfld, "tmp/EllipticPotential_LIP_Solve.dat" );
#endif

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
