// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianBoundaryFrame_Galerkin_LIP_btest
// testing of frame-integral jacobian: LIP Kutta condition

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"
#include "pde/FullPotential/BCLinearizedIncompressiblePotential3D.h"
#include "Field/output_Tecplot.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Discretization/Galerkin/JacobianBoundaryFrame_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryFrame_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "Surreal/SurrealS.h"

#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGADS/extrude.h"
#include "Meshing/EGADS/solidBoolean.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

using namespace std;
using namespace SANS;
using namespace EGADS;


namespace           // local definition
{

//----------------------------------------------------------------------------//
// jacobian dumps
template <int N>
void
dump( Real jac[][N], int m, int n )
{
  cout << endl << "{";
  for (int i = 0; i < m; i++)
  {
    cout << "{";
    for (int j = 0; j < n; j++)
    {
      cout << jac[i][j];
      if (j < n-1) cout << ", ";
    }
    cout << "}";
    if (i < m-1) cout << ","  << endl << " ";
  }
  cout << "}";
}

void
dump( Real jac[][3], int m, int n )
{
  cout << "{";
  for (int i = 0; i < m; i++)
  {
    cout << "{";
    for (int j = 0; j < n; j++)
    {
      cout << jac[i][j];
      if (j < n-1) cout << ", ";
    }
    cout << "}";
    if (i < m-1) cout << ","  << endl << " ";
  }
  cout << "}";
}

}                   // local definition


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

EGModel<3> makeWakedBox(const EGContext& context,
                        const std::vector<double>& wakeParams)
{
  //Create the farfield box that is slightly larger than the Trefftz plane wake intersection
  // defined as x = 0.1, y in [-1,1], z = 0
  std::vector<double> Box = {0.0, -1.1, -0.1,
                             0.1,  2.20, 0.2};
  EGBody<3> box = context.makeSolidBody( BOX, Box );

  EGNode<3> nodeL(context, {-0.1,-1,0});
  EGNode<3> nodeR(context, {-0.1, 1,0});

  EGEdge<3> wakeedge(nodeL, nodeR);

  Real len = 5;
  DLA::VectorS<3,Real> dir = {1, 0., 0};

  //Nodes outside the box
  EGNode<3> outL(context, {-0.1, -2, 0.});
  EGNode<3> outR(context, {-0.1,  2, 0.});

  //Edges that extend from the wing tips outside the box
  EGEdge<3> edgeTipL (outL, nodeL);
  EGEdge<3> edgeTipR (nodeR, outR);

  EGLoop<3>::edge_vector wakeEdges;

  wakeEdges = {(edgeTipL,1),(wakeedge,1),(edgeTipR,1)};

  // Surface used to create the wake and cut the box
  EGBody<3> wakecutter = extrude( EGLoop<3>(context, wakeEdges, OPEN), len, dir );

  EGBody<3> boxscribed( EGIntersection<3>(box, wakecutter) );

  EGBody<3> wakelong = extrude( EGLoop<3>((wakeedge,1), OPEN), len, dir );

  EGBody<3> wake = intersect( EGModel<3>(wakelong), box ).getBodies()[0].clone();

  //Set the tess resolution on the wake
  std::vector< EGFace<3> > wakefaces = wake.getFaces();
  for ( auto face = wakefaces.begin(); face != wakefaces.end(); face++ )
  {
    face->addAttribute(".tParams", wakeParams);
    face->addAttribute("Wake", 1);
  }

  typedef EGNode<3>::CartCoord CartCoord;
  std::vector< EGEdge<3> > WakeEdges = wake.getEdges();
  for ( auto edge = WakeEdges.begin(); edge != WakeEdges.end(); edge++ )
  {
    std::vector<EGNode<3>> nodes = edge->getNodes();

    if (nodes.size() == 1 ) continue;

    if ( ((CartCoord)nodes[0])[0] > Box[3]-1e-6 &&
         ((CartCoord)nodes[1])[0] > Box[3]-1e-6 )
    {
      edge->addAttribute("Trefftz", 1); // Mark the edge that is used for Treffetz plane integrals
    }
    else if ( ((CartCoord)nodes[0])[0] < Box[0]+1e-6 &&
         ((CartCoord)nodes[1])[0] < Box[0]+1e-6 )
    {
      edge->addAttribute("Kutta", 2); // Mark the edge that is used for Kutta condition integrals
    }

  }

  return EGModel<3>({wake, boxscribed});
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianBoundaryFrame_Galerkin_test_suite )


//typedef boost::mpl::list< SurrealS<1>, SurrealS<2> > Surreals;
typedef boost::mpl::list< SurrealS<1> > Surreals;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin3D_X1Q1, SurrealClass, Surreals )
{

  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  typedef IntegrandBoundaryFrame_Galerkin<PDEClass> IntegrandClass;

  // PDE

  Real U = 1.1;
  Real V = 0.2;
  Real W = 0.5;

  PDEClass pde( U, V, W );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // EGADS parameters
  std::vector<double> wakeParams = {2,0.001,15.};
  std::vector<double> EGADSParams = {2, 0.001, 15.0};
  // TetGen mesh parameters
  Real maxRadiusEdgeRatio = 2;
  Real minDihedralAngle = 0;

  EGContext context((CreateContext()));
  EGModel<3> model = makeWakedBox(context, wakeParams);

  // Create the mesh
  EGTetGen xfld(model, EGADSParams, maxRadiusEdgeRatio, minDihedralAngle);


  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  const int nDOF = qfld.nDOF();

  EllipticPotential phi;

  EGTetGen::FieldCellGroupType<Tet>& xCellGroup = xfld.getCellGroup<Tet>(0);
  EGTetGen::FieldCellGroupType<Tet>::ElementType<> xCellElem( xCellGroup.basis() );

  Field_CG_Cell<PhysD3, TopoD3, ArrayQ>::FieldCellGroupType<Tet>& qCellGroup = qfld.getCellGroup<Tet>(0);
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ>::FieldCellGroupType<Tet>::ElementType<> qCellElem( qCellGroup.basis() );

  for (int elem = 0; elem < xCellGroup.nElem(); elem++ )
  {
    xCellGroup.getElement(xCellElem, elem);
    qCellGroup.getElement(qCellElem, elem);

    for (int n = 0; n < xCellElem.nDOF(); n++)
      qCellElem.DOF(n) = phi(xCellElem.DOF(n));

    qCellGroup.setElement(qCellElem, elem);
  }

#if 0
  string filename = "tmp/JacobianBoundaryFrame_LIP.dat";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot_LIP( pde, qfld, filename );
#endif

  // integrand
  IntegrandClass fcn( pde, {1}, 0 );

  // quadrature rule
  int quadratureOrder[7] = {1,1,1,1,1,1,1};

  // jacobian via FD w/ residual operator; assumes scalar PDE
  // topology-specific single group interface

  SLA::SparseVector<ArrayQ> rsdPDEGlobal0(nDOF), rsdPDEGlobal1(nDOF);
  DLA::MatrixD<Real> jacPDE_q(nDOF,nDOF);

  rsdPDEGlobal0 = 0;

  ResidualBoundaryFrame_Galerkin<TopoD3>::integrate(fcn, xfld, qfld, quadratureOrder, xfld.nBoundaryFrameGroups(), rsdPDEGlobal0 );

  // jacobian wrt solution
  for (int j = 0; j < nDOF; j++)
  {
    qfld.DOF(j) += 1;

    rsdPDEGlobal1 = 0;

    ResidualBoundaryFrame_Galerkin<TopoD3>::integrate(fcn, xfld, qfld, quadratureOrder, xfld.nBoundaryFrameGroups(), rsdPDEGlobal1 );

    for (int i = 0; i < nDOF; i++)
      jacPDE_q(i,j) = rsdPDEGlobal1[i] - rsdPDEGlobal0[i];

    qfld.DOF(j) -= 1;
  }

#if 0
  cout << "FD: jacPDE_q  = ";  jacPDE_q.dump();  cout << endl;
#endif

  // jacobian via Surreal
  // topology-specific single group interface

  DLA::MatrixD<MatrixQ> mtxGlobPDE_q(nDOF,nDOF);

  mtxGlobPDE_q = 0;

  JacobianBoundaryFrame_Galerkin<SurrealClass,TopoD3>::integrate(fcn, xfld, qfld, quadratureOrder, xfld.nBoundaryFrameGroups(), mtxGlobPDE_q );


#if 0
  cout << "Surreal: mtxGlobPDE_q  = " << endl;  mtxGlobPDE_q.dump(2);
#endif

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;
  for (int i = 0; i < nDOF; i++)
  {
    for (int j = 0; j < nDOF; j++)
       SANS_CHECK_CLOSE( jacPDE_q(i,j), mtxGlobPDE_q(i,j), small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( avoid_compiler_warnings )
{
  Real jac1[2][2] = { {0, 0}, {0, 0} };
  Real jac2[3][3] = { {0, 0, 0}, {0, 0, 0}, {0, 0, 0} };

  cout.setstate( std::ios_base::badbit );
  dump( jac1, 2, 2 );
  dump( jac2, 3, 3 );
  cout.clear();
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
