// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UpdateFractionCell_Euler_btest
// testing of cell physicality check for Galerkin with Euler

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"

#include "Discretization/UpdateFraction/UpdateFractionCell.h"
#include "Discretization/UpdateFraction/UpdateFractionInteriorTrace.h"
#include "Discretization/UpdateFraction/UpdateFractionBoundaryTrace_sansLG.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( UpdateFraction_Euler_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( UpdateFractionCell_2D_1Triangle_X1Q0 )
{
  // Working with PrimitiveRhoPressure so the updateFraction equations are linear
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  Real rho, u, v, t;
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  Real maxChangeFraction, updateFraction;

  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  ArrayQ dq;

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: single triangle, P1 (aka Q1)
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  Field<PhysD2, TopoD2, ArrayQ> dqfld(qfld, FieldCopy());

  // solution data
  qfld.DOF(0) = q;

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 1;

  // Delta less than the maximum change request
  maxChangeFraction = 0.1;
  updateFraction = 1;
  dqfld.DOF(0) = 0.01*q;

  IntegrateCellGroups<TopoD2>::integrate( UpdateFractionCell(pde, {0}, maxChangeFraction, updateFraction),
                                          xfld, (qfld, dqfld), &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( 1, updateFraction );

  // Delta greater than the maximum change request
  maxChangeFraction = 0.1;
  updateFraction = 1;
  dqfld.DOF(0) = 0.5*q;

  IntegrateCellGroups<TopoD2>::integrate( UpdateFractionCell(pde, {0}, maxChangeFraction, updateFraction),
                                          xfld, (qfld, dqfld), &quadratureOrder, 1 );
  BOOST_CHECK_CLOSE( 0.2, updateFraction, 1e-10 );

  // Delta greater than the maximum change request (negated)
  maxChangeFraction = 0.1;
  updateFraction = 1;
  dqfld.DOF(0) = -0.5*q;

  IntegrateCellGroups<TopoD2>::integrate( UpdateFractionCell(pde, {0}, maxChangeFraction, updateFraction),
                                          xfld, (qfld, dqfld), &quadratureOrder, 1 );
  BOOST_CHECK_CLOSE( 0.2, updateFraction, 1e-10 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( UpdateFractionInteriorTrace_2D_2Triangle_X1Q0 )
{
  // Working with PrimitiveRhoPressure so the updateFraction equations are linear
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  typedef BCNDConvertSpace<PhysD2, BCEuler2D<BCTypeFullState_mitState, PDEClass>> BCClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  Real rho, u, v, t;
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  BCClass bc( pde, DensityVelocityTemperature2D<Real>(rho, u, v, t), true );

  Real maxChangeFraction, updateFraction;

  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  ArrayQ dq;

  // grid: two triangles, P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: single triangle, P1 (aka Q1)
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  Field<PhysD2, TopoD2, ArrayQ> dqfld(qfld, FieldCopy());

  // solution data
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;

  dqfld = 0;

  // quadrature rule (linear: basis grad is const, flux is linear)
  std::vector<int> quadratureOrder = {1};

  // Delta less than the maximum change request
  maxChangeFraction = 0.1;
  updateFraction = 1;
  dqfld.DOF(0) = 0.01*q;

  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      UpdateFractionInteriorTrace(pde, {0}, maxChangeFraction, updateFraction),
      xfld, (qfld, dqfld), quadratureOrder.data(), quadratureOrder.size() );
  BOOST_CHECK_EQUAL( 1, updateFraction );

  // Delta greater than the maximum change request
  maxChangeFraction = 0.1;
  updateFraction = 1;
  dqfld.DOF(0) = 0.5*q;

  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      UpdateFractionInteriorTrace(pde, {0}, maxChangeFraction, updateFraction),
      xfld, (qfld, dqfld), quadratureOrder.data(), quadratureOrder.size() );
  BOOST_CHECK_CLOSE( 0.2, updateFraction, 1e-10 );

  // Delta greater than the maximum change request (negated)
  maxChangeFraction = 0.1;
  updateFraction = 1;
  dqfld.DOF(0) = -0.5*q;

  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      UpdateFractionInteriorTrace(pde, {0}, maxChangeFraction, updateFraction),
      xfld, (qfld, dqfld), quadratureOrder.data(), quadratureOrder.size() );
  BOOST_CHECK_CLOSE( 0.2, updateFraction, 1e-10 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( UpdateFractionBoundaryTrace_2D_1Triangle_X1Q0 )
{
  // Working with PrimitiveRhoPressure so the updateFraction equations are linear
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  typedef BCNDConvertSpace<PhysD2, BCEuler2D<BCTypeFullState_mitState, PDEClass>> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  Real rho, u, v, t;
  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  BCClass bc( pde, DensityVelocityTemperature2D<Real>(rho, u, v, t), true );

  Real maxChangeFraction, updateFraction;

  ArrayQ q = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  ArrayQ dq;

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // solution: single triangle, P1 (aka Q1)
  int qorder = 0;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);
  Field<PhysD2, TopoD2, ArrayQ> dqfld(qfld, FieldCopy());

  // solution data
  qfld.DOF(0) = q;

  // quadrature rule (linear: basis grad is const, flux is linear)
  std::vector<int> quadratureOrder = {1, 1, 1};

  // Delta less than the maximum change request
  maxChangeFraction = 0.1;
  updateFraction = 1;
  dqfld.DOF(0) = 0.01*q;

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      UpdateFractionBoundaryTrace_sansLG<NDBCVecCat>(pde, bc, {0}, maxChangeFraction, updateFraction),
      xfld, (qfld, dqfld), quadratureOrder.data(), quadratureOrder.size() );
  BOOST_CHECK_EQUAL( 1, updateFraction );

  // Delta greater than the maximum change request
  maxChangeFraction = 0.1;
  updateFraction = 1;
  dqfld.DOF(0) = 0.5*q;

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      UpdateFractionBoundaryTrace_sansLG<NDBCVecCat>(pde, bc, {0}, maxChangeFraction, updateFraction),
      xfld, (qfld, dqfld), quadratureOrder.data(), quadratureOrder.size() );
  BOOST_CHECK_CLOSE( 0.2, updateFraction, 1e-10 );

  // Delta greater than the maximum change request (negated)
  maxChangeFraction = 0.1;
  updateFraction = 1;
  dqfld.DOF(0) = -0.5*q;

  IntegrateBoundaryTraceGroups<TopoD2>::integrate(
      UpdateFractionBoundaryTrace_sansLG<NDBCVecCat>(pde, bc, {0}, maxChangeFraction, updateFraction),
      xfld, (qfld, dqfld), quadratureOrder.data(), quadratureOrder.size() );
  BOOST_CHECK_CLOSE( 0.2, updateFraction, 1e-10 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
