// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// isValidStateInteriorTrace_Euler_btest
// testing of trace physicality check for Galerkin with Euler

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler2D.h"

#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"

#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"
#include "Discretization/isValidState/isValidStateInteriorTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( isValidStateInteriorTrace_AD_test_suite )
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidStateInteriorTrace_1D_1Line_X1Q1_1Group )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef IntegrandInteriorTrace_Galerkin<NDPDEClass> IntegrandClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas);

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 3 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  Real rho, u, t;

  rho = 1.137; u = 0.784; t = 0.987;

  // set
  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  // grid: P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qfld.nDOF() == 6 );

  // triangle solution data (left)
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;

  // triangle solution data (right)
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 1;
  bool isValidState;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  isValidState = true;

  IntegrateInteriorTraceGroups<TopoD1>::integrate(
      isValidStateInteriorTrace(pde, fcnint.interiorTraceGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, true );

  // Now for a non-physical density
  rho = -1.137;
  qDataPrim[0] = rho;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;
  isValidState = true;
  IntegrateInteriorTraceGroups<TopoD1>::integrate(
      isValidStateInteriorTrace(pde, fcnint.interiorTraceGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical temperature
  rho = 1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[2] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;
  isValidState = true;
  IntegrateInteriorTraceGroups<TopoD1>::integrate(
      isValidStateInteriorTrace(pde, fcnint.interiorTraceGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical density and temperature
  rho = -1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[2] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;
  isValidState = true;
  IntegrateInteriorTraceGroups<TopoD1>::integrate(
      isValidStateInteriorTrace(pde, fcnint.interiorTraceGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( isValidStateInteriorTrace_2D_2Triangle_X1Q1_1Group )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef IntegrandInteriorTrace_Galerkin<NDPDEClass> IntegrandClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  Real rho, u, v, t;

  rho = 1.137; u = 0.784; v = -0.231; t = 0.987;

  // set
  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY", "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  // grid: single triangle, P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qfld.nDOF() == 6 );

  // triangle solution data (left)
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;

  // triangle solution data (right)
  qfld.DOF(3) = q;
  qfld.DOF(4) = q;
  qfld.DOF(5) = q;

  // quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureOrder = 1;
  bool isValidState;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  isValidState = true;

  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      isValidStateInteriorTrace(pde, fcnint.interiorTraceGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, true );

  // Now for a non-physical density
  rho = -1.137;
  qDataPrim[0] = rho;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;
  qfld.DOF(4) = q;
  qfld.DOF(5) = q;

  isValidState = true;
  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      isValidStateInteriorTrace(pde, fcnint.interiorTraceGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical temperature
  rho = 1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[3] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;
  qfld.DOF(4) = q;
  qfld.DOF(5) = q;

  isValidState = true;
  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      isValidStateInteriorTrace(pde, fcnint.interiorTraceGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );

  // Now for a non-physical density and temperature
  rho = -1.137; t = -0.987;
  qDataPrim[0] = rho;
  qDataPrim[3] = t;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );
  qfld.DOF(0) = q;
  qfld.DOF(1) = q;
  qfld.DOF(2) = q;
  qfld.DOF(3) = q;
  qfld.DOF(4) = q;
  qfld.DOF(5) = q;

  isValidState = true;
  IntegrateInteriorTraceGroups<TopoD2>::integrate(
      isValidStateInteriorTrace(pde, fcnint.interiorTraceGroups(), isValidState), xfld, qfld, &quadratureOrder, 1 );
  BOOST_CHECK_EQUAL( isValidState, false );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
