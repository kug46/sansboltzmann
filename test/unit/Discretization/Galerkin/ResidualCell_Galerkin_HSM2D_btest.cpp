// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
  // ResidualCell_Galerkin_HSM2D_btest
// testing of cell residual functions for Galerkin: PDEHSM2D

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"

#include "Surreal/SurrealS.h"

#include "pde/HSM/PDEHSM2D.h"
#include "pde/HSM/ComplianceMatrix.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/Tuple/FieldTuple.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_HSM2D.h"
#include "Discretization/IntegrateCellGroups.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualCell_Galerkin_HSM2D_test_suite )


template<class T_, class Tc_, class Tm_, class Tq_, class Tr_>
struct SurrealCombo
{
  typedef T_ T;
  typedef Tc_ Tc;
  typedef Tm_ Tm;
  typedef Tq_ Tq;
  typedef Tr_ Tr;
};

typedef boost::mpl::list< SurrealCombo<Real,Real,Real,Real,Real>,
                          SurrealCombo<Real,SurrealS<1>,SurrealS<1>,Real,Real>
                        > TTypes;

template<int N, class T>
T SurrealValue(const SurrealS<N,T>& s) { return s.value(); }

Real SurrealValue(const Real& s) { return s; }

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin_1Triangle_2D_X1Q1, Combo, TTypes )
{
  typedef typename Combo::T T;
  typedef typename Combo::Tc Tc;
  typedef typename Combo::Tm Tm;
  typedef typename Combo::Tq Tq;
  typedef typename Combo::Tr Tr;
  typedef typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type Ts;

  typedef typename PDEHSM2D<VarTypeLambda>::template VectorX<Real> VectorX_R;
  typedef typename PDEHSM2D<VarTypeLambda>::template VectorX<Tq> VectorX_Tq;
  typedef typename PDEHSM2D<VarTypeLambda>::template VectorE<Tr> VectorE;

  typedef PDENDConvertSpace<PhysD2,  PDEHSM2D<VarTypeLambda> > PDEClass;

  typedef typename PDEClass::template ArrayQ<T> ArrayQ;
  typedef typename PDEClass::template ArrayQ<Ts> ArrayQS;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ_R;
  typedef typename PDEClass::template ComplianceMatrix<Tc> ComplianceMatrix;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  VectorX_R g = { 0.023, -9.81 };
  PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 6 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single triangle, P1 (aka Q1)
  int qorder = 1;

  // Extensional compliance matrix CG field
  Field_CG_Cell<PhysD2, TopoD2, ComplianceMatrix> Ainvfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  Real E_00 = 70e9, nu_00 = 1./3.,  t_00 = 1./100.;
  Real E_10 = 80e9, nu_10 = 1./4.,  t_10 = 1./200.;
  Real E_01 = 90e7, nu_01 = 5./11., t_01 = 1./400.;

  ComplianceMatrix AijInv_00 = calcComplianceMatrix( E_00, nu_00, t_00 );
  ComplianceMatrix AijInv_10 = calcComplianceMatrix( E_10, nu_10, t_10 );
  ComplianceMatrix AijInv_01 = calcComplianceMatrix( E_01, nu_01, t_01 );

  Ainvfld.DOF(0) = AijInv_00;
  Ainvfld.DOF(1) = AijInv_10;
  Ainvfld.DOF(2) = AijInv_01;

  // Lumped shell mass CG field
  Field_CG_Cell<PhysD2, TopoD2, Tm> mufld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  Real mu_00 = 0.0123;
  Real mu_10 = 3.923;
  Real mu_01 = 0.946;

  mufld.DOF(0) = mu_00;
  mufld.DOF(1) = mu_10;
  mufld.DOF(2) = mu_01;

  // Following force CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorE> qefld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  VectorE qe_00 = { -3   ,  4./7. };
  VectorE qe_10 = { 2./5., -2./3. };
  VectorE qe_01 = { 1./2.,  1./4. };

  qefld.DOF(0) = qe_00;
  qefld.DOF(1) = qe_10;
  qefld.DOF(2) = qe_01;

  // Global forcing CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorX_Tq> qxfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  VectorX_Tq qx_00 = {  23.  , 0.0123 };
  VectorX_Tq qx_10 = { 4.935, -2.77 };
  VectorX_Tq qx_01 = { -9.54, -0.32 };

  qxfld.DOF(0) = qx_00;
  qxfld.DOF(1) = qx_10;
  qxfld.DOF(2) = qx_01;

  // Accelerations CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorX_Tq> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  VectorX_Tq a_00 = { 3.245, -1./3. };
  VectorX_Tq a_10 = { 1.43 , 8.79 };
  VectorX_Tq a_01 = { 0.034, 23.12 };

  afld.DOF(0) = a_00;
  afld.DOF(1) = a_10;
  afld.DOF(2) = a_01;

  // Solution variable CG field
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> varfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, varfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, varfld.nElem() );

  // State vector solution at first cell corner {0,0}
  Real   rx_00 = -1;
  Real   ry_00 = -1;
  Real lam3_00 = 3./5.;
  Real  f11_00 = 3;
  Real  f22_00 = -2./5.;
  Real  f12_00 = 1./2.;

  // State vector solution at second cell corner {1,0}
  Real   rx_10 = 1;
  Real   ry_10 = -1./2.;
  Real lam3_10 = 5./13.;
  Real  f11_10 = 1;
  Real  f22_10 = -1./2.;
  Real  f12_10 = 2;

  // State vector solution at third cell corner {0,1}
  Real   rx_01 = -1./2.;
  Real   ry_01 = 2;
  Real lam3_01 = 7./25.;
  Real  f11_01 = -5;
  Real  f22_01 = 9;
  Real  f12_01 = 2;

  // Triangle solution
  varfld.DOF(0) = pde.setDOFFrom( PositionLambdaForces2D(rx_00, ry_00, lam3_00, f11_00, f22_00, f12_00) );
  varfld.DOF(1) = pde.setDOFFrom( PositionLambdaForces2D(rx_10, ry_10, lam3_10, f11_10, f22_10, f12_10) );
  varfld.DOF(2) = pde.setDOFFrom( PositionLambdaForces2D(rx_01, ry_01, lam3_01, f11_01, f22_01, f12_01) );

  // Quadrature rule
  /*
   Quad order 8
   Error: < 3e-8%

   Quad order 9
   Error: < 3e-12%

   USE MAX QUAD
   */
  int quadratureOrder = -1;
  const Real small_tol = 3e-12;
  const Real close_tol = 3e-12;
  SLA::SparseVector<ArrayQS> residual(3);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // grid integration (just one element)
  residual = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, residual),
                                          (Ainvfld, mufld, qefld, qxfld, afld, xfld),
                                          varfld,
                                          &quadratureOrder, 1 );

  Real eps11Res;
  Real eps22Res;
  Real eps12Res;
  Real f1Res;
  Real f2Res;
  Real e3Res;
  ArrayQ_R residualTrue[3];

  // PDE residuals: (source) + (advective flux)

  // Basis function 1
  eps11Res = ( 0.7310422798964399 ) + ( 0 );
  eps22Res = ( 0.2272910254055126 ) + ( 0 );
  eps12Res = ( 0.1347310365960171 ) + ( 0 );
  f1Res    = ( -2.264766817818473 ) + ( -0.2712925463509729 );
  f2Res    = ( -3.683638827278987 ) + ( 1.098543979141634 );
  e3Res    = ( 1.395322358930152 ) + ( 0 );
  residualTrue[0] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 2
  eps11Res = ( 0.698229314835413 ) + ( 0 );
  eps22Res = ( 0.2601039947294995 ) + ( 0 );
  eps12Res = ( 0.1874255246914321 ) + ( 0 );
  f1Res    = ( -4.966956438909008 ) + ( 0.725527809868143 );
  f2Res    = ( -5.6990024085711 ) + ( 0.4138708737274842 );
  e3Res    = ( 1.305080587017079 ) + ( 0 );
  residualTrue[1] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 3
  eps11Res = ( 0.6762331516750987 ) + ( 0 );
  eps22Res = ( 0.2821001203581534 ) + ( 0 );
  eps12Res = ( 0.2082161046689848 ) + ( 0 );
  f1Res    = ( -3.661482280707458 ) + ( -0.4542352635171701 );
  f2Res    = ( -4.263250082131023 ) + ( -1.512414852869118 );
  e3Res    = ( 1.25239287695413 ) + ( 0 );
  residualTrue[2] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( residualTrue[0][n], SurrealValue(residual[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( residualTrue[1][n], SurrealValue(residual[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( residualTrue[2][n], SurrealValue(residual[2][n]), small_tol, close_tol );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Galerkin_1Quad_2D_X1Q1, Combo, TTypes )
{
  typedef typename Combo::T T;
  typedef typename Combo::Tc Tc;
  typedef typename Combo::Tm Tm;
  typedef typename Combo::Tq Tq;
  typedef typename Combo::Tr Tr;
  typedef typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type Ts;

  typedef typename PDEHSM2D<VarTypeLambda>::template VectorX<Real> VectorX_R;
  typedef typename PDEHSM2D<VarTypeLambda>::template VectorX<Tq> VectorX_Tq;
  typedef typename PDEHSM2D<VarTypeLambda>::template VectorE<Tr> VectorE;

  typedef PDENDConvertSpace<PhysD2,  PDEHSM2D<VarTypeLambda> > PDEClass;

  typedef typename PDEClass::template ArrayQ<T> ArrayQ;
  typedef typename PDEClass::template ArrayQ<Ts> ArrayQS;
  typedef typename PDEClass::template ComplianceMatrix<Tc> ComplianceMatrix;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;

  VectorX_R g = { 0.023, -9.81 };
  PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 6 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Quad_X1_1Group xfld;

  BOOST_CHECK_EQUAL( 4, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfld.nElem() );

  // solution: single quad, P1 (aka Q1)
  int qorder = 1;

  // Extensional compliance matrix CG field
  Field_CG_Cell<PhysD2, TopoD2, ComplianceMatrix> Ainvfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  Real E_00 = 70e9, nu_00 = 1./3.,  t_00 = 1./100.;
  Real E_10 = 80e9, nu_10 = 1./4.,  t_10 = 1./200.;
  Real E_11 = 90e9, nu_11 = 1./5.,  t_11 = 1./300.;
  Real E_01 = 90e7, nu_01 = 5./11., t_01 = 1./400.;

  ComplianceMatrix AijInv_00 = calcComplianceMatrix( E_00, nu_00, t_00 );
  ComplianceMatrix AijInv_10 = calcComplianceMatrix( E_10, nu_10, t_10 );
  ComplianceMatrix AijInv_11 = calcComplianceMatrix( E_11, nu_11, t_11 );
  ComplianceMatrix AijInv_01 = calcComplianceMatrix( E_01, nu_01, t_01 );

  Ainvfld.DOF(0) = AijInv_00;
  Ainvfld.DOF(1) = AijInv_10;
  Ainvfld.DOF(2) = AijInv_11;
  Ainvfld.DOF(3) = AijInv_01;

  // Lumped shell mass CG field
  Field_CG_Cell<PhysD2, TopoD2, Tm> mufld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  Real mu_00 = 0.0123;
  Real mu_10 = 3.923;
  Real mu_11 = 9.13;
  Real mu_01 = 0.946;

  mufld.DOF(0) = mu_00;
  mufld.DOF(1) = mu_10;
  mufld.DOF(2) = mu_11;
  mufld.DOF(3) = mu_01;

  // Following force CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorE> qefld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  VectorE qe_00 = { -3   ,  4./7. };
  VectorE qe_10 = { 2./5., -2./3. };
  VectorE qe_11 = {  1   ,  -7    };
  VectorE qe_01 = { 1./2.,  1./4. };

  qefld.DOF(0) = qe_00;
  qefld.DOF(1) = qe_10;
  qefld.DOF(2) = qe_11;
  qefld.DOF(3) = qe_01;

  // Global forcing CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorX_Tq> qxfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  VectorX_Tq qx_00 = {  23. , 0.0123 };
  VectorX_Tq qx_10 = { 4.935, -2.77 };
  VectorX_Tq qx_11 = {  7.35, 1.43 };
  VectorX_Tq qx_01 = { -9.54, -0.32 };

  qxfld.DOF(0) = qx_00;
  qxfld.DOF(1) = qx_10;
  qxfld.DOF(2) = qx_11;
  qxfld.DOF(3) = qx_01;

  // Accelerations CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorX_Tq> afld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  VectorX_Tq a_00 = { 3.245, -1./3. };
  VectorX_Tq a_10 = { 1.43 , 8.79 };
  VectorX_Tq a_11 = { -0.874 , 1.3 };
  VectorX_Tq a_01 = { 0.034, 23.12 };

  afld.DOF(0) = a_00;
  afld.DOF(1) = a_10;
  afld.DOF(2) = a_11;
  afld.DOF(3) = a_01;

  // Solution variable CG field
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> varfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4, varfld.nDOF() );
  BOOST_CHECK_EQUAL( 1, varfld.nElem() );

  // State vector solution at first cell corner {0,0}
  Real   rx_00 = -1;
  Real   ry_00 = -1;
  Real lam3_00 = 3./5.;
  Real  f11_00 = 3;
  Real  f22_00 = -2./5.;
  Real  f12_00 = 1./2.;

  // State vector solution at second cell corner {1,0}
  Real   rx_10 = 1;
  Real   ry_10 = -1./2.;
  Real lam3_10 = 5./13.;
  Real  f11_10 = 1;
  Real  f22_10 = -1./2.;
  Real  f12_10 = 2;

  // State vector solution at third cell corner {1,1}
  Real   rx_11 = 2;
  Real   ry_11 = 3;
  Real lam3_11 = 8./17.;
  Real  f11_11 = 2;
  Real  f22_11 = 3;
  Real  f12_11 = -1./3.;

  // State vector solution at third cell corner {0,1}
  Real   rx_01 = -1./2.;
  Real   ry_01 = 2;
  Real lam3_01 = 7./25.;
  Real  f11_01 = -5;
  Real  f22_01 = 9;
  Real  f12_01 = 2;

  // Quad solution
  varfld.DOF(0) = pde.setDOFFrom( PositionLambdaForces2D(rx_00, ry_00, lam3_00, f11_00, f22_00, f12_00) );
  varfld.DOF(1) = pde.setDOFFrom( PositionLambdaForces2D(rx_10, ry_10, lam3_10, f11_10, f22_10, f12_10) );
  varfld.DOF(2) = pde.setDOFFrom( PositionLambdaForces2D(rx_11, ry_11, lam3_11, f11_11, f22_11, f12_11) );
  varfld.DOF(3) = pde.setDOFFrom( PositionLambdaForces2D(rx_01, ry_01, lam3_01, f11_01, f22_01, f12_01) );

  // Quadrature rule
  //
  // Quad order 3
  // Error: < 0.15%
  //
  // Quad order 4+
  // Error: < 0.00131 %
  //
  // USE QUAD ORDER 4: No increase in accuracy increasing past quad order 4

  int quadratureOrder = 4;

  const Real small_tol = 0.00131; // Errors are so high because SANS quadrature scheme does not
  const Real close_tol = 0.00131; // match well with Mathematica quadrature scheme
  SLA::SparseVector<ArrayQS> residual(4);

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // grid integration (just one element)
  residual = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, residual),
                                          (Ainvfld, mufld, qefld, qxfld, afld, xfld),
                                          varfld,
                                          &quadratureOrder, 1 );
  Real eps11Res;
  Real eps22Res;
  Real eps12Res;
  Real f1Res;
  Real f2Res;
  Real e3Res;
  ArrayQ residualTrue[4];

  // PDE residuals: (source) + (advective flux)

  // Basis function 1
  eps11Res = ( 1.372026572939575 ) + ( 0 );
  eps22Res = ( 0.3363067056858875 ) + ( 0 );
  eps12Res = ( 0.1970192494829645 ) + ( 0 );
  f1Res    = ( -6.166754474989174 ) + ( 0.1568768753392972 );
  f2Res    = ( -7.686675740928013 ) + ( 1.26075504443376 );
  e3Res    = ( 2.506669437094429 ) + ( 0 );
  residualTrue[0] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 2
  eps11Res = ( 1.504796077432906 ) + ( 0 );
  eps22Res = ( 0.3702039018522039 ) + ( 0 );
  eps12Res = ( 0.3185493181381063 ) + ( 0 );
  f1Res    = ( -11.5787460259529 ) + ( 0.7877862481326029 );
  f2Res    = ( -11.85616313948526 ) + ( 1.422006577287272 );
  e3Res    = ( 2.771498263061457 ) + ( 0 );
  residualTrue[1] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 3
  eps11Res = ( 1.630803614584895 ) + ( 0 );
  eps22Res = ( 0.3691963323525386 ) + ( 0 );
  eps12Res = ( 0.2769322114180279 ) + ( 0 );
  f1Res    = ( -15.01107830413221 ) + ( -0.08152857576495446 );
  f2Res    = ( -15.81974746230442 ) + ( -0.09527063626648668 );
  e3Res    = ( 2.8431700215609 ) + ( 0 );
  residualTrue[2] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 4
  eps11Res = ( 1.454811679047038 ) + ( 0 );
  eps22Res = ( 0.3785215596795534 ) + ( 0 );
  eps12Res = ( 0.2464301366891186 ) + ( 0 );
  f1Res    = ( -9.716058362919606 ) + ( -0.8631345475408619 );
  f2Res    = ( -11.04853015640661 ) + ( -2.587490984215213 );
  e3Res    = ( 2.421501474268187 ) + ( 0 );
  residualTrue[3] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };


  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( residualTrue[0][n], SurrealValue(residual[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( residualTrue[1][n], SurrealValue(residual[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( residualTrue[2][n], SurrealValue(residual[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( residualTrue[3][n], SurrealValue(residual[3][n]), small_tol, close_tol );
  }

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
