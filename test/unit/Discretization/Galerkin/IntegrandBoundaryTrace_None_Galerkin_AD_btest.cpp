// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_Galerkin_AD_btest
// testing of boundary integrands: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementIntegral.h"

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None> PDEAdvectionDiffusion2D;
typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_None> PDEAdvectionDiffusion3D;

typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass1D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass2D;
typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass3D;

typedef BCNDConvertSpace<PhysD1, BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N> > BCClass1D;
typedef BCNDConvertSpace<PhysD2, BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> > BCClass2D;
typedef BCNDConvertSpace<PhysD3, BCNone<PhysD3,AdvectionDiffusionTraits<PhysD3>::N> > BCClass3D;

typedef NDVectorCategory<boost::mpl::vector1<BCClass1D>, BCClass1D::Category> NDBCVecCat1D;
typedef NDVectorCategory<boost::mpl::vector1<BCClass2D>, BCClass2D::Category> NDBCVecCat2D;
typedef NDVectorCategory<boost::mpl::vector1<BCClass3D>, BCClass3D::Category> NDBCVecCat3D;

typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldArea_Triangle;
typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldArea_Quad;
typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldVolume_Tet;
typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldVolume_Hex;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, Galerkin>
               ::BasisWeighted<Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::BasisWeighted<Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::BasisWeighted<Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
               ::BasisWeighted<Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
               ::BasisWeighted<Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;

// 1D
template class IntegrandBoundaryTrace<PDEClass1D, NDBCVecCat1D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD0, Node, TopoD1, Line, ElementXFieldLine>;
// 2D
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldArea_Triangle>;
template class IntegrandBoundaryTrace<PDEClass2D, NDBCVecCat2D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD1, Line, TopoD2, Quad, ElementXFieldArea_Quad>;
// 3D
template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldVolume_Tet>;
template class IntegrandBoundaryTrace<PDEClass3D, NDBCVecCat3D, Galerkin>
               ::FieldWeighted<Real, Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldVolume_Hex>;

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_None_Galerkin_AD_btest_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_BCNone_Line_X1Q1_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // BC

  BCClass bc;

  BOOST_CHECK( bc.D == 1 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent line grid
  Real x1, x2;

  x1 = 0;
  x2 = 1;

  xfldElem.DOF(0) = {x1};
  xfldElem.DOF(1) = {x2};

  xnode.DOF(0) = {x2};
  xnode.normalSignL() = 1;

  // solution

  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 2;
  qfldElem.DOF(1) = 3;

  // integrand functor

  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xnode, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrandPDE1, integrandPDE2;
  ArrayQ integrandPDE[2] = {0,0};

  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  sRef = 0;
  fcn( sRef, integrandPDE, 2 );

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  integrandPDE1 = (0)          + (0)            + (0);
  integrandPDE2 = (33./10.)    + (-2123./1000.)*0 + (0);

  SANS_CHECK_CLOSE( integrandPDE1, integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDE2, integrandPDE[1], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = 0;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, ArrayQ> integral(quadratureorder, nIntegrandL);

  ArrayQ rsdPDEElemL[2] = {0,0};

  // cell integration for canonical element
  integral( fcn, xnode, rsdPDEElemL, nIntegrandL );

  // PDE residuals (left): (advective) + (viscous) + (Lagrange)
  Real rsd1PDE = (0)          + (0)            + (0);
  Real rsd2PDE = (33./10.)    + (-2123./1000.)*0 + (0);

  SANS_CHECK_CLOSE( rsd1PDE, rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2PDE, rsdPDEElemL[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_BCNone_Triangle_X1Q1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real, TopoD1, Line, TopoD2, Triangle, ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kyx = 0.478;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                                 kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;


  // integrand functor
  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrandPDETrue[3];
  ArrayQ integrandPDE[3];

  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  // Test at sRef={0}, {s,t}={1, 0}
  sRef={0.};
  fcn( sRef, integrandPDE, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue[0] = ( 0 ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( 39/(10.*sqrt(2)) ) + ( -4941/(500.*sqrt(2))*0 ); // Basis function 2
  integrandPDETrue[2] = ( 0 ) + ( 0 ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );


  // Test at sRef={1}, {s,t}={0, 1}
  sRef={1.};
  fcn( sRef, integrandPDE, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue[0] = ( 0 ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( 0 ) + ( 0 ); // Basis function 2
  integrandPDETrue[2] = ( (13*sqrt(2))/5. ) + ( -4941/(500.*sqrt(2))*0 ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );


  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef={1./2.};
  fcn( sRef, integrandPDE, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue[0] = ( 0 ) + ( 0 ); // Basis function 1
  integrandPDETrue[1] = ( 91/(40.*sqrt(2)) ) + ( -4941/(1000.*sqrt(2))*0 ); // Basis function 2
  integrandPDETrue[2] = ( 91/(40.*sqrt(2)) ) + ( -4941/(1000.*sqrt(2))*0 ); // Basis function 3

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );

  // test the trace element integral of the functor
  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrandL);

  ArrayQ rsdPDEElemL[3] = {0,0,0};

  Real rsdPDETrue[3];

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDEElemL, nIntegrandL );

  //PDE residuals: (advective) + (viscous)
  rsdPDETrue[0] = ( 0 ) + ( 0 ); // Basis function 1
  rsdPDETrue[1] = ( 13./6. ) + ( -4941./1000. )*0; // Basis function 2
  rsdPDETrue[2] = ( 143./60. ) + ( -4941./1000. )*0; // Basis function 3

  SANS_CHECK_CLOSE( rsdPDETrue[0], rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[1], rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDETrue[2], rsdPDEElemL[2], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_BCNone_Quad_X1Q1_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Quad> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real, TopoD1, Line, TopoD2, Quad, ElementXFieldCell> BasisWeightedClass;


  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 1;  y3 = 1;
  x4 = 0;  y4 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};
  xfldElem.DOF(3) = {x4, y4};

  xedge.DOF(0) = {x1, y1};
  xedge.DOF(1) = {x2, y2};


  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // quad solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;


  // integrand functor
  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDETrue[4];
  ArrayQ integrandPDE[4];

  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  sRef = 0;
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (-1./5.) + (6141./1000.)*0;
  integrandPDETrue[1] = (0)      + (0)          ;
  integrandPDETrue[2] = (0)      + (0)          ;
  integrandPDETrue[3] = (0)      + (0)          ;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );


  sRef = 1;
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)      + (0)          ;
  integrandPDETrue[1] = (-3./5.) + (2113./1000.)*0;
  integrandPDETrue[2] = (0)      + (0)          ;
  integrandPDETrue[3] = (0)      + (0)          ;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );


  sRef = 1./2.;
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (-1./5.) + (4127./2000.)*0;
  integrandPDETrue[1] = (-1./5.) + (4127./2000.)*0;
  integrandPDETrue[2] = (0)      + (0)          ;
  integrandPDETrue[3] = (0)      + (0)          ;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );


  // test the trace element integral of the functor
  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrandL);

  ArrayQ rsdPDEElemL[4] = {0,0,0,0};

  Real rsdPDE[4];

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDEElemL, nIntegrandL );

  // PDE residuals (left): (advective) + (viscous)
  rsdPDE[0] = (-1./6.)  + (2879./1200.)*0 ;
  rsdPDE[1] = (-7./30.) + (10367./6000.)*0;
  rsdPDE[2] = (0)       + (0)           ;
  rsdPDE[3] = (0)       + (0)           ;

  SANS_CHECK_CLOSE( rsdPDE[0], rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[1], rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[2], rsdPDEElemL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[3], rsdPDEElemL[3], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_BCNone_Tet_X1Q1_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;

  typedef BCNone<PhysD3,AdvectionDiffusionTraits<PhysD3>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD3,TopoD2,Triangle> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD3,Tet> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real, TopoD2, Triangle, TopoD3, Tet, ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kxy, kyy, kyz,
                                  kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  ElementXFieldTrace xtri(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xtri.order() );
  BOOST_CHECK_EQUAL( 3, xtri.nDOF() );

  // tetrahdral grid element
  Real x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;

  x1 = 0;  y1 = 0;  z1 = 0;
  x2 = 1;  y2 = 0;  z2 = 0;
  x3 = 0;  y3 = 1;  z3 = 0;
  x4 = 0;  y4 = 0;  z4 = 1;

  xfldElem.DOF(0) = {x1, y1, z1};
  xfldElem.DOF(1) = {x2, y2, z2};
  xfldElem.DOF(2) = {x3, y3, z3};
  xfldElem.DOF(3) = {x4, y4, z4};

  xtri.DOF(0) = {x2, y2, z2};
  xtri.DOF(1) = {x3, y3, z3};
  xtri.DOF(2) = {x4, y4, z4};


  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 4, qfldElem.nDOF() );

  // tet solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;


  // integrand functor
  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xtri, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDETrue[4];
  ArrayQ integrandPDE[4];

  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  sRef = {0, 0};
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)             + (0)                   ;
  integrandPDETrue[1] = (9.*sqrt(3)/5.) + (-8941.*sqrt(3)/1000.)*0;
  integrandPDETrue[2] = (0)             + (0)                   ;
  integrandPDETrue[3] = (0)             + (0)                   ;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );


  sRef = {1,0};
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)              + (0)                   ;
  integrandPDETrue[1] = (0)              + (0)                   ;
  integrandPDETrue[2] = (12.*sqrt(3)/5.) + (-8941.*sqrt(3)/1000.)*0;
  integrandPDETrue[3] = (0)              + (0)                   ;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );


  sRef = {0,1};
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)               + (0)                   ;
  integrandPDETrue[1] = (0)               + (0)                   ;
  integrandPDETrue[2] = (0)               + (0)                   ;
  integrandPDETrue[3] = (18.*sqrt(3)/5.)  + (-8941.*sqrt(3)/1000.)*0;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );


  sRef = {1./3., 1./3.};
  fcn( sRef, integrandPDE, 4 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)                + (0)                     ;
  integrandPDETrue[1] = (13./(5.*sqrt(3))) + (-8941./(1000.*sqrt(3)))*0;
  integrandPDETrue[2] = (13./(5.*sqrt(3))) + (-8941./(1000.*sqrt(3)))*0;
  integrandPDETrue[3] = (13./(5.*sqrt(3))) + (-8941./(1000.*sqrt(3)))*0;

  SANS_CHECK_CLOSE( integrandPDETrue[0], integrandPDE[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[1], integrandPDE[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[2], integrandPDE[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandPDETrue[3], integrandPDE[3], small_tol, close_tol );


  // test the trace element integral of the functor
  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrandL);

  ArrayQ rsdPDEElemL[4] = {0,0,0,0};

  Real rsdPDE[4];

  // cell integration for canonical element
  integral( fcn, xtri, rsdPDEElemL, nIntegrandL );

  // PDE residuals (left): (advective) + (viscous)
  rsdPDE[0] = (0)       + (0)           ;
  rsdPDE[1] = (6./5.)   + (-8941./2000.)*0;
  rsdPDE[2] = (51./40.) + (-8941./2000.)*0;
  rsdPDE[3] = (57./40.) + (-8941./2000.)*0;

  SANS_CHECK_CLOSE( rsdPDE[0], rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[1], rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[2], rsdPDEElemL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdPDE[3], rsdPDEElemL[3], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_BCNone_Hex_X1Q1_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_None> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEAdvectionDiffusion3D::ArrayQ<Real> ArrayQ;

  typedef BCNone<PhysD3,AdvectionDiffusionTraits<PhysD3>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD3, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD3,TopoD2,Quad> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD3,Hex> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real, TopoD2, Quad, TopoD3, Hex, ElementXFieldCell> BasisWeightedClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kxy, kyy, kyz,
                                  kxz, kyz, kzz );

  Source3D_None source;

  PDEClass pde(adv, visc, source);

  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 3 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 8, xfldElem.nDOF() );

  ElementXFieldTrace xquad(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xquad.order() );
  BOOST_CHECK_EQUAL( 4, xquad.nDOF() );

  // hexahedral grid element
  xfldElem.DOF(0) = {0, 0, 0};
  xfldElem.DOF(1) = {1, 0, 0};
  xfldElem.DOF(2) = {1, 1, 0};
  xfldElem.DOF(3) = {0, 1, 0};

  xfldElem.DOF(4) = {0, 0, 1};
  xfldElem.DOF(5) = {1, 0, 1};
  xfldElem.DOF(6) = {1, 1, 1};
  xfldElem.DOF(7) = {0, 1, 1};

  xquad.DOF(0) = {0, 0, 0};
  xquad.DOF(1) = {0, 1, 0};
  xquad.DOF(2) = {1, 1, 0};
  xquad.DOF(3) = {1, 0, 0};


  // solution
  order = 1;
  ElementQFieldCell qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 8, qfldElem.nDOF() );

  // hex solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;
  qfldElem.DOF(3) = 6;

  qfldElem.DOF(4) = 8;
  qfldElem.DOF(5) = 2;
  qfldElem.DOF(6) = 9;
  qfldElem.DOF(7) = 7;

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xquad, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 8, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );
  BOOST_CHECK( fcnint.needsFieldTrace() == false );

  const Real small_tol = 2e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandPDETrue[8];
  ArrayQ integrandPDE[8];

  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  sRef = {0, 0};
  fcn( sRef, integrandPDE, 8 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (-1./2.) + (13749./1000.)*0;
  integrandPDETrue[1] = (0)      + (0)           ;
  integrandPDETrue[2] = (0)      + (0)           ;
  integrandPDETrue[3] = (0)      + (0)           ;

  integrandPDETrue[4] = (0)      + (0)           ;
  integrandPDETrue[5] = (0)      + (0)           ;
  integrandPDETrue[6] = (0)      + (0)           ;
  integrandPDETrue[7] = (0)      + (0)           ;

  for (int k = 0; k < 8; k++)
    SANS_CHECK_CLOSE( integrandPDETrue[k], integrandPDE[k], small_tol, close_tol );


  sRef = {1, 0};
  fcn( sRef, integrandPDE, 8 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)  + (0)          ;
  integrandPDETrue[1] = (0)  + (0)          ;
  integrandPDETrue[2] = (0)  + (0)          ;
  integrandPDETrue[3] = (-3) + (3773./1000.)*0;

  integrandPDETrue[4] = (0)  + (0);
  integrandPDETrue[5] = (0)  + (0);
  integrandPDETrue[6] = (0)  + (0);
  integrandPDETrue[7] = (0)  + (0);

  for (int k = 0; k < 8; k++)
    SANS_CHECK_CLOSE( integrandPDETrue[k], integrandPDE[k], small_tol, close_tol );


  sRef = {1, 1};
  fcn( sRef, integrandPDE, 8 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)  + (0)          ;
  integrandPDETrue[1] = (0)  + (0)          ;
  integrandPDETrue[2] = (-2) + (5649./1000.)*0;
  integrandPDETrue[3] = (0)  + (0)          ;

  integrandPDETrue[4] = (0)  + (0)          ;
  integrandPDETrue[5] = (0)  + (0)          ;
  integrandPDETrue[6] = (0)  + (0)          ;
  integrandPDETrue[7] = (0)  + (0)          ;

  for (int k = 0; k < 8; k++)
    SANS_CHECK_CLOSE( integrandPDETrue[k], integrandPDE[k], small_tol, close_tol );


  sRef = {0, 1};
  fcn( sRef, integrandPDE, 8 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (0)      + (0)         ;
  integrandPDETrue[1] = (-3./2.) + (817./1000.)*0;
  integrandPDETrue[2] = (0)      + (0)         ;
  integrandPDETrue[3] = (0)      + (0)         ;

  integrandPDETrue[4] = (0)  + (0);
  integrandPDETrue[5] = (0)  + (0);
  integrandPDETrue[6] = (0)  + (0);
  integrandPDETrue[7] = (0)  + (0);

  for (int k = 0; k < 8; k++)
    SANS_CHECK_CLOSE( integrandPDETrue[k], integrandPDE[k], small_tol, close_tol );


  sRef = {1./2., 1./2.};
  fcn( sRef, integrandPDE, 8 );

  // PDE residuals: (inviscid) + (viscous)
  integrandPDETrue[0] = (-7./16.) + (5997./4000.)*0;
  integrandPDETrue[1] = (-7./16.) + (5997./4000.)*0;
  integrandPDETrue[2] = (-7./16.) + (5997./4000.)*0;
  integrandPDETrue[3] = (-7./16.) + (5997./4000.)*0;

  integrandPDETrue[4] = (0)  + (0);
  integrandPDETrue[5] = (0)  + (0);
  integrandPDETrue[6] = (0)  + (0);
  integrandPDETrue[7] = (0)  + (0);

  for (int k = 0; k < 8; k++)
    SANS_CHECK_CLOSE( integrandPDETrue[k], integrandPDE[k], small_tol, close_tol );


  // test the trace element integral of the functor
  int quadratureorder = 2;
  int nIntegrandL = qfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Quad, ArrayQ> integral(quadratureorder, nIntegrandL);

  ArrayQ rsdPDEElemL[8] = {0,0,0,0, 0,0,0,0};

  Real rsdPDE[8];

  // cell integration for canonical element
  integral( fcn, xquad, rsdPDEElemL, nIntegrandL );

  // PDE residuals (left): (advective) + (viscous)
  rsdPDE[0] = (-13./36.) + (931./480.)  *0;
  rsdPDE[1] = (-7./18.)  + (5093./4000.)*0;
  rsdPDE[2] = (-35./72.) + (607./480.)  *0;
  rsdPDE[3] = (-37./72.) + (3647./2400.)*0;

  rsdPDE[4] = (0)        + (0);
  rsdPDE[5] = (0)        + (0);
  rsdPDE[6] = (0)        + (0);
  rsdPDE[7] = (0)        + (0);

  for (int k = 0; k < 8; k++)
    SANS_CHECK_CLOSE( rsdPDE[k], rsdPDEElemL[k], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_BCNone_Triangle_X1Q1W2_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;

  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> FieldWeightedClass;

  // PDE

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.321;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC

  BCClass bc;

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution
  int qorder = 1;
  ElementQFieldCell qfldElem(qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution (left)
  qfldElem.DOF(0) = 1;
  qfldElem.DOF(1) = 3;
  qfldElem.DOF(2) = 4;

  // weight
  ElementQFieldCell wfldElem(qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElem.order() );
  BOOST_CHECK_EQUAL( 6, wfldElem.nDOF() );

  // triangle solution (left)
  wfldElem.DOF(0) = -2;
  wfldElem.DOF(1) =  4;
  wfldElem.DOF(2) =  3;
  wfldElem.DOF(3) =  2;
  wfldElem.DOF(4) =  4;
  wfldElem.DOF(5) = -1;

  Element<Real,TopoD2,Triangle> efldElem( 0, BasisFunctionCategory_Legendre );

  // integrand functor
  IntegrandClass fcnint( pde, bc, {0} );

  FieldWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1),
                                             xfldElem,
                                             qfldElem,
                                             wfldElem,
                                             efldElem);

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;
  RefCoordTraceType sRef;
  Real integrand[1]={0};
  Real integrandPDETrue;

  //=======================================================
  // Viscous terms zeroed out per request by Prof. Darmofal
  //=======================================================

  // Test at sRef={0}, {s,t}={1, 0}
  sRef = {0.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue = ( (39.*sqrt(2.))/5. ) + ( -5487./(125.*sqrt(2.)) )*0;

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );

  // Test at sRef={1}, {s,t}={0, 1}
  sRef = {1.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue = ( (39.*sqrt(2.))/5. ) + ( -16461./(500.*sqrt(2.)) )*0;

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );

  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef = {1./2.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue = ( 1001./(40.*sqrt(2.)) ) + ( -60357./(1000.*sqrt(2.)) )*0;

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );

  // Test at sRef={1/5}, {s,t}={4/5, 1/5}
  sRef = {1./5.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue = ( (6604.*sqrt(2.))/625. ) + ( -696849./(12500.*sqrt(2.)) )*0;

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );

  // Test at sRef={1/8}, {s,t}={7/8, 1/8}
  sRef = {1./8.};
  fcn( sRef, integrand, 1 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue = ( 1235./(64.*sqrt(2.)) ) + ( -104253./(2000.*sqrt(2.)) )*0;

  SANS_CHECK_CLOSE( integrandPDETrue, integrand[0], small_tol, close_tol );

  // test the trace element integral of the functor

  int quadratureorder = 3;
  GalerkinWeightedIntegral<TopoD1, Line, Real> integral(quadratureorder, 1);

  Real rsdPDETrue;

  Real rsd[1]={0};

  // Trace integration for canonical element
  integral( fcn, xedge, rsd, 1 );

  //PDE residual: (advective) + (viscous) + (lagrange)
  rsdPDETrue = ( 1313./60. ) + ( -53041./1000. )*0;

  SANS_CHECK_CLOSE( rsdPDETrue, rsd[0], small_tol, close_tol );


}
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_2D_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N> BCClassRaw;
  typedef BCNDConvertSpace<PhysD2, BCClassRaw> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,Real,TopoD1,Line,TopoD2,Triangle,ElementXFieldCell> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 2.3;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc , source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // BC
  BCClass bc;

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 1 );
  BOOST_CHECK( bc.NBC == 0 );

  // grid
  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  Element<Real,TopoD2,Triangle> efldElem( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 2; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElem( qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElem( qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());
    // line solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);
      wfldElem.DOF(dof) = 0;
    }

    IntegrandClass fcnint( pde, bc, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem );
    FieldWeightedClass fcnW = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), xfldElem, qfldElem, wfldElem, efldElem );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-12;
    const int nIntegrand = qfldElem.nDOF();
    Real rsdElemW[1]={0};
    std::vector<ArrayQ> rsdElemB(nIntegrand,0); // trickery to get round the POD warning

    int quadratureorder = -1;
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralB(quadratureorder, nIntegrand);

    // cell integration for canonical element
    integralB( fcnB, xedge, rsdElemB.data(), nIntegrand );

    GalerkinWeightedIntegral<TopoD1, Line, Real> integralW(quadratureorder, 1);

    for (int i = 0; i < wfldElem.nDOF(); i++) // testing the wfld
    {
      // set just one of the weights to one
      wfldElem.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemW[0] = 0;
      integralW( fcnW, xedge, rsdElemW, 1 );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdElemW[0], rsdElemB[i], small_tol, close_tol );

      // reset to 0
      wfldElem.DOF(i) = 0;
    }
  }

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
