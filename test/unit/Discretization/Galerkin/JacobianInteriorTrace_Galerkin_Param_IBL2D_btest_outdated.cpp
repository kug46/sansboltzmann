// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianInteriorTrace_Galerkin_Param_IBL2D_btest
// testing of 2-D line-integral jacobian: IBL

//#define DISPLAY_FOR_DEBUGGING

#include <boost/mpl/list.hpp>

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "pde/IBL/SetVelDofCell_IBL2D.h"
#include "pde/IBL/PDEIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_manifold.h"
#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin_Param.h"
#include "Discretization/Galerkin/ResidualInteriorTrace_Galerkin.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField2D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
using namespace std;
using namespace SANS;
namespace SANS
{
typedef SurrealS<8> SurrealClass;

typedef PhysD2 PhysDim;
typedef VarData2DDANCt VarDataType;
typedef VarTypeDANCt VarType;
typedef PDEIBL<PhysDim,VarType> PDEClass;
typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef PDEClass::template ArrayParam<Real> ArrayParam;
typedef PDEClass::template MatrixParam<Real> MatrixParam;
typedef NDPDEClass::VectorX VectorX;

typedef IntegrandInteriorTrace_Galerkin_manifold<NDPDEClass> IntegrandClass;
}

#define SANS_CHECK_PING_ORDER( err_vec, delta_vec, small_tol ) \
    if (err_vec[1] > small_tol){ \
      Real rate = log(err_vec[1]/err_vec[0])/log(delta_vec[1]/delta_vec[0]); \
      BOOST_CHECK_MESSAGE( rate >= 1.9 && rate <= 4.0, "Rate check failed: rate = " << rate << \
                           ", err_vec = [" << err_vec[0] << "," << err_vec[1] << "]" ); \
    }

//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianInteriorTrace_Galerkin_Param_IBL2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin2D_2Lines_X1Q1Param1_Surreal8 )
{
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);

  // ----------------------- SET UP FIELDS ----------------------- //
  const int order = 1;
  const int nBasis = order + 1;
  const int nelem = 2;

  // grid: two lines (order X1)
  std::vector<VectorX> coordinates(nelem+1);
  coordinates[0] = {-1.5, 0};
  coordinates[1] = {-0.5, 0.25};
  coordinates[2] = { 0.5, -0.25};

  XField2D_Line_X1_1Group xfld(coordinates);

  BOOST_CHECK_EQUAL( 3, xfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, xfld.nElem() );

  // EIF edge velocity DG field
  Field_DG_Cell<PhysDim, TopoD1, VectorX> vfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis*nelem, vfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, vfld.nElem() );

  std::vector<Real> dataVel = {6, 2, -3};

  Field_DG_Cell<PhysDim, TopoD1, VectorX> vxXfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis*nelem, vxXfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, vxXfld.nElem() );

  Field_DG_Cell<PhysDim, TopoD1, VectorX> vzXfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis*nelem, vzXfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, vzXfld.nElem() );

  for_each_CellGroup<TopoD1>::apply( SetVelocityDofCell_IBL2D(dataVel, {0}), (vfld, vxXfld, vzXfld, xfld) );

#if 1 // need to change velocity field to avoid non-smooth residual associated with Lax-Friedrichs dissipation coefficient
  vfld.DOF(0) = {1,0.1};
  vfld.DOF(1) = {2,0.2};
  vfld.DOF(2) = {3,-0.1};
  vfld.DOF(3) = {4,-0.3};
#endif

  // EIF stagnation state DG field
  Field_DG_Cell<PhysDim, TopoD1, ArrayQ> stagfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis*nelem, stagfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, stagfld.nElem() );

  Real p0i_L0 = 1.20e+5,  T0i_L0 = 200., p0i_L1 = 1.30e+5,  T0i_L1 = 350.;
  Real p0i_R0 = 1.40e+5,  T0i_R0 = 300., p0i_R1 = 1.10e+5,  T0i_R1 = 250.;

  ArrayQ stag_L0 = { p0i_L0, T0i_L0 }, stag_L1 = { p0i_L1, T0i_L1 };
  ArrayQ stag_R0 = { p0i_R0, T0i_R0 }, stag_R1 = { p0i_R1, T0i_R1 };

  stagfld.DOF(0) = stag_L0;
  stagfld.DOF(1) = stag_L1;
  stagfld.DOF(2) = stag_R0;
  stagfld.DOF(3) = stag_R1;

  // parameter field
  Field_DG_Cell<PhysDim, TopoD1, ArrayParam> paramfld(xfld, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( nBasis*nelem, paramfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem,        paramfld.nElem() );

  paramfld.DOF(0) = {vfld.DOF(0)[0], vfld.DOF(0)[1],
                     vxXfld.DOF(0)[0], vxXfld.DOF(0)[1],
                     vzXfld.DOF(0)[0], vzXfld.DOF(0)[1],
                     stagfld.DOF(0)[0], stagfld.DOF(0)[1]};

  paramfld.DOF(1) = {vfld.DOF(1)[0], vfld.DOF(1)[1],
                     vxXfld.DOF(1)[0], vxXfld.DOF(1)[1],
                     vzXfld.DOF(1)[0], vzXfld.DOF(1)[1],
                     stagfld.DOF(1)[0], stagfld.DOF(1)[1]};

  paramfld.DOF(2) = {vfld.DOF(2)[0], vfld.DOF(2)[1],
                     vxXfld.DOF(2)[0], vxXfld.DOF(2)[1],
                     vzXfld.DOF(2)[0], vzXfld.DOF(2)[1],
                     stagfld.DOF(2)[0], stagfld.DOF(2)[1]};

  paramfld.DOF(3) = {vfld.DOF(3)[0], vfld.DOF(3)[1],
                     vxXfld.DOF(3)[0], vxXfld.DOF(3)[1],
                     vzXfld.DOF(3)[0], vzXfld.DOF(3)[1],
                     stagfld.DOF(3)[0], stagfld.DOF(3)[1]};

  // Solution variable DG field
  Field_DG_Cell<PhysDim, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( nBasis*nelem, qfld.nDOF() );
  BOOST_CHECK_EQUAL( nelem, qfld.nElem() );

  Real delta_L0 = 2e-2, A_L0 = 1.2, delta_L1 = 5e-2, A_L1 = 0.2;
  Real delta_R0 = 1e-2, A_R0 = 1.0, delta_R1 = 3e-2, A_R1 = -0.1;

  // Line solution
  qfld.DOF(0) = pde.setDOFFrom( VarDataType(delta_L0,A_L0) );
  qfld.DOF(1) = pde.setDOFFrom( VarDataType(delta_L1,A_L1) );
  qfld.DOF(2) = pde.setDOFFrom( VarDataType(delta_R0,A_R0) );
  qfld.DOF(3) = pde.setDOFFrom( VarDataType(delta_R1,A_R1) );

  // ----------------------- INTEGRATE ----------------------- //
  IntegrandClass fcnint( pde, {0} );
  const int nCellGroup = 1;

  const int quadratureOrder = 0;

  const int nDOFL = order + 1, nDOFR = order + 1;
  const int paramDOFL = order + 1, paramDOFR = order + 1;

  const int nPDE = pde.N;
  const int nArrayP = pde.Nparam;

  const int m_jac = nPDE*(nDOFL+nDOFR);            // number of rows in jac
  const int n_jac = nArrayP*(paramDOFL+paramDOFR); // number of columns in jac

  // jacobian via FD w/ residual operator
  std::vector<Real> delta = {1e-1, 1e-2}; // FD step sizes
  const int nk = 2;                       // number of FD steps

  Real jac[m_jac][n_jac][nk];

  SLA::SparseVector<ArrayQ> rsdGlobalm1(nDOFL+nDOFR), rsdGlobalp1(nDOFL+nDOFR);

  for (int kk = 0; kk < nk; kk++)
  {
    for (int j = 0; j < paramDOFL+paramDOFR; j++)
    {
      for (int n = 0; n < nArrayP; n++)
      {
        // residual(param - d param)
        paramfld.DOF(j)[n] -= 1.0*delta[kk];
        rsdGlobalm1 = 0;
        IntegrateInteriorTraceGroups<TopoD1>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobalm1),
                                                         (paramfld, xfld), qfld, &quadratureOrder, nCellGroup);

        // residual(param + d param)
        paramfld.DOF(j)[n] += 2.0*delta[kk];
        rsdGlobalp1 = 0;
        IntegrateInteriorTraceGroups<TopoD1>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdGlobalp1),
                                                         (paramfld, xfld), qfld, &quadratureOrder, nCellGroup);

        BOOST_REQUIRE(rsdGlobalm1.m()*nPDE == m_jac);
        BOOST_REQUIRE(rsdGlobalp1.m()*nPDE == m_jac);

        const int slot_column = j*nArrayP + n;
        for (int i = 0; i < nDOFL+nDOFR; i++)
        {
          for (int m = 0; m < nPDE; m++)
          {
            const int slot_row = i*nPDE + m;
            jac[slot_row][slot_column][kk] = (rsdGlobalp1[i][m] - rsdGlobalm1[i][m]) / (2.0*delta[kk]);
          }
        }

        // reset param
        paramfld.DOF(j)[n] -= 1.0*delta[kk];
      }
    }
  }

  // jacobian via Surreal
  DLA::MatrixD<MatrixParam> mtxGlob(nDOFL+nDOFR, paramDOFL+paramDOFR);
  mtxGlob = 0;
  const int iParam = 0; // parameter index in tuple
  IntegrateInteriorTraceGroups<TopoD1>::integrate( JacobianInteriorTrace_Galerkin_Param<SurrealClass, iParam>(fcnint, mtxGlob),
                                                   (paramfld, xfld), qfld, &quadratureOrder, nCellGroup);

  // ping test
  std::vector<Real> err(nk);
  const Real small_tol = 1e-10;
  for (int i = 0; i < nDOFL+nDOFR; i++)
  {
    for (int m = 0; m < nPDE; m++)
    {
      const int slot_row = i*nPDE + m;
      for (int j = 0; j < paramDOFL+paramDOFR; j++)
      {
        for (int n = 0; n < nArrayP; n++)
        {
          const int slot_column = j*nArrayP + n;
          for (int k = 0; k < nk; k++)
          {
            err[k] = (jac[slot_row][slot_column][k] - DLA::index(mtxGlob(i,j),m,n));
          }
          SANS_CHECK_PING_ORDER(err, delta, small_tol);
#ifdef DISPLAY_FOR_DEBUGGING
          std::cout << "jac["<<slot_row<<"]["<<slot_column<<"]:" << std::endl; \
          std::cout << "err_vec = [" << err[0] << "," << err[1] << "]" << std::endl;
#endif
        }
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
