// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandInteriorTrace_Galerkin_IBL2D_btest
// testing of trace element residual integrands for Galerkin: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "Surreal/SurrealS.h"

#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_manifold.h"

#include "Field/Element/GalerkinWeightedIntegral.h" // Basis Weighted

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Tuple/ElementTuple.h"

#include "pde/IBL/PDEIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

using namespace std;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarData2DDANCt VarDataType;
typedef VarTypeDANCt VarType;
typedef PDEIBL<PhysDim,VarType> PDEClass;
typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef PDEClass::template ArrayParam<Real> ArrayParam;
typedef PDEClass::template VectorX<Real> VectorX;
typedef DLA::VectorS<PhysDim::D,VectorX> VectorVectorX;

typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldClass;
typedef ElementXField<PhysDim,TopoD0,Node> ElementXFieldTraceClass;
typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
typedef Element<ArrayParam,TopoD1,Line> ElementParamFieldClass;
typedef Element<VectorX,TopoD1,Line> ElementVFieldClass;

typedef MakeTuple<ElementTuple, ElementParamFieldClass,
                                ElementXFieldClass>::type ElementParamType;

typedef typename ElementXFieldTraceClass::RefCoordType RefCoordTraceType;

typedef IntegrandInteriorTrace_Galerkin_manifold<NDPDEClass> IntegrandClass;
typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,Line,
                                      ElementParamType,ElementParamType> BasisWeightedClass;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class IntegrandInteriorTrace_Galerkin_manifold<NDPDEClass>::
BasisWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParamType,ElementParamType>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandInteriorTrace_Galerkin_IBL2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IntegrandInteriorTrace_Galerkin_manifold_IBL2D_Line_X1Q1_IncompressibleLaminarWallProfile_test )
{
  // static tests
  BOOST_CHECK( PDEClass::D == IntegrandClass::D );
  BOOST_CHECK( PDEClass::N == IntegrandClass::N );

  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);

  // ----------------------- ELEMENTAL FIELDS ----------------------- //
  const int order = 1;

  // Grid
  ElementXFieldClass xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldClass xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTraceClass xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  VectorX X1 = { 0.000, 0.000 };
  VectorX X2 = { 0.800, 0.600 };
  VectorX X3 = { 1.600, 0.000 };

  xfldElemL.DOF(0) = X1;
  xfldElemL.DOF(1) = X2;

  xfldElemR.DOF(0) = X2;
  xfldElemR.DOF(1) = X3;

  xnode.DOF(0) = X2;
  xnode.normalSignL() =  1;
  xnode.normalSignR() = -1;

  // Parameter: edge velocity field
  const Real qxi_L_0 = 4.000, qzi_L_0 = 2.000, p0_L_0 = 1.100e+05, T0_L_0 = 290.00;
  const Real qxi_L_1 = 3.000, qzi_L_1 = 4.000, p0_L_1 = 1.000e+05, T0_L_1 = 350.00;
  const Real qxi_R_0 = 2.000, qzi_R_0 = 1.000, p0_R_0 = 1.300e+05, T0_R_0 = 330.00;
  const Real qxi_R_1 = 5.000, qzi_R_1 = 8.000, p0_R_1 = 9.000e+04, T0_R_1 = 310.00;

  ElementQFieldClass vfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass vfldElemR(order, BasisFunctionCategory_Hierarchical);

  // Parameter: edge velocity gradient fields
  std::vector<VectorX> gradphi0_L(vfldElemL.nDOF()), gradphi1_L(vfldElemL.nDOF());
  xfldElemL.evalBasisGradient( 0.0, xfldElemL, gradphi0_L.data(), gradphi0_L.size() );
  xfldElemL.evalBasisGradient( 1.0, xfldElemL, gradphi1_L.data(), gradphi1_L.size() );

  VectorVectorX gradv0_L, gradv1_L; // grad(v) at node 0 and 1 of element L
  vfldElemL.evalFromBasis( gradphi0_L.data(), gradphi0_L.size(), gradv0_L );
  vfldElemL.evalFromBasis( gradphi1_L.data(), gradphi1_L.size(), gradv1_L );

  std::vector<VectorX> gradphi0_R(vfldElemR.nDOF()), gradphi1_R(vfldElemR.nDOF());
  xfldElemR.evalBasisGradient( 0.0, xfldElemR, gradphi0_R.data(), gradphi0_R.size() );
  xfldElemR.evalBasisGradient( 1.0, xfldElemR, gradphi1_R.data(), gradphi1_R.size() );

  VectorVectorX gradv0_R, gradv1_R; // grad(v) at node 0 and 1 of element R
  vfldElemR.evalFromBasis( gradphi0_R.data(), gradphi0_R.size(), gradv0_R );
  vfldElemR.evalFromBasis( gradphi1_R.data(), gradphi1_R.size(), gradv1_R );

  // Parameter field
  ElementParamFieldClass paramElemL(order, BasisFunctionCategory_Hierarchical);
  ElementParamFieldClass paramElemR(order, BasisFunctionCategory_Hierarchical);

  paramElemL.DOF(0) = {qxi_L_0, qzi_L_0, gradv0_L[0][0], gradv0_L[1][0], gradv0_L[0][1], gradv0_L[1][1], p0_L_0, T0_L_0};
  paramElemL.DOF(1) = {qxi_L_1, qzi_L_1, gradv1_L[0][0], gradv1_L[1][0], gradv1_L[0][1], gradv1_L[1][1], p0_L_1, T0_L_1};

  paramElemR.DOF(0) = {qxi_R_0, qzi_R_0, gradv0_R[0][0], gradv0_R[1][0], gradv0_R[0][1], gradv0_R[1][1], p0_R_0, T0_R_0};
  paramElemR.DOF(1) = {qxi_R_1, qzi_R_1, gradv1_R[0][0], gradv1_R[1][0], gradv1_R[0][1], gradv1_R[1][1], p0_R_1, T0_R_1};

  // Solution
  ElementQFieldClass qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldClass qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemR.nDOF() );

  // State vector at L element basis fcn 0
  const Real delta_L_0 = 0.1000;
  const Real A_L_0     = 3.0000;

  // State vector at L element basis fcn 1
  const Real delta_L_1 = 0.0800;
  const Real A_L_1     = 3.3000;

  // State vector at R element basis fcn 0
  const Real delta_R_0 = 0.1200;
  const Real A_R_0     = 2.8000;

  // State vector at R element basis fcn 1
  const Real delta_R_1 = 0.0900;
  const Real A_R_1     = 2.4000;

  // Line solution
  qfldElemL.DOF(0) = pde.setDOFFrom( VarDataType(delta_L_0,A_L_0) );
  qfldElemL.DOF(1) = pde.setDOFFrom( VarDataType(delta_L_1,A_L_1) );

  qfldElemR.DOF(0) = pde.setDOFFrom( VarDataType(delta_R_0,A_R_0) );
  qfldElemR.DOF(1) = pde.setDOFFrom( VarDataType(delta_R_1,A_R_1) );

  // ----------------------------- TEST INTEGRAND ----------------------------- //
  const std::vector<int> interiorTraceGroups = {0};
  IntegrandClass fcnint(pde, interiorTraceGroups);

  BOOST_CHECK(interiorTraceGroups.size() == fcnint.nInteriorTraceGroups());

  for (std::size_t i=0; i<interiorTraceGroups.size(); ++i)
  {
    BOOST_CHECK_EQUAL(interiorTraceGroups[i], fcnint.interiorTraceGroup(i));
    BOOST_CHECK_EQUAL(interiorTraceGroups[i], fcnint.interiorTraceGroups()[i]);
  }

  //Explicitly create a paramater instance.
  //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior
  ElementParamType tupleElemL = (paramElemL, xfldElemL);
  ElementParamType tupleElemR = (paramElemR, xfldElemR);

  BasisWeightedClass fcnPDE = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                                tupleElemL, qfldElemL,
                                                tupleElemR, qfldElemR );

  BOOST_CHECK( true == fcnPDE.needsEvaluation() );
  BOOST_CHECK( PDEClass::N == fcnPDE.nEqn() );
  BOOST_CHECK( qfldElemL.nDOF() == fcnPDE.nDOFLeft() );
  BOOST_CHECK( qfldElemR.nDOF() == fcnPDE.nDOFRight() );

  const Real small_tol = 5e-13;
  const Real close_tol = 5e-13;

  RefCoordTraceType sRef;

  Real eIntegrand;
  Real starIntegrand;

  const int nBasis = order+1;  // number of basis functions

  ArrayQ integrandPDETrueL[nBasis];
  ArrayQ integrandPDETrueR[nBasis];

  ArrayQ integrandPDEL[nBasis];
  ArrayQ integrandPDER[nBasis];

  // Test
  sRef = 0.;
  fcnPDE( sRef, integrandPDEL, nBasis, integrandPDER, nBasis ); // IntegrandInteriorTrace Functor operator()

  // PDE residual integrands: (source) + (advective flux)
  // L element
  // Basis function 1
  eIntegrand    = 0.0000000000000000e+00;
  starIntegrand = 0.0000000000000000e+00;
  integrandPDETrueL[0] = { eIntegrand, starIntegrand };

  // Basis function 2
#if ISIBLLFFLUX
  eIntegrand    = 2.1111220850655377e-01;
  starIntegrand = 1.6795512158342558e+00;
#else
  eIntegrand    = 1.2491659676254671e-01;
  starIntegrand = 1.0416263626844637e+00;
#endif
  integrandPDETrueL[1] = { eIntegrand, starIntegrand };

  // R element
  // Basis function 1
#if ISIBLLFFLUX
  eIntegrand    = -2.1111220850655377e-01;
  starIntegrand = -1.6795512158342558e+00;
#else
  eIntegrand    = -1.2491659676254671e-01;
  starIntegrand = -1.0416263626844637e+00;
#endif

  integrandPDETrueR[0] = { eIntegrand, starIntegrand };

  // Basis function 2
  eIntegrand    = -0.0000000000000000e+00;
  starIntegrand = -0.0000000000000000e+00;
  integrandPDETrueR[1] = { eIntegrand, starIntegrand };

  for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandPDETrueL[ibasis][n], integrandPDEL[ibasis][n], small_tol, close_tol );
      SANS_CHECK_CLOSE( integrandPDETrueR[ibasis][n], integrandPDER[ibasis][n], small_tol, close_tol );
    }
  }

  // ----------------------------- TEST ELEMENTAL INTEGRAL (RESIDUAL) ----------------------------- //
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();

  // Test the element integral of the functor:
  int quadratureorder = 0;
  GalerkinWeightedIntegral<TopoD0, Node, ArrayQ, ArrayQ> integralPDE(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdElemPDEL[nBasis] = {0,0};
  ArrayQ rsdElemPDER[nBasis] = {0,0};

  ArrayQ rsdPDELTrue[nBasis],rsdPDERTrue[nBasis];

  rsdPDELTrue[0] = integrandPDETrueL[0];
  rsdPDELTrue[1] = integrandPDETrueL[1];
  rsdPDERTrue[0] = integrandPDETrueR[0];
  rsdPDERTrue[1] = integrandPDETrueR[1];

  // cell integration for canonical element
  integralPDE( fcnPDE, xnode, rsdElemPDEL, nIntegrandL, rsdElemPDER, nIntegrandR );

  for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( rsdPDELTrue[ibasis][n], rsdElemPDEL[ibasis][n], small_tol, close_tol );
      SANS_CHECK_CLOSE( rsdPDERTrue[ibasis][n], rsdElemPDER[ibasis][n], small_tol, close_tol );
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
