// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_Stabilized_NS_btest
// testing of cell residual integrands for Stabilized CG; NS

#include <boost/test/unit_test.hpp>

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"

#include "Field/Element/GalerkinWeightedIntegral.h" // Basis Weighted
#include "Field/Element/ElementIntegral.h" // Field Weighted

#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_Stabilized_NS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_Stabilized_NS_2D_Triangle_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  //  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_Stabilized<NDPDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,ElementParam> BasisWeightedClass;
  typedef ElementXFieldClass::RefCoordType RefCoordType;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

  const Real muRef = 0.1;      // kg/(m s) // FOR RE = 10
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution

  // set
  ArrayQ q1 = 0;
  ArrayQ q2 = 0;
  ArrayQ q3 = 0;
  pde.setDOFFrom( q1, DensityVelocityPressure2D<Real>(967./1000.,  1./4., 32./100., 107./100) );
  pde.setDOFFrom( q2, DensityVelocityPressure2D<Real>(103./100.,  23./100., 32./100., 108./100) );
  pde.setDOFFrom( q3, DensityVelocityPressure2D<Real>(11./10.,  21./100., 36./100., 105./100) );


  qfldElem.DOF(0) = q1;
  qfldElem.DOF(1) = q2;
  qfldElem.DOF(2) = q3;

  StabilizationMatrix tau( StabilizationType::SUPG, TauType::Glasby, order );

  // integrand
  IntegrandClass fcnint( pde, {0}, tau );

  // integrand functor
  BasisWeightedClass fcn = fcnint.integrand(xfldElem, qfldElem );

  BOOST_CHECK_EQUAL( 4, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordType sRef;
  Real integrandPDETrue[3][4];
  ArrayQ integrand[3];

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue[0][0] = ( 55119./100000. ) + ( 0 ) + ( -0.020325397232862994130325804872809 );
  integrandPDETrue[0][1] = ( 483119./400000. ) + ( 7./750. ) + ( -0.015978491008198309316239051819257 );
  integrandPDETrue[0][2] = ( 194747./156250. ) + ( -1./375. ) + ( -0.017087057410185403079181954469466 );
  integrandPDETrue[0][3] = ( 4360191231./2000000000. ) + ( 8102388629./70131675000. ) + ( 0.0034453953612866909732957376369137 );
  integrandPDETrue[1][0] = ( -967./4000. ) + ( 0 ) + ( 0.0087202425655800719060133155375513 );
  integrandPDETrue[1][1] = ( -18087./16000. ) + ( -2./375. ) + ( 0.014213764925472538221636032434387 );
  integrandPDETrue[1][2] = ( -967./12500. ) + ( -1./250. ) + ( 0.0017647260827257710946030193848693 );
  integrandPDETrue[1][3] = ( -76494583./80000000. ) + ( -858143104./26299378125. ) + ( -0.0022800605340056595147841733508433 );
  integrandPDETrue[2][0] = ( -967./3125. ) + ( 0 ) + ( 0.011605154667282922224312489335258 );
  integrandPDETrue[2][1] = ( -967./12500. ) + ( -1./250. ) + ( 0.0017647260827257710946030193848693 );
  integrandPDETrue[2][2] = ( -365319./312500. ) + ( 1./150. ) + ( 0.015322331327459631984578935084597 );
  integrandPDETrue[2][3] = ( -76494583./62500000. ) + ( -3488404211./42079005000. ) + ( -0.0011653348272810314585115642860704 );



  for (int i=0; i< fcn.nDOF(); i++)
      for (int j=0; j< fcn.nEqn(); j++)
      {
        SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrand[i][j], small_tol, close_tol );
      }

  // Test at {1, 0}
  sRef = {1, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue[0][0] = ( 1133./2000. ) + ( 0 ) + ( -0.019828301085087955738304269466092 );
  integrandPDETrue[0][1] = ( 242059./200000. ) + ( 7./750. ) + ( -0.015303809659332030316102471908461 );
  integrandPDETrue[0][2] = ( 7883./6250. ) + ( -1./375. ) + ( -0.017043028292161142191028080257522 );
  integrandPDETrue[0][3] = ( 84919549./40000000. ) + ( 61469461./596756250. ) + ( 0.0035931400978359647615485332287906 );
  integrandPDETrue[1][0] = ( -2369./10000. ) + ( 0 ) + ( 0.0076925710848002255312852480004041 );
  integrandPDETrue[1][1] = ( -1134487./1000000. ) + ( -2./375. ) + ( 0.013751883111328450382107238951166 );
  integrandPDETrue[1][2] = ( -2369./31250. ) + ( -1./250. ) + ( 0.0015519265480035799339952329572951 );
  integrandPDETrue[1][3] = ( -177559057./200000000. ) + ( -69136601./2387025000. ) + ( -0.0037483590870024947685619692596287 );
  integrandPDETrue[2][0] = ( -206./625. ) + ( 0 ) + ( 0.012135730000287730207019021465688 );
  integrandPDETrue[2][1] = ( -2369./31250. ) + ( -1./250. ) + ( 0.0015519265480035799339952329572951 );
  integrandPDETrue[2][2] = ( -18523./15625. ) + ( 1./150. ) + ( 0.015491101744157562257032847300227 );
  integrandPDETrue[2][3] = ( -7719959./6250000. ) + ( -176741243./2387025000. ) + ( 0.00015521898916653000701343603083807 );

  for (int i=0; i< fcn.nDOF(); i++)
      for (int j=0; j< fcn.nEqn(); j++)
        SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrand[i][j], small_tol, close_tol );

  // Test at {0, 1}
  sRef = {0, 1};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue[0][0] = ( 627./1000. ) + ( 0 ) + ( -0.022151290622751690872826329312999 );
  integrandPDETrue[0][1] = ( 118167./100000. ) + ( 7./750. ) + ( -0.014570800029112448146147654202088 );
  integrandPDETrue[0][2] = ( 31893./25000. ) + ( -1./375. ) + ( -0.018843547657916307860402730128394 );
  integrandPDETrue[0][3] = ( 42984099./20000000. ) + ( 95939./1089000. ) + ( 0.0019782703343109908515415832778561 );
  integrandPDETrue[1][0] = ( -231./1000. ) + ( 0 ) + ( 0.0062197880448817067078138974023496 );
  integrandPDETrue[1][1] = ( -109851./100000. ) + ( -2./375. ) + ( 0.013040717137566819305638368896969 );
  integrandPDETrue[1][2] = ( -2079./25000. ) + ( -1./250. ) + ( 0.0015300828915456288405092853051195 );
  integrandPDETrue[1][3] = ( -15836247./20000000. ) + ( -5383193./217800000. ) + ( -0.0073828495641933532413244668524123 );
  integrandPDETrue[2][0] = ( -99./250. ) + ( 0 ) + ( 0.015931502577869984165012431910649 );
  integrandPDETrue[2][1] = ( -2079./25000. ) + ( -1./250. ) + ( 0.0015300828915456288405092853051195 );
  integrandPDETrue[2][2] = ( -14907./12500. ) + ( 1./150. ) + ( 0.017313464766370679019893444823275 );
  integrandPDETrue[2][3] = ( -6786963./5000000. ) + ( -13804607./217800000. ) + ( 0.0054045792298823623897828835745562 );

  for (int i=0; i< fcn.nDOF(); i++)
      for (int j=0; j< fcn.nEqn(); j++)
        SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrand[i][j], small_tol, close_tol );

  // Test at {1/3, 1/3}
  sRef = {1./3., 1./3.};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (advective) + (viscous)
  //PDE residual integrands: (advective) + (viscous)
  integrandPDETrue[0][0] = ( 523393./900000. ) + ( 0 ) + ( -0.02073954395968269536573588904461 );
  integrandPDETrue[0][1] = ( 108038039./90000000. ) + ( 7./750. ) + ( -0.01531230551331112220565655947788 );
  integrandPDETrue[0][2] = ( 3403393./2700000. ) + ( -1./375. ) + ( -0.017628630939304209210936008138522 );
  integrandPDETrue[0][3] = ( 348429804073./162000000000. ) + ( 218676399997./2158067025000. ) + ( 0.0030361385174942284008224508169279 );
  integrandPDETrue[1][0] = ( -71231./300000. ) + ( 0 ) + ( 0.0075817127737732241458531962721579 );
  integrandPDETrue[1][1] = ( -11212771./10000000. ) + ( -2./375. ) + ( 0.013667722976031185423871956683529 );
  integrandPDETrue[1][2] = ( -71231./900000. ) + ( -1./250. ) + ( 0.0016445825372799367817846027943517 );
  integrandPDETrue[1][3] = ( -47419440791./54000000000. ) + ( -10252103389./359677837500. ) + ( -0.0045160701726934835348232977088493 );
  integrandPDETrue[2][0] = ( -3097./9000. ) + ( 0 ) + ( 0.013157831185909471219882692772452 );
  integrandPDETrue[2][1] = ( -71231./900000. ) + ( -1./250. ) + ( 0.0016445825372799367817846027943517 );
  integrandPDETrue[2][2] = ( -31897./27000. ) + ( 1./150. ) + ( 0.01598404840202427242915140534417 );
  integrandPDETrue[2][3] = ( -2061714817./1620000000. ) + ( -8271777877./113582475000. ) + ( 0.0014799316551992551340008468919214 );

  for (int i=0; i< fcn.nDOF(); i++)
      for (int j=0; j< fcn.nEqn(); j++)
        SANS_CHECK_CLOSE( integrandPDETrue[i][j], integrand[i][j], small_tol, close_tol );



//
//
//  // test the element integral of the functor
//
//  int quadratureorder = -1;
//  int nIntegrand = qfldElem.nDOF();
//  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integral(quadratureorder, nIntegrand);
//
//  Real rsdPDETrue[3][4];
//  ArrayQ rsdElem[3];
//
//  // cell integration for canonical element
//  integral( fcn, xfldElem, rsdElem, nIntegrand );
////
//
//  //PDE residual: (advection) + (diffusion)
//  rsdPDETrue[0][0] = ( 348931./1200000. ) + ( 0 );
//  rsdPDETrue[0][1] = ( 180044713./300000000. ) + ( 307./180000. );
//  rsdPDETrue[0][2] = ( 189100171./300000000. ) + ( -989./180000. );
//  rsdPDETrue[0][3] = ( 387142357567./360000000000. ) + ( -0.00249983511875611 );
//  rsdPDETrue[1][0] = ( -47443./400000. ) + ( 0 );
//  rsdPDETrue[1][1] = ( -84093299./150000000. ) + ( -239./90000. );
//  rsdPDETrue[1][2] = ( -790541./20000000. ) + ( 19./20000. );
//  rsdPDETrue[1][3] = ( -52695175427./120000000000. ) + ( -0.00539847735443355 );
//  rsdPDETrue[2][0] = ( -103301./600000. ) + ( 0 );
//  rsdPDETrue[2][1] = ( -790541./20000000. ) + ( 19./20000. );
//  rsdPDETrue[2][2] = ( -22155257./37500000. ) + ( 409./90000. );
//  rsdPDETrue[2][3] = ( -114528415643./180000000000. ) + ( 0.00789831247304454 );
//
//  const Real small_tol_rsd = 1e-11;
//  const Real close_tol_rsd = 1e-10;
//
//  for (int i=0; i< fcn.nDOF(); i++)
//    for (int j=0; j< fcn.nEqn(); j++)
//    {
//      //      SANS_CHECK_CLOSE( rsdPDETrue[i][j], rsdElem[i][j], small_tol_rsd, close_tol_rsd );
//    }

}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_WeightVary_2D_NS_Triangle_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  //  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_Stabilized<NDPDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam> FieldWeightedClass;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

  const Real muRef = 0.1;      // kg/(m s) // FOR RE = 10
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution

  // set
  ArrayQ q1 = 0;
  pde.setDOFFrom( q1, DensityVelocityPressure2D<Real>(967./1000.,  1./4., 32./100., 107./100) );

  // estimate element
  Element<Real,TopoD2,Triangle> efldElem( 0, BasisFunctionCategory_Legendre );

  const int nstab = (int)StabilizationType::nStabilizationType;
  StabilizationType stabs[nstab]; //0 for
  for (int i = 0; i < nstab; i++ )
  {
    stabs[i] = (StabilizationType)i;

    if (i>2) stabs[i] = (StabilizationType)5;
  }

  const int ntau = (int)TauType::nTauType;
  TauType taus[ntau];
  for (int i = 0; i < ntau; i++ )
    taus[i] = (TauType)i;

  for (int qorder = 1; qorder <= 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );

    // solution
    for (int dof = 0; dof < qfldElem.nDOF();dof++)
    {

      qfldElem.DOF(dof) = q1;

      for (int j=0; j< pde.N; j++)
        qfldElem.DOF(dof)(j) += 0.001*Real((dof+1)*pow(-1,dof));
    }

    ElementQFieldClass wfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, wfldElem.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElem.nDOF() );

    // weighting
    wfldElem.vectorViewDOF() = 0;

    BOOST_REQUIRE_EQUAL(wfldElem.nDOF(), qfldElem.nDOF());

    for (int dof = 0; dof < wfldElem.nDOF(); dof ++ )
      wfldElem.DOF(dof) = 0;

    for (int istab = 0; istab < nstab; istab++)
      for (int itau = 0; itau < ntau; itau++)
      {
        // skip invalid combinations
        if (!StabilizationMatrix::checkStabTau(stabs[istab], taus[itau]))
        {
          continue;
        }

        StabilizationMatrix tau(stabs[istab], taus[itau], qorder );
        StabilizationMatrix tau2(stabs[istab], taus[itau], qorder );

        // integrand
        IntegrandClass fcnint( pde, {0}, tau );
        IntegrandClass fcnint2( pde, {0}, tau2 );

        BasisWeightedClass fcnB = fcnint2.integrand( xfldElem, qfldElem );
        FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

        const Real small_tol = 1e-10;
        const Real close_tol = 1e-10;
        Real rsdPDEElemW[1]={0};
        std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

        int quadratureorder = 2*qorder;
        int nIntegrand = qfldElem.nDOF();
        GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralB(quadratureorder, nIntegrand);

        // cell integration for canonical element
        integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

        GalerkinWeightedIntegral<TopoD2, Triangle, Real> integralW(quadratureorder, 1);

        for (int i = 0; i < wfldElem.nDOF(); i++)
        {
          // set just one of the weights to one
          for (int k=0; k<pde.N; k++)
          {
            wfldElem.DOF(i)(k) = 1.0;

            // cell integration for canonical element
            rsdPDEElemW[0] = 0;
            integralW( fcnW, xfldElem, rsdPDEElemW, 1 );

            // test the the two integrands are identical

            SANS_CHECK_CLOSE( rsdPDEElemW[0], rsdPDEElemB[i][k], small_tol, close_tol );

            // reset to zero
            wfldElem.DOF(i)(k) = 0;
          }
        }
      }
  }

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_EstimateVary_2D_NS_Triangle_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  //  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldClass;

  typedef ElementXFieldClass ElementParam;

  typedef IntegrandCell_Galerkin_Stabilized<NDPDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD2,Triangle,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD2,Triangle,ElementParam> FieldWeightedClass;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

  const Real muRef = 0.1;      // kg/(m s) // FOR RE = 10
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 4 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.fluxViscousLinearInGradient() == true );
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  // triangle grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  // solution

  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 3, qfldElem.nDOF() );

  // triangle solution

  // set
  ArrayQ q1 = 0;
  pde.setDOFFrom( q1, DensityVelocityPressure2D<Real>(967./1000.,  1./4., 32./100., 107./100) );

  // estimate element
  ElementQFieldClass wfldElem( 0, BasisFunctionCategory_Legendre );
  wfldElem.vectorViewDOF() = 0;

  for (int k = 0; k < pde.N; k++)
  {
    wfldElem.DOF(0)(k) = 1;
    for (int qorder = 1; qorder <= 4; qorder++) // making sure the cubic basis works too
    {
      ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

      BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
      BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElem.nDOF() );

      // solution
      for (int dof = 0; dof < qfldElem.nDOF();dof++)
      {
        qfldElem.DOF(dof) = q1;

        for (int j=0; j< pde.N; j++)
          qfldElem.DOF(dof)(j) += 0.001*Real((dof+1)*pow(-1,dof));
      }

      Element<Real,TopoD2,Triangle> efldElem(qorder, BasisFunctionCategory_Hierarchical);

      StabilizationMatrix tau(StabilizationType::Unstabilized, TauType::Constant, qorder );

      IntegrandClass fcnint( pde, {0}, tau );

      BasisWeightedClass fcnB = fcnint.integrand( xfldElem, qfldElem );
      FieldWeightedClass fcnW = fcnint.integrand( xfldElem, qfldElem, wfldElem, efldElem );

      const Real small_tol = 1e-10;
      const Real close_tol = 1e-10;
      std::vector<Real> rsdPDEElemW(efldElem.nDOF(), 0);
      std::vector<ArrayQ> rsdPDEElemB(qfldElem.nDOF(), 0);

      int quadratureorder = 2*(qorder+1);
      int nIntegrand = qfldElem.nDOF();
      GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralB(quadratureorder, nIntegrand);

      // cell integration for canonical element
      integralB( fcnB, xfldElem, rsdPDEElemB.data(), nIntegrand );

      GalerkinWeightedIntegral<TopoD2, Triangle, Real> integralW(quadratureorder, efldElem.nDOF());

      integralW( fcnW, xfldElem, rsdPDEElemW.data(), rsdPDEElemW.size() );

      for (int i = 0; i < efldElem.nDOF(); i++)
        SANS_CHECK_CLOSE( rsdPDEElemW[i], rsdPDEElemB[i][k], small_tol, close_tol );

    }
    wfldElem.DOF(0)(k) = 0;
  }
}

#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
