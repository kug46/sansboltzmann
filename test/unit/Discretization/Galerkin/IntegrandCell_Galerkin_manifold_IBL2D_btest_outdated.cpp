// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_IBL2D_btest
// testing of cell element residual integrands for Galerkin: IBL2D specific

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>

#include "SANS_btest.h"

#include "Surreal/SurrealS.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_manifold.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Tuple/ElementTuple.h"

#include "pde/IBL/PDEIBL2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

using namespace std;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef VarData2DDANCt VarDataType;
typedef VarTypeDANCt VarType;
typedef PDEIBL<PhysDim,VarType> PDEClass;
typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
typedef PDEClass::template ArrayParam<Real> ArrayParam;
typedef PDEClass::template VectorX<Real> VectorX;
typedef DLA::VectorS<PhysDim::D,VectorX> VectorVectorX;

typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldClass;
typedef Element<ArrayQ,TopoD1,Line> ElementQFieldClass;
typedef Element<ArrayParam,TopoD1,Line> ElementParamFieldClass;
typedef Element<VectorX,TopoD1,Line> ElementVFieldClass;

typedef typename ElementXFieldClass::BaseType::RefCoordType RefCoordType;

typedef MakeTuple<ElementTuple, ElementParamFieldClass,
                                ElementXFieldClass>::type ElementParamType;

typedef IntegrandCell_Galerkin_manifold<NDPDEClass> IntegrandClass;
typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,ElementParamType> BasisWeightedClass;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
template class IntegrandCell_Galerkin_manifold<NDPDEClass>::BasisWeighted<Real,TopoD1,Line,ElementParamType>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_IBL2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IntegrandCell_Galerkin_IBL2D_Line_X1Q1R1_IncompressibleLaminarWallProfile_test )
{
  NDPDEClass pde(VelocityProfile2D::LaminarBL);

  // solution approximation order
  const int order = 1;

  // ----------------------- ELEMENT PARAMETERS ----------------------- //
  // Geometry (line grid)
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  Real x_L = 1.2, x_R = x_L + 0.8;
  Real z_L = 2.2, z_R = z_L + 0.6;

  xfldElem.DOF(0) = { x_L, z_L };
  xfldElem.DOF(1) = { x_R, z_R };

  // Parameter: edge velocity field
  ElementVFieldClass vfldElem(order, BasisFunctionCategory_Hierarchical);

  const Real q1x_L = 3.,  q1z_L = 4.;
  const Real q1x_R = 5.,  q1z_R = 2.;

  VectorX q1_L = { q1x_L, q1z_L };
  VectorX q1_R = { q1x_R, q1z_R };

  vfldElem.DOF(0) = q1_L;
  vfldElem.DOF(1) = q1_R;

  // Parameter: edge velocity gradient fields
  VectorVectorX gradvL, gradvR;
  std::vector<VectorX> gradphiL(vfldElem.nDOF()), gradphiR(vfldElem.nDOF());

  xfldElem.evalBasisGradient( 0.0, xfldElem, gradphiL.data(), gradphiL.size() );
  xfldElem.evalBasisGradient( 1.0, xfldElem, gradphiR.data(), gradphiR.size() );

  vfldElem.evalFromBasis( gradphiL.data(), gradphiL.size(), gradvL );
  vfldElem.evalFromBasis( gradphiR.data(), gradphiR.size(), gradvR );

  VectorX q1x_XL = { gradvL[0][0], gradvL[1][0] };
  VectorX q1x_XR = { gradvR[0][0], gradvR[1][0] };

  // Parameter: stagnation state field
  const Real p0i_L = 1.30e+5,  T0i_L = 290.;
  const Real p0i_R = 1.40e+5,  T0i_R = 350.;

  // Parameter field
  ElementParamFieldClass paramElem(order, BasisFunctionCategory_Hierarchical);

  paramElem.DOF(0) = {q1x_L, q1z_L, gradvL[0][0], gradvL[1][0], gradvL[0][1], gradvL[1][1], p0i_L, T0i_L};
  paramElem.DOF(1) = {q1x_R, q1z_R, gradvR[0][0], gradvR[1][0], gradvR[0][1], gradvR[1][1], p0i_R, T0i_R};

  // Solution
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // State vector solution at line cell left node
  Real delta_L = 2e-2;
  Real A_L     = 1.2;

  // State vector solution at line cell right node
  Real delta_R = 2.2e-2;
  Real A_R     = 1.3;

  // Line solution
  qfldElem.DOF(0) = pde.setDOFFrom( VarDataType(delta_L,A_L) );
  qfldElem.DOF(1) = pde.setDOFFrom( VarDataType(delta_R,A_R) );

  // ----------------------------- TEST INTEGRAND ----------------------------- //
  const Real small_tol = 5e-13;
  const Real close_tol = 5e-13;

  IntegrandClass fcnint( pde, {0} );

  //Explicitly create a paramater instance.
  //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior
  ElementParamType tupleElem = (paramElem, xfldElem);

  BasisWeightedClass fcnPDE = fcnint.integrand( tupleElem, qfldElem );

  BOOST_CHECK_EQUAL( 2, fcnPDE.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcnPDE.nDOF() );
  BOOST_CHECK( fcnPDE.needsEvaluation() == true );

  RefCoordType sRef;

  Real eIntegrand;
  Real starIntegrand;

  const int nBasis = qfldElem.nDOF();  // number of basis functions

  std::vector<ArrayQ> integrandPDETrue(nBasis);
  std::vector<ArrayQ> integrandPDE(nBasis);

  // Test
  sRef = 0.;
  fcnPDE( sRef, integrandPDE.data(), nBasis ); // IntegrandCell Functor operator()

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  eIntegrand = ( 1.5410637956585847e-02 ) + ( 8.6834925249693717e-02 );
  starIntegrand = ( -6.1432717970541020e-02 ) + ( 6.9943484299232916e-01 );
  integrandPDETrue[0] = { eIntegrand, starIntegrand };

  // Basis function 2
  eIntegrand = ( 0.0000000000000000e+00 ) + ( -8.6834925249693717e-02 );
  starIntegrand = ( -0.0000000000000000e+00 ) + ( -6.9943484299232916e-01 );
  integrandPDETrue[1] = { eIntegrand, starIntegrand };

  for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
    }
  }

  // Test
  sRef = 0.6;
  fcnPDE( sRef, integrandPDE.data(), nBasis ); // IntegrandCell Functor operator()

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  eIntegrand = ( 6.2940253565260760e-03 ) + ( 9.4377985248203677e-02 );
  starIntegrand = ( -2.3745397884246271e-02 ) + ( 7.3872162780871742e-01 );
  integrandPDETrue[0] = { eIntegrand, starIntegrand };

  // Basis function 2
  eIntegrand = ( 9.4410380347891131e-03 ) + ( -9.4377985248203677e-02 );
  starIntegrand = ( -3.5618096826369405e-02 ) + ( -7.3872162780871742e-01 );
  integrandPDETrue[1] = { eIntegrand, starIntegrand };

  for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
    }
  }

  // Test
  sRef = 1;
  fcnPDE( sRef, integrandPDE.data(), nBasis ); // IntegrandCell Functor operator()

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  eIntegrand = ( 0.0000000000000000e+00 ) + ( 9.9905982556669951e-02 );
  starIntegrand = ( -0.0000000000000000e+00 ) + ( 8.6328331301762540e-01 );
  integrandPDETrue[0] = { eIntegrand, starIntegrand };

  // Basis function 2
  eIntegrand = ( 1.6019260953562833e-02 ) + ( -9.9905982556669951e-02 );
  starIntegrand = ( -6.5338342955398448e-02 ) + ( -8.6328331301762540e-01 );
  integrandPDETrue[1] = { eIntegrand, starIntegrand };

  for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( integrandPDETrue[ibasis][n], integrandPDE[ibasis][n], small_tol, close_tol );
    }
  }

  // ----------------------------- TEST ELEMENTAL INTEGRAL (RESIDUAL) ----------------------------- //
  const Real small_tol_res = small_tol;
  const Real close_tol_res = close_tol;

  int nIntegrand = qfldElem.nDOF();

  // Test the element integral of the functor: quadrature order = 39
  int quadratureorder = 39;
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integralPDE(quadratureorder, nIntegrand);

  Real eRes;
  Real starRes;

  std::vector<ArrayQ> rsdPDETrue(nBasis);
  std::vector<ArrayQ> rsdElemPDE(nBasis);

  // cell integration for canonical element
  integralPDE( fcnPDE, xfldElem, rsdElemPDE.data(), nIntegrand );

  // PDE residuals: (source) + (advective flux)
  // Basis function 1
  eRes = ( 7.7921476912574941e-03 ) + ( 9.3161624940993476e-02 );
  starRes = ( -2.9807898960344921e-02 ) + ( 7.4059965764333957e-01 );
  rsdPDETrue[0] = { eRes, starRes };

  // Basis function 2
  eRes = ( 7.8940289947055185e-03 ) + ( -9.3161624940993476e-02 );
  starRes = ( -3.0485550145167101e-02 ) + ( -7.4059965764333957e-01 );
  rsdPDETrue[1] = { eRes, starRes };

  for ( int ibasis = 0; ibasis < nBasis; ibasis++ )
  {
    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( rsdPDETrue[ibasis][n], rsdElemPDE[ibasis][n], small_tol_res, close_tol_res );
    }
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_1D_Line_Jacobian_test )
{
  /* default parameters
      const Real gam = 1.4;
      const Real R = 287.15;
      const Real mui = 1.827e-5;
      const Real Pr = 0.715;
   */
  NDPDEClass pde(VelocityProfile2D::LaminarBL);

  // solution approximation order
  const int order = 1;

  // ----------------------- ELEMENT PARAMETERS ----------------------- //
  // Geometry (line grid)
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  Real x_L = 1.2, x_R = x_L + 0.8;
  Real z_L = 2.2, z_R = z_L + 0.6;

  xfldElem.DOF(0) = { x_L, z_L };
  xfldElem.DOF(1) = { x_R, z_R };

  // Parameter: edge velocity field
  ElementVFieldClass vfldElem(order, BasisFunctionCategory_Hierarchical);

  const Real q1x_L = 3.,  q1z_L = 4.;
  const Real q1x_R = 5.,  q1z_R = 2.;

  VectorX q1_L = { q1x_L, q1z_L };
  VectorX q1_R = { q1x_R, q1z_R };

  vfldElem.DOF(0) = q1_L;
  vfldElem.DOF(1) = q1_R;

  // Parameter: edge velocity gradient fields
  VectorVectorX gradvL, gradvR;
  std::vector<VectorX> gradphiL(vfldElem.nDOF()), gradphiR(vfldElem.nDOF());

  xfldElem.evalBasisGradient( 0.0, xfldElem, gradphiL.data(), gradphiL.size() );
  xfldElem.evalBasisGradient( 1.0, xfldElem, gradphiR.data(), gradphiR.size() );

  vfldElem.evalFromBasis( gradphiL.data(), gradphiL.size(), gradvL );
  vfldElem.evalFromBasis( gradphiR.data(), gradphiR.size(), gradvR );

  // Parameter: stagnation state field
  const Real p0i_L = 1.30e+5,  T0i_L = 290.;
  const Real p0i_R = 1.40e+5,  T0i_R = 350.;

  // Parameter field
  ElementParamFieldClass paramElem(order, BasisFunctionCategory_Hierarchical);

  paramElem.DOF(0) = {q1x_L, q1z_L, gradvL[0][0], gradvL[1][0], gradvL[0][1], gradvL[1][1], p0i_L, T0i_L};
  paramElem.DOF(1) = {q1x_R, q1z_R, gradvR[0][0], gradvR[1][0], gradvR[0][1], gradvR[1][1], p0i_R, T0i_R};

  // Solution
  ElementQFieldClass qfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElem.order() );
  BOOST_CHECK_EQUAL( 2, qfldElem.nDOF() );

  // State vector solution at line cell left node
  Real delta_L = 2e-2;
  Real A_L     = 1.2;

  // State vector solution at line cell right node
  Real delta_R = 2.2e-2;
  Real A_R     = 1.3;

  // Line solution
  qfldElem.DOF(0) = pde.setDOFFrom( VarDataType(delta_L,A_L) );
  qfldElem.DOF(1) = pde.setDOFFrom( VarDataType(delta_R,A_R) );

  // ----------------------------- TEST INTEGRAND JACOBIAN----------------------------- //

  const std::vector<int> cellGroups = {0};
  IntegrandClass fcnint( pde, cellGroups );

  for (int qorder = 1; qorder <= 1; qorder++) // only checking order 1, really need to check rate decay of central difference or have a linear PDE
  {
    ElementQFieldClass qfldElem(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder, qfldElem.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElem.nDOF() );

    const int nDOF = qfldElem.nDOF();

    // line solution
    for (int dof = 0; dof < nDOF;dof++)
      qfldElem.DOF(dof) = (dof+1)*pow(-1,dof);

    //Explicitly create a paramater instance.
    //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior
    ElementParamType tupleElem = (paramElem, xfldElem);

    BasisWeightedClass fcn = fcnint.integrand( tupleElem, qfldElem );

    const Real small_tol = 1e-11;
    const Real close_tol = 6e-5;
    RefCoordType sRef = 0.5;
    DLA::VectorD<ArrayQ> integrand0(nDOF), integrand1(nDOF);
    DLA::MatrixD<MatrixQ> mtxPDEElemTrue(nDOF, nDOF);
    DLA::MatrixD<MatrixQ> mtxPDEElem(nDOF, nDOF);


    // compute jacobians via finite difference
    fcn(sRef, &integrand0[0], integrand0.m());

    const Real del[PDEClass::N] = {1e-6, 1e-8};

    for (int j = 0; j < nDOF; j++)
      for (int n = 0; n < PDEClass::N; n++)
      {
        qfldElem.DOF(j)[n] += del[n];
        fcn(sRef, &integrand1[0], integrand1.m());
        qfldElem.DOF(j)[n] -= del[n]; // reset

        for (int i = 0; i < nDOF; i++)
          for (int m = 0; m < PDEClass::N; m++)
            mtxPDEElemTrue(i,j)(m,n) = (integrand1[i][m] - integrand0[i][m])/del[n];
      }

    mtxPDEElem = 0;

    // accumulate the jacobian via Surreal
    fcn(1., sRef, mtxPDEElem);

    for (int i = 0; i < nDOF; i++)
      for (int j = 0; j < nDOF; j++)
        for (int m = 0; m < PDEClass::N; m++)
          for (int n = 0; n < PDEClass::N; n++)
            SANS_CHECK_CLOSE( mtxPDEElemTrue(i,j)(m,n), mtxPDEElem(i,j)(m,n), small_tol, close_tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
