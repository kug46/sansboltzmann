// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_HSM2D_btest
// testing of cell element residual integrands for Galerkin: PDEHSM2D

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"

#include "Surreal/SurrealS.h"

#include "pde/HSM/PDEHSM2D.h"
#include "pde/HSM/ComplianceMatrix.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_HSM2D.h" // 2D in 2D, not manifold, PDEHSM2D specific

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Tuple/ElementTuple.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_HSM2D_test_suite )

template<class T_, class Tc_, class Tm_, class Tq_, class Tr_>
struct SurrealCombo
{
  typedef T_ T;
  typedef Tc_ Tc;
  typedef Tm_ Tm;
  typedef Tq_ Tq;
  typedef Tr_ Tr;
};

typedef boost::mpl::list< SurrealCombo<Real,Real,Real,Real,Real>,
                          SurrealCombo<SurrealS< PDEHSM2D<VarTypeLambda>::N >,Real,Real,Real,Real>,
                          SurrealCombo<Real,SurrealS<1>,SurrealS<1>,Real,Real>
                        > TTypes;

template<int N, class T>
T SurrealValue(const SurrealS<N,T>& s) { return s.value(); }

Real SurrealValue(const Real& s) { return s; }

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Triangle_2D_test, Combo, TTypes )
{
  typedef typename Combo::T T;
  typedef typename Combo::Tc Tc;
  typedef typename Combo::Tm Tm;
  typedef typename Combo::Tq Tq;
  typedef typename Combo::Tr Tr;
  typedef typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type Ts;

  typedef typename PDEHSM2D<VarTypeLambda>::template VectorX<Real> VectorX_R;
  typedef typename PDEHSM2D<VarTypeLambda>::template VectorX<Tq> VectorX_Tq;
  typedef typename PDEHSM2D<VarTypeLambda>::template VectorE<Tr> VectorE;

  typedef PDENDConvertSpace<PhysD2,  PDEHSM2D<VarTypeLambda> > PDEClass;

  typedef typename PDEClass::template ArrayQ<T> ArrayQ;
  typedef typename PDEClass::template ArrayQ<Ts> ArrayQS;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ_R;
  typedef typename PDEClass::template ComplianceMatrix<Tc> ComplianceMatrix;

  typedef ElementXField<PhysD2    , TopoD2, Triangle> ElementXFieldClass; // r0 grid
  typedef Element<ArrayQ          , TopoD2, Triangle> ElementQFieldClass; // State variable field

  // Parameter types
  typedef Element<ComplianceMatrix, TopoD2, Triangle> ElementAinvFieldClass;        // Ainv field
  typedef Element<Tm              , TopoD2, Triangle> ElementMassFieldClass;        // mu field
  typedef Element<VectorE         , TopoD2, Triangle> ElementFollowForceFieldClass; // qe field
  typedef Element<VectorX_Tq      , TopoD2, Triangle> ElementGlobForceFieldClass;   // qx field
  typedef Element<VectorX_Tq      , TopoD2, Triangle> ElementAccelFieldClass;       // a field

  typedef typename MakeTuple<ElementTuple, ElementAinvFieldClass,
                                           ElementMassFieldClass,
                                           ElementFollowForceFieldClass,
                                           ElementGlobForceFieldClass,
                                           ElementAccelFieldClass,
                                           ElementXFieldClass>::type ElementParam;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef typename IntegrandClass::template BasisWeighted<T,TopoD2,Triangle, ElementParam > BasisWeightedClass;

  VectorX_R g = { 0.023, -9.81 };
  PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 6 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  int order = 1;

  // Extensional compliance matrix
  ElementAinvFieldClass AinvfldElem(order, BasisFunctionCategory_Hierarchical);

  Real E_00 = 70e9, nu_00 = 1./3.,  t_00 = 1./100.;
  Real E_10 = 80e9, nu_10 = 1./4.,  t_10 = 1./200.;
  Real E_01 = 90e7, nu_01 = 5./11., t_01 = 1./400.;

  ComplianceMatrix AijInv_00 = calcComplianceMatrix( E_00, nu_00, t_00 );
  ComplianceMatrix AijInv_10 = calcComplianceMatrix( E_10, nu_10, t_10 );
  ComplianceMatrix AijInv_01 = calcComplianceMatrix( E_01, nu_01, t_01 );

  AinvfldElem.DOF(0) = AijInv_00;
  AinvfldElem.DOF(1) = AijInv_10;
  AinvfldElem.DOF(2) = AijInv_01;

  // Lumped shell mass
  ElementMassFieldClass mufldElem(order, BasisFunctionCategory_Hierarchical);

  Real mu_00 = 0.0123;
  Real mu_10 = 3.923;
  Real mu_01 = 0.946;

  mufldElem.DOF(0) = mu_00;
  mufldElem.DOF(1) = mu_10;
  mufldElem.DOF(2) = mu_01;

  // Following force
  ElementFollowForceFieldClass qefldElem(order, BasisFunctionCategory_Hierarchical);

  VectorE qe_00 = { -3   ,  4./7. };
  VectorE qe_10 = { 2./5., -2./3. };
  VectorE qe_01 = { 1./2.,  1./4. };

  qefldElem.DOF(0) = qe_00;
  qefldElem.DOF(1) = qe_10;
  qefldElem.DOF(2) = qe_01;

  // Global forcing
  ElementGlobForceFieldClass qxfldElem(order, BasisFunctionCategory_Hierarchical);

  VectorX_Tq qx_00 = {  23.  , 0.0123 };
  VectorX_Tq qx_10 = { 4.935, -2.77 };
  VectorX_Tq qx_01 = { -9.54, -0.32 };

  qxfldElem.DOF(0) = qx_00;
  qxfldElem.DOF(1) = qx_10;
  qxfldElem.DOF(2) = qx_01;

  // Accelerations
  ElementAccelFieldClass afldElem(order, BasisFunctionCategory_Hierarchical);

  VectorX_Tq a_00 = { 3.245, -1./3. };
  VectorX_Tq a_10 = { 1.43 , 8.79 };
  VectorX_Tq a_01 = { 0.034, 23.12 };

  afldElem.DOF(0) = a_00;
  afldElem.DOF(1) = a_10;
  afldElem.DOF(2) = a_01;

  // Undeformed geometry (triangle grid)
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_REQUIRE_EQUAL( 3, xfldElem.nDOF() );

  Real x0_00 = 0, y0_00 = 0;
  Real x0_10 = 1, y0_10 = 0;
  Real x0_01 = 0, y0_01 = 1;

  xfldElem.DOF(0) = {x0_00, y0_00};
  xfldElem.DOF(1) = {x0_10, y0_10};
  xfldElem.DOF(2) = {x0_01, y0_01};

  ElementQFieldClass varfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, varfldElem.order() );
  BOOST_REQUIRE_EQUAL( 3, varfldElem.nDOF() );

  // State vector solution at first cell corner {0,0}
  Real   rx_00 = -1;
  Real   ry_00 = -1;
  Real lam3_00 = 3./5.;
  Real  f11_00 = 3;
  Real  f22_00 = -2./5.;
  Real  f12_00 = 1./2.;

  // State vector solution at second cell corner {1,0}
  Real  rx_10 = 1;
  Real  ry_10 = -1./2.;
  Real lam3_10 = 5./13.;
  Real  f11_10 = 1;
  Real  f22_10 = -1./2.;
  Real  f12_10 = 2;

  // State vector solution at third cell corner {0,1}
  Real   rx_01 = -1./2.;
  Real   ry_01 = 2;
  Real lam3_01 = 7./25.;
  Real  f11_01 = -5;
  Real  f22_01 = 9;
  Real  f12_01 = 2;

  // Triangle solution
  varfldElem.DOF(0) = pde.setDOFFrom( PositionLambdaForces2D(rx_00, ry_00, lam3_00, f11_00, f22_00, f12_00) );
  varfldElem.DOF(1) = pde.setDOFFrom( PositionLambdaForces2D(rx_10, ry_10, lam3_10, f11_10, f22_10, f12_10) );
  varfldElem.DOF(2) = pde.setDOFFrom( PositionLambdaForces2D(rx_01, ry_01, lam3_01, f11_01, f22_01, f12_01) );

  // Integrand
  IntegrandClass fcnint( pde, {0} );

  // Explicitly create a paramater instance.
  // If it was created in the argument below, the temporary variable in the
  // function argument would go out of scope and result in undefined behavior
  ElementParam params = ( AinvfldElem, mufldElem, qefldElem, qxfldElem, afldElem, xfldElem );

  BasisWeightedClass fcn = fcnint.integrand( params, varfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 6, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  typedef ElementXFieldClass::RefCoordType RefCoordType;

  const Real small_tol = 8e-13;
  const Real close_tol = 8e-13;
  RefCoordType sRef;
  Real eps11Integrand;
  Real eps22Integrand;
  Real eps12Integrand;
  Real f1Integrand;
  Real f2Integrand;
  Real e3Integrand;
  ArrayQ_R integrandTrue[3];
  ArrayQS integrand[3];

  // Test at {0, 0}
  sRef = {0, 0};
  fcn( sRef, integrand, 3 );

  //PDE residual integrands: (source) + (advective flux)

  // Basis function 1
  eps11Integrand = ( 2.8749999955238095-(5*cos(2.4))/4.+(5*sin(2.4))/4. ) + ( 0 );
  eps22Integrand = ( 2.875000002+(5*cos(2.4))/4.-(5*sin(2.4))/4. ) + ( 0 );
  eps12Integrand = ( -9.523809523809524e-10+(5*cos(2.4))/4.+(5*sin(2.4))/4. ) + ( 0 );
  f1Integrand    = ( 5.666823418505387 ) + ( (7*cos(1.2)+5*sin(1.2))/2. );
  f2Integrand    = ( -23.039314004084986 ) + ( (cos(1.2)+9*sin(1.2))/10. );
  e3Integrand    = ( (19*cos(1.2))/16.+(77*sin(1.2))/8. ) + ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( (-6*cos(1.2)+sin(1.2))/2. );
  f2Integrand    = ( 0. ) + ( -cos(1.2)/2.-(2*sin(1.2))/5. );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( -cos(1.2)/2.-3*sin(1.2) );
  f2Integrand    = ( 0. ) + ( (2*cos(1.2))/5.-sin(1.2)/2. );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };


  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
  }


  // Test at {1,0}
  sRef = {1, 0};
  fcn( sRef, integrand, 3 );

  // PDE residual integrands: (source) + (advective flux)

  // Basis function 1
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( 3*cos(0.7692307692307693)-sin(0.7692307692307693) );
  f2Integrand    = ( 0. ) + ( (3*cos(0.7692307692307693)+5*sin(0.7692307692307693))/2. );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 2.8749999971875-(5*cos(1.5384615384615385))/4.+(5*sin(1.5384615384615385))/4. ) + ( 0 );
  eps22Integrand = ( 2.875000001875+(5*cos(1.5384615384615385))/4.-(5*sin(1.5384615384615385))/4. ) + ( 0 );
  eps12Integrand = ( -6.25e-9+(5*cos(1.5384615384615385))/4.+(5*sin(1.5384615384615385))/4. ) + ( 0 );
  f1Integrand    = ( -51.27272792076321 ) + ( -cos(0.7692307692307693)+2*sin(0.7692307692307693) );
  f2Integrand    = ( -55.748498765029964 ) + ( -2*cos(0.7692307692307693)-sin(0.7692307692307693)/2. );
  e3Integrand    = ( (19*cos(0.7692307692307693))/16.+(77*sin(0.7692307692307693))/8. ) + ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( -2*cos(0.7692307692307693)-sin(0.7692307692307693) );
  f2Integrand    = ( 0. ) + ( (cos(0.7692307692307693)-4*sin(0.7692307692307693))/2. );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++) // Chage pde.N to 3 to check the first 3 equations
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
  }

  // Test at {0,1}
  sRef = {0, 1};
  fcn( sRef, integrand, 3 );

  // PDE residual integrands: (source) + (advective flux)

  // Basis function 1
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( -3*cos(0.56)-7*sin(0.56) );
  f2Integrand    = ( 0. ) + ( 11*cos(0.56)-7*sin(0.56) );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( 5*cos(0.56)+2*sin(0.56) );
  f2Integrand    = ( 0. ) + ( -2*cos(0.56)+9*sin(0.56) );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 2.8750040404040402-(5*cos(1.12))/4.+(5*sin(1.12))/4. ) + ( 0 );
  eps22Integrand = ( 2.87499498989899+(5*cos(1.12))/4.-(5*sin(1.12))/4. ) + ( 0 );
  eps12Integrand = ( -1.2929292929292929e-6+(5*cos(1.12))/4.+(5*sin(1.12))/4. ) + ( 0 );
  f1Integrand    = ( -20.078322846969087 ) + ( -2*cos(0.56)+5*sin(0.56) );
  f2Integrand    = ( -18.443778812769402 ) + ( -9*cos(0.56)-2*sin(0.56) );
  e3Integrand    = ( (19*cos(0.56))/16.+(77*sin(0.56))/8. ) + ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++) // Chage pde.N to 3 to check the first 3 equations
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
  }


  // Test at {1/3, 1/3}
  sRef = {1./3., 1./3.};
  fcn( sRef, integrand, 3 );

  // PDE residual integrands: (source) + (advective flux)

  // Basis function 1
  eps11Integrand = ( 0.5416667442094925+(13*pow(sin(0.42153846153846153),4))/6.+(5*sin(0.8430769230769231))/6.
                 +(11*pow(sin(0.8430769230769231),2))/8.-(pow(sin(0.42153846153846153),2)*(13+10*sin(0.8430769230769231)))/6. ) + ( 0 );
  eps22Integrand = ( 1.3749998579650806+(11*pow(sin(0.42153846153846153),4))/2.-(5*sin(0.8430769230769231))/6.
                 +(13*pow(sin(0.8430769230769231),2))/24.+(pow(sin(0.42153846153846153),2)*(-33+10*sin(0.8430769230769231)))/6. ) + ( 0 );
  eps12Integrand = ( 0.4166665580842653+(5*pow(sin(0.42153846153846153),4))/3.+(5*sin(0.8430769230769231))/6.
                 -(5*pow(sin(0.8430769230769231),2))/12.-(5*pow(sin(0.42153846153846153),2)*(1+sin(0.8430769230769231)))/3. ) + ( 0 );
  f1Integrand    = ( -7.446797147209737 ) + ( (7-56*pow(sin(0.42153846153846153),6)+pow(sin(0.42153846153846153),4)*(84-44*sin(0.8430769230769231))
                 -11*sin(0.8430769230769231)+7*pow(sin(0.8430769230769231),2)-11*pow(sin(0.8430769230769231),3)
                 -2*pow(sin(0.42153846153846153),2)*(21-22*sin(0.8430769230769231)+7*pow(sin(0.8430769230769231),2)))/6. );
  f2Integrand    = ( -8.413509484998805 ) + ( (-3*(-7+56*pow(sin(0.42153846153846153),6)+2*sin(0.8430769230769231)-7*pow(sin(0.8430769230769231),2)
                 +2*pow(sin(0.8430769230769231),3)+pow(sin(0.42153846153846153),4)*(-84+8*sin(0.8430769230769231))
                 +2*pow(sin(0.42153846153846153),2)*(21-4*sin(0.8430769230769231)+7*pow(sin(0.8430769230769231),2))))/5. );
  e3Integrand    = ( (19*cos(0.8430769230769231)+154*sin(0.8430769230769231))/48. ) + ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0.5416667442094925+(13*pow(sin(0.42153846153846153),4))/6.+(5*sin(0.8430769230769231))/6.
                 +(11*pow(sin(0.8430769230769231),2))/8.-(pow(sin(0.42153846153846153),2)*(13+10*sin(0.8430769230769231)))/6. ) + ( 0 );
  eps22Integrand = ( 1.3749998579650806+(11*pow(sin(0.42153846153846153),4))/2.-(5*sin(0.8430769230769231))/6.
                 +(13*pow(sin(0.8430769230769231),2))/24.+(pow(sin(0.42153846153846153),2)*(-33+10*sin(0.8430769230769231)))/6. ) + ( 0 );
  eps12Integrand = ( 0.4166665580842653+(5*pow(sin(0.42153846153846153),4))/3.+(5*sin(0.8430769230769231))/6.
                 -(5*pow(sin(0.8430769230769231),2))/12.-(5*pow(sin(0.42153846153846153),2)*(1+sin(0.8430769230769231)))/3. ) + ( 0 );
  f1Integrand    = ( -7.446797147209737 ) + ( (2-16*pow(sin(0.42153846153846153),6)+9*sin(0.8430769230769231)+2*pow(sin(0.8430769230769231),2)
                 +9*pow(sin(0.8430769230769231),3)+12*pow(sin(0.42153846153846153),4)*(2+3*sin(0.8430769230769231))
                 -4*pow(sin(0.42153846153846153),2)*(3+9*sin(0.8430769230769231)+pow(sin(0.8430769230769231),2)))/6. );
  f2Integrand    = ( -8.413509484998805 ) + ( (3*(-5+40*pow(sin(0.42153846153846153),6)+9*sin(0.8430769230769231)-5*pow(sin(0.8430769230769231),2)
                 +9*pow(sin(0.8430769230769231),3)+12*pow(sin(0.42153846153846153),4)*(-5+3*sin(0.8430769230769231))
                 +2*pow(sin(0.42153846153846153),2)*(15-18*sin(0.8430769230769231)+5*pow(sin(0.8430769230769231),2))))/10. );
  e3Integrand    = ( (19*cos(0.8430769230769231)+154*sin(0.8430769230769231))/48. ) + ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0.5416667442094925+(13*pow(sin(0.42153846153846153),4))/6.+(5*sin(0.8430769230769231))/6.
                 +(11*pow(sin(0.8430769230769231),2))/8.-(pow(sin(0.42153846153846153),2)*(13+10*sin(0.8430769230769231)))/6. ) + ( 0 );
  eps22Integrand = ( 1.3749998579650806+(11*pow(sin(0.42153846153846153),4))/2.-(5*sin(0.8430769230769231))/6.
                 +(13*pow(sin(0.8430769230769231),2))/24.+(pow(sin(0.42153846153846153),2)*(-33+10*sin(0.8430769230769231)))/6. ) + ( 0 );
  eps12Integrand = ( 0.4166665580842653+(5*pow(sin(0.42153846153846153),4))/3.+(5*sin(0.8430769230769231))/6.
                 -(5*pow(sin(0.8430769230769231),2))/12.-(5*pow(sin(0.42153846153846153),2)*(1+sin(0.8430769230769231)))/3. ) + ( 0 );
  f1Integrand    = ( -7.446797147209737 ) + ( (-9+72*pow(sin(0.42153846153846153),6)+2*sin(0.8430769230769231)-9*pow(sin(0.8430769230769231),2)
                 +2*pow(sin(0.8430769230769231),3)+4*pow(sin(0.42153846153846153),4)*(-27+2*sin(0.8430769230769231))
                 +2*pow(sin(0.42153846153846153),2)*(27-4*sin(0.8430769230769231)+9*pow(sin(0.8430769230769231),2)))/6. );
  f2Integrand    = ( -8.413509484998805 ) + ( (3*(-9+72*pow(sin(0.42153846153846153),6)-5*sin(0.8430769230769231)-9*pow(sin(0.8430769230769231),2)
                 -5*pow(sin(0.8430769230769231),3)-4*pow(sin(0.42153846153846153),4)*(27+5*sin(0.8430769230769231))
                 +2*pow(sin(0.42153846153846153),2)*(27+10*sin(0.8430769230769231)+9*pow(sin(0.8430769230769231),2))))/10. );
  e3Integrand    = ( (19*cos(0.8430769230769231)+154*sin(0.8430769230769231))/48. ) + ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++) // Chage pde.N to 3 to check the first 3 equations
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
  }

  // Test the element integral of the functor
  /*
   Quad order 8
   Error: < 3e-8%

   Quad order 9
   Error: < 3e-12%

   USE MAX QUAD
   */
  int quadratureorder = -1;
  const Real small_tol_res = 3e-12;
  const Real close_tol_res = 3e-12;
  int nIntegrand = varfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQS> integral(quadratureorder, nIntegrand);

  Real eps11Res;
  Real eps22Res;
  Real eps12Res;
  Real f1Res;
  Real f2Res;
  Real e3Res;
  ArrayQ_R residualTrue[3];
  ArrayQS residual[3] = {0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, residual, nIntegrand );

  // PDE residuals: (source) + (advective flux)

  // Basis function 1
  eps11Res = ( 0.7310422798964399 ) + ( 0 );
  eps22Res = ( 0.2272910254055126 ) + ( 0 );
  eps12Res = ( 0.1347310365960171 ) + ( 0 );
  f1Res    = ( -2.264766817818473 ) + ( -0.2712925463509729 );
  f2Res    = ( -3.683638827278987 ) + ( 1.098543979141634 );
  e3Res    = ( 1.395322358930152 ) + ( 0 );
  residualTrue[0] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 2
  eps11Res = ( 0.698229314835413 ) + ( 0 );
  eps22Res = ( 0.2601039947294995 ) + ( 0 );
  eps12Res = ( 0.1874255246914321 ) + ( 0 );
  f1Res    = ( -4.966956438909008 ) + ( 0.725527809868143 );
  f2Res    = ( -5.6990024085711 ) + ( 0.4138708737274842 );
  e3Res    = ( 1.305080587017079 ) + ( 0 );
  residualTrue[1] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 3
  eps11Res = ( 0.6762331516750987 ) + ( 0 );
  eps22Res = ( 0.2821001203581534 ) + ( 0 );
  eps12Res = ( 0.2082161046689848 ) + ( 0 );
  f1Res    = ( -3.661482280707458 ) + ( -0.4542352635171701 );
  f2Res    = ( -4.263250082131023 ) + ( -1.512414852869118 );
  e3Res    = ( 1.25239287695413 ) + ( 0 );
  residualTrue[2] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( residualTrue[0][n], SurrealValue(residual[0][n]), small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[1][n], SurrealValue(residual[1][n]), small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[2][n], SurrealValue(residual[2][n]), small_tol_res, close_tol_res );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( Quad_2D_test, Combo, TTypes )
{
  typedef typename Combo::T T;
  typedef typename Combo::Tc Tc;
  typedef typename Combo::Tm Tm;
  typedef typename Combo::Tq Tq;
  typedef typename Combo::Tr Tr;
  typedef typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type Ts;

  typedef typename PDEHSM2D<VarTypeLambda>::template VectorX<Real> VectorX_R;
  typedef typename PDEHSM2D<VarTypeLambda>::template VectorX<Tq> VectorX_Tq;
  typedef typename PDEHSM2D<VarTypeLambda>::template VectorE<Tr> VectorE;

  typedef PDENDConvertSpace<PhysD2,  PDEHSM2D<VarTypeLambda> > PDEClass;

  typedef typename PDEClass::template ArrayQ<T> ArrayQ;
  typedef typename PDEClass::template ArrayQ<Ts> ArrayQS;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ_R;
  typedef typename PDEClass::template ComplianceMatrix<Tc> ComplianceMatrix;

  typedef ElementXField<PhysD2    , TopoD2, Quad> ElementXFieldClass; // r0 grid
  typedef Element<ArrayQ          , TopoD2, Quad> ElementQFieldClass; // State variable field

  // Parameter types
  typedef Element<ComplianceMatrix, TopoD2, Quad> ElementAinvFieldClass;        // Ainv field
  typedef Element<Tm              , TopoD2, Quad> ElementMassFieldClass;        // mu field
  typedef Element<VectorE         , TopoD2, Quad> ElementFollowForceFieldClass; // qe field
  typedef Element<VectorX_Tq      , TopoD2, Quad> ElementGlobForceFieldClass;   // qx field
  typedef Element<VectorX_Tq      , TopoD2, Quad> ElementAccelFieldClass;       // a field

  typedef typename MakeTuple<ElementTuple, ElementAinvFieldClass,
                                           ElementMassFieldClass,
                                           ElementFollowForceFieldClass,
                                           ElementGlobForceFieldClass,
                                           ElementAccelFieldClass,
                                           ElementXFieldClass>::type ElementParam;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandClass;
  typedef typename IntegrandClass::template BasisWeighted<T,TopoD2,Quad, ElementParam > BasisWeightedClass;

  VectorX_R g = { 0.023, -9.81 };
  PDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);


  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 6 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );

  int order = 1;

  // Extensional compliance matrix
  ElementAinvFieldClass AinvfldElem(order, BasisFunctionCategory_Hierarchical);

  Real E_00 = 70e9, nu_00 = 1./3.,  t_00 = 1./100.;
  Real E_10 = 80e9, nu_10 = 1./4.,  t_10 = 1./200.;
  Real E_11 = 90e9, nu_11 = 1./5.,  t_11 = 1./300.;
  Real E_01 = 90e7, nu_01 = 5./11., t_01 = 1./400.;

  ComplianceMatrix AijInv_00 = calcComplianceMatrix( E_00, nu_00, t_00 );
  ComplianceMatrix AijInv_10 = calcComplianceMatrix( E_10, nu_10, t_10 );
  ComplianceMatrix AijInv_11 = calcComplianceMatrix( E_11, nu_11, t_11 );
  ComplianceMatrix AijInv_01 = calcComplianceMatrix( E_01, nu_01, t_01 );

  AinvfldElem.DOF(0) = AijInv_00;
  AinvfldElem.DOF(1) = AijInv_10;
  AinvfldElem.DOF(2) = AijInv_11;
  AinvfldElem.DOF(3) = AijInv_01;

  // Lumped shell mass
  ElementMassFieldClass mufldElem(order, BasisFunctionCategory_Hierarchical);

  Real mu_00 = 0.0123;
  Real mu_10 = 3.923;
  Real mu_11 = 9.13;
  Real mu_01 = 0.946;

  mufldElem.DOF(0) = mu_00;
  mufldElem.DOF(1) = mu_10;
  mufldElem.DOF(2) = mu_11;
  mufldElem.DOF(3) = mu_01;

  // Following force
  ElementFollowForceFieldClass qefldElem(order, BasisFunctionCategory_Hierarchical);

  VectorE qe_00 = { -3   ,  4./7. };
  VectorE qe_10 = { 2./5., -2./3. };
  VectorE qe_11 = {  1   ,  -7    };
  VectorE qe_01 = { 1./2.,  1./4. };

  qefldElem.DOF(0) = qe_00;
  qefldElem.DOF(1) = qe_10;
  qefldElem.DOF(2) = qe_11;
  qefldElem.DOF(3) = qe_01;

  // Global forcing
  ElementGlobForceFieldClass qxfldElem(order, BasisFunctionCategory_Hierarchical);

  VectorX_Tq qx_00 = {  23. , 0.0123 };
  VectorX_Tq qx_10 = { 4.935, -2.77 };
  VectorX_Tq qx_11 = {  7.35, 1.43 };
  VectorX_Tq qx_01 = { -9.54, -0.32 };

  qxfldElem.DOF(0) = qx_00;
  qxfldElem.DOF(1) = qx_10;
  qxfldElem.DOF(2) = qx_11;
  qxfldElem.DOF(3) = qx_01;

  // Accelerations
  ElementAccelFieldClass afldElem(order, BasisFunctionCategory_Hierarchical);

  VectorX_Tq a_00 = { 3.245, -1./3. };
  VectorX_Tq a_10 = { 1.43 , 8.79 };
  VectorX_Tq a_11 = { -0.874 , 1.3 };
  VectorX_Tq a_01 = { 0.034, 23.12 };

  afldElem.DOF(0) = a_00;
  afldElem.DOF(1) = a_10;
  afldElem.DOF(2) = a_11;
  afldElem.DOF(3) = a_01;

  // Undeformed geometry (quad grid)
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 4, xfldElem.nDOF() );

  Real x0_00 = 0, y0_00 = 0;
  Real x0_10 = 1, y0_10 = 0;
  Real x0_11 = 1, y0_11 = 1;
  Real x0_01 = 0, y0_01 = 1;

  xfldElem.DOF(0) = {x0_00, y0_00};
  xfldElem.DOF(1) = {x0_10, y0_10};
  xfldElem.DOF(2) = {x0_11, y0_11};
  xfldElem.DOF(3) = {x0_01, y0_01};

  ElementQFieldClass varfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, varfldElem.order() );
  BOOST_CHECK_EQUAL( 4, varfldElem.nDOF() );

  // State vector solution at first cell corner {0,0}
  Real   rx_00 = -1;
  Real   ry_00 = -1;
  Real lam3_00 = 3./5.;
  Real  f11_00 = 3;
  Real  f22_00 = -2./5.;
  Real  f12_00 = 1./2.;

  // State vector solution at second cell corner {1,0}
  Real   rx_10 = 1;
  Real   ry_10 = -1./2.;
  Real lam3_10 = 5./13.;
  Real  f11_10 = 1;
  Real  f22_10 = -1./2.;
  Real  f12_10 = 2;

  // State vector solution at third cell corner {1,1}
  Real   rx_11 = 2;
  Real   ry_11 = 3;
  Real lam3_11 = 8./17.;
  Real  f11_11 = 2;
  Real  f22_11 = 3;
  Real  f12_11 = -1./3.;

  // State vector solution at third cell corner {0,1}
  Real   rx_01 = -1./2.;
  Real   ry_01 = 2;
  Real lam3_01 = 7./25.;
  Real  f11_01 = -5;
  Real  f22_01 = 9;
  Real  f12_01 = 2;

  // Quad solution
  varfldElem.DOF(0) = pde.setDOFFrom( PositionLambdaForces2D(rx_00, ry_00, lam3_00, f11_00, f22_00, f12_00) );
  varfldElem.DOF(1) = pde.setDOFFrom( PositionLambdaForces2D(rx_10, ry_10, lam3_10, f11_10, f22_10, f12_10) );
  varfldElem.DOF(2) = pde.setDOFFrom( PositionLambdaForces2D(rx_11, ry_11, lam3_11, f11_11, f22_11, f12_11) );
  varfldElem.DOF(3) = pde.setDOFFrom( PositionLambdaForces2D(rx_01, ry_01, lam3_01, f11_01, f22_01, f12_01) );

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // Explicitly create a paramater instance.
  // If it was created in the argument below, the temporary variable in the
  // function argument would go out of scope and result in undefined behavior
  ElementParam params = ( AinvfldElem, mufldElem, qefldElem, qxfldElem, afldElem, xfldElem );

  BasisWeightedClass fcn = fcnint.integrand( params, varfldElem );

  //BOOST_CHECK( fcn.N == 1);
  BOOST_CHECK_EQUAL( 6, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );


  typedef ElementXFieldClass::RefCoordType RefCoordType;

  const Real small_tol = 8e-13;
  const Real close_tol = 8e-13;
  RefCoordType sRef;
  Real eps11Integrand;
  Real eps22Integrand;
  Real eps12Integrand;
  Real f1Integrand;
  Real f2Integrand;
  Real e3Integrand;
  ArrayQ_R integrandTrue[4];
  ArrayQS integrand[4];

  // Test at {0,0}
  sRef = {0, 0};
  fcn( sRef, integrand, 4 );

  // PDE residual integrands: (source) + (advective flux)

  // Basis function 1
  eps11Integrand = ( 2.8749999955238095-(5*cos(2.4))/4.+(5*sin(2.4))/4. ) + ( 0 );
  eps22Integrand = ( 2.875000002+(5*cos(2.4))/4.-(5*sin(2.4))/4. ) + ( 0 );
  eps12Integrand = ( -9.523809523809524e-10+(5*cos(2.4))/4.+(5*sin(2.4))/4. ) + ( 0 );
  f1Integrand    = ( 5.666823418505385 ) + ( (7*cos(1.2)+5*sin(1.2))/2. );
  f2Integrand    = ( -23.039314004084986 ) + ( (cos(1.2)+9*sin(1.2))/10. );
  e3Integrand    = ( (19*cos(1.2))/16.+(77*sin(1.2))/8. ) + ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( (-6*cos(1.2)+sin(1.2))/2. );
  f2Integrand    = ( 0. ) + ( -cos(1.2)/2.-(2*sin(1.2))/5. );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( 0 );
  f2Integrand    = ( 0. ) + ( 0 );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 4
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( -cos(1.2)/2.-3*sin(1.2) );
  f2Integrand    = ( 0. ) + ( (2*cos(1.2))/5.-sin(1.2)/2. );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[3] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };


  for (int n = 0; n < pde.N; n++) // Check equations n to pde.N
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[3][n], SurrealValue(integrand[3][n]), small_tol, close_tol );
  }


  // Test at {1,0}
  sRef = {1, 0};
  fcn( sRef, integrand, 4 );

  // PDE residual integrands: (source) + (advective flux)

  // Basis function 1
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( cos(0.7692307692307693)-2*sin(0.7692307692307693) );
  f2Integrand    = ( 0. ) + ( (4*cos(0.7692307692307693)+sin(0.7692307692307693))/2. );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 3.8749999971875-(9*cos(1.5384615384615385))/4.+(15*sin(1.5384615384615385))/8. ) + ( 0 );
  eps22Integrand = ( 3.875000001875+(9*cos(1.5384615384615385))/4.-(15*sin(1.5384615384615385))/8. ) + ( 0 );
  eps12Integrand = ( -6.25e-9+(15*cos(1.5384615384615385))/8.+(9*sin(1.5384615384615385))/4. ) + ( 0 );
  f1Integrand    = ( -52.110609290730594 ) + ( cos(0.7692307692307693)+3*sin(0.7692307692307693) );
  f2Integrand    = ( -54.0170451110021 ) + ( (-5*cos(0.7692307692307693)+3*sin(0.7692307692307693))/2. );
  e3Integrand    = ( (3*(13*cos(0.7692307692307693)+71*sin(0.7692307692307693)))/16. ) + ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( -2*cos(0.7692307692307693)-sin(0.7692307692307693) );
  f2Integrand    = ( 0. ) + ( (cos(0.7692307692307693)-4*sin(0.7692307692307693))/2. );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 4
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( 0 );
  f2Integrand    = ( 0. ) + ( 0 );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[3] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };


  for (int n = 0; n < pde.N; n++) // Check equations n to pde.N
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[3][n], SurrealValue(integrand[3][n]), small_tol, close_tol );
  }

  // Test at {1,1}
  sRef = {1, 1};
  fcn( sRef, integrand, 4 );

  // PDE residual integrands: (source) + (advective flux)

  // Basis function 1
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( 0 );
  f2Integrand    = ( 0. ) + ( 0 );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( -cos(0.9411764705882353)/3.+2*sin(0.9411764705882353) );
  f2Integrand    = ( 0. ) + ( 3*cos(0.9411764705882353)-sin(0.9411764705882353)/3. );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 4.624999995333333-(3*cos(1.8823529411764706))/2.+3*sin(1.8823529411764706) ) + ( 0 );
  eps22Integrand = ( 4.6249999913333335+(3*cos(1.8823529411764706))/2.-3*sin(1.8823529411764706) ) + ( 0 );
  eps12Integrand = ( 1.3333333333333333e-9+3*cos(1.8823529411764706)+(3*sin(1.8823529411764706))/2. ) + ( 0 );
  f1Integrand    = ( -69.93665345709802 ) + ( (-5*cos(0.9411764705882353)-7*sin(0.9411764705882353))/3. );
  f2Integrand    = ( -77.65039970359557 ) + ( (-8*cos(0.9411764705882353)+10*sin(0.9411764705882353))/3. );
  e3Integrand    = ( (3*(18*cos(0.9411764705882353)+79*sin(0.9411764705882353)))/16. ) + ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 4
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( (6*cos(0.9411764705882353)+sin(0.9411764705882353))/3. );
  f2Integrand    = ( 0. ) + ( -cos(0.9411764705882353)/3.-3*sin(0.9411764705882353) );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[3] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };


  for (int n = 0; n < pde.N; n++) // Check equations n to pde.N
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[3][n], SurrealValue(integrand[3][n]), small_tol, close_tol );
  }


  // Test at {0,1}
  sRef = {0, 1};
  fcn( sRef, integrand, 4 );

  // PDE residual integrands: (source) + (advective flux)

  // Basis function 1
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( 2*cos(0.56)-5*sin(0.56) );
  f2Integrand    = ( 0. ) + ( 9*cos(0.56)+2*sin(0.56) );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( 0 );
  f2Integrand    = ( 0. ) + ( 0 );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) + ( 0 );
  eps22Integrand = ( 0 ) + ( 0 );
  eps12Integrand = ( 0 ) + ( 0 );
  f1Integrand    = ( 0. ) + ( 5*cos(0.56)+2*sin(0.56) );
  f2Integrand    = ( 0. ) + ( -2*cos(0.56)+9*sin(0.56) );
  e3Integrand    = ( 0 ) + ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 4
  eps11Integrand = ( 3.6250040404040402-cos(1.12)/2.+(17*sin(1.12))/8. ) + ( 0 );
  eps22Integrand = ( 3.62499498989899+cos(1.12)/2.-(17*sin(1.12))/8. ) + ( 0 );
  eps12Integrand = ( -1.2929292929292929e-6+(17*cos(1.12))/8.+sin(1.12)/2. ) + ( 0 );
  f1Integrand    = ( -17.572523987449564 ) + ( -7*cos(0.56)+3*sin(0.56) );
  f2Integrand    = ( -22.74599323951638 ) + ( -7*cos(0.56)-11*sin(0.56) );
  e3Integrand    = ( (19*(cos(0.56)+9*sin(0.56)))/16. ) + ( 0 );
  integrandTrue[3] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };


  for (int n = 0; n < pde.N; n++) // Check equations n to pde.N
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[3][n], SurrealValue(integrand[3][n]), small_tol, close_tol );
  }

  // Test at {1/2, 1/2}
  sRef = {1./2., 1./2.};
  fcn( sRef, integrand, 4 );

  // PDE residual integrands: (source) + (advective flux)

  // Basis function 1
  eps11Integrand = ( 0.5781250282865799+(37*pow(sin(0.43380090497737556),4))/16.+(33*sin(0.8676018099547511))/32.
                 +(81*pow(sin(0.8676018099547511),2))/64.-(pow(sin(0.43380090497737556),2)*(37+33*sin(0.8676018099547511)))/16. ) + ( 0 );
  eps22Integrand = ( 1.2656249248413685-(81*pow(sin(0.43380090497737556),2))/16.+(33*cos(0.43380090497737556)*pow(sin(0.43380090497737556),3))/8.
                 +(81*pow(sin(0.43380090497737556),4))/16.-(33*sin(0.8676018099547511))/32.+(37*pow(sin(0.8676018099547511),2))/64. ) + ( 0 );
  eps12Integrand = ( 0.5156249573245828-(33*pow(sin(0.43380090497737556),2))/16.-(11*cos(0.43380090497737556)*pow(sin(0.43380090497737556),3))/4.
                 +(33*pow(sin(0.43380090497737556),4))/16.+(11*sin(0.8676018099547511))/16.-(33*pow(sin(0.8676018099547511),2))/64. ) + ( 0 );
  f1Integrand    = ( -11.537127131938005 ) + ( (31-248*pow(sin(0.43380090497737556),6)
       +pow(sin(0.43380090497737556),4)*(372-76*sin(0.8676018099547511))-19*sin(0.8676018099547511)+31*pow(sin(0.8676018099547511),2)
       -19*pow(sin(0.8676018099547511),3)-2*pow(sin(0.43380090497737556),2)*(93-38*sin(0.8676018099547511)+31*pow(sin(0.8676018099547511),2)))/48. );
  f2Integrand    = ( -11.352967651260172 ) + ( (229-1832*pow(sin(0.43380090497737556),6)
   +pow(sin(0.43380090497737556),4)*(2748-416*sin(0.8676018099547511))-104*sin(0.8676018099547511)+229*pow(sin(0.8676018099547511),2)
   -104*pow(sin(0.8676018099547511),3)-2*pow(sin(0.43380090497737556),2)*(687-208*sin(0.8676018099547511)+229*pow(sin(0.8676018099547511),2)))/120.);
  e3Integrand    = ( (3*(23*cos(0.8676018099547511)+129*sin(0.8676018099547511)))/128. ) + ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0.5781250282865799+(37*pow(sin(0.43380090497737556),4))/16.+(33*sin(0.8676018099547511))/32.
                 +(81*pow(sin(0.8676018099547511),2))/64.-(pow(sin(0.43380090497737556),2)*(37+33*sin(0.8676018099547511)))/16. ) + ( 0 );
  eps22Integrand = ( 1.2656249248413685-(81*pow(sin(0.43380090497737556),2))/16.+(33*cos(0.43380090497737556)*pow(sin(0.43380090497737556),3))/8.
                 +(81*pow(sin(0.43380090497737556),4))/16.-(33*sin(0.8676018099547511))/32.+(37*pow(sin(0.8676018099547511),2))/64. ) + ( 0 );
  eps12Integrand = ( 0.5156249573245828-(33*pow(sin(0.43380090497737556),2))/16.-(11*cos(0.43380090497737556)*pow(sin(0.43380090497737556),3))/4.
                 +(33*pow(sin(0.43380090497737556),4))/16.+(11*sin(0.8676018099547511))/16.-(33*pow(sin(0.8676018099547511),2))/64. ) + ( 0 );
  f1Integrand    = ( -11.537127131938005 ) + ( (19-152*pow(sin(0.43380090497737556),6)+31*sin(0.8676018099547511)+19*pow(sin(0.8676018099547511),2)
                 +31*pow(sin(0.8676018099547511),3)+4*pow(sin(0.43380090497737556),4)*(57+31*sin(0.8676018099547511))
                 -2*pow(sin(0.43380090497737556),2)*(57+62*sin(0.8676018099547511)+19*pow(sin(0.8676018099547511),2)))/48. );
  f2Integrand    = ( -11.352967651260172 ) + ( (104-832*pow(sin(0.43380090497737556),6)+229*sin(0.8676018099547511)
          +104*pow(sin(0.8676018099547511),2)+229*pow(sin(0.8676018099547511),3)+4*pow(sin(0.43380090497737556),4)*(312+229*sin(0.8676018099547511))
          -4*pow(sin(0.43380090497737556),2)*(156+229*sin(0.8676018099547511)+52*pow(sin(0.8676018099547511),2)))/120. );
  e3Integrand    = ( (3*(23*cos(0.8676018099547511)+129*sin(0.8676018099547511)))/128. ) + ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0.5781250282865799+(37*pow(sin(0.43380090497737556),4))/16.+(33*sin(0.8676018099547511))/32.
                 +(81*pow(sin(0.8676018099547511),2))/64.-(pow(sin(0.43380090497737556),2)*(37+33*sin(0.8676018099547511)))/16. ) + ( 0 );
  eps22Integrand = ( 1.2656249248413685-(81*pow(sin(0.43380090497737556),2))/16.+(33*cos(0.43380090497737556)*pow(sin(0.43380090497737556),3))/8.
                 +(81*pow(sin(0.43380090497737556),4))/16.-(33*sin(0.8676018099547511))/32.+(37*pow(sin(0.8676018099547511),2))/64. ) + ( 0 );
  eps12Integrand = ( 0.5156249573245828-(33*pow(sin(0.43380090497737556),2))/16.-(11*cos(0.43380090497737556)*pow(sin(0.43380090497737556),3))/4.
                 +(33*pow(sin(0.43380090497737556),4))/16.+(11*sin(0.8676018099547511))/16.-(33*pow(sin(0.8676018099547511),2))/64. ) + ( 0 );
  f1Integrand    = ( -11.537127131938005 ) + ( (-31+248*pow(sin(0.43380090497737556),6)+19*sin(0.8676018099547511)-31*pow(sin(0.8676018099547511),2)
                 +19*pow(sin(0.8676018099547511),3)+4*pow(sin(0.43380090497737556),4)*(-93+19*sin(0.8676018099547511))
                 +2*pow(sin(0.43380090497737556),2)*(93-38*sin(0.8676018099547511)+31*pow(sin(0.8676018099547511),2)))/48. );
  f2Integrand    = ( -11.352967651260172 ) + ( (-229+1832*pow(sin(0.43380090497737556),6)+104*sin(0.8676018099547511)
          -229*pow(sin(0.8676018099547511),2)+104*pow(sin(0.8676018099547511),3)+4*pow(sin(0.43380090497737556),4)*(-687+104*sin(0.8676018099547511))
          +2*pow(sin(0.43380090497737556),2)*(687-208*sin(0.8676018099547511)+229*pow(sin(0.8676018099547511),2)))/120. );
  e3Integrand    = ( (3*(23*cos(0.8676018099547511)+129*sin(0.8676018099547511)))/128. ) + ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 4
  eps11Integrand = ( 0.5781250282865799+(37*pow(sin(0.43380090497737556),4))/16.+(33*sin(0.8676018099547511))/32.
                 +(81*pow(sin(0.8676018099547511),2))/64.-(pow(sin(0.43380090497737556),2)*(37+33*sin(0.8676018099547511)))/16. ) + ( 0 );
  eps22Integrand = ( 1.2656249248413685-(81*pow(sin(0.43380090497737556),2))/16.+(33*cos(0.43380090497737556)*pow(sin(0.43380090497737556),3))/8.
                 +(81*pow(sin(0.43380090497737556),4))/16.-(33*sin(0.8676018099547511))/32.+(37*pow(sin(0.8676018099547511),2))/64. ) + ( 0 );
  eps12Integrand = ( 0.5156249573245828-(33*pow(sin(0.43380090497737556),2))/16.-(11*cos(0.43380090497737556)*pow(sin(0.43380090497737556),3))/4.
                 +(33*pow(sin(0.43380090497737556),4))/16.+(11*sin(0.8676018099547511))/16.-(33*pow(sin(0.8676018099547511),2))/64. ) + ( 0 );
  f1Integrand    = ( -11.537127131938005 ) + ( (-19+152*pow(sin(0.43380090497737556),6)-31*sin(0.8676018099547511)-19*pow(sin(0.8676018099547511),2)
                 -31*pow(sin(0.8676018099547511),3)-4*pow(sin(0.43380090497737556),4)*(57+31*sin(0.8676018099547511))
                 +2*pow(sin(0.43380090497737556),2)*(57+62*sin(0.8676018099547511)+19*pow(sin(0.8676018099547511),2)))/48. );
  f2Integrand    = ( -11.352967651260172 ) + ( (-104+832*pow(sin(0.43380090497737556),6)-229*sin(0.8676018099547511)
           -104*pow(sin(0.8676018099547511),2)-229*pow(sin(0.8676018099547511),3)-4*pow(sin(0.43380090497737556),4)*(312+229*sin(0.8676018099547511))
           +4*pow(sin(0.43380090497737556),2)*(156+229*sin(0.8676018099547511)+52*pow(sin(0.8676018099547511),2)))/120. );
  e3Integrand    = ( (3*(23*cos(0.8676018099547511)+129*sin(0.8676018099547511)))/128. ) + ( 0 );
  integrandTrue[3] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++) // Check equations n to pde.N
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], SurrealValue(integrand[0][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], SurrealValue(integrand[1][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], SurrealValue(integrand[2][n]), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[3][n], SurrealValue(integrand[3][n]), small_tol, close_tol );
  }

  // Test the element integral of the functor with a matching Gauss-quadrature scheme in Mathematica
  //
  // Quad order 3
  // Error: < 0.15%
  //
  // Quad order 4+
  // Error: < 0.00131 %
  //
  // USE QUAD ORDER 4: No increase in accuracy increasing past quad order 4

  int quadratureorder = 4;
  const Real small_tol_res = 0.00131; // Errors are so high because SANS quadrature scheme does not
  const Real close_tol_res = 0.00131; // match well with Mathematica quadrature scheme
  int nIntegrand = varfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD2, Quad, ArrayQS> integral(quadratureorder, nIntegrand);

  Real eps11Res;
  Real eps22Res;
  Real eps12Res;
  Real f1Res;
  Real f2Res;
  Real e3Res;
  ArrayQ_R residualTrue[4];
  ArrayQS residual[4] = {0,0,0,0};

  // cell integration for canonical element
  integral( fcn, xfldElem, residual, nIntegrand );

  // PDE residuals: (source) + (advective flux)

  // Basis function 1
  eps11Res = ( 1.372026572939575 ) + ( 0 );
  eps22Res = ( 0.3363067056858875 ) + ( 0 );
  eps12Res = ( 0.1970192494829645 ) + ( 0 );
  f1Res    = ( -6.166754474989174 ) + ( 0.1568768753392972 );
  f2Res    = ( -7.686675740928013 ) + ( 1.26075504443376 );
  e3Res    = ( 2.506669437094429 ) + ( 0 );
  residualTrue[0] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 2
  eps11Res = ( 1.504796077432906 ) + ( 0 );
  eps22Res = ( 0.3702039018522039 ) + ( 0 );
  eps12Res = ( 0.3185493181381063 ) + ( 0 );
  f1Res    = ( -11.5787460259529 ) + ( 0.7877862481326029 );
  f2Res    = ( -11.85616313948526 ) + ( 1.422006577287272 );
  e3Res    = ( 2.771498263061457 ) + ( 0 );
  residualTrue[1] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 3
  eps11Res = ( 1.630803614584895 ) + ( 0 );
  eps22Res = ( 0.3691963323525386 ) + ( 0 );
  eps12Res = ( 0.2769322114180279 ) + ( 0 );
  f1Res    = ( -15.01107830413221 ) + ( -0.08152857576495446 );
  f2Res    = ( -15.81974746230442 ) + ( -0.09527063626648668 );
  e3Res    = ( 2.8431700215609 ) + ( 0 );
  residualTrue[2] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 4
  eps11Res = ( 1.454811679047038 ) + ( 0 );
  eps22Res = ( 0.3785215596795534 ) + ( 0 );
  eps12Res = ( 0.2464301366891186 ) + ( 0 );
  f1Res    = ( -9.716058362919606 ) + ( -0.8631345475408619 );
  f2Res    = ( -11.04853015640661 ) + ( -2.587490984215213 );
  e3Res    = ( 2.421501474268187 ) + ( 0 );
  residualTrue[3] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };


  for (int n = 0; n < pde.N; n++) // Chage pde.N to 3 to check the first 3 equations
  {
    SANS_CHECK_CLOSE( residualTrue[0][n], SurrealValue(residual[0][n]), small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[1][n], SurrealValue(residual[1][n]), small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[2][n], SurrealValue(residual[2][n]), small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[3][n], SurrealValue(residual[3][n]), small_tol_res, close_tol_res );
  }

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
