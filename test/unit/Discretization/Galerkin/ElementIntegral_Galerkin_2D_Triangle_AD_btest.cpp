// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementIntegral_Galerkin_2D_Triangle_AD_btest
// testing of element residual integrands for Galerkin: Advection-Diffusion on Triangles

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDE1DClassPlusTime;
template class IntegrandCell_Galerkin< PDE1DClassPlusTime >::BasisWeighted<Real, TopoD1, Line, ElementXField<PhysD1,TopoD1, Line> >;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDE2DClassPlusTime;
template class IntegrandCell_Galerkin< PDE2DClassPlusTime >::BasisWeighted<Real, TopoD2, Triangle, ElementXField<PhysD2,TopoD2, Triangle> >;

template class IntegrandCell_Galerkin< PDE2DClassPlusTime >::BasisWeighted<Real, TopoD2, Quad, ElementXField<PhysD2,TopoD2, Quad> >;

typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_None> PDEAdvectionDiffusion3D;
typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDE3DClassPlusTime;
template class IntegrandCell_Galerkin< PDE3DClassPlusTime >::BasisWeighted<Real, TopoD3, Tet, ElementXField<PhysD3,TopoD3, Tet> >;

template class IntegrandCell_Galerkin< PDE3DClassPlusTime >::BasisWeighted<Real, TopoD3, Hex, ElementXField<PhysD3,TopoD3, Hex> >;
}

using namespace SANS;



//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementIntegral_Galerkin_2D_Triangle_AD_test_suite )

//----------------------------------------------------------------------------//
// residual check for complete interior element: P0 w/ const in (x,y)
//
BOOST_AUTO_TEST_CASE( P0const_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldAreaClass;

  typedef ElementXFieldAreaClass ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandLineClass;
  typedef IntegrandLineClass::
      BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedLineClass;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandAreaClass;
  typedef IntegrandAreaClass::BasisWeighted<Real,TopoD2,Triangle, ElementParam > BasisWeightedAreaClass;

  // grid

  int order = 1;
  ElementXFieldAreaClass xarea1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea3(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea4(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldLineClass xedge1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge3(order, BasisFunctionCategory_Hierarchical);

  // grid coordinates
  Real x1, x2, x3, x4, x5, x6;
  Real y1, y2, y3, y4, y5, y6;

  x1 =  0;  y1 =  0;
  x2 =  1;  y2 =  0;
  x3 =  0;  y3 =  1;
  x4 =  1;  y4 =  1;
  x5 = -1;  y5 =  1;
  x6 =  1;  y6 = -1;

  xarea1.DOF(0) = {x1, y1};
  xarea1.DOF(1) = {x2, y2};
  xarea1.DOF(2) = {x3, y3};

  xarea2.DOF(0) = {x4, y4};
  xarea2.DOF(1) = {x3, y3};
  xarea2.DOF(2) = {x2, y2};

  xarea3.DOF(0) = {x5, y5};
  xarea3.DOF(1) = {x1, y1};
  xarea3.DOF(2) = {x3, y3};

  xarea4.DOF(0) = {x6, y6};
  xarea4.DOF(1) = {x2, y2};
  xarea4.DOF(2) = {x1, y1};

  xedge1.DOF(0) = {x2, y2};
  xedge1.DOF(1) = {x3, y3};

  xedge2.DOF(0) = {x3, y3};
  xedge2.DOF(1) = {x1, y1};

  xedge3.DOF(0) = {x1, y1};
  xedge3.DOF(1) = {x2, y2};

  // solution

  order = 0;
  ElementQFieldAreaClass qarea1(order, BasisFunctionCategory_Legendre);
  ElementQFieldAreaClass qarea2(order, BasisFunctionCategory_Legendre);
  ElementQFieldAreaClass qarea3(order, BasisFunctionCategory_Legendre);
  ElementQFieldAreaClass qarea4(order, BasisFunctionCategory_Legendre);

  // solution DOFs: const @ a
  Real a = 2.347;

  qarea1.DOF(0) = a;
  qarea2.DOF(0) = a;
  qarea3.DOF(0) = a;
  qarea4.DOF(0) = a;

  // area quadrature rule (linear: basis grad is const, flux is const)
  int quadratureorderArea = 1;

  // line quadrature rule (linear: basis is linear, flux is const)
  int quadratureorderLine = 1;

  {
  // PDE 1: advection
  //
  // residual = 0

  Real u = 1.731;
  Real v = 0.287;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 0;
  Real kxy = 0;
  Real kyy = 0;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // integrand functors
  IntegrandAreaClass fcnareaint( pde, {0} );
  IntegrandLineClass fcnlineint( pde, {0} );

  BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand( xarea1, qarea1 );
  BasisWeightedLineClass fcnline1 = fcnlineint.integrand( xedge1, CanonicalTraceToCell(0,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea2,qarea2 );
  BasisWeightedLineClass fcnline2 = fcnlineint.integrand( xedge2, CanonicalTraceToCell(1,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea3,qarea3 );
  BasisWeightedLineClass fcnline3 = fcnlineint.integrand( xedge3, CanonicalTraceToCell(2,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea4,qarea4 );


  ArrayQ rsd[1], rsdL[1], rsdR[1];

  rsd[0] = 0; rsdL[0] = 0; rsdR[0] = 0;
  int nIntegrand = 1;
  int nIntegrandL = 1;
  int nIntegrandR = 1;
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

  integralArea( fcnarea1, xarea1, rsdL, nIntegrand );                     rsd[0] += rsdL[0];
  integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];
  integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];
  integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  SANS_CHECK_CLOSE( 0, rsd[0], small_tol, close_tol );
  }

  {
  // PDE 2: diffusion
  //
  // residual = 0

  Real u = 0;
  Real v = 0;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // integrand functors
  IntegrandAreaClass fcnareaint( pde, {0} );
  IntegrandLineClass fcnlineint( pde, {0} );


  BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand( xarea1, qarea1 );
  BasisWeightedLineClass fcnline1 = fcnlineint.integrand( xedge1, CanonicalTraceToCell(0,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea2,qarea2 );
  BasisWeightedLineClass fcnline2 = fcnlineint.integrand( xedge2, CanonicalTraceToCell(1,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea3,qarea3 );
  BasisWeightedLineClass fcnline3 = fcnlineint.integrand( xedge3, CanonicalTraceToCell(2,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea4,qarea4 );


  ArrayQ rsd[1], rsdL[1], rsdR[1];

  rsd[0] = 0; rsdL[0] = 0; rsdR[0] = 0;

  int nIntegrand = 1;
  int nIntegrandL = 1;
  int nIntegrandR = 1;
  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

  integralArea( fcnarea1, xarea1, rsdL, nIntegrand );                     rsd[0] += rsdL[0];
  integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];
  integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];
  integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); rsd[0] += rsdL[0];

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  SANS_CHECK_CLOSE( 0, rsd[0], small_tol, close_tol );
  }

}


//----------------------------------------------------------------------------//
// residual check for complete interior element: P1 w/ linear in (x,y)
//
BOOST_AUTO_TEST_CASE( P1linear_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldAreaClass;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandLineClass;

  typedef ElementXFieldAreaClass ElementParam;
  typedef IntegrandLineClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedLineClass;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandAreaClass;
  typedef IntegrandAreaClass::BasisWeighted<Real,TopoD2,Triangle, ElementParam > BasisWeightedAreaClass;

  // grid

  int order = 1;
  ElementXFieldAreaClass xarea1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea3(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea4(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldLineClass xedge1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge3(order, BasisFunctionCategory_Hierarchical);

  // grid coordinates
  Real x1, x2, x3, x4, x5, x6;
  Real y1, y2, y3, y4, y5, y6;

  x1 =  0;  y1 =  0;
  x2 =  1;  y2 =  0;
  x3 =  0;  y3 =  1;
  x4 =  1;  y4 =  1;
  x5 = -1;  y5 =  1;
  x6 =  1;  y6 = -1;

  xarea1.DOF(0) = {x1, y1};
  xarea1.DOF(1) = {x2, y2};
  xarea1.DOF(2) = {x3, y3};

  xarea2.DOF(0) = {x4, y4};
  xarea2.DOF(1) = {x3, y3};
  xarea2.DOF(2) = {x2, y2};

  xarea3.DOF(0) = {x5, y5};
  xarea3.DOF(1) = {x1, y1};
  xarea3.DOF(2) = {x3, y3};

  xarea4.DOF(0) = {x6, y6};
  xarea4.DOF(1) = {x2, y2};
  xarea4.DOF(2) = {x1, y1};

  xedge1.DOF(0) = {x2, y2};
  xedge1.DOF(1) = {x3, y3};

  xedge2.DOF(0) = {x3, y3};
  xedge2.DOF(1) = {x1, y1};

  xedge3.DOF(0) = {x1, y1};
  xedge3.DOF(1) = {x2, y2};

  // solution

  order = 1;
  ElementQFieldAreaClass qarea1(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea2(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea3(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea4(order, BasisFunctionCategory_Hierarchical);

  // solution DOFs: linear @ a*x + b*y
  Real a =  2.347;
  Real b = -0.993;

  qarea1.DOF(0) = a*x1 + b*y1;
  qarea1.DOF(1) = a*x2 + b*y2;
  qarea1.DOF(2) = a*x3 + b*y3;

  qarea2.DOF(0) = a*x4 + b*y4;
  qarea2.DOF(1) = a*x3 + b*y3;
  qarea2.DOF(2) = a*x2 + b*y2;

  qarea3.DOF(0) = a*x5 + b*y5;
  qarea3.DOF(1) = a*x1 + b*y1;
  qarea3.DOF(2) = a*x3 + b*y3;

  qarea4.DOF(0) = a*x6 + b*y6;
  qarea4.DOF(1) = a*x2 + b*y2;
  qarea4.DOF(2) = a*x1 + b*y1;

  // area quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureorderArea = 1;

  // line quadrature rule (quadratic: basis & flux both linear)
  int quadratureorderLine = 2;

  Real area1 = 0.5;

  {
  // PDE 1: advection
  //
  // residual = (1/3)*area*(u*a + v*b) * {1,1,1}

  Real u = 1.731;
  Real v = 0.287;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 0;
  Real kxy = 0;
  Real kyy = 0;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // integrand functors
  IntegrandAreaClass fcnareaint( pde, {0} );
  IntegrandLineClass fcnlineint( pde, {0} );


  BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand( xarea1, qarea1 );
  BasisWeightedLineClass fcnline1 = fcnlineint.integrand( xedge1, CanonicalTraceToCell(0,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea2,qarea2 );
  BasisWeightedLineClass fcnline2 = fcnlineint.integrand( xedge2, CanonicalTraceToCell(1,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea3,qarea3 );
  BasisWeightedLineClass fcnline3 = fcnlineint.integrand( xedge3, CanonicalTraceToCell(2,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea4,qarea4 );


  ArrayQ rsd[3], rsdL[3], rsdR[3];

  int nIntegrand = 3;
  int nIntegrandL = 3;
  int nIntegrandR = 3;

  for (int n = 0; n < nIntegrand; n++) rsd[n] = rsdL[n] = rsdR[n] = 0;

  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

  integralArea( fcnarea1, xarea1, rsdL, nIntegrand );                     for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  Real rsdTrue = (1./3.)*area1*(u*a + v*b);
  SANS_CHECK_CLOSE( rsdTrue, rsd[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[2], small_tol, close_tol );
  }

  {
  // PDE 2: diffusion
  //
  // residual = 0

  Real u = 0;
  Real v = 0;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // integrand functors
  IntegrandAreaClass fcnareaint( pde, {0} );
  IntegrandLineClass fcnlineint( pde, {0} );


  BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand( xarea1, qarea1 );
  BasisWeightedLineClass fcnline1 = fcnlineint.integrand( xedge1, CanonicalTraceToCell(0,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea2,qarea2 );
  BasisWeightedLineClass fcnline2 = fcnlineint.integrand( xedge2, CanonicalTraceToCell(1,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea3,qarea3 );
  BasisWeightedLineClass fcnline3 = fcnlineint.integrand( xedge3, CanonicalTraceToCell(2,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea4,qarea4 );


  ArrayQ rsd[3], rsdL[3], rsdR[3];

  int nIntegrand = 3;
  int nIntegrandL = 3;
  int nIntegrandR = 3;

  for (int n = 0; n < nIntegrand; n++) rsd[n] = rsdL[n] = rsdR[n] = 0;

  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

  integralArea( fcnarea1, xarea1, rsdL, nIntegrand );                     for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  SANS_CHECK_CLOSE( 0, rsd[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, rsd[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, rsd[2], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
// residual check for complete interior element: P1 w/ quadratic in (x,y)
//
BOOST_AUTO_TEST_CASE( P1quadratic_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldAreaClass;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandLineClass;

  typedef ElementXFieldAreaClass ElementParam;
  typedef IntegrandLineClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam, ElementParam> BasisWeightedLineClass;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandAreaClass;
  typedef IntegrandAreaClass::BasisWeighted<Real,TopoD2,Triangle, ElementParam > BasisWeightedAreaClass;

  // grid

  int order = 1;
  ElementXFieldAreaClass xarea1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea3(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea4(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldLineClass xedge1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge3(order, BasisFunctionCategory_Hierarchical);

  // grid coordinates
  Real x1, x2, x3, x4, x5, x6;
  Real y1, y2, y3, y4, y5, y6;

  x1 =  0;  y1 =  0;
  x2 =  1;  y2 =  0;
  x3 =  0;  y3 =  1;
  x4 =  1;  y4 =  1;
  x5 = -1;  y5 =  1;
  x6 =  1;  y6 = -1;

  xarea1.DOF(0) = {x1, y1};
  xarea1.DOF(1) = {x2, y2};
  xarea1.DOF(2) = {x3, y3};

  xarea2.DOF(0) = {x4, y4};
  xarea2.DOF(1) = {x3, y3};
  xarea2.DOF(2) = {x2, y2};

  xarea3.DOF(0) = {x5, y5};
  xarea3.DOF(1) = {x1, y1};
  xarea3.DOF(2) = {x3, y3};

  xarea4.DOF(0) = {x6, y6};
  xarea4.DOF(1) = {x2, y2};
  xarea4.DOF(2) = {x1, y1};

  xedge1.DOF(0) = {x2, y2};
  xedge1.DOF(1) = {x3, y3};

  xedge2.DOF(0) = {x3, y3};
  xedge2.DOF(1) = {x1, y1};

  xedge3.DOF(0) = {x1, y1};
  xedge3.DOF(1) = {x2, y2};

  // solution

  order = 1;
  ElementQFieldAreaClass qarea1(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea2(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea3(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea4(order, BasisFunctionCategory_Hierarchical);

  // solution DOFs: quadratic @ a*x^2 + b*y^2
  Real a =  2.347;
  Real b = -0.993;

  qarea1.DOF(0) = a*x1*x1 + b*y1*y1;
  qarea1.DOF(1) = a*x2*x2 + b*y2*y2;
  qarea1.DOF(2) = a*x3*x3 + b*y3*y3;

  qarea2.DOF(0) = a*x4*x4 + b*y4*y4;
  qarea2.DOF(1) = a*x3*x3 + b*y3*y3;
  qarea2.DOF(2) = a*x2*x2 + b*y2*y2;

  qarea3.DOF(0) = a*x5*x5 + b*y5*y5;
  qarea3.DOF(1) = a*x1*x1 + b*y1*y1;
  qarea3.DOF(2) = a*x3*x3 + b*y3*y3;

  qarea4.DOF(0) = a*x6*x6 + b*y6*y6;
  qarea4.DOF(1) = a*x2*x2 + b*y2*y2;
  qarea4.DOF(2) = a*x1*x1 + b*y1*y1;

  // area quadrature rule (linear: basis grad is const, flux is linear)
  int quadratureorderArea = 1;

  // line quadrature rule (quadratic: basis & flux both linear)
  int quadratureorderLine = 2;

  Real area1 = 0.5;

  {
  // PDE 1: advection
  //
  // residual = (1/3)*area*(u*a + v*b) * {1,1,1}

  Real u = 1.731;
  Real v = 0.287;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 0;
  Real kxy = 0;
  Real kyy = 0;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // integrand functors
  IntegrandAreaClass fcnareaint( pde, {0} );
  IntegrandLineClass fcnlineint( pde, {0} );

  BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand( xarea1, qarea1 );
  BasisWeightedLineClass fcnline1 = fcnlineint.integrand( xedge1, CanonicalTraceToCell(0,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea2,qarea2 );
  BasisWeightedLineClass fcnline2 = fcnlineint.integrand( xedge2, CanonicalTraceToCell(1,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea3,qarea3 );
  BasisWeightedLineClass fcnline3 = fcnlineint.integrand( xedge3, CanonicalTraceToCell(2,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea4,qarea4 );


  ArrayQ rsd[3], rsdL[3], rsdR[3];

  rsd[0] = 0;  rsdL[0] = 0;  rsdR[0] = 0;
  rsd[1] = 0;  rsdL[1] = 0;  rsdR[1] = 0;
  rsd[2] = 0;  rsdL[2] = 0;  rsdR[2] = 0;

  int nIntegrand = 3;
  int nIntegrandL = 3;
  int nIntegrandR = 3;

  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

  integralArea( fcnarea1, xarea1, rsdL, nIntegrand );                     for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  Real rsdTrue = (1./3.)*area1*(u*a + v*b);
  SANS_CHECK_CLOSE( rsdTrue, rsd[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[2], small_tol, close_tol );
  }

  {
  // PDE 2: diffusion
  //
  // residual = -area*{ (a*kxx + b*kyy), b*kyy, a*kxx }

  Real u = 0;
  Real v = 0;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // integrand functors
  IntegrandAreaClass fcnareaint( pde, {0} );
  IntegrandLineClass fcnlineint( pde, {0} );

  BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand( xarea1, qarea1 );
  BasisWeightedLineClass fcnline1 = fcnlineint.integrand( xedge1, CanonicalTraceToCell(0,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea2,qarea2 );
  BasisWeightedLineClass fcnline2 = fcnlineint.integrand( xedge2, CanonicalTraceToCell(1,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea3,qarea3 );
  BasisWeightedLineClass fcnline3 = fcnlineint.integrand( xedge3, CanonicalTraceToCell(2,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea4,qarea4 );


  ArrayQ rsd[3], rsdL[3], rsdR[3];

  rsd[0] = 0;  rsdL[0] = 0;  rsdR[0] = 0;
  rsd[1] = 0;  rsdL[1] = 0;  rsdR[1] = 0;
  rsd[2] = 0;  rsdL[2] = 0;  rsdR[2] = 0;

  int nIntegrand = 3;
  int nIntegrandL = 3;
  int nIntegrandR = 3;

  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

  integralArea( fcnarea1, xarea1, rsdL, nIntegrand );                     for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  Real rsdTrue[3] = { -area1*(kxx*a + kyy*b), -area1*(kyy*b), -area1*(kxx*a) };
  SANS_CHECK_CLOSE( rsdTrue[0], rsd[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue[1], rsd[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue[2], rsd[2], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
// residual check for complete interior element: P2 w/ linear in (x,y)
//
BOOST_AUTO_TEST_CASE( P2linear_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldAreaClass;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandLineClass;
  typedef ElementXFieldAreaClass ElementParam;
  typedef IntegrandLineClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedLineClass;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandAreaClass;
  typedef IntegrandAreaClass::BasisWeighted<Real,TopoD2,Triangle, ElementParam > BasisWeightedAreaClass;

  // grid

  int order = 1;
  ElementXFieldAreaClass xarea1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea3(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea4(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldLineClass xedge1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge3(order, BasisFunctionCategory_Hierarchical);

  // grid coordinates
  Real x1, x2, x3, x4, x5, x6;
  Real y1, y2, y3, y4, y5, y6;

  x1 =  0;  y1 =  0;
  x2 =  1;  y2 =  0;
  x3 =  0;  y3 =  1;
  x4 =  1;  y4 =  1;
  x5 = -1;  y5 =  1;
  x6 =  1;  y6 = -1;

  xarea1.DOF(0) = {x1, y1};
  xarea1.DOF(1) = {x2, y2};
  xarea1.DOF(2) = {x3, y3};

  xarea2.DOF(0) = {x4, y4};
  xarea2.DOF(1) = {x3, y3};
  xarea2.DOF(2) = {x2, y2};

  xarea3.DOF(0) = {x5, y5};
  xarea3.DOF(1) = {x1, y1};
  xarea3.DOF(2) = {x3, y3};

  xarea4.DOF(0) = {x6, y6};
  xarea4.DOF(1) = {x2, y2};
  xarea4.DOF(2) = {x1, y1};

  xedge1.DOF(0) = {x2, y2};
  xedge1.DOF(1) = {x3, y3};

  xedge2.DOF(0) = {x3, y3};
  xedge2.DOF(1) = {x1, y1};

  xedge3.DOF(0) = {x1, y1};
  xedge3.DOF(1) = {x2, y2};

  // solution

  order = 2;
  ElementQFieldAreaClass qarea1(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea2(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea3(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea4(order, BasisFunctionCategory_Hierarchical);

  // solution DOFs: linear @ a*x + b*y
  Real a =  2.347;
  Real b = -0.993;

  qarea1.DOF(0) = a*x1 + b*y1;
  qarea1.DOF(1) = a*x2 + b*y2;
  qarea1.DOF(2) = a*x3 + b*y3;
  qarea1.DOF(3) = 0;
  qarea1.DOF(4) = 0;
  qarea1.DOF(5) = 0;

  qarea2.DOF(0) = a*x4 + b*y4;
  qarea2.DOF(1) = a*x3 + b*y3;
  qarea2.DOF(2) = a*x2 + b*y2;
  qarea2.DOF(3) = 0;
  qarea2.DOF(4) = 0;
  qarea2.DOF(5) = 0;

  qarea3.DOF(0) = a*x5 + b*y5;
  qarea3.DOF(1) = a*x1 + b*y1;
  qarea3.DOF(2) = a*x3 + b*y3;
  qarea3.DOF(3) = 0;
  qarea3.DOF(4) = 0;
  qarea3.DOF(5) = 0;

  qarea4.DOF(0) = a*x6 + b*y6;
  qarea4.DOF(1) = a*x2 + b*y2;
  qarea4.DOF(2) = a*x1 + b*y1;
  qarea4.DOF(3) = 0;
  qarea4.DOF(4) = 0;
  qarea4.DOF(5) = 0;

  // area quadrature rule (degree 3: basis grad is linear, flux is quadratic)
  int quadratureorderArea = 3;

  // line quadrature rule (degree 4: basis is quadratic, flux is quadratic)
  int quadratureorderLine = 4;

  Real area1 = 0.5;

  {
  // PDE 1: advection
  //
  // residual = (1/3)*area*(u*a + v*b) * {1,1,1,1,1,1}

  Real u = 1.731;
  Real v = 0.287;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 0;
  Real kxy = 0;
  Real kyy = 0;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // integrand functors
  IntegrandAreaClass fcnareaint( pde, {0} );
  IntegrandLineClass fcnlineint( pde, {0} );

  BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand( xarea1, qarea1 );
  BasisWeightedLineClass fcnline1 = fcnlineint.integrand( xedge1, CanonicalTraceToCell(0,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea2,qarea2 );
  BasisWeightedLineClass fcnline2 = fcnlineint.integrand( xedge2, CanonicalTraceToCell(1,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea3,qarea3 );
  BasisWeightedLineClass fcnline3 = fcnlineint.integrand( xedge3, CanonicalTraceToCell(2,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea4,qarea4 );


  ArrayQ rsd[6], rsdL[6], rsdR[6];

  int nIntegrand = 6;
  int nIntegrandL = 6;
  int nIntegrandR = 6;

  for (int n = 0; n < nIntegrand; n++) rsd[n] = rsdL[n] = rsdR[n] = 0;

  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

  integralArea( fcnarea1, xarea1, rsdL, nIntegrand );                     for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  Real rsdTrue = (1./3.)*area1*(u*a + v*b);
  SANS_CHECK_CLOSE( rsdTrue, rsd[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[3], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[5], small_tol, close_tol );
#if 0
  cout << "PDE 1: rsd ="; for (int k = 0; k < 6; k++) { cout << " " << rsd[k]; } cout << endl;
#endif
  }

  {
  // PDE 2: diffusion
  //
  // residual = 0

  Real u = 0;
  Real v = 0;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // integrand functors
  IntegrandAreaClass fcnareaint( pde, {0} );
  IntegrandLineClass fcnlineint( pde, {0} );

  BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand( xarea1, qarea1 );
  BasisWeightedLineClass fcnline1 = fcnlineint.integrand( xedge1, CanonicalTraceToCell(0,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea2,qarea2 );
  BasisWeightedLineClass fcnline2 = fcnlineint.integrand( xedge2, CanonicalTraceToCell(1,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea3,qarea3 );
  BasisWeightedLineClass fcnline3 = fcnlineint.integrand( xedge3, CanonicalTraceToCell(2,1),CanonicalTraceToCell(0,-1),xarea1,qarea1,xarea4,qarea4 );


  ArrayQ rsd[6], rsdL[6], rsdR[6];

  int nIntegrand = 6;
  int nIntegrandL = 6;
  int nIntegrandR = 6;

  for (int n = 0; n < nIntegrand; n++) rsd[n] = rsdL[n] = rsdR[n] = 0;

  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

  integralArea( fcnarea1, xarea1, rsdL, nIntegrand );                     for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  SANS_CHECK_CLOSE( 0, rsd[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, rsd[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, rsd[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, rsd[3], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, rsd[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0, rsd[5], small_tol, close_tol );
#if 0
  cout << "PDE 2: rsd ="; for (int k = 0; k < 6; k++) { cout << " " << rsd[k]; } cout << endl;
#endif
  }
}


//----------------------------------------------------------------------------//
// residual check for complete interior element: P2 w/ quadratic in (x,y)
//
BOOST_AUTO_TEST_CASE( P2quadratic_test )
{
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldLineClass;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldAreaClass;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldAreaClass;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandLineClass;
  typedef ElementXFieldAreaClass ElementParam;
  typedef IntegrandLineClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedLineClass;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandAreaClass;
  typedef IntegrandAreaClass::BasisWeighted<Real,TopoD2,Triangle, ElementParam > BasisWeightedAreaClass;

  typedef TraceToCellRefCoord<Line, TopoD2, Triangle> TraceToCellTriangle;

  // grid

  int order = 1;
  ElementXFieldAreaClass xarea1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea3(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldAreaClass xarea4(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldLineClass xedge1(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge2(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldLineClass xedge3(order, BasisFunctionCategory_Hierarchical);

  // grid coordinates
  Real x1, x2, x3, x4, x5, x6;
  Real y1, y2, y3, y4, y5, y6;

  x1 =  0;  y1 =  0;
  x2 =  1;  y2 =  0;
  x3 =  0;  y3 =  1;
  x4 =  1;  y4 =  1;
  x5 = -1;  y5 =  1;
  x6 =  1;  y6 = -1;


  xarea1.DOF(0) = {x1, y1};
  xarea1.DOF(1) = {x2, y2};
  xarea1.DOF(2) = {x3, y3};

  xarea2.DOF(0) = {x4, y4};
  xarea2.DOF(1) = {x3, y3};
  xarea2.DOF(2) = {x2, y2};

  xarea3.DOF(0) = {x3, y3};
  xarea3.DOF(1) = {x5, y5};
  xarea3.DOF(2) = {x1, y1};

  xarea4.DOF(0) = {x2, y2};
  xarea4.DOF(1) = {x1, y1};
  xarea4.DOF(2) = {x6, y6};

  xedge1.DOF(0) = {x2, y2};
  xedge1.DOF(1) = {x3, y3};

  xedge2.DOF(0) = {x3, y3};
  xedge2.DOF(1) = {x1, y1};

  xedge3.DOF(0) = {x1, y1};
  xedge3.DOF(1) = {x2, y2};

  std::vector<int> trinodes2 = {3, 2, 1};
  std::vector<int> trinodes3 = {2, 4, 0};
  std::vector<int> trinodes4 = {1, 0, 5};
  std::vector<int> nedge1 = {1, 2};
  std::vector<int> nedge2 = {2, 0};
  std::vector<int> nedge3 = {0, 1};

  CanonicalTraceToCell traceR1 = TraceToCellTriangle::getCanonicalTrace(nedge1, trinodes2);
  CanonicalTraceToCell traceR2 = TraceToCellTriangle::getCanonicalTrace(nedge2, trinodes3);
  CanonicalTraceToCell traceR3 = TraceToCellTriangle::getCanonicalTrace(nedge3, trinodes4);

  // solution

  order = 2;
  ElementQFieldAreaClass qarea1(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea2(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea3(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldAreaClass qarea4(order, BasisFunctionCategory_Hierarchical);

  // solution DOFs: quadratic @ a*x^2 + b*y^2
  Real a =  2.347;
  Real b = -0.993;

  qarea1.DOF(0) = 0;
  qarea1.DOF(1) = a;
  qarea1.DOF(2) = b;
  qarea1.DOF(3) = -0.25*(a + b);
  qarea1.DOF(4) = -0.25*b;
  qarea1.DOF(5) = -0.25*a;

  qarea2.DOF(0) = a + b;
  qarea2.DOF(1) = b;
  qarea2.DOF(2) = a;
  qarea2.DOF(3) = -0.25*(a + b);
  qarea2.DOF(4) = -0.25*b;
  qarea2.DOF(5) = -0.25*a;

  qarea3.DOF(0) = b;
  qarea3.DOF(1) = a + b;
  qarea3.DOF(2) = 0;
  qarea3.DOF(3) = -0.25*(a + b);
  qarea3.DOF(4) = -0.25*b;
  qarea3.DOF(5) = -0.25*a;

  qarea4.DOF(0) = a;
  qarea4.DOF(1) = 0;
  qarea4.DOF(2) = a + b;
  qarea4.DOF(3) = -0.25*(a + b);
  qarea4.DOF(4) = -0.25*b;
  qarea4.DOF(5) = -0.25*a;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  ArrayQ q;

  qarea1.eval( 1./3., 1./3., q );
  SANS_CHECK_CLOSE( (a + b)/9., q, small_tol, close_tol );

  qarea2.eval( 1./3., 1./3., q );
  SANS_CHECK_CLOSE( (4*a + 4*b)/9., q, small_tol, close_tol );

  qarea3.eval( 1./3., 1./3., q );
  SANS_CHECK_CLOSE( (a + 4*b)/9., q, small_tol, close_tol );

  qarea4.eval( 1./3., 1./3., q );
  SANS_CHECK_CLOSE( (4*a + b)/9., q, small_tol, close_tol );

  // area quadrature rule (degree 3: basis grad is linear, flux is quadratic)
  int quadratureorderArea = 3;

  // line quadrature rule (degree 4: basis & flux both quadratic)
  int quadratureorderLine = 4;

  Real area1 = 0.5;

  {
  // PDE 1: advection

  Real u = 1.731;
  Real v = 0.287;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 0;
  Real kxy = 0;
  Real kyy = 0;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // integrand functors
  IntegrandAreaClass fcnareaint( pde, {0} );
  IntegrandLineClass fcnlineint( pde, {0} );


  BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand( xarea1, qarea1 );
  BasisWeightedLineClass fcnline1 = fcnlineint.integrand( xedge1, CanonicalTraceToCell(0,1), traceR1, xarea1, qarea1, xarea2, qarea2 );
  BasisWeightedLineClass fcnline2 = fcnlineint.integrand( xedge2, CanonicalTraceToCell(1,1), traceR2, xarea1, qarea1, xarea3, qarea3 );
  BasisWeightedLineClass fcnline3 = fcnlineint.integrand( xedge3, CanonicalTraceToCell(2,1), traceR3, xarea1, qarea1, xarea4, qarea4 );


  ArrayQ rsd[6], rsdL[6], rsdR[6];

  int nIntegrand = 6;
  int nIntegrandL = 6;
  int nIntegrandR = 6;

  for (int n = 0; n < nIntegrand; n++) rsd[n] = rsdL[n] = rsdR[n] = 0;

  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

  integralArea( fcnarea1, xarea1, rsdL, nIntegrand );                     for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

  Real rsdTrue[6];
  rsdTrue[0] = area1*(  u*a +   v*b)/6.;
  rsdTrue[1] = area1*(2*u*a +   v*b)/6.;
  rsdTrue[2] = area1*(  u*a + 2*v*b)/6.;
  rsdTrue[3] = area1*(  u*a +   v*b)*(4./15.);
  rsdTrue[4] = area1*(  u*a + 2*v*b)*(2./15.);
  rsdTrue[5] = area1*(2*u*a +   v*b)*(2./15.);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  SANS_CHECK_CLOSE( rsdTrue[0], rsd[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue[1], rsd[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue[2], rsd[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue[3], rsd[3], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue[4], rsd[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue[5], rsd[5], small_tol, close_tol );
#if 0
  cout << "PDE 1: rsd ="; for (int k = 0; k < 6; k++) { cout << " " << rsd[k]; } cout << endl;
#endif
  }

  {
  // PDE 2: diffusion
  //
  // residual = -(2/3)*area*(a*kxx + b*kyy) {1,1,1,1,1,1}

  Real u = 0;
  Real v = 0;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // integrand functors
  IntegrandAreaClass fcnareaint( pde, {0} );
  IntegrandLineClass fcnlineint( pde, {0} );

  BasisWeightedAreaClass fcnarea1 = fcnareaint.integrand( xarea1, qarea1 );
  BasisWeightedLineClass fcnline1 = fcnlineint.integrand( xedge1, CanonicalTraceToCell(0,1), traceR1, xarea1, qarea1, xarea2, qarea2 );
  BasisWeightedLineClass fcnline2 = fcnlineint.integrand( xedge2, CanonicalTraceToCell(1,1), traceR2, xarea1, qarea1, xarea3, qarea3 );
  BasisWeightedLineClass fcnline3 = fcnlineint.integrand( xedge3, CanonicalTraceToCell(2,1), traceR3, xarea1, qarea1, xarea4, qarea4 );


  ArrayQ rsd[6], rsdL[6], rsdR[6];

  int nIntegrand = 6;
  int nIntegrandL = 6;
  int nIntegrandR = 6;

  for (int n = 0; n < nIntegrand; n++) rsd[n] = rsdL[n] = rsdR[n] = 0;

  GalerkinWeightedIntegral<TopoD2, Triangle, ArrayQ> integralArea(quadratureorderArea, nIntegrand);
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralLine(quadratureorderLine, nIntegrandL, nIntegrandR);

  integralArea( fcnarea1, xarea1, rsdL, nIntegrand );                     for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline1, xedge1, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline2, xedge2, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];
  integralLine( fcnline3, xedge3, rsdL, nIntegrandL, rsdR, nIntegrandR ); for (int n = 0; n < nIntegrand; n++) rsd[n] += rsdL[n];

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  Real rsdTrue = -area1*(kxx*a + kyy*b)*(2./3.);
  SANS_CHECK_CLOSE( rsdTrue, rsd[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[3], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdTrue, rsd[5], small_tol, close_tol );
#if 0
  cout << "PDE 2: rsd ="; for (int k = 0; k < 6; k++) { cout << " " << rsd[k]; } cout << endl;
#endif
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
