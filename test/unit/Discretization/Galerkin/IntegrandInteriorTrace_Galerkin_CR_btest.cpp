// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandInteriorTrace_Galerkin_CR_btest
// testing of trace element residual integrands for Galerkin: Cauchy-Riemann

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"

#include "SANS_btest.h"

#include "Surreal/SurrealS.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/CauchyRiemann/PDECauchyRiemann2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDE2DClassPlusTime;

typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTriangle;

template class IntegrandInteriorTrace_Galerkin<PDE2DClassPlusTime>::
BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementXFieldTriangle,ElementXFieldTriangle>;

typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldQuad;
template class IntegrandInteriorTrace_Galerkin<PDE2DClassPlusTime>::
BasisWeighted<Real,TopoD1,Line,TopoD2,Quad,Quad,ElementXFieldQuad,ElementXFieldQuad>;
}

using namespace SANS;



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandInteriorTrace_Galerkin_CR_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_Triangle_test )
{
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDECauchyRiemann2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedClass;

  PDEClass pde;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) = {1, 2};
  qfldElemL.DOF(1) = {3, 4};
  qfldElemL.DOF(2) = {5, 6};

  // triangle solution (right)
  qfldElemR.DOF(0) = {3, 7};
  qfldElemR.DOF(1) = {4, 2};
  qfldElemR.DOF(2) = {8, 9};

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                             xfldElemL, qfldElemL,
                                             xfldElemR, qfldElemR );

  BOOST_CHECK( fcnint.N == 2);
  BOOST_CHECK_EQUAL( 2, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  ArrayQ integrandLTrue[3];
  ArrayQ integrandRTrue[3];
  ArrayQ integrandL[3];
  ArrayQ integrandR[3];

  sRef = 0;
  fcn( sRef, integrandL, 3, integrandR, 3 );

  // PDE residuals (left)
  integrandLTrue[0] = {0, 0};
  integrandLTrue[1] = {-2.5 + 6*sqrt(2), -2.5 - 1./sqrt(2)};
  integrandLTrue[2] = {0, 0};

  // PDE residuals (right)
  integrandRTrue[0] = {0, 0};
  integrandRTrue[1] = {0, 0};
  integrandRTrue[2] = {2.5 - 6*sqrt(2), 2.5 + 1./sqrt(2)};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandLTrue[0][k], integrandL[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLTrue[1][k], integrandL[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLTrue[2][k], integrandL[2][k], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandRTrue[0][k], integrandR[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandRTrue[1][k], integrandR[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandRTrue[2][k], integrandR[2][k], small_tol, close_tol );
  }


  sRef = 1;
  fcn( sRef, integrandL, 3, integrandR, 3 );

  // PDE residuals (left)
  integrandLTrue[0] = {0, 0};
  integrandLTrue[1] = {0, 0};
  integrandLTrue[2] = {0.25*(2 + 17*sqrt(2)), 2 + 0.25*sqrt(2)};

  // PDE residuals (right)
  integrandRTrue[0] = {0, 0};
  integrandRTrue[1] = {-0.25*(2 + 17*sqrt(2)), -2 - 0.25*sqrt(2)};
  integrandRTrue[2] = {0, 0};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandLTrue[0][k], integrandL[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLTrue[1][k], integrandL[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLTrue[2][k], integrandL[2][k], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandRTrue[0][k], integrandR[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandRTrue[1][k], integrandR[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandRTrue[2][k], integrandR[2][k], small_tol, close_tol );
  }


  sRef = 0.5;
  fcn( sRef, integrandL, 3, integrandR, 3 );

  // PDE residuals (left)
  integrandLTrue[0] = {0, 0};
  integrandLTrue[1] = {-0.5 + 41*sqrt(2)/16., -(2 + sqrt(2))/16.};
  integrandLTrue[2] = {-0.5 + 41*sqrt(2)/16., -(2 + sqrt(2))/16.};

  // PDE residuals (right)
  integrandRTrue[0] = {0, 0};
  integrandRTrue[1] = {0.5 - 41*sqrt(2)/16., (2 + sqrt(2))/16.};
  integrandRTrue[2] = {0.5 - 41*sqrt(2)/16., (2 + sqrt(2))/16.};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrandLTrue[0][k], integrandL[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLTrue[1][k], integrandL[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandLTrue[2][k], integrandL[2][k], small_tol, close_tol );

    SANS_CHECK_CLOSE( integrandRTrue[0][k], integrandR[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandRTrue[1][k], integrandR[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandRTrue[2][k], integrandR[2][k], small_tol, close_tol );
  }


  // test the trace element integral of the functor

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureorder = 2;

  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[3] = {0,0,0};
  ArrayQ rsdPDEElemR[3] = {0,0,0};

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDEElemL, nIntegrandL, rsdPDEElemR, nIntegrandR );

  // PDE residuals (left)
  ArrayQ rsd1L = {0, 0};
  ArrayQ rsd2L = {65/12. - 0.25*3*sqrt(2), -0.25 - 1./sqrt(2)};
  ArrayQ rsd3L = {29/6. - 0.25*sqrt(2), 0.25*sqrt(2)};

  // PDE residuals (right)
  ArrayQ rsd1R = {0, 0};
  ArrayQ rsd2R = {-29/6. + 0.25*sqrt(2), -0.25*sqrt(2)};
  ArrayQ rsd3R = {-65/12. + 0.25*3*sqrt(2), 0.25 + 1./sqrt(2)};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( rsd1L[k], rsdPDEElemL[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2L[k], rsdPDEElemL[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3L[k], rsdPDEElemL[2][k], small_tol, close_tol );

    SANS_CHECK_CLOSE( rsd1R[k], rsdPDEElemR[0][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2R[k], rsdPDEElemR[1][k], small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3R[k], rsdPDEElemR[2][k], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin_2D_Triangle_Triangle_Surreal_test )
{
  typedef SurrealS<2> SurrealClass;
  typedef PDENDConvertSpace<PhysD2, PDECauchyRiemann2D> PDEClass;
  typedef PDECauchyRiemann2D::template ArrayQ<SurrealClass> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<SurrealClass,TopoD1,Line,TopoD2,Triangle,Triangle,ElementXFieldCell,ElementXFieldCell> BasisWeightedClass;

  PDEClass pde;

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 2 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == false );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) = {1, 2};
  qfldElemL.DOF(1) = {3, 4};
  qfldElemL.DOF(2) = {5, 6};

  // jacobians wrt qfldElemL.DOF(1)
  qfldElemL.DOF(1)[0].deriv(0) = 1;
  qfldElemL.DOF(1)[1].deriv(1) = 1;

  // triangle solution (right)
  qfldElemR.DOF(0) = {3, 7};
  qfldElemR.DOF(1) = {4, 2};
  qfldElemR.DOF(2) = {8, 9};

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                             xfldElemL, qfldElemL,
                                             xfldElemR, qfldElemR );

  BOOST_CHECK( fcnint.N == 2);
  BOOST_CHECK_EQUAL( 2, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  ArrayQ integrandL[3];
  ArrayQ integrandR[3];

  {
  sRef = 0;
  fcn( sRef, integrandL, 3, integrandR, 3 );

  // PDE residuals (left)
  Real integrand1LData[2] = {0, 0};
  Real integrand2LData[2] = {-2.5 + 6*sqrt(2), -2.5 - 1./sqrt(2)};
  Real integrand3LData[2] = {0, 0};

  Real integrand1LDeriv0[2] = {0, 0};
  Real integrand2LDeriv0[2] = {0.5 + 0.25*sqrt(2), 0.5/sqrt(2)};
  Real integrand3LDeriv0[2] = {0, 0};

  Real integrand1LDeriv1[2] = {0, 0};
  Real integrand2LDeriv1[2] = {0.5/sqrt(2), 0.5 - 0.25*sqrt(2)};
  Real integrand3LDeriv1[2] = {0, 0};

  // PDE residuals (right)
  Real integrand1RData[2] = {0, 0};
  Real integrand2RData[2] = {0, 0};
  Real integrand3RData[2] = {2.5 - 6*sqrt(2), 2.5 + 1./sqrt(2)};

  Real integrand1RDeriv0[2] = {0, 0};
  Real integrand2RDeriv0[2] = {0, 0};
  Real integrand3RDeriv0[2] = {-0.5 - 0.25*sqrt(2), -0.5/sqrt(2)};

  Real integrand1RDeriv1[2] = {0, 0};
  Real integrand2RDeriv1[2] = {0, 0};
  Real integrand3RDeriv1[2] = {-0.5/sqrt(2), -0.5 + 0.25*sqrt(2)};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrand1LData[k], integrandL[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2LData[k], integrandL[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3LData[k], integrandL[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1RData[k], integrandR[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2RData[k], integrandR[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3RData[k], integrandR[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1LDeriv0[k], integrandL[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2LDeriv0[k], integrandL[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3LDeriv0[k], integrandL[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1RDeriv0[k], integrandR[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2RDeriv0[k], integrandR[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3RDeriv0[k], integrandR[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1LDeriv1[k], integrandL[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2LDeriv1[k], integrandL[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3LDeriv1[k], integrandL[2][k].deriv(1), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1RDeriv1[k], integrandR[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2RDeriv1[k], integrandR[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3RDeriv1[k], integrandR[2][k].deriv(1), small_tol, close_tol );
  }
  }


  {
  sRef = 1;
  fcn( sRef, integrandL, 3, integrandR, 3 );

  // PDE residuals (left)
  Real integrand1LData[2] = {0, 0};
  Real integrand2LData[2] = {0, 0};
  Real integrand3LData[2] = {0.25*(2 + 17*sqrt(2)), 2 + 0.25*sqrt(2)};

  Real integrand1LDeriv0[2] = {0, 0};
  Real integrand2LDeriv0[2] = {0, 0};
  Real integrand3LDeriv0[2] = {0, 0};

  Real integrand1LDeriv1[2] = {0, 0};
  Real integrand2LDeriv1[2] = {0, 0};
  Real integrand3LDeriv1[2] = {0, 0};

  // PDE residuals (right)
  Real integrand1RData[2] = {0, 0};
  Real integrand2RData[2] = {-0.25*(2 + 17*sqrt(2)), -2 - 0.25*sqrt(2)};
  Real integrand3RData[2] = {0, 0};

  Real integrand1RDeriv0[2] = {0, 0};
  Real integrand2RDeriv0[2] = {0, 0};
  Real integrand3RDeriv0[2] = {0, 0};

  Real integrand1RDeriv1[2] = {0, 0};
  Real integrand2RDeriv1[2] = {0, 0};
  Real integrand3RDeriv1[2] = {0, 0};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrand1LData[k], integrandL[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2LData[k], integrandL[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3LData[k], integrandL[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1RData[k], integrandR[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2RData[k], integrandR[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3RData[k], integrandR[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1LDeriv0[k], integrandL[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2LDeriv0[k], integrandL[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3LDeriv0[k], integrandL[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1RDeriv0[k], integrandR[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2RDeriv0[k], integrandR[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3RDeriv0[k], integrandR[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1LDeriv1[k], integrandL[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2LDeriv1[k], integrandL[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3LDeriv1[k], integrandL[2][k].deriv(1), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1RDeriv1[k], integrandR[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2RDeriv1[k], integrandR[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3RDeriv1[k], integrandR[2][k].deriv(1), small_tol, close_tol );
  }
  }

  {
  sRef = 0.5;
  fcn( sRef, integrandL, 3, integrandR, 3 );

  // PDE residuals (left)
  Real integrand1LData[2] = {0, 0};
  Real integrand2LData[2] = {-0.5 + 41*sqrt(2)/16., -(2 + sqrt(2))/16.};
  Real integrand3LData[2] = {-0.5 + 41*sqrt(2)/16., -(2 + sqrt(2))/16.};

  Real integrand1LDeriv0[2] = {0, 0};
  Real integrand2LDeriv0[2] = {(2 + sqrt(2))/16., 0.125/sqrt(2)};
  Real integrand3LDeriv0[2] = {(2 + sqrt(2))/16., 0.125/sqrt(2)};

  Real integrand1LDeriv1[2] = {0, 0};
  Real integrand2LDeriv1[2] = {0.125/sqrt(2), (2 - sqrt(2))/16.};
  Real integrand3LDeriv1[2] = {0.125/sqrt(2), (2 - sqrt(2))/16.};

  // PDE residuals (right)
  Real integrand1RData[2] = {0, 0};
  Real integrand2RData[2] = {0.5 - 41*sqrt(2)/16., (2 + sqrt(2))/16.};
  Real integrand3RData[2] = {0.5 - 41*sqrt(2)/16., (2 + sqrt(2))/16.};

  Real integrand1RDeriv0[2] = {0, 0};
  Real integrand2RDeriv0[2] = {(-2 - sqrt(2))/16., -0.125/sqrt(2)};
  Real integrand3RDeriv0[2] = {(-2 - sqrt(2))/16., -0.125/sqrt(2)};

  Real integrand1RDeriv1[2] = {0, 0};
  Real integrand2RDeriv1[2] = {-0.125/sqrt(2), (-2 + sqrt(2))/16.};
  Real integrand3RDeriv1[2] = {-0.125/sqrt(2), (-2 + sqrt(2))/16.};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( integrand1LData[k], integrandL[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2LData[k], integrandL[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3LData[k], integrandL[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1RData[k], integrandR[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2RData[k], integrandR[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3RData[k], integrandR[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1LDeriv0[k], integrandL[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2LDeriv0[k], integrandL[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3LDeriv0[k], integrandL[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1RDeriv0[k], integrandR[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2RDeriv0[k], integrandR[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3RDeriv0[k], integrandR[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1LDeriv1[k], integrandL[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2LDeriv1[k], integrandL[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3LDeriv1[k], integrandL[2][k].deriv(1), small_tol, close_tol );

    SANS_CHECK_CLOSE( integrand1RDeriv1[k], integrandR[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand2RDeriv1[k], integrandR[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( integrand3RDeriv1[k], integrandR[2][k].deriv(1), small_tol, close_tol );
  }
  }


  // test the trace element integral of the functor

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureorder = 2;

  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[3] = {0,0,0};
  ArrayQ rsdPDEElemR[3] = {0,0,0};

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDEElemL, nIntegrandL, rsdPDEElemR, nIntegrandR );

  // PDE residuals (left)
  Real rsd1LData[2] = {0, 0};
  Real rsd2LData[2] = {65/12. - 0.25*3*sqrt(2), -0.25 - 1./sqrt(2)};
  Real rsd3LData[2] = {29/6. - 0.25*sqrt(2), 0.25*sqrt(2)};

  Real rsd1LDeriv0[2] = {0, 0};
  Real rsd2LDeriv0[2] = {(1 + sqrt(2))/6., 1./6.};
  Real rsd3LDeriv0[2] = {(1 + sqrt(2))/12., 1./12.};

  Real rsd1LDeriv1[2] = {0, 0};
  Real rsd2LDeriv1[2] = {1./6., (-1 + sqrt(2))/6.};
  Real rsd3LDeriv1[2] = {1./12., (-1 + sqrt(2))/12.};

  // PDE residuals (right)
  Real rsd1RData[2] = {0, 0};
  Real rsd2RData[2] = {-29/6. + 0.25*sqrt(2), -0.25*sqrt(2)};
  Real rsd3RData[2] = {-65/12. + 0.25*3*sqrt(2), 0.25 + 1./sqrt(2)};

  Real rsd1RDeriv0[2] = {0, 0};
  Real rsd2RDeriv0[2] = {(-1 - sqrt(2))/12., -1./12.};
  Real rsd3RDeriv0[2] = {(-1 - sqrt(2))/6., -1/6.};

  Real rsd1RDeriv1[2] = {0, 0};
  Real rsd2RDeriv1[2] = {-1./12., (1 - sqrt(2))/12.};
  Real rsd3RDeriv1[2] = {-1/6., (1 - sqrt(2))/6.};

  for (int k = 0; k < 2; k++)
  {
    SANS_CHECK_CLOSE( rsd1LData[k], rsdPDEElemL[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2LData[k], rsdPDEElemL[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3LData[k], rsdPDEElemL[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( rsd1RData[k], rsdPDEElemR[0][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2RData[k], rsdPDEElemR[1][k].value(), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3RData[k], rsdPDEElemR[2][k].value(), small_tol, close_tol );

    SANS_CHECK_CLOSE( rsd1LDeriv0[k], rsdPDEElemL[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2LDeriv0[k], rsdPDEElemL[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3LDeriv0[k], rsdPDEElemL[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( rsd1RDeriv0[k], rsdPDEElemR[0][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2RDeriv0[k], rsdPDEElemR[1][k].deriv(0), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3RDeriv0[k], rsdPDEElemR[2][k].deriv(0), small_tol, close_tol );

    SANS_CHECK_CLOSE( rsd1LDeriv1[k], rsdPDEElemL[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2LDeriv1[k], rsdPDEElemL[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3LDeriv1[k], rsdPDEElemL[2][k].deriv(1), small_tol, close_tol );

    SANS_CHECK_CLOSE( rsd1RDeriv1[k], rsdPDEElemR[0][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd2RDeriv1[k], rsdPDEElemR[1][k].deriv(1), small_tol, close_tol );
    SANS_CHECK_CLOSE( rsd3RDeriv1[k], rsdPDEElemR[2][k].deriv(1), small_tol, close_tol );
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
