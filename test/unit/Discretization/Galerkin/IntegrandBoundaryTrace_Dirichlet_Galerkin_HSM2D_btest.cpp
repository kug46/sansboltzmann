// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandBoundaryTrace_Dirichlet_Galerkin_HSM2D_btest
// testing of boundary integrands: PDEHSM2D

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/HSM/BCHSM2D.h"
#include "pde/HSM/PDEHSM2D.h"
#include "pde/HSM/ComplianceMatrix.h"
//#include "pde/NDConvert/NDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
//#include "pde/NDConvert/NDConvertSpace3D.h"
//#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_Galerkin_HSM2D.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"

#include "Field/Tuple/FieldTuple.h"

using namespace std;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandBoundaryTrace_Dirichlet_Galerkin_HSM2D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_X1Q1_sansLG_BCTypeDisplacements_test )
{
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCHSM2D<BCTypeDisplacements, PDEClass> BCHSM2D;
  typedef BCNDConvertSpace<PhysD2, BCHSM2D> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSM2D<VarTypeLambda>::VectorE<Real> VectorE;
  typedef PDEHSM2D<VarTypeLambda>::ComplianceMatrix<Real> ComplianceMatrix;

  typedef Real Tm;
  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX_Tq;

  // Parameter types
  typedef Element<ComplianceMatrix, TopoD2, Triangle> ElementAinvFieldCell;        // Ainv field
  typedef Element<Tm              , TopoD2, Triangle> ElementMassFieldCell;        // mu field
  typedef Element<VectorE         , TopoD2, Triangle> ElementFollowForceFieldCell; // qe field
  typedef Element<VectorX_Tq      , TopoD2, Triangle> ElementGlobForceFieldCell;   // qx field
  typedef Element<VectorX_Tq      , TopoD2, Triangle> ElementAccelFieldCell;       // a field

  typedef typename MakeTuple<ElementTuple, ElementAinvFieldCell,
                                             ElementMassFieldCell,
                                             ElementFollowForceFieldCell,
                                             ElementGlobForceFieldCell,
                                             ElementAccelFieldCell,
                                             ElementXFieldCell>::type ElementParam;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedClass;

  // PDE
  VectorX g = { 0.023, -9.81};
  NDPDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  // BC
  Real rshiftx = 0.21;
  Real rshifty = -0.043;
  BCClass bc(pde, rshiftx, rshifty);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 2 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // Initialize parameters to 0 (not used in boundary trace calculations)

  // Extensional compliance matrix
  ElementAinvFieldCell AinvfldElem(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < xfldElem.nDOF(); i++)
    AinvfldElem.DOF(i) = 0;

  // Lumped shell mass
  ElementMassFieldCell mufldElem(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < xfldElem.nDOF(); i++)
    mufldElem.DOF(i) = 0;

  // Following force
  ElementFollowForceFieldCell qefldElem(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < xfldElem.nDOF(); i++)
    qefldElem.DOF(i) = 0;

  // Global forcing
  ElementGlobForceFieldCell qxfldElem(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < xfldElem.nDOF(); i++)
    qxfldElem.DOF(i) = 0;

  // Accelerations
  ElementAccelFieldCell afldElem(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < xfldElem.nDOF(); i++)
    afldElem.DOF(i) = 0;

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell varfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, varfldElem.order() );
  BOOST_CHECK_EQUAL( 3, varfldElem.nDOF() );

  // triangle solution (left)

  // State vector solution at first cell corner {0,0}
  Real   rx_00 = -1;
  Real   ry_00 = -1;
  Real lam3_00 = 3./5.;
  Real  f11_00 = 3;
  Real  f22_00 = -2./5.;
  Real  f12_00 = 1./2.;

  // State vector solution at second cell corner {1,0}
  Real  rx_10 = 1;
  Real  ry_10 = -1./2.;
  Real lam3_10 = 5./13.;
  Real  f11_10 = 1;
  Real  f22_10 = -1./2.;
  Real  f12_10 = 2;

  // State vector solution at third cell corner {0,1}
  Real   rx_01 = -1./2.;
  Real   ry_01 = 2;
  Real lam3_01 = 7./25.;
  Real  f11_01 = -5;
  Real  f22_01 = 9;
  Real  f12_01 = 2;

  // Triangle solution
  varfldElem.DOF(0) = pde.setDOFFrom( PositionLambdaForces2D(rx_00, ry_00, lam3_00, f11_00, f22_00, f12_00) );
  varfldElem.DOF(1) = pde.setDOFFrom( PositionLambdaForces2D(rx_10, ry_10, lam3_10, f11_10, f22_10, f12_10) );
  varfldElem.DOF(2) = pde.setDOFFrom( PositionLambdaForces2D(rx_01, ry_01, lam3_01, f11_01, f22_01, f12_01) );


  // integrand functor

  StabilizationNitsche stab(order);
  IntegrandClass fcnint( pde, bc, {0}, stab );

  // Explicitly create a paramater instance.
  // If it was created in the argument below, the temporary variable in the
  // function argument would go out of scope and result in undefined behavior
  ElementParam params = ( AinvfldElem, mufldElem, qefldElem, qxfldElem, afldElem, xfldElem );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), params, varfldElem );

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;
  RefCoordTraceType sRef;
  Real eps11Integrand;
  Real eps22Integrand;
  Real eps12Integrand;
  Real f1Integrand;
  Real f2Integrand;
  Real e3Integrand;
  ArrayQ integrandTrue[3];
  ArrayQ integrand[3];

  // Displacement BC integrand test at s = 0
  sRef = 0;
  fcn( sRef, integrand, 3 );

  // Boundary integrands: (boundary flux) + (BC term)

  // Basis function 1
  eps11Integrand = ( 0 ) - ( 0. );
  eps22Integrand = ( 0 ) - ( 0. );
  eps12Integrand = ( 0 ) - ( 0. );
  f1Integrand    = ( 0 ) - ( 0 );
  f2Integrand    = ( 0 ) - ( 0 );
  e3Integrand    = ( 0 ) - ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) - ( -1.4923973333156284 );
  eps22Integrand = ( 0 ) - ( -0.009851023315191783 );
  eps12Integrand = ( 0 ) - ( -0.6333914182431661 );
  f1Integrand    = ( (pow(2,-1/2.))*((3)*(cos(10/13.))+(-1)*(sin(10/13.))) ) - ( 0 );
  f2Integrand    = ( (1/2.)*((pow(2,-1/2.))*((3)*(cos(10/13.))+(5)*(sin(10/13.)))) ) - ( 0 );
  e3Integrand    = ( 0 ) - ( -0.22769145671191415 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) - ( 0. );
  eps22Integrand = ( 0 ) - ( 0. );
  eps12Integrand = ( 0 ) - ( 0. );
  f1Integrand    = ( 0 ) - ( 0 );
  f2Integrand    = ( 0 ) - ( 0 );
  e3Integrand    = ( 0 ) - ( 0. );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], integrand[2][n], small_tol, close_tol );
  }


  // Displacement BC integrand test at s = 1
  sRef = 1;
  fcn( sRef, integrand, 3 );

  // Boundary integrands: (boundary flux) + (BC term)

  // Basis function 1
  eps11Integrand = ( 0 ) - ( 0. );
  eps22Integrand = ( 0 ) - ( 0. );
  eps12Integrand = ( 0 ) - ( 0. );
  f1Integrand    = ( 0 ) - ( 0 );
  f2Integrand    = ( 0 ) - ( 0 );
  e3Integrand    = ( 0 ) - ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) - ( 0. );
  eps22Integrand = ( 0 ) - ( 0. );
  eps12Integrand = ( 0 ) - ( 0. );
  f1Integrand    = ( 0 ) - ( 0 );
  f2Integrand    = ( 0 ) - ( 0 );
  e3Integrand    = ( 0 ) - ( 0. );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) - ( 0.6942355269369065 );
  eps22Integrand = ( 0 ) - ( 0.6319432411784635 );
  eps12Integrand = ( 0 ) - ( 2.9152183316408724 );
  f1Integrand    = ( (-1)*((pow(2,-1/2.))*((3)*(cos(14/25.))+(7)*(sin(14/25.)))) ) - ( 0 );
  f2Integrand    = ( (pow(2,-1/2.))*((11)*(cos(14/25.))+(-7)*(sin(14/25.))) ) - ( 0 );
  e3Integrand    = ( 0 ) - ( 4.0283910668728655 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], integrand[2][n], small_tol, close_tol );
  }


  // Displacement BC integrand test at s = 1/2.
  sRef = 1/2.;
  fcn( sRef, integrand, 3 );

  // Boundary integrands: (boundary flux) + (BC term)

  // Basis function 1
  eps11Integrand = ( 0 ) - ( 0. );
  eps22Integrand = ( 0 ) - ( 0. );
  eps12Integrand = ( 0 ) - ( 0. );
  f1Integrand    = ( 0 ) - ( 0 );
  f2Integrand    = ( 0 ) - ( 0 );
  e3Integrand    = ( 0 ) - ( 0. );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) - ( -0.10353454742853441 );
  eps22Integrand = ( 0 ) - ( 0.05951715029967183 );
  eps12Integrand = ( 0 ) - ( 0.4777969823763537 );
  f1Integrand    = ( (-1)*((pow(2,1/2.))*((sin(216/325.))*(1+(-4)*(pow(sin(108/325.),2))
                     +(4)*(pow(sin(108/325.),4))+pow(sin(216/325.),2)))) )               - ( 0 );
  f2Integrand    = ( (1/8.)*((pow(2,-1/2.))*(25+(72)*((cos(108/325.))*(pow(sin(108/325.),3)))
                     +(300)*(pow(sin(108/325.),4))+(-72)*((cos(108/325.))*(pow(sin(108/325.),5)))
                     +(-200)*(pow(sin(108/325.),6))+(-9)*(sin(216/325.))+(25)*(pow(sin(216/325.),2))
                     +(-9)*(pow(sin(216/325.),3))+(-50)*((pow(sin(108/325.),2))*(3+pow(sin(216/325.),2))))) ) - ( 0 );
  e3Integrand    = ( 0 ) - ( 0.9680022302377039 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) - ( -0.10353454742853441 );
  eps22Integrand = ( 0 ) - ( 0.05951715029967183 );
  eps12Integrand = ( 0 ) - ( 0.4777969823763537 );
  f1Integrand    = ( (-1)*((pow(2,1/2.))*((sin(216/325.))*(1+(-4)*(pow(sin(108/325.),2))
                     +(4)*(pow(sin(108/325.),4))+pow(sin(216/325.),2)))) )               - ( 0 );
  f2Integrand    = ( (1/8.)*((pow(2,-1/2.))*(25+(72)*((cos(108/325.))*(pow(sin(108/325.),3)))
                     +(300)*(pow(sin(108/325.),4))+(-72)*((cos(108/325.))*(pow(sin(108/325.),5)))
                     +(-200)*(pow(sin(108/325.),6))+(-9)*(sin(216/325.))+(25)*(pow(sin(216/325.),2))
                     +(-9)*(pow(sin(216/325.),3))+(-50)*((pow(sin(108/325.),2))*(3+pow(sin(216/325.),2))))) ) - ( 0 );
  e3Integrand    = ( 0 ) - ( 0.9680022302377039 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], integrand[2][n], small_tol, close_tol );
  }


  // test the trace element integral of the functor

  int quadratureorder = 9;
  const Real small_tol_res = 1e-10; // Had to make this a little bigger for integral discrepancy
  const Real close_tol_res = 1e-10; // Had to make this a little bigger for integral discrepancy
  int nIntegrandL = varfldElem.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrandL);

  Real eps11Res;
  Real eps22Res;
  Real eps12Res;
  Real f1Res;
  Real f2Res;
  Real e3Res;
  ArrayQ residualTrue[3];
  ArrayQ residual[3] = {0, 0, 0};

  // Boundary trace integration for canonical element
  integral( fcn, xedge, residual, nIntegrandL );

  // Boundary trace residuals: (boundary flux) + (BC term)

  // Basis function 1
  eps11Res = ( 0 ) - ( 0. );
  eps22Res = ( 0 ) - ( 0. );
  eps12Res = ( 0 ) - ( 0. );
  f1Res    = ( 0 ) - ( 0 );
  f2Res    = ( 0 ) - ( 0 );
  e3Res    = ( 0 ) - ( 0. );
  residualTrue[0] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 2
  eps11Res = ( 0 ) - ( -0.4504588771415585 );
  eps22Res = ( 0 ) - ( 0.054875543808224674 );
  eps12Res = ( 0 ) - ( 0.29749561138225294 );
  f1Res    = ( (325/39304.)*((-150150)*(cos(14/25.))+(183878)*(cos(10/13.))+(177775)*(sin(14/25.))+(-142891)*(sin(10/13.))) ) - ( 0 );
  f2Res    = ( (325/314432.)*((-2249975)*(cos(14/25.))+(1818515)*(cos(10/13.))+(-1852175)*(sin(14/25.))+(2279011)*(sin(10/13.))) ) - ( 0 );
  e3Res    = ( 0 ) - ( 0.8578325447198129 );
  residualTrue[1] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  // Basis function 3
  eps11Res = ( 0 ) - ( 0.06683598249516343 );
  eps22Res = ( 0 ) - ( 0.2042473508381711 );
  eps12Res = ( 0 ) - ( 1.1415492430101368 );
  f1Res    = ( (325/39304.)*((129529)*(cos(14/25.))+(-166725)*(cos(10/13.))+(-192616)*(sin(14/25.))+(161200)*(sin(10/13.))) ) - ( 0 );
  f2Res    = ( (325/314432.)*((2427557)*(cos(14/25.))+(-2040025)*(cos(10/13.))+(1591361)*(sin(14/25.))+(-2062125)*(sin(10/13.))) ) - ( 0 );
  e3Res    = ( 0 ) - ( 1.863273001545678 );
  residualTrue[2] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( residualTrue[0][n], residual[0][n], small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[1][n], residual[1][n], small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[2][n], residual[2][n], small_tol_res, close_tol_res );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_X1Q1_sansLG_BCTypeForces_test )
{
  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCHSM2D<BCTypeForces, PDEClass> BCHSM2D;
  typedef BCNDConvertSpace<PhysD2, BCHSM2D> BCClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;

  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSM2D<VarTypeLambda>::VectorE<Real> VectorE;
  typedef PDEHSM2D<VarTypeLambda>::ComplianceMatrix<Real> ComplianceMatrix;

  typedef Real Tm;
  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX_Tq;

  // Parameter types
  typedef Element<ComplianceMatrix, TopoD2, Triangle> ElementAinvFieldCell;        // Ainv field
  typedef Element<Tm              , TopoD2, Triangle> ElementMassFieldCell;        // mu field
  typedef Element<VectorE         , TopoD2, Triangle> ElementFollowForceFieldCell; // qe field
  typedef Element<VectorX_Tq      , TopoD2, Triangle> ElementGlobForceFieldCell;   // qx field
  typedef Element<VectorX_Tq      , TopoD2, Triangle> ElementAccelFieldCell;       // a field

  typedef typename MakeTuple<ElementTuple, ElementAinvFieldCell,
      ElementMassFieldCell,
      ElementFollowForceFieldCell,
      ElementGlobForceFieldCell,
      ElementAccelFieldCell,
      ElementXFieldCell>::type ElementParam;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,ElementParam> BasisWeightedClass;

  // PDE
  VectorX g = { 0.023, -9.81};
  NDPDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  // BC
  Real f1BC = -1.1;
  Real f2BC = 2.43;
  BCClass bc(pde, f1BC, f2BC);

  BOOST_CHECK( bc.D == 2 );
  BOOST_CHECK( bc.N == 6 );
  BOOST_CHECK( bc.NBC == 2 );

  // grid

  int order = 1;
  ElementXFieldCell xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 3, xfldElem.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // Initialize parameters to 0 (not used in boundary trace calculations)

  // Extensional compliance matrix
  ElementAinvFieldCell AinvfldElem(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < xfldElem.nDOF(); i++)
    AinvfldElem.DOF(i) = 0;

  // Lumped shell mass
  ElementMassFieldCell mufldElem(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < xfldElem.nDOF(); i++)
    mufldElem.DOF(i) = 0;

  // Following force
  ElementFollowForceFieldCell qefldElem(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < xfldElem.nDOF(); i++)
    qefldElem.DOF(i) = 0;

  // Global forcing
  ElementGlobForceFieldCell qxfldElem(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < xfldElem.nDOF(); i++)
    qxfldElem.DOF(i) = 0;

  // Accelerations
  ElementAccelFieldCell afldElem(order, BasisFunctionCategory_Hierarchical);
  for (int i = 0; i < xfldElem.nDOF(); i++)
    afldElem.DOF(i) = 0;

  // adjacent triangles grid
  Real x1, x2, x3, y1, y2, y3;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;

  xfldElem.DOF(0) = {x1, y1};
  xfldElem.DOF(1) = {x2, y2};
  xfldElem.DOF(2) = {x3, y3};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell varfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, varfldElem.order() );
  BOOST_CHECK_EQUAL( 3, varfldElem.nDOF() );

  // triangle solution (left)

  // State vector solution at first cell corner {0,0}
  Real   rx_00 = -1;
  Real   ry_00 = -1;
  Real lam3_00 = 3./5.;
  Real  f11_00 = 3;
  Real  f22_00 = -2./5.;
  Real  f12_00 = 1./2.;

  // State vector solution at second cell corner {1,0}
  Real  rx_10 = 1;
  Real  ry_10 = -1./2.;
  Real lam3_10 = 5./13.;
  Real  f11_10 = 1;
  Real  f22_10 = -1./2.;
  Real  f12_10 = 2;

  // State vector solution at third cell corner {0,1}
  Real   rx_01 = -1./2.;
  Real   ry_01 = 2;
  Real lam3_01 = 7./25.;
  Real  f11_01 = -5;
  Real  f22_01 = 9;
  Real  f12_01 = 2;

  // Triangle solution
  varfldElem.DOF(0) = pde.setDOFFrom( PositionLambdaForces2D(rx_00, ry_00, lam3_00, f11_00, f22_00, f12_00) );
  varfldElem.DOF(1) = pde.setDOFFrom( PositionLambdaForces2D(rx_10, ry_10, lam3_10, f11_10, f22_10, f12_10) );
  varfldElem.DOF(2) = pde.setDOFFrom( PositionLambdaForces2D(rx_01, ry_01, lam3_01, f11_01, f22_01, f12_01) );


  // integrand functor

  StabilizationNitsche stab(order);
  IntegrandClass fcnint( pde, bc, {0}, stab );

  // Explicitly create a paramater instance.
  // If it was created in the argument below, the temporary variable in the
  // function argument would go out of scope and result in undefined behavior
  ElementParam params = ( AinvfldElem, mufldElem, qefldElem, qxfldElem, afldElem, xfldElem );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0, 1), params, varfldElem );

  BOOST_CHECK_EQUAL( 3, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;
  RefCoordTraceType sRef;
  Real eps11Integrand;
  Real eps22Integrand;
  Real eps12Integrand;
  Real f1Integrand;
  Real f2Integrand;
  Real e3Integrand;
  ArrayQ integrandTrue[3];
  ArrayQ integrand[3];

  // Displacement BC integrand test at s = 0
  sRef = 0;
  fcn( sRef, integrand, 3 );

  // Boundary integrands: (boundary flux) + (BC term)

  // Basis function 1
  eps11Integrand = ( 0 ) - ( 0 );
  eps22Integrand = ( 0 ) - ( 0 );
  eps12Integrand = ( 0 ) - ( 0 );
  f1Integrand    = ( 0 ) - ( 0. );
  f2Integrand    = ( 0 ) - ( 0. );
  e3Integrand    = ( 0 ) - ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) - ( 0 );
  eps22Integrand = ( 0 ) - ( 0 );
  eps12Integrand = ( 0 ) - ( 0 );
  f1Integrand    = ( (pow(2,-1/2.))*((3)*(cos(10/13.))+(-1)*(sin(10/13.))) ) - ( 2.132202690245283 );
  f2Integrand    = ( (1/2.)*((pow(2,-1/2.))*((3)*(cos(10/13.))+(5)*(sin(10/13.)))) ) - ( -0.43834472386960466 );
  e3Integrand    = ( 0 ) - ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) - ( 0 );
  eps22Integrand = ( 0 ) - ( 0 );
  eps12Integrand = ( 0 ) - ( 0 );
  f1Integrand    = ( 0 ) - ( 0. );
  f2Integrand    = ( 0 ) - ( 0. );
  e3Integrand    = ( 0 ) - ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], integrand[2][n], small_tol, close_tol );
  }


  // Displacement BC integrand test at s = 1
  sRef = 1;
  fcn( sRef, integrand, 3 );

  // Boundary integrands: (boundary flux) + (BC term)

  // Basis function 1
  eps11Integrand = ( 0 ) - ( 0 );
  eps22Integrand = ( 0 ) - ( 0 );
  eps12Integrand = ( 0 ) - ( 0 );
  f1Integrand    = ( 0 ) - ( 0. );
  f2Integrand    = ( 0 ) - ( 0. );
  e3Integrand    = ( 0 ) - ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) - ( 0 );
  eps22Integrand = ( 0 ) - ( 0 );
  eps12Integrand = ( 0 ) - ( 0 );
  f1Integrand    = ( 0 ) - ( 0. );
  f2Integrand    = ( 0 ) - ( 0. );
  e3Integrand    = ( 0 ) - ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) - ( 0 );
  eps22Integrand = ( 0 ) - ( 0 );
  eps12Integrand = ( 0 ) - ( 0 );
  f1Integrand    = ( (-1)*((pow(2,-1/2.))*((3)*(cos(14/25.))+(7)*(sin(14/25.)))) ) - ( -3.3265370415355364 );
  f2Integrand    = ( (pow(2,-1/2.))*((11)*(cos(14/25.))+(-7)*(sin(14/25.))) ) - ( 1.5308606399601308 );
  e3Integrand    = ( 0 ) - ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], integrand[2][n], small_tol, close_tol );
  }


  // Displacement BC integrand test at s = 1/2.
  sRef = 1/2.;
  fcn( sRef, integrand, 3 );

  // Boundary integrands: (boundary flux) + (BC term)

  // Basis function 1
  eps11Integrand = ( 0 ) - ( 0 );
  eps22Integrand = ( 0 ) - ( 0 );
  eps12Integrand = ( 0 ) - ( 0 );
  f1Integrand    = ( 0 ) - ( 0. );
  f2Integrand    = ( 0 ) - ( 0. );
  e3Integrand    = ( 0 ) - ( 0 );
  integrandTrue[0] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 2
  eps11Integrand = ( 0 ) - ( 0 );
  eps22Integrand = ( 0 ) - ( 0 );
  eps12Integrand = ( 0 ) - ( 0 );
  f1Integrand    = ( (-1)*((pow(2,1/2.))*((sin(216/325.))*(1+(-4)*(pow(sin(108/325.),2))
                     +(4)*(pow(sin(108/325.),4))+pow(sin(216/325.),2)))) )               - ( -0.3222253034515825 );
  f2Integrand    = ( (1/8.)*((pow(2,-1/2.))*(25+(72)*((cos(108/325.))*(pow(sin(108/325.),3)))
                     +(300)*(pow(sin(108/325.),4))+(-72)*((cos(108/325.))*(pow(sin(108/325.),5)))
                     +(-200)*(pow(sin(108/325.),6))+(-9)*(sin(216/325.))+(25)*(pow(sin(216/325.),2))
                     +(-9)*(pow(sin(216/325.),3))+(-50)*((pow(sin(108/325.),2))*(3+pow(sin(216/325.),2))))) ) - ( 0.033754428663976155 );
  e3Integrand    = ( 0 ) - ( 0 );
  integrandTrue[1] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  // Basis function 3
  eps11Integrand = ( 0 ) - ( 0 );
  eps22Integrand = ( 0 ) - ( 0 );
  eps12Integrand = ( 0 ) - ( 0 );
  f1Integrand    = ( (-1)*((pow(2,1/2.))*((sin(216/325.))*(1+(-4)*(pow(sin(108/325.),2))
                     +(4)*(pow(sin(108/325.),4))+pow(sin(216/325.),2)))) )               - ( -0.3222253034515825 );
  f2Integrand    = ( (1/8.)*((pow(2,-1/2.))*(25+(72)*((cos(108/325.))*(pow(sin(108/325.),3)))
                     +(300)*(pow(sin(108/325.),4))+(-72)*((cos(108/325.))*(pow(sin(108/325.),5)))
                     +(-200)*(pow(sin(108/325.),6))+(-9)*(sin(216/325.))+(25)*(pow(sin(216/325.),2))
                     +(-9)*(pow(sin(216/325.),3))+(-50)*((pow(sin(108/325.),2))*(3+pow(sin(216/325.),2))))) ) - ( 0.033754428663976155 );
  e3Integrand    = ( 0 ) - ( 0 );
  integrandTrue[2] = { eps11Integrand, eps22Integrand, eps12Integrand,
                       f1Integrand, f2Integrand, e3Integrand };

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[2][n], integrand[2][n], small_tol, close_tol );
  }


  // test the trace element integral of the functor

    int quadratureorder = 9;
    const Real small_tol_res = 5e-11; // Had to make this a little bigger for integral discrepancy
    const Real close_tol_res = 5e-11; // Had to make this a little bigger for integral discrepancy
    int nIntegrandL = varfldElem.nDOF();
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrandL);

    Real eps11Res;
    Real eps22Res;
    Real eps12Res;
    Real f1Res;
    Real f2Res;
    Real e3Res;
    ArrayQ residualTrue[3];
    ArrayQ residual[3] = {0, 0, 0};

    // Boundary trace integration for canonical element
    integral( fcn, xedge, residual, nIntegrandL );

    // Boundary trace residuals: (boundary flux) + (BC term)

    // Basis function 1
    eps11Res = ( 0 ) - ( 0 );
    eps22Res = ( 0 ) - ( 0 );
    eps12Res = ( 0 ) - ( 0 );
    f1Res    = ( 0 ) - ( 0. );
    f2Res    = ( 0 ) - ( 0. );
    e3Res    = ( 0 ) - ( 0 );
    residualTrue[0] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

    // Basis function 2
    eps11Res = ( 0 ) - ( 0 );
    eps22Res = ( 0 ) - ( 0 );
    eps12Res = ( 0 ) - ( 0 );
    f1Res    = ( (325/39304.)*((-150150)*(cos(14/25.))+(183878)*(cos(10/13.))
                 +(177775)*(sin(14/25.))+(-142891)*(sin(10/13.))) )              - ( 0.2002698501352971 );
    f2Res    = ( (325/314432.)*((-2249975)*(cos(14/25.))+(1818515)*(cos(10/13.))
                 +(-1852175)*(sin(14/25.))+(2279011)*(sin(10/13.))) )            - ( -0.07177767178982043 );
    e3Res    = ( 0 ) - ( 0 );
    residualTrue[1] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

    // Basis function 3
    eps11Res = ( 0 ) - ( 0 );
    eps22Res = ( 0 ) - ( 0 );
    eps12Res = ( 0 ) - ( 0 );
    f1Res    = ( (325/39304.)*((129529)*(cos(14/25.))+(-166725)*(cos(10/13.))
                 +(-192616)*(sin(14/25.))+(161200)*(sin(10/13.))) )              - ( -1.0893563783475355 );
    f2Res    = ( (325/314432.)*((2427557)*(cos(14/25.))+(-2040025)*(cos(10/13.))
                 +(1591361)*(sin(14/25.))+(-2062125)*(sin(10/13.))) )            - ( 0.3931012929154225 );
    e3Res    = ( 0 ) - ( 0 );
    residualTrue[2] = { eps11Res, eps22Res, eps12Res,f1Res, f2Res, e3Res };

    for (int n = 0; n < pde.N; n++)
    {
      SANS_CHECK_CLOSE( residualTrue[0][n], residual[0][n], small_tol_res, close_tol_res );
      SANS_CHECK_CLOSE( residualTrue[1][n], residual[1][n], small_tol_res, close_tol_res );
      SANS_CHECK_CLOSE( residualTrue[2][n], residual[2][n], small_tol_res, close_tol_res );
    }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
