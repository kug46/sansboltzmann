// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandInteriorTrace_Galerkin_Triangle_AD_btest
// testing of trace element residual integrands for Galerkin: Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "Field/Element/GalerkinWeightedIntegral.h" // Basis Weighted
#include "Field/Element/ElementIntegral.h" // Field Weighted

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_None> PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDE1DClassPlusTime;

typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldLine;
template class IntegrandInteriorTrace_Galerkin< PDE1DClassPlusTime >::
BasisWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementXFieldLine,ElementXFieldLine>;
template class IntegrandInteriorTrace_Galerkin< PDE1DClassPlusTime >::
FieldWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementXFieldLine,ElementXFieldLine>;

typedef PDEAdvectionDiffusion<PhysD2,
                              AdvectiveFlux2D_Uniform,
                              ViscousFlux2D_Uniform,
                              Source2D_None > PDEAdvectionDiffusion2D;
typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDE2DClassPlusTime;

typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldTri;
template class IntegrandInteriorTrace_Galerkin< PDE2DClassPlusTime >::
BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementXFieldTri,ElementXFieldTri>;

template class IntegrandInteriorTrace_Galerkin< PDE2DClassPlusTime >::
FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementXFieldTri,ElementXFieldTri>;

typedef ElementXField<PhysD2,TopoD2,Quad> ElementXFieldQuad;
template class IntegrandInteriorTrace_Galerkin< PDE2DClassPlusTime >::
BasisWeighted<Real,TopoD1,Line,TopoD2,Quad,Quad,ElementXFieldQuad,ElementXFieldQuad>;

typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_None> PDEAdvectionDiffusion3D;
typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDE3DClassPlusTime;
typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldTet;
template class IntegrandInteriorTrace_Galerkin< PDE3DClassPlusTime >::
BasisWeighted<Real,TopoD2,Triangle,TopoD3,Tet,Tet,ElementXFieldTet,ElementXFieldTet>;

typedef ElementXField<PhysD3,TopoD3,Hex> ElementXFieldHex;
template class IntegrandInteriorTrace_Galerkin< PDE3DClassPlusTime >::
BasisWeighted<Real,TopoD2,Quad,TopoD3,Hex,Hex,ElementXFieldHex,ElementXFieldHex>;
}

using namespace SANS;



//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandInteriorTrace_Galerkin_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Galerkin_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParam,ElementParam> BasisWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemR.nDOF() );

  // line solution (left)
  qfldElemL.DOF(0) = 2;
  qfldElemL.DOF(1) = 3;

  // line solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 4;

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                             xfldElemL, qfldElemL,
                                             xfldElemR, qfldElemR );

  BOOST_CHECK( fcnint.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandLTrue[2];
  Real integrandRTrue[2];
  ArrayQ integrandL[2];
  ArrayQ integrandR[2];

  sRef = 0;
  fcn( sRef, integrandL, 2, integrandR, 2 );

  // PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = ( 0)        + (0)           + (-2123./500.);
  integrandLTrue[1] = ( 33./10.)  + (2123./1000.) + ( 2123./500.);

  // PDE residuals (right)
  integrandRTrue[0] = (-33./10.)  + (-2123./1000.) + (-2123./500.);
  integrandRTrue[1] = ( 0)        + ( 0)           + ( 2123./500.);

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );


  // test the trace element integral of the functor

  int quadratureorder = 0;
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  GalerkinWeightedIntegral<TopoD0, Node, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[2] = {0,0};
  ArrayQ rsdPDEElemR[2] = {0,0};

  // cell integration for canonical element
  integral( fcn, xnode, rsdPDEElemL, nIntegrandL, rsdPDEElemR, nIntegrandR );

  // PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  Real rsd1L = ( 0)        + (0)           + (-2123./500.);
  Real rsd2L = ( 33./10.)  + (2123./1000.) + ( 2123./500.);

  // PDE residuals (right): (advective) + (viscous) + (dual-consistent)
  Real rsd1R = (-33./10.)  + (-2123./1000.) + (-2123./500.);
  Real rsd2R = ( 0)        + ( 0)           + ( 2123./500.);

  SANS_CHECK_CLOSE( rsd1L, rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2L, rsdPDEElemL[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1R, rsdPDEElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2R, rsdPDEElemR[1], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Galerkin_1D_Line_Line_Jacobian_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> MatrixQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParam,ElementParam> BasisWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3, b = 1.4;
  Source1D_UniformGrad source(a, b);

  PDEClass pde( adv, visc, source );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  for (int qorder = 1; qorder <= 4; qorder++)
  {
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
    }

    BasisWeightedClass fcn = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                               xfldElemL, qfldElemL,
                                               xfldElemR, qfldElemR );

    const Real small_tol = 1e-11;
    const Real close_tol = 5e-11;

    RefCoordTraceType sRefTrace = 0;

    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();

    DLA::VectorD<ArrayQ> integrand0L(nDOFL), integrand1L(nDOFL);
    DLA::VectorD<ArrayQ> integrand0R(nDOFR), integrand1R(nDOFR);

    JacobianElemInteriorTraceSize sizeL(nDOFL, nDOFL, nDOFR);
    JacobianElemInteriorTraceSize sizeR(nDOFR, nDOFL, nDOFR);

    JacobianElemInteriorTrace_Galerkin<MatrixQ> mtxElemLTrue(sizeL);
    JacobianElemInteriorTrace_Galerkin<MatrixQ> mtxElemRTrue(sizeR);

    JacobianElemInteriorTrace_Galerkin<MatrixQ> mtxElemL(sizeL);
    JacobianElemInteriorTrace_Galerkin<MatrixQ> mtxElemR(sizeR);

    // compute jacobians via finite differenec (exact for linear PDE)
    fcn(sRefTrace, &integrand0L[0], integrand0L.m(), &integrand0R[0], integrand0R.m());

    // wrt qL
    for (int i = 0; i < nDOFL; i++)
    {
      qfldElemL.DOF(i) += 1;
      fcn(sRefTrace, &integrand1L[0], integrand1L.m(), &integrand1R[0], integrand1R.m());
      qfldElemL.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxElemLTrue.PDE_qL(j,i) = integrand1L[j] - integrand0L[j];

      for (int j = 0; j < nDOFR; j++)
        mtxElemRTrue.PDE_qL(j,i) = integrand1R[j] - integrand0R[j];
    }

    // wrt qR
    for (int i = 0; i < nDOFR; i++)
    {
      qfldElemR.DOF(i) += 1;
      fcn(sRefTrace, &integrand1L[0], integrand1L.m(), &integrand1R[0], integrand1R.m());
      qfldElemR.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxElemLTrue.PDE_qR(j,i) = integrand1L[j] - integrand0L[j];

      for (int j = 0; j < nDOFR; j++)
        mtxElemRTrue.PDE_qR(j,i) = integrand1R[j] - integrand0R[j];
    }

    mtxElemL = 0;
    mtxElemR = 0;

    // accumulate the jacobian via Surreal
    fcn(1., sRefTrace, mtxElemL, mtxElemR);

    for (int i = 0; i < nDOFL; i++)
    {
      for (int j = 0; j < nDOFL; j++)
        SANS_CHECK_CLOSE( mtxElemLTrue.PDE_qL(i,j), mtxElemL.PDE_qL(i,j), small_tol, close_tol );

      for (int j = 0; j < nDOFR; j++)
        SANS_CHECK_CLOSE( mtxElemLTrue.PDE_qR(i,j), mtxElemL.PDE_qR(i,j), small_tol, close_tol );
    }

    for (int i = 0; i < nDOFR; i++)
    {
      for (int j = 0; j < nDOFL; j++)
        SANS_CHECK_CLOSE( mtxElemRTrue.PDE_qL(i,j), mtxElemR.PDE_qL(i,j), small_tol, close_tol );

      for (int j = 0; j < nDOFR; j++)
        SANS_CHECK_CLOSE( mtxElemRTrue.PDE_qR(i,j), mtxElemR.PDE_qR(i,j), small_tol, close_tol );
    }
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_WeightVary_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParam,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  Element<Real,TopoD1,Line> efldElemL( 0, BasisFunctionCategory_Legendre );
  Element<Real,TopoD1,Line> efldElemR( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 1; qorder <= 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qorder,   wfldElemL.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElemR.order() );
    BOOST_CHECK_EQUAL( qorder+1, wfldElemR.nDOF() );

    BOOST_REQUIRE_EQUAL( wfldElemL.nDOF(), qfldElemL.nDOF() );
    BOOST_REQUIRE_EQUAL( wfldElemR.nDOF(), qfldElemR.nDOF() );
    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
      wfldElemL.DOF(dof) = 0;
      wfldElemR.DOF(dof) = 0;
    }

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                                xfldElemL, qfldElemL,
                                                xfldElemR, qfldElemR );
    FieldWeightedClass fcnW = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                                xfldElemL, qfldElemL, wfldElemL, efldElemL,
                                                xfldElemR, qfldElemR, wfldElemR, efldElemR );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    RefCoordTraceType sRef;
    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandR = qfldElemR.nDOF();
    Real rsdElemWL[1]={0}, rsdElemWR[1]={0};
    std::vector<ArrayQ> rsdElemBL(nIntegrandL, 0);
    std::vector<ArrayQ> rsdElemBR(nIntegrandR, 0);

    int quadratureorder = 0;
    GalerkinWeightedIntegral<TopoD0, Node, ArrayQ, ArrayQ> integralB(quadratureorder, nIntegrandL, nIntegrandR);
    GalerkinWeightedIntegral<TopoD0, Node, Real, Real> integralW(quadratureorder, efldElemL.nDOF(), efldElemR.nDOF() );

    // cell integration for canonical element
    integralB( fcnB, xnode, rsdElemBL.data(), nIntegrandL, rsdElemBR.data(), nIntegrandR );
    for (int i = 0; i < wfldElemL.nDOF(); i++)
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1;
      wfldElemR.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL[0] = 0; rsdElemWR[0] = 0;
      integralW( fcnW, xnode, rsdElemWL, 1, rsdElemWR, 1 );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdElemWL[0], rsdElemBL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( rsdElemWR[0], rsdElemBR[i], small_tol, close_tol );

      // reset to zero
      wfldElemL.DOF(i) = 0;
      wfldElemR.DOF(i) = 0;
    }
  }

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_EstimateVary_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef Element<Real,TopoD1,Line> ElementEFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParam,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD0,Node,TopoD1,Line,Line,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  Element<ArrayQ,TopoD1,Line> wfldElemL( 0, BasisFunctionCategory_Legendre );
  Element<ArrayQ,TopoD1,Line> wfldElemR( 0, BasisFunctionCategory_Legendre );
  wfldElemL.DOF(0) = 1; wfldElemR.DOF(0) = 1;

  for (int qorder = 1; qorder <= 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementEFieldCell efldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementEFieldCell efldElemR(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( qorder+1, qfldElemR.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
    }

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                                xfldElemL, qfldElemL,
                                                xfldElemR, qfldElemR );
    FieldWeightedClass fcnW = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                                xfldElemL, qfldElemL, wfldElemL, efldElemL,
                                                xfldElemR, qfldElemR, wfldElemR, efldElemR );

    const Real small_tol = 1e-13;
    const Real close_tol = 1e-13;
    RefCoordTraceType sRef;
    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandR = qfldElemR.nDOF();
    std::vector<ArrayQ> rsdElemBL(nIntegrandL, 0);
    std::vector<ArrayQ> rsdElemBR(nIntegrandR, 0);
    std::vector<Real> rsdElemWL( efldElemL.nDOF(), 0 );
    std::vector<Real> rsdElemWR( efldElemR.nDOF(), 0 );

    int quadratureorder = 0;
    GalerkinWeightedIntegral<TopoD0, Node, ArrayQ, ArrayQ> integralB(quadratureorder, nIntegrandL, nIntegrandR);
    GalerkinWeightedIntegral<TopoD0, Node, Real, Real> integralW(quadratureorder, efldElemL.nDOF(), efldElemR.nDOF() );

    // cell integration for canonical element
    integralB( fcnB, xnode, rsdElemBL.data(), nIntegrandL, rsdElemBR.data(), nIntegrandR );

    integralW( fcnW, xnode, rsdElemWL.data(), rsdElemWL.size(),
                            rsdElemWR.data(), rsdElemWR.size() );

    for (int i = 0; i < efldElemL.nDOF(); i++)
      SANS_CHECK_CLOSE( rsdElemWL[i], rsdElemBL[i], small_tol, close_tol );

    for (int i = 0; i < efldElemR.nDOF(); i++)
      SANS_CHECK_CLOSE( rsdElemWR[i], rsdElemBR[i], small_tol, close_tol );

  }

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Galerkin_1D_Line_Line_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD1,TopoD0,Node> ElementXFieldTrace;
  typedef ElementXField<PhysD1,TopoD1,Line> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD1,Line> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<ArrayQ, TopoD0, Node, TopoD1, Line, Line,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, xfldElemR.nDOF() );

  ElementXFieldTrace xnode(0, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 0, xnode.order() );
  BOOST_CHECK_EQUAL( 1, xnode.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3;

  x1 = 0;
  x2 = 1;
  x3 = 2;

  xfldElemL.DOF(0) = x1;
  xfldElemL.DOF(1) = x2;

  xfldElemR.DOF(0) = x2;
  xfldElemR.DOF(1) = x3;

  xnode.DOF(0) = x2;
  xnode.normalSignL() = 1;

  // solution
  int qorder = 1;
  ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 2, qfldElemR.nDOF() );

  // line solution (left)
  qfldElemL.DOF(0) =  5;
  qfldElemL.DOF(1) = -4;

  // line solution (right)
  qfldElemR.DOF(0) = -4;
  qfldElemR.DOF(1) =  2;


  ElementQFieldCell wfldElemL(qorder+1,BasisFunctionCategory_Hierarchical);
  ElementQFieldCell wfldElemR(qorder+1,BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, wfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 2, wfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, wfldElemR.nDOF() );

  // line solution (left)
  wfldElemL.DOF(0) =  3;
  wfldElemL.DOF(1) =  4;
  wfldElemL.DOF(2) =  5;

  // line solution (right)
  wfldElemR.DOF(0) =  4;
  wfldElemR.DOF(1) = -4;
  wfldElemR.DOF(2) = -1;

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  FieldWeightedClass fcn = fcnint.integrand( xnode, CanonicalTraceToCell(0,0), CanonicalTraceToCell(1,0),
                                       xfldElemL, qfldElemL, wfldElemL,
                                       xfldElemR, qfldElemR, wfldElemR );

  BOOST_CHECK( fcnint.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandTrue, integrand;

  // Test at sRef={0}, s={1}
  sRef = {0.};
  fcn( sRef, integrand );

  //PDE residual integrands: (viscous)
  integrandTrue = ( -6369./50. );   // Single Valued

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // test the trace element integral of the functor

  int quadratureorder =0;
  ElementIntegral<TopoD0, Node, ArrayQ> integral(quadratureorder);

  Real rsdTrue=0,rsd=0;

  // Trace integration for canonical element
  integral( fcn, xnode, rsd );

  //PDE residual: (viscous)
  rsdTrue = ( -6369./50. );   // Basis function

  SANS_CHECK_CLOSE( rsdTrue, rsd, small_tol, close_tol );


}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Galerkin_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 4;

  // triangle solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 2;
  qfldElemR.DOF(2) = 9;

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                             xfldElemL, qfldElemL,
                                             xfldElemR, qfldElemR );

  BOOST_CHECK( fcnint.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-10;
  RefCoordTraceType sRef;
  Real integrandLTrue[3];
  Real integrandRTrue[3];
  ArrayQ integrandL[3];
  ArrayQ integrandR[3];

  sRef = 0;
  fcn( sRef, integrandL, 3, integrandR,3 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = ( 0 ) + ( 0 ) + ( -12483/(1000.*sqrt(2)) );  // Basis function 1
  integrandLTrue[1] = ( 39/(10.*sqrt(2)) ) + ( -19767/(2000.*sqrt(2)) ) + ( 2007/(250.*sqrt(2)) );  // Basis function 2
  integrandLTrue[2] = ( 0 ) + ( 0 ) + ( 891/(200.*sqrt(2)) );  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent)
  integrandRTrue[0] = ( 0 ) + ( 0 ) + ( 12483/(1000.*sqrt(2)) );  // Basis function 1
  integrandRTrue[1] = ( 0 ) + ( 0 ) + ( -2007/(250.*sqrt(2)) );  // Basis function 2
  integrandRTrue[2] = ( -39/(10.*sqrt(2)) ) + ( 19767/(2000.*sqrt(2)) ) + ( -891/(200.*sqrt(2)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[2], integrandL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[2], integrandR[2], small_tol, close_tol );


  sRef = 1;
  fcn( sRef, integrandL, 3, integrandR,3 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = ( 0 ) + ( 0 ) + ( 4161/(1000.*sqrt(2)) );  // Basis function 1
  integrandLTrue[1] = ( 0 ) + ( 0 ) + ( -669/(250.*sqrt(2)) );  // Basis function 2
  integrandLTrue[2] = ( (13*sqrt(2))/5. ) + ( -19767/(2000.*sqrt(2)) ) + ( -297/(200.*sqrt(2)) );  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent)
  integrandRTrue[0] = ( 0 ) + ( 0 ) + ( -4161/(1000.*sqrt(2)) );  // Basis function 1
  integrandRTrue[1] = ( (-13*sqrt(2))/5. ) + ( 19767/(2000.*sqrt(2)) ) + ( 669/(250.*sqrt(2)) );  // Basis function 2
  integrandRTrue[2] = ( 0 ) + ( 0 ) + ( 297/(200.*sqrt(2)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[2], integrandL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[2], integrandR[2], small_tol, close_tol );


  sRef = 0.5;
  fcn( sRef, integrandL, 3, integrandR,3 );

  //PDE residual integrands (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = ( 0 ) + ( 0 ) + ( -4161/(1000.*sqrt(2)) );  // Basis function 1
  integrandLTrue[1] = ( 91/(40.*sqrt(2)) ) + ( -19767/(4000.*sqrt(2)) ) + ( 669/(250.*sqrt(2)) );  // Basis function 2
  integrandLTrue[2] = ( 91/(40.*sqrt(2)) ) + ( -19767/(4000.*sqrt(2)) ) + ( 297/(200.*sqrt(2)) );  // Basis function 3

  //PDE residual integrands (right): (advective) + (viscous) + (dual-consistent)
  integrandRTrue[0] = ( 0 ) + ( 0 ) + ( 4161/(1000.*sqrt(2)) );  // Basis function 1
  integrandRTrue[1] = ( -91/(40.*sqrt(2)) ) + ( 19767/(4000.*sqrt(2)) ) + ( -669/(250.*sqrt(2)) );  // Basis function 2
  integrandRTrue[2] = ( -91/(40.*sqrt(2)) ) + ( 19767/(4000.*sqrt(2)) ) + ( -297/(200.*sqrt(2)) );  // Basis function 3

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[2], integrandL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[2], integrandR[2], small_tol, close_tol );


  // test the trace element integral of the functor

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureorder = 2;

  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

  ArrayQ rsdPDEElemL[3] = {0,0,0};
  ArrayQ rsdPDEElemR[3] = {0,0,0};
  Real rsdLTrue[3];
  Real rsdRTrue[3];

  // cell integration for canonical element
  integral( fcn, xedge, rsdPDEElemL, nIntegrandL, rsdPDEElemR, nIntegrandR );

  //PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  rsdLTrue[0] = ( 0 ) + ( 0 ) + ( -4161./1000. ) ;   // Basis function 1
  rsdLTrue[1] = ( 13./6. ) + ( -19767./4000. ) + ( 669./250. ) ;   // Basis function 2
  rsdLTrue[2] = ( 143./60. ) + ( -19767./4000. ) + ( 297./200. ) ;   // Basis function 3

  //PDE residuals (right): (advective) + (viscous) + (dual-consistent)
  rsdRTrue[0] = ( 0 ) + ( 0 ) + ( 4161./1000. ) ;   // Basis function 1
  rsdRTrue[1] = ( -143./60. ) + ( 19767./4000. ) + ( -669./250. ) ;   // Basis function 2
  rsdRTrue[2] = ( -13./6. ) + ( 19767./4000. ) + ( -297./200. ) ;   // Basis function 3

  SANS_CHECK_CLOSE( rsdLTrue[0], rsdPDEElemL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLTrue[1], rsdPDEElemL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdLTrue[2], rsdPDEElemL[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsdRTrue[0], rsdPDEElemR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdRTrue[1], rsdPDEElemR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsdRTrue[2], rsdPDEElemR[2], small_tol, close_tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( BasisWeighted_Galerkin_2D_Triangle_Triangle_Jacobian_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> MatrixQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 0.4, b = 0.7, c = -0.2;
  Source2D_UniformGrad source(a, b, c);

  PDEClass pde( adv, visc, source );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  for (int qorder = 1; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemR.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
    }

    BasisWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                               xfldElemL, qfldElemL,
                                               xfldElemR, qfldElemR );

    const Real small_tol = 1e-12;
    const Real close_tol = 1e-11;

    RefCoordTraceType sRefTrace = Line::centerRef;

    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();

    DLA::VectorD<ArrayQ> integrand0L(nDOFL), integrand1L(nDOFL);
    DLA::VectorD<ArrayQ> integrand0R(nDOFR), integrand1R(nDOFR);

    JacobianElemInteriorTraceSize sizeL(nDOFL, nDOFL, nDOFR);
    JacobianElemInteriorTraceSize sizeR(nDOFR, nDOFL, nDOFR);

    JacobianElemInteriorTrace_Galerkin<MatrixQ> mtxElemLTrue(sizeL);
    JacobianElemInteriorTrace_Galerkin<MatrixQ> mtxElemRTrue(sizeR);

    JacobianElemInteriorTrace_Galerkin<MatrixQ> mtxElemL(sizeL);
    JacobianElemInteriorTrace_Galerkin<MatrixQ> mtxElemR(sizeR);

    // compute jacobians via finite differenec (exact for linear PDE)
    fcn(sRefTrace, &integrand0L[0], integrand0L.m(), &integrand0R[0], integrand0R.m());

    // wrt qL
    for (int i = 0; i < nDOFL; i++)
    {
      qfldElemL.DOF(i) += 1;
      fcn(sRefTrace, &integrand1L[0], integrand1L.m(), &integrand1R[0], integrand1R.m());
      qfldElemL.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxElemLTrue.PDE_qL(j,i) = integrand1L[j] - integrand0L[j];

      for (int j = 0; j < nDOFR; j++)
        mtxElemRTrue.PDE_qL(j,i) = integrand1R[j] - integrand0R[j];
    }

    // wrt qR
    for (int i = 0; i < nDOFR; i++)
    {
      qfldElemR.DOF(i) += 1;
      fcn(sRefTrace, &integrand1L[0], integrand1L.m(), &integrand1R[0], integrand1R.m());
      qfldElemR.DOF(i) -= 1;

      for (int j = 0; j < nDOFL; j++)
        mtxElemLTrue.PDE_qR(j,i) = integrand1L[j] - integrand0L[j];

      for (int j = 0; j < nDOFR; j++)
        mtxElemRTrue.PDE_qR(j,i) = integrand1R[j] - integrand0R[j];
    }

    mtxElemL = 0;
    mtxElemR = 0;

    // accumulate the jacobian via Surreal
    fcn(1., sRefTrace, mtxElemL, mtxElemR);

    for (int i = 0; i < nDOFL; i++)
    {
      for (int j = 0; j < nDOFL; j++)
        SANS_CHECK_CLOSE( mtxElemLTrue.PDE_qL(i,j), mtxElemL.PDE_qL(i,j), small_tol, close_tol );

      for (int j = 0; j < nDOFR; j++)
        SANS_CHECK_CLOSE( mtxElemLTrue.PDE_qR(i,j), mtxElemL.PDE_qR(i,j), small_tol, close_tol );
    }

    for (int i = 0; i < nDOFR; i++)
    {
      for (int j = 0; j < nDOFL; j++)
        SANS_CHECK_CLOSE( mtxElemRTrue.PDE_qL(i,j), mtxElemR.PDE_qL(i,j), small_tol, close_tol );

      for (int j = 0; j < nDOFR; j++)
        SANS_CHECK_CLOSE( mtxElemRTrue.PDE_qR(i,j), mtxElemR.PDE_qR(i,j), small_tol, close_tol );
    }
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldWeighted_Galerkin_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::FieldWeighted<ArrayQ, TopoD1, Line,
                                          TopoD2, Triangle, Triangle, ElementParam, ElementParam> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.321;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // static tests
  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  // solution

  int qorder = 1;
  ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, qfldElemR.nDOF() );

  // triangle solution (left)
  qfldElemL.DOF(0) =  1;
  qfldElemL.DOF(1) =  3;
  qfldElemL.DOF(2) =  4;

  // triangle solution (right)
  qfldElemR.DOF(0) = -4;
  qfldElemR.DOF(1) =  4;
  qfldElemR.DOF(2) =  3;

  ElementQFieldCell wfldElemL(qorder+1, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell wfldElemR(qorder+1, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, wfldElemL.order() );
  BOOST_CHECK_EQUAL( 6, wfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 2, wfldElemR.order() );
  BOOST_CHECK_EQUAL( 6, wfldElemR.nDOF() );

  // triangle solution (left)
  wfldElemL.DOF(0) = -2;
  wfldElemL.DOF(1) =  4;
  wfldElemL.DOF(2) =  3;
  wfldElemL.DOF(3) =  2;
  wfldElemL.DOF(4) =  4;
  wfldElemL.DOF(5) = -1;

  // triangle solution (right)
  wfldElemR.DOF(0) =  4;
  wfldElemR.DOF(1) =  3;
  wfldElemR.DOF(2) =  4;
  wfldElemR.DOF(3) =  2;
  wfldElemR.DOF(4) =  3;
  wfldElemR.DOF(5) = -2;

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  FieldWeightedClass fcn = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                       xfldElemL, qfldElemL, wfldElemL,
                                       xfldElemR, qfldElemR, wfldElemR );

  BOOST_CHECK( fcnint.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 3, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandTrue, integrand;

  // Test at sRef={0}, {s,t}={1, 0}
  sRef = {0.};
  fcn( sRef, integrand );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 91.*sqrt(2.) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // Test at sRef={1/2}, {s,t}={1/2, 1/2}
  sRef = {1./2.};
  fcn( sRef, integrand );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 1001./(4.*sqrt(2.)) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // Test at sRef={1}, {s,t}={0, 1}
  sRef = {1.};
  fcn( sRef, integrand );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 273./(2.*sqrt(2.)) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // Test at sRef={1/5}, {s,t}={4/5, 1/5}
  sRef = {1./5.};
  fcn( sRef, integrand );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 11557./(50.*sqrt(2.)) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // Test at sRef={1/8}, {s,t}={7/8, 1/8}
  sRef = {1./8.};
  fcn( sRef, integrand );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 1729./(8.*sqrt(2.)) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );

  // Test at sRef={6/7}, {s,t}={1/7, 6/7}
  sRef = {6./7.};
  fcn( sRef, integrand );

  //PDE residual integrands: (viscous)
  integrandTrue = ( 1313./(7.*sqrt(2.)) );   // Single Value

  SANS_CHECK_CLOSE( integrandTrue, integrand, small_tol, close_tol );




  // test the trace element integral of the functor

  int quadratureorder =3;
  ElementIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder);

  Real rsdTrue=0,rsd=0;

  // Trace integration for canonical element
  integral( fcn, xedge, rsd );

  //PDE residual: (viscous)
  rsdTrue = ( 2639./12. );   // Basis function

  SANS_CHECK_CLOSE( rsdTrue, rsd, small_tol, close_tol );

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_WeightVary_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 3.1;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  Element<Real,TopoD2,Triangle> efldElemL( 0, BasisFunctionCategory_Legendre );
  Element<Real,TopoD2,Triangle> efldElemR( 0, BasisFunctionCategory_Legendre );

  for (int qorder = 1; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell wfldElemR(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemR.nDOF() );

    BOOST_CHECK_EQUAL( qorder,   wfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   wfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, wfldElemR.nDOF() );

    BOOST_REQUIRE_EQUAL( wfldElemL.nDOF(), qfldElemL.nDOF() );
    BOOST_REQUIRE_EQUAL( wfldElemR.nDOF(), qfldElemR.nDOF() );
    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
      wfldElemL.DOF(dof) = 0;
      wfldElemR.DOF(dof) = 0;
    }

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                                xfldElemL, qfldElemL,
                                                xfldElemR, qfldElemR );
    FieldWeightedClass fcnW = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                                xfldElemL, qfldElemL, wfldElemL, efldElemL,
                                                xfldElemR, qfldElemR, wfldElemR, efldElemR );

    const Real small_tol = 2e-13;
    const Real close_tol = 2e-13;
    RefCoordTraceType sRef;
    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandR = qfldElemR.nDOF();
    Real rsdElemWL[1]={0}, rsdElemWR[1]={0};
    std::vector<ArrayQ> rsdElemBL(nIntegrandL, 0);
    std::vector<ArrayQ> rsdElemBR(nIntegrandR, 0);

    int quadratureorder = qorder + 1;
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralB(quadratureorder, nIntegrandL, nIntegrandR);
    GalerkinWeightedIntegral<TopoD1, Line, Real, Real> integralW(quadratureorder, efldElemL.nDOF(), efldElemR.nDOF() );

    // cell integration for canonical element
    integralB( fcnB, xedge, rsdElemBL.data(), nIntegrandL, rsdElemBR.data(), nIntegrandR );
    for (int i = 0; i < wfldElemL.nDOF(); i++)
    {
      // set just one of the weights to one
      wfldElemL.DOF(i) = 1;
      wfldElemR.DOF(i) = 1;

      // cell integration for canonical element
      rsdElemWL[0] = 0; rsdElemWR[0] = 0;
      integralW( fcnW, xedge, rsdElemWL, 1, rsdElemWR, 1 );

      // test the the two integrands are identical
      SANS_CHECK_CLOSE( rsdElemWL[0], rsdElemBL[i], small_tol, close_tol );
      SANS_CHECK_CLOSE( rsdElemWR[0], rsdElemBR[i], small_tol, close_tol );

      // reset to zero
      wfldElemL.DOF(i) = 0;
      wfldElemR.DOF(i) = 0;
    }
  }

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Basis_Field_Weight_Equal_EstimateVary_2D_Triangle_Triangle_test )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD2,TopoD1,Line> ElementXFieldTrace;
  typedef ElementXField<PhysD2,TopoD2,Triangle> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD2,Triangle> ElementQFieldCell;
  typedef Element<Real,TopoD2,Triangle> ElementEFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef ElementXFieldCell ElementParam;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> BasisWeightedClass;
  typedef IntegrandClass::FieldWeighted<Real,TopoD1,Line,TopoD2,Triangle,Triangle,ElementParam,ElementParam> FieldWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyx = 0.478;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Real a = 3.1;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid
  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 3, xfldElemR.nDOF() );

  ElementXFieldTrace xedge(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xedge.order() );
  BOOST_CHECK_EQUAL( 2, xedge.nDOF() );

  // adjacent triangles grid
  Real x1, x2, x3, x4, y1, y2, y3, y4;

  x1 = 0;  y1 = 0;
  x2 = 1;  y2 = 0;
  x3 = 0;  y3 = 1;
  x4 = 1;  y4 = 1;

  xfldElemL.DOF(0) = {x1, y1};
  xfldElemL.DOF(1) = {x2, y2};
  xfldElemL.DOF(2) = {x3, y3};

  xfldElemR.DOF(0) = {x4, y4};
  xfldElemR.DOF(1) = {x3, y3};
  xfldElemR.DOF(2) = {x2, y2};

  xedge.DOF(0) = {x2, y2};
  xedge.DOF(1) = {x3, y3};

  ElementQFieldCell wfldElemL( 0, BasisFunctionCategory_Legendre );
  ElementQFieldCell wfldElemR( 0, BasisFunctionCategory_Legendre );
  wfldElemL.DOF(0) = 1; wfldElemR.DOF(0) = 1;

  for (int qorder = 1; qorder < 4; qorder++) // making sure the cubic basis works too
  {
    ElementQFieldCell qfldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementQFieldCell qfldElemR(qorder, BasisFunctionCategory_Hierarchical);
    ElementEFieldCell efldElemL(qorder, BasisFunctionCategory_Hierarchical);
    ElementEFieldCell efldElemR(qorder, BasisFunctionCategory_Hierarchical);

    BOOST_CHECK_EQUAL( qorder,   qfldElemL.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemL.nDOF() );
    BOOST_CHECK_EQUAL( qorder,   qfldElemR.order() );
    BOOST_CHECK_EQUAL( (qorder+1)*(qorder+2)/2, qfldElemR.nDOF() );

    // line solution
    for (int dof = 0; dof < qfldElemL.nDOF();dof++)
    {
      qfldElemL.DOF(dof) = (dof+1)*pow(-1,dof);
      qfldElemR.DOF(dof) = (dof+2)*pow(-1,dof+1);
    }

    IntegrandClass fcnint( pde, {0} );

    BasisWeightedClass fcnB = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                                xfldElemL, qfldElemL,
                                                xfldElemR, qfldElemR );
    FieldWeightedClass fcnW = fcnint.integrand( xedge, CanonicalTraceToCell(0,1), CanonicalTraceToCell(0,-1),
                                                xfldElemL, qfldElemL, wfldElemL, efldElemL,
                                                xfldElemR, qfldElemR, wfldElemR, efldElemR );

    const Real small_tol = 2e-13;
    const Real close_tol = 2e-13;
    RefCoordTraceType sRef;
    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandR = qfldElemR.nDOF();
    std::vector<ArrayQ> rsdElemBL(nIntegrandL, 0);
    std::vector<ArrayQ> rsdElemBR(nIntegrandR, 0);
    std::vector<Real> rsdElemWL(efldElemL.nDOF(), 0);
    std::vector<Real> rsdElemWR(efldElemR.nDOF(), 0);

    int quadratureorder = qorder + 1;
    GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ> integralB(quadratureorder, nIntegrandL, nIntegrandR);
    GalerkinWeightedIntegral<TopoD1, Line, Real, Real> integralW(quadratureorder, efldElemL.nDOF(), efldElemR.nDOF() );

    // cell integration for canonical element
    integralB( fcnB, xedge, rsdElemBL.data(), nIntegrandL, rsdElemBR.data(), nIntegrandR );

    integralW( fcnW, xedge, rsdElemWL.data(), rsdElemWL.size(),
                            rsdElemWR.data(), rsdElemWR.size() );

    for (int i = 0; i < efldElemL.nDOF(); i++)
      SANS_CHECK_CLOSE( rsdElemWL[i], rsdElemBL[i], small_tol, close_tol );
    for (int i = 0; i < efldElemR.nDOF(); i++)
      SANS_CHECK_CLOSE( rsdElemWR[i], rsdElemBR[i], small_tol, close_tol );

  }

}
#endif


//TODO: This needs to be implemented to make sure 3D Trace integrands are correct

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IntegrandTrace_Galerkin_3D_Tet_Tet_test )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform> PDEClass;
  typedef PDEAdvectionDiffusion3D::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysD3,TopoD2,Triangle> ElementXFieldTrace;
  typedef ElementXField<PhysD3,TopoD3,Tet> ElementXFieldCell;
  typedef Element<ArrayQ,TopoD3,Tet> ElementQFieldCell;
  typedef ElementXFieldTrace::RefCoordType RefCoordTraceType;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;
  typedef IntegrandClass::FunctoBasisWeightedTopoD2,Triangle,TopoD3,Tet,Tet> BasisWeightedClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                                  kxy, kyy, kyz,
                                  kxz, kyz, kzz );


//  PDEClass pde(adv, visc);
  SolutionFunction3D solnExact;

  PDEClass pde( adv, visc, solnExact );

  // static tests
  BOOST_CHECK( pde.D == 3 );
  BOOST_CHECK( pde.N == 1 );

  // flux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == true );
  BOOST_CHECK( pde.hasSource() == false );
  BOOST_CHECK( pde.hasForcingFunction() == false );

  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );

  // grid

  int order = 1;
  ElementXFieldCell xfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElemL.order() );
  BOOST_CHECK_EQUAL( 4, xfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, xfldElemR.order() );
  BOOST_CHECK_EQUAL( 4, xfldElemR.nDOF() );

  ElementXFieldTrace xtri(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xtri.order() );
  BOOST_CHECK_EQUAL( 3, xtri.nDOF() );

  /*
               2
              /|\
             / | \
            /  |  \
           /   |   \
          /    |    \
         / (R) | (L) \
        /      |      \
       4 ----- 0 ----- 1
        .     /     .
         .   /   .
          . / .
           3
  */

  // adjacent tets grid
  Real x1, x2, x3, x4, x5, y1, y2, y3, y4, y5, z1, z2, z3, z4, z5;

  x1 =  0;  y1 = 0;  z1 = 0;
  x2 =  1;  y2 = 0;  z2 = 0;
  x3 =  0;  y3 = 1;  z3 = 0;
  x4 =  0;  y4 = 0;  z4 = 1;
  x5 = -1;  y5 = 0;  z5 = 0;

  xfldElemL.DOF(0) = {x1, y1, z1};
  xfldElemL.DOF(1) = {x2, y2, z2};
  xfldElemL.DOF(2) = {x3, y3, z3};
  xfldElemL.DOF(3) = {x4, y4, z4};

  xfldElemR.DOF(0) = {x5, y5, z5};
  xfldElemR.DOF(1) = {x1, y1, z1};
  xfldElemR.DOF(2) = {x3, y3, z1};
  xfldElemR.DOF(3) = {x4, y4, z4};

  xtri.DOF(0) = {x1, y1, z1};
  xtri.DOF(1) = {x4, y4, z4};
  xtri.DOF(2) = {x3, y3, z3};

  // solution

  order = 1;
  ElementQFieldCell qfldElemL(order, BasisFunctionCategory_Hierarchical);
  ElementQFieldCell qfldElemR(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, qfldElemL.order() );
  BOOST_CHECK_EQUAL( 4, qfldElemL.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldElemR.order() );
  BOOST_CHECK_EQUAL( 4, qfldElemR.nDOF() );

  // tet solution (left)
  qfldElemL.DOF(0) = 1;
  qfldElemL.DOF(1) = 3;
  qfldElemL.DOF(2) = 4;
  qfldElemL.DOF(3) = 6;

  // tet solution (right)
  qfldElemR.DOF(0) = 7;
  qfldElemR.DOF(1) = 2;
  qfldElemR.DOF(2) = 9;
  qfldElemR.DOF(3) = 8;

  // integrand functor
  IntegrandClass fcnint( pde, {0} );

  BasisWeightedClass fcn = fcnint.integrand( xtri, CanonicalTraceToCell(1,1), CanonicalTraceToCell(0,-1),
                                             xfldElemL, qfldElemL,
                                             xfldElemR, qfldElemR );

  BOOST_CHECK( fcnint.N == 1);
  BOOST_CHECK_EQUAL( 1, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOFLeft() );
  BOOST_CHECK_EQUAL( 4, fcn.nDOFRight() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;
  RefCoordTraceType sRef;
  Real integrandLTrue[4];
  Real integrandRTrue[4];
  ArrayQ integrandL[4];
  ArrayQ integrandR[4];

  sRef = {0, 0};
  fcn( sRef, integrandL, 4, integrandR, 4 );

  // PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = (0)             + (0)                    + (-3177./(250*sqrt(2)));
  integrandLTrue[1] = (9*sqrt(2)/5.)  + (-5073./(500*sqrt(2))) + ( 2007./(250*sqrt(2)));
  integrandLTrue[2] = (0)             + (0)                    + ( 117/(25*sqrt(2)));
  integrandLTrue[3] = (0)             + (0)                    + ( 117/(25*sqrt(2)));

  // PDE residuals (right)
  integrandRTrue[0] = (0)             + (0)                   + ( 3177./(250*sqrt(2)));
  integrandRTrue[1] = (0)             + (0)                   + (-2007./(250*sqrt(2)));
  integrandRTrue[2] = (-9*sqrt(2)/5.) + (5073./(500*sqrt(2))) + (-117/(25*sqrt(2)));
  integrandRTrue[3] = (-9*sqrt(2)/5.) + (5073./(500*sqrt(2))) + (-117/(25*sqrt(2)));

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[2], integrandL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[3], integrandL[3], small_tol, close_tol );

  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[2], integrandR[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[3], integrandR[3], small_tol, close_tol );

#if 0
  sRef = {1, 0};
  fcn( sRef, integrandL, 4, integrandR, 4 );

  // PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = (0)             + (0)                    + ( 1059./(250*sqrt(2)));
  integrandLTrue[1] = (0)             + (0)                    + ( -669./(250*sqrt(2)));
  integrandLTrue[2] = (12*sqrt(2)/5.) + (-5073./(500*sqrt(2))) + ( -39/(25*sqrt(2)));

  // PDE residuals (right)
  integrandRTrue[0] = (0)              + (0)                   + (-1059./(250*sqrt(2)));
  integrandRTrue[1] = (-12*sqrt(2)/5.) + (5073./(500*sqrt(2))) + (  669./(250*sqrt(2)));
  integrandRTrue[2] = (0)              + (0)                   + (  39/(25*sqrt(2)));

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[2], integrandL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[2], integrandR[2], small_tol, close_tol );


  sRef = {1./3., 1./3.};
  fcn( sRef, integrandL, 4, integrandR, 4 );

  // PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  integrandLTrue[0] = (0)                + (0)                     + (-1059./(250*sqrt(2)));
  integrandLTrue[1] = (21./(10*sqrt(2))) + (-5073./(1000*sqrt(2))) + (  669./(250*sqrt(2)));
  integrandLTrue[2] = (21./(10*sqrt(2))) + (-5073./(1000*sqrt(2))) + (  39/(25*sqrt(2)));

  // PDE residuals (right)
  integrandRTrue[0] = (0)                 + (0)                    + ( 1059./(250*sqrt(2)));
  integrandRTrue[1] = (-21./(10*sqrt(2))) + (5073./(1000*sqrt(2))) + ( -669./(250*sqrt(2)));
  integrandRTrue[2] = (-21./(10*sqrt(2))) + (5073./(1000*sqrt(2))) + ( -39/(25*sqrt(2)));

  SANS_CHECK_CLOSE( integrandLTrue[0], integrandL[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[1], integrandL[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandLTrue[2], integrandL[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[0], integrandR[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[1], integrandR[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( integrandRTrue[2], integrandR[2], small_tol, close_tol );
#endif
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
