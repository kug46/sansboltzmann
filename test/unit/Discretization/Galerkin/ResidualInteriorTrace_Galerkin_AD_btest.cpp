// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ResidualInteriorTrace_Triangle_AD_btest
// testing of interior trace residual functions with Advection-Diffusion

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualInteriorTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualInteriorTrace_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( rsdPDE1D_1Line_X1Q1_1Group )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;

  Real u = 1.1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_None source;

  PDEClass pde( adv, visc, source );

  // grid: P1 (aka X1)
  XField1D_2Line_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qfld.nDOF() == 4 );

  // triangle solution data (left)
  qfld.DOF(0) = 2;
  qfld.DOF(1) = 3;

  // triangle solution data (right)
  qfld.DOF(2) = 7;
  qfld.DOF(3) = 4;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureOrder = 0;

  // PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  Real rsd1L = ( 0)        + (0)           + (-2123./500.);
  Real rsd2L = ( 33./10.)  + (2123./1000.) + ( 2123./500.);

  // PDE residuals (right): (advective) + (viscous) + (dual-consistent)
  Real rsd1R = (-33./10.)  + (-2123./1000.) + (-2123./500.);
  Real rsd2R = ( 0)        + ( 0)           + ( 2123./500.);

  // PDE global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(4);

  // general interface
  rsdPDEGlobal = 0;

  IntegrateInteriorTraceGroups<TopoD1>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdPDEGlobal),
                                                   xfld, qfld, &quadratureOrder, 1);

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsd1L, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2L, rsdPDEGlobal[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1R, rsdPDEGlobal[2], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2R, rsdPDEGlobal[3], small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( rsdPDE2D_2Triangle_X1Q1_1Group )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef IntegrandInteriorTrace_Galerkin<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_None source;

  PDEClass pde( adv, visc, source );

  // grid: P1 (aka X1)
  XField2D_2Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1)
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qfld.nDOF() == 6 );

  // triangle solution data (left)
  qfld.DOF(0) = 1;
  qfld.DOF(1) = 3;
  qfld.DOF(2) = 4;

  // triangle solution data (right)
  qfld.DOF(3) = 7;
  qfld.DOF(4) = 2;
  qfld.DOF(5) = 9;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  // quadrature rule: quadratic (basis & flux both linear)
  int quadratureOrder = 2;

  // PDE residuals (left): (advective) + (viscous) + (dual-consistent)
  Real rsd1L = (0)      + (0)            + (-1059./250.);
  Real rsd2L = (2)      + (-5073./1000.) + ( 669./250.);
  Real rsd3L = (11./5.) + (-5073./1000.) + (  39./25.);

  // PDE residuals (right): (advective) + (viscous) + (dual-consistent)
  Real rsd1R = (0)       + (0)           + (1059./250.);
  Real rsd2R = (-11./5.) + (5073./1000.) + (-669./250.);
  Real rsd3R = (-2)      + (5073./1000.) + ( -39./25.);

  // PDE global residuals
  SLA::SparseVector<ArrayQ> rsdPDEGlobal(6);

  // general interface
  rsdPDEGlobal = 0;

  IntegrateInteriorTraceGroups<TopoD2>::integrate( ResidualInteriorTrace_Galerkin(fcnint, rsdPDEGlobal),
                                                   xfld, qfld, &quadratureOrder, 1);

  const Real small_tol = 1e-10;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( rsd1L, rsdPDEGlobal[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2L, rsdPDEGlobal[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3L, rsdPDEGlobal[2], small_tol, close_tol );

  SANS_CHECK_CLOSE( rsd1R, rsdPDEGlobal[3], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd2R, rsdPDEGlobal[4], small_tol, close_tol );
  SANS_CHECK_CLOSE( rsd3R, rsdPDEGlobal[5], small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
