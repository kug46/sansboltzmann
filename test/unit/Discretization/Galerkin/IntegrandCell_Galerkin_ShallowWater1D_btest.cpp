// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// IntegrandCell_Galerkin_ShallowWater1D_btest
// testing of cell element residual integrands for Galerkin: ShallowWater1D

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"

#include "pde/ShallowWater/PDEShallowWater1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Tuple/ElementTuple.h"

using namespace std;


// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( IntegrandCell_Galerkin_ShallowWater1D_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Integrand_SolutionGeometricSeriesx_test )
{
  typedef PhysD1 PhysDim;
  typedef ShallowWaterSolutionFunction1D_GeometricSeriesx SolutionClass;
  typedef PDEShallowWater<PhysDim,VarTypeHVelocity1D,SolutionClass> PDEClass;
  typedef PDENDConvertSpace<PhysDim,PDEClass> NDPDEClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ElementXField<PhysDim,TopoD1,Line> ElementXFieldClass;
  typedef Element<ArrayQ       ,TopoD1,Line> ElementQFieldClass;

  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandClass;
  typedef IntegrandClass::BasisWeighted< Real,TopoD1,Line,ElementXFieldClass > BasisWeightedClass;

  // define pde object
  const Real g = 9.81;  // g: gravitational acceleration [m/s^2]
  const Real q0 = 18.5;    // q0 = v*H
  const int p = 5;      // solution H polynomial degree

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  NDPDEClass pde(q0,solnArgs,PDEClass::ShallowWater_ResidInterp_Raw);

  // static tests: check dimensions
  BOOST_CHECK( pde.D == 1 );
  BOOST_CHECK( pde.N == 2 );

  // check hasFlux components
  BOOST_CHECK( pde.hasFluxAdvectiveTime() == true );
  BOOST_CHECK( pde.hasFluxAdvective() == true );
  BOOST_CHECK( pde.hasFluxViscous() == false );
  BOOST_CHECK( pde.hasSource() == true );
  BOOST_CHECK( pde.hasForcingFunction() == false );

#if PDEShallowWater_sansgH2fluxAdvective1D
  BOOST_CHECK( pde.needsSolutionGradientforSource() == true );
#else
  BOOST_CHECK( pde.needsSolutionGradientforSource() == false );
#endif

  // ----------------------- ELEMENT PARAMETERS ----------------------- //

  // Geometry (line grid)
  int order = 1;
  ElementXFieldClass xfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, xfldElem.order() );
  BOOST_CHECK_EQUAL( 2, xfldElem.nDOF() );

  Real x_L = 1.2;
  Real x_R = 1.4;

  xfldElem.DOF(0) = x_L;
  xfldElem.DOF(1) = x_R;

  // Solution
  ElementQFieldClass varfldElem(order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 1, varfldElem.order() );
  BOOST_CHECK_EQUAL( 2, varfldElem.nDOF() );

  // State vector solution at line cell first node {0}
  Real HL  = 1 + x_L + pow(x_L,2) + pow(x_L,3) + pow(x_L,4) + pow(x_L,5);
  Real uL = q0/HL;
  VarTypeHVelocity1D vardataL( HL, uL);

  // State vector solution at line cell second node {1}
  Real HR  = 1 + x_R + pow(x_R,2) + pow(x_R,3) + pow(x_R,4) + pow(x_R,5);
  Real uR = q0/HR;
  VarTypeHVelocity1D vardataR( HR, uR );

  // Line solution
  varfldElem.DOF(0) = pde.setDOFFrom( vardataL );
  varfldElem.DOF(1) = pde.setDOFFrom( vardataR );

  // ----------------------------- TEST INTEGRAND ----------------------------- //
  IntegrandClass fcnint( pde, {0} );

  //Explicitly create a paramater instance.
  //If it was created in the argument below, the temporary variable in the function argument would go out of scope and result in undefined behavior

  BasisWeightedClass fcn = fcnint.integrand( xfldElem, varfldElem );

  BOOST_CHECK_EQUAL( 2, fcn.nEqn() );
  BOOST_CHECK_EQUAL( 2, fcn.nDOF() );
  BOOST_CHECK( fcn.needsEvaluation() == true );

  typedef ElementXFieldClass::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  RefCoordType sRef; // reference coordinate

  Real HIntegrand;
  Real uIntegrand;

  ArrayQ integrandTrue[2];
  ArrayQ integrand[2];

#if PDEShallowWater_sansgH2fluxAdvective1D
  // Test
  sRef = 0.0;
  fcn( sRef, integrand, 2 ); // IntegrandCell Functor operator()

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  HIntegrand = ( 0.0000000000000000e+00 ) + ( 9.2500000000000014e+01 );
  uIntegrand = ( 7.6570073522617429e+02 ) + ( 1.7233270761496573e+02 );
  integrandTrue[0] = { HIntegrand, uIntegrand };

  // Basis function 2
  HIntegrand = ( 0.0000000000000000e+00 ) + ( -9.2500000000000014e+01 );
  uIntegrand = ( 0.0000000000000000e+00 ) + ( -1.7233270761496573e+02 );
  integrandTrue[1] = { HIntegrand, uIntegrand };
  // True values obtained from: residual_script.m //TODO to be added

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
  }

  // Test at xi = 0.6
  sRef = 0.6;
  fcn( sRef, integrand, 2 ); // IntegrandCell Functor operator()

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  HIntegrand = ( 0.0000000000000000e+00 ) + ( 9.8099113764798886e+01 );
  uIntegrand = ( -4.2988984864277022e+01 ) + ( 1.3981179685304713e+02 );
  integrandTrue[0] = { HIntegrand, uIntegrand };

  // Basis function 2
  HIntegrand = ( 0.0000000000000000e+00 ) + ( -9.8099113764798886e+01 );
  uIntegrand = ( -6.4483477296415529e+01 ) + ( -1.3981179685304713e+02 );
  integrandTrue[1] = { HIntegrand, uIntegrand };
  // True values obtained from: residual_script.m //TODO to be added

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
  }


  // Test at xi = 1.0
  sRef = 1.0;
  fcn( sRef, integrand, 2 ); // IntegrandCell Functor operator()

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  HIntegrand = ( 0.0000000000000000e+00 ) + ( 9.2500000000000014e+01 );
  uIntegrand = ( 0.0000000000000000e+00 ) + ( 1.0483133870461856e+02 );
  integrandTrue[0] = { HIntegrand, uIntegrand };

  // Basis function 2
  HIntegrand = ( 0.0000000000000000e+00 ) + ( -9.2500000000000014e+01 );
  uIntegrand = ( -1.2129833647820985e+03 ) + ( -1.0483133870461856e+02 );
  integrandTrue[1] = { HIntegrand, uIntegrand };
  // True values obtained from: residual_script.m //TODO to be added

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
  }
#else
  // Test
  sRef = 0.0;
  fcn( sRef, integrand, 2 ); // IntegrandCell Functor operator()

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  HIntegrand = ( 0.0000000000000000e+00 ) + ( 9.2500000000000014e+01 );
  uIntegrand = ( -2.3485384107117452e+03 ) + ( 2.5905789149519255e+03 );
  integrandTrue[0] = { HIntegrand, uIntegrand };

  // Basis function 2
  HIntegrand = ( 0.0000000000000000e+00 ) + ( -9.2500000000000014e+01 );
  uIntegrand = ( 0.0000000000000000e+00 ) + ( -2.5905789149519255e+03 );
  integrandTrue[1] = { HIntegrand, uIntegrand };
  // True values obtained from: residual_script.m //TODO to be added

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
  }

  // Test at xi = 0.6
  sRef = 0.6;
  fcn( sRef, integrand, 2 ); // IntegrandCell Functor operator()

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  HIntegrand = ( 0.0000000000000000e+00 ) + ( 9.8099113764798886e+01 );
  uIntegrand = ( -1.7699500542799051e+03 ) + ( 4.7875505500331037e+03 );
  integrandTrue[0] = { HIntegrand, uIntegrand };

  // Basis function 2
  HIntegrand = ( 0.0000000000000000e+00 ) + ( -9.8099113764798886e+01 );
  uIntegrand = ( -2.6549250814198576e+03 ) + ( -4.7875505500331037e+03 );
  integrandTrue[1] = { HIntegrand, uIntegrand };
  // True values obtained from: residual_script.m //TODO to be added

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
  }


  // Test at xi = 1.0
  sRef = 1.0;
  fcn( sRef, integrand, 2 ); // IntegrandCell Functor operator()

  // PDE residual integrands: (source) + (advective flux)
  // Basis function 1
  HIntegrand = ( 0.0000000000000000e+00 ) + ( 9.2500000000000014e+01 );
  uIntegrand = ( 0.0000000000000000e+00 ) + ( 6.6399529649804572e+03 );
  integrandTrue[0] = { HIntegrand, uIntegrand };

  // Basis function 2
  HIntegrand = ( 0.0000000000000000e+00 ) + ( -9.2500000000000014e+01 );
  uIntegrand = ( -6.3324950567219375e+03 ) + ( -6.6399529649804572e+03 );
  integrandTrue[1] = { HIntegrand, uIntegrand };
  // True values obtained from: residual_script.m //TODO to be added

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( integrandTrue[0][n], integrand[0][n], small_tol, close_tol );
    SANS_CHECK_CLOSE( integrandTrue[1][n], integrand[1][n], small_tol, close_tol );
  }
#endif

  // ----------------------------- TEST ELEMENTAL INTEGRAL (RESIDUAL) ----------------------------- //
  const Real small_tol_res = 5e-11;
  const Real close_tol_res = 5e-11;
  int nIntegrand = varfldElem.nDOF();

  Real HRes;
  Real uRes;
  ArrayQ residualTrue[2];
  ArrayQ residual[2] = {0,0};

  // Test the element integral of the functor: quadrature order = 39
  int quadratureorder = 39;
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ> integral(quadratureorder, nIntegrand);

  // cell integration for canonical element
  integral( fcn, xfldElem, residual, nIntegrand );

#if PDEShallowWater_sansgH2fluxAdvective1D
  // PDE residuals: (source) + (advective flux)
  // Basis function 1
  HRes = ( 0.0000000000000000e+00 ) + ( 1.9277654689555398e+01 );
  uRes = ( 3.1771776396599357e+01 ) + ( 2.8881474471939907e+01 );
  residualTrue[0] = { HRes, uRes };

  // Basis function 2
  HRes = ( 0.0000000000000000e+00 ) + ( -1.9277654689555398e+01 );
  uRes = ( -3.3521234680426311e+01 ) + ( -2.8881474471939907e+01 );
  residualTrue[1] = { HRes, uRes };
  // True values obtained from: residual_script.m //TODO to be added
#else
  // PDE residuals: (source) + (advective flux)
  // Basis function 1
  HRes = ( 0.0000000000000000e+00 ) + ( 1.9277654689555398e+01 );
  uRes = ( -3.4649455639725636e+02 ) + ( 8.9079704873318758e+02 );
  residualTrue[0] = { HRes, uRes };

  // Basis function 2
  HRes = ( 0.0000000000000000e+00 ) + ( -1.9277654689555398e+01 );
  uRes = ( -4.7862998567434613e+02 ) + ( -8.9079704873318758e+02 );
  residualTrue[1] = { HRes, uRes };
  // True values obtained from: residual_script.m //TODO to be added
#endif

  for (int n = 0; n < pde.N; n++)
  {
    SANS_CHECK_CLOSE( residualTrue[0][n], residual[0][n], small_tol_res, close_tol_res );
    SANS_CHECK_CLOSE( residualTrue[1][n], residual[1][n], small_tol_res, close_tol_res );
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
