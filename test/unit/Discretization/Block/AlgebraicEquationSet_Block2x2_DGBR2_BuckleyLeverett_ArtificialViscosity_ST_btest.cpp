// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_Block2x2_DGBR2_BuckleyLeverett_ArtificialViscosity_ST_btest
// testing AlgebraicEquationSet_DGBR2

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <fstream>
#include <string>

#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"
#include "pde/PorousMedia/Sensor_BuckleyLeverett.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/JacobianParam.h"
#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/HField/GenHFieldArea_CG.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#include "unit/Discretization/jacobianPingTest_btest.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Block2x2_DGBR2_BuckleyLeverett_ArtificialViscosity_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseSystem )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;

  typedef QTypePrimitive_Sw QType;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef TraitsModelBuckleyLeverett<QType, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass_noAV;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_noAV> NDPDEClass_noAV;

  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass_Primal;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_Primal> NDPDEClass_Primal;

  typedef NDPDEClass_Primal::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass_Primal::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass_Primal::template MatrixQ<Real> MatrixQ;
  typedef NDPDEClass_Primal::template MatrixParam<Real> PDEMatrixParam;

  typedef Sensor_BuckleyLeverett<PDEClass_noAV> Sensor;
  typedef Sensor_AdvectiveFlux1D_Uniform Sensor_Advection;
  typedef Sensor_ViscousFlux1D_GenHScale Sensor_Diffusion;
  typedef Source1D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             SensorParameterTraits<PhysD1>,
                             Sensor_Advection,
                             Sensor_Diffusion,
                             Source_JumpSensor > PDEClass_Sensor;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_Sensor> NDPDEClass_Sensor;

  typedef NDPDEClass_Sensor::template ArrayQ<Real> SensorArrayQ;
  typedef NDPDEClass_Sensor::template VectorArrayQ<Real> SensorVectorArrayQ;
  typedef NDPDEClass_Sensor::template MatrixQ<Real> SensorMatrixQ;
  typedef NDPDEClass_Sensor::template MatrixParam<Real> SensorMatrixParam;

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QType, PDEClass_noAV> ExactSolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef BCBuckleyLeverett_ArtificialViscosity1DVector<TraitsSizeBuckleyLeverett, TraitsModelClass> BCVectorPrimal;
  typedef BCParameters<BCVectorPrimal> BCParamsPrimal;

  typedef BCSensorParameter1DVector<Sensor_Advection, Sensor_Diffusion> BCVectorSensor;
  typedef BCParameters<BCVectorSensor> BCParamsSensor;

  typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         Field<PhysDim, TopoDim, SensorArrayQ>,
                                         XField<PhysDim, TopoDim>>::type ParamFieldPrimal;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         Field<PhysDim, TopoDim, ArrayQ>,
                                         XField<PhysDim, TopoDim>>::type ParamFieldSensor;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Primal, BCNDConvertSpaceTime, BCVectorPrimal,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldPrimal> PrimalEquationSetClass;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Sensor, BCNDConvertSpaceTime, BCVectorSensor,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldSensor> SensorEquationSetClass;

  int order_primal = 1;
  int order_sensor = 1;

  // Primal PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 0.0;
  CapillaryModel cap_model(pc_max);

  NDPDEClass_noAV pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  //Create artificial viscosity PDE
  const bool hasSpaceTimeDiffusion = false;
  NDPDEClass_Primal pde_primal(order_primal, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 0.9;
  const Real SwR = 0.1;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(pde, SwL, SwR, xinit);


  //Sensor PDE
  Sensor sensor(pde_primal);

  Sensor_Advection sensor_adv(0.0);
  Real Cdiff = 3.0;
  Sensor_Diffusion sensor_visc(Cdiff);
  Source_JumpSensor sensor_source(order_primal, sensor);

  NDPDEClass_Sensor pde_sensor(sensor_adv, sensor_visc, sensor_source);


  // Primal PDE BCs
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitState, BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>> BCParamsAV;

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.None;

  PyDict BCDirichletL;
  BCDirichletL[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.Dirichlet_mitState;
  BCDirichletL[BCParamsAV::params.qB] = SwL;
  BCDirichletL[BCParamsAV::params.Cdiff] = Cdiff;

  PyDict BCDirichletR;
  BCDirichletR[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.Dirichlet_mitState;
  BCDirichletR[BCParamsAV::params.qB] = SwR;
  BCDirichletR[BCParamsAV::params.Cdiff] = Cdiff;

  PyDict PyBCList_Primal;
  PyBCList_Primal["DirichletL"] = BCDirichletL;
  PyBCList_Primal["DirichletR"] = BCDirichletR;
  PyBCList_Primal["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Primal;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_Primal["DirichletL"] = {3}; //Left boundary
  BCBoundaryGroups_Primal["DirichletR"] = {0,1}; //Bottom and right boundary
  BCBoundaryGroups_Primal["None"] = {2}; //Top boundary

  //Check the BC dictionary
  BCParamsPrimal::checkInputs(PyBCList_Primal);

  std::vector<int> active_boundaries_primal = BCParamsPrimal::getLGBoundaryGroups(PyBCList_Primal, BCBoundaryGroups_Primal);

  // Sensor BCs
  PyDict BCSensorRobin;
  BCSensorRobin[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 10.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorNone;
  BCSensorNone[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.None;

  PyDict PyBCList_Sensor;
  PyBCList_Sensor["SensorRobin"] = BCSensorRobin;
//  PyBCList_Sensor["SensorNone"] = BCSensorNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Sensor;
  BCBoundaryGroups_Sensor["SensorRobin"] = {0, 1, 2, 3}; // Bottom, right, left
//  BCBoundaryGroups_Sensor["SensorNone"] = {2}; // Top

  //Check the BC dictionary
  BCParamsSensor::checkInputs(PyBCList_Sensor);

  std::vector<int> active_boundaries_sensor = BCParamsSensor::getLGBoundaryGroups(PyBCList_Sensor, BCBoundaryGroups_Sensor);


  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  std::vector<Real> tol = {1e-11, 1e-11};


  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////

  int ii = 1;
  int jj = 1;
  XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 6.0, 0, 3.0, true );

  GenHField_CG<PhysDim, TopoDim> hfld(xfld);

  QuadratureOrder quadratureOrder( xfld, - 1 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate primal solution fields
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> pde_qfld(xfld, order_primal, BasisFunctionCategory_Legendre);
  FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ> pde_rfld(xfld, order_primal, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> pde_lgfld( xfld, order_primal, BasisFunctionCategory_Legendre, active_boundaries_primal );
  pde_qfld = 0.2;
  pde_rfld = 0;
  pde_lgfld = 0;

  //Perturb field so that there are small jumps
  for (int i = 0; i < pde_qfld.nDOF(); i++)
  {
    Real eps = (2*(Real)rand()/RAND_MAX - 1);
    pde_qfld.DOF(i) += 1e-3*eps;
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate sensor solution fields
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysDim, TopoDim, SensorArrayQ> sensorfld(xfld, order_sensor, BasisFunctionCategory_Legendre);
  FieldLift_DG_Cell<PhysDim, TopoDim, SensorVectorArrayQ> sensor_rfld(xfld, order_sensor, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysDim, TopoDim, SensorArrayQ> sensor_lgfld(xfld, order_sensor, BasisFunctionCategory_Legendre, active_boundaries_sensor);
  sensorfld = 0.1;
  sensor_rfld = 0;
  sensor_lgfld = 0;

  //Perturb field so that there are small jumps
  for (int i = 0; i < sensorfld.nDOF(); i++)
  {
    Real eps = (2*(Real)rand()/RAND_MAX - 1);
    sensorfld.DOF(i) += 1e-3*eps;
  }


  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our AES to solve our PDE mit AV
  ////////////////////////////////////////////////////////////////////////////////////////

  ParamFieldPrimal paramfld_primal = (hfld, sensorfld, xfld);
  PrimalEquationSetClass PrimalEqSet(paramfld_primal, pde_qfld, pde_rfld, pde_lgfld, pde_primal,
                                     disc, quadratureOrder, ResidualNorm_Default, tol,
                                     {0}, {0}, PyBCList_Primal, BCBoundaryGroups_Primal);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our AES to solve the sensor PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  ParamFieldSensor paramfield_sensor = ( hfld, pde_qfld, xfld);
  SensorEquationSetClass SensorEqSet(paramfield_sensor, sensorfld, sensor_rfld, sensor_lgfld, pde_sensor,
                                     disc, quadratureOrder, ResidualNorm_Default, tol,
                                     {0}, {0}, PyBCList_Sensor, BCBoundaryGroups_Sensor);

  //Initialize the sensor field to be consistent with the initial qfld
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass00;
  typedef SensorEquationSetClass::SystemMatrix SystemMatrixClass11;

//  NewtonSolver<SystemMatrixClass11,SystemNonZeroPattern11> SensorSolver( SensorEqSet, NewtonSolverDict );
//
//  // set initial condition from current solution in solution fields
//  SystemVectorClass1 sensor_sln0(SensorEqSet.vectorStateSize());
//  SensorEqSet.fillSystemVector(sensor_sln0);
//
//  // solve for the sensor field for the initial qfld solution
//  SystemVectorClass1 sensor_sln(SensorEqSet.vectorStateSize());
//  SolveStatus status = SensorSolver.solve(sensor_sln0, sensor_sln);
//  BOOST_CHECK( status.converged );


  ////////////////////////////////////////////////////////////////////////////////////////
  // Generate off diagonal AES
  ////////////////////////////////////////////////////////////////////////////////////////
  const int iPDE_SensorParam = 1; // Sensor parameter in PDE index
  const int iSensor_PDEParam = 1; // PDE solution parameter in sensor index

  typedef JacobianParam<boost::mpl::vector1_c<int,iPDE_SensorParam>,PrimalEquationSetClass> JacobianParam_PDE;
  typedef JacobianParam<boost::mpl::vector1_c<int,iSensor_PDEParam>,SensorEquationSetClass> JacobianParam_Sensor;

  typedef JacobianParam_PDE::SystemMatrix    SystemMatrixClass01;
  typedef JacobianParam_Sensor::SystemMatrix SystemMatrixClass10;

  std::map<int,int> PDEMap;
  PDEMap[iPDE_SensorParam] = SensorEqSet.iq; //The parameter in the PDE

  std::map<int,int> SensorMap;
  SensorMap[iSensor_PDEParam] = PrimalEqSet.iq;

  JacobianParam_PDE JP01(PrimalEqSet, PDEMap);
  JacobianParam_Sensor JP10(SensorEqSet, SensorMap);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Generate 2x2 Block system
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass00, SystemMatrixClass01,
                                        SystemMatrixClass10, SystemMatrixClass11> AlgEqSetType_2x2;

  AlgEqSetType_2x2 BlockAES(PrimalEqSet, JP01,
                            JP10, SensorEqSet);

  typedef AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
  typedef AlgEqSetType_2x2::SystemVector BlockVectorClass;
  typedef AlgEqSetType_2x2::SystemNonZeroPattern BlockNonZeroPattern;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial guess
  ////////////////////////////////////////////////////////////////////////////////////////
  BlockVectorClass q(BlockAES.vectorStateSize());
  BlockVectorClass sln(BlockAES.vectorStateSize());
  BlockAES.fillSystemVector(q);
  sln = q;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Residual
  ////////////////////////////////////////////////////////////////////////////////////////
  BlockVectorClass rsd(BlockAES.vectorEqSize());
  BlockVectorClass rsdp(BlockAES.vectorEqSize());
  BlockVectorClass rsdm(BlockAES.vectorEqSize());
  rsd = 0;
  rsdp = 0;
  rsdm = 0;
  BlockAES.residual(q, rsd);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Jacobian
  ////////////////////////////////////////////////////////////////////////////////////////
  BlockNonZeroPattern nz(BlockAES.matrixSize());
  BlockAES.jacobian(q, nz);
  BlockMatrixClass jac(nz);
  jac = 0;
  BlockAES.jacobian(q, jac);

  BOOST_REQUIRE_EQUAL(jac.m(), 2);
  BOOST_REQUIRE_EQUAL(jac.n(), 2);

  BOOST_REQUIRE_EQUAL(jac.m00.m(), 2);
  BOOST_REQUIRE_EQUAL(jac.m00.n(), 2);

  BOOST_REQUIRE_EQUAL(jac.m01.m(), 2);
  BOOST_REQUIRE_EQUAL(jac.m01.n(), 2);

  BOOST_REQUIRE_EQUAL(jac.m10.m(), 2);
  BOOST_REQUIRE_EQUAL(jac.m10.n(), 2);

  BOOST_REQUIRE_EQUAL(jac.m11.m(), 2);
  BOOST_REQUIRE_EQUAL(jac.m11.n(), 2);

#if 0
  std::string filename = "tmp/test00.mtx";
  std::fstream fout00( filename, std::fstream::out );
  WriteMatrixMarketFile( jac.m00, fout00 );

  filename = "tmp/test01.mtx";
  std::fstream fout01( filename, std::fstream::out );
  WriteMatrixMarketFile( jac.m01, fout01 );

  filename = "tmp/test10.mtx";
  std::fstream fout10( filename, std::fstream::out );
  WriteMatrixMarketFile( jac.m10, fout10 );

  filename = "tmp/test11.mtx";
  std::fstream fout11( filename, std::fstream::out );
  WriteMatrixMarketFile( jac.m11, fout11 );
#endif

#ifdef DISPLAY_FOR_DEBUGGING
  bool verbose = true;
#else
  bool verbose = false;
#endif

  {
    const ArrayQ qArrayQscale = {1};

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(0,0) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ>(jac.m00, rsdp, rsdm, rsdp.v0, rsdm.v0, q, q.v0, BlockAES,
                            {1e-3, 1e-4}, qArrayQscale, {1.9, 2.1}, verbose);
#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(1,0) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<SensorMatrixParam>(jac.m10, rsdp, rsdm, rsdp.v1, rsdm.v1, q, q.v0, BlockAES,
                                      {1e-5, 1e-6}, qArrayQscale, {1.9, 2.1}, verbose);
  }

  {
    const SensorArrayQ qArrayQscale = {1};

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(0,1) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<PDEMatrixParam>(jac.m01, rsdp, rsdm, rsdp.v0, rsdm.v0, q, q.v1, BlockAES,
                                   {1e-3, 1e-4}, qArrayQscale, {1.9, 2.1}, verbose);

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(1,1) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<SensorMatrixQ>(jac.m11, rsdp, rsdm, rsdp.v1, rsdm.v1, q, q.v1, BlockAES,
                                  {1e-3, 1e-4}, qArrayQscale);
  }


}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
