// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#define IS_DISPLAY_FOR_DEBUGGING 0

#include <boost/test/unit_test.hpp>

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "SANS_btest.h"

#define ALGEBRAICEQUATIONSET_BLOCK4X4_INSTANTIATE // HACK : need proper instantiations
#include "Discretization/Block/AlgebraicEquationSet_Block4x4_impl.h"

#include "Discretization/Block/AlgebraicEquationSet_Block4x4.h"
#include "Discretization/Block/JacobianParam_Decoupled.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "LinearAlgebra/SparseLinAlg/ScalarMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_4x4.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_4.h"

#include "unit/LinearAlgebra/Heat1D_btest.h"

using namespace std;
using namespace SANS;
using namespace SANS::BLA;
using namespace SANS::DLA;
using namespace SANS::SLA;

namespace SANS
{

}

//----------------------------------------------------------------------------//
// Utility stuff

static const int M0 = 4, M1 = 3, M2 = 2, M3 = 2;
static const int N0 = 4, N1 = 3, N2 = 2, N3 = 2;

typedef AlgEqSetTraits_Sparse AESTraitsTag;
typedef AlgebraicEquationSetTraits<MatrixS<M0,N0,Real>, VectorS<N0,Real>, AESTraitsTag> Mtraits0;
typedef AlgebraicEquationSetTraits<MatrixS<M1,N1,Real>, VectorS<N1,Real>, AESTraitsTag> Mtraits1;
typedef AlgebraicEquationSetTraits<MatrixS<M2,N2,Real>, VectorS<N2,Real>, AESTraitsTag> Mtraits2;
typedef AlgebraicEquationSetTraits<MatrixS<M3,N3,Real>, VectorS<N3,Real>, AESTraitsTag> Mtraits3;

typedef MatrixD<SparseMatrix_CRS<MatrixS<M0,N0,Real>>> M00;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M0,N1,Real>>> M01;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M0,N2,Real>>> M02;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M0,N3,Real>>> M03;

typedef MatrixD<SparseMatrix_CRS<MatrixS<M1,N0,Real>>> M10;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M1,N1,Real>>> M11;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M1,N2,Real>>> M12;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M1,N3,Real>>> M13;

typedef MatrixD<SparseMatrix_CRS<MatrixS<M2,N0,Real>>> M20;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M2,N1,Real>>> M21;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M2,N2,Real>>> M22;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M2,N3,Real>>> M23;

typedef MatrixD<SparseMatrix_CRS<MatrixS<M3,N0,Real>>> M30;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M3,N1,Real>>> M31;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M3,N2,Real>>> M32;
typedef MatrixD<SparseMatrix_CRS<MatrixS<M3,N3,Real>>> M33;

typedef MatrixD<SparseNonZeroPattern<MatrixS<M0,N0,Real>>> MNZ00;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M0,N1,Real>>> MNZ01;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M0,N2,Real>>> MNZ02;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M0,N3,Real>>> MNZ03;

typedef MatrixD<SparseNonZeroPattern<MatrixS<M1,N0,Real>>> MNZ10;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M1,N1,Real>>> MNZ11;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M1,N2,Real>>> MNZ12;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M1,N3,Real>>> MNZ13;

typedef MatrixD<SparseNonZeroPattern<MatrixS<M2,N0,Real>>> MNZ20;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M2,N1,Real>>> MNZ21;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M2,N2,Real>>> MNZ22;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M2,N3,Real>>> MNZ23;

typedef MatrixD<SparseNonZeroPattern<MatrixS<M3,N0,Real>>> MNZ30;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M3,N1,Real>>> MNZ31;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M3,N2,Real>>> MNZ32;
typedef MatrixD<SparseNonZeroPattern<MatrixS<M3,N3,Real>>> MNZ33;

typedef AlgebraicEquationSet_Block4x4<M00, M01, M02, M03,
                                      M10, M11, M12, M13,
                                      M20, M21, M22, M23,
                                      M30, M31, M32, M33,
                                      AlgebraicEquationSetBase> AlgebraicEquationSetBlock4x4_Type;

typedef AlgebraicEquationSetBlock4x4_Type::SystemMatrix BlockSystemMatrixType;
typedef AlgebraicEquationSetBlock4x4_Type::SystemNonZeroPattern BlockSystemMatrixNZType;
typedef AlgebraicEquationSetBlock4x4_Type::SystemVector BlockSystemVectorType;

//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Block4x4_Heat1D )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_Block4x4_Heat1D_Decoupled_test )
{
  const Real small_tol = 1e-14, close_tol = 1e-14;
  const int m0 = 4, m1 = 3, m2 = 2, m3 = 5;

  // individual block components: equation set or parameter jacobians
  VectorType<M00>::type q0({ SparseVectorSize(m0) });
  Heat1DEquationSet<M00> AE00(q0);

  VectorType<M11>::type q1({ SparseVectorSize(m1) });
  Heat1DEquationSet<M11> AE11(q1);

  VectorType<M22>::type q2({ SparseVectorSize(m2) });
  Heat1DEquationSet<M22> AE22(q2);

  VectorType<M33>::type q3({ SparseVectorSize(m3) });
  Heat1DEquationSet<M33> AE33(q3);

  JacobianParam_Decoupled<M01,Mtraits0> JP01(AE00);
  JacobianParam_Decoupled<M02,Mtraits0> JP02(AE00);
  JacobianParam_Decoupled<M03,Mtraits0> JP03(AE00);

  JacobianParam_Decoupled<M10,Mtraits1> JP10(AE11);
  JacobianParam_Decoupled<M12,Mtraits1> JP12(AE11);
  JacobianParam_Decoupled<M13,Mtraits1> JP13(AE11);

  JacobianParam_Decoupled<M20,Mtraits2> JP20(AE22);
  JacobianParam_Decoupled<M21,Mtraits2> JP21(AE22);
  JacobianParam_Decoupled<M23,Mtraits2> JP23(AE22);

  JacobianParam_Decoupled<M30,Mtraits3> JP30(AE33);
  JacobianParam_Decoupled<M31,Mtraits3> JP31(AE33);
  JacobianParam_Decoupled<M32,Mtraits3> JP32(AE33);

  // block system

  // constructor
  AlgebraicEquationSetBlock4x4_Type BlockAES(AE00, JP01, JP02, JP03,
                                             JP10, AE11, JP12, JP13,
                                             JP20, JP21, AE22, JP23,
                                             JP30, JP31, JP32, AE33);
#ifdef __clang_analyzer__
  return; // I just don't know how to fix the warnings...
#endif

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Block AES query
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  BlockSystemVectorType q(BlockAES.vectorStateSize());
  q.v0 = 4; q.v0[0][m0-2][M0-2] = 40;
  q.v1 = 3; q.v1[0][m1-2][M1-2] = 30;
  q.v2 = 2; q.v2[0][m2-2][M2-2] = 20;
  q.v3 = 1; q.v3[0][m3-2][M3-2] = 10;

#if IS_DISPLAY_FOR_DEBUGGING // print block vector
  cout << "q.v0 = {" << q.v0 << "}" << endl;
  cout << "q.v1 = {" << q.v1 << "}" << endl;
  cout << "q.v2 = {" << q.v2 << "}" << endl;
  cout << "q.v3 = {" << q.v3 << "}" << endl;
#endif

  // set solution field from system vector input
  BlockAES.setSolutionField(q);

  // fill system vector output from solution field
  BlockSystemVectorType q_copy(BlockAES.vectorStateSize());
  BlockAES.fillSystemVector(q_copy);

#if IS_DISPLAY_FOR_DEBUGGING // print block vector
  cout << "q_copy.v0 = {" << q_copy.v0 << "}" << endl;
  cout << "q_copy.v1 = {" << q_copy.v1 << "}" << endl;
  cout << "q_copy.v2 = {" << q_copy.v2 << "}" << endl;
  cout << "q_copy.v3 = {" << q_copy.v3 << "}" << endl;
#endif

  // check state validity
  BOOST_CHECK( BlockAES.isValidStateSystemVector(q) );

  // residual
  BlockSystemVectorType rsd(BlockAES.vectorEqSize());
  BlockAES.residual(rsd);

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Check the residuals of BlockAES match individual AES directly
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  VectorType<M00>::type rsd0(AE00.vectorEqSize());
  AE00.residual(q.v0, rsd0);
  BOOST_REQUIRE_EQUAL( rsd.v0.m(), rsd0.m() );

  for ( int ii = 0; ii < rsd.v0.m(); ii++ )
    for ( int i = 0; i < rsd.v0[ii].m(); i++ )
      for ( int j = 0; j < M0; ++j )
        SANS_CHECK_CLOSE( index(rsd.v0[ii][i],j), index(rsd0[ii][i],j), small_tol, close_tol );

  VectorType<M11>::type rsd1(AE11.vectorEqSize());
  AE11.residual(q.v1, rsd1);
  BOOST_REQUIRE_EQUAL( rsd.v1.m(), rsd1.m() );

  for ( int ii = 0; ii < rsd.v1.m(); ii++ )
    for ( int i = 0; i < rsd.v1[ii].m(); i++ )
      for ( int j = 0; j < M1; ++j )
        SANS_CHECK_CLOSE( index(rsd.v1[ii][i],j), index(rsd1[ii][i],j), small_tol, close_tol );


  VectorType<M22>::type rsd2(AE22.vectorEqSize());
  AE22.residual(q.v2, rsd2);
  BOOST_REQUIRE_EQUAL( rsd.v2.m(), rsd2.m() );

  for ( int ii = 0; ii < rsd.v2.m(); ii++ )
    for ( int i = 0; i < rsd.v2[ii].m(); i++ )
      for ( int j = 0; j < M2; ++j )
        SANS_CHECK_CLOSE( index(rsd.v2[ii][i],j), index(rsd2[ii][i],j), small_tol, close_tol );


  VectorType<M33>::type rsd3(AE33.vectorEqSize());
  AE33.residual(q.v3, rsd3);
  BOOST_REQUIRE_EQUAL( rsd.v3.m(), rsd3.m() );

  for ( int ii = 0; ii < rsd.v3.m(); ii++ )
    for ( int i = 0; i < rsd.v3[ii].m(); i++ )
      for ( int j = 0; j < M3; ++j )
        SANS_CHECK_CLOSE( index(rsd.v3[ii][i],j), index(rsd3[ii][i],j), small_tol, close_tol );
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Check the jacobian and tranpose of BlockAES by observation & pattern matching
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // jacobian nonzero pattern
  BlockSystemMatrixNZType nz(BlockAES.matrixSize());
  BlockAES.jacobian(nz);

  // jacobian
  BlockSystemMatrixType jac(nz);
  BlockAES.jacobian(jac);

  // jacobian nonzero pattern transpose
  BlockSystemMatrixNZType nzT(BlockAES.matrixSize());
  BlockAES.jacobianTranspose(nzT);

  // jacobian transpose
  BlockSystemMatrixType jacT(nzT);
  BlockAES.jacobianTranspose(jacT);

#if 0 // just printout, no check
  std::fstream file_output("tmp/block4x4jac.mtx", std::ios::out);
  WriteMatrixMarketFile(jac,file_output);
#else // check output by pattern matching
  std::string filename_jac("IO/Discretization/AESblock4x4jac_Heat1D_Decoupled.mtx");
  //Set the 2nd argument to false to regenerate the file
  output_test_stream output_jac( filename_jac, true );
  WriteMatrixMarketFile(jac, output_jac);
  BOOST_CHECK( output_jac.match_pattern() );

  std::string filename_jacT("IO/Discretization/AESblock4x4jacT_Heat1D_Decoupled.mtx");
  //Set the 2nd argument to false to regenerate the file
  output_test_stream output_jacT( filename_jacT, true );
  WriteMatrixMarketFile(jacT, output_jacT);
  BOOST_CHECK( output_jacT.match_pattern() );
#endif

  // residual norm
  const std::vector<std::vector<Real>> rsdNorm = BlockAES.residualNorm(rsd);
  const std::vector<std::vector<Real>> rsdNorm0 = AE00.residualNorm(rsd.v0);
  const std::vector<std::vector<Real>> rsdNorm1 = AE11.residualNorm(rsd.v1);
  const std::vector<std::vector<Real>> rsdNorm2 = AE22.residualNorm(rsd.v2);
  const std::vector<std::vector<Real>> rsdNorm3 = AE33.residualNorm(rsd.v3);

  // TODO: Strictly speaking, std::equal shouldn't be used for floating point number comparison
  BOOST_CHECK( std::equal(rsdNorm0.begin(), rsdNorm0.begin()+rsdNorm0.size(), rsdNorm.begin() ) );
  BOOST_CHECK( std::equal(rsdNorm1.begin(), rsdNorm1.begin()+rsdNorm1.size(), rsdNorm.begin()+rsdNorm0.size() ) );
  BOOST_CHECK( std::equal(rsdNorm2.begin(), rsdNorm2.begin()+rsdNorm2.size(), rsdNorm.begin()+rsdNorm0.size()+rsdNorm1.size() ) );
  BOOST_CHECK( std::equal(rsdNorm3.begin(), rsdNorm3.begin()+rsdNorm3.size(), rsdNorm.begin()+rsdNorm0.size()+rsdNorm1.size()+rsdNorm2.size() ) );

  // check residual convergence
  BOOST_CHECK( !(BlockAES.convergedResidual(rsdNorm)) ); // residual has not converged yet

  // check residual decrease
  BOOST_CHECK( !(BlockAES.decreasedResidual(rsdNorm,rsdNorm)) ); // rsdNorm here is calculated from the same rsd, so it the residual is not decreased

  // print decrease residual failure
  {
    std::string filename("IO/Discretization/AESblock4x4jac_Heat1D_Decoupled_printDecreaseResidualFailure.txt");
    //Set the 2nd argument to false to regenerate the file
    output_test_stream output_stream( filename, true );
    BlockAES.printDecreaseResidualFailure(rsdNorm, output_stream);
    BOOST_CHECK( output_stream.match_pattern() );
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // TODO: Throw developer exceptions for parts not yet implemented
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_THROW(BlockAES.indexPDE(), DeveloperException);
  BOOST_CHECK_THROW(BlockAES.indexQ(), DeveloperException);
  BOOST_CHECK_THROW(BlockAES.nResidNorm(), DeveloperException);

  AlgebraicEquationSetBlock4x4_Type::LinesearchData pData_dummy;
  BOOST_CHECK_THROW(BlockAES.updateLinesearchDebugInfo(0.0, rsd, pData_dummy, pData_dummy), DeveloperException);
  BOOST_CHECK_THROW(BlockAES.dumpLinesearchDebugInfo("dummystr", -1, pData_dummy), DeveloperException);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
