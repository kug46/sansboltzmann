// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_Block2x2_Triangle_AD_btest
// testing AlgebraicEquationSet_Block2x2

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <string>
#include <fstream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/output_std_vector.h"

#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"

#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"
#include "Discretization/Block/JacobianParam_Decoupled.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Block2x2_SameDPE_Triangle_AD_btest )


template<class MatrixQ>
void checkSparseJacobianEquality(const SLA::SparseMatrix_CRS<MatrixQ>& djac, const SLA::SparseMatrix_CRS<MatrixQ>& sjac )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const int *row_ptr = sjac.get_row_ptr();

  for (int i = 0; i < sjac.m(); i++)
    for (int j = row_ptr[i]; j < row_ptr[i+1]; j++)
      SANS_CHECK_CLOSE( djac[j], sjac[j], small_tol, close_tol );
}

template<class SparseMatrixClass>
void checkJacobianTranspose(const DLA::MatrixD<SparseMatrixClass>& jac, const DLA::MatrixD<SparseMatrixClass>& jacT )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  //Check that jacT is a transpose of jac
  for ( int i = 0; i < jac.m(); i++ )
    for ( int j = 0; j < jac.n(); j++ )
    {
      const SparseMatrixClass& jacij = jac(i,j);
      const SparseMatrixClass& jacTji = jacT(j,i);

      BOOST_REQUIRE_EQUAL(jacij.m(), jacTji.n());
      BOOST_REQUIRE_EQUAL(jacij.n(), jacTji.m());

      for ( int ib = 0; ib < jacij.m(); ib++ )
        for ( int jb = 0; jb < jacij.n(); jb++ )
        {
          //Only compare non-zero entries
          if ( !jacij.isNonZero(ib,jb) )
          {
            //Make sure the transpose is also a zero entry
            BOOST_CHECK( !jacTji.isNonZero(jb,ib) );
            continue;
          }
          //Check that the numbers are the same
          SANS_CHECK_CLOSE( jacij(ib,jb), jacTji(jb,ib), small_tol, close_tol );
        }
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseSystem_test )
{
#if 1
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> SolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetClass::TraitsType AlgEqTraitsType;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  // PDE
  Real u1 = 1;
  Real u2 = 0;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv1( u1, v );
  AdvectiveFlux2D_Uniform adv2( u2, v );

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);
  SolutionExact solnExact;
  Source2D_None source;

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact) );

  NDPDEClass pde1( adv1, visc, source, forcingptr );
  NDPDEClass pde2( adv2, visc, source, forcingptr );

  // BC
  PyDict SineSine;
  SineSine[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SineSine;

  PyDict PyBCList;
  PyBCList["BCNameSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  // grid: HierarchicalP1 (aka X1)
  int ii = 3;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  // solution: HierarchicalP1 (aka Q1)
  int order = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld1(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld1 = 0;

  // Lagrange multiplier: Legendre P1
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld1( xfld, order, BasisFunctionCategory_Hierarchical,
                                                         BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld1 = 0;


  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld2(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld2 = 0;

  // Lagrange multiplier: Legendre P1
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld2( xfld, order, BasisFunctionCategory_Hierarchical,
                                                         BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld2 = 0;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  typedef JacobianParam_Decoupled< SystemMatrixClass,
                                   AlgEqTraitsType > JacobianParamClass;

  StabilizationNitsche stab(order);
  PrimalEquationSetClass AES00(xfld, qfld1, lgfld1, pde1, stab, quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );
  PrimalEquationSetClass AES11(xfld, qfld2, lgfld2, pde2, stab, quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );

  JacobianParamClass JP01(AES00);
  JacobianParamClass JP10(AES11);

  typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass, SystemMatrixClass,
                                        SystemMatrixClass, SystemMatrixClass> AlgEqSetType_2x2;

  AlgEqSetType_2x2 BlockAES(AES00, JP01,
                            JP10, AES11);

  typedef AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
  typedef AlgEqSetType_2x2::SystemVector BlockVectorClass;
  typedef AlgEqSetType_2x2::SystemNonZeroPattern BlockNonZeroPattern;

  typename AlgEqSetType_2x2::MatrixSizeClass blockMatrixSize = BlockAES.matrixSize();

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // AES00 direct query
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  SystemVectorClass q0(AES00.vectorStateSize());
  q0 = 0;
  AES00.fillSystemVector(q0);

  SystemVectorClass rsd0(AES00.vectorEqSize());
  rsd0 = 0;
  AES00.residual(q0, rsd0);

  // jacobian nonzero pattern
  SystemNonZeroPattern nz0(AES00.matrixSize());
  AES00.jacobian(q0, nz0);

  // jacobian
  SystemMatrixClass jac00(nz0);
  jac00 = 0;
  AES00.jacobian(q0, jac00);
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // JP01 direct query
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // jacobian nonzero pattern
  SystemNonZeroPattern nz01(blockMatrixSize.m01);
  JP01.jacobian(nz01);

  // jacobian
  SystemMatrixClass jac01(nz01);
  jac01 = 0;
  JP01.jacobian(jac01);
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // AES11 direct query
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  SystemVectorClass q1(AES00.vectorStateSize());
  q1 = 0;
  AES00.fillSystemVector(q1);

  SystemVectorClass rsd1(AES00.vectorEqSize());
  rsd1 = 0;
  AES11.residual(q1, rsd1);

  // jacobian nonzero pattern
  SystemNonZeroPattern nz1(AES11.matrixSize());
  AES00.jacobian(q1, nz1);

  // jacobian
  SystemMatrixClass jac11(nz1);
  jac11 = 0;
  AES11.jacobian(q1, jac11);
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // JP10 direct query
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // jacobian nonzero pattern
  SystemNonZeroPattern nz10(blockMatrixSize.m10);
  JP10.jacobian(nz10);

  // jacobian
  SystemMatrixClass jac10(nz10);
  jac10 = 0;
  JP10.jacobian(jac10);
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Block AES query
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  BlockVectorClass q(BlockAES.vectorStateSize());
  q = 0;
  BlockAES.fillSystemVector(q);

  BlockVectorClass rsd(BlockAES.vectorEqSize());
  rsd = 0;
  BlockAES.residual(q, rsd);

  // jacobian nonzero pattern
  BlockNonZeroPattern nz(BlockAES.matrixSize());
  BlockAES.jacobian(q, nz);

  // jacobian
  BlockMatrixClass jac(nz);
  jac.m00 = 0;
  jac.m01 = 0;
  jac.m10 = 0;
  jac.m11 = 0;
  BlockAES.jacobian(q, jac);
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Check the residuals for the BlockAES match AES00 directly
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( q.v0.m(), q0.m() );
  BOOST_REQUIRE_EQUAL( rsd.v0.m(), rsd0.m() );

  for ( int ii = 0; ii < rsd.v0.m(); ii++ )
    for ( int i = 0; i < rsd.v0[ii].m(); i++ )
      SANS_CHECK_CLOSE( rsd.v0[ii][i], rsd0[ii][i], small_tol, close_tol );
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Check the residuals for the BlockAES match AES11 directly
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( q.v1.m(), q1.m() );
  BOOST_REQUIRE_EQUAL( rsd.v1.m(), rsd1.m() );

  for ( int ii = 0; ii < rsd.v1.m(); ii++ )
    for ( int i = 0; i < rsd.v1[ii].m(); i++ )
      SANS_CHECK_CLOSE( rsd.v1[ii][i], rsd1[ii][i], small_tol, close_tol );
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Check the jacobians for the BlockAES match AES00 directly
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( jac.m00.m(), jac00.m() );
  BOOST_REQUIRE_EQUAL( jac.m00.n(), jac00.n() );

  BOOST_TEST_CHECKPOINT( "AES00 PDE_q" );
  checkSparseJacobianEquality(jac.m00(0,0), jac00(0,0));

  BOOST_TEST_CHECKPOINT( "AES0 0PDE_lg" );
  checkSparseJacobianEquality(jac.m00(0,1), jac00(0,1));

  BOOST_TEST_CHECKPOINT( "AES00 BC_q" );
  checkSparseJacobianEquality(jac.m00(1,0), jac00(1,0));

  BOOST_TEST_CHECKPOINT( "AES00 BC_lg" );
  checkSparseJacobianEquality(jac.m00(1,1), jac00(1,1));
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Check the jacobians for the BlockAES match AES11 directly
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( jac.m11.m(), jac11.m() );
  BOOST_REQUIRE_EQUAL( jac.m11.n(), jac11.n() );

  BOOST_TEST_CHECKPOINT( "AES11 PDE_q" );
  checkSparseJacobianEquality(jac.m11(0,0), jac11(0,0));

  BOOST_TEST_CHECKPOINT( "AES11 PDE_lg" );
  checkSparseJacobianEquality(jac.m11(0,1), jac11(0,1));

  BOOST_TEST_CHECKPOINT( "AES11 BC_q" );
  checkSparseJacobianEquality(jac.m11(1,0), jac11(1,0));

  BOOST_TEST_CHECKPOINT( "AES11 BC_lg" );
  checkSparseJacobianEquality(jac.m11(1,1), jac11(1,1));
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Check the residual evaluation functions
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  std::vector<std::vector<Real>> rsdNorm = BlockAES.residualNorm(rsd);
  std::vector<std::vector<Real>> rsdNorm0 = AES00.residualNorm(rsd.v0);
  std::vector<std::vector<Real>> rsdNorm1 = AES11.residualNorm(rsd.v1);

  BOOST_CHECK_CLOSE( rsdNorm[0][0], rsdNorm0[0][0], close_tol );
  BOOST_CHECK_CLOSE( rsdNorm[1][0], rsdNorm0[1][0], close_tol );
  BOOST_CHECK_CLOSE( rsdNorm[2][0], rsdNorm1[0][0], close_tol );
  BOOST_CHECK_CLOSE( rsdNorm[3][0], rsdNorm1[1][0], close_tol );

  BOOST_CHECK(BlockAES.convergedResidual(rsdNorm) == false);
  BOOST_CHECK(BlockAES.isValidStateSystemVector(q) == true);

  std::vector<std::vector<Real>> dummyrsdNorm = BlockAES.residualNorm(rsd);
  for (unsigned int i = 0; i < dummyrsdNorm.size(); i++)
    for (unsigned int j = 0; j < dummyrsdNorm[i].size(); j++)
        dummyrsdNorm[i][j] = 0;
  BOOST_CHECK(BlockAES.decreasedResidual(rsdNorm, dummyrsdNorm) == true);
  for (unsigned int i = 0; i < dummyrsdNorm.size(); i++)
    for (unsigned int j = 0; j < dummyrsdNorm[i].size(); j++)
        dummyrsdNorm[i][j] = 100;
  BOOST_CHECK(BlockAES.decreasedResidual(rsdNorm, dummyrsdNorm) == false);

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Discretization/AlgebraicEquationSet_Block2x2_Triangle_AD_pattern.txt", true );

  // Set some nice values for the pattern file
  rsdNorm[0][0] = 1;
  rsdNorm[1][0] = 2;
  rsdNorm[2][0] = 3;
  rsdNorm[3][0] = 4;

  BlockAES.printDecreaseResidualFailure(rsdNorm, output);
  BOOST_CHECK( output.match_pattern() );

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // jacobian transpose nonzero pattern
  BlockNonZeroPattern nzT(BlockAES.matrixSize());
  BlockAES.jacobianTranspose(q, nzT);

  // jacobian
  BlockMatrixClass jacT(nzT);
  jacT.m00 = 0;
  jacT.m01 = 0;
  jacT.m10 = 0;
  jacT.m11 = 0;
  BlockAES.jacobianTranspose(q, jacT);

  //Check if the jacobian matches the jacobian transpose
  checkJacobianTranspose(jac.m00, jacT.m00);
  checkJacobianTranspose(jac.m01, jacT.m10);
  checkJacobianTranspose(jac.m10, jacT.m01);
  checkJacobianTranspose(jac.m11, jacT.m11);


  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // That we get developer exception for parts I haven't implemented yet
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  BOOST_CHECK_THROW(BlockAES.indexPDE(), DeveloperException);
  BOOST_CHECK_THROW(BlockAES.indexQ(), DeveloperException);
  BOOST_CHECK_THROW(BlockAES.nResidNorm(), DeveloperException);
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __clang_analyzer__
  return; // clang analyzer complains about the hard coded (0,0) matrix access below
#endif

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Check the off-diagonal block jacobains for the BlockAES
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE(jac01.m() == 2);
  BOOST_REQUIRE(jac01.n() == 2);

  BOOST_TEST_CHECKPOINT( "AES00 PDE_q" );
  BOOST_CHECK(jac01(0,0).getNumNonZero() == 0);

  BOOST_TEST_CHECKPOINT( "JP01 PDE_lg" );
  BOOST_CHECK(jac01(0,1).getNumNonZero() == 0);

  BOOST_TEST_CHECKPOINT( "JP10 BC_q" );
  BOOST_CHECK(jac01(1,0).getNumNonZero() == 0);

  BOOST_TEST_CHECKPOINT( "AES11 BC_lg" );
  BOOST_CHECK(jac01(1,1).getNumNonZero() == 0);
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Check the off-diagonal block jacobains for the BlockAES
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE(jac10.m() == 2);
  BOOST_REQUIRE(jac10.n() == 2);

  BOOST_TEST_CHECKPOINT( "AES00 PDE_q" );
  BOOST_CHECK(jac10(0,0).getNumNonZero() == 0);

  BOOST_TEST_CHECKPOINT( "JP01 PDE_lg" );
  BOOST_CHECK(jac10(0,1).getNumNonZero() == 0);

  BOOST_TEST_CHECKPOINT( "JP10 BC_q" );
  BOOST_CHECK(jac10(1,0).getNumNonZero() == 0);

  BOOST_TEST_CHECKPOINT( "AES11 BC_lg" );
  BOOST_CHECK(jac10(1,1).getNumNonZero() == 0);
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
