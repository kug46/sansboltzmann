// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_Block2x2_DGBR2_Line_Euler1DSensor_btest
// testing AlgebraicEquationSet_DGBR2

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <fstream>
#include <string>

#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/BCEuler1D.h"
#include "pde/NS/SolutionFunction_Euler1D.h"
#include "pde/NS/Fluids1D_Sensor.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_BDF.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin_BDF.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin_BDF.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/JacobianParam.h"
#define JACOBIANPARAM_INSTANTIATE
#include "Discretization/JacobianParam_impl.h" // HACK: This should go away BEFORE being committed

#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h" // HACK: This should go away BEFORE being committed

#include "NonLinearSolver/NewtonSolver.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/HField/HFieldLine_DG.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "Meshing/XField1D/XField1D.h"

#include "unit/Discretization/jacobianPingTest_btest.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Block2x2_DGBR2_Line_Euler1DSensor_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseSystem )
{
//  typedef QTypeEntropy QType;
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> ArtificialViscosity;
  typedef PDENDConvertSpace<PhysD1, ArtificialViscosity> NDArtificialViscosity;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef NDArtificialViscosity::template MatrixParam<Real> PDEMatrixParam;

  typedef Fluids_Sensor<PhysD1, PDEClass> Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             TraitsSizeEuler<PhysD1>,
                             Sensor_AdvectiveFlux1D_None,
                             Sensor_ViscousFlux1D_Uniform,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;
  typedef SensorNDPDEClass::template MatrixQ<Real> SensorMatrixQ;
  typedef SensorNDPDEClass::template VectorArrayQ<Real> SensorVectorArrayQ;
  typedef SensorNDPDEClass::template MatrixParam<Real> SensorMatrixParam;

  typedef typename MakeTuple<FieldTuple, Field<PhysD1, TopoD1, Real>,
                                         Field<PhysD1, TopoD1, SensorArrayQ>,
                                         XField<PhysD1, TopoD1>>::type AVParamField;

  typedef SolutionFunction_Euler1D_Riemann<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolnNDConvertSpace<PhysD1, SolutionClass> SolutionNDClass;

  typedef BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDArtificialViscosity, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, AVParamField> PDEPrimalEquationSetClass;

  typedef PDEPrimalEquationSetClass::BCParams PDEBCParams;

  typedef PDEPrimalEquationSetClass::SystemMatrix SystemMatrixClass;

  typedef BCSensorParameter1DVector<Sensor_AdvectiveFlux1D_None, Sensor_ViscousFlux1D_None> SensorBCVector;

  typedef typename MakeTuple<FieldTuple, Field<PhysD1, TopoD1, Real>,
                                         Field<PhysD1, TopoD1, ArrayQ>,
                                         XField<PhysD1, TopoD1>>::type ParamField;
  typedef AlgebraicEquationSet_DGBR2<SensorNDPDEClass, BCNDConvertSpace, SensorBCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamField> SensorPrimalEquationSetClass;
  typedef SensorPrimalEquationSetClass::BCParams SensorBCParams;

  typedef SensorPrimalEquationSetClass::SystemMatrix SensorSystemMatrixClass;

  int order = 2;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler PDE mit AV
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);
  bool hasSpaceTimeDiffusion = false;
  NDArtificialViscosity avpde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                              gas, PDEClass::Euler_ResidInterp_Momentum);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict BCReflect;

  BCReflect[PDEBCParams::params.BC.BCType] = PDEBCParams::params.BC.Reflect_mitState;

  PyDict PyPDEBCList;
  PyPDEBCList["BCReflect"] = BCReflect;

  std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;
  // Define the BoundaryGroups for each boundary condition
  PDEBCBoundaryGroups["BCReflect"] = {0,1}; //left and right
  //No exceptions should be thrown
  PDEBCParams::checkInputs(PyPDEBCList);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And Initial conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  //  See: "Riemann Solvers and Numerical Methods for Fluid Dynamics" - Toro (Section 4.3.3 Table 4.1 in my copy)
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.interface] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.rhoL] = 1.0;
  solnArgs[SolutionClass::ParamsType::params.uL] = 0.01;
  solnArgs[SolutionClass::ParamsType::params.pL] = 1.0;
  solnArgs[SolutionClass::ParamsType::params.rhoR] = 0.125;
  solnArgs[SolutionClass::ParamsType::params.uR] = 0.01;
  solnArgs[SolutionClass::ParamsType::params.pR] = 0.1;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionNDClass solnExact(solnArgs);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our sensor parameter PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  Sensor sensor(avpde);
  Sensor_AdvectiveFlux1D_None sensor_adv;
  Sensor_ViscousFlux1D_Uniform sensor_visc(0.0);
  Source_JumpSensor sensor_source(order, sensor);
  SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict BCSensorRobin, PySensorBCList;
  BCSensorRobin[SensorBCParams::params.BC.BCType] = SensorBCParams::params.BC.LinearRobin_sansLG;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 0.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;
  PySensorBCList["LinearRobin_sansLG"] = BCSensorRobin;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["LinearRobin_sansLG"] = {0, 1}; // All sides
  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  int ii = 2; //grid size - using timesteps = grid size
  XField1D xfld( ii );
  HField_DG<PhysD1,TopoD1> hfld(xfld);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate our Euler fields
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> pde_qfld(xfld, order, BasisFunctionCategory_Legendre);
  pde_qfld = 0;
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> pde_rfld(xfld, order, BasisFunctionCategory_Legendre);
  pde_rfld = 0;
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ>
      pde_lgfld( xfld, order, BasisFunctionCategory_Legendre, PDEBCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups) );
  pde_lgfld = 0;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate our sensor parameter field
  ////////////////////////////////////////////////////////////////////////////////////////
  int sensororder = 0;
  Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, sensororder, BasisFunctionCategory_Legendre);
  FieldLift_DG_Cell<PhysD1, TopoD1, SensorVectorArrayQ> sensor_rfld(xfld, sensororder, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysD1, TopoD1, SensorArrayQ> sensor_lgfld(xfld, sensororder-1, BasisFunctionCategory_Legendre,
                                                                    SensorBCParams::getLGBoundaryGroups(PySensorBCList, BCBoundaryGroups));
  sensorfld = 0.7;
  sensor_rfld = 0;
  sensor_lgfld = 0;

  // Initial condition
  for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our AES to solve our PDE mit AV
  ////////////////////////////////////////////////////////////////////////////////////////
  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-12, 1e-12};
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  AVParamField avparamfld = (hfld, sensorfld, xfld);
  PDEPrimalEquationSetClass PDEPrimalEqSet(avparamfld, pde_qfld, pde_rfld, pde_lgfld, avpde,
                                             disc, quadratureOrder, ResidualNorm_Default, tol,
                                             {0}, {0}, PyPDEBCList, PDEBCBoundaryGroups);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our AES to solve the sensor PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  ParamField paramfield = (hfld, pde_qfld, xfld);
  SensorPrimalEquationSetClass SensorPrimalEqSet(paramfield, sensorfld, sensor_rfld, sensor_lgfld, sensorpde,
                                                 disc, quadratureOrder, ResidualNorm_Default, tol,
                                                 {0}, {0}, PySensorBCList, BCBoundaryGroups);


  ////////////////////////////////////////////////////////////////////////////////////////
  // Generate off diagonal AES
  ////////////////////////////////////////////////////////////////////////////////////////
  const int iPDE_SensorParam = 1; // Sensor parameter in PDE index
  const int iSensor_PDEParam = 1; // PDE solution parameter in sensor index

  typedef JacobianParam<boost::mpl::vector1_c<int,iPDE_SensorParam>,PDEPrimalEquationSetClass> JacobianParam_PDE;
  typedef JacobianParam<boost::mpl::vector1_c<int,iSensor_PDEParam>,SensorPrimalEquationSetClass> JacobianParam_Sensor;

  typedef JacobianParam_PDE::SystemMatrix    SystemMatrix01;
  typedef JacobianParam_Sensor::SystemMatrix SystemMatrix10;

  std::map<int,int> PDEMap;
  PDEMap[iPDE_SensorParam] = PDEPrimalEqSet.iq; //The parameter in the PDE

  std::map<int,int> SensorMap;
  SensorMap[iSensor_PDEParam] = SensorPrimalEqSet.iq;

  JacobianParam_PDE JP01(PDEPrimalEqSet, PDEMap);
  JacobianParam_Sensor JP10(SensorPrimalEqSet, SensorMap);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Generate 2x2 Block system
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass, SystemMatrix01,
                                        SystemMatrix10, SensorSystemMatrixClass> AlgEqSetType_2x2;

  AlgEqSetType_2x2 BlockAES(PDEPrimalEqSet, JP01,
                            JP10, SensorPrimalEqSet);

  typedef AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
  typedef AlgEqSetType_2x2::SystemVector BlockVectorClass;
  typedef AlgEqSetType_2x2::SystemNonZeroPattern BlockNonZeroPattern;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial guess
  ////////////////////////////////////////////////////////////////////////////////////////
  BlockVectorClass q(BlockAES.vectorStateSize());
  BlockVectorClass sln(BlockAES.vectorStateSize());
  for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );
  BlockAES.fillSystemVector(q);
  sln = q;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Residual
  ////////////////////////////////////////////////////////////////////////////////////////
  BlockVectorClass rsd(BlockAES.vectorEqSize());
  BlockVectorClass rsdp(BlockAES.vectorEqSize());
  BlockVectorClass rsdm(BlockAES.vectorEqSize());
  rsd = 0;
  rsdp = 0;
  rsdm = 0;
  BlockAES.residual(q, rsd);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Jacobian
  ////////////////////////////////////////////////////////////////////////////////////////
  BlockNonZeroPattern nz(BlockAES.matrixSize());
  BlockAES.jacobian(q, nz);
  BlockMatrixClass jac(nz);
  jac = 0;
  BlockAES.jacobian(q, jac);

  BOOST_REQUIRE_EQUAL(jac.m(), 2);
  BOOST_REQUIRE_EQUAL(jac.n(), 2);

  BOOST_REQUIRE_EQUAL(jac.m00.m(), 2);
  BOOST_REQUIRE_EQUAL(jac.m00.n(), 2);

  BOOST_REQUIRE_EQUAL(jac.m01.m(), 2);
  BOOST_REQUIRE_EQUAL(jac.m01.n(), 2);

  BOOST_REQUIRE_EQUAL(jac.m10.m(), 2);
  BOOST_REQUIRE_EQUAL(jac.m10.n(), 2);

  BOOST_REQUIRE_EQUAL(jac.m11.m(), 2);
  BOOST_REQUIRE_EQUAL(jac.m11.n(), 2);
#if __clang_analyzer__
  return; // clang things we are accessing zero size memory below... sigh...
#endif

#ifdef DISPLAY_FOR_DEBUGGING
  bool verbose = true;
#else
  bool verbose = false;
#endif

  {
    const ArrayQ qArrayQscale = {1,1,1};

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(0,0) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ>(jac.m00, rsdp, rsdm, rsdp.v0, rsdm.v0, q, q.v0, BlockAES,
                            {1e-3, 1e-4}, qArrayQscale, {1.9, 2.1}, verbose);

#ifdef DISPLAY_FOR_DEBUGGING
  std::cout << std::endl << "========== Testing jac_block = jac(1,0) ==========" << std::endl << std::endl;
#endif
  detail_jacobianPingTest::jacobianPingTest::
  ping_block<SensorMatrixParam>(jac.m10, rsdp, rsdm, rsdp.v1, rsdm.v1, q, q.v0, BlockAES,
                                    {1e-5, 1e-6}, qArrayQscale, {1.9, 2.1}, verbose);
  }

  {
    const SensorArrayQ qArrayQscale = {1};

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(0,1) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<PDEMatrixParam>(jac.m01, rsdp, rsdm, rsdp.v0, rsdm.v0, q, q.v1, BlockAES,
                                   {1e-3, 1e-4}, qArrayQscale, {1.9, 2.1}, verbose);

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(1,1) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<SensorMatrixQ>(jac.m11, rsdp, rsdm, rsdp.v1, rsdm.v1, q, q.v1, BlockAES,
                                  {1e-3, 1e-4}, qArrayQscale, {1.9, 2.1}, verbose);
  }


}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
