// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_Block2x2_DGBR2_Triangle_BuckleyLeverett_AV_btest
// testing AlgebraicEquationSet_Block2x2

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <string>
#include <fstream>

#include <boost/mpl/vector_c.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "tools/output_std_vector.h"

#include "Surreal/SurrealS.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"
#include "pde/PorousMedia/Sensor_BuckleyLeverett.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/JacobianParam.h"
#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/HField/GenHFieldArea_CG.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Block2x2_DGBR2_Triangle_BuckleyLeverett_AV_btest )


template<class SparseMatrixClass>
void checkJacobianTranspose(const DLA::MatrixD<SparseMatrixClass>& jac, const DLA::MatrixD<SparseMatrixClass>& jacT )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  //Check that jacT is a transpose of jac
  for ( int i = 0; i < jac.m(); i++ )
    for ( int j = 0; j < jac.n(); j++ )
    {
      const SparseMatrixClass& jacij = jac(i,j);
      const SparseMatrixClass& jacTji = jacT(j,i);

      BOOST_REQUIRE_EQUAL(jacij.m(), jacTji.n());
      BOOST_REQUIRE_EQUAL(jacij.n(), jacTji.m());

      for ( int ib = 0; ib < jacij.m(); ib++ )
        for ( int jb = 0; jb < jacij.n(); jb++ )
        {
          //Only compare non-zero entries
          if ( !jacij.isNonZero(ib,jb) )
          {
            //Make sure the transpose is also a zero entry
            BOOST_CHECK( !jacTji.isNonZero(jb,ib) );
            continue;
          }
          //Check that the numbers are the same
          SANS_CHECK_CLOSE( jacij(ib,jb), jacTji(jb,ib), small_tol, close_tol );
        }
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseSystem_test )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;

  typedef QTypePrimitive_Sw QType;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef TraitsModelBuckleyLeverett<QType, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass_noAV;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_noAV> NDPDEClass_noAV;

  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass_Primal;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_Primal> NDPDEClass_Primal;

  typedef NDPDEClass_Primal::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass_Primal::template VectorArrayQ<Real> VectorArrayQ;

  typedef Sensor_BuckleyLeverett<PDEClass_noAV> Sensor;
  typedef Sensor_AdvectiveFlux1D_Uniform Sensor_Advection;
  typedef Sensor_ViscousFlux1D_GenHScale Sensor_Diffusion;
  typedef Source1D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             SensorParameterTraits<PhysD1>,
                             Sensor_Advection,
                             Sensor_Diffusion,
                             Source_JumpSensor > PDEClass_Sensor;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_Sensor> NDPDEClass_Sensor;

  typedef NDPDEClass_Sensor::template ArrayQ<Real> SensorArrayQ;
  typedef NDPDEClass_Sensor::template VectorArrayQ<Real> SensorVectorArrayQ;

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QType, PDEClass_noAV> ExactSolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef BCBuckleyLeverett_ArtificialViscosity1DVector<TraitsSizeBuckleyLeverett, TraitsModelClass> BCVectorPrimal;
  typedef BCParameters<BCVectorPrimal> BCParamsPrimal;

  typedef BCSensorParameter1DVector<Sensor_Advection, Sensor_Diffusion> BCVectorSensor;
  typedef BCParameters<BCVectorSensor> BCParamsSensor;

  typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         Field<PhysDim, TopoDim, SensorArrayQ>,
                                         XField<PhysDim, TopoDim>>::type ParamFieldPrimal;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         Field<PhysDim, TopoDim, ArrayQ>,
                                         XField<PhysDim, TopoDim>>::type ParamFieldSensor;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Primal, BCNDConvertSpaceTime, BCVectorPrimal,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldPrimal> PrimalEquationSetClass;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Sensor, BCNDConvertSpaceTime, BCVectorSensor,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldSensor> SensorEquationSetClass;

  int order_primal = 1;
  int order_sensor = 1;

  // Primal PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 0.0;
  CapillaryModel cap_model(pc_max);

  NDPDEClass_noAV pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  //Create artificial viscosity PDE
  const bool hasSpaceTimeDiffusion = false;
  NDPDEClass_Primal pde_primal(order_primal, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 0.9;
  const Real SwR = 0.1;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(pde, SwL, SwR, xinit);


  //Sensor PDE
  Sensor sensor(pde_primal);

  Sensor_Advection sensor_adv(0.0);
  Real Cdiff = 3.0;
  Sensor_Diffusion sensor_visc(Cdiff);
  Source_JumpSensor sensor_source(order_primal, sensor);

  NDPDEClass_Sensor pde_sensor(sensor_adv, sensor_visc, sensor_source);


  // Primal PDE BCs
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitState, BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>> BCParamsAV;

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.None;

  PyDict BCDirichletL;
  BCDirichletL[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.Dirichlet_mitState;
  BCDirichletL[BCParamsAV::params.qB] = SwL;
  BCDirichletL[BCParamsAV::params.Cdiff] = Cdiff;

  PyDict BCDirichletR;
  BCDirichletR[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.Dirichlet_mitState;
  BCDirichletR[BCParamsAV::params.qB] = SwR;
  BCDirichletR[BCParamsAV::params.Cdiff] = Cdiff;

  PyDict PyBCList_Primal;
  PyBCList_Primal["DirichletL"] = BCDirichletL;
  PyBCList_Primal["DirichletR"] = BCDirichletR;
  PyBCList_Primal["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Primal;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_Primal["DirichletL"] = {3}; //Left boundary
  BCBoundaryGroups_Primal["DirichletR"] = {0,1}; //Bottom and right boundary
  BCBoundaryGroups_Primal["None"] = {2}; //Top boundary

  //Check the BC dictionary
  BCParamsPrimal::checkInputs(PyBCList_Primal);

  std::vector<int> active_boundaries_primal = BCParamsPrimal::getLGBoundaryGroups(PyBCList_Primal, BCBoundaryGroups_Primal);

  // Sensor BCs
  PyDict BCSensorRobin;
  BCSensorRobin[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 10.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorNone;
  BCSensorNone[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.None;

  PyDict PyBCList_Sensor;
  PyBCList_Sensor["SensorRobin"] = BCSensorRobin;
//  PyBCList_Sensor["SensorNone"] = BCSensorNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Sensor;
  BCBoundaryGroups_Sensor["SensorRobin"] = {0, 1, 2, 3}; // Bottom, right, left
//  BCBoundaryGroups_Sensor["SensorNone"] = {2}; // Top

  //Check the BC dictionary
  BCParamsSensor::checkInputs(PyBCList_Sensor);

  std::vector<int> active_boundaries_sensor = BCParamsSensor::getLGBoundaryGroups(PyBCList_Sensor, BCBoundaryGroups_Sensor);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  std::vector<Real> tol = {1e-11, 1e-11};

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////

  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 6.0, 0, 3.0, true );

  GenHField_CG<PhysDim, TopoDim> hfld(xfld);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate primal solution fields
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> pde_qfld(xfld, order_primal, BasisFunctionCategory_Legendre);
  FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ> pde_rfld(xfld, order_primal, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> pde_lgfld( xfld, order_primal, BasisFunctionCategory_Legendre, active_boundaries_primal );
  pde_qfld = 0.2;
  pde_rfld = 0;
  pde_lgfld = 0;

  //Perturb field so that there are small jumps
  for (int i = 0; i < pde_qfld.nDOF(); i++)
  {
    Real eps = (2*(Real)rand()/RAND_MAX - 1);
    pde_qfld.DOF(i) += 1e-3*eps;
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate sensor solution fields
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysDim, TopoDim, SensorArrayQ> sensorfld(xfld, order_sensor, BasisFunctionCategory_Legendre);
  FieldLift_DG_Cell<PhysDim, TopoDim, SensorVectorArrayQ> sensor_rfld(xfld, order_sensor, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysDim, TopoDim, SensorArrayQ> sensor_lgfld(xfld, order_sensor, BasisFunctionCategory_Legendre, active_boundaries_sensor);
  sensorfld = 0.1;
  sensor_rfld = 0;
  sensor_lgfld = 0;

  //Perturb field so that there are small jumps
  for (int i = 0; i < sensorfld.nDOF(); i++)
  {
    Real eps = (2*(Real)rand()/RAND_MAX - 1);
    sensorfld.DOF(i) += 1e-3*eps;
  }

  QuadratureOrder quadratureOrder( xfld, - 1 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our AES to solve our PDE mit AV
  ////////////////////////////////////////////////////////////////////////////////////////

  ParamFieldPrimal paramfld_primal = (hfld, sensorfld, xfld);
  PrimalEquationSetClass AES00(paramfld_primal, pde_qfld, pde_rfld, pde_lgfld, pde_primal,
                               disc, quadratureOrder, ResidualNorm_Default, tol,
                               {0}, {0,1,2}, PyBCList_Primal, BCBoundaryGroups_Primal);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our AES to solve the sensor PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  ParamFieldSensor paramfield_sensor = (hfld, pde_qfld, xfld);
  SensorEquationSetClass AES11(paramfield_sensor, sensorfld, sensor_rfld, sensor_lgfld, pde_sensor,
                               disc, quadratureOrder, ResidualNorm_Default, tol,
                               {0}, {0,1,2}, PyBCList_Sensor, BCBoundaryGroups_Sensor);

  //Initialize the sensor field to be consistent with the initial qfld
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass00;
  typedef SensorEquationSetClass::SystemMatrix SystemMatrixClass11;


  ////////////////////////////////////////////////////////////////////////////////////////
  // Generate off diagonal AES
  ////////////////////////////////////////////////////////////////////////////////////////

  const int iPDE_SensorParam = 1; // Sensor parameter in PDE index
  const int iSensor_PDEParam = 1; // PDE solution parameter in sensor index

  typedef JacobianParam<boost::mpl::vector1_c<int,iPDE_SensorParam>,PrimalEquationSetClass> JacobianParam_PDE;
  typedef JacobianParam<boost::mpl::vector1_c<int,iSensor_PDEParam>,SensorEquationSetClass> JacobianParam_Sensor;

  typedef JacobianParam_PDE::SystemMatrix    SystemMatrixClass01;
  typedef JacobianParam_Sensor::SystemMatrix SystemMatrixClass10;

  std::map<int,int> PDEMap;
  PDEMap[iPDE_SensorParam] = AES11.iq; //The parameter in the PDE

  std::map<int,int> SensorMap;
  SensorMap[iSensor_PDEParam] = AES00.iq;

  JacobianParam_PDE JP01(AES00, PDEMap);
  JacobianParam_Sensor JP10(AES11, SensorMap);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Generate 2x2 Block system
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass00, SystemMatrixClass01,
                                        SystemMatrixClass10, SystemMatrixClass11> AlgEqSetType_2x2;

  AlgEqSetType_2x2 BlockAES(AES00, JP01,
                            JP10, AES11);

  typedef AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
  typedef AlgEqSetType_2x2::SystemVector BlockVectorClass;
  typedef AlgEqSetType_2x2::SystemNonZeroPattern BlockNonZeroPattern;

  BlockVectorClass q(BlockAES.vectorStateSize());
  q = 0;
  BlockAES.fillSystemVector(q);

  // jacobian nonzero pattern
  BlockNonZeroPattern nz(BlockAES.matrixSize());
  BlockAES.jacobian(q, nz);

  // jacobian
  BlockMatrixClass jac(nz);
  jac.m00 = 0;
  jac.m01 = 0;
  jac.m10 = 0;
  jac.m11 = 0;
  BlockAES.jacobian(q, jac);

  // jacobian transpose nonzero pattern
  BlockNonZeroPattern nzT(BlockAES.matrixSize());
  BlockAES.jacobianTranspose(q, nzT);

  // jacobian
  BlockMatrixClass jacT(nzT);
  jacT.m00 = 0;
  jacT.m01 = 0;
  jacT.m10 = 0;
  jacT.m11 = 0;
  BlockAES.jacobianTranspose(q, jacT);

  //Check if the jacobian matches the jacobian transpose
  checkJacobianTranspose(jac.m00, jacT.m00);
  checkJacobianTranspose(jac.m01, jacT.m10);
  checkJacobianTranspose(jac.m10, jacT.m01);
  checkJacobianTranspose(jac.m11, jacT.m11);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
