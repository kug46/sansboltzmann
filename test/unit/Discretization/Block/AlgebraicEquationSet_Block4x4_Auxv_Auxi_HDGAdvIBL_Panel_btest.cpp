// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
//#define IBL2D_DUMP
//#define DISPLAY_FOR_DEBUGGING
//#define ISTIMINGBlock4x4
//#define IS_FULLTEST_LOSESPEEDUP // turn on to run full test. Turn off for speedup on Jenkins

#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"
#include "SANS_btest.h"

#define JACOBIANPARAM_INSTANTIATE
#include "Discretization/JacobianParam_impl.h"

#define ALGEBRAICEQUATIONSET_BLOCK4X4_INSTANTIATE // HACK : need proper instantiations
#include "Discretization/Block/AlgebraicEquationSet_Block4x4_impl.h"

#include "Discretization/Block/JacobianParam_Decoupled.h"

#define ALGEBRAICEQUATIONSET_HDGADVECTIVE_INSTANTIATE
#include "Discretization/HDG/AlgebraicEquationSet_HDGAdvective_impl.h"

#include "Discretization/HDG/AlgebraicEquationSet_Hubtrace_HDGAdvective.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_DG_HubTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#define SCALARVECTOR_INSTANTIATE
#include "LinearAlgebra/SparseLinAlg/ScalarVector_impl.h"

#include "LinearAlgebra/SparseLinAlg/ScalarMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/WritePlainVector.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_4x4.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_4.h"

#include "PanelMethod/AlgebraicEquationSet_ProjectToQauxi.h"
#include "PanelMethod/JacobianParam_PanelEqn_Qauxi.h"
#include "PanelMethod/ProjectionToQauxi_IBL.h"

#include "PanelMethod/XfoilPanel.h"
#include "PanelMethod/XfoilPanelEquationSet.h"

//#include "pde/IBL/PDEIBL2D.h"
#include "pde/IBL/PDEIBL2D_impl.h"
#include "pde/IBL/BCIBL2D.h"
#include "pde/IBL/SetSolnDofCell_IBL2D.h"
#include "pde/IBL/SetVelDofCell_IBL2D.h"

#include "pde/IBL/AlgebraicEquationSet_ProjectToQauxv_PanelMethod.h"
#include "pde/IBL/JacobianParam_ProjectToQauxv_Qinv.h"
#include "pde/IBL/JacobianParam_ProjectToQauxi_QIBL.h"
#include "pde/IBL/JacobianParam_ProjectToQauxi_Qauxv.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#ifdef ISTIMINGBlock4x4
#include "tools/timer.h"
#endif

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_X1_2Group_AirfoilWithWake.h"

#include "unit/Discretization/jacobianPingTest_btest.h"

using namespace std;
using namespace SANS;
using namespace SANS::BLA;
using namespace SANS::DLA;
using namespace SANS::SLA;

//----------------------------------------------------------------------------//
// Utility stuff
//
namespace SANS
{
typedef PhysD2 PhysDim;
typedef TopoD1 TopoDim;

typedef AlgEqSetTraits_Sparse AESTraitsTag;

/////////////////////////////////////////////////////////////////////
// Panel method
typedef XfoilPanel PanelMethodType;
typedef XfoilPanelEquationSet<PanelMethodType,AESTraitsTag> PanelMethodEquationType;

typedef typename PanelMethodEquationType::GamFieldType<Real> GamFieldType;
typedef typename PanelMethodEquationType::LamFieldType<Real> LamFieldType;

/////////////////////////////////////////////////////////////////////
// IBL
typedef VarTypeDANCt VarType;
typedef VarData2DDANCt VarDataType;

typedef PDEIBL<PhysDim,VarType> PDEClassIBL;
typedef PDENDConvertSpace<PhysDim, PDEClassIBL> NDPDEClassIBL;

typedef BCIBL2DVector_AirfoilAndWake<VarType> BCVectorClass;

typedef NDPDEClassIBL::template ArrayQ<Real> ArrayQIBL;
typedef NDPDEClassIBL::template ArrayParam<Real> ArrayQauxv;
typedef NDPDEClassIBL::VectorX VectorX;

typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQauxv>,
                                       XField<PhysDim, TopoDim> >::type TupleFieldIBLParamType;

typedef AlgebraicEquationSet_HubTrace_HDGAdvective<
          AlgebraicEquationSet_HDGAdvective<NDPDEClassIBL,BCNDConvertSpace,BCVectorClass,
                                           AESTraitsTag,HDGAdv_manifold,TupleFieldIBLParamType> > IBLEquationType;

typedef IBLEquationType::BCParams BCParamsIBL;

/////////////////////////////////////////////////////////////////////
// Auxiliary equations
typedef FunctionEvalQauxv<PanelMethodType,Real,Real> FcnEvalQauxvType;
typedef IntegrandCell_ProjectFunction<FcnEvalQauxvType,IntegrandCell_ProjFcn_detail::FcnSref> IntegrandCellAuxvType;
typedef AlgebraicEquationSet_ProjectToQauxv_PanelMethod<
          XField<PhysDim,TopoDim>, IntegrandCellAuxvType, TopoDim, AESTraitsTag, PanelMethodEquationType> AuxvEquationType;

// TODO: note that the ordering of QIBL and Qauxv need to be consistent with that defined in ProjectionToQauxi
typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQIBL>,
                                       Field<PhysDim, TopoDim, ArrayQauxv>,
                                       XField<PhysDim, TopoDim> >::type TupleFieldAuxiParamType;

typedef typename AESProjToQauxi_detail::IntegrandCellProjNull IntegrandAuxiNullCellType;
typedef AlgebraicEquationSet_ProjectToQauxi<TupleFieldAuxiParamType, IntegrandAuxiNullCellType, TopoDim, AESTraitsTag,
                                            IBLEquationType, AuxvEquationType> AuxiEquationType;

/////////////////////////////////////////////////////////////////////
// Block 4x4
// diagonal block equation sets
typedef AuxvEquationType::TraitsType TraitsTypeBlock0;
typedef AuxiEquationType::TraitsType TraitsTypeBlock1;
typedef IBLEquationType::TraitsType TraitsTypeBlock2;
typedef PanelMethodEquationType::TraitsType TraitsTypeBlock3;

// primary unknown size
static const int MQ0 = VectorSize<typename TraitsTypeBlock0::ArrayQ>::M;
static const int MQ1 = VectorSize<typename TraitsTypeBlock1::ArrayQ>::M;
static const int MQ2 = VectorSize<typename TraitsTypeBlock2::ArrayQ>::M;
static const int MQ3 = VectorSize<typename TraitsTypeBlock3::ArrayQ>::M;

typedef typename TraitsTypeBlock0::MatrixQ        MatrixQ00;
typedef typename MatrixS_or_T<MQ0,MQ1,Real>::type MatrixQ01;
typedef typename MatrixS_or_T<MQ0,MQ2,Real>::type MatrixQ02;
typedef typename MatrixS_or_T<MQ0,MQ3,Real>::type MatrixQ03;

typedef typename MatrixS_or_T<MQ1,MQ0,Real>::type MatrixQ10;
typedef typename TraitsTypeBlock1::MatrixQ        MatrixQ11;
typedef typename MatrixS_or_T<MQ1,MQ2,Real>::type MatrixQ12;
typedef typename MatrixS_or_T<MQ1,MQ3,Real>::type MatrixQ13;

typedef typename MatrixS_or_T<MQ2,MQ0,Real>::type MatrixQ20;
typedef typename MatrixS_or_T<MQ2,MQ1,Real>::type MatrixQ21;
typedef typename TraitsTypeBlock2::MatrixQ        MatrixQ22;
typedef typename MatrixS_or_T<MQ2,MQ3,Real>::type MatrixQ23;

typedef typename MatrixS_or_T<MQ3,MQ0,Real>::type MatrixQ30;
typedef typename MatrixS_or_T<MQ3,MQ1,Real>::type MatrixQ31;
typedef typename MatrixS_or_T<MQ3,MQ2,Real>::type MatrixQ32;
typedef typename TraitsTypeBlock3::MatrixQ        MatrixQ33;

// block system matrices
typedef TraitsTypeBlock0::SystemMatrix        BlockSM00;
typedef MatrixD<SparseMatrix_CRS<MatrixQ01> > BlockSM01;
typedef MatrixD<SparseMatrix_CRS<MatrixQ02> > BlockSM02;
typedef MatrixD<SparseMatrix_CRS<MatrixQ03> > BlockSM03;

typedef MatrixD<SparseMatrix_CRS<MatrixQ10> > BlockSM10;
typedef TraitsTypeBlock1::SystemMatrix        BlockSM11;
typedef MatrixD<SparseMatrix_CRS<MatrixQ12> > BlockSM12;
typedef MatrixD<SparseMatrix_CRS<MatrixQ13> > BlockSM13;

typedef MatrixD<SparseMatrix_CRS<MatrixQ20> > BlockSM20;
typedef MatrixD<SparseMatrix_CRS<MatrixQ21> > BlockSM21;
typedef TraitsTypeBlock2::SystemMatrix        BlockSM22;
typedef MatrixD<SparseMatrix_CRS<MatrixQ23> > BlockSM23;

typedef MatrixD<SparseMatrix_CRS<MatrixQ30> > BlockSM30;
typedef MatrixD<SparseMatrix_CRS<MatrixQ31> > BlockSM31;
typedef MatrixD<SparseMatrix_CRS<MatrixQ32> > BlockSM32;
typedef TraitsTypeBlock3::SystemMatrix        BlockSM33;

typedef AlgebraicEquationSet_Block4x4<BlockSM00, BlockSM01, BlockSM02, BlockSM03,
                                      BlockSM10, BlockSM11, BlockSM12, BlockSM13,
                                      BlockSM20, BlockSM21, BlockSM22, BlockSM23,
                                      BlockSM30, BlockSM31, BlockSM32, BlockSM33,
                                      AlgebraicEquationSetBase> Block4x4AlgebraicEquationSetType;

typedef Block4x4AlgebraicEquationSetType::SystemMatrix BlockSystemMatrixType;
typedef Block4x4AlgebraicEquationSetType::SystemNonZeroPattern BlockSystemMatrixNZType;
typedef Block4x4AlgebraicEquationSetType::SystemVector BlockSystemVectorType;

/////////////////////////////////////////////////////////////////////
// Coupling jacobians
typedef JacobianParam_Decoupled<BlockSM01,TraitsTypeBlock0> DecoupledJacobianType01;
typedef JacobianParam_Decoupled<BlockSM02,TraitsTypeBlock0> DecoupledJacobianType02;
typedef JacobianParam_ProjectToQauxv_Qinv<AuxvEquationType> CouplingJacobianType03;

typedef JacobianParam_ProjectToQauxi_Qauxv<AuxiEquationType> CouplingJacobianType10;
typedef JacobianParam_ProjectToQauxi_QIBL<AuxiEquationType> CouplingJacobianType12;
typedef JacobianParam_Decoupled<BlockSM13,TraitsTypeBlock1> DecoupledJacobianType13;

typedef JacobianParam<boost::mpl::vector1_c<int,0>,IBLEquationType> CouplingJacobianType20_IBLeqn_Qauxv;
typedef JacobianParam_Decoupled<BlockSM21,TraitsTypeBlock2> DecoupledJacobianType21;
typedef JacobianParam_Decoupled<BlockSM23,TraitsTypeBlock2> DecoupledJacobianType23;

typedef JacobianParam_Decoupled<BlockSM30,TraitsTypeBlock3> DecoupledJacobianType30;
typedef JacobianParam_PanelEqn_Qauxi<PanelMethodEquationType> CouplingJacobianType31;
typedef JacobianParam_Decoupled<BlockSM32,TraitsTypeBlock3> DecoupledJacobianType32;

/////////////////////////////////////////////////////////////////////

} // namespace SANS

//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Block4x4_Coupled_HDGAdvIBL_Panel )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_Block4x4_Coupled_HDGAdvIBL_Panel_test )
{
#ifdef ISTIMINGBlock4x4
  timer totaltime; // start timing the whole unit test
#endif

  /////////////////////////////////////////////////////////////////////
  // ---------- Set problem parameters ---------- //
  const int order_soln = 1; // solution order
  const int order_param_grid = 1;  // order: parameter field and grid

  const int alpha = 0; // [radian] angle of attack
  const Real qinf = 1.573869150000000; // [m/s] freestream speed
  const Real p0 = 1.e+5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  // ---------- Set up IBL ---------- //
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelIBL gasModel(gasModelDict);

  const Real mue = 1.827e-5;
  typename PDEClassIBL::ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict_wall;
  transitionModelDict_wall[TransitionModelParams::params.ntinit] = 0.0;
  transitionModelDict_wall[TransitionModelParams::params.ntcrit] = 9.0;
  transitionModelDict_wall[TransitionModelParams::params.xtr] = 10.0;
  transitionModelDict_wall[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict_wall[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict_wall); // necessary for checking input parameter validity
  const typename PDEClassIBL::TransitionModelType transitionModel_wall(transitionModelDict_wall);

  PyDict transitionModelDict_wake(transitionModelDict_wall);
#if 0 // laminar wake
  transitionModelDict_wake[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict_wake[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarWake;
#else // turbulent wake
  transitionModelDict_wake[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict_wake[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.turbulentWake;
#endif
  TransitionModelParams::checkInputs(transitionModelDict_wake); // necessary for checking input parameter validity
  const typename PDEClassIBL::TransitionModelType transitionModel_wake(transitionModelDict_wake);

  typedef typename PDEClassIBL::ThicknessesCoefficientsType ThicknessesCoefficientsType;
  typedef typename ThicknessesCoefficientsType::ClassParamsType ThicknessesCoefficientsParamsType;

  PyDict tcVarDict;
  tcVarDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
            = ThicknessesCoefficientsParamsType::params.nameCDclosure.ibl3eqm;

  // PDE
  NDPDEClassIBL pdeIBLWall(gasModel, viscosityModel, transitionModel_wall, tcVarDict);
  NDPDEClassIBL pdeIBLWake(gasModel, viscosityModel, transitionModel_wake, tcVarDict);

  // BC
  // Create a BC dictionaries
  PyDict BCNoneTE;
  BCNoneTE[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.Matching;
  BCNoneTE[BCIBL2DParams<BCTypeWakeMatch>::params.matchingType] =
      BCIBL2DParams<BCTypeWakeMatch>::params.matchingType.trailingEdge;

  PyDict BCWakeIn;
  BCWakeIn[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.Matching;
  BCWakeIn[BCIBL2DParams<BCTypeWakeMatch>::params.matchingType] =
      BCIBL2DParams<BCTypeWakeMatch>::params.matchingType.wakeInflow;

  PyDict BCOutArgs;
  BCOutArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.None;

  PyDict PyBCList_a;
  PyBCList_a["_BCNoneTE_"] = BCNoneTE;

  PyDict PyBCList_w;
  PyBCList_w["_BCWakeIn_"] = BCWakeIn;
  PyBCList_w["BCOut"] = BCOutArgs;

  // No exceptions should be thrown
  BCParamsIBL::checkInputs(PyBCList_a);
  BCParamsIBL::checkInputs(PyBCList_w);

  std::map<std::string, std::vector<int>> BCBoundaryGroups_a;
  std::map<std::string, std::vector<int>> BCBoundaryGroups_w;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_a["_BCNoneTE_"] = {0};
  BCBoundaryGroups_w["_BCWakeIn_"] = {1};
  BCBoundaryGroups_w["BCOut"] = {2};

  // ---------- Set up grid ---------- //
  std::vector<VectorX> coordinates_a = { {1.0,  0.3},
                                         {0.0,  0.2},
                                         {1.0, -0.5} };
  std::vector<VectorX> coordinates_w = { {1.01, 0.0},
                                         {1.2,    0.2},
                                         {1.5,    0.0} };
  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);

  // ---------- Set up panel ---------- //
  const VectorX Vinf = { qinf*cos(static_cast<Real>(alpha)*PI/180.0),
                         qinf*sin(static_cast<Real>(alpha)*PI/180.0) }; // freestream velocity

  const std::vector<int> panelCellGroups = {0,1};

  const std::vector<int> cellGroup_a = {0};
  const std::vector<int> cellGroup_w = {1};

  const std::vector<int> interiorTraceGroups_a = {0};
  const std::vector<int> interiorTraceGroups_w = {1};

  const std::vector<int> boundaryTraceGroups_a = {0};
  const std::vector<int> boundaryTraceGroups_w = {1,2};

  XfoilPanel panel(Vinf,p0,T0,xfld,panelCellGroups);

  const int nelem_a = panel.nPanelAirfoil();
  const int nelem_w = panel.nPanelWake();

  const int nnode_a = panel.nNodeAirfoil();
  const int nnode_w = panel.nNodeWake();

  const int nDOFgam = nnode_a;
  const int nDOFlam = nelem_a + nelem_w;

  // ---------- Initialize solution/parameter fields ---------- //
  // parameter field: velocity and gradients
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velxXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velzXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  std::vector<Real> dataVel_a = {-1, -0.4, 0.9};
  std::vector<Real> dataVel_w = {1.2, 1.3, 1.9};
  for_each_CellGroup<TopoDim>::apply( SetVelocityDofCell_IBL2D(dataVel_a, cellGroup_a), (velfld, velxXfld, velzXfld, xfld) );
  for_each_CellGroup<TopoDim>::apply( SetVelocityDofCell_IBL2D(dataVel_w, cellGroup_w), (velfld, velxXfld, velzXfld, xfld) );

#if 1
  // manually make sure that velocity magnitude are genuinely discontinuous. Otherwise finite differencing used in jacobian ping test
  // will be problematic in the presence of possibly discontinuous residual caused by max() function in DG numerical flux
  velfld.DOF(1) = 1.2*velfld.DOF(1);

  velfld.DOF(5) = 0.5*velfld.DOF(5);
#endif

  BOOST_REQUIRE_EQUAL( xfld.nElem(), velfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velxXfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velzXfld.nElem() );

  // parameter field
  Field_DG_Cell<PhysDim, TopoDim, ArrayQauxv> qauxvfld(xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( xfld.nElem(), qauxvfld.nElem() );

  const auto paramInterpret = pdeIBLWall.getParamInterpreter();
  for (int i = 0; i < qauxvfld.nDOF(); ++i)
  {
    const typename PDEClassIBL::ParamInterpType::TensorX<Real> gradq1
      = { {velxXfld.DOF(i)[0], velxXfld.DOF(i)[1]},
          {velzXfld.DOF(i)[0], velzXfld.DOF(i)[1]} };
    qauxvfld.DOF(i) = paramInterpret.setDOFFrom(velfld.DOF(i), gradq1, p0, T0);
  }

  // IBL solution field
  Field_DG_Cell<PhysDim, TopoDim, ArrayQIBL> qIBLfld( xfld, order_soln, BasisFunctionCategory_Hierarchical );
  Field_DG_Trace<PhysDim, TopoDim, ArrayQIBL> qTraceIBLfld(xfld, order_soln, BasisFunctionCategory_Hierarchical); // interior trace solution

  BOOST_REQUIRE_EQUAL( xfld.nElem(), qIBLfld.nElem() );
  BOOST_CHECK(nnode_a + nnode_w == qTraceIBLfld.nDOF());

  std::vector<ArrayQIBL> dataQinit_a(nnode_a, pdeIBLWall.setDOFFrom(VarDataType(0.05, 1.2, 0.04, 2.3, 6.0, 0.002)));
  std::vector<ArrayQIBL> dataQinit_w(nnode_w, pdeIBLWake.setDOFFrom(VarDataType(0.06, 0.4, 0.02, 5.0, 10.5, 0.001)));
  for_each_CellGroup<TopoDim>::apply( SetSolnDofCell_IBL2D<VarType>(dataQinit_a, cellGroup_a, order_soln), (xfld, qIBLfld) );
  for_each_CellGroup<TopoDim>::apply( SetSolnDofCell_IBL2D<VarType>(dataQinit_w, cellGroup_w, order_soln), (xfld, qIBLfld) );

  for (int j = 0; j < qTraceIBLfld.nDOF(); ++j)
    qTraceIBLfld.DOF(j) = dataQinit_a.at(0);

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQIBL>
  lgfld( xfld, order_soln, BasisFunctionCategory_Legendre, BCParamsIBL::getLGBoundaryGroups(PyBCList_w, BCBoundaryGroups_w) );
  lgfld = 0.0;

  // Hub trace field
  Field_DG_HubTrace<PhysDim, TopoDim, ArrayQIBL>
    hbfld( xfld, order_soln, BasisFunctionCategory_Legendre );

  hbfld = {0.2, 0.1, 4.175094153900431e-02, 2.894029513470550e-02, 3.4, 0.06};

  // Panel solution field
  GamFieldType gamfld(xfld, PanelMethodEquationType::order_gam, BasisFunctionCategory_Hierarchical, cellGroup_a);
  for ( int i = 0; i < nDOFgam; ++i )
    gamfld.DOF(i) = -dataVel_a[i];

  Real Psi0 = -0.007570739918295;

  LamFieldType lamfld(xfld, PanelMethodEquationType::order_lam, BasisFunctionCategory_Legendre);
  for ( int i = 0; i < nDOFlam; ++i )
    lamfld.DOF(i) = 0.6;

  LamFieldType qauxifld(xfld, PanelMethodEquationType::order_lam, BasisFunctionCategory_Legendre);
  qauxifld = -0.6;

  /////////////////////////////////////////////////////////////////////
  // SET UP EQUATION SETS

  const Real tol_eqn = 1.e-12;

  // IBL
  TupleFieldIBLParamType tupleIBLfld = (qauxvfld, xfld);

  QuadratureOrder quadratureOrder(xfld, 4);
  std::vector<Real> tol_IBL(IBLEquationType::nEqnSet,tol_eqn);
  IBLEquationType IBLEqSet(hbfld,tupleIBLfld,qIBLfld,qTraceIBLfld,lgfld,pdeIBLWall, quadratureOrder,ResidualNorm_L2,
                           tol_IBL,cellGroup_a,interiorTraceGroups_a,PyBCList_a,BCBoundaryGroups_a);

  auto IBLEqSet_w_ptr
    = std::make_shared<IBLEquationType>(hbfld,tupleIBLfld,qIBLfld,qTraceIBLfld,lgfld,pdeIBLWake,quadratureOrder,ResidualNorm_L2,
                                        tol_IBL,cellGroup_w,interiorTraceGroups_w,PyBCList_w,BCBoundaryGroups_w);

  IBLEqSet.addAlgebraicEquationSet(IBLEqSet_w_ptr);

  // Panel
  PanelMethodEquationType PanelEqSet(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  // Auxiliary viscous equation
  ElementXField<PhysDim,TopoDim,Line> xfldElemDummy(FcnEvalQauxvType::order_qauxv, BasisFunctionCategory_Hierarchical);
  xfldElemDummy.DOF(0) = {0.0, 0.0};
  xfldElemDummy.DOF(1) = {1.0, 0.0};
  FcnEvalQauxvType fcnEvalQauxvDummy(panel,xfldElemDummy,gamfld,lamfld,0,0); //TODO: dummy here
  IntegrandCellAuxvType integrandCellAuxv(fcnEvalQauxvDummy, panelCellGroups);
  AuxvEquationType AuxvEqSet(xfld, qauxvfld, integrandCellAuxv, quadratureOrder, {{tol_eqn}}, PanelEqSet, panelCellGroups);

  // Auxiliary inviscid equation
  IntegrandAuxiNullCellType integrandCellAuxiNull(panelCellGroups);

  TupleFieldAuxiParamType tupleAuxifld = (qIBLfld, qauxvfld, xfld);

  typedef typename AuxiEquationType::IntegrandInteriorTraceType integrandAuxiInteriorTraceType;
  typedef typename AuxiEquationType::IntegrandBoundaryTraceType integrandAuxiBoundaryTraceType;

  PDENDConvertSpace<PhysDim, ProjectionToQauxi<PanelMethodEquationType, NDPDEClassIBL> > projectorToQauxi_a(pdeIBLWall);
  PDENDConvertSpace<PhysDim, ProjectionToQauxi<PanelMethodEquationType, NDPDEClassIBL> > projectorToQauxi_w(pdeIBLWake);

  std::vector<std::shared_ptr<integrandAuxiInteriorTraceType> > integrandAuxiInteriorTraceArray;
  integrandAuxiInteriorTraceArray.push_back( std::make_shared<integrandAuxiInteriorTraceType>(projectorToQauxi_a, interiorTraceGroups_a) );
  integrandAuxiInteriorTraceArray.push_back( std::make_shared<integrandAuxiInteriorTraceType>(projectorToQauxi_w, interiorTraceGroups_w) );

  std::vector<std::shared_ptr<integrandAuxiBoundaryTraceType> > integrandAuxiBoundaryTraceArray;
  integrandAuxiBoundaryTraceArray.push_back( std::make_shared<integrandAuxiBoundaryTraceType>(projectorToQauxi_a, boundaryTraceGroups_a) );
  integrandAuxiBoundaryTraceArray.push_back( std::make_shared<integrandAuxiBoundaryTraceType>(projectorToQauxi_w, boundaryTraceGroups_w) );

  AuxiEquationType AuxiEqSet(tupleAuxifld, qauxifld, integrandCellAuxiNull, quadratureOrder, {{tol_eqn}},
                             integrandAuxiInteriorTraceArray, integrandAuxiBoundaryTraceArray);

  // Coupling jacobians
  DecoupledJacobianType01 JP01(AuxvEqSet);
  DecoupledJacobianType02 JP02(AuxvEqSet);
  CouplingJacobianType03 JP03(AuxvEqSet);

  CouplingJacobianType10 JP10(AuxiEqSet);
  CouplingJacobianType12 JP12(AuxiEqSet);
  DecoupledJacobianType13 JP13(AuxiEqSet);

  std::map<int,int> columnMap;
  columnMap[0] = 0;
  CouplingJacobianType20_IBLeqn_Qauxv JP20(IBLEqSet, columnMap);
  auto JP20_w = std::make_shared<CouplingJacobianType20_IBLeqn_Qauxv>(*IBLEqSet_w_ptr, columnMap);
  JP20.addJacobianParamSet(JP20_w);

  DecoupledJacobianType21 JP21(IBLEqSet);
  DecoupledJacobianType23 JP23(IBLEqSet);

  DecoupledJacobianType30 JP30(PanelEqSet);
  CouplingJacobianType31  JP31(PanelEqSet);
  DecoupledJacobianType32 JP32(PanelEqSet);

  // block system
  Block4x4AlgebraicEquationSetType BlockAES(AuxvEqSet, JP01,      JP02,     JP03,
                                            JP10,      AuxiEqSet, JP12,     JP13,
                                            JP20,      JP21,      IBLEqSet, JP23,
                                            JP30,      JP31,      JP32,     PanelEqSet);

  /////////////////////////////////////////////////////////////////////
  // Test stuff
  BlockSystemVectorType q(BlockAES.vectorStateSize());
  BlockAES.fillSystemVector(q);

#ifdef DISPLAY_FOR_DEBUGGING // print block vector
  cout << "q.v0 = {" << q.v0 << "}" << endl;
  cout << "q.v1 = {" << q.v1 << "}" << endl;
  cout << "q.v2 = {" << q.v2 << "}" << endl;
  cout << "q.v3 = {" << q.v3 << "}" << endl;
#endif

  BlockSystemVectorType rsd(BlockAES.vectorEqSize());
  BlockSystemVectorType rsdp(BlockAES.vectorEqSize());
  BlockSystemVectorType rsdm(BlockAES.vectorEqSize());
  BlockAES.residual(q,rsd);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Tests on Jacobian
  ////////////////////////////////////////////////////////////////////////////////////////
  BlockSystemMatrixNZType nz(BlockAES.matrixSize());
  BlockAES.jacobian(q,nz);
  BlockSystemMatrixType jac(nz);
  BlockAES.jacobian(q,jac);

  // check sizes
  BOOST_REQUIRE_EQUAL(jac.m(), 4); BOOST_REQUIRE_EQUAL(jac.n(), 4);

  BOOST_REQUIRE_EQUAL(jac.m00.m(), 1); BOOST_REQUIRE_EQUAL(jac.m00.n(), 1);
  BOOST_REQUIRE_EQUAL(jac.m01.m(), 1); BOOST_REQUIRE_EQUAL(jac.m01.n(), 1);
  BOOST_REQUIRE_EQUAL(jac.m02.m(), 1); BOOST_REQUIRE(jac.m02.n() == IBLEquationType::nSolSet);
  BOOST_REQUIRE_EQUAL(jac.m03.m(), 1); BOOST_REQUIRE_EQUAL(jac.m03.n(), 3);

  BOOST_REQUIRE_EQUAL(jac.m10.m(), 1); BOOST_REQUIRE_EQUAL(jac.m10.n(), 1);
  BOOST_REQUIRE_EQUAL(jac.m11.m(), 1); BOOST_REQUIRE_EQUAL(jac.m11.n(), 1);
  BOOST_REQUIRE_EQUAL(jac.m12.m(), 1); BOOST_REQUIRE(jac.m12.n() == IBLEquationType::nSolSet);
  BOOST_REQUIRE_EQUAL(jac.m13.m(), 1); BOOST_REQUIRE_EQUAL(jac.m13.n(), 3);

  BOOST_REQUIRE(jac.m20.m() == IBLEquationType::nEqnSet); BOOST_REQUIRE_EQUAL(jac.m20.n(), 1);
  BOOST_REQUIRE(jac.m21.m() == IBLEquationType::nEqnSet); BOOST_REQUIRE_EQUAL(jac.m21.n(), 1);
  BOOST_REQUIRE(jac.m22.m() == IBLEquationType::nEqnSet); BOOST_REQUIRE(jac.m22.n() == IBLEquationType::nSolSet);
  BOOST_REQUIRE(jac.m23.m() == IBLEquationType::nEqnSet); BOOST_REQUIRE_EQUAL(jac.m23.n(), 3);

  BOOST_REQUIRE_EQUAL(jac.m30.m(), 3); BOOST_REQUIRE_EQUAL(jac.m30.n(), 1);
  BOOST_REQUIRE_EQUAL(jac.m31.m(), 3); BOOST_REQUIRE_EQUAL(jac.m31.n(), 1);
  BOOST_REQUIRE_EQUAL(jac.m32.m(), 3); BOOST_REQUIRE(jac.m32.n() == IBLEquationType::nSolSet);
  BOOST_REQUIRE_EQUAL(jac.m33.m(), 3); BOOST_REQUIRE_EQUAL(jac.m33.n(), 3);

  // ping tests on jacobian

#ifdef ISTIMINGBlock4x4
  timer pingtime;
#endif

#ifdef DISPLAY_FOR_DEBUGGING
  const bool verbose = true;
#else
  const bool verbose = false;
#endif

  const std::vector<Real> step_vec = {2e-3, 1e-3};
  const std::vector<Real>& rate_range = {1.8, 2.3};
  const Real nonzero_tol = 1.e-13;
  const Real small_tol = 3.5e-12;

  {
    const typename TraitsTypeBlock0::ArrayQ qArrayQscale = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, p0, T0};

#ifdef IS_FULLTEST_LOSESPEEDUP
#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(0,0) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ00>(jac.m00, rsdp, rsdm, rsdp.v0, rsdm.v0, q, q.v0, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(1,0) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ10>(jac.m10, rsdp, rsdm, rsdp.v1, rsdm.v1, q, q.v0, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);
#endif

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(2,0) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ20>(jac.m20, rsdp, rsdm, rsdp.v2, rsdm.v2, q, q.v0, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);

#ifdef IS_FULLTEST_LOSESPEEDUP
#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(3,0) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ30>(jac.m30, rsdp, rsdm, rsdp.v3, rsdm.v3, q, q.v0, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol, false); // this block does not have any nonzeros
#endif
  }

#ifdef IS_FULLTEST_LOSESPEEDUP
  {
    const typename TraitsTypeBlock1::ArrayQ qArrayQscale = {1.0};

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(0,1) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ01>(jac.m01, rsdp, rsdm, rsdp.v0, rsdm.v0, q, q.v1, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol, false); // this block does not have any nonzeros

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(1,1) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ11>(jac.m11, rsdp, rsdm, rsdp.v1, rsdm.v1, q, q.v1, BlockAES,
                                                           step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(2,1) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ21>(jac.m21, rsdp, rsdm, rsdp.v2, rsdm.v2, q, q.v1, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol, false); // this block does not have any nonzeros

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(3,1) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ31>(jac.m31, rsdp, rsdm, rsdp.v3, rsdm.v3, q, q.v1, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);
  }
#endif

  {
    const typename TraitsTypeBlock2::ArrayQ qArrayQscale = {0.5, 1.0, 0.5, 2.0, 1.0, 0.001};

#ifdef IS_FULLTEST_LOSESPEEDUP
#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(0,2) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ02>(jac.m02, rsdp, rsdm, rsdp.v0, rsdm.v0, q, q.v2, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol, false); // this block does not have any nonzeros

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(1,2) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ12>(jac.m12, rsdp, rsdm, rsdp.v1, rsdm.v1, q, q.v2, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);
#endif

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(2,2) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ22>(jac.m22, rsdp, rsdm, rsdp.v2, rsdm.v2, q, q.v2, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);

#ifdef IS_FULLTEST_LOSESPEEDUP
#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(3,2) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ32>(jac.m32, rsdp, rsdm, rsdp.v3, rsdm.v3, q, q.v2, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol, false); // this block does not have any nonzeros
#endif
  }

#ifdef IS_FULLTEST_LOSESPEEDUP
  {
    const typename TraitsTypeBlock3::ArrayQ qArrayQscale = {1.0};

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(0,3) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ03>(jac.m03, rsdp, rsdm, rsdp.v0, rsdm.v0, q, q.v3, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(1,3) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ13>(jac.m13, rsdp, rsdm, rsdp.v1, rsdm.v1, q, q.v3, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol, false); // this block does not have any nonzeros

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(2,3) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ23>(jac.m23, rsdp, rsdm, rsdp.v2, rsdm.v2, q, q.v3, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol, false); // this block does not have any nonzeros

#ifdef DISPLAY_FOR_DEBUGGING
    std::cout << std::endl << "========== Testing jac_block = jac(3,3) ==========" << std::endl << std::endl;
#endif
    detail_jacobianPingTest::jacobianPingTest::
    ping_block<MatrixQ33>(jac.m33, rsdp, rsdm, rsdp.v3, rsdm.v3, q, q.v3, BlockAES,
                          step_vec, qArrayQscale, rate_range, verbose, nonzero_tol, small_tol);
  }
#endif

  // dump out jacobian for inspection
#ifdef IBL2D_DUMP
  std::string filename = "tmp/HDGAdv_test.mtx";
  std::cout << "Dumping " << filename << std::endl;
  WriteMatrixMarketFile( jac, filename );

  filename = "tmp/HDGAdv_test00.mtx";
  std::cout << "Dumping " << filename << std::endl;
  WriteMatrixMarketFile( jac.m00, filename );

  filename = "tmp/HDGAdv_test11.mtx";
  std::cout << "Dumping " << filename << std::endl;
  WriteMatrixMarketFile( jac.m11, filename );

  filename = "tmp/HDGAdv_test22.mtx";
  std::cout << "Dumping " << filename << std::endl;
  WriteMatrixMarketFile( jac.m22, filename );

  filename = "tmp/HDGAdv_test33.mtx";
  std::cout << "Dumping " << filename << std::endl;
  WriteMatrixMarketFile( jac.m33, filename );
#endif

#ifdef ISTIMINGBlock4x4
  std::cout << "Wall-clock time of ping tests : " << pingtime.elapsed() << " second(s)" << std::endl;
#endif

  ////////////////////////////////////////////////////////////////////////////////////////
  // Test solver
  ////////////////////////////////////////////////////////////////////////////////////////
  { // UMFPACK linear solver for debugging
#if defined(DISPLAY_FOR_DEBUGGING)
    std::cout << "Testing linear solver" << std::endl;
#endif
    PyDict UMFPACKDict;
    UMFPACKParam::checkInputs(UMFPACKDict);

    UMFPACK<BlockSystemMatrixType> linearSolver(UMFPACKDict, BlockAES);

    BlockSystemVectorType r0(BlockAES.vectorEqSize());
    r0 = 0.0;
    DLA::index(r0.v0[0][0],0) = 1.0;

    BlockSystemVectorType dq(BlockAES.vectorStateSize());
    dq = 0.0;

    linearSolver.solve(r0, dq);

    BlockSystemVectorType b(BlockAES.vectorEqSize());
    b = linearSolver.A() * dq;

    ScalarVector b_plain(b), r0_plain(r0);
    const Real tol_small = 1.5e-11, tol_close = 4.e-14;
    for (int j = 0; j < b_plain.m; ++j)
      SANS_CHECK_CLOSE(r0_plain.v[j], b_plain.v[j], tol_small, tol_close);

#ifdef IBL2D_DUMP
    cout << "writing r0 to tmp/r0.dat" << endl;
    WritePlainVector( r0, "tmp/r0.dat" );

    cout << "writing dq to tmp/dq.dat" << endl;
    WritePlainVector( dq, "tmp/dq.dat" );

    cout << "writing b to tmp/b.dat" << endl;
    WritePlainVector( b, "tmp/b.dat" );

    cout << "writing jacUMFPACK to tmp/jacUMFPACK.mtx" << endl;
    WriteMatrixMarketFile( linearSolver.A(), "tmp/jacUMFPACK.mtx" );
#endif
  }

#ifdef ISTIMINGBlock4x4
  std::cout << "Wall-clock time of the whole test : " << totaltime.elapsed() << " second(s)" << std::endl;
#endif

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
