INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

RUN_WITH_MPIEXEC( 1 )

GenerateUnitTests( UnitGridsLib 
                   FieldLib 
                   BasisFunctionLib 
                   QuadratureLib 
                   SurrealLib 
                   AdvectionDiffusionLib 
                   PorousMediaLib 
                   AnalyticFunctionLib
                   NonLinearSolverLib
                   SparseLinAlgLib 
                   DenseLinAlgLib 
                   PythonLib 
                   TopologyLib 
                   toolsLib )

# TODO: Remove this when we have a FV_TPFALib library
ADD_DEPENDENCIES( ${FOLDER_TEST} Discretization )