// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// JacobianCell_FV_TPFA_AD_btest
// testing of cell-integral jacobian: advection-diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/minmax.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"

#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Discretization/FV_TPFA/IntegrandCell_FV_TPFA.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/IntegrateCellGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_1Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_1Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( JacobianCell_FV_TPFA_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FV1D_1Line_X1Q0 )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::MatrixQ<Real> MatrixQ;

  typedef IntegrandCell_FV_TPFA<PDEClass> IntegrandClass;

  Real u = 1;
  AdvectiveFlux1D_Uniform adv(u);

  Real kxx = 2.123;
  ViscousFlux1D_Uniform visc(kxx);

  Real a = 2.3;
  Source1D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid: single triangle, P1 (aka X1)
  XField1D_1Line_X1_1Group xfld;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  int qorder = 0;

  // solution
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  const int nDOF = qfld.nDOF();

  // solution data
  for (int dof = 0; dof < qfld.nDOF();dof++)
    qfld.DOF(dof) = (dof+1)*pow(-1,dof);

  // quadrature rule
  int quadratureOrder = 2*qorder;

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
  DLA::MatrixD<MatrixQ> mtxGlobTrue(nDOF, nDOF);
  DLA::MatrixD<MatrixQ> mtxGlob(nDOF, nDOF);

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal0), xfld, qfld, &quadratureOrder, 1 );

  for (int i = 0; i < nDOF; i++)
  {
    qfld.DOF(i) += 1;
    rsdGlobal1 = 0;
    IntegrateCellGroups<TopoD1>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal1), xfld, qfld, &quadratureOrder, 1 );
    qfld.DOF(i) -= 1;

    for (int j = 0; j < nDOF; j++)
      mtxGlobTrue(j,i) = rsdGlobal1[j] - rsdGlobal0[j];
  }

  // jacobian via Surreal
  mtxGlob = 0;
  IntegrateCellGroups<TopoD1>::integrate( JacobianCell_Galerkin(fcnint, mtxGlob), xfld, qfld, &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < nDOF; i++)
    for (int j = 0; j < nDOF; j++)
      SANS_CHECK_CLOSE( mtxGlobTrue(i,j), mtxGlob(i,j), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin2D_1Triangle_X1Q0 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  typedef IntegrandCell_FV_TPFA<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 0.4;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid: single triangle, P1 (aka X1)
  XField2D_1Triangle_X1_1Group xfld;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  int qorder = 0;

  // solution
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  const int nDOF = qfld.nDOF();

  // solution data
  for (int dof = 0; dof < qfld.nDOF();dof++)
    qfld.DOF(dof) = (dof+1)*pow(-1,dof);

  // quadrature rule
  int quadratureOrder = 2*qorder;

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
  DLA::MatrixD<MatrixQ> mtxGlobTrue(nDOF, nDOF);
  DLA::MatrixD<MatrixQ> mtxGlob(nDOF, nDOF);

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal0), xfld, qfld, &quadratureOrder, 1 );

  for (int i = 0; i < nDOF; i++)
  {
    qfld.DOF(i) += 1;
    rsdGlobal1 = 0;
    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal1), xfld, qfld, &quadratureOrder, 1 );
    qfld.DOF(i) -= 1;

    for (int j = 0; j < nDOF; j++)
      mtxGlobTrue(j,i) = rsdGlobal1[j] - rsdGlobal0[j];
  }

  // jacobian via Surreal
  mtxGlob = 0;
  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnint, mtxGlob), xfld, qfld, &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < nDOF; i++)
    for (int j = 0; j < nDOF; j++)
      SANS_CHECK_CLOSE( mtxGlobTrue(i,j), mtxGlob(i,j), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin2D_1Quad_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_Uniform> PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  typedef IntegrandCell_FV_TPFA<PDEClass> IntegrandClass;

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Real a = 0.4;
  Source2D_Uniform source(a);

  PDEClass pde( adv, visc, source );

  // grid: single quad, P1 (aka X1)
  XField2D_1Quad_X1_1Group xfld;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  int qorder = 0;

  // solution
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  const int nDOF = qfld.nDOF();

  // solution data
  for (int dof = 0; dof < qfld.nDOF();dof++)
    qfld.DOF(dof) = (dof+1)*pow(-1,dof);

  // quadrature rule
  int quadratureOrder = 2*qorder;

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
  DLA::MatrixD<MatrixQ> mtxGlobTrue(nDOF, nDOF);
  DLA::MatrixD<MatrixQ> mtxGlob(nDOF, nDOF);

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal0), xfld, qfld, &quadratureOrder, 1 );

  for (int i = 0; i < nDOF; i++)
  {
    qfld.DOF(i) += 1;
    rsdGlobal1 = 0;
    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal1), xfld, qfld, &quadratureOrder, 1 );
    qfld.DOF(i) -= 1;

    for (int j = 0; j < nDOF; j++)
      mtxGlobTrue(j,i) = rsdGlobal1[j] - rsdGlobal0[j];
  }

  // jacobian via Surreal
  mtxGlob = 0;
  IntegrateCellGroups<TopoD2>::integrate( JacobianCell_Galerkin(fcnint, mtxGlob), xfld, qfld, &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < nDOF; i++)
    for (int j = 0; j < nDOF; j++)
      SANS_CHECK_CLOSE( mtxGlobTrue(i,j), mtxGlob(i,j), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin3D_1Tet_X1Q0 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_Uniform> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  typedef IntegrandCell_FV_TPFA<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Real s0 = 0.25;
  Source3D_Uniform source(s0);

  PDEClass pde( adv, visc, source );

  // grid: single tet, P1 (aka X1)
  XField3D_1Tet_X1_1Group xfld;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  int qorder = 0;

  // solution
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  const int nDOF = qfld.nDOF();

  // solution data
  for (int dof = 0; dof < qfld.nDOF();dof++)
    qfld.DOF(dof) = (dof+1)*pow(-1,dof);

  // quadrature rule
  int quadratureOrder = 2*qorder;

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
  DLA::MatrixD<MatrixQ> mtxGlobTrue(nDOF, nDOF);
  DLA::MatrixD<MatrixQ> mtxGlob(nDOF, nDOF);

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal0), xfld, qfld, &quadratureOrder, 1 );

  for (int i = 0; i < nDOF; i++)
  {
    qfld.DOF(i) += 1;
    rsdGlobal1 = 0;
    IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal1), xfld, qfld, &quadratureOrder, 1 );
    qfld.DOF(i) -= 1;

    for (int j = 0; j < nDOF; j++)
      mtxGlobTrue(j,i) = rsdGlobal1[j] - rsdGlobal0[j];
  }

  // jacobian via Surreal
  mtxGlob = 0;
  IntegrateCellGroups<TopoD3>::integrate( JacobianCell_Galerkin(fcnint, mtxGlob), xfld, qfld, &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < nDOF; i++)
    for (int j = 0; j < nDOF; j++)
      SANS_CHECK_CLOSE( mtxGlobTrue(i,j), mtxGlob(i,j), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Galerkin3D_1Hex_X1Q1 )
{
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_Uniform> PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> PDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  typedef IntegrandCell_FV_TPFA<PDEClass> IntegrandClass;

  Real u = 1.1;
  Real v = 0.2;
  Real w = 0.5;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  Real kxz = 0.643;
  Real kyz = 0.765;
  Real kzz = 1.234;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kxy, kyy, kyz,
                              kxz, kyz, kzz );

  Real s0 = 0.25;
  Source3D_Uniform source(s0);

  PDEClass pde( adv, visc, source );

  // grid: single hexahedron, P1 (aka X1)
  XField3D_1Hex_X1_1Group xfld;

  // integrand
  IntegrandClass fcnint( pde, {0} );

  int qorder = 0;

  // solution
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, qorder, BasisFunctionCategory_Legendre);

  const int nDOF = qfld.nDOF();

  // solution data
  for (int dof = 0; dof < qfld.nDOF();dof++)
    qfld.DOF(dof) = (dof+1)*pow(-1,dof);

  // quadrature rule
  int quadratureOrder = 2*qorder;

  // jacobian via FD w/ residual operator; assumes scalar PDE

  SLA::SparseVector<ArrayQ> rsdGlobal0(nDOF), rsdGlobal1(nDOF);
  DLA::MatrixD<MatrixQ> mtxGlobTrue(nDOF, nDOF);
  DLA::MatrixD<MatrixQ> mtxGlob(nDOF, nDOF);

  rsdGlobal0 = 0;
  IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal0), xfld, qfld, &quadratureOrder, 1 );

  for (int i = 0; i < nDOF; i++)
  {
    qfld.DOF(i) += 1;
    rsdGlobal1 = 0;
    IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnint, rsdGlobal1), xfld, qfld, &quadratureOrder, 1 );
    qfld.DOF(i) -= 1;

    for (int j = 0; j < nDOF; j++)
      mtxGlobTrue(j,i) = rsdGlobal1[j] - rsdGlobal0[j];
  }

  // jacobian via Surreal
  mtxGlob = 0;
  IntegrateCellGroups<TopoD3>::integrate( JacobianCell_Galerkin(fcnint, mtxGlob), xfld, qfld, &quadratureOrder, 1 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;
  for (int i = 0; i < nDOF; i++)
    for (int j = 0; j < nDOF; j++)
      SANS_CHECK_CLOSE( mtxGlobTrue(i,j), mtxGlob(i,j), small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
