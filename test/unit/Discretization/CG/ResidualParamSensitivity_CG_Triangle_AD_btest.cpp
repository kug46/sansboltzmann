// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <string>
#include <fstream>
#include <limits>

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "pde/HSM/PDEHSM2D.h"
#include "pde/HSM/BCHSM2D.h"
#include "pde/HSM/ComplianceMatrix.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/CG/ResidualParamSensitivity_CG.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_HSM2D.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_Galerkin_HSM2D.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/Tuple/FieldTuple.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ResidualParamSensitivity_CG_Triangle_AD_test_suite )

typedef PDEHSM2D<VarTypeLambda> PDEClass;
typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

typedef BCHSM2DVector<VarTypeLambda> BCVector;

typedef PDEClass::VectorX<Real> VectorX;
typedef PDEClass::VectorE<Real> VectorE;
typedef PDEClass::ComplianceMatrix<Real> ComplianceMatrix;
typedef PDEClass::ComplianceMatrix<SurrealS<1>> ComplianceMatrixSurreal;

typedef MakeTuple<FieldTuple, Field<PhysD2, TopoD2, ComplianceMatrix>,
                              Field<PhysD2, TopoD2, Real>,
                              Field<PhysD2, TopoD2, VectorE>,
                              Field<PhysD2, TopoD2, VectorX>,
                              Field<PhysD2, TopoD2, VectorX>,
                              XField<PhysD2, TopoD2> >::type FieldParamPrimalType;

typedef MakeTuple<FieldTuple, Field<PhysD2, TopoD2, ComplianceMatrixSurreal>,
                              Field<PhysD2, TopoD2, SurrealS<1> >,
                              Field<PhysD2, TopoD2, VectorE>,
                              Field<PhysD2, TopoD2, VectorX>,
                              Field<PhysD2, TopoD2, VectorX>,
                              XField<PhysD2, TopoD2> >::type FieldParamSensType;


typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector, AlgEqSetTraits_Sparse, FieldParamPrimalType > PrimalEquationSetClass;
typedef ResidualParamSensitivity_CG<NDPDEClass, BCNDConvertSpace, BCVector, SurrealS<1>, FieldParamSensType> SensEquationSetClass;
typedef PrimalEquationSetClass::BCParams BCParams;

typedef NDPDEClass::ArrayQ<Real> ArrayQ;
typedef NDPDEClass::ArrayQ<SurrealS<1>> ArrayQSurreal;

typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

typedef PrimalEquationSetClass::SystemVectorTemplate<ArrayQSurreal> SystemVectorSurrealClass;

const Real rho = 1;
const Real E = 500000;
const Real nu = 0;

void setMufldPrimal(const XField<PhysD2, TopoD2>& xfld,
                    const Element<  SurrealS<1>, TopoD1, Line >& thickness,
                    Field_CG_Cell<PhysD2, TopoD2, Real>& mufldPrimal)
{
  // NOTE: This assumes P1 CG field
  for (int i = 0; i < xfld.nDOF(); i++)
  {
    Real t = thickness.eval( xfld.DOF(i)[1] ).value(); // Don't want negative thickenss
    mufldPrimal.DOF(i) = rho*t;
  }
}

void setMufldSens(const XField<PhysD2, TopoD2>& xfld,
                  const Element<  SurrealS<1>, TopoD1, Line >& thickness,
                  Field_CG_Cell<PhysD2, TopoD2, SurrealS<1>>& mufldSens)
{
  // NOTE: This assumes P1 CG field
  for (int i = 0; i < xfld.nDOF(); i++)
  {
    SurrealS<1> t = thickness.eval( xfld.DOF(i)[1] ); // Don't want negative thickenss
    mufldSens.DOF(i) = rho*t;
  }
}

void setParamfldPrimal(const XField<PhysD2, TopoD2>& xfld,
                       const Element<  SurrealS<1>, TopoD1, Line >& thickness,
                       Field_CG_Cell<PhysD2, TopoD2, Real>& mufldPrimal,
                       Field_CG_Cell<PhysD2, TopoD2, ComplianceMatrix>& AinvfldPrimal )
{
  // NOTE: This assumes P1 CG field
  for (int i = 0; i < xfld.nDOF(); i++)
  {
    Real t = thickness.eval( xfld.DOF(i)[1] ).value(); // Don't want negative thickenss
    mufldPrimal.DOF(i)   = rho*t;
    AinvfldPrimal.DOF(i) = calcComplianceMatrix( E, nu, t );
  }
}

void setParamfldSens(const XField<PhysD2, TopoD2>& xfld,
                     const Element<  SurrealS<1>, TopoD1, Line >& thickness,
                     Field_CG_Cell<PhysD2, TopoD2, SurrealS<1>>& mufldSens,
                     Field_CG_Cell<PhysD2, TopoD2, ComplianceMatrixSurreal>& AinvfldSens)
{
  // NOTE: This assumes P1 CG field
  for (int i = 0; i < xfld.nDOF(); i++)
  {
    const SurrealS<1> t = thickness.eval( xfld.DOF(i)[1] ); // Don't want negative thickenss
    mufldSens.DOF(i)   = rho*t;
    AinvfldSens.DOF(i) = calcComplianceMatrix( E, nu, t );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SparseSystem )
{

  // PDE

  VectorX g = { 0.023, -9.81 };
  NDPDEClass pde(g, PDEClass::HSM_ResidInterp_Separate);

  // BC

  PyDict BCDisp;
  BCDisp[BCParams::params.BC.BCType] = BCParams::params.BC.Displacements;
  BCDisp[BCHSM2DParams<BCTypeDisplacements>::params.rshiftx] = 0;
  BCDisp[BCHSM2DParams<BCTypeDisplacements>::params.rshifty] = 0;

  PyDict BCForceZero;
  BCForceZero[BCParams::params.BC.BCType] = BCParams::params.BC.Forces;
  BCForceZero[BCHSM2DParams<BCTypeForces>::params.f1BC] = 0; // f1BC
  BCForceZero[BCHSM2DParams<BCTypeForces>::params.f2BC] = 0; // f2BC

  // String arguments in PyBCList and BCBoundaryGroups must always match

  PyDict PyBCList;
  PyBCList["BCDisp"] = BCDisp;
  PyBCList["BCForceZero"] = BCForceZero;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  const int iBCbottom = 0;
  const int iBCright = 1;
  const int iBCtop = 2;
  const int iBCleft = 3;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDisp"] = {iBCbottom};
  BCBoundaryGroups["BCForceZero"] = {iBCtop, iBCright, iBCleft};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // grid: HierarchicalP1 (aka X1)

  int ii = 3;
  int jj = 2;

  XField2D_Box_Triangle_X1 xfld( ii, jj );

  // solution: HierarchicalP1 (aka Q1)

  int order = 1;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> varfld(xfld, order, BasisFunctionCategory_Hierarchical);

  for (int i = 0; i < xfld.nDOF(); i++)
  {
    PositionLambdaForces2D varData;
    varData.rx    = xfld.DOF(i)[0];
    varData.ry    = xfld.DOF(i)[1] + 0.1;
    varData.lam3  = 7./25.;
    varData.f11   = -5;
    varData.f22   = 9;
    varData.f12   = 2;

    varfld.DOF(i) = pde.setDOFFrom( varData );
  }

  // Extensional compliance matrix CG field
  Field_CG_Cell<PhysD2, TopoD2, ComplianceMatrix> AinvfldPrimal(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD2, TopoD2, ComplianceMatrixSurreal> AinvfldSens(xfld, 1, BasisFunctionCategory_Hierarchical);

  // Lumped shell mass CG field
  Field_CG_Cell<PhysD2, TopoD2, Real> mufldPrimal(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD2, TopoD2, SurrealS<1>> mufldSens(xfld, 1, BasisFunctionCategory_Hierarchical);

  // Following force CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorE> qefld(xfld, 1, BasisFunctionCategory_Hierarchical);
  qefld = 0;

  // Global forcing CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorX> qxfld(xfld, 1, BasisFunctionCategory_Hierarchical);
  qxfld = 0;

  // Accelerations CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorX> afld(xfld, 1, BasisFunctionCategory_Hierarchical);
  afld = 0;

  // Lagrange multiplier: Legendre P1

#if 0
  order = 0;
  QField2D_DG_BoundaryEdge<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#elif 1
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
  order = 1;
  QField2D_CG_BoundaryEdge_Independent<PDEClass> lgfld( xfld, order );
#endif

  lgfld = 0;

  FieldParamPrimalType fldParamPrimal = (AinvfldPrimal, mufldPrimal, qefld, qxfld, afld, xfld);
  FieldParamSensType fldParamSens = (AinvfldSens, mufldSens, qefld, qxfld, afld, xfld);

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  StabilizationNitsche stab(1);
  PrimalEquationSetClass PrimalEqSet(fldParamPrimal, varfld, lgfld, pde, stab,
                                     quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );
  SensEquationSetClass SensEqSet(fldParamSens, varfld, lgfld, pde, stab, {0}, PyBCList, BCBoundaryGroups );

  SystemVectorClass sln(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd0(sln.size()), rsd1(sln.size());

  Element<  SurrealS<1>, TopoD1, Line > thickness(1, BasisFunctionCategory_Hierarchical);

  thickness.DOF(0) = 0.01;
  thickness.DOF(1) = 0.015;

  PrimalEqSet.fillSystemVector(sln);

  setParamfldPrimal(xfld,thickness,mufldPrimal,AinvfldPrimal);

  rsd0 = 0;
  PrimalEqSet.residual(sln, rsd0);

  Real eps = sqrt(std::numeric_limits<Real>::epsilon());

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-4;

  SystemVectorSurrealClass rsd(PrimalEqSet.vectorEqSize());

  // Use finite difference as the "truth" value for the parameter sensitivity

  for (int i = 0; i < thickness.nDOF(); i++ )
  {
    // VIA Surreal
    thickness.DOF(i).deriv() = 1;

    setParamfldSens(xfld,thickness,mufldSens,AinvfldSens);

    rsd = 0;
    SensEqSet.residualSensitivity(rsd);

    thickness.DOF(i).deriv() = 0;

    // VIA Finite Difference
    thickness.DOF(i) += eps;

    setParamfldPrimal(xfld,thickness,mufldPrimal,AinvfldPrimal);

    rsd1 = 0;
    PrimalEqSet.residual(sln, rsd1);

    thickness.DOF(i) -= eps;

    for (int j = 0; j < rsd.m(); j++)
    {
      for (int k = 0; k < rsd[j].m(); k++)
      {
        for (int m = 0; m < ArrayQ::M; m++)
        {
          Real fd = (rsd1[j][k][m] - rsd0[j][k][m])/eps;
          SANS_CHECK_CLOSE( fd, rsd[j][k][m].deriv(), small_tol, close_tol );
        }
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
