INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

RUN_WITH_MPIEXEC( 1 )

GenerateUnitTests( UnitGridsLib
                   FieldLib
                   BasisFunctionLib
                   QuadratureLib
                   HSMLib
                   SurrealLib
                   AdvectionDiffusionLib
                   AnalyticFunctionLib
                   pdeLib
                   SparseLinAlgLib
                   DenseLinAlgLib
                   PythonLib
                   TopologyLib
                   toolsLib )
