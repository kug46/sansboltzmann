// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionVolume_Tetrahedron_Legendre_btest
// testing of BasisFunctionVolume<Tetrahedron, Legendre> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionVolume.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Legendre.h"
#include "Quadrature/QuadratureVolume.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionVolume_Tetrahedron_Legendre_test_suite )

void fillTrueValues(const int& order, const Real& s, const Real& t, const Real& u, Real phi[], Real phis[], Real phit[], Real phiu[])
{
  phi[0] = 1;
  phis[0] = 0;
  phit[0] = 0;
  phiu[0] = 0;

  if (order == 0) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[1] = (sqrt(5/3.))*(-1+(4)*(s));
  phi[2] = (sqrt(10/3.))*(-1+s+(3)*(t));
  phi[3] = (sqrt(10))*(-1+s+t+(2)*(u));

  phis[1] = (4)*(sqrt(5/3.));
  phis[2] = sqrt(10/3.);
  phis[3] = sqrt(10);

  phit[1] = 0;
  phit[2] = sqrt(30);
  phit[3] = sqrt(10);

  phiu[1] = 0;
  phiu[2] = 0;
  phiu[3] = (2)*(sqrt(10));

  if (order == 1) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[4] = (sqrt(7/3.))*(1+(5)*((s)*(-2+(3)*(s))));
  phi[5] = (sqrt(14/3.))*((-1+(6)*(s))*(-1+s+(3)*(t)));
  phi[6] = (sqrt(14))*((-1+(6)*(s))*(-1+s+t+(2)*(u)));
  phi[7] = (sqrt(7))*(pow(-1+s,2)+(8)*((-1+s)*(t))+(10)*(pow(t,2)));
  phi[8] = (sqrt(21))*((-1+s+(5)*(t))*(-1+s+t+(2)*(u)));
  phi[9] = (sqrt(35))*(pow(-1+s+t,2)+(6)*((-1+s+t)*(u))+(6)*(pow(u,2)));

  phis[4] = (10)*((sqrt(7/3.))*(-1+(3)*(s)));
  phis[5] = (sqrt(14/3.))*(-7+(12)*(s)+(18)*(t));
  phis[6] = (sqrt(14))*(-7+(12)*(s)+(6)*(t)+(12)*(u));
  phis[7] = (2)*((sqrt(7))*(-1+s+(4)*(t)));
  phis[8] = (2)*((sqrt(21))*(-1+s+(3)*(t)+u));
  phis[9] = (2)*((sqrt(35))*(-1+s+t+(3)*(u)));

  phit[4] = 0;
  phit[5] = (sqrt(42))*(-1+(6)*(s));
  phit[6] = (sqrt(14))*(-1+(6)*(s));
  phit[7] = (4)*((sqrt(7))*(-2+(2)*(s)+(5)*(t)));
  phit[8] = (2)*((sqrt(21))*(-3+(3)*(s)+(5)*(t)+(5)*(u)));
  phit[9] = (2)*((sqrt(35))*(-1+s+t+(3)*(u)));

  phiu[4] = 0;
  phiu[5] = 0;
  phiu[6] = (2)*((sqrt(14))*(-1+(6)*(s)));
  phiu[7] = 0;
  phiu[8] = (2)*((sqrt(21))*(-1+s+(5)*(t)));
  phiu[9] = (6)*((sqrt(35))*(-1+s+t+(2)*(u)));

  if (order == 2) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[10] = (sqrt(3))*(-1+(s)*(18+(7)*((s)*(-9+(8)*(s)))));
  phi[11] = (sqrt(6))*((1+(14)*((s)*(-1+(2)*(s))))*(-1+s+(3)*(t)));
  phi[12] = (3)*((sqrt(2))*((1+(14)*((s)*(-1+(2)*(s))))*(-1+s+t+(2)*(u))));
  phi[13] = (3)*((-1+(8)*(s))*(pow(-1+s,2)+(8)*((-1+s)*(t))+(10)*(pow(t,2))));
  phi[14] = (3)*((sqrt(3))*((-1+(8)*(s))*((-1+s+(5)*(t))*(-1+s+t+(2)*(u)))));
  phi[15] = (3)*((sqrt(5))*((-1+(8)*(s))*(pow(-1+s+t,2)+(6)*((-1+s+t)*(u))+(6)*(pow(u,2)))));
  phi[16] = (2)*((sqrt(3))*(pow(-1+s,3)+(15)*((pow(-1+s,2))*(t))+(45)*((-1+s)*(pow(t,2)))+(35)*(pow(t,3))));
  phi[17] = (6)*((pow(-1+s,2)+(12)*((-1+s)*(t))+(21)*(pow(t,2)))*(-1+s+t+(2)*(u)));
  phi[18] = (2)*((sqrt(15))*((-1+s+(7)*(t))*(pow(-1+s+t,2)+(6)*((-1+s+t)*(u))+(6)*(pow(u,2)))));
  phi[19] = (2)*((sqrt(21))*(pow(-1+s+t,3)+(12)*((pow(-1+s+t,2))*(u))+(30)*((-1+s+t)*(pow(u,2)))+(20)*(pow(u,3))));

  phis[10] = (6)*((sqrt(3))*(3+(7)*((s)*(-3+(4)*(s)))));
  phis[11] = (3)*((sqrt(6))*(5+(-14)*(t)+(28)*((s)*(-1+s+(2)*(t)))));
  phis[12] = (3)*((sqrt(2))*(15+(-14)*(t)+(-28)*(u)+(28)*((s)*(-3+(3)*(s)+(2)*(t)+(4)*(u)))));
  phis[13] = (6)*(5+(12)*(pow(s,2))+(4)*((t)*(-9+(10)*(t)))+(s)*(-17+(64)*(t)));
  phis[14] = (6)*((sqrt(3))*(5+(12)*(pow(s,2))+(-9)*(u)+(s)*(-17+(48)*(t)+(16)*(u))+(t)*(-27+(20)*(t)+(40)*(u))));
  phis[15] = (6)*((sqrt(5))*(5+(12)*(pow(s,2))+(4)*(pow(t,2))+(3)*((u)*(-9+(8)*(u)))+(3)*((t)*(-3+(8)*(u)))+(s)*(-17+(16)*(t)+(48)*(u))));
  phis[16] = (6)*((sqrt(3))*(pow(-1+s,2)+(10)*((-1+s)*(t))+(15)*(pow(t,2))));
  phis[17] = (6)*(3+(3)*(pow(s,2))+(-4)*(u)+(s)*(-6+(26)*(t)+(4)*(u))+(t)*(-26+(33)*(t)+(24)*(u)));
  phis[18] = (6)*((sqrt(15))*(1+pow(s,2)+(5)*(pow(t,2))+(2)*((-2+u)*(u))+(s)*(-2+(6)*(t)+(4)*(u))+(2)*((t)*(-3+(8)*(u)))));
  phis[19] = (6)*((sqrt(21))*(pow(-1+s+t,2)+(8)*((-1+s+t)*(u))+(10)*(pow(u,2))));

  phit[10] = 0;
  phit[11] = (3)*((sqrt(6))*(1+(14)*((s)*(-1+(2)*(s)))));
  phit[12] = (3)*((sqrt(2))*(1+(14)*((s)*(-1+(2)*(s)))));
  phit[13] = (12)*((-1+(8)*(s))*(-2+(2)*(s)+(5)*(t)));
  phit[14] = (6)*((sqrt(3))*((-1+(8)*(s))*(-3+(3)*(s)+(5)*(t)+(5)*(u))));
  phit[15] = (6)*((sqrt(5))*((-1+(8)*(s))*(-1+s+t+(3)*(u))));
  phit[16] = (30)*((sqrt(3))*(pow(-1+s,2)+(6)*((-1+s)*(t))+(7)*(pow(t,2))));
  phit[17] = (6)*(13+(13)*(pow(s,2))+(-24)*(u)+(s)*(-26+(66)*(t)+(24)*(u))+(3)*((t)*(-22+(21)*(t)+(28)*(u))));
  phit[18] = (6)*((sqrt(15))*(3+(3)*(pow(s,2))+(7)*(pow(t,2))+(2)*((u)*(-8+(7)*(u)))+(2)*((s)*(-3+(5)*(t)+(8)*(u)))+(2)*((t)*(-5+(14)*(u)))));
  phit[19] = (6)*((sqrt(21))*(pow(-1+s+t,2)+(8)*((-1+s+t)*(u))+(10)*(pow(u,2))));

  phiu[10] = 0;
  phiu[11] = 0;
  phiu[12] = (6)*((sqrt(2))*(1+(14)*((s)*(-1+(2)*(s)))));
  phiu[13] = 0;
  phiu[14] = (6)*((sqrt(3))*((-1+(8)*(s))*(-1+s+(5)*(t))));
  phiu[15] = (18)*((sqrt(5))*((-1+(8)*(s))*(-1+s+t+(2)*(u))));
  phiu[16] = 0;
  phiu[17] = (12)*(pow(-1+s,2)+(12)*((-1+s)*(t))+(21)*(pow(t,2)));
  phiu[18] = (12)*((sqrt(15))*((-1+s+(7)*(t))*(-1+s+t+(2)*(u))));
  phiu[19] = (24)*((sqrt(21))*(pow(-1+s+t,2)+(5)*((-1+s+t)*(u))+(5)*(pow(u,2))));

  if (order == 3) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[20] = (sqrt(11/3.))*(1+(14)*((s)*(-2+(3)*((s)*(4+(s)*(-8+(5)*(s)))))));
  phi[21] = (sqrt(22/3.))*((-1+(12)*((s)*(2+(s)*(-9+(10)*(s)))))*(-1+s+(3)*(t)));
  phi[22] = (sqrt(22))*((-1+(12)*((s)*(2+(s)*(-9+(10)*(s)))))*(-1+s+t+(2)*(u)));
  phi[23] = (sqrt(11))*((1+(9)*((s)*(-2+(5)*(s))))*(pow(-1+s,2)+(8)*((-1+s)*(t))+(10)*(pow(t,2))));
  phi[24] = (sqrt(33))*((1+(9)*((s)*(-2+(5)*(s))))*((-1+s+(5)*(t))*(-1+s+t+(2)*(u))));
  phi[25] = (sqrt(55))*((1+(9)*((s)*(-2+(5)*(s))))*(pow(-1+s+t,2)+(6)*((-1+s+t)*(u))+(6)*(pow(u,2))));
  phi[26] = (2)*((sqrt(11/3.))*((-1+(10)*(s))*(pow(-1+s,3)+(15)*((pow(-1+s,2))*(t))+(45)*((-1+s)*(pow(t,2)))+(35)*(pow(t,3)))));
  phi[27] = (2)*((sqrt(11))*((-1+(10)*(s))*((pow(-1+s,2)+(12)*((-1+s)*(t))+(21)*(pow(t,2)))*(-1+s+t+(2)*(u)))));
  phi[28] = (2)*((sqrt(55/3.))*((-1+(10)*(s))*((-1+s+(7)*(t))*(pow(-1+s+t,2)+(6)*((-1+s+t)*(u))+(6)*(pow(u,2))))));
  phi[29] = (2)*((sqrt(77/3.))*((-1+(10)*(s))*(pow(-1+s+t,3)+(12)*((pow(-1+s+t,2))*(u))+(30)*((-1+s+t)*(pow(u,2)))+(20)*(pow(u,3)))));
  phi[30] = (sqrt(55/3.))*(pow(-1+s,4)+(24)*((pow(-1+s,3))*(t))+(126)*((pow(-1+s,2))*(pow(t,2)))+(224)*((-1+s)*(pow(t,3)))+(126)*(pow(t,4)));
  phi[31] = (sqrt(55))*((pow(-1+s,3)+(21)*((pow(-1+s,2))*(t))+(84)*((-1+s)*(pow(t,2)))+(84)*(pow(t,3)))*(-1+s+t+(2)*(u)));
  phi[32] = (5)*((sqrt(11/3.))*((pow(-1+s,2)+(16)*((-1+s)*(t))+(36)*(pow(t,2)))*(pow(-1+s+t,2)+(6)*((-1+s+t)*(u))+(6)*(pow(u,2)))));
  phi[33] = (sqrt(385/3.))*((-1+s+(9)*(t))*(pow(-1+s+t,3)+(12)*((pow(-1+s+t,2))*(u))+(30)*((-1+s+t)*(pow(u,2)))+(20)*(pow(u,3))));
  phi[34] = (sqrt(165))*(pow(-1+s+t,4)+(20)*((pow(-1+s+t,3))*(u))+(90)*((pow(-1+s+t,2))*(pow(u,2)))+(140)*((-1+s+t)*(pow(u,3)))+(70)*(pow(u,4)));

  phis[20] = (28)*((sqrt(11/3.))*(-1+(6)*((s)*(2+(s)*(-6+(5)*(s))))));
  phis[21] = (sqrt(22/3.))*(-25+(72)*(t)+(12)*((s)*(22+(-54)*(t)+(s)*(-57+(40)*(s)+(90)*(t)))));
  phis[22] = (sqrt(22))*(-1+(24)*(s)+(-108)*(pow(s,2))+(120)*(pow(s,3))+(24)*((1+(3)*((s)*(-3+(5)*(s))))*(-1+s+t+(2)*(u))));
  phis[23] = (4)*((sqrt(11))*(-5+(s)*(41+(9)*((s)*(-9+(5)*(s))))+(38)*(t)+(18)*((s)*((-14+(15)*(s))*(t)))+(45)*((-1+(5)*(s))*(pow(t,2)))));
  phis[24] = (sqrt(33))*((1+(9)*((s)*(-2+(5)*(s))))*(-1+s+(5)*(t))+(1+(9)*((s)*(-2+(5)*(s))))*(-1+s+t+(2)*(u))
             +(18)*((-1+(5)*(s))*((-1+s+(5)*(t))*(-1+s+t+(2)*(u)))));
  phis[25] = (sqrt(55))*((2)*((1+(9)*((s)*(-2+(5)*(s))))*(-1+s+t+(3)*(u)))+(-18+(90)*(s))*(pow(-1+s+t,2)+(6)*((-1+s+t)*(u))+(6)*(pow(u,2))));
  phis[26] = (2)*((sqrt(11/3.))*((pow(-1+s,2))*(-13+(40)*(s))+(90)*((-1+s)*((-2+(5)*(s))*(t)))+(45)*((-11+(20)*(s))*(pow(t,2)))+(350)*(pow(t,3))));
  phis[27] = (2)*((sqrt(11))*(-13+(40)*(pow(s,3))+(24)*(u)+(pow(s,2))*(-93+(390)*(t)+(60)*(u))+(6)*((s)*(11+(-14)*(u)+(t)*(-91+(110)*(t)+(80)*(u))))
             +(3)*((t)*(52+(-88)*(u)+(t)*(-121+(70)*(t)+(140)*(u))))));
  phis[28] = (2)*((sqrt(55/3.))*(-13+(40)*(pow(s,3))+(70)*(pow(t,3))+(72)*(u)+(-66)*(pow(u,2))+(15)*((pow(t,2))*(-11+(28)*(u)))
             +(12)*((t)*((-1+u)*(-9+(35)*(u))))+(3)*((pow(s,2))*(-31+(90)*(t)+(60)*(u)))+(6)*((s)*(11+(t)*(-63+(50)*(t))+(-42)*(u)+(160)*((t)*(u))
             +(20)*(pow(u,2))))));
  phis[29] = (2)*((sqrt(77/3.))*((pow(-1+s+t,2))*(-13+(40)*(s)+(10)*(t))+(24)*((-1+s+t)*((-6+(15)*(s)+(5)*(t))*(u)))
             +(30)*((-11+(20)*(s)+(10)*(t))*(pow(u,2)))+(200)*(pow(u,3))));
  phis[30] = (4)*((sqrt(55/3.))*(pow(-1+s,3)+(18)*((pow(-1+s,2))*(t))+(63)*((-1+s)*(pow(t,2)))+(56)*(pow(t,3))));
  phis[31] = (2)*((sqrt(55))*(-2+(2)*(pow(s,3))+(3)*(u)+(3)*((pow(s,2))*(-2+(11)*(t)+u))+(3)*((t)*(11+(-14)*(u)+(7)*((t)*(-5+(4)*(t)+(4)*(u)))))
             +(3)*((s)*(2+(-2)*(u)+(t)*(-22+(35)*(t)+(14)*(u))))));
  phis[32] = (5)*((sqrt(11/3.))*((2)*((pow(-1+s,2)+(16)*((-1+s)*(t))+(36)*(pow(t,2)))*(-1+s+t+(3)*(u)))+(2)*((-1+s+(8)*(t))*(pow(-1+s+t,2)
             +(6)*((-1+s+t)*(u))+(6)*(pow(u,2))))));
  phis[33] = (4)*((sqrt(385/3.))*((pow(-1+s+t,2))*(-1+s+(7)*(t))+(3)*((-1+s+t)*((-3+(3)*(s)+(19)*(t))*(u)))
             +(15)*((-1+s+(5)*(t))*(pow(u,2)))+(5)*(pow(u,3))));
  phis[34] = (4)*((sqrt(165))*(pow(-1+s+t,3)+(15)*((pow(-1+s+t,2))*(u))+(45)*((-1+s+t)*(pow(u,2)))+(35)*(pow(u,3))));

  phit[20] = 0;
  phit[21] = (sqrt(66))*(-1+(12)*((s)*(2+(s)*(-9+(10)*(s)))));
  phit[22] = (sqrt(22))*(-1+(12)*((s)*(2+(s)*(-9+(10)*(s)))));
  phit[23] = (4)*((sqrt(11))*((1+(9)*((s)*(-2+(5)*(s))))*(-2+(2)*(s)+(5)*(t))));
  phit[24] = (2)*((sqrt(33))*((1+(9)*((s)*(-2+(5)*(s))))*(-3+(3)*(s)+(5)*(t)+(5)*(u))));
  phit[25] = (2)*((sqrt(55))*((1+(9)*((s)*(-2+(5)*(s))))*(-1+s+t+(3)*(u))));
  phit[26] = (10)*((sqrt(33))*((-1+(10)*(s))*(pow(-1+s,2)+(6)*((-1+s)*(t))+(7)*(pow(t,2)))));
  phit[27] = (2)*((sqrt(11))*((-1+(10)*(s))*(13+(13)*(pow(s,2))+(-24)*(u)+(s)*(-26+(66)*(t)+(24)*(u))+(3)*((t)*(-22+(21)*(t)+(28)*(u))))));
  phit[28] = (2)*((sqrt(165))*((-1+(10)*(s))*(3+(3)*(pow(s,2))+(7)*(pow(t,2))+(2)*((u)*(-8+(7)*(u)))
             +(2)*((s)*(-3+(5)*(t)+(8)*(u)))+(2)*((t)*(-5+(14)*(u))))));
  phit[29] = (2)*((sqrt(231))*((-1+(10)*(s))*(pow(-1+s+t,2)+(8)*((-1+s+t)*(u))+(10)*(pow(u,2)))));
  phit[30] = (4)*((sqrt(165))*((2)*(pow(-1+s,3))+(21)*((pow(-1+s,2))*(t))+(56)*((-1+s)*(pow(t,2)))+(42)*(pow(t,3))));
  phit[31] = (sqrt(55))*(-1+pow(s,3)+(21)*(t)+(-84)*(pow(t,2))+(84)*(pow(t,3))+(3)*((pow(s,2))*(-1+(7)*(t)))+(s)*(3+(42)*((t)*(-1+(2)*(t))))
             +(21)*((-1+s+(2)*(t))*((-1+s+(6)*(t))*(-1+s+t+(2)*(u)))));
  phit[32] = (5)*((sqrt(11/3.))*((2)*((pow(-1+s,2)+(16)*((-1+s)*(t))+(36)*(pow(t,2)))*(-1+s+t+(3)*(u)))
             +(8)*((-2+(2)*(s)+(9)*(t))*(pow(-1+s+t,2)+(6)*((-1+s+t)*(u))+(6)*(pow(u,2))))));
  phit[33] = (4)*((sqrt(1155))*((pow(-1+s+t,2))*(-1+s+(3)*(t))+(-1+s+t)*((-11+(11)*(s)+(27)*(t))*(u))
             +(5)*((-5+(5)*(s)+(9)*(t))*(pow(u,2)))+(15)*(pow(u,3))));
  phit[34] = (4)*((sqrt(165))*(pow(-1+s+t,3)+(15)*((pow(-1+s+t,2))*(u))+(45)*((-1+s+t)*(pow(u,2)))+(35)*(pow(u,3))));

  phiu[20] = 0;
  phiu[21] = 0;
  phiu[22] = (2)*((sqrt(22))*(-1+(12)*((s)*(2+(s)*(-9+(10)*(s))))));
  phiu[23] = 0;
  phiu[24] = (2)*((sqrt(33))*((1+(9)*((s)*(-2+(5)*(s))))*(-1+s+(5)*(t))));
  phiu[25] = (6)*((sqrt(55))*((1+(9)*((s)*(-2+(5)*(s))))*(-1+s+t+(2)*(u))));
  phiu[26] = 0;
  phiu[27] = (4)*((sqrt(11))*((-1+(10)*(s))*(pow(-1+s,2)+(12)*((-1+s)*(t))+(21)*(pow(t,2)))));
  phiu[28] = (4)*((sqrt(165))*((-1+(10)*(s))*((-1+s+(7)*(t))*(-1+s+t+(2)*(u)))));
  phiu[29] = (8)*((sqrt(231))*((-1+(10)*(s))*(pow(-1+s+t,2)+(5)*((-1+s+t)*(u))+(5)*(pow(u,2)))));
  phiu[30] = 0;
  phiu[31] = (2)*((sqrt(55))*(pow(-1+s,3)+(21)*((pow(-1+s,2))*(t))+(84)*((-1+s)*(pow(t,2)))+(84)*(pow(t,3))));
  phiu[32] = (10)*((sqrt(33))*((pow(-1+s,2)+(16)*((-1+s)*(t))+(36)*(pow(t,2)))*(-1+s+t+(2)*(u))));
  phiu[33] = (4)*((sqrt(1155))*((-1+s+(9)*(t))*(pow(-1+s+t,2)+(5)*((-1+s+t)*(u))+(5)*(pow(u,2)))));
  phiu[34] = (20)*((sqrt(165))*(pow(-1+s+t,3)+(9)*((pow(-1+s+t,2))*(u))+(21)*((-1+s+t)*(pow(u,2)))+(14)*(pow(u,3))));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Tet::volumeRef, 1./6., 1e-12 );
  BOOST_CHECK_CLOSE( Tet::centerRef[0], 1./4., 1e-12 );
  BOOST_CHECK_CLOSE( Tet::centerRef[1], 1./4., 1e-12 );
  BOOST_CHECK_CLOSE( Tet::centerRef[2], 1./4., 1e-12 );

  BOOST_CHECK( Tet::TopoDim::D == topoDim(eTet) );
  BOOST_CHECK( Tet::NNode == topoNNode(eTet) );

  BOOST_CHECK( BasisFunctionVolumeBase<Tet>::D == 3 );

  BOOST_CHECK( (BasisFunctionVolume<Tet,Legendre,0>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Tet,Legendre,1>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Tet,Legendre,2>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Tet,Legendre,3>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Tet,Legendre,4>::D == 3) );

  BOOST_CHECK( BasisFunctionVolume_Tet_LegendrePMax == 4 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionVolumeBase<Tet>::getBasisFunction(-1, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionVolumeBase<Tet>::getBasisFunction( BasisFunctionVolume_Tet_LegendrePMax+1, BasisFunctionCategory_Legendre),
                     DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP0 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(0, BasisFunctionCategory_Legendre),
                     BasisFunctionVolumeBase<Tet>::LegendreP0 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolume<Tet,Legendre,0>::self();

  BOOST_CHECK_EQUAL( 0, basis->order() );
  BOOST_CHECK_EQUAL( 1, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 1, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t, u;
  Real phi[1], phis[1], phit[1], phiu[1];
  Real phiTrue[1], phisTrue[1], phitTrue[1], phiuTrue[1];
  Int4 facesign = {{+1, +1, +1, +1}};

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;  u = 0;
      break;

    case 1:
      s = 1;  t = 0;  u = 0;
      break;

    case 2:
      s = 0;  t = 1;  u = 0;
      break;

    case 3:
      s = 0;  t = 0;  u = 1;
      break;

    case 4:
      s = 1./4.;  t = 1./4.;  u = 1./4.;
      break;

    case 5:
      s = 1./4.;  t = 1./5.;  u = 1./6.;
      break;
    }

    fillTrueValues(0, s, t, u, phiTrue, phisTrue, phitTrue, phiuTrue);

    basis->evalBasis( s, t, u, facesign, phi, 1 );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, 1 );
    for (int k = 0; k < 1; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP1 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(1, BasisFunctionCategory_Legendre),
                     BasisFunctionVolumeBase<Tet>::LegendreP1 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolume<Tet,Legendre,1>::self();

  const int nBasis = 4;

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t, u;

  Real phi[nBasis], phis[nBasis], phit[nBasis], phiu[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis], phiuTrue[nBasis];
  Int4 facesign = {{+1, +1, +1, +1}};

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;  u = 0;
      break;

    case 1:
      s = 1;  t = 0;  u = 0;
      break;

    case 2:
      s = 0;  t = 1;  u = 0;
      break;

    case 3:
      s = 0;  t = 0;  u = 1;
      break;

    case 4:
      s = 1./4.;  t = 1./4.;  u = 1./4.;
      break;

    case 5:
      s = 1./4.;  t = 1./5.;  u = 1./6.;
      break;
    }

    fillTrueValues(1, s, t, u, phiTrue, phisTrue, phitTrue, phiuTrue);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, nBasis );
    for (int k = 0; k < nBasis; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP2 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(2, BasisFunctionCategory_Legendre),
                     BasisFunctionVolumeBase<Tet>::LegendreP2 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolume<Tet,Legendre,2>::self();

  const int nBasis = 10;

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t, u;

  Real phi[nBasis], phis[nBasis], phit[nBasis], phiu[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis], phiuTrue[nBasis];
  Int4 facesign = {{+1, +1, +1, +1}};

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;  u = 0;
      break;

    case 1:
      s = 1;  t = 0;  u = 0;
      break;

    case 2:
      s = 0;  t = 1;  u = 0;
      break;

    case 3:
      s = 0;  t = 0;  u = 1;
      break;

    case 4:
      s = 1./4.;  t = 1./4.;  u = 1./4.;
      break;

    case 5:
      s = 1./4.;  t = 1./5.;  u = 1./6.;
      break;
    }

    fillTrueValues(2, s, t, u, phiTrue, phisTrue, phitTrue, phiuTrue);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, nBasis );
    for (int k = 0; k < nBasis; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP3 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-12;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(3, BasisFunctionCategory_Legendre),
                     BasisFunctionVolumeBase<Tet>::LegendreP3 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolume<Tet,Legendre,3>::self();

  const int nBasis = 20;

  BOOST_CHECK_EQUAL( 3, basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t, u;

  Real phi[nBasis], phis[nBasis], phit[nBasis], phiu[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis], phiuTrue[nBasis];
  Int4 facesign = {{+1, +1, +1, +1}};

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;  u = 0;
      break;

    case 1:
      s = 1;  t = 0;  u = 0;
      break;

    case 2:
      s = 0;  t = 1;  u = 0;
      break;

    case 3:
      s = 0;  t = 0;  u = 1;
      break;

    case 4:
      s = 1./4.;  t = 1./4.;  u = 1./4.;
      break;

    case 5:
      s = 1./4.;  t = 1./5.;  u = 1./6.;
      break;
    }

    fillTrueValues(3, s, t, u, phiTrue, phisTrue, phitTrue, phiuTrue);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, nBasis );
    for (int k = 0; k < nBasis; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP4 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 5e-11;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(4, BasisFunctionCategory_Legendre),
                     BasisFunctionVolumeBase<Tet>::LegendreP4 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolume<Tet,Legendre,4>::self();

  const int nBasis = 35;

  BOOST_CHECK_EQUAL( 4, basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t, u;

  Real phi[nBasis], phis[nBasis], phit[nBasis], phiu[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis], phiuTrue[nBasis];
  Int4 facesign = {{+1, +1, +1, +1}};

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;  u = 0;
      break;

    case 1:
      s = 1;  t = 0;  u = 0;
      break;

    case 2:
      s = 0;  t = 1;  u = 0;
      break;

    case 3:
      s = 0;  t = 0;  u = 1;
      break;

    case 4:
      s = 1./4.;  t = 1./4.;  u = 1./4.;
      break;

    case 5:
      s = 1./4.;  t = 1./5.;  u = 1./6.;
      break;
    }

    fillTrueValues(4, s, t, u, phiTrue, phisTrue, phitTrue, phiuTrue);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, nBasis );
    for (int k = 0; k < nBasis; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      BOOST_CHECK_CLOSE( phiuTrue[k], phiu[k], tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP0 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-13;

  int order;
  int nquad;
  Real s, t, u;
  Real phi[1];
  Real w, sum;
  Int4 facesign = {{+1, +1, +1, +1}};

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::LegendreP0;

  order = 2*0;
  QuadratureVolume<Tet> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  sum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t, u);
    quad.weight(n, w);

    basis->evalBasis( s, t, u, facesign, phi, 1 );

    sum += w*phi[0]*phi[0];
  }

  BOOST_CHECK_CLOSE( 1.0, sum, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP1 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-13;

  int order;
  int nquad;
  Real s, t, u;
  Real phi[4];
  Real w, sum[10];
  Int4 facesign = {{+1, +1, +1, +1}};

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::LegendreP1;

  order = 2*1;
  QuadratureVolume<Tet> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < 10; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t, u);
    quad.weight(n, w);

    basis->evalBasis( s, t, u, facesign, phi, 4 );

    sum[0] += w*phi[0]*phi[0];
    sum[1] += w*phi[1]*phi[1];
    sum[2] += w*phi[2]*phi[2];
    sum[3] += w*phi[3]*phi[3];
    sum[4] += w*phi[0]*phi[1];
    sum[5] += w*phi[0]*phi[2];
    sum[6] += w*phi[0]*phi[3];
    sum[7] += w*phi[1]*phi[2];
    sum[8] += w*phi[1]*phi[3];
    sum[9] += w*phi[2]*phi[3];
  }

  BOOST_CHECK_CLOSE( sum[0], 1.0, tol );
  BOOST_CHECK_CLOSE( sum[1], 1.0, tol );
  BOOST_CHECK_CLOSE( sum[2], 1.0, tol );
  BOOST_CHECK_CLOSE( sum[3], 1.0, tol );
  BOOST_CHECK_SMALL( sum[4], tol );
  BOOST_CHECK_SMALL( sum[5], tol );
  BOOST_CHECK_SMALL( sum[6], tol );
  BOOST_CHECK_SMALL( sum[7], tol );
  BOOST_CHECK_SMALL( sum[8], tol );
  BOOST_CHECK_SMALL( sum[9], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP2 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-12;

  int order;
  int nquad;
  const int nBasis = 10;
  Real s, t, u;
  Real phi[nBasis];
  const int nSum = nBasis*(nBasis+1)/2;
  Real w, sum[nSum];
  Int4 facesign = {{+1, +1, +1, +1}};

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::LegendreP2;

  order = 2*2;
  QuadratureVolume<Tet> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nSum; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t, u);
    quad.weight(n, w);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );

    int m = 0;
    for (int i = 0; i < nBasis; i++)
      for (int j = i; j < nBasis; j++)
      {
        sum[m] += w*phi[i]*phi[j];
        m++;
      }
  }

  int m = 0;
  for (int i = 0; i < nBasis; i++)
    for (int j = i; j < nBasis; j++)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( sum[m], 1.0, tol );
      else
        BOOST_CHECK_SMALL( sum[m], tol );
//      std::cout << m << ", " << i << ", " << j << " : " << sum[m] << std::endl;
      m++;
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP3 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-12;

  int order;
  int nquad;
  const int nBasis = 20;
  Real s, t, u;
  Real phi[nBasis];
  const int nSum = nBasis*(nBasis+1)/2;
  Real w, sum[nSum];
  Int4 facesign = {{+1, +1, +1, +1}};

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::LegendreP3;

  order = 2*3;
  QuadratureVolume<Tet> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nSum; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t, u);
    quad.weight(n, w);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );

    int m = 0;
    for (int i = 0; i < nBasis; i++)
      for (int j = i; j < nBasis; j++)
      {
        sum[m] += w*phi[i]*phi[j];
        m++;
      }
  }

  int m = 0;
  for (int i = 0; i < nBasis; i++)
    for (int j = i; j < nBasis; j++)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( sum[m], 1.0, tol );
      else
        BOOST_CHECK_SMALL( sum[m], tol );
//      std::cout << m << ", " << i << ", " << j << " : " << sum[m] << std::endl;
      m++;
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP4 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-12;

  int order;
  int nquad;
  const int nBasis = 35;
  Real s, t, u;
  Real phi[nBasis];
  const int nSum = nBasis*(nBasis+1)/2;
  Real w, sum[nSum];
  Int4 facesign = {{+1, +1, +1, +1}};

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::LegendreP4;

  order = 2*4;
  QuadratureVolume<Tet> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nSum; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t, u);
    quad.weight(n, w);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );

    int m = 0;
    for (int i = 0; i < nBasis; i++)
      for (int j = i; j < nBasis; j++)
      {
        sum[m] += w*phi[i]*phi[j];
        m++;
      }
  }

  int m = 0;
  for (int i = 0; i < nBasis; i++)
    for (int j = i; j < nBasis; j++)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( sum[m], 1.0, tol );
      else
        BOOST_CHECK_SMALL( sum[m], tol );
//      std::cout << m << ", " << i << ", " << j << " : " << sum[m] << std::endl;
      m++;
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionVolume_Tetrahedron_Legendre_pattern.txt", true );

  BasisFunctionVolumeBase<Tet>::getBasisFunction(0, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionVolumeBase<Tet>::getBasisFunction(1, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionVolumeBase<Tet>::getBasisFunction(2, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionVolumeBase<Tet>::getBasisFunction(3, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionVolumeBase<Tet>::getBasisFunction(4, BasisFunctionCategory_Legendre)->dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
