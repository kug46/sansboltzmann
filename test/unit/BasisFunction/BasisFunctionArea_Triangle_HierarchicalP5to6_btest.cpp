// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionArea_Triangle_HierarchicalP5to6_btest
// testing of BasisFunctionArea<Triangle,Hierarchical> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Hierarchical.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;


namespace       // auxilliary copy functions
{

//----------------------------------------------------------------------------//
template <int nB, int nO>
void fillTrue1( int order, Real phiTrue[nB], Real phisTrue[nB], Real phitTrue[nB],
                Real phissTrue[nB], Real phistTrue[nB], Real phittTrue[nB],
                const Real phi[nO], const Real phis[nO], const Real phit[nO],
                const Real phiss[nO], const Real phist[nO], const Real phitt[nO] )
{
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nOrder = 3;
  int k;

  //cout << "fillTrue1: nBasis = " << nBasis << " nB = " << nB << " nOrder = " << nOrder << " nO = " << nO << endl;
  BOOST_REQUIRE_EQUAL( nBasis, nB );
  BOOST_REQUIRE_EQUAL( nOrder, nO );

  for (int ii = 0; ii < 3; ii++)
  {
    k = ii;
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }
}


//----------------------------------------------------------------------------//
template <int nB, int nO>
void fillTrue2( int order, Real phiTrue[nB], Real phisTrue[nB], Real phitTrue[nB],
                Real phissTrue[nB], Real phistTrue[nB], Real phittTrue[nB],
                const Real phi[nO], const Real phis[nO], const Real phit[nO],
                const Real phiss[nO], const Real phist[nO], const Real phitt[nO] )
{
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nOrder = 3;
  const int nEdge  = 3*(order - 1);
  int k;

  BOOST_REQUIRE_EQUAL( nBasis, nB );
  BOOST_REQUIRE_EQUAL( nOrder, nO );

  for (int ii = 0; ii < 3; ii++)
  {
    k = 3 + (nEdge/3)*ii;
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }
}


//----------------------------------------------------------------------------//
template <int nB, int nO>
void fillTrue3( int order, Real phiTrue[nB], Real phisTrue[nB], Real phitTrue[nB],
                Real phissTrue[nB], Real phistTrue[nB], Real phittTrue[nB],
                const Real phi[nO], const Real phis[nO], const Real phit[nO],
                const Real phiss[nO], const Real phist[nO], const Real phitt[nO] )
{
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nOrder = 4;
  const int nEdge  = 3*(order - 1);
  int k;

  BOOST_REQUIRE_EQUAL( nBasis, nB );
  BOOST_REQUIRE_EQUAL( nOrder, nO );

  // edge basis functions
  for (int ii = 0; ii < 3; ii++)
  {
    k = 3 + (nEdge/3)*ii + 1;
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }

  // interior basis function
  k = 3 + nEdge;
  phiTrue[k] = phi[3];  phisTrue[k] = phis[3];  phitTrue[k] = phit[3];
  phissTrue[k] = phiss[3];  phistTrue[k] = phist[3];  phittTrue[k] = phitt[3];
}


//----------------------------------------------------------------------------//
template <int nB, int nO>
void fillTrue4( int order, Real phiTrue[nB], Real phisTrue[nB], Real phitTrue[nB],
                Real phissTrue[nB], Real phistTrue[nB], Real phittTrue[nB],
                const Real phi[nO], const Real phis[nO], const Real phit[nO],
                const Real phiss[nO], const Real phist[nO], const Real phitt[nO] )
{
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nOrder = 5;
  const int nEdge  = 3*(order - 1);
  int k;

  BOOST_REQUIRE_EQUAL( nBasis, nB );
  BOOST_REQUIRE_EQUAL( nOrder, nO );

  // edge basis functions
  for (int ii = 0; ii < 3; ii++)
  {
    k = 3 + (nEdge/3)*ii + 2;
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }

  // interior basis functions
  for (int ii = 3; ii < 5; ii++)
  {
    k = 3 + nEdge + 1 + (ii - 3);
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }
}


//----------------------------------------------------------------------------//
template <int nB, int nO>
void fillTrue5( int order, Real phiTrue[nB], Real phisTrue[nB], Real phitTrue[nB],
                Real phissTrue[nB], Real phistTrue[nB], Real phittTrue[nB],
                const Real phi[nO], const Real phis[nO], const Real phit[nO],
                const Real phiss[nO], const Real phist[nO], const Real phitt[nO] )
{
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nOrder = 6;
  const int nEdge  = 3*(order - 1);
  int k;

  BOOST_REQUIRE_EQUAL( nBasis, nB );
  BOOST_REQUIRE_EQUAL( nOrder, nO );

  // edge basis functions
  for (int ii = 0; ii < 3; ii++)
  {
    k = 3 + (nEdge/3)*ii + 3;
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }

  // interior basis functions
  for (int ii = 3; ii < 6; ii++)
  {
    k = 3 + nEdge + 3 + (ii - 3);
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }
}


//----------------------------------------------------------------------------//
template <int nB, int nO>
void fillTrue6( int order, Real phiTrue[nB], Real phisTrue[nB], Real phitTrue[nB],
                Real phissTrue[nB], Real phistTrue[nB], Real phittTrue[nB],
                const Real phi[nO], const Real phis[nO], const Real phit[nO],
                const Real phiss[nO], const Real phist[nO], const Real phitt[nO] )
{
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nOrder = 7;
  const int nEdge  = 3*(order - 1);
  int k;

  BOOST_CHECK( (nBasis == nB) && (nOrder == nO) );

  // edge basis functions
  for (int ii = 0; ii < 3; ii++)
  {
    k = 3 + (nEdge/3)*ii + 4;
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }

  // interior basis functions
  for (int ii = 3; ii < 7; ii++)
  {
    k = 3 + nEdge + 6 + (ii - 3);
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }
}

}     // auxilliary copy functions


//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionArea_TriangleP5to6_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Triangle::areaRef, 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Triangle::centerRef[0], 1./3., 1e-12 );
  BOOST_CHECK_CLOSE( Triangle::centerRef[1], 1./3., 1e-12 );


  BOOST_CHECK( BasisFunctionAreaBase<Triangle>::D == 2 );

  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,1>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,2>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,3>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,4>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,5>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,6>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,7>::D == 2) );

  BOOST_CHECK( BasisFunctionArea_Triangle_HierarchicalPMax == 7 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Triangle>::getBasisFunction( 0, BasisFunctionCategory_Hierarchical ), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Triangle>::getBasisFunction( 8, BasisFunctionCategory_Hierarchical ), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP5 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 5;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Triangle>::HierarchicalP5 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::HierarchicalP5;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiss[nBasis], phist[nBasis], phitt[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Real phissTrue[nBasis], phistTrue[nBasis], phittTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};

  {
  s = 0;  t = 0;
  Real   phi1[3] = { 1,  0,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 0,  0,  4};
  Real  phit2[3] = { 0,  4,  0};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = { 0,          0,           0,  0};
  Real  phis3[4] = { 0,          0,   6*sqrt(3),  0};
  Real  phit3[4] = { 0, -6*sqrt(3),           0,  0};
  Real phiss3[4] = { 0,          0, -36*sqrt(3),  0};
  Real phist3[4] = { 0, 12*sqrt(3), -12*sqrt(3), 27};
  Real phitt3[4] = { 0, 36*sqrt(3),           0,  0};

  Real   phi4[5] = { 0,  0,  0,               0,               0};
  Real  phis4[5] = { 0,  0,  0,               0,               0};
  Real  phit4[5] = { 0,  0,  0,               0,               0};
  Real phiss4[5] = { 0,  0, 32,               0,               0};
  Real phist4[5] = { 0,  0,  0, 512/(3*sqrt(3)), 512/(3*sqrt(3))};
  Real phitt4[5] = { 0, 32,  0,               0,               0};

  Real   phi5[6] = { 0,           0,          0, 0, 0, 0};
  Real  phis5[6] = { 0,           0,          0, 0, 0, 0};
  Real  phit5[6] = { 0,           0,          0, 0, 0, 0};
  Real phiss5[6] = { 0,           0, 50*sqrt(5), 0, 0, 0};
  Real phist5[6] = { 0,           0,          0, 0, 0, 0};
  Real phitt5[6] = { 0, -50*sqrt(5),          0, 0, 0, 0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );
  fillTrue5<nBasis,6>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi5, phis5, phit5, phiss5, phist5, phitt5 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

#if 0
  for (int i = 0; i < nBasis; i++)
    cout << " (" << i << "," << phistTrue[i] << "," << phist[i] << "," << phistTrue[i] - phist[i] << ")" << endl;
#endif
  }

  {
  s = 1;  t = 0;
  Real   phi1[3] = { 0,  1,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 0,  0, -4};
  Real  phit2[3] = { 4,  0, -4};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {          0, 0,          0,   0};
  Real  phis3[4] = {          0, 0,  6*sqrt(3),   0};
  Real  phit3[4] = {  6*sqrt(3), 0,  6*sqrt(3),   0};
  Real phiss3[4] = {          0, 0, 36*sqrt(3),   0};
  Real phist3[4] = { 12*sqrt(3), 0, 24*sqrt(3), -27};
  Real phitt3[4] = {-12*sqrt(3), 0, 12*sqrt(3), -54};

  Real   phi4[5] = { 0, 0,  0,                0, 0};
  Real  phis4[5] = { 0, 0,  0,                0, 0};
  Real  phit4[5] = { 0, 0,  0,                0, 0};
  Real phiss4[5] = { 0, 0, 32,                0, 0};
  Real phist4[5] = { 0, 0, 32,  512/(3*sqrt(3)), 0};
  Real phitt4[5] = {32, 0, 32, 1024/(3*sqrt(3)), 0};

  Real   phi5[6] = {         0, 0,           0, 0, 0, 0};
  Real  phis5[6] = {         0, 0,           0, 0, 0, 0};
  Real  phit5[6] = {         0, 0,           0, 0, 0, 0};
  Real phiss5[6] = {         0, 0, -50*sqrt(5), 0, 0, 0};
  Real phist5[6] = {         0, 0, -50*sqrt(5), 0, 0, 0};
  Real phitt5[6] = {50*sqrt(5), 0, -50*sqrt(5), 0, 0, 0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );
  fillTrue5<nBasis,6>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi5, phis5, phit5, phiss5, phist5, phitt5 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0;  t = 1;
  Real   phi1[3] = { 0,  0,  1};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 4, -4,  0};
  Real  phit2[3] = { 0, -4,  0};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {          0,           0, 0,   0};
  Real  phis3[4] = { -6*sqrt(3),  -6*sqrt(3), 0,   0};
  Real  phit3[4] = {          0,  -6*sqrt(3), 0,   0};
  Real phiss3[4] = { 12*sqrt(3), -12*sqrt(3), 0, -54};
  Real phist3[4] = {-12*sqrt(3), -24*sqrt(3), 0, -27};
  Real phitt3[4] = {          0, -36*sqrt(3), 0,   0};

  Real   phi4[5] = { 0,  0, 0, 0,                0};
  Real  phis4[5] = { 0,  0, 0, 0,                0};
  Real  phit4[5] = { 0,  0, 0, 0,                0};
  Real phiss4[5] = {32, 32, 0, 0, 1024/(3*sqrt(3))};
  Real phist4[5] = { 0, 32, 0, 0,  512/(3*sqrt(3))};
  Real phitt4[5] = { 0, 32, 0, 0,                0};

  Real   phi5[6] = {          0,          0, 0, 0, 0, 0};
  Real  phis5[6] = {          0,          0, 0, 0, 0, 0};
  Real  phit5[6] = {          0,          0, 0, 0, 0, 0};
  Real phiss5[6] = {-50*sqrt(5), 50*sqrt(5), 0, 0, 0, 0};
  Real phist5[6] = {          0, 50*sqrt(5), 0, 0, 0, 0};
  Real phitt5[6] = {          0, 50*sqrt(5), 0, 0, 0, 0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );
  fillTrue5<nBasis,6>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi5, phis5, phit5, phiss5, phist5, phitt5 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 1./3.;  t = 1./3.;
  Real   phi1[3] = { 1/3.,  1/3.,  1/3.};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 4/9.,  4/9.,   4/9.};
  Real  phis2[3] = { 4/3., -4/3.,      0};
  Real  phit2[3] = { 4/3.,     0,  -4/3.};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {         0,          0,          0,   1};
  Real  phis3[4] = { 2/sqrt(3),  2/sqrt(3), -4/sqrt(3),   0};
  Real  phit3[4] = {-2/sqrt(3),  4/sqrt(3), -2/sqrt(3),   0};
  Real phiss3[4] = { 4*sqrt(3), -4*sqrt(3),          0, -18};
  Real phist3[4] = {         0, -4*sqrt(3),  4*sqrt(3),  -9};
  Real phitt3[4] = {-4*sqrt(3),          0,  4*sqrt(3), -18};

  Real   phi4[5] = { 16/81., 16/81., 16/81., 0, 0};
  Real  phis4[5] = { 32/27., -32/27., 0, -1024/(81*sqrt(3)),  -512/(81*sqrt(3))};
  Real  phit4[5] = { 32/27., 0, -32/27.,  -512/(81*sqrt(3)), -1024/(81*sqrt(3))};
  Real phiss4[5] = { 32/9.,  32/9., -64/9., 0, 0};
  Real phist4[5] = { 64/9., -32/9., -32/9., 0, 0};
  Real phitt4[5] = { 32/9., -64/9.,  32/9., 0, 0};

  Real   phi5[6] = {               0,                0,               0,  3125/3888.,  3125/3888.,  3125/3888.};
  Real  phis5[6] = {  25*sqrt(5)/81.,   25*sqrt(5)/81., -50*sqrt(5)/81.,  3125/1296., -3125/1296.,           0};
  Real  phit5[6] = { -25*sqrt(5)/81.,   50*sqrt(5)/81., -25*sqrt(5)/81.,  3125/1296.,           0, -3125/1296.};
  Real phiss5[6] = { 100*sqrt(5)/27., -100*sqrt(5)/27.,               0, -3125/216. , -3125/216. , -3125/108. };
  Real phist5[6] = {               0, -100*sqrt(5)/27., 100*sqrt(5)/27.,           0, -3125/216. , -3125/216. };
  Real phitt5[6] = {-100*sqrt(5)/27.,                0, 100*sqrt(5)/27., -3125/216. , -3125/108. , -3125/216. };

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );
  fillTrue5<nBasis,6>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi5, phis5, phit5, phiss5, phist5, phitt5 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0.2;  t = 0.7;
  Real   phi1[3] = { 0.1, 0.2, 0.7};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0.56,  0.28,  0.08};
  Real  phis2[3] = { 2.8 , -2.8 , -0.4 };
  Real  phit2[3] = { 0.8,  -2.4 , -0.8 };
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {-21*sqrt(3)/50.,   63*sqrt(3)/250., -3*sqrt(3)/250.,  189/500.};
  Real  phis3[4] = {-63*sqrt(3)/50.,  -21*sqrt(3)/10. , -9*sqrt(3)/50. , -189/100.};
  Real  phit3[4] = {-36*sqrt(3)/25.,  -33*sqrt(3)/25. ,               0,  -81/25. };
  Real phiss3[4] = { 42*sqrt(3)/5. ,  -42*sqrt(3)/5.  , 18*sqrt(3)/5.  , -189/5.  };
  Real phist3[4] = { -6*sqrt(3)    ,  -78*sqrt(3)/5.  , 18*sqrt(3)/5.  , -108/5.  };
  Real phitt3[4] = {-12*sqrt(3)/5. , -108*sqrt(3)/5.  , 12*sqrt(3)/5.  ,  -54/5.  };

  Real   phi4[5] = {196/625.,   49/625.,   4/625., -448/(1875*sqrt(3)),  -896/(625*sqrt(3))};
  Real  phis4[5] = {392/125., -196/125.,  -8/125., -448/( 125*sqrt(3)),  1792/(375*sqrt(3))};
  Real  phit4[5] = {112/125., -168/125., -16/125., -128/( 375*sqrt(3)),  2816/(375*sqrt(3))};
  Real phiss4[5] = {392/25. ,  392/25. , -24/25. , 1792/(  25*sqrt(3)), 12544/( 75*sqrt(3))};
  Real phist4[5] = {224/25. ,   56/5.  ,        0, 1664/(  25*sqrt(3)),  9472/( 75*sqrt(3))};
  Real phitt4[5] = { 32/25. ,  176/25. ,  32/25. , 3584/(  75*sqrt(3)),    1024*sqrt(3)/25.};

  Real   phi5[6] = { -49/(40*sqrt(5))  ,  147/(400*sqrt(5))  , -1/(200*sqrt(5))  ,    49/128.,   49/256.,    7/128.};
  Real  phis5[6] = { -49/( 5*sqrt(5))  , -539/( 80*sqrt(5))  , -1/( 20*sqrt(5))  ,          0, -735/256.,  -35/64. };
  Real  phit5[6] = {-119/(20*sqrt(5))  , -203/( 40*sqrt(5))  ,  1/( 20*sqrt(5))  ,  -175/64. , -105/32. ,  -65/64. };
  Real phiss5[6] = { -49/( 4*sqrt(5))  ,   49/(    sqrt(5))  , 11/(  4*sqrt(5))  , -3675/64. ,         0, -525/64. };
  Real phist5[6] = {     -21*sqrt(5)/2.,         7*sqrt(5)/2.,         sqrt(5)/2., -1225/32. ,  175/16. ,  -25/32. };
  Real phitt5[6] = { -19/(   sqrt(5))  ,   -9/(    sqrt(5))  ,  1/(    sqrt(5))  ,  -325/16. ,  275/16. ,  125/16. };

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );
  fillTrue5<nBasis,6>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi5, phis5, phit5, phiss5, phist5, phitt5 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

#if 0
  for (int i = 0; i < nBasis; i++)
    cout << " (" << i << "," << phiTrue[i] << "," << phi[i] << ", " << phiTrue[i] - phi[i] << ")" << endl;
#endif

  // check sign change for asymmetric basis functions

  int i;

  edgesign[0] = -1;
  i = 4;
  for (int n = 0; n < 2; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[1] = -1;
  i = 4 + (order - 1);
  for (int n = 0; n < 2; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[2] = -1;
  i = 4 + 2*(order - 1);
  for (int n = 0; n < 2; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP6 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 6;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Triangle>::HierarchicalP6 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::HierarchicalP6;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiss[nBasis], phist[nBasis], phitt[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Real phissTrue[nBasis], phistTrue[nBasis], phittTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};

  {
  s = 0;  t = 0;
  Real   phi1[3] = { 1,  0,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 0,  0,  4};
  Real  phit2[3] = { 0,  4,  0};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = { 0,          0,           0,  0};
  Real  phis3[4] = { 0,          0,   6*sqrt(3),  0};
  Real  phit3[4] = { 0, -6*sqrt(3),           0,  0};
  Real phiss3[4] = { 0,          0, -36*sqrt(3),  0};
  Real phist3[4] = { 0, 12*sqrt(3), -12*sqrt(3), 27};
  Real phitt3[4] = { 0, 36*sqrt(3),           0,  0};

  Real   phi4[5] = { 0,  0,  0,               0,               0};
  Real  phis4[5] = { 0,  0,  0,               0,               0};
  Real  phit4[5] = { 0,  0,  0,               0,               0};
  Real phiss4[5] = { 0,  0, 32,               0,               0};
  Real phist4[5] = { 0,  0,  0, 512/(3*sqrt(3)), 512/(3*sqrt(3))};
  Real phitt4[5] = { 0, 32,  0,               0,               0};

  Real   phi5[6] = { 0,           0,          0, 0, 0, 0};
  Real  phis5[6] = { 0,           0,          0, 0, 0, 0};
  Real  phit5[6] = { 0,           0,          0, 0, 0, 0};
  Real phiss5[6] = { 0,           0, 50*sqrt(5), 0, 0, 0};
  Real phist5[6] = { 0,           0,          0, 0, 0, 0};
  Real phitt5[6] = { 0, -50*sqrt(5),          0, 0, 0, 0};

  Real   phi6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real  phis6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real  phit6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real phiss6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real phist6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real phitt6[7] = { 0, 0, 0, 0, 0, 0, 0};

#if 0
  for (int ii = 0; ii < 3; ii++)
  {
    k = ii;
    phiTrue[k] = phi1[ii];  phisTrue[k] = phis1[ii];  phitTrue[k] = phit1[ii];
    phissTrue[k] = phiss1[ii];  phistTrue[k] = phist1[ii];  phittTrue[k] = phitt1[ii];

    k = 3 + (nEdge/3)*ii;
    phiTrue[k] = phi2[ii];  phisTrue[k] = phis2[ii];  phitTrue[k] = phit2[ii];
    phissTrue[k] = phiss2[ii];  phistTrue[k] = phist2[ii];  phittTrue[k] = phitt2[ii];

    k = 3 + (nEdge/3)*ii + 1;
    phiTrue[k] = phi3[ii];  phisTrue[k] = phis3[ii];  phitTrue[k] = phit3[ii];
    phissTrue[k] = phiss3[ii];  phistTrue[k] = phist3[ii];  phittTrue[k] = phitt3[ii];

    k = 3 + (nEdge/3)*ii + 2;
    phiTrue[k] = phi4[ii];  phisTrue[k] = phis4[ii];  phitTrue[k] = phit4[ii];
    phissTrue[k] = phiss4[ii];  phistTrue[k] = phist4[ii];  phittTrue[k] = phitt4[ii];

    k = 3 + (nEdge/3)*ii + 3;
    phiTrue[k] = phi5[ii];  phisTrue[k] = phis5[ii];  phitTrue[k] = phit5[ii];
    phissTrue[k] = phiss5[ii];  phistTrue[k] = phist5[ii];  phittTrue[k] = phitt5[ii];

    k = 3 + (nEdge/3)*ii + 4;
    phiTrue[k] = phi6[ii];  phisTrue[k] = phis6[ii];  phitTrue[k] = phit6[ii];
    phissTrue[k] = phiss6[ii];  phistTrue[k] = phist6[ii];  phittTrue[k] = phitt6[ii];
  }

  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];
  phissTrue[k] = phiss3[3];  phistTrue[k] = phist3[3];  phittTrue[k] = phitt3[3];
  for (int ii = 3; ii < 5; ii++)
  {
    k = 3 + nEdge + 1 + (ii - 3);
    phiTrue[k] = phi4[ii];  phisTrue[k] = phis4[ii];  phitTrue[k] = phit4[ii];
    phissTrue[k] = phiss4[ii];  phistTrue[k] = phist4[ii];  phittTrue[k] = phitt4[ii];
  }
  for (int ii = 3; ii < 6; ii++)
  {
    k = 3 + nEdge + 3 + (ii - 3);
    phiTrue[k] = phi5[ii];  phisTrue[k] = phis5[ii];  phitTrue[k] = phit5[ii];
    phissTrue[k] = phiss5[ii];  phistTrue[k] = phist5[ii];  phittTrue[k] = phitt5[ii];
  }
  for (int ii = 3; ii < 7; ii++)
  {
    k = 3 + nEdge + 6 + (ii - 3);
    phiTrue[k] = phi6[ii];  phisTrue[k] = phis6[ii];  phitTrue[k] = phit6[ii];
    phissTrue[k] = phiss6[ii];  phistTrue[k] = phist6[ii];  phittTrue[k] = phitt6[ii];
  }
#else
  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );
  fillTrue5<nBasis,6>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi5, phis5, phit5, phiss5, phist5, phitt5 );
  fillTrue6<nBasis,7>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi6, phis6, phit6, phiss6, phist6, phitt6 );
#endif

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

#if 0
  for (int i = 0; i < nBasis; i++)
    cout << " (" << i << "," << phistTrue[i] << "," << phist[i] << "," << phistTrue[i] - phist[i] << ")" << endl;
#endif
  }

  {
  s = 1;  t = 0;
  Real   phi1[3] = { 0,  1,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 0,  0, -4};
  Real  phit2[3] = { 4,  0, -4};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {          0, 0,          0,   0};
  Real  phis3[4] = {          0, 0,  6*sqrt(3),   0};
  Real  phit3[4] = {  6*sqrt(3), 0,  6*sqrt(3),   0};
  Real phiss3[4] = {          0, 0, 36*sqrt(3),   0};
  Real phist3[4] = { 12*sqrt(3), 0, 24*sqrt(3), -27};
  Real phitt3[4] = {-12*sqrt(3), 0, 12*sqrt(3), -54};

  Real   phi4[5] = { 0, 0,  0,                0, 0};
  Real  phis4[5] = { 0, 0,  0,                0, 0};
  Real  phit4[5] = { 0, 0,  0,                0, 0};
  Real phiss4[5] = { 0, 0, 32,                0, 0};
  Real phist4[5] = { 0, 0, 32,  512/(3*sqrt(3)), 0};
  Real phitt4[5] = {32, 0, 32, 1024/(3*sqrt(3)), 0};

  Real   phi5[6] = {         0, 0,           0, 0, 0, 0};
  Real  phis5[6] = {         0, 0,           0, 0, 0, 0};
  Real  phit5[6] = {         0, 0,           0, 0, 0, 0};
  Real phiss5[6] = {         0, 0, -50*sqrt(5), 0, 0, 0};
  Real phist5[6] = {         0, 0, -50*sqrt(5), 0, 0, 0};
  Real phitt5[6] = {50*sqrt(5), 0, -50*sqrt(5), 0, 0, 0};

  Real   phi6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real  phis6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real  phit6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real phiss6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real phist6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real phitt6[7] = { 0, 0, 0, 0, 0, 0, 0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );
  fillTrue5<nBasis,6>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi5, phis5, phit5, phiss5, phist5, phitt5 );
  fillTrue6<nBasis,7>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi6, phis6, phit6, phiss6, phist6, phitt6 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0;  t = 1;
  Real   phi1[3] = { 0,  0,  1};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 4, -4,  0};
  Real  phit2[3] = { 0, -4,  0};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {          0,           0, 0,   0};
  Real  phis3[4] = { -6*sqrt(3),  -6*sqrt(3), 0,   0};
  Real  phit3[4] = {          0,  -6*sqrt(3), 0,   0};
  Real phiss3[4] = { 12*sqrt(3), -12*sqrt(3), 0, -54};
  Real phist3[4] = {-12*sqrt(3), -24*sqrt(3), 0, -27};
  Real phitt3[4] = {          0, -36*sqrt(3), 0,   0};

  Real   phi4[5] = { 0,  0, 0, 0,                0};
  Real  phis4[5] = { 0,  0, 0, 0,                0};
  Real  phit4[5] = { 0,  0, 0, 0,                0};
  Real phiss4[5] = {32, 32, 0, 0, 1024/(3*sqrt(3))};
  Real phist4[5] = { 0, 32, 0, 0,  512/(3*sqrt(3))};
  Real phitt4[5] = { 0, 32, 0, 0,                0};

  Real   phi5[6] = {          0,          0, 0, 0, 0, 0};
  Real  phis5[6] = {          0,          0, 0, 0, 0, 0};
  Real  phit5[6] = {          0,          0, 0, 0, 0, 0};
  Real phiss5[6] = {-50*sqrt(5), 50*sqrt(5), 0, 0, 0, 0};
  Real phist5[6] = {          0, 50*sqrt(5), 0, 0, 0, 0};
  Real phitt5[6] = {          0, 50*sqrt(5), 0, 0, 0, 0};

  Real   phi6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real  phis6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real  phit6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real phiss6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real phist6[7] = { 0, 0, 0, 0, 0, 0, 0};
  Real phitt6[7] = { 0, 0, 0, 0, 0, 0, 0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );
  fillTrue5<nBasis,6>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi5, phis5, phit5, phiss5, phist5, phitt5 );
  fillTrue6<nBasis,7>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi6, phis6, phit6, phiss6, phist6, phitt6 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 1./3.;  t = 1./3.;
  Real   phi1[3] = { 1/3.,  1/3.,  1/3.};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 4/9.,  4/9.,   4/9.};
  Real  phis2[3] = { 4/3., -4/3.,      0};
  Real  phit2[3] = { 4/3.,     0,  -4/3.};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {         0,          0,          0,   1};
  Real  phis3[4] = { 2/sqrt(3),  2/sqrt(3), -4/sqrt(3),   0};
  Real  phit3[4] = {-2/sqrt(3),  4/sqrt(3), -2/sqrt(3),   0};
  Real phiss3[4] = { 4*sqrt(3), -4*sqrt(3),          0, -18};
  Real phist3[4] = {         0, -4*sqrt(3),  4*sqrt(3),  -9};
  Real phitt3[4] = {-4*sqrt(3),          0,  4*sqrt(3), -18};

  Real   phi4[5] = { 16/81., 16/81., 16/81., 0, 0};
  Real  phis4[5] = { 32/27., -32/27., 0, -1024/(81*sqrt(3)),  -512/(81*sqrt(3))};
  Real  phit4[5] = { 32/27., 0, -32/27.,  -512/(81*sqrt(3)), -1024/(81*sqrt(3))};
  Real phiss4[5] = { 32/9.,  32/9., -64/9., 0, 0};
  Real phist4[5] = { 64/9., -32/9., -32/9., 0, 0};
  Real phitt4[5] = { 32/9., -64/9.,  32/9., 0, 0};

  Real   phi5[6] = {               0,                0,               0,  3125/3888.,  3125/3888.,  3125/3888.};
  Real  phis5[6] = {  25*sqrt(5)/81.,   25*sqrt(5)/81., -50*sqrt(5)/81.,  3125/1296., -3125/1296.,           0};
  Real  phit5[6] = { -25*sqrt(5)/81.,   50*sqrt(5)/81., -25*sqrt(5)/81.,  3125/1296.,           0, -3125/1296.};
  Real phiss5[6] = { 100*sqrt(5)/27., -100*sqrt(5)/27.,               0, -3125/216. , -3125/216. , -3125/108. };
  Real phist5[6] = {               0, -100*sqrt(5)/27., 100*sqrt(5)/27.,           0, -3125/216. , -3125/216. };
  Real phitt5[6] = {-100*sqrt(5)/27.,                0, 100*sqrt(5)/27., -3125/216. , -3125/108. , -3125/216. };

  Real   phi6[7] = { 64/729.,   64/729.,   64/729.,   1,                  0,                  0,                 0};
  Real  phis6[7] = { 64/81. ,  -64/81. ,         0,   0,   192/(25*sqrt(5)),   192/(25*sqrt(5)), -384/(25*sqrt(5))};
  Real  phit6[7] = { 64/81. ,         0,  -64/81. ,   0,  -192/(25*sqrt(5)),   384/(25*sqrt(5)), -192/(25*sqrt(5))};
  Real phiss6[7] = {128/27. ,  128/27. , -128/27. , -36,  1152/(25*sqrt(5)), -1152/(25*sqrt(5)),                 0};
  Real phist6[7] = { 64/9.  ,  -64/27. ,  -64/27. , -18,                  0, -1152/(25*sqrt(5)), 1152/(25*sqrt(5))};
  Real phitt6[7] = {128/27. , -128/27. ,  128/27. , -36, -1152/(25*sqrt(5)),                  0, 1152/(25*sqrt(5))};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );
  fillTrue5<nBasis,6>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi5, phis5, phit5, phiss5, phist5, phitt5 );
  fillTrue6<nBasis,7>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi6, phis6, phit6, phiss6, phist6, phitt6 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0.2;  t = 0.7;
  Real   phi1[3] = { 0.1, 0.2, 0.7};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0.56,  0.28,  0.08};
  Real  phis2[3] = { 2.8 , -2.8 , -0.4 };
  Real  phit2[3] = { 0.8,  -2.4 , -0.8 };
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {-21*sqrt(3)/50.,   63*sqrt(3)/250., -3*sqrt(3)/250.,  189/500.};
  Real  phis3[4] = {-63*sqrt(3)/50.,  -21*sqrt(3)/10. , -9*sqrt(3)/50. , -189/100.};
  Real  phit3[4] = {-36*sqrt(3)/25.,  -33*sqrt(3)/25. ,               0,  -81/25. };
  Real phiss3[4] = { 42*sqrt(3)/5. ,  -42*sqrt(3)/5.  , 18*sqrt(3)/5.  , -189/5.  };
  Real phist3[4] = { -6*sqrt(3)    ,  -78*sqrt(3)/5.  , 18*sqrt(3)/5.  , -108/5.  };
  Real phitt3[4] = {-12*sqrt(3)/5. , -108*sqrt(3)/5.  , 12*sqrt(3)/5.  ,  -54/5.  };

  Real   phi4[5] = {196/625.,   49/625.,   4/625., -448/(1875*sqrt(3)),  -896/(625*sqrt(3))};
  Real  phis4[5] = {392/125., -196/125.,  -8/125., -448/( 125*sqrt(3)),  1792/(375*sqrt(3))};
  Real  phit4[5] = {112/125., -168/125., -16/125., -128/( 375*sqrt(3)),  2816/(375*sqrt(3))};
  Real phiss4[5] = {392/25. ,  392/25. , -24/25. , 1792/(  25*sqrt(3)), 12544/( 75*sqrt(3))};
  Real phist4[5] = {224/25. ,   56/5.  ,        0, 1664/(  25*sqrt(3)),  9472/( 75*sqrt(3))};
  Real phitt4[5] = { 32/25. ,  176/25. ,  32/25. , 3584/(  75*sqrt(3)),    1024*sqrt(3)/25.};

  Real   phi5[6] = { -49/(40*sqrt(5))  ,  147/(400*sqrt(5))  , -1/(200*sqrt(5))  ,    49/128.,   49/256.,    7/128.};
  Real  phis5[6] = { -49/( 5*sqrt(5))  , -539/( 80*sqrt(5))  , -1/( 20*sqrt(5))  ,          0, -735/256.,  -35/64. };
  Real  phit5[6] = {-119/(20*sqrt(5))  , -203/( 40*sqrt(5))  ,  1/( 20*sqrt(5))  ,  -175/64. , -105/32. ,  -65/64. };
  Real phiss5[6] = { -49/( 4*sqrt(5))  ,   49/(    sqrt(5))  , 11/(  4*sqrt(5))  , -3675/64. ,         0, -525/64. };
  Real phist5[6] = {     -21*sqrt(5)/2.,         7*sqrt(5)/2.,         sqrt(5)/2., -1225/32. ,  175/16. ,  -25/32. };
  Real phitt5[6] = { -19/(   sqrt(5))  ,   -9/(    sqrt(5))  ,  1/(    sqrt(5))  ,  -325/16. ,  275/16. ,  125/16. };

  Real   phi6[7] = {2744/15625.,  343/15625.,  8/15625.,  35721/250000., -142884/(78125*sqrt(5)),   428652/(390625*sqrt(5)), -20412/(390625*sqrt(5))};
  Real  phis6[7] = {8232/3125. , -2058/3125., -24/3125., -35721/25000. ,  285768/(78125*sqrt(5)), -1143072/( 78125*sqrt(5)), -40824/( 78125*sqrt(5))};
  Real  phit6[7] = {2352/3125. , -1764/3125., -48/3125., -15309/6250.  ,  734832/(78125*sqrt(5)), -1183896/( 78125*sqrt(5)),  34992/( 78125*sqrt(5))};
  Real phiss6[7] = {16464/625. ,  8232/625. , -48/625. , -107163/5000. ,  857304/( 3125*sqrt(5)),  -857304/( 15625*sqrt(5)), 449064/( 15625*sqrt(5))};
  Real phist6[7] = { 7056/625. ,  6468/625. ,  48/625. ,  -5103/1250.  ,  489888/( 3125*sqrt(5)),  -367416/( 15625*sqrt(5)), 396576/( 15625*sqrt(5))};
  Real phitt6[7] = { 1344/625. ,  4872/625. , 192/625. ,   8019/625.   ,   93312/(  625*sqrt(5)),  -419904/( 15625*sqrt(5)), 186624/( 15625*sqrt(5))};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );
  fillTrue5<nBasis,6>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi5, phis5, phit5, phiss5, phist5, phitt5 );
  fillTrue6<nBasis,7>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi6, phis6, phit6, phiss6, phist6, phitt6 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  // check sign change for asymmetric basis functions

  int i;

  edgesign[0] = -1;
  i = 4;
  for (int n = 0; n < 2; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[1] = -1;
  i = 4 + (order - 1);
  for (int n = 0; n < 2; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[2] = -1;
  i = 4 + 2*(order - 1);
  for (int n = 0; n < 2; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionArea_Triangle_Hierarchical_pattern.txt", true );

  BasisFunctionAreaBase<Triangle>::getBasisFunction(1, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(2, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(3, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(4, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(5, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(6, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(7, BasisFunctionCategory_Hierarchical)->dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
