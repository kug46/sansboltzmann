// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionArea_Quad_Hierarchical_btest
// testing of BasisFunctionArea<Quad,Hierarchical> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisFunctionArea_Quad_Hierarchical.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionArea_Quad_Hierarchical_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Quad::areaRef, 1., 1e-12 );
  BOOST_CHECK_CLOSE( Quad::centerRef[0], 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Quad::centerRef[1], 1./2., 1e-12 );

  BOOST_CHECK( Quad::TopoDim::D == topoDim(eQuad) );
  BOOST_CHECK( Quad::NNode == topoNNode(eQuad) );

  BOOST_CHECK( BasisFunctionAreaBase<Quad>::D == 2 );

  BOOST_CHECK( (BasisFunctionArea<Quad,Hierarchical,1>::D == 2) );
#if 0
  BOOST_CHECK( (BasisFunctionArea<Quad,Hierarchical,2>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Hierarchical,3>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Hierarchical,4>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Hierarchical,5>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Hierarchical,6>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Hierarchical,7>::D == 2) );
#endif
  BOOST_CHECK( BasisFunctionArea_Quad_HierarchicalPMax == 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Quad>::getBasisFunction( 0, BasisFunctionCategory_Hierarchical ), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Quad>::getBasisFunction(
                            BasisFunctionArea_Quad_HierarchicalPMax+1, BasisFunctionCategory_Hierarchical ), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP1 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(1, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Quad>::HierarchicalP1 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::HierarchicalP1;

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 4, basis->nBasis() );
  BOOST_CHECK_EQUAL( 4, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[4], phis[4], phit[4];
  Real phiss[4], phist[4], phitt[4];
  Real phiTrue[4], phisTrue[4], phitTrue[4];
  Real phissTrue[4], phistTrue[4], phittTrue[4];
  Int4 edgesign = {{+1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 5; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 0;  t = 1;
      break;

    case 4:
      s = 1./2.;  t = 1./2.;
      break;
    }

    phiTrue[0] = (1 - s)*(1 - t);
    phiTrue[1] = s*(1 - t);
    phiTrue[2] = s*t;
    phiTrue[3] = (1 - s)*t;

    phisTrue[0] = -(1 - t);
    phisTrue[1] =  (1 - t);
    phisTrue[2] =  t;
    phisTrue[3] = -t;

    phitTrue[0] = -(1 - s);
    phitTrue[1] = -s;
    phitTrue[2] =  s;
    phitTrue[3] =  (1 - s);

    for (int ki=0;ki<4;ki++)
    {
      phissTrue[ki] = 0;
      phittTrue[ki] = 0;
      phistTrue[ki] = pow(-1.,ki);
    }

    basis->evalBasis( s, t, edgesign, phi, 4 );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, 4 );
    basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, 4 );
    for (k = 0; k < 4; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], tol, tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], tol, tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], tol, tol );
    }
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP2 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(2, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Quad>::HierarchicalP2 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::HierarchicalP2;

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( 6, basis->nBasis() );
  BOOST_CHECK_EQUAL( 3, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 3, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[6], phis[6], phit[6];
  Real phiTrue[6], phisTrue[6], phitTrue[6];
  Int3 edgesign = {{+1, +1, +1}};
  int k;

  for (int test = 0; test < 3; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 1./3.;  t = 1./3.;
      break;
    }

    phiTrue[0] = 1 - s - t;
    phiTrue[1] = s;
    phiTrue[2] = t;
    phiTrue[3] = 4*s*t;
    phiTrue[4] = 4*t*(1 - s - t);
    phiTrue[5] = 4*s*(1 - s - t);

    phisTrue[0] = -1;
    phisTrue[1] =  1;
    phisTrue[2] =  0;
    phisTrue[3] =  4*t;
    phisTrue[4] = -4*t;
    phisTrue[5] =  4*(1 - 2*s - t);

    phitTrue[0] = -1;
    phitTrue[1] =  0;
    phitTrue[2] =  1;
    phitTrue[3] =  4*s;
    phitTrue[4] =  4*(1 - s - 2*t);
    phitTrue[5] = -4*s;

    basis->evalBasis( s, t, edgesign, phi, 6 );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, 6 );
    for (k = 0; k < 6; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    }
  }

  // repeat with data rather than formulas

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  s = 0;  t = 0;
  for (k = 0; k < 6; k++)
  {
    phiTrue[k] = 0;
    phisTrue[k] = 0;
    phitTrue[k] = 0;
  }
  phiTrue[0]  =  1;
  phisTrue[0] = -1;
  phisTrue[1] =  1;
  phisTrue[5] =  4;
  phitTrue[0] = -1;
  phitTrue[2] =  1;
  phitTrue[4] =  4;

  basis->evalBasis( s, t, edgesign, phi, 6 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 6 );
  for (k = 0; k < 6; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
#if 0
  cout << "basis_HierarchicalP2: s = " << s << "  t = " << t << endl;
  for (k = 0; k < 6; k++)
  {
    cout << "  " << k << ":  phi = (" << phiTrue[k] << ", " << phi[k] << ")"
                      << "  phis = (" << phisTrue[k] << ", " << phis[k] << ")"
                      << "  phit = (" << phitTrue[k] << ", " << phit[k] << ")" << endl;
  }
#endif

  s = 1;  t = 0;
  for (k = 0; k < 6; k++)
  {
    phiTrue[k] = 0;
    phisTrue[k] = 0;
    phitTrue[k] = 0;
  }
  phiTrue[1]  =  1;
  phisTrue[0] = -1;
  phisTrue[1] =  1;
  phisTrue[5] = -4;
  phitTrue[0] = -1;
  phitTrue[2] =  1;
  phitTrue[3] =  4;
  phitTrue[5] = -4;

  basis->evalBasis( s, t, edgesign, phi, 6 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 6 );
  for (k = 0; k < 6; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  s = 0;  t = 1;
  for (k = 0; k < 6; k++)
  {
    phiTrue[k] = 0;
    phisTrue[k] = 0;
    phitTrue[k] = 0;
  }
  phiTrue[2]  =  1;
  phisTrue[0] = -1;
  phisTrue[1] =  1;
  phisTrue[3] =  4;
  phisTrue[4] = -4;
  phitTrue[0] = -1;
  phitTrue[2] =  1;
  phitTrue[4] = -4;

  basis->evalBasis( s, t, edgesign, phi, 6 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 6 );
  for (k = 0; k < 6; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  s = 1./3.;  t = 1./3.;
  phiTrue[0] = 1./3.;
  phiTrue[1] = 1./3.;
  phiTrue[2] = 1./3.;
  phiTrue[3] = 4./9.;
  phiTrue[4] = 4./9.;
  phiTrue[5] = 4./9.;
  phisTrue[0] = -1;
  phisTrue[1] =  1;
  phisTrue[2] =  0;
  phisTrue[3] =  4./3.;
  phisTrue[4] = -4./3.;
  phisTrue[5] =  0;
  phitTrue[0] = -1;
  phitTrue[1] =  0;
  phitTrue[2] =  1;
  phitTrue[3] =  4./3.;
  phitTrue[4] =  0;
  phitTrue[5] = -4./3.;

  basis->evalBasis( s, t, edgesign, phi, 6 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 6 );
  for (k = 0; k < 6; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP3 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(3, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Quad>::HierarchicalP3 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::HierarchicalP3;

  BOOST_CHECK_EQUAL(  3, basis->order() );
  BOOST_CHECK_EQUAL( 10, basis->nBasis() );
  BOOST_CHECK_EQUAL(  3, basis->nBasisNode() );
  BOOST_CHECK_EQUAL(  6, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL(  1, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[10], phis[10], phit[10];
  Real phiTrue[10], phisTrue[10], phitTrue[10];
  Int3 edgesign = {{+1, +1, +1}};
  int k;

  s = 0;  t = 0;
  for (k = 0; k < 10; k++)
  {
    phiTrue[k] = 0;
    phisTrue[k] = 0;
    phitTrue[k] = 0;
  }
  phiTrue[0] = 1;
  phisTrue[0] = -1;
  phisTrue[1] =  1;
  phisTrue[7] =  4;
  phisTrue[8] = -6*sqrt(3);
  phitTrue[0] = -1;
  phitTrue[2] =  1;
  phitTrue[5] =  4;
  phitTrue[6] =  6*sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 10 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 10 );
  for (k = 0; k < 10; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
#if 0
  cout << "basis_HierarchicalP3: s = " << s << "  t = " << t << endl;
  for (k = 0; k < 10; k++)
  {
    cout << "  " << k << ":  phi = (" << phiTrue[k] << ", " << phi[k] << ")"
                      << "  phis = (" << phisTrue[k] << ", " << phis[k] << ")"
                      << "  phit = (" << phitTrue[k] << ", " << phit[k] << ")" << endl;
  }
#endif

  s = 1;  t = 0;
  for (k = 0; k < 10; k++)
  {
    phiTrue[k] = 0;
    phisTrue[k] = 0;
    phitTrue[k] = 0;
  }
  phiTrue[1] = 1;
  phisTrue[0] = -1;
  phisTrue[1] =  1;
  phisTrue[7] = -4;
  phisTrue[8] = -6*sqrt(3);
  phitTrue[0] = -1;
  phitTrue[2] =  1;
  phitTrue[3] =  4;
  phitTrue[4] = -6*sqrt(3);
  phitTrue[7] = -4;
  phitTrue[8] = -6*sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 10 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 10 );
  for (k = 0; k < 10; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  s = 0;  t = 1;
  for (k = 0; k < 10; k++)
  {
    phiTrue[k] = 0;
    phisTrue[k] = 0;
    phitTrue[k] = 0;
  }
  phiTrue[2] = 1;
  phisTrue[0] = -1;
  phisTrue[1] =  1;
  phisTrue[3] =  4;
  phisTrue[4] =  6*sqrt(3);
  phisTrue[5] = -4;
  phisTrue[6] =  6*sqrt(3);
  phitTrue[0] = -1;
  phitTrue[2] =  1;
  phitTrue[5] = -4;
  phitTrue[6] =  6*sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 10 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 10 );
  for (k = 0; k < 10; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  s = 1./3.;  t = 1./3.;
  phiTrue[0] = 1./3.;
  phiTrue[1] = 1./3.;
  phiTrue[2] = 1./3.;
  phiTrue[3] = 4./9.;
  phiTrue[4] = 0;
  phiTrue[5] = 4./9.;
  phiTrue[6] = 0;
  phiTrue[7] = 4./9.;
  phiTrue[8] = 0;
  phiTrue[9] = 1;
  phisTrue[0] = -1;
  phisTrue[1] =  1;
  phisTrue[2] =  0;
  phisTrue[3] =  4./3.;
  phisTrue[4] = -2./sqrt(3);
  phisTrue[5] = -4./3.;
  phisTrue[6] = -2./sqrt(3);
  phisTrue[7] =  0;
  phisTrue[8] =  4./sqrt(3);
  phisTrue[9] =  0;
  phitTrue[0] = -1;
  phitTrue[1] =  0;
  phitTrue[2] =  1;
  phitTrue[3] =  4./3.;
  phitTrue[4] =  2./sqrt(3);
  phitTrue[5] =  0;
  phitTrue[6] = -4./sqrt(3);
  phitTrue[7] = -4./3.;
  phitTrue[8] =  2./sqrt(3);
  phitTrue[9] =  0;

  basis->evalBasis( s, t, edgesign, phi, 10 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 10 );
  for (k = 0; k < 10; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  // check sign change for asymmetric basis functions

  edgesign[0] = -1;
  phiTrue[4] = 0;
  phiTrue[6] = 0;
  phiTrue[8] = 0;
  phisTrue[4] =  2./sqrt(3);
  phisTrue[6] = -2./sqrt(3);
  phisTrue[8] =  4./sqrt(3);
  phitTrue[4] = -2./sqrt(3);
  phitTrue[6] = -4./sqrt(3);
  phitTrue[8] =  2./sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 10 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 10 );
  for (k = 4; k <= 8; k += 2)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  edgesign[1] = -1;
  phiTrue[4] = 0;
  phiTrue[6] = 0;
  phiTrue[8] = 0;
  phisTrue[4] =  2./sqrt(3);
  phisTrue[6] =  2./sqrt(3);
  phisTrue[8] =  4./sqrt(3);
  phitTrue[4] = -2./sqrt(3);
  phitTrue[6] =  4./sqrt(3);
  phitTrue[8] =  2./sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 10 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 10 );
  for (k = 4; k <= 8; k += 2)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  edgesign[2] = -1;
  phiTrue[4] = 0;
  phiTrue[6] = 0;
  phiTrue[8] = 0;
  phisTrue[4] =  2./sqrt(3);
  phisTrue[6] =  2./sqrt(3);
  phisTrue[8] = -4./sqrt(3);
  phitTrue[4] = -2./sqrt(3);
  phitTrue[6] =  4./sqrt(3);
  phitTrue[8] = -2./sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 10 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 10 );
  for (k = 4; k <= 8; k += 2)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP4 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(4, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Quad>::HierarchicalP4 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::HierarchicalP4;

  BOOST_CHECK_EQUAL(  4, basis->order() );
  BOOST_CHECK_EQUAL( 15, basis->nBasis() );
  BOOST_CHECK_EQUAL(  3, basis->nBasisNode() );
  BOOST_CHECK_EQUAL(  9, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL(  3, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[15], phis[15], phit[15];
  Real phiTrue[15], phisTrue[15], phitTrue[15];
  Int3 edgesign = {{+1, +1, +1}};
  int k;

  s = 0;  t = 0;
  for (k = 0; k < 15; k++)
  {
    phiTrue[k] = 0;
    phisTrue[k] = 0;
    phitTrue[k] = 0;
  }
  phiTrue[0] = 1;
  phisTrue[ 0] = -1;
  phisTrue[ 1] =  1;
  phisTrue[ 9] =  4;
  phisTrue[10] = -6*sqrt(3);
  phitTrue[ 0] = -1;
  phitTrue[ 2] =  1;
  phitTrue[ 6] =  4;
  phitTrue[ 7] =  6*sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 15 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 15 );
  for (k = 0; k < 15; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  s = 1;  t = 0;
  for (k = 0; k < 15; k++)
  {
    phiTrue[k] = 0;
    phisTrue[k] = 0;
    phitTrue[k] = 0;
  }
  phiTrue[1] = 1;
  phisTrue[ 0] = -1;
  phisTrue[ 1] =  1;
  phisTrue[ 9] = -4;
  phisTrue[10] = -6*sqrt(3);
  phitTrue[ 0] = -1;
  phitTrue[ 2] =  1;
  phitTrue[ 3] =  4;
  phitTrue[ 4] = -6*sqrt(3);
  phitTrue[ 9] = -4;
  phitTrue[10] = -6*sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 15 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 15 );
  for (k = 0; k < 15; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  s = 0;  t = 1;
  for (k = 0; k < 15; k++)
  {
    phiTrue[k] = 0;
    phisTrue[k] = 0;
    phitTrue[k] = 0;
  }
  phiTrue[2] = 1;
  phisTrue[ 0] = -1;
  phisTrue[ 1] =  1;
  phisTrue[ 3] =  4;
  phisTrue[ 4] =  6*sqrt(3);
  phisTrue[ 6] = -4;
  phisTrue[ 7] =  6*sqrt(3);
  phitTrue[ 0] = -1;
  phitTrue[ 2] =  1;
  phitTrue[ 6] = -4;
  phitTrue[ 7] =  6*sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 15 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 15 );
  for (k = 0; k < 15; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  s = 1./3.;  t = 1./3.;
  for (k = 0; k < 15; k++)
  {
    phiTrue[k] = 0;
    phisTrue[k] = 0;
    phitTrue[k] = 0;
  }
  phiTrue[ 0] = 1./3.;
  phiTrue[ 1] = 1./3.;
  phiTrue[ 2] = 1./3.;
  phiTrue[ 3] = 4./9.;
  phiTrue[ 5] = 16./81.;
  phiTrue[ 6] = 4./9.;
  phiTrue[ 8] = 16./81.;
  phiTrue[ 9] = 4./9.;
  phiTrue[11] = 16./81.;
  phiTrue[12] = 1;
  phisTrue[ 0] = -1;
  phisTrue[ 1] =  1;
  phisTrue[ 3] =  4./3.;
  phisTrue[ 4] = -2./sqrt(3);
  phisTrue[ 5] =  32./27;
  phisTrue[ 6] = -4./3.;
  phisTrue[ 7] = -2./sqrt(3);
  phisTrue[ 8] = -32./27;
  phisTrue[10] =  4./sqrt(3);
  phisTrue[12] =  0;
  phisTrue[13] = -1024./(81*sqrt(3));
  phisTrue[14] =  -512./(81*sqrt(3));
  phitTrue[ 0] = -1;
  phitTrue[ 2] =  1;
  phitTrue[ 3] =  4./3.;
  phitTrue[ 4] =  2./sqrt(3);
  phitTrue[ 5] =  32./27;
  phitTrue[ 7] = -4./sqrt(3);
  phitTrue[ 9] = -4./3.;
  phitTrue[10] =  2./sqrt(3);
  phitTrue[11] = -32./27;
  phitTrue[13] =  -512./(81*sqrt(3));
  phitTrue[14] = -1024./(81*sqrt(3));

  basis->evalBasis( s, t, edgesign, phi, 15 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 15 );
  for (k = 0; k < 15; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  // check sign change for asymmetric basis functions

  edgesign[0] = -1;
  phiTrue[ 4] = 0;
  phiTrue[ 7] = 0;
  phiTrue[10] = 0;
  phisTrue[ 4] =  2./sqrt(3);
  phisTrue[ 7] = -2./sqrt(3);
  phisTrue[10] =  4./sqrt(3);
  phitTrue[ 4] = -2./sqrt(3);
  phitTrue[ 7] = -4./sqrt(3);
  phitTrue[10] =  2./sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 15 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 15 );
  for (k = 4; k <= 7; k += 3)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  edgesign[1] = -1;
  phiTrue[ 4] = 0;
  phiTrue[ 7] = 0;
  phiTrue[10] = 0;
  phisTrue[ 4] =  2./sqrt(3);
  phisTrue[ 7] =  2./sqrt(3);
  phisTrue[10] =  4./sqrt(3);
  phitTrue[ 4] = -2./sqrt(3);
  phitTrue[ 7] =  4./sqrt(3);
  phitTrue[10] =  2./sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 15 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 15 );
  for (k = 4; k <= 7; k += 3)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  edgesign[2] = -1;
  phiTrue[ 4] = 0;
  phiTrue[ 7] = 0;
  phiTrue[10] = 0;
  phisTrue[ 4] =  2./sqrt(3);
  phisTrue[ 7] =  2./sqrt(3);
  phisTrue[10] = -4./sqrt(3);
  phitTrue[ 4] = -2./sqrt(3);
  phitTrue[ 7] =  4./sqrt(3);
  phitTrue[10] = -2./sqrt(3);

  basis->evalBasis( s, t, edgesign, phi, 15 );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, 15 );
  for (k = 4; k <= 7; k += 3)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP5 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  const int order  = 5;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(order, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Quad>::HierarchicalP5 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::HierarchicalP5;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};
  int i, k;

  {
  s = 0;  t = 0;
  Real  phi1[3] = { 1,  0,  0};
  Real phis1[3] = {-1,  1,  0};
  Real phit1[3] = {-1,  0,  1};
  Real  phi2[3] = { 0,  0,  0};
  Real phis2[3] = { 0,  0,  4};
  Real phit2[3] = { 0,  4,  0};
  Real  phi3[4] = { 0,         0,          0, 0};
  Real phis3[4] = { 0,         0, -6*sqrt(3), 0};
  Real phit3[4] = { 0, 6*sqrt(3),          0, 0};

  for (k = 0; k < nBasis; k++)
  {
    phiTrue[k] = 0;  phisTrue[k] = 0;  phitTrue[k] = 0;
  }
  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }


  {
  s = 1;  t = 0;
  Real  phi1[3] = { 0,  1,  0};
  Real phis1[3] = {-1,  1,  0};
  Real phit1[3] = {-1,  0,  1};
  Real  phi2[3] = { 0,  0,  0};
  Real phis2[3] = { 0,  0, -4};
  Real phit2[3] = { 4,  0, -4};
  Real  phi3[4] = {         0, 0,          0, 0};
  Real phis3[4] = {         0, 0, -6*sqrt(3), 0};
  Real phit3[4] = {-6*sqrt(3), 0, -6*sqrt(3), 0};

  for (k = 0; k < nBasis; k++)
  {
    phiTrue[k] = 0;  phisTrue[k] = 0;  phitTrue[k] = 0;
  }
  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }


  {
  s = 0;  t = 1;
  Real  phi1[3] = { 0,  0,  1};
  Real phis1[3] = {-1,  1,  0};
  Real phit1[3] = {-1,  0,  1};
  Real  phi2[3] = { 0,  0,  0};
  Real phis2[3] = { 4, -4,  0};
  Real phit2[3] = { 0, -4,  0};
  Real  phi3[4] = {         0,         0, 0, 0};
  Real phis3[4] = { 6*sqrt(3), 6*sqrt(3), 0, 0};
  Real phit3[4] = {         0, 6*sqrt(3), 0, 0};

  for (k = 0; k < nBasis; k++)
  {
    phiTrue[k] = 0;  phisTrue[k] = 0;  phitTrue[k] = 0;
  }
  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }


  {
  s = 1./3.;  t = 1./3.;
  Real  phi1[3] = { 1./3.,  1./3.,  1./3.};
  Real phis1[3] = {    -1,      1,      0};
  Real phit1[3] = {    -1,      0,      1};
  Real  phi2[3] = { 4./9.,  4./9.,  4./9.};
  Real phis2[3] = { 4./3., -4./3.,      0};
  Real phit2[3] = { 4./3.,      0, -4./3.};
  Real  phi3[4] = {          0,           0,          0, 1};
  Real phis3[4] = {-2./sqrt(3), -2./sqrt(3), 4./sqrt(3), 0};
  Real phit3[4] = { 2./sqrt(3), -4./sqrt(3), 2./sqrt(3), 0};
  Real  phi4[5] = { 16./81.,  16./81.,  16./81.,                   0,                  0};
  Real phis4[5] = { 32./27., -32./27.,        0, -1024./(81*sqrt(3)),  -512./(81*sqrt(3))};
  Real phit4[5] = { 32./27.,        0, -32./27.,  -512./(81*sqrt(3)), -1024./(81*sqrt(3))};
  Real  phi5[6] = {              0,               0,               0,  3125./3888.,  3125./3888.,  3125./3888.};
  Real phis5[6] = {-25*sqrt(5)/81., -25*sqrt(5)/81.,  50*sqrt(5)/81.,  3125./1296., -3125./1296.,            0};
  Real phit5[6] = { 25*sqrt(5)/81., -50*sqrt(5)/81.,  25*sqrt(5)/81.,  3125./1296.,            0, -3125./1296.};

  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];

    k = 3 + (nEdge/3)*i + 2;
    phiTrue[k] = phi4[i];  phisTrue[k] = phis4[i];  phitTrue[k] = phit4[i];

    k = 3 + (nEdge/3)*i + 3;
    phiTrue[k] = phi5[i];  phisTrue[k] = phis5[i];  phitTrue[k] = phit5[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];
  for (i = 3; i < 5; i++)
  {
    k = 3 + nEdge + 1 + (i - 3);
    phiTrue[k] = phi4[i];  phisTrue[k] = phis4[i];  phitTrue[k] = phit4[i];
  }
  for (i = 3; i < 6; i++)
  {
    k = 3 + nEdge + 3 + (i - 3);
    phiTrue[k] = phi5[i];  phisTrue[k] = phis5[i];  phitTrue[k] = phit5[i];
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
#if 0
  cout << "basis_HierarchicalP5: s = " << s << "  t = " << t << endl;
  for (k = 0; k < nBasis; k++)
  {
    cout << "  " << k << ":  phi = (" << phiTrue[k] << ", " << phi[k] << ")"
                      << "  phis = (" << phisTrue[k] << ", " << phis[k] << ")"
                      << "  phit = (" << phitTrue[k] << ", " << phit[k] << ")" << endl;
  }
#endif

  // check sign change for asymmetric basis functions

  edgesign[0] = -1;
  i = 4;
   phiTrue[i    ] *= -1;
  phisTrue[i    ] *= -1;
  phitTrue[i    ] *= -1;
   phiTrue[i + 2] *= -1;
  phisTrue[i + 2] *= -1;
  phitTrue[i + 2] *= -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  edgesign[1] = -1;
  i = 4 + (order - 1);
   phiTrue[i    ] *= -1;
  phisTrue[i    ] *= -1;
  phitTrue[i    ] *= -1;
   phiTrue[i + 2] *= -1;
  phisTrue[i + 2] *= -1;
  phitTrue[i + 2] *= -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  edgesign[2] = -1;
  i = 4 + 2*(order - 1);
   phiTrue[i    ] *= -1;
  phisTrue[i    ] *= -1;
  phitTrue[i    ] *= -1;
   phiTrue[i + 2] *= -1;
  phisTrue[i + 2] *= -1;
  phitTrue[i + 2] *= -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP6 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  const int order  = 6;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(order, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Quad>::HierarchicalP6 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::HierarchicalP6;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};
  int i, k;

  {
  s = 0;  t = 0;
  Real  phi1[3] = { 1,  0,  0};
  Real phis1[3] = {-1,  1,  0};
  Real phit1[3] = {-1,  0,  1};
  Real  phi2[3] = { 0,  0,  0};
  Real phis2[3] = { 0,  0,  4};
  Real phit2[3] = { 0,  4,  0};
  Real  phi3[4] = { 0,         0,          0, 0};
  Real phis3[4] = { 0,         0, -6*sqrt(3), 0};
  Real phit3[4] = { 0, 6*sqrt(3),          0, 0};

  for (k = 0; k < nBasis; k++)
  {
    phiTrue[k] = 0;  phisTrue[k] = 0;  phitTrue[k] = 0;
  }
  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }


  {
  s = 1;  t = 0;
  Real  phi1[3] = { 0,  1,  0};
  Real phis1[3] = {-1,  1,  0};
  Real phit1[3] = {-1,  0,  1};
  Real  phi2[3] = { 0,  0,  0};
  Real phis2[3] = { 0,  0, -4};
  Real phit2[3] = { 4,  0, -4};
  Real  phi3[4] = {         0, 0,          0, 0};
  Real phis3[4] = {         0, 0, -6*sqrt(3), 0};
  Real phit3[4] = {-6*sqrt(3), 0, -6*sqrt(3), 0};

  for (k = 0; k < nBasis; k++)
  {
    phiTrue[k] = 0;  phisTrue[k] = 0;  phitTrue[k] = 0;
  }
  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }


  {
  s = 0;  t = 1;
  Real  phi1[3] = { 0,  0,  1};
  Real phis1[3] = {-1,  1,  0};
  Real phit1[3] = {-1,  0,  1};
  Real  phi2[3] = { 0,  0,  0};
  Real phis2[3] = { 4, -4,  0};
  Real phit2[3] = { 0, -4,  0};
  Real  phi3[4] = {         0,         0, 0, 0};
  Real phis3[4] = { 6*sqrt(3), 6*sqrt(3), 0, 0};
  Real phit3[4] = {         0, 6*sqrt(3), 0, 0};

  for (k = 0; k < nBasis; k++)
  {
    phiTrue[k] = 0;  phisTrue[k] = 0;  phitTrue[k] = 0;
  }
  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }


  {
  s = 1./3.;  t = 1./3.;
  Real  phi1[3] = { 1./3.,  1./3.,  1./3.};
  Real phis1[3] = {    -1,      1,      0};
  Real phit1[3] = {    -1,      0,      1};
  Real  phi2[3] = { 4./9.,  4./9.,  4./9.};
  Real phis2[3] = { 4./3., -4./3.,      0};
  Real phit2[3] = { 4./3.,      0, -4./3.};
  Real  phi3[4] = {          0,           0,          0, 1};
  Real phis3[4] = {-2./sqrt(3), -2./sqrt(3), 4./sqrt(3), 0};
  Real phit3[4] = { 2./sqrt(3), -4./sqrt(3), 2./sqrt(3), 0};
  Real  phi4[5] = { 16./81.,  16./81.,  16./81.,                   0,                  0};
  Real phis4[5] = { 32./27., -32./27.,        0, -1024./(81*sqrt(3)),  -512./(81*sqrt(3))};
  Real phit4[5] = { 32./27.,        0, -32./27.,  -512./(81*sqrt(3)), -1024./(81*sqrt(3))};
  Real  phi5[6] = {              0,               0,               0,  3125./3888.,  3125./3888.,  3125./3888.};
  Real phis5[6] = {-25*sqrt(5)/81., -25*sqrt(5)/81.,  50*sqrt(5)/81.,  3125./1296., -3125./1296.,            0};
  Real phit5[6] = { 25*sqrt(5)/81., -50*sqrt(5)/81.,  25*sqrt(5)/81.,  3125./1296.,            0, -3125./1296.};
  Real  phi6[7] = { 64./729.,  64./729.,  64./729., 1,                  0,                  0,                 0};
  Real phis6[7] = { 64./81. , -64./81. ,         0, 0, -192./(25*sqrt(5)), -192./(25*sqrt(5)), 384./(25*sqrt(5))};
  Real phit6[7] = { 64./81. ,         0, -64./81. , 0,  192./(25*sqrt(5)), -384./(25*sqrt(5)), 192./(25*sqrt(5))};

  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];

    k = 3 + (nEdge/3)*i + 2;
    phiTrue[k] = phi4[i];  phisTrue[k] = phis4[i];  phitTrue[k] = phit4[i];

    k = 3 + (nEdge/3)*i + 3;
    phiTrue[k] = phi5[i];  phisTrue[k] = phis5[i];  phitTrue[k] = phit5[i];

    k = 3 + (nEdge/3)*i + 4;
    phiTrue[k] = phi6[i];  phisTrue[k] = phis6[i];  phitTrue[k] = phit6[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];
  for (i = 3; i < 5; i++)
  {
    k = 3 + nEdge + 1 + (i - 3);
    phiTrue[k] = phi4[i];  phisTrue[k] = phis4[i];  phitTrue[k] = phit4[i];
  }
  for (i = 3; i < 6; i++)
  {
    k = 3 + nEdge + 3 + (i - 3);
    phiTrue[k] = phi5[i];  phisTrue[k] = phis5[i];  phitTrue[k] = phit5[i];
  }
  for (i = 3; i < 7; i++)
  {
    k = 3 + nEdge + 6 + (i - 3);
    phiTrue[k] = phi6[i];  phisTrue[k] = phis6[i];  phitTrue[k] = phit6[i];
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
#if 0
  cout << "basis_HierarchicalP6: s = " << s << "  t = " << t << endl;
  for (k = 0; k < nBasis; k++)
  {
    cout << "  " << k << ":  phi = (" << phiTrue[k] << ", " << phi[k] << ")"
                      << "  phis = (" << phisTrue[k] << ", " << phis[k] << ")"
                      << "  phit = (" << phitTrue[k] << ", " << phit[k] << ")" << endl;
  }
#endif

  // check sign change for asymmetric basis functions

  edgesign[0] = -1;
  i = 4;
   phiTrue[i    ] *= -1;
  phisTrue[i    ] *= -1;
  phitTrue[i    ] *= -1;
   phiTrue[i + 2] *= -1;
  phisTrue[i + 2] *= -1;
  phitTrue[i + 2] *= -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  edgesign[1] = -1;
  i = 4 + (order - 1);
   phiTrue[i    ] *= -1;
  phisTrue[i    ] *= -1;
  phitTrue[i    ] *= -1;
   phiTrue[i + 2] *= -1;
  phisTrue[i + 2] *= -1;
  phitTrue[i + 2] *= -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  edgesign[2] = -1;
  i = 4 + 2*(order - 1);
   phiTrue[i    ] *= -1;
  phisTrue[i    ] *= -1;
  phitTrue[i    ] *= -1;
   phiTrue[i + 2] *= -1;
  phisTrue[i + 2] *= -1;
  phitTrue[i + 2] *= -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP7 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  const int order  = 7;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(order, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Quad>::HierarchicalP7 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::HierarchicalP7;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};
  int i, k;

  {
  s = 0;  t = 0;
  Real  phi1[3] = { 1,  0,  0};
  Real phis1[3] = {-1,  1,  0};
  Real phit1[3] = {-1,  0,  1};
  Real  phi2[3] = { 0,  0,  0};
  Real phis2[3] = { 0,  0,  4};
  Real phit2[3] = { 0,  4,  0};
  Real  phi3[4] = { 0,         0,          0, 0};
  Real phis3[4] = { 0,         0, -6*sqrt(3), 0};
  Real phit3[4] = { 0, 6*sqrt(3),          0, 0};

  for (k = 0; k < nBasis; k++)
  {
    phiTrue[k] = 0;  phisTrue[k] = 0;  phitTrue[k] = 0;
  }
  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }


  {
  s = 1;  t = 0;
  Real  phi1[3] = { 0,  1,  0};
  Real phis1[3] = {-1,  1,  0};
  Real phit1[3] = {-1,  0,  1};
  Real  phi2[3] = { 0,  0,  0};
  Real phis2[3] = { 0,  0, -4};
  Real phit2[3] = { 4,  0, -4};
  Real  phi3[4] = {         0, 0,          0, 0};
  Real phis3[4] = {         0, 0, -6*sqrt(3), 0};
  Real phit3[4] = {-6*sqrt(3), 0, -6*sqrt(3), 0};

  for (k = 0; k < nBasis; k++)
  {
    phiTrue[k] = 0;  phisTrue[k] = 0;  phitTrue[k] = 0;
  }
  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }


  {
  s = 0;  t = 1;
  Real  phi1[3] = { 0,  0,  1};
  Real phis1[3] = {-1,  1,  0};
  Real phit1[3] = {-1,  0,  1};
  Real  phi2[3] = { 0,  0,  0};
  Real phis2[3] = { 4, -4,  0};
  Real phit2[3] = { 0, -4,  0};
  Real  phi3[4] = {         0,         0, 0, 0};
  Real phis3[4] = { 6*sqrt(3), 6*sqrt(3), 0, 0};
  Real phit3[4] = {         0, 6*sqrt(3), 0, 0};

  for (k = 0; k < nBasis; k++)
  {
    phiTrue[k] = 0;  phisTrue[k] = 0;  phitTrue[k] = 0;
  }
  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }


  {
  s = 1./3.;  t = 1./3.;
  Real  phi1[3] = { 1./3.,  1./3.,  1./3.};
  Real phis1[3] = {    -1,      1,      0};
  Real phit1[3] = {    -1,      0,      1};
  Real  phi2[3] = { 4./9.,  4./9.,  4./9.};
  Real phis2[3] = { 4./3., -4./3.,      0};
  Real phit2[3] = { 4./3.,      0, -4./3.};
  Real  phi3[4] = {          0,           0,          0, 1};
  Real phis3[4] = {-2./sqrt(3), -2./sqrt(3), 4./sqrt(3), 0};
  Real phit3[4] = { 2./sqrt(3), -4./sqrt(3), 2./sqrt(3), 0};
  Real  phi4[5] = { 16./81.,  16./81.,  16./81.,                   0,                  0};
  Real phis4[5] = { 32./27., -32./27.,        0, -1024./(81*sqrt(3)),  -512./(81*sqrt(3))};
  Real phit4[5] = { 32./27.,        0, -32./27.,  -512./(81*sqrt(3)), -1024./(81*sqrt(3))};
  Real  phi5[6] = {              0,               0,               0,  3125./3888.,  3125./3888.,  3125./3888.};
  Real phis5[6] = {-25*sqrt(5)/81., -25*sqrt(5)/81.,  50*sqrt(5)/81.,  3125./1296., -3125./1296.,            0};
  Real phit5[6] = { 25*sqrt(5)/81., -50*sqrt(5)/81.,  25*sqrt(5)/81.,  3125./1296.,            0, -3125./1296.};
  Real  phi6[7] = { 64./729.,  64./729.,  64./729., 1,                  0,                  0,                 0};
  Real phis6[7] = { 64./81. , -64./81. ,         0, 0, -192./(25*sqrt(5)), -192./(25*sqrt(5)), 384./(25*sqrt(5))};
  Real phit6[7] = { 64./81. ,         0, -64./81. , 0,  192./(25*sqrt(5)), -384./(25*sqrt(5)), 192./(25*sqrt(5))};

  Real  phi7e[3] = {0, 0, 0};
  Real phis7e[3] = {-2744*sqrt(7)/19683., -2744*sqrt(7)/19683., 5488*sqrt(7)/19683.};
  Real phit7e[3] = { 2744*sqrt(7)/19683., -5488*sqrt(7)/19683., 2744*sqrt(7)/19683.};
  Real  phi7c[5] = {                      0,                       0, 823543/1594323.,  823543/1594323.,  823543/1594323.};
  Real phis7c[5] = {-823543/(36450*sqrt(5)), -823543/(72900*sqrt(5)), 1647086/531441., -1647086/531441.,                0};
  Real phit7c[5] = {-823543/(72900*sqrt(5)), -823543/(36450*sqrt(5)), 1647086/531441.,                0, -1647086/531441.};

  for (i = 0; i < 3; i++)
  {
    k = i;
    phiTrue[k] = phi1[i];  phisTrue[k] = phis1[i];  phitTrue[k] = phit1[i];

    k = 3 + (nEdge/3)*i;
    phiTrue[k] = phi2[i];  phisTrue[k] = phis2[i];  phitTrue[k] = phit2[i];

    k = 3 + (nEdge/3)*i + 1;
    phiTrue[k] = phi3[i];  phisTrue[k] = phis3[i];  phitTrue[k] = phit3[i];

    k = 3 + (nEdge/3)*i + 2;
    phiTrue[k] = phi4[i];  phisTrue[k] = phis4[i];  phitTrue[k] = phit4[i];

    k = 3 + (nEdge/3)*i + 3;
    phiTrue[k] = phi5[i];  phisTrue[k] = phis5[i];  phitTrue[k] = phit5[i];

    k = 3 + (nEdge/3)*i + 4;
    phiTrue[k] = phi6[i];  phisTrue[k] = phis6[i];  phitTrue[k] = phit6[i];

    k = 3 + (nEdge/3)*i + 5;
    phiTrue[k] = phi7e[i];  phisTrue[k] = phis7e[i];  phitTrue[k] = phit7e[i];
  }
  k = 3 + nEdge;
  phiTrue[k] = phi3[3];  phisTrue[k] = phis3[3];  phitTrue[k] = phit3[3];
  for (i = 3; i < 5; i++)
  {
    k = 3 + nEdge + 1 + (i - 3);
    phiTrue[k] = phi4[i];  phisTrue[k] = phis4[i];  phitTrue[k] = phit4[i];
  }
  for (i = 3; i < 6; i++)
  {
    k = 3 + nEdge + 3 + (i - 3);
    phiTrue[k] = phi5[i];  phisTrue[k] = phis5[i];  phitTrue[k] = phit5[i];
  }
  for (i = 3; i < 7; i++)
  {
    k = 3 + nEdge + 6 + (i - 3);
    phiTrue[k] = phi6[i];  phisTrue[k] = phis6[i];  phitTrue[k] = phit6[i];
  }
  for (i = 0; i < 5; i++)
  {
    k = 3 + nEdge + 10 + i;
    phiTrue[k] = phi7c[i];  phisTrue[k] = phis7c[i];  phitTrue[k] = phit7c[i];
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
#if 0
  cout << "basis_HierarchicalP7: s = " << s << "  t = " << t << endl;
  for (k = 0; k < nBasis; k++)
  {
    cout << "  " << k << ":  phi = (" << phiTrue[k] << ", " << phi[k] << ")"
                      << "  phis = (" << phisTrue[k] << ", " << phis[k] << ")"
                      << "  phit = (" << phitTrue[k] << ", " << phit[k] << ")" << endl;
  }
#endif

  // check sign change for asymmetric basis functions

  edgesign[0] = -1;
  i = 4;
   phiTrue[i    ] *= -1;
  phisTrue[i    ] *= -1;
  phitTrue[i    ] *= -1;
   phiTrue[i + 2] *= -1;
  phisTrue[i + 2] *= -1;
  phitTrue[i + 2] *= -1;
   phiTrue[i + 4] *= -1;
  phisTrue[i + 4] *= -1;
  phitTrue[i + 4] *= -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
#if 0
  cout << "basis_HierarchicalP7: s = " << s << "  t = " << t << endl;
  for (k = 0; k < nBasis; k++)
  {
    cout << "  " << k << ":  phi = (" << phiTrue[k] << ", " << phi[k] << ")"
                      << "  phis = (" << phisTrue[k] << ", " << phis[k] << ")"
                      << "  phit = (" << phitTrue[k] << ", " << phit[k] << ")" << endl;
  }
#endif

  edgesign[1] = -1;
  i = 4 + (order - 1);
   phiTrue[i    ] *= -1;
  phisTrue[i    ] *= -1;
  phitTrue[i    ] *= -1;
   phiTrue[i + 2] *= -1;
  phisTrue[i + 2] *= -1;
  phitTrue[i + 2] *= -1;
   phiTrue[i + 4] *= -1;
  phisTrue[i + 4] *= -1;
  phitTrue[i + 4] *= -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }

  edgesign[2] = -1;
  i = 4 + 2*(order - 1);
   phiTrue[i    ] *= -1;
  phisTrue[i    ] *= -1;
  phitTrue[i    ] *= -1;
   phiTrue[i + 2] *= -1;
  phisTrue[i + 2] *= -1;
  phitTrue[i + 2] *= -1;
   phiTrue[i + 4] *= -1;
  phisTrue[i + 4] *= -1;
  phitTrue[i + 4] *= -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  for (k = 0; k < nBasis; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
  }
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionArea_Quad_Hierarchical_pattern.txt", true );

  BasisFunctionAreaBase<Quad>::getBasisFunction(1, BasisFunctionCategory_Hierarchical)->dump( 2, output );
#if 0
  BasisFunctionAreaBase<Quad>::getBasisFunction(2, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(3, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(4, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(5, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(6, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(7, BasisFunctionCategory_Hierarchical)->dump( 2, output );
#endif

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
