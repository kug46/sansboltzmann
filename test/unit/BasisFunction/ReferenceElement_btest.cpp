// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ReferenceElement_btest
// testing of reference element functions

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "BasisFunction/ReferenceElement.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ReferenceElement_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReferenceElement_Node_test )
{
  typedef ReferenceElement<Node> RefElement;
  typedef RefElement::RefCoordType RefCoordType;

  BOOST_CHECK_EQUAL( RefElement::centerRef[0], 0.0 );

  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.0}), true );
  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.1}), false );

  RefCoordType dsRef, sRef_sol;
  bool modified;

  dsRef = 0;
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  BOOST_CHECK_EQUAL( sRef_sol[0], 0.0 );
  BOOST_CHECK_EQUAL( modified, false );

  dsRef = 0.1;
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  BOOST_CHECK_EQUAL( sRef_sol[0], 0.0 );
  BOOST_CHECK_EQUAL( modified, true );

  int itrace = 0;
  RefElement::MatrixConst jac = 0;
  RefElement::VectorConst res = 0;

  // empty functions
  RefElement::constraintJacobian(itrace=0, jac);
  RefElement::constraintResidual(itrace=0, sRef_sol, res);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReferenceElement_Line_test )
{
  typedef ReferenceElement<Line> RefElement;
  typedef RefElement::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( RefElement::centerRef[0], 0.5 );
  BOOST_CHECK_EQUAL( RefElement::lengthRef, 1.0 );

  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.2}), true );
  BOOST_CHECK_EQUAL( RefElement::isInside({-0.1}), false );
  BOOST_CHECK_EQUAL( RefElement::isInside({ 1.3}), false );

  RefCoordType dsRef, sRef_sol;
  bool modified;

  dsRef = 0.1;
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.6, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, false );

  dsRef = 0.51;
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 1.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = -0.51;
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );


  int itrace;
  RefElement::MatrixConst jac = 0;
  RefElement::VectorConst res = 0;

  sRef_sol = 0.25;

  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=0, jac);
  RefElement::constraintResidual(itrace=0, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 1, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );

  BOOST_CHECK_EQUAL(             0, res[0] );
  BOOST_CHECK_EQUAL( sRef_sol[0]-1, res[1] );

  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=1, jac);
  RefElement::constraintResidual(itrace=1, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 1, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL( sRef_sol[0], res[1] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReferenceElement_Triangle_test )
{
  typedef ReferenceElement<Triangle> RefElement;
  typedef RefElement::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( RefElement::centerRef[0], 1.0/3.0 );
  BOOST_CHECK_EQUAL( RefElement::centerRef[1], 1.0/3.0 );
  BOOST_CHECK_EQUAL( RefElement::areaRef, 0.5 );

  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.2, 0.4}), true );
  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.3, 0.7}), true );
  BOOST_CHECK_EQUAL( RefElement::isInside({-0.1, 0.2}), false );
  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.4, 0.7}), false );

  RefCoordType dsRef, sRef_sol;
  bool modified;

  dsRef = {0.1, -0.2};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 13.0/30.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 2.0/15.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, false );

  dsRef = {-1.0, -2.0/3.0};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 1.0/9.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = {-2.0/3.0, -1.0};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 1.0/9.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = {1.0/3.0, 2.0/3.0};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 4.0/9.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 5.0/9.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );


  int itrace;
  RefElement::MatrixConst jac = 0;
  RefElement::VectorConst res = 0;

  sRef_sol = {0.25, 0.3};

  // Edge 0: s + t = 1
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=0, jac);
  RefElement::constraintResidual(itrace=0, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 1, jac(2,0) );
  BOOST_CHECK_EQUAL( 1, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );

  BOOST_CHECK_EQUAL(                         0, res[0] );
  BOOST_CHECK_EQUAL(                         0, res[1] );
  BOOST_CHECK_EQUAL( sRef_sol[0]+sRef_sol[1]-1, res[2] );

  // Edge 1: s = 0
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=1, jac);
  RefElement::constraintResidual(itrace=1, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 1, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL(           0, res[1] );
  BOOST_CHECK_EQUAL( sRef_sol[0], res[2] );

  // Edge 2: t = 0
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=2, jac);
  RefElement::constraintResidual(itrace=2, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 1, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL(           0, res[1] );
  BOOST_CHECK_EQUAL( sRef_sol[1], res[2] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReferenceElement_Quad_test )
{
  typedef ReferenceElement<Quad> RefElement;
  typedef RefElement::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( RefElement::centerRef[0], 0.5 );
  BOOST_CHECK_EQUAL( RefElement::centerRef[1], 0.5 );
  BOOST_CHECK_EQUAL( RefElement::areaRef, 1.0 );

  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.2, 0.4}), true );
  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.3, 0.7}), true );
  BOOST_CHECK_EQUAL( RefElement::isInside({-0.1, 0.2}), false );
  BOOST_CHECK_EQUAL( RefElement::isInside({ 1.1, 0.7}), false );

  RefCoordType dsRef, sRef_sol;
  bool modified;

  dsRef = {0.1, -0.2};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.6, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.3, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, false );

  dsRef = {-1.0, -0.75};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.125, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = { 2.0, -0.8};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 1.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.3, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = {-0.75, -1.0};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.125, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = { 0.8, 2.0};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.7, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 1.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );


  int itrace;
  RefElement::MatrixConst jac = 0;
  RefElement::VectorConst res = 0;

  sRef_sol = {0.25, 0.3};

  // Edge 0: t = 0
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=0, jac);
  RefElement::constraintResidual(itrace=0, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 1, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL(           0, res[1] );
  BOOST_CHECK_EQUAL( sRef_sol[1], res[2] );

  // Edge 1: s = 1
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=1, jac);
  RefElement::constraintResidual(itrace=1, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 1, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );

  BOOST_CHECK_EQUAL(             0, res[0] );
  BOOST_CHECK_EQUAL(             0, res[1] );
  BOOST_CHECK_EQUAL( sRef_sol[0]-1, res[2] );

  // Edge 2: t = 1
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=2, jac);
  RefElement::constraintResidual(itrace=2, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 1, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );

  BOOST_CHECK_EQUAL(             0, res[0] );
  BOOST_CHECK_EQUAL(             0, res[1] );
  BOOST_CHECK_EQUAL( sRef_sol[1]-1, res[2] );

  // Edge 3: s = 0
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=3, jac);
  RefElement::constraintResidual(itrace=3, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 1, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL(           0, res[1] );
  BOOST_CHECK_EQUAL( sRef_sol[0], res[2] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReferenceElement_Tet_test )
{
  typedef ReferenceElement<Tet> RefElement;
  typedef RefElement::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( RefElement::centerRef[0], 0.25 );
  BOOST_CHECK_EQUAL( RefElement::centerRef[1], 0.25 );
  BOOST_CHECK_EQUAL( RefElement::centerRef[2], 0.25 );
  BOOST_CHECK_EQUAL( RefElement::volumeRef, 1.0/6.0 );

  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.2, 0.4, 0.1}), true );
  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.3, 0.5, 0.2}), true );
  BOOST_CHECK_EQUAL( RefElement::isInside({-0.1, 0.2, 0.5}), false );
  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.3, 0.6, 0.2}), false );

  RefCoordType dsRef, sRef_sol;
  bool modified;

  dsRef = {0.1, -0.2, -0.1};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.35, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.05, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 0.15, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, false );

  dsRef = {-0.5, -0.3, 0.4};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.1, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 0.45, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = {0.3, -0.5, -0.4};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.4, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 0.05, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = {-0.3, -0.4, -0.5};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.1, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.05, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 0.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = {-0.3, 0.4, 0.5};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.125, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 5.0/12.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 11.0/24.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );


  int itrace;
  RefElement::MatrixConst jac = 0;
  RefElement::VectorConst res = 0;

  sRef_sol = {0.25, 0.3, 0.12};

  // Face 0: s + t + u = 1
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=0, jac);
  RefElement::constraintResidual(itrace=0, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );
  BOOST_CHECK_EQUAL( 1, jac(3,0) );
  BOOST_CHECK_EQUAL( 1, jac(3,1) );
  BOOST_CHECK_EQUAL( 1, jac(3,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,3) );

  BOOST_CHECK_EQUAL(                                     0, res[0] );
  BOOST_CHECK_EQUAL(                                     0, res[1] );
  BOOST_CHECK_EQUAL(                                     0, res[2] );
  BOOST_CHECK_EQUAL( sRef_sol[0]+sRef_sol[1]+sRef_sol[2]-1, res[3] );

  // Face 1: s = 0
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=1, jac);
  RefElement::constraintResidual(itrace=1, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );
  BOOST_CHECK_EQUAL( 1, jac(3,0) );
  BOOST_CHECK_EQUAL( 0, jac(3,1) );
  BOOST_CHECK_EQUAL( 0, jac(3,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,3) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL(           0, res[1] );
  BOOST_CHECK_EQUAL(           0, res[2] );
  BOOST_CHECK_EQUAL( sRef_sol[0], res[3] );

  // Face 2: t = 0
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=2, jac);
  RefElement::constraintResidual(itrace=2, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,0) );
  BOOST_CHECK_EQUAL( 1, jac(3,1) );
  BOOST_CHECK_EQUAL( 0, jac(3,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,3) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL(           0, res[1] );
  BOOST_CHECK_EQUAL(           0, res[2] );
  BOOST_CHECK_EQUAL( sRef_sol[1], res[3] );

  // Face 3: u = 0
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=3, jac);
  RefElement::constraintResidual(itrace=3, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,0) );
  BOOST_CHECK_EQUAL( 0, jac(3,1) );
  BOOST_CHECK_EQUAL( 1, jac(3,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,3) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL(           0, res[1] );
  BOOST_CHECK_EQUAL(           0, res[2] );
  BOOST_CHECK_EQUAL( sRef_sol[2], res[3] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReferenceElement_Hex_test )
{
  typedef ReferenceElement<Hex> RefElement;
  typedef RefElement::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( RefElement::centerRef[0], 0.5 );
  BOOST_CHECK_EQUAL( RefElement::centerRef[1], 0.5 );
  BOOST_CHECK_EQUAL( RefElement::centerRef[2], 0.5 );
  BOOST_CHECK_EQUAL( RefElement::volumeRef, 1.0 );

  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.2, 0.4, 0.1}), true );
  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.3, 0.5, 0.2}), true );
  BOOST_CHECK_EQUAL( RefElement::isInside({-0.1, 0.2, 0.5}), false );
  BOOST_CHECK_EQUAL( RefElement::isInside({ 0.3, 1.1, 0.2}), false );

  RefCoordType dsRef, sRef_sol;
  bool modified;

  dsRef = {0.1, -0.2, 0.3};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.6, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.3, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 0.8, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, false );

  dsRef = {-1.0, -0.75, 0.6};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.125, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 0.8, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = { 2.0, -0.8, 1.0};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 1.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.3, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 0.75, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = {-0.75, -1.0, 0.6};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.125, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 0.8, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = { 0.8, 2.0, -1.0};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.7, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 1.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 0.25, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = {-0.75, -0.6, -1.0};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.125, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.2, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 0.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );

  dsRef = { 0.8, -1.0, 2.0};
  modified = RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE( sRef_sol[0], 0.7, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[1], 0.25, small_tol, close_tol );
  SANS_CHECK_CLOSE( sRef_sol[2], 1.0, small_tol, close_tol );
  BOOST_CHECK_EQUAL( modified, true );


  int itrace;
  RefElement::MatrixConst jac = 0;
  RefElement::VectorConst res = 0;

  sRef_sol = {0.25, 0.3, 0.12};

  // Face 0: u = 0
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=0, jac);
  RefElement::constraintResidual(itrace=0, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,0) );
  BOOST_CHECK_EQUAL( 0, jac(3,1) );
  BOOST_CHECK_EQUAL( 1, jac(3,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,3) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL(           0, res[1] );
  BOOST_CHECK_EQUAL(           0, res[2] );
  BOOST_CHECK_EQUAL( sRef_sol[2], res[3] );

  // Face 1: t = 0
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=1, jac);
  RefElement::constraintResidual(itrace=1, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,0) );
  BOOST_CHECK_EQUAL( 1, jac(3,1) );
  BOOST_CHECK_EQUAL( 0, jac(3,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,3) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL(           0, res[1] );
  BOOST_CHECK_EQUAL(           0, res[2] );
  BOOST_CHECK_EQUAL( sRef_sol[1], res[3] );

  // Face 2: s = 1
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=2, jac);
  RefElement::constraintResidual(itrace=2, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );
  BOOST_CHECK_EQUAL( 1, jac(3,0) );
  BOOST_CHECK_EQUAL( 0, jac(3,1) );
  BOOST_CHECK_EQUAL( 0, jac(3,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,3) );

  BOOST_CHECK_EQUAL(             0, res[0] );
  BOOST_CHECK_EQUAL(             0, res[1] );
  BOOST_CHECK_EQUAL(             0, res[2] );
  BOOST_CHECK_EQUAL( sRef_sol[0]-1, res[3] );

  // Face 3: t = 1
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=3, jac);
  RefElement::constraintResidual(itrace=3, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,0) );
  BOOST_CHECK_EQUAL( 1, jac(3,1) );
  BOOST_CHECK_EQUAL( 0, jac(3,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,3) );

  BOOST_CHECK_EQUAL(             0, res[0] );
  BOOST_CHECK_EQUAL(             0, res[1] );
  BOOST_CHECK_EQUAL(             0, res[2] );
  BOOST_CHECK_EQUAL( sRef_sol[1]-1, res[3] );

  // Face 4: s = 0
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=4, jac);
  RefElement::constraintResidual(itrace=4, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );
  BOOST_CHECK_EQUAL( 1, jac(3,0) );
  BOOST_CHECK_EQUAL( 0, jac(3,1) );
  BOOST_CHECK_EQUAL( 0, jac(3,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,3) );

  BOOST_CHECK_EQUAL(           0, res[0] );
  BOOST_CHECK_EQUAL(           0, res[1] );
  BOOST_CHECK_EQUAL(           0, res[2] );
  BOOST_CHECK_EQUAL( sRef_sol[0], res[3] );

  // Face 5: u = 1
  jac = 0; res = 0;
  RefElement::constraintJacobian(itrace=5, jac);
  RefElement::constraintResidual(itrace=5, sRef_sol, res);

  BOOST_CHECK_EQUAL( 0, jac(0,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,0) );
  BOOST_CHECK_EQUAL( 0, jac(1,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,0) );
  BOOST_CHECK_EQUAL( 0, jac(2,1) );
  BOOST_CHECK_EQUAL( 0, jac(2,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,0) );
  BOOST_CHECK_EQUAL( 0, jac(3,1) );
  BOOST_CHECK_EQUAL( 1, jac(3,2) );
  BOOST_CHECK_EQUAL( 0, jac(3,3) );

  BOOST_CHECK_EQUAL(             0, res[0] );
  BOOST_CHECK_EQUAL(             0, res[1] );
  BOOST_CHECK_EQUAL(             0, res[2] );
  BOOST_CHECK_EQUAL( sRef_sol[2]-1, res[3] );
}

BOOST_AUTO_TEST_CASE(ReferenceElement_Pentatope_test)
{
  typedef ReferenceElement<Pentatope> RefElement;
  typedef RefElement::RefCoordType RefCoordType;

  const Real small_tol= 1e-12;
  const Real close_tol= 1e-12;

  BOOST_CHECK_EQUAL(RefElement::centerRef[0], 1./5.);
  BOOST_CHECK_EQUAL(RefElement::centerRef[1], 1./5.);
  BOOST_CHECK_EQUAL(RefElement::centerRef[2], 1./5.);
  BOOST_CHECK_EQUAL(RefElement::centerRef[3], 1./5.);

  BOOST_CHECK_EQUAL(RefElement::isInside({0.2, 0.4, 0.1, 0.0}), true);
  BOOST_CHECK_EQUAL(RefElement::isInside({0.3, 0.5, 0.1, 0.1}), true);
  BOOST_CHECK_EQUAL(RefElement::isInside({-0.1, 0.2, 0.3, 0.4}), false);
  BOOST_CHECK_EQUAL(RefElement::isInside({0.3, 0.6, 0.2, 0.5}), false);

  RefCoordType dsRef;
  RefCoordType sRef_sol;
  bool modified;

  dsRef= {0.1, -0.2, -0.1, 0.2};
  modified= RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE(sRef_sol[0], 0.3, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[1], 0.0, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[2], 0.1, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[3], 0.4, small_tol, close_tol);
  BOOST_CHECK_EQUAL(modified, false);

  dsRef= {0.3, 0.2, 0.4, 0.5};
  modified= RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE(sRef_sol[0], 242.857142857143e-003, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[1], 228.571428571429e-003, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[2], 257.142857142857e-003, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[3], 271.428571428571e-003, small_tol, close_tol);
  BOOST_CHECK_EQUAL(modified, true);

  dsRef= {-0.4, 0.3, -0.5, 0.2};
  modified= RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE(sRef_sol[0], 0.04, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[1], 0.32, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[2], 0.0, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[3], 0.28, small_tol, close_tol);
  BOOST_CHECK_EQUAL(modified, true);

  dsRef= {0.5, -0.3, 0.4, 0.2};
  modified= RefElement::getInteriorSolution(RefElement::centerRef, dsRef, sRef_sol);
  SANS_CHECK_CLOSE(sRef_sol[0], 0.325, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[1], 0.125, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[2], 0.300, small_tol, close_tol);
  SANS_CHECK_CLOSE(sRef_sol[3], 0.250, small_tol, close_tol);
  BOOST_CHECK_EQUAL(modified, true);

  int itrace;
  RefElement::MatrixConst jac= 0;
  RefElement::VectorConst res= 0;

  sRef_sol= {0.25, 0.3, 0.12, 0.4};

  // face 0: s + t + u + v= 1
  jac= 0;
  res= 0;
  RefElement::constraintJacobian(itrace= 0, jac);
  RefElement::constraintResidual(itrace= 0, sRef_sol, res);

  BOOST_CHECK_EQUAL(jac(0, 0), 0);
  BOOST_CHECK_EQUAL(jac(1, 0), 0);
  BOOST_CHECK_EQUAL(jac(1, 1), 0);
  BOOST_CHECK_EQUAL(jac(2, 0), 0);
  BOOST_CHECK_EQUAL(jac(2, 1), 0);
  BOOST_CHECK_EQUAL(jac(2, 2), 0);
  BOOST_CHECK_EQUAL(jac(3, 0), 0);
  BOOST_CHECK_EQUAL(jac(3, 1), 0);
  BOOST_CHECK_EQUAL(jac(3, 2), 0);
  BOOST_CHECK_EQUAL(jac(3, 3), 0);
  BOOST_CHECK_EQUAL(jac(4, 0), 1);
  BOOST_CHECK_EQUAL(jac(4, 1), 1);
  BOOST_CHECK_EQUAL(jac(4, 2), 1);
  BOOST_CHECK_EQUAL(jac(4, 3), 1);
  BOOST_CHECK_EQUAL(jac(4, 4), 0);

  BOOST_CHECK_EQUAL(res[0], 0);
  BOOST_CHECK_EQUAL(res[1], 0);
  BOOST_CHECK_EQUAL(res[2], 0);
  BOOST_CHECK_EQUAL(res[3], 0);
  BOOST_CHECK_EQUAL(res[4], sRef_sol[0] + sRef_sol[1] + sRef_sol[2] + sRef_sol[3] - 1);

  // face 1: s= 0
  jac= 0;
  res= 0;
  RefElement::constraintJacobian(itrace= 1, jac);
  RefElement::constraintResidual(itrace= 1, sRef_sol, res);

  BOOST_CHECK_EQUAL(jac(0, 0), 0);
  BOOST_CHECK_EQUAL(jac(1, 0), 0);
  BOOST_CHECK_EQUAL(jac(1, 1), 0);
  BOOST_CHECK_EQUAL(jac(2, 0), 0);
  BOOST_CHECK_EQUAL(jac(2, 1), 0);
  BOOST_CHECK_EQUAL(jac(2, 2), 0);
  BOOST_CHECK_EQUAL(jac(3, 0), 0);
  BOOST_CHECK_EQUAL(jac(3, 1), 0);
  BOOST_CHECK_EQUAL(jac(3, 2), 0);
  BOOST_CHECK_EQUAL(jac(3, 3), 0);
  BOOST_CHECK_EQUAL(jac(4, 0), 1);
  BOOST_CHECK_EQUAL(jac(4, 1), 0);
  BOOST_CHECK_EQUAL(jac(4, 2), 0);
  BOOST_CHECK_EQUAL(jac(4, 3), 0);
  BOOST_CHECK_EQUAL(jac(4, 4), 0);

  BOOST_CHECK_EQUAL(res[0], 0);
  BOOST_CHECK_EQUAL(res[1], 0);
  BOOST_CHECK_EQUAL(res[2], 0);
  BOOST_CHECK_EQUAL(res[3], 0);
  BOOST_CHECK_EQUAL(res[4], sRef_sol[0]);

  // face 2: t= 0
  jac= 0;
  res= 0;
  RefElement::constraintJacobian(itrace= 2, jac);
  RefElement::constraintResidual(itrace= 2, sRef_sol, res);

  BOOST_CHECK_EQUAL(jac(0, 0), 0);
  BOOST_CHECK_EQUAL(jac(1, 0), 0);
  BOOST_CHECK_EQUAL(jac(1, 1), 0);
  BOOST_CHECK_EQUAL(jac(2, 0), 0);
  BOOST_CHECK_EQUAL(jac(2, 1), 0);
  BOOST_CHECK_EQUAL(jac(2, 2), 0);
  BOOST_CHECK_EQUAL(jac(3, 0), 0);
  BOOST_CHECK_EQUAL(jac(3, 1), 0);
  BOOST_CHECK_EQUAL(jac(3, 2), 0);
  BOOST_CHECK_EQUAL(jac(3, 3), 0);
  BOOST_CHECK_EQUAL(jac(4, 0), 0);
  BOOST_CHECK_EQUAL(jac(4, 1), 1);
  BOOST_CHECK_EQUAL(jac(4, 2), 0);
  BOOST_CHECK_EQUAL(jac(4, 3), 0);
  BOOST_CHECK_EQUAL(jac(4, 4), 0);

  BOOST_CHECK_EQUAL(res[0], 0);
  BOOST_CHECK_EQUAL(res[1], 0);
  BOOST_CHECK_EQUAL(res[2], 0);
  BOOST_CHECK_EQUAL(res[3], 0);
  BOOST_CHECK_EQUAL(res[4], sRef_sol[1]);

  // face 3: t= 0
  jac= 0;
  res= 0;
  RefElement::constraintJacobian(itrace= 3, jac);
  RefElement::constraintResidual(itrace= 3, sRef_sol, res);

  BOOST_CHECK_EQUAL(jac(0, 0), 0);
  BOOST_CHECK_EQUAL(jac(1, 0), 0);
  BOOST_CHECK_EQUAL(jac(1, 1), 0);
  BOOST_CHECK_EQUAL(jac(2, 0), 0);
  BOOST_CHECK_EQUAL(jac(2, 1), 0);
  BOOST_CHECK_EQUAL(jac(2, 2), 0);
  BOOST_CHECK_EQUAL(jac(3, 0), 0);
  BOOST_CHECK_EQUAL(jac(3, 1), 0);
  BOOST_CHECK_EQUAL(jac(3, 2), 0);
  BOOST_CHECK_EQUAL(jac(3, 3), 0);
  BOOST_CHECK_EQUAL(jac(4, 0), 0);
  BOOST_CHECK_EQUAL(jac(4, 1), 0);
  BOOST_CHECK_EQUAL(jac(4, 2), 1);
  BOOST_CHECK_EQUAL(jac(4, 3), 0);
  BOOST_CHECK_EQUAL(jac(4, 4), 0);

  BOOST_CHECK_EQUAL(res[0], 0);
  BOOST_CHECK_EQUAL(res[1], 0);
  BOOST_CHECK_EQUAL(res[2], 0);
  BOOST_CHECK_EQUAL(res[3], 0);
  BOOST_CHECK_EQUAL(res[4], sRef_sol[2]);

  // face 4: u= 0
  jac= 0;
  res= 0;
  RefElement::constraintJacobian(itrace= 4, jac);
  RefElement::constraintResidual(itrace= 4, sRef_sol, res);

  BOOST_CHECK_EQUAL(jac(0, 0), 0);
  BOOST_CHECK_EQUAL(jac(1, 0), 0);
  BOOST_CHECK_EQUAL(jac(1, 1), 0);
  BOOST_CHECK_EQUAL(jac(2, 0), 0);
  BOOST_CHECK_EQUAL(jac(2, 1), 0);
  BOOST_CHECK_EQUAL(jac(2, 2), 0);
  BOOST_CHECK_EQUAL(jac(3, 0), 0);
  BOOST_CHECK_EQUAL(jac(3, 1), 0);
  BOOST_CHECK_EQUAL(jac(3, 2), 0);
  BOOST_CHECK_EQUAL(jac(3, 3), 0);
  BOOST_CHECK_EQUAL(jac(4, 0), 0);
  BOOST_CHECK_EQUAL(jac(4, 1), 0);
  BOOST_CHECK_EQUAL(jac(4, 2), 0);
  BOOST_CHECK_EQUAL(jac(4, 3), 1);
  BOOST_CHECK_EQUAL(jac(4, 4), 0);

  BOOST_CHECK_EQUAL(res[0], 0);
  BOOST_CHECK_EQUAL(res[1], 0);
  BOOST_CHECK_EQUAL(res[2], 0);
  BOOST_CHECK_EQUAL(res[3], 0);
  BOOST_CHECK_EQUAL(res[4], sRef_sol[3]);

  // face 5: v= 0
  jac= 0;
  res= 0;
  BOOST_CHECK_THROW(RefElement::constraintJacobian(itrace= 5, jac), DeveloperException);
  BOOST_CHECK_THROW(RefElement::constraintResidual(itrace= 5, sRef_sol, res), DeveloperException);

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
