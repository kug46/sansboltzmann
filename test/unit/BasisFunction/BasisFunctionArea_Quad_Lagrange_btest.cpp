// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionArea_Quad_Lagrange_btest
// testing of BasisFunctionArea<Quad,Lagrange> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"

#include "BasisFunction/BasisFunctionLine_Lagrange.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionArea_Quad_Lagrange_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Quad::areaRef, 1., 1e-12 );
  BOOST_CHECK_CLOSE( Quad::centerRef[0], 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Quad::centerRef[1], 1./2., 1e-12 );

  BOOST_CHECK( Quad::TopoDim::D == topoDim(eQuad) );
  BOOST_CHECK( Quad::NNode == topoNNode(eQuad) );

  BOOST_CHECK( BasisFunctionAreaBase<Quad>::D == 2 );

  BOOST_CHECK( (BasisFunctionArea<Quad,Lagrange,1>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Lagrange,2>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Lagrange,3>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Lagrange,4>::D == 2) );
#if 0
  BOOST_CHECK( (BasisFunctionArea<Quad,Lagrange,5>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Lagrange,6>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Lagrange,7>::D == 2) );
#endif
  BOOST_CHECK( BasisFunctionArea_Quad_LagrangePMax == 4 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Quad>::getBasisFunction( 0, BasisFunctionCategory_Lagrange ), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Quad>::getBasisFunction(
                            BasisFunctionArea_Quad_LagrangePMax+1, BasisFunctionCategory_Lagrange ), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP1 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(1, BasisFunctionCategory_Lagrange),
                     BasisFunctionAreaBase<Quad>::LagrangeP1 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::LagrangeP1;

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 4, basis->nBasis() );
  BOOST_CHECK_EQUAL( 4, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  const int N = 4;

  Real s, t;
  Real phi[N], phis[N], phit[N];
  Real phiss[N], phist[N], phitt[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N];
  Int4 edgesign = {{+1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 5; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 0;  t = 1;
      break;

    case 4:
      s = 1./2.;  t = 1./2.;
      break;
    }

    phiTrue[0] = (1 - s)*(1 - t);
    phiTrue[1] = s*(1 - t);
    phiTrue[2] = s*t;
    phiTrue[3] = (1 - s)*t;

    phisTrue[0] = -(1 - t);
    phisTrue[1] =  (1 - t);
    phisTrue[2] =  t;
    phisTrue[3] = -t;

    phitTrue[0] = -(1 - s);
    phitTrue[1] = -s;
    phitTrue[2] =  s;
    phitTrue[3] =  (1 - s);

    for (int ki=0;ki<4;ki++)
    {
      phissTrue[ki] = 0;
      phittTrue[ki] = 0;
      phistTrue[ki] = pow(-1.,ki);
    }

    basis->evalBasis( s, t, edgesign, phi, 4 );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, 4 );
    basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, 4 );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
    }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis());
  BasisFunctionAreaBase<Quad>::LagrangeP1->coordinates( coord_s, coord_t );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], edgesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }

  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*basis->order()+1;
  for (int j = 0; j < npoints; j++)
  {
    for (int i = 0; i < npoints; i++)
    {
      // Reference coordinates
      Real sTrue = i*1/Real(npoints+1);
      Real tTrue = j*1/Real(npoints+1);

      // Interpolated reference coordinate
      basis->evalBasis( sTrue, tTrue, edgesign, phi, basis->nBasis() );

      s = 0;
      t = 0;
      for (int n = 0; n < basis->nBasis(); n++)
      {
        s += phi[n]*coord_s[n];
        t += phi[n]*coord_t[n];
      }

      // Interpolation should match
      SANS_CHECK_CLOSE( sTrue, s, small_tol, close_tol );
      SANS_CHECK_CLOSE( tTrue, t, small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP2 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(2, BasisFunctionCategory_Lagrange),
                     BasisFunctionAreaBase<Quad>::LagrangeP2 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::LagrangeP2;

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( 9, basis->nBasis() );
  BOOST_CHECK_EQUAL( 4, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 4, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 1, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  const int N = 9;

  Real s, t;
  Real phi[N], phis[N], phit[N];
  Real phiss[N], phist[N], phitt[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N];
  Int4 edgesign = {{+1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 0;  t = 1;
      break;

    case 4:
      s = 1./2.;  t = 1./2.;
      break;

    case 5:
      s = 1./3.;  t = 2./3.;
      break;
    }

    Real sphi[3], tphi[3];
    BasisFunctionLine<Lagrange,2>::self()->evalBasis(s, sphi, 3);
    BasisFunctionLine<Lagrange,2>::self()->evalBasis(t, tphi, 3);

    Real sphis[3], tphit[3];
    BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(s, sphis, 3);
    BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(t, tphit, 3);

    Real sphiss[3], tphitt[3];
    BasisFunctionLine<Lagrange,2>::self()->evalBasisHessianDerivative(s, sphiss, 3);
    BasisFunctionLine<Lagrange,2>::self()->evalBasisHessianDerivative(t, tphitt, 3);

    //phi -----------------------//
    phiTrue[0] = sphi[0]*tphi[0];
    phiTrue[1] = sphi[1]*tphi[0];
    phiTrue[2] = sphi[1]*tphi[1];
    phiTrue[3] = sphi[0]*tphi[1];

    phiTrue[4] = sphi[2]*tphi[0];
    phiTrue[5] = sphi[1]*tphi[2];
    phiTrue[6] = sphi[2]*tphi[1];
    phiTrue[7] = sphi[0]*tphi[2];

    phiTrue[8] = sphi[2]*tphi[2];

    //phis, phit ----------------------//
    BasisFunctionArea<Quad,Lagrange,2>::self()->tensorProduct(sphis, tphi , phisTrue);
    BasisFunctionArea<Quad,Lagrange,2>::self()->tensorProduct(sphi , tphit, phitTrue);

    //phiss, phist, phitt -------------//
    BasisFunctionArea<Quad,Lagrange,2>::self()->tensorProduct(sphiss, tphi  , phissTrue);
    BasisFunctionArea<Quad,Lagrange,2>::self()->tensorProduct(sphis , tphit , phistTrue);
    BasisFunctionArea<Quad,Lagrange,2>::self()->tensorProduct(sphi  , tphitt, phittTrue);

    basis->evalBasis( s, t, edgesign, phi, N );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, N );
    basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, N );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
    }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis());
  BasisFunctionAreaBase<Quad>::LagrangeP2->coordinates( coord_s, coord_t );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], edgesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }


  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*basis->order()+1;
  for (int j = 0; j < npoints; j++)
  {
    for (int i = 0; i < npoints; i++)
    {
      // Reference coordinates
      Real sTrue = i*1/Real(npoints+1);
      Real tTrue = j*1/Real(npoints+1);

      // Interpolated reference coordinate
      basis->evalBasis( sTrue, tTrue, edgesign, phi, basis->nBasis() );

      s = 0;
      t = 0;
      for (int n = 0; n < basis->nBasis(); n++)
      {
        s += phi[n]*coord_s[n];
        t += phi[n]*coord_t[n];
      }

      // Interpolation should match
      SANS_CHECK_CLOSE( sTrue, s, small_tol, close_tol );
      SANS_CHECK_CLOSE( tTrue, t, small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP3 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(3, BasisFunctionCategory_Lagrange),
                     BasisFunctionAreaBase<Quad>::LagrangeP3 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::LagrangeP3;

  BOOST_CHECK_EQUAL( 3, basis->order() );
  BOOST_CHECK_EQUAL( 16, basis->nBasis() );
  BOOST_CHECK_EQUAL( 4, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 8, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 4, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  const int N = 16;

  Real s, t;
  Real phi[N], phis[N], phit[N];
  Real phiss[N], phist[N], phitt[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N];
  Int4 edgesign = {{+1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 0;  t = 1;
      break;

    case 4:
      s = 1./2.;  t = 1./2.;
      break;

    case 5:
      s = 1./3.;  t = 2./3.;
      break;
    }

    Real sphi[4], tphi[4];
    BasisFunctionLine<Lagrange,3>::self()->evalBasis(s, sphi, 4);
    BasisFunctionLine<Lagrange,3>::self()->evalBasis(t, tphi, 4);

    Real sphis[4], tphit[4];
    BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(s, sphis, 4);
    BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(t, tphit, 4);

    Real sphiss[4], tphitt[4];
    BasisFunctionLine<Lagrange,3>::self()->evalBasisHessianDerivative(s, sphiss, 4);
    BasisFunctionLine<Lagrange,3>::self()->evalBasisHessianDerivative(t, tphitt, 4);

    //phi -----------------------//
    phiTrue[0] = sphi[0]*tphi[0];
    phiTrue[1] = sphi[1]*tphi[0];
    phiTrue[2] = sphi[1]*tphi[1];
    phiTrue[3] = sphi[0]*tphi[1];

    // Edge 0
    phiTrue[4] = sphi[2]*tphi[0];
    phiTrue[5] = sphi[3]*tphi[0];

    // Edge 1
    phiTrue[6] = sphi[1]*tphi[2];
    phiTrue[7] = sphi[1]*tphi[3];

    // Edge 2
    phiTrue[8] = sphi[3]*tphi[1];
    phiTrue[9] = sphi[2]*tphi[1];

    // Edge 3
    phiTrue[10] = sphi[0]*tphi[3];
    phiTrue[11] = sphi[0]*tphi[2];

    // Face
    phiTrue[12] = sphi[2]*tphi[2];
    phiTrue[13] = sphi[3]*tphi[2];
    phiTrue[14] = sphi[3]*tphi[3];
    phiTrue[15] = sphi[2]*tphi[3];

    //phis, phit ----------------------//
    BasisFunctionArea<Quad,Lagrange,3>::self()->tensorProduct(sphis, tphi , phisTrue);
    BasisFunctionArea<Quad,Lagrange,3>::self()->tensorProduct(sphi , tphit, phitTrue);

    //phiss, phist, phitt -------------//
    BasisFunctionArea<Quad,Lagrange,3>::self()->tensorProduct(sphiss, tphi  , phissTrue);
    BasisFunctionArea<Quad,Lagrange,3>::self()->tensorProduct(sphis , tphit , phistTrue);
    BasisFunctionArea<Quad,Lagrange,3>::self()->tensorProduct(sphi  , tphitt, phittTrue);

    basis->evalBasis( s, t, edgesign, phi, N );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, N );
    basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, N );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
    }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis());
  BasisFunctionAreaBase<Quad>::LagrangeP3->coordinates( coord_s, coord_t );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], edgesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }


  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*basis->order()+1;
  for (int j = 0; j < npoints; j++)
  {
    for (int i = 0; i < npoints; i++)
    {
      // Reference coordinates
      Real sTrue = i*1/Real(npoints+1);
      Real tTrue = j*1/Real(npoints+1);

      // Interpolated reference coordinate
      basis->evalBasis( sTrue, tTrue, edgesign, phi, basis->nBasis() );

      s = 0;
      t = 0;
      for (int n = 0; n < basis->nBasis(); n++)
      {
        s += phi[n]*coord_s[n];
        t += phi[n]*coord_t[n];
      }

      // Interpolation should match
      SANS_CHECK_CLOSE( sTrue, s, small_tol, close_tol );
      SANS_CHECK_CLOSE( tTrue, t, small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP4 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 5e-12;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(4, BasisFunctionCategory_Lagrange),
                     BasisFunctionAreaBase<Quad>::LagrangeP4 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::LagrangeP4;

  BOOST_CHECK_EQUAL(  4, basis->order() );
  BOOST_CHECK_EQUAL( 25, basis->nBasis() );
  BOOST_CHECK_EQUAL(  4, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 12, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL(  9, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  const int N = 25;

  Real s, t;
  Real phi[N], phis[N], phit[N];
  Real phiss[N], phist[N], phitt[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N];
  Int4 edgesign = {{+1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 0;  t = 1;
      break;

    case 4:
      s = 1./2.;  t = 1./2.;
      break;

    case 5:
      s = 1./3.;  t = 2./3.;
      break;
    }

    Real sphi[5], tphi[5];
    BasisFunctionLine<Lagrange,4>::self()->evalBasis(s, sphi, 5);
    BasisFunctionLine<Lagrange,4>::self()->evalBasis(t, tphi, 5);

    Real sphis[5], tphit[5];
    BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(s, sphis, 5);
    BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(t, tphit, 5);

    Real sphiss[5], tphitt[5];
    BasisFunctionLine<Lagrange,4>::self()->evalBasisHessianDerivative(s, sphiss, 5);
    BasisFunctionLine<Lagrange,4>::self()->evalBasisHessianDerivative(t, tphitt, 5);

    //phi -----------------------//
    phiTrue[0] = sphi[0]*tphi[0];
    phiTrue[1] = sphi[1]*tphi[0];
    phiTrue[2] = sphi[1]*tphi[1];
    phiTrue[3] = sphi[0]*tphi[1];

    // Edge 0
    phiTrue[4] = sphi[2]*tphi[0];
    phiTrue[5] = sphi[3]*tphi[0];
    phiTrue[6] = sphi[4]*tphi[0];

    // Edge 1
    phiTrue[7] = sphi[1]*tphi[2];
    phiTrue[8] = sphi[1]*tphi[3];
    phiTrue[9] = sphi[1]*tphi[4];

    // Edge 2
    phiTrue[10] = sphi[4]*tphi[1];
    phiTrue[11] = sphi[3]*tphi[1];
    phiTrue[12] = sphi[2]*tphi[1];

    // Edge 3
    phiTrue[13] = sphi[0]*tphi[4];
    phiTrue[14] = sphi[0]*tphi[3];
    phiTrue[15] = sphi[0]*tphi[2];

    // Face
    phiTrue[16] = sphi[2]*tphi[2];
    phiTrue[17] = sphi[4]*tphi[2];
    phiTrue[18] = sphi[4]*tphi[4];
    phiTrue[19] = sphi[2]*tphi[4];

    phiTrue[20] = sphi[3]*tphi[2];
    phiTrue[21] = sphi[4]*tphi[3];
    phiTrue[22] = sphi[3]*tphi[4];
    phiTrue[23] = sphi[2]*tphi[3];

    phiTrue[24] = sphi[3]*tphi[3];

    //phis, phit ----------------------//
    BasisFunctionArea<Quad,Lagrange,4>::self()->tensorProduct(sphis, tphi , phisTrue);
    BasisFunctionArea<Quad,Lagrange,4>::self()->tensorProduct(sphi , tphit, phitTrue);

    //phiss, phist, phitt -------------//
    BasisFunctionArea<Quad,Lagrange,4>::self()->tensorProduct(sphiss, tphi  , phissTrue);
    BasisFunctionArea<Quad,Lagrange,4>::self()->tensorProduct(sphis , tphit , phistTrue);
    BasisFunctionArea<Quad,Lagrange,4>::self()->tensorProduct(sphi  , tphitt, phittTrue);

    basis->evalBasis( s, t, edgesign, phi, N );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, N );
    basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, N );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
    }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis());
  BasisFunctionAreaBase<Quad>::LagrangeP4->coordinates( coord_s, coord_t );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], edgesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }


  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*basis->order()+1;
  for (int j = 0; j < npoints; j++)
  {
    for (int i = 0; i < npoints; i++)
    {
      // Reference coordinates
      Real sTrue = i*1/Real(npoints+1);
      Real tTrue = j*1/Real(npoints+1);

      // Interpolated reference coordinate
      basis->evalBasis( sTrue, tTrue, edgesign, phi, basis->nBasis() );

      s = 0;
      t = 0;
      for (int n = 0; n < basis->nBasis(); n++)
      {
        s += phi[n]*coord_s[n];
        t += phi[n]*coord_t[n];
      }

      // Interpolation should match
      SANS_CHECK_CLOSE( sTrue, s, small_tol, close_tol );
      SANS_CHECK_CLOSE( tTrue, t, small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( points_LagrangeP1 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const std::vector<Real> sTrue = {0.0, 1.0, 1.0, 0.0};
  const std::vector<Real> tTrue = {0.0, 0.0, 1.0, 1.0};

  const BasisFunctionArea<Quad,Lagrange,1>* basis = BasisFunctionArea<Quad,Lagrange,1>::self();
  const int N = basis->nBasis();

  std::vector<Real> sVec, tVec;
  basis->coordinates(sVec, tVec);

  BOOST_REQUIRE_EQUAL(N, sVec.size());
  BOOST_REQUIRE_EQUAL(N, tVec.size());

  for (int i = 0; i < N; i++)
  {
    SANS_CHECK_CLOSE( sTrue[i], sVec[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( tTrue[i], tVec[i], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( points_LagrangeP2 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const std::vector<Real> sTrue = {0.0, 1.0, 1.0, 0.0, 0.5, 1.0, 0.5, 0.0, 0.5};
  const std::vector<Real> tTrue = {0.0, 0.0, 1.0, 1.0, 0.0, 0.5, 1.0, 0.5, 0.5};

  const BasisFunctionArea<Quad,Lagrange,2>* basis = BasisFunctionArea<Quad,Lagrange,2>::self();
  const int N = basis->nBasis();

  std::vector<Real> sVec, tVec;
  basis->coordinates(sVec, tVec);

  BOOST_REQUIRE_EQUAL(N, sVec.size());
  BOOST_REQUIRE_EQUAL(N, tVec.size());

  for (int i = 0; i < N; i++)
  {
    SANS_CHECK_CLOSE( sTrue[i], sVec[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( tTrue[i], tVec[i], small_tol, close_tol );
  }
}

#define O3 1./3. // One over 3
#define T3 2./3. // Two over 3

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( points_LagrangeP3 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const std::vector<Real> sTrue = {0., 1., 1., 0., O3, T3, 1., 1., T3, O3, 0., 0., O3, T3, T3, O3};
  const std::vector<Real> tTrue = {0., 0., 1., 1., 0., 0., O3, T3, 1., 1., T3, O3, O3, O3, T3, T3};

  const BasisFunctionArea<Quad,Lagrange,3>* basis = BasisFunctionArea<Quad,Lagrange,3>::self();
  const int N = basis->nBasis();

  std::vector<Real> sVec, tVec;
  basis->coordinates(sVec, tVec);

  BOOST_REQUIRE_EQUAL(N, sVec.size());
  BOOST_REQUIRE_EQUAL(N, tVec.size());

  for (int i = 0; i < N; i++)
  {
    SANS_CHECK_CLOSE( sTrue[i], sVec[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( tTrue[i], tVec[i], small_tol, close_tol );
  }
}

#define Q4 1./4. // Quarter
#define H2 1./2. // Half
#define T4 3./4. // Three quarters

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( points_LagrangeP4 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

                           //SANS:  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24
  const std::vector<Real> sTrue = {0., 1., 1., 0., Q4, H2, T4, 1., 1., 1., T4, H2, Q4, 0., 0., 0., Q4, T4, T4, Q4, H2, T4, H2, Q4, H2};
  const std::vector<Real> tTrue = {0., 0., 1., 1., 0., 0., 0., Q4, H2, T4, 1., 1., 1., T4, H2, Q4, Q4, Q4, T4, T4, Q4, H2, T4, H2, H2};

  const BasisFunctionArea<Quad,Lagrange,4>* basis = BasisFunctionArea<Quad,Lagrange,4>::self();
  const int N = basis->nBasis();

  std::vector<Real> sVec, tVec;
  basis->coordinates(sVec, tVec);

  BOOST_REQUIRE_EQUAL(N, sVec.size());
  BOOST_REQUIRE_EQUAL(N, tVec.size());

  for (int i = 0; i < N; i++)
  {
    SANS_CHECK_CLOSE( sTrue[i], sVec[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( tTrue[i], tVec[i], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionArea_Quad_Lagrange_pattern.txt", true );

  BasisFunctionAreaBase<Quad>::getBasisFunction(1, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(2, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(3, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(4, BasisFunctionCategory_Lagrange)->dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
