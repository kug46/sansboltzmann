// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionArea_Triangle_Lagrange_btest
// testing of BasisFunctionArea<Triangle,Lagrange> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Bernstein.h"
#include "BasisFunction/LagrangeDOFMap.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionArea_Triangle_Bernstein_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_BernsteinP1 )
{

  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 1;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;


  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Bernstein),
                     BasisFunctionAreaBase<Triangle>::BernsteinP1 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::BernsteinP1;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Bernstein, basis->category() );

  Real s = 0.25;
  Real t = 0.4375;
  Int3 edgesign = {{+1, +1, +1}};
  Real phi[nBasis] = {0.,0.,0.};
  Real phis[nBasis] = {0.,0.,0.};
  Real phit[nBasis] = {0.,0.,0.};


  basis->evalBasis( s, t, edgesign, phi, nBasis);
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );

  Real phiTrue[3];
  phiTrue[0] = 0.3125;
  phiTrue[1] = 0.25;
  phiTrue[2] = 0.4375;

  Real phisTrue[3];
  phisTrue[0] = -1;
  phisTrue[1] =  1;
  phisTrue[2] =  0;

  Real phitTrue[3];
  phitTrue[0] = -1;
  phitTrue[1] =  0;
  phitTrue[2] =  1;

  for (int ki = 0; ki < 3; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
  }
}
#if 0

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_BernsteinP2 )
{

  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 2;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;


  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Bernstein),
                     BasisFunctionAreaBase<Triangle>::BernsteinP2 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::BernsteinP2;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Bernstein, basis->category() );

  Real s = 0.25;
  Real t = 0.25;
  Int3 edgesign = {{+1, +1, +1}};
  Real phi[6] = {0.,0.,0.,0.,0.,0.};
  Real phis[6] = {0.,0.,0.,0.,0.,0.};
  Real phit[6] = {0.,0.,0.,0.,0.,0.};
  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );

  Real phiTrue[6];
  phiTrue[0] = 0.25000;
  phiTrue[1] = 0.062500;
  phiTrue[2] = 0.062500;
  phiTrue[3] = 0.12500;
  phiTrue[4] = 0.25000;
  phiTrue[5] = 0.25000;

  Real phisTrue[6];
  phisTrue[0] =  -1.;
  phisTrue[1] =  0.5;
  phisTrue[2] =  0.;
  phisTrue[3] =  0.50000;
  phisTrue[4] = -0.50000;
  phisTrue[5] =  0.5;

  Real phitTrue[6];
  phitTrue[0] = -1.;
  phitTrue[1] =  0.;
  phitTrue[2] =  0.50000;
  phitTrue[3] =  0.50000;
  phitTrue[4] =  0.50000;
  phitTrue[5] = -0.50000;

  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
  }


}

BOOST_AUTO_TEST_CASE( basis_BernsteinP3 )
{

  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 3;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;


  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Bernstein),
                     BasisFunctionAreaBase<Triangle>::BernsteinP3 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::BernsteinP3;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Bernstein, basis->category() );

  Real s = 0.1;
  Real t = 0.3;
  Int3 edgesign = {{+1, +1, +1}};
  Real phi[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phis[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phit[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );

  Real phiTrue[10];
  phiTrue[0] = 0.21600;
  phiTrue[1] = 0.0010000;
  phiTrue[2] = 0.027000;
  phiTrue[3] = 0.0090000;
  phiTrue[4] = 0.027000;
  phiTrue[5] = 0.16200;
  phiTrue[6] = 0.32400;
  phiTrue[7] = 0.10800;
  phiTrue[8] = 0.018000;
  phiTrue[9] = 0.10800;

  Real phisTrue[10];
  phisTrue[0] = -1.0800;
  phisTrue[1] =  0.030000;
  phisTrue[2] = 0;
  phisTrue[3] =  0.18000;
  phisTrue[4] =  0.27000;
  phisTrue[5] = -0.27000;
  phisTrue[6] = -1.0800;
  phisTrue[7] =  0.72000;
  phisTrue[8] =  0.33000;
  phisTrue[9] =  0.90000;

  Real phitTrue[10];
  phitTrue[0] = -1.0800;
  phitTrue[1] =  0;
  phitTrue[2] =  0.27000;
  phitTrue[3] =  0.030000;
  phitTrue[4] =  0.18000;
  phitTrue[5] =  0.81000;
  phitTrue[6] =  0;
  phitTrue[7] = -0.36000;
  phitTrue[8] = -0.030000;
  phitTrue[9] =  0.18000;

  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
  }


}

BOOST_AUTO_TEST_CASE( basis_BernsteinP4 )
{

  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 4;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;


  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Bernstein),
                     BasisFunctionAreaBase<Triangle>::BernsteinP4 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::BernsteinP4;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Bernstein, basis->category() );

  Real s = 0.1;
  Real t = 0.3;
  Int3 edgesign = {{+1, +1, +1}};
  Real phi[15] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phis[15] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phit[15] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );


  Real phiTrue[15];
  phiTrue[0] = 0.12960;
  phiTrue[1] = 0.0001;
  phiTrue[2] = 0.0081000;
  phiTrue[3] = 0.0012000;
  phiTrue[4] = 0.0054000;
  phiTrue[5] = 0.010800;
  phiTrue[6] = 0.064800;
  phiTrue[7] = 0.19440;
  phiTrue[8] = 0.25920;
  phiTrue[9] = 0.086400;
  phiTrue[10] = 0.021600;
  phiTrue[11] = 0.0024000;
  phiTrue[12] = 0.12960;
  phiTrue[13] = 0.021600;
  phiTrue[14] = 0.064800;

  Real phisTrue[15];
  phisTrue[0] = -0.86400;
  phisTrue[1] =  0.0040000;
  phisTrue[2] = 0;
  phisTrue[3] =  0.036000;
  phisTrue[4] =  0.10800;
  phisTrue[5] =  0.10800;
  phisTrue[6] = -0.10800;
  phisTrue[7] = -0.64800;
  phisTrue[8] = -1.2960;
  phisTrue[9] =  0.43200;
  phisTrue[10] =  0.36000;
  phisTrue[11] =  0.068000;
  phisTrue[12] =  0.86400;
  phisTrue[13] =  0.39600;
  phisTrue[14] =  0.54000;

  Real phitTrue[15];
  phitTrue[0] = -0.86400;
  phitTrue[1] = 0;
  phitTrue[2] =  0.10800;
  phitTrue[3] =  0.0040000;
  phitTrue[4] =  0.036000;
  phitTrue[5] =  0.10800;
  phitTrue[6] =  0.54000;
  phitTrue[7] =  0.64800;
  phitTrue[8] = -0.43200;
  phitTrue[9] = -0.43200;
  phitTrue[10] = -0.072000;
  phitTrue[11] = -0.0040000;
  phitTrue[12] =  0;
  phitTrue[13] =  0.036000;
  phitTrue[14] =  0.32400;




  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
  }


}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
