// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementLine_btest
// testing of Element class with Line specializiation

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Field/Element/ElementNode.h"
#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementArea.h"
#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementSpacetime.h"

#include "BasisFunction/LagrangeDOFMap.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope_Lagrange.h"

#include "Quadrature/QuadratureVolume.h"

using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( LagrangeDOFMap_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  std::vector<int> canonicalNodes = LagrangeDOFMap<Line>::getCanonicalNodes();

  BOOST_REQUIRE_EQUAL( 2, canonicalNodes.size() );
  BOOST_CHECK_EQUAL( 0, canonicalNodes[0] );
  BOOST_CHECK_EQUAL( 1, canonicalNodes[1] );

  std::vector< std::vector<int> > tracesCanonicalNodes = LagrangeDOFMap<Line>::getTracesCanonicalNodes();

  BOOST_REQUIRE_EQUAL( 2, tracesCanonicalNodes.size() );
  BOOST_REQUIRE_EQUAL( 1, tracesCanonicalNodes[0].size() );
  BOOST_REQUIRE_EQUAL( 1, tracesCanonicalNodes[1].size() );

  BOOST_CHECK_EQUAL( 1, tracesCanonicalNodes[0][0] );
  BOOST_CHECK_EQUAL( 0, tracesCanonicalNodes[1][0] );

  std::vector<TopologyTypes> tracesTopo = LagrangeDOFMap<Line>::getTracesTopo();

  BOOST_CHECK_EQUAL( eNode, tracesTopo[0] );
  BOOST_CHECK_EQUAL( eNode, tracesTopo[1] );

  BOOST_CHECK_THROW( LagrangeDOFMap<Line>::getCanonicalTraceMap(2, 1), DeveloperException ); // Non-existing trace

  typedef Element< Real, TopoD1, Line > CellClass;
  typedef Element< Real, TopoD0, Node > TraceClass;

  std::vector<Real> s; //Lagrange nodes in the element, also used here to construct the element
  std::vector<int> traceMap, cellMap;
  int itrace;

  // Check that when traces are extracted from the element the interpolated
  // values on the trace elements matches that of the cell element

  for (int order = 1; order <= BasisFunctionLine_LagrangePMax; order++)
  {
    CellClass  cell(order, BasisFunctionCategory_Lagrange);
    TraceClass trace0(0, BasisFunctionCategory_Legendre);
    TraceClass trace1(0, BasisFunctionCategory_Legendre);

    BOOST_CHECK_EQUAL( cell.nDOF(), LagrangeDOFMap<Line>::nBasis(order) );

    getLagrangeNodes_Line(order, s);

    for (int i = 0; i < cell.nDOF(); i++)
      cell.DOF(i) = s[i];

    itrace = 0;
    traceMap = LagrangeDOFMap<Line>::getCanonicalTraceMap(itrace, order);
    trace0.DOF(0) = cell.DOF(traceMap[0]);

    SANS_CHECK_CLOSE( cell.eval(1), trace0.eval(0), small_tol, close_tol );

    itrace = 1;
    traceMap = LagrangeDOFMap<Line>::getCanonicalTraceMap(itrace, order);
    trace1.DOF(0) = cell.DOF(traceMap[0]);

    SANS_CHECK_CLOSE( cell.eval(0), trace1.eval(0), small_tol, close_tol );

    cellMap = LagrangeDOFMap<Line>::getCellMap( order );

    for (std::size_t i = 0; i < cellMap.size(); i++)
    {
      BOOST_CHECK_EQUAL( cellMap[i], i+2);
//      std::cout << "cellMap[i] = " << cellMap[i] << std::endl;
    }

    for (int dof = 0; dof < cell.nDOF(); dof++)
    {
      canonicalNodes = LagrangeDOFMap<Line>::getNodalSupport( dof, order );
      if (dof == 0 || dof == 1)
      {
        BOOST_CHECK_EQUAL( 1, static_cast<int>(canonicalNodes.size() ) );
        BOOST_CHECK_EQUAL( dof, canonicalNodes[0] );
      }
      else
      {
        BOOST_CHECK_EQUAL( 2, static_cast<int>(canonicalNodes.size() ) );
        BOOST_CHECK_EQUAL( 0, canonicalNodes[0] );
        BOOST_CHECK_EQUAL( 1, canonicalNodes[1] );
      }

    }
  }



}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_test )
{
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-10;
  int iedge;

  std::vector<int> canonicalNodes = LagrangeDOFMap<Triangle>::getCanonicalNodes();

  BOOST_REQUIRE_EQUAL( 3, canonicalNodes.size() );
  BOOST_CHECK_EQUAL( 0, canonicalNodes[0] );
  BOOST_CHECK_EQUAL( 1, canonicalNodes[1] );
  BOOST_CHECK_EQUAL( 2, canonicalNodes[2] );

  std::vector< std::vector<int> > tracesCanonicalNodes = LagrangeDOFMap<Triangle>::getTracesCanonicalNodes();

  const int (*TraceNodes)[ Line::NNode ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  BOOST_REQUIRE_EQUAL( 3, tracesCanonicalNodes.size() );

  iedge = 0;
  BOOST_REQUIRE_EQUAL( 2, tracesCanonicalNodes[iedge].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][0], tracesCanonicalNodes[iedge][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][1], tracesCanonicalNodes[iedge][1] );

  iedge = 1;
  BOOST_REQUIRE_EQUAL( 2, tracesCanonicalNodes[iedge].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][0], tracesCanonicalNodes[iedge][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][1], tracesCanonicalNodes[iedge][1] );

  iedge = 2;
  BOOST_REQUIRE_EQUAL( 2, tracesCanonicalNodes[iedge].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][0], tracesCanonicalNodes[iedge][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][1], tracesCanonicalNodes[iedge][1] );

  std::vector<TopologyTypes> tracesTopo = LagrangeDOFMap<Triangle>::getTracesTopo();

  BOOST_REQUIRE_EQUAL( 3, tracesTopo.size() );
  BOOST_CHECK_EQUAL( eLine, tracesTopo[0] );
  BOOST_CHECK_EQUAL( eLine, tracesTopo[1] );
  BOOST_CHECK_EQUAL( eLine, tracesTopo[2] );

  BOOST_CHECK_THROW( LagrangeDOFMap<Triangle>::getCanonicalTraceMap(3, 1), AssertionException ); // Non-existing trace

  typedef DLA::VectorS<2,Real> VectorX;
  typedef Element< VectorX, TopoD2, Triangle > CellClass;
  typedef Element< VectorX, TopoD1,     Line > TraceClass;

  std::vector<Real> s, t; //Lagrange nodes in the element, also used here to construct the element
  std::vector<int> traceMap;
  CanonicalTraceToCell canonicalTrace;
  canonicalTrace.orientation = 1; // All orientation should be canonical

  Real sRefC, tRefC; // Cell coordinates
  Real sRefT;        // Trace coordinates

  for (int order = 1; order <= BasisFunctionArea_Triangle_LagrangePMax; order++)
  {
    CellClass  cell(order, BasisFunctionCategory_Lagrange);
    TraceClass trace(order, BasisFunctionCategory_Lagrange);

    BOOST_CHECK_EQUAL( cell.nDOF(), LagrangeDOFMap<Triangle>::nBasis(order) );

    getLagrangeNodes_Triangle(order, s, t);

    for (int i = 0; i < cell.nDOF(); i++)
      cell.DOF(i) = {s[i], t[i]};

    for (int itrace = 0; itrace < Triangle::NTrace; itrace++)
    {
      canonicalTrace.trace = itrace;
      traceMap = LagrangeDOFMap<Triangle>::getCanonicalTraceMap(canonicalTrace.trace, order);

      BOOST_REQUIRE_EQUAL( trace.nDOF(), traceMap.size() );

      // copy DOF's from the cell element to the trace based on the trace map
      for (int i = 0; i < trace.nDOF(); i++)
        trace.DOF(i) = cell.DOF(traceMap[i]);

      int kmax = order+2;
      for (int k = 0; k < kmax; k++)
      {
        // trace reference-element coordinate
        sRefT = k/static_cast<Real>(kmax-1);

        // cell reference-element coords
        TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sRefT, sRefC, tRefC );

        SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC)[0], trace.eval(sRefT)[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC)[1], trace.eval(sRefT)[1], small_tol, close_tol );
      }
    }

    // check that cell dofs are the interior to a triangle

    std::vector<int> cellMap = LagrangeDOFMap<Triangle>::getCellMap( order );
    if ( order < 3)
    {
      BOOST_CHECK( cellMap.empty() );
    }
    else if (order == 3)
    {
      BOOST_CHECK_EQUAL( cellMap[0], 9 );
    }
    else if (order ==  4)
    {
      BOOST_CHECK_EQUAL( cellMap[0], 12 );
      BOOST_CHECK_EQUAL( cellMap[1], 13 );
      BOOST_CHECK_EQUAL( cellMap[2], 14 );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_test )
{
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-10;
  int iedge;

  std::vector<int> canonicalNodes = LagrangeDOFMap<Quad>::getCanonicalNodes();

  BOOST_REQUIRE_EQUAL( 4, canonicalNodes.size() );
  BOOST_CHECK_EQUAL( 0, canonicalNodes[0] );
  BOOST_CHECK_EQUAL( 1, canonicalNodes[1] );
  BOOST_CHECK_EQUAL( 2, canonicalNodes[2] );
  BOOST_CHECK_EQUAL( 3, canonicalNodes[3] );

  std::vector< std::vector<int> > tracesCanonicalNodes = LagrangeDOFMap<Quad>::getTracesCanonicalNodes();

  const int (*TraceNodes)[ Line::NNode ] = TraceToCellRefCoord<Line, TopoD2, Quad>::TraceNodes;

  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes.size() );

  iedge = 0;
  BOOST_REQUIRE_EQUAL( 2, tracesCanonicalNodes[iedge].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][0], tracesCanonicalNodes[iedge][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][1], tracesCanonicalNodes[iedge][1] );

  iedge = 1;
  BOOST_REQUIRE_EQUAL( 2, tracesCanonicalNodes[iedge].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][0], tracesCanonicalNodes[iedge][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][1], tracesCanonicalNodes[iedge][1] );

  iedge = 2;
  BOOST_REQUIRE_EQUAL( 2, tracesCanonicalNodes[iedge].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][0], tracesCanonicalNodes[iedge][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][1], tracesCanonicalNodes[iedge][1] );

  iedge = 3;
  BOOST_REQUIRE_EQUAL( 2, tracesCanonicalNodes[iedge].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][0], tracesCanonicalNodes[iedge][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iedge][1], tracesCanonicalNodes[iedge][1] );

  std::vector<TopologyTypes> tracesTopo = LagrangeDOFMap<Quad>::getTracesTopo();

  BOOST_REQUIRE_EQUAL( 4, tracesTopo.size() );
  BOOST_CHECK_EQUAL( eLine, tracesTopo[0] );
  BOOST_CHECK_EQUAL( eLine, tracesTopo[1] );
  BOOST_CHECK_EQUAL( eLine, tracesTopo[2] );
  BOOST_CHECK_EQUAL( eLine, tracesTopo[3] );

  BOOST_CHECK_THROW( LagrangeDOFMap<Quad>::getCanonicalTraceMap(4, 1), AssertionException ); // Non-existing trace
//  BOOST_CHECK_THROW( LagrangeDOFMap<Quad>::getCanonicalTraceMap(0, BasisFunctionArea_Quad_LagrangePMax+1), DeveloperException ); // Order wrong

  typedef DLA::VectorS<2,Real> VectorX;
  typedef Element< VectorX, TopoD2, Quad > CellClass;
  typedef Element< VectorX, TopoD1, Line > TraceClass;

  std::vector<Real> s, t; //Lagrange nodes in the element, also used here to construct the element
  std::vector<int> traceMap;
  CanonicalTraceToCell canonicalTrace;
  canonicalTrace.orientation = 1; // All orientation should be canonical

  Real sRefC, tRefC; // Cell coordinates
  Real sRefT;        // Trace coordinates

//  for (int order = 1; order <= BasisFunctionArea_Quad_LagrangePMax; order++)
  for (int order = 1; order <= 1; order++)
  {
    CellClass  cell(order, BasisFunctionCategory_Lagrange);
    TraceClass trace(order, BasisFunctionCategory_Lagrange);

    BOOST_CHECK_EQUAL( cell.nDOF(), LagrangeDOFMap<Quad>::nBasis(order) );

    getLagrangeNodes_Quad(order, s, t);

    for (int i = 0; i < cell.nDOF(); i++)
      cell.DOF(i) = {s[i], t[i]};

    for (int itrace = 0; itrace < Quad::NTrace; itrace++)
    {
      canonicalTrace.trace = itrace;
      traceMap = LagrangeDOFMap<Quad>::getCanonicalTraceMap(canonicalTrace.trace, order);

      BOOST_REQUIRE_EQUAL( trace.nDOF(), traceMap.size() );

      // copy DOF's from the cell element to the trace based on the trace map
      for (int i = 0; i < trace.nDOF(); i++)
        trace.DOF(i) = cell.DOF(traceMap[i]);

      int kmax = order+2;
      for (int k = 0; k < kmax; k++)
      {
        // trace reference-element coordinate
        sRefT = k/static_cast<Real>(kmax-1);

        // cell reference-element coords
        TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalTrace, sRefT, sRefC, tRefC );

        SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC)[0], trace.eval(sRefT)[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC)[1], trace.eval(sRefT)[1], small_tol, close_tol );
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tet_test )
{
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-10;
  int itri;

  std::vector<int> canonicalNodes = LagrangeDOFMap<Tet>::getCanonicalNodes();

  BOOST_REQUIRE_EQUAL( 4, canonicalNodes.size() );
  BOOST_CHECK_EQUAL( 0, canonicalNodes[0] );
  BOOST_CHECK_EQUAL( 1, canonicalNodes[1] );
  BOOST_CHECK_EQUAL( 2, canonicalNodes[2] );
  BOOST_CHECK_EQUAL( 3, canonicalNodes[3] );

  std::vector< std::vector<int> > tracesCanonicalNodes = LagrangeDOFMap<Tet>::getTracesCanonicalNodes();

  const int (*TraceNodes)[ Triangle::NNode ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes.size() );

  itri = 0;
  BOOST_REQUIRE_EQUAL( 3, tracesCanonicalNodes[itri].size() );
  BOOST_CHECK_EQUAL( TraceNodes[itri][0], tracesCanonicalNodes[itri][0] );
  BOOST_CHECK_EQUAL( TraceNodes[itri][1], tracesCanonicalNodes[itri][1] );
  BOOST_CHECK_EQUAL( TraceNodes[itri][2], tracesCanonicalNodes[itri][2] );

  itri = 1;
  BOOST_REQUIRE_EQUAL( 3, tracesCanonicalNodes[itri].size() );
  BOOST_CHECK_EQUAL( TraceNodes[itri][0], tracesCanonicalNodes[itri][0] );
  BOOST_CHECK_EQUAL( TraceNodes[itri][1], tracesCanonicalNodes[itri][1] );
  BOOST_CHECK_EQUAL( TraceNodes[itri][2], tracesCanonicalNodes[itri][2] );

  itri = 2;
  BOOST_REQUIRE_EQUAL( 3, tracesCanonicalNodes[itri].size() );
  BOOST_CHECK_EQUAL( TraceNodes[itri][0], tracesCanonicalNodes[itri][0] );
  BOOST_CHECK_EQUAL( TraceNodes[itri][1], tracesCanonicalNodes[itri][1] );
  BOOST_CHECK_EQUAL( TraceNodes[itri][2], tracesCanonicalNodes[itri][2] );

  itri = 3;
  BOOST_REQUIRE_EQUAL( 3, tracesCanonicalNodes[itri].size() );
  BOOST_CHECK_EQUAL( TraceNodes[itri][0], tracesCanonicalNodes[itri][0] );
  BOOST_CHECK_EQUAL( TraceNodes[itri][1], tracesCanonicalNodes[itri][1] );
  BOOST_CHECK_EQUAL( TraceNodes[itri][2], tracesCanonicalNodes[itri][2] );

  std::vector<TopologyTypes> tracesTopo = LagrangeDOFMap<Tet>::getTracesTopo();

  BOOST_REQUIRE_EQUAL( 4, tracesTopo.size() );
  BOOST_CHECK_EQUAL( eTriangle, tracesTopo[0] );
  BOOST_CHECK_EQUAL( eTriangle, tracesTopo[1] );
  BOOST_CHECK_EQUAL( eTriangle, tracesTopo[2] );
  BOOST_CHECK_EQUAL( eTriangle, tracesTopo[3] );

  BOOST_CHECK_THROW( LagrangeDOFMap<Tet>::getCanonicalTraceMap(4, 1), AssertionException ); // Non-existing trace
  BOOST_CHECK_THROW( LagrangeDOFMap<Tet>::getCanonicalTraceMap(0, BasisFunctionVolume_Tet_LagrangePMax+1), DeveloperException ); // Order wrong

  typedef DLA::VectorS<3,Real> VectorX;
  typedef Element< VectorX, TopoD3,      Tet > CellClass;
  typedef Element< VectorX, TopoD2, Triangle > TraceClass;

  std::vector<Real> s, t, u; //Lagrange nodes in the element, also used here to construct the element
  std::vector<int> traceMap;
  CanonicalTraceToCell canonicalTrace;
  canonicalTrace.orientation = 1; // All orientation should be canonical

  Real sRefC, tRefC, uRefC; // Cell coordinates
  Real sRefT, tRefT;        // Trace coordinates

  for (int order = 1; order <= BasisFunctionVolume_Tet_LagrangePMax; order++)
  {
    CellClass  cell(order, BasisFunctionCategory_Lagrange);
    TraceClass trace(order, BasisFunctionCategory_Lagrange);

    BOOST_CHECK_EQUAL( cell.nDOF(), LagrangeDOFMap<Tet>::nBasis(order) );

    getLagrangeNodes_Tet(order, s, t, u);

    for (int i = 0; i < cell.nDOF(); i++)
      cell.DOF(i) = {s[i], t[i], u[i]};

    //std::cout << "order = " << order << std::endl;
    for (int itrace = 0; itrace < Tet::NTrace; itrace++)
    {
      canonicalTrace.trace = itrace;
      traceMap = LagrangeDOFMap<Tet>::getCanonicalTraceMap(canonicalTrace.trace, order);

      BOOST_REQUIRE_EQUAL( trace.nDOF(), traceMap.size() );

      // copy DOF's from the cell element to the trace based on the trace map
      for (int i = 0; i < trace.nDOF(); i++)
        trace.DOF(i) = cell.DOF(traceMap[i]);

      //std::cout << "trace = " << itrace << std::endl;
      int kmax = order+2;
      for (int ks = 0; ks < kmax; ks++)
      {
        // trace reference-element coordinate
        sRefT = ks/static_cast<Real>(kmax-1);

        for (int kt = 0; kt < kmax-ks; kt++)
        {
          // trace reference-element coordinate
          tRefT = (1-sRefT)*kt/static_cast<Real>(std::max(kmax-ks-1,1));
          //std::cout << "s, t = " << sRefT << ", " << tRefT << std::endl;

          // cell reference-element coords
          TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTrace, sRefT, tRefT, sRefC, tRefC, uRefC );

          SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC, uRefC)[0], trace.eval(sRefT, tRefT)[0], small_tol, close_tol );
          SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC, uRefC)[1], trace.eval(sRefT, tRefT)[1], small_tol, close_tol );
          SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC, uRefC)[2], trace.eval(sRefT, tRefT)[2], small_tol, close_tol );
        }
      }
    }

    // check that cell dofs are the interior to a tet

    std::vector<int> cellMap = LagrangeDOFMap<Tet>::getCellMap( order );
    if ( order < 4)
    {
      BOOST_CHECK( cellMap.empty() );
    }
    else if (order == 4)
    {
      BOOST_CHECK_EQUAL( cellMap[0], 34 );
    }

  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hex_test )
{
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-10;
  int iquad;

  std::vector<int> canonicalNodes = LagrangeDOFMap<Hex>::getCanonicalNodes();

  BOOST_REQUIRE_EQUAL( 8, canonicalNodes.size() );
  BOOST_CHECK_EQUAL( 0, canonicalNodes[0] );
  BOOST_CHECK_EQUAL( 1, canonicalNodes[1] );
  BOOST_CHECK_EQUAL( 2, canonicalNodes[2] );
  BOOST_CHECK_EQUAL( 3, canonicalNodes[3] );
  BOOST_CHECK_EQUAL( 4, canonicalNodes[4] );
  BOOST_CHECK_EQUAL( 5, canonicalNodes[5] );
  BOOST_CHECK_EQUAL( 6, canonicalNodes[6] );
  BOOST_CHECK_EQUAL( 7, canonicalNodes[7] );

  std::vector< std::vector<int> > tracesCanonicalNodes = LagrangeDOFMap<Hex>::getTracesCanonicalNodes();

  const int (*TraceNodes)[ Quad::NNode ] = TraceToCellRefCoord<Quad, TopoD3, Hex>::TraceNodes;

  BOOST_REQUIRE_EQUAL( 6, tracesCanonicalNodes.size() );

  iquad = 0;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[iquad].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][0], tracesCanonicalNodes[iquad][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][1], tracesCanonicalNodes[iquad][1] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][2], tracesCanonicalNodes[iquad][2] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][3], tracesCanonicalNodes[iquad][3] );

  iquad = 1;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[iquad].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][0], tracesCanonicalNodes[iquad][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][1], tracesCanonicalNodes[iquad][1] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][2], tracesCanonicalNodes[iquad][2] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][3], tracesCanonicalNodes[iquad][3] );

  iquad = 2;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[iquad].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][0], tracesCanonicalNodes[iquad][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][1], tracesCanonicalNodes[iquad][1] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][2], tracesCanonicalNodes[iquad][2] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][3], tracesCanonicalNodes[iquad][3] );

  iquad = 3;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[iquad].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][0], tracesCanonicalNodes[iquad][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][1], tracesCanonicalNodes[iquad][1] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][2], tracesCanonicalNodes[iquad][2] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][3], tracesCanonicalNodes[iquad][3] );

  iquad = 4;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[iquad].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][0], tracesCanonicalNodes[iquad][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][1], tracesCanonicalNodes[iquad][1] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][2], tracesCanonicalNodes[iquad][2] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][3], tracesCanonicalNodes[iquad][3] );

  iquad = 5;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[iquad].size() );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][0], tracesCanonicalNodes[iquad][0] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][1], tracesCanonicalNodes[iquad][1] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][2], tracesCanonicalNodes[iquad][2] );
  BOOST_CHECK_EQUAL( TraceNodes[iquad][3], tracesCanonicalNodes[iquad][3] );

  std::vector<TopologyTypes> tracesTopo = LagrangeDOFMap<Hex>::getTracesTopo();

  BOOST_REQUIRE_EQUAL( 6, tracesTopo.size() );
  BOOST_CHECK_EQUAL( eQuad, tracesTopo[0] );
  BOOST_CHECK_EQUAL( eQuad, tracesTopo[1] );
  BOOST_CHECK_EQUAL( eQuad, tracesTopo[2] );
  BOOST_CHECK_EQUAL( eQuad, tracesTopo[3] );
  BOOST_CHECK_EQUAL( eQuad, tracesTopo[4] );
  BOOST_CHECK_EQUAL( eQuad, tracesTopo[5] );

  BOOST_CHECK_THROW( LagrangeDOFMap<Hex>::getCanonicalTraceMap(6, 1), AssertionException ); // Non-existing trace
  BOOST_CHECK_THROW( LagrangeDOFMap<Hex>::getCanonicalTraceMap(0, BasisFunctionVolume_Hex_LagrangePMax+1), DeveloperException ); // Order wrong

  typedef DLA::VectorS<3,Real> VectorX;
  typedef Element< VectorX, TopoD3,  Hex > CellClass;
  typedef Element< VectorX, TopoD2, Quad > TraceClass;

  std::vector<Real> s, t, u; //Lagrange nodes in the element, also used here to construct the element
  std::vector<int> traceMap;
  CanonicalTraceToCell canonicalTrace;
  canonicalTrace.orientation = 1; // All orientation should be canonical

  Real sRefC, tRefC, uRefC; // Cell coordinates
  Real sRefT, tRefT;        // Trace coordinates

  for (int order = 1; order <= BasisFunctionVolume_Hex_LagrangePMax; order++)
  {
    //std::cout << " ORDER      " << order << std::endl;
    CellClass  cell(order, BasisFunctionCategory_Lagrange);
    TraceClass trace(order, BasisFunctionCategory_Lagrange);

    BOOST_CHECK_EQUAL( cell.nDOF(), LagrangeDOFMap<Hex>::nBasis(order) );

    getLagrangeNodes_Hex(order, s, t, u);

    for (int i = 0; i < cell.nDOF(); i++)
      cell.DOF(i) = {s[i], t[i], u[i]};

    //std::cout << "order = " << order << std::endl;
    for (int itrace = 0; itrace < Hex::NTrace; itrace++)
    {
      canonicalTrace.trace = itrace;
      traceMap = LagrangeDOFMap<Hex>::getCanonicalTraceMap(canonicalTrace.trace, order);

      BOOST_REQUIRE_EQUAL( trace.nDOF(), traceMap.size() );

      // copy DOF's from the cell element to the trace based on the trace map
      for (int i = 0; i < trace.nDOF(); i++)
        trace.DOF(i) = cell.DOF(traceMap[i]);

        //std::cout << "trace = " << itrace << std::endl;
      int kmax = order+2;
      for (int ks = 0; ks < kmax; ks++)
      {
        // trace reference-element coordinate
        sRefT = ks/static_cast<Real>(kmax-1);

        for (int kt = 0; kt < kmax; kt++)
        {
          // trace reference-element coordinate
          tRefT = kt/static_cast<Real>(kmax-1);
          //std::cout << "s, t = " << sRefT << ", " << tRefT << std::endl;

          // cell reference-element coords
          TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalTrace, sRefT, tRefT, sRefC, tRefC, uRefC );

          SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC, uRefC)[0], trace.eval(sRefT, tRefT)[0], small_tol, close_tol );
          SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC, uRefC)[1], trace.eval(sRefT, tRefT)[1], small_tol, close_tol );
          SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC, uRefC)[2], trace.eval(sRefT, tRefT)[2], small_tol, close_tol );
        }
      }
    }
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pentatope_test )
{
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-10;
  int itet;

  std::vector<int> canonicalNodes = LagrangeDOFMap<Pentatope>::getCanonicalNodes();

  BOOST_REQUIRE_EQUAL( 5, canonicalNodes.size() );
  BOOST_CHECK_EQUAL( 0, canonicalNodes[0] );
  BOOST_CHECK_EQUAL( 1, canonicalNodes[1] );
  BOOST_CHECK_EQUAL( 2, canonicalNodes[2] );
  BOOST_CHECK_EQUAL( 3, canonicalNodes[3] );
  BOOST_CHECK_EQUAL( 4, canonicalNodes[4] );

  std::vector< std::vector<int> > tracesCanonicalNodes = LagrangeDOFMap<Pentatope>::getTracesCanonicalNodes();

  const int (*TraceNodes)[ Tet::NNode ] = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::TraceNodes;

  BOOST_REQUIRE_EQUAL( 5, tracesCanonicalNodes.size() );

  itet = 0;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[itet].size() );
  BOOST_CHECK_EQUAL( TraceNodes[itet][0], tracesCanonicalNodes[itet][0] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][1], tracesCanonicalNodes[itet][1] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][2], tracesCanonicalNodes[itet][2] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][3], tracesCanonicalNodes[itet][3] );

  itet = 1;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[itet].size() );
  BOOST_CHECK_EQUAL( TraceNodes[itet][0], tracesCanonicalNodes[itet][0] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][1], tracesCanonicalNodes[itet][1] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][2], tracesCanonicalNodes[itet][2] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][3], tracesCanonicalNodes[itet][3] );

  itet = 2;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[itet].size() );
  BOOST_CHECK_EQUAL( TraceNodes[itet][0], tracesCanonicalNodes[itet][0] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][1], tracesCanonicalNodes[itet][1] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][2], tracesCanonicalNodes[itet][2] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][3], tracesCanonicalNodes[itet][3] );

  itet = 3;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[itet].size() );
  BOOST_CHECK_EQUAL( TraceNodes[itet][0], tracesCanonicalNodes[itet][0] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][1], tracesCanonicalNodes[itet][1] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][2], tracesCanonicalNodes[itet][2] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][3], tracesCanonicalNodes[itet][3] );

  itet = 4;
  BOOST_REQUIRE_EQUAL( 4, tracesCanonicalNodes[itet].size() );
  BOOST_CHECK_EQUAL( TraceNodes[itet][0], tracesCanonicalNodes[itet][0] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][1], tracesCanonicalNodes[itet][1] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][2], tracesCanonicalNodes[itet][2] );
  BOOST_CHECK_EQUAL( TraceNodes[itet][3], tracesCanonicalNodes[itet][3] );

  std::vector<TopologyTypes> tracesTopo = LagrangeDOFMap<Pentatope>::getTracesTopo();

  BOOST_REQUIRE_EQUAL( 5, tracesTopo.size() );
  BOOST_CHECK_EQUAL( eTet, tracesTopo[0] );
  BOOST_CHECK_EQUAL( eTet, tracesTopo[1] );
  BOOST_CHECK_EQUAL( eTet, tracesTopo[2] );
  BOOST_CHECK_EQUAL( eTet, tracesTopo[3] );
  BOOST_CHECK_EQUAL( eTet, tracesTopo[4] );

  //BOOST_CHECK_THROW( LagrangeDOFMap<Tet>::getCanonicalTraceMap(4, 1), AssertionException ); // Non-existing trace
  //BOOST_CHECK_THROW( LagrangeDOFMap<Tet>::getCanonicalTraceMap(0, BasisFunctionVolume_Tet_LagrangePMax+1), DeveloperException ); // Order wrong

  typedef DLA::VectorS<4,Real> VectorX;
  typedef Element< VectorX, TopoD4, Pentatope > CellClass;
  typedef Element< VectorX, TopoD3, Tet > TraceClass;

  std::vector<Real> s, t, u, v; //Lagrange nodes in the element, also used here to construct the element
  std::vector<int> traceMap;
  CanonicalTraceToCell canonicalTrace;
  canonicalTrace.orientation = 1; // All orientation should be canonical

  Real sRefC, tRefC, uRefC, vRefC; // Cell coordinates
  Real sRefT, tRefT, uRefT;        // Trace coordinates

  // can only go to p = 2 until trace dof ordering matches that of tetrahedra
  for (int order = 1; order <= 2; order++)
  {
    CellClass  cell(order, BasisFunctionCategory_Lagrange);
    TraceClass trace(order, BasisFunctionCategory_Lagrange);

    BOOST_CHECK_EQUAL( cell.nDOF(), LagrangeDOFMap<Pentatope>::nBasis(order) );

    getLagrangeNodes_Pentatope(order, s, t, u, v);

    for (int i = 0; i < cell.nDOF(); i++)
      cell.DOF(i) = {s[i], t[i], u[i], v[i]};

    //std::cout << "order = " << order << std::endl;
    for (int itrace = 0; itrace < Pentatope::NTrace; itrace++)
    {
      canonicalTrace.trace = itrace;
      traceMap = LagrangeDOFMap<Pentatope>::getCanonicalTraceMap(canonicalTrace.trace, order);

      BOOST_REQUIRE_EQUAL( trace.nDOF(), traceMap.size() );

      // copy DOF's from the cell element to the trace based on the trace map
      for (int i = 0; i < trace.nDOF(); i++)
        trace.DOF(i) = cell.DOF(traceMap[i]);

      // loop through quadrature points and ensure evaluation is the same from both sides
      QuadratureVolume<Tet> quad(0);
      for (int i=0;i<quad.nQuadrature();i++)
      {
        quad.coordinates( i , sRefT , tRefT , uRefT );
        TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalTrace, sRefT, tRefT, uRefT, sRefC, tRefC, uRefC, vRefC );

        // ensure the coordinate evaluation on both trace and cells match
        SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC, uRefC, vRefC)[0], trace.eval(sRefT, tRefT, uRefT)[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC, uRefC, vRefC)[1], trace.eval(sRefT, tRefT, uRefT)[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC, uRefC, vRefC)[2], trace.eval(sRefT, tRefT, uRefT)[2], small_tol, close_tol );
        SANS_CHECK_CLOSE( cell.eval(sRefC, tRefC, uRefC, vRefC)[3], trace.eval(sRefT, tRefT, uRefT)[3], small_tol, close_tol );

      }
    }

  }


}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
