// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunction_RefElement_Split_TraceToCell_btest
// testing of reference coordinate transformations

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunction_RefElement_Split_TraceToCell.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunction_RefElement_Split_TraceToCell_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_Split_test )
{
  typedef DLA::VectorS<TopoD1::D,Real> RefCoord_Trace;
  typedef DLA::VectorS<TopoD1::D,Real> RefCoord_Cell;
  typedef BasisFunction_RefElement_Split_TraceToCell<TopoD1, Line> RefElementSplitType;

  const Real tol = 1e-13;

  RefCoord_Trace ref_trace;
  RefCoord_Cell ref_cell;
  int split_edge_index = -1;

  ref_trace[0] = 0.0;
  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );

  //Incorrect split-type
  split_edge_index = -1;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell)),
                     DeveloperException );

  //Incorrect split_edge_index
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 0, ref_cell)),
                     DeveloperException );

  //Incorrect sub_trace_index
  split_edge_index = -1;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 1, ref_cell)),
                     DeveloperException );

  //Incorrect trace ref-coord
  ref_trace[0] = 0.5;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 0, ref_cell)),
                     DeveloperException );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_Split_test )
{
  typedef DLA::VectorS<TopoD1::D,Real> RefCoord_Trace;
  typedef DLA::VectorS<TopoD2::D,Real> RefCoord_Cell;
  typedef BasisFunction_RefElement_Split_TraceToCell<TopoD2, Triangle> RefElementSplitType;

  const Real tol = 1e-13;

  RefCoord_Trace ref_trace;
  RefCoord_Cell ref_cell;

  //Splitting trace 0
  int split_edge_index = 0;

  ref_trace = {0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.5, tol );

  ref_trace = {0.5};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.25, tol );

  ref_trace = {1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );

  //Splitting trace 1
  split_edge_index = 1;

  ref_trace = {0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.5, tol );

  ref_trace = {0.5};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.25, tol );

  ref_trace = {1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );

  //Splitting trace 2
  split_edge_index = 2;

  ref_trace = {0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );

  ref_trace = {0.5};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.50, tol );

  ref_trace = {1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 1.0, tol );


  //Uniform split - sub element 0 --------------------
  split_edge_index = -1;

  ref_trace = {0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.5, tol );

  ref_trace = {0.5};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.25, tol );

  ref_trace = {1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );

  //Uniform split - sub element 1 --------------------
  split_edge_index = -1;

  ref_trace = {0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 1, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.5, tol );

  ref_trace = {0.5};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 1, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.50, tol );

  ref_trace = {1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 1, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.5, tol );

  //Uniform split - sub element 2 --------------------
  split_edge_index = -1;

  ref_trace = {0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 2, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );

  ref_trace = {0.5};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 2, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.25, tol );

  ref_trace = {1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 2, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.5, tol );


  //Incorrect split-type
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Last, split_edge_index, 0, ref_cell)),
                     DeveloperException );

  //Incorrect sub-trace-index
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 1, ref_cell)),
                     DeveloperException );

  //Incorrect sub-trace-index
  split_edge_index = -1;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 3, ref_cell)),
                     DeveloperException );

  //Incorrect split_edge_index
  split_edge_index = 3;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell)),
                     DeveloperException );

  //Incorrect split_edge_index
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 0, ref_cell)),
                     DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_Split_test )
{
  typedef DLA::VectorS<TopoD1::D,Real> RefCoord_Trace;
  typedef DLA::VectorS<TopoD2::D,Real> RefCoord_Cell;
  typedef BasisFunction_RefElement_Split_TraceToCell<TopoD2, Quad> RefElementSplitType;

  RefCoord_Trace ref_trace;
  RefCoord_Cell ref_cell;

  int split_edge_index = 0;

  //Unsupported
  ref_trace = {0.0};
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell)),
                     DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tet_Split_test )
{
  typedef DLA::VectorS<TopoD2::D,Real> RefCoord_Trace;
  typedef DLA::VectorS<TopoD3::D,Real> RefCoord_Cell;
  typedef BasisFunction_RefElement_Split_TraceToCell<TopoD3, Tet> RefElementSplitType;

  const Real tol = 1e-13;

  RefCoord_Trace ref_trace;
  RefCoord_Cell ref_cell;

  //Splitting edge 0 ---------------------------------
  int split_edge_index = 0;

  ref_trace = {0.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {1.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {0.0, 1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.5, tol );

  ref_trace = {0.2, 0.6};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.3, tol );

  //Splitting edge 1 ---------------------------------
  split_edge_index = 1;

  ref_trace = {0.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {1.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {0.0, 1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.5, tol );

  ref_trace = {0.2, 0.6};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.3, tol );

  //Splitting edge 2 ---------------------------------
  split_edge_index = 2;

  ref_trace = {0.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {1.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 1.0, tol );

  ref_trace = {0.0, 1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {0.2, 0.6};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.2, tol );

  //Splitting edge 3 ---------------------------------
  split_edge_index = 3;

  ref_trace = {0.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {1.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {0.0, 1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 1.0, tol );

  ref_trace = {0.2, 0.6};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.6, tol );

  //Splitting edge 4 ---------------------------------
  split_edge_index = 4;

  ref_trace = {0.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {1.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {0.0, 1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.5, tol );

  ref_trace = {0.2, 0.6};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.3, tol );

  //Splitting edge 5 ---------------------------------
  split_edge_index = 5;

  ref_trace = {0.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {1.0, 0.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.0, tol );

  ref_trace = {0.0, 1.0};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 1.0, tol );

  ref_trace = {0.2, 0.6};
  RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.6, tol );


  //Isotropic splits ---------------------------------
  split_edge_index = -1;
  ref_trace = {0.2, 0.6};

  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.3, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 1, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.5, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 2, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.1, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 3, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.3, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 4, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.4, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 5, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.3, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 6, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.4, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 7, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.1, tol );

  //Isotropic face splits - face 0---------------------------------
  int split_face_index = 0;

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.3, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 1, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.4, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 2, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.1, tol );

  //Isotropic face splits - face 1---------------------------------
  split_face_index = 1;

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.6, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.2, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 1, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.3, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 2, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.3, tol );

  //Isotropic face splits - face 2---------------------------------
  split_face_index = 2;

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.6, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.1, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 1, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.4, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 2, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.3, tol );

  //Isotropic face splits - face 3---------------------------------
  split_face_index = 3;

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 0, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.6, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 1, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.3, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.2, tol );

  RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 2, ref_cell);
  BOOST_CHECK_CLOSE( ref_cell[0], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[1], 0.1, tol );
  BOOST_CHECK_CLOSE( ref_cell[2], 0.6, tol );

  //Incorrect split-type
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Last, split_edge_index, 0, ref_cell)),
                     DeveloperException );

  //Incorrect sub-trace-index
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 1, ref_cell)),
                     DeveloperException );

  //Incorrect sub-trace-index
  split_edge_index = -1;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 8, ref_cell)),
                     DeveloperException );

  //Incorrect sub-trace-index
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 3, ref_cell)),
                     DeveloperException );

  //Incorrect split_edge_index
  split_edge_index = 6;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell)),
                     DeveloperException );

  //Incorrect split_edge_index
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Isotropic, split_edge_index, 0, ref_cell)),
                     DeveloperException );

  //Incorrect split_face_index
  split_face_index = 4;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::IsotropicFace, split_face_index, 0, ref_cell)),
                     DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hex_Split_test )
{
  typedef DLA::VectorS<TopoD2::D,Real> RefCoord_Trace;
  typedef DLA::VectorS<TopoD3::D,Real> RefCoord_Cell;
  typedef BasisFunction_RefElement_Split_TraceToCell<TopoD3, Hex> RefElementSplitType;

  RefCoord_Trace ref_trace;
  RefCoord_Cell ref_cell;

  int split_edge_index = 0;

  //Unsupported
  ref_trace = {0.0, 0.0};
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_trace, ElementSplitType::Edge, split_edge_index, 0, ref_cell)),
                     DeveloperException );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
