// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionArea_Triangle_Lagrange_btest
// testing of BasisFunctionArea<Triangle,Lagrange> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/LagrangeDOFMap.h"
#include "BasisFunctionArea_Triangle_Lagrange_known.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionArea_Triangle_Lagrange_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Triangle::areaRef, 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Triangle::centerRef[0], 1./3., 1e-12 );
  BOOST_CHECK_CLOSE( Triangle::centerRef[1], 1./3., 1e-12 );

  BOOST_CHECK( Triangle::TopoDim::D == topoDim(eTriangle) );
  BOOST_CHECK( Triangle::NNode == topoNNode(eTriangle) );

  BOOST_CHECK( BasisFunctionAreaBase<Triangle>::D == 2 );

  BOOST_CHECK( (BasisFunctionArea<Triangle,Lagrange,1>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Lagrange,2>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Lagrange,3>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Lagrange,4>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Lagrange,5>::D == 2) );
  BOOST_CHECK( BasisFunctionArea_Triangle_LagrangePMax == 5 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Triangle>::getBasisFunction( 0, BasisFunctionCategory_Lagrange ), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Triangle>::getBasisFunction( 6, BasisFunctionCategory_Lagrange ), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP1 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 1;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Lagrange),
                     BasisFunctionAreaBase<Triangle>::LagrangeP1 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LagrangeP1;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s, t;
  Real phi[3] = {0.,0.,0.}, phis[3] = {0.,0.,0.}, phit[3] = {0.,0.,0.};
  Real phiss[3] = {0.,0.,0.}, phist[3] = {0.,0.,0.}, phitt[3] = {0.,0.,0.};
  Real phiTrue[3] = {0.,0.,0.}, phisTrue[3] = {0.,0.,0.}, phitTrue[3] = {0.,0.,0.};
  Real phissTrue[3] = {0.,0.,0.}, phistTrue[3] = {0.,0.,0.}, phittTrue[3] = {0.,0.,0.};
  Int3 edgesign = {{+1, +1, +1}};

  for (int ki = 0; ki < nBasis; ki++)
  {
    phisTrue[ki] = 0; phitTrue[ki] = 0; // Suppress clang analyzer
  }

  {
  s = .25;  t = .5;
  lagrange_p1_test1_true(phiTrue,phisTrue,phitTrue,phissTrue,phistTrue,phittTrue);
  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis());
  BasisFunctionAreaBase<Triangle>::LagrangeP1->coordinates( coord_s, coord_t );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], edgesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP2 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 2;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Lagrange),
                     BasisFunctionAreaBase<Triangle>::LagrangeP2 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LagrangeP2;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s, t;
  Real phi[6], phis[6], phit[6], phiss[6], phist[6], phitt[6];
  Real phiTrue[6], phisTrue[6], phitTrue[6];
  Real phissTrue[6], phistTrue[6], phittTrue[6];
  Int3 edgesign = {{+1, +1, +1}};

  {
  s = .25;  t = .5;
  lagrange_p2_test1_true(phiTrue,phisTrue,phitTrue,phissTrue,phistTrue,phittTrue);
  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis());
  BasisFunctionAreaBase<Triangle>::LagrangeP2->coordinates( coord_s, coord_t );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], edgesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP3 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 3;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Lagrange),
                     BasisFunctionAreaBase<Triangle>::LagrangeP3 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LagrangeP3;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s, t;
  Real phi[10], phis[10], phit[10], phiss[10], phist[10], phitt[10];
  Real phiTrue[10], phisTrue[10], phitTrue[10];
  Real phissTrue[10], phistTrue[10], phittTrue[10];
  Int3 edgesign = {{+1, +1, +1}};

  {
  s = .25;  t = .5;
  lagrange_p3_test1_true(phiTrue,phisTrue,phitTrue,phissTrue,phistTrue,phittTrue);
  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis());
  BasisFunctionAreaBase<Triangle>::LagrangeP3->coordinates( coord_s, coord_t );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], edgesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP4 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 4;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Lagrange),
                     BasisFunctionAreaBase<Triangle>::LagrangeP4 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LagrangeP4;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s, t;
  Real phi[15], phis[15], phit[15], phiss[15], phist[15], phitt[15];
  Real phiTrue[15], phisTrue[15], phitTrue[15];
  Real phissTrue[15], phistTrue[15], phittTrue[15];
  Int3 edgesign = {{+1, +1, +1}};

  {
  s = .25;  t = .5;
  lagrange_p4_test1_true(phiTrue,phisTrue,phitTrue,phissTrue,phistTrue,phittTrue);
  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis());
  BasisFunctionAreaBase<Triangle>::LagrangeP4->coordinates( coord_s, coord_t );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], edgesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP5 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-9;
  const Real close_tol = 1e-9;

  const int order  = 5;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Lagrange),
                     BasisFunctionAreaBase<Triangle>::LagrangeP5 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LagrangeP5;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s, t;
  Real phi[21], phis[21], phit[21], phiss[21], phist[21], phitt[21];
  Real phiTrue[21], phisTrue[21], phitTrue[21];
  Real phissTrue[21], phistTrue[21], phittTrue[21];
  Int3 edgesign = {{+1, +1, +1}};

  {
  s = .25;  t = .5;
  lagrange_p5_test1_true(phiTrue,phisTrue,phitTrue,phissTrue,phistTrue,phittTrue);
  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis());
  BasisFunctionAreaBase<Triangle>::LagrangeP5->coordinates( coord_s, coord_t );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], edgesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( points_LagrangeP1 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const std::vector<Real> sTrue = {0.0, 1.0, 0.0};
  const std::vector<Real> tTrue = {0.0, 0.0, 1.0};

  const BasisFunctionArea<Triangle,Lagrange,1>* basis = BasisFunctionArea<Triangle,Lagrange,1>::self();
  const int N = basis->nBasis();

  std::vector<Real> sVec, tVec;
  basis->coordinates(sVec, tVec);

  BOOST_CHECK_EQUAL(N, sVec.size());
  BOOST_CHECK_EQUAL(N, tVec.size());

  for (int i = 0; i < N; i++)
  {
    SANS_CHECK_CLOSE( sTrue[i], sVec[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( tTrue[i], tVec[i], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( points_LagrangeP2 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const std::vector<Real> sTrue = {0.0, 1.0, 0.0, 0.5, 0.0, 0.5};
  const std::vector<Real> tTrue = {0.0, 0.0, 1.0, 0.5, 0.5, 0.0};

  const BasisFunctionArea<Triangle,Lagrange,2>* basis = BasisFunctionArea<Triangle,Lagrange,2>::self();
  const int N = basis->nBasis();

  std::vector<Real> sVec, tVec;
  basis->coordinates(sVec, tVec);

  BOOST_CHECK_EQUAL(N, sVec.size());
  BOOST_CHECK_EQUAL(N, tVec.size());

  for (int i = 0; i < N; i++)
  {
    SANS_CHECK_CLOSE( sTrue[i], sVec[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( tTrue[i], tVec[i], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( points_LagrangeP3 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const std::vector<Real> sTrue = {0.0, 1.0, 0.0, 2./3., 1./3., 0.0, 0.0, 1./3., 2./3., 1./3.};
  const std::vector<Real> tTrue = {0.0, 0.0, 1.0, 1./3., 2./3., 2./3., 1./3., 0.0, 0.0, 1./3.};

  const BasisFunctionArea<Triangle,Lagrange,3>* basis = BasisFunctionArea<Triangle,Lagrange,3>::self();
  const int N = basis->nBasis();

  std::vector<Real> sVec, tVec;
  basis->coordinates(sVec, tVec);

  BOOST_CHECK_EQUAL(N, sVec.size());
  BOOST_CHECK_EQUAL(N, tVec.size());

  for (int i = 0; i < N; i++)
  {
    SANS_CHECK_CLOSE( sTrue[i], sVec[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( tTrue[i], tVec[i], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( points_LagrangeP4 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const std::vector<Real> sTrue = {0.0, 1.0, 0.0,
                                   0.75, 0.5, 0.25,
                                   0.0, 0.0, 0.0,
                                   0.25, 0.5, 0.75,
                                   0.25, 0.5, 0.25
                                  };
  const std::vector<Real> tTrue = {0.0, 0.0, 1.0,
                                   0.25, 0.5, 0.75,
                                   0.75, 0.5, 0.25,
                                   0.0, 0.0, 0.0,
                                   0.25, 0.25, 0.5
                                  };

  const BasisFunctionArea<Triangle,Lagrange,4>* basis = BasisFunctionArea<Triangle,Lagrange,4>::self();
  const int N = basis->nBasis();

  std::vector<Real> sVec, tVec;
  basis->coordinates(sVec, tVec);

  BOOST_CHECK_EQUAL(N, sVec.size());
  BOOST_CHECK_EQUAL(N, tVec.size());

  for (int i = 0; i < N; i++)
  {
    SANS_CHECK_CLOSE( sTrue[i], sVec[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( tTrue[i], tVec[i], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( points_LagrangeP5 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const std::vector<Real> sTrue = {0.0, 1.0, 0.0,
                                   0.8, 0.6, 0.4, 0.2,
                                   0.0, 0.0, 0.0, 0.0,
                                   0.2, 0.4, 0.6, 0.8,
                                   0.2, 0.4, 0.6, 0.2, 0.4, 0.2
                                  };
  const std::vector<Real> tTrue = {0.0, 0.0, 1.0,
                                   0.2, 0.4, 0.6, 0.8,
                                   0.8, 0.6, 0.4, 0.2,
                                   0.0, 0.0, 0.0, 0.0,
                                   0.2, 0.2, 0.2, 0.4, 0.4, 0.6
                                  };

  const BasisFunctionArea<Triangle,Lagrange,5>* basis = BasisFunctionArea<Triangle,Lagrange,5>::self();
  const int N = basis->nBasis();

  std::vector<Real> sVec, tVec;
  basis->coordinates(sVec, tVec);

  BOOST_CHECK_EQUAL(N, sVec.size());
  BOOST_CHECK_EQUAL(N, tVec.size());

  for (int i = 0; i < N; i++)
  {
    SANS_CHECK_CLOSE( sTrue[i], sVec[i], small_tol, close_tol );
    SANS_CHECK_CLOSE( tTrue[i], tVec[i], small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionArea_Triangle_Lagrange_pattern.txt", true );

  BasisFunctionAreaBase<Triangle>::getBasisFunction(1, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(2, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(3, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(4, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(5, BasisFunctionCategory_Lagrange)->dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
