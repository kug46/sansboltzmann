// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionLine_btest
// testing of BasisFunctionLine classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/range_c.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionLine.h"
#include "Quadrature/QuadratureLine.h"
#include "Topology/ElementTopology.h"

#include "BasisFunctionLine_Lagrange_known.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionLine_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Line::centerRef[0], 1/2., 1e-12 );

  BOOST_CHECK( Line::TopoDim::D == topoDim(eLine) );
  BOOST_CHECK( Line::NNode == topoNNode(eLine) );

  BOOST_CHECK( BasisFunctionLineBase::D == 1 );

  BOOST_CHECK( (BasisFunctionLine<Hierarchical,1>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Hierarchical,2>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Hierarchical,3>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Hierarchical,4>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Hierarchical,5>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Hierarchical,6>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Hierarchical,7>::D == 1) );

  BOOST_CHECK( (BasisFunctionLine<Legendre,0>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Legendre,1>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Legendre,2>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Legendre,3>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Legendre,4>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Legendre,5>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Legendre,6>::D == 1) );
  BOOST_CHECK( (BasisFunctionLine<Legendre,7>::D == 1) );

  BOOST_CHECK( BasisFunctionLine_HierarchicalPMax == 7 );
  BOOST_CHECK( BasisFunctionLine_LegendrePMax == 7 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  int orderMax;

  orderMax = BasisFunctionLine_HierarchicalPMax;
  BOOST_CHECK_THROW( BasisFunctionLineBase::getBasisFunction(          0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionLineBase::getBasisFunction( orderMax+1, BasisFunctionCategory_Hierarchical), DeveloperException );

  orderMax = BasisFunctionLine_LegendrePMax;
  BOOST_CHECK_THROW( BasisFunctionLineBase::getBasisFunction(         -1, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionLineBase::getBasisFunction( orderMax+1, BasisFunctionCategory_Legendre), DeveloperException );

  orderMax = BasisFunctionLine_LagrangePMax;
  BOOST_CHECK_THROW( BasisFunctionLineBase::getBasisFunction(         0, BasisFunctionCategory_Lagrange), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionLineBase::getBasisFunction(orderMax+1, BasisFunctionCategory_Lagrange), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP1 )
{
  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(1, BasisFunctionCategory_Hierarchical),
                     BasisFunctionLineBase::HierarchicalP1 );

  const BasisFunctionLine<Hierarchical,1>* basis = BasisFunctionLine<Hierarchical,1>::self();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 2, basis->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s;
  Real phi[2], phis[2], phiss[2];
  Real phiTrue[2], phisTrue[2], phissTrue[2];
  int k;

  for (int test = 0; test < 2; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;
      break;

    case 1:
      s = 1;
      break;

    case 2:
      s = 0.5;
      break;
    default:
      s = -1;
      BOOST_CHECK( false );
    }

    phiTrue[0] = 1 - s;
    phiTrue[1] = s;

    phisTrue[0] = -1;
    phisTrue[1] =  1;

    phissTrue[0] = 0;
    phissTrue[1] = 0;

    basis->evalBasis( s, phi, 2 );
    basis->evalBasisDerivative( s, phis, 2 );
    basis->evalBasisHessianDerivative( s, phiss, 2 );
    for (k = 0; k < 2; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], tol, tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP2 )
{
  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(2, BasisFunctionCategory_Hierarchical),
                     BasisFunctionLineBase::HierarchicalP2 );

  const BasisFunctionLine<Hierarchical,2>* basis = BasisFunctionLine<Hierarchical,2>::self();

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( 3, basis->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 1, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s;
  Real phi[3], phis[3], phiss[3];
  Real phiTrue[3], phisTrue[3], phissTrue[3];
  int k;

  for (int test = 0; test < 2; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;
      break;

    case 1:
      s = 1;
      break;

    case 2:
      s = 0.5;
      break;
    default:
      s = -1;
      BOOST_CHECK( false );
    }

    phiTrue[0] = 1 - s;
    phiTrue[1] = s;
    phiTrue[2] = 4*s*(1 - s);

    phisTrue[0] = -1;
    phisTrue[1] =  1;
    phisTrue[2] =  4*(1 - 2*s);

    phissTrue[0] = 0;
    phissTrue[1] = 0;
    phissTrue[2] = -8;

    basis->evalBasis( s, phi, 3 );
    basis->evalBasisDerivative( s, phis, 3 );
    basis->evalBasisHessianDerivative( s, phiss, 3 );
    for (k = 0; k < 3; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], tol,tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP3 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(3, BasisFunctionCategory_Hierarchical),
                     BasisFunctionLineBase::HierarchicalP3 );

  const BasisFunctionLine<Hierarchical,3>* basis = BasisFunctionLine<Hierarchical,3>::self();

  BOOST_CHECK_EQUAL( 3, basis->order() );
  BOOST_CHECK_EQUAL( 4, basis->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s;
  Real phi[4], phis[4], phiss[4];

  {
  s = 0;
  Real phiTrue[4]   = {1, 0, 0, 0};
  Real phisTrue[4]  = {-1, 1, 4, 6*sqrt(3)};
  Real phissTrue[4] = {0, 0, -8, -36*sqrt(3)};

  basis->evalBasis( s, phi, 4 );
  basis->evalBasisDerivative( s, phis, 4 );
  basis->evalBasisHessianDerivative( s, phiss, 4 );
  for (int k = 0; k < 4; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 1;
  Real phiTrue[4]  = {0, 1, 0, 0};
  Real phisTrue[4] = {-1, 1, -4, 6*sqrt(3)};
  Real phissTrue[4] = {0, 0, -8, 36*sqrt(3)};

  basis->evalBasis( s, phi, 4 );
  basis->evalBasisDerivative( s, phis, 4 );
  basis->evalBasisHessianDerivative( s, phiss, 4 );
  for (int k = 0; k < 4; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.5;
  Real phiTrue[4]  = {0.5, 0.5, 1, 0};
  Real phisTrue[4] = {-1, 1, 0, -3*sqrt(3)};
  Real phissTrue[4] = {0, 0, -8, 0};

  basis->evalBasis( s, phi, 4 );
  basis->evalBasisDerivative( s, phis, 4 );
  basis->evalBasisHessianDerivative( s, phiss, 4 );
  for (int k = 0; k < 4; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP4 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(4, BasisFunctionCategory_Hierarchical),
                     BasisFunctionLineBase::HierarchicalP4 );

  const BasisFunctionLine<Hierarchical,4>* basis = BasisFunctionLine<Hierarchical,4>::self();

  BOOST_CHECK_EQUAL( 4, basis->order() );
  BOOST_CHECK_EQUAL( 5, basis->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 3, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s;
  Real phi[5], phis[5], phiss[5];

  {
  s = 0;
  Real phiTrue[5]  = {1, 0, 0, 0, 0};
  Real phisTrue[5] = {-1, 1, 4, 6*sqrt(3), 0};
  Real phissTrue[5] = {0, 0, -8, -36*sqrt(3), 32};

  basis->evalBasis( s, phi, 5 );
  basis->evalBasisDerivative( s, phis, 5 );
  basis->evalBasisHessianDerivative( s, phiss, 5 );
  for (int k = 0; k < 5; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 1;
  Real phiTrue[5]  = {0, 1, 0, 0, 0};
  Real phisTrue[5] = {-1, 1, -4, 6*sqrt(3), 0};
  Real phissTrue[5] = {0, 0, -8, 36*sqrt(3), 32};

  basis->evalBasis( s, phi, 5 );
  basis->evalBasisDerivative( s, phis, 5 );
  basis->evalBasisHessianDerivative( s, phiss, 5 );
  for (int k = 0; k < 5; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.5;
  Real phiTrue[5]  = {0.5, 0.5, 1, 0, 1};
  Real phisTrue[5] = {-1, 1, 0, -3*sqrt(3), 0};
  Real phissTrue[5] = {0, 0, -8, 0, -16};

  basis->evalBasis( s, phi, 5 );
  basis->evalBasisDerivative( s, phis, 5 );
  basis->evalBasisHessianDerivative( s, phiss, 5 );
  for (int k = 0; k < 5; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.1;
  Real phiTrue[5]  = {0.9, 0.1, 9/25., 54*sqrt(3)/125., 81/625.};
  Real phisTrue[5] = {-1, 1, 3.2, 69*sqrt(3)/25., 288/125.};
  Real phissTrue[5] = {0, 0, -8, -144*sqrt(3)/5., 368/25.};

  basis->evalBasis( s, phi, 5 );
  basis->evalBasisDerivative( s, phis, 5 );
  basis->evalBasisHessianDerivative( s, phiss, 5 );
  for (int k = 0; k < 5; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.9;
  Real phiTrue[5]  = {0.1, 0.9, 9/25., -54*sqrt(3)/125., 81/625.};
  Real phisTrue[5] = {-1, 1, -3.2, 69*sqrt(3)/25., -288/125.};
  Real phissTrue[5] = {0, 0, -8, 144*sqrt(3)/5., 368/25.};

  basis->evalBasis( s, phi, 5 );
  basis->evalBasisDerivative( s, phis, 5 );
  basis->evalBasisHessianDerivative( s, phiss, 5 );
  for (int k = 0; k < 5; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP5 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  const int nB = 6;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(5, BasisFunctionCategory_Hierarchical),
                     BasisFunctionLineBase::HierarchicalP5 );

  const BasisFunctionLine<Hierarchical,5>* basis = BasisFunctionLine<Hierarchical,5>::self();

  BOOST_CHECK_EQUAL(  5, basis->order() );
  BOOST_CHECK_EQUAL( nB, basis->nBasis() );
  BOOST_CHECK_EQUAL(  2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL(  4, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s;
  Real phi[nB], phis[nB], phiss[nB];

  {
  s = 0;
  Real phiTrue[nB]    = { 1, 0, 0,           0,  0, 0};
  Real phisTrue[nB]   = {-1, 1, 4,   6*sqrt(3),  0, 0};
  Real phissTrue[nB]  = { 0, 0,-8, -36*sqrt(3), 32, 50*sqrt(5)};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 1;
  Real phiTrue[nB]   = { 0, 1,  0,          0,  0, 0};
  Real phisTrue[nB]  = {-1, 1, -4,  6*sqrt(3),  0, 0};
  Real phissTrue[nB] = { 0, 0, -8, 36*sqrt(3), 32,-50*sqrt(5) };

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.5;
  Real phiTrue[nB]   = {0.5, 0.5, 1, 0, 1, 0};
  Real phisTrue[nB]  = {-1, 1, 0, -3*sqrt(3), 0, -25*sqrt(5)/8.};
  Real phissTrue[nB] = {0, 0, -8, 0, -16, 0};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.1;
  Real phiTrue[nB]   = { 0.9, 0.1, 9/25., 54*sqrt(3)/125., 81/625., 81/(100*sqrt(5))};
  Real phisTrue[nB]  = {-1  , 1  ,   3.2,   69*sqrt(3)/25., 288/125., 99/(8*sqrt(5))};
  Real phissTrue[nB] = {   0,   0,    -8, -144*sqrt(3)/5., 368/25., 4*sqrt(5)};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.9;
  Real phiTrue[nB]   = { 0.1, 0.9, 9/25., -54*sqrt(3)/125.,  81/625., -81/(100*sqrt(5))};
  Real phisTrue[nB]  = {-1  , 1,    -3.2,  69*sqrt(3)/25., -288/125.,  99/(8*sqrt(5))};
  Real phissTrue[nB] = {0, 0, -8, 144*sqrt(3)/5., 368/25., -4*sqrt(5.)};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP6 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  const int nB = 7;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(6, BasisFunctionCategory_Hierarchical),
                     BasisFunctionLineBase::HierarchicalP6 );

  const BasisFunctionLine<Hierarchical,6>* basis = BasisFunctionLine<Hierarchical,6>::self();

  BOOST_CHECK_EQUAL( nB-1, basis->order() );
  BOOST_CHECK_EQUAL( nB,   basis->nBasis() );
  BOOST_CHECK_EQUAL(    2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nB-2, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s;
  Real phi[nB], phis[nB], phiss[nB];

  {
  s = 0;
  Real phiTrue[nB]   = { 1, 0, 0,          0,  0,          0, 0};
  Real phisTrue[nB]  = {-1, 1, 4,  6*sqrt(3),  0,          0, 0};
  Real phissTrue[nB] = { 0, 0,-8,-36*sqrt(3), 32, 50*sqrt(5), 0};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 1;
  Real phiTrue[nB]  = { 0, 1,  0,         0, 0, 0, 0};
  Real phisTrue[nB] = {-1, 1, -4, 6*sqrt(3), 0, 0, 0};
  Real phissTrue[nB] = { 0, 0, -8, 36*sqrt(3), 32,-50*sqrt(5), 0};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.5;
  Real phiTrue[nB]   = { 0.5, 0.5, 1,          0, 1,              0, 1};
  Real phisTrue[nB]  = {-1  , 1  , 0, -3*sqrt(3), 0, -25*sqrt(5)/8., 0};
  Real phissTrue[nB] = {0, 0, -8, 0, -16, 0, -24};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.1;
  Real phiTrue[nB]   = { 0.9, 0.1, 9/25., 54*sqrt(3)/125., 81/625., 81/(100*sqrt(5)), 729/15625.};
  Real phisTrue[nB]  = {-1  , 1  ,   3.2, 69*sqrt(3)/25., 288/125., 99/(8*sqrt(5)),   3888/3125.};
  Real phissTrue[nB] = {   0,   0,   -8, -144*sqrt(3)/5., 368/25., 4*sqrt(5), 2376/125.};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.9;
  Real phiTrue[nB]   = { 0.1, 0.9, 9/25., -54*sqrt(3)/125.,  81/625., -81/(100*sqrt(5)), 729/15625.};
  Real phisTrue[nB]  = {-1  , 1,    -3.2,  69*sqrt(3)/25., -288/125.,  99/(8*sqrt(5)),  -3888/3125.};
  Real phissTrue[nB] = {0, 0, -8, 144*sqrt(3)/5., 368/25., -4*sqrt(5.), 2376/125.};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP7 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  const int nB = 8;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(7, BasisFunctionCategory_Hierarchical),
                     BasisFunctionLineBase::HierarchicalP7 );

  const BasisFunctionLine<Hierarchical,7>* basis = BasisFunctionLine<Hierarchical,7>::self();

  BOOST_CHECK_EQUAL( nB-1, basis->order() );
  BOOST_CHECK_EQUAL( nB,   basis->nBasis() );
  BOOST_CHECK_EQUAL(    2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nB-2, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s;
  Real phi[nB], phis[nB], phiss[nB];

  {
  s = 0;
  Real phiTrue[nB]   = { 1, 0,  0,           0,  0,          0, 0, 0};
  Real phisTrue[nB]  = {-1, 1,  4,   6*sqrt(3),  0,          0, 0, 0};
  Real phissTrue[nB] = { 0, 0, -8, -36*sqrt(3), 32, 50*sqrt(5), 0, 0};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 1;
  Real phiTrue[nB]   = { 0, 1,  0,         0,   0,           0, 0, 0};
  Real phisTrue[nB]  = {-1, 1, -4,  6*sqrt(3),  0,           0, 0, 0};
  Real phissTrue[nB] = { 0, 0, -8, 36*sqrt(3), 32, -50*sqrt(5), 0, 0};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.5;
  Real phiTrue[nB]   = { 0.5, 0.5,  1,          0,   1,              0,   1,                 0};
  Real phisTrue[nB]  = {-1  , 1  ,  0, -3*sqrt(3),   0, -25*sqrt(5)/8.,   0, -343*sqrt(7)/108.};
  Real phissTrue[nB] = { 0  , 0  , -8,          0, -16,              0, -24,                 0};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.1;
  Real phiTrue[nB]   = { 0.9, 0.1, 9/25.,   54*sqrt(3)/125., 81/625., 81/(100*sqrt(5)), 729/15625., 9261*sqrt(7)/156250.};
  Real phisTrue[nB]  = {-1  , 1  ,   3.2,   69*sqrt(3)/25., 288/125., 99/(8*sqrt(5)), 3888/3125., 89523*sqrt(7)/62500.};
  Real phissTrue[nB] = { 0  , 0  ,  -8  , -144*sqrt(3)/5., 368/25., 4*sqrt(5), 2376/125., 50764*sqrt(7)/3125.};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.9;
  Real phiTrue[nB]   = { 0.1, 0.9, 9/25., -54*sqrt(3)/125.,  81/625., -81/(100*sqrt(5)), 729/15625., -9261*sqrt(7)/156250.};
  Real phisTrue[nB]  = {-1  , 1,    -3.2,  69*sqrt(3)/25., -288/125.,  99/(8*sqrt(5)),  -3888/3125.,  89523*sqrt(7)/62500.};
  Real phissTrue[nB] = {0, 0, -8, 144*sqrt(3)/5., 368/25., -4*sqrt(5), 2376/125., -50764*sqrt(7)/3125.};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP0 )
{
  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(0, BasisFunctionCategory_Legendre),
                     BasisFunctionLineBase::LegendreP0 );

  const BasisFunctionLine<Legendre,0>* basis = BasisFunctionLine<Legendre,0>::self();

  BOOST_CHECK_EQUAL( 0, basis->order() );
  BOOST_CHECK_EQUAL( 1, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 1, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s;
  Real phi[1], phis[1], phiss[1];
  Real phiTrue[1], phisTrue[1], phissTrue[1];
  int k;

  for (int test = 0; test < 2; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;
      break;

    case 1:
      s = 1;
      break;

    case 2:
      s = 0.5;
      break;
    default:
      s = -1;
      BOOST_CHECK( false );
    }

    phiTrue[0] = 1;

    phisTrue[0] = 0;

    phissTrue[0] = 0;

    basis->evalBasis( s, phi, 1 );
    basis->evalBasisDerivative( s, phis, 1 );
    basis->evalBasisHessianDerivative( s , phiss, 1 );
    for (k = 0; k < 1; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP1 )
{
  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(1, BasisFunctionCategory_Legendre),
                     BasisFunctionLineBase::LegendreP1 );

  const BasisFunctionLine<Legendre,1>* basis = BasisFunctionLine<Legendre,1>::self();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 2, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s;
  Real phi[2], phis[2], phiss[2];
  Real phiTrue[2], phisTrue[2], phissTrue[2];
  int k;

  for (int test = 0; test < 2; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;
      break;

    case 1:
      s = 1;
      break;

    case 2:
      s = 0.5;
      break;
    default:
      s = -1;
      BOOST_CHECK( false );
    }

    phiTrue[0] = 1;
    phiTrue[1] = sqrt(3)*(2*s - 1);

    phisTrue[0] = 0;
    phisTrue[1] = 2*sqrt(3);

    phissTrue[0] = 0;
    phissTrue[1] = 0;

    basis->evalBasis( s, phi, 2 );
    basis->evalBasisDerivative( s, phis, 2 );
    basis->evalBasisHessianDerivative( s, phiss, 2 );
    for (k = 0; k < 2; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP2 )
{
  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(2, BasisFunctionCategory_Legendre),
                     BasisFunctionLineBase::LegendreP2 );

  const BasisFunctionLine<Legendre,2>* basis = BasisFunctionLine<Legendre,2>::self();

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( 3, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 3, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s;
  Real phi[3], phis[3], phiss[3];
  Real phiTrue[3], phisTrue[3], phissTrue[3];
  int k;

  for (int test = 0; test < 2; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;
      break;

    case 1:
      s = 1;
      break;

    case 2:
      s = 0.5;
      break;
    default:
      s = -1;
      BOOST_CHECK( false );
    }

    phiTrue[0] = 1;
    phiTrue[1] = sqrt(3)*(2*s - 1);
    phiTrue[2] = sqrt(5)*(6*s*(s - 1) + 1);

    phisTrue[0] = 0;
    phisTrue[1] = 2*sqrt(3);
    phisTrue[2] = 6*sqrt(5)*(2*s - 1);

    phissTrue[0] = 0;
    phissTrue[1] = 0;
    phissTrue[2] = 12*sqrt(5);

    basis->evalBasis( s, phi, 3 );
    basis->evalBasisDerivative( s, phis, 3 );
    basis->evalBasisHessianDerivative( s, phiss, 3 );
    for (k = 0; k < 3; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP3 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(3, BasisFunctionCategory_Legendre),
                     BasisFunctionLineBase::LegendreP3 );

  const BasisFunctionLine<Legendre,3>* basis = BasisFunctionLine<Legendre,3>::self();

  BOOST_CHECK_EQUAL( 3, basis->order() );
  BOOST_CHECK_EQUAL( 4, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 4, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s;
  Real phi[4], phis[4], phiss[4];

  {
  s = 0;
  Real phiTrue[4]   = {1, -sqrt(3), sqrt(5), -sqrt(7)};
  Real phisTrue[4]  = {0, 2*sqrt(3), -6*sqrt(5), 12*sqrt(7)};
  Real phissTrue[4] = {0, 0, 12*sqrt(5), -60*sqrt(7)};

  basis->evalBasis( s, phi, 4 );
  basis->evalBasisDerivative( s, phis, 4 );
  basis->evalBasisHessianDerivative( s, phiss, 4 );
  for (int k = 0; k < 4; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 1;
  Real phiTrue[4]   = {1, sqrt(3), sqrt(5), sqrt(7)};
  Real phisTrue[4]  = {0, 2*sqrt(3), 6*sqrt(5), 12*sqrt(7)};
  Real phissTrue[4] = {0, 0, 12*sqrt(5), 60*sqrt(7)};

  basis->evalBasis( s, phi, 4 );
  basis->evalBasisDerivative( s, phis, 4 );
  basis->evalBasisHessianDerivative( s, phiss, 4 );
  for (int k = 0; k < 4; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.5;
  Real phiTrue[4]   = {1, 0, -0.5*sqrt(5), 0};
  Real phisTrue[4]  = {0, 2*sqrt(3), 0, -3*sqrt(7)};
  Real phissTrue[4] = {0, 0, 12*sqrt(5), 0};

  basis->evalBasis( s, phi, 4 );
  basis->evalBasisDerivative( s, phis, 4 );
  basis->evalBasisHessianDerivative( s, phiss, 4 );
  for (int k = 0; k < 4; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP4 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(4, BasisFunctionCategory_Legendre),
                     BasisFunctionLineBase::LegendreP4 );

  const BasisFunctionLine<Legendre,4>* basis = BasisFunctionLine<Legendre,4>::self();

  BOOST_CHECK_EQUAL( 4, basis->order() );
  BOOST_CHECK_EQUAL( 5, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 5, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s;
  Real phi[5], phis[5], phiss[5];

  {
  s = 0;
  Real phiTrue[5]   = {1, -sqrt(3), sqrt(5), -sqrt(7), 3};
  Real phisTrue[5]  = {0, 2*sqrt(3), -6*sqrt(5), 12*sqrt(7), -60};
  Real phissTrue[5] = {0, 0, 12*sqrt(5), -60*sqrt(7), 540};

  basis->evalBasis( s, phi, 5 );
  basis->evalBasisDerivative( s, phis, 5 );
  basis->evalBasisHessianDerivative( s, phiss, 5 );
  for (int k = 0; k < 5; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 1;
  Real phiTrue[5]   = {1, sqrt(3), sqrt(5), sqrt(7), 3};
  Real phisTrue[5]  = {0, 2*sqrt(3), 6*sqrt(5), 12*sqrt(7), 60};
  Real phissTrue[5] = {0, 0, 12*sqrt(5), 60*sqrt(7), 540};

  basis->evalBasis( s, phi, 5 );
  basis->evalBasisDerivative( s, phis, 5 );
  basis->evalBasisHessianDerivative( s, phiss, 5 );
  for (int k = 0; k < 5; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.5;
  Real phiTrue[5]   = {1, 0, -0.5*sqrt(5), 0, 9/8.};
  Real phisTrue[5]  = {0, 2*sqrt(3), 0, -3*sqrt(7), 0};
  Real phissTrue[5] = {0, 0, 12*sqrt(5), 0, -90};

  basis->evalBasis( s, phi, 5 );
  basis->evalBasisDerivative( s, phis, 5 );
  basis->evalBasisHessianDerivative( s, phiss, 5 );
  for (int k = 0; k < 5; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP5 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(5, BasisFunctionCategory_Legendre),
                     BasisFunctionLineBase::LegendreP5 );

  const BasisFunctionLine<Legendre,5>* basis = BasisFunctionLineBase::LegendreP5;

  BOOST_CHECK_EQUAL( 5, basis->order() );
  BOOST_CHECK_EQUAL( 6, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 6, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s;
  Real phi[6], phis[6], phiss[6];

  {
  s = 0;
  Real phiTrue[6]   = {1,  -sqrt(3),    sqrt(5),   -sqrt(7),   3,   -sqrt(11)};
  Real phisTrue[6]  = {0, 2*sqrt(3), -6*sqrt(5), 12*sqrt(7), -60, 30*sqrt(11)};
  Real phissTrue[6] = {0, 0, 12*sqrt(5), -60*sqrt(7), 540, -420*sqrt(11) };

  basis->evalBasis( s, phi, 6 );
  basis->evalBasisDerivative( s, phis, 6 );
  basis->evalBasisHessianDerivative( s, phiss, 6 );
  for (int k = 0; k < 6; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 1;
  Real phiTrue[6]   = {1,   sqrt(3),   sqrt(5),    sqrt(7),  3,    sqrt(11)};
  Real phisTrue[6]  = {0, 2*sqrt(3), 6*sqrt(5), 12*sqrt(7), 60, 30*sqrt(11)};
  Real phissTrue[6] = {0, 0, 12*sqrt(5), 60*sqrt(7), 540, 420*sqrt(11)};

  basis->evalBasis( s, phi, 6 );
  basis->evalBasisDerivative( s, phis, 6 );
  basis->evalBasisHessianDerivative( s, phiss, 6 );
  for (int k = 0; k < 6; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.5;
  Real phiTrue[6]   = {1,         0, -0.5*sqrt(5),          0, 9/8.,              0};
  Real phisTrue[6]  = {0, 2*sqrt(3),            0, -3*sqrt(7),     0, 15*sqrt(11)/4.};
  Real phissTrue[6] = {0, 0, 12*sqrt(5), 0, -90, 0};

  basis->evalBasis( s, phi, 6 );
  basis->evalBasisDerivative( s, phis, 6 );
  basis->evalBasisHessianDerivative( s, phiss, 6 );
  for (int k = 0; k < 6; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP6 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const int nB = 7;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(6, BasisFunctionCategory_Legendre),
                     BasisFunctionLineBase::LegendreP6 );

  const BasisFunctionLine<Legendre,6>* basis = BasisFunctionLineBase::LegendreP6;

  BOOST_CHECK_EQUAL(  6, basis->order() );
  BOOST_CHECK_EQUAL( nB, basis->nBasis() );
  BOOST_CHECK_EQUAL(  0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nB, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s;
  Real phi[nB], phis[nB], phiss[nB];

  {
  s = 0;
  Real phiTrue[nB]   = {1,  -sqrt(3),    sqrt(5),   -sqrt(7),   3,   -sqrt(11),     sqrt(13)};
  Real phisTrue[nB]  = {0, 2*sqrt(3), -6*sqrt(5), 12*sqrt(7), -60, 30*sqrt(11), -42*sqrt(13)};
  Real phissTrue[nB] = {0, 0, 12*sqrt(5), -60*sqrt(7), 540, -420*sqrt(11),  840*sqrt(13)};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 1;
  Real phiTrue[nB]   = {1,   sqrt(3),   sqrt(5),    sqrt(7),  3,    sqrt(11),    sqrt(13)};
  Real phisTrue[nB]  = {0, 2*sqrt(3), 6*sqrt(5), 12*sqrt(7), 60, 30*sqrt(11), 42*sqrt(13)};
  Real phissTrue[nB] = {0, 0, 12*sqrt(5), 60*sqrt(7), 540, 420*sqrt(11), 840*sqrt(13)};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.5;
  Real phiTrue[nB]   = {1,         0, -0.5*sqrt(5),          0, 9/8.,              0, -5*sqrt(13)/16.};
  Real phisTrue[nB]  = {0, 2*sqrt(3),            0, -3*sqrt(7),     0, 15*sqrt(11)/4.,               0};
  Real phissTrue[nB] = {0, 0, 12*sqrt(5), 0, -90, 0, 105*sqrt(13)/2.};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP7 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const int nB = 8;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(7, BasisFunctionCategory_Legendre),
                     BasisFunctionLineBase::LegendreP7 );

  const BasisFunctionLine<Legendre,7>* basis = BasisFunctionLineBase::LegendreP7;

  BOOST_CHECK_EQUAL(  7, basis->order() );
  BOOST_CHECK_EQUAL( nB, basis->nBasis() );
  BOOST_CHECK_EQUAL(  0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nB, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s;
  Real phi[nB], phis[nB], phiss[nB];

  {
  s = 0;
  Real phiTrue[nB]   = {1,  -sqrt(3),    sqrt(5),   -sqrt(7),   3,   -sqrt(11),     sqrt(13),   -sqrt(15)};
  Real phisTrue[nB]  = {0, 2*sqrt(3), -6*sqrt(5), 12*sqrt(7), -60, 30*sqrt(11), -42*sqrt(13), 56*sqrt(15)};
  Real phissTrue[nB] = {0, 0, 12*sqrt(5), -60*sqrt(7), 540, -420*sqrt(11),  840*sqrt(13), -1512*sqrt(15)};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 1;
  Real phiTrue[nB]   = {1,   sqrt(3),   sqrt(5),    sqrt(7),  3,    sqrt(11),    sqrt(13),    sqrt(15)};
  Real phisTrue[nB]  = {0, 2*sqrt(3), 6*sqrt(5), 12*sqrt(7), 60, 30*sqrt(11), 42*sqrt(13), 56*sqrt(15)};
  Real phissTrue[nB] = {0, 0, 12*sqrt(5), 60*sqrt(7), 540, 420*sqrt(11), 840*sqrt(13), 1512*sqrt(15)};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

  {
  s = 0.5;
  Real phiTrue[nB]   = {1,         0, -0.5*sqrt(5),          0, 9/8.,              0, -5*sqrt(13)/16.,               0};
  Real phisTrue[nB]  = {0, 2*sqrt(3),            0, -3*sqrt(7),     0, 15*sqrt(11)/4.,               0, -35*sqrt(15)/8.};
  Real phissTrue[nB] = {0, 0, 12*sqrt(5), 0, -90, 0, 105*sqrt(13)/2., 0};

  basis->evalBasis( s, phi, nB );
  basis->evalBasisDerivative( s, phis, nB );
  basis->evalBasisHessianDerivative( s, phiss, nB );
  for (int k = 0; k < nB; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP0 )
{
  const Real tol = 1e-13;

  int order;
  int nquad;
  Real s;
  Real phi[1];
  Real w, sum;

  const BasisFunctionLine<Legendre,0>* basis = BasisFunctionLine<Legendre,0>::self();

  order = 0;
  QuadratureLine quad(order);
  BOOST_CHECK( quad.order() >= 0 );

  nquad = quad.nQuadrature();

  sum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinate(n, s);
    quad.weight(n, w);

    basis->evalBasis( s, phi, 1 );

    sum += w*phi[0]*phi[0];
  }

  BOOST_CHECK_CLOSE( 1, sum, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP1 )
{
  const Real tol = 1e-13;

  int order;
  int nquad;
  Real s;
  Real phi[2];
  Real w, sum[3];

  const BasisFunctionLine<Legendre,1>* basis = BasisFunctionLine<Legendre,1>::self();

  order = 2;
  QuadratureLine quad(order);
  BOOST_CHECK( quad.order() >= 2 );

  nquad = quad.nQuadrature();

  for (int k = 0; k < 3; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinate(n, s);
    quad.weight(n, w);

    basis->evalBasis( s, phi, 2 );

    sum[0] += w*phi[0]*phi[0];
    sum[1] += w*phi[1]*phi[1];
    sum[2] += w*phi[0]*phi[1];
  }

  BOOST_CHECK_CLOSE( sum[0], 1, tol );
  BOOST_CHECK_CLOSE( sum[1], 1, tol );
  BOOST_CHECK_SMALL( sum[2], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP2 )
{
  const Real tol = 1e-13;

  int order;
  int nquad;
  Real s;
  Real phi[3];
  Real w, sum[6];

  const BasisFunctionLine<Legendre,2>* basis = BasisFunctionLine<Legendre,2>::self();

  order = 4;
  QuadratureLine quad(order);
  BOOST_CHECK( quad.order() >= 4 );

  nquad = quad.nQuadrature();

  for (int k = 0; k < 6; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinate(n, s);
    quad.weight(n, w);

    basis->evalBasis( s, phi, 3 );

    sum[0] += w*phi[0]*phi[0];
    sum[1] += w*phi[1]*phi[1];
    sum[2] += w*phi[2]*phi[2];
    sum[3] += w*phi[0]*phi[1];
    sum[4] += w*phi[0]*phi[2];
    sum[5] += w*phi[1]*phi[2];
  }

  BOOST_CHECK_CLOSE( sum[0], 1, tol );
  BOOST_CHECK_CLOSE( sum[1], 1, tol );
  BOOST_CHECK_CLOSE( sum[2], 1, tol );
  BOOST_CHECK_SMALL( sum[3], tol );
  BOOST_CHECK_SMALL( sum[4], tol );
  BOOST_CHECK_SMALL( sum[5], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP3 )
{
  const Real tol = 1e-13;

  int order;
  int nquad;
  Real s;
  Real phi[4];
  Real w, sum[10];

  const BasisFunctionLine<Legendre,3>* basis = BasisFunctionLine<Legendre,3>::self();

  order = 6;
  QuadratureLine quad(order);
  BOOST_CHECK( quad.order() >= 6 );

  nquad = quad.nQuadrature();

  for (int k = 0; k < 10; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinate(n, s);
    quad.weight(n, w);

    basis->evalBasis( s, phi, 4 );

    sum[0] += w*phi[0]*phi[0];
    sum[1] += w*phi[1]*phi[1];
    sum[2] += w*phi[2]*phi[2];
    sum[3] += w*phi[3]*phi[3];

    sum[4] += w*phi[0]*phi[1];
    sum[5] += w*phi[0]*phi[2];
    sum[6] += w*phi[0]*phi[3];
    sum[7] += w*phi[1]*phi[2];
    sum[8] += w*phi[1]*phi[3];
    sum[9] += w*phi[2]*phi[3];
  }

  BOOST_CHECK_CLOSE( sum[0], 1, tol );
  BOOST_CHECK_CLOSE( sum[1], 1, tol );
  BOOST_CHECK_CLOSE( sum[2], 1, tol );
  BOOST_CHECK_CLOSE( sum[3], 1, tol );
  BOOST_CHECK_SMALL( sum[4], tol );
  BOOST_CHECK_SMALL( sum[5], tol );
  BOOST_CHECK_SMALL( sum[6], tol );
  BOOST_CHECK_SMALL( sum[7], tol );
  BOOST_CHECK_SMALL( sum[8], tol );
  BOOST_CHECK_SMALL( sum[9], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP4 )
{
  const Real tol = 1e-13;

  int order;
  int nquad;
  Real s;
  Real phi[5];
  Real w, sum[15];

  const BasisFunctionLine<Legendre,4>* basis = BasisFunctionLine<Legendre,4>::self();

  order = 8;
  QuadratureLine quad(order);
  BOOST_CHECK( quad.order() >= 8 );

  nquad = quad.nQuadrature();

  for (int k = 0; k < 15; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinate(n, s);
    quad.weight(n, w);

    basis->evalBasis( s, phi, 5 );

    sum[0] += w*phi[0]*phi[0];
    sum[1] += w*phi[1]*phi[1];
    sum[2] += w*phi[2]*phi[2];
    sum[3] += w*phi[3]*phi[3];
    sum[4] += w*phi[4]*phi[4];

    sum[ 5] += w*phi[0]*phi[1];
    sum[ 6] += w*phi[0]*phi[2];
    sum[ 7] += w*phi[0]*phi[3];
    sum[ 8] += w*phi[0]*phi[4];
    sum[ 9] += w*phi[1]*phi[2];
    sum[10] += w*phi[1]*phi[3];
    sum[11] += w*phi[1]*phi[4];
    sum[12] += w*phi[2]*phi[3];
    sum[13] += w*phi[2]*phi[4];
    sum[14] += w*phi[3]*phi[4];
  }

  BOOST_CHECK_CLOSE( sum[0], 1, tol );
  BOOST_CHECK_CLOSE( sum[1], 1, tol );
  BOOST_CHECK_CLOSE( sum[2], 1, tol );
  BOOST_CHECK_CLOSE( sum[3], 1, tol );
  BOOST_CHECK_CLOSE( sum[4], 1, tol );

  BOOST_CHECK_SMALL( sum[ 5], tol );
  BOOST_CHECK_SMALL( sum[ 6], tol );
  BOOST_CHECK_SMALL( sum[ 7], tol );
  BOOST_CHECK_SMALL( sum[ 8], tol );
  BOOST_CHECK_SMALL( sum[ 9], tol );
  BOOST_CHECK_SMALL( sum[10], tol );
  BOOST_CHECK_SMALL( sum[11], tol );
  BOOST_CHECK_SMALL( sum[12], tol );
  BOOST_CHECK_SMALL( sum[13], tol );
  BOOST_CHECK_SMALL( sum[14], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP5 )
{
  const Real tol = 1e-13;

  const int nB = 6;

  int order;
  int nquad;
  int offset;
  Real s;
  Real phi[nB];
  Real w, sum[nB*(nB + 1)/2];

  const BasisFunctionLine<Legendre,5>* basis = BasisFunctionLineBase::LegendreP5;

  order = 10;
  QuadratureLine quad(order);
  BOOST_CHECK( quad.order() >= 10 );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nB*(nB + 1)/2; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinate(n, s);
    quad.weight(n, w);

    basis->evalBasis( s, phi, nB );

    for (int k = 0; k < nB; k++)
      sum[k] += w*phi[k]*phi[k];
    offset = nB;

    for (int i = 0; i < nB-1; i++)
    {
      for (int k = i+1; k < nB; k++)
        sum[offset + k - (i+1)] += w*phi[i]*phi[k];
      offset += nB - (i+1);
    }
  }

  for (int k = 0; k < nB; k++)
    BOOST_CHECK_CLOSE( sum[k], 1, tol );

  for (int k = nB; k < nB*(nB + 1)/2; k++)
    BOOST_CHECK_SMALL( sum[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP6 )
{
  const Real tol = 2e-13;

  const int nB = 7;

  int order;
  int nquad;
  int offset;
  Real s;
  Real phi[nB];
  Real w, sum[nB*(nB + 1)/2];

  const BasisFunctionLine<Legendre,6>* basis = BasisFunctionLineBase::LegendreP6;

  order = 12;
  QuadratureLine quad(order);
  BOOST_CHECK( quad.order() >= 12 );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nB*(nB + 1)/2; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinate(n, s);
    quad.weight(n, w);

    basis->evalBasis( s, phi, nB );

    for (int k = 0; k < nB; k++)
      sum[k] += w*phi[k]*phi[k];
    offset = nB;

    for (int i = 0; i < nB-1; i++)
    {
      for (int k = i+1; k < nB; k++)
        sum[offset + k - (i+1)] += w*phi[i]*phi[k];
      offset += nB - (i+1);
    }
  }

  for (int k = 0; k < nB; k++)
    BOOST_CHECK_CLOSE( sum[k], 1, tol );

  for (int k = nB; k < nB*(nB + 1)/2; k++)
    BOOST_CHECK_SMALL( sum[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP7 )
{
  const Real tol = 2e-13;

  const int nB = 8;

  int order;
  int nquad;
  int offset;
  Real s;
  Real phi[nB];
  Real w, sum[nB*(nB + 1)/2];

  const BasisFunctionLine<Legendre,7>* basis = BasisFunctionLineBase::LegendreP7;

  order = 14;
  QuadratureLine quad(order);
  BOOST_CHECK( quad.order() >= 14 );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nB*(nB + 1)/2; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinate(n, s);
    quad.weight(n, w);

    basis->evalBasis( s, phi, nB );

    for (int k = 0; k < nB; k++)
      sum[k] += w*phi[k]*phi[k];
    offset = nB;

    for (int i = 0; i < nB-1; i++)
    {
      for (int k = i+1; k < nB; k++)
        sum[offset + k - (i+1)] += w*phi[i]*phi[k];
      offset += nB - (i+1);
    }
  }

  for (int k = 0; k < nB; k++)
    BOOST_CHECK_CLOSE( sum[k], 1, tol );

  for (int k = nB; k < nB*(nB + 1)/2; k++)
    BOOST_CHECK_SMALL( sum[k], tol );
}




//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP1 )
{
  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(1, BasisFunctionCategory_Lagrange),
                     BasisFunctionLineBase::LagrangeP1 );

  const BasisFunctionLine<Lagrange,1>* basis = BasisFunctionLine<Lagrange,1>::self();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 2, basis->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s;
  Real phi[2], phis[2], phiss[2];
  Real phiTrue[2], phisTrue[2], phissTrue[2];
  int k;

  for (int test = 0; test < 1; test++)
  {
    switch (test)
    {
    case 0:
      s = .25;
      lagrange_p1_test1_true(phiTrue,phisTrue,phissTrue);
      break;
    }

    basis->evalBasis( s, phi, 2 );
    basis->evalBasisDerivative( s, phis, 2 );
    basis->evalBasisHessianDerivative( s, phiss, 2 );
    for (k = 0; k < 2; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP2 )
{
  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(2, BasisFunctionCategory_Lagrange),
                     BasisFunctionLineBase::LagrangeP2 );

  const BasisFunctionLine<Lagrange,2>* basis = BasisFunctionLine<Lagrange,2>::self();

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( 3, basis->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 1, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s;
  Real phi[3], phis[3], phiss[3];
  Real phiTrue[3], phisTrue[3], phissTrue[3];
  int k;

  for (int test = 0; test < 1; test++)
  {
    switch (test)
    {
    case 0:
      s = .25;
      lagrange_p2_test1_true(phiTrue,phisTrue,phissTrue);
      break;
    }

    basis->evalBasis( s, phi, 3 );
    basis->evalBasisDerivative( s, phis, 3 );
    basis->evalBasisHessianDerivative( s, phiss, 3 );
    for (k = 0; k < 3; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phissTrue[k], phiss[k], tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP3 )
{
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(3, BasisFunctionCategory_Lagrange),
                     BasisFunctionLineBase::LagrangeP3 );

  const BasisFunctionLine<Lagrange,3>* basis = BasisFunctionLine<Lagrange,3>::self();

  BOOST_CHECK_EQUAL( 3, basis->order() );
  BOOST_CHECK_EQUAL( 4, basis->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s;
  Real phi[4], phis[4], phiss[4];

  {
  Real phiTrue[4];
  Real phisTrue[4];
  Real phissTrue[4];

  s = .25;
  lagrange_p3_test1_true(phiTrue,phisTrue,phissTrue);

  basis->evalBasis( s, phi, 4 );
  basis->evalBasisDerivative( s, phis, 4 );
  basis->evalBasisHessianDerivative( s, phiss, 4 );
  for (int k = 0; k < 4; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP4 )
{
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(4, BasisFunctionCategory_Lagrange),
                     BasisFunctionLineBase::LagrangeP4 );

  const BasisFunctionLine<Lagrange,4>* basis = BasisFunctionLine<Lagrange,4>::self();

  BOOST_CHECK_EQUAL( 4, basis->order() );
  BOOST_CHECK_EQUAL( 5, basis->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 3, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s;
  Real phi[5], phis[5], phiss[5];

  {
  Real phiTrue[5];
  Real phisTrue[5];
  Real phissTrue[5];

  s = .25;
  lagrange_p4_test1_true(phiTrue,phisTrue,phissTrue);

  basis->evalBasis( s, phi, 5 );
  basis->evalBasisDerivative( s, phis, 5 );
  basis->evalBasisHessianDerivative( s, phiss, 5 );
  for (int k = 0; k < 5; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }


}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP5 )
{
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( BasisFunctionLineBase::getBasisFunction(5, BasisFunctionCategory_Lagrange),
                     BasisFunctionLineBase::LagrangeP5 );

  const BasisFunctionLine<Lagrange,5>* basis = BasisFunctionLineBase::LagrangeP5;

  BOOST_CHECK_EQUAL( 5, basis->order() );
  BOOST_CHECK_EQUAL( 6, basis->nBasis() );
  BOOST_CHECK_EQUAL( 2, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 4, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s;
  Real phi[6], phis[6], phiss[6];

  {
  Real phiTrue[6];
  Real phisTrue[6];
  Real phissTrue[6];

  s = .25;
  lagrange_p5_test1_true(phiTrue,phisTrue,phissTrue);

  basis->evalBasis( s, phi, 6 );
  basis->evalBasisDerivative( s, phis, 6 );
  basis->evalBasisHessianDerivative( s, phiss, 6 );
  for (int k = 0; k < 6; k++)
  {
    SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
  }
  }

}

//----------------------------------------------------------------------------//
typedef boost::mpl::range_c<int,1,BasisFunctionLine_LagrangePMax+1> orders;

BOOST_AUTO_TEST_CASE_TEMPLATE( nodes_Lagrange, Order, orders )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  const BasisFunctionLine<Lagrange,Order::value>* basis = BasisFunctionLine<Lagrange,Order::value>::self();
  const int N = basis->nBasis();

  std::vector<Real> sVec;
  basis->coordinates(sVec);

  for (int i = 0; i < N; i++)
  {
    Real sTrue;
    if (i == 0)
      sTrue = 0.0; //left node basis
    else if (i == 1)
      sTrue = 1.0; //right node basis
    else
      sTrue = ((Real) (i-1)/(Real) (N-1)); //edge basis

    SANS_CHECK_CLOSE( sTrue, sVec[i], small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionLine_pattern.txt", true );

  BasisFunctionLineBase::getBasisFunction(1, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(2, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(3, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(4, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(5, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(6, BasisFunctionCategory_Hierarchical)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(7, BasisFunctionCategory_Hierarchical)->dump( 2, output );

  BasisFunctionLineBase::getBasisFunction(0, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(1, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(2, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(3, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(4, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(5, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(6, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionLineBase::getBasisFunction(7, BasisFunctionCategory_Legendre)->dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
