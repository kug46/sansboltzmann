// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"   // Triangle
#include "BasisFunction/TraceToCellRefCoord.h"


using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( TraceToCellRefCoord_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hexahedron_eval_test )
{

  const Real tol = 1e-13;
  Real sRefArea, tRefArea, sRefVol, tRefVol, uRefVol, sRefVolTrue, tRefVolTrue, uRefVolTrue;
  CanonicalTraceToCell canonicalFace(0,0);

  // A macro for looping over varying s with constant t on a face gives the correct volume coordinate
#define CHECK_S( sRef, tRef, uRef ) \
  for ( Real s = 0.0; s <= 1.0; s += 0.25 ) \
  { \
    sRefArea = s; \
    tRefArea = 0; \
    sRefVolTrue = (sRef); tRefVolTrue = (tRef); uRefVolTrue = (uRef); \
 \
    TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ); \
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol ); \
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol ); \
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol ); \
  }

  // A macro for looping over varying t with constant s on a face gives the correct volume coordinate
#define CHECK_T( sRef, tRef, uRef ) \
  for ( Real t = 0.0; t <= 1.0; t += 0.25 ) \
  { \
    sRefArea = 0; \
    tRefArea = t; \
    sRefVolTrue = (sRef); tRefVolTrue = (tRef); uRefVolTrue = (uRef); \
 \
    TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ); \
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol ); \
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol ); \
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol ); \
  }

  // Checks the center coordinate on a face gives the correct volume coordinate
#define CHECK_C( sRef, tRef, uRef ) \
  sRefArea = 1./2.; \
  tRefArea = 1./2.; \
  sRefVolTrue = (sRef); tRefVolTrue = (tRef); uRefVolTrue = (uRef); \
 \
  TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ); \
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol ); \
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol ); \
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );


  //===================================
  canonicalFace.orientation = 1;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  CHECK_S( 0, s, 0 )
  CHECK_T( t, 0, 0 )
  CHECK_C( 1./2., 1./2., 0 )

  //------------
  canonicalFace.trace = 1;
  //------------

  CHECK_S( s, 0, 0 )
  CHECK_T( 0, 0, t )
  CHECK_C( 1./2., 0, 1./2. )

  //------------
  canonicalFace.trace = 2;
  //------------

  CHECK_S( 1, s, 0 )
  CHECK_T( 1, 0, t )
  CHECK_C( 1, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 3;
  //------------

  CHECK_S( 0, 1, s )
  CHECK_T( t, 1, 0 )
  CHECK_C( 1./2., 1, 1./2. )

  //------------
  canonicalFace.trace = 4;
  //------------

  CHECK_S( 0, 0, s )
  CHECK_T( 0, t, 0 )
  CHECK_C( 0, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 5;
  //------------

  CHECK_S( s, 0, 1 )
  CHECK_T( 0, t, 1 )
  CHECK_C( 1./2., 1./2., 1 )



  //===================================
  canonicalFace.orientation = 2;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  CHECK_S( s, 1, 0 )
  CHECK_T( 0, 1-t, 0 )
  CHECK_C( 1./2., 1./2., 0 )

  //------------
  canonicalFace.trace = 1;
  //------------

  CHECK_S( 1, 0, s )
  CHECK_T( 1-t, 0, 0 )
  CHECK_C( 1./2., 0, 1./2. )

  //------------
  canonicalFace.trace = 2;
  //------------

  CHECK_S( 1, 1, s )
  CHECK_T( 1, 1-t, 0 )
  CHECK_C( 1, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 3;
  //------------

  CHECK_S( s, 1, 1 )
  CHECK_T( 0, 1, 1-t )
  CHECK_C( 1./2., 1, 1./2. )

  //------------
  canonicalFace.trace = 4;
  //------------

  CHECK_S( 0, s, 1 )
  CHECK_T( 0, 0, 1-t )
  CHECK_C( 0, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 5;
  //------------

  CHECK_S( 1, s, 1 )
  CHECK_T( 1-t, 0, 1 )
  CHECK_C( 1./2., 1./2., 1 )



  //===================================
  canonicalFace.orientation = 3;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  CHECK_S( 1, 1-s, 0 )
  CHECK_T( 1-t, 1, 0 )
  CHECK_C( 1./2., 1./2., 0 )

  //------------
  canonicalFace.trace = 1;
  //------------

  CHECK_S( 1-s, 0, 1 )
  CHECK_T( 1, 0, 1-t )
  CHECK_C( 1./2., 0, 1./2. )

  //------------
  canonicalFace.trace = 2;
  //------------

  CHECK_S( 1, 1-s, 1 )
  CHECK_T( 1, 1, 1-t )
  CHECK_C( 1, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 3;
  //------------

  CHECK_S( 1, 1, 1-s )
  CHECK_T( 1-t, 1, 1 )
  CHECK_C( 1./2., 1, 1./2. )

  //------------
  canonicalFace.trace = 4;
  //------------

  CHECK_S( 0, 1, 1-s )
  CHECK_T( 0, 1-t, 1 )
  CHECK_C( 0, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 5;
  //------------

  CHECK_S( 1-s, 1, 1 )
  CHECK_T( 1, 1-t, 1 )
  CHECK_C( 1./2., 1./2., 1 )



  //===================================
  canonicalFace.orientation = 4;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  CHECK_S( 1-s, 0, 0 )
  CHECK_T( 1, t, 0 )
  CHECK_C( 1./2., 1./2., 0 )

  //------------
  canonicalFace.trace = 1;
  //------------

  CHECK_S( 0, 0, 1-s )
  CHECK_T( t, 0, 1 )
  CHECK_C( 1./2., 0, 1./2. )

  //------------
  canonicalFace.trace = 2;
  //------------

  CHECK_S( 1, 0, 1-s )
  CHECK_T( 1, t, 1 )
  CHECK_C( 1, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 3;
  //------------

  CHECK_S( 1-s, 1, 0 )
  CHECK_T( 1, 1, t )
  CHECK_C( 1./2., 1, 1./2. )

  //------------
  canonicalFace.trace = 4;
  //------------

  CHECK_S( 0, 1-s, 0 )
  CHECK_T( 0, 1, t )
  CHECK_C( 0, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 5;
  //------------

  CHECK_S( 0, 1-s, 1 )
  CHECK_T( t, 1, 1 )
  CHECK_C( 1./2., 1./2., 1 )



  //===================================
  canonicalFace.orientation = -1;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  CHECK_S( s, 0, 0 )
  CHECK_T( 0, t, 0 )
  CHECK_C( 1./2., 1./2., 0 )

  //------------
  canonicalFace.trace = 1;
  //------------

  CHECK_S( 0, 0, s )
  CHECK_T( t, 0, 0 )
  CHECK_C( 1./2., 0, 1./2. )

  //------------
  canonicalFace.trace = 2;
  //------------

  CHECK_S( 1, 0, s )
  CHECK_T( 1, t, 0 )
  CHECK_C( 1, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 3;
  //------------

  CHECK_S( s, 1, 0 )
  CHECK_T( 0, 1, t )
  CHECK_C( 1./2., 1, 1./2. )

  //------------
  canonicalFace.trace = 4;
  //------------

  CHECK_S( 0, s, 0 )
  CHECK_T( 0, 0, t )
  CHECK_C( 0, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 5;
  //------------

  CHECK_S( 0, s, 1 )
  CHECK_T( t, 0, 1 )
  CHECK_C( 1./2., 1./2., 1 )



  //===================================
  canonicalFace.orientation = -2;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  CHECK_S( 0, 1-s, 0 )
  CHECK_T( t, 1, 0 )
  CHECK_C( 1./2., 1./2., 0 )

  //------------
  canonicalFace.trace = 1;
  //------------

  CHECK_S( 1-s, 0, 0 )
  CHECK_T( 1, 0, t )
  CHECK_C( 1./2., 0, 1./2. )

  //------------
  canonicalFace.trace = 2;
  //------------

  CHECK_S( 1, 1-s, 0 )
  CHECK_T( 1, 1, t )
  CHECK_C( 1, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 3;
  //------------

  CHECK_S( 0, 1, 1-s )
  CHECK_T( t, 1, 1 )
  CHECK_C( 1./2., 1, 1./2. )

  //------------
  canonicalFace.trace = 4;
  //------------

  CHECK_S( 0, 0, 1-s )
  CHECK_T( 0, t, 1 )
  CHECK_C( 0, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 5;
  //------------

  CHECK_S( 1-s, 0, 1 )
  CHECK_T( 1, t, 1 )
  CHECK_C( 1./2., 1./2., 1 )



  //===================================
  canonicalFace.orientation = -3;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  CHECK_S( 1-s, 1, 0 )
  CHECK_T( 1, 1-t, 0 )
  CHECK_C( 1./2., 1./2., 0 )

  //------------
  canonicalFace.trace = 1;
  //------------

  CHECK_S( 1, 0, 1-s )
  CHECK_T( 1-t, 0, 1 )
  CHECK_C( 1./2., 0, 1./2. )

  //------------
  canonicalFace.trace = 2;
  //------------

  CHECK_S( 1, 1, 1-s )
  CHECK_T( 1, 1-t, 1 )
  CHECK_C( 1, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 3;
  //------------

  CHECK_S( 1-s, 1, 1 )
  CHECK_T( 1, 1, 1-t )
  CHECK_C( 1./2., 1, 1./2. )

  //------------
  canonicalFace.trace = 4;
  //------------

  CHECK_S( 0, 1-s, 1 )
  CHECK_T( 0, 1, 1-t )
  CHECK_C( 0, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 5;
  //------------

  CHECK_S( 1, 1-s, 1 )
  CHECK_T( 1-t, 1, 1 )
  CHECK_C( 1./2., 1./2., 1 )



  //===================================
  canonicalFace.orientation = -4;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  CHECK_S( 1, s, 0 )
  CHECK_T( 1-t, 0, 0 )
  CHECK_C( 1./2., 1./2., 0 )

  //------------
  canonicalFace.trace = 1;
  //------------

  CHECK_S( s, 0, 1 )
  CHECK_T( 0, 0, 1-t )
  CHECK_C( 1./2., 0, 1./2. )

  //------------
  canonicalFace.trace = 2;
  //------------

  CHECK_S( 1, s, 1 )
  CHECK_T( 1, 0, 1-t )
  CHECK_C( 1, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 3;
  //------------

  CHECK_S( 1, 1, s )
  CHECK_T( 1-t, 1, 0 )
  CHECK_C( 1./2., 1, 1./2. )

  //------------
  canonicalFace.trace = 4;
  //------------

  CHECK_S( 0, 1, s )
  CHECK_T( 0, 1-t, 0 )
  CHECK_C( 0, 1./2., 1./2. )

  //------------
  canonicalFace.trace = 5;
  //------------

  CHECK_S( s, 1, 1 )
  CHECK_T( 0, 1-t, 1 )
  CHECK_C( 1./2., 1./2., 1 )



  typedef TraceToCellRefCoord<Quad, TopoD3, Hex> TraceToCellRefCoordQuadHex;

  canonicalFace.orientation = 1;
  canonicalFace.trace = 6;
  BOOST_CHECK_THROW( TraceToCellRefCoordQuadHex::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );

  canonicalFace.orientation = 0;
  canonicalFace.trace = 0;
  BOOST_CHECK_THROW( TraceToCellRefCoordQuadHex::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );


  canonicalFace.trace = -1;

  canonicalFace.orientation = 1;
  BOOST_CHECK_THROW( TraceToCellRefCoordQuadHex::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );

  canonicalFace.orientation = 5;
  BOOST_CHECK_THROW( TraceToCellRefCoordQuadHex::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );

  canonicalFace.orientation = -5;
  BOOST_CHECK_THROW( TraceToCellRefCoordQuadHex::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hexahedron_Orientation_test )
{

  const int hex[8] = {0, 1, 2, 3, 4, 5, 6, 7};

  const int (*TraceNodes)[ Quad::NTrace ] = TraceToCellRefCoord<Quad, TopoD3, Hex>::TraceNodes;

  CanonicalTraceToCell canonicalFace;

  for (int face = 0; face < 6; face++ )
  {
    // Orientation is 1 for all quad that match the canonical face
    const int quad[4] = {hex[TraceNodes[face][0]], hex[TraceNodes[face][1]],
                         hex[TraceNodes[face][2]], hex[TraceNodes[face][3]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(quad, 4, hex, 8);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 1 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }

  for (int face = 0; face < 6; face++ )
  {
    // Reverse the last two nodes results in a -1 orientation
    const int quad[4] = {hex[TraceNodes[face][0]], hex[TraceNodes[face][3]],
                         hex[TraceNodes[face][2]], hex[TraceNodes[face][1]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(quad, 4, hex, 8);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, -1 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }



  for (int face = 0; face < 6; face++ )
  {
    // Revolve the canonical nodes once is orientation 2
    const int quad[4] = {hex[TraceNodes[face][1]], hex[TraceNodes[face][2]],
                         hex[TraceNodes[face][3]], hex[TraceNodes[face][0]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(quad, 4, hex, 8);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 2 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }

  for (int face = 0; face < 6; face++ )
  {
    // Reverse the last two nodes results in a -2 orientation
    const int quad[4] = {hex[TraceNodes[face][1]], hex[TraceNodes[face][0]],
                         hex[TraceNodes[face][3]], hex[TraceNodes[face][2]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(quad, 4, hex, 8);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, -2 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }



  for (int face = 0; face < 6; face++ )
  {
    // Revolve the canonical nodes twice is orientation 3
    const int quad[4] = {hex[TraceNodes[face][2]], hex[TraceNodes[face][3]],
                         hex[TraceNodes[face][0]], hex[TraceNodes[face][1]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(quad, 4, hex, 8);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 3 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }

  for (int face = 0; face < 6; face++ )
  {
    // Reverse the last two nodes results in a -3 orientation
    const int quad[4] = {hex[TraceNodes[face][2]], hex[TraceNodes[face][1]],
                         hex[TraceNodes[face][0]], hex[TraceNodes[face][3]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(quad, 4, hex, 8);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, -3 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }



  for (int face = 0; face < 6; face++ )
  {
    // Revolve the canonical nodes twice is orientation 4
    const int quad[4] = {hex[TraceNodes[face][3]], hex[TraceNodes[face][0]],
                         hex[TraceNodes[face][1]], hex[TraceNodes[face][2]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(quad, 4, hex, 8);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 4 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }

  for (int face = 0; face < 6; face++ )
  {
    // Reverse the last two nodes results in a -4 orientation
    const int quad[4] = {hex[TraceNodes[face][3]], hex[TraceNodes[face][2]],
                         hex[TraceNodes[face][1]], hex[TraceNodes[face][0]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(quad, 4, hex, 8);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, -4 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }


  // Test retrieving the canonical face given the nodes on the face.
  // The nodes can be randomly ordered, and the correct canonical ordering is returned

  int canonicalQuad[4];
  for (int face = 0; face < 6; face++ )
  {
    // Test a jumbled node ordering retrieves the canonical node ordering
    const int quad[4] = {hex[TraceNodes[face][0]], hex[TraceNodes[face][1]],
                         hex[TraceNodes[face][3]], hex[TraceNodes[face][2]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft(quad, 4, hex, 8, canonicalQuad, 4);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 1 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );

    BOOST_CHECK_EQUAL( hex[TraceNodes[face][0]], canonicalQuad[0] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][1]], canonicalQuad[1] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][2]], canonicalQuad[2] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][3]], canonicalQuad[3] );
  }

  for (int face = 0; face < 6; face++ )
  {
    // Test a jumbled node ordering retrieves the canonical node ordering
    const int quad[4] = {hex[TraceNodes[face][1]], hex[TraceNodes[face][0]],
                         hex[TraceNodes[face][3]], hex[TraceNodes[face][2]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft(quad, 4, hex, 8, canonicalQuad, 4);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 1 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );

    BOOST_CHECK_EQUAL( hex[TraceNodes[face][0]], canonicalQuad[0] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][1]], canonicalQuad[1] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][2]], canonicalQuad[2] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][3]], canonicalQuad[3] );
  }

  for (int face = 0; face < 6; face++ )
  {
    // Test a jumbled node ordering retrieves the canonical node ordering
    const int quad[4] = {hex[TraceNodes[face][1]], hex[TraceNodes[face][0]],
                         hex[TraceNodes[face][2]], hex[TraceNodes[face][3]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft(quad, 4, hex, 8, canonicalQuad, 4);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 1 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );

    BOOST_CHECK_EQUAL( hex[TraceNodes[face][0]], canonicalQuad[0] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][1]], canonicalQuad[1] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][2]], canonicalQuad[2] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][3]], canonicalQuad[3] );
  }

  for (int face = 0; face < 6; face++ )
  {
    // Test a jumbled node ordering retrieves the canonical node ordering
    const int quad[4] = {hex[TraceNodes[face][3]], hex[TraceNodes[face][2]],
                         hex[TraceNodes[face][1]], hex[TraceNodes[face][0]]};

    canonicalFace = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft(quad, 4, hex, 8, canonicalQuad, 4);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 1 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );

    BOOST_CHECK_EQUAL( hex[TraceNodes[face][0]], canonicalQuad[0] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][1]], canonicalQuad[1] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][2]], canonicalQuad[2] );
    BOOST_CHECK_EQUAL( hex[TraceNodes[face][3]], canonicalQuad[3] );
  }


  // Using nodes that do not make up a face causes an exception
  BOOST_CHECK_THROW( (TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft(
                      {hex[0], hex[1], hex[6], hex[7]}, hex, 8, canonicalQuad, 4)), DeveloperException );

  BOOST_CHECK_THROW( (TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft(
                      {hex[0], hex[1], hex[2], hex[7]}, hex, 8, canonicalQuad, 4)), DeveloperException );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
