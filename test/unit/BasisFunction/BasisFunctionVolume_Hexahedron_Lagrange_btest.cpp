// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionVolume_Hexanhedron_Lagrange_btest
// testing of BasisFunctionVolume<Hex,Lagrange> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionVolume.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange.h"

#include "BasisFunction/BasisFunctionLine_Lagrange.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionVolume_Hexahedron_Lagrange_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Hex::volumeRef, 1., 1e-12 );
  BOOST_CHECK_CLOSE( Hex::centerRef[0], 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Hex::centerRef[1], 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Hex::centerRef[2], 1./2., 1e-12 );

  BOOST_CHECK( Hex::TopoDim::D == topoDim(eHex) );
  BOOST_CHECK( Hex::NNode == topoNNode(eHex) );

  BOOST_CHECK( BasisFunctionVolumeBase<Hex>::D == 3 );

  BOOST_CHECK( (BasisFunctionVolume<Hex,Lagrange,1>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Lagrange,2>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Lagrange,3>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Lagrange,4>::D == 3) );
#if 0
  BOOST_CHECK( (BasisFunctionVolume<Hex,Lagrange,5>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Lagrange,6>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Lagrange,7>::D == 3) );
#endif

  BOOST_CHECK( BasisFunctionVolume_Hex_LagrangePMax == 4 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionVolumeBase<Hex>::getBasisFunction( 0, BasisFunctionCategory_Lagrange ), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionVolumeBase<Hex>::getBasisFunction( BasisFunctionVolume_Hex_LagrangePMax+1,
                                                                     BasisFunctionCategory_Lagrange ), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP1 )
{
  typedef std::array<int,6> Int6;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 1;
  const int nBasis = (order + 1)*(order + 1)*(order + 1);

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Hex>::getBasisFunction(order, BasisFunctionCategory_Lagrange),
                     BasisFunctionVolumeBase<Hex>::LagrangeP1 );

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolumeBase<Hex>::LagrangeP1;

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 8, basis->nBasis() );
  BOOST_CHECK_EQUAL( 8, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisFace() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  const int N = 8;

  Real s, t, u;
  Real phi[N], phis[N], phit[N], phiu[N];
  Real phiss[N], phist[N], phitt[N], phisu[N], phitu[N], phiuu[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N], phiuTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N], phisuTrue[N], phituTrue[N], phiuuTrue[N];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 9; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0; u = 0;
      break;

    case 1:
      s = 1;  t = 0; u = 0;
      break;

    case 2:
      s = 1;  t = 1; u = 0;
      break;

    case 3:
      s = 0;  t = 1; u = 0;
      break;

    case 4:
      s = 0;  t = 0; u = 1;
      break;

    case 5:
      s = 1;  t = 0; u = 1;
      break;

    case 6:
      s = 1;  t = 1; u = 1;
      break;

    case 7:
      s = 0;  t = 1; u = 1;
      break;

    case 8:
      s = 1./2.;  t = 1./2.;  u = 1./2.;
      break;
    }


    Real sphi[2], tphi[2], uphi[2];
    BasisFunctionLine<Lagrange,1>::self()->evalBasis(s, sphi, 2);
    BasisFunctionLine<Lagrange,1>::self()->evalBasis(t, tphi, 2);
    BasisFunctionLine<Lagrange,1>::self()->evalBasis(u, uphi, 2);

    Real sphis[2], tphit[2], uphiu[2];
    BasisFunctionLine<Lagrange,1>::self()->evalBasisDerivative(s, sphis, 2);
    BasisFunctionLine<Lagrange,1>::self()->evalBasisDerivative(t, tphit, 2);
    BasisFunctionLine<Lagrange,1>::self()->evalBasisDerivative(u, uphiu, 2);

    Real sphiss[2], tphitt[2], uphiuu[2];
    BasisFunctionLine<Lagrange,1>::self()->evalBasisHessianDerivative(s, sphiss, 2);
    BasisFunctionLine<Lagrange,1>::self()->evalBasisHessianDerivative(t, tphitt, 2);
    BasisFunctionLine<Lagrange,1>::self()->evalBasisHessianDerivative(u, uphiuu, 2);

    //phi -----------------------//
    phiTrue[0] = sphi[0]*tphi[0]*uphi[0]; // 1 @ node 0 (s = 0, t = 0, u = 0)
    phiTrue[1] = sphi[1]*tphi[0]*uphi[0]; // 1 @ node 1 (s = 1, t = 0, u = 0)
    phiTrue[2] = sphi[1]*tphi[1]*uphi[0]; // 1 @ node 2 (s = 1, t = 1, u = 0)
    phiTrue[3] = sphi[0]*tphi[1]*uphi[0]; // 1 @ node 3 (s = 0, t = 1, u = 0)

    phiTrue[4] = sphi[0]*tphi[0]*uphi[1]; // 1 @ node 4 (s = 0, t = 0, u = 1)
    phiTrue[5] = sphi[1]*tphi[0]*uphi[1]; // 1 @ node 5 (s = 1, t = 0, u = 1)
    phiTrue[6] = sphi[1]*tphi[1]*uphi[1]; // 1 @ node 6 (s = 1, t = 1, u = 1)
    phiTrue[7] = sphi[0]*tphi[1]*uphi[1]; // 1 @ node 7 (s = 0, t = 1, u = 1)

    //phis, phit, phit -----------------------------//
    BasisFunctionVolume<Hex,Lagrange,1>::self()->tensorProduct(sphis, tphi , uphi , phisTrue);
    BasisFunctionVolume<Hex,Lagrange,1>::self()->tensorProduct(sphi , tphit, uphi , phitTrue);
    BasisFunctionVolume<Hex,Lagrange,1>::self()->tensorProduct(sphi , tphi , uphiu, phiuTrue);

    //phiss, phist, phitt, phisu, phitu, phiuu -----//
    BasisFunctionVolume<Hex,Lagrange,1>::self()->tensorProduct(sphiss, tphi  , uphi  , phissTrue);
    BasisFunctionVolume<Hex,Lagrange,1>::self()->tensorProduct(sphis , tphit , uphi  , phistTrue);
    BasisFunctionVolume<Hex,Lagrange,1>::self()->tensorProduct(sphi  , tphitt, uphi  , phittTrue);
    BasisFunctionVolume<Hex,Lagrange,1>::self()->tensorProduct(sphis , tphi  , uphiu , phisuTrue);
    BasisFunctionVolume<Hex,Lagrange,1>::self()->tensorProduct(sphi  , tphit , uphiu , phituTrue);
    BasisFunctionVolume<Hex,Lagrange,1>::self()->tensorProduct(sphi  , tphi  , uphiuu, phiuuTrue);


    basis->evalBasis( s, t, u, facesign, phi, N );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, N );
    basis->evalBasisHessianDerivative( s, t, u, facesign, phiss, phist, phitt, phisu, phitu, phiuu, N );
    for (k = 0; k < N; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
    }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(nBasis), coord_t(nBasis), coord_u(nBasis);
  BasisFunctionVolumeBase<Hex>::LagrangeP1->coordinates( coord_s, coord_t, coord_u );

  for (int n = 0; n < nBasis; n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], coord_u[n], facesign, phi, nBasis );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }


  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*basis->order()+1;
  for (int k = 0; k < npoints; k++)
  {
    for (int j = 0; j < npoints; j++)
    {
      for (int i = 0; i < npoints; i++)
      {
        // Reference coordinates
        Real sTrue = i*1/Real(npoints+1);
        Real tTrue = j*1/Real(npoints+1);
        Real uTrue = k*1/Real(npoints+1);

        // Interpolated reference coordinate
        basis->evalBasis( sTrue, tTrue, uTrue, facesign, phi, basis->nBasis() );

        s = 0, t = 0, u =0;
        for (int n = 0; n < basis->nBasis(); n++)
        {
          s += phi[n]*coord_s[n];
          t += phi[n]*coord_t[n];
          u += phi[n]*coord_u[n];
        }

        // Interpolation should match
        SANS_CHECK_CLOSE( sTrue, s, small_tol, close_tol );
        SANS_CHECK_CLOSE( tTrue, t, small_tol, close_tol );
        SANS_CHECK_CLOSE( uTrue, u, small_tol, close_tol );
      }
    }
  }
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP2 )
{
  typedef std::array<int,6> Int6;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Hex>::getBasisFunction(2, BasisFunctionCategory_Lagrange),
                     BasisFunctionVolumeBase<Hex>::LagrangeP2 );

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolumeBase<Hex>::LagrangeP2;

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( 27, basis->nBasis() );
  BOOST_CHECK_EQUAL( 8, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 6, basis->nBasisFace() );
  BOOST_CHECK_EQUAL( 12, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 1, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  const int N = 27;

  //Hessian Not Tested

  Real s, t, u;
  Real phi[N], phis[N], phit[N], phiu[N];
  Real phiss[N], phist[N], phitt[N], phisu[N], phitu[N], phiuu[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N], phiuTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N], phisuTrue[N], phituTrue[N], phiuuTrue[N];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 12; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0; u = 0;
      break;

    case 1:
      s = 1;  t = 0; u = 0;
      break;

    case 2:
      s = 0;  t = 1; u = 0;
      break;

    case 3:
      s = 0;  t = 1; u = 0;
      break;

    case 4:
      s = 1./2.;  t = 1./2.; u = 0;
      break;

    case 5:
      s = 1./3.;  t = 2./3.; u = 0;
      break;

    case 6:
      s = 0;  t = 0; u = 1;
      break;

    case 7:
      s = 1;  t = 0; u = 1;
      break;

    case 8:
      s = 0;  t = 1; u = 1;
      break;

    case 9:
      s = 0;  t = 1; u = 1;
      break;

    case 10:
      s = 1./2.;  t = 1./2.; u = 1;
      break;

    case 11:
      s = 1./3.;  t = 2./3.; u = 1;
      break;
    }

    Real sphi[3], tphi[3], uphi[3];
    BasisFunctionLine<Lagrange,2>::self()->evalBasis(s, sphi, 3);
    BasisFunctionLine<Lagrange,2>::self()->evalBasis(t, tphi, 3);
    BasisFunctionLine<Lagrange,2>::self()->evalBasis(u, uphi, 3);

    Real sphis[3], tphit[3], uphiu[3];
    BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(s, sphis, 3);
    BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(t, tphit, 3);
    BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(u, uphiu, 3);

    Real sphiss[3], tphitt[3], uphiuu[3];
    BasisFunctionLine<Lagrange,2>::self()->evalBasisHessianDerivative(s, sphiss, 3);
    BasisFunctionLine<Lagrange,2>::self()->evalBasisHessianDerivative(t, tphitt, 3);
    BasisFunctionLine<Lagrange,2>::self()->evalBasisHessianDerivative(u, uphiuu, 3);

    //phi -----------------------//
    phiTrue[0] = sphi[0]*tphi[0]*uphi[0]; // 1 @ node 0 (s = 0, t = 0, u = 0)
    phiTrue[1] = sphi[1]*tphi[0]*uphi[0]; // 1 @ node 1 (s = 1, t = 0, u = 0)
    phiTrue[2] = sphi[1]*tphi[1]*uphi[0]; // 1 @ node 2 (s = 1, t = 1, u = 0)
    phiTrue[3] = sphi[0]*tphi[1]*uphi[0]; // 1 @ node 3 (s = 0, t = 1, u = 0)

    phiTrue[4] = sphi[0]*tphi[0]*uphi[1]; // 1 @ node 4 (s = 0, t = 0, u = 1)
    phiTrue[5] = sphi[1]*tphi[0]*uphi[1]; // 1 @ node 5 (s = 1, t = 0, u = 1)
    phiTrue[6] = sphi[1]*tphi[1]*uphi[1]; // 1 @ node 6 (s = 1, t = 1, u = 1)
    phiTrue[7] = sphi[0]*tphi[1]*uphi[1]; // 1 @ node 7 (s = 0, t = 1, u = 1)

    phiTrue[8]  = sphi[0]*tphi[2]*uphi[0]; // 1 @ node 8  (s = 0.0, t = 0.5, u = 0.0)  E0
    phiTrue[9]  = sphi[2]*tphi[1]*uphi[0]; // 1 @ node 9  (s = 0.5, t = 1.0, u = 0.0)  E1
    phiTrue[10] = sphi[1]*tphi[2]*uphi[0]; // 1 @ node 10 (s = 1.0, t = 0.5, u = 0.0)  E2
    phiTrue[11] = sphi[2]*tphi[0]*uphi[0]; // 1 @ node 11 (s = 0.5, t = 0.0, u = 0.0)  E3
    phiTrue[12] = sphi[1]*tphi[0]*uphi[2]; // 1 @ node 12 (s = 1.0, t = 0.0  u = 0.5)  E4
    phiTrue[13] = sphi[2]*tphi[0]*uphi[1]; // 1 @ node 13 (s = 0.5, t = 0.0  u = 1.0)  E5
    phiTrue[14] = sphi[0]*tphi[0]*uphi[2]; // 1 @ node 14 (s = 0.0, t = 0.0  u = 0.5)  E6
    phiTrue[15] = sphi[1]*tphi[1]*uphi[2]; // 1 @ node 15 (s = 1.0, t = 1.0  u = 0.5)  E7
    phiTrue[16] = sphi[1]*tphi[2]*uphi[1]; // 1 @ node 16 (s = 1.0, t = 0.5  u = 1.0)  E8
    phiTrue[17] = sphi[0]*tphi[2]*uphi[1]; // 1 @ node 17 (s = 0.0, t = 0.5  u = 1.0)  E9
    phiTrue[18] = sphi[0]*tphi[1]*uphi[2]; // 1 @ node 18 (s = 0.0, t = 1.0  u = 0.5)  E10
    phiTrue[19] = sphi[2]*tphi[1]*uphi[1]; // 1 @ node 19 (s = 0.5, t = 1.0  u = 1.0)  E11

    phiTrue[20] = sphi[2]*tphi[2]*uphi[0]; // 1 @ node 20 (s = 0.5, t = 0.5  u = 0.0)  F0
    phiTrue[21] = sphi[2]*tphi[0]*uphi[2]; // 1 @ node 21 (s = 0.5, t = 0.0  u = 0.5)  F1
    phiTrue[22] = sphi[1]*tphi[2]*uphi[2]; // 1 @ node 22 (s = 1.0, t = 0.5  u = 0.5)  F2
    phiTrue[23] = sphi[2]*tphi[1]*uphi[2]; // 1 @ node 23 (s = 0.5, t = 1.0  u = 0.5)  F3
    phiTrue[24] = sphi[0]*tphi[2]*uphi[2]; // 1 @ node 24 (s = 0.0, t = 0.5  u = 0.5)  F4
    phiTrue[25] = sphi[2]*tphi[2]*uphi[1]; // 1 @ node 25 (s = 0.5, t = 0.5  u = 1.0)  F5

    phiTrue[26] = sphi[2]*tphi[2]*uphi[2]; // 1 @ node 26 (s = 0.5, t = 0.5, u = 0.5)  C0

    //phis, phit, phiu ----------------------//
    BasisFunctionVolume<Hex,Lagrange,2>::self()->tensorProduct(sphis, tphi , uphi, phisTrue);
    BasisFunctionVolume<Hex,Lagrange,2>::self()->tensorProduct(sphi , tphit, uphi, phitTrue);
    BasisFunctionVolume<Hex,Lagrange,2>::self()->tensorProduct(sphi , tphi, uphiu, phiuTrue);

    //phiss, phist, phitt, phisu, phitu, phiuu -----//
    BasisFunctionVolume<Hex,Lagrange,2>::self()->tensorProduct(sphiss, tphi  , uphi  , phissTrue);
    BasisFunctionVolume<Hex,Lagrange,2>::self()->tensorProduct(sphis , tphit , uphi  , phistTrue);
    BasisFunctionVolume<Hex,Lagrange,2>::self()->tensorProduct(sphi  , tphitt, uphi  , phittTrue);
    BasisFunctionVolume<Hex,Lagrange,2>::self()->tensorProduct(sphis , tphi  , uphiu , phisuTrue);
    BasisFunctionVolume<Hex,Lagrange,2>::self()->tensorProduct(sphi  , tphit , uphiu , phituTrue);
    BasisFunctionVolume<Hex,Lagrange,2>::self()->tensorProduct(sphi  , tphi  , uphiuu, phiuuTrue);


    basis->evalBasis( s, t, u, facesign, phi, N );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, N );
    basis->evalBasisHessianDerivative( s, t, u, facesign, phiss, phist, phitt, phisu, phitu, phiuu, N );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
    }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis()), coord_u(basis->nBasis());
  BasisFunctionVolumeBase<Hex>::LagrangeP2->coordinates( coord_s, coord_t, coord_u );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n],coord_u[n], facesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }


  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*basis->order()+1;
  for (int k = 0; k < npoints; k++)
  {
    for (int j = 0; j < npoints; j++)
    {
      for (int i = 0; i < npoints; i++)
      {
        // Reference coordinates
        Real sTrue = i*1/Real(npoints+1);
        Real tTrue = j*1/Real(npoints+1);
        Real uTrue = k*1/Real(npoints+1);

        // Interpolated reference coordinate
        basis->evalBasis( sTrue, tTrue, uTrue, facesign, phi, basis->nBasis() );

        s = 0, t = 0, u =0;
        for (int n = 0; n < basis->nBasis(); n++)
        {
          s += phi[n]*coord_s[n];
          t += phi[n]*coord_t[n];
          u += phi[n]*coord_u[n];
        }

        // Interpolation should match
        SANS_CHECK_CLOSE( sTrue, s, small_tol, close_tol );
        SANS_CHECK_CLOSE( tTrue, t, small_tol, close_tol );
        SANS_CHECK_CLOSE( uTrue, u, small_tol, close_tol );
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP3 )
{
  typedef std::array<int,6> Int6;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Hex>::getBasisFunction(3, BasisFunctionCategory_Lagrange),
                     BasisFunctionVolumeBase<Hex>::LagrangeP3 );

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolumeBase<Hex>::LagrangeP3;

  BOOST_CHECK_EQUAL( 3, basis->order() );
  BOOST_CHECK_EQUAL( 64, basis->nBasis() );
  BOOST_CHECK_EQUAL( 8, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 24, basis->nBasisFace() );
  BOOST_CHECK_EQUAL( 24, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 8, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  const int N = 64;

  //Hessian Not Tested

  Real s, t, u;
  Real phi[N], phis[N], phit[N], phiu[N];
  Real phiss[N], phist[N], phitt[N], phisu[N], phitu[N], phiuu[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N], phiuTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N], phisuTrue[N], phituTrue[N], phiuuTrue[N];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 24; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0; u = 0;
      break;

    case 1:
      s = 1;  t = 0; u = 0;
      break;

    case 2:
      s = 0;  t = 1; u = 0;
      break;

    case 3:
      s = 0;  t = 1; u = 0;
      break;

    case 4:
      s = 1./2.;  t = 1./2.; u = 0;
      break;

    case 5:
      s = 1./3.;  t = 2./3.; u = 0;
      break;

    case 6:
      s = 0;  t = 0; u = 1;
      break;

    case 7:
      s = 1;  t = 0; u = 1;
      break;

    case 8:
      s = 0;  t = 1; u = 1;
      break;

    case 9:
      s = 0;  t = 1; u = 1;
      break;

    case 10:
      s = 1./2.;  t = 1./2.; u = 1;
      break;

    case 11:
      s = 1./3.;  t = 2./3.; u = 1;
      break;

    case 12:
      s = 0;  t = 0; u = 1/3;
      break;

    case 13:
      s = 1;  t = 0; u = 1/3;
      break;

    case 14:
      s = 0;  t = 1; u = 1/3;
      break;

    case 15:
      s = 0;  t = 1; u = 1/3;
      break;

    case 16:
      s = 1./2.;  t = 1./2.; u = 1/3;
      break;

    case 17:
      s = 1./3.;  t = 2./3.; u = 1/3;
      break;

    case 18:
      s = 0;  t = 0; u = 2/3;
      break;

    case 19:
      s = 1;  t = 0; u = 2/3;
      break;

    case 20:
      s = 0;  t = 1; u = 2/3;
      break;

    case 21:
      s = 0;  t = 1; u = 2/3;
      break;

    case 22:
      s = 1./2.;  t = 1./2.; u = 2/3;
      break;

    case 23:
      s = 1./3.;  t = 2./3.; u = 2/3;
      break;
    }

    Real sphi[4], tphi[4], uphi[4];
    BasisFunctionLine<Lagrange,3>::self()->evalBasis(s, sphi, 4);
    BasisFunctionLine<Lagrange,3>::self()->evalBasis(t, tphi, 4);
    BasisFunctionLine<Lagrange,3>::self()->evalBasis(u, uphi, 4);

    Real sphis[4], tphit[4], uphiu[4];
    BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(s, sphis, 4);
    BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(t, tphit, 4);
    BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(u, uphiu, 4);

    Real sphiss[4], tphitt[4], uphiuu[4];
    BasisFunctionLine<Lagrange,3>::self()->evalBasisHessianDerivative(s, sphiss, 4);
    BasisFunctionLine<Lagrange,3>::self()->evalBasisHessianDerivative(t, tphitt, 4);
    BasisFunctionLine<Lagrange,3>::self()->evalBasisHessianDerivative(u, uphiuu, 4);

    //phi -----------------------//
    phiTrue[0] = sphi[0]*tphi[0]*uphi[0]; // 1 @ node 0 (s = 0, t = 0, u = 0)
    phiTrue[1] = sphi[1]*tphi[0]*uphi[0]; // 1 @ node 1 (s = 1, t = 0, u = 0)
    phiTrue[2] = sphi[1]*tphi[1]*uphi[0]; // 1 @ node 2 (s = 1, t = 1, u = 0)
    phiTrue[3] = sphi[0]*tphi[1]*uphi[0]; // 1 @ node 3 (s = 0, t = 1, u = 0)

    phiTrue[4] = sphi[0]*tphi[0]*uphi[1]; // 1 @ node 4 (s = 0, t = 0, u = 1)
    phiTrue[5] = sphi[1]*tphi[0]*uphi[1]; // 1 @ node 5 (s = 1, t = 0, u = 1)
    phiTrue[6] = sphi[1]*tphi[1]*uphi[1]; // 1 @ node 6 (s = 1, t = 1, u = 1)
    phiTrue[7] = sphi[0]*tphi[1]*uphi[1]; // 1 @ node 7 (s = 0, t = 1, u = 1)

    // Edge 0 (s = 0,         u = 0)
    phiTrue[8]  = sphi[0]*tphi[2]*uphi[0]; // 1 @ node 8 (s = 0.0, t = 1/3, u = 0.0)
    phiTrue[9]  = sphi[0]*tphi[3]*uphi[0]; // 1 @ node 9 (s = 0.0, t = 2/3, u = 0.0)

    // Edge 1 (       t = 1,  u = 0)
    phiTrue[10] = sphi[2]*tphi[1]*uphi[0]; // 1 @ node 10 (s = 1/3, t = 1.0, u = 0.0)
    phiTrue[11] = sphi[3]*tphi[1]*uphi[0]; // 1 @ node 11 (s = 2/3, t = 1.0, u = 0.0)

    // Edge 2 (s = 1,         u = 0)
    phiTrue[12] = sphi[1]*tphi[3]*uphi[0]; // 1 @ node 12 (s = 1.0, t = 2/3, u = 0.0)
    phiTrue[13] = sphi[1]*tphi[2]*uphi[0]; // 1 @ node 13 (s = 1.0, t = 1/3, u = 0.0)

    // Edge 3 (       t = 0,  u = 0)
    phiTrue[14] = sphi[3]*tphi[0]*uphi[0]; // 1 @ node 14 (s = 2/3, t = 0.0, u = 0.0)
    phiTrue[15] = sphi[2]*tphi[0]*uphi[0]; // 1 @ node 15 (s = 1/3, t = 0.0, u = 0.0)

    // Edge 4 (s = 1, t = 0        )
    phiTrue[16] = sphi[1]*tphi[0]*uphi[2]; // 1 @ node 16 (s = 1.0, t = 0.0, u = 1/3)
    phiTrue[17] = sphi[1]*tphi[0]*uphi[3]; // 1 @ node 17 (s = 1.0, t = 0.0, u = 2/3)

    // Edge 5 (t = 0,         u = 1)
    phiTrue[18] = sphi[3]*tphi[0]*uphi[1]; // 1 @ node 18 (s = 2/3, t = 0.0, u = 1.0)
    phiTrue[19] = sphi[2]*tphi[0]*uphi[1]; // 1 @ node 19 (s = 1/3, t = 0.0, u = 1.0)

    // Edge 6 (s = 0,  t = 0       )
    phiTrue[20] = sphi[0]*tphi[0]*uphi[3]; // 1 @ node 20 (s = 0.0, t = 0.0, u = 2/3)
    phiTrue[21] = sphi[0]*tphi[0]*uphi[2]; // 1 @ node 21 (s = 0.0, t = 0.0, u = 1/3)

    // Edge 7 (s = 1,  t = 1       )
    phiTrue[22] = sphi[1]*tphi[1]*uphi[2]; // 1 @ node 22 (s = 1.0, t = 1.0, u = 1/3)
    phiTrue[23] = sphi[1]*tphi[1]*uphi[3]; // 1 @ node 23 (s = 1.0, t = 1.0, u = 2/3)

    // Edge 8 (s = 1,         u = 1)
    phiTrue[24] = sphi[1]*tphi[3]*uphi[1]; // 1 @ node 24 (s = 1.0, t = 2/3, u = 1.0)
    phiTrue[25] = sphi[1]*tphi[2]*uphi[1]; // 1 @ node 25 (s = 1.0, t = 1/3, u = 1.0)

    // Edge 9 (s = 0,         u = 1)
    phiTrue[26] = sphi[0]*tphi[2]*uphi[1]; // 1 @ node 26 (s = 0.0, t = 1/3, u = 1.0)
    phiTrue[27] = sphi[0]*tphi[3]*uphi[1]; // 1 @ node 27 (s = 0.0, t = 2/3, u = 1.0)

    // Edge 10 (s = 0,        t = 1)
    phiTrue[28] = sphi[0]*tphi[1]*uphi[2]; // 1 @ node 28 (s = 0.0, t = 1.0, u = 1/3)
    phiTrue[29] = sphi[0]*tphi[1]*uphi[3]; // 1 @ node 29 (s = 0.0, t = 1.0, u = 2/3)

    // Edge 11 (      t = 1,  u = 1)
    phiTrue[30] = sphi[2]*tphi[1]*uphi[1]; // 1 @ node 30 (s = 1/3, t = 1.0, u = 1.0)
    phiTrue[31] = sphi[3]*tphi[1]*uphi[1]; // 1 @ node 31 (s = 2/3, t = 1.0, u = 1.0)

    // Face 0 (u = 0)
    phiTrue[32] = sphi[2]*tphi[2]*uphi[0]; // 1 @ node 32 (s = 1/3, t = 1/3, u = 0.0)
    phiTrue[33] = sphi[2]*tphi[3]*uphi[0]; // 1 @ node 33 (s = 1/3, t = 2/3, u = 0.0)
    phiTrue[34] = sphi[3]*tphi[3]*uphi[0]; // 1 @ node 34 (s = 2/3, t = 2/3, u = 0.0)
    phiTrue[35] = sphi[3]*tphi[2]*uphi[0]; // 1 @ node 35 (s = 2/3, t = 1/3, u = 0.0)

    // Face 1 (t = 0)
    phiTrue[36] = sphi[2]*tphi[0]*uphi[2]; // 1 @ node 36 (s = 1/3, t = 0.0, u = 1/3)
    phiTrue[37] = sphi[3]*tphi[0]*uphi[2]; // 1 @ node 37 (s = 2/3, t = 0.0, u = 1/3)
    phiTrue[38] = sphi[3]*tphi[0]*uphi[3]; // 1 @ node 38 (s = 2/3, t = 0.0, u = 2/3)
    phiTrue[39] = sphi[2]*tphi[0]*uphi[3]; // 1 @ node 39 (s = 1/3, t = 0.0, u = 2/3)

    // Edge 2 (s = 1)
    phiTrue[40] = sphi[1]*tphi[2]*uphi[2]; // 1 @ node 40 (s = 1.0, t = 1/3, u = 1/3)
    phiTrue[41] = sphi[1]*tphi[3]*uphi[2]; // 1 @ node 41 (s = 1.0, t = 2/3, u = 1/3)
    phiTrue[42] = sphi[1]*tphi[3]*uphi[3]; // 1 @ node 42 (s = 1.0, t = 2/3, u = 2/3)
    phiTrue[43] = sphi[1]*tphi[2]*uphi[3]; // 1 @ node 43 (s = 1.0, t = 1/3, u = 2/3)

    // Face 3 (t = 1)
    phiTrue[44] = sphi[2]*tphi[1]*uphi[2]; // 1 @ node 44 (s = 1/3, t = 1.0, u = 1/3)
    phiTrue[45] = sphi[2]*tphi[1]*uphi[3]; // 1 @ node 45 (s = 1/3, t = 1.0, u = 2/3)
    phiTrue[46] = sphi[3]*tphi[1]*uphi[3]; // 1 @ node 46 (s = 2/3, t = 1.0, u = 2/3)
    phiTrue[47] = sphi[3]*tphi[1]*uphi[2]; // 1 @ node 47 (s = 2/3, t = 1.0, u = 1/3)

    // Face 4 (s = 0)
    phiTrue[48] = sphi[0]*tphi[2]*uphi[2]; // 1 @ node 48 (s = 0.0, t = 1/3, u = 1/3)
    phiTrue[49] = sphi[0]*tphi[2]*uphi[3]; // 1 @ node 49 (s = 0.0, t = 1/3, u = 2/3)
    phiTrue[50] = sphi[0]*tphi[3]*uphi[3]; // 1 @ node 50 (s = 0.0, t = 2/3, u = 2/3)
    phiTrue[51] = sphi[0]*tphi[3]*uphi[2]; // 1 @ node 51 (s = 0.0, t = 2/3, u = 1/3)

    // Face 5 (u = 1)
    phiTrue[52] = sphi[2]*tphi[2]*uphi[1]; // 1 @ node 52 (s = 1/3, t = 1/3, u = 1.0)
    phiTrue[53] = sphi[3]*tphi[2]*uphi[1]; // 1 @ node 53 (s = 2/3, t = 1/3, u = 1.0)
    phiTrue[54] = sphi[3]*tphi[3]*uphi[1]; // 1 @ node 54 (s = 2/3, t = 2/3, u = 1.0)
    phiTrue[55] = sphi[2]*tphi[3]*uphi[1]; // 1 @ node 55 (s = 1/3, t = 2/3, u = 1.0)

    // Cell
    phiTrue[56] = sphi[2]*tphi[2]*uphi[2]; // 1 @ node 56 (s = 1/3, t = 1/3, u = 1/3)
    phiTrue[57] = sphi[3]*tphi[2]*uphi[2]; // 1 @ node 57 (s = 2/3, t = 1/3, u = 1/3)
    phiTrue[58] = sphi[3]*tphi[3]*uphi[2]; // 1 @ node 58 (s = 2/3, t = 2/3, u = 1/3)
    phiTrue[59] = sphi[2]*tphi[3]*uphi[2]; // 1 @ node 59 (s = 1/3, t = 2/3, u = 1/3)
    phiTrue[60] = sphi[2]*tphi[2]*uphi[3]; // 1 @ node 60 (s = 1/3, t = 1/3, u = 2/3)
    phiTrue[61] = sphi[3]*tphi[2]*uphi[3]; // 1 @ node 61 (s = 2/3, t = 1/3, u = 2/3)
    phiTrue[62] = sphi[3]*tphi[3]*uphi[3]; // 1 @ node 62 (s = 2/3, t = 2/3, u = 2/3)
    phiTrue[63] = sphi[2]*tphi[3]*uphi[3]; // 1 @ node 63 (s = 1/3, t = 2/3, u = 2/3)

    //phis, phit, phiu ----------------------//
    BasisFunctionVolume<Hex,Lagrange,3>::self()->tensorProduct(sphis, tphi , uphi, phisTrue);
    BasisFunctionVolume<Hex,Lagrange,3>::self()->tensorProduct(sphi , tphit, uphi, phitTrue);
    BasisFunctionVolume<Hex,Lagrange,3>::self()->tensorProduct(sphi , tphi, uphiu, phiuTrue);

    //phiss, phist, phitt, phisu, phitu, phiuu -----//
    BasisFunctionVolume<Hex,Lagrange,3>::self()->tensorProduct(sphiss, tphi  , uphi  , phissTrue);
    BasisFunctionVolume<Hex,Lagrange,3>::self()->tensorProduct(sphis , tphit , uphi  , phistTrue);
    BasisFunctionVolume<Hex,Lagrange,3>::self()->tensorProduct(sphi  , tphitt, uphi  , phittTrue);
    BasisFunctionVolume<Hex,Lagrange,3>::self()->tensorProduct(sphis , tphi  , uphiu , phisuTrue);
    BasisFunctionVolume<Hex,Lagrange,3>::self()->tensorProduct(sphi  , tphit , uphiu , phituTrue);
    BasisFunctionVolume<Hex,Lagrange,3>::self()->tensorProduct(sphi  , tphi  , uphiuu, phiuuTrue);


    basis->evalBasis( s, t, u, facesign, phi, N );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, N );
    basis->evalBasisHessianDerivative( s, t, u, facesign, phiss, phist, phitt, phisu, phitu, phiuu, N );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
    }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis()), coord_u(basis->nBasis());
  BasisFunctionVolumeBase<Hex>::LagrangeP3->coordinates( coord_s, coord_t, coord_u );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n],coord_u[n], facesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }


  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*basis->order()+1;
  for (int k = 0; k < npoints; k++)
  {
    for (int j = 0; j < npoints; j++)
    {
      for (int i = 0; i < npoints; i++)
      {
        // Reference coordinates
        Real sTrue = i*1/Real(npoints+1);
        Real tTrue = j*1/Real(npoints+1);
        Real uTrue = k*1/Real(npoints+1);

        // Interpolated reference coordinate
        basis->evalBasis( sTrue, tTrue, uTrue, facesign, phi, basis->nBasis() );

        s = 0, t = 0, u =0;
        for (int n = 0; n < basis->nBasis(); n++)
        {
          s += phi[n]*coord_s[n];
          t += phi[n]*coord_t[n];
          u += phi[n]*coord_u[n];
        }

        // Interpolation should match
        SANS_CHECK_CLOSE( sTrue, s, small_tol, close_tol );
        SANS_CHECK_CLOSE( tTrue, t, small_tol, close_tol );
        SANS_CHECK_CLOSE( uTrue, u, small_tol, close_tol );
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP4 )
{
  typedef std::array<int,6> Int6;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Hex>::getBasisFunction(4, BasisFunctionCategory_Lagrange),
                     BasisFunctionVolumeBase<Hex>::LagrangeP4 );

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolumeBase<Hex>::LagrangeP4;

  BOOST_CHECK_EQUAL( 4, basis->order() );
  BOOST_CHECK_EQUAL( 125, basis->nBasis() );
  BOOST_CHECK_EQUAL( 8, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 54, basis->nBasisFace() );
  BOOST_CHECK_EQUAL( 36, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 27, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  const int N = 125;

  Real s, t, u;
  Real phi[N], phis[N], phit[N], phiu[N];
  Real phiss[N], phist[N], phitt[N], phisu[N], phitu[N], phiuu[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N], phiuTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N], phisuTrue[N], phituTrue[N], phiuuTrue[N];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 24; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0; u = 0;
      break;

    case 1:
      s = 1;  t = 0; u = 0;
      break;

    case 2:
      s = 0;  t = 1; u = 0;
      break;

    case 3:
      s = 0;  t = 1; u = 0;
      break;

    case 4:
      s = 1./2.;  t = 1./2.; u = 0;
      break;

    case 5:
      s = 1./3.;  t = 2./3.; u = 0;
      break;

    case 6:
      s = 0;  t = 0; u = 1;
      break;

    case 7:
      s = 1;  t = 0; u = 1;
      break;

    case 8:
      s = 0;  t = 1; u = 1;
      break;

    case 9:
      s = 0;  t = 1; u = 1;
      break;

    case 10:
      s = 1./2.;  t = 1./2.; u = 1;
      break;

    case 11:
      s = 1./3.;  t = 2./3.; u = 1;
      break;

    case 12:
      s = 0;  t = 0; u = 1/3;
      break;

    case 13:
      s = 1;  t = 0; u = 1/3;
      break;

    case 14:
      s = 0;  t = 1; u = 1/3;
      break;

    case 15:
      s = 0;  t = 1; u = 1/3;
      break;

    case 16:
      s = 1./2.;  t = 1./2.; u = 1/3;
      break;

    case 17:
      s = 1./3.;  t = 2./3.; u = 1/3;
      break;

    case 18:
      s = 0;  t = 0; u = 2/3;
      break;

    case 19:
      s = 1;  t = 0; u = 2/3;
      break;

    case 20:
      s = 0;  t = 1; u = 2/3;
      break;

    case 21:
      s = 0;  t = 1; u = 2/3;
      break;

    case 22:
      s = 1./2.;  t = 1./2.; u = 2/3;
      break;

    case 23:
      s = 1./3.;  t = 2./3.; u = 2/3;
      break;
    }

    Real sphi[5], tphi[5], uphi[5];
    BasisFunctionLine<Lagrange,4>::self()->evalBasis(s, sphi, 5);
    BasisFunctionLine<Lagrange,4>::self()->evalBasis(t, tphi, 5);
    BasisFunctionLine<Lagrange,4>::self()->evalBasis(u, uphi, 5);

    Real sphis[5], tphit[5], uphiu[5];
    BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(s, sphis, 5);
    BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(t, tphit, 5);
    BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(u, uphiu, 5);

    Real sphiss[5], tphitt[5], uphiuu[5];
    BasisFunctionLine<Lagrange,4>::self()->evalBasisHessianDerivative(s, sphiss, 5);
    BasisFunctionLine<Lagrange,4>::self()->evalBasisHessianDerivative(t, tphitt, 5);
    BasisFunctionLine<Lagrange,4>::self()->evalBasisHessianDerivative(u, uphiuu, 5);

    //phi -----------------------//

    phiTrue[0] = sphi[0]*tphi[0]*uphi[0]; // 1 @ node 0 (s = 0, t = 0, u = 0)
    phiTrue[1] = sphi[1]*tphi[0]*uphi[0]; // 1 @ node 1 (s = 1, t = 0, u = 0)
    phiTrue[2] = sphi[1]*tphi[1]*uphi[0]; // 1 @ node 2 (s = 1, t = 1, u = 0)
    phiTrue[3] = sphi[0]*tphi[1]*uphi[0]; // 1 @ node 3 (s = 0, t = 1, u = 0)

    phiTrue[4] = sphi[0]*tphi[0]*uphi[1]; // 1 @ node 4 (s = 0, t = 0, u = 1)
    phiTrue[5] = sphi[1]*tphi[0]*uphi[1]; // 1 @ node 5 (s = 1, t = 0, u = 1)
    phiTrue[6] = sphi[1]*tphi[1]*uphi[1]; // 1 @ node 6 (s = 1, t = 1, u = 1)
    phiTrue[7] = sphi[0]*tphi[1]*uphi[1]; // 1 @ node 7 (s = 0, t = 1, u = 1)

    // Edge 0 (s = 0,         u = 0)
    phiTrue[8]  = sphi[0]*tphi[2]*uphi[0]; // 1 @ node 8 (s = 0.0, t = 1/4, u = 0.0)
    phiTrue[9]  = sphi[0]*tphi[3]*uphi[0]; // 1 @ node 9 (s = 0.0, t = 2/4, u = 0.0)
    phiTrue[10] = sphi[0]*tphi[4]*uphi[0]; // 1 @ node 10 (s = 0.0, t = 3/4, u = 0.0)

    // Edge 1 (       t = 1,  u = 0)
    phiTrue[11] = sphi[2]*tphi[1]*uphi[0]; // 1 @ node 11 (s = 1/4, t = 1.0, u = 0.0)
    phiTrue[12] = sphi[3]*tphi[1]*uphi[0]; // 1 @ node 12 (s = 2/4, t = 1.0, u = 0.0)
    phiTrue[13] = sphi[4]*tphi[1]*uphi[0]; // 1 @ node 13 (s = 3/4, t = 1.0, u = 0.0)

    // Edge 2 (s = 1,         u = 0)
    phiTrue[14] = sphi[1]*tphi[4]*uphi[0]; // 1 @ node 14 (s = 1.0, t = 3/4, u = 0.0)
    phiTrue[15] = sphi[1]*tphi[3]*uphi[0]; // 1 @ node 15 (s = 1.0, t = 2/4, u = 0.0)
    phiTrue[16] = sphi[1]*tphi[2]*uphi[0]; // 1 @ node 16 (s = 1.0, t = 1/4, u = 0.0)

    // Edge 3 (       t = 0,  u = 0)
    phiTrue[17] = sphi[4]*tphi[0]*uphi[0]; // 1 @ node 17 (s = 3/4, t = 0.0, u = 0.0)
    phiTrue[18] = sphi[3]*tphi[0]*uphi[0]; // 1 @ node 18 (s = 2/4, t = 0.0, u = 0.0)
    phiTrue[19] = sphi[2]*tphi[0]*uphi[0]; // 1 @ node 19 (s = 1/4, t = 0.0, u = 0.0)

    // Edge 4 (s = 1, t = 0        )
    phiTrue[20] = sphi[1]*tphi[0]*uphi[2]; // 1 @ node 20 (s = 1.0, t = 0.0, u = 1/4)
    phiTrue[21] = sphi[1]*tphi[0]*uphi[3]; // 1 @ node 21 (s = 1.0, t = 0.0, u = 2/4)
    phiTrue[22] = sphi[1]*tphi[0]*uphi[4]; // 1 @ node 22 (s = 1.0, t = 0.0, u = 3/4)

    // Edge 5 (t = 0,         u = 1)
    phiTrue[23] = sphi[4]*tphi[0]*uphi[1]; // 1 @ node 23 (s = 3/4, t = 0.0, u = 1.0)
    phiTrue[24] = sphi[3]*tphi[0]*uphi[1]; // 1 @ node 24 (s = 2/4, t = 0.0, u = 1.0)
    phiTrue[25] = sphi[2]*tphi[0]*uphi[1]; // 1 @ node 25 (s = 1/4, t = 0.0, u = 1.0)

    // Edge 6 (s = 0,  t = 0       )
    phiTrue[26] = sphi[0]*tphi[0]*uphi[4]; // 1 @ node 26 (s = 0.0, t = 0.0, u = 3/4)
    phiTrue[27] = sphi[0]*tphi[0]*uphi[3]; // 1 @ node 27 (s = 0.0, t = 0.0, u = 2/4)
    phiTrue[28] = sphi[0]*tphi[0]*uphi[2]; // 1 @ node 28 (s = 0.0, t = 0.0, u = 1/4)

    // Edge 7 (s = 1,  t = 1       )
    phiTrue[29] = sphi[1]*tphi[1]*uphi[2]; // 1 @ node 29 (s = 1.0, t = 1.0, u = 1/4)
    phiTrue[30] = sphi[1]*tphi[1]*uphi[3]; // 1 @ node 30 (s = 1.0, t = 1.0, u = 2/4)
    phiTrue[31] = sphi[1]*tphi[1]*uphi[4]; // 1 @ node 31 (s = 1.0, t = 1.0, u = 3/4)

    // Edge 8 (s = 1,         u = 1)
    phiTrue[32] = sphi[1]*tphi[4]*uphi[1]; // 1 @ node 32 (s = 1.0, t = 3/4, u = 1.0)
    phiTrue[33] = sphi[1]*tphi[3]*uphi[1]; // 1 @ node 33 (s = 1.0, t = 2/4, u = 1.0)
    phiTrue[34] = sphi[1]*tphi[2]*uphi[1]; // 1 @ node 34 (s = 1.0, t = 1/4, u = 1.0)

    // Edge 9 (s = 0,         u = 1)
    phiTrue[35] = sphi[0]*tphi[2]*uphi[1]; // 1 @ node 35 (s = 0.0, t = 1/4, u = 1.0)
    phiTrue[36] = sphi[0]*tphi[3]*uphi[1]; // 1 @ node 36 (s = 0.0, t = 2/4, u = 1.0)
    phiTrue[37] = sphi[0]*tphi[4]*uphi[1]; // 1 @ node 37 (s = 0.0, t = 3/4, u = 1.0)

    // Edge 10 (s = 0,        t = 1)
    phiTrue[38] = sphi[0]*tphi[1]*uphi[2]; // 1 @ node 38 (s = 0.0, t = 1.0, u = 1/4)
    phiTrue[39] = sphi[0]*tphi[1]*uphi[3]; // 1 @ node 39 (s = 0.0, t = 1.0, u = 2/4)
    phiTrue[40] = sphi[0]*tphi[1]*uphi[4]; // 1 @ node 40 (s = 0.0, t = 1.0, u = 3/4)

    // Edge 11 (      t = 1,  u = 1)
    phiTrue[41] = sphi[2]*tphi[1]*uphi[1]; // 1 @ node 41 (s = 1/4, t = 1.0, u = 1.0)
    phiTrue[42] = sphi[3]*tphi[1]*uphi[1]; // 1 @ node 42 (s = 2/4, t = 1.0, u = 1.0)
    phiTrue[43] = sphi[4]*tphi[1]*uphi[1]; // 1 @ node 43 (s = 3/4, t = 1.0, u = 1.0)

    // Face 0 (u = 0)
    phiTrue[44] = sphi[2]*tphi[2]*uphi[0]; // 1 @ node 44 (s = 1/4, t = 1/4, u = 0.0)
    phiTrue[45] = sphi[2]*tphi[4]*uphi[0]; // 1 @ node 45 (s = 1/4, t = 3/4, u = 0.0)
    phiTrue[46] = sphi[4]*tphi[4]*uphi[0]; // 1 @ node 46 (s = 3/4, t = 3/4, u = 0.0)
    phiTrue[47] = sphi[4]*tphi[2]*uphi[0]; // 1 @ node 47 (s = 3/4, t = 1/4, u = 0.0)
    phiTrue[48] = sphi[2]*tphi[3]*uphi[0]; // 1 @ node 48 (s = 1/4, t = 2/4, u = 0.0)
    phiTrue[49] = sphi[3]*tphi[4]*uphi[0]; // 1 @ node 49 (s = 2/4, t = 3/4, u = 0.0)
    phiTrue[50] = sphi[4]*tphi[3]*uphi[0]; // 1 @ node 50 (s = 3/4, t = 2/4, u = 0.0)
    phiTrue[51] = sphi[3]*tphi[2]*uphi[0]; // 1 @ node 51 (s = 2/4, t = 1/4, u = 0.0)
    phiTrue[52] = sphi[3]*tphi[3]*uphi[0]; // 1 @ node 52 (s = 2/4, t = 2/4, u = 0.0)

    // Face 1 (t = 0)
    phiTrue[53] = sphi[2]*tphi[0]*uphi[2]; // 1 @ node 53 (s = 1/4, t = 0.0, u = 1/4)
    phiTrue[54] = sphi[4]*tphi[0]*uphi[2]; // 1 @ node 54 (s = 3/4, t = 0.0, u = 1/4)
    phiTrue[55] = sphi[4]*tphi[0]*uphi[4]; // 1 @ node 55 (s = 3/4, t = 0.0, u = 3/4)
    phiTrue[56] = sphi[2]*tphi[0]*uphi[4]; // 1 @ node 56 (s = 1/4, t = 0.0, u = 3/4)
    phiTrue[57] = sphi[3]*tphi[0]*uphi[2]; // 1 @ node 57 (s = 2/4, t = 0.0, u = 1/4)
    phiTrue[58] = sphi[4]*tphi[0]*uphi[3]; // 1 @ node 58 (s = 3/4, t = 0.0, u = 2/4)
    phiTrue[59] = sphi[3]*tphi[0]*uphi[4]; // 1 @ node 59 (s = 2/4, t = 0.0, u = 3/4)
    phiTrue[60] = sphi[2]*tphi[0]*uphi[3]; // 1 @ node 60 (s = 1/4, t = 0.0, u = 2/4)
    phiTrue[61] = sphi[3]*tphi[0]*uphi[3]; // 1 @ node 61 (s = 2/4, t = 0.0, u = 2/4)

    // Face 2 (s = 1)
    phiTrue[62] = sphi[1]*tphi[2]*uphi[2]; // 1 @ node 62 (s = 1.0, t = 1/4, u = 1/4)
    phiTrue[63] = sphi[1]*tphi[4]*uphi[2]; // 1 @ node 63 (s = 1.0, t = 3/4, u = 1/4)
    phiTrue[64] = sphi[1]*tphi[4]*uphi[4]; // 1 @ node 64 (s = 1.0, t = 3/4, u = 3/4)
    phiTrue[65] = sphi[1]*tphi[2]*uphi[4]; // 1 @ node 65 (s = 1.0, t = 1/4, u = 3/4)
    phiTrue[66] = sphi[1]*tphi[3]*uphi[2]; // 1 @ node 66 (s = 1.0, t = 2/4, u = 1/4)
    phiTrue[67] = sphi[1]*tphi[4]*uphi[3]; // 1 @ node 67 (s = 1.0, t = 3/4, u = 2/4)
    phiTrue[68] = sphi[1]*tphi[3]*uphi[4]; // 1 @ node 68 (s = 1.0, t = 2/4, u = 3/4)
    phiTrue[69] = sphi[1]*tphi[2]*uphi[3]; // 1 @ node 69 (s = 1.0, t = 1/4, u = 2/4)
    phiTrue[70] = sphi[1]*tphi[3]*uphi[3]; // 1 @ node 70 (s = 1.0, t = 2/4, u = 2/4)

    // Face 3 (t = 1)
    phiTrue[71] = sphi[2]*tphi[1]*uphi[2]; // 1 @ node 71 (s = 1/4, t = 1.0, u = 1/4)
    phiTrue[72] = sphi[2]*tphi[1]*uphi[4]; // 1 @ node 72 (s = 1/4, t = 1.0, u = 3/4)
    phiTrue[73] = sphi[4]*tphi[1]*uphi[4]; // 1 @ node 73 (s = 3/4, t = 1.0, u = 3/4)
    phiTrue[74] = sphi[4]*tphi[1]*uphi[2]; // 1 @ node 74 (s = 3/4, t = 1.0, u = 1/4)
    phiTrue[75] = sphi[2]*tphi[1]*uphi[3]; // 1 @ node 75 (s = 1/4, t = 1.0, u = 2/4)
    phiTrue[76] = sphi[3]*tphi[1]*uphi[4]; // 1 @ node 76 (s = 2/4, t = 1.0, u = 3/4)
    phiTrue[77] = sphi[4]*tphi[1]*uphi[3]; // 1 @ node 77 (s = 3/4, t = 1.0, u = 2/4)
    phiTrue[78] = sphi[3]*tphi[1]*uphi[2]; // 1 @ node 78 (s = 2/4, t = 1.0, u = 1/4)
    phiTrue[79] = sphi[3]*tphi[1]*uphi[3]; // 1 @ node 79 (s = 2/4, t = 1.0, u = 2/4)

    // Face 4 (s = 0)
    phiTrue[80] = sphi[0]*tphi[2]*uphi[2]; // 1 @ node 80 (s = 0.0, t = 1/4, u = 1/4)
    phiTrue[81] = sphi[0]*tphi[2]*uphi[4]; // 1 @ node 81 (s = 0.0, t = 1/4, u = 3/4)
    phiTrue[82] = sphi[0]*tphi[4]*uphi[4]; // 1 @ node 82 (s = 0.0, t = 3/4, u = 3/4)
    phiTrue[83] = sphi[0]*tphi[4]*uphi[2]; // 1 @ node 83 (s = 0.0, t = 3/4, u = 1/4)
    phiTrue[84] = sphi[0]*tphi[2]*uphi[3]; // 1 @ node 84 (s = 0.0, t = 1/4, u = 2/4)
    phiTrue[85] = sphi[0]*tphi[3]*uphi[4]; // 1 @ node 85 (s = 0.0, t = 2/4, u = 3/4)
    phiTrue[86] = sphi[0]*tphi[4]*uphi[3]; // 1 @ node 86 (s = 0.0, t = 3/4, u = 2/4)
    phiTrue[87] = sphi[0]*tphi[3]*uphi[2]; // 1 @ node 87 (s = 0.0, t = 2/4, u = 1/4)
    phiTrue[88] = sphi[0]*tphi[3]*uphi[3]; // 1 @ node 88 (s = 0.0, t = 2/4, u = 2/4)

    // Face 5 (u = 1)
    phiTrue[89] = sphi[2]*tphi[2]*uphi[1]; // 1 @ node 89 (s = 1/4, t = 1/4, u = 1.0)
    phiTrue[90] = sphi[4]*tphi[2]*uphi[1]; // 1 @ node 90 (s = 3/4, t = 1/4, u = 1.0)
    phiTrue[91] = sphi[4]*tphi[4]*uphi[1]; // 1 @ node 91 (s = 3/4, t = 3/4, u = 1.0)
    phiTrue[92] = sphi[2]*tphi[4]*uphi[1]; // 1 @ node 92 (s = 1/4, t = 3/4, u = 1.0)
    phiTrue[93] = sphi[3]*tphi[2]*uphi[1]; // 1 @ node 93 (s = 2/4, t = 1/4, u = 1.0)
    phiTrue[94] = sphi[4]*tphi[3]*uphi[1]; // 1 @ node 94 (s = 3/4, t = 2/4, u = 1.0)
    phiTrue[95] = sphi[3]*tphi[4]*uphi[1]; // 1 @ node 95 (s = 2/4, t = 3/4, u = 1.0)
    phiTrue[96] = sphi[2]*tphi[3]*uphi[1]; // 1 @ node 96 (s = 1/4, t = 2/4, u = 1.0)
    phiTrue[97] = sphi[3]*tphi[3]*uphi[1]; // 1 @ node 97 (s = 2/4, t = 2/4, u = 1.0)

    // Cell
    phiTrue[98]  = sphi[2]*tphi[2]*uphi[2]; // 1 @ node 98  (s = 1/4, t = 1/4, u = 1/4)
    phiTrue[99]  = sphi[4]*tphi[2]*uphi[2]; // 1 @ node 99  (s = 3/4, t = 1/4, u = 1/4)
    phiTrue[100] = sphi[4]*tphi[4]*uphi[2]; // 1 @ node 100 (s = 3/4, t = 3/4, u = 1/4)
    phiTrue[101] = sphi[2]*tphi[4]*uphi[2]; // 1 @ node 101 (s = 1/4, t = 3/4, u = 1/4)

    phiTrue[102] = sphi[2]*tphi[2]*uphi[4]; // 1 @ node 102 (s = 1/4, t = 1/4, u = 3/4)
    phiTrue[103] = sphi[4]*tphi[2]*uphi[4]; // 1 @ node 103 (s = 3/4, t = 1/4, u = 3/4)
    phiTrue[104] = sphi[4]*tphi[4]*uphi[4]; // 1 @ node 104 (s = 3/4, t = 3/4, u = 3/4)
    phiTrue[105] = sphi[2]*tphi[4]*uphi[4]; // 1 @ node 105 (s = 1/4, t = 3/4, u = 3/4)

    phiTrue[106] = sphi[3]*tphi[2]*uphi[2]; // 1 @ node 106 (s = 2/4, t = 1/4, u = 1/4)
    phiTrue[107] = sphi[4]*tphi[3]*uphi[2]; // 1 @ node 107 (s = 3/4, t = 2/4, u = 1/4)
    phiTrue[108] = sphi[3]*tphi[4]*uphi[2]; // 1 @ node 108 (s = 2/4, t = 3/4, u = 1/4)
    phiTrue[109] = sphi[2]*tphi[3]*uphi[2]; // 1 @ node 109 (s = 1/4, t = 2/4, u = 1/4)
    phiTrue[110] = sphi[3]*tphi[3]*uphi[2]; // 1 @ node 110 (s = 2/4, t = 2/4, u = 1/4)

    phiTrue[111] = sphi[3]*tphi[2]*uphi[4]; // 1 @ node 111 (s = 2/4, t = 1/4, u = 3/4)
    phiTrue[112] = sphi[4]*tphi[3]*uphi[4]; // 1 @ node 112 (s = 3/4, t = 2/4, u = 3/4)
    phiTrue[113] = sphi[3]*tphi[4]*uphi[4]; // 1 @ node 113 (s = 2/4, t = 3/4, u = 3/4)
    phiTrue[114] = sphi[2]*tphi[3]*uphi[4]; // 1 @ node 114 (s = 1/4, t = 2/4, u = 3/4)
    phiTrue[115] = sphi[3]*tphi[3]*uphi[4]; // 1 @ node 115 (s = 2/4, t = 2/4, u = 3/4)

    phiTrue[116] = sphi[2]*tphi[2]*uphi[3]; // 1 @ node 116 (s = 1/4, t = 1/4, u = 2/4)
    phiTrue[117] = sphi[4]*tphi[2]*uphi[3]; // 1 @ node 117 (s = 3/4, t = 1/4, u = 2/4)
    phiTrue[118] = sphi[4]*tphi[4]*uphi[3]; // 1 @ node 118 (s = 3/4, t = 3/4, u = 2/4)
    phiTrue[119] = sphi[2]*tphi[4]*uphi[3]; // 1 @ node 119 (s = 1/4, t = 3/4, u = 2/4)
    phiTrue[120] = sphi[3]*tphi[2]*uphi[3]; // 1 @ node 120 (s = 2/4, t = 1/4, u = 2/4)
    phiTrue[121] = sphi[4]*tphi[3]*uphi[3]; // 1 @ node 121 (s = 3/4, t = 2/4, u = 2/4)
    phiTrue[122] = sphi[3]*tphi[4]*uphi[3]; // 1 @ node 122 (s = 2/4, t = 3/4, u = 2/4)
    phiTrue[123] = sphi[2]*tphi[3]*uphi[3]; // 1 @ node 123 (s = 1/4, t = 2/4, u = 2/4)
    phiTrue[124] = sphi[3]*tphi[3]*uphi[3]; // 1 @ node 124 (s = 2/4, t = 2/4, u = 2/4)

    //phis, phit, phiu ----------------------//
    BasisFunctionVolume<Hex,Lagrange,4>::self()->tensorProduct(sphis, tphi , uphi, phisTrue);
    BasisFunctionVolume<Hex,Lagrange,4>::self()->tensorProduct(sphi , tphit, uphi, phitTrue);
    BasisFunctionVolume<Hex,Lagrange,4>::self()->tensorProduct(sphi , tphi, uphiu, phiuTrue);

    //phiss, phist, phitt, phisu, phitu, phiuu -----//
    BasisFunctionVolume<Hex,Lagrange,4>::self()->tensorProduct(sphiss, tphi  , uphi  , phissTrue);
    BasisFunctionVolume<Hex,Lagrange,4>::self()->tensorProduct(sphis , tphit , uphi  , phistTrue);
    BasisFunctionVolume<Hex,Lagrange,4>::self()->tensorProduct(sphi  , tphitt, uphi  , phittTrue);
    BasisFunctionVolume<Hex,Lagrange,4>::self()->tensorProduct(sphis , tphi  , uphiu , phisuTrue);
    BasisFunctionVolume<Hex,Lagrange,4>::self()->tensorProduct(sphi  , tphit , uphiu , phituTrue);
    BasisFunctionVolume<Hex,Lagrange,4>::self()->tensorProduct(sphi  , tphi  , uphiuu, phiuuTrue);


    basis->evalBasis( s, t, u, facesign, phi, N );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, N );
    basis->evalBasisHessianDerivative( s, t, u, facesign, phiss, phist, phitt, phisu, phitu, phiuu, N );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
    }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis()), coord_u(basis->nBasis());
  BasisFunctionVolumeBase<Hex>::LagrangeP4->coordinates( coord_s, coord_t, coord_u );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n],coord_u[n], facesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }


  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*basis->order()+1;
  for (int k = 0; k < npoints; k++)
  {
    for (int j = 0; j < npoints; j++)
    {
      for (int i = 0; i < npoints; i++)
      {
        // Reference coordinates
        Real sTrue = i*1/Real(npoints+1);
        Real tTrue = j*1/Real(npoints+1);
        Real uTrue = k*1/Real(npoints+1);

        // Interpolated reference coordinate
        basis->evalBasis( sTrue, tTrue, uTrue, facesign, phi, basis->nBasis() );

        s = 0, t = 0, u =0;
        for (int n = 0; n < basis->nBasis(); n++)
        {
          s += phi[n]*coord_s[n];
          t += phi[n]*coord_t[n];
          u += phi[n]*coord_u[n];
        }

        // Interpolation should match
        SANS_CHECK_CLOSE( sTrue, s, small_tol, close_tol );
        SANS_CHECK_CLOSE( tTrue, t, small_tol, close_tol );
        SANS_CHECK_CLOSE( uTrue, u, small_tol, close_tol );
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange_pattern.txt", true );

  BasisFunctionVolumeBase<Hex>::getBasisFunction(1, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(2, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(3, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(4, BasisFunctionCategory_Lagrange)->dump( 2, output );
#if 0
  BasisFunctionVolumeBase<Hex>::getBasisFunction(5, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(6, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(7, BasisFunctionCategory_Lagrange)->dump( 2, output );
#endif

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
