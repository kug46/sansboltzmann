// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ReferenceElement_btest
// testing of reference element functions

#include <boost/test/unit_test.hpp>

#include "BasisFunction/ElementFrame.h"
#include "SANS_btest.h"

#include "tools/SANSException.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementFrame_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pentatope_test )
{
  const int (*FrameNodes)[ Pentatope::TopologyFrame::NNode ] = ElementFrame<Pentatope>::FrameNodes;

  BOOST_CHECK_EQUAL( 0, FrameNodes[0][0] ); BOOST_CHECK_EQUAL( 1, FrameNodes[0][1] ); BOOST_CHECK_EQUAL( 2, FrameNodes[0][2] ); // Frame 0
  BOOST_CHECK_EQUAL( 0, FrameNodes[1][0] ); BOOST_CHECK_EQUAL( 1, FrameNodes[1][1] ); BOOST_CHECK_EQUAL( 3, FrameNodes[1][2] ); // Frame 1
  BOOST_CHECK_EQUAL( 0, FrameNodes[2][0] ); BOOST_CHECK_EQUAL( 1, FrameNodes[2][1] ); BOOST_CHECK_EQUAL( 4, FrameNodes[2][2] ); // Frame 2
  BOOST_CHECK_EQUAL( 0, FrameNodes[3][0] ); BOOST_CHECK_EQUAL( 2, FrameNodes[3][1] ); BOOST_CHECK_EQUAL( 3, FrameNodes[3][2] ); // Frame 3
  BOOST_CHECK_EQUAL( 0, FrameNodes[4][0] ); BOOST_CHECK_EQUAL( 2, FrameNodes[4][1] ); BOOST_CHECK_EQUAL( 4, FrameNodes[4][2] ); // Frame 4
  BOOST_CHECK_EQUAL( 0, FrameNodes[5][0] ); BOOST_CHECK_EQUAL( 3, FrameNodes[5][1] ); BOOST_CHECK_EQUAL( 4, FrameNodes[5][2] ); // Frame 5
  BOOST_CHECK_EQUAL( 1, FrameNodes[6][0] ); BOOST_CHECK_EQUAL( 2, FrameNodes[6][1] ); BOOST_CHECK_EQUAL( 3, FrameNodes[6][2] ); // Frame 6
  BOOST_CHECK_EQUAL( 1, FrameNodes[7][0] ); BOOST_CHECK_EQUAL( 2, FrameNodes[7][1] ); BOOST_CHECK_EQUAL( 4, FrameNodes[7][2] ); // Frame 7
  BOOST_CHECK_EQUAL( 1, FrameNodes[8][0] ); BOOST_CHECK_EQUAL( 3, FrameNodes[8][1] ); BOOST_CHECK_EQUAL( 4, FrameNodes[8][2] ); // Frame 8
  BOOST_CHECK_EQUAL( 2, FrameNodes[9][0] ); BOOST_CHECK_EQUAL( 3, FrameNodes[9][1] ); BOOST_CHECK_EQUAL( 4, FrameNodes[9][2] ); // Frame 9


  const std::vector<int> cell = {20, 22, 23, 25, 24};
  for (int i = 0; i < Pentatope::NFrame; i++)
  {
    {
      const int frame0[Triangle::NNode] = {cell[FrameNodes[i][0]], cell[FrameNodes[i][1]], cell[FrameNodes[i][2]]};
      const int frame1[Triangle::NNode] = {cell[FrameNodes[i][1]], cell[FrameNodes[i][0]], cell[FrameNodes[i][2]]};
      const int frame2[Triangle::NNode] = {cell[FrameNodes[i][0]], cell[FrameNodes[i][2]], cell[FrameNodes[i][1]]};
      const int frame3[Triangle::NNode] = {cell[FrameNodes[i][2]], cell[FrameNodes[i][0]], cell[FrameNodes[i][1]]};
      const int frame4[Triangle::NNode] = {cell[FrameNodes[i][2]], cell[FrameNodes[i][1]], cell[FrameNodes[i][0]]};
      const int frame5[Triangle::NNode] = {cell[FrameNodes[i][1]], cell[FrameNodes[i][2]], cell[FrameNodes[i][0]]};

      BOOST_CHECK_EQUAL( i, ElementFrame<Pentatope>::getCanonicalFrame(frame0, Triangle::NNode, cell.data(), Pentatope::NNode) );
      BOOST_CHECK_EQUAL( i, ElementFrame<Pentatope>::getCanonicalFrame(frame1, Triangle::NNode, cell.data(), Pentatope::NNode) );
      BOOST_CHECK_EQUAL( i, ElementFrame<Pentatope>::getCanonicalFrame(frame2, Triangle::NNode, cell.data(), Pentatope::NNode) );
      BOOST_CHECK_EQUAL( i, ElementFrame<Pentatope>::getCanonicalFrame(frame3, Triangle::NNode, cell.data(), Pentatope::NNode) );
      BOOST_CHECK_EQUAL( i, ElementFrame<Pentatope>::getCanonicalFrame(frame4, Triangle::NNode, cell.data(), Pentatope::NNode) );
      BOOST_CHECK_EQUAL( i, ElementFrame<Pentatope>::getCanonicalFrame(frame5, Triangle::NNode, cell.data(), Pentatope::NNode) );

    }
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
