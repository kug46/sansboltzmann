// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"   // Triangle
#include "BasisFunction/TraceToCellRefCoord.h"


using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( TraceToCellRefCoord_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tetrahedron_eval_test )
{

  const Real tol = 1e-13;
  Real sRefArea, tRefArea, sRefVol, tRefVol, uRefVol, sRefVolTrue, tRefVolTrue, uRefVolTrue;
  CanonicalTraceToCell canonicalFace(0,0);

  //===================================
  canonicalFace.orientation = 1;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 1-s; tRefVolTrue = s; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 1-t; tRefVolTrue = 0; uRefVolTrue = t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 1;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = 0; uRefVolTrue = s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = t; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 0; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 2;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = s; tRefVolTrue = 0; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = 0; uRefVolTrue = t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 0; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 3;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = s; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = t; tRefVolTrue = 0; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 0;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //===================================
  canonicalFace.orientation = 2;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = 1-s; uRefVolTrue = s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = t; tRefVolTrue = 1-t; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 1;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = s; uRefVolTrue = 1-s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = 0; uRefVolTrue = 1-t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 0; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 2;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 1-s; tRefVolTrue = 0; uRefVolTrue = s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 1-t; tRefVolTrue = 0; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 0; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 3;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = s; tRefVolTrue = 1-s; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = 1-t; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 0;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //===================================
  canonicalFace.orientation = 3;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = s; tRefVolTrue = 0; uRefVolTrue = 1-s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = t; uRefVolTrue = 1-t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 1;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = 1-s; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = 1-t; uRefVolTrue = t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 0; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 2;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = 0; uRefVolTrue = 1-s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = t; tRefVolTrue = 0; uRefVolTrue = 1-t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 0; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 3;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 1-s; tRefVolTrue = 0; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 1-t; tRefVolTrue = t; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 0;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //===================================
  canonicalFace.orientation = -1;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 1-s; tRefVolTrue = 0; uRefVolTrue = s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 1-t; tRefVolTrue = t; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 1;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = s; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = 0; uRefVolTrue = t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 0; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 2;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = 0; uRefVolTrue = s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = t; tRefVolTrue = 0; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 0; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 3;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = s; tRefVolTrue = 0; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = t; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 0;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //===================================
  canonicalFace.orientation = -2;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = s; tRefVolTrue = 1-s; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = 1-t; uRefVolTrue = t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 1;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = 0; uRefVolTrue = 1-s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = t; uRefVolTrue = 1-t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 0; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 2;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 1-s; tRefVolTrue = 0; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 1-t; tRefVolTrue = 0; uRefVolTrue = t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 0; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 3;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = 1-s; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = t; tRefVolTrue = 1-t; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 0;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //===================================
  canonicalFace.orientation = -3;
  //===================================

  //------------
  canonicalFace.trace = 0;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = s; uRefVolTrue = 1-s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = t; tRefVolTrue = 0; uRefVolTrue = 1-t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 1;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 0; tRefVolTrue = 1-s; uRefVolTrue = s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = 1-t; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 0; tRefVolTrue = 1./3.; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 2;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = s; tRefVolTrue = 0; uRefVolTrue = 1-s;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 0; tRefVolTrue = 0; uRefVolTrue = 1-t;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 0; uRefVolTrue = 1./3.;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );



  //------------
  canonicalFace.trace = 3;
  //------------

  for ( Real s = 0.0; s <= 1.0; s += 0.25 )
  {
    sRefArea = s;
    tRefArea = 0;
    sRefVolTrue = 1-s; tRefVolTrue = s; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  for ( Real t = 0.0; t <= 1.0; t += 0.25 )
  {
    sRefArea = 0;
    tRefArea = t;
    sRefVolTrue = 1-t; tRefVolTrue = 0; uRefVolTrue = 0;

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
    BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
    BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
    BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );
  }

  sRefArea = 1./3.;
  tRefArea = 1./3.;
  sRefVolTrue = 1./3.; tRefVolTrue = 1./3.; uRefVolTrue = 0;

  TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol );
  BOOST_CHECK_CLOSE( sRefVolTrue, sRefVol, tol );
  BOOST_CHECK_CLOSE( tRefVolTrue, tRefVol, tol );
  BOOST_CHECK_CLOSE( uRefVolTrue, uRefVol, tol );


  typedef TraceToCellRefCoord<Triangle, TopoD3, Tet> TraceToCellRefCoordTriangleTet;

  canonicalFace.orientation = 1;
  canonicalFace.trace = 4;
  BOOST_CHECK_THROW( TraceToCellRefCoordTriangleTet::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );

  canonicalFace.trace = -4;
  BOOST_CHECK_THROW( TraceToCellRefCoordTriangleTet::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );

  canonicalFace.trace = -1;
  BOOST_CHECK_THROW( TraceToCellRefCoordTriangleTet::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );


  canonicalFace.trace = 0;

  canonicalFace.orientation = 0;
  BOOST_CHECK_THROW( TraceToCellRefCoordTriangleTet::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );

  canonicalFace.orientation = 4;
  BOOST_CHECK_THROW( TraceToCellRefCoordTriangleTet::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );

  canonicalFace.orientation = -4;
  BOOST_CHECK_THROW( TraceToCellRefCoordTriangleTet::eval( canonicalFace, sRefArea, tRefArea, sRefVol, tRefVol, uRefVol ), DeveloperException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tetrahedron_Orientation_test )
{

  const int tet[4] = {0, 1, 2, 3};

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  const int (*OrientPos)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientPos;
  const int (*OrientNeg)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientNeg;

  CanonicalTraceToCell canonicalFace;

  for (int face = 0; face < 4; face++ )
  {
    // Orientation is 1 for all triangles that match the canonical face
    const int tri[3] = {tet[TraceNodes[face][OrientPos[0][0]]],
                        tet[TraceNodes[face][OrientPos[0][1]]],
                        tet[TraceNodes[face][OrientPos[0][2]]]};

    canonicalFace = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(tri, 3, tet, 4);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 1 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }

  for (int face = 0; face < 4; face++ )
  {
    // Reverse the last two nodes results in a -1 orientation
    const int tri[3] = {tet[TraceNodes[face][OrientNeg[0][0]]],
                        tet[TraceNodes[face][OrientNeg[0][1]]],
                        tet[TraceNodes[face][OrientNeg[0][2]]]};

    canonicalFace = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(tri, 3, tet, 4);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, -1 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }



  for (int face = 0; face < 4; face++ )
  {
    // Revolve the canonical nodes once is orientation 2
    const int tri[3] = {tet[TraceNodes[face][OrientPos[1][0]]],
                        tet[TraceNodes[face][OrientPos[1][1]]],
                        tet[TraceNodes[face][OrientPos[1][2]]]};

    canonicalFace = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(tri, 3, tet, 4);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 2 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }

  for (int face = 0; face < 4; face++ )
  {
    // Reverse the last two nodes results in a -2 orientation
    const int tri[3] = {tet[TraceNodes[face][OrientNeg[1][0]]],
                        tet[TraceNodes[face][OrientNeg[1][1]]],
                        tet[TraceNodes[face][OrientNeg[1][2]]]};

    canonicalFace = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(tri, 3, tet, 4);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, -2 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }



  for (int face = 0; face < 4; face++ )
  {
    // Revolve the canonical nodes twice is orientation 3
    const int tri[3] = {tet[TraceNodes[face][OrientPos[2][0]]],
                        tet[TraceNodes[face][OrientPos[2][1]]],
                        tet[TraceNodes[face][OrientPos[2][2]]]};

    canonicalFace = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(tri, 3, tet, 4);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, 3 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }

  for (int face = 0; face < 4; face++ )
  {
    // Reverse the last two nodes results in a -3 orientation
    const int tri[3] = {tet[TraceNodes[face][OrientNeg[2][0]]],
                        tet[TraceNodes[face][OrientNeg[2][1]]],
                        tet[TraceNodes[face][OrientNeg[2][2]]]};

    canonicalFace = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(tri, 3, tet, 4);
    BOOST_CHECK_EQUAL( canonicalFace.orientation, -3 );
    BOOST_CHECK_EQUAL( canonicalFace.trace, face );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tetrahedron_TraceEdgeMap_test )
{

  const int (*TraceEdges)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceEdges;

  CanonicalTraceToCell canonicalFace;

  for (int face = 0; face < Tet::NFace; face++ )
  {
    canonicalFace.trace = face;

    for (int edge = 0; edge < Triangle::NEdge; edge++ )
    {
      canonicalFace.orientation = 1;
      int cell_edge = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCellEdge(canonicalFace, edge);
      BOOST_CHECK_EQUAL( cell_edge, TraceEdges[face][edge] );

      canonicalFace.orientation = 2;
      cell_edge = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCellEdge(canonicalFace, edge);
      BOOST_CHECK_EQUAL( cell_edge, TraceEdges[face][(edge+1)%3] );

      canonicalFace.orientation = 3;
      cell_edge = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCellEdge(canonicalFace, edge);
      BOOST_CHECK_EQUAL( cell_edge, TraceEdges[face][(edge+2)%3] );

      canonicalFace.orientation = -1;
      cell_edge = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCellEdge(canonicalFace, edge);
      BOOST_CHECK_EQUAL( cell_edge, TraceEdges[face][(3-edge)%3] );

      canonicalFace.orientation = -2;
      cell_edge = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCellEdge(canonicalFace, edge);
      BOOST_CHECK_EQUAL( cell_edge, TraceEdges[face][(4-edge)%3] );

      canonicalFace.orientation = -3;
      cell_edge = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCellEdge(canonicalFace, edge);
      BOOST_CHECK_EQUAL( cell_edge, TraceEdges[face][(5-edge)%3] );
    }
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
