// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionNode_btest
// testing of BasisFunctionNode class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionNode.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionNode_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( BasisFunctionNodeBase::D == 0 );

  BOOST_CHECK( Node::TopoDim::D == topoDim(eNode) );
  BOOST_CHECK( Node::NNode == topoNNode(eNode) );

  BOOST_CHECK( BasisFunctionNode_PMax == 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionNodeBase::getBasisFunction( 0, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionNodeBase::getBasisFunction( 8, BasisFunctionCategory_Hierarchical), DeveloperException );

  BOOST_CHECK_THROW( BasisFunctionNodeBase::getBasisFunction(BasisFunctionNode_PMax-1, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionNodeBase::getBasisFunction(BasisFunctionNode_PMax+1, BasisFunctionCategory_Legendre), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_P0 )
{
  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionNodeBase::getBasisFunction(0, BasisFunctionCategory_Legendre),
                     BasisFunctionNodeBase::self );

  const BasisFunctionNodeBase* basis = BasisFunctionNodeBase::self;

  BOOST_CHECK_EQUAL( 0, basis->order() );
  BOOST_CHECK_EQUAL( 1, basis->nBasis() );
  BOOST_CHECK_EQUAL( 1, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real phi[1], phis[1], phiss[1];
  Real phiTrue[1], phisTrue[1], phissTrue[1];

  phiTrue[0] = 1;
  phisTrue[0] = 0;
  phissTrue[0] = 0;

  basis->evalBasis( phi, 1 );
  basis->evalBasisDerivative( phis, 1 );
  basis->evalBasisHessianDerivative( phiss, 1 );
  BOOST_CHECK_CLOSE( phiTrue[0], phi[0], tol );
  BOOST_CHECK_CLOSE( phisTrue[0], phis[0], tol );
  BOOST_CHECK_CLOSE( phissTrue[0], phiss[0], tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionNode_pattern.txt", true );

  BasisFunctionNodeBase::getBasisFunction(0, BasisFunctionCategory_Legendre)->dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
