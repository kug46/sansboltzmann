// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionVolume_Tetranhedron_Lagrange_btest
// testing of BasisFunctionVolume<Tet,Lagrange> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionSpacetime.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope_Lagrange.h"
#include "BasisFunction/LagrangeDOFMap.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionVolume_Pentatope_Lagrange_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{

}

//----------------------------------------------------------------------------//
Real
func_P1( const std::vector<Real>& s , Real* C )
{
  return C[0] +C[1]*s[0] +C[2]*s[1] +C[3]*s[2] +C[4]*s[3];
}

BOOST_AUTO_TEST_CASE( basis_LagrangeP1 )
{

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  typedef std::array<int,5> Int5;
  const int nBasis = 5;
  const Int5 sign = {{1,1,1,1,1}};
  const BasisFunctionSpacetime<Pentatope, Lagrange, 1>* basis =
                  BasisFunctionSpacetime<Pentatope, Lagrange, 1>::self();

  BOOST_CHECK_EQUAL( basis->order() , 1);
  BOOST_CHECK_EQUAL( basis->nBasis() , 5 );
  BOOST_CHECK_EQUAL( basis->nBasisNode() , 5 );
  BOOST_CHECK_EQUAL( basis->nBasisEdge() , 0 );
  BOOST_CHECK_EQUAL( basis->nBasisArea() , 0 );
  BOOST_CHECK_EQUAL( basis->nBasisFace() , 0 );
  BOOST_CHECK_EQUAL( basis->nBasisCell() , 0 );

  Real s,t,u,v;

  // interpolate a linear function
  Real C[5] = { 1.2 , 2.3 , 9.8 , 4.56 , 6.039 };

  // evaluate the function at the lattice values
  std::vector<Real> sref,tref,uref,vref;
  basis->coordinates(sref,tref,uref,vref);

  // evaluate the linear function at each value
  std::vector<Real> f0( nBasis );
  for (int j=0;j<nBasis;j++)
    f0[j] = func_P1( {sref[j],tref[j],uref[j],vref[j]} , C );

  // test 1
  {
    // evaluation point
    s = 0.2;
    t = 0.2;
    u = 0.2;
    v = 0.2;
    Real phi[5];
    basis->evalBasis( s,t,u,v , sign , phi , nBasis );

    Real fb = 0.0;
    for (int j=0;j<nBasis;j++)
      fb += phi[j]*f0[j];

    // not evaluate at s,t,u,v
    Real f1 = func_P1( {s,t,u,v} , C );
    SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

    // the gradient should be equal to C
    Real phis[5],phit[5],phiu[5],phiv[5];
    basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
    Real gb[4] = {0,0,0,0};
    for (int j=0;j<nBasis;j++)
    {
      gb[0] += f0[j]*phis[j];
      gb[1] += f0[j]*phit[j];
      gb[2] += f0[j]*phiu[j];
      gb[3] += f0[j]*phiv[j];
    }

    SANS_CHECK_CLOSE( gb[0] , C[1] , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[1] , C[2] , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[2] , C[3] , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[3] , C[4] , small_tol , close_tol );

    // the hessian should be equal to 0
    Real phiss[5],phist[5],phitt[5],phisu[5],phitu[5],phiuu[5],phisv[5],phitv[5],phiuv[5],phivv[5];
    basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);
    for (int i=0;i<nBasis;i++)
    {
      SANS_CHECK_CLOSE( phiss[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phist[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phitt[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phisu[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phitu[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phiuu[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phisv[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phitv[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phiuv[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phivv[i] , 0. , small_tol , close_tol );
    }

  } // end test 1

  // test 2
  {
    // evaluation point
    s = 0.3819384;
    t = 0.112948;
    u = 0.8732;
    v = 0.10384;
    Real phi[5];
    basis->evalBasis( s,t,u,v , sign , phi , nBasis );

    Real fb = 0.0;
    for (int j=0;j<nBasis;j++)
      fb += phi[j]*f0[j];

    // not evaluate at s,t,u,v
    Real f1 = func_P1( {s,t,u,v} , C );
    SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

    // the gradient should be equal to C
    Real phis[5],phit[5],phiu[5],phiv[5];
    basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
    Real gb[4] = {0,0,0,0};
    for (int j=0;j<nBasis;j++)
    {
      gb[0] += f0[j]*phis[j];
      gb[1] += f0[j]*phit[j];
      gb[2] += f0[j]*phiu[j];
      gb[3] += f0[j]*phiv[j];
    }

    SANS_CHECK_CLOSE( gb[0] , C[1] , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[1] , C[2] , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[2] , C[3] , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[3] , C[4] , small_tol , close_tol );

    // the hessian should be equal to 0
    Real phiss[5],phist[5],phitt[5],phisu[5],phitu[5],phiuu[5],phisv[5],phitv[5],phiuv[5],phivv[5];
    basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);
    for (int i=0;i<nBasis;i++)
    {
      SANS_CHECK_CLOSE( phiss[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phist[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phitt[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phisu[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phitu[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phiuu[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phisv[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phitv[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phiuv[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phivv[i] , 0. , small_tol , close_tol );
    }

  } // end test 2

  // test 3
  {
    // evaluation point
    s = 0.148224;
    t = 0.48;
    u = 0.4738;
    v = 0.12;
    Real phi[5];
    basis->evalBasis( s,t,u,v , sign , phi , nBasis );

    Real fb = 0.0;
    for (int j=0;j<nBasis;j++)
      fb += phi[j]*f0[j];

    // not evaluate at s,t,u,v
    Real f1 = func_P1( {s,t,u,v} , C );
    SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

    // the gradient should be equal to C
    Real phis[5],phit[5],phiu[5],phiv[5];
    basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
    Real gb[4] = {0,0,0,0};
    for (int j=0;j<nBasis;j++)
    {
      gb[0] += f0[j]*phis[j];
      gb[1] += f0[j]*phit[j];
      gb[2] += f0[j]*phiu[j];
      gb[3] += f0[j]*phiv[j];
    }

    SANS_CHECK_CLOSE( gb[0] , C[1] , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[1] , C[2] , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[2] , C[3] , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[3] , C[4] , small_tol , close_tol );

    // the hessian should be equal to 0
    Real phiss[5],phist[5],phitt[5],phisu[5],phitu[5],phiuu[5],phisv[5],phitv[5],phiuv[5],phivv[5];
    basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);
    for (int i=0;i<nBasis;i++)
    {
      SANS_CHECK_CLOSE( phiss[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phist[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phitt[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phisu[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phitu[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phiuu[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phisv[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phitv[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phiuv[i] , 0. , small_tol , close_tol );
      SANS_CHECK_CLOSE( phivv[i] , 0. , small_tol , close_tol );
    }

  } // end test 3

  // get the lagrange coordinate where the basis functions should all be 1
  {
    std::vector<Real> coord_s(nBasis);
    std::vector<Real> coord_t(nBasis);
    std::vector<Real> coord_u(nBasis);
    std::vector<Real> coord_v(nBasis);

    Real phi[nBasis];

    BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1->coordinates(coord_s, coord_t, coord_u, coord_v);

    for (int n= 0; n < nBasis; n++)
    {
      basis->evalBasis(coord_s[n], coord_t[n], coord_u[n], coord_v[n], sign, phi, nBasis);

      for (int ki= 0; ki < basis->nBasis(); ki++)
      {
        if (ki == n)
          BOOST_CHECK_CLOSE(1, phi[ki], close_tol);
        else
          BOOST_CHECK_SMALL(phi[ki], small_tol);
      }
    }

  }

}

Real
func_P2( Real s , Real t , Real u , Real v , Real* C )
{
  return C[0] +C[1]*s*s +C[2]*s*t +C[3]*s*u +C[4]*s*v +C[5]*t*t +C[6]*t*u +C[7]*t*v +C[8]*u*u +C[9]*u*v +C[10]*v*v;
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP2 )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  typedef std::array<int,5> Int5;
  const int nBasis = 15;
  const Int5 sign = {{1,1,1,1,1}};
  const BasisFunctionSpacetime<Pentatope, Lagrange, 2>* basis =
                  BasisFunctionSpacetime<Pentatope, Lagrange, 2>::self();

  BOOST_CHECK_EQUAL( basis->order() , 2);
  BOOST_CHECK_EQUAL( basis->nBasis() , 15 );
  BOOST_CHECK_EQUAL( basis->nBasisNode() , 5 );
  BOOST_CHECK_EQUAL( basis->nBasisEdge() , 10 );
  BOOST_CHECK_EQUAL( basis->nBasisArea() , 0 );
  BOOST_CHECK_EQUAL( basis->nBasisFace() , 0 );
  BOOST_CHECK_EQUAL( basis->nBasisCell() , 0 );


  Real s,t,u,v;

  // interpolate a linear function
  Real C[11] = { 1.2 , 2.3 , 9.8 , 4.56 , 6.039 , 3.2332 , 1.2938 , 9.1193 , 114.3293 , 0.111 , 1.2334 };

  // evaluate the function at the lattice values
  std::vector<Real> sref,tref,uref,vref;
  basis->coordinates(sref,tref,uref,vref);

  // evaluate the linear function at each value
  std::vector<Real> f0( nBasis );
  for (int j=0;j<nBasis;j++)
    f0[j] = func_P2( sref[j],tref[j],uref[j],vref[j] , C );

  // test 1
  {
    s = 0.25748;
    t = 0.000133;
    u = 0.12343;
    v = 0.938489;
    Real phi[15];
    basis->evalBasis( s,t,u,v , sign , phi , nBasis );

    Real fb = 0.0;
    for (int j=0;j<nBasis;j++)
      fb += phi[j]*f0[j];

    // evaluate at s,t,u,v
    Real f1 = func_P2( s,t,u,v , C );
    SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

    // check the gradient
    Real phis[15],phit[15],phiu[15],phiv[15];
    basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
    Real gb[4] = {0,0,0,0};
    for (int j=0;j<nBasis;j++)
    {
      gb[0] += f0[j]*phis[j];
      gb[1] += f0[j]*phit[j];
      gb[2] += f0[j]*phiu[j];
      gb[3] += f0[j]*phiv[j];
    }
    SANS_CHECK_CLOSE( gb[0] , 2*C[1]*s +C[2]*t +C[3]*u +C[4]*v  , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[1] , C[2]*s +2*C[5]*t +C[6]*u +C[7]*v  , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[2] , C[3]*s +C[6]*t +2*C[8]*u +C[9]*v  , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[3] , C[4]*s +C[7]*t +C[9]*u +2*C[10]*v , small_tol , close_tol );

    // check the hessian
    Real phiss[15],phist[15],phitt[15],phisu[15],phitu[15],phiuu[15],phisv[15],phitv[15],phiuv[15],phivv[15];
    basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);
    Real H[4][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };
    for (int i=0;i<nBasis;i++)
    {
      H[0][0] += phiss[i]*f0[i];
      H[0][1] += phist[i]*f0[i];
      H[0][2] += phisu[i]*f0[i];
      H[0][3] += phisv[i]*f0[i];

      H[1][0] = H[0][1];
      H[1][1] += phitt[i]*f0[i];
      H[1][2] += phitu[i]*f0[i];
      H[1][3] += phitv[i]*f0[i];

      H[2][0] = H[0][2];
      H[2][1] = H[1][2];
      H[2][2] += phiuu[i]*f0[i];
      H[2][3] += phiuv[i]*f0[i];

      H[3][0] = H[0][3];
      H[3][1] = H[1][3];
      H[3][2] = H[2][3];
      H[3][3] += phivv[i]*f0[i];
    }
    SANS_CHECK_CLOSE( H[0][0] , 2*C[1] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][1] , C[2] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][2] , C[3] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][3] , C[4] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][1] , 2*C[5] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][2] , C[6], small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][3] , C[7] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][2] , 2*C[8] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][3] , C[9] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[3][3] , 2*C[10] , small_tol , close_tol );

  } // end test 1

  // test 2
  {
    s = 0.0139283213;
    t = 0.8;
    u = 1.0;
    v = 0.324;
    Real phi[15];
    basis->evalBasis( s,t,u,v , sign , phi , nBasis );

    Real fb = 0.0;
    for (int j=0;j<nBasis;j++)
      fb += phi[j]*f0[j];

    // evaluate at s,t,u,v
    Real f1 = func_P2( s,t,u,v , C );
    SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

    // check the gradient
    Real phis[15],phit[15],phiu[15],phiv[15];
    basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
    Real gb[4] = {0,0,0,0};
    for (int j=0;j<nBasis;j++)
    {
      gb[0] += f0[j]*phis[j];
      gb[1] += f0[j]*phit[j];
      gb[2] += f0[j]*phiu[j];
      gb[3] += f0[j]*phiv[j];
    }
    SANS_CHECK_CLOSE( gb[0] , 2*C[1]*s +C[2]*t +C[3]*u +C[4]*v  , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[1] , C[2]*s +2*C[5]*t +C[6]*u +C[7]*v  , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[2] , C[3]*s +C[6]*t +2*C[8]*u +C[9]*v  , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[3] , C[4]*s +C[7]*t +C[9]*u +2*C[10]*v , small_tol , close_tol );

    // check the hessian
    Real phiss[15],phist[15],phitt[15],phisu[15],phitu[15],phiuu[15],phisv[15],phitv[15],phiuv[15],phivv[15];
    basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);
    Real H[4][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };
    for (int i=0;i<nBasis;i++)
    {
      H[0][0] += phiss[i]*f0[i];
      H[0][1] += phist[i]*f0[i];
      H[0][2] += phisu[i]*f0[i];
      H[0][3] += phisv[i]*f0[i];

      H[1][0] = H[0][1];
      H[1][1] += phitt[i]*f0[i];
      H[1][2] += phitu[i]*f0[i];
      H[1][3] += phitv[i]*f0[i];

      H[2][0] = H[0][2];
      H[2][1] = H[1][2];
      H[2][2] += phiuu[i]*f0[i];
      H[2][3] += phiuv[i]*f0[i];

      H[3][0] = H[0][3];
      H[3][1] = H[1][3];
      H[3][2] = H[2][3];
      H[3][3] += phivv[i]*f0[i];
    }
    SANS_CHECK_CLOSE( H[0][0] , 2*C[1] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][1] , C[2] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][2] , C[3] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][3] , C[4] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][1] , 2*C[5] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][2] , C[6], small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][3] , C[7] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][2] , 2*C[8] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][3] , C[9] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[3][3] , 2*C[10] , small_tol , close_tol );

  } // end test 2

  // test 3
  {
    s = 0.00001;
    t = 0.898492;
    u = 0.193892;
    v = 0.222222;
    Real phi[15];
    basis->evalBasis( s,t,u,v , sign , phi , nBasis );

    Real fb = 0.0;
    for (int j=0;j<nBasis;j++)
      fb += phi[j]*f0[j];

    // evaluate at s,t,u,v
    Real f1 = func_P2( s,t,u,v , C );
    SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

    // check the gradient
    Real phis[15],phit[15],phiu[15],phiv[15];
    basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
    Real gb[4] = {0,0,0,0};
    for (int j=0;j<nBasis;j++)
    {
      gb[0] += f0[j]*phis[j];
      gb[1] += f0[j]*phit[j];
      gb[2] += f0[j]*phiu[j];
      gb[3] += f0[j]*phiv[j];
    }
    SANS_CHECK_CLOSE( gb[0] , 2*C[1]*s +C[2]*t +C[3]*u +C[4]*v  , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[1] , C[2]*s +2*C[5]*t +C[6]*u +C[7]*v  , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[2] , C[3]*s +C[6]*t +2*C[8]*u +C[9]*v  , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[3] , C[4]*s +C[7]*t +C[9]*u +2*C[10]*v , small_tol , close_tol );

    // check the hessian
    Real phiss[15],phist[15],phitt[15],phisu[15],phitu[15],phiuu[15],phisv[15],phitv[15],phiuv[15],phivv[15];
    basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);
    Real H[4][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };
    for (int i=0;i<nBasis;i++)
    {
      H[0][0] += phiss[i]*f0[i];
      H[0][1] += phist[i]*f0[i];
      H[0][2] += phisu[i]*f0[i];
      H[0][3] += phisv[i]*f0[i];

      H[1][0] = H[0][1];
      H[1][1] += phitt[i]*f0[i];
      H[1][2] += phitu[i]*f0[i];
      H[1][3] += phitv[i]*f0[i];

      H[2][0] = H[0][2];
      H[2][1] = H[1][2];
      H[2][2] += phiuu[i]*f0[i];
      H[2][3] += phiuv[i]*f0[i];

      H[3][0] = H[0][3];
      H[3][1] = H[1][3];
      H[3][2] = H[2][3];
      H[3][3] += phivv[i]*f0[i];
    }
    SANS_CHECK_CLOSE( H[0][0] , 2*C[1] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][1] , C[2] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][2] , C[3] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][3] , C[4] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][1] , 2*C[5] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][2] , C[6], small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][3] , C[7] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][2] , 2*C[8] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][3] , C[9] , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[3][3] , 2*C[10] , small_tol , close_tol );

  } // end test 3

  // get the lagrange coordinate where the basis functions should all be 1
  {
    std::vector<Real> coord_s(nBasis);
    std::vector<Real> coord_t(nBasis);
    std::vector<Real> coord_u(nBasis);
    std::vector<Real> coord_v(nBasis);

    Real phi[nBasis];

    BasisFunctionSpacetimeBase<Pentatope>::LagrangeP2->coordinates(coord_s, coord_t, coord_u, coord_v);

    for (int n= 0; n < nBasis; n++)
    {
      basis->evalBasis(coord_s[n], coord_t[n], coord_u[n], coord_v[n], sign, phi, nBasis);

      for (int ki= 0; ki < basis->nBasis(); ki++)
      {
        if (ki == n)
          BOOST_CHECK_CLOSE(1, phi[ki], close_tol);
        else
          BOOST_CHECK_SMALL(phi[ki], small_tol);
      }
    }

  }

}

Real
func_P3( Real s , Real t , Real u , Real v , Real* C )
{
  return C[0]
        +C[1]*s*s*s +C[2]*s*s*t +C[3]*s*s*u +C[4]*s*s*v
        +C[5]*t*t*t +C[6]*t*t*s +C[7]*t*t*u +C[8]*t*t*v
        +C[9]*u*u*u +C[10]*u*u*s +C[11]*u*u*t +C[12]*u*u*v
        +C[13]*v*v*v +C[14]*v*v*s +C[15]*v*v*t +C[16]*v*v*u
        +C[17]*s*t*u +C[18]*t*u*v +C[19]*s*t*v +C[20]*s*u*v;
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP3 )
{

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  typedef std::array<int,5> Int5;
  const int nBasis = 35;
  const Int5 sign = {{1,1,1,1,1}};
  const BasisFunctionSpacetime<Pentatope, Lagrange, 3>* basis=
                  BasisFunctionSpacetime<Pentatope, Lagrange, 3>::self();

  BOOST_CHECK_EQUAL( basis->order() , 3 );
  BOOST_CHECK_EQUAL( basis->nBasis() , 35 );
  BOOST_CHECK_EQUAL( basis->nBasisNode() , 5 );
  BOOST_CHECK_EQUAL( basis->nBasisEdge() , 20 );
  BOOST_CHECK_EQUAL( basis->nBasisArea() , 10 );
  BOOST_CHECK_EQUAL( basis->nBasisFace() , 0 );
  BOOST_CHECK_EQUAL( basis->nBasisCell() , 0 );

  Real s,t,u,v;

  // interpolate a cubic function
  Real C[21] = { 1.2 , 2.3 , 9.8 , 4.56 , 6.039 , 3.2332 , 1.2938 , 9.1193 , 114.3293 , 0.111 , 1.2334 ,
                 0.9 , .783 , .423 , .03 , 9.819 , 1.414 , 0.707 , 9.888 , 12.3 , .142};

  // evaluate the function at the lattice values
  std::vector<Real> sref,tref,uref,vref;
  basis->coordinates(sref,tref,uref,vref);

  // evaluate the linear function at each value
  std::vector<Real> f0( nBasis );
  for (int j=0;j<nBasis;j++)
    f0[j] = func_P3( sref[j],tref[j],uref[j],vref[j] , C );

  // get the lagrange coordinate where the basis functions should all be 1
  {
    std::vector<Real> coord_s(nBasis);
    std::vector<Real> coord_t(nBasis);
    std::vector<Real> coord_u(nBasis);
    std::vector<Real> coord_v(nBasis);

    Real phi[nBasis];

    BasisFunctionSpacetimeBase<Pentatope>::LagrangeP3->coordinates(coord_s, coord_t, coord_u, coord_v);

    BOOST_CHECK_EQUAL(nBasis, coord_s.size());
    BOOST_CHECK_EQUAL(nBasis, coord_t.size());
    BOOST_CHECK_EQUAL(nBasis, coord_u.size());
    BOOST_CHECK_EQUAL(nBasis, coord_v.size());

    for (int n= 0; n < nBasis; n++)
    {
      basis->evalBasis(coord_s[n], coord_t[n], coord_u[n], coord_v[n], sign, phi, nBasis);

      for (int ki= 0; ki < basis->nBasis(); ki++)
      {
        if (ki == n)
          BOOST_CHECK_CLOSE(1, phi[ki], close_tol);
        else
          BOOST_CHECK_SMALL(phi[ki], small_tol);
      }
    }

  }

 // test 1
 {
   s = 0.25748;
   t = 0.000133;
   u = 0.12343;
   v = 0.938489;
   Real phi[35];
   basis->evalBasis( s,t,u,v , sign , phi , nBasis );

   Real fb = 0.0;
   for (int j=0;j<nBasis;j++)
     fb += phi[j]*f0[j];

   // evaluate at s,t,u,v
   Real f1 = func_P3( s,t,u,v , C );
   SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

   // check the gradient
   Real phis[35],phit[35],phiu[35],phiv[35];
   basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
   Real gb[4] = {0,0,0,0};
   for (int j=0;j<nBasis;j++)
   {
     gb[0] += f0[j]*phis[j];
     gb[1] += f0[j]*phit[j];
     gb[2] += f0[j]*phiu[j];
     gb[3] += f0[j]*phiv[j];
   }
   SANS_CHECK_CLOSE( gb[0] , 3*C[1]*s*s +2*C[2]*s*t +2*C[3]*s*u +2*C[4]*s*v +C[6]*t*t
                             +C[10]*u*u +C[14]*v*v +C[17]*t*u +C[19]*t*v +C[20]*u*v  , small_tol , close_tol );
   SANS_CHECK_CLOSE( gb[1] , C[2]*s*s +3*C[5]*t*t +2*C[6]*s*t +2*C[7]*t*u +2*C[8]*t*v
                             +C[11]*u*u +C[15]*v*v +C[17]*s*u +C[18]*u*v +C[19]*s*v, small_tol , close_tol );
   SANS_CHECK_CLOSE( gb[2] , C[3]*s*s +C[7]*t*t +3*C[9]*u*u +2*C[10]*u*s +2*C[11]*u*t
                             +2*C[12]*u*v +C[16]*v*v +C[17]*s*t +C[18]*t*v +C[20]*s*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( gb[3] , C[4]*s*s +C[8]*t*t +C[12]*u*u +3*C[13]*v*v +2*C[14]*s*v
                             +2*C[15]*t*v +2*C[16]*u*v +C[18]*t*u +C[19]*s*t +C[20]*s*u  , small_tol , close_tol );

   // check the hessian
   Real phiss[35],phist[35],phitt[35],phisu[35],phitu[35],phiuu[35],phisv[35],phitv[35],phiuv[35],phivv[35];
   basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);
   Real H[4][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };
   for (int i=0;i<nBasis;i++)
   {
     H[0][0] += phiss[i]*f0[i];
     H[0][1] += phist[i]*f0[i];
     H[0][2] += phisu[i]*f0[i];
     H[0][3] += phisv[i]*f0[i];

     H[1][0] = H[0][1];
     H[1][1] += phitt[i]*f0[i];
     H[1][2] += phitu[i]*f0[i];
     H[1][3] += phitv[i]*f0[i];

     H[2][0] = H[0][2];
     H[2][1] = H[1][2];
     H[2][2] += phiuu[i]*f0[i];
     H[2][3] += phiuv[i]*f0[i];

     H[3][0] = H[0][3];
     H[3][1] = H[1][3];
     H[3][2] = H[2][3];
     H[3][3] += phivv[i]*f0[i];
   }
   SANS_CHECK_CLOSE( H[0][0] , 6*C[1]*s +2*C[2]*t +2*C[3]*u +2*C[4]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[0][1] , 2*C[2]*s +2*C[6]*t +C[17]*u +C[19]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[0][2] , 2*C[3]*s +2*C[10]*u +C[17]*t +C[20]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[0][3] , 2*C[4]*s +2*C[14]*v +C[19]*t +C[20]*u , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[1][1] , 6*C[5]*t +2*C[6]*s +2*C[7]*u +2*C[8]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[1][2] , 2*C[7]*t +2*C[11]*u +C[17]*s +C[18]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[1][3] , 2*C[8]*t +2*C[15]*v +C[18]*u +C[19]*s , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[2][2] , 6*C[9]*u +2*C[10]*s +2*C[11]*t +2*C[12]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[2][3] ,2*C[12]*u +2*C[16]*v +C[18]*t +C[20]*s , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[3][3] , 6*C[13]*v +2*C[14]*s +2*C[15]*t +2*C[16]*u , small_tol , close_tol );

 } // end test 1

 // test 2
 {
   s = 0.101183;
   t = 0.8193;
   u = 0.239483;
   v = 0.000000564;
   Real phi[35];
   basis->evalBasis( s,t,u,v , sign , phi , nBasis );

   Real fb = 0.0;
   for (int j=0;j<nBasis;j++)
     fb += phi[j]*f0[j];

   // evaluate at s,t,u,v
   Real f1 = func_P3( s,t,u,v , C );
   SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

   // check the gradient
   Real phis[35],phit[35],phiu[35],phiv[35];
   basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
   Real gb[4] = {0,0,0,0};
   for (int j=0;j<nBasis;j++)
   {
     gb[0] += f0[j]*phis[j];
     gb[1] += f0[j]*phit[j];
     gb[2] += f0[j]*phiu[j];
     gb[3] += f0[j]*phiv[j];
   }
   SANS_CHECK_CLOSE( gb[0] , 3*C[1]*s*s +2*C[2]*s*t +2*C[3]*s*u +2*C[4]*s*v +C[6]*t*t
                             +C[10]*u*u +C[14]*v*v +C[17]*t*u +C[19]*t*v +C[20]*u*v  , small_tol , close_tol );
   SANS_CHECK_CLOSE( gb[1] , C[2]*s*s +3*C[5]*t*t +2*C[6]*s*t +2*C[7]*t*u +2*C[8]*t*v
                             +C[11]*u*u +C[15]*v*v +C[17]*s*u +C[18]*u*v +C[19]*s*v, small_tol , close_tol );
   SANS_CHECK_CLOSE( gb[2] , C[3]*s*s +C[7]*t*t +3*C[9]*u*u +2*C[10]*u*s +2*C[11]*u*t
                             +2*C[12]*u*v +C[16]*v*v +C[17]*s*t +C[18]*t*v +C[20]*s*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( gb[3] , C[4]*s*s +C[8]*t*t +C[12]*u*u +3*C[13]*v*v +2*C[14]*s*v
                             +2*C[15]*t*v +2*C[16]*u*v +C[18]*t*u +C[19]*s*t +C[20]*s*u  , small_tol , close_tol );

   // check the hessian
   Real phiss[35],phist[35],phitt[35],phisu[35],phitu[35],phiuu[35],phisv[35],phitv[35],phiuv[35],phivv[35];
   basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);
   Real H[4][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };
   for (int i=0;i<nBasis;i++)
   {
     H[0][0] += phiss[i]*f0[i];
     H[0][1] += phist[i]*f0[i];
     H[0][2] += phisu[i]*f0[i];
     H[0][3] += phisv[i]*f0[i];

     H[1][0] = H[0][1];
     H[1][1] += phitt[i]*f0[i];
     H[1][2] += phitu[i]*f0[i];
     H[1][3] += phitv[i]*f0[i];

     H[2][0] = H[0][2];
     H[2][1] = H[1][2];
     H[2][2] += phiuu[i]*f0[i];
     H[2][3] += phiuv[i]*f0[i];

     H[3][0] = H[0][3];
     H[3][1] = H[1][3];
     H[3][2] = H[2][3];
     H[3][3] += phivv[i]*f0[i];
   }
   SANS_CHECK_CLOSE( H[0][0] , 6*C[1]*s +2*C[2]*t +2*C[3]*u +2*C[4]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[0][1] , 2*C[2]*s +2*C[6]*t +C[17]*u +C[19]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[0][2] , 2*C[3]*s +2*C[10]*u +C[17]*t +C[20]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[0][3] , 2*C[4]*s +2*C[14]*v +C[19]*t +C[20]*u , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[1][1] , 6*C[5]*t +2*C[6]*s +2*C[7]*u +2*C[8]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[1][2] , 2*C[7]*t +2*C[11]*u +C[17]*s +C[18]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[1][3] , 2*C[8]*t +2*C[15]*v +C[18]*u +C[19]*s , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[2][2] , 6*C[9]*u +2*C[10]*s +2*C[11]*t +2*C[12]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[2][3] ,2*C[12]*u +2*C[16]*v +C[18]*t +C[20]*s , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[3][3] , 6*C[13]*v +2*C[14]*s +2*C[15]*t +2*C[16]*u , small_tol , close_tol );

 } // end test 2

 // test 3
 {
   s = 0.25;
   t = 0.26;
   u = 0.21;
   v = 0.24;
   Real phi[35];
   basis->evalBasis( s,t,u,v , sign , phi , nBasis );

   Real fb = 0.0;
   for (int j=0;j<nBasis;j++)
     fb += phi[j]*f0[j];

   // evaluate at s,t,u,v
   Real f1 = func_P3( s,t,u,v , C );
   SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

   // check the gradient
   Real phis[35],phit[35],phiu[35],phiv[35];
   basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
   Real gb[4] = {0,0,0,0};
   for (int j=0;j<nBasis;j++)
   {
     gb[0] += f0[j]*phis[j];
     gb[1] += f0[j]*phit[j];
     gb[2] += f0[j]*phiu[j];
     gb[3] += f0[j]*phiv[j];
   }
   SANS_CHECK_CLOSE( gb[0] , 3*C[1]*s*s +2*C[2]*s*t +2*C[3]*s*u +2*C[4]*s*v +C[6]*t*t
                             +C[10]*u*u +C[14]*v*v +C[17]*t*u +C[19]*t*v +C[20]*u*v  , small_tol , close_tol );
   SANS_CHECK_CLOSE( gb[1] , C[2]*s*s +3*C[5]*t*t +2*C[6]*s*t +2*C[7]*t*u +2*C[8]*t*v
                             +C[11]*u*u +C[15]*v*v +C[17]*s*u +C[18]*u*v +C[19]*s*v, small_tol , close_tol );
   SANS_CHECK_CLOSE( gb[2] , C[3]*s*s +C[7]*t*t +3*C[9]*u*u +2*C[10]*u*s +2*C[11]*u*t
                             +2*C[12]*u*v +C[16]*v*v +C[17]*s*t +C[18]*t*v +C[20]*s*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( gb[3] , C[4]*s*s +C[8]*t*t +C[12]*u*u +3*C[13]*v*v +2*C[14]*s*v
                             +2*C[15]*t*v +2*C[16]*u*v +C[18]*t*u +C[19]*s*t +C[20]*s*u  , small_tol , close_tol );

   // check the hessian
   Real phiss[35],phist[35],phitt[35],phisu[35],phitu[35],phiuu[35],phisv[35],phitv[35],phiuv[35],phivv[35];
   basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);
   Real H[4][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };
   for (int i=0;i<nBasis;i++)
   {
     H[0][0] += phiss[i]*f0[i];
     H[0][1] += phist[i]*f0[i];
     H[0][2] += phisu[i]*f0[i];
     H[0][3] += phisv[i]*f0[i];

     H[1][0] = H[0][1];
     H[1][1] += phitt[i]*f0[i];
     H[1][2] += phitu[i]*f0[i];
     H[1][3] += phitv[i]*f0[i];

     H[2][0] = H[0][2];
     H[2][1] = H[1][2];
     H[2][2] += phiuu[i]*f0[i];
     H[2][3] += phiuv[i]*f0[i];

     H[3][0] = H[0][3];
     H[3][1] = H[1][3];
     H[3][2] = H[2][3];
     H[3][3] += phivv[i]*f0[i];
   }
   SANS_CHECK_CLOSE( H[0][0] , 6*C[1]*s +2*C[2]*t +2*C[3]*u +2*C[4]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[0][1] , 2*C[2]*s +2*C[6]*t +C[17]*u +C[19]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[0][2] , 2*C[3]*s +2*C[10]*u +C[17]*t +C[20]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[0][3] , 2*C[4]*s +2*C[14]*v +C[19]*t +C[20]*u , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[1][1] , 6*C[5]*t +2*C[6]*s +2*C[7]*u +2*C[8]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[1][2] , 2*C[7]*t +2*C[11]*u +C[17]*s +C[18]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[1][3] , 2*C[8]*t +2*C[15]*v +C[18]*u +C[19]*s , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[2][2] , 6*C[9]*u +2*C[10]*s +2*C[11]*t +2*C[12]*v , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[2][3] ,2*C[12]*u +2*C[16]*v +C[18]*t +C[20]*s , small_tol , close_tol );
   SANS_CHECK_CLOSE( H[3][3] , 6*C[13]*v +2*C[14]*s +2*C[15]*t +2*C[16]*u , small_tol , close_tol );

 } // end test 3

}

Real
func_P4( Real s , Real t , Real u , Real v , Real* C )
{
  return C[0]
      + C[ 1]*s*s*s*s + C[ 2]*s*s*s*t + C[ 3]*s*s*s*u + C[ 4]*s*s*s*v
      + C[ 5]*s*s*t*t + C[ 6]*s*s*t*u + C[ 7]*s*s*t*v
      + C[ 8]*s*s*u*u + C[ 9]*s*s*u*v
      + C[10]*s*s*v*v
      + C[11]*s*t*t*t + C[12]*s*t*t*u + C[13]*s*t*t*v
      + C[14]*s*t*u*u + C[15]*s*t*u*v
      + C[16]*s*t*v*v
      + C[17]*s*u*u*u + C[18]*s*u*u*v
      + C[19]*s*u*v*v
      + C[20]*s*v*v*v
      + C[21]*t*t*t*t + C[22]*t*t*t*u + C[23]*t*t*t*v
      + C[24]*t*t*u*u + C[25]*t*t*u*v
      + C[26]*t*t*v*v
      + C[27]*t*u*u*u + C[28]*t*t*u*v
      + C[29]*t*t*v*v
      + C[30]*t*v*v*v
      + C[31]*u*u*u*u + C[32]*u*u*u*v
      + C[33]*u*u*v*v
      + C[34]*u*v*v*v
      + C[35]*v*v*v*v;
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP4 )
{

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-10;

  typedef std::array<int,5> Int5;
  const int nBasis = 70;
  const Int5 sign = {{1,1,1,1,1}};
  const BasisFunctionSpacetime<Pentatope, Lagrange, 4>* basis=
                  BasisFunctionSpacetime<Pentatope, Lagrange, 4>::self();

  BOOST_CHECK_EQUAL( basis->order() , 4 );
  BOOST_CHECK_EQUAL( basis->nBasis() , 70 );
  BOOST_CHECK_EQUAL( basis->nBasisNode() , 5 );
  BOOST_CHECK_EQUAL( basis->nBasisEdge() , 30 );
  BOOST_CHECK_EQUAL( basis->nBasisArea() , 30 );
  BOOST_CHECK_EQUAL( basis->nBasisFace() , 5 );
  BOOST_CHECK_EQUAL( basis->nBasisCell() , 0 );

  Real s,t,u,v;

  // interpolate a cubic function
  Real C[36] = { 0.815, 0.906, 0.127, 0.913, 0.632, 0.098, 0.278, 0.547, 0.958, 0.965,
                 0.158, 0.971, 0.957, 0.485, 0.800, 0.142, 0.422, 0.916, 0.792, 0.959,
                 0.656, 0.036, 0.849, 0.934, 0.679, 0.758, 0.743, 0.392, 0.655, 0.171,
                 0.706, 0.032, 0.277, 0.046, 0.097, 0.823 };

  // evaluate the function at the lattice values
  std::vector<Real> sref,tref,uref,vref;
  basis->coordinates(sref,tref,uref,vref);

  // evaluate the linear function at each value
  std::vector<Real> f0( nBasis );
  for (int j=0;j<nBasis;j++)
    f0[j] = func_P4( sref[j],tref[j],uref[j],vref[j] , C );

  // get the lagrange coordinate where the basis functions should all be 1
  {
    std::vector<Real> coord_s(nBasis);
    std::vector<Real> coord_t(nBasis);
    std::vector<Real> coord_u(nBasis);
    std::vector<Real> coord_v(nBasis);

    Real phi[nBasis];

    BasisFunctionSpacetimeBase<Pentatope>::LagrangeP4->coordinates(coord_s, coord_t, coord_u, coord_v);

    BOOST_CHECK_EQUAL(nBasis, coord_s.size());
    BOOST_CHECK_EQUAL(nBasis, coord_t.size());
    BOOST_CHECK_EQUAL(nBasis, coord_u.size());
    BOOST_CHECK_EQUAL(nBasis, coord_v.size());

    for (int n= 0; n < nBasis; n++)
    {
      basis->evalBasis(coord_s[n], coord_t[n], coord_u[n], coord_v[n], sign, phi, nBasis);

      for (int ki= 0; ki < basis->nBasis(); ki++)
      {
        if (ki == n)
          BOOST_CHECK_CLOSE(1, phi[ki], close_tol);
        else
          BOOST_CHECK_SMALL(phi[ki], small_tol);
      }
    }

  }

  // test 1
  {
    s = 0.25748;
    t = 0.000133;
    u = 0.12343;
    v = 0.938489;
    Real phi[70];
    basis->evalBasis( s,t,u,v , sign , phi , nBasis );

    Real fb = 0.0;
    for (int j=0;j<nBasis;j++)
    fb += phi[j]*f0[j];

    // evaluate at s,t,u,v
    Real f1 = func_P4( s,t,u,v , C );
    SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

    // check the gradient
    Real phis[70],phit[70],phiu[70],phiv[70];
    basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
    Real gb[4] = {0,0,0,0};
    for (int j=0;j<nBasis;j++)
    {
      gb[0] += f0[j]*phis[j];
      gb[1] += f0[j]*phit[j];
      gb[2] += f0[j]*phiu[j];
      gb[3] += f0[j]*phiv[j];
    }

    SANS_CHECK_CLOSE( gb[0] ,
      C[ 1]*s*s*s*4 + C[ 2]*s*s*3*t + C[ 3]*s*s*3*u + C[ 4]*s*s*3*v
          + C[ 5]*s*2*t*t + C[ 6]*s*2*t*u + C[ 7]*s*2*t*v
          + C[ 8]*s*2*u*u + C[ 9]*s*2*u*v
          + C[10]*s*2*v*v
          + C[11]*1*t*t*t + C[12]*1*t*t*u + C[13]*1*t*t*v
          + C[14]*1*t*u*u + C[15]*1*t*u*v
          + C[16]*1*t*v*v
          + C[17]*1*u*u*u + C[18]*1*u*u*v
          + C[19]*1*u*v*v
          + C[20]*1*v*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[1] ,
      C[ 2]*s*s*s*1
          + C[ 5]*s*s*t*2 + C[ 6]*s*s*1*u + C[ 7]*s*s*1*v
          + C[11]*s*t*t*3 + C[12]*s*t*2*u + C[13]*s*t*2*v
          + C[14]*s*1*u*u + C[15]*s*1*u*v
          + C[16]*s*1*v*v
          + C[21]*t*t*t*4 + C[22]*t*t*3*u + C[23]*t*t*3*v
          + C[24]*t*2*u*u + C[25]*t*2*u*v
          + C[26]*t*2*v*v
          + C[27]*1*u*u*u + C[28]*t*2*u*v
          + C[29]*t*2*v*v
          + C[30]*1*v*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[2] ,
      C[ 3]*s*s*s*1
          + C[ 6]*s*s*t*1
          + C[ 8]*s*s*u*2 + C[ 9]*s*s*1*v
          + C[12]*s*t*t*1
          + C[14]*s*t*u*2 + C[15]*s*t*1*v
          + C[17]*s*u*u*3 + C[18]*s*u*2*v
          + C[19]*s*1*v*v
          + C[22]*t*t*t*1
          + C[24]*t*t*u*2 + C[25]*t*t*1*v
          + C[27]*t*u*u*3 + C[28]*t*t*1*v
          + C[31]*u*u*u*4 + C[32]*u*u*3*v
          + C[33]*u*2*v*v
          + C[34]*1*v*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[3] ,
      C[ 4]*s*s*s*1
          + C[ 7]*s*s*t*1
          + C[ 9]*s*s*u*1
          + C[10]*s*s*v*2
          + C[13]*s*t*t*1
          + C[15]*s*t*u*1
          + C[16]*s*t*v*2
          + C[18]*s*u*u*1
          + C[19]*s*u*v*2
          + C[20]*s*v*v*3
          + C[23]*t*t*t*1
          + C[25]*t*t*u*1
          + C[26]*t*t*v*2
          + C[28]*t*t*u*1
          + C[29]*t*t*v*2
          + C[30]*t*v*v*3
          + C[32]*u*u*u*1
          + C[33]*u*u*v*2
          + C[34]*u*v*v*3
          + C[35]*v*v*v*4 , small_tol , close_tol );

    // check the hessian
    Real phiss[70],phist[70],phitt[70],phisu[70],phitu[70],phiuu[70],phisv[70],phitv[70],phiuv[70],phivv[70];
    basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);

    Real H[4][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };
    for (int i=0;i<nBasis;i++)
    {
      H[0][0] += phiss[i]*f0[i];
      H[0][1] += phist[i]*f0[i];
      H[0][2] += phisu[i]*f0[i];
      H[0][3] += phisv[i]*f0[i];

      H[1][0] = H[0][1];
      H[1][1] += phitt[i]*f0[i];
      H[1][2] += phitu[i]*f0[i];
      H[1][3] += phitv[i]*f0[i];

      H[2][0] = H[0][2];
      H[2][1] = H[1][2];
      H[2][2] += phiuu[i]*f0[i];
      H[2][3] += phiuv[i]*f0[i];

      H[3][0] = H[0][3];
      H[3][1] = H[1][3];
      H[3][2] = H[2][3];
      H[3][3] += phivv[i]*f0[i];
    }

    SANS_CHECK_CLOSE( H[0][0] ,
      C[ 1]*s*s*3*4 + C[ 2]*s*2*3*t + C[ 3]*s*2*3*u + C[ 4]*s*2*3*v
          + C[ 5]*1*2*t*t + C[ 6]*1*2*t*u + C[ 7]*1*2*t*v
          + C[ 8]*1*2*u*u + C[ 9]*1*2*u*v
          + C[10]*1*2*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][1] ,
      C[ 2]*s*s*3*1
          + C[ 5]*s*2*t*2 + C[ 6]*s*2*1*u + C[ 7]*s*2*1*v
          + C[11]*1*t*t*3 + C[12]*1*t*2*u + C[13]*1*t*2*v
          + C[14]*1*1*u*u + C[15]*1*1*u*v
          + C[16]*1*1*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][2] ,
      C[ 3]*s*s*3*1
          + C[ 6]*s*2*t*1
          + C[ 8]*s*2*u*2 + C[ 9]*s*2*1*v
          + C[12]*1*t*t*1
          + C[14]*1*t*u*2 + C[15]*1*t*1*v
          + C[17]*1*u*u*3 + C[18]*1*u*2*v
          + C[19]*1*1*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][3] ,
      C[ 4]*s*s*3*1
          + C[ 7]*s*2*t*1
          + C[ 9]*s*2*u*1
          + C[10]*s*2*v*2
          + C[13]*1*t*t*1
          + C[15]*1*t*u*1
          + C[16]*1*t*v*2
          + C[18]*1*u*u*1
          + C[19]*1*u*v*2
          + C[20]*1*v*v*3 , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][1] ,
      C[ 5]*s*s*1*2
          + C[11]*s*t*2*3 + C[12]*s*1*2*u + C[13]*s*1*2*v
          + C[21]*t*t*3*4 + C[22]*t*2*3*u + C[23]*t*2*3*v
          + C[24]*1*2*u*u + C[25]*1*2*u*v
          + C[26]*1*2*v*v
          + C[28]*1*2*u*v
          + C[29]*1*2*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][2] ,
      C[ 6]*s*s*1*1
          + C[12]*s*t*2*1
          + C[14]*s*1*u*2 + C[15]*s*1*1*v
          + C[22]*t*t*3*1
          + C[24]*t*2*u*2 + C[25]*t*2*1*v
          + C[27]*1*u*u*3 + C[28]*t*2*1*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][3] ,
      C[ 7]*s*s*1*1
          + C[13]*s*t*2*1
          + C[15]*s*1*u*1
          + C[16]*s*1*v*2
          + C[23]*t*t*3*1
          + C[25]*t*2*u*1
          + C[26]*t*2*v*2
          + C[28]*t*2*u*1
          + C[29]*t*2*v*2
          + C[30]*1*v*v*3, small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][2] ,
      C[ 8]*s*s*1*2
          + C[14]*s*t*1*2
          + C[17]*s*u*2*3 + C[18]*s*1*2*v
          + C[24]*t*t*1*2
          + C[27]*t*u*2*3
          + C[31]*u*u*3*4 + C[32]*u*2*3*v
          + C[33]*1*2*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][3] ,
      C[ 9]*s*s*1*1
          + C[15]*s*t*1*1
          + C[18]*s*u*2*1
          + C[19]*s*1*v*2
          + C[25]*t*t*1*1
          + C[28]*t*t*1*1
          + C[32]*u*u*3*1
          + C[33]*u*2*v*2
          + C[34]*1*v*v*3, small_tol , close_tol );
    SANS_CHECK_CLOSE( H[3][3] ,
      C[10]*s*s*1*2
          + C[16]*s*t*1*2
          + C[19]*s*u*1*2
          + C[20]*s*v*2*3
          + C[26]*t*t*1*2
          + C[29]*t*t*1*2
          + C[30]*t*v*2*3
          + C[33]*u*u*1*2
          + C[34]*u*v*2*3
          + C[35]*v*v*3*4 , small_tol , close_tol );

  } // end test 1

  // test 2
  {
    s = 0.101183;
    t = 0.8193;
    u = 0.239483;
    v = 0.000000564;
    Real phi[70];
    basis->evalBasis( s,t,u,v , sign , phi , nBasis );

    Real fb = 0.0;
    for (int j=0;j<nBasis;j++)
    fb += phi[j]*f0[j];

    // evaluate at s,t,u,v
    Real f1 = func_P4( s,t,u,v , C );
    SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

    // check the gradient
    Real phis[70],phit[70],phiu[70],phiv[70];
    basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
    Real gb[4] = {0,0,0,0};
    for (int j=0;j<nBasis;j++)
    {
      gb[0] += f0[j]*phis[j];
      gb[1] += f0[j]*phit[j];
      gb[2] += f0[j]*phiu[j];
      gb[3] += f0[j]*phiv[j];
    }

    SANS_CHECK_CLOSE( gb[0] ,
      C[ 1]*s*s*s*4 + C[ 2]*s*s*3*t + C[ 3]*s*s*3*u + C[ 4]*s*s*3*v
          + C[ 5]*s*2*t*t + C[ 6]*s*2*t*u + C[ 7]*s*2*t*v
          + C[ 8]*s*2*u*u + C[ 9]*s*2*u*v
          + C[10]*s*2*v*v
          + C[11]*1*t*t*t + C[12]*1*t*t*u + C[13]*1*t*t*v
          + C[14]*1*t*u*u + C[15]*1*t*u*v
          + C[16]*1*t*v*v
          + C[17]*1*u*u*u + C[18]*1*u*u*v
          + C[19]*1*u*v*v
          + C[20]*1*v*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[1] ,
      C[ 2]*s*s*s*1
          + C[ 5]*s*s*t*2 + C[ 6]*s*s*1*u + C[ 7]*s*s*1*v
          + C[11]*s*t*t*3 + C[12]*s*t*2*u + C[13]*s*t*2*v
          + C[14]*s*1*u*u + C[15]*s*1*u*v
          + C[16]*s*1*v*v
          + C[21]*t*t*t*4 + C[22]*t*t*3*u + C[23]*t*t*3*v
          + C[24]*t*2*u*u + C[25]*t*2*u*v
          + C[26]*t*2*v*v
          + C[27]*1*u*u*u + C[28]*t*2*u*v
          + C[29]*t*2*v*v
          + C[30]*1*v*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[2] ,
      C[ 3]*s*s*s*1
          + C[ 6]*s*s*t*1
          + C[ 8]*s*s*u*2 + C[ 9]*s*s*1*v
          + C[12]*s*t*t*1
          + C[14]*s*t*u*2 + C[15]*s*t*1*v
          + C[17]*s*u*u*3 + C[18]*s*u*2*v
          + C[19]*s*1*v*v
          + C[22]*t*t*t*1
          + C[24]*t*t*u*2 + C[25]*t*t*1*v
          + C[27]*t*u*u*3 + C[28]*t*t*1*v
          + C[31]*u*u*u*4 + C[32]*u*u*3*v
          + C[33]*u*2*v*v
          + C[34]*1*v*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[3] ,
      C[ 4]*s*s*s*1
          + C[ 7]*s*s*t*1
          + C[ 9]*s*s*u*1
          + C[10]*s*s*v*2
          + C[13]*s*t*t*1
          + C[15]*s*t*u*1
          + C[16]*s*t*v*2
          + C[18]*s*u*u*1
          + C[19]*s*u*v*2
          + C[20]*s*v*v*3
          + C[23]*t*t*t*1
          + C[25]*t*t*u*1
          + C[26]*t*t*v*2
          + C[28]*t*t*u*1
          + C[29]*t*t*v*2
          + C[30]*t*v*v*3
          + C[32]*u*u*u*1
          + C[33]*u*u*v*2
          + C[34]*u*v*v*3
          + C[35]*v*v*v*4 , small_tol , close_tol );

    // check the hessian
    Real phiss[70],phist[70],phitt[70],phisu[70],phitu[70],phiuu[70],phisv[70],phitv[70],phiuv[70],phivv[70];
    basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);

    Real H[4][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };
    for (int i=0;i<nBasis;i++)
    {
      H[0][0] += phiss[i]*f0[i];
      H[0][1] += phist[i]*f0[i];
      H[0][2] += phisu[i]*f0[i];
      H[0][3] += phisv[i]*f0[i];

      H[1][0] = H[0][1];
      H[1][1] += phitt[i]*f0[i];
      H[1][2] += phitu[i]*f0[i];
      H[1][3] += phitv[i]*f0[i];

      H[2][0] = H[0][2];
      H[2][1] = H[1][2];
      H[2][2] += phiuu[i]*f0[i];
      H[2][3] += phiuv[i]*f0[i];

      H[3][0] = H[0][3];
      H[3][1] = H[1][3];
      H[3][2] = H[2][3];
      H[3][3] += phivv[i]*f0[i];
    }

    SANS_CHECK_CLOSE( H[0][0] ,
      C[ 1]*s*s*3*4 + C[ 2]*s*2*3*t + C[ 3]*s*2*3*u + C[ 4]*s*2*3*v
          + C[ 5]*1*2*t*t + C[ 6]*1*2*t*u + C[ 7]*1*2*t*v
          + C[ 8]*1*2*u*u + C[ 9]*1*2*u*v
          + C[10]*1*2*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][1] ,
      C[ 2]*s*s*3*1
          + C[ 5]*s*2*t*2 + C[ 6]*s*2*1*u + C[ 7]*s*2*1*v
          + C[11]*1*t*t*3 + C[12]*1*t*2*u + C[13]*1*t*2*v
          + C[14]*1*1*u*u + C[15]*1*1*u*v
          + C[16]*1*1*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][2] ,
      C[ 3]*s*s*3*1
          + C[ 6]*s*2*t*1
          + C[ 8]*s*2*u*2 + C[ 9]*s*2*1*v
          + C[12]*1*t*t*1
          + C[14]*1*t*u*2 + C[15]*1*t*1*v
          + C[17]*1*u*u*3 + C[18]*1*u*2*v
          + C[19]*1*1*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][3] ,
      C[ 4]*s*s*3*1
          + C[ 7]*s*2*t*1
          + C[ 9]*s*2*u*1
          + C[10]*s*2*v*2
          + C[13]*1*t*t*1
          + C[15]*1*t*u*1
          + C[16]*1*t*v*2
          + C[18]*1*u*u*1
          + C[19]*1*u*v*2
          + C[20]*1*v*v*3 , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][1] ,
      C[ 5]*s*s*1*2
          + C[11]*s*t*2*3 + C[12]*s*1*2*u + C[13]*s*1*2*v
          + C[21]*t*t*3*4 + C[22]*t*2*3*u + C[23]*t*2*3*v
          + C[24]*1*2*u*u + C[25]*1*2*u*v
          + C[26]*1*2*v*v
          + C[28]*1*2*u*v
          + C[29]*1*2*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][2] ,
      C[ 6]*s*s*1*1
          + C[12]*s*t*2*1
          + C[14]*s*1*u*2 + C[15]*s*1*1*v
          + C[22]*t*t*3*1
          + C[24]*t*2*u*2 + C[25]*t*2*1*v
          + C[27]*1*u*u*3 + C[28]*t*2*1*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][3] ,
      C[ 7]*s*s*1*1
          + C[13]*s*t*2*1
          + C[15]*s*1*u*1
          + C[16]*s*1*v*2
          + C[23]*t*t*3*1
          + C[25]*t*2*u*1
          + C[26]*t*2*v*2
          + C[28]*t*2*u*1
          + C[29]*t*2*v*2
          + C[30]*1*v*v*3, small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][2] ,
      C[ 8]*s*s*1*2
          + C[14]*s*t*1*2
          + C[17]*s*u*2*3 + C[18]*s*1*2*v
          + C[24]*t*t*1*2
          + C[27]*t*u*2*3
          + C[31]*u*u*3*4 + C[32]*u*2*3*v
          + C[33]*1*2*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][3] ,
      C[ 9]*s*s*1*1
          + C[15]*s*t*1*1
          + C[18]*s*u*2*1
          + C[19]*s*1*v*2
          + C[25]*t*t*1*1
          + C[28]*t*t*1*1
          + C[32]*u*u*3*1
          + C[33]*u*2*v*2
          + C[34]*1*v*v*3, small_tol , close_tol );
    SANS_CHECK_CLOSE( H[3][3] ,
      C[10]*s*s*1*2
          + C[16]*s*t*1*2
          + C[19]*s*u*1*2
          + C[20]*s*v*2*3
          + C[26]*t*t*1*2
          + C[29]*t*t*1*2
          + C[30]*t*v*2*3
          + C[33]*u*u*1*2
          + C[34]*u*v*2*3
          + C[35]*v*v*3*4 , small_tol , close_tol );

  } // end test 2

  // test 3
  {
    s = 0.25;
    t = 0.26;
    u = 0.21;
    v = 0.24;

    Real phi[70];
    basis->evalBasis( s,t,u,v , sign , phi , nBasis );

    Real fb = 0.0;
    for (int j=0;j<nBasis;j++)
    fb += phi[j]*f0[j];

    // evaluate at s,t,u,v
    Real f1 = func_P4( s,t,u,v , C );
    SANS_CHECK_CLOSE( fb , f1 , small_tol , close_tol );

    // check the gradient
    Real phis[70],phit[70],phiu[70],phiv[70];
    basis->evalBasisDerivative( s,t,u,v, sign , phis,phit,phiu,phiv , nBasis );
    Real gb[4] = {0,0,0,0};
    for (int j=0;j<nBasis;j++)
    {
      gb[0] += f0[j]*phis[j];
      gb[1] += f0[j]*phit[j];
      gb[2] += f0[j]*phiu[j];
      gb[3] += f0[j]*phiv[j];
    }

    SANS_CHECK_CLOSE( gb[0] ,
      C[ 1]*s*s*s*4 + C[ 2]*s*s*3*t + C[ 3]*s*s*3*u + C[ 4]*s*s*3*v
          + C[ 5]*s*2*t*t + C[ 6]*s*2*t*u + C[ 7]*s*2*t*v
          + C[ 8]*s*2*u*u + C[ 9]*s*2*u*v
          + C[10]*s*2*v*v
          + C[11]*1*t*t*t + C[12]*1*t*t*u + C[13]*1*t*t*v
          + C[14]*1*t*u*u + C[15]*1*t*u*v
          + C[16]*1*t*v*v
          + C[17]*1*u*u*u + C[18]*1*u*u*v
          + C[19]*1*u*v*v
          + C[20]*1*v*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[1] ,
      C[ 2]*s*s*s*1
          + C[ 5]*s*s*t*2 + C[ 6]*s*s*1*u + C[ 7]*s*s*1*v
          + C[11]*s*t*t*3 + C[12]*s*t*2*u + C[13]*s*t*2*v
          + C[14]*s*1*u*u + C[15]*s*1*u*v
          + C[16]*s*1*v*v
          + C[21]*t*t*t*4 + C[22]*t*t*3*u + C[23]*t*t*3*v
          + C[24]*t*2*u*u + C[25]*t*2*u*v
          + C[26]*t*2*v*v
          + C[27]*1*u*u*u + C[28]*t*2*u*v
          + C[29]*t*2*v*v
          + C[30]*1*v*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[2] ,
      C[ 3]*s*s*s*1
          + C[ 6]*s*s*t*1
          + C[ 8]*s*s*u*2 + C[ 9]*s*s*1*v
          + C[12]*s*t*t*1
          + C[14]*s*t*u*2 + C[15]*s*t*1*v
          + C[17]*s*u*u*3 + C[18]*s*u*2*v
          + C[19]*s*1*v*v
          + C[22]*t*t*t*1
          + C[24]*t*t*u*2 + C[25]*t*t*1*v
          + C[27]*t*u*u*3 + C[28]*t*t*1*v
          + C[31]*u*u*u*4 + C[32]*u*u*3*v
          + C[33]*u*2*v*v
          + C[34]*1*v*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( gb[3] ,
      C[ 4]*s*s*s*1
          + C[ 7]*s*s*t*1
          + C[ 9]*s*s*u*1
          + C[10]*s*s*v*2
          + C[13]*s*t*t*1
          + C[15]*s*t*u*1
          + C[16]*s*t*v*2
          + C[18]*s*u*u*1
          + C[19]*s*u*v*2
          + C[20]*s*v*v*3
          + C[23]*t*t*t*1
          + C[25]*t*t*u*1
          + C[26]*t*t*v*2
          + C[28]*t*t*u*1
          + C[29]*t*t*v*2
          + C[30]*t*v*v*3
          + C[32]*u*u*u*1
          + C[33]*u*u*v*2
          + C[34]*u*v*v*3
          + C[35]*v*v*v*4 , small_tol , close_tol );

    // check the hessian
    Real phiss[70],phist[70],phitt[70],phisu[70],phitu[70],phiuu[70],phisv[70],phitv[70],phiuv[70],phivv[70];
    basis->evalBasisHessianDerivative(s,t,u,v,sign,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv,nBasis);

    Real H[4][4] = { {0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0} };
    for (int i=0;i<nBasis;i++)
    {
      H[0][0] += phiss[i]*f0[i];
      H[0][1] += phist[i]*f0[i];
      H[0][2] += phisu[i]*f0[i];
      H[0][3] += phisv[i]*f0[i];

      H[1][0] = H[0][1];
      H[1][1] += phitt[i]*f0[i];
      H[1][2] += phitu[i]*f0[i];
      H[1][3] += phitv[i]*f0[i];

      H[2][0] = H[0][2];
      H[2][1] = H[1][2];
      H[2][2] += phiuu[i]*f0[i];
      H[2][3] += phiuv[i]*f0[i];

      H[3][0] = H[0][3];
      H[3][1] = H[1][3];
      H[3][2] = H[2][3];
      H[3][3] += phivv[i]*f0[i];
    }

    SANS_CHECK_CLOSE( H[0][0] ,
      C[ 1]*s*s*3*4 + C[ 2]*s*2*3*t + C[ 3]*s*2*3*u + C[ 4]*s*2*3*v
          + C[ 5]*1*2*t*t + C[ 6]*1*2*t*u + C[ 7]*1*2*t*v
          + C[ 8]*1*2*u*u + C[ 9]*1*2*u*v
          + C[10]*1*2*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][1] ,
      C[ 2]*s*s*3*1
          + C[ 5]*s*2*t*2 + C[ 6]*s*2*1*u + C[ 7]*s*2*1*v
          + C[11]*1*t*t*3 + C[12]*1*t*2*u + C[13]*1*t*2*v
          + C[14]*1*1*u*u + C[15]*1*1*u*v
          + C[16]*1*1*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][2] ,
      C[ 3]*s*s*3*1
          + C[ 6]*s*2*t*1
          + C[ 8]*s*2*u*2 + C[ 9]*s*2*1*v
          + C[12]*1*t*t*1
          + C[14]*1*t*u*2 + C[15]*1*t*1*v
          + C[17]*1*u*u*3 + C[18]*1*u*2*v
          + C[19]*1*1*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[0][3] ,
      C[ 4]*s*s*3*1
          + C[ 7]*s*2*t*1
          + C[ 9]*s*2*u*1
          + C[10]*s*2*v*2
          + C[13]*1*t*t*1
          + C[15]*1*t*u*1
          + C[16]*1*t*v*2
          + C[18]*1*u*u*1
          + C[19]*1*u*v*2
          + C[20]*1*v*v*3 , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][1] ,
      C[ 5]*s*s*1*2
          + C[11]*s*t*2*3 + C[12]*s*1*2*u + C[13]*s*1*2*v
          + C[21]*t*t*3*4 + C[22]*t*2*3*u + C[23]*t*2*3*v
          + C[24]*1*2*u*u + C[25]*1*2*u*v
          + C[26]*1*2*v*v
          + C[28]*1*2*u*v
          + C[29]*1*2*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][2] ,
      C[ 6]*s*s*1*1
          + C[12]*s*t*2*1
          + C[14]*s*1*u*2 + C[15]*s*1*1*v
          + C[22]*t*t*3*1
          + C[24]*t*2*u*2 + C[25]*t*2*1*v
          + C[27]*1*u*u*3 + C[28]*t*2*1*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[1][3] ,
      C[ 7]*s*s*1*1
          + C[13]*s*t*2*1
          + C[15]*s*1*u*1
          + C[16]*s*1*v*2
          + C[23]*t*t*3*1
          + C[25]*t*2*u*1
          + C[26]*t*2*v*2
          + C[28]*t*2*u*1
          + C[29]*t*2*v*2
          + C[30]*1*v*v*3, small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][2] ,
      C[ 8]*s*s*1*2
          + C[14]*s*t*1*2
          + C[17]*s*u*2*3 + C[18]*s*1*2*v
          + C[24]*t*t*1*2
          + C[27]*t*u*2*3
          + C[31]*u*u*3*4 + C[32]*u*2*3*v
          + C[33]*1*2*v*v , small_tol , close_tol );
    SANS_CHECK_CLOSE( H[2][3] ,
      C[ 9]*s*s*1*1
          + C[15]*s*t*1*1
          + C[18]*s*u*2*1
          + C[19]*s*1*v*2
          + C[25]*t*t*1*1
          + C[28]*t*t*1*1
          + C[32]*u*u*3*1
          + C[33]*u*2*v*2
          + C[34]*1*v*v*3, small_tol , close_tol );
    SANS_CHECK_CLOSE( H[3][3] ,
      C[10]*s*s*1*2
          + C[16]*s*t*1*2
          + C[19]*s*u*1*2
          + C[20]*s*v*2*3
          + C[26]*t*t*1*2
          + C[29]*t*t*1*2
          + C[30]*t*v*2*3
          + C[33]*u*u*1*2
          + C[34]*u*v*2*3
          + C[35]*v*v*3*4 , small_tol , close_tol );

  } // end test 2

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
