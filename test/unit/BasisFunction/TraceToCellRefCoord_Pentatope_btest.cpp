// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Field/Element/ElementVolume.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"   // Tet
#include "BasisFunction/ElementEdges.h"
#include "BasisFunction/ElementFrame.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Quadrature/QuadratureVolume.h"


using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( TraceToCellRefCoord_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TraceToCellRefCoord_Pentatope_basics_test )
{

  // number of dimensions
  const int D= TopoD4::D;

  // the trace nodes!
  const int (*TraceNodes)[D]= TraceToCellRefCoord<Tet, TopoD4, Pentatope>::TraceNodes;
  // the trace edges
  const int (*TraceEdges)[6]= TraceToCellRefCoord<Tet, TopoD4, Pentatope>::TraceEdges;
  // the edge nodes
  const int (*EdgeNodes)[2]= ElementEdges<Pentatope>::EdgeNodes;

  // these checks validate that the tracenodes are ordered according to below
  // tests. if you change them, you'll need to make corresponding changes to the
  // TraceEdges, the ElementFrame, and eval in TraceToCellRefCoord_Pentatope

  BOOST_CHECK_EQUAL(TraceNodes[0][0], 1);
  BOOST_CHECK_EQUAL(TraceNodes[0][1], 2);
  BOOST_CHECK_EQUAL(TraceNodes[0][2], 4);
  BOOST_CHECK_EQUAL(TraceNodes[0][3], 3);

  BOOST_CHECK_EQUAL(TraceNodes[1][0], 0);
  BOOST_CHECK_EQUAL(TraceNodes[1][1], 2);
  BOOST_CHECK_EQUAL(TraceNodes[1][2], 3);
  BOOST_CHECK_EQUAL(TraceNodes[1][3], 4);

  BOOST_CHECK_EQUAL(TraceNodes[2][0], 0);
  BOOST_CHECK_EQUAL(TraceNodes[2][1], 1);
  BOOST_CHECK_EQUAL(TraceNodes[2][2], 4);
  BOOST_CHECK_EQUAL(TraceNodes[2][3], 3);

  BOOST_CHECK_EQUAL(TraceNodes[3][0], 0);
  BOOST_CHECK_EQUAL(TraceNodes[3][1], 1);
  BOOST_CHECK_EQUAL(TraceNodes[3][2], 2);
  BOOST_CHECK_EQUAL(TraceNodes[3][3], 4);

  BOOST_CHECK_EQUAL(TraceNodes[4][0], 0);
  BOOST_CHECK_EQUAL(TraceNodes[4][1], 1);
  BOOST_CHECK_EQUAL(TraceNodes[4][2], 3);
  BOOST_CHECK_EQUAL(TraceNodes[4][3], 2);

  // these checks validate that the traceedges are ordered according to below
  // tests. if you change them, you'll need to make corresponding changes to the
  // the ElementFrame, and eval in TraceToCellRefCoord_Pentatope

  BOOST_CHECK_EQUAL(TraceEdges[0][0], 4);
  BOOST_CHECK_EQUAL(TraceEdges[0][1], 6);
  BOOST_CHECK_EQUAL(TraceEdges[0][2], 5);
  BOOST_CHECK_EQUAL(TraceEdges[0][3], 8);
  BOOST_CHECK_EQUAL(TraceEdges[0][4], 7);
  BOOST_CHECK_EQUAL(TraceEdges[0][5], 9);

  BOOST_CHECK_EQUAL(TraceEdges[1][0], 1);
  BOOST_CHECK_EQUAL(TraceEdges[1][1], 2);
  BOOST_CHECK_EQUAL(TraceEdges[1][2], 3);
  BOOST_CHECK_EQUAL(TraceEdges[1][3], 7);
  BOOST_CHECK_EQUAL(TraceEdges[1][4], 8);
  BOOST_CHECK_EQUAL(TraceEdges[1][5], 9);

  BOOST_CHECK_EQUAL(TraceEdges[2][0], 0);
  BOOST_CHECK_EQUAL(TraceEdges[2][1], 3);
  BOOST_CHECK_EQUAL(TraceEdges[2][2], 2);
  BOOST_CHECK_EQUAL(TraceEdges[2][3], 6);
  BOOST_CHECK_EQUAL(TraceEdges[2][4], 5);
  BOOST_CHECK_EQUAL(TraceEdges[2][5], 9);

  BOOST_CHECK_EQUAL(TraceEdges[3][0], 0);
  BOOST_CHECK_EQUAL(TraceEdges[3][1], 1);
  BOOST_CHECK_EQUAL(TraceEdges[3][2], 3);
  BOOST_CHECK_EQUAL(TraceEdges[3][3], 4);
  BOOST_CHECK_EQUAL(TraceEdges[3][4], 6);
  BOOST_CHECK_EQUAL(TraceEdges[3][5], 8);

  BOOST_CHECK_EQUAL(TraceEdges[4][0], 0);
  BOOST_CHECK_EQUAL(TraceEdges[4][1], 2);
  BOOST_CHECK_EQUAL(TraceEdges[4][2], 1);
  BOOST_CHECK_EQUAL(TraceEdges[4][3], 5);
  BOOST_CHECK_EQUAL(TraceEdges[4][4], 4);
  BOOST_CHECK_EQUAL(TraceEdges[4][5], 7);

  // these checks validate that the edgenodes are ordered according to below
  // tests. if you change them, you'll need to make corresponding changes to the
  // TraceEdges, the ElementFrame, and eval in TraceToCellRefCoord_Pentatope

  BOOST_CHECK_EQUAL(EdgeNodes[0][0], 0);
  BOOST_CHECK_EQUAL(EdgeNodes[0][1], 1);

  BOOST_CHECK_EQUAL(EdgeNodes[1][0], 0);
  BOOST_CHECK_EQUAL(EdgeNodes[1][1], 2);

  BOOST_CHECK_EQUAL(EdgeNodes[2][0], 0);
  BOOST_CHECK_EQUAL(EdgeNodes[2][1], 3);

  BOOST_CHECK_EQUAL(EdgeNodes[3][0], 0);
  BOOST_CHECK_EQUAL(EdgeNodes[3][1], 4);

  BOOST_CHECK_EQUAL(EdgeNodes[4][0], 1);
  BOOST_CHECK_EQUAL(EdgeNodes[4][1], 2);

  BOOST_CHECK_EQUAL(EdgeNodes[5][0], 1);
  BOOST_CHECK_EQUAL(EdgeNodes[5][1], 3);

  BOOST_CHECK_EQUAL(EdgeNodes[6][0], 1);
  BOOST_CHECK_EQUAL(EdgeNodes[6][1], 4);

  BOOST_CHECK_EQUAL(EdgeNodes[7][0], 2);
  BOOST_CHECK_EQUAL(EdgeNodes[7][1], 3);

  BOOST_CHECK_EQUAL(EdgeNodes[8][0], 2);
  BOOST_CHECK_EQUAL(EdgeNodes[8][1], 4);

  BOOST_CHECK_EQUAL(EdgeNodes[9][0], 3);
  BOOST_CHECK_EQUAL(EdgeNodes[9][1], 4);


}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pentatope_eval_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;
  Real sCellTrue,tCellTrue,uCellTrue,vCellTrue;
  CanonicalTraceToCell canonicalFace(0,0);

  typedef DLA::VectorS<4,Real> VectorX;
  typedef Element< VectorX, TopoD3, Tet> TraceClass;

  const int (*OrientPos)[4] = TraceToCellRefCoord<Tet,TopoD4,Pentatope>::OrientPos;
  const int (*OrientNeg)[4] = TraceToCellRefCoord<Tet,TopoD4,Pentatope>::OrientNeg;

  QuadratureVolume<Tet> quad(-1);

  // we are on the 1-s-t-u plane
  canonicalFace.trace = 0;

  TraceClass trace(1,BasisFunctionCategory_Lagrange);

  // loop through all orientations of the trace
  for (int orientation=-12;orientation<=12;orientation++)
  {
    if (orientation==0) continue;

    // set the orientation
    canonicalFace.orientation = orientation;

    // trace vertices are 1-2-4-3
    if (orientation<0)
    {
      trace.DOF( OrientNeg[-orientation-1][0]) = {1,0,0,0};
      trace.DOF( OrientNeg[-orientation-1][1]) = {0,1,0,0};
      trace.DOF( OrientNeg[-orientation-1][2]) = {0,0,0,1};
      trace.DOF( OrientNeg[-orientation-1][3]) = {0,0,1,0};
    }
    else
    {
      trace.DOF( OrientPos[orientation-1][0]) = {1,0,0,0};
      trace.DOF( OrientPos[orientation-1][1]) = {0,1,0,0};
      trace.DOF( OrientPos[orientation-1][2]) = {0,0,0,1};
      trace.DOF( OrientPos[orientation-1][3]) = {0,0,1,0};
    }

    // loop through a set of points on the trace -> use the quadrature points
    for (int iquad=0;iquad<quad.nQuadrature();iquad++)
    {
      Real sTrace,tTrace,uTrace;
      quad.coordinates( iquad , sTrace , tTrace , uTrace );

      // evaluate the cell coordinates
      Real sCell, tCell, uCell, vCell;
      TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalFace, sTrace , tTrace , uTrace , sCell , tCell , uCell , vCell );

      // evaluate what the coordinates should be on this trace
      VectorX X;
      trace.eval( sTrace , tTrace , uTrace , X );
      sCellTrue = X[0];
      tCellTrue = X[1];
      uCellTrue = X[2];
      vCellTrue = X[3];

      // ensure the coordinates match
      SANS_CHECK_CLOSE( sCellTrue , sCell , small_tol , close_tol );
      SANS_CHECK_CLOSE( tCellTrue , tCell , small_tol , close_tol  );
      SANS_CHECK_CLOSE( uCellTrue , uCell , small_tol , close_tol );
      SANS_CHECK_CLOSE( vCellTrue , vCell , small_tol , close_tol );


    }

  }

  // we are on the s = 0 plane
  canonicalFace.trace = 1;

  // loop through all orientations of the trace
  for (int orientation=-12;orientation<=12;orientation++)
  {
    if (orientation==0) continue;

    // set the orientation
    canonicalFace.orientation = orientation;

    // trace vertices are 0-2-3-4
    if (orientation<0)
    {
      trace.DOF( OrientNeg[-orientation-1][0]) = {0,0,0,0};
      trace.DOF( OrientNeg[-orientation-1][1]) = {0,1,0,0};
      trace.DOF( OrientNeg[-orientation-1][2]) = {0,0,1,0};
      trace.DOF( OrientNeg[-orientation-1][3]) = {0,0,0,1};
    }
    else
    {
      trace.DOF( OrientPos[orientation-1][0]) = {0,0,0,0};
      trace.DOF( OrientPos[orientation-1][1]) = {0,1,0,0};
      trace.DOF( OrientPos[orientation-1][2]) = {0,0,1,0};
      trace.DOF( OrientPos[orientation-1][3]) = {0,0,0,1};
    }

    // loop through a set of points on the trace -> use the quadrature points
    for (int iquad=0;iquad<quad.nQuadrature();iquad++)
    {
      Real sTrace,tTrace,uTrace;
      quad.coordinates( iquad , sTrace , tTrace , uTrace );

      // evaluate the cell coordinates
      Real sCell, tCell, uCell, vCell;
      TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalFace, sTrace , tTrace , uTrace , sCell , tCell , uCell , vCell );

      // evaluate what the coordinates should be on this trace
      VectorX X;
      trace.eval( sTrace , tTrace , uTrace , X );
      sCellTrue = X[0];
      tCellTrue = X[1];
      uCellTrue = X[2];
      vCellTrue = X[3];

      // ensure the coordinates match
      SANS_CHECK_CLOSE( sCellTrue , sCell , small_tol , close_tol );
      SANS_CHECK_CLOSE( tCellTrue , tCell , small_tol , close_tol  );
      SANS_CHECK_CLOSE( uCellTrue , uCell , small_tol , close_tol );
      SANS_CHECK_CLOSE( vCellTrue , vCell , small_tol , close_tol );


    }

  }

  // we are on the t = 0 plane
  canonicalFace.trace = 2;

  // loop through all orientations of the trace
  for (int orientation=-12;orientation<=12;orientation++)
  {
    if (orientation==0) continue;

    // set the orientation
    canonicalFace.orientation = orientation;

    // trace vertices are 0-1-4-3
    if (orientation<0)
    {
      trace.DOF( OrientNeg[-orientation-1][0]) = {0,0,0,0};
      trace.DOF( OrientNeg[-orientation-1][1]) = {1,0,0,0};
      trace.DOF( OrientNeg[-orientation-1][2]) = {0,0,0,1};
      trace.DOF( OrientNeg[-orientation-1][3]) = {0,0,1,0};
    }
    else
    {
      trace.DOF( OrientPos[orientation-1][0]) = {0,0,0,0};
      trace.DOF( OrientPos[orientation-1][1]) = {1,0,0,0};
      trace.DOF( OrientPos[orientation-1][2]) = {0,0,0,1};
      trace.DOF( OrientPos[orientation-1][3]) = {0,0,1,0};
    }

    // loop through a set of points on the trace -> use the quadrature points
    for (int iquad=0;iquad<quad.nQuadrature();iquad++)
    {
      Real sTrace,tTrace,uTrace;
      quad.coordinates( iquad , sTrace , tTrace , uTrace );

      // evaluate the cell coordinates
      Real sCell, tCell, uCell, vCell;
      TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalFace, sTrace , tTrace , uTrace , sCell , tCell , uCell , vCell );

      // evaluate what the coordinates should be on this trace
      VectorX X;
      trace.eval( sTrace , tTrace , uTrace , X );
      sCellTrue = X[0];
      tCellTrue = X[1];
      uCellTrue = X[2];
      vCellTrue = X[3];

      // ensure the coordinates match
      SANS_CHECK_CLOSE( sCellTrue , sCell , small_tol , close_tol );
      SANS_CHECK_CLOSE( tCellTrue , tCell , small_tol , close_tol  );
      SANS_CHECK_CLOSE( uCellTrue , uCell , small_tol , close_tol );
      SANS_CHECK_CLOSE( vCellTrue , vCell , small_tol , close_tol );


    }

  }

  // we are on the u = 0 plane
  canonicalFace.trace = 3;

  // loop through all orientations of the trace
  for (int orientation=-12;orientation<=12;orientation++)
  {
    if (orientation==0) continue;

    // set the orientation
    canonicalFace.orientation = orientation;

    // trace vertices are 0-1-2-4
    if (orientation<0)
    {
      trace.DOF( OrientNeg[-orientation-1][0]) = {0,0,0,0};
      trace.DOF( OrientNeg[-orientation-1][1]) = {1,0,0,0};
      trace.DOF( OrientNeg[-orientation-1][2]) = {0,1,0,0};
      trace.DOF( OrientNeg[-orientation-1][3]) = {0,0,0,1};
    }
    else
    {
      trace.DOF( OrientPos[orientation-1][0]) = {0,0,0,0};
      trace.DOF( OrientPos[orientation-1][1]) = {1,0,0,0};
      trace.DOF( OrientPos[orientation-1][2]) = {0,1,0,0};
      trace.DOF( OrientPos[orientation-1][3]) = {0,0,0,1};
    }

    // loop through a set of points on the trace -> use the quadrature points
    for (int iquad=0;iquad<quad.nQuadrature();iquad++)
    {
      Real sTrace,tTrace,uTrace;
      quad.coordinates( iquad , sTrace , tTrace , uTrace );

      // evaluate the cell coordinates
      Real sCell, tCell, uCell, vCell;
      TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalFace, sTrace , tTrace , uTrace , sCell , tCell , uCell , vCell );

      // evaluate what the coordinates should be on this trace
      VectorX X;
      trace.eval( sTrace , tTrace , uTrace , X );
      sCellTrue = X[0];
      tCellTrue = X[1];
      uCellTrue = X[2];
      vCellTrue = X[3];

      // ensure the coordinates match
      SANS_CHECK_CLOSE( sCellTrue , sCell , small_tol , close_tol );
      SANS_CHECK_CLOSE( tCellTrue , tCell , small_tol , close_tol  );
      SANS_CHECK_CLOSE( uCellTrue , uCell , small_tol , close_tol );
      SANS_CHECK_CLOSE( vCellTrue , vCell , small_tol , close_tol );
    }

  }

  // we are on the v=0 plane
  canonicalFace.trace = 4;

  // loop through all orientations of the trace
  for (int orientation=-12;orientation<=12;orientation++)
  {
    if (orientation==0) continue;

    // set the orientation
    canonicalFace.orientation = orientation;

    // trace vertices are 0-1-3-2
    if (orientation<0)
    {
      trace.DOF( OrientNeg[-orientation-1][0]) = {0,0,0,0};
      trace.DOF( OrientNeg[-orientation-1][1]) = {1,0,0,0};
      trace.DOF( OrientNeg[-orientation-1][2]) = {0,0,1,0};
      trace.DOF( OrientNeg[-orientation-1][3]) = {0,1,0,0};
    }
    else
    {
      trace.DOF( OrientPos[orientation-1][0]) = {0,0,0,0};
      trace.DOF( OrientPos[orientation-1][1]) = {1,0,0,0};
      trace.DOF( OrientPos[orientation-1][2]) = {0,0,1,0};
      trace.DOF( OrientPos[orientation-1][3]) = {0,1,0,0};
    }

    // loop through a set of points on the trace -> use the quadrature points
    for (int iquad=0;iquad<quad.nQuadrature();iquad++)
    {
      Real sTrace,tTrace,uTrace;
      quad.coordinates( iquad , sTrace , tTrace , uTrace );

      // evaluate the cell coordinates
      Real sCell, tCell, uCell, vCell;
      TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalFace, sTrace , tTrace , uTrace , sCell , tCell , uCell , vCell );

      // evaluate what the coordinates should be on this trace
      VectorX X;
      trace.eval( sTrace , tTrace , uTrace , X );
      sCellTrue = X[0];
      tCellTrue = X[1];
      uCellTrue = X[2];
      vCellTrue = X[3];

      // ensure the coordinates match
      SANS_CHECK_CLOSE( sCellTrue , sCell , small_tol , close_tol );
      SANS_CHECK_CLOSE( tCellTrue , tCell , small_tol , close_tol  );
      SANS_CHECK_CLOSE( uCellTrue , uCell , small_tol , close_tol );
      SANS_CHECK_CLOSE( vCellTrue , vCell , small_tol , close_tol );


    }

  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pentatope_Orientation_test )
{
  const int pentatope[5] = {0, 1, 2, 3, 4};

  const int (*TraceNodes)[ Tet::NTrace ] = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::TraceNodes;

  const int (*OrientPos)[ Tet::NTrace ] = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::OrientPos;
  const int (*OrientNeg)[ Tet::NTrace ] = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::OrientNeg;



  // loop through all orientations of the trace
  for (int orientation=-12;orientation<=12;orientation++)
  {
    if (orientation==0) continue;

    for (int face=0;face<5;face++)
    {
      CanonicalTraceToCell canonicalFace(face,orientation);

      if (orientation>0)
      {
        const int tet[4] = {pentatope[ TraceNodes[face][OrientPos[orientation-1][0]] ],
                            pentatope[ TraceNodes[face][OrientPos[orientation-1][1]] ],
                            pentatope[ TraceNodes[face][OrientPos[orientation-1][2]] ],
                            pentatope[ TraceNodes[face][OrientPos[orientation-1][3]] ]};

        canonicalFace = TraceToCellRefCoord<Tet,TopoD4,Pentatope>::getCanonicalTrace(tet, 4, pentatope, 5);

        BOOST_CHECK_EQUAL( canonicalFace.orientation, orientation );
        BOOST_CHECK_EQUAL( canonicalFace.trace, face );
      }
      else
      {
        const int tet[4] = {pentatope[TraceNodes[face][OrientNeg[-orientation-1][0]]],
                            pentatope[TraceNodes[face][OrientNeg[-orientation-1][1]]],
                            pentatope[TraceNodes[face][OrientNeg[-orientation-1][2]]],
                            pentatope[TraceNodes[face][OrientNeg[-orientation-1][3]]]};

        canonicalFace = TraceToCellRefCoord<Tet,TopoD4,Pentatope>::getCanonicalTrace(tet, 4, pentatope, 5);
        BOOST_CHECK_EQUAL( canonicalFace.orientation, orientation );
        BOOST_CHECK_EQUAL( canonicalFace.trace, face );

      }

    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Pentatope_TraceEdgeMap_test )
{
  // not implemented yet, will throw an error if the function is called
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
