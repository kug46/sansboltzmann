// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"   // Triangle
#include "BasisFunction/TraceToCellRefCoord.h"


using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( TraceToCellRefCoord_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_eval_test )
{
  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  Real sRef, sRefArea, tRefArea, sRefAreaTrue, tRefAreaTrue;
  CanonicalTraceToCell canonicalEdge(0,0);

  //===================================
  canonicalEdge.orientation = 1;
  //===================================

  canonicalEdge.trace = 0;
  sRef = 0;
  sRefAreaTrue = 0; tRefAreaTrue = 0;

  TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  SANS_CHECK_CLOSE( sRefAreaTrue, sRefArea, small_tol, close_tol );
  SANS_CHECK_CLOSE( tRefAreaTrue, tRefArea, small_tol, close_tol );

  canonicalEdge.trace = 1;
  sRef = 0;
  sRefAreaTrue = 1; tRefAreaTrue = 0;

  TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  SANS_CHECK_CLOSE( sRefAreaTrue, sRefArea, small_tol, close_tol );
  SANS_CHECK_CLOSE( tRefAreaTrue, tRefArea, small_tol, close_tol );

  canonicalEdge.trace = 2;
  sRef = 0;
  sRefAreaTrue = 1; tRefAreaTrue = 1;

  TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  SANS_CHECK_CLOSE( sRefAreaTrue, sRefArea, small_tol, close_tol );
  SANS_CHECK_CLOSE( tRefAreaTrue, tRefArea, small_tol, close_tol );

  canonicalEdge.trace = 3;
  sRef = 0;
  sRefAreaTrue = 0; tRefAreaTrue = 1;

  TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  SANS_CHECK_CLOSE( sRefAreaTrue, sRefArea, small_tol, close_tol );
  SANS_CHECK_CLOSE( tRefAreaTrue, tRefArea, small_tol, close_tol );

  //===================================
  canonicalEdge.orientation = -1;
  //===================================

  canonicalEdge.trace = 0;
  sRef = 0;
  sRefAreaTrue = 1; tRefAreaTrue = 0;

  TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  SANS_CHECK_CLOSE( sRefAreaTrue, sRefArea, small_tol, close_tol );
  SANS_CHECK_CLOSE( tRefAreaTrue, tRefArea, small_tol, close_tol );

  canonicalEdge.trace = 1;
  sRef = 0;
  sRefAreaTrue = 1; tRefAreaTrue = 1;

  TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  SANS_CHECK_CLOSE( sRefAreaTrue, sRefArea, small_tol, close_tol );
  SANS_CHECK_CLOSE( tRefAreaTrue, tRefArea, small_tol, close_tol );

  canonicalEdge.trace = 2;
  sRef = 0;
  sRefAreaTrue = 0; tRefAreaTrue = 1;

  TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  SANS_CHECK_CLOSE( sRefAreaTrue, sRefArea, small_tol, close_tol );
  SANS_CHECK_CLOSE( tRefAreaTrue, tRefArea, small_tol, close_tol );

  canonicalEdge.trace = 3;
  sRef = 0;
  sRefAreaTrue = 0; tRefAreaTrue = 0;

  TraceToCellRefCoord<Line, TopoD2, Quad>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  SANS_CHECK_CLOSE( sRefAreaTrue, sRefArea, small_tol, close_tol );
  SANS_CHECK_CLOSE( tRefAreaTrue, tRefArea, small_tol, close_tol );


  typedef TraceToCellRefCoord<Line, TopoD2, Quad> TraceToCellRefCoordLineQuad;

  canonicalEdge.trace = 4;
  BOOST_CHECK_THROW( TraceToCellRefCoordLineQuad::eval( canonicalEdge, sRef, sRefArea, tRefArea ), DeveloperException );

  canonicalEdge.trace = -4;
  BOOST_CHECK_THROW( TraceToCellRefCoordLineQuad::eval( canonicalEdge, sRef, sRefArea, tRefArea ), DeveloperException );

  canonicalEdge.trace = -1;
  BOOST_CHECK_THROW( TraceToCellRefCoordLineQuad::eval( canonicalEdge, sRef, sRefArea, tRefArea ), DeveloperException );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_Orientation_test )
{

  const int quad[4] = {0, 1, 2, 3};

  const int (*TraceNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Quad>::TraceNodes;

  CanonicalTraceToCell canonicalEdge(0,0);

  for (int edge = 0; edge < 4; edge++ )
  {
    // Orientation is 1 for all edges that match the canonical edge
    const int line[2] = {quad[TraceNodes[edge][0]], quad[TraceNodes[edge][1]]};

    canonicalEdge = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTrace(line, 2, quad, 4);
    BOOST_CHECK_EQUAL( canonicalEdge.trace, edge );
    BOOST_CHECK_EQUAL( canonicalEdge.orientation, 1 );
  }

  for (int edge = 0; edge < 4; edge++ )
  {
    // Reverse the two nodes results in a -1 orientation
    const int line[2] = {quad[TraceNodes[edge][1]], quad[TraceNodes[edge][0]]};

    canonicalEdge = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTrace(line, 2, quad, 4);
    BOOST_CHECK_EQUAL( canonicalEdge.trace, edge );
    BOOST_CHECK_EQUAL( canonicalEdge.orientation, -1 );
  }

  // Test retrieving the canonical edge given the nodes on the face.
  // The nodes can be randomly ordered, and the correct canonical ordering is returned

  int canonicalLine[2];
  for (int edge = 0; edge < 4; edge++ )
  {
    // Test a jumbled node ordering retrieves the canonical node ordering
    const int line[2] = {quad[TraceNodes[edge][0]], quad[TraceNodes[edge][1]]};

    canonicalEdge = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTraceLeft(line, 2, quad, 4, canonicalLine, 2);
    BOOST_CHECK_EQUAL( canonicalEdge.trace, edge );
    BOOST_CHECK_EQUAL( canonicalEdge.orientation, 1 );

    BOOST_CHECK_EQUAL( quad[TraceNodes[edge][0]], canonicalLine[0] );
    BOOST_CHECK_EQUAL( quad[TraceNodes[edge][1]], canonicalLine[1] );
  }

  for (int edge = 0; edge < 4; edge++ )
  {
    // Test a jumbled node ordering retrieves the canonical node ordering
    const int line[2] = {quad[TraceNodes[edge][1]], quad[TraceNodes[edge][0]]};

    canonicalEdge = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTraceLeft(line, 2, quad, 4, canonicalLine, 2);
    BOOST_CHECK_EQUAL( canonicalEdge.trace, edge );
    BOOST_CHECK_EQUAL( canonicalEdge.orientation, 1 );

    BOOST_CHECK_EQUAL( quad[TraceNodes[edge][0]], canonicalLine[0] );
    BOOST_CHECK_EQUAL( quad[TraceNodes[edge][1]], canonicalLine[1] );
  }

  // Using nodes that do not make up a edge causes an exception
  BOOST_CHECK_THROW( (TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTraceLeft({0, 2}, quad, 4, canonicalLine, 2)), DeveloperException );
  BOOST_CHECK_THROW( (TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTraceLeft({1, 3}, quad, 4, canonicalLine, 2)), DeveloperException );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
