// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionArea_Triangle_Legendre_btest
// testing of BasisFunctionArea<Triangle, Legendre> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Legendre.h"
#include "Quadrature/QuadratureArea.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionArea_Triangle_Legendre_test_suite )

void fillTrueValues(const int& order, const Real& s, const Real& t, Real phi[], Real phis[], Real phit[])
{
  phi[0] = 1;
  phis[0] = 0;
  phit[0] = 0;

  if (order == 0) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[1] = (sqrt(2))*(-1+(3)*(s));
  phi[2] = (sqrt(6))*(-1+s+(2)*(t));

  phis[1] = (3)*(sqrt(2));
  phis[2] = sqrt(6);

  phit[1] = 0;
  phit[2] = (2)*(sqrt(6));

  if (order == 1) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[3] = (sqrt(3))*(1+(2)*((s)*(-4+(5)*(s))));
  phi[4] = (3)*((-1+(5)*(s))*(-1+s+(2)*(t)));
  phi[5] = (sqrt(15))*(pow(-1+s,2)+(6)*((-1+s)*(t))+(6)*(pow(t,2)));

  phis[3] = (4)*((sqrt(3))*(-2+(5)*(s)));
  phis[4] = (6)*(-3+(5)*(s)+(5)*(t));
  phis[5] = (2)*((sqrt(15))*(-1+s+(3)*(t)));

  phit[3] = 0;
  phit[4] = -6+(30)*(s);
  phit[5] = (6)*((sqrt(15))*(-1+s+(2)*(t)));

  if (order == 2) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[6] = -2+(10)*((s)*(3+(s)*(-9+(7)*(s))));
  phi[7] = (2)*((sqrt(3))*((1+(3)*((s)*(-4+(7)*(s))))*(-1+s+(2)*(t))));
  phi[8] = (2)*((sqrt(5))*((-1+(7)*(s))*(pow(-1+s,2)+(6)*((-1+s)*(t))+(6)*(pow(t,2)))));
  phi[9] = (2)*((sqrt(7))*((-1+s+(2)*(t))*(pow(-1+s,2)+(10)*((-1+s)*(t))+(10)*(pow(t,2)))));

  phis[6] = (30)*(1+(s)*(-6+(7)*(s)));
  phis[7] = (2)*((sqrt(3))*(13+(-24)*(t)+(3)*((s)*(-22+(21)*(s)+(28)*(t)))));
  phis[8] = (6)*((sqrt(5))*(3+(7)*(pow(s,2))+(2)*((t)*(-8+(7)*(t)))+(2)*((s)*(-5+(14)*(t)))));
  phis[9] = (6)*((sqrt(7))*(pow(-1+s,2)+(8)*((-1+s)*(t))+(10)*(pow(t,2))));

  phit[6] = 0;
  phit[7] = (4)*((sqrt(3))*(1+(3)*((s)*(-4+(7)*(s)))));
  phit[8] = (12)*((sqrt(5))*((-1+(7)*(s))*(-1+s+(2)*(t))));
  phit[9] = (24)*((sqrt(7))*(pow(-1+s,2)+(5)*((-1+s)*(t))+(5)*(pow(t,2))));

  if (order == 3) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[10] = (sqrt(5))*(1+(2)*((s)*(-12+(7)*((s)*(9+(s)*(-16+(9)*(s)))))));
  phi[11] = (sqrt(15))*((-1+(21)*((pow(1+(-2)*(s),2))*(s)))*(-1+s+(2)*(t)));
  phi[12] = (5)*((1+(4)*((s)*(-4+(9)*(s))))*(pow(-1+s,2)+(6)*((-1+s)*(t))+(6)*(pow(t,2))));
  phi[13] = (sqrt(35))*((-1+(9)*(s))*((-1+s+(2)*(t))*(pow(-1+s,2)+(10)*((-1+s)*(t))+(10)*(pow(t,2)))));
  phi[14] = (3)*((sqrt(5))*(pow(-1+s,4)+(20)*((pow(-1+s,3))*(t))+(90)*((pow(-1+s,2))*(pow(t,2)))+(140)*((-1+s)*(pow(t,3)))+(70)*(pow(t,4))));

  phis[10] = (12)*((sqrt(5))*(-2+(7)*((s)*(3+(-8)*(s)+(6)*(pow(s,2))))));
  phis[11] = (2)*((sqrt(15))*(-11+(21)*(t)+(21)*((s)*(5+(-8)*(t)+(4)*((s)*(-3+(2)*(s)+(3)*(t)))))));
  phis[12] = (30)*(-3+(s)*(23+(4)*((s)*(-11+(6)*(s))))+(17)*(t)+(4)*((s)*((-26+(27)*(s))*(t)))+(8)*((-2+(9)*(s))*(pow(t,2))));
  phis[13] = (12)*((sqrt(35))*((pow(-1+s,2))*(-1+(3)*(s))+(-1+s)*((-11+(27)*(s))*(t))+(5)*((-5+(9)*(s))*(pow(t,2)))+(15)*(pow(t,3))));
  phis[14] = (12)*((sqrt(5))*(pow(-1+s,3)+(15)*((pow(-1+s,2))*(t))+(45)*((-1+s)*(pow(t,2)))+(35)*(pow(t,3))));

  phit[10] = 0;
  phit[11] = (2)*((sqrt(15))*(-1+(21)*((pow(1+(-2)*(s),2))*(s))));
  phit[12] = (30)*((1+(4)*((s)*(-4+(9)*(s))))*(-1+s+(2)*(t)));
  phit[13] = (12)*((sqrt(35))*((-1+(9)*(s))*(pow(-1+s,2)+(5)*((-1+s)*(t))+(5)*(pow(t,2)))));
  phit[14] = (60)*((sqrt(5))*((-1+s+(2)*(t))*(pow(-1+s,2)+(7)*((-1+s)*(t))+(7)*(pow(t,2)))));

  if (order == 4) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[15] = (sqrt(6))*(-1+(7)*((s)*(5+(2)*((s)*(-20+(3)*((s)*(20+(s)*(-25+(11)*(s)))))))));
  phi[16] = (3)*((sqrt(2))*((1+(2)*((s)*(-16+(3)*((s)*(36+(-80)*(s)+(55)*(pow(s,2)))))))*(-1+s+(2)*(t))));
  phi[17] = (sqrt(30))*((-1+(3)*((s)*(9+(5)*((s)*(-9+(11)*(s))))))*(pow(-1+s,2)+(6)*((-1+s)*(t))+(6)*(pow(t,2))));
  phi[18] = (sqrt(42))*((1+(5)*((s)*(-4+(11)*(s))))*((-1+s+(2)*(t))*(pow(-1+s,2)+(10)*((-1+s)*(t))+(10)*(pow(t,2)))));
  phi[19] = (3)*((sqrt(6))*((-1+(11)*(s))*(pow(-1+s,4)+(20)*((pow(-1+s,3))*(t))+(90)*((pow(-1+s,2))*(pow(t,2)))+(140)*((-1+s)*(pow(t,3)))
            +(70)*(pow(t,4)))));
  phi[20] = (sqrt(66))*(pow(-1+s,5)+(30)*((pow(-1+s,4))*(t))+(210)*((pow(-1+s,3))*(pow(t,2)))+(560)*((pow(-1+s,2))*(pow(t,3)))
            +(630)*((-1+s)*(pow(t,4)))+(252)*(pow(t,5)));

  phis[15] = (35)*((sqrt(6))*(1+(2)*((s)*(-8+(3)*((s)*(12+(s)*(-20+(11)*(s))))))));
  phis[16] = (3)*((sqrt(2))*(33+(-64)*(t)+(2)*((s)*((8)*(-31+(54)*(t))+(3)*((s)*(348+(-480)*(t)+(5)*((s)*(-108+(55)*(s)+(88)*(t)))))))));
  phis[17] = (sqrt(30))*((2)*((-1+(3)*((s)*(9+(5)*((s)*(-9+(11)*(s))))))*(-1+s+(3)*(t)))+(9)*((3+(5)*((s)*(-6+(11)*(s))))*(pow(-1+s,2)
             +(6)*((-1+s)*(t))+(6)*(pow(t,2)))));
  phis[18] = (sqrt(42))*((pow(-1+s,2))*(23+(5)*((s)*(-38+(55)*(s))))+(24)*((-1+s)*((11+(5)*((s)*(-17+(22)*(s))))*(t)))
             +(90)*((7+(-50)*(s)+(55)*(pow(s,2)))*(pow(t,2)))+(200)*((-2+(11)*(s))*(pow(t,3))));
  phis[19] = (15)*((sqrt(6))*((pow(-1+s,3))*(-3+(11)*(s))+(8)*((pow(-1+s,2))*((-7+(22)*(s))*(t)))+(18)*((-1+s)*((-13+(33)*(s))*(pow(t,2))))
             +(56)*((-6+(11)*(s))*(pow(t,3)))+(154)*(pow(t,4))));
  phis[20] = (5)*((sqrt(66))*(pow(-1+s,4)+(24)*((pow(-1+s,3))*(t))+(126)*((pow(-1+s,2))*(pow(t,2)))+(224)*((-1+s)*(pow(t,3)))+(126)*(pow(t,4))));

  phit[15] = 0;
  phit[16] = (6)*((sqrt(2))*(1+(2)*((s)*(-16+(3)*((s)*(36+(-80)*(s)+(55)*(pow(s,2))))))));
  phit[17] = (6)*((sqrt(30))*((-1+(3)*((s)*(9+(5)*((s)*(-9+(11)*(s))))))*(-1+s+(2)*(t))));
  phit[18] = (12)*((sqrt(42))*((1+(5)*((s)*(-4+(11)*(s))))*(pow(-1+s,2)+(5)*((-1+s)*(t))+(5)*(pow(t,2)))));
  phit[19] = (60)*((sqrt(6))*((-1+(11)*(s))*((-1+s+(2)*(t))*(pow(-1+s,2)+(7)*((-1+s)*(t))+(7)*(pow(t,2))))));
  phit[20] = (30)*((sqrt(66))*(pow(-1+s,4)+(14)*((pow(-1+s,3))*(t))+(56)*((pow(-1+s,2))*(pow(t,2)))+(84)*((-1+s)*(pow(t,3)))+(42)*(pow(t,4))));

  if (order == 5) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[21] = (sqrt(7))*(1+(6)*((s)*(-8+(s)*(90+(s)*(-400+(11)*((s)*(75+(-72)*(s)+(26)*(pow(s,2)))))))));
  phi[22] = (sqrt(21))*((-1+(3)*((s)*(15+(s)*(-150+(11)*((s)*(50+(-75)*(s)+(39)*(pow(s,2))))))))*(-1+s+(2)*(t)));
  phi[23] = (sqrt(35))*((1+(5)*((s)*(-8+(11)*((s)*(6+(s)*(-16+(13)*(s)))))))*(pow(-1+s,2)+(6)*((-1+s)*(t))+(6)*(pow(t,2))));
  phi[24] = (7)*((-1+(11)*((s)*(3+(2)*((s)*(-9+(13)*(s))))))*((-1+s+(2)*(t))*(pow(-1+s,2)+(10)*((-1+s)*(t))+(10)*(pow(t,2)))));
  phi[25] = (3)*((sqrt(7))*((1+(6)*((s)*(-4+(13)*(s))))*(pow(-1+s,4)+(20)*((pow(-1+s,3))*(t))+(90)*((pow(-1+s,2))*(pow(t,2)))
            +(140)*((-1+s)*(pow(t,3)))+(70)*(pow(t,4)))));
  phi[26] = (sqrt(77))*((-1+(13)*(s))*(pow(-1+s,5)+(30)*((pow(-1+s,4))*(t))+(210)*((pow(-1+s,3))*(pow(t,2)))+(560)*((pow(-1+s,2))*(pow(t,3)))
            +(630)*((-1+s)*(pow(t,4)))+(252)*(pow(t,5))));
  phi[27] = (sqrt(91))*(pow(-1+s,6)+(42)*((pow(-1+s,5))*(t))+(420)*((pow(-1+s,4))*(pow(t,2)))+(1680)*((pow(-1+s,3))*(pow(t,3)))
            +(3150)*((pow(-1+s,2))*(pow(t,4)))+(2772)*((-1+s)*(pow(t,5)))+(924)*(pow(t,6)));

  phis[21] = (24)*((sqrt(7))*(-2+(3)*((s)*(15+(s)*(-100+(11)*((s)*(25+(s)*(-30+(13)*(s)))))))));
  phis[22] = (2)*((sqrt(21))*(-23+(45)*(t)+(3)*((s)*(165+(-300)*(t)+(s)*((150)*(-7+(11)*(t))+(11)*((s)*((50)*(5+(-6)*(t))
             +(3)*((s)*(-95+(39)*(s)+(65)*(t))))))))));
  phis[23] = (sqrt(35))*((2)*((1+(5)*((s)*(-8+(11)*((s)*(6+(s)*(-16+(13)*(s)))))))*(-1+s+(3)*(t)))
             +(20)*((-2+(11)*((s)*(3+(s)*(-12+(13)*(s)))))*(pow(-1+s,2)+(6)*((-1+s)*(t))+(6)*(pow(t,2)))));
  phis[24] = (21)*((-1+(11)*((s)*(3+(2)*((s)*(-9+(13)*(s))))))*(pow(-1+s,2)+(8)*((-1+s)*(t))+(10)*(pow(t,2))))
             +(231)*((1+(2)*((s)*(-6+(13)*(s))))*((-1+s+(2)*(t))*(pow(-1+s,2)+(10)*((-1+s)*(t))+(10)*(pow(t,2)))));
  phis[25] = (12)*((sqrt(7))*((pow(-1+s,3))*(7+(3)*((s)*(-23+(39)*(s))))+(15)*((pow(-1+s,2))*((9+(2)*((s)*(-42+(65)*(s))))*(t)))
             +(45)*((-1+s)*((13+(6)*((s)*(-19+(26)*(s))))*(pow(t,2))))+(35)*((25+(6)*((s)*(-34+(39)*(s))))*(pow(t,3)))
             +(210)*((-2+(13)*(s))*(pow(t,4)))));
  phis[26] = (6)*((sqrt(77))*((pow(-1+s,4))*(-3+(13)*(s))+(5)*((pow(-1+s,3))*((-17+(65)*(s))*(t)))+(140)*((pow(-1+s,2))*((-4+(13)*(s))*(pow(t,2))))
             +(280)*((-1+s)*((-5+(13)*(s))*(pow(t,3))))+(210)*((-7+(13)*(s))*(pow(t,4)))+(546)*(pow(t,5))));
  phis[27] = (6)*((sqrt(91))*(pow(-1+s,5)+(35)*((pow(-1+s,4))*(t))+(280)*((pow(-1+s,3))*(pow(t,2)))+(840)*((pow(-1+s,2))*(pow(t,3)))
             +(1050)*((-1+s)*(pow(t,4)))+(462)*(pow(t,5))));

  phit[21] = 0;
  phit[22] = (2)*((sqrt(21))*(-1+(3)*((s)*(15+(s)*(-150+(11)*((s)*(50+(-75)*(s)+(39)*(pow(s,2)))))))));
  phit[23] = (6)*((sqrt(35))*((1+(5)*((s)*(-8+(11)*((s)*(6+(s)*(-16+(13)*(s)))))))*(-1+s+(2)*(t))));
  phit[24] = (84)*((-1+(11)*((s)*(3+(2)*((s)*(-9+(13)*(s))))))*(pow(-1+s,2)+(5)*((-1+s)*(t))+(5)*(pow(t,2))));
  phit[25] = (60)*((sqrt(7))*((1+(6)*((s)*(-4+(13)*(s))))*((-1+s+(2)*(t))*(pow(-1+s,2)+(7)*((-1+s)*(t))+(7)*(pow(t,2))))));
  phit[26] = (30)*((sqrt(77))*((-1+(13)*(s))*(pow(-1+s,4)+(14)*((pow(-1+s,3))*(t))+(56)*((pow(-1+s,2))*(pow(t,2)))+(84)*((-1+s)*(pow(t,3)))
             +(42)*(pow(t,4)))));
  phit[27] = (42)*((sqrt(91))*(pow(-1+s,5)+(20)*((pow(-1+s,4))*(t))+(120)*((pow(-1+s,3))*(pow(t,2)))+(300)*((pow(-1+s,2))*(pow(t,3)))
             +(330)*((-1+s)*(pow(t,4)))+(132)*(pow(t,5))));

  if (order == 6) return;
  //---------------------------------------------------------------------------------------------------------------------------------------------

  phi[28] = (2)*((sqrt(2))*(-1+(3)*((s)*(21+(s)*(-315+(11)*((s)*(175+(s)*(-525+(13)*((s)*(63+(s)*(-49+(15)*(s))))))))))));
  phi[29] = (2)*((sqrt(6))*((1+(s)*(-60+(11)*((s)*(75+(s)*(-400+(13)*((s)*(75+(7)*((s)*(-12+(5)*(s))))))))))*(-1+s+(2)*(t))));
  phi[30] = (2)*((sqrt(10))*((-1+(11)*((s)*(5+(s)*(-60+(13)*((s)*(20+(7)*((s)*(-5+(3)*(s)))))))))*(pow(-1+s,2)+(6)*((-1+s)*(t))+(6)*(pow(t,2)))));
  phi[31] = (2)*((sqrt(14))*((1+(s)*(-48+(13)*((s)*(36+(7)*((s)*(-16+(15)*(s)))))))*((-1+s+(2)*(t))*(pow(-1+s,2)
            +(10)*((-1+s)*(t))+(10)*(pow(t,2))))));
  phi[32] = (6)*((sqrt(2))*((-1+(13)*((s)*(3+(7)*((s)*(-3+(5)*(s))))))*(pow(-1+s,4)+(20)*((pow(-1+s,3))*(t))
            +(90)*((pow(-1+s,2))*(pow(t,2)))+(140)*((-1+s)*(pow(t,3)))+(70)*(pow(t,4)))));
  phi[33] = (2)*((sqrt(22))*((1+(7)*((s)*(-4+(15)*(s))))*(pow(-1+s,5)+(30)*((pow(-1+s,4))*(t))+(210)*((pow(-1+s,3))*(pow(t,2)))
            +(560)*((pow(-1+s,2))*(pow(t,3)))+(630)*((-1+s)*(pow(t,4)))+(252)*(pow(t,5)))));
  phi[34] = (2)*((sqrt(26))*((-1+(15)*(s))*(pow(-1+s,6)+(42)*((pow(-1+s,5))*(t))+(420)*((pow(-1+s,4))*(pow(t,2)))
            +(1680)*((pow(-1+s,3))*(pow(t,3)))+(3150)*((pow(-1+s,2))*(pow(t,4)))+(2772)*((-1+s)*(pow(t,5)))+(924)*(pow(t,6)))));
  phi[35] = (2)*((sqrt(30))*(pow(-1+s,7)+(56)*((pow(-1+s,6))*(t))+(756)*((pow(-1+s,5))*(pow(t,2)))+(4200)*((pow(-1+s,4))*(pow(t,3)))
            +(11550)*((pow(-1+s,3))*(pow(t,4)))+(16632)*((pow(-1+s,2))*(pow(t,5)))+(12012)*((-1+s)*(pow(t,6)))+(3432)*(pow(t,7))));

  phis[28] = (126)*((sqrt(2))*(1+(s)*(-30+(11)*((s)*(25+(s)*(-100+(13)*((s)*(15+(s)*(-14+(5)*(s))))))))));
  phis[29] = (2)*((sqrt(6))*(61+(-120)*(t)+(s)*((30)*(-59+(110)*(t))+(11)*((s)*((75)*(19+(-32)*(t))+(s)*((100)*(-55+(78)*(t))
             +(13)*((s)*(795+(-714)*(s)+(245)*(pow(s,2))+(420)*((-2+s)*(t))))))))));
  phis[30] = (2)*((sqrt(10))*((2)*((-1+(11)*((s)*(5+(s)*(-60+(13)*((s)*(20+(7)*((s)*(-5+(3)*(s)))))))))*(-1+s+(3)*(t)))
             +(55)*((1+(s)*(-24+(13)*((s)*(12+(7)*((s)*(-4+(3)*(s)))))))*(pow(-1+s,2)+(6)*((-1+s)*(t))+(6)*(pow(t,2))))));
  phis[31] = (2)*((sqrt(14))*((3)*((1+(s)*(-48+(13)*((s)*(36+(7)*((s)*(-16+(15)*(s)))))))*(pow(-1+s,2)+(8)*((-1+s)*(t))+(10)*(pow(t,2))))
             +(12)*((-4+(13)*((s)*(6+(7)*((s)*(-4+(5)*(s))))))*((-1+s+(2)*(t))*(pow(-1+s,2)+(10)*((-1+s)*(t))+(10)*(pow(t,2)))))));
  phis[32] = (6)*((sqrt(2))*((pow(-1+s,3))*(-43+(13)*((s)*(57+(7)*((s)*(-33+(35)*(s))))))
             +(120)*((pow(-1+s,2))*((-7+(13)*((s)*(9+(35)*((-1+s)*(s)))))*(t)))
             +(90)*((-1+s)*((-41+(13)*((s)*(51+(7)*((s)*(-27+(25)*(s))))))*(pow(t,2))))
             +(560)*((-10+(13)*((s)*(12+(7)*((s)*(-6+(5)*(s))))))*(pow(t,3)))+(2730)*((1+(7)*((s)*(-2+(5)*(s))))*(pow(t,4)))));
  phis[33] = (6)*((sqrt(22))*((pow(-1+s,4))*(11+(7)*((s)*(-18+(35)*(s))))+(20)*((pow(-1+s,3))*((16+(35)*((s)*(-5+(9)*(s))))*(t)))
             +(70)*((pow(-1+s,2))*((31+(7)*((s)*(-46+(75)*(s))))*(pow(t,2))))+(1120)*((-1+s)*((5+(7)*((s)*(-7+(10)*(s))))*(pow(t,3))))
             +(210)*((29+(7)*((s)*(-38+(45)*(s))))*(pow(t,4)))+(1176)*((-2+(15)*(s))*(pow(t,5)))));
  phis[34] = (42)*((sqrt(26))*((pow(-1+s,5))*(-1+(5)*(s))+(20)*((pow(-1+s,4))*((-2+(9)*(s))*(t)))+(20)*((pow(-1+s,3))*((-19+(75)*(s))*(pow(t,2))))
             +(480)*((pow(-1+s,2))*((-3+(10)*(s))*(pow(t,3))))+(150)*((-1+s)*((-17+(45)*(s))*(pow(t,4))))
             +(264)*((-8+(15)*(s))*(pow(t,5)))+(660)*(pow(t,6))));
  phis[35] = (14)*((sqrt(30))*(pow(-1+s,6)+(48)*((pow(-1+s,5))*(t))+(540)*((pow(-1+s,4))*(pow(t,2)))+(2400)*((pow(-1+s,3))*(pow(t,3)))
             +(4950)*((pow(-1+s,2))*(pow(t,4)))+(4752)*((-1+s)*(pow(t,5)))+(1716)*(pow(t,6))));

  phit[28] = 0;
  phit[29] = (4)*((sqrt(6))*(1+(s)*(-60+(11)*((s)*(75+(s)*(-400+(13)*((s)*(75+(7)*((s)*(-12+(5)*(s)))))))))));
  phit[30] = (12)*((sqrt(10))*((-1+(11)*((s)*(5+(s)*(-60+(13)*((s)*(20+(7)*((s)*(-5+(3)*(s)))))))))*(-1+s+(2)*(t))));
  phit[31] = (24)*((sqrt(14))*((1+(s)*(-48+(13)*((s)*(36+(7)*((s)*(-16+(15)*(s)))))))*(pow(-1+s,2)+(5)*((-1+s)*(t))+(5)*(pow(t,2)))));
  phit[32] = (120)*((sqrt(2))*((-1+(13)*((s)*(3+(7)*((s)*(-3+(5)*(s))))))*((-1+s+(2)*(t))*(pow(-1+s,2)+(7)*((-1+s)*(t))+(7)*(pow(t,2))))));
  phit[33] = (60)*((sqrt(22))*((1+(7)*((s)*(-4+(15)*(s))))*(pow(-1+s,4)+(14)*((pow(-1+s,3))*(t))+(56)*((pow(-1+s,2))*(pow(t,2)))
             +(84)*((-1+s)*(pow(t,3)))+(42)*(pow(t,4)))));
  phit[34] = (84)*((sqrt(26))*((-1+(15)*(s))*(pow(-1+s,5)+(20)*((pow(-1+s,4))*(t))+(120)*((pow(-1+s,3))*(pow(t,2)))+(300)*((pow(-1+s,2))*(pow(t,3)))
             +(330)*((-1+s)*(pow(t,4)))+(132)*(pow(t,5)))));
  phit[35] = (112)*((sqrt(30))*(pow(-1+s,6)+(27)*((pow(-1+s,5))*(t))+(225)*((pow(-1+s,4))*(pow(t,2)))+(825)*((pow(-1+s,3))*(pow(t,3)))
             +(1485)*((pow(-1+s,2))*(pow(t,4)))+(1287)*((-1+s)*(pow(t,5)))+(429)*(pow(t,6))));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Triangle::areaRef, 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Triangle::centerRef[0], 1./3., 1e-12 );
  BOOST_CHECK_CLOSE( Triangle::centerRef[1], 1./3., 1e-12 );

  BOOST_CHECK( Triangle::TopoDim::D == topoDim(eTriangle) );
  BOOST_CHECK( Triangle::NNode == topoNNode(eTriangle) );

  BOOST_CHECK( BasisFunctionAreaBase<Triangle>::D == 2 );

  BOOST_CHECK( (BasisFunctionArea<Triangle,Legendre,0>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Legendre,1>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Legendre,2>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Legendre,3>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Legendre,4>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Legendre,5>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Legendre,6>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Legendre,7>::D == 2) );

  BOOST_CHECK( BasisFunctionArea_Triangle_LegendrePMax == 7 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Triangle>::getBasisFunction(-1, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Triangle>::getBasisFunction( 8, BasisFunctionCategory_Legendre), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP0 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(0, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Triangle>::LegendreP0 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionArea<Triangle,Legendre,0>::self();

  BOOST_CHECK_EQUAL( 0, basis->order() );
  BOOST_CHECK_EQUAL( 1, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 1, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t;
  Real phi[1], phis[1], phit[1];
  Real phiTrue[1], phisTrue[1], phitTrue[1];
  Int3 edgesign = {{+1, +1, +1}};
  int k;

  for (int test = 0; test < 4; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 1./3.;  t = 1./3.;
      break;
    }

    fillTrueValues(0, s, t, phiTrue, phisTrue, phitTrue);

    basis->evalBasis( s, t, edgesign, phi, 1 );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, 1 );
    for (k = 0; k < 1; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP1 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(1, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Triangle>::LegendreP1 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionArea<Triangle,Legendre,1>::self();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 3, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 3, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t;
  Real phi[3], phis[3], phit[3];
  Real phiTrue[3], phisTrue[3], phitTrue[3];
  Int3 edgesign = {{+1, +1, +1}};
  int k;

  for (int test = 0; test < 4; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 1./3.;  t = 1./3.;
      break;
    }

    fillTrueValues(1, s, t, phiTrue, phisTrue, phitTrue);

    basis->evalBasis( s, t, edgesign, phi, 3 );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, 3 );
    for (k = 0; k < 3; k++)
    {
      BOOST_CHECK_CLOSE( phiTrue[k], phi[k], tol );
      BOOST_CHECK_CLOSE( phisTrue[k], phis[k], tol );
      BOOST_CHECK_CLOSE( phitTrue[k], phit[k], tol );
    }
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP2 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(2, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Triangle>::LegendreP2 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionArea<Triangle,Legendre,2>::self();

  const int nBasis = 6;

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};

  for (int test = 0; test < 4; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 1./3.;  t = 1./3.;
      break;
    }

    fillTrueValues(2, s, t, phiTrue, phisTrue, phitTrue);

    basis->evalBasis( s, t, edgesign, phi, nBasis );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );

    for (int k = 0; k < nBasis; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol);
    }
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP3 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(3, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Triangle>::LegendreP3 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionArea<Triangle,Legendre,3>::self();

  const int nBasis = 10;

  BOOST_CHECK_EQUAL( 3, basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};

  for (int test = 0; test < 4; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 1./3.;  t = 1./3.;
      break;
    }

    fillTrueValues(3, s, t, phiTrue, phisTrue, phitTrue);

    basis->evalBasis( s, t, edgesign, phi, nBasis );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );

    for (int k = 0; k < nBasis; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol);
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP4 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 5e-12;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(4, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Triangle>::LegendreP4 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionArea<Triangle,Legendre,4>::self();

  const int nBasis = 15;

  BOOST_CHECK_EQUAL( 4, basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};

  for (int test = 0; test < 4; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 1./3.;  t = 1./3.;
      break;
    }

    fillTrueValues(4, s, t, phiTrue, phisTrue, phitTrue);

    basis->evalBasis( s, t, edgesign, phi, nBasis );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );

    for (int k = 0; k < nBasis; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol);
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP5 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(5, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Triangle>::LegendreP5 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP5;

  const int nBasis = 21;

  BOOST_CHECK_EQUAL( 5, basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};

  for (int test = 0; test < 4; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 1./3.;  t = 1./3.;
      break;
    }

    fillTrueValues(5, s, t, phiTrue, phisTrue, phitTrue);

    basis->evalBasis( s, t, edgesign, phi, nBasis );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );

    for (int k = 0; k < nBasis; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol);
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP6 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-10;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(6, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Triangle>::LegendreP6 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP6;

  const int nBasis = 28;

  BOOST_CHECK_EQUAL( 6, basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};

  for (int test = 0; test < 4; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 1./3.;  t = 1./3.;
      break;
    }

    fillTrueValues(6, s, t, phiTrue, phisTrue, phitTrue);

    basis->evalBasis( s, t, edgesign, phi, nBasis );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );

    for (int k = 0; k < nBasis; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol);
    }
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP7 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 5e-12;
  const Real close_tol = 5e-10;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(7, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Triangle>::LegendreP7 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP7;

  const int nBasis = 36;

  BOOST_CHECK_EQUAL( 7, basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};

  for (int test = 0; test < 4; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 1./3.;  t = 1./3.;
      break;
    }

    fillTrueValues(7, s, t, phiTrue, phisTrue, phitTrue);

    basis->evalBasis( s, t, edgesign, phi, nBasis );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );

    for (int k = 0; k < nBasis; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol);
    }
  }
}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP0 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 1e-13;

  int order;
  int nquad;
  Real s, t;
  Real phi[1];
  Real w, sum;
  Int3 edgesign = {{+1, +1, +1}};

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP0;

  order = 0;
  QuadratureArea<Triangle> quad(order);
  BOOST_CHECK( quad.order() >= 0 );

  nquad = quad.nQuadrature();

  sum = 0;
  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, edgesign, phi, 1 );

    sum += w*phi[0]*phi[0];
  }

  BOOST_CHECK_CLOSE( 1.0, sum, tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP1 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 1e-13;

  int order;
  int nquad;
  Real s, t;
  Real phi[3];
  Real w, sum[6];
  Int3 edgesign = {{+1, +1, +1}};

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP1;

  order = 2;
  QuadratureArea<Triangle> quad(order);
  BOOST_CHECK( quad.order() >= 2 );

  nquad = quad.nQuadrature();

  for (int k = 0; k < 6; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, edgesign, phi, 3 );

    sum[0] += w*phi[0]*phi[0];
    sum[1] += w*phi[1]*phi[1];
    sum[2] += w*phi[2]*phi[2];
    sum[3] += w*phi[0]*phi[1];
    sum[4] += w*phi[0]*phi[2];
    sum[5] += w*phi[1]*phi[2];
  }

  BOOST_CHECK_CLOSE( sum[0], 1.0, tol );
  BOOST_CHECK_CLOSE( sum[1], 1.0, tol );
  BOOST_CHECK_CLOSE( sum[2], 1.0, tol );
  BOOST_CHECK_SMALL( sum[3], tol );
  BOOST_CHECK_SMALL( sum[4], tol );
  BOOST_CHECK_SMALL( sum[5], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP2 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 2e-11;

  const int nB = 6;

  int order;
  int nquad;
  Real s, t;
  Real phi[nB];
  Real w, sum[nB*(nB + 1)/2];
  int i, k, n, offset;
  Int3 edgesign = {{+1, +1, +1}};

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP2;

  order = 4;
  QuadratureArea<Triangle> quad(order);
  BOOST_CHECK( quad.order() >= 4 );

  nquad = quad.nQuadrature();

  for (k = 0; k < nB*(nB + 1)/2; k++)
    sum[k] = 0;

  for (n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, edgesign, phi, nB );

    for (k = 0; k < nB; k++)
      sum[k] += w*phi[k]*phi[k];
    offset = nB;

    for (i = 0; i < nB-1; i++)
    {
      for (k = i+1; k < nB; k++)
        sum[offset + k - (i+1)] += w*phi[i]*phi[k];
      offset += nB - (i+1);
    }
  }

  for (int k = 0; k < nB; k++)
    BOOST_CHECK_CLOSE( sum[k], 1.0, tol );

  for (int k = nB; k < nB*(nB + 1)/2; k++)
    BOOST_CHECK_SMALL( sum[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP3 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 2e-11;

  const int nB = 10;

  int order;
  int nquad;
  Real s, t;
  Real phi[nB];
  Real w, sum[nB*(nB + 1)/2];
  int i, k, n, offset;
  Int3 edgesign = {{+1, +1, +1}};

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP3;

  order = 6;
  QuadratureArea<Triangle> quad(order);
  BOOST_CHECK( quad.order() >= 6 );

  nquad = quad.nQuadrature();

  for (k = 0; k < nB*(nB + 1)/2; k++)
    sum[k] = 0;

  for (n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, edgesign, phi, nB );

    for (k = 0; k < nB; k++)
      sum[k] += w*phi[k]*phi[k];
    offset = nB;

    for (i = 0; i < nB-1; i++)
    {
      for (k = i+1; k < nB; k++)
        sum[offset + k - (i+1)] += w*phi[i]*phi[k];
      offset += nB - (i+1);
    }
  }

  for (int k = 0; k < nB; k++)
    BOOST_CHECK_CLOSE( sum[k], 1.0, tol );

  for (int k = nB; k < nB*(nB + 1)/2; k++)
    BOOST_CHECK_SMALL( sum[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP4 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 2e-11;

  const int nB = 15;

  int order;
  int nquad;
  Real s, t;
  Real phi[nB];
  Real w, sum[nB*(nB + 1)/2];
  int i, k, n, offset;
  Int3 edgesign = {{+1, +1, +1}};

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP4;

  order = 8;
  QuadratureArea<Triangle> quad(order);
  BOOST_CHECK( quad.order() >= 8 );

  nquad = quad.nQuadrature();

  for (k = 0; k < nB*(nB + 1)/2; k++)
    sum[k] = 0;

  for (n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, edgesign, phi, nB );

    for (k = 0; k < nB; k++)
      sum[k] += w*phi[k]*phi[k];
    offset = nB;

    for (i = 0; i < nB-1; i++)
    {
      for (k = i+1; k < nB; k++)
        sum[offset + k - (i+1)] += w*phi[i]*phi[k];
      offset += nB - (i+1);
    }
  }

  for (int k = 0; k < nB; k++)
    BOOST_CHECK_CLOSE( sum[k], 1.0, tol );

  for (int k = nB; k < nB*(nB + 1)/2; k++)
    BOOST_CHECK_SMALL( sum[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP5 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 2e-11;

  const int nB = 21;

  int order;
  int nquad;
  Real s, t;
  Real phi[nB];
  Real w, sum[nB*(nB + 1)/2];
  int i, k, n, offset;
  Int3 edgesign = {{+1, +1, +1}};

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP5;

  order = 10;
  QuadratureArea<Triangle> quad(order);
  BOOST_CHECK( quad.order() >= 10 );

  nquad = quad.nQuadrature();

  for (k = 0; k < nB*(nB + 1)/2; k++)
    sum[k] = 0;

  for (n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, edgesign, phi, nB );

    for (k = 0; k < nB; k++)
      sum[k] += w*phi[k]*phi[k];
    offset = nB;

    for (i = 0; i < nB-1; i++)
    {
      for (k = i+1; k < nB; k++)
        sum[offset + k - (i+1)] += w*phi[i]*phi[k];
      offset += nB - (i+1);
    }
  }

  for (int k = 0; k < nB; k++)
    BOOST_CHECK_CLOSE( sum[k], 1.0, tol );

  for (int k = nB; k < nB*(nB + 1)/2; k++)
    BOOST_CHECK_SMALL( sum[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP6 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 5e-11;

  const int nB = 28;

  int order;
  int nquad;
  Real s, t;
  Real phi[nB];
  Real w, sum[nB*(nB + 1)/2];
  int i, k, n, offset;
  Int3 edgesign = {{+1, +1, +1}};

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP6;

  order = 12;
  QuadratureArea<Triangle> quad(order);
  BOOST_CHECK( quad.order() >= 12 );

  nquad = quad.nQuadrature();

  for (k = 0; k < nB*(nB + 1)/2; k++)
    sum[k] = 0;

  for (n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, edgesign, phi, nB );

    for (k = 0; k < nB; k++)
      sum[k] += w*phi[k]*phi[k];
    offset = nB;

    for (i = 0; i < nB-1; i++)
    {
      for (k = i+1; k < nB; k++)
        sum[offset + k - (i+1)] += w*phi[i]*phi[k];
      offset += nB - (i+1);
    }
  }

  for (int k = 0; k < nB; k++)
    BOOST_CHECK_CLOSE( sum[k], 1.0, tol );

  for (int k = nB; k < nB*(nB + 1)/2; k++)
    BOOST_CHECK_SMALL( sum[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP7 )
{
  typedef std::array<int,3> Int3;

  const Real tol = 1e-10;

  const int nB = 36;

  int order;
  int nquad;
  Real s, t;
  Real phi[nB];
  Real w, sum[nB*(nB + 1)/2];
  int i, k, n, offset;
  Int3 edgesign = {{+1, +1, +1}};

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::LegendreP7;

  order = 14;
  QuadratureArea<Triangle> quad(order);
  BOOST_CHECK( quad.order() >= 14 );

  nquad = quad.nQuadrature();

  for (k = 0; k < nB*(nB + 1)/2; k++)
    sum[k] = 0;

  for (n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, edgesign, phi, nB );

    for (k = 0; k < nB; k++)
      sum[k] += w*phi[k]*phi[k];
    offset = nB;

    for (i = 0; i < nB-1; i++)
    {
      for (k = i+1; k < nB; k++)
        sum[offset + k - (i+1)] += w*phi[i]*phi[k];
      offset += nB - (i+1);
    }
  }

  for (int k = 0; k < nB; k++)
    BOOST_CHECK_CLOSE( sum[k], 1.0, tol );

  for (int k = nB; k < nB*(nB + 1)/2; k++)
    BOOST_CHECK_SMALL( sum[k], tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionArea_Triangle_Legendre_pattern.txt", true );

  BasisFunctionAreaBase<Triangle>::getBasisFunction(0, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(1, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(2, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(3, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(4, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(5, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(6, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Triangle>::getBasisFunction(7, BasisFunctionCategory_Legendre)->dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
