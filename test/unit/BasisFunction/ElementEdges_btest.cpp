// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "BasisFunction/ElementEdges.h"
#include "SANS_btest.h"

#include "tools/SANSException.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementEdges_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_test )
{
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Triangle>::EdgeNodes;

  BOOST_CHECK_EQUAL( 1, EdgeNodes[0][0] ); BOOST_CHECK_EQUAL( 2, EdgeNodes[0][1] ); // Edge 0
  BOOST_CHECK_EQUAL( 2, EdgeNodes[1][0] ); BOOST_CHECK_EQUAL( 0, EdgeNodes[1][1] ); // Edge 1
  BOOST_CHECK_EQUAL( 0, EdgeNodes[2][0] ); BOOST_CHECK_EQUAL( 1, EdgeNodes[2][1] ); // Edge 2

  const std::vector<int> cell = {20, 21, 22};
  for (int i = 0; i < Triangle::NEdge; i++)
  {
    {
      const int edgeP[Line::NNode] = {cell[EdgeNodes[i][0]], cell[EdgeNodes[i][1]]};
      const int edgeN[Line::NNode] = {cell[EdgeNodes[i][1]], cell[EdgeNodes[i][0]]};
      BOOST_CHECK_EQUAL( i, ElementEdges<Triangle>::getCanonicalEdge(edgeP, Line::NNode, cell.data(), Triangle::NNode) );
      BOOST_CHECK_EQUAL( i, ElementEdges<Triangle>::getCanonicalEdge(edgeN, Line::NNode, cell.data(), Triangle::NNode) );
    }

    {
      const std::vector<int> edgeP = {cell[EdgeNodes[i][0]], cell[EdgeNodes[i][1]]};
      const std::vector<int> edgeN = {cell[EdgeNodes[i][1]], cell[EdgeNodes[i][0]]};
      BOOST_CHECK_EQUAL( i, ElementEdges<Triangle>::getCanonicalEdge(edgeP, cell) );
      BOOST_CHECK_EQUAL( i, ElementEdges<Triangle>::getCanonicalEdge(edgeN, cell) );
    }
  }

  // check the runtime version
  const int (*EdgeNodes2)[ Line::NNode ] = nullptr;
  int nEdge;
  int NEdge = Triangle::NEdge;

  elementEdges(eTriangle, EdgeNodes2, nEdge);

  BOOST_CHECK_EQUAL(EdgeNodes, EdgeNodes2);
  BOOST_CHECK_EQUAL(NEdge, nEdge);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_test )
{
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Quad>::EdgeNodes;

  BOOST_CHECK_EQUAL( 0, EdgeNodes[0][0] ); BOOST_CHECK_EQUAL( 1, EdgeNodes[0][1] ); // Edge 0
  BOOST_CHECK_EQUAL( 1, EdgeNodes[1][0] ); BOOST_CHECK_EQUAL( 2, EdgeNodes[1][1] ); // Edge 1
  BOOST_CHECK_EQUAL( 2, EdgeNodes[2][0] ); BOOST_CHECK_EQUAL( 3, EdgeNodes[2][1] ); // Edge 2
  BOOST_CHECK_EQUAL( 3, EdgeNodes[3][0] ); BOOST_CHECK_EQUAL( 0, EdgeNodes[3][1] ); // Edge 3

  const std::vector<int> cell = {20, 21, 22, 23};
  for (int i = 0; i < Quad::NEdge; i++)
  {
    {
      const int edgeP[Line::NNode] = {cell[EdgeNodes[i][0]], cell[EdgeNodes[i][1]]};
      const int edgeN[Line::NNode] = {cell[EdgeNodes[i][1]], cell[EdgeNodes[i][0]]};
      BOOST_CHECK_EQUAL( i, ElementEdges<Quad>::getCanonicalEdge(edgeP, Line::NNode, cell.data(), Quad::NNode) );
      BOOST_CHECK_EQUAL( i, ElementEdges<Quad>::getCanonicalEdge(edgeN, Line::NNode, cell.data(), Quad::NNode) );
    }

    {
      const std::vector<int> edgeP = {cell[EdgeNodes[i][0]], cell[EdgeNodes[i][1]]};
      const std::vector<int> edgeN = {cell[EdgeNodes[i][1]], cell[EdgeNodes[i][0]]};
      BOOST_CHECK_EQUAL( i, ElementEdges<Quad>::getCanonicalEdge(edgeP, cell) );
      BOOST_CHECK_EQUAL( i, ElementEdges<Quad>::getCanonicalEdge(edgeN, cell) );
    }
  }

  // check the runtime version
  const int (*EdgeNodes2)[ Line::NNode ] = nullptr;
  int nEdge;
  int NEdge = Quad::NEdge;

  elementEdges(eQuad, EdgeNodes2, nEdge);

  BOOST_CHECK_EQUAL(EdgeNodes, EdgeNodes2);
  BOOST_CHECK_EQUAL(NEdge, nEdge);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tetrahedron_test )
{
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Tet>::EdgeNodes;

  BOOST_CHECK_EQUAL( 2, EdgeNodes[0][0] ); BOOST_CHECK_EQUAL( 3, EdgeNodes[0][1] ); // Edge 0
  BOOST_CHECK_EQUAL( 3, EdgeNodes[1][0] ); BOOST_CHECK_EQUAL( 1, EdgeNodes[1][1] ); // Edge 1
  BOOST_CHECK_EQUAL( 1, EdgeNodes[2][0] ); BOOST_CHECK_EQUAL( 2, EdgeNodes[2][1] ); // Edge 2
  BOOST_CHECK_EQUAL( 2, EdgeNodes[3][0] ); BOOST_CHECK_EQUAL( 0, EdgeNodes[3][1] ); // Edge 3
  BOOST_CHECK_EQUAL( 0, EdgeNodes[4][0] ); BOOST_CHECK_EQUAL( 3, EdgeNodes[4][1] ); // Edge 4
  BOOST_CHECK_EQUAL( 0, EdgeNodes[5][0] ); BOOST_CHECK_EQUAL( 1, EdgeNodes[5][1] ); // Edge 5


  const std::vector<int> cell = {20, 21, 22, 23};
  for (int i = 0; i < Tet::NEdge; i++)
  {
    {
      const int edgeP[Line::NNode] = {cell[EdgeNodes[i][0]], cell[EdgeNodes[i][1]]};
      const int edgeN[Line::NNode] = {cell[EdgeNodes[i][1]], cell[EdgeNodes[i][0]]};
      BOOST_CHECK_EQUAL( i, ElementEdges<Tet>::getCanonicalEdge(edgeP, Line::NNode, cell.data(), Tet::NNode) );
      BOOST_CHECK_EQUAL( i, ElementEdges<Tet>::getCanonicalEdge(edgeN, Line::NNode, cell.data(), Tet::NNode) );
    }

    {
      const std::vector<int> edgeP = {cell[EdgeNodes[i][0]], cell[EdgeNodes[i][1]]};
      const std::vector<int> edgeN = {cell[EdgeNodes[i][1]], cell[EdgeNodes[i][0]]};
      BOOST_CHECK_EQUAL( i, ElementEdges<Tet>::getCanonicalEdge(edgeP, cell) );
      BOOST_CHECK_EQUAL( i, ElementEdges<Tet>::getCanonicalEdge(edgeN, cell) );
    }
  }

  // check the runtime version
  const int (*EdgeNodes2)[ Line::NNode ] = nullptr;
  int nEdge;
  int NEdge = Tet::NEdge;

  elementEdges(eTet, EdgeNodes2, nEdge);

  BOOST_CHECK_EQUAL(EdgeNodes, EdgeNodes2);
  BOOST_CHECK_EQUAL(NEdge, nEdge);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hexahedron_test )
{
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Hex>::EdgeNodes;

  BOOST_CHECK_EQUAL( 0, EdgeNodes[0][0]  ); BOOST_CHECK_EQUAL( 3, EdgeNodes[0][1]  ); // Edge 0
  BOOST_CHECK_EQUAL( 3, EdgeNodes[1][0]  ); BOOST_CHECK_EQUAL( 2, EdgeNodes[1][1]  ); // Edge 1
  BOOST_CHECK_EQUAL( 2, EdgeNodes[2][0]  ); BOOST_CHECK_EQUAL( 1, EdgeNodes[2][1]  ); // Edge 2
  BOOST_CHECK_EQUAL( 1, EdgeNodes[3][0]  ); BOOST_CHECK_EQUAL( 0, EdgeNodes[3][1]  ); // Edge 3
  BOOST_CHECK_EQUAL( 1, EdgeNodes[4][0]  ); BOOST_CHECK_EQUAL( 5, EdgeNodes[4][1]  ); // Edge 4
  BOOST_CHECK_EQUAL( 5, EdgeNodes[5][0]  ); BOOST_CHECK_EQUAL( 4, EdgeNodes[5][1]  ); // Edge 5
  BOOST_CHECK_EQUAL( 4, EdgeNodes[6][0]  ); BOOST_CHECK_EQUAL( 0, EdgeNodes[6][1]  ); // Edge 6
  BOOST_CHECK_EQUAL( 2, EdgeNodes[7][0]  ); BOOST_CHECK_EQUAL( 6, EdgeNodes[7][1]  ); // Edge 7
  BOOST_CHECK_EQUAL( 6, EdgeNodes[8][0]  ); BOOST_CHECK_EQUAL( 5, EdgeNodes[8][1]  ); // Edge 8
  BOOST_CHECK_EQUAL( 4, EdgeNodes[9][0]  ); BOOST_CHECK_EQUAL( 7, EdgeNodes[9][1]  ); // Edge 9
  BOOST_CHECK_EQUAL( 3, EdgeNodes[10][0] ); BOOST_CHECK_EQUAL( 7, EdgeNodes[10][1] ); // Edge 10
  BOOST_CHECK_EQUAL( 7, EdgeNodes[11][0] ); BOOST_CHECK_EQUAL( 6, EdgeNodes[11][1] ); // Edge 11



  const std::vector<int> cell = {20, 21, 22, 23,
                                 30, 31, 32, 33};
  for (int i = 0; i < Hex::NEdge; i++)
  {
    {
      const int edgeP[Line::NNode] = {cell[EdgeNodes[i][0]], cell[EdgeNodes[i][1]]};
      const int edgeN[Line::NNode] = {cell[EdgeNodes[i][1]], cell[EdgeNodes[i][0]]};
      BOOST_CHECK_EQUAL( i, ElementEdges<Hex>::getCanonicalEdge(edgeP, Line::NNode, cell.data(), Hex::NNode) );
      BOOST_CHECK_EQUAL( i, ElementEdges<Hex>::getCanonicalEdge(edgeN, Line::NNode, cell.data(), Hex::NNode) );
    }

    {
      const std::vector<int> edgeP = {cell[EdgeNodes[i][0]], cell[EdgeNodes[i][1]]};
      const std::vector<int> edgeN = {cell[EdgeNodes[i][1]], cell[EdgeNodes[i][0]]};
      BOOST_CHECK_EQUAL( i, ElementEdges<Hex>::getCanonicalEdge(edgeP, cell) );
      BOOST_CHECK_EQUAL( i, ElementEdges<Hex>::getCanonicalEdge(edgeN, cell) );
    }
  }

  // check the runtime version
  const int (*EdgeNodes2)[ Line::NNode ] = nullptr;
  int nEdge;
  int NEdge = Hex::NEdge;

  elementEdges(eHex, EdgeNodes2, nEdge);

  BOOST_CHECK_EQUAL(EdgeNodes, EdgeNodes2);
  BOOST_CHECK_EQUAL(NEdge, nEdge);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Pentatope_test)
{
  const int (*EdgeNodes)[Line::NNode]= ElementEdges<Pentatope>::EdgeNodes;

  BOOST_CHECK_EQUAL(0, EdgeNodes[0][0]);    BOOST_CHECK_EQUAL(1, EdgeNodes[0][1]);    // edge 0
  BOOST_CHECK_EQUAL(0, EdgeNodes[1][0]);    BOOST_CHECK_EQUAL(2, EdgeNodes[1][1]);    // edge 1
  BOOST_CHECK_EQUAL(0, EdgeNodes[2][0]);    BOOST_CHECK_EQUAL(3, EdgeNodes[2][1]);    // edge 2
  BOOST_CHECK_EQUAL(0, EdgeNodes[3][0]);    BOOST_CHECK_EQUAL(4, EdgeNodes[3][1]);    // edge 3
  BOOST_CHECK_EQUAL(1, EdgeNodes[4][0]);    BOOST_CHECK_EQUAL(2, EdgeNodes[4][1]);    // edge 4
  BOOST_CHECK_EQUAL(1, EdgeNodes[5][0]);    BOOST_CHECK_EQUAL(3, EdgeNodes[5][1]);    // edge 5
  BOOST_CHECK_EQUAL(1, EdgeNodes[6][0]);    BOOST_CHECK_EQUAL(4, EdgeNodes[6][1]);    // edge 6
  BOOST_CHECK_EQUAL(2, EdgeNodes[7][0]);    BOOST_CHECK_EQUAL(3, EdgeNodes[7][1]);    // edge 7
  BOOST_CHECK_EQUAL(2, EdgeNodes[8][0]);    BOOST_CHECK_EQUAL(4, EdgeNodes[8][1]);    // edge 8
  BOOST_CHECK_EQUAL(3, EdgeNodes[9][0]);    BOOST_CHECK_EQUAL(4, EdgeNodes[9][1]);    // edge 9

  const std::vector<int> cell= {20, 21, 22, 23, 24};

  for (int i= 0; i < Pentatope::NEdge; i++)
  {
    {
      const int edgeP[Line::NNode]= {cell[EdgeNodes[i][0]], cell[EdgeNodes[i][1]]};
      const int edgeN[Line::NNode]= {cell[EdgeNodes[i][1]], cell[EdgeNodes[i][0]]};
      BOOST_CHECK_EQUAL(i, ElementEdges<Pentatope>::getCanonicalEdge(edgeP, Line::NNode, cell.data(), Pentatope::NNode));
      BOOST_CHECK_EQUAL(i, ElementEdges<Pentatope>::getCanonicalEdge(edgeN, Line::NNode, cell.data(), Pentatope::NNode));
    }

    {
      const std::vector<int> edgeP= {cell[EdgeNodes[i][0]], cell[EdgeNodes[i][1]]};
      const std::vector<int> edgeN= {cell[EdgeNodes[i][1]], cell[EdgeNodes[i][0]]};
      BOOST_CHECK_EQUAL(i, ElementEdges<Pentatope>::getCanonicalEdge(edgeP, cell));
      BOOST_CHECK_EQUAL(i, ElementEdges<Pentatope>::getCanonicalEdge(edgeN, cell));
    }
  }

  // runtime version
  const int (*EdgeNodes2electricBoogaloo)[Line::NNode]= nullptr;
  int nEdge;
  int NEdge= Pentatope::NEdge;

  elementEdges(ePentatope, EdgeNodes2electricBoogaloo, nEdge);

  BOOST_CHECK_EQUAL(EdgeNodes, EdgeNodes2electricBoogaloo);
  BOOST_CHECK_EQUAL(NEdge, nEdge);

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
