// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionVolume_Tetranhedron_Lagrange_btest
// testing of BasisFunctionVolume<Tet,Lagrange> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionVolume.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunction/LagrangeDOFMap.h"
#include "BasisFunctionVolume_Tetrahedron_Lagrange_known.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionVolume_Tetrahedron_Lagrange_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Tet::volumeRef, 1./6., 1e-12 );
  BOOST_CHECK_CLOSE( Tet::centerRef[0], 1./4., 1e-12 );
  BOOST_CHECK_CLOSE( Tet::centerRef[1], 1./4., 1e-12 );
  BOOST_CHECK_CLOSE( Tet::centerRef[2], 1./4., 1e-12 );

  BOOST_CHECK( Tet::TopoDim::D == topoDim(eTet) );
  BOOST_CHECK( Tet::NNode == topoNNode(eTet) );

  BOOST_CHECK( BasisFunctionVolumeBase<Tet>::D == 3 );

  BOOST_CHECK( (BasisFunctionVolume<Tet,Lagrange,1>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Tet,Lagrange,2>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Tet,Lagrange,3>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Tet,Lagrange,4>::D == 3) );

  BOOST_CHECK( BasisFunctionVolume_Tet_LagrangePMax == 4 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionVolumeBase<Tet>::getBasisFunction( 0, BasisFunctionCategory_Lagrange ), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionVolumeBase<Tet>::getBasisFunction( BasisFunctionVolume_Tet_LagrangePMax+1,
                                                                     BasisFunctionCategory_Lagrange ), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP1 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 1;
  const int nBasis = (order + 1)*(order + 2)*(order + 3)/6;
  const int nNode  = 4;
  const int nEdge  = 6*(order - 1);
  const int nFace  = 4*(order - 1)*(order - 2)/2;
  const int nCell  = (order - 1)*(order - 2)*(order - 3)/6;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(order, BasisFunctionCategory_Lagrange),
                     BasisFunctionVolumeBase<Tet>::LagrangeP1 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::LagrangeP1;

  BOOST_CHECK_EQUAL( order , basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode , basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge , basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nFace , basis->nBasisFace() );
  BOOST_CHECK_EQUAL( nCell , basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s, t, u;
  Real phi[nBasis], phis[nBasis], phit[nBasis], phiu[nBasis];
  Real phiss[nBasis],
       phist[nBasis], phitt[nBasis],
       phisu[nBasis], phitu[nBasis], phiuu[nBasis];

  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis], phiuTrue[nBasis];
  Real phissTrue[nBasis],
       phistTrue[nBasis], phittTrue[nBasis],
       phisuTrue[nBasis], phituTrue[nBasis], phiuuTrue[nBasis];
  Int4 facesign = {{+1, +1, +1, +1}};

  {
  s = .25;  t = .5;  u = .2;
  lagrange_tet_p1_test1_true(phiTrue,
                             phisTrue, phitTrue, phiuTrue,
                             phissTrue,
                             phistTrue, phittTrue,
                             phisuTrue, phituTrue, phiuuTrue);

  basis->evalBasis( s, t, u, facesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, nBasis );
  basis->evalBasisHessianDerivative( s, t, u, facesign,
                                     phiss,
                                     phist, phitt,
                                     phisu, phitu, phiuu, nBasis );

  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki]  , phi[ki]  , small_tol, close_tol );

    SANS_CHECK_CLOSE( phisTrue[ki] , phis[ki] , small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki] , phit[ki] , small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuTrue[ki] , phiu[ki] , small_tol, close_tol );

    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisuTrue[ki], phisu[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phituTrue[ki], phitu[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuuTrue[ki], phiuu[ki], small_tol, close_tol );
  }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(nBasis), coord_t(nBasis), coord_u(nBasis);
  BasisFunctionVolumeBase<Tet>::LagrangeP1->coordinates( coord_s, coord_t, coord_u );

  for (int n = 0; n < nBasis; n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], coord_u[n], facesign, phi, nBasis );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP2 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 2;
  const int nBasis = (order + 1)*(order + 2)*(order + 3)/6;
  const int nNode  = 4;
  const int nEdge  = 6*(order - 1);
  const int nFace  = 4*(order - 1)*(order - 2)/2;
  const int nCell  = (order - 1)*(order - 2)*(order - 3)/6;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(order, BasisFunctionCategory_Lagrange),
                     BasisFunctionVolumeBase<Tet>::LagrangeP2 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::LagrangeP2;

  BOOST_CHECK_EQUAL( order , basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode , basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge , basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nFace , basis->nBasisFace() );
  BOOST_CHECK_EQUAL( nCell , basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s, t, u;
  Real phi[nBasis], phis[nBasis], phit[nBasis], phiu[nBasis];
  Real phiss[nBasis],
       phist[nBasis], phitt[nBasis],
       phisu[nBasis], phitu[nBasis], phiuu[nBasis];

  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis], phiuTrue[nBasis];
  Real phissTrue[nBasis],
       phistTrue[nBasis], phittTrue[nBasis],
       phisuTrue[nBasis], phituTrue[nBasis], phiuuTrue[nBasis];
  Int4 facesign = {{+1, +1, +1, +1}};

  {
  s = .25;  t = .5;  u = .2;
  lagrange_tet_p2_test1_true(phiTrue,
                             phisTrue, phitTrue, phiuTrue,
                             phissTrue,
                             phistTrue, phittTrue,
                             phisuTrue, phituTrue, phiuuTrue);

  basis->evalBasis( s, t, u, facesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, nBasis );
  basis->evalBasisHessianDerivative( s, t, u, facesign,
                                     phiss,
                                     phist, phitt,
                                     phisu, phitu, phiuu, nBasis );

  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki]  , phi[ki]  , small_tol, close_tol );

    SANS_CHECK_CLOSE( phisTrue[ki] , phis[ki] , small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki] , phit[ki] , small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuTrue[ki] , phiu[ki] , small_tol, close_tol );

    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisuTrue[ki], phisu[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phituTrue[ki], phitu[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuuTrue[ki], phiuu[ki], small_tol, close_tol );
  }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(nBasis), coord_t(nBasis), coord_u(nBasis);
  BasisFunctionVolumeBase<Tet>::LagrangeP2->coordinates( coord_s, coord_t, coord_u );

  for (int n = 0; n < nBasis; n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], coord_u[n], facesign, phi, nBasis );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP3 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 3;
  const int nBasis = (order + 1)*(order + 2)*(order + 3)/6;
  const int nNode  = 4;
  const int nEdge  = 6*(order - 1);
  const int nFace  = 4*(order - 1)*(order - 2)/2;
  const int nCell  = (order - 1)*(order - 2)*(order - 3)/6;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(order, BasisFunctionCategory_Lagrange),
                     BasisFunctionVolumeBase<Tet>::LagrangeP3 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::LagrangeP3;

  BOOST_CHECK_EQUAL( order , basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode , basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge , basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nFace , basis->nBasisFace() );
  BOOST_CHECK_EQUAL( nCell , basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s, t, u;
  Real phi[nBasis], phis[nBasis], phit[nBasis], phiu[nBasis];
  Real phiss[nBasis],
       phist[nBasis], phitt[nBasis],
       phisu[nBasis], phitu[nBasis], phiuu[nBasis];

  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis], phiuTrue[nBasis];
  Real phissTrue[nBasis],
       phistTrue[nBasis], phittTrue[nBasis],
       phisuTrue[nBasis], phituTrue[nBasis], phiuuTrue[nBasis];
  Int4 facesign = {{+1, +1, +1, +1}};

  {
  s = .25;  t = .5;  u = .2;
  lagrange_tet_p3_test1_true(phiTrue,
                             phisTrue, phitTrue, phiuTrue,
                             phissTrue,
                             phistTrue, phittTrue,
                             phisuTrue, phituTrue, phiuuTrue);

  basis->evalBasis( s, t, u, facesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, nBasis );
  basis->evalBasisHessianDerivative( s, t, u, facesign,
                                     phiss,
                                     phist, phitt,
                                     phisu, phitu, phiuu, nBasis );

  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki]  , phi[ki]  , small_tol, close_tol );

    SANS_CHECK_CLOSE( phisTrue[ki] , phis[ki] , small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki] , phit[ki] , small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuTrue[ki] , phiu[ki] , small_tol, close_tol );

    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisuTrue[ki], phisu[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phituTrue[ki], phitu[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuuTrue[ki], phiuu[ki], small_tol, close_tol );
  }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis()), coord_u(basis->nBasis());
  BasisFunctionVolumeBase<Tet>::LagrangeP3->coordinates( coord_s, coord_t, coord_u );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], coord_u[n], facesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LagrangeP4 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 5e-11;

  const int order  = 4;
  const int nBasis = (order + 1)*(order + 2)*(order + 3)/6;
  const int nNode  = 4;
  const int nEdge  = 6*(order - 1);
  const int nFace  = 4*(order - 1)*(order - 2)/2;
  const int nCell  = (order - 1)*(order - 2)*(order - 3)/6;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(order, BasisFunctionCategory_Lagrange),
                     BasisFunctionVolumeBase<Tet>::LagrangeP4 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::LagrangeP4;

  BOOST_CHECK_EQUAL( order , basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode , basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge , basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nFace , basis->nBasisFace() );
  BOOST_CHECK_EQUAL( nCell , basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Lagrange, basis->category() );

  Real s, t, u;
  Real phi[nBasis], phis[nBasis], phit[nBasis], phiu[nBasis];
  Real phiss[nBasis],
       phist[nBasis], phitt[nBasis],
       phisu[nBasis], phitu[nBasis], phiuu[nBasis];

  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis], phiuTrue[nBasis];
  Real phissTrue[nBasis],
       phistTrue[nBasis], phittTrue[nBasis],
       phisuTrue[nBasis], phituTrue[nBasis], phiuuTrue[nBasis];
  Int4 facesign = {{+1, +1, +1, +1}};

  {
  s = .25;  t = .5;  u = .2;
  lagrange_tet_p4_test1_true(phiTrue,
                             phisTrue, phitTrue, phiuTrue,
                             phissTrue,
                             phistTrue, phittTrue,
                             phisuTrue, phituTrue, phiuuTrue);

  basis->evalBasis( s, t, u, facesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, nBasis );
  basis->evalBasisHessianDerivative( s, t, u, facesign,
                                     phiss,
                                     phist, phitt,
                                     phisu, phitu, phiuu, nBasis );

  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki]  , phi[ki]  , small_tol, close_tol );

    SANS_CHECK_CLOSE( phisTrue[ki] , phis[ki] , small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki] , phit[ki] , small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuTrue[ki] , phiu[ki] , small_tol, close_tol );

    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisuTrue[ki], phisu[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phituTrue[ki], phitu[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuuTrue[ki], phiuu[ki], small_tol, close_tol );
  }
  }

  // Get the Lagrange coordinates where the basis functions should all be 1
  std::vector<Real> coord_s(basis->nBasis()), coord_t(basis->nBasis()), coord_u(basis->nBasis());
  BasisFunctionVolumeBase<Tet>::LagrangeP4->coordinates( coord_s, coord_t, coord_u );

  for (int n = 0; n < basis->nBasis(); n++)
  {
    basis->evalBasis( coord_s[n], coord_t[n], coord_u[n], facesign, phi, basis->nBasis() );

    for (int ki = 0; ki < basis->nBasis(); ki++)
      if (ki == n)
        BOOST_CHECK_CLOSE( 1, phi[ki], close_tol );
      else
        BOOST_CHECK_SMALL( phi[ki], small_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange_pattern.txt", true );

  BasisFunctionVolumeBase<Tet>::getBasisFunction(1, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionVolumeBase<Tet>::getBasisFunction(2, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionVolumeBase<Tet>::getBasisFunction(3, BasisFunctionCategory_Lagrange)->dump( 2, output );
  BasisFunctionVolumeBase<Tet>::getBasisFunction(4, BasisFunctionCategory_Lagrange)->dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
