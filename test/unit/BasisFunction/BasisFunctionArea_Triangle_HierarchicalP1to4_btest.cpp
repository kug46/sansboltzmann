// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionArea_Triangle_HierarchicalP1to4_btest
// testing of BasisFunctionArea<Triangle,Hierarchical> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Hierarchical.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;


namespace       // auxilliary copy functions
{

//----------------------------------------------------------------------------//
template <int nB, int nO>
void fillTrue1( int order, Real phiTrue[nB], Real phisTrue[nB], Real phitTrue[nB],
                Real phissTrue[nB], Real phistTrue[nB], Real phittTrue[nB],
                const Real phi[nO], const Real phis[nO], const Real phit[nO],
                const Real phiss[nO], const Real phist[nO], const Real phitt[nO] )
{
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nOrder = 3;
  int k;

  //cout << "fillTrue1: nBasis = " << nBasis << " nB = " << nB << " nOrder = " << nOrder << " nO = " << nO << endl;
  BOOST_REQUIRE_EQUAL( nBasis, nB );
  BOOST_REQUIRE_EQUAL( nOrder, nO );

  for (int ii = 0; ii < 3; ii++)
  {
    k = ii;
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }
}


//----------------------------------------------------------------------------//
template <int nB, int nO>
void fillTrue2( int order, Real phiTrue[nB], Real phisTrue[nB], Real phitTrue[nB],
                Real phissTrue[nB], Real phistTrue[nB], Real phittTrue[nB],
                const Real phi[nO], const Real phis[nO], const Real phit[nO],
                const Real phiss[nO], const Real phist[nO], const Real phitt[nO] )
{
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nOrder = 3;
  const int nEdge  = 3*(order - 1);
  int k;

  BOOST_REQUIRE_EQUAL( nBasis, nB );
  BOOST_REQUIRE_EQUAL( nOrder, nO );

  for (int ii = 0; ii < 3; ii++)
  {
    k = 3 + (nEdge/3)*ii;
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }
}


//----------------------------------------------------------------------------//
template <int nB, int nO>
void fillTrue3( int order, Real phiTrue[nB], Real phisTrue[nB], Real phitTrue[nB],
                Real phissTrue[nB], Real phistTrue[nB], Real phittTrue[nB],
                const Real phi[nO], const Real phis[nO], const Real phit[nO],
                const Real phiss[nO], const Real phist[nO], const Real phitt[nO] )
{
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nOrder = 4;
  const int nEdge  = 3*(order - 1);
  int k;

  BOOST_REQUIRE_EQUAL( nBasis, nB );
  BOOST_REQUIRE_EQUAL( nOrder, nO );

  // edge basis functions
  for (int ii = 0; ii < 3; ii++)
  {
    k = 3 + (nEdge/3)*ii + 1;
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }

  // interior basis function
  k = 3 + nEdge;
  phiTrue[k] = phi[3];  phisTrue[k] = phis[3];  phitTrue[k] = phit[3];
  phissTrue[k] = phiss[3];  phistTrue[k] = phist[3];  phittTrue[k] = phitt[3];
}


//----------------------------------------------------------------------------//
template <int nB, int nO>
void fillTrue4( int order, Real phiTrue[nB], Real phisTrue[nB], Real phitTrue[nB],
                Real phissTrue[nB], Real phistTrue[nB], Real phittTrue[nB],
                const Real phi[nO], const Real phis[nO], const Real phit[nO],
                const Real phiss[nO], const Real phist[nO], const Real phitt[nO] )
{
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nOrder = 5;
  const int nEdge  = 3*(order - 1);
  int k;

  BOOST_CHECK( (nBasis == nB) && (nOrder == nO) );

  // edge basis functions
  for (int ii = 0; ii < 3; ii++)
  {
    k = 3 + (nEdge/3)*ii + 2;
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }

  // interior basis functions
  for (int ii = 3; ii < 5; ii++)
  {
    k = 3 + nEdge + 1 + (ii - 3);
    phiTrue[k] = phi[ii];  phisTrue[k] = phis[ii];  phitTrue[k] = phit[ii];
    phissTrue[k] = phiss[ii];  phistTrue[k] = phist[ii];  phittTrue[k] = phitt[ii];
  }
}

}     // auxilliary copy functions


//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionArea_TriangleP1to4_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Triangle::areaRef, 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Triangle::centerRef[0], 1./3., 1e-12 );
  BOOST_CHECK_CLOSE( Triangle::centerRef[1], 1./3., 1e-12 );

  BOOST_CHECK( Triangle::TopoDim::D == topoDim(eTriangle) );
  BOOST_CHECK( Triangle::NNode == topoNNode(eTriangle) );

  BOOST_CHECK( BasisFunctionAreaBase<Triangle>::D == 2 );

  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,1>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,2>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,3>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,4>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,5>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,6>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Triangle,Hierarchical,7>::D == 2) );

  BOOST_CHECK( BasisFunctionArea_Triangle_HierarchicalPMax == 7 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Triangle>::getBasisFunction( 0, BasisFunctionCategory_Hierarchical ), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Triangle>::getBasisFunction( 8, BasisFunctionCategory_Hierarchical ), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP1 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 1;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Triangle>::HierarchicalP1 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::HierarchicalP1;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[3], phis[3], phit[3], phiss[3], phist[3], phitt[3];
  Real phiTrue[3], phisTrue[3], phitTrue[3];
  Real phissTrue[3], phistTrue[3], phittTrue[3];
  Int3 edgesign = {{+1, +1, +1}};

  {
  s = 0;  t = 0;
  Real   phi1[3] = { 1,  0,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 1;  t = 0;
  Real   phi1[3] = { 0,  1,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0;  t = 1;
  Real   phi1[3] = { 0,  0,  1};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 1./3.;  t = 1./3.;
  Real   phi1[3] = { 1/3.,  1/3.,  1/3.};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  // check sign change for asymmetric basis functions

  edgesign[0] = -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[1] = -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[2] = -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP2 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  const int order  = 2;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Triangle>::HierarchicalP2 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::HierarchicalP2;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[6], phis[6], phit[6], phiss[6], phist[6], phitt[6];
  Real phiTrue[6], phisTrue[6], phitTrue[6];
  Real phissTrue[6], phistTrue[6], phittTrue[6];
  Int3 edgesign = {{+1, +1, +1}};

  {
  s = 0;  t = 0;
  Real   phi1[3] = { 1,  0,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 0,  0,  4};
  Real  phit2[3] = { 0,  4,  0};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 1;  t = 0;
  Real   phi1[3] = { 0,  1,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 0,  0, -4};
  Real  phit2[3] = { 4,  0, -4};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0;  t = 1;
  Real   phi1[3] = { 0,  0,  1};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 4, -4,  0};
  Real  phit2[3] = { 0, -4,  0};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 1./3.;  t = 1./3.;
  Real   phi1[3] = { 1/3.,  1/3.,  1/3.};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 4/9.,  4/9.,   4/9.};
  Real  phis2[3] = { 4/3., -4/3.,      0};
  Real  phit2[3] = { 4/3.,     0,  -4/3.};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0.2;  t = 0.7;
  Real   phi1[3] = { 0.1, 0.2, 0.7};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0.56,  0.28,  0.08};
  Real  phis2[3] = { 2.8 , -2.8 , -0.4 };
  Real  phit2[3] = { 0.8,  -2.4 , -0.8 };
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  // check sign change for asymmetric basis functions

  edgesign[0] = -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[1] = -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[2] = -1;

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP3 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  const int order  = 3;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Triangle>::HierarchicalP3 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::HierarchicalP3;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiss[nBasis], phist[nBasis], phitt[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Real phissTrue[nBasis], phistTrue[nBasis], phittTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};

  {
  s = 0;  t = 0;
  Real   phi1[3] = { 1,  0,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 0,  0,  4};
  Real  phit2[3] = { 0,  4,  0};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = { 0,          0,           0,  0};
  Real  phis3[4] = { 0,          0,   6*sqrt(3),  0};
  Real  phit3[4] = { 0, -6*sqrt(3),           0,  0};
  Real phiss3[4] = { 0,          0, -36*sqrt(3),  0};
  Real phist3[4] = { 0, 12*sqrt(3), -12*sqrt(3), 27};
  Real phitt3[4] = { 0, 36*sqrt(3),           0,  0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 1;  t = 0;
  Real   phi1[3] = { 0,  1,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 0,  0, -4};
  Real  phit2[3] = { 4,  0, -4};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {          0, 0,          0,   0};
  Real  phis3[4] = {          0, 0,  6*sqrt(3),   0};
  Real  phit3[4] = {  6*sqrt(3), 0,  6*sqrt(3),   0};
  Real phiss3[4] = {          0, 0, 36*sqrt(3),   0};
  Real phist3[4] = { 12*sqrt(3), 0, 24*sqrt(3), -27};
  Real phitt3[4] = {-12*sqrt(3), 0, 12*sqrt(3), -54};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0;  t = 1;
  Real   phi1[3] = { 0,  0,  1};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 4, -4,  0};
  Real  phit2[3] = { 0, -4,  0};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {          0,           0, 0,   0};
  Real  phis3[4] = { -6*sqrt(3),  -6*sqrt(3), 0,   0};
  Real  phit3[4] = {          0,  -6*sqrt(3), 0,   0};
  Real phiss3[4] = { 12*sqrt(3), -12*sqrt(3), 0, -54};
  Real phist3[4] = {-12*sqrt(3), -24*sqrt(3), 0, -27};
  Real phitt3[4] = {          0, -36*sqrt(3), 0,   0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 1./3.;  t = 1./3.;
  Real   phi1[3] = { 1/3.,  1/3.,  1/3.};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 4/9.,  4/9.,   4/9.};
  Real  phis2[3] = { 4/3., -4/3.,      0};
  Real  phit2[3] = { 4/3.,     0,  -4/3.};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {         0,          0,          0,   1};
  Real  phis3[4] = { 2/sqrt(3),  2/sqrt(3), -4/sqrt(3),   0};
  Real  phit3[4] = {-2/sqrt(3),  4/sqrt(3), -2/sqrt(3),   0};
  Real phiss3[4] = { 4*sqrt(3), -4*sqrt(3),          0, -18};
  Real phist3[4] = {         0, -4*sqrt(3),  4*sqrt(3),  -9};
  Real phitt3[4] = {-4*sqrt(3),          0,  4*sqrt(3), -18};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0.2;  t = 0.7;
  Real   phi1[3] = { 0.1, 0.2, 0.7};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0.56,  0.28,  0.08};
  Real  phis2[3] = { 2.8 , -2.8 , -0.4 };
  Real  phit2[3] = { 0.8,  -2.4 , -0.8 };
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {-21*sqrt(3)/50.,   63*sqrt(3)/250., -3*sqrt(3)/250.,  189/500.};
  Real  phis3[4] = {-63*sqrt(3)/50.,  -21*sqrt(3)/10. , -9*sqrt(3)/50. , -189/100.};
  Real  phit3[4] = {-36*sqrt(3)/25.,  -33*sqrt(3)/25. ,               0,  -81/25. };
  Real phiss3[4] = { 42*sqrt(3)/5. ,  -42*sqrt(3)/5.  , 18*sqrt(3)/5.  , -189/5.  };
  Real phist3[4] = { -6*sqrt(3)    ,  -78*sqrt(3)/5.  , 18*sqrt(3)/5.  , -108/5.  };
  Real phitt3[4] = {-12*sqrt(3)/5. , -108*sqrt(3)/5.  , 12*sqrt(3)/5.  ,  -54/5.  };

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  // check sign change for asymmetric basis functions

  int i;

  edgesign[0] = -1;
  i = 4;
  for (int n = 0; n < 1; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[1] = -1;
  i = 4 + (order - 1);
  for (int n = 0; n < 1; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[2] = -1;
  i = 4 + 2*(order - 1);
  for (int n = 0; n < 1; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_HierarchicalP4 )
{
  typedef std::array<int,3> Int3;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 4;
  const int nBasis = (order + 1)*(order + 2)/2;
  const int nNode  = 3;
  const int nEdge  = 3*(order - 1);
  const int nCell  = (order - 1)*(order - 2)/2;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Triangle>::getBasisFunction(order, BasisFunctionCategory_Hierarchical),
                     BasisFunctionAreaBase<Triangle>::HierarchicalP4 );

  const BasisFunctionAreaBase<Triangle>* basis = BasisFunctionAreaBase<Triangle>::HierarchicalP4;

  BOOST_CHECK_EQUAL( order,  basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode,  basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge,  basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nCell,  basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Hierarchical, basis->category() );

  Real s, t;
  Real phi[nBasis], phis[nBasis], phit[nBasis];
  Real phiss[nBasis], phist[nBasis], phitt[nBasis];
  Real phiTrue[nBasis], phisTrue[nBasis], phitTrue[nBasis];
  Real phissTrue[nBasis], phistTrue[nBasis], phittTrue[nBasis];
  Int3 edgesign = {{+1, +1, +1}};

  {
  s = 0;  t = 0;
  Real   phi1[3] = { 1,  0,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 0,  0,  4};
  Real  phit2[3] = { 0,  4,  0};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = { 0,          0,           0,  0};
  Real  phis3[4] = { 0,          0,   6*sqrt(3),  0};
  Real  phit3[4] = { 0, -6*sqrt(3),           0,  0};
  Real phiss3[4] = { 0,          0, -36*sqrt(3),  0};
  Real phist3[4] = { 0, 12*sqrt(3), -12*sqrt(3), 27};
  Real phitt3[4] = { 0, 36*sqrt(3),           0,  0};

  Real   phi4[5] = { 0,  0,  0,               0,               0};
  Real  phis4[5] = { 0,  0,  0,               0,               0};
  Real  phit4[5] = { 0,  0,  0,               0,               0};
  Real phiss4[5] = { 0,  0, 32,               0,               0};
  Real phist4[5] = { 0,  0,  0, 512/(3*sqrt(3)), 512/(3*sqrt(3))};
  Real phitt4[5] = { 0, 32,  0,               0,               0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 1;  t = 0;
  Real   phi1[3] = { 0,  1,  0};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 0,  0, -4};
  Real  phit2[3] = { 4,  0, -4};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {          0, 0,          0,   0};
  Real  phis3[4] = {          0, 0,  6*sqrt(3),   0};
  Real  phit3[4] = {  6*sqrt(3), 0,  6*sqrt(3),   0};
  Real phiss3[4] = {          0, 0, 36*sqrt(3),   0};
  Real phist3[4] = { 12*sqrt(3), 0, 24*sqrt(3), -27};
  Real phitt3[4] = {-12*sqrt(3), 0, 12*sqrt(3), -54};

  Real   phi4[5] = { 0, 0,  0,                0, 0};
  Real  phis4[5] = { 0, 0,  0,                0, 0};
  Real  phit4[5] = { 0, 0,  0,                0, 0};
  Real phiss4[5] = { 0, 0, 32,                0, 0};
  Real phist4[5] = { 0, 0, 32,  512/(3*sqrt(3)), 0};
  Real phitt4[5] = {32, 0, 32, 1024/(3*sqrt(3)), 0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0;  t = 1;
  Real   phi1[3] = { 0,  0,  1};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0,  0,  0};
  Real  phis2[3] = { 4, -4,  0};
  Real  phit2[3] = { 0, -4,  0};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {          0,           0, 0,   0};
  Real  phis3[4] = { -6*sqrt(3),  -6*sqrt(3), 0,   0};
  Real  phit3[4] = {          0,  -6*sqrt(3), 0,   0};
  Real phiss3[4] = { 12*sqrt(3), -12*sqrt(3), 0, -54};
  Real phist3[4] = {-12*sqrt(3), -24*sqrt(3), 0, -27};
  Real phitt3[4] = {          0, -36*sqrt(3), 0,   0};

  Real   phi4[5] = { 0,  0, 0, 0,                0};
  Real  phis4[5] = { 0,  0, 0, 0,                0};
  Real  phit4[5] = { 0,  0, 0, 0,                0};
  Real phiss4[5] = {32, 32, 0, 0, 1024/(3*sqrt(3))};
  Real phist4[5] = { 0, 32, 0, 0,  512/(3*sqrt(3))};
  Real phitt4[5] = { 0, 32, 0, 0,                0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 1./3.;  t = 1./3.;
  Real   phi1[3] = { 1/3.,  1/3.,  1/3.};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 4/9.,  4/9.,   4/9.};
  Real  phis2[3] = { 4/3., -4/3.,      0};
  Real  phit2[3] = { 4/3.,     0,  -4/3.};
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {         0,          0,          0,   1};
  Real  phis3[4] = { 2/sqrt(3),  2/sqrt(3), -4/sqrt(3),   0};
  Real  phit3[4] = {-2/sqrt(3),  4/sqrt(3), -2/sqrt(3),   0};
  Real phiss3[4] = { 4*sqrt(3), -4*sqrt(3),          0, -18};
  Real phist3[4] = {         0, -4*sqrt(3),  4*sqrt(3),  -9};
  Real phitt3[4] = {-4*sqrt(3),          0,  4*sqrt(3), -18};

  Real   phi4[5] = { 16/81., 16/81., 16/81., 0, 0};
  Real  phis4[5] = { 32/27., -32/27., 0, -1024/(81*sqrt(3)),  -512/(81*sqrt(3))};
  Real  phit4[5] = { 32/27., 0, -32/27.,  -512/(81*sqrt(3)), -1024/(81*sqrt(3))};
  Real phiss4[5] = { 32/9.,  32/9., -64/9., 0, 0};
  Real phist4[5] = { 64/9., -32/9., -32/9., 0, 0};
  Real phitt4[5] = { 32/9., -64/9.,  32/9., 0, 0};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }

  {
  s = 0.2;  t = 0.7;
  Real   phi1[3] = { 0.1, 0.2, 0.7};
  Real  phis1[3] = {-1,  1,  0};
  Real  phit1[3] = {-1,  0,  1};
  Real phiss1[3] = { 0,  0,  0};
  Real phist1[3] = { 0,  0,  0};
  Real phitt1[3] = { 0,  0,  0};

  Real   phi2[3] = { 0.56,  0.28,  0.08};
  Real  phis2[3] = { 2.8 , -2.8 , -0.4 };
  Real  phit2[3] = { 0.8,  -2.4 , -0.8 };
  Real phiss2[3] = { 0,  0, -8};
  Real phist2[3] = { 4, -4, -4};
  Real phitt2[3] = { 0, -8,  0};

  Real   phi3[4] = {-21*sqrt(3)/50.,   63*sqrt(3)/250., -3*sqrt(3)/250.,  189/500.};
  Real  phis3[4] = {-63*sqrt(3)/50.,  -21*sqrt(3)/10. , -9*sqrt(3)/50. , -189/100.};
  Real  phit3[4] = {-36*sqrt(3)/25.,  -33*sqrt(3)/25. ,               0,  -81/25. };
  Real phiss3[4] = { 42*sqrt(3)/5. ,  -42*sqrt(3)/5.  , 18*sqrt(3)/5.  , -189/5.  };
  Real phist3[4] = { -6*sqrt(3)    ,  -78*sqrt(3)/5.  , 18*sqrt(3)/5.  , -108/5.  };
  Real phitt3[4] = {-12*sqrt(3)/5. , -108*sqrt(3)/5.  , 12*sqrt(3)/5.  ,  -54/5.  };

  Real   phi4[5] = {196/625.,   49/625.,   4/625., -448/(1875*sqrt(3)),  -896/(625*sqrt(3))};
  Real  phis4[5] = {392/125., -196/125.,  -8/125., -448/( 125*sqrt(3)),  1792/(375*sqrt(3))};
  Real  phit4[5] = {112/125., -168/125., -16/125., -128/( 375*sqrt(3)),  2816/(375*sqrt(3))};
  Real phiss4[5] = {392/25. ,  392/25. , -24/25. , 1792/(  25*sqrt(3)), 12544/( 75*sqrt(3))};
  Real phist4[5] = {224/25. ,   56/5.  ,        0, 1664/(  25*sqrt(3)),  9472/( 75*sqrt(3))};
  Real phitt4[5] = { 32/25. ,  176/25. ,  32/25. , 3584/(  75*sqrt(3)),    1024*sqrt(3)/25.};

  fillTrue1<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi1, phis1, phit1, phiss1, phist1, phitt1 );
  fillTrue2<nBasis,3>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi2, phis2, phit2, phiss2, phist2, phitt2 );
  fillTrue3<nBasis,4>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi3, phis3, phit3, phiss3, phist3, phitt3 );
  fillTrue4<nBasis,5>( order, phiTrue, phisTrue, phitTrue, phissTrue, phistTrue, phittTrue, phi4, phis4, phit4, phiss4, phist4, phitt4 );

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  // check sign change for asymmetric basis functions

  int i;

  edgesign[0] = -1;
  i = 4;
  for (int n = 0; n < 1; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[1] = -1;
  i = 4 + (order - 1);
  for (int n = 0; n < 1; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }

  edgesign[2] = -1;
  i = 4 + 2*(order - 1);
  for (int n = 0; n < 1; n++)
  {
    phiTrue[i] *= -1;  phisTrue[i] *= -1;  phitTrue[i] *= -1;
    phissTrue[i] *= -1;  phistTrue[i] *= -1;  phittTrue[i] *= -1;
    i += 2;
  }

  basis->evalBasis( s, t, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, edgesign, phis, phit, nBasis );
  basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, nBasis );
  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phissTrue[ki], phiss[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phistTrue[ki], phist[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phittTrue[ki], phitt[ki], small_tol, close_tol );
  }
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
