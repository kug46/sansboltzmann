// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunction_TraceOrientation_btest
// testing of reference coordinate transformations

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunction_TraceOrientation.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunction_TraceOrientation_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Node_Orientation_test )
{
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordType;
  typedef BasisFunction_TraceOrientation<TopoD0, Node> RefElementTransformType;

  const Real tol = 1e-13;

  RefCoordType ref, ref_new;
  int orientation = 1;

  ref = 0.0;
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );

  ref = 0.4;
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.4, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_Orientation_test )
{
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordType;
  typedef BasisFunction_TraceOrientation<TopoD1, Line> RefElementTransformType;

  const Real tol = 1e-13;

  RefCoordType ref, ref_new;
  int orientation = 1;

  ref = 0.0;
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );

  ref = 0.4;
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.4, tol );

  ref = 1.0;
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );

  orientation = -1;

  ref = 0.0;
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );

  ref = 0.4;
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.6, tol );

  ref = 1.0;
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );

  //Incorrect orientation
  orientation = 0;
  BOOST_CHECK_THROW( (RefElementTransformType::transform(ref, orientation, ref_new)), DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_Orientation_test )
{
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;
  typedef BasisFunction_TraceOrientation<TopoD2, Triangle> RefElementTransformType;

  const Real tol = 1e-13;

  RefCoordType ref, ref_new;
  int orientation = 1;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.4, tol );

  orientation = 2;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.4, tol );

  orientation = 3;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.2, tol );

  orientation = -1;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.2, tol );

  orientation = -2;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.4, tol );

  orientation = -3;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.4, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_Orientation_test )
{
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;
  typedef BasisFunction_TraceOrientation<TopoD2, Quad> RefElementTransformType;

  const Real tol = 1e-13;

  RefCoordType ref, ref_new;
  int orientation = 1;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.4, tol );

  orientation = 2;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.8, tol );

  orientation = 3;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {1.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.8, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.6, tol );

  orientation = 4;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {1.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.6, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.2, tol );

  orientation = -1;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {1.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.4, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.2, tol );

  orientation = -2;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.8, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.4, tol );

  orientation = -3;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {1.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.6, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.8, tol );

  orientation = -4;

  ref = {0.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {1.0, 0.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 1.0, tol );

  ref = {1.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.0, 1.0};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.0, tol );

  ref = {0.2, 0.4};
  RefElementTransformType::transform(ref, orientation, ref_new);
  BOOST_CHECK_CLOSE( ref_new[0], 0.2, tol );
  BOOST_CHECK_CLOSE( ref_new[1], 0.6, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tet_Orientation_test )
{
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;
  typedef BasisFunction_TraceOrientation<TopoD3, Tet> RefElementTransformType;

  RefCoordType ref;
  RefCoordType ref_new;
  RefCoordType ref_new_true;

  const Real tol = 1e-10;

  {
    int orientation= 1;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.2, 0.2, 0.5};
    ref_new_true= {0.2, 0.2, 0.5};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -1;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.1, 0.4, 0.2};
    ref_new_true= {0.1, 0.2, 0.4};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 2;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.5, 0.1, 0.3};
    ref_new_true= {0.1, 0.3, 0.5};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -2;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.3, 0.2, 0.2};
    ref_new_true= {0.2, 0.3, 0.2};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 3;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.3, 0.5, 0.1};
    ref_new_true= {0.1, 0.3, 0.5};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -3;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.1, 0.1, 0.2};
    ref_new_true= {0.2, 0.1, 0.1};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 4;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.2, 0.3, 0.2};
    ref_new_true= {0.3, 0.2, 0.3};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -4;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.4, 0.1, 0.5};
    ref_new_true= {0.0, 0.1, 0.5};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 5;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.1, 0.5, 0.2};
    ref_new_true= {0.5, 0.2, 0.2};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -5;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.1, 0.1, 0.6};
    ref_new_true= {0.1, 0.6, 0.2};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 6;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.2, 0.4, 0.2};
    ref_new_true= {0.2, 0.4, 0.2};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -6;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.2, 0.1, 0.6};
    ref_new_true= {0.6, 0.1, 0.1};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 7;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.1, 0.5, 0.4};
    ref_new_true= {0.0, 0.1, 0.4};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -7;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.2, 0.2, 0.5};
    ref_new_true= {0.1, 0.5, 0.2};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 8;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.1, 0.1, 0.4};
    ref_new_true= {0.1, 0.4, 0.4};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -8;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.1, 0.3, 0.2};
    ref_new_true= {0.1, 0.4, 0.2};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 9;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.5, 0.1, 0.2};
    ref_new_true= {0.2, 0.2, 0.5};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -9;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.3, 0.1, 0.6};
    ref_new_true= {0.6, 0.3, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 10;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.3, 0.4, 0.2};
    ref_new_true= {0.1, 0.4, 0.3};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -10;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.2, 0.1, 0.5};
    ref_new_true= {0.2, 0.2, 0.1};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 11;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.2, 0.2, 0.3};
    ref_new_true= {0.2, 0.3, 0.2};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -11;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.5, 0.1, 0.4};
    ref_new_true= {0.5, 0.1, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= 12;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.5, 0.3, 0.2};
    ref_new_true= {0.3, 0.5, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }

  {
    int orientation= -12;

    ref= {0.0, 0.0, 0.0};
    ref_new_true= {0.0, 1.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {1.0, 0.0, 0.0};
    ref_new_true= {0.0, 0.0, 1.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 1.0, 0.0};
    ref_new_true= {1.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.0, 0.0, 1.0};
    ref_new_true= {0.0, 0.0, 0.0};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);

    ref= {0.6, 0.1, 0.2};
    ref_new_true= {0.1, 0.1, 0.6};
    RefElementTransformType::transform(ref, orientation, ref_new);
    BOOST_CHECK_CLOSE(ref_new[0], ref_new_true[0], tol);
    BOOST_CHECK_CLOSE(ref_new[1], ref_new_true[1], tol);
    BOOST_CHECK_CLOSE(ref_new[2], ref_new_true[2], tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hex_Orientation_test )
{
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;
  typedef BasisFunction_TraceOrientation<TopoD3, Hex> RefElementTransformType;

  RefCoordType ref, ref_new;
  int orientation = 1;

  //Unsupported
  ref = {0.0, 0.0, 0.0};
  BOOST_CHECK_THROW( (RefElementTransformType::transform(ref, orientation, ref_new)), DeveloperException );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
