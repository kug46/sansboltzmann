// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"   // Triangle
#include "BasisFunction/TraceToCellRefCoord.h"


using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( TraceToCellRefCoord_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_eval_test )
{
  const Real tol = 1e-13;
  Real sRef, sRefArea, tRefArea, sRefAreaTrue, tRefAreaTrue;
  CanonicalTraceToCell canonicalEdge(0,0);

  //===================================
  canonicalEdge.orientation = 1;
  //===================================

  canonicalEdge.trace = 0;
  sRef = 0;
  sRefAreaTrue = 1; tRefAreaTrue = 0;

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  BOOST_CHECK_CLOSE( sRefAreaTrue, sRefArea, tol );
  BOOST_CHECK_CLOSE( tRefAreaTrue, tRefArea, tol );

  canonicalEdge.trace = 1;
  sRef = 0;
  sRefAreaTrue = 0; tRefAreaTrue = 1;

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  BOOST_CHECK_CLOSE( sRefAreaTrue, sRefArea, tol );
  BOOST_CHECK_CLOSE( tRefAreaTrue, tRefArea, tol );

  canonicalEdge.trace = 2;
  sRef = 0;
  sRefAreaTrue = 0; tRefAreaTrue = 0;

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  BOOST_CHECK_CLOSE( sRefAreaTrue, sRefArea, tol );
  BOOST_CHECK_CLOSE( tRefAreaTrue, tRefArea, tol );

  //===================================
  canonicalEdge.orientation = -1;
  //===================================

  canonicalEdge.trace = 0;
  sRef = 0;
  sRefAreaTrue = 0; tRefAreaTrue = 1;

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  BOOST_CHECK_CLOSE( sRefAreaTrue, sRefArea, tol );
  BOOST_CHECK_CLOSE( tRefAreaTrue, tRefArea, tol );

  canonicalEdge.trace = 1;
  sRef = 0;
  sRefAreaTrue = 0; tRefAreaTrue = 0;

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  BOOST_CHECK_CLOSE( sRefAreaTrue, sRefArea, tol );
  BOOST_CHECK_CLOSE( tRefAreaTrue, tRefArea, tol );

  canonicalEdge.trace = 2;
  sRef = 0;
  sRefAreaTrue = 1; tRefAreaTrue = 0;

  TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdge, sRef, sRefArea, tRefArea );
  BOOST_CHECK_CLOSE( sRefAreaTrue, sRefArea, tol );
  BOOST_CHECK_CLOSE( tRefAreaTrue, tRefArea, tol );

  typedef TraceToCellRefCoord<Line, TopoD2, Triangle> TraceToCellRefCoordLineTriangle;

  canonicalEdge.trace = 3;
  BOOST_CHECK_THROW( TraceToCellRefCoordLineTriangle::eval( canonicalEdge, sRef, sRefArea, tRefArea ), DeveloperException );

  canonicalEdge.trace = -3;
  BOOST_CHECK_THROW( TraceToCellRefCoordLineTriangle::eval( canonicalEdge, sRef, sRefArea, tRefArea ), DeveloperException );

  canonicalEdge.trace = -1;
  BOOST_CHECK_THROW( TraceToCellRefCoordLineTriangle::eval( canonicalEdge, sRef, sRefArea, tRefArea ), DeveloperException );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_Orientation_test )
{

  const int tri[3] = {0, 1, 2};

  const int (*TraceNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  CanonicalTraceToCell canonicalEdge(0,0);

  for (int edge = 0; edge < 3; edge++ )
  {
    // Orientation is 1 for all edges that match the canonical edge
    const int line[2] = {tri[TraceNodes[edge][0]], tri[TraceNodes[edge][1]]};

    canonicalEdge = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(line, 2, tri, 3);
    BOOST_CHECK_EQUAL( canonicalEdge.trace, edge );
    BOOST_CHECK_EQUAL( canonicalEdge.orientation, 1 );
  }

  for (int edge = 0; edge < 3; edge++ )
  {
    // Reverse the two nodes results in a -1 orientation
    const int line[3] = {tri[TraceNodes[edge][1]], tri[TraceNodes[edge][0]]};

    canonicalEdge = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(line, 2, tri, 3);
    BOOST_CHECK_EQUAL( canonicalEdge.trace, edge );
    BOOST_CHECK_EQUAL( canonicalEdge.orientation, -1 );
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
