// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionArea_Quad_Legendre_btest
// testing of BasisFunctionArea<Quad, Legendre> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisFunctionArea_Quad_Legendre.h"
#include "BasisFunction/BasisFunctionLine_Legendre.h"
#include "Quadrature/QuadratureArea.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionArea_Quad_Legendre_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Quad::areaRef, 1., 1e-12 );
  BOOST_CHECK_CLOSE( Quad::centerRef[0], 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Quad::centerRef[1], 1./2., 1e-12 );

  BOOST_CHECK( Quad::TopoDim::D == topoDim(eQuad) );
  BOOST_CHECK( Quad::NNode == topoNNode(eQuad) );

  BOOST_CHECK( BasisFunctionAreaBase<Quad>::D == 2 );

  BOOST_CHECK( (BasisFunctionArea<Quad,Legendre,0>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Legendre,1>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Legendre,2>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Legendre,3>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Legendre,4>::D == 2) );
#if 0
  BOOST_CHECK( (BasisFunctionArea<Quad,Legendre,5>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Legendre,6>::D == 2) );
  BOOST_CHECK( (BasisFunctionArea<Quad,Legendre,7>::D == 2) );
#endif
  BOOST_CHECK( BasisFunctionArea_Quad_LegendrePMax == 4 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Quad>::getBasisFunction(-1, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionAreaBase<Quad>::getBasisFunction( BasisFunctionArea_Quad_LegendrePMax+1,
                                                                    BasisFunctionCategory_Legendre), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP0 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(0, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Quad>::LegendreP0 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionArea<Quad,Legendre,0>::self();

  BOOST_CHECK_EQUAL( 0, basis->order() );
  BOOST_CHECK_EQUAL( 1, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 1, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  const int N = 1;

  Real s, t;
  Real phi[N], phis[N], phit[N];
  Real phiss[N], phist[N], phitt[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N];
  Int4 edgesign = {{+1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 5; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 1;  t = 1;
      break;

    case 3:
      s = 0;  t = 1;
      break;

    case 4:
      s = 1./2.;  t = 1./2.;
      break;
    }

    phiTrue[0]  = 1;
    phisTrue[0] = 0;
    phitTrue[0] = 0;

    phissTrue[0] = 0;
    phistTrue[0] = 0;
    phittTrue[0] = 0;

    basis->evalBasis( s, t, edgesign, phi, 1 );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, 1 );
    basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, 1 );
    for (k = 0; k < 1; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP1 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(1, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Quad>::LegendreP1 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionArea<Quad,Legendre,1>::self();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 4, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 4, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  const int N = 4;

  Real s, t;
  Real phi[N], phis[N], phit[N];
  Real phiss[N], phist[N], phitt[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N];
  Int4 edgesign = {{+1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 5; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 1;  t = 1;
      break;

    case 3:
      s = 0;  t = 1;
      break;

    case 4:
      s = 1./2.;  t = 1./2.;
      break;
    }

    Real sphi[2], tphi[2];
    BasisFunctionLine<Legendre,1>::self()->evalBasis(s, sphi, 2);
    BasisFunctionLine<Legendre,1>::self()->evalBasis(t, tphi, 2);

    Real sphis[2], tphit[2];
    BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(s, sphis, 2);
    BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(t, tphit, 2);

    Real sphiss[2], tphitt[2];
    BasisFunctionLine<Legendre,1>::self()->evalBasisHessianDerivative(s, sphiss, 2);
    BasisFunctionLine<Legendre,1>::self()->evalBasisHessianDerivative(t, tphitt, 2);

    //phi -----------------------//
    // P0
    phiTrue[0] = sphi[0]*tphi[0];

    // P1
    phiTrue[1] = sphi[1]*tphi[0];
    phiTrue[2] = sphi[0]*tphi[1];
    phiTrue[3] = sphi[1]*tphi[1];

    //phis, phit ----------------------//
    BasisFunctionArea<Quad,Legendre,1>::self()->tensorProduct(sphis, tphi , phisTrue);
    BasisFunctionArea<Quad,Legendre,1>::self()->tensorProduct(sphi , tphit, phitTrue);

    //phiss, phist, phitt -------------//
    BasisFunctionArea<Quad,Legendre,1>::self()->tensorProduct(sphiss, tphi  , phissTrue);
    BasisFunctionArea<Quad,Legendre,1>::self()->tensorProduct(sphis , tphit , phistTrue);
    BasisFunctionArea<Quad,Legendre,1>::self()->tensorProduct(sphi  , tphitt, phittTrue);

    basis->evalBasis( s, t, edgesign, phi, N );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, N );
    basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, N );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k]  , phi[k]  , small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k] , phis[k] , small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k] , phit[k] , small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP2 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(2, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Quad>::LegendreP2 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::LegendreP2;

  BOOST_CHECK_EQUAL( 2, basis->order() );
  BOOST_CHECK_EQUAL( 9, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 9, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  const int N = 9;

  Real s, t;
  Real phi[N], phis[N], phit[N];
  Real phiss[N], phist[N], phitt[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N];
  Int4 edgesign = {{+1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 0;  t = 1;
      break;

    case 4:
      s = 1./2.;  t = 1./2.;
      break;

    case 5:
      s = 1./3.;  t = 2./3.;
      break;
    }

    Real sphi[3], tphi[3];
    BasisFunctionLine<Legendre,2>::self()->evalBasis(s, sphi, 3);
    BasisFunctionLine<Legendre,2>::self()->evalBasis(t, tphi, 3);

    Real sphis[3], tphit[3];
    BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(s, sphis, 3);
    BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(t, tphit, 3);

    Real sphiss[3], tphitt[3];
    BasisFunctionLine<Legendre,2>::self()->evalBasisHessianDerivative(s, sphiss, 3);
    BasisFunctionLine<Legendre,2>::self()->evalBasisHessianDerivative(t, tphitt, 3);

    //phi -----------------------//
    // P0
    phiTrue[0] = sphi[0]*tphi[0];

    // P1
    phiTrue[1] = sphi[1]*tphi[0];
    phiTrue[2] = sphi[0]*tphi[1];
    phiTrue[3] = sphi[1]*tphi[1];

    // P2
    phiTrue[4] = sphi[2]*tphi[0];
    phiTrue[5] = sphi[0]*tphi[2];
    phiTrue[6] = sphi[2]*tphi[1];
    phiTrue[7] = sphi[1]*tphi[2];
    phiTrue[8] = sphi[2]*tphi[2];

    //phis, phit ----------------------//
    BasisFunctionArea<Quad,Legendre,2>::self()->tensorProduct(sphis, tphi , phisTrue);
    BasisFunctionArea<Quad,Legendre,2>::self()->tensorProduct(sphi , tphit, phitTrue);

    //phiss, phist, phitt -------------//
    BasisFunctionArea<Quad,Legendre,2>::self()->tensorProduct(sphiss, tphi  , phissTrue);
    BasisFunctionArea<Quad,Legendre,2>::self()->tensorProduct(sphis , tphit , phistTrue);
    BasisFunctionArea<Quad,Legendre,2>::self()->tensorProduct(sphi  , tphitt, phittTrue);

    basis->evalBasis( s, t, edgesign, phi, N );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, N );
    basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, N );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k]  , phi[k]  , small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k] , phis[k] , small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k] , phit[k] , small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP3 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(3, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Quad>::LegendreP3 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::LegendreP3;

  BOOST_CHECK_EQUAL(  3, basis->order() );
  BOOST_CHECK_EQUAL( 16, basis->nBasis() );
  BOOST_CHECK_EQUAL(  0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL(  0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 16, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  const int N = 16;

  Real s, t;
  Real phi[N], phis[N], phit[N];
  Real phiss[N], phist[N], phitt[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N];
  Int4 edgesign = {{+1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 0;  t = 1;
      break;

    case 4:
      s = 1./2.;  t = 1./2.;
      break;

    case 5:
      s = 1./3.;  t = 2./3.;
      break;
    }

    Real sphi[4], tphi[4];
    BasisFunctionLine<Legendre,3>::self()->evalBasis(s, sphi, 4);
    BasisFunctionLine<Legendre,3>::self()->evalBasis(t, tphi, 4);

    Real sphis[4], tphit[4];
    BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(s, sphis, 4);
    BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(t, tphit, 4);

    Real sphiss[4], tphitt[4];
    BasisFunctionLine<Legendre,3>::self()->evalBasisHessianDerivative(s, sphiss, 4);
    BasisFunctionLine<Legendre,3>::self()->evalBasisHessianDerivative(t, tphitt, 4);

    //phi -----------------------//
    // P0
    phiTrue[0] = sphi[0]*tphi[0];

    // P1
    phiTrue[1] = sphi[1]*tphi[0];
    phiTrue[2] = sphi[0]*tphi[1];
    phiTrue[3] = sphi[1]*tphi[1];

    // P2
    phiTrue[4] = sphi[2]*tphi[0];
    phiTrue[5] = sphi[0]*tphi[2];
    phiTrue[6] = sphi[2]*tphi[1];
    phiTrue[7] = sphi[1]*tphi[2];
    phiTrue[8] = sphi[2]*tphi[2];

    // P3
    phiTrue[9]  = sphi[3]*tphi[0];
    phiTrue[10] = sphi[0]*tphi[3];
    phiTrue[11] = sphi[3]*tphi[1];
    phiTrue[12] = sphi[1]*tphi[3];
    phiTrue[13] = sphi[3]*tphi[2];
    phiTrue[14] = sphi[2]*tphi[3];
    phiTrue[15] = sphi[3]*tphi[3];

    //phis, phit ----------------------//
    BasisFunctionArea<Quad,Legendre,3>::self()->tensorProduct(sphis, tphi , phisTrue);
    BasisFunctionArea<Quad,Legendre,3>::self()->tensorProduct(sphi , tphit, phitTrue);

    //phiss, phist, phitt -------------//
    BasisFunctionArea<Quad,Legendre,3>::self()->tensorProduct(sphiss, tphi  , phissTrue);
    BasisFunctionArea<Quad,Legendre,3>::self()->tensorProduct(sphis , tphit , phistTrue);
    BasisFunctionArea<Quad,Legendre,3>::self()->tensorProduct(sphi  , tphitt, phittTrue);

    basis->evalBasis( s, t, edgesign, phi, N );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, N );
    basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, N );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k]  , phi[k]  , small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k] , phis[k] , small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k] , phit[k] , small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP4 )
{
  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  BOOST_CHECK_EQUAL( BasisFunctionAreaBase<Quad>::getBasisFunction(4, BasisFunctionCategory_Legendre),
                     BasisFunctionAreaBase<Quad>::LegendreP4 );

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::LegendreP4;

  BOOST_CHECK_EQUAL(  4, basis->order() );
  BOOST_CHECK_EQUAL( 25, basis->nBasis() );
  BOOST_CHECK_EQUAL(  0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL(  0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 25, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  const int N = 25;

  Real s, t;
  Real phi[N], phis[N], phit[N];
  Real phiss[N], phist[N], phitt[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N];
  Int4 edgesign = {{+1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 6; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0;
      break;

    case 1:
      s = 1;  t = 0;
      break;

    case 2:
      s = 0;  t = 1;
      break;

    case 3:
      s = 0;  t = 1;
      break;

    case 4:
      s = 1./2.;  t = 1./2.;
      break;

    case 5:
      s = 1./3.;  t = 2./3.;
      break;
    }

    Real sphi[5], tphi[5];
    BasisFunctionLine<Legendre,4>::self()->evalBasis(s, sphi, 5);
    BasisFunctionLine<Legendre,4>::self()->evalBasis(t, tphi, 5);

    Real sphis[5], tphit[5];
    BasisFunctionLine<Legendre,4>::self()->evalBasisDerivative(s, sphis, 5);
    BasisFunctionLine<Legendre,4>::self()->evalBasisDerivative(t, tphit, 5);

    Real sphiss[5], tphitt[5];
    BasisFunctionLine<Legendre,4>::self()->evalBasisHessianDerivative(s, sphiss, 5);
    BasisFunctionLine<Legendre,4>::self()->evalBasisHessianDerivative(t, tphitt, 5);

    //phi -----------------------//
    // P0
    phiTrue[0] = sphi[0]*tphi[0];

    // P1
    phiTrue[1] = sphi[1]*tphi[0];
    phiTrue[2] = sphi[0]*tphi[1];
    phiTrue[3] = sphi[1]*tphi[1];

    // P2
    phiTrue[4] = sphi[2]*tphi[0];
    phiTrue[5] = sphi[0]*tphi[2];
    phiTrue[6] = sphi[2]*tphi[1];
    phiTrue[7] = sphi[1]*tphi[2];
    phiTrue[8] = sphi[2]*tphi[2];

    // P3
    phiTrue[9]  = sphi[3]*tphi[0];
    phiTrue[10] = sphi[0]*tphi[3];
    phiTrue[11] = sphi[3]*tphi[1];
    phiTrue[12] = sphi[1]*tphi[3];
    phiTrue[13] = sphi[3]*tphi[2];
    phiTrue[14] = sphi[2]*tphi[3];
    phiTrue[15] = sphi[3]*tphi[3];

    // P4
    phiTrue[16] = sphi[4]*tphi[0];
    phiTrue[17] = sphi[0]*tphi[4];
    phiTrue[18] = sphi[4]*tphi[1];
    phiTrue[19] = sphi[1]*tphi[4];
    phiTrue[20] = sphi[4]*tphi[2];
    phiTrue[21] = sphi[2]*tphi[4];
    phiTrue[22] = sphi[4]*tphi[3];
    phiTrue[23] = sphi[3]*tphi[4];
    phiTrue[24] = sphi[4]*tphi[4];

    //phis, phit ----------------------//
    BasisFunctionArea<Quad,Legendre,4>::self()->tensorProduct(sphis, tphi , phisTrue);
    BasisFunctionArea<Quad,Legendre,4>::self()->tensorProduct(sphi , tphit, phitTrue);

    //phiss, phist, phitt -------------//
    BasisFunctionArea<Quad,Legendre,4>::self()->tensorProduct(sphiss, tphi  , phissTrue);
    BasisFunctionArea<Quad,Legendre,4>::self()->tensorProduct(sphis , tphit , phistTrue);
    BasisFunctionArea<Quad,Legendre,4>::self()->tensorProduct(sphi  , tphitt, phittTrue);

    basis->evalBasis( s, t, edgesign, phi, N );
    basis->evalBasisDerivative( s, t, edgesign, phis, phit, N );
    basis->evalBasisHessianDerivative( s, t, edgesign, phiss, phist, phitt, N );
    for (k = 0; k < basis->nBasis(); k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k]  , phi[k]  , small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k] , phis[k] , small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k] , phit[k] , small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP0 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-12;

  int order;
  int nquad;
  const int nBasis = 1;
  Real s, t;
  Real phi[nBasis];
  const int nSum = nBasis*(nBasis+1)/2;
  Real w, sum[nSum];
  Int4 facesign = {{+1, +1, +1, +1}};

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::LegendreP0;

  order = 2*0;
  QuadratureArea<Quad> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nSum; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, facesign, phi, nBasis );

    int m = 0;
    for (int i = 0; i < nBasis; i++)
      for (int j = i; j < nBasis; j++)
      {
        sum[m] += w*phi[i]*phi[j];
        m++;
      }
  }

  int m = 0;
  for (int i = 0; i < nBasis; i++)
    for (int j = i; j < nBasis; j++)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( sum[m], 1.0, tol );
      else
        BOOST_CHECK_SMALL( sum[m], tol );
//      std::cout << m << ", " << i << ", " << j << " : " << sum[m] << std::endl;
      m++;
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP1 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-12;

  int order;
  int nquad;
  const int nBasis = 4;
  Real s, t;
  Real phi[nBasis];
  const int nSum = nBasis*(nBasis+1)/2;
  Real w, sum[nSum];
  Int4 facesign = {{+1, +1, +1, +1}};

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::LegendreP1;

  order = 2*1;
  QuadratureArea<Quad> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nSum; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, facesign, phi, nBasis );

    int m = 0;
    for (int i = 0; i < nBasis; i++)
      for (int j = i; j < nBasis; j++)
      {
        sum[m] += w*phi[i]*phi[j];
        m++;
      }
  }

  int m = 0;
  for (int i = 0; i < nBasis; i++)
    for (int j = i; j < nBasis; j++)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( sum[m], 1.0, tol );
      else
        BOOST_CHECK_SMALL( sum[m], tol );
//      std::cout << m << ", " << i << ", " << j << " : " << sum[m] << std::endl;
      m++;
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP2 )
{
  typedef std::array<int,4> Int4;

  const Real tol = 1e-12;

  int order;
  int nquad;
  const int nBasis = 9;
  Real s, t;
  Real phi[nBasis];
  const int nSum = nBasis*(nBasis+1)/2;
  Real w, sum[nSum];
  Int4 facesign = {{+1, +1, +1, +1}};

  const BasisFunctionAreaBase<Quad>* basis = BasisFunctionAreaBase<Quad>::LegendreP2;

  order = 2*2;
  QuadratureArea<Quad> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nSum; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t);
    quad.weight(n, w);

    basis->evalBasis( s, t, facesign, phi, nBasis );

    int m = 0;
    for (int i = 0; i < nBasis; i++)
      for (int j = i; j < nBasis; j++)
      {
        sum[m] += w*phi[i]*phi[j];
        m++;
      }
  }

  int m = 0;
  for (int i = 0; i < nBasis; i++)
    for (int j = i; j < nBasis; j++)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( sum[m], 1.0, tol );
      else
        BOOST_CHECK_SMALL( sum[m], tol );
//      std::cout << m << ", " << i << ", " << j << " : " << sum[m] << std::endl;
      m++;
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionArea_Quad_Legendre_pattern.txt", true );

  BasisFunctionAreaBase<Quad>::getBasisFunction(0, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(1, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(2, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(3, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(4, BasisFunctionCategory_Legendre)->dump( 2, output );
#if 0
  BasisFunctionAreaBase<Quad>::getBasisFunction(5, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(6, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionAreaBase<Quad>::getBasisFunction(7, BasisFunctionCategory_Legendre)->dump( 2, output );
#endif

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
