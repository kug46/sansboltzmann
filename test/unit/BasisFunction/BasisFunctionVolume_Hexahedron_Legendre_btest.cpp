// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionVolume_Hex_Legendre_btest
// testing of BasisFunctionVolume<Hex, Legendre> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionVolume.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Legendre.h"
#include "BasisFunction/BasisFunctionLine_Legendre.h"
#include "Quadrature/QuadratureVolume.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionVolume_Hexahedron_Legendre_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK_CLOSE( Hex::volumeRef, 1., 1e-12 );
  BOOST_CHECK_CLOSE( Hex::centerRef[0], 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Hex::centerRef[1], 1./2., 1e-12 );
  BOOST_CHECK_CLOSE( Hex::centerRef[2], 1./2., 1e-12 );

  BOOST_CHECK( Hex::TopoDim::D == topoDim(eHex) );
  BOOST_CHECK( Hex::NNode == topoNNode(eHex) );

  BOOST_CHECK( BasisFunctionVolumeBase<Hex>::D == 3 );

  BOOST_CHECK( (BasisFunctionVolume<Hex,Legendre,0>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Legendre,1>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Legendre,2>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Legendre,3>::D == 3) );
#if 0
  BOOST_CHECK( (BasisFunctionVolume<Hex,Legendre,4>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Legendre,5>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Legendre,6>::D == 3) );
  BOOST_CHECK( (BasisFunctionVolume<Hex,Legendre,7>::D == 3) );
#endif
  BOOST_CHECK( BasisFunctionVolume_Hex_LegendrePMax == 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exceptions )
{
  BOOST_CHECK_THROW( BasisFunctionVolumeBase<Hex>::getBasisFunction(-1, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( BasisFunctionVolumeBase<Hex>::getBasisFunction( BasisFunctionVolume_Hex_LegendrePMax+1,
                                                                     BasisFunctionCategory_Legendre), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP0 )
{
  typedef std::array<int,6> Int6;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Hex>::getBasisFunction(0, BasisFunctionCategory_Legendre),
                     BasisFunctionVolumeBase<Hex>::LegendreP0 );

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolume<Hex,Legendre,0>::self();

  BOOST_CHECK_EQUAL( 0, basis->order() );
  BOOST_CHECK_EQUAL( 1, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 1, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  const int N = 1;

  Real s, t, u;
  Real phi[N], phis[N], phit[N], phiu[N];
  Real phiss[N], phist[N], phitt[N], phisu[N], phitu[N], phiuu[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N], phiuTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N], phisuTrue[N], phituTrue[N], phiuuTrue[N];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 1; test++)
  {
    switch (test)
    {
    case 0:
      s = 1./2.;  t = 1./2.;  u = 1./2.;
      break;
    }

    phiTrue[0]  = 1;
    phisTrue[0] = 0;
    phitTrue[0] = 0;
    phiuTrue[0] = 0;

    phissTrue[0] = 0;
    phistTrue[0] = 0;
    phittTrue[0] = 0;
    phisuTrue[0] = 0;
    phituTrue[0] = 0;
    phiuuTrue[0] = 0;

    basis->evalBasis( s, t, u, facesign, phi, N );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, N );
    basis->evalBasisHessianDerivative( s, t, u, facesign, phiss, phist, phitt, phisu, phitu, phiuu, N );
    for (k = 0; k < N; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP1 )
{
  typedef std::array<int,6> Int6;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Hex>::getBasisFunction(1, BasisFunctionCategory_Legendre),
                     BasisFunctionVolumeBase<Hex>::LegendreP1 );

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolume<Hex,Legendre,1>::self();

  BOOST_CHECK_EQUAL( 1, basis->order() );
  BOOST_CHECK_EQUAL( 8, basis->nBasis() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL( 0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 8, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  const int N = 8;

  Real s, t, u;
  Real phi[N], phis[N], phit[N], phiu[N];
  Real phiss[N], phist[N], phitt[N], phisu[N], phitu[N], phiuu[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N], phiuTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N], phisuTrue[N], phituTrue[N], phiuuTrue[N];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 12; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0; u = 0;
      break;

    case 1:
      s = 1;  t = 0; u = 0;
      break;

    case 2:
      s = 0;  t = 1; u = 0;
      break;

    case 3:
      s = 0;  t = 1; u = 0;
      break;

    case 4:
      s = 1./2.;  t = 1./2.; u = 0;
      break;

    case 5:
      s = 1./3.;  t = 2./3.; u = 0;
      break;

    case 6:
      s = 0;  t = 0; u = 1;
      break;

    case 7:
      s = 1;  t = 0; u = 1;
      break;

    case 8:
      s = 0;  t = 1; u = 1;
      break;

    case 9:
      s = 0;  t = 1; u = 1;
      break;

    case 10:
      s = 1./2.;  t = 1./2.; u = 1;
      break;

    case 11:
      s = 1./7.;  t = 5./6.; u = 4./5.;
      break;
    }

    Real sphi[2], tphi[2], uphi[2];
    BasisFunctionLine<Legendre,1>::self()->evalBasis(s, sphi, 2);
    BasisFunctionLine<Legendre,1>::self()->evalBasis(t, tphi, 2);
    BasisFunctionLine<Legendre,1>::self()->evalBasis(u, uphi, 2);

    Real sphis[2], tphit[2], uphiu[2];
    BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(s, sphis, 2);
    BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(t, tphit, 2);
    BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(u, uphiu, 2);

    Real sphiss[2], tphitt[2], uphiuu[2];
    BasisFunctionLine<Legendre,1>::self()->evalBasisHessianDerivative(s, sphiss, 2);
    BasisFunctionLine<Legendre,1>::self()->evalBasisHessianDerivative(t, tphitt, 2);
    BasisFunctionLine<Legendre,1>::self()->evalBasisHessianDerivative(u, uphiuu, 2);

    //phi -----------------------//
    // P0
    phiTrue[0] = sphi[0]*tphi[0]*uphi[0];

    // P1
    phiTrue[1] = sphi[1]*tphi[0]*uphi[0];
    phiTrue[2] = sphi[0]*tphi[1]*uphi[0];
    phiTrue[3] = sphi[1]*tphi[1]*uphi[0];

    phiTrue[4] = sphi[0]*tphi[0]*uphi[1];
    phiTrue[5] = sphi[1]*tphi[0]*uphi[1];
    phiTrue[6] = sphi[0]*tphi[1]*uphi[1];
    phiTrue[7] = sphi[1]*tphi[1]*uphi[1];

    //phis, phit, phit -----------------------------//
    BasisFunctionVolume<Hex,Legendre,1>::self()->tensorProduct(sphis, tphi , uphi , phisTrue);
    BasisFunctionVolume<Hex,Legendre,1>::self()->tensorProduct(sphi , tphit, uphi , phitTrue);
    BasisFunctionVolume<Hex,Legendre,1>::self()->tensorProduct(sphi , tphi , uphiu, phiuTrue);

    //phiss, phist, phitt, phisu, phitu, phiuu -----//
    BasisFunctionVolume<Hex,Legendre,1>::self()->tensorProduct(sphiss, tphi  , uphi  , phissTrue);
    BasisFunctionVolume<Hex,Legendre,1>::self()->tensorProduct(sphis , tphit , uphi  , phistTrue);
    BasisFunctionVolume<Hex,Legendre,1>::self()->tensorProduct(sphi  , tphitt, uphi  , phittTrue);
    BasisFunctionVolume<Hex,Legendre,1>::self()->tensorProduct(sphis , tphi  , uphiu , phisuTrue);
    BasisFunctionVolume<Hex,Legendre,1>::self()->tensorProduct(sphi  , tphit , uphiu , phituTrue);
    BasisFunctionVolume<Hex,Legendre,1>::self()->tensorProduct(sphi  , tphi  , uphiuu, phiuuTrue);


    basis->evalBasis( s, t, u, facesign, phi, N );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, N );
    basis->evalBasisHessianDerivative( s, t, u, facesign, phiss, phist, phitt, phisu, phitu, phiuu, N );
    for (k = 0; k < N; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP2 )
{
  typedef std::array<int,6> Int6;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Hex>::getBasisFunction(2, BasisFunctionCategory_Legendre),
                     BasisFunctionVolumeBase<Hex>::LegendreP2 );

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolume<Hex,Legendre,2>::self();

  BOOST_CHECK_EQUAL(  2, basis->order() );
  BOOST_CHECK_EQUAL( 27, basis->nBasis() );
  BOOST_CHECK_EQUAL(  0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL(  0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 27, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  const int N = 27;

  Real s, t, u;
  Real phi[N], phis[N], phit[N], phiu[N];
  Real phiss[N], phist[N], phitt[N], phisu[N], phitu[N], phiuu[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N], phiuTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N], phisuTrue[N], phituTrue[N], phiuuTrue[N];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 12; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0; u = 0;
      break;

    case 1:
      s = 1;  t = 0; u = 0;
      break;

    case 2:
      s = 0;  t = 1; u = 0;
      break;

    case 3:
      s = 0;  t = 1; u = 0;
      break;

    case 4:
      s = 1./2.;  t = 1./2.; u = 0;
      break;

    case 5:
      s = 1./3.;  t = 2./3.; u = 0;
      break;

    case 6:
      s = 0;  t = 0; u = 1;
      break;

    case 7:
      s = 1;  t = 0; u = 1;
      break;

    case 8:
      s = 0;  t = 1; u = 1;
      break;

    case 9:
      s = 0;  t = 1; u = 1;
      break;

    case 10:
      s = 1./2.;  t = 1./2.; u = 1;
      break;

    case 11:
      s = 1./7.;  t = 5./6.; u = 4./5.;
      break;
    }

    Real sphi[3], tphi[3], uphi[3];
    BasisFunctionLine<Legendre,2>::self()->evalBasis(s, sphi, 3);
    BasisFunctionLine<Legendre,2>::self()->evalBasis(t, tphi, 3);
    BasisFunctionLine<Legendre,2>::self()->evalBasis(u, uphi, 3);

    Real sphis[3], tphit[3], uphiu[3];
    BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(s, sphis, 3);
    BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(t, tphit, 3);
    BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(u, uphiu, 3);

    Real sphiss[3], tphitt[3], uphiuu[3];
    BasisFunctionLine<Legendre,2>::self()->evalBasisHessianDerivative(s, sphiss, 3);
    BasisFunctionLine<Legendre,2>::self()->evalBasisHessianDerivative(t, tphitt, 3);
    BasisFunctionLine<Legendre,2>::self()->evalBasisHessianDerivative(u, uphiuu, 3);

    //phi -----------------------//
    // P0
    phiTrue[0] = sphi[0]*tphi[0]*uphi[0];

    // P1
    phiTrue[1] = sphi[1]*tphi[0]*uphi[0];
    phiTrue[2] = sphi[0]*tphi[1]*uphi[0];
    phiTrue[3] = sphi[1]*tphi[1]*uphi[0];

    phiTrue[4] = sphi[0]*tphi[0]*uphi[1];
    phiTrue[5] = sphi[1]*tphi[0]*uphi[1];
    phiTrue[6] = sphi[0]*tphi[1]*uphi[1];
    phiTrue[7] = sphi[1]*tphi[1]*uphi[1];

    // P2
    phiTrue[8]  = sphi[2]*tphi[0]*uphi[0];
    phiTrue[9]  = sphi[0]*tphi[2]*uphi[0];
    phiTrue[10] = sphi[2]*tphi[1]*uphi[0];
    phiTrue[11] = sphi[1]*tphi[2]*uphi[0];
    phiTrue[12] = sphi[2]*tphi[2]*uphi[0];

    phiTrue[13] = sphi[2]*tphi[0]*uphi[1];
    phiTrue[14] = sphi[0]*tphi[2]*uphi[1];
    phiTrue[15] = sphi[2]*tphi[1]*uphi[1];
    phiTrue[16] = sphi[1]*tphi[2]*uphi[1];
    phiTrue[17] = sphi[2]*tphi[2]*uphi[1];

    phiTrue[18] = sphi[0]*tphi[0]*uphi[2];
    phiTrue[19] = sphi[1]*tphi[0]*uphi[2];
    phiTrue[20] = sphi[0]*tphi[1]*uphi[2];
    phiTrue[21] = sphi[1]*tphi[1]*uphi[2];
    phiTrue[22] = sphi[2]*tphi[0]*uphi[2];
    phiTrue[23] = sphi[0]*tphi[2]*uphi[2];
    phiTrue[24] = sphi[2]*tphi[1]*uphi[2];
    phiTrue[25] = sphi[1]*tphi[2]*uphi[2];
    phiTrue[26] = sphi[2]*tphi[2]*uphi[2];

    //phis, phit, phit -----------------------------//
    BasisFunctionVolume<Hex,Legendre,2>::self()->tensorProduct(sphis, tphi , uphi , phisTrue);
    BasisFunctionVolume<Hex,Legendre,2>::self()->tensorProduct(sphi , tphit, uphi , phitTrue);
    BasisFunctionVolume<Hex,Legendre,2>::self()->tensorProduct(sphi , tphi , uphiu, phiuTrue);

    //phiss, phist, phitt, phisu, phitu, phiuu -----//
    BasisFunctionVolume<Hex,Legendre,2>::self()->tensorProduct(sphiss, tphi  , uphi  , phissTrue);
    BasisFunctionVolume<Hex,Legendre,2>::self()->tensorProduct(sphis , tphit , uphi  , phistTrue);
    BasisFunctionVolume<Hex,Legendre,2>::self()->tensorProduct(sphi  , tphitt, uphi  , phittTrue);
    BasisFunctionVolume<Hex,Legendre,2>::self()->tensorProduct(sphis , tphi  , uphiu , phisuTrue);
    BasisFunctionVolume<Hex,Legendre,2>::self()->tensorProduct(sphi  , tphit , uphiu , phituTrue);
    BasisFunctionVolume<Hex,Legendre,2>::self()->tensorProduct(sphi  , tphi  , uphiuu, phiuuTrue);


    basis->evalBasis( s, t, u, facesign, phi, N );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, N );
    basis->evalBasisHessianDerivative( s, t, u, facesign, phiss, phist, phitt, phisu, phitu, phiuu, N );
    for (k = 0; k < N; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( basis_LegendreP3 )
{
  typedef std::array<int,6> Int6;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Hex>::getBasisFunction(3, BasisFunctionCategory_Legendre),
                     BasisFunctionVolumeBase<Hex>::LegendreP3 );

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolume<Hex,Legendre,3>::self();

  BOOST_CHECK_EQUAL(  3, basis->order() );
  BOOST_CHECK_EQUAL( 64, basis->nBasis() );
  BOOST_CHECK_EQUAL(  0, basis->nBasisNode() );
  BOOST_CHECK_EQUAL(  0, basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( 64, basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Legendre, basis->category() );

  const int N = 64;

  Real s, t, u;
  Real phi[N], phis[N], phit[N], phiu[N];
  Real phiss[N], phist[N], phitt[N], phisu[N], phitu[N], phiuu[N];
  Real phiTrue[N], phisTrue[N], phitTrue[N], phiuTrue[N];
  Real phissTrue[N], phistTrue[N], phittTrue[N], phisuTrue[N], phituTrue[N], phiuuTrue[N];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};
  int k;

  for (int test = 0; test < 12; test++)
  {
    switch (test)
    {
    case 0:
      s = 0;  t = 0; u = 0;
      break;

    case 1:
      s = 1;  t = 0; u = 0;
      break;

    case 2:
      s = 0;  t = 1; u = 0;
      break;

    case 3:
      s = 0;  t = 1; u = 0;
      break;

    case 4:
      s = 1./2.;  t = 1./2.; u = 0;
      break;

    case 5:
      s = 1./3.;  t = 2./3.; u = 0;
      break;

    case 6:
      s = 0;  t = 0; u = 1;
      break;

    case 7:
      s = 1;  t = 0; u = 1;
      break;

    case 8:
      s = 0;  t = 1; u = 1;
      break;

    case 9:
      s = 0;  t = 1; u = 1;
      break;

    case 10:
      s = 1./2.;  t = 1./2.; u = 1;
      break;

    case 11:
      s = 1./7.;  t = 5./6.; u = 4./5.;
      break;
    }

    Real sphi[4], tphi[4], uphi[4];
    BasisFunctionLine<Legendre,3>::self()->evalBasis(s, sphi, 4);
    BasisFunctionLine<Legendre,3>::self()->evalBasis(t, tphi, 4);
    BasisFunctionLine<Legendre,3>::self()->evalBasis(u, uphi, 4);

    Real sphis[4], tphit[4], uphiu[4];
    BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(s, sphis, 4);
    BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(t, tphit, 4);
    BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(u, uphiu, 4);

    Real sphiss[4], tphitt[4], uphiuu[4];
    BasisFunctionLine<Legendre,3>::self()->evalBasisHessianDerivative(s, sphiss, 4);
    BasisFunctionLine<Legendre,3>::self()->evalBasisHessianDerivative(t, tphitt, 4);
    BasisFunctionLine<Legendre,3>::self()->evalBasisHessianDerivative(u, uphiuu, 4);

    //phi -----------------------//
    // P0
    phiTrue[0] = sphi[0]*tphi[0]*uphi[0];

    // P1
    phiTrue[1] = sphi[1]*tphi[0]*uphi[0];
    phiTrue[2] = sphi[0]*tphi[1]*uphi[0];
    phiTrue[3] = sphi[1]*tphi[1]*uphi[0];

    phiTrue[4] = sphi[0]*tphi[0]*uphi[1];
    phiTrue[5] = sphi[1]*tphi[0]*uphi[1];
    phiTrue[6] = sphi[0]*tphi[1]*uphi[1];
    phiTrue[7] = sphi[1]*tphi[1]*uphi[1];

    // P2
    phiTrue[8]  = sphi[2]*tphi[0]*uphi[0];
    phiTrue[9]  = sphi[0]*tphi[2]*uphi[0];
    phiTrue[10] = sphi[2]*tphi[1]*uphi[0];
    phiTrue[11] = sphi[1]*tphi[2]*uphi[0];
    phiTrue[12] = sphi[2]*tphi[2]*uphi[0];

    phiTrue[13] = sphi[2]*tphi[0]*uphi[1];
    phiTrue[14] = sphi[0]*tphi[2]*uphi[1];
    phiTrue[15] = sphi[2]*tphi[1]*uphi[1];
    phiTrue[16] = sphi[1]*tphi[2]*uphi[1];
    phiTrue[17] = sphi[2]*tphi[2]*uphi[1];

    phiTrue[18] = sphi[0]*tphi[0]*uphi[2];
    phiTrue[19] = sphi[1]*tphi[0]*uphi[2];
    phiTrue[20] = sphi[0]*tphi[1]*uphi[2];
    phiTrue[21] = sphi[1]*tphi[1]*uphi[2];
    phiTrue[22] = sphi[2]*tphi[0]*uphi[2];
    phiTrue[23] = sphi[0]*tphi[2]*uphi[2];
    phiTrue[24] = sphi[2]*tphi[1]*uphi[2];
    phiTrue[25] = sphi[1]*tphi[2]*uphi[2];
    phiTrue[26] = sphi[2]*tphi[2]*uphi[2];

    // P3
    phiTrue[27] = sphi[3]*tphi[0]*uphi[0];
    phiTrue[28] = sphi[0]*tphi[3]*uphi[0];
    phiTrue[29] = sphi[3]*tphi[1]*uphi[0];
    phiTrue[30] = sphi[1]*tphi[3]*uphi[0];
    phiTrue[31] = sphi[3]*tphi[2]*uphi[0];
    phiTrue[32] = sphi[2]*tphi[3]*uphi[0];
    phiTrue[33] = sphi[3]*tphi[3]*uphi[0];

    phiTrue[34] = sphi[3]*tphi[0]*uphi[1];
    phiTrue[35] = sphi[0]*tphi[3]*uphi[1];
    phiTrue[36] = sphi[3]*tphi[1]*uphi[1];
    phiTrue[37] = sphi[1]*tphi[3]*uphi[1];
    phiTrue[38] = sphi[3]*tphi[2]*uphi[1];
    phiTrue[39] = sphi[2]*tphi[3]*uphi[1];
    phiTrue[40] = sphi[3]*tphi[3]*uphi[1];

    phiTrue[41] = sphi[3]*tphi[0]*uphi[2];
    phiTrue[42] = sphi[0]*tphi[3]*uphi[2];
    phiTrue[43] = sphi[3]*tphi[1]*uphi[2];
    phiTrue[44] = sphi[1]*tphi[3]*uphi[2];
    phiTrue[45] = sphi[3]*tphi[2]*uphi[2];
    phiTrue[46] = sphi[2]*tphi[3]*uphi[2];
    phiTrue[47] = sphi[3]*tphi[3]*uphi[2];

    phiTrue[48] = sphi[0]*tphi[0]*uphi[3];
    phiTrue[49] = sphi[1]*tphi[0]*uphi[3];
    phiTrue[50] = sphi[2]*tphi[0]*uphi[3];
    phiTrue[51] = sphi[3]*tphi[0]*uphi[3];
    phiTrue[52] = sphi[0]*tphi[1]*uphi[3];
    phiTrue[53] = sphi[0]*tphi[2]*uphi[3];
    phiTrue[54] = sphi[0]*tphi[3]*uphi[3];
    phiTrue[55] = sphi[1]*tphi[1]*uphi[3];
    phiTrue[56] = sphi[2]*tphi[1]*uphi[3];
    phiTrue[57] = sphi[3]*tphi[1]*uphi[3];
    phiTrue[58] = sphi[1]*tphi[2]*uphi[3];
    phiTrue[59] = sphi[2]*tphi[2]*uphi[3];
    phiTrue[60] = sphi[3]*tphi[2]*uphi[3];
    phiTrue[61] = sphi[1]*tphi[3]*uphi[3];
    phiTrue[62] = sphi[2]*tphi[3]*uphi[3];
    phiTrue[63] = sphi[3]*tphi[3]*uphi[3];

    //phis, phit, phit -----------------------------//
    BasisFunctionVolume<Hex,Legendre,3>::self()->tensorProduct(sphis, tphi , uphi , phisTrue);
    BasisFunctionVolume<Hex,Legendre,3>::self()->tensorProduct(sphi , tphit, uphi , phitTrue);
    BasisFunctionVolume<Hex,Legendre,3>::self()->tensorProduct(sphi , tphi , uphiu, phiuTrue);

    //phiss, phist, phitt, phisu, phitu, phiuu -----//
    BasisFunctionVolume<Hex,Legendre,3>::self()->tensorProduct(sphiss, tphi  , uphi  , phissTrue);
    BasisFunctionVolume<Hex,Legendre,3>::self()->tensorProduct(sphis , tphit , uphi  , phistTrue);
    BasisFunctionVolume<Hex,Legendre,3>::self()->tensorProduct(sphi  , tphitt, uphi  , phittTrue);
    BasisFunctionVolume<Hex,Legendre,3>::self()->tensorProduct(sphis , tphi  , uphiu , phisuTrue);
    BasisFunctionVolume<Hex,Legendre,3>::self()->tensorProduct(sphi  , tphit , uphiu , phituTrue);
    BasisFunctionVolume<Hex,Legendre,3>::self()->tensorProduct(sphi  , tphi  , uphiuu, phiuuTrue);


    basis->evalBasis( s, t, u, facesign, phi, N );
    basis->evalBasisDerivative( s, t, u, facesign, phis, phit, phiu, N );
    basis->evalBasisHessianDerivative( s, t, u, facesign, phiss, phist, phitt, phisu, phitu, phiuu, N );
    for (k = 0; k < N; k++)
    {
      SANS_CHECK_CLOSE( phiTrue[k], phi[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisTrue[k], phis[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phitTrue[k], phit[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuTrue[k], phiu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phissTrue[k], phiss[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phistTrue[k], phist[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phittTrue[k], phitt[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phisuTrue[k], phisu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phituTrue[k], phitu[k], small_tol, close_tol );
      SANS_CHECK_CLOSE( phiuuTrue[k], phiuu[k], small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP0 )
{
  typedef std::array<int,6> Int6;

  const Real tol = 1e-12;

  int order;
  int nquad;
  const int nBasis = 1;
  Real s, t, u;
  Real phi[nBasis];
  const int nSum = nBasis*(nBasis+1)/2;
  Real w, sum[nSum];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolumeBase<Hex>::LegendreP0;

  order = 2*0;
  QuadratureVolume<Tet> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nSum; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t, u);
    quad.weight(n, w);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );

    int m = 0;
    for (int i = 0; i < nBasis; i++)
      for (int j = i; j < nBasis; j++)
      {
        sum[m] += w*phi[i]*phi[j];
        m++;
      }
  }

  int m = 0;
  for (int i = 0; i < nBasis; i++)
    for (int j = i; j < nBasis; j++)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( sum[m], 1.0, tol );
      else
        BOOST_CHECK_SMALL( sum[m], tol );
//      std::cout << m << ", " << i << ", " << j << " : " << sum[m] << std::endl;
      m++;
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP1 )
{
  typedef std::array<int,6> Int6;

  const Real tol = 1e-12;

  int order;
  int nquad;
  const int nBasis = 8;
  Real s, t, u;
  Real phi[nBasis];
  const int nSum = nBasis*(nBasis+1)/2;
  Real w, sum[nSum];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolumeBase<Hex>::LegendreP1;

  order = 2*1;
  QuadratureVolume<Hex> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nSum; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t, u);
    quad.weight(n, w);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );

    int m = 0;
    for (int i = 0; i < nBasis; i++)
      for (int j = i; j < nBasis; j++)
      {
        sum[m] += w*phi[i]*phi[j];
        m++;
      }
  }

  int m = 0;
  for (int i = 0; i < nBasis; i++)
    for (int j = i; j < nBasis; j++)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( sum[m], 1.0, tol );
      else
        BOOST_CHECK_SMALL( sum[m], tol );
      //std::cout << m << ", " << i << ", " << j << " : " << sum[m] << std::endl;
      m++;
    }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP2 )
{
  typedef std::array<int,6> Int6;

  const Real tol = 1e-12;

  int order;
  int nquad;
  const int nBasis = 27;
  Real s, t, u;
  Real phi[nBasis];
  const int nSum = nBasis*(nBasis+1)/2;
  Real w, sum[nSum];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolumeBase<Hex>::LegendreP2;

  order = 2*2;
  QuadratureVolume<Hex> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nSum; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t, u);
    quad.weight(n, w);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );

    int m = 0;
    for (int i = 0; i < nBasis; i++)
      for (int j = i; j < nBasis; j++)
      {
        sum[m] += w*phi[i]*phi[j];
        m++;
      }
  }

  int m = 0;
  for (int i = 0; i < nBasis; i++)
    for (int j = i; j < nBasis; j++)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( sum[m], 1.0, tol );
      else
        BOOST_CHECK_SMALL( sum[m], tol );
      //std::cout << m << ", " << i << ", " << j << " : " << sum[m] << std::endl;
      m++;
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( orthogonal_LegendreP3 )
{
  typedef std::array<int,6> Int6;

  const Real tol = 1e-12;

  int order;
  int nquad;
  const int nBasis = 64;
  Real s, t, u;
  Real phi[nBasis];
  const int nSum = nBasis*(nBasis+1)/2;
  Real w, sum[nSum];
  Int6 facesign = {{+1, +1, +1, +1, +1, +1}};

  const BasisFunctionVolumeBase<Hex>* basis = BasisFunctionVolumeBase<Hex>::LegendreP3;

  order = 2*3;
  QuadratureVolume<Hex> quad(order);
  BOOST_CHECK( quad.order() >= order );

  nquad = quad.nQuadrature();

  for (int k = 0; k < nSum; k++)
    sum[k] = 0;

  for (int n = 0; n < nquad; n++)
  {
    quad.coordinates(n, s, t, u);
    quad.weight(n, w);

    basis->evalBasis( s, t, u, facesign, phi, nBasis );

    int m = 0;
    for (int i = 0; i < nBasis; i++)
      for (int j = i; j < nBasis; j++)
      {
        sum[m] += w*phi[i]*phi[j];
        m++;
      }
  }

  int m = 0;
  for (int i = 0; i < nBasis; i++)
    for (int j = i; j < nBasis; j++)
    {
      if (i == j)
        BOOST_CHECK_CLOSE( sum[m], 1.0, tol );
      else
        BOOST_CHECK_SMALL( sum[m], tol );
      //std::cout << m << ", " << i << ", " << j << " : " << sum[m] << std::endl;
      m++;
    }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/BasisFunction/BasisFunctionVolume_Hex_Legendre_pattern.txt", true );

  BasisFunctionVolumeBase<Hex>::getBasisFunction(0, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(1, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(2, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(3, BasisFunctionCategory_Legendre)->dump( 2, output );
#if 0
  BasisFunctionVolumeBase<Hex>::getBasisFunction(4, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(5, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(6, BasisFunctionCategory_Legendre)->dump( 2, output );
  BasisFunctionVolumeBase<Hex>::getBasisFunction(7, BasisFunctionCategory_Legendre)->dump( 2, output );
#endif

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
