// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunction_RefElement_Split_btest
// testing of reference coordinate transformations

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunction_RefElement_Split.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunction_RefElement_Split_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_Split_test )
{
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordType;
  typedef BasisFunction_RefElement_Split<TopoD1, Line> RefElementSplitType;

  const Real tol = 1e-13;

  RefCoordType ref_sub, ref_main;
  int split_edge_index = -1;

  ref_sub[0] = 0.0;
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );

  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );

  ref_sub[0] = 0.5;
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );

  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.75, tol );

  ref_sub[0] = 1.0;
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );

  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );

  //Incorrect split-type
  // an edge split on a line is the same as an isotropic split so this should be allowed
  //  split_edge_index = -1;
 // BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main)),
 //                    DeveloperException );

  //Incorrect split_edge_index
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main)),
                     DeveloperException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Triangle_Split_test )
{
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;
  typedef BasisFunction_RefElement_Split<TopoD2, Triangle> RefElementSplitType;

  const Real tol = 1e-13;

  RefCoordType ref_sub, ref_main;

  //Splitting trace 0 - sub element 0 --------------------
  int split_edge_index = 0;

  ref_sub = {0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  ref_sub = {0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.75, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );

  //Splitting trace 0 - sub element 1 --------------------

  ref_sub = {0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  ref_sub = {0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );

  ref_sub = {0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.75, tol );

  //Splitting trace 1 - sub element 0 --------------------
  split_edge_index = 1;

  ref_sub = {0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  ref_sub = {1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );

  ref_sub = {0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  //Splitting trace 1 - sub element 1 --------------------

  ref_sub = {0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  ref_sub = {0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );

  //Splitting trace 2 - sub element 0 --------------------
  split_edge_index = 2;

  ref_sub = {0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );

  ref_sub = {0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  //Splitting trace 2 - sub element 1 --------------------

  ref_sub = {0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );

  ref_sub = {0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );


  //Uniform split - sub element 0 --------------------
  split_edge_index = -1;

  ref_sub = {0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  ref_sub = {0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.75, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );

  //Uniform split - sub element 1 --------------------

  ref_sub = {0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  ref_sub = {1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  ref_sub = {0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );

  ref_sub = {0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.75, tol );

  //Uniform split - sub element 2 --------------------

  ref_sub = {0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 2, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 2, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 2, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  ref_sub = {0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 2, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );

  //Uniform split - sub element 3 --------------------

  ref_sub = {0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 3, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  ref_sub = {1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 3, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );

  ref_sub = {0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 3, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );

  ref_sub = {0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 3, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );

  //Incorrect split-type
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Last, split_edge_index, 0, ref_main)),
                     DeveloperException );

  //Incorrect sub-cell-index
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 2, ref_main)),
                     DeveloperException );

  //Incorrect sub-cell-index
  split_edge_index = -1;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 4, ref_main)),
                     DeveloperException );

  //Incorrect split_edge_index
  split_edge_index = 3;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main)),
                     DeveloperException );

  //Incorrect split_edge_index
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main)),
                     DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Quad_Split_test )
{
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;
  typedef BasisFunction_RefElement_Split<TopoD2, Quad> RefElementSplitType;

  RefCoordType ref_sub, ref_main;
  int split_edge_index = 0;

  //Unsupported
  split_edge_index = 0;
  ref_sub = {0.0, 0.0};
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main)),
                     DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tet_Split_test )
{
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;
  typedef BasisFunction_RefElement_Split<TopoD3, Tet> RefElementSplitType;

  const Real tol = 1e-13;

  RefCoordType ref_sub, ref_main;

  //Splitting trace 0 - sub element 0 --------------------
  int split_edge_index = 0;

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.75, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.25, tol );

  //Splitting trace 0 - sub element 1 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.75, tol );

  //Splitting trace 1 - sub element 0 --------------------
  split_edge_index = 1;

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.75, tol );

  //Splitting trace 1 - sub element 1 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.75, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.25, tol );

  //Splitting trace 2 - sub element 0 --------------------
  split_edge_index = 2;

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.75, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.50, tol );

  //Splitting trace 2 - sub element 1 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.75, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.50, tol );

  //Splitting trace 3 - sub element 0 --------------------
  split_edge_index = 3;

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.50, tol );

  //Splitting trace 3 - sub element 1 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.50, tol );

  //Splitting trace 4 - sub element 0 --------------------
  split_edge_index = 4;

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.25, tol );

  //Splitting trace 4 - sub element 1 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.25, tol );

  //Splitting trace 5 - sub element 0 --------------------
  split_edge_index = 5;

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.50, tol );

  //Splitting trace 5 - sub element 1 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );

  ref_sub = {0.5, 0.5, 0.5};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.50, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.50, tol );


  //Uniform split - sub element 0 --------------------
  split_edge_index = -1;

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.25, 0.25, 0.25};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.125, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.625, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.125, tol );

  //Uniform split - sub element 1 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );

  ref_sub = {0.25, 0.25, 0.25};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 1, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.125, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.125, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.625, tol );

  //Uniform split - sub element 2 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 2, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 2, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 2, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 2, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.25, 0.25, 0.25};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 2, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.625, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.125, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.125, tol );

  //Uniform split - sub element 3 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 3, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 3, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 3, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 3, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.25, 0.25, 0.25};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 3, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.125, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.125, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.125, tol );

  //Uniform split - sub element 4 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 4, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 4, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 4, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 4, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.25, 0.25, 0.25};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 4, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.250, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.375, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.250, tol );

  //Uniform split - sub element 5 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 5, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 5, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 5, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 5, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.25, 0.25, 0.25};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 5, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.125, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.250, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.375, tol );

  //Uniform split - sub element 6 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 6, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 6, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 6, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 6, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.25, 0.25, 0.25};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 6, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.375, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.250, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.125, tol );

  //Uniform split - sub element 7 --------------------

  ref_sub = {0.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 7, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {1.0, 0.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 7, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );

  ref_sub = {0.0, 1.0, 0.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 7, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.0, 0.0, 1.0};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 7, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );

  ref_sub = {0.25, 0.25, 0.25};
  RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 7, ref_main);
  BOOST_CHECK_CLOSE( ref_main[0], 0.250, tol );
  BOOST_CHECK_CLOSE( ref_main[1], 0.125, tol );
  BOOST_CHECK_CLOSE( ref_main[2], 0.250, tol );

  //Incorrect split-type
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Last, split_edge_index, 0, ref_main)),
                     DeveloperException );

  //Incorrect sub-cell-index
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 2, ref_main)),
                     DeveloperException );

  //Incorrect sub-cell-index
  split_edge_index = -1;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 8, ref_main)),
                     DeveloperException );

  //Incorrect sub-cell-index
  int split_face_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::IsotropicFace, split_face_index, 4, ref_main)),
                     DeveloperException );

  //Incorrect split_edge_index
  split_edge_index = 6;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main)),
                     DeveloperException );

  //Incorrect split_edge_index
  split_edge_index = 0;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Isotropic, split_edge_index, 0, ref_main)),
                     DeveloperException );

  //Incorrect split_face_index
  split_face_index = 4;
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::IsotropicFace, split_face_index, 0, ref_main)),
                     DeveloperException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hex_Split_test )
{
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;
  typedef BasisFunction_RefElement_Split<TopoD3, Hex> RefElementSplitType;

  RefCoordType ref_sub, ref_main;
  int split_edge_index = 0;

  //Unsupported
  split_edge_index = 0;
  ref_sub = {0.0, 0.0, 0.0};
  BOOST_CHECK_THROW( (RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main)),
                     DeveloperException );
}

BOOST_AUTO_TEST_CASE( Pentatope_Split_test )
{
  typedef DLA::VectorS<TopoD4::D,Real> RefCoordType;
  typedef BasisFunction_RefElement_Split<TopoD4, Pentatope> RefElementSplitType;

  const Real tol = 1e-13;

  RefCoordType ref_sub, ref_main;

  // edge split 0
  {
    // sub element 0
    int split_edge_index = 0;

    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5 , tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    // sub element 1
    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );
  }

  // edge split 1
  {
    // sub element 0
    int split_edge_index = 1;

    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.25 , tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    // sub element 1
    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );
  }

  // edge split 2
  {
    // sub element 0
    int split_edge_index = 2;

    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5 , tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.25, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    // sub element 1
    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );
  }

  // edge split 3
  {
    // sub element 0
    int split_edge_index = 3;

    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5 , tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.25, tol );

    // sub element 1
    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );
  }

  // edge split 4
  {
    // sub element 0
    int split_edge_index = 4;

    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.75, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.25 , tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    // sub element 1
    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.75, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );
  }

  // edge split 5
  {
    // sub element 0
    int split_edge_index = 5;

    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.75, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5 , tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.25, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    // sub element 1
    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.75, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );
  }

  // edge split 6
  {
    // sub element 0
    int split_edge_index = 6;

    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.75, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5 , tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.25, tol );

    // sub element 1
    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.25, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.75, tol );
  }

  // edge split 7
  {
    // sub element 0
    int split_edge_index = 7;

    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.75 , tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.25, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    // sub element 1
    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.75, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );
  }

  // edge split 8
  {
    // sub element 0
    int split_edge_index = 8;

    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.75 , tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.25, tol );

    // sub element 1
    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.25, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.75, tol );
  }

  // edge split 9
  {
    // sub element 0
    int split_edge_index = 9;

    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 0, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5 , tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.75, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.25, tol );

    // sub element 1
    ref_sub = {0.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {1.0, 0.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 1.0, 0.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 1.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.0, tol );

    ref_sub = {0.0, 0.0, 1.0, 0.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.5, tol );

    ref_sub = {0.0, 0.0, 0.0, 1.0};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.0, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 1.0, tol );

    ref_sub = {0.5, 0.5, 0.5, 0.5};
    RefElementSplitType::transform(ref_sub, ElementSplitType::Edge, split_edge_index, 1, ref_main);
    BOOST_CHECK_CLOSE( ref_main[0], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[1], 0.5, tol );
    BOOST_CHECK_CLOSE( ref_main[2], 0.25, tol );
    BOOST_CHECK_CLOSE( ref_main[3], 0.75, tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
