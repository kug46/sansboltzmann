// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BasisFunctionVolume_Tet_Bernstein_btest
// testing of BasisFunctionVolume<Tetrahedton,Bernstein> classes

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionVolume.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Bernstein.h"
#include "BasisFunction/LagrangeDOFMap.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( BasisFunctionVolume_Tet_Bernstein_test_suite )

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( basis_BernsteinP1 )
{

  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 1;
  const int nBasis = (order + 1)*(order + 2)*(order + 3)/6;
  const int nNode  = 4;
  const int nEdge  = 6*(order - 1);
  const int nFace  = 4*(order - 1)*(order - 2)/2;
  const int nCell  = (order - 1)*(order - 2)*(order - 3)/6;


  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(order, BasisFunctionCategory_Bernstein),
                     BasisFunctionVolumeBase<Tet>::BernsteinP1 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::BernsteinP1;

  BOOST_CHECK_EQUAL( order , basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode , basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge , basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nFace , basis->nBasisFace() );
  BOOST_CHECK_EQUAL( nCell , basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Bernstein, basis->category() );

  Real s = 0.25;
  Real t = 0.4375;
  Real u = 0.25;
  Int4 edgesign = {{+1, +1, +1,+1}};
  Real phi[4] = {0.,0.,0.,0.};
  Real phis[4] = {0.,0.,0.,0.};
  Real phit[4] = {0.,0.,0.,0.};
  Real phiu[4] = {0.,0.,0.,0.};
  basis->evalBasis( s, t, u, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, u, edgesign, phis, phit, phiu, nBasis );

  Real phiTrue[4];
  phiTrue[0] =  0.062500;
  phiTrue[1] =  0.25000;
  phiTrue[2] =  0.43750;
  phiTrue[3] =  0.25000;

  Real phisTrue[4];
  phisTrue[0] = -1;
  phisTrue[1] = 1;
  phisTrue[2] = 0;
  phisTrue[3] = 0;

  Real phitTrue[4];
  phitTrue[0] = -1;
  phitTrue[1] = 0;
  phitTrue[2] = 1;
  phitTrue[3] = 0;

  Real phiuTrue[4];
  phiuTrue[0] = -1;
  phiuTrue[1] = 0;
  phiuTrue[2] = 0;
  phiuTrue[3] = 1;

  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuTrue[ki], phiu[ki], small_tol, close_tol );
  }


}


BOOST_AUTO_TEST_CASE( basis_BernsteinP2 )
{

  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 2;
  const int nBasis = (order + 1)*(order + 2)*(order + 3)/6;
  const int nNode  = 4;
  const int nEdge  = 6*(order - 1);
  const int nFace  = 4*(order - 1)*(order - 2)/2;
  const int nCell  = (order - 1)*(order - 2)*(order - 3)/6;


  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(order, BasisFunctionCategory_Bernstein),
                     BasisFunctionVolumeBase<Tet>::BernsteinP2 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::BernsteinP2;

  BOOST_CHECK_EQUAL( order , basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode , basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge , basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nFace , basis->nBasisFace() );
  BOOST_CHECK_EQUAL( nCell , basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Bernstein, basis->category() );

  Real s = 0.25;
  Real t = 0.4375;
  Real u = 0.25;
  Int4 edgesign = {{+1, +1, +1,+1}};
  Real phi[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phis[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phit[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phiu[10] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  basis->evalBasis( s, t, u, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, u, edgesign, phis, phit, phiu, nBasis );

  Real phiTrue[10];
  phiTrue[0] =  0.00390625;
  phiTrue[1] =  0.062500;
  phiTrue[2] =  0.19140625;
  phiTrue[3] =  0.062500;
  phiTrue[4] =  0.031250;
  phiTrue[5] =  0.21875;
  phiTrue[6] =  0.0546875;
  phiTrue[7] =  0.031250;
  phiTrue[8] =  0.12500;
  phiTrue[9] =  0.21875;

  Real phisTrue[10];
  phisTrue[0] = -0.12500;
  phisTrue[1] =  0.50000;
  phisTrue[2] = 0;
  phisTrue[3] = 0;
  phisTrue[4] = -0.37500;
  phisTrue[5] =  0.87500;
  phisTrue[6] = -0.87500;
  phisTrue[7] = -0.50000;
  phisTrue[8] =  0.50000;
  phisTrue[9] = 0;

  Real phitTrue[10];
  phitTrue[0] = -0.12500;
  phitTrue[1] = 0;
  phitTrue[2] =  0.87500;
  phitTrue[3] = 0;
  phitTrue[4] = -0.50000;
  phitTrue[5] =  0.50000;
  phitTrue[6] = -0.75000;
  phitTrue[7] = -0.50000;
  phitTrue[8] = 0;
  phitTrue[9] =  0.50000;

  Real phiuTrue[10];
  phiuTrue[0] = -0.12500;
  phiuTrue[1] = 0;
  phiuTrue[2] = 0;
  phiuTrue[3] =  0.50000;
  phiuTrue[4] = -0.50000;
  phiuTrue[5] = 0;
  phiuTrue[6] = -0.87500;
  phiuTrue[7] = -0.37500;
  phiuTrue[8] =  0.50000;
  phiuTrue[9] =  0.87500;

  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuTrue[ki], phiu[ki], small_tol, close_tol );
  }


}

BOOST_AUTO_TEST_CASE( basis_BernsteinP3 )
{

  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 3;
  const int nBasis = (order + 1)*(order + 2)*(order + 3)/6;
  const int nNode  = 4;
  const int nEdge  = 6*(order - 1);
  const int nFace  = 4*(order - 1)*(order - 2)/2;
  const int nCell  = (order - 1)*(order - 2)*(order - 3)/6;


  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(order, BasisFunctionCategory_Bernstein),
                     BasisFunctionVolumeBase<Tet>::BernsteinP3 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::BernsteinP3;

  BOOST_CHECK_EQUAL( order , basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode , basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge , basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nFace , basis->nBasisFace() );
  BOOST_CHECK_EQUAL( nCell , basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Bernstein, basis->category() );

  Real s = 0.25;
  Real t = 0.4375;
  Real u = 0.25;
  Int4 edgesign = {{+1, +1, +1,+1}};
  Real phi[20] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phis[20] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phit[20] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phiu[20] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  basis->evalBasis( s, t, u, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, u, edgesign, phis, phit, phiu, nBasis );

  Real phiTrue[20];
  phiTrue[0] =  0.0002441406250000;
  phiTrue[1] =  0.0156250000000000;
  phiTrue[2] =  0.0837402343750000;
  phiTrue[3] =  0.0156250000000000;
  phiTrue[4] =  0.00292968750000000;
  phiTrue[5] =  0.0117187500000000;
  phiTrue[6] =  0.0820312500000000;
  phiTrue[7] =  0.143554687500000;
  phiTrue[8] =  0.0358886718750000;
  phiTrue[9] =  0.00512695312500000;
  phiTrue[10] =  0.00292968750000000;
  phiTrue[11] =  0.0117187500000000;
  phiTrue[12] =  0.143554687500000;
  phiTrue[13] =  0.0820312500000000;
  phiTrue[14] =  0.0468750000000000;
  phiTrue[15] =  0.0468750000000000;
  phiTrue[16] =  0.0410156250000000;
  phiTrue[17] =  0.0410156250000000;
  phiTrue[18] =  0.0234375000000000;
  phiTrue[19] =  0.164062500000000;

  Real phisTrue[20];
  phisTrue[0] = -0.0117187500000000;
  phisTrue[1] =  0.187500000000000;
  phisTrue[2] = 0;
  phisTrue[3] = 0;
  phisTrue[4] = -0.0820312500000000;
  phisTrue[5] = -0.0937500000000000;
  phisTrue[6] =  0.656250000000000;
  phisTrue[7] =  0.574218750000000;
  phisTrue[8] = -0.574218750000000;
  phisTrue[9] = -0.164062500000000;
  phisTrue[10] = -0.0937500000000000;
  phisTrue[11] = -0.187500000000000;
  phisTrue[12] = 0;
  phisTrue[13] = 0;
  phisTrue[14] =  0.375000000000000;
  phisTrue[15] =  0.187500000000000;
  phisTrue[16] = -0.492187500000000;
  phisTrue[17] = -0.656250000000000;
  phisTrue[18] = -0.281250000000000;
  phisTrue[19] =  0.656250000000000;

  Real phitTrue[20];
  phitTrue[0] = -0.0117187500000000;
  phitTrue[1] = 0;
  phitTrue[2] =  0.574218750000000;
  phitTrue[3] = 0;
  phitTrue[4] = -0.0937500000000000;
  phitTrue[5] = -0.187500000000000;
  phitTrue[6] =  0.187500000000000;
  phitTrue[7] =  0.656250000000000;
  phitTrue[8] = -0.410156250000000;
  phitTrue[9] = -0.152343750000000;
  phitTrue[10] = -0.0937500000000000;
  phitTrue[11] = -0.187500000000000;
  phitTrue[12] =  0.656250000000000;
  phitTrue[13] =  0.187500000000000;
  phitTrue[14] = 0;
  phitTrue[15] = 0;
  phitTrue[16] = -0.562500000000000;
  phitTrue[17] = -0.562500000000000;
  phitTrue[18] = -0.375000000000000;
  phitTrue[19] =  0.375000000000000;

  Real phiuTrue[20];
  phiuTrue[0] = -0.0117187500000000;
  phiuTrue[1] = 0;
  phiuTrue[2] = 0;
  phiuTrue[3] =  0.187500000000000;
  phiuTrue[4] = -0.0937500000000000;
  phiuTrue[5] = -0.187500000000000;
  phiuTrue[6] = 0;
  phiuTrue[7] = 0;
  phiuTrue[8] = -0.574218750000000;
  phiuTrue[9] = -0.164062500000000;
  phiuTrue[10] = -0.0820312500000000;
  phiuTrue[11] = -0.0937500000000000;
  phiuTrue[12] =  0.574218750000000;
  phiuTrue[13] =  0.656250000000000;
  phiuTrue[14] =  0.187500000000000;
  phiuTrue[15] =  0.375000000000000;
  phiuTrue[16] = -0.656250000000000;
  phiuTrue[17] = -0.492187500000000;
  phiuTrue[18] = -0.281250000000000;
  phiuTrue[19] =  0.656250000000000;

  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuTrue[ki], phiu[ki], small_tol, close_tol );
  }


}

BOOST_AUTO_TEST_CASE( basis_BernsteinP4 )
{

  typedef std::array<int,4> Int4;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-11;

  const int order  = 4;
  const int nBasis = (order + 1)*(order + 2)*(order + 3)/6;
  const int nNode  = 4;
  const int nEdge  = 6*(order - 1);
  const int nFace  = 4*(order - 1)*(order - 2)/2;
  const int nCell  = (order - 1)*(order - 2)*(order - 3)/6;


  BOOST_CHECK_EQUAL( BasisFunctionVolumeBase<Tet>::getBasisFunction(order, BasisFunctionCategory_Bernstein),
                     BasisFunctionVolumeBase<Tet>::BernsteinP4 );

  const BasisFunctionVolumeBase<Tet>* basis = BasisFunctionVolumeBase<Tet>::BernsteinP4;

  BOOST_CHECK_EQUAL( order , basis->order() );
  BOOST_CHECK_EQUAL( nBasis, basis->nBasis() );
  BOOST_CHECK_EQUAL( nNode , basis->nBasisNode() );
  BOOST_CHECK_EQUAL( nEdge , basis->nBasisEdge() );
  BOOST_CHECK_EQUAL( nFace , basis->nBasisFace() );
  BOOST_CHECK_EQUAL( nCell , basis->nBasisCell() );
  BOOST_CHECK_EQUAL( BasisFunctionCategory_Bernstein, basis->category() );

  Real s = 0.25;
  Real t = 0.4375;
  Real u = 0.25;
  Int4 edgesign = {{+1, +1, +1,+1}};
  Real phi[35] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phis[35] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phit[35] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  Real phiu[35] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  basis->evalBasis( s, t, u, edgesign, phi, nBasis );
  basis->evalBasisDerivative( s, t, u, edgesign, phis, phit, phiu, nBasis );

  Real phiTrue[35];
  phiTrue[0] =   0.0000152587890625000;
  phiTrue[1] =   0.00390625000000000;
  phiTrue[2] =   0.0366363525390625;
  phiTrue[3] =   0.00390625000000000;
  phiTrue[4] =   0.000244140625000000;
  phiTrue[5] =   0.00146484375000000;
  phiTrue[6] =   0.00390625000000000;
  phiTrue[7] =   0.0273437500000000;
  phiTrue[8] =   0.0717773437500000;
  phiTrue[9] =   0.0837402343750000;
  phiTrue[10] =  0.0209350585937500;
  phiTrue[11] =  0.00448608398437500;
  phiTrue[12] =  0.000427246093750000;
  phiTrue[13] =  0.0837402343750000;
  phiTrue[14] =  0.0717773437500000;
  phiTrue[15] =  0.0273437500000000;
  phiTrue[16] =  0.000244140625000000;
  phiTrue[17] =  0.00146484375000000;
  phiTrue[18] =  0.00390625000000000;
  phiTrue[19] =  0.0156250000000000;
  phiTrue[20] =  0.0234375000000000;
  phiTrue[21] =  0.0156250000000000;
  phiTrue[22] =  0.00512695312500000;
  phiTrue[23] =  0.0205078125000000;
  phiTrue[24] =  0.0358886718750000;
  phiTrue[25] =  0.00512695312500000;
  phiTrue[26] =  0.0205078125000000;
  phiTrue[27] =  0.0358886718750000;
  phiTrue[28] =  0.00292968750000000;
  phiTrue[29] =  0.0117187500000000;
  phiTrue[30] =  0.0117187500000000;
  phiTrue[31] =  0.0820312500000000;
  phiTrue[32] =  0.143554687500000;
  phiTrue[33] =  0.0820312500000000;
  phiTrue[34] =  0.0410156250000000;

  Real phisTrue[35];
  phisTrue[0] =   -0.000976562500000000;
  phisTrue[1] =  0.0625000000000000;
  phisTrue[2] = 0;
  phisTrue[3] = 0;
  phisTrue[4] = -0.0107421875000000;
  phisTrue[5] = -0.0351562500000000;
  phisTrue[6] = -0.0156250000000000;
  phisTrue[7] =  0.328125000000000;
  phisTrue[8] =  0.574218750000000;
  phisTrue[9] =  0.334960937500000;
  phisTrue[10] = -0.334960937500000;
  phisTrue[11] = -0.143554687500000;
  phisTrue[12] = -0.0205078125000000;
  phisTrue[13] = 0;
  phisTrue[14] = 0;
  phisTrue[15] = 0;
  phisTrue[16] = -0.0117187500000000;
  phisTrue[17] = -0.0468750000000000;
  phisTrue[18] = -0.0625000000000000;
  phisTrue[19] =  0.187500000000000;
  phisTrue[20] =  0.187500000000000;
  phisTrue[21] =  0.0625000000000000;
  phisTrue[22] = -0.143554687500000;
  phisTrue[23] = -0.164062500000000;
  phisTrue[24] = -0.430664062500000;
  phisTrue[25] = -0.164062500000000;
  phisTrue[26] = -0.328125000000000;
  phisTrue[27] = -0.574218750000000;
  phisTrue[28] = -0.0820312500000000;
  phisTrue[29] = -0.0937500000000000;
  phisTrue[30] = -0.140625000000000;
  phisTrue[31] =  0.656250000000000;
  phisTrue[32] =  0.574218750000000;
  phisTrue[33] =  0.328125000000000;
  phisTrue[34] = -0.492187500000000;

  Real phitTrue[35];
  phitTrue[0] =   -0.000976562500000000;
  phitTrue[1] = 0;
  phitTrue[2] =  0.334960937500000;
  phitTrue[3] = 0;
  phitTrue[4] = -0.0117187500000000;
  phitTrue[5] = -0.0468750000000000;
  phitTrue[6] = -0.0625000000000000;
  phitTrue[7] =  0.0625000000000000;
  phitTrue[8] =  0.328125000000000;
  phitTrue[9] =  0.574218750000000;
  phitTrue[10] = -0.191406250000000;
  phitTrue[11] = -0.123046875000000;
  phitTrue[12] = -0.0195312500000000;
  phitTrue[13] =  0.574218750000000;
  phitTrue[14] =  0.328125000000000;
  phitTrue[15] =  0.0625000000000000;
  phitTrue[16] = -0.0117187500000000;
  phitTrue[17] = -0.0468750000000000;
  phitTrue[18] = -0.0625000000000000;
  phitTrue[19] = 0;
  phitTrue[20] = 0;
  phitTrue[21] = 0;
  phitTrue[22] = -0.152343750000000;
  phitTrue[23] = -0.281250000000000;
  phitTrue[24] = -0.410156250000000;
  phitTrue[25] = -0.152343750000000;
  phitTrue[26] = -0.281250000000000;
  phitTrue[27] = -0.410156250000000;
  phitTrue[28] = -0.0937500000000000;
  phitTrue[29] = -0.187500000000000;
  phitTrue[30] = -0.187500000000000;
  phitTrue[31] =  0.187500000000000;
  phitTrue[32] =  0.656250000000000;
  phitTrue[33] =  0.187500000000000;
  phitTrue[34] = -0.56250000000000;

  Real phiuTrue[35];
  phiuTrue[0] =   -0.000976562500000000;
  phiuTrue[1] = 0;
  phiuTrue[2] = 0;
  phiuTrue[3] =  0.0625000000000000;
  phiuTrue[4] = -0.0117187500000000;
  phiuTrue[5] = -0.0468750000000000;
  phiuTrue[6] = -0.0625000000000000;
  phiuTrue[7] = 0;
  phiuTrue[8] = 0;
  phiuTrue[9] = 0;
  phiuTrue[10] = -0.334960937500000;
  phiuTrue[11] = -0.143554687500000;
  phiuTrue[12] = -0.0205078125000000;
  phiuTrue[13] =  0.334960937500000;
  phiuTrue[14] =  0.574218750000000;
  phiuTrue[15] =  0.328125000000000;
  phiuTrue[16] = -0.0107421875000000;
  phiuTrue[17] = -0.0351562500000000;
  phiuTrue[18] = -0.0156250000000000;
  phiuTrue[19] =  0.0625000000000000;
  phiuTrue[20] =  0.187500000000000;
  phiuTrue[21] =  0.187500000000000;
  phiuTrue[22] = -0.164062500000000;
  phiuTrue[23] = -0.328125000000000;
  phiuTrue[24] = -0.574218750000000;
  phiuTrue[25] = -0.143554687500000;
  phiuTrue[26] = -0.164062500000000;
  phiuTrue[27] = -0.430664062500000;
  phiuTrue[28] = -0.0820312500000000;
  phiuTrue[29] = -0.140625000000000;
  phiuTrue[30] = -0.0937500000000000;
  phiuTrue[31] =  0.328125000000000;
  phiuTrue[32] =  0.574218750000000;
  phiuTrue[33] =  0.656250000000000;
  phiuTrue[34] = -0.492187500000000;

  for (int ki = 0; ki < nBasis; ki++)
  {
    SANS_CHECK_CLOSE( phiTrue[ki], phi[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phisTrue[ki], phis[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phitTrue[ki], phit[ki], small_tol, close_tol );
    SANS_CHECK_CLOSE( phiuTrue[ki], phiu[ki], small_tol, close_tol );
  }


}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
