// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ReferenceElement_btest
// testing of 2-D reference element operators

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"   // Triangle
#include "BasisFunction/TraceToCellRefCoord.h"


using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( TraceToCellRefCoord_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_eval_test )
{
  const Real tol = 1e-13;
  Real sRefLine, tRefLineTrue;
  CanonicalTraceToCell canonicalNode(0,0);

  canonicalNode.trace = 0;
  tRefLineTrue = 1;

  TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalNode, sRefLine );
  BOOST_CHECK_CLOSE( tRefLineTrue, sRefLine, tol );

  canonicalNode.trace = 1;
  tRefLineTrue = 0;

  TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalNode, sRefLine );
  BOOST_CHECK_CLOSE( tRefLineTrue, sRefLine, tol );


  typedef TraceToCellRefCoord<Node, TopoD1, Line> TraceToCellRefCoordNodeLine;

  canonicalNode.trace = 3;
  BOOST_CHECK_THROW( TraceToCellRefCoordNodeLine::eval( canonicalNode, sRefLine ), DeveloperException );

  canonicalNode.trace = -3;
  BOOST_CHECK_THROW( TraceToCellRefCoordNodeLine::eval( canonicalNode, sRefLine ), DeveloperException );

  canonicalNode.trace = -1;
  BOOST_CHECK_THROW( TraceToCellRefCoordNodeLine::eval( canonicalNode, sRefLine ), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_Orientation_test )
{
  const int line[2] = {0,1};

  const int (*TraceNodes)[ Node::NTrace ] = TraceToCellRefCoord<Node, TopoD1, Line>::TraceNodes;

  CanonicalTraceToCell canonicalNode(0,0);

  for (int node = 0; node < 2; node++)
  {
    // Orientation is 1 for all edges

    const int point[1] = { line[TraceNodes[node][0]] };

    canonicalNode = TraceToCellRefCoord<Node,TopoD1,Line>::getCanonicalTrace( point, 1, line, 2 );

    BOOST_CHECK_EQUAL( canonicalNode.trace, node );
    BOOST_CHECK_EQUAL( canonicalNode.orientation, 0 );
  }
}




//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
