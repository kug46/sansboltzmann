// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_8Tet_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_8Tet_X1_1Group_test )
{
  typedef std::array<int,4> Int4;

  XField3D_8Tet_X1_1Group xfld;

#if 0
  xfld.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( xfld.nDOF(), 10 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 1 );

  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], 0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0], 0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[2], 0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1], 0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[2], 0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[2], 0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0], 0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[1], 0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[2], 0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(9)[0], 0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(9)[1], 0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(9)[2], 0.0 );


  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  const XField3D_8Tet_X1_1Group::FieldCellGroupType<Tet>& xfldCellGroup = xfld.getCellGroup<Tet>(0);

  BOOST_CHECK_EQUAL( xfldCellGroup.nElem(), 8 );

  int nodeMap[4];

  // Index table for each tet in the hex
  const int tets[8][4] = { {7, 6, 2, 4},
                           {8, 5, 4, 3},
                           {9, 1, 6, 5},
                           {0, 9, 7, 8},

                           {7, 6, 4, 5},
                           {8, 4, 5, 7},
                           {9, 5, 6, 7},
                           {9, 7, 8, 5} };

  for (int cell = 0; cell < 8; cell++)
  {
    xfldCellGroup.associativity(cell).getNodeGlobalMapping( nodeMap, 4 );
    BOOST_CHECK_EQUAL( nodeMap[0], tets[cell][0] );
    BOOST_CHECK_EQUAL( nodeMap[1], tets[cell][1] );
    BOOST_CHECK_EQUAL( nodeMap[2], tets[cell][2] );
    BOOST_CHECK_EQUAL( nodeMap[3], tets[cell][3] );
  }

  Int4 faceSign;

  //Corner cells
  for (int cell = 0; cell < 4; cell++)
  {
    faceSign = xfldCellGroup.associativity(cell).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], +1 );
    BOOST_CHECK_EQUAL( faceSign[1], +1 );
    BOOST_CHECK_EQUAL( faceSign[2], +1 );
    BOOST_CHECK_EQUAL( faceSign[3], +1 );
  }

  //Inner cells
  faceSign = xfldCellGroup.associativity(4).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup.associativity(5).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -3 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup.associativity(6).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -3 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup.associativity(7).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -2 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], -1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );


  // interior-face field variable
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField3D_8Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldITraceGroup = xfld.getInteriorTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 8 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupRight(), 0 );

  // Index table for each tet in the hex
  const int Itraces[8][3] = { {7, 6, 4},
                              {8, 4, 5},
                              {9, 5, 6},
                              {9, 7, 8},

                              {7, 5, 4},
                              {7, 6, 5},
                              {8, 7, 5},
                              {9, 5, 7} };


  int Itrace_elemLR_list[8][2] = {{0,4},{1,5},{2,6},{3,7},
                                  {4,5},{4,6},{5,7},{6,7}};

  int Itrace_faceLR_list[8][2] = {{2,3},{3,3},{1,3},{0,3},
                                  {1,0},{2,0},{1,0},{2,2}};

  for (int faceI = 0; faceI < 8; faceI++)
  {
    int elemL = Itrace_elemLR_list[faceI][0];
    int elemR = Itrace_elemLR_list[faceI][1];

    int faceL = Itrace_faceLR_list[faceI][0];
    int faceR = Itrace_faceLR_list[faceI][1];

    xfldITraceGroup.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
    for (int i = 0; i < 3; i++)
      BOOST_CHECK_EQUAL( nodeMap[i], Itraces[faceI][i] );

    BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(faceI), elemL );
    BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(faceI), elemR );
    BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(faceI).trace, faceL );
    BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(faceI).trace, faceR );

    BOOST_CHECK_EQUAL( xfldCellGroup.associativity(elemL).faceSign()[faceL], +1 );
    BOOST_CHECK_EQUAL( xfldCellGroup.associativity(elemR).faceSign()[faceR], xfldITraceGroup.getCanonicalTraceRight(faceI).orientation );
  }


  // boundary-face field variable


  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField3D_8Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBTraceGroup = xfld.getBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.nElem(), 16 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getGroupLeft(), 0 );

  int Btrace_elem_face_list[16][2] = {{0,0},{1,0},{2,0},{4,0},
                                      {1,1},{0,1},{3,1},{5,2},
                                      {2,2},{1,2},{3,2},{7,1},
                                      {0,3},{2,3},{3,3},{6,1}};

  for (int faceB = 0; faceB < 16; faceB++)
  {
      int elemL = Btrace_elem_face_list[faceB][0];
      int faceL = Btrace_elem_face_list[faceB][1];

      xfldBTraceGroup.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
      for (int i = 0; i < 3; i++)
        BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

      BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementLeft(faceB), elemL );
      BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(faceB).trace, faceL );

      BOOST_CHECK_EQUAL( xfldCellGroup.associativity(elemL).faceSign()[faceL], +1 );
  }

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
