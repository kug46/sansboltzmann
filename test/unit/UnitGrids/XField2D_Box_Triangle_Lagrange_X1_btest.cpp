// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/linspace.h"

#include "Field/output_Tecplot.h"

#include "XField2D_Box_Triangle_Lagrange_X1.h"

#include "unit/Field/XField2D_CheckTraceCoord2D_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Triangle_Lagrange_X1_test )
{
  // global communicator
  mpi::communicator world;

  XField2D_Box_Triangle_Lagrange_X1 xfld(world, 3, 3, 0., 1., 0., 1.);
  //output_Tecplot(xfld, "tmp/xfld_rank" + std::to_string(world.rank()) + ".dat");

  BOOST_REQUIRE_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE_EQUAL( 4, xfld.nBoundaryTraceGroups() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
        CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Triangle_Lagrange_X1_Periodic_test1 )
{
  // global communicator
  mpi::communicator world;

  const std::array<bool,2> periodic = {{ true, false }};
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, 3, 3, 0., 1., 0., 1., periodic);
  //output_Tecplot(xfld, "tmp/xfld_rank" + std::to_string(world.rank()) + ".dat");

  BOOST_REQUIRE_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE_EQUAL( 4, xfld.nBoundaryTraceGroups() );

  // check the left cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iLeft).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iRight).nElem() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
        CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Triangle_Lagrange_X1_Periodic_test2 )
{
  // global communicator
  mpi::communicator world;

  const std::array<bool,2> periodic = {{ false, true }};
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, 3, 3, 0., 1., 0., 1., periodic);
  //output_Tecplot(xfld, "tmp/xfld_rank" + std::to_string(world.rank()) + ".dat");

  BOOST_REQUIRE_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE_EQUAL( 4, xfld.nBoundaryTraceGroups() );

  // check the bottom cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iBottom).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iTop).nElem() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
        CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Triangle_Lagrange_X1_Periodic_test3 )
{
  // global communicator
  mpi::communicator world;

  const std::array<bool,2> periodic = {{ true, true }};
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, 3, 3, 0., 1., 0., 1., periodic);
  //output_Tecplot(xfld, "tmp/xfld_rank" + std::to_string(world.rank()) + ".dat");

  BOOST_REQUIRE_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE_EQUAL( 4, xfld.nBoundaryTraceGroups() );

  // check the left cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iLeft).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iRight).nElem() );

  // check the bottom cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iBottom).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iTop).nElem() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
        CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Triangle_Lagrange_X1_Periodic_test4 )
{
  // global communicator
  mpi::communicator world;
  for (int power = 1; power <= 3; power++)
  {
    int ii = pow(2,power);
    int jj = ii;
    const std::array<bool,2> periodic = {{ true, true }};
    XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj, 0., 1., 0., 1., periodic);
    //output_Tecplot(xfld, "tmp/xfld_rank" + std::to_string(world.rank()) + ".dat");

    BOOST_REQUIRE_EQUAL( 1, xfld.nCellGroups() );
    BOOST_REQUIRE_EQUAL( 4, xfld.nBoundaryTraceGroups() );

    // check the left cell group
    BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iLeft).getGroupRight() );

    // the periodic BC should have no elements
    BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iRight).nElem() );

    // check the bottom cell group
    BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iBottom).getGroupRight() );

    // the periodic BC should have no elements
    BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iTop).nElem() );

    const Real small_tol = 1e-11;
    const Real close_tol = 1e-11;

    // check that grid trace is identical for adjacent elements
    if (xfld.nInteriorTraceGroups() > 0)
      for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
          CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
        CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
