// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_FlatPlate_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_FlatPlate_X1_test )
{
//  int power = 0;

  // Check is called at the end of the constructor, hence this is actually tested

  mpi::communicator world;

  for (int i = 0; i < 1; i++)
  {
    XField3D_FlatPlate_X1 xfld1( world, i );

#if 0
      string filename = "tmp/flatplate";
      filename += to_string(world.rank());
      filename += "_X";
      filename += to_string(i);
      filename += ".plt";
      output_Tecplot(xfld1, filename);

//      filename += ".grm";
//      output_grm(xfld1, filename);
#endif
  }

  mpi::communicator comm_local = world.split(world.rank());

  XField3D_FlatPlate_X1 xfld_global( comm_local, 0 );

  // Check that all the boundary trace element IDs are set correctly
  // This check works because the boundary groups were add in order
  int bndID = 0;
  for (int i = 0; i < xfld_global.nBoundaryTraceGroups(); i++)
  {
    XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTrace = xfld_global.getBoundaryTraceGroup<Triangle>(i);

    int nElem = xfldBTrace.nElem();

    const std::vector<int>& boundaryTraceIDs = xfld_global.boundaryTraceIDs(i);

    BOOST_REQUIRE_EQUAL(nElem, boundaryTraceIDs.size());
    for (int elem = 0; elem < nElem; elem++)
    {
      BOOST_CHECK_EQUAL(bndID, boundaryTraceIDs[elem]);
      bndID++;
    }
  }
}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
