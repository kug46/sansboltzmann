// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD3D_GaussianBUMP_XQ
#define XFIELD3D_GaussianBUMP_XQ

#include "XField3D_Box_Tet_X1.h"

namespace SANS
{

// cubic-source bump channel flow
// triangle grid in a 3x1 channel with 4 sides as separate boundary-edge groups
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// area elements in 1 group
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 4 groups: lower, right, upper, left
//
// all edges are straight (X1)

class XField3D_GaussianBump_Xq : public XField3D_Box_Tet_X1
{
public:
  XField3D_GaussianBump_Xq( mpi::communicator comm, const int ii, const int jj, const int kk);
  XField3D_GaussianBump_Xq( mpi::communicator comm, const int ii, const int jj, const int kk, const int order );

protected:
  void init_X1(mpi::communicator comm, const int ii, const int jj, const int kk);
};


}

#endif // XFIELD3D_GaussianBUMP_XQ
