// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField3D_FlatPlate_X1.h"

#include <map>

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Field/Partition/XField_Lagrange.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace // private to this file
{
//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj x kk (quad) elements
//

double xcoord( const Real xmin, const Real xmax, const int iL, int i )
{

  Real itmp = i;
  Real iLtmp = iL;

  // COARSE SPACING AT INLET
//  Real delta = 3.2;
//  Real tmp = (xmax-xmin)*(tanh( delta*(itmp/iLtmp) ) / tanh(delta) - 1);

  //FINE SPACING AT INLET
  Real delta = 5.0;
  Real tmp = (xmax-xmin)*0.5*(1 + tanh(delta*( itmp/iLtmp - 0.5) )/tanh(delta/2) ) + xmin;

  return tmp;
}

double xcoordIn( const Real xmin, const Real xmax, const int iL, int i )
{

  Real itmp = i;
  Real iLtmp = iL;

  // COARSE SPACING AT INLET
  Real delta = 3.0;
  Real tmp = (xmax-xmin)*(tanh( delta*(itmp/iLtmp) ) / tanh(delta) - 1);

  //FINE SPACING AT INLET
//  Real delta = 5.4;
//  Real tmp = (xmax-xmin)*0.5*(1 + tanh(delta*( itmp/iLtmp - 0.5) )/tanh(delta/2) ) + xmin;

  return tmp;
}

double xcoordOut( const Real xmin, const Real xmax, const int iL, int i )
{

  Real itmp = i;
  Real iLtmp = iL;

  // COARSE SPACING AT INLET
  Real delta = 2.8;
  Real tmp = xmax-(xmax-xmin)*(tanh( delta*( 1- itmp/iLtmp) ) / tanh(delta) );

  //FINE SPACING AT INLET
//  Real delta = 5.4;
//  Real tmp = (xmax-xmin)*0.5*(1 + tanh(delta*( itmp/iLtmp - 0.5) )/tanh(delta/2) ) + xmin;

  return tmp;
}


double ycoord( const Real ymax, const int jj, int j )
{
  Real jtmp = j;
  Real jjtmp = jj;

  Real delta = 3.4;
  Real tmp = ymax*(1 + tanh( delta*( jtmp/jjtmp - 1) )/ tanh(delta));

  return tmp;

}

}

namespace SANS
{

const int XField3D_FlatPlate_X1::iSlipIn  = 0;
const int XField3D_FlatPlate_X1::iPlate   = 1;
const int XField3D_FlatPlate_X1::iSlipOut = 2;
const int XField3D_FlatPlate_X1::iRight   = 3;
const int XField3D_FlatPlate_X1::iTop     = 4;
const int XField3D_FlatPlate_X1::iLeft    = 5;
const int XField3D_FlatPlate_X1::iBack    = 6;
const int XField3D_FlatPlate_X1::iFront   = 7;

/*
// Node ordering of the original hex
//
//         y
//  2----------3
//  |\     ^   |\
//  | \    |   | \
//  |  \   |   |  \
//  |   6------+---7
//  |   |  +-- |-- | -> x
//  0---+---\--1   |
//   \  |    \  \  |
//    \ |     \  \ |
//     \|      z  \|
//      4----------5

// First split hex into two prism, then divide each prism into 3 tets
//
// Nodes that make up each of the 6 hexahedron:
//
// Left prism
// Tet[0]: {0, 1, 2, 4}
// Tet[1]: {1, 4, 3, 2}
// Tet[2]: {6, 2, 3, 4}
//
// Right prism
// Tet[3]: {1, 4, 5, 3}
// Tet[4]: {4, 6, 5, 3}
// Tet[5]: {7, 6, 3, 5}
//
// The specific ordering for each tetrahedron is chosen to give positive volumes,
// but without any other consideration
*/


XField3D_FlatPlate_X1::XField3D_FlatPlate_X1(mpi::communicator& comm, const int power )
{
  XField_Lagrange<PhysD3>::VectorX X;

  XField_Lagrange<PhysD3> xfldin(comm);

  // Arthur version of params | const Real center = 0.5; const Real length = 1; const Real offset = 1;
  const Real center = 0.5; const Real length = 1; const Real offset = 1; const Real height = 2;

  // TMR version
  // const Real center = 1; const Real length = 2; const Real offset = 2; const Real height = 4;

  int iL = 8*pow( 2, power );
  int iM = 16*pow( 2, power );
  int iR = 8*pow( 2, power );

  int jj = 8*pow( 2, power );
  int kk = 8*pow( 2, power );

  int ii = iL + iM + iR;

  // // Old version
  // Real xmin = -1.0;
  // Real xmax = 1.0;
  // Real xmax2 = 2.0;
  // Real ymax = 2.0;

  // New version
  Real xDmin = center - offset - (length/2);
  Real xPmin = center - (length/2);
  Real xDmax = center + offset + (length/2);
  Real xPmax = center + (length/2);
  Real ymax = height;

  int nnode = (ii + 1)*(jj + 1)*(kk + 1);
  int nelem = 6*ii*jj*kk;

  const int joffset = (ii+1);
  const int koffset = (ii+1)*(jj+1);

  // create the grid-coordinate DOF arrays
  xfldin.sizeDOF( nnode );

  // EPIC requires all boundary nodes to be first in the grid...
  // The numerous loops below constructs first all boundary nodes, and then the interior nodes
  std::map<int,int> EPICmap;

  if (comm.rank() == 0)
  {
    int EPICidx = 0;

    // y-min
    for (int k = 0; k < kk+1; k++)
    {
      int j = 0;
      {
        for (int i = 0; i < iL+1; i++)
        {
          const int n0 = k*koffset + j*joffset + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoordIn(xDmin, xPmin, iL, i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }

        for (int i = 1; i < iM+1; i++)
        {
          const int n0 = k*koffset + j*joffset + iL + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoord(xPmin, xPmax,iM,i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }

        for (int i = 1; i < iR+1; i++)
        {
          const int n0 = k*koffset + j*joffset + iL + iM + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoordOut(xPmax, xDmax, iR, i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }
      }
    }

    // y-max
    for (int k = 0; k < kk+1; k++)
    {
      int j = jj;
      {
        for (int i = 0; i < iL+1; i++)
        {
          const int n0 = k*koffset + j*joffset + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoordIn(xDmin, xPmin, iL, i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }

        for (int i = 1; i < iM+1; i++)
        {
          const int n0 = k*koffset + j*joffset + iL + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoord(xPmin, xPmax,iM,i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }

        for (int i = 1; i < iR+1; i++)
        {
          const int n0 = k*koffset + j*joffset + iL + iM + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoordOut(xPmax, xDmax, iR, i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }
      }
    }


    // z-min
    {
      int k = 0;
      {
        for (int j = 1; j < jj; j++) // corners already covered by y-min,y-max
        {
          for (int i = 0; i < iL+1; i++)
          {
            const int n0 = k*koffset + j*joffset + i;
            EPICmap[n0] = EPICidx++;

            X[0] = xcoordIn(xDmin, xPmin, iL, i);
            X[1] = ycoord(ymax, jj, j);
            X[2] = k/Real(kk);
            xfldin.addDOF(X);
          }

          for (int i = 1; i < iM+1; i++)
          {
            const int n0 = k*koffset + j*joffset + iL + i;
            EPICmap[n0] = EPICidx++;

            X[0] = xcoord(xPmin, xPmax,iM,i);
            X[1] = ycoord(ymax, jj, j);
            X[2] = k/Real(kk);
            xfldin.addDOF(X);
          }

          for (int i = 1; i < iR+1; i++)
          {
            const int n0 = k*koffset + j*joffset + iL + iM + i;
            EPICmap[n0] = EPICidx++;

            X[0] = xcoordOut(xPmax, xDmax, iR, i);
            X[1] = ycoord(ymax, jj, j);
            X[2] = k/Real(kk);
            xfldin.addDOF(X);
          }
        }
      }
    }

    // z-max
    {
      int k = kk;
      {
        for (int j = 1; j < jj; j++) // corners already covered by y-min,y-max
        {
          for (int i = 0; i < iL+1; i++)
          {
            const int n0 = k*koffset + j*joffset + i;
            EPICmap[n0] = EPICidx++;

            X[0] = xcoordIn(xDmin, xPmin, iL, i);
            X[1] = ycoord(ymax, jj, j);
            X[2] = k/Real(kk);
            xfldin.addDOF(X);
          }

          for (int i = 1; i < iM+1; i++)
          {
            const int n0 = k*koffset + j*joffset + iL + i;
            EPICmap[n0] = EPICidx++;

            X[0] = xcoord(xPmin, xPmax,iM,i);
            X[1] = ycoord(ymax, jj, j);
            X[2] = k/Real(kk);
            xfldin.addDOF(X);
          }

          for (int i = 1; i < iR+1; i++)
          {
            const int n0 = k*koffset + j*joffset + iL + iM + i;
            EPICmap[n0] = EPICidx++;

            X[0] = xcoordOut(xPmax, xDmax, iR, i);
            X[1] = ycoord(ymax, jj, j);
            X[2] = k/Real(kk);
            xfldin.addDOF(X);
          }
        }
      }
    }

    // x-min
    for (int k = 1; k < kk; k++) // corners already covered by z-min,z-max
    {
      for (int j = 1; j < jj; j++) // corners already covered by y-min,y-max
      {
        int i = 0;
        {
          const int n0 = k*koffset + j*joffset + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoordIn(xDmin, xPmin, iL, i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }
      }
    }

    // x-max
    for (int k = 1; k < kk; k++) // corners already covered by z-min,z-max
    {
      for (int j = 1; j < jj; j++) // corners already covered by y-min,y-max
      {
        int i = iR;
        {
          const int n0 = k*koffset + j*joffset + iL + iM + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoordOut(xPmax, xDmax, iR, i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }
      }
    }

    // map the interior DOFs
    for (int k = 1; k < kk; k++)
    {
      for (int j = 1; j < jj; j++)
      {
        for (int i = 1; i < iL+1; i++) // don't use start point
        {
          const int n0 = k*koffset + j*joffset + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoordIn(xDmin, xPmin, iL, i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }

        for (int i = 1; i < iM+1; i++)
        {
          const int n0 = k*koffset + j*joffset + iL + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoord(xPmin, xPmax,iM,i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }

        for (int i = 1; i < iR; i++) // don't go to end point
        {
          const int n0 = k*koffset + j*joffset + iL + iM + i;
          EPICmap[n0] = EPICidx++;

          X[0] = xcoordOut(xPmax, xDmax, iR, i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X);
        }
      }
    }

    SANS_ASSERT_MSG( EPICidx == nnode, "EPICidx = %d, nnode = %d", EPICidx, nnode );
    SANS_ASSERT( (int)EPICmap.size() == nnode );
  }

#if 0
  if (comm.rank() == 0)
  {
    for (int k = 0; k < kk+1; k++)
    {
      for (int j = 0; j < jj+1; j++)
      {
        for (int i = 0; i < iL+1; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          X[0] = xcoordIn(xDmin, xPmin, iL, i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X, EPICmap.at(n0));

          std::cout << n0 << " " << EPICmap.at(n0) << " " << X[0] << " " << X[1] << " " << X[2] << std::endl;
        }

        for (int i = 1; i < iM+1; i++)
        {
          const int n0 = k*koffset + j*joffset + iL + i;

          X[0] = xcoord(xPmin, xPmax,iM,i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X, EPICmap.at(n0));

          std::cout << n0 << " " << EPICmap.at(n0) << " " << X[0] << " " << X[1] << " " << X[2] << std::endl;
        }

        for (int i = 1; i < iR+1; i++)
        {
          const int n0 = k*koffset + j*joffset + iL + iM + i;

          X[0] = xcoordOut(xPmax, xDmax, iR, i);
          X[1] = ycoord(ymax, jj, j);
          X[2] = k/Real(kk);
          xfldin.addDOF(X, EPICmap.at(n0));

          std::cout << n0 << " " << EPICmap.at(n0) << " " << X[0] << " " << X[1] << " " << X[2] << std::endl;
        }
      }
    }
  }
#endif

  // Index table for each tet in the hex
  const int hextets[6][4] = { {0, 1, 2, 4},
                              {1, 4, 3, 2},
                              {6, 2, 3, 4},

                              {1, 4, 5, 3},
                              {4, 6, 5, 3},
                              {7, 6, 3, 5} };

  // Start the process of adding cells
  xfldin.sizeCells(nelem);

  // element grid-coordinate DOF associativity (cell-to-node connectivity)
  int order = 1;

  if (comm.rank() == 0)
  {
    int group = 0;

    std::vector<int> tetnodes(4);

    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up an individial hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          //Loop over all tets that make up a hex
          for (int tet = 0; tet < 6; tet++)
          {
            //Get the nodes from the hex for each tet
            for (int n = 0; n < 4; n++)
              tetnodes[n] = EPICmap.at(hexnodes[hextets[tet][n]]);

            // add the indices to the group
            xfldin.addCell(group, eTet, order, tetnodes);
          }
        }
      }
    }
  }

  // Start the process of adding boundary trace elements
  xfldin.sizeBoundaryTrace(2*(2*ii*jj + 2*jj*kk + 2*ii*kk));

  if (comm.rank() == 0)
  {
    const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

    int hextetL, faceL;

    std::vector<int> faceNodes(3);

    // lower boundary slipwall left of the plate
    for (int k = 0; k < kk; k++)
    {
      int j = 0;
      {
        for (int i = 0; i < iL; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };


          hextetL = 0; faceL = 2; //Hex nodes {0, 1, X, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iSlipIn, eTriangle, faceNodes );

          hextetL = 3; faceL = 3; //Hex nodes {1, 4, 5, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iSlipIn, eTriangle, faceNodes );
        }
      }
    }

    // Plate
    for (int k = 0; k < kk; k++)
    {
      int j = 0;
      {
        for (int i = iL; i < iL+iM; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };


          hextetL = 0; faceL = 2; //Hex nodes {0, 1, X, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iPlate, eTriangle, faceNodes );

          hextetL = 3; faceL = 3; //Hex nodes {1, 4, 5, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iPlate, eTriangle, faceNodes );
        }
      }
    }

    // lower boundary slipwall right of the plate
    for (int k = 0; k < kk; k++)
    {
      int j = 0;
      {
        for (int i = (iL+iM); i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          hextetL = 0; faceL = 2; //Hex nodes {0, 1, X, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iSlipOut, eTriangle, faceNodes );

          hextetL = 3; faceL = 3; //Hex nodes {1, 4, 5, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iSlipOut, eTriangle, faceNodes );
        }
      }
    }

    // right boundary
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        int i = ii-1;
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          hextetL = 3; faceL = 1; //Hex nodes {1, X, 5, 3}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iRight, eTriangle, faceNodes );


          hextetL = 5; faceL = 1; //Hex nodes {7, X, 3, 5}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iRight, eTriangle, faceNodes );
        }
      }
    }

    // upper boundary
    for (int k = 0; k < kk; k++)
    {
      int j = jj-1;
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          hextetL = 2; faceL = 3; //Hex nodes {6, 2, 3, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iTop, eTriangle, faceNodes );


          hextetL = 5; faceL = 3; //Hex nodes {7, 6, 3, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iTop, eTriangle, faceNodes );
        }
      }
    }

    // left boundary
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        int i = 0;
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          hextetL = 0; faceL = 1; //Hex nodes {0, X, 2, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iLeft, eTriangle, faceNodes );

          hextetL = 2; faceL = 2; //Hex nodes {6, 2, X, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

          xfldin.addBoundaryTrace( iLeft, eTriangle, faceNodes );
        }
      }
    }

    // back boundary
    {
      int k = 0;
      {
        for (int j = 0; j < jj; j++)
        {
          for (int i = 0; i < ii; i++)
          {
            const int n0 = k*koffset + j*joffset + i;

            //All the nodes that make up the left hex
            const int hexnodes[8] = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 0,
                                      n0 + joffset + 1,

                                      n0 + koffset + 0,
                                      n0 + koffset + 1,
                                      n0 + koffset + joffset + 0,
                                      n0 + koffset + joffset + 1 };

            hextetL = 0; faceL = 3; //Hex nodes {0, 1, 2, X}
            for (int n = 0; n < 3; n++)
              faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

            xfldin.addBoundaryTrace( iBack, eTriangle, faceNodes );


            hextetL = 1; faceL = 1; //Hex nodes {1, X, 3, 2}
            for (int n = 0; n < 3; n++)
              faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

            xfldin.addBoundaryTrace( iBack, eTriangle, faceNodes );
          }
        }
      }
    }

    // front boundary
    {
      int k = kk-1;
      {
        for (int j = 0; j < jj; j++)
        {
          for (int i = 0; i < ii; i++)
          {
            const int n0 = k*koffset + j*joffset + i;

            //All the nodes that make up the left hex
            const int hexnodes[8] = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 0,
                                      n0 + joffset + 1,

                                      n0 + koffset + 0,
                                      n0 + koffset + 1,
                                      n0 + koffset + joffset + 0,
                                      n0 + koffset + joffset + 1 };

            hextetL = 4; faceL = 3; //Hex nodes {4, 6, 5, X}
            for (int n = 0; n < 3; n++)
              faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

            xfldin.addBoundaryTrace( iFront, eTriangle, faceNodes );


            hextetL = 5; faceL = 2; //Hex nodes {7, 6, X, 5}
            for (int n = 0; n < 3; n++)
              faceNodes[n] = EPICmap.at(hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ]);

            xfldin.addBoundaryTrace( iFront, eTriangle, faceNodes );
          }
        }
      }
    }
  }

  //construct the grid
  this->buildFrom( xfldin );
}

}
