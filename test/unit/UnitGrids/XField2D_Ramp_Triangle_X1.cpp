// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_Ramp_Triangle_X1.h"

namespace SANS
{

XField2D_Ramp_Triangle_X1::XField2D_Ramp_Triangle_X1( int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax, Real theta )
  : XField2D_Box_UnionJack_Triangle_X1(ii, jj, xmin, xmax, ymin, ymax)
{
  // Modify the grid to include the ramp
  Real offset = (xmax - xmin) / 2.0;

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      if (i < (ii+1)/2) // Before the ramp starts
      {
        DOF(j*(ii+1) + i)[0] = xmin + (xmax - xmin)*i/Real(ii);
        DOF(j*(ii+1) + i)[1] = ymin + (ymax - ymin)*j/Real(jj);
      }
      else // On the ramp
      {
        Real x = xmin + (xmax - xmin)*i/Real(ii);
        DOF(j*(ii+1) + i)[0] = x;
        DOF(j*(ii+1) + i)[1] = ymin + (ymax - ymin)*j/Real(jj) + (x - offset)*tan(theta);
      }
    }
  }

  //Check that the grid is correct
  checkGrid();
}

}
