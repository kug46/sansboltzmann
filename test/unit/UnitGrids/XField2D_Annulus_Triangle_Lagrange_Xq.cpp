// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_Annulus_Triangle_Lagrange_Xq.h"

#include <iostream>

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

namespace SANS
{

XField2D_Annulus_Triangle_Lagrange_Xq::
XField2D_Annulus_Triangle_Lagrange_Xq(mpi::communicator comm, int ii, int jj, int order, Real thetamin, Real thetamax,
          std::function<Real(Real)> Rlower, std::function<Real(Real)> Rupper)
   : XField2D_Box_Triangle_Lagrange_X1(comm, ii, jj, 0, 1, 0, 1)
{

/* Creates a triangular mesh based on an annulus with the given parameters.
 * Arguments:
 *     Real thetamin - Minimum radians angle of physical domain (where xi is 0).
 *     Real thetamax - Maximum radians angle of physical domain (where xi is 1).
 *     function Rlower - Function that returns the lower surface's height given
 *         horizontal coordinate x.
 *     function Rupper - Function that returns the upper surface's height given
 *         horizontal coordinate x.
 */

    if (comm.rank() == 0)
    {

        SANS_ASSERT_MSG( (order >= 1), "Geometric order Q must be equal or greater than 1." );
        SANS_ASSERT_MSG( (abs(thetamax-thetamin) > 0) && (abs(thetamax-thetamin) <= 2*PI),
                "Grid angle span must be larger than 0 and less or equal than 2PI.");

        if (order > 1)
        {
        XField2D_Box_Triangle_Lagrange_X1 xfld_linear(comm, ii, jj, 0, 1, 0, 1);
        buildFrom(xfld_linear, order);
        }

        Real s;
        Real t;
        Real R;
        Real theta;
        Real x;
        Real y;

        // In Lagrange, the DOFs are the s and t coordinates
        typedef FieldCellGroupType<Triangle> FieldCellGroupTypeClass;
        typedef FieldCellGroupTypeClass::template ElementType<> ElementType;
        FieldCellGroupTypeClass& xfldCell = this->getCellGroup<Triangle>(0);
        ElementType xfldElem( xfldCell.basis() );

        const int nDOF     = this->nDOF();       // total number of DOFs in mesh
        const int nDOFelem = xfldElem.nDOF();    // number of total DOFs per element

        cout << "nDOF: " << nDOF << endl;
        cout << "nDOFelem: " << nDOFelem << endl;
        for (int i = 0; i < this->nDOF(); i++)
        {
            s = this->DOF(i)[0];
            t = this->DOF(i)[1];

            theta = thetamin + s*(thetamax-thetamin);
            R = Rlower(theta) + t*(Rupper(theta) - Rlower(theta));

            x = R*cos(theta);
            y = R*sin(theta);

            this->DOF(i)[0] = x;
            this->DOF(i)[1] = y;
        }

    }
}


}
