// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "unit/UnitGrids/XField3D_GaussianBump_Xq.h"

#include <cmath> // isnan

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "LinearAlgebra/DenseLinAlg/InverseQR.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Gaussian bump channel flow
// create triangle grid in 3x1 channel with ii x jj (quad) elements
//
// diagonals oriented as Union Jack flag
//
// area elements in 1 group: 2*ii*jj triangles
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 4 groups: lower, right, upper, left

XField3D_GaussianBump_Xq::XField3D_GaussianBump_Xq( mpi::communicator comm, const int ii, const int jj, const int kk)
 : XField3D_Box_Tet_X1(comm, ii, jj, kk)
{
  init_X1(comm, ii, jj, kk);
}

void XField3D_GaussianBump_Xq::init_X1( mpi::communicator comm, const int ii, const int jj, const int kk )
{
  for (int i = 0; i < nDOF(); i++)
  {
    Real a = 3.0*DOF(i)[0]; // x-ish
    Real b = DOF(i)[1]; // y-ish
    Real c = DOF(i)[2]; // z-ish

    Real z0 = 0.0625*exp(-25.*pow(b-0.5,2)) * exp(-25.*pow(a-1.5,2));
    DOF(i)[0] = a;
    DOF(i)[2] = c*(0.5 - z0) + z0;
  }
  //Check that the grid is correct
  checkGrid();
}

// General Order P grid

XField3D_GaussianBump_Xq::XField3D_GaussianBump_Xq( mpi::communicator comm, const int ii, const int jj, const int kk, const int order )
 : XField3D_Box_Tet_X1(comm, ii, jj, kk)
{
  SANS_ASSERT_MSG( (order > 0), "order must be greater than 0" );
//  SANS_ASSERT_MSG( (order == 1), "order must be 1 right now" );

  if (order == 1)
  {
    // Simplly initialize the X1 grid
    init_X1(comm, ii, jj, kk);
    return;
  }
  else
  {
    // Create a temporary X1 grid that is used to construct the higher-order grid
    XField3D_Box_Tet_X1 xfld(comm, ii, jj, kk);
    buildFrom(xfld, order);
  }

  for (int i = 0; i < nDOF(); i++)
  {
    Real a = 3.0*DOF(i)[0]; // x-ish
    Real b = DOF(i)[1]; // y-ish
    Real c = DOF(i)[2]; // z-ish

    Real z0 = 0.0625*exp(-25.*pow(b-0.5,2)) * exp(-25.*pow(a-1.5,2));
    DOF(i)[0] = a;
    DOF(i)[2] = c*(0.5 - z0) + z0;
  }

  checkGrid();

}

}
