// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_KUHNFREUDENTHAL_H
#define XFIELD_KUHNFREUDENTHAL_H

#include <memory>
#include <vector>

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"
#include "Field/XFieldSpacetime.h"

#ifdef SANS_AVRO
#include "geometry/context.h"
#include "library/cube.h"

namespace avro
{
class Body;
}
#endif

namespace SANS
{

template<class PhysDim, class TopoDim>
class XField_KuhnFreudenthal : public XField<PhysDim,TopoDim>
{
public:
  XField_KuhnFreudenthal( mpi::communicator& comm,
                       const std::vector<int>& dims );

  #ifdef SANS_AVRO
  avro::Context* context();// { return &context_; }
  std::shared_ptr<avro::Body>& body_ptr();// { return body_; }
  #endif

protected:
  void createBody();
  void generateMesh( mpi::communicator& comm, XField_Lagrange<PhysDim>& xfldin );

  std::vector<unsigned long> dims_;
  std::vector<Real> lengths_;

  #ifdef SANS_AVRO
  avro::library::CubeMesh mesh_;
  std::shared_ptr<avro::Body> body_;
  avro::Context context_;
  #endif
};

}

#endif // XFIELD3D_BOX_TET_LAGRANGE_X1_H
