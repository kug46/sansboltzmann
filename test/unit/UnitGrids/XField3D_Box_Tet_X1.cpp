// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "Field/Partition/XField_Lagrange.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// create tetrahedral grid in unit box with ii x jj x kk (hex) elements
/*
// volume elements in 1 group: 6*ii*jj*kk tetrahdedron
// interior-triangle elements in 4 groups: x-const, y-const, z-const, hex-interior
// boundary-triangle elements in 6 groups: x-min, x-max, y-min, y-max, z-min, z-max

// Node ordering of the original hex
//
//         y
//  2----------3
//  |\     ^   |\
//  | \    |   | \
//  |  \   |   |  \
//  |   6------+---7
//  |   |  +-- |-- | -> x
//  0---+---\--1   |
//   \  |    \  \  |
//    \ |     \  \ |
//     \|      z  \|
//      4----------5

// First split hex into two prism, then divide each prism into 3 tets
//
// Nodes that make up each of the 6 hexahedron:
//
// Left prism
// Tet[0]: {0, 1, 2, 4}
// Tet[1]: {1, 4, 3, 2}
// Tet[2]: {6, 2, 3, 4}
//
// Right prism
// Tet[3]: {1, 4, 5, 3}
// Tet[4]: {4, 6, 5, 3}
// Tet[5]: {7, 6, 3, 5}
//
// The specific ordering for each tetrahedron is chosen to give positive volumes,
// but without any other consideration
*/

const int XField3D_Box_Tet_X1::iXmin = 0;
const int XField3D_Box_Tet_X1::iXmax = 1;
const int XField3D_Box_Tet_X1::iYmin = 2;
const int XField3D_Box_Tet_X1::iYmax = 3;
const int XField3D_Box_Tet_X1::iZmin = 4;
const int XField3D_Box_Tet_X1::iZmax = 5;
//---------------------------------------------------------------------------//
XField3D_Box_Tet_X1::
XField3D_Box_Tet_X1( mpi::communicator comm,
                     const int ii, const int jj, const int kk,
                     std::array<bool,3> periodic ) :
  XField3D_Box_Tet_X1(comm, ii, jj, kk, 0,1, 0,1, 0,1, periodic)
{
}

//---------------------------------------------------------------------------//
XField3D_Box_Tet_X1::
XField3D_Box_Tet_X1( mpi::communicator comm,
                              const int ii, const int jj, const int kk,
                              Real xmin, Real xmax,
                              Real ymin, Real ymax,
                              Real zmin, Real zmax,
                              std::array<bool,3> periodic ) : XField<PhysD3,TopoD3>(comm)
{
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);
  std::vector<Real> zvec(kk+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = xmin + (xmax - xmin)*i/Real(ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = ymin + (ymax - ymin)*j/Real(jj);

  for (int k = 0; k < kk+1; k++)
    zvec[k] = zmin + (zmax - zmin)*k/Real(kk);

  XField_Lagrange<PhysD3> xfldin(comm);

  generateGrid(comm, xfldin, xvec, yvec, zvec, periodic);
}

//---------------------------------------------------------------------------//
XField3D_Box_Tet_X1::
XField3D_Box_Tet_X1( mpi::communicator comm,
                              const std::vector<Real>& xvec,
                              const std::vector<Real>& yvec,
                              const std::vector<Real>& zvec,
                              std::array<bool,3> periodic ) : XField<PhysD3,TopoD3>(comm)
{
  std::size_t ii = xvec.size() - 1;
  std::size_t jj = yvec.size() - 1;
  std::size_t kk = zvec.size() - 1;

  //Check ascending order
  for (std::size_t i = 0; i < ii; i++)
    SANS_ASSERT( xvec[i] < xvec[i+1] );

  for (std::size_t j = 0; j < jj; j++)
    SANS_ASSERT( yvec[j] < yvec[j+1] );

  for (std::size_t k = 0; k < kk; k++)
    SANS_ASSERT( zvec[k] < zvec[k+1] );

  XField_Lagrange<PhysD3> xfldin(comm);

  generateGrid(comm, xfldin, xvec, yvec, zvec, periodic);
}
//---------------------------------------------------------------------------//
XField3D_Box_Tet_X1::
XField3D_Box_Tet_X1( mpi::communicator comm, const std::string& filename,
                     const int ii, const int jj, const int kk,
                     std::array<bool,3> periodic ) :
  XField3D_Box_Tet_X1(comm, filename, ii, jj, kk, 0,1, 0,1, 0,1, periodic)
{
}
//---------------------------------------------------------------------------//
XField3D_Box_Tet_X1::
XField3D_Box_Tet_X1( mpi::communicator comm, const std::string& filename,
                     const int ii, const int jj, const int kk,
                     Real xmin, Real xmax,
                     Real ymin, Real ymax,
                     Real zmin, Real zmax,
                     std::array<bool,3> periodic ) : XField<PhysD3,TopoD3>(comm)
{
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);
  std::vector<Real> zvec(kk+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = xmin + (xmax - xmin)*i/Real(ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = ymin + (ymax - ymin)*j/Real(jj);

  for (int k = 0; k < kk+1; k++)
    zvec[k] = zmin + (zmax - zmin)*k/Real(kk);

  XField_Lagrange<PhysD3> xfldin(comm, XFieldBalance::CellPartitionRead, filename);

  generateGrid(comm, xfldin, xvec, yvec, zvec, periodic );
}

//---------------------------------------------------------------------------//
XField3D_Box_Tet_X1::
XField3D_Box_Tet_X1( mpi::communicator comm, const std::string& filename,
                     const std::vector<Real>& xvec,
                     const std::vector<Real>& yvec,
                     const std::vector<Real>& zvec,
                     std::array<bool,3> periodic) : XField<PhysD3,TopoD3>(comm)
{
  std::size_t ii = xvec.size() - 1;
  std::size_t jj = yvec.size() - 1;
  std::size_t kk = zvec.size() - 1;

  //Check ascending order
  for (std::size_t i = 0; i < ii; i++)
    SANS_ASSERT( xvec[i] < xvec[i+1] );

  for (std::size_t j = 0; j < jj; j++)
    SANS_ASSERT( yvec[j] < yvec[j+1] );

  for (std::size_t k = 0; k < kk; k++)
    SANS_ASSERT( zvec[k] < zvec[k+1] );

  XField_Lagrange<PhysD3> xfldin(comm, XFieldBalance::CellPartitionRead, filename);

  generateGrid(comm, xfldin, xvec, yvec, zvec, periodic);
}
//---------------------------------------------------------------------------//
void
XField3D_Box_Tet_X1::
generateGrid( mpi::communicator comm,
              XField_Lagrange<PhysD3>& xfldin,
              const std::vector<Real>& xvec,
              const std::vector<Real>& yvec,
              const std::vector<Real>& zvec,
              std::array<bool,3> periodic )
{
  SANS_ASSERT( xvec.size() > 0 && yvec.size() > 0 && zvec.size() > 0);

  int order = 1;

  int ii = (int) xvec.size() - 1;
  int jj = (int) yvec.size() - 1;
  int kk = (int) zvec.size() - 1;

  int nnode = (ii + 1)*(jj + 1)*(kk + 1);
  int nCell = 6*ii*jj*kk;

  // create the grid-coordinate DOF arrays
  xfldin.sizeDOF( nnode );

  const int joffset = (ii+1);
  const int koffset = (ii+1)*(jj+1);

  // Add DOFs to rank 0
  if (comm.rank() == 0)
  {
    for (int k = 0; k < kk+1; k++)
    {
      for (int j = 0; j < jj+1; j++)
      {
        for (int i = 0; i < ii+1; i++)
        {
          xfldin.addDOF( { xvec[i], yvec[j], zvec[k] } );
        }
      }
    }
  }

  // Index table for each tet in the hex
  const int hextets[6][4] = { {0, 1, 2, 4},
                              {1, 4, 3, 2},
                              {6, 2, 3, 4},

                              {1, 4, 5, 3},
                              {4, 6, 5, 3},
                              {7, 6, 3, 5} };


  // Start the process of adding cells
  xfldin.sizeCells(nCell);

  // Add elements to rank 0
  if (comm.rank() == 0)
  {
    std::vector<int> tetnodes(4);

    int group = 0;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up an individial hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          //Loop over all tets that make up a hex
          for (int tet = 0; tet < 6; tet++)
          {
            //Get the nodes from the hex for each tet
            for (int n = 0; n < 4; n++)
              tetnodes[n] = hexnodes[hextets[tet][n]];

            // add the indices to the group
            xfldin.addCell(group, eTet, order, tetnodes);
          }
        }
      }
    }
  } // rank == 0

  // Start the process of adding boundary trace elements
  xfldin.sizeBoundaryTrace(2*(2*ii*jj + 2*jj*kk + 2*ii*kk));

  // Add elements to rank 0
  if (comm.rank() == 0)
  {
    const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

    int hextetL, faceL;

    std::vector<int> faceNodes(Triangle::NNode);

    // x-min boundary
    {
      int group = iXmin;

      for (int k = 0; k < kk; k++)
      {
        for (int j = 0; j < jj; j++)
        {
          int i = 0;
          {
            const int n0 = k*koffset + j*joffset + i;

            //All the nodes that make up the left hex
            const int hexnodes[8] = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 0,
                                      n0 + joffset + 1,

                                      n0 + koffset + 0,
                                      n0 + koffset + 1,
                                      n0 + koffset + joffset + 0,
                                      n0 + koffset + joffset + 1 };

            hextetL = 0; faceL = 1; //Hex nodes {0, X, 2, 4}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );

            hextetL = 2; faceL = 2; //Hex nodes {6, 2, X, 4}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
          }
        }
      }
    }

    // x-max boundary
    {
      int group = iXmax;

      for (int k = 0; k < kk; k++)
      {
        for (int j = 0; j < jj; j++)
        {
          int i = ii-1;
          {
            const int n0 = k*koffset + j*joffset + i;

            //All the nodes that make up the left hex
            const int hexnodes[8] = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 0,
                                      n0 + joffset + 1,

                                      n0 + koffset + 0,
                                      n0 + koffset + 1,
                                      n0 + koffset + joffset + 0,
                                      n0 + koffset + joffset + 1 };

            hextetL = 3; faceL = 1; //Hex nodes {1, X, 5, 3}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );


            hextetL = 5; faceL = 1; //Hex nodes {7, X, 3, 5}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
          }
        }
      }
    }

    // y-min boundary
    {
      int group = iYmin;

      for (int k = 0; k < kk; k++)
      {
        int j = 0;
        {
          for (int i = 0; i < ii; i++)
          {
            const int n0 = k*koffset + j*joffset + i;

            //All the nodes that make up the left hex
            const int hexnodes[8] = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 0,
                                      n0 + joffset + 1,

                                      n0 + koffset + 0,
                                      n0 + koffset + 1,
                                      n0 + koffset + joffset + 0,
                                      n0 + koffset + joffset + 1 };

            hextetL = 0; faceL = 2; //Hex nodes {0, 1, X, 4}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );

            hextetL = 3; faceL = 3; //Hex nodes {1, 4, 5, X}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
          }
        }
      }
    }

    // y-max boundary
    {
      int group = iYmax;

      for (int k = 0; k < kk; k++)
      {
        int j = jj-1;
        {
          for (int i = 0; i < ii; i++)
          {
            const int n0 = k*koffset + j*joffset + i;

            //All the nodes that make up the left hex
            const int hexnodes[8] = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 0,
                                      n0 + joffset + 1,

                                      n0 + koffset + 0,
                                      n0 + koffset + 1,
                                      n0 + koffset + joffset + 0,
                                      n0 + koffset + joffset + 1 };

            hextetL = 2; faceL = 3; //Hex nodes {6, 2, 3, X}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );


            hextetL = 5; faceL = 3; //Hex nodes {7, 6, 3, X}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
          }
        }
      }
    }

    // z-min boundary
    {
      int group = iZmin;

      int k = 0;
      {
        for (int j = 0; j < jj; j++)
        {
          for (int i = 0; i < ii; i++)
          {
            const int n0 = k*koffset + j*joffset + i;

            //All the nodes that make up the left hex
            const int hexnodes[8] = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 0,
                                      n0 + joffset + 1,

                                      n0 + koffset + 0,
                                      n0 + koffset + 1,
                                      n0 + koffset + joffset + 0,
                                      n0 + koffset + joffset + 1 };

            hextetL = 0; faceL = 3; //Hex nodes {0, 1, 2, X}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );


            hextetL = 1; faceL = 1; //Hex nodes {1, X, 3, 2}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
          }
        }
      }
    }

    // z-max boundary
    {
      int group = iZmax;

      int k = kk-1;
      {
        for (int j = 0; j < jj; j++)
        {
          for (int i = 0; i < ii; i++)
          {
            const int n0 = k*koffset + j*joffset + i;

            //All the nodes that make up the left hex
            const int hexnodes[8] = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 0,
                                      n0 + joffset + 1,

                                      n0 + koffset + 0,
                                      n0 + koffset + 1,
                                      n0 + koffset + joffset + 0,
                                      n0 + koffset + joffset + 1 };

            hextetL = 4; faceL = 3; //Hex nodes {4, 6, 5, X}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );


            hextetL = 5; faceL = 2; //Hex nodes {7, 6, X, 5}
            for (int n = 0; n < Triangle::NNode; n++)
              faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

            xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
          }
        }
      }
    }
  }


  std::vector<PeriodicBCNodeMap> periodicity;

  // Construct periodic information if requested
  if (comm.rank() == 0)
  {
    // x-min to x-max periodicity
    if (periodic[0])
    {
      int groupL = iXmin;
      int groupR = iXmax;

      periodicity.emplace_back(groupL, groupR);
      PeriodicBCNodeMap& period = periodicity.back();

      for (int k = 0; k < kk+1; k++)
      {
        for (int j = 0; j < jj+1; j++)
        {
          int iL = 0;
          int iR = ii;
          {
            const int nL = k*koffset + j*joffset + iL;
            const int nR = k*koffset + j*joffset + iR;

            period.mapLtoR[nL] = nR;
          }
        }
      }
    }

    // y-min to y-max periodicity
    if (periodic[1])
    {
      int groupL = iYmin;
      int groupR = iYmax;

      periodicity.emplace_back(groupL, groupR);
      PeriodicBCNodeMap& period = periodicity.back();

      for (int k = 0; k < kk+1; k++)
      {
        int jL = 0;
        int jR = jj;
        {
          for (int i = 0; i < ii+1; i++)
          {
            const int nL = k*koffset + jL*joffset + i;
            const int nR = k*koffset + jR*joffset + i;

            period.mapLtoR[nL] = nR;
          }
        }
      }
    }

    // z-min to z-max periodicity
    if (periodic[2])
    {
      int groupL = iZmin;
      int groupR = iZmax;

      periodicity.emplace_back(groupL, groupR);
      PeriodicBCNodeMap& period = periodicity.back();

      int kL = 0;
      int kR = kk;
      {
        for (int j = 0; j < jj+1; j++)
        {
          for (int i = 0; i < ii+1; i++)
          {
            const int nL = kL*koffset + j*joffset + i;
            const int nR = kR*koffset + j*joffset + i;

            period.mapLtoR[nL] = nR;
          }
        }
      }
    }
  }

  // Create the boundary periodicity
  xfldin.addBoundaryPeriodicity(periodicity);

  // Finalize the grid construction
  this->buildFrom( xfldin );
}

}
