// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_Box_Triangle_Ghost_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj (quad) elements
//
// area elements in 1 group: 2*ii*jj triangles
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
//    (Note: the implementation allows for 1 to 3 interior-trace groups depending on
//           whether horizontal and vertical traces exist)
// boundary-edge elements in 4 groups: lower, right, upper, left
// Ghost boundaries are upper and left

const int XField2D_Box_Triangle_Ghost_X1::iLeft   = 0;
const int XField2D_Box_Triangle_Ghost_X1::iBottom = 1;
const int XField2D_Box_Triangle_Ghost_X1::iRight  = 2;
const int XField2D_Box_Triangle_Ghost_X1::iTop    = 3;


/* --------------SouthEast to NorthWest diagonals-------------- */
void
XField2D_Box_Triangle_Ghost_X1::XField2D_Box_Triangle_Ghost_X1_SENW( int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
{
  int comm_rank = comm_->rank();

  int nnode = (ii + 1)*(jj + 1);
  int nelem = 2*ii*jj;

  // create the grid-coordinate DOF arrays
  resizeDOF( nnode );

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      DOF(j*(ii+1) + i)[0] = xmin + (xmax - xmin)*i/Real(ii);
      DOF(j*(ii+1) + i)[1] = ymin + (ymax - ymin)*j/Real(jj);
    }
  }

  // create the element groups
  int cnt = 1;          // interior: diagonal
  if (ii > 1) cnt++;    // interior: horizontal
  if (jj > 1) cnt++;    // interior: vertical
  resizeInteriorTraceGroups(cnt);

  resizeBoundaryTraceGroups(2);     // bottom, left
  resizeGhostBoundaryTraceGroups(2); // right, upper
  resizeCellGroups(1);

  // grid area field variable

  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nelem );

  // area element grid-coordinate DOF associativity (cell-to-node connectivity)

  ElementAssociativityConstructor<TopoD2, Triangle> xDOFAssocArea( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  int n1, n2, n3, n4;
  for (int j = 0; j < jj; j++)
  {
    for (int i = 0; i < ii; i++)
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  |\ |
      n3 = n2 + (ii+1);         //  | \|
      n4 = n1 + (ii+1);         // n1--n2

      // set the processor rank
      xDOFAssocArea.setRank( comm_rank );

      xDOFAssocArea.setNodeGlobalMapping( {n1, n2, n4} );

      // edge signs for area elements (L is +, R is -)
      // NOTE: assumes edge orientations set below in interior-edge field variables

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n3, n4, n2} );

      xDOFAssocArea.setEdgeSign( -1, 0 );
      xDOFAssocArea.setEdgeSign( +1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );

    }
  }

  // left boundary-edge: oriented down
  for (int j = 0; j < jj; j++)
  {
    int i = 0;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i ).setEdgeSign( +1, 1 );
  }

  // top boundary-edge: oriented left
  for (int i = 0; i < ii; i++)
  {
    int j = jj-1;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i + 1 ).setEdgeSign( +1, 2 );
  }

  FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
  cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

  xfldElementGroup->setDOF(DOF_, nDOF_);

  nElem_ = nelem;

  // grid interior-edge field variable
  FieldTraceGroupType<Line>* xfldIedge = NULL;
  int edge, nedge;
  int c1, c2;

  cnt = 0;

  // horizontal edges

  if (jj > 1)
  {
    nedge = (jj-1)*ii;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;
    for (int j = 1; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(2*ii) + 2*i;
        c2 = (j-1)*(2*ii) + 2*i + 1;

        // set the processor rank
        fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

        // grid-coordinate DOF association (edge-to-node connectivity)
        fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }
    }

    interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
    cnt++;

    xfldIedge->setDOF(DOF_, nDOF_);
  }

  // vertical edges

  if (ii > 1)
  {
    nedge = (ii-1)*jj;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;
    for (int i = 1; i < ii; i++)
    {
      for (int j = 0; j < jj; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        // set the processor rank
        fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

        // grid-coordinate DOF association (edge-to-node connectivity)
        fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }
    }

    interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
    cnt++;

    xfldIedge->setDOF(DOF_, nDOF_);
  }

  // diagonal edges

  nedge = ii*jj;
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType
    fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

  fldAssocIedge.setGroupLeft( 0 );
  fldAssocIedge.setGroupRight( 0 );

  edge = 0;
  for (int j = 0; j < jj; j++)
  {
    for (int i = 0; i < ii; i++)
    {
      n1 = j*(ii+1) + i + 1;
      n2 = n1 + (ii+1) - 1;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      // set the processor rank
      fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

      edge++;
    }
  }

  interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
  cnt++;

  xfldIedge->setDOF(DOF_, nDOF_);

  // grid boundary-edge field variables
  // Note: all edges oriented so domain is on its left

  FieldTraceGroupType<Line>* xfldBedge = NULL;
  FieldTraceGroupType<Line>* xfldGBedge = NULL;

  // lower boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, ii );

    fldAssocBedge.setGroupLeft( 0 );

    for (int i = 0; i < ii; i++)
    {
      n1 = i;
      n2 = i + 1;
      c1 = 2*i;
      edge = i;

      // set the processor rank
      fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), edge );
    }

    boundaryTraceGroups_[iBottom] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  // right boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocGBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocGBedge.setGroupLeft( 0 );

    // edge element grid-coordinate DOF associativity (edge-to-node connectivity)
    for (int j = 0; j < jj; j++)
    {
      n1 = j*(ii+1) + ii;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*ii - 1;
      edge = j;

      // set the processor rank
      fldAssocGBedge.setAssociativity( edge ).setRank( comm_rank );

      // edge-to-cell connectivity
      fldAssocGBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocGBedge.setElementLeft(  c1, edge );
      fldAssocGBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), edge );
    }

    ghostBoundaryTraceGroups_[iRight-this->nBoundaryTraceGroups()] = xfldGBedge = new FieldTraceGroupType<Line>( fldAssocGBedge );

    xfldGBedge->setDOF(DOF_, nDOF_);
  }

  // upper boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocGBedge( BasisFunctionLineBase::HierarchicalP1, ii );

    fldAssocGBedge.setGroupLeft( 0 );

    for (int i = ii-1; i >= 0; i--)
    {
      n1 = jj*(ii+1) + i + 1;
      n2 = jj*(ii+1) + i;
      c1 = (jj-1)*(2*ii) + 2*i + 1;
      edge = ii-1 - i;
      // printf( "upper: i = %d  n1 = %d  n2 = %d  c1 = %d\n", i, n1, n2, c1 );

      // set the processor rank
      fldAssocGBedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocGBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocGBedge.setElementLeft(  c1, edge );
      fldAssocGBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), edge );
    }

    ghostBoundaryTraceGroups_[iTop-this->nBoundaryTraceGroups()] = xfldGBedge = new FieldTraceGroupType<Line>( fldAssocGBedge );

    xfldGBedge->setDOF(DOF_, nDOF_);
  }

  // left boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocBedge.setGroupLeft( 0 );

    for (int j = jj-1; j >= 0; j--)
    {
      n1 = j*(ii+1) + (ii+1);
      n2 = j*(ii+1);
      c1 = j*(2*ii);
      edge = jj-1 - j;

      // set the processor rank
      fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), edge );
    }

    boundaryTraceGroups_[iLeft] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  //Check that the grid is correct
  checkGrid();
}


/* --------------SouthWest to NorthEast diagonals-------------- */
void
XField2D_Box_Triangle_Ghost_X1::XField2D_Box_Triangle_Ghost_X1_SWNE( int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
{
  int comm_rank = comm_->rank();

  int nnode = (ii + 1)*(jj + 1);
  int nelem = 2*ii*jj;

  // create the grid-coordinate DOF arrays
  resizeDOF( nnode );

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      DOF(j*(ii+1) + i)[0] = xmin + (xmax - xmin)*i/Real(ii);
      DOF(j*(ii+1) + i)[1] = ymin + (ymax - ymin)*j/Real(jj);
    }
  }

  // create the element groups
  int cnt = 1;          // interior: diagonal
  if (ii > 1) cnt++;    // interior: horizontal
  if (jj > 1) cnt++;    // interior: vertical
  resizeInteriorTraceGroups(cnt);

  resizeBoundaryTraceGroups(4);     // bottom, right, upper, left
  resizeCellGroups(1);

  // grid area field variable

  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nelem );

  // area element grid-coordinate DOF associativity (cell-to-node connectivity)

  ElementAssociativityConstructor<TopoD2, Triangle> xDOFAssocArea( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  int n1, n2, n3, n4;
  for (int j = 0; j < jj; j++)
  {
    for (int i = 0; i < ii; i++)
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  | /|
      n3 = n2 + (ii+1);         //  |/ |
      n4 = n1 + (ii+1);         // n1--n2

      // set the processor rank
      xDOFAssocArea.setRank( comm_rank );

      xDOFAssocArea.setNodeGlobalMapping( {n4, n1, n3} );

      // edge signs for area elements (L is +, R is -)
      // NOTE: assumes edge orientations set below in interior-edge field variables

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n1, n2, n3} );

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );

    }
  }

  // left boundary-edge: oriented down
  for (int j = 0; j < jj; j++)
  {
    int i = 0;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i ).setEdgeSign( +1, 2 );
  }

  // top boundary-edge: oriented left
  for (int i = 0; i < ii; i++)
  {
    int j = jj-1;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i ).setEdgeSign( +1, 1 );
  }

  FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
  cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

  xfldElementGroup->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // grid interior-edge field variable
  FieldTraceGroupType<Line>* xfldIedge = NULL;
  int edge, nedge;
  int c1, c2;

  cnt = 0;

  // horizontal edges

  if (jj > 1)
  {
    nedge = (jj-1)*ii;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;
    for (int j = 1; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(2*ii) + 2*i + 1;
        c2 = (j-1)*(2*ii) + 2*i;

        // set the processor rank
        fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

        // grid-coordinate DOF association (edge-to-node connectivity)
        fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }
    }

    interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
    cnt++;

    xfldIedge->setDOF(DOF_, nDOF_);
  }

  // vertical edges

  if (ii > 1)
  {
    nedge = (ii-1)*jj;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;
    for (int i = 1; i < ii; i++)
    {
      for (int j = 0; j < jj; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        // set the processor rank
        fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

        // grid-coordinate DOF association (edge-to-node connectivity)
        fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }
    }

    interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
    cnt++;

    xfldIedge->setDOF(DOF_, nDOF_);
  }

  // diagonal edges

  nedge = ii*jj;
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType
    fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

  fldAssocIedge.setGroupLeft( 0 );
  fldAssocIedge.setGroupRight( 0 );

  edge = 0;
  for (int j = 0; j < jj; j++)
  {
    for (int i = 0; i < ii; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1) + 1;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      // set the processor rank
      fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

      edge++;
    }
  }

  interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
  cnt++;

  xfldIedge->setDOF(DOF_, nDOF_);

  // grid boundary-edge field variables
  // Note: all edges oriented so domain is on its left

  FieldTraceGroupType<Line>* xfldBedge = NULL;
  FieldTraceGroupType<Line>* xfldGBedge = NULL;

  // lower boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, ii );

    fldAssocBedge.setGroupLeft( 0 );

    for (int i = 0; i < ii; i++)
    {
      n1 = i;
      n2 = i + 1;
      c1 = 2*i + 1;
      edge = i;

      // set the processor rank
      fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), edge );
    }

    boundaryTraceGroups_[iBottom] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  // right boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocBedge.setGroupLeft( 0 );

    // edge element grid-coordinate DOF associativity (edge-to-node connectivity)
    for (int j = 0; j < jj; j++)
    {
      n1 = j*(ii+1) + ii;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*ii - 1;
      edge = j;

      // set the processor rank
      fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

      // edge-to-cell connectivity
      fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), edge );
    }

    boundaryTraceGroups_[iRight] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  // upper boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocGBedge( BasisFunctionLineBase::HierarchicalP1, ii );

    fldAssocGBedge.setGroupLeft( 0 );

    for (int i = ii-1; i >= 0; i--)
    {
      n1 = jj*(ii+1) + i + 1;
      n2 = jj*(ii+1) + i;
      c1 = (jj-1)*(2*ii) + 2*i;
      edge = ii-1 - i;
      // printf( "upper: i = %d  n1 = %d  n2 = %d  c1 = %d\n", i, n1, n2, c1 );

      // set the processor rank
      fldAssocGBedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocGBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocGBedge.setElementLeft(  c1, edge );
      fldAssocGBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), edge );
    }

    ghostBoundaryTraceGroups_[iTop-this->nBoundaryTraceGroups()] = xfldGBedge = new FieldTraceGroupType<Line>( fldAssocGBedge );

    xfldGBedge->setDOF(DOF_, nDOF_);
  }

  // left boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocGBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocGBedge.setGroupLeft( 0 );

    for (int j = jj-1; j >= 0; j--)
    {
      n1 = j*(ii+1) + (ii+1);
      n2 = j*(ii+1);
      c1 = j*(2*ii);
      edge = jj-1 - j;

      // set the processor rank
      fldAssocGBedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocGBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocGBedge.setElementLeft(  c1, edge );
      fldAssocGBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), edge );
    }

    ghostBoundaryTraceGroups_[iLeft-this->nBoundaryTraceGroups()] = xfldGBedge = new FieldTraceGroupType<Line>( fldAssocGBedge );

    xfldGBedge->setDOF(DOF_, nDOF_);
  }

  //Check that the grid is correct
  checkGrid();
}

}
