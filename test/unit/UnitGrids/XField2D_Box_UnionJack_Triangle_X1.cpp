// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_Box_UnionJack_Triangle_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj (quad) elements
//
// diagonals oriented as in Union Jack flag
//
// area elements in 1 group: 2*ii*jj triangles
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 4 groups: lower, right, upper, left

const int XField2D_Box_UnionJack_Triangle_X1::iBottom = 0;
const int XField2D_Box_UnionJack_Triangle_X1::iRight  = 1;
const int XField2D_Box_UnionJack_Triangle_X1::iTop    = 2;
const int XField2D_Box_UnionJack_Triangle_X1::iLeft   = 3;

XField2D_Box_UnionJack_Triangle_X1::
XField2D_Box_UnionJack_Triangle_X1( int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
{

  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = xmin + (xmax - xmin)*i/Real(ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = ymin + (ymax - ymin)*j/Real(jj);

  generateGrid( xvec, yvec );
}


//---------------------------------------------------------------------------//
XField2D_Box_UnionJack_Triangle_X1::
XField2D_Box_UnionJack_Triangle_X1( const std::vector<Real>& xvec,
                                    const std::vector<Real>& yvec )
{
 std::size_t ii = xvec.size() - 1;
 std::size_t jj = yvec.size() - 1;

 //Check ascending order
 for (std::size_t i = 0; i < ii; i++)
   SANS_ASSERT( xvec[i] < xvec[i+1] );

 for (std::size_t j = 0; j < jj; j++)
   SANS_ASSERT( yvec[j] < yvec[j+1] );

 generateGrid( xvec, yvec );
}

//---------------------------------------------------------------------------//
void
XField2D_Box_UnionJack_Triangle_X1::
generateGrid( const std::vector<Real>& xvec,
              const std::vector<Real>& yvec )
{
  int ii = (int) xvec.size() - 1;
  int jj = (int) yvec.size() - 1;

  SANS_ASSERT_MSG( (ii%2 == 0) && (jj%2 == 0), "coded for even number of cells in I and J" );

  int comm_rank = comm_->rank();

  int nnode = (ii + 1)*(jj + 1);
  int nelem = 2*ii*jj;
  int i, j;

  // create the grid-coordinate DOF arrays
  resizeDOF( nnode );

  for (i = 0; i < ii+1; i++)
  {
    for (j = 0; j < jj+1; j++)
    {
      DOF(j*(ii+1) + i)[0] = xvec[i];
      DOF(j*(ii+1) + i)[1] = yvec[j];
    }
  }

  // create the element groups
  int cnt = 1;          // interior: diagonal
  if (ii > 1) cnt++;    // interior: horizontal
  if (jj > 1) cnt++;    // interior: vertical
  resizeInteriorTraceGroups(cnt);

  resizeBoundaryTraceGroups(4);     // bottom, right, upper, left
  resizeCellGroups(1);

  // grid area field variable

  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nelem );

  // area element grid-coordinate DOF associativity (cell-to-node connectivity)
  // edge signs for area elements (L is +, R is -)
  // NOTE: assumes edge orientations set below in interior & boundary-edge field variables

  ElementAssociativityConstructor<TopoD2, Triangle> xDOFAssocArea( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  int n1, n2, n3, n4;

  xDOFAssocArea.setRank( comm_rank );

  // lower, left
  for (j = 0; j < jj/2; j++)
  {
    for (i = 0; i < ii/2; i++)
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  | /|
      n3 = n2 + (ii+1);         //  |/ |
      n4 = n1 + (ii+1);         // n1--n2

      xDOFAssocArea.setNodeGlobalMapping( {n4, n1, n3} );

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n2, n3, n1} );

      xDOFAssocArea.setEdgeSign( -1, 0 );
      xDOFAssocArea.setEdgeSign( +1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );

#if 0
      std::cout << "XField2D_Box_Triangle_X1: i = " << i << "  j = " << j
        << "  {n1,n2,n3,n4} = " << n1 << " " << n2 << " " << n3 << " " << n4 << std::endl;
#endif
    }
  }

  // lower, right
  for (j = 0; j < jj/2; j++)
  {
    for (i = ii/2; i < ii; i++)
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  |\ |
      n3 = n2 + (ii+1);         //  | \|
      n4 = n1 + (ii+1);         // n1--n2

      xDOFAssocArea.setNodeGlobalMapping( {n1, n2, n4} );

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n3, n4, n2} );

      xDOFAssocArea.setEdgeSign( -1, 0 );
      xDOFAssocArea.setEdgeSign( +1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );
#if 0
      std::cout << "XField2D_Box_Triangle_X1: i = " << i << "  j = " << j
        << "  {n1,n2,n3,n4} = " << n1 << " " << n2 << " " << n3 << " " << n4 << std::endl;
#endif
    }
  }

  // upper, left
  for (j = jj/2; j < jj; j++)
  {
    for (i = 0; i < ii/2; i++)
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  |\ |
      n3 = n2 + (ii+1);         //  | \|
      n4 = n1 + (ii+1);         // n1--n2

      xDOFAssocArea.setNodeGlobalMapping( {n1, n2, n4} );

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n3, n4, n2} );

      xDOFAssocArea.setEdgeSign( -1, 0 );
      xDOFAssocArea.setEdgeSign( +1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );
#if 0
      std::cout << "XField2D_Box_Triangle_X1: i = " << i << "  j = " << j
        << "  {n1,n2,n3,n4} = " << n1 << " " << n2 << " " << n3 << " " << n4 << std::endl;
#endif
    }
  }

  // upper, right
  for (j = jj/2; j < jj; j++)
  {
    for (i = ii/2; i < ii; i++)
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  | /|
      n3 = n2 + (ii+1);         //  |/ |
      n4 = n1 + (ii+1);         // n1--n2

      xDOFAssocArea.setNodeGlobalMapping( {n4, n1, n3} );

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n2, n3, n1} );

      xDOFAssocArea.setEdgeSign( -1, 0 );
      xDOFAssocArea.setEdgeSign( +1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );
#if 0
      std::cout << "XField2D_Box_Triangle_X1: i = " << i << "  j = " << j
        << "  {n1,n2,n3,n4} = " << n1 << " " << n2 << " " << n3 << " " << n4 << std::endl;
#endif
    }
  }

#if 0
  std::cout << "XField2D_Box_Triangle_X1: dumping xfldArea" << std::endl;  fldAssocCell.dump(2);
#endif

  // left boundary-edge: oriented down
  for (int j = 0; j < jj/2; j++)
  {
    i = 0;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i ).setEdgeSign( +1, 2 );
  }
  for (j = jj/2; j < jj; j++)
  {
    i = 0;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i ).setEdgeSign( +1, 1 );
  }

  // top boundary-edge: oriented left
  for (int i = 0; i < ii/2; i++)
  {
    j = jj-1;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i + 1 ).setEdgeSign( +1, 2 );
  }
  for (int i = ii/2; i < ii; i++)
  {
    j = jj-1;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i ).setEdgeSign( +1, 1 );
  }


  FieldCellGroupType<Triangle>* xfldArea = NULL;
  cellGroups_[0] = xfldArea = new FieldCellGroupType<Triangle>( fldAssocCell );

  xfldArea->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // grid interior-edge field variable

  FieldTraceGroupType<Line>* xfldIedge = NULL;
  ElementAssociativityConstructor<TopoD1, Line> xDOFAssocEdge( BasisFunctionLineBase::HierarchicalP1 );
  int edge, nedge;
  int c1, c2;

  cnt = 0;

  xDOFAssocEdge.setRank( comm_rank );

  // horizontal edges

  if (jj > 1)
  {
    nedge = (jj-1)*ii;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;

    // lower, left
    for (j = 1; j < jj/2; j++)
    {
      for (i = 0; i < ii/2; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(2*ii) + 2*i + 1;
        c2 = (j-1)*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }
    }

    // lower, right
    for (j = 1; j < jj/2; j++)
    {
      for (i = ii/2; i < ii; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(2*ii) + 2*i;
        c2 = (j-1)*(2*ii) + 2*i + 1;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }
    }

    // upper, left
    for (j = jj/2 + 1; j < jj; j++)
    {
      for (i = 0; i < ii/2; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(2*ii) + 2*i;
        c2 = (j-1)*(2*ii) + 2*i + 1;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }
    }

    // upper, right
    for (j = jj/2 + 1; j < jj; j++)
    {
      for (i = ii/2; i < ii; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(2*ii) + 2*i + 1;
        c2 = (j-1)*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }
    }

    // bisectors
    j = jj/2;
    for (i = 0; i < ii/2; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + 1;
      c1 = (j  )*(2*ii) + 2*i;
      c2 = (j-1)*(2*ii) + 2*i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

      edge++;
    }
    for (i = ii/2; i < ii; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + 1;
      c1 = (j  )*(2*ii) + 2*i + 1;
      c2 = (j-1)*(2*ii) + 2*i + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

      edge++;
    }

    interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
    cnt++;

    xfldIedge->setDOF(DOF_, nDOF_);
  }

  // vertical edges

  if (ii > 1)
  {
    nedge = (ii-1)*jj;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;

    // lower, left
    for (i = 1; i < ii/2; i++)
    {
      for (j = 0; j < jj/2; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }
    }

    // lower, right
    for (i = ii/2 + 1; i < ii; i++)
    {
      for (j = 0; j < jj/2; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }
    }

    // upper, left
    for (i = 1; i < ii/2; i++)
    {
      for (j = jj/2; j < jj; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }
    }

    // upper, right
    for (i = ii/2 + 1; i < ii; i++)
    {
      for (j = jj/2; j < jj; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }
    }

    // bisectors
    i = ii/2;
    for (j = 0; j < jj/2; j++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*i - 1;
      c2 = j*(2*ii) + 2*i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

      edge++;
    }
    for (j = jj/2; j < jj; j++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*i - 1;
      c2 = j*(2*ii) + 2*i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

      edge++;
    }

    interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
    cnt++;

    xfldIedge->setDOF(DOF_, nDOF_);
  }

  // diagonal edges

  nedge = ii*jj;
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType
    fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

  fldAssocIedge.setGroupLeft( 0 );
  fldAssocIedge.setGroupRight( 0 );

  edge = 0;

  // lower, left
  for (j = 0; j < jj/2; j++)
  {
    for (i = 0; i < ii/2; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1) + 1;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

      edge++;
    }
  }

  // lower, right
  for (j = 0; j < jj/2; j++)
  {
    for (i = ii/2; i < ii; i++)
    {
      n1 = j*(ii+1) + i + 1;
      n2 = n1 + (ii+1) - 1;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

      edge++;
    }
  }

  // upper, left
  for (j = jj/2; j < jj; j++)
  {
    for (i = 0; i < ii/2; i++)
    {
      n1 = j*(ii+1) + i + 1;
      n2 = n1 + (ii+1) - 1;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

      edge++;
    }
  }

  // upper, right
  for (j = jj/2; j < jj; j++)
  {
    for (i = ii/2; i < ii; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1) + 1;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

      edge++;
    }
  }

  interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
  cnt++;

  xfldIedge->setDOF(DOF_, nDOF_);

  // grid boundary-edge field variables
  // Note: all edges oriented so domain is on its left

  FieldTraceGroupType<Line>* xfldBedge = NULL;

  // lower boundary
  {
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, ii );

    fldAssocBedge.setGroupLeft( 0 );

    for (i = 0; i < ii/2; i++)
    {
      n1 = i;
      n2 = i + 1;
      c1 = 2*i + 1;
      edge = i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
    }

    for (i = ii/2; i < ii; i++)
    {
      n1 = i;
      n2 = i + 1;
      c1 = 2*i;
      edge = i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
    }

    boundaryTraceGroups_[iBottom] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  // right boundary
  {
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocBedge.setGroupLeft( 0 );

    for (j = 0; j < jj/2; j++)
    {
      n1 = j*(ii+1) + ii;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*ii - 1;
      edge = j;

      // edge-to-cell connectivity
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
    }

    for (j = jj/2; j < jj; j++)
    {
      n1 = j*(ii+1) + ii;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*ii - 1;
      edge = j;

      // edge-to-cell connectivity
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
    }

    boundaryTraceGroups_[iRight] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  // upper boundary
  {
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, ii );

    fldAssocBedge.setGroupLeft( 0 );

    for (i = ii-1; i >= ii/2; i--)
    {
      n1 = jj*(ii+1) + i + 1;
      n2 = jj*(ii+1) + i;
      c1 = (jj-1)*(2*ii) + 2*i;
      edge = ii-1 - i;
      // printf( "upper: i = %d  n1 = %d  n2 = %d  c1 = %d\n", i, n1, n2, c1 );

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
    }

    for (i = ii/2 - 1; i >= 0; i--)
    {
      n1 = jj*(ii+1) + i + 1;
      n2 = jj*(ii+1) + i;
      c1 = (jj-1)*(2*ii) + 2*i + 1;
      edge = ii-1 - i;
      // printf( "upper: i = %d  n1 = %d  n2 = %d  c1 = %d\n", i, n1, n2, c1 );

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
    }

    boundaryTraceGroups_[iTop] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  // left boundary
  {
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocBedge.setGroupLeft( 0 );

    for (j = jj-1; j >= jj/2; j--)
    {
      n1 = j*(ii+1) + (ii+1);
      n2 = j*(ii+1);
      c1 = j*(2*ii);
      edge = jj-1 - j;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
    }

    for (j = jj/2 - 1; j >= 0; j--)
    {
      n1 = j*(ii+1) + (ii+1);
      n2 = j*(ii+1);
      c1 = j*(2*ii);
      edge = jj-1 - j;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
    }

    boundaryTraceGroups_[iLeft] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  //Check that the grid is correct
  checkGrid();

#if 0
  output_Tecplot( *xfldArea, "tmp/grid.plt" );
#endif
}

}
