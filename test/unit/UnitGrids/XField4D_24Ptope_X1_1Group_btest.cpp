// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField4D_24Ptope_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField4D_24Ptope_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField4D_24Ptope_X1_1Group_test )
{

  const int nDim= 4;
  const int nPentatope= 24;
  const int nVertex= 16;
  const int nBoundaries= 8;
  const int nTraceTet= 6;
  const int nInteriorTet= 36;

  // these were the vertices that were
  // dumped out of XField_KuhnFreudenthal
  const int DOFarray[nVertex][nDim]=
      {{0, 0, 0, 0}, {0, 0, 0, 1}, {0, 0, 1, 0}, {0, 0, 1, 1},
       {0, 1, 0, 0}, {0, 1, 0, 1}, {0, 1, 1, 0}, {0, 1, 1, 1},
       {1, 0, 0, 0}, {1, 0, 0, 1}, {1, 0, 1, 0}, {1, 0, 1, 1},
       {1, 1, 0, 0}, {1, 1, 0, 1}, {1, 1, 1, 0}, {1, 1, 1, 1}};

  // these were the vertices that were
  // dumped out of XField_KuhnFreudenthal
  const int simplexVertices[nPentatope][nDim + 1]=
      {{0, 8, 10, 15, 14}, {0, 8, 10, 11, 15}, {0, 8, 9, 13, 15},
       {0, 8, 9, 15, 11}, {0, 2, 10, 14, 15}, {0, 2, 10, 15, 11},
       {0, 8, 12, 14, 15}, {0, 8, 12, 15, 13}, {0, 4, 12, 15, 14},
       {0, 4, 12, 13, 15}, {0, 4, 6, 14, 15}, {0, 4, 6, 15, 7},
       {0, 4, 5, 15, 13}, {0, 4, 5, 7, 15}, {0, 2, 6, 15, 14},
       {0, 2, 6, 7, 15}, {0, 2, 3, 11, 15}, {0, 2, 3, 15, 7},
       {0, 1, 9, 15, 13}, {0, 1, 9, 11, 15}, {0, 1, 5, 13, 15},
       {0, 1, 5, 15, 7}, {0, 1, 3, 15, 11}, {0, 1, 3, 7, 15}};

  // these were the boundary trace groups that were
  // dumped out of XField_KuhnFreudenthal
  const int boundaryTraceGroups[nBoundaries][nTraceTet][nDim]=
      {{{0, 4, 6, 7}, {0, 4, 7, 5}, {0, 2, 7, 6}, {0, 2, 3, 7}, {0, 1, 5, 7}, {0, 1, 7, 3}},
       {{8, 10, 14, 15}, {8, 10, 15, 11}, {8, 9, 15, 13}, {8, 9, 11, 15}, {8, 12, 15, 14}, {8, 12, 13, 15}},
       {{0, 8, 11, 10}, {0, 8, 9, 11}, {0, 2, 10, 11}, {0, 2, 11, 3}, {0, 1, 11, 9}, {0, 1, 3, 11}},
       {{4, 12, 14, 15}, {4, 12, 15, 13}, {4, 6, 15, 14}, {4, 6, 7, 15}, {4, 5, 13, 15}, {4, 5, 15, 7}},
       {{0, 8, 13, 9}, {0, 8, 12, 13}, {0, 4, 13, 12}, {0, 4, 5, 13}, {0, 1, 9, 13}, {0, 1, 13, 5}},
       {{2, 10, 15, 14}, {2, 10, 11, 15}, {2, 6, 14, 15}, {2, 6, 15, 7}, {2, 3, 15, 11}, {2, 3, 7, 15}},
       {{0, 8, 10, 14}, {0, 2, 14, 10}, {0, 8, 14, 12}, {0, 4, 12, 14}, {0, 4, 14, 6}, {0, 2, 6, 14}},
       {{1, 9, 13, 15}, {1, 9, 15, 11}, {1, 5, 15, 13}, {1, 5, 7, 15}, {1, 3, 11, 15}, {1, 3, 15, 7}}};

  // these were the interior trace groups that were
  // dumped out of XField_KuhnFreudenthal
  const int interiorTraceGroups[nInteriorTet][nDim]=
      {{0, 8, 10, 15}, {0, 8, 11, 15}, {0, 8, 15, 9}, {0, 10, 14, 15},
       {0, 10, 15, 11}, {0, 2, 15, 10}, {0, 8, 15, 14}, {0, 8, 13, 15},
       {0, 8, 15, 12}, {0, 12, 15, 14}, {0, 12, 13, 15}, {0, 4, 12, 15},
       {0, 4, 15, 14}, {0, 4, 15, 6}, {0, 4, 13, 15}, {0, 4, 15, 7},
       {0, 4, 5, 15}, {0, 6, 15, 14}, {0, 2, 14, 15}, {0, 6, 7, 15},
       {0, 2, 6, 15}, {0, 2, 15, 11}, {0, 2, 7, 15}, {0, 2, 15, 3},
       {0, 9, 15, 13}, {0, 9, 11, 15}, {0, 1, 9, 15}, {0, 5, 13, 15},
       {0, 1, 15, 13}, {0, 5, 15, 7}, {0, 1, 15, 5}, {0, 3, 15, 11},
       {0, 1, 11, 15}, {0, 3, 7, 15}, {0, 1, 15, 7}, {0, 1, 3, 15}};

  // these are the left and right boundaryelements that were
  // dumped out of XField_KuhnFreudenthal
  const int boundaryElemL[nBoundaries][nTraceTet]= {{11, 13, 15, 17, 21, 23}, {0, 1, 2, 3, 6, 7},
                                                    {1, 3, 5, 16, 19, 22}, {8, 9, 10, 11, 12, 13},
                                                    {2, 7, 9, 12, 18, 20}, {4, 5, 14, 15, 16, 17},
                                                    {0, 4, 6, 8, 10, 14}, {18, 19, 20, 21, 22, 23}};
  const int boundaryCanonicalL[nBoundaries][nTraceTet][2]= {{{3, 1}, {4, 1}, {4, 1}, {3, 1}, {3, 1}, {4, 1}},
                                                            {{0, 1}, {0, 1}, {0, 1}, {0, 1}, {0, 1}, {0, 1}},
                                                            {{4, 1}, {3, 1}, {3, 1}, {4, 1}, {4, 1}, {3, 1}},
                                                            {{0, 1}, {0, 1}, {0, 1}, {0, 1}, {0, 1}, {0, 1}},
                                                            {{4, 1}, {3, 1}, {4, 1}, {3, 1}, {3, 1}, {4, 1}},
                                                            {{0, 1}, {0, 1}, {0, 1}, {0, 1}, {0, 1}, {0, 1}},
                                                            {{3, 1}, {4, 1}, {4, 1}, {3, 1}, {4, 1}, {3, 1}},
                                                            {{0, 1}, {0, 1}, {0, 1}, {0, 1}, {0, 1}, {0, 1}}};

  // these are the left and right interior elements that were
  // dumped out of XField_KuhnFreudenthal
  const int interiorElemL[nInteriorTet]= {1, 3, 3, 4, 5, 5, 6, 7, 7, 8, 9, 9, 10, 11, 12, 13, 13, 14, 14, 15,
                                          15, 16, 17, 17, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 23};
  const int interiorElemR[nInteriorTet]= {0, 1, 2, 0, 1, 4, 0, 2, 6, 6, 7, 8, 8, 10, 9, 11, 12, 10, 4, 11, 14,
                                          5, 15, 16, 2, 3, 18, 12, 18, 13, 20, 16, 19, 17, 21, 22};
  const int interiorCanonicalL[nInteriorTet][2]= {{3, 1}, {2, 1}, {4, 1}, {1, 1}, {1, 1}, {4, 1},
                                                  {2, 1}, {2, 1}, {4, 1}, {1, 1}, {1, 1}, {3, 1},
                                                  {2, 1}, {4, 1}, {2, 1}, {2, 1}, {3, 1}, {1, 1},
                                                  {2, 1}, {1, 1}, {3, 1}, {2, 1}, {2, 1}, {4, 1},
                                                  {1, 1}, {1, 1}, {3, 1}, {1, 1}, {2, 1}, {1, 1},
                                                  {4, 1}, {1, 1}, {2, 1}, {1, 1}, {2, 1}, {3, 1}};
  const int interiorCanonicalR[nInteriorTet][2]= {{4, -1}, {2, -1}, {3, -1}, {1, -1}, {1, -1}, {3, -1},
                                                  {2, -1}, {2, -1}, {3, -1}, {1, -1}, {1, -1}, {4, -1},
                                                  {2, -1}, {3, -1}, {2, -1}, {2, -1}, {4, -1}, {1, -1},
                                                  {2, -1}, {1, -1}, {4, -1}, {2, -1}, {2, -1}, {3, -1},
                                                  {1, -1}, {1, -1}, {4, -1}, {1, -1}, {2, -1}, {1, -1},
                                                  {3, -1}, {1, -1}, {2, -1}, {1, -1}, {2, -1}, {4, -1}};

  XField4D_24Ptope_X1_1Group xfld;

  BOOST_CHECK_EQUAL(xfld.nDOF(), nVertex);

  // check the node values
  for (int i= 0; i < nVertex; i++)
  {
    for (int j= 0; j < nDim; j++)
    {
      BOOST_CHECK_EQUAL(xfld.DOF(i)[j], DOFarray[i][j]);
    }
  }

  // spacetime field variable
  BOOST_CHECK_EQUAL(xfld.nCellGroups(), 1);
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  const XField4D_24Ptope_X1_1Group::FieldCellGroupType<Pentatope> &xfldVol= xfld.getCellGroup<Pentatope>(0);

  int nodeMap[5];
  int elemL;
  int elemR;
  int faceL;
  int faceR;

  for (int i= 0; i < nPentatope; i++)
  {
    xfldVol.associativity(i).getNodeGlobalMapping(nodeMap, 5);

    for (int j= 0; j < 5; j++)
    {
      BOOST_CHECK_EQUAL(nodeMap[j], simplexVertices[i][j]);
    }
  }

  // interior-face field variables

  BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 1);
  BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Tet));

  const XField4D_24Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldIface= xfld.getInteriorTraceGroup<Tet>(0);

  BOOST_CHECK_EQUAL(xfldIface.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldIface.getGroupRight(), 0);

  // interior face-to-cell connectivity

  for (int faceI= 0; faceI < nInteriorTet; faceI++)
  {

    elemL= interiorElemL[faceI];
    elemR= interiorElemR[faceI];
    faceL= interiorCanonicalL[faceI][0];
    faceR= interiorCanonicalR[faceI][0];

    xfldIface.associativity(faceI).getNodeGlobalMapping(nodeMap, 4);

    // HERE, LOOP OVER NODES IN xfldIface, validate that they match the NODES IN ELEML's CORRECT FACE!!!
    for (int i= 0; i < nDim; i++)
    {
      BOOST_CHECK_EQUAL(nodeMap[i], interiorTraceGroups[faceI][i]);
      int dummy= simplexVertices[elemL][TraceToCellRefCoord<Tet, TopoD4, Pentatope>::TraceNodes[faceL][i]];
      BOOST_CHECK_EQUAL(nodeMap[i], dummy);
    }

    BOOST_CHECK_EQUAL(xfldIface.getElementLeft(faceI), elemL);
    BOOST_CHECK_EQUAL(xfldIface.getElementRight(faceI), elemR);
    BOOST_CHECK_EQUAL(xfldIface.getCanonicalTraceLeft(faceI).trace, faceL);
    BOOST_CHECK_EQUAL(xfldIface.getCanonicalTraceRight(faceI).trace, faceR);

    BOOST_CHECK_EQUAL(xfldVol.associativity(elemL).faceSign()[faceL], +1);
    BOOST_CHECK_EQUAL(xfldVol.associativity(elemR).faceSign()[faceR], xfldIface.getCanonicalTraceRight(faceI).orientation);

  }

  // boundary face field variables

  BOOST_CHECK_EQUAL(xfld.nBoundaryTraceGroups(), 1);
  BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Tet));

  const XField4D_24Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBface= xfld.getBoundaryTraceGroup<Tet>(0);

  BOOST_CHECK_EQUAL(xfldBface.getGroupLeft(), 0);

  for (int boundary = 0; boundary < nBoundaries; boundary++)
  {
    for (int boundaryTet= 0; boundaryTet < nTraceTet; boundaryTet++)
    {

      int faceB= nTraceTet*boundary + boundaryTet;

      elemL= boundaryElemL[boundary][boundaryTet];
      elemR= -1;
      faceL= boundaryCanonicalL[boundary][boundaryTet][0];
      faceR= -1;

      xfldBface.associativity(faceB).getNodeGlobalMapping(nodeMap, 4);

      for (int i= 0; i < nDim; i++)
      {
        BOOST_CHECK_EQUAL(nodeMap[i], boundaryTraceGroups[boundary][boundaryTet][i]);
        int dummy= simplexVertices[elemL][TraceToCellRefCoord<Tet, TopoD4, Pentatope>::TraceNodes[faceL][i]];
        BOOST_CHECK_EQUAL(nodeMap[i], dummy);
      }

      BOOST_CHECK_EQUAL(xfldBface.getElementLeft(faceB), elemL);
      BOOST_CHECK_EQUAL(xfldBface.getElementRight(faceB), elemR);
      BOOST_CHECK_EQUAL(xfldBface.getCanonicalTraceLeft(faceB).trace, faceL);
      BOOST_CHECK_EQUAL(xfldBface.getCanonicalTraceRight(faceB).trace, faceR);

      BOOST_CHECK_EQUAL(xfldVol.associativity(elemL).faceSign()[faceL], +1);

    }
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
