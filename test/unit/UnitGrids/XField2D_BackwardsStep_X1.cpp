// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_BackwardsStep_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/Partition/XField_Lagrange.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

const int XField2D_BackwardsStep_X1::iBottomInlet  = 0;
const int XField2D_BackwardsStep_X1::iStep         = 1;
const int XField2D_BackwardsStep_X1::iBottomOutlet = 2;
const int XField2D_BackwardsStep_X1::iOutlet       = 3;
const int XField2D_BackwardsStep_X1::iTop          = 4;
const int XField2D_BackwardsStep_X1::iInlet        = 5;

//---------------------------------------------------------------------------//
// takes
void
XField2D_BackwardsStep_X1::
generateGrid( mpi::communicator& comm,
              const std::vector<Real>& xvec_L, const std::vector<Real>& xvec_R,
              const std::vector<Real>& yvec_U, const std::vector<Real>& yvec_D )
{
  XField_Lagrange<PhysD2>::VectorX X;

  XField_Lagrange<PhysD2> xfldin(comm);

  int nnode=0, nelem=0, ii, jj;

  // Top Right Box
  ii = (int) xvec_R.size() - 1;
  jj = (int) yvec_U.size() - 1;

  SANS_ASSERT_MSG( (ii%2 == 0) && (jj%2 == 0), "coded for even number of cells in I and J, Top Right" );

  nnode += (ii + 1)*(jj + 1);
  nelem += 2*ii*jj;

  // Top Left box
  ii = (int) xvec_L.size() - 1;
  jj = (int) yvec_U.size() - 1;

  SANS_ASSERT_MSG( (ii%2 == 0), "coded for even number of cells in I, Top Left" );

  nnode += (ii)*(jj + 1); // no ii + 1 because the right hand dofs are used in the top right box
  nelem += 2*ii*jj;

  // Bottom Right Box
  ii = (int) xvec_R.size() - 1;
  jj = (int) yvec_D.size() - 1;

  SANS_ASSERT_MSG( (jj%2 == 0), "coded for even number of cells in J, Bottom Right" );

  nnode += (ii + 1)*jj; // no jj + 1 because the top dofs are used in the top right box
  nelem += 2*ii*jj;

  // resizeDOF( nnode );
  xfldin.sizeDOF( nnode );

  const int iiR = (int) xvec_R.size() - 1;
  const int iiL = (int) xvec_L.size() - 1;
  const int jjU = (int) yvec_U.size() - 1;
  const int jjD = (int) yvec_D.size() - 1;

  if (comm.rank() == 0)
  {
    // int offset = 0;
    // Top right box
    ii = (int) xvec_R.size() - 1;
    jj = (int) yvec_U.size() - 1;

    for (int j = 0; j < jj+1; j++)
    {
      for (int i = 0; i < ii+1; i++)
      {
        X[0] = xvec_R[i];
        X[1] = yvec_U[j];

        xfldin.addDOF(X);
      }
    }

    // offset += (ii + 1)*(jj + 1);

    // Top Left box
    ii = (int) xvec_L.size() - 1;
    jj = (int) yvec_U.size() - 1;

    for (int j = 0; j < jj+1; j++)
    {
      for (int i = 0; i < ii; i++) // only loop up to ii, the ii+1 dofs are assigned in top right box
      {
        X[0] = xvec_L[i];
        X[1] = yvec_U[j];

        xfldin.addDOF(X);
      }
    }

    // offset += (ii)*(jj + 1); // only offset up to ii, the ii+1 dofs are assigned in top right box

    // Bottom Right box
    jj = (int) yvec_D.size() - 1;
    ii = (int) xvec_R.size() - 1;

    for (int j = 0; j < jj; j++) // only loop up to jj, the jj+1 dofs are assigned in top right box
    {
      for (int i = 0; i < ii+1; i++)
      {
        X[0] = xvec_R[i];
        X[1] = yvec_D[j];

        xfldin.addDOF(X);
      }
    }
  }

  // DOFs have now been added, now add cells

  xfldin.sizeCells(nelem);

  // area element grid-coordinate DOF associativity (cell-to-node connectivity)
  int cellGroup = 0, order = 1;
  int n1, n2, n3, n4;

  if (comm.rank() == 0)
  {
    // Top Right Box
    int offset = 0;
    ii = (int) xvec_R.size() - 1;
    jj = (int) yvec_U.size() - 1;

    for (int j = 0; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        n1 = offset + j*(ii+1) + i;   // n4--n3
        n2 =     n1 + 1;              //  |\ |
        n3 =     n2 + (ii+1);         //  | \|
        n4 =     n1 + (ii+1);         // n1--n2

        xfldin.addCell(cellGroup, eTriangle, order, {n1, n2, n4});
        xfldin.addCell(cellGroup, eTriangle, order, {n3, n4, n2});
      }
    }

    offset += (ii + 1)*(jj + 1);

    // Top Left Box
    ii = (int) xvec_L.size() - 1;
    jj = (int) yvec_U.size() - 1;
    for (int j = 0; j < jj; j++)
    {
      for (int i = 0; i < ii-1; i++) // Not connecting the right column
      {
        // only + (ii) because the row is shortened by 1 dof
        n1 = offset + j*(ii) + i;   // n4--n3
        n2 =     n1 + 1;            //  |\ |
        n3 =     n2 + (ii);         //  | \|
        n4 =     n1 + (ii);         // n1--n2

        xfldin.addCell(cellGroup, eTriangle, order, {n1, n2, n4});
        xfldin.addCell(cellGroup, eTriangle, order, {n3, n4, n2});
      }
    }

    offset += (ii)*(jj + 1); // only offset up to ii, the ii+1 dofs are assigned in top right box

    // Bottom Right Box
    jj = (int) yvec_D.size() - 1;
    ii = (int) xvec_R.size() - 1;
    for (int j = 0; j < jj-1; j++) // Not connecting the top row
    {
      for (int i = 0; i < ii; i++)
      {
        n1 = offset + j*(ii+1) + i;   // n4--n3
        n2 =     n1 + 1;              //  |\ |
        n3 =     n2 + (ii+1);         //  | \|
        n4 =     n1 + (ii+1);         // n1--n2

        xfldin.addCell(cellGroup, eTriangle, order, {n1, n2, n4});
        xfldin.addCell(cellGroup, eTriangle, order, {n3, n4, n2});
      }
    }

    // Add in the connecting columns

    offset = (iiR + 1)*(jjU + 1);

    // Right Column of Top Left Box
    for (int j = 0; j < jjU; j++)
    {
      int i = iiL - 1; // end of the row

      n1 = offset + j*(iiL) + i;  // n4--n3
      n2 =          j*(iiR+1);    //  |\ |
      n3 =     n2 + (iiR+1);      //  | \|
      n4 =     n1 + (iiL);        // n1--n2

      xfldin.addCell(cellGroup, eTriangle, order, {n1, n2, n4});
      xfldin.addCell(cellGroup, eTriangle, order, {n3, n4, n2});
    }

    offset += (iiL)*(jjU + 1);

    // Top Row of the Bottom Right Box
    for (int i = 0; i < iiR; i++)
    {
      int j = jjD - 1; // top of the column

      n1 = offset + j*(iiR+1) + i; // n4--n3
      n2 =     n1 + 1;             //  |\ |
      n3 =      i + 1;             //  | \|
      n4 =      i;                 // n1--n2

      xfldin.addCell(cellGroup, eTriangle, order, {n1, n2, n4});
      xfldin.addCell(cellGroup, eTriangle, order, {n3, n4, n2});
    }

  }

  // size boundary trace elements
  xfldin.sizeBoundaryTrace(2*(iiL+iiR) + 2*(jjU+jjD));

  if (comm.rank() == 0)
  {
    int offset = 0;

    // Top on Top right box
    for (int i = iiR-1; i >= 0; i--)
    {
      n1 = jjU*(iiR+1) + i + 1;
      n2 = jjU*(iiR+1) + i;

      xfldin.addBoundaryTrace( iTop, eLine, {n1, n2} );
    }

    offset = (iiR + 1)*(jjU + 1);

    // Bottom on Top left box

    for (int i = 0; i < iiL-1; i++) // All but final element
    {
      n1 = offset + i;
      n2 =     n1 + 1;

      xfldin.addBoundaryTrace( iBottomInlet, eLine, {n1, n2} );
    }
    n1 = offset + iiL-1;
    n2 = 0;
    xfldin.addBoundaryTrace( iBottomInlet, eLine, {n1, n2} ); // final element

    // Top on Top left box
    n1 = jjU*(iiR + 1);
    n2 = offset + jjU*(iiL) + iiL - 1;
    xfldin.addBoundaryTrace( iTop, eLine, {n1, n2} ); // first element

    for (int i = iiL-2; i >= 0; i--) // start from 1 in
    {
      n1 = offset + jjU*(iiL) + i + 1;
      n2 =     n1 - 1;

      xfldin.addBoundaryTrace( iTop, eLine, {n1, n2} );
    }

    // Inlet on Top left Box
    for (int j = jjU-1; j >= 0; j--)
    {
      n1 = offset + j*(iiL) + (iiL);
      n2 =     n1 - (iiL);

      xfldin.addBoundaryTrace( iInlet, eLine, {n1, n2} );
    }

    offset += (iiL)*(jjU+1);

    // vertical and horizontal on Bottom right box
    n1 = 0;
    n2 = offset + (jjD-1)*(iiR+1);
    xfldin.addBoundaryTrace( iStep, eLine, {n1, n2} );

    //vertical
    for (int j = jjD-2; j >= 0; j--)
    {
      n1 = offset + j*(iiR+1) + (iiR+1);
      n2 =     n1 - (iiR+1);

      xfldin.addBoundaryTrace( iStep, eLine, {n1, n2} );
    }

    //horizontal
    for (int i = 0; i < iiR; i++)
    {
      n1 = offset + i;
      n2 =     n1 + 1;

      xfldin.addBoundaryTrace( iBottomOutlet, eLine, {n1, n2} );
    }

    // Outlet on bottom right box
    for (int j = 0; j < jjD-1; j++)
    {
      n1 = offset + j*(iiR+1) + iiR;
      n2 =     n1 + (iiR+1);

      xfldin.addBoundaryTrace( iOutlet, eLine, {n1,n2} );

    }

    // top row
    n1 = offset + (jjD-1)*(iiR+1) + iiR;
    n2 =          iiR; // end of first row

    xfldin.addBoundaryTrace( iOutlet, eLine, {n1,n2} );

    // Outlet on Top right box
    for (int j = 0; j < jjU; j++)
    {
      n1 =      j*(iiR+1) + iiR;
      n2 = n1 + (iiR+1);

      xfldin.addBoundaryTrace( iOutlet, eLine, {n1,n2} );
    }
  }// if (world.rank==0)

  this->buildFrom( xfldin );
}

XField2D_BackwardsStep_X1::XField2D_BackwardsStep_X1( mpi::communicator& comm, int power,
                                                      const Real xUpstream, const Real xDownstream,
                                                      const Real yUpstream ) : XField<PhysD2,TopoD2>(comm)
{
  /*
  xUpstream   - X distance upstream of the step
  xDownstream - X distance downstream of the step
  yUpstream   - Y Height upstream of the step

  The step is defined as a height of 1, everything else should be scaled accordingly

  The knee of the step is 0,0

  This is 3 UnionJack grids stapled together really
  */
  XField_Lagrange<PhysD2>::VectorX X;


  const int ii = pow(2,power+1);
  const int jj = ii;

  std::vector<Real> xvec_L(ii+1), yvec_U(jj+1), xvec_R(ii+1), yvec_D(jj+1);

  // xcoords upstream
  for (int i = 0; i < ii+1; i++)
    xvec_L[i] = (xUpstream)*i/Real(ii) - xUpstream;

  // ycoords above step
  for (int j = 0; j < jj+1; j++)
    yvec_U[j] = (yUpstream)*j/Real(jj);

  // xcoords downstream
  for (int i = 0; i < ii+1; i++)
    xvec_R[i] = (xDownstream)*i/Real(ii);

  // ycoords below step
  for (int j = 0; j < jj+1; j++)
    yvec_D[j] = 1*j/Real(jj) - 1;

  generateGrid( comm, xvec_L, xvec_R, yvec_U, yvec_D );


}

}
