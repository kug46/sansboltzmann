// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField3D_2Tet_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_2Tet_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_2Tet_X1_1Group_test )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  typedef std::array<int,4> Int4;

  XField3D_2Tet_X1_1Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 5 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0],-1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], 0 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  const XField3D_2Tet_X1_1Group::FieldCellGroupType<Tet>& xfldVolume = xfld.getCellGroup<Tet>(0);

  int nodeMap[4];

  xfldVolume.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  xfldVolume.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  Int4 faceSign;

  faceSign = xfldVolume.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldVolume.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );
  const XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldIface = xfld.getInteriorTraceGroup<Triangle>(0);

  xfldIface.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  BOOST_CHECK_EQUAL( xfldIface.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIface.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldIface.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldIface.getElementRight(0), 1 );

  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(0).trace, 0 );

  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(0).orientation, -1 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 6 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(2).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(3).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(4).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(5).topoTypeID() == typeid(Triangle) );

  const XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBface0 = xfld.getBoundaryTraceGroup<Triangle>(0);
  const XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBface1 = xfld.getBoundaryTraceGroup<Triangle>(1);
  const XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBface2 = xfld.getBoundaryTraceGroup<Triangle>(2);
  const XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBface3 = xfld.getBoundaryTraceGroup<Triangle>(3);
  const XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBface4 = xfld.getBoundaryTraceGroup<Triangle>(4);
  const XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBface5 = xfld.getBoundaryTraceGroup<Triangle>(5);

  xfldBface0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldBface1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldBface2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );


  xfldBface3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldBface4.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldBface5.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );


  XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle>::ElementType<> xfldElem( xfldBface0.basis() );
  XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle>::ElementType<>::RefCoordType sRef;
  XField3D_2Tet_X1_1Group::FieldTraceGroupType<Triangle>::ElementType<>::VectorX N;

  // Check areas and unit normals
  Real nxTrue, nyTrue, nzTrue;
  sRef = 1./3.;

  //------
  xfldBface0.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), sqrt(3.)/4.*2., 1e-12 );

  nxTrue =  1./sqrt(3);
  nyTrue =  1./sqrt(3);
  nzTrue =  1./sqrt(3);

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  //------
  xfldBface1.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 0.5, 1e-12 );

  nxTrue =  0;
  nyTrue = -1;
  nzTrue =  0;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  //------
  xfldBface2.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 0.5, 1e-12 );

  nxTrue =  0;
  nyTrue =  0;
  nzTrue = -1;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );



  //------
  xfldBface3.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), sqrt(3.)/4.*2., 1e-12 );

  nxTrue = -1./sqrt(3);
  nyTrue =  1./sqrt(3);
  nzTrue =  1./sqrt(3);

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  //------
  xfldBface4.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 0.5, 1e-12 );

  nxTrue =  0;
  nyTrue = -1;
  nzTrue =  0;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );

  //------
  xfldBface5.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 0.5, 1e-12 );

  nxTrue =  0;
  nyTrue =  0;
  nzTrue = -1;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBface0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBface1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBface2.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBface3.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBface4.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBface5.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBface0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBface1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBface2.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBface3.getElementLeft(0), 1 );
  BOOST_CHECK_EQUAL( xfldBface4.getElementLeft(0), 1 );
  BOOST_CHECK_EQUAL( xfldBface5.getElementLeft(0), 1 );

  BOOST_CHECK_EQUAL( xfldBface0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBface1.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBface2.getCanonicalTraceLeft(0).trace, 3 );

  BOOST_CHECK_EQUAL( xfldBface3.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBface4.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBface5.getCanonicalTraceLeft(0).trace, 3 );


  BOOST_CHECK_EQUAL( xfldBface0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBface1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBface2.getCanonicalTraceLeft(0).orientation, 1 );

  BOOST_CHECK_EQUAL( xfldBface3.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBface4.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBface5.getCanonicalTraceLeft(0).orientation, 1 );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
