// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD1D_1GROUP_PERIODIC_H
#define XFIELD1D_1GROUP_PERIODIC_H

#include "Field/XFieldLine.h"

namespace SANS
{
/*
   A unit grid that consists of ii lines within a single group with periodic connectivity (i.e. no boundary edges)
*/

class XField1D_1Group_Periodic : public XField<PhysD1,TopoD1>
{
public:
  explicit XField1D_1Group_Periodic(const int ii, Real xL = 0, Real xR = 1);
};

}

#endif //XFIELD1D_1GROUP_PERIODIC_H
