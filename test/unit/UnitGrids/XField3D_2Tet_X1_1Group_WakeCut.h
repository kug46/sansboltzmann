// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD3D_2TET_X1_1GROUP_WAKECUT_H
#define XFIELD3D_2TET_X1_1GROUP_WAKECUT_H

#include "Field/XFieldVolume.h"

namespace SANS
{
/*
   A unit grid that consists of two tetrahedron within a single group
   The interface between the two triangles is a wake cut

             2
            /|\
           / | \
          /  |  \
         /   |   \
        /    |    \
       / (1) | (0) \
      /      |      \
     4 ---- 5,0 ---- 1
      .     /     .
       .   /   .
        . / .
          3
*/

class XField3D_2Tet_X1_1Group_WakeCut : public XField<PhysD3,TopoD3>
{
public:
  XField3D_2Tet_X1_1Group_WakeCut();
};

}

#endif //XFIELD3D_2TET_X1_1GROUP_WAKECUT_H
