// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD4D_1PTOPE_X1_1GROUP
#define XFIELD4D_1PTOPE_X1_1GROUP

#include "Field/XFieldSpacetime.h"

namespace SANS
{

/* a unit grid that consists of one pentatope within a single group
 *
 *      2
 *      |\
 *      | \
 *     .|  \
 *      |   \
 *      |    \            ----> in another dimension: (4)
 *    . |     \                  (to the tune of the Wolfmother song)
 *      |      \
 *      0-------1
 *   . /     .
 *    /   .
 *   / .
 *  3
 *
 */

class XField4D_1Ptope_X1_1Group : public XField<PhysD4, TopoD4>
{
public:
  XField4D_1Ptope_X1_1Group();
};

}

#endif /* XFIELD4D_1PTOPE_X1_1GROUP */
