// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_BOX_TRIANGLE_GHOST_X1
#define XFIELD2D_BOX_TRIANGLE_GHOST_X1

#include <vector>

#include "Field/XFieldArea.h"

namespace SANS
{
// triangle grid in a unit box with 4 sides as separate boundary-edge groups
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// area elements in 1 group
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
//    (Note: the implementation allows for 1 to 3 interior-trace groups depending on
//           whether horizontal and vertical traces exist)
// boundary-edge elements in 4 groups: lower, right, upper, left
//
// SWNE_flag specifies whether the diagonals are from bottom-left to top-right (SWNE) or bottom-right to top-left (SENW)
// Default is SENW

class XField2D_Box_Triangle_Ghost_X1 : public XField<PhysD2,TopoD2>
{
public:
  XField2D_Box_Triangle_Ghost_X1( int ii, int jj, Real xmin = 0, Real xmax = 1, Real ymin = 0, Real ymax = 1, bool SWNE_flag = false )
  {
    if (SWNE_flag == false)
      XField2D_Box_Triangle_Ghost_X1_SENW( ii, jj, xmin, xmax, ymin, ymax);
    else
      XField2D_Box_Triangle_Ghost_X1_SWNE( ii, jj, xmin, xmax, ymin, ymax);
  }

  XField2D_Box_Triangle_Ghost_X1( const std::vector<Real>& xvec, const std::vector<Real>& yvec, bool SWNE_flag = false )
  {
    int ii = (int) xvec.size() - 1;
    int jj = (int) yvec.size() - 1;

    //Check ascending order
    for (int i = 0; i < ii; i++)
      SANS_ASSERT( xvec[i] < xvec[i+1] );

    for (int j = 0; j < jj; j++)
      SANS_ASSERT( yvec[j] < yvec[j+1] );

    if (SWNE_flag == false)
      XField2D_Box_Triangle_Ghost_X1_SENW( ii, jj, 0, 1, 0, 1);
    else
      XField2D_Box_Triangle_Ghost_X1_SWNE( ii, jj, 0, 1, 0, 1);

    for (int i = 0; i < ii+1; i++)
    {
      for (int j = 0; j < jj+1; j++)
      {
        DOF(j*(ii+1) + i)[0] = xvec[i];
        DOF(j*(ii+1) + i)[1] = yvec[j];
      }
    }

    checkGrid();
  }

  static const int iBottom, iRight, iTop, iLeft;

protected:
  void XField2D_Box_Triangle_Ghost_X1_SENW( int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax ); // Diagonal : |\|
  void XField2D_Box_Triangle_Ghost_X1_SWNE( int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax ); // Diagonal : |/|
};

}

#endif // XFIELD2D_BOX_TRIANGLE_GHOST_X1
