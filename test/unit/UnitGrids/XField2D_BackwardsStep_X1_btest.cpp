// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <string>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_BackwardsStep_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/output_Tecplot.h"
#include "Field/output_fluent.h"
#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_BackwardsStep_X1_test )
{

  // Check is called at the end of the constructor, hence this is actually tested

  mpi::communicator world;

  for (int i = 0; i < 2; i++)
  {
    XField2D_BackwardsStep_X1 xfld1( world, i );

#if 0
      string filename = "tmp/BackwardStep2D_X";
      filename += to_string(i);
      filename += ".dat";
      output_Tecplot(xfld1, filename);

//      filename += ".cas";
//      output_fluent(xfld1, filename);
//


//      filename += ".grm";
//      WriteMeshGrm(xfld1, filename);
#endif
  }
}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
