// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"

#include <cmath> // isnan

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "LinearAlgebra/DenseLinAlg/InverseQR.h"

namespace SANS
{

namespace       // local definitions
{

bool verbose = false;

//----------------------------------------------------------------------------//
//  cubic-source streamline: source strength
//
// tau    bump height (e.g. tau = 0.1)
// src    source strength

void src_cubicsource( const Real& tau, Real& src )
{
  src = tau*15000 / (1875*PI - 160 - 3750*atan(0.2) - 3243*atan(5.));
}

//----------------------------------------------------------------------------//
//  cubic-source streamline: streamfunction for given (x,y)
//
// src    source strength
// rhs0   streamline const (additive)
// rhs    streamline const

void rhs_cubicsource( const Real& src, const Real& rhs0, const Real& x, const Real& y, Real& rhs )
{
  if (y == 0)
  {
    if ((x > 0) && (x < 1))
      rhs = -0.25*src*PI*(x*x * (x - 1)*(x - 1));
    else
      rhs = 0;
  }
  else
  {
    rhs = y + (src/12.)*( -y*(1 + 9*x*(x - 1) - 3*y*y)
          + 3*(x*x*(x - 1)*(x - 1) - y*y*(1 + 6*x*(x - 1) - y*y))*(atan((x-1)/y) - atan(x/y))
          + 6*(x - 0.5)*y*(x*(x - 1) - y*y)*log((x*x + y*y)/((x - 1)*(x - 1) + y*y)) );
  }

  rhs = rhs + rhs0;

  //write(*,'("rhs: src =",es13.5,"  rhs0 =",es13.5," (x,y) =",2es13.5,"  rhs =",es13.5)')  &
  //  src, rhs0, x, y, rhs
  if (verbose)
    std::cout << "rhs: src = " << src << "  rhs0 = " << rhs0 << "  (x,y) = " << x << " " << y << "  rhs = " << rhs << std::endl;
}

//----------------------------------------------------------------------------//
//  cubic-source streamline: streamfunction jacobian wrt y for given (x,y)
//                           (also u velocity)
//
// src    source strength
// jac    jacobian of streamline const wrt y

void jac_cubicsource( const Real& src, const Real& x, const Real& y, Real& jac )
{
  if (y == 0)
    if (x == 0 || x == 1)
      jac = 1 - (src/12.);
    else
      jac = 1 - (src/12.)*( 1 + 12*x*(x - 1) - 6*x*(x - 1)*(x - 0.5)*log(x*x/((x - 1)*(x - 1))) );
  else
    jac = 1 - (src/12.)*( 1 + 12*x*(x - 1) - 12*y*y
          + 6*y*(1 + 6*x*(x - 1) - 2*y*y)*(atan((x-1)/y) - atan(x/y))
          - 6*(x - 0.5)*(x*(x - 1) - 3*y*y)*log((x*x + y*y)/((x - 1)*(x - 1) + y*y)) );

////  write(*,'("jac: src =",es13.5," (x,y) =",2es13.5,"  jac =",es13.5)') src, x, y, jac
}

//----------------------------------------------------------------------------//
//  cubic-source streamline: calc y given src, x
//
// src    source strength
// rhs0   streamline const (additive)

void yofx_cubicsource( const Real& src, const Real& rhs0, const Real& x, Real& y )
{
  Real rhs, jac, dy, eps;
  int nn;

  //write(*,'("yofx: src =",es13.5,"  rhs0 =",es13.5," (x,y) =",2es13.5)')  &
  //  src, rhs0, x, y
  if (verbose)
    std::cout << "yofx_cubicsource: src = " << src << "  rhs0 = " <<  rhs0 << "  (x,y) = " << x << " " << y << std::endl;

  eps = 1e-12;
  nn = 10;
  for (int n = 0; n < nn; n++)
  {
    rhs_cubicsource( src, rhs0, x, y, rhs );
    jac_cubicsource( src, x, y, jac );

    dy = -rhs/jac;

    //write(*,'(i3,4es13.5)') n, y, dy, rhs, jac
    if (verbose)
      std::cout << "yofx_cubicsource: " << n <<  " " << y << " " << dy << " " << rhs << " " << jac << std::endl;
    y  = y + dy;
    if (fabs(dy) <= eps) break;
  }
}

}               // local definitions


//----------------------------------------------------------------------------//
// cubic-source bump channel flow
// create triangle grid in 3x1 channel with ii x jj (quad) elements
//
// diagonals oriented as Union Jack flag
//
// area elements in 1 group: 2*ii*jj triangles
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 4 groups: lower, right, upper, left

XField2D_CubicSourceBump_Xq::XField2D_CubicSourceBump_Xq( const int ii, const int jj, Real tau )
 : XField2D_Box_UnionJack_Triangle_X1(ii, jj)
{
  init_X1(ii, jj, tau);
}

void XField2D_CubicSourceBump_Xq::init_X1( const int ii, const int jj, Real tau )
{
  // channel dimensions
  Real xmin = -1;
  Real xle  =  0;
  Real xte  =  1;
  Real xmax =  2;
  Real ymin =  0;
  Real ymax =  1;

  // cell allotment
  int iibump = ii - 2*jj;
  int iifore = jj;

  // x-distribution of nodes

  int ile = iifore;
  int ite = ile + iibump;
  Real dx = (xte - xle)/static_cast<Real>(iibump);
  Real stretch = - log(dx) / log(iifore);
  Real arg;

  for (int i = 0; i < ile; i++)
  {
    arg = static_cast<Real>(ile - i) / static_cast<Real>(ile);
    DOF(i)[0] = xle - (xle - xmin) * pow( arg, stretch );
  }
  for (int i = ile; i < ite; i++)
  {
    DOF(i)[0] = xle + (xte - xle) * static_cast<Real>(i - ile) / static_cast<Real>(iibump);
  }
  for (int i = ite; i < ii+1; i++)
  {
    arg = static_cast<Real>(i - ite) / static_cast<Real>(ii - ite);
    DOF(i)[0] = xte + (xmax - xte) * pow( arg, stretch );
  }
  DOF(0)[0]   = xmin;
  DOF(ile)[0] = xle;
  DOF(ite)[0] = xte;
  DOF(ii)[0]  = xmax;

  for (int j = 1; j < jj+1; j++)
    for (int i = 0; i < ii+1; i++)
      DOF(j*(ii+1) + i)[0] = DOF(i)[0];

  // y-distribution of nodes

  Real src, rhs;

  src_cubicsource( tau, src );

  // lower wall (special to avoid numerical issues)
  for (int i = 0; i <= ile; i++)
    DOF(i)[1] = ymin;
  for (int i = ite; i < ii+1; i++)
    DOF(i)[1] = ymin;

  rhs_cubicsource( src, 0, xle, ymin, rhs );
  for (int i = ile+1; i < ite; i++)
  {
    DOF(i)[1] = tau;
    yofx_cubicsource( src, -rhs, DOF(i)[0], DOF(i)[1] );
  }

  for (int j = 1; j < jj+1; j++)
  {
    // Ni bump distribution along inlet
    arg = static_cast<Real>(j) / static_cast<Real>(jj);
    DOF(j*(ii+1))[1] = ymin + (ymax - ymin) * pow( arg, stretch );

    rhs_cubicsource( src, 0, xmin, DOF(j*(ii+1))[1], rhs );
    for (int i = 1; i < ii+1; i++)
    {
      DOF(j*(ii+1) + i)[1] = DOF(j*(ii+1) + i - 1)[1];
      yofx_cubicsource( src, -rhs, DOF(j*(ii+1) + i)[0], DOF(j*(ii+1) + i)[1] );
    }
    DOF(jj*(ii+1))[1]      = ymax;
    DOF(jj*(ii+1) + ii)[1] = ymax;
  }

#undef USE_GGP
#ifdef USE_GGP
  std::fstream fout( "tmp/cubic.ggp", std::fstream::out );
  fout.precision(16);
  for (j = 0; j < jj+1; j++)
  {
    fout << "j" << j << std::endl;
    fout << "i j x y" << std::endl;
    for (i = 0; i < ii+1; i++)
      fout << i+1 << " " << j+1 << " " << DOF(j*(ii+1) + i)[0] << " " << DOF(j*(ii+1) + i)[1] << std::endl;
    fout << "*EOF" << std::endl;
  }
#endif  // USE_GGP


  //Check that the grid is correct
  checkGrid();
}

// General Order P grid

XField2D_CubicSourceBump_Xq::XField2D_CubicSourceBump_Xq( const int ii, const int jj, Real tau, const int order )
 : XField2D_Box_UnionJack_Triangle_X1(ii, jj)
{
  SANS_ASSERT_MSG( (order > 0), "order must be greater than 0" );
  typedef FieldTraceGroupType<Line>::template ElementType<> ElementType;
  typedef DLA::VectorS<2, Real> VectorX;

  if (order == 1)
  {
    // Simplly initialize the X1 grid
    init_X1(ii, jj, tau);
    return;
  }
  else
  {
    // Create a temporary X1 grid that is used to construct the higher-order grid
    XField2D_CubicSourceBump_Xq xfld(ii, jj, tau);
    buildFrom(xfld, order);
  }

  Real src, rhs;

  Real xmin = -1;
  Real xle  =  0;
  Real ymin =  0;
  Real ymax =  1;

  src_cubicsource( tau, src );
  rhs_cubicsource( src, 0, xle, ymin, rhs );

  FieldTraceGroupType<Line>& xfldBottom = this->getBoundaryTraceGroup<Line>(0);

  ElementType xfldElem( xfldBottom.basis() );

  DLA::VectorD<Real> s(order+1);
  Real pr = order;

  //extra evaluation points (equally spaced)
  for (int i = 0; i < (order+1); i++)
  {
    Real ir = i;
    s[i] = ir/pr;
  }

  //extra evaluation points (extended chebyshev)
//  for (int i=0; i<(order+1); i++)
//  {
//    Real ir = i;
//    s[i] = (1+cos( (2*ir+1)*PI / (2*pr+2)) / cos( PI / (2*pr2)))/2;
//  }


//  std::cout << "Bottom: \n";
  for (int i = 0; i < xfldBottom.nElem(); i++)
  {
    xfldBottom.getElement( xfldElem , i);

    VectorX xy = 0;

    DLA::VectorD<Real> bx(order+1);
    DLA::VectorD<Real> by(order+1);
    DLA::VectorD<Real> cx(order+1);
    DLA::VectorD<Real> cy(order+1);
    DLA::MatrixD<Real> A(order+1,order+1);

    DLA::VectorD<Real> phi(order+1);

    bx = 0; by = 0; cx = 0; cy = 0;

    // set interior nodes (bx, by) with linear data
    for (int j = 0; j < order+1; j++)
    {
      xy = xfldElem.eval( s[j] );

      bx[j] = xy[0];
      by[j] = xy[1];

      //move y coord to exact solution
      yofx_cubicsource( src, -rhs, xy[0], xy[1]  );

      if (!std::isnan(xy[1])) //(skip edge nodes to avoid numerical troubles)
      {
        by[j] = xy[1];
      }

    }

    // construct jacobian
    for (int j = 0; j < order+1; j++)
    {
    xfldElem.basis()->evalBasis(s[j], phi.data(), phi.size());
    for (int k=0; k<(order+1); k++)
      A(j,k) = phi[k];
    }

    DLA::MatrixD<Real> Ainv = DLA::InverseQR::Inverse(A);

    cx = Ainv*bx;
    cy = Ainv*by;

    for (int j=0; j < xfldElem.nDOF(); j++)
    {
      xfldElem.DOF(j)(0) = cx[j];
      xfldElem.DOF(j)(1) = cy[j];
    }

    xfldBottom.setElement( xfldElem, i );

    //DUMP
//    for (int j=0; j<4; j++)
//    {
//      Real s1 = j/3.0;
//      xy = xfldElem.eval(s1);
//      Real y2 = 0;
//      yofx_cubicsource( src, -rhs, xy[0], y2  );
//      std::cout << xy[0] << " " << xy[1] << " " << y2 << "\n";
//    }

  }

  //top side
  FieldTraceGroupType<Line>& xfldTop = this->getBoundaryTraceGroup<Line>(2);

  rhs_cubicsource( src, 0, xmin, ymax, rhs );

//  std::cout << "Top: \n";
  for (int i = 0; i < xfldTop.nElem(); i++)
  {
    xfldTop.getElement( xfldElem ,i);

    VectorX xy;

    DLA::VectorD<Real> bx(order+1);
    DLA::VectorD<Real> by(order+1);
    DLA::VectorD<Real> cx(order+1);
    DLA::VectorD<Real> cy(order+1);
    DLA::MatrixD<Real> A(order+1,order+1);

    DLA::VectorD<Real> phi(order+1);

    bx = 0; by =0; cx = 0; cy=0;

    // set interior nodes (bx, by) with linear data
    for (int j=0; j < order+1; j++)
    {
      xy = xfldElem.eval( s[j] );

      bx[j] = xy[0];
      by[j] = xy[1];

      //move y coord to exact solution
      yofx_cubicsource( src, -rhs, xy[0], xy[1]  );

      if (!std::isnan(xy[1])) //(skip edge nodes to avoid numerical troubles)
      {
        by[j] = xy[1];
      }

    }

    // construct jacobian
    for (int j = 0; j < order+1; j++)
    {
      xfldElem.basis()->evalBasis(s[j], phi.data(), phi.size());
      for (int k = 0; k < (order+1); k++)
        A(j,k) = phi[k];
    }

    DLA::MatrixD<Real> Ainv = DLA::InverseQR::Inverse(A);

    cx = Ainv*bx;
    cy = Ainv*by;

    for (int j=0; j<xfldElem.nDOF(); j++)
    {
      xfldElem.DOF(j)(0) = cx[j];
      xfldElem.DOF(j)(1) = cy[j];
    }

    xfldTop.setElement( xfldElem,i);
  }

//  std::cout << "\n";
//  //examine nodes
//  FieldCellGroupType<Triangle>& xfldCell = this->getCellGroup<Triangle>(0);
//
//  typedef FieldCellGroupType<Triangle>::template ElementType<> ElementCellType;
//  ElementCellType xfldCellElem( xfldCell.basis() );
//  DLA::VectorD<Real> sref = {0.0, 1.0, 0.0, 2.0/3.0, 1.0/3.0, 0.0, 0.0, 1.0/3.0, 2.0/3.0, 1.0/3.0};
//  DLA::VectorD<Real> tref = {0.0, 0.0, 1.0, 1.0/3.0, 2.0/3.0, 2.0/3.0, 1.0/3.0, 0.0, 0.0, 1.0/3.0};
//
//  for (int i=0; i<xfldCell.nElem(); i++)
//  {
//
//    xfldCell.getElement(xfldCellElem,i);
//    VectorX xy;
//
//    for (int j=0; j<10; j++)
//    {
//      xy = xfldCellElem.eval( sref[j], tref[j] );
//      std::cout << xy << "\n";
//    }
//
//
//  }




  checkGrid();

}

}
