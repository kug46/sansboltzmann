// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "XField3D_Sphere_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// triangle grid on a unit sphere with 2 sides at the poles
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// thetamin and thetamax define the polar angle range 0 < theta < 180
// area elements in 1 group
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 2 groups: southern, northern

XField3D_Sphere_Triangle_X1::XField3D_Sphere_Triangle_X1( int ii, int jj, Real thetamin, Real thetamax, Real phimin, Real phimax )
{
  int comm_rank = comm_->rank();

  int nnode = (ii+1)*jj;
  int nelem = 2*ii*jj;

  nElem_ = nelem;

  SANS_ASSERT( thetamin > 0 && thetamin < 180);
  SANS_ASSERT( thetamax > 0 && thetamax < 180);
  SANS_ASSERT( thetamin < thetamax);

  // create the grid-coordinate DOF arrays
  resizeDOF( nnode );

  thetamin *= PI/180.;
  thetamax *= PI/180.;

  Real r = 1;

  for (int j = 0; j < jj; j++)
  {
    Real phi = phimin + (phimax - phimin)*j/Real(jj);
    for (int i = 0; i < ii+1; i++)
    {
      Real theta = thetamin + (thetamax - thetamin)*i/Real(ii);
      DOF(j*(ii+1) + i)[0] = r*sin(theta)*cos(phi);
      DOF(j*(ii+1) + i)[1] = r*sin(theta)*sin(phi);
      DOF(j*(ii+1) + i)[2] = r*cos(theta);
    }
  }

  // create the element groups
  int cnt = 1;          // interior: diagonal
  if (ii > 1) cnt++;    // interior: horizontal
  if (jj > 1) cnt++;    // interior: vertical
  resizeInteriorTraceGroups(cnt);

  resizeBoundaryTraceGroups(2);     // south, north
  resizeCellGroups(1);

  // grid area field variable

  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nelem );

  // area element grid-coordinate DOF associativity (cell-to-node connectivity)

  int n1, n2, n3, n4;
  int c1, c2;
  for (int j = 0; j < jj-1; j++)
  {
    // Connect the last set of nodes to the first set
    for (int i = 0; i < ii; i++)
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  |\ |
      n3 = n2 + (ii+1);         //  | \|
      n4 = n1 + (ii+1);         // n1--n2

      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      fldAssocCell.setAssociativity( c1 ).setRank( comm_rank );
      fldAssocCell.setAssociativity( c2 ).setRank( comm_rank );

      fldAssocCell.setAssociativity( c1 ).setNodeGlobalMapping( {n1, n2, n4} );
      fldAssocCell.setAssociativity( c2 ).setNodeGlobalMapping( {n3, n4, n2} );
    }
  }

  // Connect the last set of nodes to the first set
  for (int i = 0; i < ii; i++)
  {
    n1 = (jj-1)*(ii+1) + i;   // n4--n3
    n2 = n1 + 1;              //  |\ |
    n3 = i+1;                 //  | \|
    n4 = i;                   // n1--n2

    c1 = (jj-1)*(2*ii) + 2*i;
    c2 = (jj-1)*(2*ii) + 2*i + 1;

    fldAssocCell.setAssociativity( c1 ).setRank( comm_rank );
    fldAssocCell.setAssociativity( c2 ).setRank( comm_rank );

    fldAssocCell.setAssociativity( c1 ).setNodeGlobalMapping( {n1, n2, n4} );
    fldAssocCell.setAssociativity( c2 ).setNodeGlobalMapping( {n3, n4, n2} );
  }



  // grid interior-edge field variable
  int edge, nedge;

  cnt = 0;

  // Longitude edges from north to south edges

  if (jj > 1)
  {
    nedge = jj*ii;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;
    for (int j = 1; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;

        c1 = (j  )*(2*ii) + 2*i;
        c2 = (j-1)*(2*ii) + 2*i + 1;

        fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

        // grid-coordinate DOF association (edge-to-node connectivity)
        fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

        // L is +1, and R is -1
        fldAssocCell.setAssociativity( c1 ).setEdgeSign( +1, 2 );
        fldAssocCell.setAssociativity( c2 ).setEdgeSign( -1, 2 );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }

    }

    // Connect the last set of nodes to the first set
    for (int i = 0; i < ii; i++)
    {
      n1 = i;
      n2 = n1 + 1;

      c1 = 2*i;
      c2 = (jj-1)*(2*ii) + 2*i + 1;

      fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // L is +1, and R is -1
      fldAssocCell.setAssociativity( c1 ).setEdgeSign( +1, 2 );
      fldAssocCell.setAssociativity( c2 ).setEdgeSign( -1, 2 );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

      edge++;
    }


    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Line>( fldAssocIedge );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);

    cnt++;
  }

  // Latitude edges from west to east
  if (ii > 1)
  {
    nedge = (ii-1)*jj;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;
    for (int i = 1; i < ii; i++)
    {
      for (int j = 0; j < jj-1; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

        // grid-coordinate DOF association (edge-to-node connectivity)
        fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

        // L is +1, and R is -1
        fldAssocCell.setAssociativity( c1 ).setEdgeSign( +1, 1 );
        fldAssocCell.setAssociativity( c2 ).setEdgeSign( -1, 1 );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }

      n1 = (jj-1)*(ii+1) + i;
      n2 = i;
      c1 = (jj-1)*(2*ii) + 2*i - 1;
      c2 = (jj-1)*(2*ii) + 2*i;

      fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // L is +1, and R is -1
      fldAssocCell.setAssociativity( c1 ).setEdgeSign( +1, 1 );
      fldAssocCell.setAssociativity( c2 ).setEdgeSign( -1, 1 );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

      edge++;
    }

    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Line>( fldAssocIedge );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;

  }

  // diagonal edges
  {
    nedge = ii*jj;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
       fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;
    for (int j = 0; j < jj-1; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        n1 = j*(ii+1) + i + 1;
        n2 = n1 + (ii+1) - 1;
        c1 = j*(2*ii) + 2*i;
        c2 = j*(2*ii) + 2*i + 1;

        fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

        // grid-coordinate DOF association (edge-to-node connectivity)
        fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

        // L is +1, and R is -1
        fldAssocCell.setAssociativity( c1 ).setEdgeSign( +1, 0 );
        fldAssocCell.setAssociativity( c2 ).setEdgeSign( -1, 0 );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

        edge++;
      }
    }

    for (int i = 0; i < ii; i++)
    {
      n1 = (jj-1)*(ii+1) + i + 1;
      n2 = i;
      c1 = (jj-1)*(2*ii) + 2*i;
      c2 = (jj-1)*(2*ii) + 2*i + 1;

      fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // L is +1, and R is -1
      fldAssocCell.setAssociativity( c1 ).setEdgeSign( +1, 0 );
      fldAssocCell.setAssociativity( c2 ).setEdgeSign( -1, 0 );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

      edge++;
    }

    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Line>( fldAssocIedge );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;
  }

  // grid boundary-edge field variables
  // Note: all edges oriented so domain is on its left

  // southern boundary (z min)
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocBedge.setGroupLeft( 0 );

    // edge element grid-coordinate DOF associativity (edge-to-node connectivity)
    for (int j = 0; j < jj-1; j++)
    {
      n1 = j*(ii+1) + ii;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*ii - 1;
      edge = j;

      fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

      // edge-to-cell connectivity
      fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // L is +1
      fldAssocCell.setAssociativity( c1 ).setEdgeSign( +1, 1 );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), edge );
    }

    n1 = (jj-1)*(ii+1) + ii;
    n2 = ii;
    c1 = (jj-1)*(2*ii) + 2*ii - 1;
    edge = (jj-1);

    fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

    // edge-to-cell connectivity
    fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

    // L is +1
    fldAssocCell.setAssociativity( c1 ).setEdgeSign( +1, 1 );

    // grid-coordinate DOF association (edge-to-node connectivity)
    fldAssocBedge.setElementLeft( c1, edge );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), edge );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge );
    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);

  }

  // northern boundary (z max)
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocBedge.setGroupLeft( 0 );

    n1 = 0;
    n2 = (jj-1)*(ii+1);
    c1 = (jj-1)*(2*ii);
    edge = 0;

    fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

    // grid-coordinate DOF association (edge-to-node connectivity)
    fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

    // L is +1
    fldAssocCell.setAssociativity( c1 ).setEdgeSign( +1, 1 );

    // edge-to-cell connectivity
    fldAssocBedge.setElementLeft(  c1, edge );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), edge );

    for (int j = jj-2; j >= 0; j--)
    {
      n1 = j*(ii+1) + (ii+1);
      n2 = j*(ii+1);
      c1 = j*(2*ii);
      edge = jj-1 - j;

      fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // L is +1
      fldAssocCell.setAssociativity( c1 ).setEdgeSign( +1, 1 );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), edge );
    }

    boundaryTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocBedge );
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  }

  cellGroups_[0] = new FieldCellGroupType<Triangle>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
