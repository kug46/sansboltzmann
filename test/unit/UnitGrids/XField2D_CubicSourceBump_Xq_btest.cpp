// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <string>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_CubicSourceBump_Xq.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/output_grm.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_CubicSourceBump_Xq_test )
{
  const int ii = 32;
  const int jj = 8;
  Real tau = 0.1;

  // Check is called at the end of the constructor, hence this is actually tested
  XField2D_CubicSourceBump_Xq xfld( ii, jj, tau );

  int order = 2;
  XField2D_CubicSourceBump_Xq xfld2( ii, jj, tau, order );

  order = 3;
  XField2D_CubicSourceBump_Xq xfld3( ii, jj, tau, order );

  order = 4;
  XField2D_CubicSourceBump_Xq xfld4( ii, jj, tau, order );
}

BOOST_AUTO_TEST_CASE( XField2D_CubicSourceBump_Xq_Series_test )
{
//  int ii = 6;
//  int jj = 2;
  Real tau = 0.1;


  //test high order constructor
  for (int order = 3; order < 4; order++)
  {
    for (int j = 2; j < 3; j++)
    {
      int jj = pow(2,j);
      int ii = 3*jj;

      // Check is called at the end of the constructor, hence this is actually tested
      XField2D_CubicSourceBump_Xq xfld(ii, jj, tau, order);

#if 0
      string filename = "tmp/cubicsource_Q";
      filename += to_string(p);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".grm";
      output_grm(xfld, p, filename);
#endif

    }
  }

}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
