// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField1D_4Line_X1_2Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField1D_4Line_X1_2Group.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_4Line_X1_2Group_test )
{
  XField1D_4Line_X1_2Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 5 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 2 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 3 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 4 );

  // line field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getCellGroupBase(1).topoTypeID() == typeid(Line) );

  const XField1D_4Line_X1_2Group::FieldCellGroupType<Line>& xfldCell0 = xfld.getCellGroup<Line>(0);
  const XField1D_4Line_X1_2Group::FieldCellGroupType<Line>& xfldCell1 = xfld.getCellGroup<Line>(1);

  int nodeMap[2];

  xfldCell0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldCell0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldCell1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldCell1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 3 );

  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(1).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(2).topoTypeID() == typeid(Node) );

  const XField1D_4Line_X1_2Group::FieldTraceGroupType<Node>& xfldInode0 = xfld.getInteriorTraceGroup<Node>(0);
  const XField1D_4Line_X1_2Group::FieldTraceGroupType<Node>& xfldInode1 = xfld.getInteriorTraceGroup<Node>(1);
  const XField1D_4Line_X1_2Group::FieldTraceGroupType<Node>& xfldInode2 = xfld.getInteriorTraceGroup<Node>(2);

  xfldInode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  xfldInode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  xfldInode2.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );

  //Normals
  BOOST_CHECK_EQUAL( xfldInode0.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode0.associativity(0).normalSignR(), -1 );
  BOOST_CHECK_EQUAL( xfldInode1.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode1.associativity(0).normalSignR(), -1 );
  BOOST_CHECK_EQUAL( xfldInode2.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode2.associativity(0).normalSignR(), -1 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode0.getGroupRight(), 1 );
  BOOST_CHECK_EQUAL( xfldInode0.getElementLeft(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode0.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode0.getCanonicalTraceRight(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldInode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode1.getGroupRight(), 0 );
  BOOST_CHECK_EQUAL( xfldInode1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode1.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode1.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode1.getCanonicalTraceRight(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldInode2.getGroupLeft(), 1 );
  BOOST_CHECK_EQUAL( xfldInode2.getGroupRight(), 1 );
  BOOST_CHECK_EQUAL( xfldInode2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode2.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode2.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode2.getCanonicalTraceRight(0).trace, 1 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField1D_4Line_X1_2Group::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField1D_4Line_X1_2Group::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );

  //Normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(),-1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(), 0 );

  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(), 0 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
