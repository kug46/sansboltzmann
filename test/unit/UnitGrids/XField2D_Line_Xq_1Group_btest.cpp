// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// XField2D_Line_Xq_1Group_btest.cpp

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_Line_X1_1Group.h"
#include "XField2D_Line_Xq_1Group.h"

//#include "Field/output_Tecplot.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Line_Xq_1Group_test )
{
  const int nelem = 10;
  const Real thetamin = 0;   // unit: degree angle
  const Real thetamax = 45;  // unit: degree angle

  // ---------- Generate linear grid of arc ----------
  BOOST_CHECK_EQUAL( nelem+1, XField2D_Line_Xq_1Group::nNode(1,nelem) );

  std::vector<DLA::VectorS<2,Real>> coordinates_X1(nelem+1);
  Real theta;
  for (int i = 0; i < nelem+1; i++)
  {
    theta = ( (thetamax - thetamin) * PI/180.0 ) * ( static_cast<Real>(i) / static_cast<Real>(nelem) );
    coordinates_X1[i] = { cos(theta), sin(theta) };
  }

  XField2D_Line_X1_1Group xfld_X1(coordinates_X1);

  // ---------- Generate and test higher-order grids ----------
  for ( int order = 2; order < 8; order++ )  // loop over order
  {
    BOOST_CHECK_EQUAL( nelem*order+1, XField2D_Line_Xq_1Group::nNode(order,nelem) );

    // Compute coordinates
    const int nDOF = nelem*order+1;
    std::vector<DLA::VectorS<2,Real>> coordinates(nDOF);
    Real theta;
    for (int iDOF = 0; iDOF < nDOF; iDOF++ )
    {
      theta = ( (thetamax - thetamin) * PI/180.0 ) * ( static_cast<Real>(iDOF) / static_cast<Real>(nDOF-1) );
      coordinates[iDOF] = { cos(theta), sin(theta) };
    }

    // Construct grids so that checkGrid() is invoked
    XField2D_Line_Xq_1Group xfld( xfld_X1, order, coordinates);
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Line_Xq_1Group_unstructured_test )
{
  const int nelem = 10;
  const Real thetamin = 0;   // unit: degree angle
  const Real thetamax = 45;  // unit: degree angle

  // ---------- Generate linear grid of arc ----------

  std::vector<DLA::VectorS<2,Real>> coordinates_X1(nelem+1);
  Real theta;
  for (int i = 0; i < nelem+1; i++)
  {
    theta = ( (thetamax - thetamin) * PI/180.0 ) * ( static_cast<Real>(i) / static_cast<Real>(nelem) );
    coordinates_X1[i] = { cos(theta), sin(theta) };
  }

  XField2D_Line_X1_1Group_unstructured xfld_X1(coordinates_X1);

  // ---------- Generate and test higher-order grids ----------
  for ( int order = 2; order < 8; order++ )  // loop over order
  {
    BOOST_CHECK_EQUAL( nelem*order+1, XField2D_Line_Xq_1Group::nNode(order,nelem) );

    // Compute coordinates
    const int nDOF = nelem*order+1;
    std::vector<DLA::VectorS<2,Real>> coordinates(nDOF);
    Real theta;

    for (int iDOF = 0; iDOF < nDOF; iDOF++ )
    {
      theta = ( (thetamax - thetamin) * PI/180.0 ) * ( static_cast<Real>(iDOF) / static_cast<Real>(nDOF-1) );
      coordinates[iDOF] = { cos(theta), sin(theta) };
    }

    // Construct grids so that checkGrid() is invoked
    XField2D_Line_Xq_1Group_unstructured xfld( xfld_X1, order, coordinates);
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
