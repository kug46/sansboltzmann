// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_1Triangle_GhostBoundary_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
/*
    2
    |\
    | \
    |  \
    |   \
    |    \
    | (0) \
    |      \
    0 ----- 1
*/

XField2D_1Triangle_GhostBoundary_X1_1Group::XField2D_1Triangle_GhostBoundary_X1_1Group(  )
{
  // Last boundary trace group is a ghost boundary
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(3);
  //Create the element groups
  resizeInteriorTraceGroups(0);
  resizeBoundaryTraceGroups(2);
  resizeGhostBoundaryTraceGroups(1);
  resizeCellGroups(1);

  // nodal coordinates for the triangle.
  DOF(0) = {0, 0};
  DOF(1) = {1, 0};
  DOF(2) = {0, 1};

  // area field variable
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );

  // set the cell processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );

  //element area associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );

  // edge signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );


  FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
  cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

  xfldElementGroup->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // interior-edge field variable

  // none

  // boundary-edge field variable
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge0( BasisFunctionLineBase::HierarchicalP1, 1 );
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge1( BasisFunctionLineBase::HierarchicalP1, 1 );
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge2( BasisFunctionLineBase::HierarchicalP1, 1 );

  // boundary edge processor ranks
  fldAssocBedge0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBedge1.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBedge2.setAssociativity( 0 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocBedge0.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
  fldAssocBedge1.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );
  fldAssocBedge2.setAssociativity( 0 ).setNodeGlobalMapping( {2, 0} );

  // edge-to-cell connectivity
  fldAssocBedge0.setGroupLeft( 0 );
  fldAssocBedge1.setGroupLeft( 0 );
  fldAssocBedge2.setGroupLeft( 0 );
  fldAssocBedge0.setElementLeft( 0, 0 );
  fldAssocBedge1.setElementLeft( 0, 0 );
  fldAssocBedge2.setElementLeft( 0, 0 );
  fldAssocBedge0.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
  fldAssocBedge1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocBedge2.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );


  boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge0 );
  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);

  boundaryTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocBedge1 );
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

  ghostBoundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge2 );
  ghostBoundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);


  //Check that the grid is correct
  checkGrid();
}

}
