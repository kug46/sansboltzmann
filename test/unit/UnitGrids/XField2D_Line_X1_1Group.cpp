// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "tools/SANSException.h"
#include <fstream>

#include "XField2D_Line_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

// -----------------------------------------------------
XField2D_Line_X1_1Group::XField2D_Line_X1_1Group(const ArrayVector& coordinates)
{
  const int ii = (int) coordinates.size() - 1; // number of line elements

  SANS_ASSERT_MSG( ii >= 2, "This grid must have at least two elements." );

  int comm_rank = comm_->rank();

  // Create the DOF arrays
  resizeDOF( ii+1 );

  // Create groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(2);
  resizeCellGroups(1);

  // Assign DOFs with input grid coordinates
  for (int i = 0; i < ii+1; i++)
    DOF(i) = coordinates[i];

  // Set associativity for cell group

  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, ii );

  for ( int i = 0; i < ii; i++ )
  {
    fldAssocCell.setAssociativity( i ).setRank( comm_rank );
    fldAssocCell.setAssociativity( i ).setNodeGlobalMapping( {i, i+1} );
  }

  cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // Set associativity for interior trace group

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode( BasisFunctionNodeBase::P0, ii-1 );

  fldAssocInode.setGroupLeft( 0 );
  fldAssocInode.setGroupRight( 0 );

  for ( int i = 0; i < ii-1; i++ )
  {
    fldAssocInode.setAssociativity( i ).setRank( comm_rank );

    fldAssocInode.setAssociativity( i ).setNodeGlobalMapping( {i+1} );

    fldAssocInode.setAssociativity( i ).setNormalSignL(  1 );
    fldAssocInode.setAssociativity( i ).setNormalSignR( -1 );

    fldAssocInode.setElementLeft( i, i );
    fldAssocInode.setElementRight( i+1, i );
    fldAssocInode.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), i );
    fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), i );
  }

  interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocInode );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

  // Set associativity for boundary trace group

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode0( BasisFunctionNodeBase::P0, 1 );
  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode1( BasisFunctionNodeBase::P0, 1 );

  fldAssocBnode0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBnode1.setAssociativity( 0 ).setRank( comm_rank );

  fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
  fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {ii} );

  fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
  fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

  fldAssocBnode0.setGroupLeft( 0 );
  fldAssocBnode0.setElementLeft( 0, 0 );
  fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

  fldAssocBnode1.setGroupLeft( 0 );
  fldAssocBnode1.setElementLeft( ii-1, 0 );
  fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

  // Check grid validity
  checkGrid();
}


// -----------------------------------------------------
XField2D_Line_X1_1Group_unstructured::
    XField2D_Line_X1_1Group_unstructured(const ArrayVector& coordinates)
{
  const int ii = (int) coordinates.size() - 1; // number of line elements

  SANS_ASSERT_MSG( (ii % 2 == 0), "Number of elements should be even." );

  int comm_rank = comm_->rank();

  // Create the DOF arrays
  resizeDOF( ii+1 );

  // Create groups
  resizeInteriorTraceGroups(1); // Interior
  resizeBoundaryTraceGroups(2); // no boundary trace
  resizeCellGroups(1);

  // Assign DOFs with input grid coordinates
  for (int i = 0; i < ii+1; i++)
    DOF(i) = coordinates[i];

  // Set associativity for cell group

  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, ii );

  for ( int i = 0; i < ii; i++ )
  {
    fldAssocCell.setAssociativity( i ).setRank( comm_rank );
    if ( i % 2 == 0 )
      fldAssocCell.setAssociativity( i ).setNodeGlobalMapping( {i, i+1} );
    else
      fldAssocCell.setAssociativity( i ).setNodeGlobalMapping( {i+1, i} ); // flipping sRef direction
  }

  cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = ii;

  // Set associativity for interior trace group

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode( BasisFunctionNodeBase::P0, ii-1 );

  fldAssocInode.setGroupLeft( 0 );
  fldAssocInode.setGroupRight( 0 );

  for ( int i = 0; i < ii-1; i++ )
  {
    fldAssocInode.setAssociativity( i ).setRank( comm_rank );

    fldAssocInode.setAssociativity( i ).setNodeGlobalMapping( {i+1} );

    fldAssocInode.setElementLeft( i, i );
    fldAssocInode.setElementRight( i+1, i );

    // reference coordinate direction alternates between odd/even-numbered elements
    if ( i % 2 == 0 )
    {
      fldAssocInode.setAssociativity( i ).setNormalSignL( 1 );
      fldAssocInode.setAssociativity( i ).setNormalSignR( 1 );
      fldAssocInode.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), i );
      fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(0, 0), i );
    }
    else if ( i % 2 == 1)
    {
      fldAssocInode.setAssociativity( i ).setNormalSignL( -1 );
      fldAssocInode.setAssociativity( i ).setNormalSignR( -1 );
      fldAssocInode.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), i );
      fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), i );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Error: integer i should be either odd or even." );
  }

  interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocInode );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

  // boundary-trace field variable

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode0( BasisFunctionNodeBase::P0, 1 );
  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode1( BasisFunctionNodeBase::P0, 1 );

  fldAssocBnode0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBnode1.setAssociativity( 0 ).setRank( comm_rank );

  fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
  fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {ii} );

  fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
  fldAssocBnode1.setAssociativity( 0 ).setNormalSignL( -1 );

  fldAssocBnode0.setGroupLeft( 0 );
  fldAssocBnode0.setElementLeft( 0, 0 );
  fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

  fldAssocBnode1.setGroupLeft( 0 );
  fldAssocBnode1.setElementLeft( ii-1, 0 );
  fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

  // Check grid validity
  checkGrid();
}


// -----------------------------------------------------
void XField2D_Line_X1_1Group::readXFoilGrid(const std::string& filename, ArrayVector& coordinates)
{
  std::ifstream grid(filename);
  int nline = 0;
  DLA::VectorS<2,Real> xy;

  if (grid.is_open())
  {
    while ( grid >> xy[0] >> xy[1] )
    {
      coordinates.push_back(xy);
      nline++;
    }
    grid.close();

    SANS_ASSERT_MSG( nline == (int) coordinates.size(), "nline = %d", "coordinates.size() = %d", nline, (int) coordinates.size() );
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Error: the grid file '" + filename + "' cannot be opened." );
  }
}
}
