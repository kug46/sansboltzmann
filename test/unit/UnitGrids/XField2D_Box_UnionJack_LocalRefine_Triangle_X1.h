// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_BOX_UNIONJACK_LOCALREFINE_TRIANGLE_X1
#define XFIELD2D_BOX_UNIONJACK_LOCALREFINE_TRIANGLE_X1

#include "Field/XFieldArea.h"

namespace SANS
{
// triangle grid in a unit box with 4 sides as separate boundary-edge groups
//
// diagonals oriented as in Union Jack flag
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// area elements in 1 group: 2*ii*jj triangles
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 4 groups: lower, right, upper, left
//
// has an additional flag for crossing the central element, useful for comparing local solves

class XField2D_Box_UnionJack_LocalRefine_Triangle_X1 : public XField<PhysD2,TopoD2>
{
public:
  XField2D_Box_UnionJack_LocalRefine_Triangle_X1( int ii, int jj, bool refine = true,
                                                  Real xmin = 0, Real xmax = 1, Real ymin = 0, Real ymax = 1 );

  static const int iBottom, iRight, iTop, iLeft;
};

}

#endif // XFIELD2D_BOX_UNIONJACK_LOCALREFINE_TRIANGLE_X1
