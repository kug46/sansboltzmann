// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TEST_UNIT_UNITGRIDS_XFIELD2D_LINE_X1_2GROUP_AIRFOILWITHWAKE_H_
#define TEST_UNIT_UNITGRIDS_XFIELD2D_LINE_X1_2GROUP_AIRFOILWITHWAKE_H_

#include "Field/XFieldLine.h"

namespace SANS
{
// Line grid in physically 2D space of airfoil and wake
//
// generate line grid from prescribed nodal coordinates
//  line cells in 2 groups: 0 - airfoil, 1 - wake
//  interior traces in 2 groups: 0 - airfoil interior, 1 - wake interior
//  boundary traces in 3 groups: 0 - airfoil TE, 1 - wake inlet, 2 - wake trailing edge
//  hub trace in 1 group: 0 - wake inlet

// * reference coordinate direction: CCW around the airfoil and then downstream in the wake
// * N and Nw cells on airfoil and wake respectively
//                (1)
//    ---...--- 2 --- 1
//  /                   \ (0)
// |                     0        (0)                  (Nw-1)
// |                            0 --- 1 --- ...--- Nw-1 ---- Nw
// |                     N
//  \                   / (N-1)
//    ---...-- N-2 --- N-1
//                (N-2)
//
//     CellGroup[0]  --->|      |<---     CellGroup[1]
// Note: the node indices here are local to each cell group instead of global

class XField2D_Line_X1_2Group_AirfoilWithWake : public XField<PhysD2,TopoD1>
{
public:
  typedef std::vector<DLA::VectorS<2,Real>> ArrayVector;

  explicit XField2D_Line_X1_2Group_AirfoilWithWake(const ArrayVector& AirfoilCoordinates,
                                                   const ArrayVector& WakeCoordinates);

  static void readXFoilGrid(const std::string& filename,
                            ArrayVector& coordinates);
};

}


#endif /* TEST_UNIT_UNITGRIDS_XFIELD2D_LINE_X1_2GROUP_AIRFOILWITHWAKE_H_ */
