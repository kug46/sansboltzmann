// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_6Tet_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_6Tet_X1_1Group_test )
{
  XField3D_6Tet_X1_1Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 8 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[2], 1 );

  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  const XField3D_6Tet_X1_1Group::FieldCellGroupType<Tet>& xfldVol = xfld.getCellGroup<Tet>(0);

  int nodeMap[4];
  int elemL, elemR;
  int faceL, faceR;
  int faceI;

  // Left prism
  xfldVol.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );

  xfldVol.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );
  BOOST_CHECK_EQUAL( nodeMap[3], 2 );

  xfldVol.associativity(2).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );

  // Right prism
  xfldVol.associativity(3).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  xfldVol.associativity(4).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  xfldVol.associativity(5).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );

  // Index table for each tet in the hex
  const int tets[6][4] = { {0, 1, 2, 4},
                           {1, 4, 3, 2},
                           {6, 2, 3, 4},

                           {1, 4, 5, 3},
                           {4, 6, 5, 3},
                           {7, 6, 3, 5} };

#if 0
  Int3 edgeSign;

  edgeSign = xfldVol.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldVol.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );
#endif
  // interior-face field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField3D_6Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldIface = xfld.getInteriorTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldIface.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIface.getGroupRight(), 0 );
  faceI = 0;

  // interior face-to-cell connectivity

  // Two faces inside the left prism
  elemL = 0; faceL = 0;
  elemR = 1; faceR = 2;

  xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
  BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], xfldIface.getCanonicalTraceRight(faceI).orientation );
  faceI++;

  elemL = 1; faceL = 0;
  elemR = 2; faceR = 0;

  xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
  BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], xfldIface.getCanonicalTraceRight(faceI).orientation );
  faceI++;


  //Two faces connecting the prisms
  elemL = 1; faceL = 3;
  elemR = 3; faceR = 2;

  xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
  BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], xfldIface.getCanonicalTraceRight(faceI).orientation );
  faceI++;

  elemL = 2; faceL = 1;
  elemR = 4; faceR = 2;

  xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
  BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], xfldIface.getCanonicalTraceRight(faceI).orientation );
  faceI++;


  //Two faces in the right prism
  elemL = 3; faceL = 0;
  elemR = 4; faceR = 1;

  xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
  BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], xfldIface.getCanonicalTraceRight(faceI).orientation );
  faceI++;

  elemL = 4; faceL = 0;
  elemR = 5; faceR = 0;

  xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
  BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], xfldIface.getCanonicalTraceRight(faceI).orientation );
  faceI++;

  BOOST_REQUIRE_EQUAL( 6, faceI );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField3D_6Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBface = xfld.getBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldBface.getGroupLeft(), 0 );

  int faceB = 0;

  elemL = 0; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 0; faceL = 2;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 0; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 1; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 2; faceL = 2;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 2; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 3; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 3; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 4; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 5; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 5; faceL = 2;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 5; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  BOOST_REQUIRE_EQUAL( 12, faceB );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
