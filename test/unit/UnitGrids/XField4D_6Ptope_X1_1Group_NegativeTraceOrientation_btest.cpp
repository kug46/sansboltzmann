// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/linspace.h"

#include "XField4D_6Ptope_X1_1Group_NegativeTraceOrientation.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

BOOST_AUTO_TEST_CASE( XField4D_6Pentatope1Group_WithTraceOrientation_test )
{
  mpi::communicator world;

  int orient = -1;
  XField4D_6Ptope_X1_1Group_NegativeTraceOrientation xfld(world);

  // loop through the interior traces and ensure the orientations are in the correct direction
  const XField4D_6Ptope_X1_1Group_NegativeTraceOrientation::FieldCellGroupType<Pentatope>& xfldCell = xfld.getCellGroup<Pentatope>(0);
  const XField4D_6Ptope_X1_1Group_NegativeTraceOrientation::FieldTraceGroupType<Tet>& xfldIface = xfld.getInteriorTraceGroup<Tet>(0);

  int faceNodes[4],elemNodes[5];
  for (int faceI=0;faceI<xfldIface.nElem();faceI++)
  {
    // get the vertices of the interior trace element
    xfldIface.associativity(faceI).getNodeGlobalMapping( faceNodes , 4 );

    int elemL = xfldIface.getElementLeft(faceI);
    int elemR = xfldIface.getElementRight(faceI);
    int faceL = xfldIface.getCanonicalTraceLeft(faceI).trace;
    int faceR = xfldIface.getCanonicalTraceRight(faceI).trace;

    // the left element should be 0
    BOOST_CHECK_EQUAL( elemL , 0 );
    BOOST_CHECK_EQUAL( elemR , faceI+1 );

    // get the vertices of the left cell and check the orientation of the trace is +1
    xfldCell.associativity(elemL).getNodeGlobalMapping( elemNodes , 5 );
    CanonicalTraceToCell traceL = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes,4,elemNodes,5);
    BOOST_CHECK_EQUAL( traceL.orientation, 1);

    // get the vertice of the right cell and check the orientation of the trace is orient
    xfldCell.associativity(elemR).getNodeGlobalMapping( elemNodes , 5 );
    CanonicalTraceToCell traceR = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes,4,elemNodes,5);
    BOOST_CHECK_EQUAL( traceR.orientation, orient);

    BOOST_CHECK_EQUAL( faceI , faceL );
    BOOST_CHECK_EQUAL( 4 , faceR );

  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField4D_24Pentatope1Group_test )
{
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
