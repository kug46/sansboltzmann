// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "tools/SANSException.h"
#include <fstream>

#include "XField2D_Line_X1_2Group_AirfoilWithWake.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

// -----------------------------------------------------
XField2D_Line_X1_2Group_AirfoilWithWake::
XField2D_Line_X1_2Group_AirfoilWithWake(const ArrayVector& AirfoilCoordinates,
                                        const ArrayVector& WakeCoordinates)
{
  int comm_rank = comm_->rank();

  // count node/element
  const int nnode_a = (int) AirfoilCoordinates.size();
  const int nnode_w = (int) WakeCoordinates.size();
  const int nnode = nnode_a + nnode_w;

  const int nelem_a = nnode_a - 1;
  const int nelem_w = nnode_w - 1;

  SANS_ASSERT_MSG( nelem_a >= 2, "This grid must have at least two elements." );
  SANS_ASSERT_MSG( nelem_w >= 1, "This grid must have at least one elements." );

  // Create the DOF arrays
  resizeDOF( nnode_a + nnode_w );

  // Create groups
  resizeCellGroups(2);
  resizeInteriorTraceGroups(2);
  resizeBoundaryTraceGroups(3);
  resizeHubTraceGroups(1);

  // Assign DOFs with input grid coordinates
  for (int i = 0; i < nnode_a; i++)
    DOF(i) = AirfoilCoordinates[i];

  for (int i = 0; i < nnode_w; i++)
    DOF(i+nnode_a) = WakeCoordinates[i];

  // ------------------------------------------------ //
  // Set associativity for cell group
  // ------------------------------------------------ //
  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell_a( BasisFunctionLineBase::HierarchicalP1, nelem_a );
  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell_w( BasisFunctionLineBase::HierarchicalP1, nelem_w );

  for (int i = 0; i < nelem_a; i++)
  {
    fldAssocCell_a.setAssociativity( i ).setRank( comm_rank );
    fldAssocCell_a.setAssociativity( i ).setNodeGlobalMapping( {i, i+1} );
  }

  for (int i = 0; i < nelem_w; i++)
  {
    fldAssocCell_w.setAssociativity( i ).setRank( comm_rank );
    fldAssocCell_w.setAssociativity( i ).setNodeGlobalMapping( {i+nnode_a, i+1+nnode_a} );
  }

  cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell_a );
  cellGroups_[0]->setDOF(DOF_, nDOF_);  // TODO:?

  cellGroups_[1] = new FieldCellGroupType<Line>( fldAssocCell_w );
  cellGroups_[1]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell_a.nElem() + fldAssocCell_w.nElem();

  // ------------------------------------------------ //
  // Set associativity for interior trace group
  // ------------------------------------------------ //
  { // interior trace group 0: airfoil interior nodes
    const int nnode_group = nnode_a - 2;
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode_aI( BasisFunctionNodeBase::P0, nnode_group );

    fldAssocInode_aI.setGroupLeft( 0 );
    fldAssocInode_aI.setGroupRight( 0 );

    for (int i = 0; i < nnode_group; i++)
    {
      fldAssocInode_aI.setAssociativity( i ).setRank( comm_rank );

      fldAssocInode_aI.setAssociativity( i ).setNodeGlobalMapping( {i+1} );

      fldAssocInode_aI.setElementLeft( i, i );
      fldAssocInode_aI.setElementRight( i+1, i );

      fldAssocInode_aI.setAssociativity( i ).setNormalSignL(  1 );
      fldAssocInode_aI.setAssociativity( i ).setNormalSignR( -1 );

      fldAssocInode_aI.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), i );
      fldAssocInode_aI.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), i );
    }

    interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocInode_aI );
    interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);
  }

  { // interior trace group 1: wake interior nodes
    const int nnode_group = nnode_w - 2;
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode_wI( BasisFunctionNodeBase::P0, nnode_group );

    fldAssocInode_wI.setGroupLeft( 1 );
    fldAssocInode_wI.setGroupRight( 1 );

    for (int i = 0; i < nnode_group; i++)
    {
      fldAssocInode_wI.setAssociativity( i ).setRank( comm_rank );

      fldAssocInode_wI.setAssociativity( i ).setNodeGlobalMapping( {i+1+nnode_a} );

      fldAssocInode_wI.setElementLeft( i, i );
      fldAssocInode_wI.setElementRight( i+1, i );

      fldAssocInode_wI.setAssociativity( i ).setNormalSignL(  1 );
      fldAssocInode_wI.setAssociativity( i ).setNormalSignR( -1 );

      fldAssocInode_wI.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), i );
      fldAssocInode_wI.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), i );
    }

    interiorTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocInode_wI );
    interiorTraceGroups_[1]->setDOF(DOF_, nDOF_);
  }

  // ------------------------------------------------ //
  // Set associativity for boundary trace group
  // ------------------------------------------------ //
  { // boundary trace group 0: airfoil TE nodes
    const int nnode_group = 2; // # of airfoil TE nodes
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode_aTE( BasisFunctionNodeBase::P0, nnode_group );

    fldAssocBnode_aTE.setGroupLeft( 0 );
    fldAssocBnode_aTE.setGroupRight( 0 );
    fldAssocBnode_aTE.setGroupRightType( eHubTraceGroup );

    int inode; // node index in this group

    // upper TE node
    inode = 0;
    fldAssocBnode_aTE.setAssociativity( inode ).setRank( comm_rank );
    fldAssocBnode_aTE.setAssociativity( inode ).setNodeGlobalMapping( {0} );

    fldAssocBnode_aTE.setElementLeft( 0, inode );
    fldAssocBnode_aTE.setElementRight( 0, inode );

    fldAssocBnode_aTE.setAssociativity( inode ).setNormalSignL( -1 );

    fldAssocBnode_aTE.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), inode );

    // lower TE node
    inode = 1;
    fldAssocBnode_aTE.setAssociativity( inode ).setRank( comm_rank );
    fldAssocBnode_aTE.setAssociativity( inode ).setNodeGlobalMapping( {nnode_a-1} );

    fldAssocBnode_aTE.setElementLeft( nelem_a-1, inode );
    fldAssocBnode_aTE.setElementRight( 0, inode );

    fldAssocBnode_aTE.setAssociativity( inode ).setNormalSignL( 1 );

    fldAssocBnode_aTE.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), inode );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBnode_aTE );
    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  }

  { // boundary trace group 1: Wake inflow boundary
    const int nnode_group = 1;
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnodeW( BasisFunctionNodeBase::P0, nnode_group );

    fldAssocBnodeW.setAssociativity( 0 ).setRank( comm_rank );
    fldAssocBnodeW.setAssociativity( 0 ).setNodeGlobalMapping( {nnode_a} );

    fldAssocBnodeW.setAssociativity( 0 ).setNormalSignL( -1 );

    fldAssocBnodeW.setGroupLeft( 1 );
    fldAssocBnodeW.setElementLeft( 0, 0 );
    fldAssocBnodeW.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

    fldAssocBnodeW.setGroupRight( 0 );
    fldAssocBnodeW.setGroupRightType( eHubTraceGroup );
    fldAssocBnodeW.setElementRight( 0, 0 );

    boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnodeW );
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  }

  { // boundary trace group 2: Wake farfield boundary
    const int nnode_group = 1;
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode_ff( BasisFunctionNodeBase::P0, nnode_group );

    fldAssocBnode_ff.setAssociativity( 0 ).setRank( comm_rank );
    fldAssocBnode_ff.setAssociativity( 0 ).setNodeGlobalMapping( {nnode-1} );

    fldAssocBnode_ff.setAssociativity( 0 ).setNormalSignL( 1 );

    fldAssocBnode_ff.setGroupLeft( 1 );
    fldAssocBnode_ff.setElementLeft( nelem_w-1, 0 );
    fldAssocBnode_ff.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

    boundaryTraceGroups_[2] = new FieldTraceGroupType<Node>( fldAssocBnode_ff );
    boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);
  }

  // ------------------------------------------------ //
  // Set associativity for hub trace group
  // ------------------------------------------------ //
  { // hub trace group 0: hub trace between TE and wake
    const int nnode_group = 1;
    FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocHnode(BasisFunctionNodeBase::P0, nnode_group);

    const int inode = 0;
    fldAssocHnode.setAssociativity( inode ).setRank( comm_rank );
    fldAssocHnode.setAssociativity( inode ).setNodeGlobalMapping( {nnode_a} ); // coincides with first wake node

    hubTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocHnode );
    hubTraceGroups_[0]->setDOF(DOF_, nDOF_);
  }

  // Check grid validity
  checkGrid();
}

} // namespace SANS
