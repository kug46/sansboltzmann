// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_BOXPERIODIC_TRIANGLE_X1
#define XFIELD2D_BOXPERIODIC_TRIANGLE_X1

#include "Field/XFieldArea.h"

namespace SANS
{
// triangle grid in a unit box with periodic connectivity (i.e. no boundary edges)
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// both area and edge elements in single group
//
// Note: this can be used for testing interior operators

class XField2D_BoxPeriodic_Triangle_X1 : public XField<PhysD2,TopoD2>
{
public:
  XField2D_BoxPeriodic_Triangle_X1( int ii, int jj, Real xmin = 0, Real xmax = 1, Real ymin = 0, Real ymax = 1 );
};

}

#endif // XFIELD2D_BOXPERIODIC_TRIANGLE_X1
