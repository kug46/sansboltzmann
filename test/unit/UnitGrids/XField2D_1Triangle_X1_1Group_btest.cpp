// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_1Triangle_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_1Triangle_X1_1Group.h"
#include "XField2D_1Triangle_GhostBoundary_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Quadrature/Quadrature.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_1Triangle_X1_1Group_test )
{
  typedef std::array<int,3> Int3;

  XField2D_1Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 3 );

  BOOST_CHECK_THROW( xfld.nDOFCellGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFInteriorTraceGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFBoundaryTraceGroup(0), DeveloperException );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField2D_1Triangle_X1_1Group::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);

  int nodeMap[3];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  Int3 edgeSign;

  edgeSign = xfldArea.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 3 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField2D_1Triangle_X1_1Group::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  const XField2D_1Triangle_X1_1Group::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  const XField2D_1Triangle_X1_1Group::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);

  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBedge0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).orientation, 1 );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_1Triangle_GhostBoundary_X1_1Group_test )
{
  typedef typename DLA::VectorS<2,Real> VectorX;

  XField2D_1Triangle_X1_1Group xfld;
  XField2D_1Triangle_GhostBoundary_X1_1Group xfld_ghost;
  BOOST_CHECK_EQUAL( xfld_ghost.nDOF(), 3 );

  // area field variable
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  typedef XField2D_1Triangle_X1_1Group::FieldCellGroupType<Triangle> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  //Get order 4 quad points
  Quadrature<TopoD2, Triangle> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType RefCoord;  // reference-element coordinates
  VectorX qeval, qeval_ghost;

  const Real tol = 1e-11;

  const FieldCellGroupType& xfld_cellgrp = xfld.getCellGroup<Triangle>(0);
  ElementFieldClass fldElem( xfld_cellgrp.basis() );
  xfld_cellgrp.getElement(fldElem, 0);

  const FieldCellGroupType& xfld_ghost_cellgrp = xfld_ghost.getCellGroup<Triangle>(0);
  ElementFieldClass fldElem_ghost( xfld_ghost_cellgrp.basis() );
  xfld_ghost_cellgrp.getElement(fldElem_ghost, 0);

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, RefCoord );

    fldElem_ghost.coordinates ( RefCoord, qeval_ghost);

    fldElem.coordinates(RefCoord, qeval);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_ghost[i],  qeval[i], tol, tol);
    }
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
