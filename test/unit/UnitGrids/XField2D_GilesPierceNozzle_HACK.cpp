// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_GilesPierceNozzle_HACK.h"

#include "Field/Partition/XField_Lagrange.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

XField2D_GilesPierceNozzle_HACK::
XField2D_GilesPierceNozzle_HACK( mpi::communicator comm, int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax, bool curved )
: XField<PhysD2,TopoD2>(comm)
  {
  Real scale = .01;
  if (curved)
  {
    XField_Lagrange<PhysD2>::VectorX X;

    int order = 3;
    int nnode = (ii*order + 1)*(jj*order + 1);
    int nCell = ii*jj*2;//ii*jj

    XField_Lagrange<PhysD2> xfldin(comm);

    // create the  grid-coordinate DOF arrays
    xfldin.sizeDOF(nnode);

    if (comm.rank() == 0)
    {
      for (int j = 0; j < (jj*order + 1); j++)
      {
        for (int i = 0; i < (ii*order + 1); i++)
        {
          X[0] = xmin + (xmax - xmin)*i/Real(ii*order);
          if (X[0]<=-0.5)
            ymax = scale*2;
          else if (X[0]<0.5)
            ymax = scale*(1 + pow(sin(PI*X[0]),2));
          else
            ymax = scale*2;
          X[1] = ymin + (ymax - ymin)*j/Real(jj*order);
          xfldin.addDOF(X);
        }
      }
    }

    // Start the process of adding cells
    xfldin.sizeCells(nCell);

    if (comm.rank() == 0)
    {
      int group = 0;
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          /* Node indexes in the grid



         12 -13 -14 -15
          |           |
          8   9  10  11
          |           |
          4   5   6   7
          |           |
          0 - 1 - 2 - 3
           */

          /* SANS canonical Lagrange node indexes for an element

                3---9----8---2
                |            |
               10  15   14   7
                |            |
               11  12   13   6
                |            |
                0---4----5---1

           */
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

          int n0 = m0 + 0;
          int n1 = m0 + 1;
          int n2 = m0 + 2;
          int n3 = m0 + 3;

          int m1 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);

          int n4 = m1 + 0;
          int n5 = m1 + 1;
          int n6 = m1 + 2;
          int n7 = m1 + 3;

          int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

          int n8  = m2 + 0;
          int n9  = m2 + 1;
          int n10 = m2 + 2;
          int n11 = m2 + 3;

          int m3 = (j*order)*(ii*order + 1) + i*order + 3*(ii*order + 1);

          int n12 = m3 + 0;
          int n13 = m3 + 1;
          int n14 = m3 + 2;
          int n15 = m3 + 3;

//          std::vector<int> quads = {n0, n3, n15, n12, n1, n2, n7, n11, n14, n13,
//                                    n8, n4, n5, n6, n10, n9};
//
//
//          // add the indices to the group
//          xfldin.addCell(group, eQuad, order, quads);
          std::vector<int> triL = {n0, n3, n12, // nodes
                                   n6, n9,      // edge 1
                                   n8, n4,      // edge 2
                                   n1, n2,      // edge 3
                                   n5};         // cell

          std::vector<int> triR = {n15, n12, n3, // nodes
                                   n9, n6,       // edge 1
                                   n7, n11,      // edge 2
                                   n14, n13,     // edge 3
                                   n10};         // cell

          // add the indices to the group
          xfldin.addCell(group, eTriangle, order, triL);
          xfldin.addCell(group, eTriangle, order, triR);

        }
      }
    }

    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(2*ii + 2*jj);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      // lower boundary
      {
        int group = 0;

        int j = 0;
        for (int i = 0; i < ii; i++)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

          int n0 = m0 + 0;
          int n3 = m0 + 3;

          xfldin.addBoundaryTrace( group, eLine, {n0, n3} );
        }
      }

      // right boundary
      {
        int group = 1;

        int i = ii-1;
        for (int j = 0; j < jj; j++)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n3 = m0 + 3;

          int m3 = (j*order)*(ii*order + 1) + i*order + 3*(ii*order + 1);
          int n15 = m3 + 3;

          xfldin.addBoundaryTrace( group, eLine, {n3, n15} );
        }
      }

      // upper boundary
      {
        int group = 2;

        int j = jj - 1;
        for (int i = ii-1; i >= 0; i--)
        {
          int m3 = (j*order)*(ii*order + 1) + i*order + 3*(ii*order + 1);

          int n12 = m3 + 0;
          int n15 = m3 + 3;

          xfldin.addBoundaryTrace( group, eLine, {n15, n12} );
        }
      }

      // left boundary
      {
        int group = 3;

        int i = 0;
        for (int j = jj-1; j >= 0; j--)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n0 = m0 + 0;

          int m3 = (j*order)*(ii*order + 1) + i*order + 3*(ii*order + 1);
          int n12 = m3 + 0;

          xfldin.addBoundaryTrace( group, eLine, {n12, n0} );
        }
      }
    }

    this->buildFrom( xfldin );
  }
  else
  {
    XField_Lagrange<PhysD2>::VectorX X;

    int order = 1;
    int nnode = (ii*order + 1)*(jj*order + 1);
    int nCell = ii*jj*2;//ii*jj

    XField_Lagrange<PhysD2> xfldin(comm);

    // create the wavy grid-coordinate DOF arrays
    xfldin.sizeDOF( nnode );
    if (comm.rank() == 0)
      for (int j = 0; j < (jj*order + 1); j++)
      {
        for (int i = 0; i < (ii*order + 1); i++)
        {
          X[0] = xmin + (xmax - xmin)*i/Real(ii*order);
          if (X[0]<=-0.5)
            ymax = scale*2;
          else if (X[0]<0.5)
            ymax = scale*(1 + pow(sin(PI*X[0]),2));
          else
            ymax = scale*2;
          X[1] = ymin + (ymax - ymin)*j/Real(jj*order);
          xfldin.addDOF(X);
        }
      }
    // Start the process of adding cells
    xfldin.sizeCells(nCell);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      int group = 0;
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          /* Node indexes in the grid

                  2-----------3
                  |           |
                  |           |
                  |           |
                  |           |
                  |           |
                  0-----------1
           */
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n1 = n0 + 1;
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
          int n3 = n2 + 1;

//          std::vector<int> quads = {n0, n1, n3, n2};
//          // add the indices to the group
//          xfldin.addCell(group, eQuad, order, quads);

          std::vector<int> triL = {n0, n1, n2}; // nodes
          std::vector<int> triR = {n3, n2, n1}; // nodes

          // add the indices to the group
          xfldin.addCell(group, eTriangle, order, triL);
          xfldin.addCell(group, eTriangle, order, triR);
        }
      }
    }
    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(2*ii + 2*jj);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      // lower boundary
      {
        int group = 0;

        int j = 0;
        for (int i = 0; i < ii; i++)
        {
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n1 = n0 + 1;

          xfldin.addBoundaryTrace( group, eLine, {n0, n1} );
        }
      }

      // right boundary
      {
        int group = 1;

        int i = ii-1;
        for (int j = 0; j < jj; j++)
        {
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n1 = n0 + 1;
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
          int n3 = n2 + 1;

          xfldin.addBoundaryTrace( group, eLine, {n1, n3} );
        }
      }

      // upper boundary
      {
        int group = 2;

        int j = jj - 1;
        for (int i = ii-1; i >= 0; i--)
        {
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
          int n3 = n2 + 1;

          xfldin.addBoundaryTrace( group, eLine, {n3, n2} );
        }
      }

      // left boundary
      {
        int group = 3;

        int i = 0;
        for (int j = jj-1; j >= 0; j--)
        {
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);

          xfldin.addBoundaryTrace( group, eLine, {n2, n0} );
        }
      }
    }

    this->buildFrom( xfldin );

  }
  }
}
