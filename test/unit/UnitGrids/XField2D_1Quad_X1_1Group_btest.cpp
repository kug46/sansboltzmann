// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_1Quad_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_1Quad_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_1Quad_X1_1Group_test )
{
  typedef std::array<int,4> Int4;

  XField2D_1Quad_X1_1Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 4 );

  BOOST_CHECK_THROW( xfld.nDOFCellGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFInteriorTraceGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFBoundaryTraceGroup(0), DeveloperException );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  const XField2D_1Quad_X1_1Group::FieldCellGroupType<Quad>& xfldArea = xfld.getCellGroup<Quad>(0);

  int nodeMap[4];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  Int4 edgeSign;

  edgeSign = xfldArea.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );
  BOOST_CHECK_EQUAL( edgeSign[3], +1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(3).topoTypeID() == typeid(Line) );

  const XField2D_1Quad_X1_1Group::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  const XField2D_1Quad_X1_1Group::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  const XField2D_1Quad_X1_1Group::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);
  const XField2D_1Quad_X1_1Group::FieldTraceGroupType<Line>& xfldBedge3 = xfld.getBoundaryTraceGroup<Line>(3);

  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBedge0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceLeft(0).trace, 3 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceLeft(0).orientation, 1 );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
