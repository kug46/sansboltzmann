// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField3D_2Hex_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
/*
//  y
//  ^
//  |
//  |
//  +-----> x
//   \
//    \
//     z
//
//  3----------4----------5
//  |\         |\         |\
//  | \        | \        | \
//  |  \       |  \       |  \
//  |   9------+--10------+---11
//  |   |      |   |      |   |
//  0---+------1---+------2   |
//   \  |       \  |       \  |
//    \ |  (0)   \ |  (1)   \ |
//     \|         \|         \|
//      6----------7----------8
*/

XField3D_2Hex_X1_1Group::XField3D_2Hex_X1_1Group()
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(12);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(6);
  resizeCellGroups(1);

  // nodal coordinates for the two Quads.
  DOF(0) = { 0, 0, 0};
  DOF(1) = { 1, 0, 0};
  DOF(2) = { 2, 0, 0};

  DOF(3) = { 0, 1, 0};
  DOF(4) = { 1, 1, 0};
  DOF(5) = { 2, 1, 0};

  DOF(6) = { 0, 0, 1};
  DOF(7) = { 1, 0, 1};
  DOF(8) = { 2, 0, 1};

  DOF(9)  = { 0, 1, 1};
  DOF(10) = { 1, 1, 1};
  DOF(11) = { 2, 1, 1};

  // volume field variable
  FieldCellGroupType<Hex>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Hex>::HierarchicalP1, 2 );

  int cell0[8] = {0, 1, 4, 3,   6, 7, 10, 9 };
  int cell1[8] = {1, 2, 5, 4,   7, 8, 11, 10};

  // set the cell processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 1 ).setRank( comm_rank );

  //element volume associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( cell0, 8 );
  fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( cell1, 8 );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 2 );
  fldAssocCell.setAssociativity( 1 ).setFaceSign( -1, 4 );

  cellGroups_[0] = new FieldCellGroupType<Hex>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // interior-edge field variable

  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocIface( BasisFunctionAreaBase<Quad>::HierarchicalP1, 1 );


  int canonicalNodesL[4];
  CanonicalTraceToCell canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {1, 4, 10, 7}, cell0, 8, canonicalNodesL, 4 );
  CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(canonicalNodesL, 4, cell1, 8 );

  fldAssocIface.setAssociativity( 0 ).setRank( comm_rank );

  fldAssocIface.setAssociativity( 0 ).setNodeGlobalMapping( canonicalNodesL, 4 );

  fldAssocIface.setGroupLeft( 0 );
  fldAssocIface.setGroupRight( 0 );

  fldAssocIface.setElementLeft( 0, 0 );
  fldAssocIface.setElementRight( 1, 0 );

  fldAssocIface.setCanonicalTraceLeft( canonicalL, 0 );
  fldAssocIface.setCanonicalTraceRight( canonicalR, 0 );

  interiorTraceGroups_[0] = new FieldTraceGroupType<Quad>( fldAssocIface );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

  // boundary-edge field variable

  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface0( BasisFunctionAreaBase<Quad>::HierarchicalP1, 1 ); // x-min
  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface1( BasisFunctionAreaBase<Quad>::HierarchicalP1, 1 ); // x-max
  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface2( BasisFunctionAreaBase<Quad>::HierarchicalP1, 2 ); // y-min
  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface3( BasisFunctionAreaBase<Quad>::HierarchicalP1, 2 ); // y-max
  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface4( BasisFunctionAreaBase<Quad>::HierarchicalP1, 2 ); // z-min
  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface5( BasisFunctionAreaBase<Quad>::HierarchicalP1, 2 ); // z-max

  // Set the groups
  fldAssocBface0.setGroupLeft( 0 );
  fldAssocBface1.setGroupLeft( 0 );
  fldAssocBface2.setGroupLeft( 0 );
  fldAssocBface3.setGroupLeft( 0 );
  fldAssocBface4.setGroupLeft( 0 );
  fldAssocBface5.setGroupLeft( 0 );

  // Set the BC elements
  // x-min
  canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {0, 6, 9, 3}, cell0, 8, canonicalNodesL, 4 );

  fldAssocBface0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBface0.setAssociativity( 0 ).setNodeGlobalMapping( canonicalNodesL, 4 );
  fldAssocBface0.setElementLeft( 0, 0 );
  fldAssocBface0.setCanonicalTraceLeft( canonicalL, 0 );


  // x-max
  canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {2, 8, 11, 5}, cell1, 8, canonicalNodesL, 4 );

  fldAssocBface1.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBface1.setAssociativity( 0 ).setNodeGlobalMapping( canonicalNodesL, 4 );
  fldAssocBface1.setElementLeft( 1, 0 );
  fldAssocBface1.setCanonicalTraceLeft( canonicalL, 0 );


  // y-min
  canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {0, 1, 6, 7}, cell0, 8, canonicalNodesL, 4 );

  fldAssocBface2.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBface2.setAssociativity( 0 ).setNodeGlobalMapping( canonicalNodesL, 4 );
  fldAssocBface2.setElementLeft( 0, 0 );
  fldAssocBface2.setCanonicalTraceLeft( canonicalL, 0 );

  canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {1, 2, 7, 8}, cell1, 8, canonicalNodesL, 4 );

  fldAssocBface2.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocBface2.setAssociativity( 1 ).setNodeGlobalMapping( canonicalNodesL, 4 );
  fldAssocBface2.setElementLeft( 1, 1 );
  fldAssocBface2.setCanonicalTraceLeft( canonicalL, 1 );


  // y-max
  canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {3, 4, 9, 10}, cell0, 8, canonicalNodesL, 4 );

  fldAssocBface3.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBface3.setAssociativity( 0 ).setNodeGlobalMapping( canonicalNodesL, 4 );
  fldAssocBface3.setElementLeft( 0, 0 );
  fldAssocBface3.setCanonicalTraceLeft( canonicalL, 0 );

  canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {4, 5, 10, 11}, cell1, 8, canonicalNodesL, 4 );

  fldAssocBface3.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocBface3.setAssociativity( 1 ).setNodeGlobalMapping( canonicalNodesL, 4 );
  fldAssocBface3.setElementLeft( 1, 1 );
  fldAssocBface3.setCanonicalTraceLeft( canonicalL, 1 );


  // z-min
  canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {0, 1, 3, 4}, cell0, 8, canonicalNodesL, 4 );

  fldAssocBface4.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBface4.setAssociativity( 0 ).setNodeGlobalMapping( canonicalNodesL, 4 );
  fldAssocBface4.setElementLeft( 0, 0 );
  fldAssocBface4.setCanonicalTraceLeft( canonicalL, 0 );

  canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {1, 2, 4, 5}, cell1, 8, canonicalNodesL, 4 );

  fldAssocBface4.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocBface4.setAssociativity( 1 ).setNodeGlobalMapping( canonicalNodesL, 4 );
  fldAssocBface4.setElementLeft( 1, 1 );
  fldAssocBface4.setCanonicalTraceLeft( canonicalL, 1 );


  // z-max
  canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {6, 7, 10, 9}, cell0, 8, canonicalNodesL, 4 );

  fldAssocBface5.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBface5.setAssociativity( 0 ).setNodeGlobalMapping( canonicalNodesL, 4 );
  fldAssocBface5.setElementLeft( 0, 0 );
  fldAssocBface5.setCanonicalTraceLeft( canonicalL, 0 );

  canonicalL = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTraceLeft( {7, 8, 10, 11}, cell1, 8, canonicalNodesL, 4 );

  fldAssocBface5.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocBface5.setAssociativity( 1 ).setNodeGlobalMapping( canonicalNodesL, 4 );
  fldAssocBface5.setElementLeft( 1, 1 );
  fldAssocBface5.setCanonicalTraceLeft( canonicalL, 1 );


  boundaryTraceGroups_[0] = new FieldTraceGroupType<Quad>( fldAssocBface0 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Quad>( fldAssocBface1 );
  boundaryTraceGroups_[2] = new FieldTraceGroupType<Quad>( fldAssocBface2 );
  boundaryTraceGroups_[3] = new FieldTraceGroupType<Quad>( fldAssocBface3 );
  boundaryTraceGroups_[4] = new FieldTraceGroupType<Quad>( fldAssocBface4 );
  boundaryTraceGroups_[5] = new FieldTraceGroupType<Quad>( fldAssocBface5 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[3]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[4]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[5]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
