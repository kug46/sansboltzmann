// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_4QUAD_X1_1GROUP
#define XFIELD2D_4QUAD_X1_1GROUP

#include "Field/XFieldArea.h"

namespace SANS
{
// A unit grid that consists of four quads within a single group
//
/*
  6-------7-------8
  |       |       |
  |  (2)  |  (3)  |
  |       |       |
  3-------4-------5
  |       |       |
  |  (0)  |  (1)  |
  |       |       |
  0 ------1-------2
*/

class XField2D_4Quad_X1_1Group : public XField<PhysD2,TopoD2>
{
public:
  XField2D_4Quad_X1_1Group();
};

}

#endif //XFIELD2D_4QUAD_X1_1GROUP
