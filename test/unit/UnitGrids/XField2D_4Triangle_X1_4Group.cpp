// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_4Triangle_X1_4Group.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//  4 ----- 2 ----- 3
//   \      |\      |
//    \ (2) | \ (1) |
//     \    |  \    |
//      \   |   \   |
//       \  |    \  |
//        \ | (0) \ |
//         \|      \|
//          0 ----- 1
//           \      |
//            \ (3) |
//             \    |
//              \   |
//               \  |
//                \ |
//                  5

XField2D_4Triangle_X1_4Group::XField2D_4Triangle_X1_4Group(BasisFunctionCategory basis_category)
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(6);

  //Create the element groups
  resizeInteriorTraceGroups(3);
  resizeBoundaryTraceGroups(3);
  resizeCellGroups(4);

  // nodal coordinates for the four triangles. Nodes==DOF for Hierarchical polynomials.
  DOF(0) = {  0,  0 };
  DOF(1) = {  1,  0 };
  DOF(2) = {  0,  1 };
  DOF(3) = {  1,  1 };
  DOF(4) = { -1,  1 };
  DOF(5) = {  1, -1 };

  // area field variable
  const BasisFunctionAreaBase<Triangle>* cell_basis = BasisFunctionAreaBase<Triangle>::getBasisFunction(1, basis_category);
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell0( cell_basis, 1 );
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell1( cell_basis, 1 );
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell2( cell_basis, 1 );
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell3( cell_basis, 1 );

  // set the cell processor rank
  fldAssocCell0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell1.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell2.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell3.setAssociativity( 0 ).setRank( comm_rank );

  //element area DOF associativity
  fldAssocCell0.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );
  fldAssocCell1.setAssociativity( 0 ).setNodeGlobalMapping( {3, 2, 1} );
  fldAssocCell2.setAssociativity( 0 ).setNodeGlobalMapping( {2, 4, 0} );
  fldAssocCell3.setAssociativity( 0 ).setNodeGlobalMapping( {1, 0, 5} );

  // edge signs for elements (L is +, R is -)
  fldAssocCell0.setAssociativity( 0 ).setEdgeSign( +1, 0 );
  fldAssocCell1.setAssociativity( 0 ).setEdgeSign( -1, 0 );
  fldAssocCell0.setAssociativity( 0 ).setEdgeSign( +1, 1 );
  fldAssocCell2.setAssociativity( 0 ).setEdgeSign( -1, 1 );
  fldAssocCell0.setAssociativity( 0 ).setEdgeSign( +1, 2 );
  fldAssocCell3.setAssociativity( 0 ).setEdgeSign( -1, 2 );

  cellGroups_[0] = new FieldCellGroupType<Triangle>( fldAssocCell0 );
  cellGroups_[1] = new FieldCellGroupType<Triangle>( fldAssocCell1 );
  cellGroups_[2] = new FieldCellGroupType<Triangle>( fldAssocCell2 );
  cellGroups_[3] = new FieldCellGroupType<Triangle>( fldAssocCell3 );

  for (int i = 0; i < 4; i ++)
    cellGroups_[i]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell0.nElem() + fldAssocCell1.nElem() + fldAssocCell2.nElem() + fldAssocCell3.nElem();

  // interior-edge field variable
  // Labeled for group they attach to
  const BasisFunctionLineBase* trace_basis = BasisFunctionLineBase::getBasisFunction(1, basis_category);
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocIedge1( trace_basis, 1 );
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocIedge2( trace_basis, 1 );
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocIedge3( trace_basis, 1 );

  // edge-element processor rank
  fldAssocIedge1.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocIedge2.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocIedge3.setAssociativity( 0 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocIedge1.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );
  fldAssocIedge2.setAssociativity( 0 ).setNodeGlobalMapping( {2, 0} );
  fldAssocIedge3.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );

  // edge-to-cell connectivity
  fldAssocIedge1.setGroupLeft( 0 );
  fldAssocIedge1.setGroupRight( 1 );
  fldAssocIedge2.setGroupLeft( 0 );
  fldAssocIedge2.setGroupRight( 2 );
  fldAssocIedge3.setGroupLeft( 0 );
  fldAssocIedge3.setGroupRight( 3 );

  fldAssocIedge1.setElementLeft( 0, 0 );
  fldAssocIedge1.setElementRight( 0, 0 );
  fldAssocIedge1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocIedge1.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), 0 );

  fldAssocIedge2.setElementLeft( 0, 0 );
  fldAssocIedge2.setElementRight( 0, 0 );
  fldAssocIedge2.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );
  fldAssocIedge2.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), 0 );

  fldAssocIedge3.setElementLeft( 0, 0 );
  fldAssocIedge3.setElementRight( 0, 0 );
  fldAssocIedge3.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
  fldAssocIedge3.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), 0 );

  interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocIedge1 );
  interiorTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocIedge2 );
  interiorTraceGroups_[2] = new FieldTraceGroupType<Line>( fldAssocIedge3 );

  for ( int i = 0; i < 3; i++)
    interiorTraceGroups_[i]->setDOF(DOF_, nDOF_);

  // boundary-edge field variable
  // Labeled for the group they're attached to
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge1( trace_basis, 2 );
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge2( trace_basis, 2 );
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge3( trace_basis, 2 );

  // boundary edge processor ranks
  fldAssocBedge1.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBedge1.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocBedge2.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBedge2.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocBedge3.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBedge3.setAssociativity( 1 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocBedge1.setAssociativity( 0 ).setNodeGlobalMapping( {1, 3} );
  fldAssocBedge1.setAssociativity( 1 ).setNodeGlobalMapping( {3, 2} );
  fldAssocBedge2.setAssociativity( 0 ).setNodeGlobalMapping( {2, 4} );
  fldAssocBedge2.setAssociativity( 1 ).setNodeGlobalMapping( {4, 0} );
  fldAssocBedge3.setAssociativity( 0 ).setNodeGlobalMapping( {0, 5} );
  fldAssocBedge3.setAssociativity( 1 ).setNodeGlobalMapping( {5, 1} );

  // edge-to-cell connectivity
  fldAssocBedge1.setGroupLeft( 1 );
  fldAssocBedge1.setElementLeft( 0, 0 );
  fldAssocBedge1.setElementLeft( 0, 1 );

  fldAssocBedge2.setGroupLeft( 2 );
  fldAssocBedge2.setElementLeft( 0, 0 );
  fldAssocBedge2.setElementLeft( 0, 1 );

  fldAssocBedge3.setGroupLeft( 3 );
  fldAssocBedge3.setElementLeft( 0, 0 );
  fldAssocBedge3.setElementLeft( 0, 1 );

  fldAssocBedge1.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );
  fldAssocBedge1.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 1 );
  fldAssocBedge2.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
  fldAssocBedge2.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 1 );
  fldAssocBedge3.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocBedge3.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge1 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocBedge2 );
  boundaryTraceGroups_[2] = new FieldTraceGroupType<Line>( fldAssocBedge3 );

  for (int i = 0; i < 3; i++)
    boundaryTraceGroups_[i]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
