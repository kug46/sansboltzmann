// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// XField4D_1Ptope_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField4D_1Ptope_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE(UnitGrids_test_suite)

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(XField4D_1Ptope_X1_1Group_test)
{
  const Real small_tol= 1e-13;
  const Real tol= 1e-13;

  typedef std::array<int, 5> Int5;

  XField4D_1Ptope_X1_1Group xfld;

  BOOST_CHECK_EQUAL(xfld.nDOF(), 5);

  BOOST_CHECK_THROW(xfld.nDOFCellGroup(0), DeveloperException);
  BOOST_CHECK_THROW(xfld.nDOFInteriorTraceGroup(0), DeveloperException);
  BOOST_CHECK_THROW(xfld.nDOFBoundaryTraceGroup(0), DeveloperException);

  // check the node values

  BOOST_CHECK_EQUAL(xfld.DOF(0)[0], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(0)[1], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(0)[2], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(0)[3], 0);

  BOOST_CHECK_EQUAL(xfld.DOF(1)[0], 1);
  BOOST_CHECK_EQUAL(xfld.DOF(1)[1], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(1)[2], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(1)[3], 0);

  BOOST_CHECK_EQUAL(xfld.DOF(2)[0], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(2)[1], 1);
  BOOST_CHECK_EQUAL(xfld.DOF(2)[2], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(2)[3], 0);

  BOOST_CHECK_EQUAL(xfld.DOF(3)[0], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(3)[1], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(3)[2], 1);
  BOOST_CHECK_EQUAL(xfld.DOF(3)[3], 0);

  BOOST_CHECK_EQUAL(xfld.DOF(4)[0], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(4)[1], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(4)[2], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(4)[3], 1);

  // check the area field variable is a pentatope
  BOOST_CHECK_EQUAL(xfld.nCellGroups(), 1);
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  const XField4D_1Ptope_X1_1Group::FieldCellGroupType<Pentatope> &xfldVolume= xfld.getCellGroup<Pentatope>(0);

  // make sure identity nodemap is achieved

  int nodeMap[5];

  xfldVolume.associativity(0).getNodeGlobalMapping(nodeMap, 5);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 2);
  BOOST_CHECK_EQUAL(nodeMap[3], 3);
  BOOST_CHECK_EQUAL(nodeMap[4], 4);

  // make sure the faces, as assigned all positive, are all positive

  Int5 faceSign;

  faceSign= xfldVolume.associativity(0).faceSign();
  BOOST_CHECK_EQUAL(faceSign[0], +1);
  BOOST_CHECK_EQUAL(faceSign[1], +1);
  BOOST_CHECK_EQUAL(faceSign[2], +1);
  BOOST_CHECK_EQUAL(faceSign[3], +1);
  BOOST_CHECK_EQUAL(faceSign[4], +1);

  // field variable for the interior traces
  BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 0);

  // field variables for the boundary traces
  BOOST_CHECK_EQUAL(xfld.nBoundaryTraceGroups(), 5);
  BOOST_REQUIRE(xfld.getBoundaryTraceGroup<Tet>(0).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroup<Tet>(1).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroup<Tet>(2).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroup<Tet>(3).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroup<Tet>(4).topoTypeID() == typeid(Tet));

  // make sure the trace tets' nodes map to the correct global nodes

  const XField4D_1Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBedge0= xfld.getBoundaryTraceGroup<Tet>(0);
  const XField4D_1Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBedge1= xfld.getBoundaryTraceGroup<Tet>(1);
  const XField4D_1Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBedge2= xfld.getBoundaryTraceGroup<Tet>(2);
  const XField4D_1Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBedge3= xfld.getBoundaryTraceGroup<Tet>(3);
  const XField4D_1Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBedge4= xfld.getBoundaryTraceGroup<Tet>(4);

  xfldBedge0.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 1);
  BOOST_CHECK_EQUAL(nodeMap[1], 2);
  BOOST_CHECK_EQUAL(nodeMap[2], 4);
  BOOST_CHECK_EQUAL(nodeMap[3], 3);

  xfldBedge1.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 2);
  BOOST_CHECK_EQUAL(nodeMap[2], 3);
  BOOST_CHECK_EQUAL(nodeMap[3], 4);

  xfldBedge2.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 4);
  BOOST_CHECK_EQUAL(nodeMap[3], 3);

  xfldBedge3.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 2);
  BOOST_CHECK_EQUAL(nodeMap[3], 4);

  xfldBedge4.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 3);
  BOOST_CHECK_EQUAL(nodeMap[3], 2);

  // check areas and unit normals!

  XField4D_1Ptope_X1_1Group::FieldTraceGroupType<Tet>::ElementType<> xfldElem(xfldBedge0.basis());
  XField4D_1Ptope_X1_1Group::FieldTraceGroupType<Tet>::ElementType<>::RefCoordType sRef;
  XField4D_1Ptope_X1_1Group::FieldTraceGroupType<Tet>::ElementType<>::VectorX N;

  Real nxTrue;
  Real nyTrue;
  Real nzTrue;
  Real ntTrue;
  sRef= 1.0/4.0;

  xfldBedge0.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/3.0, 1e-12);

  nxTrue= 0.5;
  nyTrue= 0.5;
  nzTrue= 0.5;
  ntTrue= 0.5;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(ntTrue, N[3], small_tol, tol);

  xfldBedge1.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/6.0, 1e-12);

  nxTrue= -1.0;
  nyTrue= 0.0;
  nzTrue= 0.0;
  ntTrue= 0.0;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(ntTrue, N[3], small_tol, tol);

  xfldBedge2.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/6.0, 1e-12);

  nxTrue= 0.0;
  nyTrue= -1.0;
  nzTrue= 0.0;
  ntTrue= 0.0;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(ntTrue, N[3], small_tol, tol);

  xfldBedge3.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/6.0, 1e-12);

  nxTrue= 0.0;
  nyTrue= 0.0;
  nzTrue= -1.0;
  ntTrue= 0.0;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(ntTrue, N[3], small_tol, tol);

  xfldBedge4.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/6.0, 1e-12);

  nxTrue= 0.0;
  nyTrue= 0.0;
  nzTrue= 0.0;
  ntTrue= -1.0;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(ntTrue, N[3], small_tol, tol);

  // boundary face-to-cell connectivity

  // inside of face should be this group, the only group
  BOOST_CHECK_EQUAL(xfldBedge0.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldBedge1.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldBedge2.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldBedge3.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldBedge4.getGroupLeft(), 0);

  // inside of face should be this element, the only element
  BOOST_CHECK_EQUAL(xfldBedge0.getElementLeft(0), 0);
  BOOST_CHECK_EQUAL(xfldBedge1.getElementLeft(0), 0);
  BOOST_CHECK_EQUAL(xfldBedge2.getElementLeft(0), 0);
  BOOST_CHECK_EQUAL(xfldBedge3.getElementLeft(0), 0);
  BOOST_CHECK_EQUAL(xfldBedge4.getElementLeft(0), 0);

  // the interior canonical trace should be the zeroth canonical trace ???
  BOOST_CHECK_EQUAL(xfldBedge0.getCanonicalTraceLeft(0).trace, 0);
  BOOST_CHECK_EQUAL(xfldBedge1.getCanonicalTraceLeft(0).trace, 1);
  BOOST_CHECK_EQUAL(xfldBedge2.getCanonicalTraceLeft(0).trace, 2);
  BOOST_CHECK_EQUAL(xfldBedge3.getCanonicalTraceLeft(0).trace, 3);
  BOOST_CHECK_EQUAL(xfldBedge4.getCanonicalTraceLeft(0).trace, 4);

  // interior canonical trace orientation should be +1
  BOOST_CHECK_EQUAL(xfldBedge0.getCanonicalTraceLeft(0).orientation, 1);
  BOOST_CHECK_EQUAL(xfldBedge1.getCanonicalTraceLeft(0).orientation, 1);
  BOOST_CHECK_EQUAL(xfldBedge2.getCanonicalTraceLeft(0).orientation, 1);
  BOOST_CHECK_EQUAL(xfldBedge3.getCanonicalTraceLeft(0).orientation, 1);
  BOOST_CHECK_EQUAL(xfldBedge4.getCanonicalTraceLeft(0).orientation, 1);

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
