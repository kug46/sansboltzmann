// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD3D_SPHERE_TRIANGLE_X1
#define XFIELD3D_SPHERE_TRIANGLE_X1

#include "Field/XFieldArea.h"

namespace SANS
{
// triangle grid on a unit sphere with 2 sides at the poles
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// thetamin and thetamax define the polar angle range 0 < theta < 180
// area elements in 1 group
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 2 groups: southern, northern

class XField3D_Sphere_Triangle_X1 : public XField<PhysD3,TopoD2>
{
public:
  XField3D_Sphere_Triangle_X1( int ii, int jj, Real thetamin, Real thetamax, Real phimin = 0, Real phimax = 2*PI );
};

}

#endif // XFIELD3D_SPHERE_TRIANGLE_X1
