// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD3D_BOX_HEX_X1
#define XFIELD3D_BOX_HEX_X1

#include "Field/XFieldVolume.h"

#include <vector>
#include <array>

namespace SANS
{
//----------------------------------------------------------------------------//
// hexahedron grid in a unit box with 6 sides as separate boundary-edge groups
//
// generates grid with ii x jj x kk (hex) elements
// volume elements in 1 group
// boundary-triangle elements in 6 groups: x-min, x-max, y-min, y-max, z-min, z-max

class XField3D_Box_Hex_X1 : public XField<PhysD3,TopoD3>
{
public:
  XField3D_Box_Hex_X1( mpi::communicator comm,
                       const int ii, const int jj, const int kk,
                       const std::array<bool,3> periodic = {{false, false, false}} );

  XField3D_Box_Hex_X1( mpi::communicator comm,
                       const int ii, const int jj, const int kk,
                       Real xmin, Real xmax,
                       Real ymin, Real ymax,
                       Real zmin, Real zmax,
                       const std::array<bool,3> periodic = {{false, false, false}} );


  XField3D_Box_Hex_X1( mpi::communicator comm,
                       const std::vector<Real>& xvec,
                       const std::vector<Real>& yvec,
                       const std::vector<Real>& zvec,
                       const std::array<bool,3> periodic = {{false, false, false}} );
  XField3D_Box_Hex_X1( mpi::communicator comm,const std::string& filename,
                       const int ii, const int jj, const int kk,
                       const std::array<bool,3> periodic = {{false, false, false}} );

  XField3D_Box_Hex_X1( mpi::communicator comm,const std::string& filename,
                       const int ii, const int jj, const int kk,
                       Real xmin, Real xmax,
                       Real ymin, Real ymax,
                       Real zmin, Real zmax,
                       const std::array<bool,3> periodic = {{false, false, false}} );


  XField3D_Box_Hex_X1( mpi::communicator comm,const std::string& filename,
                       const std::vector<Real>& xvec,
                       const std::vector<Real>& yvec,
                       const std::vector<Real>& zvec,
                       const std::array<bool,3> periodic = {{false, false, false}} );

  static const int iXmin, iXmax, iYmin, iYmax, iZmin, iZmax;

protected:
  void generateGrid( mpi::communicator comm,
                     XField_Lagrange<PhysD3>& xfldin,
                     const std::vector<Real>& xvec,
                     const std::vector<Real>& yvec,
                     const std::vector<Real>& zvec,
                     const std::array<bool,3> periodic );
};

}

#endif // XFIELD3D_BOX_HEX_X1
