// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_2Quad_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_2Quad_X1_1Group_test )
{
  typedef std::array<int,4> Int4;

  XField2D_2Quad_X1_1Group xfld;

  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 6 );

  BOOST_CHECK_THROW( xfld.nDOFCellGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFInteriorTraceGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFBoundaryTraceGroup(0), DeveloperException );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 2 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0], 2 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  const XField2D_2Quad_X1_1Group::FieldCellGroupType<Quad>& xfldArea = xfld.getCellGroup<Quad>(0);

  int nodeMap[4];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );
  BOOST_CHECK_EQUAL( nodeMap[3], 2 );

  Int4 edgeSign;

  edgeSign = xfldArea.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );
  BOOST_CHECK_EQUAL( edgeSign[3], +1 );

  edgeSign = xfldArea.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );
  BOOST_CHECK_EQUAL( edgeSign[3], -1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField2D_2Quad_X1_1Group::FieldTraceGroupType<Line>& xfldIedge = xfld.getInteriorTraceGroup<Line>(0);

  xfldIedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldIedge.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(0), 1 );

  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(0).trace, 3 );

  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(0).orientation, -1 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Line) );

  const XField2D_2Quad_X1_1Group::FieldTraceGroupType<Line>& xfldBedge = xfld.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 6, xfldBedge.nElem() );

  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldBedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldBedge.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );

  xfldBedge.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldBedge.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBedge.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBedge.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(1), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(2), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(3), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(4), 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(5), 0 );

  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(1).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(2).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(3).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(4).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(5).trace, 3 );

  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(1).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(2).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(3).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(4).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(5).orientation, 1 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
