// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_BOX_QUAD_X1
#define XFIELD2D_BOX_QUAD_X1

#include "Field/XFieldArea.h"

namespace SANS
{
// quad grid in a unit box with 4 sides as separate boundary-edge groups
//
// generates grid with ii x jj (quad) elements
// area elements in 1 group
// interior-edge elements in 2 groups: horizontal, vertical
// boundary-edge elements in 4 groups: lower, right, upper, left

class XField2D_Box_Quad_X1 : public XField<PhysD2,TopoD2>
{
public:
  XField2D_Box_Quad_X1( int ii, int jj, Real xmin = 0, Real xmax = 1, Real ymin = 0, Real ymax = 1 );

  XField2D_Box_Quad_X1( const std::vector<Real>& xvec,
                        const std::vector<Real>& yvec );

  static const int iBottom, iRight, iTop, iLeft;

protected:
  void generateGrid( const std::vector<Real>& xvec,
                     const std::vector<Real>& yvec );
};

}

#endif // XFIELD2D_BOX_QUAD_X1
