// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "XField2D_Line_X1_2Group_AirfoilWithWake.h"

#include "Field/output_Tecplot.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_4Line_X1_2Group_AirfoilWithWake_test )
{
  typedef PhysD2 PhysDim;
  const int D = PhysDim::D;
  typedef XField2D_Line_X1_2Group_AirfoilWithWake::FieldCellGroupType<Line> XFieldCellGroupType;
  typedef XField2D_Line_X1_2Group_AirfoilWithWake::FieldTraceGroupType<Node> XFieldTraceGroupType;
  typedef ElementXField<PhysDim, TopoD1, Line> ElementXFieldCell;
  typedef ElementXField<PhysDim, TopoD0, Node> ElementXFieldTrace;
  typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
  typedef typename ElementXFieldCell::RefCoordType RefCoordCellType;
  typedef DLA::VectorS<D,Real> VectorX;

  const Real tol = 1.e-13;

  // ---------- Set up a 2-element line grid ----------
  const int nelem_a = 2;  // number of elements on airfoil
  const int nelem_w = 2;  // number of elements on wake

  const int nnode_a = nelem_a + 1;
  const int nnode_w = nelem_w + 1;

  std::vector<VectorX> coordinates_a(nnode_a);
  coordinates_a[0] = {1, 0.1};
  coordinates_a[1] = {0, 0};
  coordinates_a[2] = {1, -0.1};

  std::vector<VectorX> coordinates_w(nnode_w);
  coordinates_w[0] = {1.1, 0};
  coordinates_w[1] = {2.1, 0.3};
  coordinates_w[2] = {3.1, -0.1};

//  BOOST_CHECKPOINT("XField2D_Line_X1_2Group_AirfoilWithWake"); // TODO: error: use of undeclared identifier

  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);

  BOOST_CHECK_EQUAL( xfld.nDOF(), nnode_a+nnode_w );
  BOOST_CHECK_EQUAL( xfld.nElem(), nelem_a+nelem_w );

  // ---------- Check DOFs ----------
  for (int i = 0; i < nnode_a; i++)
    for (int j = 0; j < D; j++)
      SANS_CHECK_CLOSE( xfld.DOF(i)[j], coordinates_a[i][j], tol, tol );

  for (int i = 0; i < nnode_w; i++)
    for (int j = 0; j < D; j++)
      SANS_CHECK_CLOSE( xfld.DOF(i+nnode_a)[j], coordinates_w[i][j], tol, tol );

  // ---------- Check cell groups ----------
  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getCellGroupBase(1).topoTypeID() == typeid(Line) );

  int nodeMap[Line::NNode]; // container for node map of a cell

  VectorX t0, t1;
  VectorX t0true, t1true;
  ElementXFieldCell xElem(1, BasisFunctionCategory_Hierarchical);

  // airfoil

  const XFieldCellGroupType& xfldCell_a = xfld.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( nelem_a, xfldCell_a.nElem() );

  // cell 0
  xfldCell_a.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldCell_a.getElement( xElem, 0 );
  xElem.unitTangent( 0.5, t0 );

  t0true = coordinates_a[1]-coordinates_a[0];
  t0true =t0true / sqrt(dot(t0true,t0true));
  for (int j = 0; j < D; j++)
    SANS_CHECK_CLOSE( t0true[j], t0[j], tol, tol);

  // cell 1
  xfldCell_a.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldCell_a.getElement( xElem, 1 );
  xElem.unitTangent( 0.5, t1 );

  t1true = coordinates_a[2]-coordinates_a[1];
  t1true =t1true / sqrt(dot(t1true,t1true));
  for (int j = 0; j < D; j++)
    SANS_CHECK_CLOSE( t1true[j], t1[j], tol, tol);

  // wake

  const XFieldCellGroupType& xfldCell_w = xfld.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( nelem_w, xfldCell_w.nElem() );

  // cell 0
  xfldCell_w.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldCell_w.getElement( xElem, 0 );
  xElem.unitTangent( 0.5, t0 );

  t0true = coordinates_w[1]-coordinates_w[0];
  t0true =t0true / sqrt(dot(t0true,t0true));
  for (int j = 0; j < D; j++)
    SANS_CHECK_CLOSE( t0true[j], t0[j], tol, tol);

  // cell 1
  xfldCell_w.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );

  xfldCell_w.getElement( xElem, 1 );
  xElem.unitTangent( 0.5, t1 );

  t1true = coordinates_w[2]-coordinates_w[1];
  t1true =t1true / sqrt(dot(t1true,t1true));
  for (int j = 0; j < D; j++)
    SANS_CHECK_CLOSE( t1true[j], t1[j], tol, tol);


  // ---------- Check interior-trace groups ----------
  BOOST_REQUIRE( xfld.nInteriorTraceGroups() == 2 );
  for (int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(i).topoTypeID() == typeid(Node) );

  ElementXFieldTrace xTraceI(0, BasisFunctionCategory_Legendre);
  ElementXFieldCell xElemL(1, BasisFunctionCategory_Hierarchical);
  ElementXFieldCell xElemR(1, BasisFunctionCategory_Hierarchical);
  RefCoordTraceType sRefTrace = Node::centerRef;
  RefCoordCellType sRefL, sRefR;

  VectorX N, NL, NR;
  VectorX e0L, e0R;
  VectorX XTraceI;

  // interior trace group 0: airfoil interior

  const XFieldTraceGroupType& xfldInode0 = xfld.getInteriorTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( 1, xfldInode0.nElem() );

  BOOST_CHECK_EQUAL( xfldInode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode0.getGroupRight(), 0 );

  // node element 0
  // node map
  xfldInode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  // normals
  BOOST_CHECK_EQUAL( xfldInode0.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode0.associativity(0).normalSignR(), -1 );

  // interior trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode0.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode0.getCanonicalTraceRight(0).trace, 1 );

  // interior trace group 1: wake interior

  const XFieldTraceGroupType& xfldInode2 = xfld.getInteriorTraceGroup<Node>(1);

  BOOST_CHECK_EQUAL( 1, xfldInode2.nElem() );

  BOOST_CHECK_EQUAL( xfldInode2.getGroupLeft(), 1 );
  BOOST_CHECK_EQUAL( xfldInode2.getGroupRight(), 1 );

  // node element 0
  // node map
  xfldInode2.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );

  // normals
  BOOST_CHECK_EQUAL( xfldInode2.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode2.associativity(0).normalSignR(), -1 );

  // interior trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode2.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode2.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode2.getCanonicalTraceRight(0).trace, 1 );


  // ---------- Check boundary-trace group ----------
  BOOST_REQUIRE_EQUAL( xfld.nBoundaryTraceGroups(), 3 );
  for (int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(i).topoTypeID() == typeid(Node) );

  // boundary trace group 0: airfoil TE

  const XFieldTraceGroupType& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( 2, xfldBnode0.nElem() );

  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupRight(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupRightType(), eHubTraceGroup );

  // node element 0
  // node map
  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(),  0 );

  // boundary trace-to-cell connectivity

  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceRight(0).trace, -1 );

  // node element 1
  // node map
  xfldBnode0.associativity(1).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(1).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(1).normalSignR(),  0 );

  // interior trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(1), 1 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementRight(1), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(1).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceRight(1).trace, -1 );

  // boundary trace group 1: wake inflow
  const XFieldTraceGroupType& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.getGroupRight(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getGroupRightType(), eHubTraceGroup );

  // node map
  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(), 0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 1 );

  // boundary trace group 2: wake farfield
  const XFieldTraceGroupType& xfldBnode2 = xfld.getBoundaryTraceGroup<Node>(2);

  BOOST_CHECK_EQUAL( xfldBnode2.getGroupLeft(), 1 );

  // node map
  xfldBnode2.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode2.associativity(0).normalSignL(), 1 );
  BOOST_CHECK_EQUAL( xfldBnode2.associativity(0).normalSignR(), 0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode2.getElementLeft(0), 1 );
  BOOST_CHECK_EQUAL( xfldBnode2.getCanonicalTraceLeft(0).trace, 0 );

//  output_Tecplot( xfld, "tmp/XField2D_4Line_X1_2Group_AirfoilWithWake.dat" );  // used for testing output_tecplot for 2D manifolds
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
