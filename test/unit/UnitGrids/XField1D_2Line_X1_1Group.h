// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD1D_2LINE_X1_1GROUP
#define XFIELD1D_2LINE_X1_1GROUP

#include "Field/XFieldLine.h"

namespace SANS
{
/*
   A unit grid that consists of two lines within a single group

       (0)     (1)
    0 ----- 1 ----- 2
*/

class XField1D_2Line_X1_1Group : public XField<PhysD1,TopoD1>
{
public:
  XField1D_2Line_X1_1Group();
};

}

#endif //XFIELD1D_2LINE_X1_1GROUP
