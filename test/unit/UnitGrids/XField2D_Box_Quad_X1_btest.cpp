// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_Box_Quad_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_X1_GroupCount_test )
{
  for (int ii=1; ii <= 2; ii++)
  {
    for (int jj=1; jj <= 2; jj++)
    {
      XField2D_Box_Quad_X1 xfld( ii, jj );
      BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );

      BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
      BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );

      if ( ii==1 && jj==1 )
        BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );
      else if ( ii==1 || jj==1 )
        BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
      else if ( ii>1 && jj>1 )
        BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 2 );
      else
        SANS_DEVELOPER_EXCEPTION("Invalid quad mesh because ii and/or jj are/is unexpected");
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_X1_test1 )
{
  const int ii = 1;
  const int jj = 1;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField2D_Box_Quad_X1 xfld( ii, jj );

  BOOST_CHECK_EQUAL( xfld.iBottom, 0 );
  BOOST_CHECK_EQUAL( xfld.iRight,  1 );
  BOOST_CHECK_EQUAL( xfld.iTop,    2 );
  BOOST_CHECK_EQUAL( xfld.iLeft,   3 );

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( ii*jj, xfld.nElem() );

  BOOST_CHECK_THROW( xfld.nDOFCellGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFInteriorTraceGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFBoundaryTraceGroup(0), DeveloperException );

  // check the nodes

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real xmin = 0;
  Real xmax = 1;
  Real ymin = 0;
  Real ymax = 1;

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      SANS_CHECK_CLOSE( xfld.DOF( j*(ii+1) + i )[0],  xmin + (xmax - xmin)*i/Real(ii), small_tol, close_tol );
      SANS_CHECK_CLOSE( xfld.DOF( j*(ii+1) + i )[1],  ymin + (ymax - ymin)*j/Real(jj), small_tol, close_tol );
    }
  }

  // area field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( 0, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Line) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_X1_test2 )
{
  const int ii = 2;
  const int jj = 1;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField2D_Box_Quad_X1 xfld( ii, jj );

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( ii*jj, xfld.nElem() );

  // check the nodes

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real xmin = 0;
  Real xmax = 1;
  Real ymin = 0;
  Real ymax = 1;

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      SANS_CHECK_CLOSE( xfld.DOF( j*(ii+1) + i )[0],  xmin + (xmax - xmin)*i/Real(ii), small_tol, close_tol );
      SANS_CHECK_CLOSE( xfld.DOF( j*(ii+1) + i )[1],  ymin + (ymax - ymin)*j/Real(jj), small_tol, close_tol );
    }
  }

  // area field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Line) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_X1_test3 )
{
  const int ii = 1;
  const int jj = 2;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField2D_Box_Quad_X1 xfld( ii, jj );

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( ii*jj, xfld.nElem() );

  // check the nodes

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real xmin = 0;
  Real xmax = 1;
  Real ymin = 0;
  Real ymax = 1;

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      SANS_CHECK_CLOSE( xfld.DOF( j*(ii+1) + i )[0],  xmin + (xmax - xmin)*i/Real(ii), small_tol, close_tol );
      SANS_CHECK_CLOSE( xfld.DOF( j*(ii+1) + i )[1],  ymin + (ymax - ymin)*j/Real(jj), small_tol, close_tol );
    }
  }

  // area field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Line) );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_X1_test4 )
{
  const int ii = 3;
  const int jj = 2;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField2D_Box_Quad_X1 xfld( ii, jj );

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( ii*jj, xfld.nElem() );

  // check the nodes

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real xmin = 0;
  Real xmax = 1;
  Real ymin = 0;
  Real ymax = 1;

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      SANS_CHECK_CLOSE( xfld.DOF( j*(ii+1) + i )[0],  xmin + (xmax - xmin)*i/Real(ii), small_tol, close_tol );
      SANS_CHECK_CLOSE( xfld.DOF( j*(ii+1) + i )[1],  ymin + (ymax - ymin)*j/Real(jj), small_tol, close_tol );
    }
  }

  // area field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( 2, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Line) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
