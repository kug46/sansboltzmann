// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_4Quad_X1_1Group.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
/*
  6-------7-------8
  |       |       |
  |  (2)  |  (3)  |
  |       |       |
  3-------4-------5
  |       |       |
  |  (0)  |  (1)  |
  |       |       |
  0 ------1-------2
*/

XField2D_4Quad_X1_1Group::XField2D_4Quad_X1_1Group()
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(9);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(1);
  resizeCellGroups(1);

  // nodal coordinates for the four Quads. Nodes==DOF for Hierarchical polynomials.
  DOF(0) = {  0,  0 };
  DOF(1) = {  1,  0 };
  DOF(2) = {  2,  0 };
  DOF(3) = {  0,  1 };
  DOF(4) = {  1,  1 };
  DOF(5) = {  2,  1 };
  DOF(6) = {  0,  2 };
  DOF(7) = {  1,  2 };
  DOF(8) = {  2,  2 };

  // area field variable
  FieldCellGroupType<Quad>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Quad>::HierarchicalP1, 4 );

  // set the cell processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 3 ).setRank( comm_rank );

  //element area DOF associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 4, 3} );
  fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {1, 2, 5, 4} );
  fldAssocCell.setAssociativity( 2 ).setNodeGlobalMapping( {3, 4, 7, 6} );
  fldAssocCell.setAssociativity( 3 ).setNodeGlobalMapping( {4, 5, 8, 7} );

  // interior-edge field variable

  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, 4 );

  // edge-element processor rank
  fldAssocIedge.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocIedge.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocIedge.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocIedge.setAssociativity( 3 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocIedge.setAssociativity( 0 ).setNodeGlobalMapping( {1, 4} );
  fldAssocIedge.setAssociativity( 1 ).setNodeGlobalMapping( {4, 7} );
  fldAssocIedge.setAssociativity( 2 ).setNodeGlobalMapping( {5, 4} );
  fldAssocIedge.setAssociativity( 3 ).setNodeGlobalMapping( {4, 3} );


  // edge-to-cell connectivity
  fldAssocIedge.setGroupLeft( 0 );
  fldAssocIedge.setGroupRight( 0 );

  fldAssocIedge.setElementLeft( 0, 0 );
  fldAssocIedge.setElementRight( 1, 0 );
  fldAssocIedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );
  fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(3, -1), 0 );

  fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 1 ); // edge signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 1 ).setEdgeSign( -1, 3 );


  fldAssocIedge.setElementLeft( 2, 1 );
  fldAssocIedge.setElementRight( 3, 1 );
  fldAssocIedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );
  fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(3, -1), 1 );

  fldAssocCell.setAssociativity( 2 ).setEdgeSign( +1, 1 ); // edge signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 3 ).setEdgeSign( -1, 3 );


  fldAssocIedge.setElementLeft( 1, 2 );
  fldAssocIedge.setElementRight( 3, 2 );
  fldAssocIedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 2 );
  fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), 2 );

  fldAssocCell.setAssociativity( 1 ).setEdgeSign( +1, 2 ); // edge signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 3 ).setEdgeSign( -1, 0 );


  fldAssocIedge.setElementLeft( 0, 3 );
  fldAssocIedge.setElementRight( 2, 3 );
  fldAssocIedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 3 );
  fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), 3 );

  fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 2 ); // edge signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 2 ).setEdgeSign( -1, 0 );


  cellGroups_[0] = new FieldCellGroupType<Quad>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocIedge );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

  // boundary-edge field variable

  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, 8 );

  // boundary edge processor ranks
  fldAssocBedge.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 3 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 4 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 5 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 6 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 7 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocBedge.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
  fldAssocBedge.setAssociativity( 1 ).setNodeGlobalMapping( {1, 2} );
  fldAssocBedge.setAssociativity( 2 ).setNodeGlobalMapping( {2, 5} );
  fldAssocBedge.setAssociativity( 3 ).setNodeGlobalMapping( {5, 8} );
  fldAssocBedge.setAssociativity( 4 ).setNodeGlobalMapping( {8, 7} );
  fldAssocBedge.setAssociativity( 5 ).setNodeGlobalMapping( {7, 6} );
  fldAssocBedge.setAssociativity( 6 ).setNodeGlobalMapping( {6, 3} );
  fldAssocBedge.setAssociativity( 7 ).setNodeGlobalMapping( {3, 0} );

  // edge-to-cell connectivity
  fldAssocBedge.setGroupLeft( 0 );
  fldAssocBedge.setElementLeft( 0, 0 );
  fldAssocBedge.setElementLeft( 1, 1 );
  fldAssocBedge.setElementLeft( 1, 2 );
  fldAssocBedge.setElementLeft( 3, 3 );
  fldAssocBedge.setElementLeft( 3, 4 );
  fldAssocBedge.setElementLeft( 2, 5 );
  fldAssocBedge.setElementLeft( 2, 6 );
  fldAssocBedge.setElementLeft( 0, 7 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 1 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 2 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 3 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 4 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 5 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(3, 1), 6 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(3, 1), 7 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge );
  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
