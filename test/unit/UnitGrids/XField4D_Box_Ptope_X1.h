// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD4D_BOX_PTOPE_LAGRANGE_X1_H
#define XFIELD4D_BOX_PTOPE_LAGRANGE_X1_H

#include <vector>

#include "Field/XFieldSpacetime.h"

#ifdef SANS_AVRO
#include "geometry/context.h"
#include "library/cube.h"

namespace avro
{
class Body;
}
#endif

namespace SANS
{
//----------------------------------------------------------------------------//
// create pentatope grid in unit box with 24 x ii x jj x kk x mm (ptope) elements

// volume elements in 1 group: 24*ii*jj*kk*mm tetrahdedron
// boundary-triangle elements in 8 groups: x-min, x-max, y-min, y-max,
//                                          z-min, z-max, w-min, w-max

class XField4D_Box_Ptope_X1 : public XField<PhysD4, TopoD4>
{
public:
  XField4D_Box_Ptope_X1(mpi::communicator comm,
                       const unsigned long ii, const unsigned long jj, const unsigned long kk, const unsigned long mm,
                       std::array<bool, 4> periodic= {{false, false, false, false}});

  XField4D_Box_Ptope_X1(mpi::communicator comm,
                       const unsigned long ii, const unsigned long jj, const unsigned long kk, const unsigned long mm,
                       Real xmin, Real xmax,
                       Real ymin, Real ymax,
                       Real zmin, Real zmax,
                       Real wmin, Real wmax,
                       std::array<bool, 4> periodic= {{false, false, false, false}});

  XField4D_Box_Ptope_X1(mpi::communicator comm,
                       const std::vector<Real>& xvec,
                       const std::vector<Real>& yvec,
                       const std::vector<Real>& zvec,
                       const std::vector<Real>& wvec,
                       std::array<bool, 4> periodic= {{false, false, false, false}});

  XField4D_Box_Ptope_X1(mpi::communicator comm,const std::string& filename,
                       const unsigned long ii, const unsigned long jj, const unsigned long kk, const unsigned long mm,
                       std::array<bool, 4> periodic= {{false, false, false, false}});

  XField4D_Box_Ptope_X1( mpi::communicator comm,const std::string& filename,
                       const unsigned long ii, const unsigned long jj, const unsigned long kk, const unsigned long mm,
                       Real xmin, Real xmax,
                       Real ymin, Real ymax,
                       Real zmin, Real zmax,
                       Real wmin, Real wmax,
                       std::array<bool, 4> periodic = {{false, false, false, false}});

  XField4D_Box_Ptope_X1( mpi::communicator comm,const std::string& filename,
                       const std::vector<Real>& xvec,
                       const std::vector<Real>& yvec,
                       const std::vector<Real>& zvec,
                       const std::vector<Real>& wvec,
                       std::array<bool, 4> periodic= {{false, false, false, false}});

  static const int iXmin, iXmax, iYmin, iYmax, iZmin, iZmax, iWmin, iWmax;

#ifdef SANS_AVRO
  avro::Context * context();
  std::shared_ptr<avro::Body> & body_ptr();
#endif

protected:
  void createBody();
  void generateGrid(mpi::communicator comm,
                     XField_Lagrange<PhysD4>& xfldin,
                     const std::vector<Real>& xvec,
                     const std::vector<Real>& yvec,
                     const std::vector<Real>& zvec,
                     const std::vector<Real>& wvec,
                     std::array<bool, 4> periodic);

  std::vector<unsigned long> dims_;
  std::vector<Real> lengths_;

#ifdef SANS_AVRO
  avro::library::CubeMesh mesh_;
  std::shared_ptr<avro::Body> body_;
  avro::Context context_;
#endif
};

}

#endif // XFIELD4D_BOX_PTOPE_LAGRANGE_X1_H
