// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_4Triangle_X1_1Group.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//  4 ----- 2 ----- 3
//   \      |\      |
//    \ (2) | \ (1) |
//     \    |  \    |
//      \   |   \   |
//       \  |    \  |
//        \ | (0) \ |
//         \|      \|
//          0 ----- 1
//           \      |
//            \ (3) |
//             \    |
//              \   |
//               \  |
//                \ |
//                  5

XField2D_4Triangle_X1_1Group::XField2D_4Triangle_X1_1Group(BasisFunctionCategory basis_category)
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(6);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(1);
  resizeCellGroups(1);

  // nodal coordinates for the four triangles. Nodes==DOF for Hierarchical polynomials.
  DOF(0) = {  0,  0 };
  DOF(1) = {  1,  0 };
  DOF(2) = {  0,  1 };
  DOF(3) = {  1,  1 };
  DOF(4) = { -1,  1 };
  DOF(5) = {  1, -1 };

  // area field variable
  const BasisFunctionAreaBase<Triangle>* cell_basis = BasisFunctionAreaBase<Triangle>::getBasisFunction(1, basis_category);
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( cell_basis, 4 );

  // set the cell processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 3 ).setRank( comm_rank );

  //element area DOF associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );
  fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {3, 2, 1} );
  fldAssocCell.setAssociativity( 2 ).setNodeGlobalMapping( {2, 4, 0} );
  fldAssocCell.setAssociativity( 3 ).setNodeGlobalMapping( {1, 0, 5} );

  // edge signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );
  fldAssocCell.setAssociativity( 1 ).setEdgeSign( -1, 0 );
  fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 1 );
  fldAssocCell.setAssociativity( 2 ).setEdgeSign( -1, 1 );
  fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 2 );
  fldAssocCell.setAssociativity( 3 ).setEdgeSign( -1, 2 );

  FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
  cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

  xfldElementGroup->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // interior-edge field variable
  const BasisFunctionLineBase* trace_basis = BasisFunctionLineBase::getBasisFunction(1, basis_category);
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocIedge( trace_basis, 3 );

  // edge-element processor rank
  fldAssocIedge.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocIedge.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocIedge.setAssociativity( 2 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocIedge.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );
  fldAssocIedge.setAssociativity( 1 ).setNodeGlobalMapping( {2, 0} );
  fldAssocIedge.setAssociativity( 2 ).setNodeGlobalMapping( {0, 1} );

  // edge-to-cell connectivity
  fldAssocIedge.setGroupLeft( 0 );
  fldAssocIedge.setGroupRight( 0 );

  fldAssocIedge.setElementLeft( 0, 0 );
  fldAssocIedge.setElementRight( 1, 0 );
  fldAssocIedge.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), 0 );

  fldAssocIedge.setElementLeft( 0, 1 );
  fldAssocIedge.setElementRight( 2, 1 );
  fldAssocIedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );
  fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), 1 );

  fldAssocIedge.setElementLeft( 0, 2 );
  fldAssocIedge.setElementRight( 3, 2 );
  fldAssocIedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 2 );
  fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), 2 );

  FieldTraceGroupType<Line>* xfldIedge = NULL;
  interiorTraceGroups_[0] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );

  xfldIedge->setDOF(DOF_, nDOF_);

  // boundary-edge field variable

  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( trace_basis, 6 );

  // boundary edge processor ranks
  fldAssocBedge.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 3 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 4 ).setRank( comm_rank );
  fldAssocBedge.setAssociativity( 5 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocBedge.setAssociativity( 0 ).setNodeGlobalMapping( {0, 5} );
  fldAssocBedge.setAssociativity( 1 ).setNodeGlobalMapping( {5, 1} );
  fldAssocBedge.setAssociativity( 2 ).setNodeGlobalMapping( {1, 3} );
  fldAssocBedge.setAssociativity( 3 ).setNodeGlobalMapping( {3, 2} );
  fldAssocBedge.setAssociativity( 4 ).setNodeGlobalMapping( {2, 4} );
  fldAssocBedge.setAssociativity( 5 ).setNodeGlobalMapping( {4, 0} );

  // edge-to-cell connectivity
  fldAssocBedge.setGroupLeft( 0 );
  fldAssocBedge.setElementLeft( 3, 0 );
  fldAssocBedge.setElementLeft( 3, 1 );
  fldAssocBedge.setElementLeft( 1, 2 );
  fldAssocBedge.setElementLeft( 1, 3 );
  fldAssocBedge.setElementLeft( 2, 4 );
  fldAssocBedge.setElementLeft( 2, 5 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 2 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 3 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 4 );
  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 5 );

  FieldTraceGroupType<Line>* xfldBedge = NULL;
  boundaryTraceGroups_[0] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

  xfldBedge->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
