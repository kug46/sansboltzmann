// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_BOX_TRIANGLE_LAGRANGE_X1
#define XFIELD2D_BOX_TRIANGLE_LAGRANGE_X1

#include <vector>

#include "Field/XFieldArea.h"

namespace SANS
{
// triangle grid in a unit box with 4 sides as separate boundary-edge groups
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// area elements in 1 group
// boundary-edge elements in 4 groups: lower, right, upper, left
//
class XField2D_Box_Triangle_Lagrange_X1 : public XField<PhysD2,TopoD2>
{
public:
  XField2D_Box_Triangle_Lagrange_X1( mpi::communicator comm, int ii, int jj, Real xmin = 0, Real xmax = 1, Real ymin = 0, Real ymax = 1,
                                     const std::array<bool,2> periodic = {{ false, false }} );

  XField2D_Box_Triangle_Lagrange_X1( mpi::communicator comm,
                                     const std::vector<Real>& xvec,
                                     const std::vector<Real>& yvec,
                                     const std::array<bool,2> periodic = {{ false, false }} );

  static const int iBottom, iRight, iTop, iLeft;

protected:
  void generateGrid( mpi::communicator comm,
                     const std::vector<Real>& xvec,
                     const std::vector<Real>& yvec,
                     const std::array<bool,2> periodic );
};

}

#endif // XFIELD2D_BOX_TRIANGLE_LAGRANGE_X1
