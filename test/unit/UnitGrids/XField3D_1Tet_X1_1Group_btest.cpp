// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField3D_1Tet_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_1Tet_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_1Tet_X1_1Group_test )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  typedef std::array<int,4> Int4;

  XField3D_1Tet_X1_1Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 4 );

  BOOST_CHECK_THROW( xfld.nDOFCellGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFInteriorTraceGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFBoundaryTraceGroup(0), DeveloperException );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  const XField3D_1Tet_X1_1Group::FieldCellGroupType<Tet>& xfldVolume = xfld.getCellGroup<Tet>(0);

  int nodeMap[4];

  xfldVolume.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  Int4 faceSign;

  faceSign = xfldVolume.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(3).topoTypeID() == typeid(Triangle) );

  const XField3D_1Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBedge0 = xfld.getBoundaryTraceGroup<Triangle>(0);
  const XField3D_1Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBedge1 = xfld.getBoundaryTraceGroup<Triangle>(1);
  const XField3D_1Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBedge2 = xfld.getBoundaryTraceGroup<Triangle>(2);
  const XField3D_1Tet_X1_1Group::FieldTraceGroupType<Triangle>& xfldBedge3 = xfld.getBoundaryTraceGroup<Triangle>(3);

  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  XField3D_1Tet_X1_1Group::FieldTraceGroupType<Triangle>::ElementType<> xfldElem( xfldBedge0.basis() );
  XField3D_1Tet_X1_1Group::FieldTraceGroupType<Triangle>::ElementType<>::RefCoordType sRef;
  XField3D_1Tet_X1_1Group::FieldTraceGroupType<Triangle>::ElementType<>::VectorX N;

  // Check areas and unit normals
  Real nxTrue, nyTrue, nzTrue;
  sRef = 1./3.;

  xfldBedge0.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), sqrt(3.)/4.*2., 1e-12 );

  nxTrue =  1./sqrt(3);
  nyTrue =  1./sqrt(3);
  nzTrue =  1./sqrt(3);

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  xfldBedge1.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 0.5, 1e-12 );

  nxTrue = -1;
  nyTrue =  0;
  nzTrue =  0;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  xfldBedge2.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 0.5, 1e-12 );

  nxTrue =  0;
  nyTrue = -1;
  nzTrue =  0;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  xfldBedge3.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 0.5, 1e-12 );

  nxTrue =  0;
  nyTrue =  0;
  nzTrue = -1;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBedge0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceLeft(0).trace, 3 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceLeft(0).orientation, 1 );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
