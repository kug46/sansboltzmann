// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField4D_1Ptope_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

/* a unit grid that consists of one pentatope within a single group
 *
 *      2
 *      |\
 *      | \
 *     .|  \
 *      |   \
 *      |    \            ----> in another dimension: (4)
 *    . |     \                  (to the tune of the Wolfmother song)
 *      |      \
 *      0-------1
 *   . /     .
 *    /   .
 *   / .
 *  3
 *
 */

XField4D_1Ptope_X1_1Group::XField4D_1Ptope_X1_1Group()
{

//  SANS_DEVELOPER_EXCEPTION("I HAVE NO IDEA WHAT I'M DOING!");

  // MPI rank
  int comm_rank= comm_->rank();

  // create the DOF arrays
  resizeDOF(5);

  // create the element groups
  resizeInteriorTraceGroups(0);   // no interior boundaries
  resizeBoundaryTraceGroups(5);   // five 3d boundaries on a ptope
  resizeCellGroups(1);            // one 4d ptope element

  // vertices of the pentatope
  DOF(0)= {0, 0, 0, 0};
  DOF(1)= {1, 0, 0, 0};
  DOF(2)= {0, 1, 0, 0};
  DOF(3)= {0, 0, 1, 0};
  DOF(4)= {0, 0, 0, 1};

  // volume field variable
  FieldCellGroupType<Pentatope>::FieldAssociativityConstructorType
  fldAssocCell(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1, 1);

  // set the cell processor rank
  fldAssocCell.setAssociativity(0).setRank(comm_rank);

  // element volume associativity
  fldAssocCell.setAssociativity(0).setNodeGlobalMapping({0, 1, 2, 3, 4});

  // face signs for elements (L <==> +, R <==> -)
  fldAssocCell.setAssociativity(0).setFaceSign(+1, 0);
  fldAssocCell.setAssociativity(0).setFaceSign(+1, 1);
  fldAssocCell.setAssociativity(0).setFaceSign(+1, 2);
  fldAssocCell.setAssociativity(0).setFaceSign(+1, 3);
  fldAssocCell.setAssociativity(0).setFaceSign(+1, 4);

  cellGroups_[0]= new FieldCellGroupType<Pentatope>(fldAssocCell);
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_= fldAssocCell.nElem();

  // interior edge field variable

  // none exist!

  // boundary-edge field variable

  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface0(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface1(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface2(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface3(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface4(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);

  // boundary face processor ranks
  fldAssocBface0.setAssociativity(0).setRank(comm_rank);
  fldAssocBface1.setAssociativity(0).setRank(comm_rank);
  fldAssocBface2.setAssociativity(0).setRank(comm_rank);
  fldAssocBface3.setAssociativity(0).setRank(comm_rank);
  fldAssocBface4.setAssociativity(0).setRank(comm_rank);

  // face-element associativity
  fldAssocBface0.setAssociativity(0).setNodeGlobalMapping({1, 2, 4, 3});
  fldAssocBface1.setAssociativity(0).setNodeGlobalMapping({0, 2, 3, 4});
  fldAssocBface2.setAssociativity(0).setNodeGlobalMapping({0, 1, 4, 3});
  fldAssocBface3.setAssociativity(0).setNodeGlobalMapping({0, 1, 2, 4});
  fldAssocBface4.setAssociativity(0).setNodeGlobalMapping({0, 1, 3, 2});

  // face-to-cell connectivity
  fldAssocBface0.setGroupLeft(0);
  fldAssocBface1.setGroupLeft(0);
  fldAssocBface2.setGroupLeft(0);
  fldAssocBface3.setGroupLeft(0);
  fldAssocBface4.setGroupLeft(0);
  fldAssocBface0.setElementLeft(0, 0);
  fldAssocBface1.setElementLeft(0, 0);
  fldAssocBface2.setElementLeft(0, 0);
  fldAssocBface3.setElementLeft(0, 0);
  fldAssocBface4.setElementLeft(0, 0);
  fldAssocBface0.setCanonicalTraceLeft(CanonicalTraceToCell(0, 1), 0);
  fldAssocBface1.setCanonicalTraceLeft(CanonicalTraceToCell(1, 1), 0);
  fldAssocBface2.setCanonicalTraceLeft(CanonicalTraceToCell(2, 1), 0);
  fldAssocBface3.setCanonicalTraceLeft(CanonicalTraceToCell(3, 1), 0);
  fldAssocBface4.setCanonicalTraceLeft(CanonicalTraceToCell(4, 1), 0);

  boundaryTraceGroups_[0]= new FieldTraceGroupType<Tet>(fldAssocBface0);
  boundaryTraceGroups_[1]= new FieldTraceGroupType<Tet>(fldAssocBface1);
  boundaryTraceGroups_[2]= new FieldTraceGroupType<Tet>(fldAssocBface2);
  boundaryTraceGroups_[3]= new FieldTraceGroupType<Tet>(fldAssocBface3);
  boundaryTraceGroups_[4]= new FieldTraceGroupType<Tet>(fldAssocBface4);

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[3]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[4]->setDOF(DOF_, nDOF_);

  // check that the grid is correct
  checkGrid();

}

}
