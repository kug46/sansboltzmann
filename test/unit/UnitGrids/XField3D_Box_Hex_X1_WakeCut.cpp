// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField3D_Box_Hex_X1_WakeCut.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Field/XFieldLine.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// create hexahedral grid in box
/*
// volume elements in 1 group: ii x jj x kk hex
// interior-quad elements in 3 groups: x-const, y-const, z-const
// boundary-quad elements in 9 groups: x-min, x-max, y-min, y-max, z-min, z-max, wing upper, wing lower, wake

// Node ordering of the original hex
//
//         y
//  2----------3
//  |\     ^   |\
//  | \    |   | \
//  |  \   |   |  \
//  |   6------+---7
//  |   |  +-- |-- | -> x
//  0---+---\--1   |
//   \  |    \  \  |
//    \ |     \  \ |
//     \|      z  \|
//      4----------5
*/
XField3D_Box_Hex_X1_WakeCut::XField3D_Box_Hex_X1_WakeCut( const int nChord, const int nSpan, const Real span, const int nWake, const bool adjoint )
{
  SANS_ASSERT( nChord >= 3 && nSpan >= 2 && nWake >= 2 );

  int comm_rank = comm_->rank();

  // Setup the number of elements in each directoin
  int ii = nWake + nChord + nWake;
  int jj = nWake + nSpan + nWake;
  int kk = 2*nWake;

  int nBoxNode = (ii + 1)*(jj + 1)*(kk + 1);
  int nWingNode = (nChord-1)*(nSpan-1); // Duplicate on wing (interior only)
  int nWakeNode = (nWake+1)*(nSpan-1); //Duplicate on TE + wake + Outflow

  // Total number of nodes
  int nnode = nBoxNode + nWingNode + nWakeNode;
  nElem_ = ii*jj*kk;

  // create the grid-coordinate DOF arrays
  resizeDOF( nnode );

  Real ffdist = span;


  std::vector<Real> rSpan(nSpan-1);
  for ( int i = 1; i < nSpan; i++ )
    //rSpan[i-1] = span*i/Real(nSpan);
    rSpan[i-1] = span*(1-0.5*(1+cos(PI*i/Real(nSpan))));

  std::vector<Real> rChord(nChord-1);
  for ( int i = 1; i < nChord; i++ )
    //rChord[i-1] = i/Real(nChord);
    rChord[i-1] = 1-0.5*(1+cos(PI*i/Real(nChord)));


  double xle = 0;
  double xte = 1;

  // stagnation line: quadratic increments w/ dx1 and dx2 matching airfoil
  double x1 = rChord[rChord.size() - 1];
  double x2 = rChord[rChord.size() - 2];

  double dx1 = xte - x1;
  double dx2 = x1 - x2;

  double rffd = (xte + ffdist) - xte/2.;
  const int iiwake = nWake+1;

  double a = (2*((rffd + (xle+xte)/2) - xte) + iiwake*((iiwake - 3)*dx1 - (iiwake - 1)*dx2)) / ((double) (2*iiwake*(2 - iiwake*(3 - iiwake))));
  double b = 0.5*(dx2 - dx1) - 3*a;
  double c = 0.5*(3*dx1 - dx2) + 2*a;

  int iim = iiwake-1;
  std::vector<Real> rwk(iiwake);
  for (int i = 0; i < iiwake; i++)
    rwk[i] = i*(c + i*(b + i*a))/(iim*(c + iim*(b + iim*a)));

  std::vector<Real> x(ii + 1), y( jj + 1 ), z( kk + 1 );

  // Setup the x-coordinates (stremwise)
  for (std::size_t i = 0; i < rwk.size(); i++ )
    x[i] = -ffdist*rwk[rwk.size()-1-i];

  for (std::size_t i = 0; i < rChord.size(); i++ )
    x[rwk.size()+i] = rChord[i];

  for (std::size_t i = 0; i < rwk.size(); i++ )
    x[rwk.size()+rChord.size()+i] = xte + ffdist*rwk[i];


  // Setup the y-coordinates (spanwise)
  for (std::size_t i = 0; i < rwk.size(); i++ )
    y[i] = -span/2 - ffdist*rwk[rwk.size()-1-i];

  for (std::size_t i = 0; i < rSpan.size(); i++ )
    y[rwk.size()+i] = -span/2 + rSpan[i];

  for (std::size_t i = 0; i < rwk.size(); i++ )
    y[rwk.size()+rSpan.size()+i] = span/2 + ffdist*rwk[i];


  // Setup the y-coordinates (normal)
  for (std::size_t i = 0; i < rwk.size(); i++ )
    z[i] = -ffdist*rwk[rwk.size()-1-i];

  for (std::size_t i = 0; i < rwk.size(); i++ )
    z[rwk.size()-1+i] = ffdist*rwk[i];



  const int joffset = (ii+1);
  const int koffset = (ii+1)*(jj+1);

  for (int k = 0; k < kk+1; k++)
    for (int j = 0; j < jj+1; j++)
      for (int i = 0; i < ii+1; i++)
        DOF(k*koffset + j*joffset + i) = { x[i], y[j], z[k] };

  // Duplicate nodes for the wing (only on the interior)
  {
    int k = nWake;
    for (int j = 0; j < nSpan-1; j++)
      for (int i = 0; i < (nChord-1); i++)
        DOF(nBoxNode + j*(nChord-1) + i) = { x[nWake+1+i], y[nWake+1+j], z[k] };
  }

  // Duplicate nodes for the wake including the wing TE and outflow, but not spanwise edges
  {
    int k = nWake;
    dupPointOffset_ = nBoxNode + nWingNode;
    invPointMap_.resize( nWakeNode );

    KuttaPoints_.resize(nSpan-1);

    for (int j = 0; j < nSpan-1; j++)
      for (int i = 0; i < nWake+1; i++)
      {
        int iW = nWake+nChord+i;
        int jW = nWake+1+j;
        DOF(dupPointOffset_ + j*(nWake+1) + i) = { x[iW], y[jW], z[k] };

        int origPoint = k*koffset + jW*joffset + iW;

        invPointMap_[j*(nWake+1) + i] = origPoint;

        if (i == 0)
          KuttaPoints_[j] = dupPointOffset_ + j*(nWake+1) + i;
      }
  }


  // create the element groups
  int cnt = 0;
  if (ii > 1) cnt++;    // interior: x-const
  if (jj > 1) cnt++;    // interior: y-const
  if (kk > 1) cnt++;    // interior: z-const
  resizeInteriorTraceGroups(cnt);
  cnt = 0;

  resizeBoundaryTraceGroups(9);     // x-min, x-max, y-min, y-max, z-min, z-max, WingLower, WingUpper, Wake
  resizeCellGroups(1);
  resizeBoundaryFrameGroups(2);    // Trefftz, Kutta

  // grid area field variable

  FieldCellGroupType<Hex>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Hex>::HierarchicalP1, nElem_ );

  // volume element grid-coordinate DOF associativity (cell-to-node connectivity)

  std::vector<int> hexnodes;
  int elem = 0;
  for (int k = 0; k < kk; k++)
  {
    for (int j = 0; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        const int n0 = k*koffset + j*joffset + i;

        //All the nodes that make up an individual hex
        if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake && i < ii-nWake) )
        {
          const int iW = i-nWake-1;
          const int jW = j-nWake-1;
          const int nWL = nBoxNode + jW*(nChord-1) + iW;
          int nW0 = nWL + 0;
          int nW1 = nWL + 1;
          int nW2 = nWL + (nChord-1) + 1;
          int nW3 = nWL + (nChord-1) + 0;

          if ( i == nWake      )
          {
            nW0 = n0 + koffset + 0;
            nW3 = n0 + koffset + joffset + 0;
          }
          if ( i == ii-nWake-1 ) //TE points
          {
            nW1 = dupPointOffset_ + jW*(nWake+1);
            nW2 = dupPointOffset_ + (jW+1)*(nWake+1);
          }

          if ( j == nWake      )
          {
            nW0 = n0 + koffset + 0;
            nW1 = n0 + koffset + 1;
          }
          if ( j == jj-nWake-1 )
          {
            nW2 = n0 + koffset + joffset + 1;
            nW3 = n0 + koffset + joffset + 0;
          }


          hexnodes = { n0 + 0,
                       n0 + 1,
                       n0 + joffset + 1,
                       n0 + joffset + 0,

                       nW0, nW1, nW2, nW3 };
        }
        else if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= ii-nWake) )
        {
          const int iW = i-nWake-nChord;
          const int jW = j-nWake-1;
          const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
          int nW0 = nWL + 0;
          int nW1 = nWL + 1;
          int nW2 = nWL + (nWake+1) + 1;
          int nW3 = nWL + (nWake+1) + 0;

          if ( j == nWake      )
          {
            nW0 = n0 + koffset + 0;
            nW1 = n0 + koffset + 1;
          }
          if ( j == jj-nWake-1 )
          {
            nW2 = n0 + koffset + joffset + 1;
            nW3 = n0 + koffset + joffset + 0;
          }

          hexnodes = { n0 + 0,
                       n0 + 1,
                       n0 + joffset + 1,
                       n0 + joffset + 0,

                       nW0, nW1, nW2, nW3 };
        }
        else
        {
          hexnodes = { n0 + 0,
                       n0 + 1,
                       n0 + joffset + 1,
                       n0 + joffset + 0,

                       n0 + koffset + 0,
                       n0 + koffset + 1,
                       n0 + koffset + joffset + 1,
                       n0 + koffset + joffset + 0 };
        }

        elem = k*jj*ii + j*ii + i;

        fldAssocCell.setAssociativity( elem ).setRank( comm_rank );

        //Set the hex nodes
        fldAssocCell.setAssociativity( elem ).setNodeGlobalMapping( hexnodes );
      }
    }
  }

  // interior-edge field variable

  int faceL;
  int elemL, elemR;
  int faceNodes[4];
  CanonicalTraceToCell canonicalL, canonicalR;

  const int (*TraceNodes)[ Quad::NTrace ] = TraceToCellRefCoord<Quad, TopoD3, Hex>::TraceNodes;

  const int n0L = 0;

  //Canonical nodes that make up the left hex
  const int hexnodesL[8] = { n0L + 0,
                             n0L + 1,
                             n0L + joffset + 1,
                             n0L + joffset + 0,

                             n0L + koffset + 0,
                             n0L + koffset + 1,
                             n0L + koffset + joffset + 1,
                             n0L + koffset + joffset + 0 };

  if ( ii > 1 )
  {
    //Local scope releases memory from fldAssocXface when it's no longer needed
    // x-constant faces
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocXface( BasisFunctionAreaBase<Quad>::HierarchicalP1, (ii-1)*jj*kk );

    fldAssocXface.setGroupLeft( 0 );
    fldAssocXface.setGroupRight( 0 );

    // The canonical traces only need to be setup once per group
    faceL = 2;

    const int n0R = 1;

    //Nodes that make up the right hex
    const int hexnodesR[8] = { n0R + 0,
                               n0R + 1,
                               n0R + joffset + 1,
                               n0R + joffset + 0,

                               n0R + koffset + 0,
                               n0R + koffset + 1,
                               n0R + koffset + joffset + 1,
                               n0R + koffset + joffset + 0 };

    for (int n = 0; n < 4; n++)
      faceNodes[n] = hexnodesL[ TraceNodes[faceL][n] ];

    canonicalL.set(faceL, 1);
    canonicalR = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(faceNodes, 4, hexnodesR, 8);

    int faceI = 0;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii-1; i++)
        {
          const int n0L = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake && i < ii-nWake) )
          {
            // Nodes on the lower surface of the wing
            const int iW = i-nWake-1;
            const int jW = j-nWake-1;
            const int nWL = nBoxNode + jW*(nChord-1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nChord-1) + 1;
            int nW3 = nWL + (nChord-1) + 0;

            if ( i == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW3 = n0L + koffset + joffset + 0;
            }
            if ( i == ii-nWake-1 ) //TE points
            {
              nW1 = dupPointOffset_ + jW*(nWake+1);
              nW2 = dupPointOffset_ + (jW+1)*(nWake+1);
            }

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 1;
              nW3 = n0L + koffset + joffset + 0;
            }

            hexnodes = { n0L + 0,
                         n0L + 1,
                         n0L + joffset + 1,
                         n0L + joffset + 0,

                         nW0, nW1, nW2, nW3 };
          }
          else if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= ii-nWake) )
          {
            // Node on the wake
            const int iW = i-nWake-nChord;
            const int jW = j-nWake-1;
            const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nWake+1) + 1;
            int nW3 = nWL + (nWake+1) + 0;

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 1;
              nW3 = n0L + koffset + joffset + 0;
            }

            hexnodes = { n0L + 0,
                         n0L + 1,
                         n0L + joffset + 1,
                         n0L + joffset + 0,

                         nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodes = { n0L + 0,
                         n0L + 1,
                         n0L + joffset + 1,
                         n0L + joffset + 0,

                         n0L + koffset + 0,
                         n0L + koffset + 1,
                         n0L + koffset + joffset + 1,
                         n0L + koffset + joffset + 0 };
          }

          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

          elemL = (k*jj*ii + j*ii + (i+0));
          elemR = (k*jj*ii + j*ii + (i+1));

          fldAssocXface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocXface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocXface.setElementLeft( elemL, faceI );
          fldAssocXface.setElementRight( elemR, faceI );
          fldAssocXface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocXface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( -1, canonicalR.trace );

          faceI++;
        }
      }
    }

    //Create the interior face group
    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Quad>( fldAssocXface );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;
  }

  if ( jj > 1 )
  {
    //Local scope releases memory from fldAssocYface when it's no longer needed
    // y-constant faces
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocYface( BasisFunctionAreaBase<Quad>::HierarchicalP1, ii*(jj-1)*kk );

    fldAssocYface.setGroupLeft( 0 );
    fldAssocYface.setGroupRight( 0 );

    const int n0R = joffset;

    //Nodes that make up the right hex
    const int hexnodesR[8] = { n0R + 0,
                               n0R + 1,
                               n0R + joffset + 1,
                               n0R + joffset + 0,

                               n0R + koffset + 0,
                               n0R + koffset + 1,
                               n0R + koffset + joffset + 1,
                               n0R + koffset + joffset + 0 };

    // The canonical traces only need to be setup once per group
    faceL = 3;

    for (int n = 0; n < 4; n++)
      faceNodes[n] = hexnodesL[ TraceNodes[faceL][n] ];

    canonicalL.set(faceL, 1);
    canonicalR = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(faceNodes, 4, hexnodesR, 8);

    int faceI = 0;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj-1; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0L = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake && i < ii-nWake) )
          {
            const int iW = i-nWake-1;
            const int jW = j-nWake-1;
            const int nWL = nBoxNode + jW*(nChord-1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nChord-1) + 1;
            int nW3 = nWL + (nChord-1) + 0;

            if ( i == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW3 = n0L + koffset + joffset + 0;
            }
            if ( i == ii-nWake-1 ) //TE points
            {
              nW1 = dupPointOffset_ + jW*(nWake+1);
              nW2 = dupPointOffset_ + (jW+1)*(nWake+1);
            }

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 1;
              nW3 = n0L + koffset + joffset + 0;
            }


            hexnodes = { n0L + 0,
                         n0L + 1,
                         n0L + joffset + 1,
                         n0L + joffset + 0,

                         nW0, nW1, nW2, nW3 };
          }
          else if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= ii-nWake) )
          {
            const int iW = i-nWake-nChord;
            const int jW = j-nWake-1;
            const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nWake+1) + 1;
            int nW3 = nWL + (nWake+1) + 0;

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 1;
              nW3 = n0L + koffset + joffset + 0;
            }

            hexnodes = { n0L + 0,
                         n0L + 1,
                         n0L + joffset + 1,
                         n0L + joffset + 0,

                         nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodes = { n0L + 0,
                         n0L + 1,
                         n0L + joffset + 1,
                         n0L + joffset + 0,

                         n0L + koffset + 0,
                         n0L + koffset + 1,
                         n0L + koffset + joffset + 1,
                         n0L + koffset + joffset + 0 };
          }

          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

          elemL = (k*jj*ii + (j+0)*ii + i);
          elemR = (k*jj*ii + (j+1)*ii + i);

          fldAssocYface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocYface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocYface.setElementLeft( elemL, faceI );
          fldAssocYface.setElementRight( elemR, faceI );
          fldAssocYface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocYface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( -1, canonicalR.trace );

          faceI++;
        }
      }
    }

    //Create the interior face group
    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Quad>( fldAssocYface );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;
  }

  if ( kk > 1 )
  {
    //Local scope releases memory from fldAssocZface when it's no longer needed
    // z-constant faces
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType
       fldAssocZface( BasisFunctionAreaBase<Quad>::HierarchicalP1, ii*jj*(kk-1) - (nChord+nWake)*nSpan );

    fldAssocZface.setGroupLeft( 0 );
    fldAssocZface.setGroupRight( 0 );

    const int n0R = koffset;

    //Nodes that make up the right hex
    const int hexnodesR[8] = { n0R + 0,
                               n0R + 1,
                               n0R + joffset + 1,
                               n0R + joffset + 0,

                               n0R + koffset + 0,
                               n0R + koffset + 1,
                               n0R + koffset + joffset + 1,
                               n0R + koffset + joffset + 0 };

    // The canonical traces only need to be setup once per group
    faceL = 5;

    for (int n = 0; n < 4; n++)
      faceNodes[n] = hexnodesL[ TraceNodes[faceL][n] ];

    canonicalL.set(faceL, 1);
    canonicalR = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(faceNodes, 4, hexnodesR, 8);

    int faceI = 0;
    for (int k = 0; k < kk-1; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          // Skip the faces that represent the wing and wake
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake) )
            continue;

          const int n0L = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0L + 0,
                                    n0L + 1,
                                    n0L + joffset + 1,
                                    n0L + joffset + 0,

                                    n0L + koffset + 0,
                                    n0L + koffset + 1,
                                    n0L + koffset + joffset + 1,
                                    n0L + koffset + joffset + 0 };

          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

          elemL = ((k+0)*jj*ii + j*ii + i);
          elemR = ((k+1)*jj*ii + j*ii + i);

          fldAssocZface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocZface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocZface.setElementLeft( elemL, faceI );
          fldAssocZface.setElementRight( elemR, faceI );
          fldAssocZface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocZface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( -1, canonicalR.trace );

          faceI++;
        }
      }
    }

    //Create the interior face group
    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Quad>( fldAssocZface );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;
  }


  {
    //Local scope releases memory from fldAssocXface when it's no longer needed
    // x-min boundry
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocXface( BasisFunctionAreaBase<Quad>::HierarchicalP1, jj*kk );

    fldAssocXface.setGroupLeft( 0 );

    // The canonical traces only need to be setup once per group
    faceL = 4;
    canonicalL.set(faceL, 1);

    int faceB = 0;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        int i = 0;
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 1,
                                    n0 + joffset + 0,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 1,
                                    n0 + koffset + joffset + 0 };

          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

          elemL = (k*jj*ii + j*ii + i);

          fldAssocXface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocXface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocXface.setElementLeft( elemL, faceB );
          fldAssocXface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[0] = new FieldTraceGroupType<Quad>( fldAssocXface );
    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  }


  {
    //Local scope releases memory from fldAssocXface when it's no longer needed
    // x-max boundry
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocXface( BasisFunctionAreaBase<Quad>::HierarchicalP1, jj*kk );

    fldAssocXface.setGroupLeft( 0 );

    // The canonical traces only need to be setup once per group
    faceL = 2;
    canonicalL.set(faceL, 1);

    int faceB = 0;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        int i = ii-1;
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) )
          {
            const int iW = i-nWake-nChord;
            const int jW = j-nWake-1;
            const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nWake+1) + 1;
            int nW3 = nWL + (nWake+1) + 0;

            if ( j == nWake      )
            {
              nW0 = n0 + koffset + 0;
              nW1 = n0 + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0 + koffset + joffset + 1;
              nW3 = n0 + koffset + joffset + 0;
            }

            hexnodes = { n0 + 0,
                         n0 + 1,
                         n0 + joffset + 1,
                         n0 + joffset + 0,

                         nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodes = { n0 + 0,
                         n0 + 1,
                         n0 + joffset + 1,
                         n0 + joffset + 0,

                         n0 + koffset + 0,
                         n0 + koffset + 1,
                         n0 + koffset + joffset + 1,
                         n0 + koffset + joffset + 0 };
          }

          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

          elemL = (k*jj*ii + j*ii + i);

          fldAssocXface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocXface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocXface.setElementLeft( elemL, faceB );
          fldAssocXface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Quad>( fldAssocXface );
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  }


  {
    //Local scope releases memory from fldAssocYface when it's no longer needed
    // y-min boundry
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocYface( BasisFunctionAreaBase<Quad>::HierarchicalP1, ii*kk );

    fldAssocYface.setGroupLeft( 0 );

    // The canonical traces only need to be setup once per group
    faceL = 1;
    canonicalL.set(faceL, 1);

    int faceB = 0;
    for (int k = 0; k < kk; k++)
    {
      int j = 0;
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 1,
                                    n0 + joffset + 0,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 1,
                                    n0 + koffset + joffset + 0 };

          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

          elemL = (k*jj*ii + j*ii + i);

          fldAssocYface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocYface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocYface.setElementLeft( elemL, faceB );
          fldAssocYface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[2] = new FieldTraceGroupType<Quad>( fldAssocYface );
    boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);
  }


  {
    //Local scope releases memory from fldAssocYface when it's no longer needed
    // y-max boundry
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocYface( BasisFunctionAreaBase<Quad>::HierarchicalP1, ii*kk );

    fldAssocYface.setGroupLeft( 0 );

    // The canonical traces only need to be setup once per group
    faceL = 3;
    canonicalL.set(faceL, 1);

    int faceB = 0;
    for (int k = 0; k < kk; k++)
    {
      int j = jj-1;
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 1,
                                    n0 + joffset + 0,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 1,
                                    n0 + koffset + joffset + 0 };

         for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

          elemL = (k*jj*ii + j*ii + i);

          canonicalL.set(faceL, 1);

          fldAssocYface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocYface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocYface.setElementLeft( elemL, faceB );
          fldAssocYface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[3] = new FieldTraceGroupType<Quad>( fldAssocYface );
    boundaryTraceGroups_[3]->setDOF(DOF_, nDOF_);
  }


  {
    //Local scope releases memory from fldAssocZface when it's no longer needed
    // z-min boundry
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocZface( BasisFunctionAreaBase<Quad>::HierarchicalP1, ii*jj );

    fldAssocZface.setGroupLeft( 0 );

    // The canonical traces only need to be setup once per group
    faceL = 0;
    canonicalL.set(faceL, 1);

    int faceB = 0;
    int k = 0;
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 1,
                                    n0 + joffset + 0,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 1,
                                    n0 + koffset + joffset + 0 };

          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

          elemL = (k*jj*ii + j*ii + i);

          fldAssocZface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocZface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocZface.setElementLeft( elemL, faceB );
          fldAssocZface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[4] = new FieldTraceGroupType<Quad>( fldAssocZface );
    boundaryTraceGroups_[4]->setDOF(DOF_, nDOF_);
  }


  {
    //Local scope releases memory from fldAssocZface when it's no longer needed
    // z-max boundry
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocZface( BasisFunctionAreaBase<Quad>::HierarchicalP1, ii*jj );

    fldAssocZface.setGroupLeft( 0 );

    // The canonical traces only need to be setup once per group
    faceL = 5;
    canonicalL.set(faceL, 1);

    int faceB = 0;
    int k = kk-1;
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 1,
                                    n0 + joffset + 0,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 1,
                                    n0 + koffset + joffset + 0 };

          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

          elemL = (k*jj*ii + j*ii + i);

          fldAssocZface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocZface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocZface.setElementLeft( elemL, faceB );
          fldAssocZface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[5] = new FieldTraceGroupType<Quad>( fldAssocZface );
    boundaryTraceGroups_[5]->setDOF(DOF_, nDOF_);
  }

  {
    //Local scope releases memory from fldAssocLower and fldAssocUpper when they are no longer needed
    // Lower and upper Wing boundary
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocLower( BasisFunctionAreaBase<Quad>::HierarchicalP1, nChord*nSpan );
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocUpper( BasisFunctionAreaBase<Quad>::HierarchicalP1, nChord*nSpan );

    fldAssocLower.setGroupLeft( 0 );
    fldAssocUpper.setGroupLeft( 0 );


    FieldFrameGroupType::FieldAssociativityConstructorType fldAssocKutta( BasisFunctionLineBase::HierarchicalP1, nSpan );

    fldAssocKutta.setGroupLeft( 7 );
    fldAssocKutta.setGroupRight( 6 );

    const int n0 = 0;

    //Nodes that make up the left hex on the upper surface of the wing
    hexnodes = { n0 + 0,
                 n0 + 1,
                 n0 + joffset + 1,
                 n0 + joffset + 0,

                 n0 + koffset + 0,
                 n0 + koffset + 1,
                 n0 + koffset + joffset + 1,
                 n0 + koffset + joffset + 0 };

    // Nodes on the trailing edge
    const int TEedgeL[2] = {n0 + 1, n0 + joffset + 1};

    // The canonical traces only need to be setup once per group
    faceL = 0;

    for (int n = 0; n < 4; n++)
      faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

    CanonicalTraceToCell canonicalFrameL = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTrace(TEedgeL, 2, faceNodes, 4);

    // Nodes on the trailing edge
    const int TEedgeR[2] = {n0 + koffset + 1, n0 + koffset + joffset + 1};

    // The canonical traces only need to be setup once per group
    int faceR = 5;

    for (int n = 0; n < 4; n++)
      faceNodes[n] = hexnodes[ TraceNodes[faceR][n] ];

    CanonicalTraceToCell canonicalFrameR = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTrace(TEedgeR, 2, faceNodes, 4);


    int faceB = 0;
    int k = nWake-1;
    {
      for (int j = nWake; j < jj-nWake; j++)
      {
        for (int i = nWake; i < ii-nWake; i++)
        {
          const int n0L = k*koffset + j*joffset + i;

          const int iW = i-nWake-1;
          const int jW = j-nWake-1;
          const int nWL = nBoxNode + jW*(nChord-1) + iW;
          int nW0 = nWL + 0;
          int nW1 = nWL + 1;
          int nW2 = nWL + (nChord-1) + 1;
          int nW3 = nWL + (nChord-1) + 0;

          if ( i == nWake      )
          {
            nW0 = n0L + koffset + 0;
            nW3 = n0L + koffset + joffset + 0;
          }
          if ( i == ii-nWake-1 ) //TE points
          {
            nW1 = dupPointOffset_ + jW*(nWake+1);
            nW2 = dupPointOffset_ + (jW+1)*(nWake+1);
          }

          if ( j == nWake      )
          {
            nW0 = n0L + koffset + 0;
            nW1 = n0L + koffset + 1;
          }
          if ( j == jj-nWake-1 )
          {
            nW2 = n0L + koffset + joffset + 1;
            nW3 = n0L + koffset + joffset + 0;
          }

          //All the nodes that make up the lower hex
          const int hexnodesL[8] = { n0L + 0,
                                     n0L + 1,
                                     n0L + joffset + 1,
                                     n0L + joffset + 0,

                                     nW0, nW1, nW2, nW3 };

          // The canonical trace
          faceL = 5;
          canonicalL.set(faceL, 1);

          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodesL[ TraceNodes[faceL][n] ];

          elemL = (k*jj*ii + j*ii + i);

          fldAssocLower.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocLower.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocLower.setElementLeft( elemL, faceB );
          fldAssocLower.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );

          // Drela-Kutta condition associativity
          if ( i == nWake+nChord-1 && j > nWake )
          {
            const int TEedge[2] = {nW1, nW2};
            KuttaBCTraceElemRight_[6][TEedge[0]].KuttaPoint = nW1;
            KuttaBCTraceElemRight_[6][TEedge[0]].elems.push_back(faceB);
          }
          if ( i == nWake+nChord-1 && j < jj-nWake-1)
          {
            const int TEedge[2] = {nW1, nW2};
            KuttaBCTraceElemRight_[6][TEedge[1]].KuttaPoint = nW2;
            KuttaBCTraceElemRight_[6][TEedge[1]].elems.push_back(faceB);
          }



          const int n0U = (k+1)*koffset + j*joffset + i;

          //All the nodes that make up the upper hex
          const int hexnodesU[8] = { n0U + 0,
                                     n0U + 1,
                                     n0U + joffset + 1,
                                     n0U + joffset + 0,

                                     n0U + koffset + 0,
                                     n0U + koffset + 1,
                                     n0U + koffset + joffset + 1,
                                     n0U + koffset + joffset + 0 };

          // The canonical trace
          faceL = 0;
          canonicalL.set(faceL, 1);

          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodesU[ TraceNodes[faceL][n] ];

          elemL = ((k+1)*jj*ii + j*ii + i);

          fldAssocUpper.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocUpper.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocUpper.setElementLeft( elemL, faceB );
          fldAssocUpper.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );

          // Drela-Kutta condition associativity
          if ( i == nWake+nChord-1 && j > nWake )
          {
            const int TEedge[2] = {n0U + 1, n0U + joffset + 1};
            KuttaBCTraceElemLeft_[7][TEedge[0]].KuttaPoint = nW1;
            KuttaBCTraceElemLeft_[7][TEedge[0]].elems.push_back(faceB);
          }
          if ( i == nWake+nChord-1 && j < jj-nWake-1)
          {
            const int TEedge[2] = {n0U + 1, n0U + joffset + 1};
            KuttaBCTraceElemLeft_[7][TEedge[1]].KuttaPoint = nW2;
            KuttaBCTraceElemLeft_[7][TEedge[1]].elems.push_back(faceB);
          }



          // Set the Kutta condition associativity
          if ( i == nWake+nChord-1 )
          {
            const int TEedgeU[2] = {n0U + 1, n0U + joffset + 1};
            int edgeK = j-nWake;

            fldAssocKutta.setAssociativity( edgeK ).setRank( comm_rank );
            fldAssocKutta.setAssociativity( edgeK ).setNodeGlobalMapping( TEedgeU, 2 );
            fldAssocKutta.setElementLeft( faceB, edgeK );
            fldAssocKutta.setElementRight( faceB, edgeK );
            fldAssocKutta.setCanonicalTraceLeft( canonicalFrameL, edgeK );
            fldAssocKutta.setCanonicalTraceRight( canonicalFrameR, edgeK );
          }

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[6] = new FieldTraceGroupType<Quad>( fldAssocLower );
    boundaryTraceGroups_[6]->setDOF(DOF_, nDOF_);

    boundaryTraceGroups_[7] = new FieldTraceGroupType<Quad>( fldAssocUpper );
    boundaryTraceGroups_[7]->setDOF(DOF_, nDOF_);

    if (!adjoint)
    {
      boundaryFrameGroups_[0] = new FieldFrameGroupType( fldAssocKutta );
      boundaryFrameGroups_[0]->setDOF(DOF_, nDOF_);
    }
  }



  {
    //Local scope releases memory from fldAssocWake when it's no longer needed
    // Lower and upper Wing boundary
    FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocWake( BasisFunctionAreaBase<Quad>::HierarchicalP1, nWake*nSpan );

    fldAssocWake.setGroupLeft( 0 );
    fldAssocWake.setGroupRight( 0 );

    FieldFrameGroupType::FieldAssociativityConstructorType fldAssocKutta( BasisFunctionLineBase::HierarchicalP1, nSpan );

    fldAssocKutta.setGroupLeft( 8 );

    FieldFrameGroupType::FieldAssociativityConstructorType fldAssocTrefftz( BasisFunctionLineBase::HierarchicalP1, nSpan );

    fldAssocTrefftz.setGroupLeft( 8 );

    const int n0L = koffset;

    //Nodes that make up the left hex
    const int hexnodesL[8] = { n0L + 0,
                               n0L + 1,
                               n0L + joffset + 1,
                               n0L + joffset + 0,

                               n0L + koffset + 0,
                               n0L + koffset + 1,
                               n0L + koffset + joffset + 1,
                               n0L + koffset + joffset + 0 };


    const int n0R = 0;

    //Nodes that make up the right hex
    const int hexnodesR[8] = { n0R + 0,
                               n0R + 1,
                               n0R + joffset + 1,
                               n0R + joffset + 0,

                               n0R + koffset + 0,
                               n0R + koffset + 1,
                               n0R + koffset + joffset + 1,
                               n0R + koffset + joffset + 0 };

    // The canonical traces only need to be setup once per group
    faceL = 0;

    for (int n = 0; n < 4; n++)
      faceNodes[n] = hexnodesL[ TraceNodes[faceL][n] ];

    canonicalL.set(faceL, 1);
    canonicalR = TraceToCellRefCoord<Quad, TopoD3, Hex>::getCanonicalTrace(faceNodes, 4, hexnodesR, 8);

    // Nodes on the trailing edge
    const int edgeK[2] = {n0L + 0, n0L + joffset + 0};

    CanonicalTraceToCell canonicalFrameK = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTrace(edgeK, 2, faceNodes, 4);

    // Nodes on the outflow edge
    const int edgeT[2] = {n0L + 1, n0L + joffset + 1};

    CanonicalTraceToCell canonicalFrameT = TraceToCellRefCoord<Line, TopoD2, Quad>::getCanonicalTrace(edgeT, 2, faceNodes, 4);

    int faceB = 0;
    int k = nWake-1;
    {
      for (int j = nWake; j < jj-nWake; j++)
      {
        for (int i = nWake+nChord; i < ii; i++)
        {
          const int n0L = (k+1)*koffset + j*joffset + i;

          const int hexnodesL[8] = { n0L + 0,
                                     n0L + 1,
                                     n0L + joffset + 1,
                                     n0L + joffset + 0,

                                     n0L + koffset + 0,
                                     n0L + koffset + 1,
                                     n0L + koffset + joffset + 1,
                                     n0L + koffset + joffset + 0 };


          for (int n = 0; n < 4; n++)
            faceNodes[n] = hexnodesL[ TraceNodes[faceL][n] ];

          elemL = ((k+1)*jj*ii + j*ii + i);
          elemR = ((k+0)*jj*ii + j*ii + i);

          fldAssocWake.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocWake.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 4 );
          fldAssocWake.setElementLeft( elemL, faceB );
          fldAssocWake.setElementRight( elemR, faceB );
          fldAssocWake.setCanonicalTraceLeft( canonicalL, faceB );
          fldAssocWake.setCanonicalTraceRight( canonicalR, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, canonicalL.trace );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( -1, canonicalR.trace );

          // Set the Kutta condition associativity
          if ( i == nWake+nChord+1 )
          {
            const int edge[2] = {n0L + 0, n0L + joffset + 0};
            int edgeK = j-nWake;

            fldAssocKutta.setAssociativity( edgeK ).setRank( comm_rank );
            fldAssocKutta.setAssociativity( edgeK ).setNodeGlobalMapping( edge, 2 );
            fldAssocKutta.setElementLeft( faceB, edgeK );
            fldAssocKutta.setCanonicalTraceLeft( canonicalFrameK, edgeK );
          }

          // Set the Trefftz plane associativity
          if ( i == ii-1 )
          {
            int edgeT = j-nWake;

            const int edge[2] = {n0L + 1, n0L + joffset + 1};

            fldAssocTrefftz.setAssociativity( edgeT ).setRank( comm_rank );
            fldAssocTrefftz.setAssociativity( edgeT ).setNodeGlobalMapping( edge, 2 );
            fldAssocTrefftz.setElementLeft( faceB, edgeT );
            fldAssocTrefftz.setCanonicalTraceLeft( canonicalFrameT, edgeT );
          }

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[8] = new FieldTraceGroupType<Quad>( fldAssocWake );
    boundaryTraceGroups_[8]->setDOF(DOF_, nDOF_);

    if (adjoint)
    {
      boundaryFrameGroups_[0] = new FieldFrameGroupType( fldAssocKutta );
      boundaryFrameGroups_[0]->setDOF(DOF_, nDOF_);
    }

    boundaryFrameGroups_[1] = new FieldFrameGroupType( fldAssocTrefftz );
    boundaryFrameGroups_[1]->setDOF(DOF_, nDOF_);
  }


  //Finally create the cell groups now that all Face signs have been set
  cellGroups_[0] = new FieldCellGroupType<Hex>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);


#if 0
  {
    //Local scope releases memory from fldAssocCellLeft and fldAssocCellRight when they are no longer needed
    // Lower (right) and upper (left) cell's along the trailing edge
    FieldCellGroupType<Hex>::FieldAssociativityConstructorType fldAssocCellLeft( BasisFunctionVolumeBase<Hex>::HierarchicalP1, 2*(nSpan+0) );
    FieldCellGroupType<Hex>::FieldAssociativityConstructorType fldAssocCellRight( BasisFunctionVolumeBase<Hex>::HierarchicalP1, 2*(nSpan+0) );


    int cellL = 0;
    int cellR = 0;
    std::vector<int> hexnodesL;
    int k = nWake-1;
    {
      for (int j = nWake; j < jj-nWake; j++)
      {
        for (int i = nWake+nChord-1; i < ii-nWake+1; i++)
        {
          const int n0L = k*koffset + j*joffset + i;

          //All the nodes that make up an individual hex
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake && i < ii-nWake) )
          {
            const int iW = i-nWake-1;
            const int jW = j-nWake-1;
            const int nWL = nBoxNode + jW*(nChord-1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nChord-1) + 1;
            int nW3 = nWL + (nChord-1) + 0;

            if ( i == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW3 = n0L + koffset + joffset + 0;
            }
            if ( i == ii-nWake-1 ) //TE points
            {
              nW1 = dupPointOffset_ + jW*(nWake+1);
              nW2 = dupPointOffset_ + (jW+1)*(nWake+1);
            }

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 1;
              nW3 = n0L + koffset + joffset + 0;
            }


            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 1,
                          n0L + joffset + 0,

                          nW0, nW1, nW2, nW3 };
          }
          else if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= ii-nWake) )
          {
            const int iW = i-nWake-nChord;
            const int jW = j-nWake-1;
            const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nWake+1) + 1;
            int nW3 = nWL + (nWake+1) + 0;

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 1;
              nW3 = n0L + koffset + joffset + 0;
            }

            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 1,
                          n0L + joffset + 0,

                          nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 1,
                          n0L + joffset + 0,

                          n0L + koffset + 0,
                          n0L + koffset + 1,
                          n0L + koffset + joffset + 1,
                          n0L + koffset + joffset + 0 };
          }


          fldAssocCellRight.setAssociativity( cellR ).setNodeGlobalMapping( &hexnodesL[0], 8 );
          cellR++;

          const int n0U = (k+1)*koffset + j*joffset + i;

          //All the nodes that make up the upper hex
          const int hexnodesU[8] = { n0U + 0,
                                     n0U + 1,
                                     n0U + joffset + 1,
                                     n0U + joffset + 0,

                                     n0U + koffset + 0,
                                     n0U + koffset + 1,
                                     n0U + koffset + joffset + 1,
                                     n0U + koffset + joffset + 0 };

          fldAssocCellLeft.setAssociativity( cellL ).setNodeGlobalMapping( hexnodesU, 8 );
          cellL++;

        }
      }
    }

    //Create the volume groups
    cellGroups_[1] = new FieldCellGroupType<Hex>( fldAssocCellLeft );
    cellGroups_[1]->setDOF(DOF_, nDOF_);

    cellGroups_[2] = new FieldCellGroupType<Hex>( fldAssocCellRight );
    cellGroups_[2]->setDOF(DOF_, nDOF_);
  }
#endif


  //Check that the grid is correct
  checkGrid();

}

}
