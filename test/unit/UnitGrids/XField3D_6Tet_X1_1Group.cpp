// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField3D_6Tet_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
// Node ordering of the original hex
/*
//         y
//  2----------3
//  |\     ^   |\
//  | \    |   | \
//  |  \   |   |  \
//  |   6------+---7
//  |   |  +-- |-- | -> x
//  0---+---\--1   |
//   \  |    \  \  |
//    \ |     \  \ |
//     \|      z  \|
//      4----------5
//
// First split hex into two prism, then divide each prism into 3 tets
//
// Nodes that make up each of the 6 hexahedron:
//
// Left prism
// Tet[0]: {0, 1, 2, 4}
// Tet[1]: {1, 4, 3, 2}
// Tet[2]: {6, 2, 3, 4}
//
// Right prism
// Tet[3]: {1, 4, 5, 3}
// Tet[4]: {4, 6, 5, 3}
// Tet[5]: {7, 6, 3, 5}
//
// The specific ordering for each tetrahedron is chosen to give positive volumes,
// but without any other consideration
*/

XField3D_6Tet_X1_1Group::XField3D_6Tet_X1_1Group()
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(8);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(1);
  resizeCellGroups(1);

  // nodal coordinates for the two triangles.
  DOF(0) = {0, 0, 0};
  DOF(1) = {1, 0, 0};
  DOF(2) = {0, 1, 0};
  DOF(3) = {1, 1, 0};

  DOF(4) = {0, 0, 1};
  DOF(5) = {1, 0, 1};
  DOF(6) = {0, 1, 1};
  DOF(7) = {1, 1, 1};

  // volume field variable
  FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1, 6 );

  // Index table for each tet in the hex
  const int tets[6][4] = { {0, 1, 2, 4},
                           {1, 4, 3, 2},
                           {6, 2, 3, 4},

                           {1, 4, 5, 3},
                           {4, 6, 5, 3},
                           {7, 6, 3, 5} };


  // set the cell processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 2 ).setRank( comm_rank );

  fldAssocCell.setAssociativity( 3 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 4 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 5 ).setRank( comm_rank );

  //element volume associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( tets[0], 4 );
  fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( tets[1], 4 );
  fldAssocCell.setAssociativity( 2 ).setNodeGlobalMapping( tets[2], 4 );

  fldAssocCell.setAssociativity( 3 ).setNodeGlobalMapping( tets[3], 4 );
  fldAssocCell.setAssociativity( 4 ).setNodeGlobalMapping( tets[4], 4 );
  fldAssocCell.setAssociativity( 5 ).setNodeGlobalMapping( tets[5], 4 );

  nElem_ = fldAssocCell.nElem();

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  // interior-face field variable

  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocIface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 6 );

  fldAssocIface.setGroupLeft( 0 );
  fldAssocIface.setGroupRight( 0 );

  // face-cell associativity
  int faceNodes[3];
  int elemL, elemR;
  int faceL, faceR;

  // Two faces inside the left prism
  elemL = 0; faceL = 0;
  elemR = 1; faceR = 2;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocIface.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 0 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 0 );
  fldAssocIface.setElementRight( elemR, 0 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 0 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 0 );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(0).orientation, faceR );

  elemL = 1; faceL = 0;
  elemR = 2; faceR = 0;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocIface.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 1 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 1 );
  fldAssocIface.setElementRight( elemR, 1 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 1 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 1 );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(1).orientation, faceR );


  //Two faces connecting the prisms
  elemL = 1; faceL = 3;
  elemR = 3; faceR = 2;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocIface.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 2 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 2 );
  fldAssocIface.setElementRight( elemR, 2 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 2 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 2 );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(2).orientation, faceR );

  elemL = 2; faceL = 1;
  elemR = 4; faceR = 2;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocIface.setAssociativity( 3 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 3 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 3 );
  fldAssocIface.setElementRight( elemR, 3 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 3 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 3 );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(3).orientation, faceR );


  //Two faces in the right prism
  elemL = 3; faceL = 0;
  elemR = 4; faceR = 1;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocIface.setAssociativity( 4 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 4 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 4 );
  fldAssocIface.setElementRight( elemR, 4 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 4 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 4 );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(4).orientation, faceR );

  elemL = 4; faceL = 0;
  elemR = 5; faceR = 0;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocIface.setAssociativity( 5 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 5 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 5 );
  fldAssocIface.setElementRight( elemR, 5 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 5 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 5 );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(5).orientation, faceR );

  //Create the interior face group
  interiorTraceGroups_[0] = new FieldTraceGroupType<Triangle>( fldAssocIface );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);


  // boundary-face field variable
  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 12 );

  fldAssocBface.setGroupLeft( 0 );

  int faceB = 0;

  elemL = 0; faceL = 1;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;

  elemL = 0; faceL = 2;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;

  elemL = 0; faceL = 3;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;


  elemL = 1; faceL = 1;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;


  elemL = 2; faceL = 2;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;

  elemL = 2; faceL = 3;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;


  elemL = 3; faceL = 1;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;

  elemL = 3; faceL = 3;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;


  elemL = 4; faceL = 3;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;


  elemL = 5; faceL = 1;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;

  elemL = 5; faceL = 2;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;

  elemL = 5; faceL = 3;
  for (int i = 0; i < 3; i++)
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

  fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
  fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocBface.setElementLeft( elemL, faceB );
  fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  faceB++;

  //Must be 12 boundary faces
  SANS_ASSERT( faceB == 12 );

  //Create the boundary group
  boundaryTraceGroups_[0] = new FieldTraceGroupType<Triangle>( fldAssocBface );
  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);

  //Finally create the cell groups now that all Face signs have been set
  cellGroups_[0] = new FieldCellGroupType<Tet>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
