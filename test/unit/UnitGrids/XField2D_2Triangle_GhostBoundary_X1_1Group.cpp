// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_2Triangle_GhostBoundary_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//  2 ----- 3
//  |\      |
//  | \ (1) |
//  |  \    |
//  |   \   |
//  |    \  |
//  | (0) \ |
//  |      \|
//  0 ----- 1

XField2D_2Triangle_GhostBoundary_X1_1Group::XField2D_2Triangle_GhostBoundary_X1_1Group(const bool NorthWest )
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(4);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(1);
  resizeGhostBoundaryTraceGroups(1);
  resizeCellGroups(1);

  // nodal coordinates for the two triangles.
  DOF(0) = {0, 0};
  DOF(1) = {1, 0};
  DOF(2) = {0, 1};
  DOF(3) = {1, 1};

  // area field variable
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 2 );

  // set the cell processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 1 ).setRank( comm_rank );

  //element area associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );
  fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {3, 2, 1} );

  // edge signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );
  fldAssocCell.setAssociativity( 1 ).setEdgeSign( -1, 0 );

  FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
  cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

  xfldElementGroup->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // interior-edge field variable

  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, 1 );

  // edge-element processor rank
  fldAssocIedge.setAssociativity( 0 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocIedge.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );

  // edge-to-cell connectivity
  fldAssocIedge.setGroupLeft( 0 );
  fldAssocIedge.setGroupRight( 0 );
  fldAssocIedge.setElementLeft( 0, 0 );
  fldAssocIedge.setElementRight( 1, 0 );
  fldAssocIedge.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), 0 );

  FieldTraceGroupType<Line>* xfldIedge = NULL;
  interiorTraceGroups_[0] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );

  xfldIedge->setDOF(DOF_, nDOF_);

  if (NorthWest) // set the two north west boundaries to ghost
  {
    // boundary-edge field variable
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, 2 );

    // boundary edge processor ranks
    fldAssocBedge.setAssociativity( 0 ).setRank( comm_rank );
    fldAssocBedge.setAssociativity( 1 ).setRank( comm_rank );
  //  fldAssocBedge.setAssociativity( 2 ).setRank( comm_rank );
  //  fldAssocBedge.setAssociativity( 3 ).setRank( comm_rank );

    // edge-element associativity
    fldAssocBedge.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocBedge.setAssociativity( 1 ).setNodeGlobalMapping( {1, 3} );
  //  fldAssocBedge.setAssociativity( 2 ).setNodeGlobalMapping( {3, 2} );
  //  fldAssocBedge.setAssociativity( 3 ).setNodeGlobalMapping( {2, 0} );

    // edge-to-cell connectivity
    fldAssocBedge.setGroupLeft( 0 );
    fldAssocBedge.setElementLeft( 0, 0 );
    fldAssocBedge.setElementLeft( 1, 1 );
  //  fldAssocBedge.setElementLeft( 1, 2 );
  //  fldAssocBedge.setElementLeft( 0, 3 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );
  //  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 2 );
  //  fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 3 );

    FieldTraceGroupType<Line>* xfldBedge = NULL;
    boundaryTraceGroups_[0] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);

    // ghost boundary-edge field variable

    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocGBedge( BasisFunctionLineBase::HierarchicalP1, 2 );

    // ghost boundary edge processor ranks
    fldAssocGBedge.setAssociativity( 0 ).setRank( comm_rank );
    fldAssocGBedge.setAssociativity( 1 ).setRank( comm_rank );

    // edge-element associativity
    fldAssocGBedge.setAssociativity( 0 ).setNodeGlobalMapping( {3, 2} );
    fldAssocGBedge.setAssociativity( 1 ).setNodeGlobalMapping( {2, 0} );

    // edge-to-cell connectivity
    fldAssocGBedge.setGroupLeft( 0 );
    fldAssocGBedge.setElementLeft( 1, 0 );
    fldAssocGBedge.setElementLeft( 0, 1 );
    fldAssocGBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
    fldAssocGBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );

    FieldTraceGroupType<Line>* xfldGBedge = NULL;
    ghostBoundaryTraceGroups_[0] = xfldGBedge = new FieldTraceGroupType<Line>( fldAssocGBedge );

    xfldGBedge->setDOF(DOF_, nDOF_);
  }
  else
  {
    // boundary-edge field variable
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, 2 );

    // boundary edge processor ranks
    fldAssocBedge.setAssociativity( 0 ).setRank( comm_rank );
//    fldAssocBedge.setAssociativity( 1 ).setRank( comm_rank );
//    fldAssocBedge.setAssociativity( 2 ).setRank( comm_rank );
    fldAssocBedge.setAssociativity( 1 ).setRank( comm_rank );

    // edge-element associativity
    fldAssocBedge.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
//    fldAssocBedge.setAssociativity( 1 ).setNodeGlobalMapping( {1, 3} );
//    fldAssocBedge.setAssociativity( 2 ).setNodeGlobalMapping( {3, 2} );
    fldAssocBedge.setAssociativity( 1 ).setNodeGlobalMapping( {2, 0} );

    // edge-to-cell connectivity
    fldAssocBedge.setGroupLeft( 0 );
    fldAssocBedge.setElementLeft( 0, 0 );
//    fldAssocBedge.setElementLeft( 1, 1 );
//    fldAssocBedge.setElementLeft( 1, 2 );
    fldAssocBedge.setElementLeft( 0, 1 );

    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
//    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );
//    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 2 );
    fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 1 );

    FieldTraceGroupType<Line>* xfldBedge = NULL;
    boundaryTraceGroups_[0] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);

    // ghost boundary-edge field variable

    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocGBedge( BasisFunctionLineBase::HierarchicalP1, 2 );

    // ghost boundary edge processor ranks
    fldAssocGBedge.setAssociativity( 0 ).setRank( comm_rank );
    fldAssocGBedge.setAssociativity( 1 ).setRank( comm_rank );

    // edge-element associativity
    fldAssocGBedge.setAssociativity( 0 ).setNodeGlobalMapping( {1, 3} );
    fldAssocGBedge.setAssociativity( 1 ).setNodeGlobalMapping( {3, 2} );

    // edge-to-cell connectivity
    fldAssocGBedge.setGroupLeft( 0 );
    fldAssocGBedge.setElementLeft( 1, 0 );
    fldAssocGBedge.setElementLeft( 1, 1 );
    fldAssocGBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );
    fldAssocGBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 1 );

    FieldTraceGroupType<Line>* xfldGBedge = NULL;
    ghostBoundaryTraceGroups_[0] = xfldGBedge = new FieldTraceGroupType<Line>( fldAssocGBedge );

    xfldGBedge->setDOF(DOF_, nDOF_);
  }

  //Check that the grid is correct
  checkGrid();
}

}
