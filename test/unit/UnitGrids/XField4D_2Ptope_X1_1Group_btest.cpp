// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField4D_2Ptope_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/linspace.h"

#include "XField4D_2Ptope_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"

#include "unit/Field/XField4D_CheckTraceCoord4D_btest.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField4D_2Ptope_X1_1Group_test )
{

  const Real small_tol= 1e-13;
  const Real tol= 1e-13;

  typedef std::array<int, 5> Int5;

  XField4D_2Ptope_X1_1Group xfld;

  // should be two 5 DOF pentatopes wnich share one tetrahedral face (6 total DOFs)
  BOOST_CHECK_EQUAL(xfld.nDOF(), 6);

  // check the nodal values
  BOOST_CHECK_EQUAL(xfld.DOF(0)[0], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(0)[1], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(0)[2], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(0)[3], 0);

  BOOST_CHECK_EQUAL(xfld.DOF(1)[0], 1);
  BOOST_CHECK_EQUAL(xfld.DOF(1)[1], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(1)[2], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(1)[3], 0);

  BOOST_CHECK_EQUAL(xfld.DOF(2)[0], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(2)[1], 1);
  BOOST_CHECK_EQUAL(xfld.DOF(2)[2], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(2)[3], 0);

  BOOST_CHECK_EQUAL(xfld.DOF(3)[0], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(3)[1], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(3)[2], 1);
  BOOST_CHECK_EQUAL(xfld.DOF(3)[3], 0);

  BOOST_CHECK_EQUAL(xfld.DOF(4)[0], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(4)[1], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(4)[2], 0);
  BOOST_CHECK_EQUAL(xfld.DOF(4)[3], 1);

  // area field variable

  // should be one group, composed of pentatopes
  BOOST_CHECK_EQUAL(xfld.nCellGroups(), 1);
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  const XField4D_2Ptope_X1_1Group::FieldCellGroupType<Pentatope> &xfldVolume= xfld.getCellGroup<Pentatope>(0);

  int nodeMap[5];

  xfldVolume.associativity(0).getNodeGlobalMapping(nodeMap, 5);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 2);
  BOOST_CHECK_EQUAL(nodeMap[3], 3);
  BOOST_CHECK_EQUAL(nodeMap[4], 4);

  xfldVolume.associativity(1).getNodeGlobalMapping(nodeMap, 5);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 5);
  BOOST_CHECK_EQUAL(nodeMap[2], 2);
  BOOST_CHECK_EQUAL(nodeMap[3], 4);
  BOOST_CHECK_EQUAL(nodeMap[4], 3);

  Int5 faceSign;

  faceSign= xfldVolume.associativity(0).faceSign();
  BOOST_CHECK_EQUAL(faceSign[0], +1);
  BOOST_CHECK_EQUAL(faceSign[1], +1);
  BOOST_CHECK_EQUAL(faceSign[2], +1);
  BOOST_CHECK_EQUAL(faceSign[3], +1);
  BOOST_CHECK_EQUAL(faceSign[4], +1);

  faceSign= xfldVolume.associativity(1).faceSign();
  BOOST_CHECK_EQUAL(faceSign[0], +1);
  BOOST_CHECK_EQUAL(faceSign[1], -1);
  BOOST_CHECK_EQUAL(faceSign[2], +1);
  BOOST_CHECK_EQUAL(faceSign[3], +1);
  BOOST_CHECK_EQUAL(faceSign[4], +1);

  // interior-edge field variable

  BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 1);
  BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Tet));
  const XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldIface= xfld.getInteriorTraceGroup<Tet>(0);

  xfldIface.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 2);
  BOOST_CHECK_EQUAL(nodeMap[2], 3);
  BOOST_CHECK_EQUAL(nodeMap[3], 4);

  BOOST_CHECK_EQUAL(xfldIface.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldIface.getGroupRight(), 0);

  BOOST_CHECK_EQUAL(xfldIface.getElementLeft(0), 0);
  BOOST_CHECK_EQUAL(xfldIface.getElementRight(0), 1);

  BOOST_CHECK_EQUAL(xfldIface.getCanonicalTraceLeft(0).trace, 1);
  BOOST_CHECK_EQUAL(xfldIface.getCanonicalTraceRight(0).trace, 1);

  // boundary-edge field variable

  BOOST_CHECK_EQUAL(xfld.nBoundaryTraceGroups(), 8);
  BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(2).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(3).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(4).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(5).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(6).topoTypeID() == typeid(Tet));
  BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(7).topoTypeID() == typeid(Tet));

  const XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBface0= xfld.getBoundaryTraceGroup<Tet>(0);
  const XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBface1= xfld.getBoundaryTraceGroup<Tet>(1);
  const XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBface2= xfld.getBoundaryTraceGroup<Tet>(2);
  const XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBface3= xfld.getBoundaryTraceGroup<Tet>(3);
  const XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBface4= xfld.getBoundaryTraceGroup<Tet>(4);
  const XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBface5= xfld.getBoundaryTraceGroup<Tet>(5);
  const XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBface6= xfld.getBoundaryTraceGroup<Tet>(6);
  const XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet> &xfldBface7= xfld.getBoundaryTraceGroup<Tet>(7);

  xfldBface0.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 1);
  BOOST_CHECK_EQUAL(nodeMap[1], 2);
  BOOST_CHECK_EQUAL(nodeMap[2], 4);
  BOOST_CHECK_EQUAL(nodeMap[3], 3);

  xfldBface1.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 4);
  BOOST_CHECK_EQUAL(nodeMap[3], 3);

  xfldBface2.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 2);
  BOOST_CHECK_EQUAL(nodeMap[3], 4);

  xfldBface3.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 1);
  BOOST_CHECK_EQUAL(nodeMap[2], 3);
  BOOST_CHECK_EQUAL(nodeMap[3], 2);

  xfldBface4.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 5);
  BOOST_CHECK_EQUAL(nodeMap[1], 2);
  BOOST_CHECK_EQUAL(nodeMap[2], 3);
  BOOST_CHECK_EQUAL(nodeMap[3], 4);

  xfldBface5.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 5);
  BOOST_CHECK_EQUAL(nodeMap[2], 3);
  BOOST_CHECK_EQUAL(nodeMap[3], 4);

  xfldBface6.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 5);
  BOOST_CHECK_EQUAL(nodeMap[2], 2);
  BOOST_CHECK_EQUAL(nodeMap[3], 3);

  xfldBface7.associativity(0).getNodeGlobalMapping(nodeMap, 4);
  BOOST_CHECK_EQUAL(nodeMap[0], 0);
  BOOST_CHECK_EQUAL(nodeMap[1], 5);
  BOOST_CHECK_EQUAL(nodeMap[2], 4);
  BOOST_CHECK_EQUAL(nodeMap[3], 2);

  XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet>::ElementType<> xfldElem(xfldBface0.basis());
  XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet>::ElementType<>::RefCoordType sRef;
  XField4D_2Ptope_X1_1Group::FieldTraceGroupType<Tet>::ElementType<>::VectorX N;

  // check areas and unit normals
  Real nxTrue;
  Real nyTrue;
  Real nzTrue;
  Real nwTrue;
  sRef= 1.0/4.0;

  xfldBface0.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/3.0, 1e-12);

  nxTrue= 0.5;
  nyTrue= 0.5;
  nzTrue= 0.5;
  nwTrue= 0.5;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(nwTrue, N[3], small_tol, tol);

  xfldBface1.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/6.0, 1e-12);

  nxTrue= 0.0;
  nyTrue= -1.0;
  nzTrue= 0.0;
  nwTrue= 0.0;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(nwTrue, N[3], small_tol, tol);

  xfldBface2.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/6.0, 1e-12);

  nxTrue= 0.0;
  nyTrue= 0.0;
  nzTrue= -1.0;
  nwTrue= 0.0;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(nwTrue, N[3], small_tol, tol);

  xfldBface3.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/6.0, 1e-12);

  nxTrue= 0.0;
  nyTrue= 0.0;
  nzTrue= 0.0;
  nwTrue= -1.0;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(nwTrue, N[3], small_tol, tol);

  xfldBface4.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/3.0, 1e-12);

  nxTrue= -0.5;
  nyTrue= 0.5;
  nzTrue= 0.5;
  nwTrue= 0.5;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(nwTrue, N[3], small_tol, tol);

  xfldBface5.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/6.0, 1e-12);

  nxTrue= 0.0;
  nyTrue= -1.0;
  nzTrue= 0.0;
  nwTrue= 0.0;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(nwTrue, N[3], small_tol, tol);

  xfldBface6.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/6.0, 1e-12);

  nxTrue= 0.0;
  nyTrue= 0.0;
  nzTrue= 0.0;
  nwTrue= -1.0;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(nwTrue, N[3], small_tol, tol);

  xfldBface7.getElement(xfldElem, 0);
  BOOST_CHECK_CLOSE(xfldElem.volume(), 1.0/6.0, 1e-12);

  nxTrue= 0.0;
  nyTrue= 0.0;
  nzTrue= -1.0;
  nwTrue= 0.0;

  xfldElem.unitNormal(sRef, N);
  SANS_CHECK_CLOSE(nxTrue, N[0], small_tol, tol);
  SANS_CHECK_CLOSE(nyTrue, N[1], small_tol, tol);
  SANS_CHECK_CLOSE(nzTrue, N[2], small_tol, tol);
  SANS_CHECK_CLOSE(nwTrue, N[3], small_tol, tol);

  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL(xfldBface0.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldBface1.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldBface2.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldBface3.getGroupLeft(), 0);

  BOOST_CHECK_EQUAL(xfldBface4.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldBface5.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldBface6.getGroupLeft(), 0);
  BOOST_CHECK_EQUAL(xfldBface7.getGroupLeft(), 0);

  BOOST_CHECK_EQUAL(xfldBface0.getElementLeft(0), 0);
  BOOST_CHECK_EQUAL(xfldBface1.getElementLeft(0), 0);
  BOOST_CHECK_EQUAL(xfldBface2.getElementLeft(0), 0);
  BOOST_CHECK_EQUAL(xfldBface3.getElementLeft(0), 0);

  BOOST_CHECK_EQUAL(xfldBface4.getElementLeft(0), 1);
  BOOST_CHECK_EQUAL(xfldBface5.getElementLeft(0), 1);
  BOOST_CHECK_EQUAL(xfldBface6.getElementLeft(0), 1);
  BOOST_CHECK_EQUAL(xfldBface7.getElementLeft(0), 1);

  BOOST_CHECK_EQUAL(xfldBface0.getCanonicalTraceLeft(0).trace, 0);
  BOOST_CHECK_EQUAL(xfldBface1.getCanonicalTraceLeft(0).trace, 2);
  BOOST_CHECK_EQUAL(xfldBface2.getCanonicalTraceLeft(0).trace, 3);
  BOOST_CHECK_EQUAL(xfldBface3.getCanonicalTraceLeft(0).trace, 4);

  BOOST_CHECK_EQUAL(xfldBface4.getCanonicalTraceLeft(0).trace, 0);
  BOOST_CHECK_EQUAL(xfldBface5.getCanonicalTraceLeft(0).trace, 2);
  BOOST_CHECK_EQUAL(xfldBface6.getCanonicalTraceLeft(0).trace, 3);
  BOOST_CHECK_EQUAL(xfldBface7.getCanonicalTraceLeft(0).trace, 4);

  BOOST_CHECK_EQUAL(xfldBface0.getCanonicalTraceLeft(0).orientation, 1);
  BOOST_CHECK_EQUAL(xfldBface1.getCanonicalTraceLeft(0).orientation, 1);
  BOOST_CHECK_EQUAL(xfldBface2.getCanonicalTraceLeft(0).orientation, 1);
  BOOST_CHECK_EQUAL(xfldBface3.getCanonicalTraceLeft(0).orientation, 1);

  BOOST_CHECK_EQUAL(xfldBface4.getCanonicalTraceLeft(0).orientation, 1);
  BOOST_CHECK_EQUAL(xfldBface5.getCanonicalTraceLeft(0).orientation, 1);
  BOOST_CHECK_EQUAL(xfldBface6.getCanonicalTraceLeft(0).orientation, 1);
  BOOST_CHECK_EQUAL(xfldBface7.getCanonicalTraceLeft(0).orientation, 1);

}

BOOST_AUTO_TEST_CASE( XField4D_2Ptope_X1_1Group_checktrace_test )
{
  XField4D_2Ptope_X1_1Group xfld;

  BOOST_CHECK_EQUAL(xfld.nCellGroups(), 1);
  BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 1);
  BOOST_CHECK_EQUAL(xfld.getCellGroupBase(0).nElem(), 2);
  BOOST_CHECK_EQUAL(xfld.nBoundaryTraceGroups(), 8);

  const Real small_tol= 1e-11;
  const Real close_tol= 1e-11;

  for_each_InteriorFieldTraceGroup_Cell<TopoD4>::apply(
      CheckInteriorTraceCoordinates4D(small_tol, close_tol, linspace(0, xfld.nInteriorTraceGroups() - 1)), xfld, xfld);

  for_each_BoundaryFieldTraceGroup_Cell<TopoD4>::apply(
      CheckBoundaryTraceCoordinates4D(small_tol, close_tol, linspace(0, xfld.nBoundaryTraceGroups() - 1)), xfld, xfld);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
