// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_5Tet_X1_1Group_AllOrientations.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/TraceToCellRefCoord.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_5Tet_X1_1Group_AllOrientations_Negative_test )
{
  typedef std::array<int,4> Int4;

  //Original configuration: center tet has orientation 1, all neighbors have orientation -1
  const int tets_orient1[5][4] = { {0, 1, 2, 3},
                                   {1, 2, 3, 4},
                                   {0, 3, 2, 5},
                                   {0, 1, 3, 6},
                                   {0, 2, 1, 7}};

  //loop over negative orientations: -1, -2, -3
  for (int orient = -1; orient >= -3; orient--)
  {

    int tets[5][4];
    tets[0][0] = 0; tets[0][1] = 1; tets[0][2] = 2; tets[0][3] = 3;

    //Rotate neighbors so that they all have orientation <orient>
    for (int i=1; i<5; i++)
    {
      for (int j=0; j<3; j++)
      {
        tets[i][j] = tets_orient1[i][(j-orient-1)%3];
      }

      tets[i][3] = tets_orient1[i][3];
    }


    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);

    BOOST_CHECK_EQUAL( xfld.nDOF(), 8 );

    //Check the node values
    BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
    BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
    BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );
    BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 1 );

    BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], 1 );
    BOOST_CHECK_EQUAL( xfld.DOF(5)[0],-1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[2], 0 );
    BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1],-1 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[2], 0 );
    BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[2],-1 );

    // volume field variable

    BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
    BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

    const XField3D_5Tet_X1_1Group_AllOrientations::FieldCellGroupType<Tet>& xfldVol = xfld.getCellGroup<Tet>(0);
    BOOST_CHECK_EQUAL( xfldVol.nElem(), 5 );

    int nodeMap[4];
    int elemL, elemR;
    int faceL, faceR;
    int faceI;

    for (int elem=0; elem<5; elem++)
    {
      xfldVol.associativity(elem).getNodeGlobalMapping( nodeMap, 4 );
      BOOST_CHECK_EQUAL( nodeMap[0], tets[elem][0] );
      BOOST_CHECK_EQUAL( nodeMap[1], tets[elem][1]  );
      BOOST_CHECK_EQUAL( nodeMap[2], tets[elem][2]  );
      BOOST_CHECK_EQUAL( nodeMap[3], tets[elem][3]  );
    }

    Int4 faceSign;

    faceSign = xfldVol.associativity(0).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], +1 );
    BOOST_CHECK_EQUAL( faceSign[1], +1 );
    BOOST_CHECK_EQUAL( faceSign[2], +1 );
    BOOST_CHECK_EQUAL( faceSign[3], +1 );

    faceSign = xfldVol.associativity(1).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], +1 );
    BOOST_CHECK_EQUAL( faceSign[1], +1 );
    BOOST_CHECK_EQUAL( faceSign[2], +1 );
    BOOST_CHECK_EQUAL( faceSign[3], orient );

    faceSign = xfldVol.associativity(2).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], +1 );
    BOOST_CHECK_EQUAL( faceSign[1], +1 );
    BOOST_CHECK_EQUAL( faceSign[2], +1 );
    BOOST_CHECK_EQUAL( faceSign[3], orient );

    faceSign = xfldVol.associativity(3).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], +1 );
    BOOST_CHECK_EQUAL( faceSign[1], +1 );
    BOOST_CHECK_EQUAL( faceSign[2], +1 );
    BOOST_CHECK_EQUAL( faceSign[3], orient );

    faceSign = xfldVol.associativity(4).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], +1 );
    BOOST_CHECK_EQUAL( faceSign[1], +1 );
    BOOST_CHECK_EQUAL( faceSign[2], +1 );
    BOOST_CHECK_EQUAL( faceSign[3], orient );


    // interior-face field variable

    BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

    const XField3D_5Tet_X1_1Group_AllOrientations::FieldTraceGroupType<Triangle>& xfldIface = xfld.getInteriorTraceGroup<Triangle>(0);
    BOOST_CHECK_EQUAL( xfldIface.nElem(), 4 );

    BOOST_CHECK_EQUAL( xfldIface.getGroupLeft(), 0 );
    BOOST_CHECK_EQUAL( xfldIface.getGroupRight(), 0 );

    // interior face-to-cell connectivity

    // Face between center and front Tets
    faceI = 0;
    elemL = 0; faceL = 0;
    elemR = 1; faceR = 3;

    xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
    for (int i = 0; i < 3; i++)
      BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

    BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
    BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

    CanonicalTraceToCell traceL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemL], 4);
    BOOST_CHECK_EQUAL( traceL.trace, 0);
    BOOST_CHECK_EQUAL( traceL.orientation, 1);

    CanonicalTraceToCell traceR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemR], 4);
    BOOST_CHECK_EQUAL( traceR.trace, 3);
    BOOST_CHECK_EQUAL( traceR.orientation, orient);

    BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
    BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], orient );
    faceI++;

    // Face between center and left Tets
    elemL = 0; faceL = 1;
    elemR = 2; faceR = 3;

    xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
    for (int i = 0; i < 3; i++)
      BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

    BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
    BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

    traceL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemL], 4);
    BOOST_CHECK_EQUAL( traceL.trace, 1);
    BOOST_CHECK_EQUAL( traceL.orientation, 1);

    traceR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemR], 4);
    BOOST_CHECK_EQUAL( traceR.trace, 3);
    BOOST_CHECK_EQUAL( traceR.orientation, orient);

    BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
    BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], orient );
    faceI++;

    // Face between center and bottom Tets
    elemL = 0; faceL = 2;
    elemR = 3; faceR = 3;

    xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
    for (int i = 0; i < 3; i++)
      BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

    BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
    BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

    traceL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemL], 4);
    BOOST_CHECK_EQUAL( traceL.trace, 2);
    BOOST_CHECK_EQUAL( traceL.orientation, 1);

    traceR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemR], 4);
    BOOST_CHECK_EQUAL( traceR.trace, 3);
    BOOST_CHECK_EQUAL( traceR.orientation, orient);

    BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
    BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], orient );
    faceI++;

    // Face between center and rear Tets
    elemL = 0; faceL = 3;
    elemR = 4; faceR = 3;

    xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
    for (int i = 0; i < 3; i++)
      BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

    BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
    BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

    traceL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemL], 4);
    BOOST_CHECK_EQUAL( traceL.trace, 3);
    BOOST_CHECK_EQUAL( traceL.orientation, 1);

    traceR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemR], 4);
    BOOST_CHECK_EQUAL( traceR.trace, 3);
    BOOST_CHECK_EQUAL( traceR.orientation, orient);

    BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
    BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], orient );
    faceI++;

    BOOST_REQUIRE_EQUAL( 4, faceI );


    // boundary-face field variable

    BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

    const XField3D_5Tet_X1_1Group_AllOrientations::FieldTraceGroupType<Triangle>& xfldBface = xfld.getBoundaryTraceGroup<Triangle>(0);
    BOOST_CHECK_EQUAL( xfldBface.nElem(), 12 );
    BOOST_CHECK_EQUAL( xfldBface.getGroupLeft(), 0 );

    int faceB = 0;

    for (int elemL=1; elemL<5; elemL++)
    {
      for (int faceL=0; faceL<3; faceL++)
      {

        xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
        for (int i = 0; i < 3; i++)
          BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

        BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
        BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

        BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
        faceB++;

      }
    }

    BOOST_REQUIRE_EQUAL( 12, faceB );

  } //loop over negative orientations

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_5Tet_X1_1Group_AllOrientations_Positive_test )
{

  typedef std::array<int,4> Int4;

  //Original configuration: center tet has orientation -1, all neighbors have orientation +1
  const int tets_orient1[5][4] = { {0, 1, 2, 3},
                                   {1, 2, 3, 4},
                                   {0, 3, 2, 5},
                                   {0, 1, 3, 6},
                                   {0, 2, 1, 7}};

  //loop over positive orientations: +1, +2, +3
  for (int orient = 1; orient <= 3; orient++)
  {

    int tets[5][4];
    tets[0][0] = 0; tets[0][1] = 1; tets[0][2] = 2; tets[0][3] = 3;

    //Rotate neighbors so that they all have orientation <orient>
    for (int i=1; i<5; i++)
    {
      for (int j=0; j<3; j++)
      {
        tets[i][j] = tets_orient1[i][(j+orient-1)%3];
      }

      tets[i][3] = tets_orient1[i][3];
    }

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);

    BOOST_CHECK_EQUAL( xfld.nDOF(), 8 );

    //Check the node values
    BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
    BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
    BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );
    BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 1 );

    BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], 1 );
    BOOST_CHECK_EQUAL( xfld.DOF(5)[0],-1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[2], 0 );
    BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1],-1 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[2], 0 );
    BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[2],-1 );

    // volume field variable

    BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
    BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

    const XField3D_5Tet_X1_1Group_AllOrientations::FieldCellGroupType<Tet>& xfldVol = xfld.getCellGroup<Tet>(0);
    BOOST_CHECK_EQUAL( xfldVol.nElem(), 5 );

    int nodeMap[4];
    int elemL, elemR;
    int faceL, faceR;
    int faceI;

    for (int elem=0; elem<5; elem++)
    {
      xfldVol.associativity(elem).getNodeGlobalMapping( nodeMap, 4 );
      BOOST_CHECK_EQUAL( nodeMap[0], tets[elem][0] );
      BOOST_CHECK_EQUAL( nodeMap[1], tets[elem][1]  );
      BOOST_CHECK_EQUAL( nodeMap[2], tets[elem][2]  );
      BOOST_CHECK_EQUAL( nodeMap[3], tets[elem][3]  );
    }

    Int4 faceSign;

    faceSign = xfldVol.associativity(0).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], -orient );
    BOOST_CHECK_EQUAL( faceSign[1], -orient );
    BOOST_CHECK_EQUAL( faceSign[2], -orient );
    BOOST_CHECK_EQUAL( faceSign[3], -orient );

    faceSign = xfldVol.associativity(1).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], +1 );
    BOOST_CHECK_EQUAL( faceSign[1], +1 );
    BOOST_CHECK_EQUAL( faceSign[2], +1 );
    BOOST_CHECK_EQUAL( faceSign[3], +1 );

    faceSign = xfldVol.associativity(2).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], +1 );
    BOOST_CHECK_EQUAL( faceSign[1], +1 );
    BOOST_CHECK_EQUAL( faceSign[2], +1 );
    BOOST_CHECK_EQUAL( faceSign[3], +1 );

    faceSign = xfldVol.associativity(3).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], +1 );
    BOOST_CHECK_EQUAL( faceSign[1], +1 );
    BOOST_CHECK_EQUAL( faceSign[2], +1 );
    BOOST_CHECK_EQUAL( faceSign[3], +1 );

    faceSign = xfldVol.associativity(4).faceSign();
    BOOST_CHECK_EQUAL( faceSign[0], +1 );
    BOOST_CHECK_EQUAL( faceSign[1], +1 );
    BOOST_CHECK_EQUAL( faceSign[2], +1 );
    BOOST_CHECK_EQUAL( faceSign[3], +1 );


    // interior-face field variable

    BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

    const XField3D_5Tet_X1_1Group_AllOrientations::FieldTraceGroupType<Triangle>& xfldIface = xfld.getInteriorTraceGroup<Triangle>(0);
    BOOST_CHECK_EQUAL( xfldIface.nElem(), 4 );

    BOOST_CHECK_EQUAL( xfldIface.getGroupLeft(), 0 );
    BOOST_CHECK_EQUAL( xfldIface.getGroupRight(), 0 );

    // interior face-to-cell connectivity

    // Face between center and front Tets
    faceI = 0;
    elemL = 1; faceL = 3;
    elemR = 0; faceR = 0;

    xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
    for (int i = 0; i < 3; i++)
      BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

    BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
    BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

    CanonicalTraceToCell traceL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemL], 4);
    BOOST_CHECK_EQUAL( traceL.trace, 3);
    BOOST_CHECK_EQUAL( traceL.orientation, 1);

    CanonicalTraceToCell traceR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemR], 4);
    BOOST_CHECK_EQUAL( traceR.trace, 0);
    BOOST_CHECK_EQUAL( traceR.orientation, -orient);

    BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
    BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], -orient );
    faceI++;

    // Face between center and left Tets
    elemL = 2; faceL = 3;
    elemR = 0; faceR = 1;

    xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
    for (int i = 0; i < 3; i++)
      BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

    BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
    BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

    traceL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemL], 4);
    BOOST_CHECK_EQUAL( traceL.trace, 3);
    BOOST_CHECK_EQUAL( traceL.orientation, 1);

    traceR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemR], 4);
    BOOST_CHECK_EQUAL( traceR.trace, 1);
    BOOST_CHECK_EQUAL( traceR.orientation, -orient);

    BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
    BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], -orient );
    faceI++;

    // Face between center and bottom Tets
    elemL = 3; faceL = 3;
    elemR = 0; faceR = 2;

    xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
    for (int i = 0; i < 3; i++)
      BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

    BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
    BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

    traceL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemL], 4);
    BOOST_CHECK_EQUAL( traceL.trace, 3);
    BOOST_CHECK_EQUAL( traceL.orientation, 1);

    traceR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemR], 4);
    BOOST_CHECK_EQUAL( traceR.trace, 2);
    BOOST_CHECK_EQUAL( traceR.orientation, -orient);

    BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
    BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], -orient );
    faceI++;

    // Face between center and rear Tets
    elemL = 4; faceL = 3;
    elemR = 0; faceR = 3;

    xfldIface.associativity(faceI).getNodeGlobalMapping( nodeMap, 3 );
    for (int i = 0; i < 3; i++)
      BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

    BOOST_CHECK_EQUAL( xfldIface.getElementLeft(faceI), elemL );
    BOOST_CHECK_EQUAL( xfldIface.getElementRight(faceI), elemR );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(faceI).trace, faceL );
    BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(faceI).trace, faceR );

    traceL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemL], 4);
    BOOST_CHECK_EQUAL( traceL.trace, 3);
    BOOST_CHECK_EQUAL( traceL.orientation, 1);

    traceR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(nodeMap, 3, tets[elemR], 4);
    BOOST_CHECK_EQUAL( traceR.trace, 3);
    BOOST_CHECK_EQUAL( traceR.orientation, -orient);

    BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
    BOOST_CHECK_EQUAL( xfldVol.associativity(elemR).faceSign()[faceR], -orient );
    faceI++;

    BOOST_REQUIRE_EQUAL( 4, faceI );


    // boundary-face field variable

    BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

    const XField3D_5Tet_X1_1Group_AllOrientations::FieldTraceGroupType<Triangle>& xfldBface = xfld.getBoundaryTraceGroup<Triangle>(0);
    BOOST_CHECK_EQUAL( xfldBface.nElem(), 12 );
    BOOST_CHECK_EQUAL( xfldBface.getGroupLeft(), 0 );

    int faceB = 0;

    for (int elemL=1; elemL<5; elemL++)
    {
      for (int faceL=0; faceL<3; faceL++)
      {

        xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
        for (int i = 0; i < 3; i++)
          BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

        BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
        BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

        BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
        faceB++;

      }
    }

    BOOST_REQUIRE_EQUAL( 12, faceB );

  } //loop over positive orientations

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
