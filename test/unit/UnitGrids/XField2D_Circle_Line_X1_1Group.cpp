// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "tools/SANSException.h"
#include <fstream>

#include "XField2D_Circle_Line_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

// -----------------------------------------------------
XField2D_Circle_Line_X1_1Group::XField2D_Circle_Line_X1_1Group(const int ii)
{
  int comm_rank = comm_->rank();

  // Create the DOF arrays
  resizeDOF( ii );

  // Create the element groups
  resizeInteriorTraceGroups(1); // interior
  resizeBoundaryTraceGroups(0); // no boundary trace
  resizeCellGroups(1);

  // Assign the grid node coordinates
  DLA::VectorS<2,Real> coordinates;
  Real theta;
  for (int i = 0; i < ii; i++)
  {
    theta = ( 2*PI / (Real) ii ) * (Real) i;
    DOF(i) = { cos(theta), sin(theta) };
  }

  // ---------- Line element field variable ----------
  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, ii );

  // Element cell associativity
  for ( int i = 0; i < ii-1; i++ )
  {
    fldAssocCell.setAssociativity( i ).setRank( comm_rank );
    fldAssocCell.setAssociativity( i ).setNodeGlobalMapping( {i, i+1} );
  }
  fldAssocCell.setAssociativity( ii-1 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( ii-1 ).setNodeGlobalMapping( {ii-1, 0} );  // the last element is connected to the first element

  cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // ---------- Interior-trace field variable ----------
  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode( BasisFunctionNodeBase::P0, ii );

  fldAssocInode.setGroupLeft( 0 );
  fldAssocInode.setGroupRight( 0 );

  // trace-element associativity
  for ( int i = 0; i < ii; i++ )
  {
    fldAssocInode.setAssociativity( i ).setRank( comm_rank );
    fldAssocInode.setAssociativity( i ).setNodeGlobalMapping( {i} );
    fldAssocInode.setAssociativity( i ).setNormalSignL(  1 );
    fldAssocInode.setAssociativity( i ).setNormalSignR( -1 );

    if ( i > 0 )
    {
      fldAssocInode.setElementLeft( i-1, i );
    }
    else
    {
      fldAssocInode.setElementLeft( ii-1, i );  // the first interior node is connected to the last element ii-1
    }

    fldAssocInode.setElementRight( i, i );

    fldAssocInode.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), i );
    fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), i );
  }

  interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocInode );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

  // ---------- Boundary-trace field variable ----------
  // no boundary trace in this case

  //Check that the grid is correct
  checkGrid();
}

}
