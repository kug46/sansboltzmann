// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_SYMAIRFOIL_H
#define XFIELD2D_SYMAIRFOIL_H

#include "Field/XFieldArea.h"

namespace SANS
{

// symmetric grid around symmetric airfoil
//
// grid is symmetric by construction
//
// parameters:
//   airf     coordinates on upper airfoil surface (lower is same). Assumed to have x in [0,1]
//   iistag   elements on stagnation streamline
//   iiwake   elements on wake-cut
//   iiffd    elements on upper farfield boundary (lower is same)

class XField2D_SymAirfoil : public XField<PhysD2,TopoD2>
{
public:
  XField2D_SymAirfoil(const std::vector<DLA::VectorS<2,Real>>& airf, const Real rffd, int iistag = 8, int iiwake = 8, int iiffd = 16);

  static const int iAirfoilBoundaryGroup = 0;
  static const int iFarfieldBoundaryGroup = 1;
};

}

#endif // XFIELD2D_SYMAIRFOIL_H
