// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_RAMP_TRIANGLE_X1
#define XFIELD2D_RAMP_TRIANGLE_X1

#include <vector>

#include "XField2D_Box_UnionJack_Triangle_X1.h"

namespace SANS
{
// triangle grid in a unit box with 4 sides as separate boundary-edge groups
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// area elements in 1 group
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 4 groups: lower, right, upper, left

class XField2D_Ramp_Triangle_X1 : public XField2D_Box_UnionJack_Triangle_X1
{
public:
  XField2D_Ramp_Triangle_X1( int ii, int jj, Real xmin = 0, Real xmax = 1, Real ymin = 0, Real ymax = 1, Real theta = 0 );

  using XField2D_Box_UnionJack_Triangle_X1::iBottom;
  using XField2D_Box_UnionJack_Triangle_X1::iRight;
  using XField2D_Box_UnionJack_Triangle_X1::iTop;
  using XField2D_Box_UnionJack_Triangle_X1::iLeft;
};

}

#endif // XFIELD2D_RAMP_TRIANGLE_X1
