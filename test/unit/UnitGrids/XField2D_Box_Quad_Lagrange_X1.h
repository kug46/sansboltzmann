// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_BOX_QUAD_LAGRANGE_X1
#define XFIELD2D_BOX_QUAD_LAGRANGE_X1

#include "Field/XFieldArea.h"

#include <vector>
#include <array>

namespace SANS
{
// quad grid in a unit box with 4 sides as separate boundary-edge groups
//
// generates grid with ii x jj (quad) elements
// area elements in 1 group
// interior-edge elements in 2 groups: horizontal, vertical
// boundary-edge elements in 4 groups: lower, right, upper, left
//
class XField2D_Box_Quad_Lagrange_X1 : public XField<PhysD2,TopoD2>
{
public:
  XField2D_Box_Quad_Lagrange_X1( mpi::communicator comm,
                                 int ii, int jj,
                                 const std::array<bool,2> periodic = {{false, false}} );

  XField2D_Box_Quad_Lagrange_X1( mpi::communicator comm,
                                 int ii, int jj,
                                 Real xmin, Real xmax,
                                 Real ymin, Real ymax,
                                 const std::array<bool,2> periodic = {{false, false}} );

  XField2D_Box_Quad_Lagrange_X1( mpi::communicator comm,
                                 const std::vector<Real>& xvec,
                                 const std::vector<Real>& yvec,
                                 const std::array<bool,2> periodic = {{false, false}} );

  static const int iBottom, iRight, iTop, iLeft;

protected:
  void generateGrid( mpi::communicator comm,
                     const std::vector<Real>& xvec,
                     const std::vector<Real>& yvec,
                     const std::array<bool,2> periodic );
};

}

#endif // XFIELD2D_BOX_QUAD_LAGRANGE_X1
