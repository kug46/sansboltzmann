// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_FlatPlate_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/Partition/XField_Lagrange.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace // privat to this file
{
//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj (quad) elements
//
// area elements in 1 group: 2*ii*jj triangles
// boundary-edge elements in 6 groups:
// bottom left (slipwall), bottom middle (flat plate), bottom right (slipwall)
// right (outflow), upper (farfield), left (inlet)
double xcoord( const Real xmin, const Real xmax, const int iL, int i )
{

  Real itmp = i;
  Real iLtmp = iL;

#if 1
  // COARSE SPACING AT INLET
  Real delta = 5.4;
  Real tmp = (xmax-xmin)*0.5*(1 + tanh(delta*( itmp/iLtmp - 0.5) )/tanh(delta/2) ) + xmin;

#else
  //FINE SPACING AT INLET
  Real delta = 5.0;
  Real tmp = (xmax-xmin)*0.5*(1 + tanh(delta*( itmp/iLtmp - 0.5) )/tanh(delta/2) ) + xmin;
#endif
  return tmp;
}

double xcoordIn( const Real xmin, const Real xmax, const int iL, int i )
{

  Real itmp = i;
  Real iLtmp = iL;
#if 1
  // COARSE SPACING AT INLET
  Real delta = 3.2;
  Real tmp = (xmax-xmin)*( tanh( delta*(itmp/iLtmp) ) / tanh(delta) ) + xmin;
#else
  //FINE SPACING AT INLET
  Real delta = 5.4;
  Real tmp = (xmax-xmin)*0.5*(1 + tanh(delta*( itmp/iLtmp - 0.5) )/tanh(delta/2) ) + xmin;
#endif
  return tmp;
}

double xcoordOut( const Real xmin, const Real xmax, const int iL, int i )
{

  Real itmp = i;
  Real iLtmp = iL;

#if 1
  // COARSE SPACING AT INLET
  Real delta = 3.2;
  Real tmp = xmax - (xmax-xmin)*(tanh( delta*( 1- itmp/iLtmp) ) / tanh(delta) );
#else
  //FINE SPACING AT INLET
  Real delta = 5.4;
  Real tmp = (xmax-xmin)*0.5*(1 + tanh(delta*( itmp/iLtmp - 0.5) )/tanh(delta/2) ) + xmin;
#endif

  return tmp;
}


double ycoord( const Real ymax, const int jj, int j )
{
  Real jtmp = j;
  Real jjtmp = jj;

  Real delta = 3.4;
  Real tmp = ymax*(1 + tanh( delta*( jtmp/jjtmp - 1) )/ tanh(delta));

  return tmp;

}

} // namespace

namespace SANS
{

const int XField2D_FlatPlate_X1::iSlipIn  = 0;
const int XField2D_FlatPlate_X1::iPlate   = 1;
const int XField2D_FlatPlate_X1::iSlipOut = 2;
const int XField2D_FlatPlate_X1::iRight   = 3;
const int XField2D_FlatPlate_X1::iTop     = 4;
const int XField2D_FlatPlate_X1::iLeft    = 5;

XField2D_FlatPlate_X1::XField2D_FlatPlate_X1(mpi::communicator& comm, const int power ) : XField<PhysD2,TopoD2>(comm)
{
  XField_Lagrange<PhysD2>::VectorX X;

  XField_Lagrange<PhysD2> xfldin(comm);

  // Arthur version of params | const Real center = 0.5; const Real length = 1; const Real offset = 1;
  const Real center = 0.5; const Real length = 1; const Real offset = 1; const Real height = 2;

  // TRM version
  // const Real center = 1; const Real length = 2; const Real offset = 2; const Real height = 4;

  int iL = 8*pow( 2, power );
  int iM = 16*pow( 2, power );
  int iR = 8*pow( 2, power );

  int jj = 8*pow( 2, power );

  int ii = iL + iM + iR;

  // // Old version
  // Real xmin = -1.0;
  // Real xmax = 1.0;
  // Real xmax2 = 2.0;
  // Real ymax = 2.0;

  // New version
  Real xDmin = center - offset - (length/2);
  Real xPmin = center - (length/2);
  Real xDmax = center + offset + (length/2);
  Real xPmax = center + (length/2);
  Real ymax = height;

  int nnode = (ii + 1)*(jj + 1);
  int nelem = 2*ii*jj;

  // create the grid-coordinate DOF arrays
  xfldin.sizeDOF( nnode );

  if (comm.rank() == 0)
  {
    for (int j = 0; j < jj+1; j++)
    {

      for (int i = 0; i < iL+1; i++)
      {
        X[0] = xcoordIn(xDmin, xPmin, iL, i);
        X[1] = ycoord(ymax, jj, j);
        xfldin.addDOF(X);

//        std::cout << X[0] << " " << X[1] << "\n";
      }

      for (int i = 1; i < iM+1; i++)
      {
        X[0] = xcoord(xPmin, xPmax,iM,i);
        X[1] = ycoord(ymax, jj, j);
        xfldin.addDOF(X);

//        std::cout << X[0] << " " << X[1] << "\n";
      }

      for (int i = 1; i < iR+1; i++)
      {
        X[0] = xcoordOut(xPmax, xDmax, iR, i);
        X[1] = ycoord(ymax, jj, j);
        xfldin.addDOF(X);

//        std::cout << X[0] << " " << X[1] << "\n";
      }
    }
  }

  // Start the process of adding cells
  xfldin.sizeCells(nelem);

  // area element grid-coordinate DOF associativity (cell-to-node connectivity)
  int cellGroup = 0, order = 1;
  int n1, n2, n3, n4;

  if (comm.rank() == 0)
  {
    for (int j = 0; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        n1 = j*(ii+1) + i;        // n4--n3
        n2 = n1 + 1;              //  |\ |
        n3 = n2 + (ii+1);         //  | \|
        n4 = n1 + (ii+1);         // n1--n2

        xfldin.addCell(cellGroup, eTriangle, order, {n1, n2, n4});
        xfldin.addCell(cellGroup, eTriangle, order, {n3, n4, n2});
      }
    }
  }

  // Start the process of adding boundary trace elements
  xfldin.sizeBoundaryTrace(2*ii + 2*jj);

  if (comm.rank() == 0)
  {
    // lower boundary slipwall left of the plate
    for (int i = 0; i < iL; i++)
    {
      n1 = i;
      n2 = i + 1;

      xfldin.addBoundaryTrace( iSlipIn, eLine, {n1, n2} );
    }

    // Plate
    for (int i = iL; i < iL+iM; i++)
    {
      n1 = i;
      n2 = i + 1;

      xfldin.addBoundaryTrace( iPlate, eLine, {n1, n2} );
    }

    // lower boundary slipwall right of the plate
    for (int i = (iL+iM); i < ii; i++)
    {
      n1 = i;
      n2 = i + 1;

      xfldin.addBoundaryTrace( iSlipOut, eLine, {n1, n2} );
    }

    // right boundary
    for (int j = 0; j < jj; j++)
    {
      n1 = j*(ii+1) + ii;
      n2 = n1 + (ii+1);

      xfldin.addBoundaryTrace( iRight, eLine, {n1, n2} );
    }

    // upper boundary
    for (int i = ii-1; i >= 0; i--)
    {
      n1 = jj*(ii+1) + i + 1;
      n2 = jj*(ii+1) + i;

      xfldin.addBoundaryTrace( iTop, eLine, {n1, n2} );
    }

    // left boundary
    for (int j = jj-1; j >= 0; j--)
    {
      n1 = j*(ii+1) + (ii+1);
      n2 = j*(ii+1);

      xfldin.addBoundaryTrace( iLeft, eLine, {n1, n2} );
    }
  }

  //construct the grid
  this->buildFrom( xfldin );
}

}
