// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD3D_BOX_HEX_X1_WAKECUT
#define XFIELD3D_BOX_HEX_X1_WAKECUT

#include "Field/XField3D_Wake.h"

namespace SANS
{
// hexahedon grid with a zero-thickness wing for full potential testing

class XField3D_Box_Hex_X1_WakeCut : public XField3D_Wake
{
public:
  XField3D_Box_Hex_X1_WakeCut( const int nChord, const int nSpan, const Real span, const int nWake, const bool adjoint = false );
};

}

#endif // XFIELD3D_BOX_HEX_X1_WAKECUT
