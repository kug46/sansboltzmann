// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UNIT_UNITGRIDS_XFIELD2D_ANNULUS_QUAD_X1_H_
#define UNIT_UNITGRIDS_XFIELD2D_ANNULUS_QUAD_X1_H_

#include "XField2D_Box_Quad_X1.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// quad grids of a portion of an annulus
//
// configuration inherited from XField2D_Box_Quad_X1

class XField2D_Annulus_Quad_X1 : public XField2D_Box_Quad_X1
{
public:
  XField2D_Annulus_Quad_X1( const int ii, const int jj,
                            const Real R1, const Real h,
                            const Real thetamin, const Real thetamax );
};

}  // namespace SANS

#endif /* UNIT_UNITGRIDS_XFIELD2D_ANNULUS_QUAD_X1_H_ */
