// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UNIT_UNITGRIDS_XFIELD2D_LINE_XQ_1GROUP_H_
#define UNIT_UNITGRIDS_XFIELD2D_LINE_XQ_1GROUP_H_

#include "Field/XFieldLine.h"
#include "XField2D_Line_X1_1Group.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Unit grid class: open-ended 2D grid field of line elements
//   generate high order (>1) line grid from prescribed nodal coordinates and an exisitng linear grid
//   line elements in 1 group
//   interior traces in 1 group
//   boundary traces in 2 group
//   geometric order is arbitrary
//
// Class variables:
//
// Member functions:
//   .nNode     compute the total of nodes required based on element # and geometric order
//----------------------------------------------------------------------------//

// line grid with consistent reference coordinate direction
//      (0)     (1)     (2)         element
//   0 ----- 1 ----- 2 ----- 3 ---  node
//      -->     -->     -->         reference coordinate s direction

class XField2D_Line_Xq_1Group : public XField<PhysD2,TopoD1>
{
public:
  typedef std::vector<DLA::VectorS<2,Real>> ArrayVector;

  //-----------------------Constructors-----------------------//
  XField2D_Line_Xq_1Group( const XField2D_Line_X1_1Group& xfld,
                           const int order, const ArrayVector& coordinates );

  //-----------------------Members-----------------------//
  // [Input]
  // order      geometric order
  // nelem      number of line elements
  // [Output]
  //            total number nodes required
  static unsigned int nNode(const int order, const int nelem) { return nelem*order + 1; }
};


// line grid with alternating reference coordinate direction
//      (0)     (1)     (2)         element
//   0 ----- 1 ----- 2 ----- 3 ---  node
//      -->     <--     -->         reference coordinate s direction
//
class XField2D_Line_Xq_1Group_unstructured : public XField<PhysD2,TopoD1>
{
public:
  typedef std::vector<DLA::VectorS<2,Real>> ArrayVector;

  XField2D_Line_Xq_1Group_unstructured( const XField2D_Line_X1_1Group_unstructured& xfld,
                                        const int order,
                                        const ArrayVector& coordinates );

  static unsigned int nNode(const int order, const int nelem) { return nelem*order + 1; }
};

}

#endif /* UNIT_UNITGRIDS_XFIELD2D_LINE_XQ_1GROUP_H_ */
