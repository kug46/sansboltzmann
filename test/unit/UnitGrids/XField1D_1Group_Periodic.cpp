// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField1D_1Group_Periodic.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

XField1D_1Group_Periodic::XField1D_1Group_Periodic(const int ii, Real xL, Real xR)
{
  SANS_ASSERT(xR > xL);
  SANS_ASSERT(ii > 0);

  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(ii+1);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(0);
  resizeCellGroups(1);

  // nodal coordinates for the lines
  for ( int i = 0; i < ii+1; i++ )
    DOF(i) = (xR - xL) * i / Real(ii) + xL;

  // area field variable
  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, ii );

  //element cell associativity
  for ( int i = 0; i < ii; i++ )
  {
    fldAssocCell.setAssociativity( i ).setRank( comm_rank );
    fldAssocCell.setAssociativity( i ).setNodeGlobalMapping( {i, i+1} );
  }


  cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // interior-trace field variable

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode( BasisFunctionNodeBase::P0, ii );

  fldAssocInode.setGroupLeft( 0 );
  fldAssocInode.setGroupRight( 0 );

  //Left periodic boundary node
  fldAssocInode.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocInode.setAssociativity( 0 ).setNodeGlobalMapping( {0} );

  fldAssocInode.setAssociativity( 0 ).setNormalSignL( 1 );
  fldAssocInode.setAssociativity( 0 ).setNormalSignR( -1 );

  // edge-to-cell connectivity
  fldAssocInode.setElementLeft( ii-1, 0 );
  fldAssocInode.setElementRight( 0, 0 );
  fldAssocInode.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );
  fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), 0 );

  // edge-element associativity
  for ( int i = 0; i < ii-1; i++ )
  {
    fldAssocInode.setAssociativity( i+1 ).setRank( comm_rank );
    fldAssocInode.setAssociativity( i+1 ).setNodeGlobalMapping( {i+1} );

    fldAssocInode.setAssociativity( i+1 ).setNormalSignL( 1 );
    fldAssocInode.setAssociativity( i+1 ).setNormalSignR( -1 );

    // edge-to-cell connectivity
    fldAssocInode.setElementLeft( i, i+1 );
    fldAssocInode.setElementRight( i+1, i+1 );
    fldAssocInode.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), i+1 );
    fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), i+1 );
  }

  interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocInode );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  //TODO: The periodic condition messes up the checks at the moment
  initDefaultElmentIDs();
//  checkGrid();
}

}
