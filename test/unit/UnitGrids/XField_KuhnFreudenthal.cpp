// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#include "Field/Partition/XField_Lagrange.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include "geometry/body.h"
#include "library/cube.h"
#include "library/geometry.h"
#include "library/tesseract.h"
#include "mesh/boundary.h"
#endif

namespace SANS
{

#ifdef SANS_AVRO
template<class PhysDim,class TopoDim>
XField_KuhnFreudenthal<PhysDim,TopoDim>::XField_KuhnFreudenthal( mpi::communicator& comm , const std::vector<int>& dims ) :
  XField<PhysDim,TopoDim>(comm),
  dims_(dims.begin(),dims.end()),
  lengths_( PhysDim::D , 1. ),
  mesh_( lengths_ , dims_ )
{
  SANS_ASSERT( PhysDim::D == TopoDim::D );

  // create the accompanying body
  createBody();

  XField_Lagrange<PhysDim> xfldin(comm);
  generateMesh(comm,xfldin);

  // Finalize the grid construction
  this->buildFrom( xfldin );
}

template<class PhysDim,class TopoDim>
avro::Context*
XField_KuhnFreudenthal<PhysDim,TopoDim>::context()
  { return &context_; }

template<class PhysDim,class TopoDim>
std::shared_ptr<avro::Body>&
XField_KuhnFreudenthal<PhysDim,TopoDim>::body_ptr()
  { return body_; }

template<class PhysDim,class TopoDim>
void
XField_KuhnFreudenthal<PhysDim,TopoDim>::createBody()
{
  const int dim = PhysDim::D;
  if (dim==2)
  {
    std::vector<Real> xc(3,0.0);
    for (int i=0;i<dim;i++)
      xc[i] = 0.5*lengths_[i];
    body_ = std::make_shared<avro::library::EGADSSquare>(&context_,xc.data(),lengths_[0],lengths_[1]);

  }
  else if (dim==3)
  {
    std::vector<Real> x0(dim,0.);
    body_ = std::make_shared<avro::library::EGADSBox>(&context_,x0.data(),lengths_.data());
  }
  else if (dim==4)
  {
    std::vector<Real> x0(dim,0.5);
    body_ = std::make_shared<avro::library::Tesseract>(x0.data(),lengths_.data());
  }
  else
    SANS_DEVELOPER_EXCEPTION("bad dimension");

  // attach the mesh vertices to the geometry
  mesh_.vertices().findGeometry( *body_ );

}

template<class PhysDim,class TopoDim>
void
XField_KuhnFreudenthal<PhysDim,TopoDim>::generateMesh( mpi::communicator& comm , XField_Lagrange<PhysDim>& xfld )
{
  using avro::index_t;
  using avro::coord_t;

  // add the DOF
  typename XField_Lagrange<PhysDim>::VectorX X;
  xfld.sizeDOF( mesh_.vertices().nb() );

  if (comm.rank()==0)
  {
    for (index_t k=0;k<mesh_.vertices().nb();k++)
    {
      for (coord_t d=0;d<mesh_.vertices().dim();d++)
        X[d] = mesh_.vertices()[k][d];
      xfld.addDOF( X );
    }
    printf("added vertices\n");
  }


  // reference the topology where everything is stored
  SANS_ASSERT( mesh_.nb_topologies()==1 );
  avro::Topology<avro::Simplex>& topology = mesh_.topology(0);
  topology.orient();

  // add the simplices
  int cellgroup = 0;
  int order = 1;
  xfld.sizeCells( topology.nb() );

  if (comm.rank()==0)
  {
    std::vector<int> simplex( TopoDim::D + 1 , 0 );
    for (index_t k=0;k<topology.nb();k++)
    {
      for (index_t j=0;j<topology.nv(k);j++)
        simplex[j] = topology(k,j);
      if (TopoDim::D==2)
        xfld.addCell( cellgroup , eTriangle , order , simplex );
      else if (TopoDim::D==3)
        xfld.addCell( cellgroup , eTet , order , simplex );
      else if (TopoDim::D==4)
        xfld.addCell( cellgroup , ePentatope , order , simplex );
      else
        SANS_DEVELOPER_EXCEPTION("mesh dimension not supported in SANS.");
    }
    printf("added cells\n");
  }

  // compute the boundary
  avro::Boundary<avro::Simplex> boundary( topology );
  boundary.extract();

  // size the boundary traces
  index_t nbnd = 0;
  for (index_t k=0;k<boundary.nb_children();k++)
    if (boundary.child(k)->number()==TopoDim::D-1)
      nbnd += boundary.child(k)->nb();
  xfld.sizeBoundaryTrace(nbnd);

  // add the boundary facets
  if (comm.rank()==0)
  {
    std::vector<int> facet( mesh_.number() );
    int group = 0;
    for (index_t k=0;k<boundary.nb_children();k++)
    {
      if (boundary.child(k)->number()!=topology.number()-1) continue;

      avro::Topology<avro::Simplex>& bndk = *boundary.child(k);

      // determine the entity this boundary is on
      avro::Entity* entity = boundary.entity(k);

      // the boundary group is the body index of the associated ego
      group = entity->bodyIndex() - 1;

      for (index_t j=0;j<bndk.nb();j++)
      {
        // retrieve the facet indices
        for (index_t i=0;i<bndk.nv(j);i++)
          facet[i] = bndk(j,i);

        if (bndk.number()==3)
          xfld.addBoundaryTrace(group, eTet , facet);
        else if (bndk.number()==2)
          xfld.addBoundaryTrace(group, eTriangle, facet);
        else if (bndk.number()==1)
          xfld.addBoundaryTrace(group, eLine , facet );
        else
          SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
      }
    }
    printf("added boundary\n");
  }
}

#else // SANS_AVRO
template<class PhysDim,class TopoDim>
XField_KuhnFreudenthal<PhysDim,TopoDim>::XField_KuhnFreudenthal( mpi::communicator& comm , const std::vector<int>& dims ) :
  XField<PhysDim,TopoDim>(comm),
  dims_(dims.begin(),dims.end()),
  lengths_( PhysDim::D , 1. )
{
  SANS_DEVELOPER_EXCEPTION("need avro to use Kuhn Freudenthal triangulation");
}

#endif

template class XField_KuhnFreudenthal<PhysD2,TopoD2>;
template class XField_KuhnFreudenthal<PhysD3,TopoD3>;
template class XField_KuhnFreudenthal<PhysD4,TopoD4>;

}
