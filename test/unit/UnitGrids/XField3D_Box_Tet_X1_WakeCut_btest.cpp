// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_Box_Tet_X1_WakeCut.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/output_Tecplot.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_WakeCut_test1 )
{
  const int nChord = 3;
  const int nSpan = 4;
  const Real span = 4;
  const int nWake = 3;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1_WakeCut xfld( nChord, nSpan, span, nWake );

  //output_Tecplot(xfld, "tmp/tmp.dat");
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
