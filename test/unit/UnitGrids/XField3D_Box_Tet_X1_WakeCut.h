// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD3D_BOX_TET_X1_WAKECUT
#define XFIELD3D_BOX_TET_X1_WAKECUT

#include "Field/XField3D_Wake.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// create tetrahedral grid in unit box with ii x jj x kk (hex) elements

// volume elements in 1 group: 6*ii*jj*kk tetrahdedron
// interior-triangle elements in 4 groups: x-const, y-const, z-const, hex-interior
// boundary-triangle elements in 9 groups: x-min, x-max, y-min, y-max, z-min, z-max, wing upper, wing lower, wake

class XField3D_Box_Tet_X1_WakeCut : public XField3D_Wake
{
public:
  XField3D_Box_Tet_X1_WakeCut( const int nChord, const int nSpan, const Real span, const int nWake, const bool adjoint = false );
};

}

#endif // XFIELD3D_BOX_TET_X1_WAKECUT
