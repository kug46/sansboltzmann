// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_Box_Quad_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// create quad grid in unit box with ii x jj elements
//
// area elements in 1 group: ii*jj quads
// interior-edge elements in 2 groups: horizontal, vertical
// boundary-edge elements in 4 groups: lower, right, upper, left

const int XField2D_Box_Quad_X1::iBottom = 0;
const int XField2D_Box_Quad_X1::iRight  = 1;
const int XField2D_Box_Quad_X1::iTop    = 2;
const int XField2D_Box_Quad_X1::iLeft   = 3;

XField2D_Box_Quad_X1::
XField2D_Box_Quad_X1( int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
{
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = xmin + (xmax - xmin)*i/Real(ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = ymin + (ymax - ymin)*j/Real(jj);

  generateGrid( xvec, yvec );
}

//---------------------------------------------------------------------------//
XField2D_Box_Quad_X1::
XField2D_Box_Quad_X1( const std::vector<Real>& xvec,
                      const std::vector<Real>& yvec )
{
 std::size_t ii = xvec.size() - 1;
 std::size_t jj = yvec.size() - 1;

 //Check ascending order
 for (std::size_t i = 0; i < ii; i++)
   SANS_ASSERT( xvec[i] < xvec[i+1] );

 for (std::size_t j = 0; j < jj; j++)
   SANS_ASSERT( yvec[j] < yvec[j+1] );

 generateGrid( xvec, yvec );
}

//---------------------------------------------------------------------------//
void
XField2D_Box_Quad_X1::
generateGrid( const std::vector<Real>& xvec,
              const std::vector<Real>& yvec )
{
  int ii = (int) xvec.size() - 1;
  int jj = (int) yvec.size() - 1;

  int comm_rank = comm_->rank();

  SANS_ASSERT( ii > 0 && jj > 0 );

  int nnode = (ii + 1)*(jj + 1);
  int nelem = ii*jj;

  // create the grid-coordinate DOF arrays
  resizeDOF( nnode );

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      DOF(j*(ii+1) + i)[0] = xvec[i];
      DOF(j*(ii+1) + i)[1] = yvec[j];
    }
  }

  // create the element groups
  int cnt = 0;
  if (ii > 1) cnt++;    // interior: horizontal
  if (jj > 1) cnt++;    // interior: vertical
  resizeInteriorTraceGroups(cnt);

  resizeBoundaryTraceGroups(4);     // bottom, right, upper, left
  resizeCellGroups(1);

  // grid area field variable

  FieldCellGroupType<Quad>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Quad>::HierarchicalP1, nelem );

  // area element grid-coordinate DOF associativity (cell-to-node connectivity)

  int n1, n2, n3, n4;
  for (int j = 0; j < jj; j++)
  {
    for (int i = 0; i < ii; i++)
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  |  |
      n3 = n2 + (ii+1);         //  |  |
      n4 = n1 + (ii+1);         // n1--n2

      // set the processor rank
      fldAssocCell.setAssociativity( j*ii + i ).setRank( comm_rank );

      // set the node mapping
      fldAssocCell.setAssociativity( j*ii + i ).setNodeGlobalMapping( {n1, n2, n3, n4} );
    }
  }

  // grid interior-edge
  int edge, nedge;
  int c1, c2;

  cnt = 0;

  // horizontal edges

  if (jj > 1)
  {
    nedge = (jj-1)*ii;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;
    for (int j = 1; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(ii) + i;
        c2 = (j-1)*(ii) + i;

        // set the processor rank
        fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

        // grid-coordinate DOF association (edge-to-node connectivity)
        fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        fldAssocCell.setAssociativity( c1 ).setEdgeSign( 1, 0 );
        fldAssocCell.setAssociativity( c2 ).setEdgeSign(-1, 2 );

        edge++;
      }
    }

    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Line>( fldAssocIedge );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;
  }

  // vertical edges

  if (ii > 1)
  {
    nedge = (ii-1)*jj;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;
    for (int i = 1; i < ii; i++)
    {
      for (int j = 0; j < jj; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(ii) + i - 1;
        c2 = j*(ii) + i;

        // set the processor rank
        fldAssocIedge.setAssociativity( edge ).setRank( comm_rank );

        // grid-coordinate DOF association (edge-to-node connectivity)
        fldAssocIedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(3, -1), edge );

        fldAssocCell.setAssociativity( c1 ).setEdgeSign( 1, 1 );
        fldAssocCell.setAssociativity( c2 ).setEdgeSign(-1, 3 );

        edge++;
      }
    }

    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Line>( fldAssocIedge );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;
  }

  // grid boundary-edge field variables
  // Note: all edges oriented so domain is on its left

  // lower boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, ii );

    fldAssocBedge.setGroupLeft( 0 );

    for (int i = 0; i < ii; i++)
    {
      n1 = i;
      n2 = i + 1;
      c1 = i;
      edge = i;

      // set the processor rank
      fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), edge );

      fldAssocCell.setAssociativity( c1 ).setEdgeSign( 1, 0 );
    }

    boundaryTraceGroups_[iBottom] = new FieldTraceGroupType<Line>( fldAssocBedge );
    boundaryTraceGroups_[iBottom]->setDOF(DOF_, nDOF_);
  }

  // right boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocBedge.setGroupLeft( 0 );

    // edge element grid-coordinate DOF associativity (edge-to-node connectivity)
    for (int j = 0; j < jj; j++)
    {
      n1 = j*(ii+1) + ii;
      n2 = n1 + (ii+1);
      c1 = j*(ii) + ii - 1;
      edge = j;

      // set the processor rank
      fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

      // edge-to-cell connectivity
      fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), edge );

      fldAssocCell.setAssociativity( c1 ).setEdgeSign( 1, 1 );
    }

    boundaryTraceGroups_[iRight] = new FieldTraceGroupType<Line>( fldAssocBedge );
    boundaryTraceGroups_[iRight]->setDOF(DOF_, nDOF_);
  }

  // upper boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, ii );

    fldAssocBedge.setGroupLeft( 0 );

    for (int i = ii-1; i >= 0; i--)
    {
      n1 = jj*(ii+1) + i + 1;
      n2 = jj*(ii+1) + i;
      c1 = (jj-1)*(ii) + i;
      edge = ii-1 - i;

      // set the processor rank
      fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), edge );

      fldAssocCell.setAssociativity( c1 ).setEdgeSign( 1, 2 );
    }

    boundaryTraceGroups_[iTop] = new FieldTraceGroupType<Line>( fldAssocBedge );
    boundaryTraceGroups_[iTop]->setDOF(DOF_, nDOF_);
  }

  // left boundary
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocBedge.setGroupLeft( 0 );

    for (int j = jj-1; j >= 0; j--)
    {
      n1 = j*(ii+1) + (ii+1);
      n2 = j*(ii+1);
      c1 = j*(ii);
      edge = jj-1 - j;

      // set the processor rank
      fldAssocBedge.setAssociativity( edge ).setRank( comm_rank );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setAssociativity( edge ).setNodeGlobalMapping( {n1, n2} );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft( CanonicalTraceToCell(3, 1), edge );

      fldAssocCell.setAssociativity( c1 ).setEdgeSign( 1, 3 );
    }

    boundaryTraceGroups_[iLeft] = new FieldTraceGroupType<Line>( fldAssocBedge );
    boundaryTraceGroups_[iLeft]->setDOF(DOF_, nDOF_);
  }

  cellGroups_[0] = new FieldCellGroupType<Quad>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  //Check that the grid is correct
  checkGrid();
}

}
