// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField3D_8Tet_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

XField3D_8Tet_X1_1Group::XField3D_8Tet_X1_1Group()
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(10);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(1);
  resizeCellGroups(1);

  // nodal coordinates for the two triangles.
  DOF(0) = {0, 0, 0};
  DOF(1) = {1, 0, 0};
  DOF(2) = {0, 1, 0};
  DOF(3) = {0, 0, 1};
  DOF(4) = {0.0, 0.5, 0.5};
  DOF(5) = {0.5, 0.0, 0.5};
  DOF(6) = {0.5, 0.5, 0.0};
  DOF(7) = {0.0, 0.5, 0.0};
  DOF(8) = {0.0, 0.0, 0.5};
  DOF(9) = {0.5, 0.0, 0.0};

  // volume field variable
  FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1, 8 );

  // Index table for each tet
  const int tets[8][4] = { {7, 6, 2, 4},
                           {8, 5, 4, 3},
                           {9, 1, 6, 5},
                           {0, 9, 7, 8},

                           {7, 6, 4, 5},
                           {8, 4, 5, 7},
                           {9, 5, 6, 7},
                           {9, 7, 8, 5} };

  //element volume associativity
  for (int elem = 0; elem < 8; elem++)
  {
    fldAssocCell.setAssociativity( elem ).setRank( comm_rank );
    fldAssocCell.setAssociativity( elem ).setNodeGlobalMapping( tets[elem], 4 );
  }

  nElem_ = fldAssocCell.nElem();

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  // interior-face field variable

  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocIface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 8 );

  fldAssocIface.setGroupLeft( 0 );
  fldAssocIface.setGroupRight( 0 );

  int faceNodes[3];

  int Itrace_elemLR_list[8][2] = {{0,4},{1,5},{2,6},{3,7},
                                  {4,5},{4,6},{5,7},{6,7}};

  int Itrace_faceLR_list[8][2] = {{2,3},{3,3},{1,3},{0,3},
                                  {1,0},{2,0},{1,0},{2,2}};

  for (int faceI = 0; faceI < 8; faceI++)
  {
    int elemL = Itrace_elemLR_list[faceI][0];
    int elemR = Itrace_elemLR_list[faceI][1];

    int faceL = Itrace_faceLR_list[faceI][0];
    int faceR = Itrace_faceLR_list[faceI][1];

    for (int i = 0; i < 3; i++)
      faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

    fldAssocIface.setAssociativity( faceI ).setRank( comm_rank );
    fldAssocIface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
    fldAssocIface.setElementLeft( elemL, faceI );
    fldAssocIface.setElementRight( elemR, faceI );
    fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), faceI );
    fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), faceI );

    // face signs for elements (L is +, R is -)
    fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
    fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(faceI).orientation, faceR );
  }

  //Create the interior face group
  interiorTraceGroups_[0] = new FieldTraceGroupType<Triangle>( fldAssocIface );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);


  // boundary-face field variable
  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 16 );

  fldAssocBface.setGroupLeft( 0 );

  int Btrace_elem_face_list[16][2] = {{0,0},{1,0},{2,0},{4,0},
                                      {1,1},{0,1},{3,1},{5,2},
                                      {2,2},{1,2},{3,2},{7,1},
                                      {0,3},{2,3},{3,3},{6,1}};

  for (int faceB = 0; faceB < 16; faceB++)
  {
    int elemL = Btrace_elem_face_list[faceB][0];
    int faceL = Btrace_elem_face_list[faceB][1];

    for (int i = 0; i < 3; i++)
      faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

    fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
    fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
    fldAssocBface.setElementLeft( elemL, faceB );
    fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

    // face signs for elements (L is +, R is -)
    fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  }

  //Create the boundary group
  boundaryTraceGroups_[0] = new FieldTraceGroupType<Triangle>( fldAssocBface );
  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);

  //Finally create the cell groups now that all Face signs have been set
  cellGroups_[0] = new FieldCellGroupType<Tet>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
