// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_Annulus_Triangle_X1.h"

#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

//#include "Field/output_Tecplot.h"  // required for output_Tecplot dump

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Annulus_Triangle_X1_test )
{
  const int ii = 32;
  const int jj = 1;
  const Real R1 = 1.0;         // R_1: inner radius
  const Real h = 1.0;          // thickness. Must be 1
  const Real thetamin = 0.0;   // degree angle
  const Real thetamax = 90.0;  // degree angle

  // Check is called at the end of the constructor, hence this is actually tested
  XField2D_Annulus_Triangle_X1 xfld( ii, jj, R1, h, thetamin, thetamax );

  const Real close_tol = 1.e-10;
  const Real small_tol = 1.e-10;

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );

  SANS_CHECK_CLOSE( sqrt(2.0), xfld.DOF(16)[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sqrt(2.0), xfld.DOF(16)[1], small_tol, close_tol);

  SANS_CHECK_CLOSE( sqrt(0.5), xfld.DOF(17 + 32)[0], small_tol, close_tol);
  SANS_CHECK_CLOSE( sqrt(0.5), xfld.DOF(17 + 32)[1], small_tol, close_tol);

  // Tecplot dump
//  output_Tecplot(xfld, "tmp/test.dat");
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
