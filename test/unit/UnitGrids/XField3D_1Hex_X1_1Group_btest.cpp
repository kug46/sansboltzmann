// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField3D_1Hex_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_1Hex_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_1Hex_X1_1Group_test )
{
  const Real small_tol = 1e-13;
  const Real tol = 1e-13;

  typedef std::array<int,6> Int6;

  XField3D_1Hex_X1_1Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 8 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[2], 1 );

  // volume field variable

  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Hex) );

  const XField3D_1Hex_X1_1Group::FieldCellGroupType<Hex>& xfldVolume = xfld.getCellGroup<Hex>(0);

  int nodeMap[8];

  xfldVolume.associativity(0).getNodeGlobalMapping( nodeMap, 8 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );
  BOOST_CHECK_EQUAL( nodeMap[4], 4 );
  BOOST_CHECK_EQUAL( nodeMap[5], 5 );
  BOOST_CHECK_EQUAL( nodeMap[6], 6 );
  BOOST_CHECK_EQUAL( nodeMap[7], 7 );

  Int6 faceSign;

  faceSign = xfldVolume.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );
  BOOST_CHECK_EQUAL( faceSign[4], +1 );
  BOOST_CHECK_EQUAL( faceSign[5], +1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 6 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(0).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(1).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(2).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(3).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(4).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Quad>(5).topoTypeID() == typeid(Quad) );

  const XField3D_1Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBedge0 = xfld.getBoundaryTraceGroup<Quad>(0);
  const XField3D_1Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBedge1 = xfld.getBoundaryTraceGroup<Quad>(1);
  const XField3D_1Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBedge2 = xfld.getBoundaryTraceGroup<Quad>(2);
  const XField3D_1Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBedge3 = xfld.getBoundaryTraceGroup<Quad>(3);
  const XField3D_1Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBedge4 = xfld.getBoundaryTraceGroup<Quad>(4);
  const XField3D_1Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBedge5 = xfld.getBoundaryTraceGroup<Quad>(5);

  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 1 );

  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );

  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );

  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );
  BOOST_CHECK_EQUAL( nodeMap[3], 2 );

  xfldBedge4.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  xfldBedge5.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );
  BOOST_CHECK_EQUAL( nodeMap[3], 7 );


  XField3D_1Hex_X1_1Group::FieldTraceGroupType<Quad>::ElementType<> xfldElem( xfldBedge0.basis() );
  XField3D_1Hex_X1_1Group::FieldTraceGroupType<Quad>::ElementType<>::RefCoordType sRef;
  XField3D_1Hex_X1_1Group::FieldTraceGroupType<Quad>::ElementType<>::VectorX N;

  // Check areas and unit normals
  Real nxTrue, nyTrue, nzTrue;
  sRef = 1./2.;

  xfldBedge0.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 1., 1e-12 );

  nxTrue =  0;
  nyTrue =  0;
  nzTrue = -1;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  xfldBedge1.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 1., 1e-12 );

  nxTrue =  0;
  nyTrue = -1;
  nzTrue =  0;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  xfldBedge2.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 1., 1e-12 );

  nxTrue =  1;
  nyTrue =  0;
  nzTrue =  0;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  xfldBedge3.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 1., 1e-12 );

  nxTrue =  0;
  nyTrue =  1;
  nzTrue =  0;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  xfldBedge4.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 1., 1e-12 );

  nxTrue = -1;
  nyTrue =  0;
  nzTrue =  0;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  xfldBedge5.getElement( xfldElem, 0 );
  BOOST_CHECK_CLOSE( xfldElem.area(), 1., 1e-12 );

  nxTrue =  0;
  nyTrue =  0;
  nzTrue =  1;

  xfldElem.unitNormal( sRef, N );
  SANS_CHECK_CLOSE( nxTrue, N[0], small_tol, tol );
  SANS_CHECK_CLOSE( nyTrue, N[1], small_tol, tol );
  SANS_CHECK_CLOSE( nzTrue, N[2], small_tol, tol );


  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBedge0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge4.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge5.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge4.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge5.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceLeft(0).trace, 3 );
  BOOST_CHECK_EQUAL( xfldBedge4.getCanonicalTraceLeft(0).trace, 4 );
  BOOST_CHECK_EQUAL( xfldBedge5.getCanonicalTraceLeft(0).trace, 5 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge4.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge5.getCanonicalTraceLeft(0).orientation, 1 );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
