// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "unit/UnitGrids/XField4D_Box_Ptope_X1.h"

#include "Field/Partition/XField_Lagrange.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include "geometry/body.h"
#include "library/cube.h"
#include "library/geometry.h"
#include "library/tesseract.h"
#include "mesh/boundary.h"
#endif

namespace SANS
{

//----------------------------------------------------------------------------//
// create pentatope grid in unit box with ii x jj x kk x mm (ptope) elements
/*
// volume elements in 1 group: 24*ii*jj*kk*mm pentatopes
// interior-triangle elements in 4 groups: x-const, y-const, z-const, hex-interior
// boundary-triangle elements in 8 groups: x-min, x-max, y-min, y-max, z-min, z-max, w-min, w-max

// see dali.jpg for inspiration

// the node ordering is based on the tesseract unitgrids
*/

const int XField4D_Box_Ptope_X1::iXmin = 3;
const int XField4D_Box_Ptope_X1::iXmax = 4;
const int XField4D_Box_Ptope_X1::iYmin = 2;
const int XField4D_Box_Ptope_X1::iYmax = 5;
const int XField4D_Box_Ptope_X1::iZmin = 0;
const int XField4D_Box_Ptope_X1::iZmax = 7;
const int XField4D_Box_Ptope_X1::iWmin = 1;
const int XField4D_Box_Ptope_X1::iWmax = 6;

//---------------------------------------------------------------------------//
XField4D_Box_Ptope_X1::
XField4D_Box_Ptope_X1(mpi::communicator comm,
                     const unsigned long ii, const unsigned long jj, const unsigned long kk, const unsigned long mm,
                     std::array<bool, 4> periodic) :
  XField4D_Box_Ptope_X1(comm, ii, jj, kk, mm, 0, 1, 0, 1, 0, 1, 0, 1, periodic)
{
}

#ifdef SANS_AVRO

//---------------------------------------------------------------------------//
XField4D_Box_Ptope_X1::
XField4D_Box_Ptope_X1(mpi::communicator comm,
                              const unsigned long ii, const unsigned long jj, const unsigned long kk, const unsigned long mm,
                              Real xmin, Real xmax,
                              Real ymin, Real ymax,
                              Real zmin, Real zmax,
                              Real wmin, Real wmax,
                              std::array<bool, 4> periodic ) :
  XField<PhysD4, TopoD4>(comm),
  dims_{ii + 1, jj + 1, kk + 1, mm + 1},
  lengths_{1, 1, 1, 1},
  mesh_(lengths_, dims_)
{
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);
  std::vector<Real> zvec(kk+1);
  std::vector<Real> wvec(mm+1);

  // construct vectors
  for (unsigned int i = 0; i < ii+1; i++)
    xvec[i] = xmin + (xmax - xmin)*i/Real(ii);

  for (unsigned int j = 0; j < jj+1; j++)
    yvec[j] = ymin + (ymax - ymin)*j/Real(jj);

  for (unsigned int k = 0; k < kk+1; k++)
    zvec[k] = zmin + (zmax - zmin)*k/Real(kk);

  for (unsigned int m = 0; m < mm+1; m++)
    wvec[m] = wmin + (wmax - wmin)*m/Real(mm);

  // create the accompanying body
  createBody();

  XField_Lagrange<PhysD4> xfldin(comm);

  generateGrid(comm, xfldin, xvec, yvec, zvec, wvec, periodic);
}

//---------------------------------------------------------------------------//

XField4D_Box_Ptope_X1::
XField4D_Box_Ptope_X1(mpi::communicator comm,
                              const std::vector<Real>& xvec,
                              const std::vector<Real>& yvec,
                              const std::vector<Real>& zvec,
                              const std::vector<Real>& wvec,
                              std::array<bool, 4> periodic) :
  XField<PhysD4, TopoD4>(comm),
  dims_{xvec.size(), yvec.size(), zvec.size(), wvec.size()},
  lengths_{1, 1, 1, 1},
  mesh_(lengths_, dims_)
{
  std::size_t ii= xvec.size() - 1;
  std::size_t jj= yvec.size() - 1;
  std::size_t kk= zvec.size() - 1;
  std::size_t mm= wvec.size() - 1;

  // check ascending order
  for (std::size_t i= 0; i < ii; i++)
    SANS_ASSERT(xvec[i] < xvec[i + 1]);

  for (std::size_t j= 0; j < jj; j++)
    SANS_ASSERT(yvec[j] < yvec[j + 1]);

  for (std::size_t k= 0; k < kk; k++)
    SANS_ASSERT(zvec[k] < zvec[k + 1]);

  for (std::size_t m= 0; m < mm; m++)
    SANS_ASSERT(wvec[m] < wvec[m + 1]);

  // create the accompanying body
  createBody();

  XField_Lagrange<PhysD4> xfldin(comm);

  generateGrid(comm, xfldin, xvec, yvec, zvec, wvec, periodic);
}

//---------------------------------------------------------------------------//

XField4D_Box_Ptope_X1::
XField4D_Box_Ptope_X1(mpi::communicator comm, const std::string& filename,
                     const unsigned long ii, const unsigned long jj, const unsigned long kk, const unsigned long mm,
                     std::array<bool, 4> periodic) :
  XField4D_Box_Ptope_X1(comm, filename, ii, jj, kk, mm, 0, 1, 0, 1, 0, 1, 0, 1, periodic)
{
}

//---------------------------------------------------------------------------//

XField4D_Box_Ptope_X1::
XField4D_Box_Ptope_X1(mpi::communicator comm, const std::string& filename,
                     const unsigned long ii, const unsigned long jj, const unsigned long kk, const unsigned long mm,
                     Real xmin, Real xmax,
                     Real ymin, Real ymax,
                     Real zmin, Real zmax,
                     Real wmin, Real wmax,
                     std::array<bool, 4> periodic ) :
  XField<PhysD4, TopoD4>(comm),
  dims_{ii + 1, jj + 1, kk + 1, mm + 1},
  lengths_{1, 1, 1, 1},
  mesh_(lengths_, dims_)
{

  SANS_DEVELOPER_EXCEPTION("still needs unit testing implementations for partition file input.");

//  std::vector<Real> xvec(ii + 1);
//  std::vector<Real> yvec(jj + 1);
//  std::vector<Real> zvec(kk + 1);
//  std::vector<Real> wvec(mm + 1);
//
//  // construct vectors
//  for (unsigned int i= 0; i < ii + 1; i++)
//    xvec[i]= xmin + (xmax - xmin)*i/Real(ii);
//
//  for (unsigned int j= 0; j < jj + 1; j++)
//    yvec[j]= ymin + (ymax - ymin)*j/Real(jj);
//
//  for (unsigned int k= 0; k < kk + 1; k++)
//    zvec[k]= zmin + (zmax - zmin)*k/Real(kk);
//
//  for (unsigned int m= 0; m < mm + 1; m++)
//    wvec[m]= wmin + (wmax - wmin)*m/Real(mm);
//
//  // create the accompanying body
//  createBody();
//
//  // initialize the XField_Lagrange
//  XField_Lagrange<PhysD4> xfldin(comm, XFieldBalance::CellPartitionRead, filename);
//
//  // generate the KuhnFreudenthal grid!
//  generateGrid(comm, xfldin, xvec, yvec, zvec, wvec, periodic);

}

//---------------------------------------------------------------------------//

XField4D_Box_Ptope_X1::
XField4D_Box_Ptope_X1(mpi::communicator comm, const std::string& filename,
                     const std::vector<Real>& xvec,
                     const std::vector<Real>& yvec,
                     const std::vector<Real>& zvec,
                     const std::vector<Real>& wvec,
                     std::array<bool, 4> periodic) :
  XField<PhysD4, TopoD4>(comm),
  dims_{xvec.size(), yvec.size(), zvec.size(), wvec.size()},
  lengths_{1, 1, 1, 1},
  mesh_(lengths_, dims_)
{

  SANS_DEVELOPER_EXCEPTION("still needs unit testing implementations for partition file input.");

//  std::size_t ii= xvec.size() - 1;
//  std::size_t jj= yvec.size() - 1;
//  std::size_t kk= zvec.size() - 1;
//  std::size_t mm= wvec.size() - 1;
//
//  // check ascending order
//  for (std::size_t i= 0; i < ii; i++)
//    SANS_ASSERT(xvec[i] < xvec[i + 1]);
//
//  for (std::size_t j= 0; j < jj; j++)
//    SANS_ASSERT(yvec[j] < yvec[j + 1]);
//
//  for (std::size_t k= 0; k < kk; k++)
//    SANS_ASSERT(zvec[k] < zvec[k + 1]);
//
//  for (std::size_t m= 0; m < mm; m++)
//    SANS_ASSERT(wvec[m] < wvec[m + 1]);
//
//  // create the accompanying body
//  createBody();
//
//  XField_Lagrange<PhysD4> xfldin(comm, XFieldBalance::CellPartitionRead, filename);
//
//  generateGrid(comm, xfldin, xvec, yvec, zvec, wvec, periodic);

}

//---------------------------------------------------------------------------//

void
XField4D_Box_Ptope_X1::
generateGrid(mpi::communicator comm,
              XField_Lagrange<PhysD4>& xfldin,
              const std::vector<Real>& xvec,
              const std::vector<Real>& yvec,
              const std::vector<Real>& zvec,
              const std::vector<Real>& wvec,
              std::array<bool, 4> periodic)
{

  using avro::index_t;
  using avro::coord_t;

  SANS_ASSERT(xvec.size() > 0 && yvec.size() > 0 && zvec.size() > 0 && wvec.size() > 0);

  int order= 1;

  int ii= (int) xvec.size() - 1;
  int jj= (int) yvec.size() - 1;
  int kk= (int) zvec.size() - 1;
  int mm= (int) wvec.size() - 1;

  unsigned int nnode= (ii + 1)*(jj + 1)*(kk + 1)*(mm + 1);
  unsigned int nCell= 24*ii*jj*kk*mm;

  typename XField_Lagrange<PhysD4>::VectorX X0;
  typename XField_Lagrange<PhysD4>::VectorX dX;

  X0[0]= xvec[0];
  X0[1]= yvec[0];
  X0[2]= zvec[0];
  X0[3]= wvec[0];

  dX[0]= xvec[ii] - xvec[0];
  dX[1]= yvec[jj] - yvec[0];
  dX[2]= zvec[kk] - zvec[0];
  dX[3]= wvec[mm] - wvec[0];

  // create the grid-coordinate DOF arrays
  xfldin.sizeDOF(nnode);
  SANS_ASSERT(nnode == mesh_.vertices().nb());

  // add the degrees of freedom
  typename XField_Lagrange<PhysD4>::VectorX X;

  if (comm.rank() == 0)
  {
    for (index_t k= 0; k < mesh_.vertices().nb(); k++)
    {

      for (coord_t d= 0; d < mesh_.vertices().dim(); d++)
        X[d]= X0[d] + dX[d]*mesh_.vertices()[k][d];

      xfldin.addDOF(X);

    }
    printf("added vertices\n");
  }

  // reference the topology
  SANS_ASSERT(mesh_.nb_topologies() == 1);
  avro::Topology<avro::Simplex> &topology= mesh_.topology(0);
  topology.orient();

  // add the simplices
  int cellgroup= 0;
  xfldin.sizeCells(topology.nb());
  SANS_ASSERT(nCell == topology.nb());

  if (comm.rank() == 0)
  {
    std::vector<int> simplex(5, 0);

    for (index_t k= 0; k < topology.nb(); k++)
    {
      for (index_t j= 0; j < topology.nv(k); j++)
        simplex[j]= topology(k, j);

      xfldin.addCell(cellgroup, ePentatope, order, simplex);
    }
    printf("added cells\n");
  }

  // compute the boundary
  avro::Boundary<avro::Simplex> boundary(topology);
  boundary.extract();

  // size the boundary traces
  int nbnd= 0;
  for (index_t k= 0; k < boundary.nb_children(); k++)
    if (boundary.child(k)->number() == 3)
      nbnd += boundary.child(k)->nb();
  xfldin.sizeBoundaryTrace(nbnd);

  // add the boundary traces
  if (comm.rank() == 0)
  {
    std::vector<int> facet(mesh_.number());

    int group= 0;
    for (index_t k= 0; k < boundary.nb_children(); k++)
    {
      if (boundary.child(k)->number() != topology.number() - 1) continue;

      avro::Topology<avro::Simplex> &bndk= *boundary.child(k);

      // determine the entity this boundary is on
      avro::Entity * entity= boundary.entity(k);

      // the boundary group is the body index of the associated ego .... CHANGE ME
      group= entity->bodyIndex() - 1;

      for (index_t j= 0; j < bndk.nb(); j++)
      {
        // retrieve the facet indices
        for (index_t i= 0; i < bndk.nv(j); i++)
          facet[i]= bndk(j, i);

        xfldin.addBoundaryTrace(group, eTet, facet);
//        printf("group: %d, facet: {%d, %d, %d, %d}\n", group,
//               facet[0], facet[1], facet[2], facet[3]);
      }
    }
    printf("added boundary\n");

  }

  std::vector<PeriodicBCNodeMap> periodicity;

  // Construct periodic information if requested
  if (comm.rank() == 0)
    if (periodic[0] | periodic[1] | periodic[2] | periodic[3])
      SANS_DEVELOPER_EXCEPTION("Not ready for periodicity yet... sorry!");
  // LOOK AT 3D CASE IF YOU WANT TO USE PERIODICITY

  // Create the boundary periodicity
  xfldin.addBoundaryPeriodicity(periodicity);

  // Finalize the grid construction
  this->buildFrom(xfldin);
}

//---------------------------------------------------------------------------//

void
XField4D_Box_Ptope_X1::
createBody()
{

  const int dim= 4;
  std::vector<Real> x0(dim, 0.5);
  body_= std::make_shared<avro::library::Tesseract>(x0.data(), lengths_.data());

  // attach the mesh vertices to the geometry
  mesh_.vertices().findGeometry(*body_);

}

#else

XField4D_Box_Ptope_X1::
XField4D_Box_Ptope_X1(mpi::communicator comm,
                             const unsigned long ii, const unsigned long jj, const unsigned long kk, const unsigned long mm,
                             Real xmin, Real xmax,
                             Real ymin, Real ymax,
                             Real zmin, Real zmax,
                             Real wmin, Real wmax,
                             std::array<bool, 4> periodic ) :
 XField<PhysD4, TopoD4>(comm)
{
  SANS_DEVELOPER_EXCEPTION("avro required for this.");
}

XField4D_Box_Ptope_X1::
XField4D_Box_Ptope_X1(mpi::communicator comm,
                             const std::vector<Real>& xvec,
                             const std::vector<Real>& yvec,
                             const std::vector<Real>& zvec,
                             const std::vector<Real>& wvec,
                             std::array<bool, 4> periodic) :
 XField<PhysD4, TopoD4>(comm)
{
  SANS_DEVELOPER_EXCEPTION("avro required for this.");
}

XField4D_Box_Ptope_X1::
XField4D_Box_Ptope_X1(mpi::communicator comm, const std::string& filename,
                     const unsigned long ii, const unsigned long jj, const unsigned long kk, const unsigned long mm,
                     Real xmin, Real xmax,
                     Real ymin, Real ymax,
                     Real zmin, Real zmax,
                     Real wmin, Real wmax,
                     std::array<bool, 4> periodic ) :
  XField<PhysD4, TopoD4>(comm)
{
  SANS_DEVELOPER_EXCEPTION("avro required for this.");
}

//---------------------------------------------------------------------------//

XField4D_Box_Ptope_X1::
XField4D_Box_Ptope_X1(mpi::communicator comm, const std::string& filename,
                     const std::vector<Real>& xvec,
                     const std::vector<Real>& yvec,
                     const std::vector<Real>& zvec,
                     const std::vector<Real>& wvec,
                     std::array<bool, 4> periodic) :
  XField<PhysD4, TopoD4>(comm)
{
  SANS_DEVELOPER_EXCEPTION("avro required for this.");
}

#endif

}
