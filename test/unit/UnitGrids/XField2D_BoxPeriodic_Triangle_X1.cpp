// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_BoxPeriodic_Triangle_X1.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define SKIP_RIGHTTOP

namespace SANS
{
//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj (quad) elements
//
// domain is periodic in both X and Y

XField2D_BoxPeriodic_Triangle_X1::XField2D_BoxPeriodic_Triangle_X1( int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
{
  int comm_rank = comm_->rank();

  int nnode = (ii + 1)*(jj + 1);
  int nelem = 2*ii*jj;
#ifdef SKIP_RIGHTTOP
  int nedge = 3*ii*jj;
#else
  int nedge = (ii+1)*jj + ii*(jj+1) + ii*jj;
#endif

  // create the grid-coordinate DOF arrays
  resizeDOF( nnode );

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      DOF(j*(ii+1) + i)[0] = xmin + (xmax - xmin)*i/Real(ii);
      DOF(j*(ii+1) + i)[1] = ymin + (ymax - ymin)*j/Real(jj);
    }
  }

  // create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(0);
  resizeCellGroups(1);

  // grid area field variable

  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nelem );

  // area element grid-coordinate DOF associativity (cell-to-node connectivity)
  // edge signs for area elements (L is +, R is -)
  // NOTE: assumes edge orientations set below in interior-edge field variables

  ElementAssociativityConstructor<TopoD2, Triangle> xDOFAssocArea( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  int n1, n2, n3, n4;

  xDOFAssocArea.setRank( comm_rank );

  for (int j = 0; j < jj; j++)
  {
    for (int i = 0; i < ii; i++)
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  |\ |
      n3 = n2 + (ii+1);         //  | \|
      n4 = n1 + (ii+1);         // n1--n2

      xDOFAssocArea.setNodeGlobalMapping( {n1, n2, n4} );

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n3, n4, n2} );

      xDOFAssocArea.setEdgeSign( -1, 0 );
      xDOFAssocArea.setEdgeSign( +1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );
#if 0
      std::cout << "XField2D_BoxPeriodic_Triangle_X1: i = " << i << "  j = " << j
        << "  {n1,n2,n3,n4} = " << n1 << " " << n2 << " " << n3 << " " << n4 << std::endl;
#endif
    }
  }

#if 0
  std::cout << "XField2D_BoxPeriodic_Triangle_X1: dumping xfldArea" << std::endl;  xfldArea->dump(2);
#endif

  FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
  cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

  xfldElementGroup->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // grid interior-edge field variable
  FieldTraceGroupType<Line>* xfldIedge = NULL;
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType
    fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

  // edge element grid-coordinate DOF associativity (edge-to-node connectivity)

  ElementAssociativityConstructor<TopoD1, Line> xDOFAssocEdge( BasisFunctionLineBase::HierarchicalP1 );
  int offset = 0;

  xDOFAssocEdge.setRank( comm_rank );

  // horizontal edges
  for (int j = 0; j < jj+1; j++)
  {
#ifdef SKIP_RIGHTTOP
    if (j == jj) continue;
#endif
    for (int i = 0; i < ii; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + 1;

      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, j*(ii) + i + offset );
    }
  }
#ifdef SKIP_RIGHTTOP
  offset += ii*jj;
#else
  offset += ii*(jj+1);
#endif

  // vertical edges
  for (int i = 0; i < ii+1; i++)
  {
#ifdef SKIP_RIGHTTOP
    if (i == ii) continue;
#endif
    for (int j = 0; j < jj; j++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1);

      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, i*(jj) + j + offset );
    }
  }
#ifdef SKIP_RIGHTTOP
  offset += ii*jj;
#else
  offset += jj*(ii+1);
#endif

  // diagonal edges
  for (int j = 0; j < jj; j++)
  {
    for (int i = 0; i < ii; i++)
    {
      n1 = j*(ii+1) + i + 1;
      n2 = n1 + (ii+1) - 1;

      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, j*(ii) + i + offset );
    }
  }

  // edge-to-cell connectivity

  int edge;
  int c1, c2;

  // horizontal edges
  offset = 0;
  for (int j = 0; j < jj+1; j++)
  {
#ifdef SKIP_RIGHTTOP
    if (j == jj) continue;
#endif
    for (int i = 0; i < ii; i++)
    {
      edge = j*(ii) + i + offset;
      c1 = (j  )*(2*ii) + 2*i;
      c2 = (j-1)*(2*ii) + 2*i + 1;

      // periodic
      if (j == 0)  c2 = (jj-1)*(2*ii) + 2*i + 1;
      else if (j == jj)  c1 = 2*i;

      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );
    }
  }
#ifdef SKIP_RIGHTTOP
  offset += ii*jj;
#else
  offset += ii*(jj+1);
#endif

  // vertical edges
  for (int i = 0; i < ii+1; i++)
  {
#ifdef SKIP_RIGHTTOP
    if (i == ii) continue;
#endif
    for (int j = 0; j < jj; j++)
    {
      edge = i*(jj) + j + offset;
      c1 = j*(2*ii) + 2*i - 1;
      c2 = j*(2*ii) + 2*i;

      // periodic
      if (i == 0)  c1 = j*(2*ii) + 2*ii - 1;
      else if (i == ii)  c2 = j*(2*ii);

      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );
    }
  }
#ifdef SKIP_RIGHTTOP
  offset += ii*jj;
#else
  offset += jj*(ii+1);
#endif

  // diagonal edges
  for (int j = 0; j < jj; j++)
  {
    for (int i = 0; i < ii; i++)
    {
      edge = j*(ii) + i + offset;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );
    }
  }

  fldAssocIedge.setGroupLeft( 0 );
  fldAssocIedge.setGroupRight( 0 );

  interiorTraceGroups_[0] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );

  xfldIedge->setDOF(DOF_, nDOF_);

  this->initDefaultElmentIDs();

  //Check that the grid is correct
  //TODO: The periodic condition messes up the checks at the moment
  //check();
}

}
