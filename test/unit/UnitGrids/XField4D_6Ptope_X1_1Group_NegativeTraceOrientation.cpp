// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "unit/UnitGrids/XField4D_6Ptope_X1_1Group_NegativeTraceOrientation.h"

//#include "Field/Partition/XField_Lagrange.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

#define PRINT_FACE_NODES( faceNodes ) printf("faceNodes = (%d,%d,%d,%d)\n",faceNodes[0],faceNodes[1],faceNodes[2],faceNodes[3]);

#define PRINT_TRACE_AND_ORIENTATION( elemL , elemR ) \
   std::cout<< TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemL], 5).trace << "," << \
               TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemL], 5).orientation <<std::endl;  \
   std::cout<< TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemR], 5).trace << "," << \
               TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemR], 5).orientation <<std::endl

XField4D_6Ptope_X1_1Group_NegativeTraceOrientation::
XField4D_6Ptope_X1_1Group_NegativeTraceOrientation( mpi::communicator& comm )
{
  generate(comm);
}

void
XField4D_6Ptope_X1_1Group_NegativeTraceOrientation::generate( mpi::communicator& comm )
{
  int comm_rank = comm.rank();

  // the code below can work for all orientations if some of them are mapped correctly
  // it means there's a bug somewhere but I don't know where and I simply need
  // a grid to test the local splits...
  int orient = -1;

  //Create the DOF arrays
  resizeDOF(10);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(1);
  resizeCellGroups(1);

  // nodal coordinates for the 5 tets.
  DOF(0) = {0, 0, 0, 0};
  DOF(1) = {1, 0, 0, 0};
  DOF(2) = {0, 1, 0, 0};
  DOF(3) = {0, 0, 1, 0};
  DOF(4) = {0, 0, 0, 1};

  DOF(5) = { 1, 1, 1, 1};
  DOF(6) = {-1, 0, 0, 0};
  DOF(7) = { 0,-1, 0, 0};
  DOF(8) = { 0, 0,-1, 0};
  DOF(9) = { 0, 0, 0,-1};

  // spacetime field variable
  FieldCellGroupType<Pentatope>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1, 6 );

  // original configuration: center pentatope has orientation +1, all neighbors have orientation -1
  const int pets_orient1[6][5] = { {0, 1, 2, 3, 4},
                                   {1, 2, 4, 3, 5},
                                   {0, 2, 3, 4, 6},
                                   {0, 1, 4, 3, 7},
                                   {0, 1, 2, 4, 8},
                                   {0, 1, 3, 2, 9} };

  int pets[6][5];
  pets[0][0] = 0; pets[0][1] = 1; pets[0][2] = 2; pets[0][3] = 3; pets[0][4] = 4;

  // rotate neighbors so that they all have orientation -orient
  const int (*Orientation)[ Tet::NNode ] = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::OrientPos;

  for (int i=1; i<6; i++)
  {
    for (int j=0; j<4; j++)
      pets[i][j] = pets_orient1[i][ Orientation[-orient-1][j] ];
    pets[i][4] = pets_orient1[i][4];
  }

  // set the cell processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 3 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 4 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 5 ).setRank( comm_rank );

  // element volume associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( pets[0], 5 );
  fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( pets[1], 5 );
  fldAssocCell.setAssociativity( 2 ).setNodeGlobalMapping( pets[2], 5 );
  fldAssocCell.setAssociativity( 3 ).setNodeGlobalMapping( pets[3], 5 );
  fldAssocCell.setAssociativity( 4 ).setNodeGlobalMapping( pets[4], 5 );
  fldAssocCell.setAssociativity( 5 ).setNodeGlobalMapping( pets[5], 5 );

  nElem_ = fldAssocCell.nElem();

  const int (*TraceNodes)[ Tet::NTrace ] = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::TraceNodes;

  // interior-face field variable
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocIface( BasisFunctionVolumeBase<Tet>::LagrangeP1, 5 );

  fldAssocIface.setGroupLeft( 0 );
  fldAssocIface.setGroupRight( 0 );

  // face-cell associativity
  int faceNodes[4];
  int elemL, elemR;
  int faceL, faceR;

  // face between center and front tets - (orientation of face from front tet = -3)
  elemL = 0; faceL = 0;
  elemR = 1; faceR = 4;
  for (int i = 0; i < 4; i++)
  {
    faceNodes[i] = pets[elemL][TraceNodes[faceL][i]];
  }
  //PRINT_FACE_NODES( faceNodes );

  fldAssocIface.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 0 ).setNodeGlobalMapping( faceNodes, 4 );
  fldAssocIface.setElementLeft( elemL, 0 );
  fldAssocIface.setElementRight( elemR, 0 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemL], 5), 0 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemR], 5), 0 );
  //PRINT_TRACE_AND_ORIENTATION(elemL,elemR);

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(0).orientation, faceR );

  // Face between center and left tets - (orientation of face from left tet = -1)
  elemL = 0; faceL = 1;
  elemR = 2; faceR = 4;
  for (int i = 0; i < 4; i++)
  {
    faceNodes[i] = pets[elemL][TraceNodes[faceL][i]];
  }
  //PRINT_FACE_NODES( faceNodes );

  fldAssocIface.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 1 ).setNodeGlobalMapping( faceNodes, 4 );
  fldAssocIface.setElementLeft( elemL, 1 );
  fldAssocIface.setElementRight( elemR, 1 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemL], 5), 1 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemR], 5), 1 );
  //PRINT_TRACE_AND_ORIENTATION(elemL,elemR);

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(1).orientation, faceR );

  // face between center and bottom tets - (orientation of face from bottom tet = -2)
  elemL = 0; faceL = 2;
  elemR = 3; faceR = 4;
  for (int i = 0; i < 4; i++)
  {
    faceNodes[i] = pets[elemL][TraceNodes[faceL][i]];
  }
  //PRINT_FACE_NODES( faceNodes );

  fldAssocIface.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 2 ).setNodeGlobalMapping( faceNodes, 4 );
  fldAssocIface.setElementLeft( elemL, 2 );
  fldAssocIface.setElementRight( elemR, 2 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemL], 5), 2 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemR], 5), 2 );
  //PRINT_TRACE_AND_ORIENTATION(elemL,elemR);

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(2).orientation, faceR );


  // face between center and rear tets - (orientation of face from rear tet = -3)
  elemL = 0; faceL = 3;
  elemR = 4; faceR = 4;
  for (int i = 0; i < 4; i++)
  {
    faceNodes[i] = pets[elemL][TraceNodes[faceL][i]];
  }
  //PRINT_FACE_NODES( faceNodes );

  fldAssocIface.setAssociativity( 3 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 3 ).setNodeGlobalMapping( faceNodes, 4 );
  fldAssocIface.setElementLeft( elemL, 3 );
  fldAssocIface.setElementRight( elemR, 3 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemL], 5), 3 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemR], 5), 3 );
  //PRINT_TRACE_AND_ORIENTATION(elemL,elemR);

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(3).orientation, faceR );

  // face between center and rear tets - (orientation of face from rear tet = -3)
  elemL = 0; faceL = 4;
  elemR = 5; faceR = 4;
  for (int i = 0; i < 4; i++)
  {
    faceNodes[i] = pets[elemL][TraceNodes[faceL][i]];
  }
  //PRINT_FACE_NODES( faceNodes );

  fldAssocIface.setAssociativity( 4 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 4 ).setNodeGlobalMapping( faceNodes, 4 );
  fldAssocIface.setElementLeft( elemL, 4 );
  fldAssocIface.setElementRight( elemR, 4 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemL], 5), 4 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCanonicalTrace(faceNodes, 4, pets[elemR], 5), 4 );
  //PRINT_TRACE_AND_ORIENTATION(elemL,elemR);

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(4).orientation, faceR );

  // create the interior face group
  interiorTraceGroups_[0] = new FieldTraceGroupType<Tet>( fldAssocIface );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

  // boundary-face field variable: number of boundary faces = 4 tets on boundary * 5 pentatopes
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface( BasisFunctionVolumeBase<Tet>::LagrangeP1, 20 );

  fldAssocBface.setGroupLeft( 0 );

  int faceB = 0;

  for (int elemL=1; elemL<6; elemL++)
  {
    for (int faceL=0; faceL<4; faceL++)
    {
      for (int i = 0; i < 4; i++)
        faceNodes[i] = pets[elemL][TraceNodes[faceL][i]];

      fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
      fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 4 );
      fldAssocBface.setElementLeft( elemL, faceB );
      fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

      // face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
      faceB++;
    }
  }

  // must be 20 boundary faces
  SANS_ASSERT( faceB == 20 );

  // create the boundary group
  boundaryTraceGroups_[0] = new FieldTraceGroupType<Tet>( fldAssocBface );
  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);

  // finally create the cell groups now that all Face signs have been set
  cellGroups_[0] = new FieldCellGroupType<Pentatope>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  // check that the grid is correct
  checkGrid();
}

}
