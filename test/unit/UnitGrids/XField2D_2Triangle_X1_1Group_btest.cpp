// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_2Triangle_X1_1Group.h"
#include "XField2D_2Triangle_GhostBoundary_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Quadrature/Quadrature.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_2Triangle_X1_1Group_test )
{
  typedef std::array<int,3> Int3;

  XField2D_2Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 4 );

  BOOST_CHECK_THROW( xfld.nDOFCellGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFInteriorTraceGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFBoundaryTraceGroup(0), DeveloperException );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField2D_2Triangle_X1_1Group::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);

  int nodeMap[3];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  Int3 edgeSign;

  edgeSign = xfldArea.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField2D_2Triangle_X1_1Group::FieldTraceGroupType<Line>& xfldIedge = xfld.getInteriorTraceGroup<Line>(0);

  xfldIedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldIedge.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(0), 1 );

  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(0).trace, 0 );

  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(0).orientation, -1 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Line) );

  const XField2D_2Triangle_X1_1Group::FieldTraceGroupType<Line>& xfldBedge = xfld.getBoundaryTraceGroup<Line>(0);

  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldBedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBedge.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldBedge.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBedge.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(1), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(2), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(3), 0 );

  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(2).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(3).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(1).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(2).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(3).orientation, 1 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_2Triangle_GhostBoundary_X1_1Group_test )
{
  typedef typename DLA::VectorS<2,Real> VectorX;

  XField2D_2Triangle_X1_1Group xfld;
  BOOST_CHECK_EQUAL( xfld.nDOF(), 4 );

  XField2D_2Triangle_GhostBoundary_X1_1Group xfld_ghost;
  BOOST_CHECK_EQUAL( xfld_ghost.nDOF(), 4 );

  XField2D_2Triangle_GhostBoundary_X1_1Group xfld_ghost2(false);
  BOOST_CHECK_EQUAL( xfld_ghost2.nDOF(), 4 );


  // area field variable
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  typedef XField2D_2Triangle_X1_1Group::FieldCellGroupType<Triangle> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  //Get order 4 quad points
  Quadrature<TopoD2, Triangle> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType RefCoord;  // reference-element coordinates
  VectorX qeval, qeval_ghost, qeval_ghost2;

  const Real tol = 1e-11;
  // loop over local groups and elements and find their corresponding global
  // then check they match at some quad points
  for (int cellGroup = 0; cellGroup < xfld.nCellGroups(); cellGroup++)
    for (int elem = 0; elem < xfld.getCellGroup<Triangle>(cellGroup).nElem(); elem++)
    {
      const FieldCellGroupType& xfld_cellgrp = xfld.getCellGroup<Triangle>(cellGroup);
      ElementFieldClass fldElem( xfld_cellgrp.basis() );
      xfld_cellgrp.getElement(fldElem, elem);

      const FieldCellGroupType& xfld_ghost_cellgrp = xfld_ghost.getCellGroup<Triangle>(cellGroup);
      ElementFieldClass fldElem_ghost( xfld_ghost_cellgrp.basis() );
      xfld_ghost_cellgrp.getElement(fldElem_ghost, elem);

      const FieldCellGroupType& xfld_ghost2_cellgrp = xfld_ghost2.getCellGroup<Triangle>(cellGroup);
      ElementFieldClass fldElem_ghost2( xfld_ghost2_cellgrp.basis() );
      xfld_ghost2_cellgrp.getElement(fldElem_ghost2, elem);

      //Check if the solutions are equal at a few quadrature points
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.coordinates( iquad, RefCoord );

        fldElem_ghost.coordinates ( RefCoord, qeval_ghost);
        fldElem_ghost2.coordinates( RefCoord, qeval_ghost2);

        fldElem.coordinates(RefCoord, qeval);

        for (int i=0; i<2; i++)
        {
          SANS_CHECK_CLOSE(qeval_ghost[i],  qeval[i], tol, tol);
          SANS_CHECK_CLOSE(qeval_ghost2[i], qeval[i], tol, tol);
        }
      }
    }

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
