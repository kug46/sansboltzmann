// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD3D_5TET_X1_1GROUP_ALLORIENTATIONS
#define XFIELD3D_5TET_X1_1GROUP_ALLORIENTATIONS

#include "Field/XFieldVolume.h"

namespace SANS
{
// A unit grid that consists of 5 tetrahedra within a single group,
// with the interior triangle faces of the outer tets rotated to
// match the specified orientation.


class XField3D_5Tet_X1_1Group_AllOrientations : public XField<PhysD3,TopoD3>
{
public:
  explicit XField3D_5Tet_X1_1Group_AllOrientations(int orient)
  {
         if (orient>0)
      XField3D_5Tet_X1_1Group_AllOrientations_Positive(orient);
    else if (orient<0)
      XField3D_5Tet_X1_1Group_AllOrientations_Negative(orient);
    else
      SANS_DEVELOPER_EXCEPTION( "XField3D_4Tet_X1_1Group_AllOrientations: Invalid orientation for generating mesh: sign = %d", orient );

  };

protected:

  void XField3D_5Tet_X1_1Group_AllOrientations_Positive(int orient);
  void XField3D_5Tet_X1_1Group_AllOrientations_Negative(int orient);

};

}

#endif //XFIELD3D_5TET_X1_1GROUP_ALLORIENTATIONS
