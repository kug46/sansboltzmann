// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_1Line_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_1Line_X1_1Group.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_1Line_X1_1Group_DefaultCoordinates_test )
{
  // ---------- Set up a 1-element line grid ----------
  XField2D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 2 );

  // ---------- Check node values ----------
  // 1st node
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );
  // 2nd node
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );

  // ---------- Check line field variable ----------
  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField2D_1Line_X1_1Group::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  // ---------- Check interior-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // ---------- Check boundary-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField2D_1Line_X1_1Group::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField2D_1Line_X1_1Group::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  // boundary trace 1
  // node map
  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(),-1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(), 0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  // boundary trace 2
  // node map
  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(), 0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_1Line_X1_1Group_InputCoordinates_test )
{
  // ---------- Set up a 1-element line grid ----------
  typedef typename XField2D_1Line_X1_1Group::VectorX VectorX;

  VectorX X1 = { -0.3,  1.2 };
  VectorX X2 = {  3.4, -2.2 };

  XField2D_1Line_X1_1Group xfld( X1, X2 );

  BOOST_CHECK_EQUAL( xfld.nDOF(), 2 );

  // ---------- Check node values ----------
  // 1st node
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], X1[0] );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], X1[1] );
  // 2nd node
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], X2[0] );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], X2[1] );

  // ---------- Check line field variable ----------
  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField2D_1Line_X1_1Group::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  // ---------- Check interior-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // ---------- Check boundary-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField2D_1Line_X1_1Group::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField2D_1Line_X1_1Group::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  // boundary trace 1
  // node map
  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(),-1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(), 0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  // boundary trace 2
  // node map
  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(), 0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
