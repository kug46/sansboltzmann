// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UNIT_UNITGRIDS_XFIELD2D_1LINE_X1_1GROUP_H_
#define UNIT_UNITGRIDS_XFIELD2D_1LINE_X1_1GROUP_H_

#include "Field/XFieldLine.h"

namespace SANS
{
/*
 A unit grid that consists of one line within a single group

       (0)
    0 ----- 1

This is a manifold: 1D line living in physically 2D space
*/

class XField2D_1Line_X1_1Group : public XField<PhysD2,TopoD1>
{
public:
  typedef DLA::VectorS<2,Real> VectorX;

  XField2D_1Line_X1_1Group( VectorX X1 = {0, 0}, VectorX X2 = {1, 0} );
};

}

#endif /* UNIT_UNITGRIDS_XFIELD2D_1LINE_X1_1GROUP_H_ */
