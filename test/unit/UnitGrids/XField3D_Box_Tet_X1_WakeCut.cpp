// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField3D_Box_Tet_X1_WakeCut.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Field/XFieldLine.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// create tetrahedral grid in unit box with ii x jj x kk (hex) elements
/*
// volume elements in 1 group: 6*ii*jj*kk tetrahdedron
// interior-triangle elements in 4 groups: x-const, y-const, z-const, hex-interior
// boundary-triangle elements in 9 groups: x-min, x-max, y-min, y-max, z-min, z-max, wing upper, wing lower, wake

// Node ordering of the original hex
//
//         y
//  2----------3
//  |\     ^   |\
//  | \    |   | \
//  |  \   |   |  \
//  |   6------+---7
//  |   |  +-- |-- | -> x
//  0---+---\--1   |
//   \  |    \  \  |
//    \ |     \  \ |
//     \|      z  \|
//      4----------5

// First split hex into two prism, then divide each prism into 3 tets
//
// Nodes that make up each of the 6 hexahedron:
//
// Left prism
// Tet[0]: {0, 1, 2, 4}
// Tet[1]: {1, 4, 3, 2}
// Tet[2]: {6, 2, 3, 4}
//
// Right prism
// Tet[3]: {1, 4, 5, 3}
// Tet[4]: {4, 6, 5, 3}
// Tet[5]: {7, 6, 3, 5}
//
// The specific ordering for each tetrahedron is chosen to give positive volumes,
// but without any other consideration
*/
XField3D_Box_Tet_X1_WakeCut::XField3D_Box_Tet_X1_WakeCut( const int nChord, const int nSpan, const Real span, const int nWake, const bool adjoint )
{
  SANS_ASSERT( nChord >= 3 && nSpan >= 2 && nWake >= 2 );

  int comm_rank = comm_->rank();

  // Setup the number of elements in each directoin
  int ii = nWake + nChord + nWake;
  int jj = nWake + nSpan + nWake;
  int kk = 2*nWake;

  int nBoxNode = (ii + 1)*(jj + 1)*(kk + 1);
  int nWingNode = (nChord-1)*(nSpan-1); // Duplicate on wing (interior only)
  int nWakeNode = (nWake+1)*(nSpan-1); //Duplicate on TE + wake + Outflow

  int nnode = nBoxNode + nWingNode + nWakeNode;
  nElem_ = 6*ii*jj*kk;

  // create the grid-coordinate DOF arrays
  resizeDOF( nnode );

  Real ffdist = span;


  std::vector<Real> rSpan(nSpan-1);
  for ( int i = 1; i < nSpan; i++ )
    //rSpan[i-1] = span*i/Real(nSpan);
    rSpan[i-1] = span*(1-0.5*(1+cos(PI*i/Real(nSpan))));

  std::vector<Real> rChord(nChord-1);
  for ( int i = 1; i < nChord; i++ )
    //rChord[i-1] = i/Real(nChord);
    rChord[i-1] = 1-0.5*(1+cos(PI*i/Real(nChord)));


  double xle = 0;
  double xte = 1;

  // stagnation line: quadratic increments w/ dx1 and dx2 matching airfoil
  double x1 = rChord[rChord.size() - 1];
  double x2 = rChord[rChord.size() - 2];

  double dx1 = xte - x1;
  double dx2 = x1 - x2;

  double rffd = (xte + ffdist) - xte/2.;
  const int iiwake = nWake+1;

  double a = (2*((rffd + (xle+xte)/2) - xte) + iiwake*((iiwake - 3)*dx1 - (iiwake - 1)*dx2)) / ((double) (2*iiwake*(2 - iiwake*(3 - iiwake))));
  double b = 0.5*(dx2 - dx1) - 3*a;
  double c = 0.5*(3*dx1 - dx2) + 2*a;

  int iim = iiwake-1;
  std::vector<Real> rwk(iiwake);
  for (int i = 0; i < iiwake; i++)
    rwk[i] = i*(c + i*(b + i*a))/(iim*(c + iim*(b + iim*a)));

  std::vector<Real> x(ii + 1), y( jj + 1 ), z( kk + 1 );

  // Setup the x-coordinates (stremwise)
  for (std::size_t i = 0; i < rwk.size(); i++ )
    x[i] = -ffdist*rwk[rwk.size()-1-i];

  for (std::size_t i = 0; i < rChord.size(); i++ )
    x[rwk.size()+i] = rChord[i];

  for (std::size_t i = 0; i < rwk.size(); i++ )
    x[rwk.size()+rChord.size()+i] = xte + ffdist*rwk[i];


  // Setup the y-coordinates (spanwise)
  for (std::size_t i = 0; i < rwk.size(); i++ )
    y[i] = -span/2 - ffdist*rwk[rwk.size()-1-i];

  for (std::size_t i = 0; i < rSpan.size(); i++ )
    y[rwk.size()+i] = -span/2 + rSpan[i];

  for (std::size_t i = 0; i < rwk.size(); i++ )
    y[rwk.size()+rSpan.size()+i] = span/2 + ffdist*rwk[i];


  // Setup the y-coordinates (normal)
  for (std::size_t i = 0; i < rwk.size(); i++ )
    z[i] = -ffdist*rwk[rwk.size()-1-i];

  for (std::size_t i = 0; i < rwk.size(); i++ )
    z[rwk.size()-1+i] = ffdist*rwk[i];



  const int joffset = (ii+1);
  const int koffset = (ii+1)*(jj+1);

  for (int k = 0; k < kk+1; k++)
    for (int j = 0; j < jj+1; j++)
      for (int i = 0; i < ii+1; i++)
        DOF(k*koffset + j*joffset + i) = { x[i], y[j], z[k] };


  // Duplicate nodes for the wing (only on the interior)
  {
    int k = nWake;
    for (int j = 0; j < nSpan-1; j++)
      for (int i = 0; i < (nChord-1); i++)
      {
        double pokex = 0;
        double pokey = 0;
#if 0
        if ( i == nChord-2)
        {
          pokex = 1./(nChord-1)*0.1;
          pokex *= (j % 2 == 0 ? 1 : -1);
          pokey = span/(nSpan-1)*0.55;
          DOF(k*koffset + (nWake+1+j)*joffset + nWake+1+i) = { x[nWake+1+i]-pokex, y[nWake+1+j]-pokey, z[k] };
        }
#endif
        DOF(nBoxNode + j*(nChord-1) + i) = { x[nWake+1+i]+pokex, y[nWake+1+j]+pokey, z[k] };
      }
  }

  // Duplicate nodes for the wake including the wing TE and outflow, but not spanwise edges
  {
    int k = nWake;
    dupPointOffset_ = nBoxNode + nWingNode;
    invPointMap_.resize( nWakeNode );

    KuttaPoints_.resize(nSpan-1);

    for (int j = 0; j < nSpan-1; j++)
      for (int i = 0; i < nWake+1; i++)
      {
        int iW = nWake+nChord+i;
        int jW = nWake+1+j;
        DOF(dupPointOffset_ + j*(nWake+1) + i) = { x[iW], y[jW], z[k] };

        int origPoint = k*koffset + jW*joffset + iW;

        invPointMap_[j*(nWake+1) + i] = origPoint;

        if (i == 0)
          KuttaPoints_[j] = dupPointOffset_ + j*(nWake+1) + i;
      }
  }


  // Index table for each tet in the hex
  const int hextets[6][4] = { {0, 1, 2, 4},
                              {1, 4, 3, 2},
                              {6, 2, 3, 4},

                              {1, 4, 5, 3},
                              {4, 6, 5, 3},
                              {7, 6, 3, 5} };

  // create the element groups
  int cnt = 1;          // interior: hex-interior
  if (ii > 1) cnt++;    // interior: x-const
  if (jj > 1) cnt++;    // interior: y-const
  if (kk > 1) cnt++;    // interior: z-const
  resizeInteriorTraceGroups(cnt);
  cnt = 0;

  resizeBoundaryTraceGroups(9);     // x-min, x-max, y-min, y-max, z-min, z-max, WingLower, WingUpper, Wake
  resizeCellGroups(1);
  resizeBoundaryFrameGroups(2);    // Trefftz, Kutta

  // grid area field variable

  FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1, nElem_ );

  // volume element grid-coordinate DOF associativity (cell-to-node connectivity)

  std::vector<int> hexnodes;
  int elem = 0;
  int tetnodes[4];
  for (int k = 0; k < kk; k++)
  {
    for (int j = 0; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        const int n0 = k*koffset + j*joffset + i;

        //All the nodes that make up an individual hex
        if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake && i < ii-nWake) )
        {
          // Nodes on the lower surface of the wing
          const int iW = i-nWake-1;
          const int jW = j-nWake-1;
          const int nWL = nBoxNode + jW*(nChord-1) + iW;
          int nW0 = nWL + 0;
          int nW1 = nWL + 1;
          int nW2 = nWL + (nChord-1) + 0;
          int nW3 = nWL + (nChord-1) + 1;

          if ( i == nWake      )
          {
            nW0 = n0 + koffset + 0;
            nW2 = n0 + koffset + joffset + 0;
          }
          if ( i == ii-nWake-1 ) //TE points
          {
            nW1 = dupPointOffset_ + jW*(nWake+1);
            nW3 = dupPointOffset_ + (jW+1)*(nWake+1);
          }

          if ( j == nWake      )
          {
            nW0 = n0 + koffset + 0;
            nW1 = n0 + koffset + 1;
          }
          if ( j == jj-nWake-1 )
          {
            nW2 = n0 + koffset + joffset + 0;
            nW3 = n0 + koffset + joffset + 1;
          }


          hexnodes = { n0 + 0,
                       n0 + 1,
                       n0 + joffset + 0,
                       n0 + joffset + 1,

                       nW0, nW1, nW2, nW3 };
        }
        else if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= ii-nWake) )
        {
          // Node on the wake
          const int iW = i-nWake-nChord;
          const int jW = j-nWake-1;
          const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
          int nW0 = nWL + 0;
          int nW1 = nWL + 1;
          int nW2 = nWL + (nWake+1) + 0;
          int nW3 = nWL + (nWake+1) + 1;

          if ( j == nWake      )
          {
            nW0 = n0 + koffset + 0;
            nW1 = n0 + koffset + 1;
          }
          if ( j == jj-nWake-1 )
          {
            nW2 = n0 + koffset + joffset + 0;
            nW3 = n0 + koffset + joffset + 1;
          }

          hexnodes = { n0 + 0,
                       n0 + 1,
                       n0 + joffset + 0,
                       n0 + joffset + 1,

                       nW0, nW1, nW2, nW3 };
        }
        else
        {
          hexnodes = { n0 + 0,
                       n0 + 1,
                       n0 + joffset + 0,
                       n0 + joffset + 1,

                       n0 + koffset + 0,
                       n0 + koffset + 1,
                       n0 + koffset + joffset + 0,
                       n0 + koffset + joffset + 1 };
        }


        //Loop over all tets that make up a hex
        for (int tet = 0; tet < 6; tet++)
        {
          //Get the nodes from the hex for each tet
          for (int n = 0; n < 4; n++)
            tetnodes[n] = hexnodes[hextets[tet][n]];

          fldAssocCell.setAssociativity( elem ).setRank( comm_rank );
          fldAssocCell.setAssociativity( elem ).setNodeGlobalMapping( tetnodes, 4 );
          elem++;
        }
      }
    }
  }

  // interior-edge field variable
  std::vector<int> hexnodesL;
  std::vector<int> hexnodesR;

  int hextetL, hextetR;
  int faceL, faceR;
  int elemL, elemR;
  int faceNodes[3];
  int tetNodesR[4];
  CanonicalTraceToCell canonicalL, canonicalR;

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  if ( ii > 1 )
  {
    //Local scope releases memory from fldAssocXface when it's no longer needed
    // x-constant faces
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocXface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, (ii-1)*jj*kk*2 );

    fldAssocXface.setGroupLeft( 0 );
    fldAssocXface.setGroupRight( 0 );

    int faceI = 0;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii-1; i++)
        {
          const int n0L = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake && i < ii-nWake) )
          {
            // Nodes on the lower surface of the wing
            const int iW = i-nWake-1;
            const int jW = j-nWake-1;
            const int nWL = nBoxNode + jW*(nChord-1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nChord-1) + 0;
            int nW3 = nWL + (nChord-1) + 1;

            if ( i == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW2 = n0L + koffset + joffset + 0;
            }
            if ( i == ii-nWake-1 ) //TE points
            {
              nW1 = dupPointOffset_ + jW*(nWake+1);
              nW3 = dupPointOffset_ + (jW+1)*(nWake+1);
            }

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 0;
              nW3 = n0L + koffset + joffset + 1;
            }


            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 0,
                          n0L + joffset + 1,

                          nW0, nW1, nW2, nW3 };
          }
          else if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= ii-nWake) )
          {
            // Node on the wake
            const int iW = i-nWake-nChord;
            const int jW = j-nWake-1;
            const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nWake+1) + 0;
            int nW3 = nWL + (nWake+1) + 1;

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 0;
              nW3 = n0L + koffset + joffset + 1;
            }

            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 0,
                          n0L + joffset + 1,

                          nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 0,
                          n0L + joffset + 1,

                          n0L + koffset + 0,
                          n0L + koffset + 1,
                          n0L + koffset + joffset + 0,
                          n0L + koffset + joffset + 1 };
          }



          const int n0R = k*koffset + j*joffset + (i+1);

          //All the nodes that make up the right hex
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && ((i+1) >= nWake && (i+1) < ii-nWake) )
          {
            // Nodes on the lower surface of the wing
            const int iW = (i+1)-nWake-1;
            const int jW = j-nWake-1;
            const int nWR = nBoxNode + jW*(nChord-1) + iW;
            int nW0 = nWR + 0;
            int nW1 = nWR + 1;
            int nW2 = nWR + (nChord-1) + 0;
            int nW3 = nWR + (nChord-1) + 1;

            if ( i+1 == nWake      )
            {
              nW0 = n0R + koffset + 0;
              nW2 = n0R + koffset + joffset + 0;
            }
            if ( i+1 == ii-nWake-1 ) //TE points
            {
              nW1 = dupPointOffset_ + jW*(nWake+1);
              nW3 = dupPointOffset_ + (jW+1)*(nWake+1);
            }

            if ( j == nWake      )
            {
              nW0 = n0R + koffset + 0;
              nW1 = n0R + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0R + koffset + joffset + 0;
              nW3 = n0R + koffset + joffset + 1;
            }

            hexnodesR = { n0R + 0,
                          n0R + 1,
                          n0R + joffset + 0,
                          n0R + joffset + 1,

                          nW0, nW1, nW2, nW3 };
          }
          else if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && ( (i+1) >= ii-nWake) )
          {
            // Node on the wake
            const int iW = (i+1)-nWake-nChord;
            const int jW = j-nWake-1;
            const int nWR = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWR + 0;
            int nW1 = nWR + 1;
            int nW2 = nWR + (nWake+1) + 0;
            int nW3 = nWR + (nWake+1) + 1;

            if ( j == nWake      )
            {
              nW0 = n0R + koffset + 0;
              nW1 = n0R + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0R + koffset + joffset + 0;
              nW3 = n0R + koffset + joffset + 1;
            }

            hexnodesR = { n0R + 0,
                          n0R + 1,
                          n0R + joffset + 0,
                          n0R + joffset + 1,

                          nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodesR = { n0R + 0,
                          n0R + 1,
                          n0R + joffset + 0,
                          n0R + joffset + 1,

                          n0R + koffset + 0,
                          n0R + koffset + 1,
                          n0R + koffset + joffset + 0,
                          n0R + koffset + joffset + 1 };
          }


          hextetL = 3; faceL = 1; //Hex nodes {1, X, 5, 3}
          hextetR = 0; faceR = 1; //Hex nodes {0, X, 2, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesL[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodesR[ hextets[hextetR][n] ];

          elemL = (k*jj*ii + j*ii + (i+0))*6 + hextetL;
          elemR = (k*jj*ii + j*ii + (i+1))*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocXface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocXface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocXface.setElementLeft( elemL, faceI );
          fldAssocXface.setElementRight( elemR, faceI );
          fldAssocXface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocXface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;


          hextetL = 5; faceL = 1; //Hex nodes {7, X, 3, 5}
          hextetR = 2; faceR = 2; //Hex nodes {6, 2, X, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesL[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodesR[ hextets[hextetR][n] ];

          elemL = (k*jj*ii + j*ii + (i+0))*6 + hextetL;
          elemR = (k*jj*ii + j*ii + (i+1))*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocXface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocXface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocXface.setElementLeft( elemL, faceI );
          fldAssocXface.setElementRight( elemR, faceI );
          fldAssocXface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocXface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;
        }
      }
    }

    //Create the interior face group
    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Triangle>( fldAssocXface );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;
  }

  if ( jj > 1 )
  {
    //Local scope releases memory from fldAssocYface when it's no longer needed
    // y-constant faces
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocYface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, ii*(jj-1)*kk*2 );

    fldAssocYface.setGroupLeft( 0 );
    fldAssocYface.setGroupRight( 0 );

    int faceI = 0;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj-1; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0L = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake && i < ii-nWake) )
          {
            // Nodes on the lower surface of the wing
            const int iW = i-nWake-1;
            const int jW = j-nWake-1;
            const int nWL = nBoxNode + jW*(nChord-1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nChord-1) + 0;
            int nW3 = nWL + (nChord-1) + 1;

            if ( i == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW2 = n0L + koffset + joffset + 0;
            }
            if ( i == ii-nWake-1 ) //TE points
            {
              nW1 = dupPointOffset_ + jW*(nWake+1);
              nW3 = dupPointOffset_ + (jW+1)*(nWake+1);
            }

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 0;
              nW3 = n0L + koffset + joffset + 1;
            }


            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 0,
                          n0L + joffset + 1,

                          nW0, nW1, nW2, nW3 };
          }
          else if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= ii-nWake) )
          {
            // Node on the wake
            const int iW = i-nWake-nChord;
            const int jW = j-nWake-1;
            const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nWake+1) + 0;
            int nW3 = nWL + (nWake+1) + 1;

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 0;
              nW3 = n0L + koffset + joffset + 1;
            }

            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 0,
                          n0L + joffset + 1,

                          nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 0,
                          n0L + joffset + 1,

                          n0L + koffset + 0,
                          n0L + koffset + 1,
                          n0L + koffset + joffset + 0,
                          n0L + koffset + joffset + 1 };
          }



          const int n0R = k*koffset + (j+1)*joffset + i;

          //All the nodes that make up the right hex
          if ( (k == nWake-1) && ((j+1) >= nWake && (j+1) < jj-nWake) && (i >= nWake && i < ii-nWake) )
          {
            // Nodes on the lower surface of the wing
            const int iW = i-nWake-1;
            const int jW = (j+1)-nWake-1;
            const int nWR = nBoxNode + jW*(nChord-1) + iW;
            int nW0 = nWR + 0;
            int nW1 = nWR + 1;
            int nW2 = nWR + (nChord-1) + 0;
            int nW3 = nWR + (nChord-1) + 1;

            if ( i == nWake      )
            {
              nW0 = n0R + koffset + 0;
              nW2 = n0R + koffset + joffset + 0;
            }
            if ( i == ii-nWake-1 ) //TE points
            {
              nW1 = dupPointOffset_ + jW*(nWake+1);
              nW3 = dupPointOffset_ + (jW+1)*(nWake+1);
            }

            if ( (j+1) == nWake      )
            {
              nW0 = n0R + koffset + 0;
              nW1 = n0R + koffset + 1;
            }
            if ( (j+1) == jj-nWake-1 )
            {
              nW2 = n0R + koffset + joffset + 0;
              nW3 = n0R + koffset + joffset + 1;
            }

            hexnodesR = { n0R + 0,
                          n0R + 1,
                          n0R + joffset + 0,
                          n0R + joffset + 1,

                          nW0, nW1, nW2, nW3 };
          }
          else if ( (k == nWake-1) && ((j+1) >= nWake && (j+1) < jj-nWake) && ( i >= ii-nWake) )
          {
            // Node on the wake
            const int iW = i-nWake-nChord;
            const int jW = (j+1)-nWake-1;
            const int nWR = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWR + 0;
            int nW1 = nWR + 1;
            int nW2 = nWR + (nWake+1) + 0;
            int nW3 = nWR + (nWake+1) + 1;

            if ( (j+1) == nWake      )
            {
              nW0 = n0R + koffset + 0;
              nW1 = n0R + koffset + 1;
            }
            if ( (j+1) == jj-nWake-1 )
            {
              nW2 = n0R + koffset + joffset + 0;
              nW3 = n0R + koffset + joffset + 1;
            }

            hexnodesR = { n0R + 0,
                          n0R + 1,
                          n0R + joffset + 0,
                          n0R + joffset + 1,

                          nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodesR = { n0R + 0,
                          n0R + 1,
                          n0R + joffset + 0,
                          n0R + joffset + 1,

                          n0R + koffset + 0,
                          n0R + koffset + 1,
                          n0R + koffset + joffset + 0,
                          n0R + koffset + joffset + 1 };
          }

          hextetL = 2; faceL = 3; //Hex nodes {6, 2, 3, X}
          hextetR = 0; faceR = 2; //Hex nodes {0, 1, X, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesL[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodesR[ hextets[hextetR][n] ];

          elemL = (k*jj*ii + (j+0)*ii + i)*6 + hextetL;
          elemR = (k*jj*ii + (j+1)*ii + i)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocYface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocYface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocYface.setElementLeft( elemL, faceI );
          fldAssocYface.setElementRight( elemR, faceI );
          fldAssocYface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocYface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;


          hextetL = 5; faceL = 3; //Hex nodes {7, 6, 3, X}
          hextetR = 3; faceR = 3; //Hex nodes {1, 4, 5, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesL[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodesR[ hextets[hextetR][n] ];

          elemL = (k*jj*ii + (j+0)*ii + i)*6 + hextetL;
          elemR = (k*jj*ii + (j+1)*ii + i)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocYface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocYface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocYface.setElementLeft( elemL, faceI );
          fldAssocYface.setElementRight( elemR, faceI );
          fldAssocYface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocYface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;
        }
      }
    }

    //Create the interior face group
    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Triangle>( fldAssocYface );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;
  }

  if ( kk > 1 )
  {
    //Local scope releases memory from fldAssocZface when it's no longer needed
    // z-constant faces
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType
        fldAssocZface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, ii*jj*(kk-1)*2 - (nChord+nWake)*nSpan*2 );

    fldAssocZface.setGroupLeft( 0 );
    fldAssocZface.setGroupRight( 0 );

    int faceI = 0;
    for (int k = 0; k < kk-1; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          // Skip the faces that represent the wing and wake
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake) )
            continue;

          const int n0L = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodesL[8] = { n0L + 0,
                                     n0L + 1,
                                     n0L + joffset + 0,
                                     n0L + joffset + 1,

                                     n0L + koffset + 0,
                                     n0L + koffset + 1,
                                     n0L + koffset + joffset + 0,
                                     n0L + koffset + joffset + 1 };

          const int n0R = (k+1)*koffset + j*joffset + i;

          //All the nodes that make up the right hex
          const int hexnodesR[8] = { n0R + 0,
                                     n0R + 1,
                                     n0R + joffset + 0,
                                     n0R + joffset + 1,

                                     n0R + koffset + 0,
                                     n0R + koffset + 1,
                                     n0R + koffset + joffset + 0,
                                     n0R + koffset + joffset + 1 };

          hextetL = 4; faceL = 3; //Hex nodes {4, 6, 5, X}
          hextetR = 0; faceR = 3; //Hex nodes {0, 1, 2, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesL[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodesR[ hextets[hextetR][n] ];

          elemL = ((k+0)*jj*ii + j*ii + i)*6 + hextetL;
          elemR = ((k+1)*jj*ii + j*ii + i)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocZface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocZface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocZface.setElementLeft( elemL, faceI );
          fldAssocZface.setElementRight( elemR, faceI );
          fldAssocZface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocZface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;


          hextetL = 5; faceL = 2; //Hex nodes {7, 6, X, 5}
          hextetR = 1; faceR = 1; //Hex nodes {1, X, 3, 2}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesL[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodesR[ hextets[hextetR][n] ];

          elemL = ((k+0)*jj*ii + j*ii + i)*6 + hextetL;
          elemR = ((k+1)*jj*ii + j*ii + i)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocZface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocZface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocZface.setElementLeft( elemL, faceI );
          fldAssocZface.setElementRight( elemR, faceI );
          fldAssocZface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocZface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;
        }
      }
    }

    //Create the interior face group
    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Triangle>( fldAssocZface );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;
  }

  {
    //Local scope releases memory from fldAssocIface when it's no longer needed
    // hex interior faces
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocIface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, ii*jj*kk*6 );

    fldAssocIface.setGroupLeft( 0 );
    fldAssocIface.setGroupRight( 0 );

    int faceI = 0;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up an individual hex
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake && i < ii-nWake) )
          {
            // Nodes on the lower surface of the wing
            const int iW = i-nWake-1;
            const int jW = j-nWake-1;
            const int nWL = nBoxNode + jW*(nChord-1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nChord-1) + 0;
            int nW3 = nWL + (nChord-1) + 1;

            if ( i == nWake      )
            {
              nW0 = n0 + koffset + 0;
              nW2 = n0 + koffset + joffset + 0;
            }
            if ( i == ii-nWake-1 ) //TE points
            {
              nW1 = dupPointOffset_ + jW*(nWake+1);
              nW3 = dupPointOffset_ + (jW+1)*(nWake+1);
            }

            if ( j == nWake      )
            {
              nW0 = n0 + koffset + 0;
              nW1 = n0 + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0 + koffset + joffset + 0;
              nW3 = n0 + koffset + joffset + 1;
            }


            hexnodes = { n0 + 0,
                         n0 + 1,
                         n0 + joffset + 0,
                         n0 + joffset + 1,

                         nW0, nW1, nW2, nW3 };
          }
          else if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= ii-nWake) )
          {
            // Node on the wake
            const int iW = i-nWake-nChord;
            const int jW = j-nWake-1;
            const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nWake+1) + 0;
            int nW3 = nWL + (nWake+1) + 1;

            if ( j == nWake      )
            {
              nW0 = n0 + koffset + 0;
              nW1 = n0 + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0 + koffset + joffset + 0;
              nW3 = n0 + koffset + joffset + 1;
            }

            hexnodes = { n0 + 0,
                         n0 + 1,
                         n0 + joffset + 0,
                         n0 + joffset + 1,

                         nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodes = { n0 + 0,
                         n0 + 1,
                         n0 + joffset + 0,
                         n0 + joffset + 1,

                         n0 + koffset + 0,
                         n0 + koffset + 1,
                         n0 + koffset + joffset + 0,
                         n0 + koffset + joffset + 1 };
          }


          hextetL = 0; faceL = 0; //Hex nodes {X, 1, 2, 4}
          hextetR = 1; faceR = 2; //Hex nodes {1, 4, X, 2}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodes[ hextets[hextetR][n] ];

          elemL = (k*jj*ii + j*ii + i + 0)*6 + hextetL;
          elemR = (k*jj*ii + j*ii + i + 0)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocIface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocIface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocIface.setElementLeft( elemL, faceI );
          fldAssocIface.setElementRight( elemR, faceI );
          fldAssocIface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocIface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;


          hextetL = 1; faceL = 0; //Hex nodes {X, 4, 3, 2}
          hextetR = 2; faceR = 0; //Hex nodes {X, 2, 3, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodes[ hextets[hextetR][n] ];

          elemL = (k*jj*ii + j*ii + i + 0)*6 + hextetL;
          elemR = (k*jj*ii + j*ii + i + 0)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocIface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocIface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocIface.setElementLeft( elemL, faceI );
          fldAssocIface.setElementRight( elemR, faceI );
          fldAssocIface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocIface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;


          hextetL = 1; faceL = 3; //Hex nodes {1, 4, 3, X}
          hextetR = 3; faceR = 2; //Hex nodes {1, 4, X, 3}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodes[ hextets[hextetR][n] ];

          elemL = (k*jj*ii + j*ii + i + 0)*6 + hextetL;
          elemR = (k*jj*ii + j*ii + i + 0)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocIface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocIface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocIface.setElementLeft( elemL, faceI );
          fldAssocIface.setElementRight( elemR, faceI );
          fldAssocIface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocIface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;


          hextetL = 2; faceL = 1; //Hex nodes {6, X, 3, 4}
          hextetR = 4; faceR = 2; //Hex nodes {4, 6, X, 3}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodes[ hextets[hextetR][n] ];

          elemL = (k*jj*ii + j*ii + i + 0)*6 + hextetL;
          elemR = (k*jj*ii + j*ii + i + 0)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocIface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocIface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocIface.setElementLeft( elemL, faceI );
          fldAssocIface.setElementRight( elemR, faceI );
          fldAssocIface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocIface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;


          hextetL = 3; faceL = 0; //Hex nodes {X, 4, 5, 3}
          hextetR = 4; faceR = 1; //Hex nodes {4, X, 5, 3}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodes[ hextets[hextetR][n] ];

          elemL = (k*jj*ii + j*ii + i + 0)*6 + hextetL;
          elemR = (k*jj*ii + j*ii + i + 0)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocIface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocIface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocIface.setElementLeft( elemL, faceI );
          fldAssocIface.setElementRight( elemR, faceI );
          fldAssocIface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocIface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;


          hextetL = 4; faceL = 0; //Hex nodes {X, 6, 5, 3}
          hextetR = 5; faceR = 0; //Hex nodes {X, 6, 3, 5}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodes[ hextets[hextetR][n] ];

          elemL = (k*jj*ii + j*ii + i + 0)*6 + hextetL;
          elemR = (k*jj*ii + j*ii + i + 0)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocIface.setAssociativity( faceI ).setRank( comm_rank );
          fldAssocIface.setAssociativity( faceI ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocIface.setElementLeft( elemL, faceI );
          fldAssocIface.setElementRight( elemR, faceI );
          fldAssocIface.setCanonicalTraceLeft( canonicalL, faceI );
          fldAssocIface.setCanonicalTraceRight( canonicalR, faceI );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          faceI++;
        }
      }
    }

    //Create the interior face group
    interiorTraceGroups_[cnt] = new FieldTraceGroupType<Triangle>( fldAssocIface );
    interiorTraceGroups_[cnt]->setDOF(DOF_, nDOF_);
    cnt++;
  }


  {
    //Local scope releases memory from fldAssocXface when it's no longer needed
    // x-min boundry
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocXface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, jj*kk*2 );

    fldAssocXface.setGroupLeft( 0 );

    int faceB = 0;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        int i = 0;
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          hextetL = 0; faceL = 1; //Hex nodes {0, X, 2, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocXface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocXface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocXface.setElementLeft( elemL, faceB );
          fldAssocXface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;


          hextetL = 2; faceL = 2; //Hex nodes {6, 2, X, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocXface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocXface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocXface.setElementLeft( elemL, faceB );
          fldAssocXface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[0] = new FieldTraceGroupType<Triangle>( fldAssocXface );
    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  }


  {
    //Local scope releases memory from fldAssocXface when it's no longer needed
    // x-max boundry
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocXface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, jj*kk*2 );

    fldAssocXface.setGroupLeft( 0 );

    int faceB = 0;
    for (int k = 0; k < kk; k++)
    {
      for (int j = 0; j < jj; j++)
      {
        int i = ii-1;
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up an individual hex
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) )
          {
            // Node on the wake
            const int iW = i-nWake-nChord;
            const int jW = j-nWake-1;
            const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nWake+1) + 0;
            int nW3 = nWL + (nWake+1) + 1;

            if ( j == nWake      )
            {
              nW0 = n0 + koffset + 0;
              nW1 = n0 + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0 + koffset + joffset + 0;
              nW3 = n0 + koffset + joffset + 1;
            }

            hexnodes = { n0 + 0,
                         n0 + 1,
                         n0 + joffset + 0,
                         n0 + joffset + 1,

                         nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodes = { n0 + 0,
                         n0 + 1,
                         n0 + joffset + 0,
                         n0 + joffset + 1,

                         n0 + koffset + 0,
                         n0 + koffset + 1,
                         n0 + koffset + joffset + 0,
                         n0 + koffset + joffset + 1 };
          }

          hextetL = 3; faceL = 1; //Hex nodes {1, X, 5, 3}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocXface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocXface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocXface.setElementLeft( elemL, faceB );
          fldAssocXface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;


          hextetL = 5; faceL = 1; //Hex nodes {7, X, 3, 5}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocXface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocXface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocXface.setElementLeft( elemL, faceB );
          fldAssocXface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Triangle>( fldAssocXface );
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  }


  {
    //Local scope releases memory from fldAssocYface when it's no longer needed
    // y-min boundry
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocYface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, ii*kk*2 );

    fldAssocYface.setGroupLeft( 0 );

    int faceB = 0;
    for (int k = 0; k < kk; k++)
    {
      int j = 0;
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          hextetL = 0; faceL = 2; //Hex nodes {0, 1, X, 4}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocYface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocYface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocYface.setElementLeft( elemL, faceB );
          fldAssocYface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;


          hextetL = 3; faceL = 3; //Hex nodes {1, 4, 5, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocYface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocYface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocYface.setElementLeft( elemL, faceB );
          fldAssocYface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[2] = new FieldTraceGroupType<Triangle>( fldAssocYface );
    boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);
  }


  {
    //Local scope releases memory from fldAssocYface when it's no longer needed
    // y-max boundry
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocYface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, ii*kk*2 );

    fldAssocYface.setGroupLeft( 0 );

    int faceB = 0;
    for (int k = 0; k < kk; k++)
    {
      int j = jj-1;
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          hextetL = 2; faceL = 3; //Hex nodes {6, 2, 3, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocYface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocYface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocYface.setElementLeft( elemL, faceB );
          fldAssocYface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;


          hextetL = 5; faceL = 3; //Hex nodes {7, 6, 3, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocYface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocYface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocYface.setElementLeft( elemL, faceB );
          fldAssocYface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[3] = new FieldTraceGroupType<Triangle>( fldAssocYface );
    boundaryTraceGroups_[3]->setDOF(DOF_, nDOF_);
  }


  {
    //Local scope releases memory from fldAssocZface when it's no longer needed
    // z-min boundry
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocZface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, ii*jj*2 );

    fldAssocZface.setGroupLeft( 0 );

    int faceB = 0;
    int k = 0;
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          hextetL = 0; faceL = 3; //Hex nodes {0, 1, 2, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocZface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocZface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocZface.setElementLeft( elemL, faceB );
          fldAssocZface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;


          hextetL = 1; faceL = 1; //Hex nodes {1, X, 3, 2}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocZface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocZface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocZface.setElementLeft( elemL, faceB );
          fldAssocZface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[4] = new FieldTraceGroupType<Triangle>( fldAssocZface );
    boundaryTraceGroups_[4]->setDOF(DOF_, nDOF_);
  }


  {
    //Local scope releases memory from fldAssocZface when it's no longer needed
    // z-max boundry
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocZface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, ii*jj*2 );

    fldAssocZface.setGroupLeft( 0 );

    int faceB = 0;
    int k = kk-1;
    {
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          const int n0 = k*koffset + j*joffset + i;

          //All the nodes that make up the left hex
          const int hexnodes[8] = { n0 + 0,
                                    n0 + 1,
                                    n0 + joffset + 0,
                                    n0 + joffset + 1,

                                    n0 + koffset + 0,
                                    n0 + koffset + 1,
                                    n0 + koffset + joffset + 0,
                                    n0 + koffset + joffset + 1 };

          hextetL = 4; faceL = 3; //Hex nodes {4, 6, 5, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocZface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocZface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocZface.setElementLeft( elemL, faceB );
          fldAssocZface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;


          hextetL = 5; faceL = 2; //Hex nodes {7, 6, X, 5}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocZface.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocZface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocZface.setElementLeft( elemL, faceB );
          fldAssocZface.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[5] = new FieldTraceGroupType<Triangle>( fldAssocZface );
    boundaryTraceGroups_[5]->setDOF(DOF_, nDOF_);
  }



  {
    //Local scope releases memory from fldAssocLower and fldAssocUpper when they are no longer needed
    // Lower and upper Wing boundary
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocLower( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 2*nChord*nSpan );
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocUpper( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 2*nChord*nSpan );

    fldAssocLower.setGroupLeft( 0 );
    fldAssocUpper.setGroupLeft( 0 );


    FieldFrameGroupType::FieldAssociativityConstructorType fldAssocKutta( BasisFunctionLineBase::HierarchicalP1, nSpan );

    fldAssocKutta.setGroupLeft( 7 );
    fldAssocKutta.setGroupRight( 6 );

    int faceB = 0;
    int k = nWake-1;
    {
      for (int j = nWake; j < jj-nWake; j++)
      {
        for (int i = nWake; i < ii-nWake; i++)
        {
          const int n0L = k*koffset + j*joffset + i;

          const int iW = i-nWake-1;
          const int jW = j-nWake-1;
          const int nWL = nBoxNode + jW*(nChord-1) + iW;
          int nW0 = nWL + 0;
          int nW1 = nWL + 1;
          int nW2 = nWL + (nChord-1) + 0;
          int nW3 = nWL + (nChord-1) + 1;

          if ( i == nWake      )
          {
            nW0 = n0L + koffset + 0;
            nW2 = n0L + koffset + joffset + 0;
          }
          if ( i == ii-nWake-1 ) //TE points
          {
            nW1 = dupPointOffset_ + jW*(nWake+1);
            nW3 = dupPointOffset_ + (jW+1)*(nWake+1);
          }

          if ( j == nWake      )
          {
            nW0 = n0L + koffset + 0;
            nW1 = n0L + koffset + 1;
          }
          if ( j == jj-nWake-1 )
          {
            nW2 = n0L + koffset + joffset + 0;
            nW3 = n0L + koffset + joffset + 1;
          }

          //All the nodes that make up the Lower hex
          hexnodesL = { n0L + 0,
                        n0L + 1,
                        n0L + joffset + 0,
                        n0L + joffset + 1,

                        nW0, nW1, nW2, nW3 };

          hextetL = 4; faceL = 3; //Hex nodes {4, 6, 5, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesL[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocLower.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocLower.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocLower.setElementLeft( elemL, faceB );
          fldAssocLower.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          // Drela-Kutta condition associativity
          if ( i == nWake+nChord-1 && j > nWake )
          {
            const int TEedge[2] = {nW1, nW3};
            KuttaBCTraceElemRight_[6][TEedge[0]].KuttaPoint = nW1;
            KuttaBCTraceElemRight_[6][TEedge[0]].elems.push_back(faceB);
          }
          //if ( i == nWake+nChord-1 && j < jj-nWake-1)
          //{
          //  const int TEedge[2] = {nW1, nW2};
          //  KuttaBCTraceElemRight_[6][TEedge[1]].KuttaPoint = nW2;
          //  KuttaBCTraceElemRight_[6][TEedge[1]].elems.push_back(faceB);
          //}


          hextetL = 5; faceL = 2; //Hex nodes {7, 6, X, 5}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesL[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = (k*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);
          const int TEedgeL[2] = {hexnodesL[5], hexnodesL[7]};
          CanonicalTraceToCell canonicalFrameL = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(TEedgeL, 2, faceNodes, 3);

          fldAssocLower.setAssociativity( faceB+1 ).setRank( comm_rank );
          fldAssocLower.setAssociativity( faceB+1 ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocLower.setElementLeft( elemL, faceB+1 );
          fldAssocLower.setCanonicalTraceLeft( canonicalL, faceB+1 );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );


          // Drela-Kutta condition associativity
          if ( i == nWake+nChord-1 && j > nWake )
          {
            const int TEedge[2] = {nW1, nW3};
            KuttaBCTraceElemRight_[6][TEedge[0]].KuttaPoint = nW1;
            KuttaBCTraceElemRight_[6][TEedge[0]].elems.push_back(faceB+1);
          }
          if ( i == nWake+nChord-1 && j < jj-nWake-1)
          {
            const int TEedge[2] = {nW1, nW3};
            KuttaBCTraceElemRight_[6][TEedge[1]].KuttaPoint = nW3;
            KuttaBCTraceElemRight_[6][TEedge[1]].elems.push_back(faceB+1);
          }


          const int n0U = (k+1)*koffset + j*joffset + i;

          //All the nodes that make up the upper hex
          const int hexnodesU[8] = { n0U + 0,
                                     n0U + 1,
                                     n0U + joffset + 0,
                                     n0U + joffset + 1,

                                     n0U + koffset + 0,
                                     n0U + koffset + 1,
                                     n0U + koffset + joffset + 0,
                                     n0U + koffset + joffset + 1 };

          hextetL = 0; faceL = 3; //Hex nodes {0, 1, 2, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesU[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = ((k+1)*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);

          fldAssocUpper.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocUpper.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocUpper.setElementLeft( elemL, faceB );
          fldAssocUpper.setCanonicalTraceLeft( canonicalL, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );

          // Drela-Kutta condition associativity
          if ( i == nWake+nChord-1 && j > nWake )
          {
            const int TEedge[2] = {n0U + 1, n0U + joffset + 1};
            KuttaBCTraceElemLeft_[7][TEedge[0]].KuttaPoint = nW1;
            KuttaBCTraceElemLeft_[7][TEedge[0]].elems.push_back(faceB);
          }
          //if ( i == nWake+nChord-1 && j < jj-nWake-1)
          //{
          //  const int TEedge[2] = {n0U + 1, n0U + joffset + 1};
          //  KuttaBCTraceElemLeft_[7][TEedge[1]].KuttaPoint = nW2;
          //  KuttaBCTraceElemLeft_[7][TEedge[1]].elems.push_back(faceB);
          //}


          hextetL = 1; faceL = 1; //Hex nodes {1, X, 3, 2}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesU[ hextets[hextetL][TraceNodes[faceL][n]] ];

          elemL = ((k+1)*jj*ii + j*ii + i)*6 + hextetL;

          canonicalL.set(faceL, 1);
          const int TEedgeU[2] = {hexnodesU[1], hexnodesU[3]};
          CanonicalTraceToCell canonicalFrameU = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(TEedgeU, 2, faceNodes, 3);

          fldAssocUpper.setAssociativity( faceB+1 ).setRank( comm_rank );
          fldAssocUpper.setAssociativity( faceB+1 ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocUpper.setElementLeft( elemL, faceB+1 );
          fldAssocUpper.setCanonicalTraceLeft( canonicalL, faceB+1 );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );


          // Drela-Kutta condition associativity
          if ( i == nWake+nChord-1 && j > nWake )
          {
            const int TEedge[2] = {n0U + 1, n0U + joffset + 1};
            KuttaBCTraceElemLeft_[7][TEedge[0]].KuttaPoint = nW1;
            KuttaBCTraceElemLeft_[7][TEedge[0]].elems.push_back(faceB+1);
          }
          if ( i == nWake+nChord-1 && j < jj-nWake-1)
          {
            const int TEedge[2] = {n0U + 1, n0U + joffset + 1};
            KuttaBCTraceElemLeft_[7][TEedge[1]].KuttaPoint = nW3;
            KuttaBCTraceElemLeft_[7][TEedge[1]].elems.push_back(faceB+1);
          }


          // Set the Kutta condition associativity
          if ( i == nWake+nChord-1 )
          {
            int edgeK = j-nWake;

            fldAssocKutta.setAssociativity( edgeK ).setRank( comm_rank );
            fldAssocKutta.setAssociativity( edgeK ).setNodeGlobalMapping( TEedgeL, 2 );
            fldAssocKutta.setElementLeft( faceB+1, edgeK );
            fldAssocKutta.setElementRight( faceB+1, edgeK );
            fldAssocKutta.setCanonicalTraceLeft( canonicalFrameU, edgeK );
            fldAssocKutta.setCanonicalTraceRight( canonicalFrameL, edgeK );
          }

          faceB += 2;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[6] = new FieldTraceGroupType<Triangle>( fldAssocLower );
    boundaryTraceGroups_[6]->setDOF(DOF_, nDOF_);

    boundaryTraceGroups_[7] = new FieldTraceGroupType<Triangle>( fldAssocUpper );
    boundaryTraceGroups_[7]->setDOF(DOF_, nDOF_);

    if (!adjoint)
    {
      boundaryFrameGroups_[0] = new FieldFrameGroupType( fldAssocKutta );
      boundaryFrameGroups_[0]->setDOF(DOF_, nDOF_);
    }
  }



  {
    //Local scope releases memory from fldAssocWake when it's no longer needed
    // Lower and upper Wing boundary
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocWake( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 2*nWake*nSpan );

    fldAssocWake.setGroupLeft( 0 );
    fldAssocWake.setGroupRight( 0 );

    FieldFrameGroupType::FieldAssociativityConstructorType fldAssocKutta( BasisFunctionLineBase::HierarchicalP1, nSpan );

    fldAssocKutta.setGroupLeft( 8 );

    FieldFrameGroupType::FieldAssociativityConstructorType fldAssocTrefftz( BasisFunctionLineBase::HierarchicalP1, nSpan );

    fldAssocTrefftz.setGroupLeft( 8 );

    int faceB = 0;
    int k = nWake-1;
    {
      for (int j = nWake; j < jj-nWake; j++)
      {
        for (int i = nWake+nChord; i < ii; i++)
        {
          const int n0L = (k+1)*koffset + j*joffset + i;

          const int hexnodesL[8] = { n0L + 0,
                                     n0L + 1,
                                     n0L + joffset + 0,
                                     n0L + joffset + 1,

                                     n0L + koffset + 0,
                                     n0L + koffset + 1,
                                     n0L + koffset + joffset + 0,
                                     n0L + koffset + joffset + 1 };

          const int n0R = k*koffset + j*joffset + i;

          //All the nodes that make up the right hex
          const int hexnodesR[8] = { n0R + 0,
                                     n0R + 1,
                                     n0R + joffset + 0,
                                     n0R + joffset + 1,

                                     n0R + koffset + 0,
                                     n0R + koffset + 1,
                                     n0R + koffset + joffset + 0,
                                     n0R + koffset + joffset + 1 };


          hextetL = 0; faceL = 3; //Hex nodes {0, 1, 2, X}
          hextetR = 4; faceR = 3; //Hex nodes {4, 6, 5, X}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesL[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodesR[ hextets[hextetR][n] ];

          elemL = ((k+1)*jj*ii + j*ii + i)*6 + hextetL;
          elemR = ((k+0)*jj*ii + j*ii + i)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocWake.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocWake.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocWake.setElementLeft( elemL, faceB );
          fldAssocWake.setElementRight( elemR, faceB );
          fldAssocWake.setCanonicalTraceLeft( canonicalL, faceB );
          fldAssocWake.setCanonicalTraceRight( canonicalR, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          // Set the Kutta condition associativity
          if ( i == nWake+nChord+1 )
          {
            const int edge[2] = {n0L + 0, n0L + joffset + 0};
            int edgeK = j-nWake;
            CanonicalTraceToCell canonicalFrameK = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(edge, 2, faceNodes, 3);

            fldAssocKutta.setAssociativity( edgeK ).setRank( comm_rank );
            fldAssocKutta.setAssociativity( edgeK ).setNodeGlobalMapping( edge, 2 );
            fldAssocKutta.setElementLeft( faceB, edgeK );
            fldAssocKutta.setCanonicalTraceLeft( canonicalFrameK, edgeK );
          }

          faceB++;


          hextetL = 1; faceL = 1; //Hex nodes {1, X, 3, 2}
          hextetR = 5; faceR = 2; //Hex nodes {7, 6, X, 5}
          for (int n = 0; n < 3; n++)
            faceNodes[n] = hexnodesL[ hextets[hextetL][TraceNodes[faceL][n]] ];
          for (int n = 0; n < 4; n++)
            tetNodesR[n] = hexnodesR[ hextets[hextetR][n] ];

          elemL = ((k+1)*jj*ii + j*ii + i)*6 + hextetL;
          elemR = ((k+0)*jj*ii + j*ii + i)*6 + hextetR;

          canonicalL.set(faceL, 1);
          canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tetNodesR, 4);

          fldAssocWake.setAssociativity( faceB ).setRank( comm_rank );
          fldAssocWake.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
          fldAssocWake.setElementLeft( elemL, faceB );
          fldAssocWake.setElementRight( elemR, faceB );
          fldAssocWake.setCanonicalTraceLeft( canonicalL, faceB );
          fldAssocWake.setCanonicalTraceRight( canonicalR, faceB );

          // face signs for elements (L is +, R is -)
          fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
          fldAssocCell.setAssociativity( elemR ).setFaceSign( canonicalR.orientation, faceR );

          // Set the Trefftz plane associativity
          if ( i == ii-1 )
          {
            int edgeT = j-nWake;

            const int edgeL[2] = {hexnodesL[1], hexnodesL[3]};
            CanonicalTraceToCell canonicalFrameT = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(edgeL, 2, faceNodes, 3);

            fldAssocTrefftz.setAssociativity( edgeT ).setRank( comm_rank );
            fldAssocTrefftz.setAssociativity( edgeT ).setNodeGlobalMapping( edgeL, 2 );
            fldAssocTrefftz.setElementLeft( faceB, edgeT );
            fldAssocTrefftz.setCanonicalTraceLeft( canonicalFrameT, edgeT );
          }


          faceB++;
        }
      }
    }

    //Create the boundary face group
    boundaryTraceGroups_[8] = new FieldTraceGroupType<Triangle>( fldAssocWake );
    boundaryTraceGroups_[8]->setDOF(DOF_, nDOF_);

    if (adjoint)
    {
      boundaryFrameGroups_[0] = new FieldFrameGroupType( fldAssocKutta );
      boundaryFrameGroups_[0]->setDOF(DOF_, nDOF_);
    }

    boundaryFrameGroups_[1] = new FieldFrameGroupType( fldAssocTrefftz );
    boundaryFrameGroups_[1]->setDOF(DOF_, nDOF_);
  }


  //Finally create the cell groups now that all Face signs have been set
  cellGroups_[0] = new FieldCellGroupType<Tet>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);


#if 0
  {
    //Local scope releases memory from fldAssocCellLeft and fldAssocCellRight when they are no longer needed
    // Lower (right) and upper (left) cell's along the trailing edge
    FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCellLeft( BasisFunctionVolumeBase<Tet>::HierarchicalP1, 9*(nSpan+0) );
    FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCellRight( BasisFunctionVolumeBase<Tet>::HierarchicalP1, 9*(nSpan+0) );


    int cellL = 0;
    int cellR = 0;
    int ntet;
    int tets[6];
    int k = nWake-1;
    {
      for (int j = nWake; j < jj-nWake; j++)
      {
        for (int i = nWake+nChord-1; i < ii-nWake+1; i++)
        {
          const int n0L = k*koffset + j*joffset + i;

          //All the nodes that make up an individual hex
          if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= nWake && i < ii-nWake) )
          {
            // Nodes on the lower surface of the wing
            const int iW = i-nWake-1;
            const int jW = j-nWake-1;
            const int nWL = nBoxNode + jW*(nChord-1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nChord-1) + 0;
            int nW3 = nWL + (nChord-1) + 1;

            if ( i == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW2 = n0L + koffset + joffset + 0;
            }
            if ( i == ii-nWake-1 ) //TE points
            {
              nW1 = dupPointOffset_ + jW*(nWake+1);
              nW3 = dupPointOffset_ + (jW+1)*(nWake+1);
            }

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 0;
              nW3 = n0L + koffset + joffset + 1;
            }


            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 0,
                          n0L + joffset + 1,

                          nW0, nW1, nW2, nW3 };
          }
          else if ( (k == nWake-1) && (j >= nWake && j < jj-nWake) && (i >= ii-nWake) )
          {
            // Node on the wake
            const int iW = i-nWake-nChord;
            const int jW = j-nWake-1;
            const int nWL = dupPointOffset_ + jW*(nWake+1) + iW;
            int nW0 = nWL + 0;
            int nW1 = nWL + 1;
            int nW2 = nWL + (nWake+1) + 0;
            int nW3 = nWL + (nWake+1) + 1;

            if ( j == nWake      )
            {
              nW0 = n0L + koffset + 0;
              nW1 = n0L + koffset + 1;
            }
            if ( j == jj-nWake-1 )
            {
              nW2 = n0L + koffset + joffset + 0;
              nW3 = n0L + koffset + joffset + 1;
            }

            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 0,
                          n0L + joffset + 1,

                          nW0, nW1, nW2, nW3 };
          }
          else
          {
            hexnodesL = { n0L + 0,
                          n0L + 1,
                          n0L + joffset + 0,
                          n0L + joffset + 1,

                          n0L + koffset + 0,
                          n0L + koffset + 1,
                          n0L + koffset + joffset + 0,
                          n0L + koffset + joffset + 1 };
          }

          if (i == ii-nWake-1)
          {
            ntet = 3;
            tets[0] = 3; //Hex nodes {6, 2, 3, 4}
            tets[1] = 4; //Hex nodes {4, 6, 5, 3}
            tets[2] = 5; //Hex nodes {7, 6, 3, 5}
          }
          else
          {
            ntet = 6;
            tets[0] = 0;
            tets[1] = 1;
            tets[2] = 2;
            tets[3] = 3;
            tets[4] = 4;
            tets[5] = 5;
          }

          //Loop over all tets that touch the TE
          for (int m = 0; m < ntet; m++)
          {
            int tet = tets[m];
            //Get the nodes from the hex for each tet
            for (int n = 0; n < 4; n++)
               tetnodes[n] = hexnodesL[hextets[tet][n]];

            fldAssocCellRight.setAssociativity( cellR ).setNodeGlobalMapping( tetnodes, 4 );
            cellR++;
          }

          const int n0U = (k+1)*koffset + j*joffset + i;

          //All the nodes that make up the upper hex
          const int hexnodesU[8] = { n0U + 0,
                                     n0U + 1,
                                     n0U + joffset + 0,
                                     n0U + joffset + 1,

                                     n0U + koffset + 0,
                                     n0U + koffset + 1,
                                     n0U + koffset + joffset + 0,
                                     n0U + koffset + joffset + 1 };

          if (i == nWake+nChord-1)
          {
            ntet = 6;
            tets[0] = 0;
            tets[1] = 1;
            tets[2] = 2;
            tets[3] = 3;
            tets[4] = 4;
            tets[5] = 5;
          }
          else
          {
            ntet = 3;
            tets[0] = 0;
            tets[1] = 1;
            tets[2] = 2;
          }

          //Loop over all tets that touch the TE
          for (int m = 0; m < ntet; m++)
          {
            int tet = tets[m];
            //Get the nodes from the hex for each tet
            for (int n = 0; n < 4; n++)
               tetnodes[n] = hexnodesU[hextets[tet][n]];

            fldAssocCellLeft.setAssociativity( cellL ).setNodeGlobalMapping( tetnodes, 4 );
            cellL++;
          }

        }
      }
    }

    //Create the volume groups
    cellGroups_[1] = new FieldCellGroupType<Tet>( fldAssocCellLeft );
    cellGroups_[1]->setDOF(DOF_, nDOF_);

    cellGroups_[2] = new FieldCellGroupType<Tet>( fldAssocCellRight );
    cellGroups_[2]->setDOF(DOF_, nDOF_);
  }
#endif


  //Check that the grid is correct
  checkGrid();

}

}
