// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UNIT_UNITGRIDS_XFIELD2D_LINE_X1_1GROUP_H_
#define UNIT_UNITGRIDS_XFIELD2D_LINE_X1_1GROUP_H_

#include "Field/XFieldLine.h"

namespace SANS
{
// open-ended line grid in physically 2D space
//
// generate line grid from prescribed nodal coordinates
// line elements in 1 group
// interior traces in 1 group
// boundary traces in 2 group

// line grid with consistent reference coordinate direction
//      (0)     (1)     (2)         element
//   0 ----- 1 ----- 2 ----- 3 ---  node
//      -->     -->     -->         reference coordinate s direction

class XField2D_Line_X1_1Group : public XField<PhysD2,TopoD1>
{
public:
  typedef std::vector<DLA::VectorS<2,Real>> ArrayVector;

  explicit XField2D_Line_X1_1Group(const ArrayVector& coordinates);

  static void readXFoilGrid(const std::string& filename,
                            ArrayVector& coordinates);
};


// line grid with alternating reference coordinate direction
//      (0)     (1)     (2)         element
//   0 ----- 1 ----- 2 ----- 3 ---  node
//      -->     <--     -->         reference coordinate s direction
//
class XField2D_Line_X1_1Group_unstructured : public XField<PhysD2,TopoD1>
{
public:
  typedef std::vector<DLA::VectorS<2,Real>> ArrayVector;

  explicit XField2D_Line_X1_1Group_unstructured(const ArrayVector& coordinates);
};

}



#endif /* UNIT_UNITGRIDS_XFIELD2D_LINE_X1_1GROUP_H_ */
