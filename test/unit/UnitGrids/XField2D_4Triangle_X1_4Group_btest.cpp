// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_4Triangle_X1_4Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_4Triangle_X1_4Group_test )
{
  typedef std::array<int,3> Int3;

  XField2D_4Triangle_X1_4Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 6 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], -1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], -1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 4 );
  for (int i = 0; i < 4; i++)
    BOOST_REQUIRE( xfld.getCellGroupBase(i).topoTypeID() == typeid(Triangle) );

  const XField2D_4Triangle_X1_4Group::FieldCellGroupType<Triangle>& xfldArea0 = xfld.getCellGroup<Triangle>(0);
  const XField2D_4Triangle_X1_4Group::FieldCellGroupType<Triangle>& xfldArea1 = xfld.getCellGroup<Triangle>(1);
  const XField2D_4Triangle_X1_4Group::FieldCellGroupType<Triangle>& xfldArea2 = xfld.getCellGroup<Triangle>(2);
  const XField2D_4Triangle_X1_4Group::FieldCellGroupType<Triangle>& xfldArea3 = xfld.getCellGroup<Triangle>(3);

  int nodeMap[3];

  xfldArea0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldArea1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  xfldArea2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );

  xfldArea3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  Int3 edgeSign;

  edgeSign = xfldArea0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea2.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], -1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea3.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], -1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 3 );
  for ( int i = 0; i < 3; i++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  // Labeled for group they connect to
  const XField2D_4Triangle_X1_4Group::FieldTraceGroupType<Line>& xfldIedge1 = xfld.getInteriorTraceGroup<Line>(0);
  const XField2D_4Triangle_X1_4Group::FieldTraceGroupType<Line>& xfldIedge2 = xfld.getInteriorTraceGroup<Line>(1);
  const XField2D_4Triangle_X1_4Group::FieldTraceGroupType<Line>& xfldIedge3 = xfld.getInteriorTraceGroup<Line>(2);

  xfldIedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldIedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  xfldIedge3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldIedge1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIedge1.getGroupRight(), 1 );

  BOOST_CHECK_EQUAL( xfldIedge2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIedge2.getGroupRight(), 2 );

  BOOST_CHECK_EQUAL( xfldIedge3.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIedge3.getGroupRight(), 3 );

  BOOST_CHECK_EQUAL( xfldIedge1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge1.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge1.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldIedge1.getCanonicalTraceRight(0).trace, 0 );

  BOOST_CHECK_EQUAL( xfldIedge2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge2.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge2.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldIedge2.getCanonicalTraceRight(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldIedge3.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge3.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge3.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldIedge3.getCanonicalTraceRight(0).trace, 2 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 3 );

  for (int i = 0; i < 3 ; i++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  // Labeled for group they attach to
  const XField2D_4Triangle_X1_4Group::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(0);
  const XField2D_4Triangle_X1_4Group::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(1);
  const XField2D_4Triangle_X1_4Group::FieldTraceGroupType<Line>& xfldBedge3 = xfld.getBoundaryTraceGroup<Line>(2);

  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );

  xfldBedge3.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBedge1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldBedge2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  BOOST_CHECK_EQUAL( xfldBedge1.getGroupLeft(), 1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getGroupRight(), -1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getGroupLeft(), 2 );
  BOOST_CHECK_EQUAL( xfldBedge2.getGroupRight(), -1 );
  BOOST_CHECK_EQUAL( xfldBedge3.getGroupLeft(), 3 );
  BOOST_CHECK_EQUAL( xfldBedge3.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBedge3.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getElementRight(0), -1 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceRight(0).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge3.getElementLeft(1), 0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getElementRight(1), -1 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceRight(1).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getElementRight(0), -1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceRight(0).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge1.getElementLeft(1), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getElementRight(1), -1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(1).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceRight(1).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getElementRight(0), -1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceRight(0).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge2.getElementLeft(1), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getElementRight(1), -1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(1).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceRight(1).trace, -1 );

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
