// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_2TRIANGLE_GHOSTBOUNDARY_X1_1GROUP
#define XFIELD2D_2TRIANGLE_GHOSTBOUNDARY_X1_1GROUP

#include "Field/XFieldArea.h"

namespace SANS
{
// A unit grid that consists of two triangles within a single cell group, one interior trace group, and one boundary trace group
//
//  2 ----- 3
//  |\      |
//  | \ (1) |
//  |  \    |
//  |   \   |
//  |    \  |
//  | (0) \ |
//  |      \|
//  0 ----- 1

class XField2D_2Triangle_GhostBoundary_X1_1Group : public XField<PhysD2,TopoD2>
{
public:
  // The default north west means that the ghost boundaries come last
  XField2D_2Triangle_GhostBoundary_X1_1Group(const bool NorthWest = true );
};

}

#endif //XFIELD2D_2TRIANGLE_X1_1GROUP
