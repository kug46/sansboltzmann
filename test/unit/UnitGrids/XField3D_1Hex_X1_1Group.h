// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD3D_1HEX_X1_1GROUP
#define XFIELD3D_1HEX_X1_1GROUP

#include "Field/XFieldVolume.h"

namespace SANS
{
/*
   A unit grid that consists of one hexahedron within a single group

//         y
//  3----------2
//  |\     ^   |\
//  | \    |   | \
//  |  \   |   |  \
//  |   7------+---6
//  |   |  +-- |-- | -> x
//  0---+---\--1   |
//   \  |    \  \  |
//    \ |     \  \ |
//     \|      z  \|
//      4----------5
*/

class XField3D_1Hex_X1_1Group : public XField<PhysD3,TopoD3>
{
public:
  XField3D_1Hex_X1_1Group();
};

}

#endif //XFIELD3D_1HEX_X1_1GROUP
