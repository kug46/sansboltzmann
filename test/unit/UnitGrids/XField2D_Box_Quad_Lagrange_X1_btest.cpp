// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/linspace.h"

#include "XField2D_Box_Quad_Lagrange_X1.h"

#include "unit/Field/XField2D_CheckTraceCoord2D_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_Lagrange_X1_static_test )
{
  BOOST_CHECK_EQUAL( XField2D_Box_Quad_Lagrange_X1::iBottom, 0 );
  BOOST_CHECK_EQUAL( XField2D_Box_Quad_Lagrange_X1::iRight , 1 );
  BOOST_CHECK_EQUAL( XField2D_Box_Quad_Lagrange_X1::iTop   , 2 );
  BOOST_CHECK_EQUAL( XField2D_Box_Quad_Lagrange_X1::iLeft  , 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_Lagrange_X1_test )
{
  // global communicator
  mpi::communicator world;

  XField2D_Box_Quad_Lagrange_X1 xfld(world, 3, 3, 0., 1., 0., 1.);

  BOOST_REQUIRE_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE_EQUAL( 1, xfld.nInteriorTraceGroups() );
  BOOST_REQUIRE_EQUAL( 4, xfld.nBoundaryTraceGroups() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
        CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_Lagrange_X1_test5 )
{
  // global communicator
  mpi::communicator world;

  const int ii = 2;
  const int jj = 3;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField2D_Box_Quad_Lagrange_X1 xfld( world, ii, jj );

#ifndef SANS_MPI
  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( ii*jj, xfld.nElem() );

  const int joffset = (ii+1);

  for (int j = 0; j < jj+1; j++)
  {
    for (int i = 0; i < ii+1; i++)
    {
      SANS_CHECK_CLOSE( xfld.DOF(j*joffset + i)[0], i/Real(ii), 1e-13, 1e-13);
      SANS_CHECK_CLOSE( xfld.DOF(j*joffset + i)[1], j/Real(jj), 1e-13, 1e-13);
    }
  }
#endif

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  // interior-face field variable

  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Line) );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_Lagrange_X1_Periodic_test1 )
{
  // global communicator
  mpi::communicator world;

  const int ii = 1; //Having 1 here tests corner situations
  const int jj = 4;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField2D_Box_Quad_Lagrange_X1 xfld( world, ii, jj,
                                      0, 1,
                                      0, 1,
                                      {{true, false}} );

#ifndef SANS_MPI
  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( ii*jj, xfld.nElem() );
#endif

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  // interior-face field variable

  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // check the left cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iRight).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iLeft).nElem() );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_Lagrange_X1_Periodic_test2 )
{
  // global communicator
  mpi::communicator world;

  const int ii = 3;
  const int jj = 4;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField2D_Box_Quad_Lagrange_X1 xfld( world, ii, jj,
                                      0, 1,
                                      0, 1,
                                      {{false, true}} );

#ifndef SANS_MPI
  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( ii*jj, xfld.nElem() );
#endif

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  // interior-face field variable

  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // check the left cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iTop).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iBottom).nElem() );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Quad_Lagrange_X1_Periodic_test3 )
{
  // global communicator
  mpi::communicator world;

  const int ii = 5;
  const int jj = 3;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField2D_Box_Quad_Lagrange_X1 xfld( world, ii, jj,
                                      {{true, true}} );

#ifndef SANS_MPI
  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( ii*jj, xfld.nElem() );
#endif

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  // interior-face field variable

  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // check the left cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iRight).getGroupRight() );
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iTop).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iLeft).nElem() );
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iBottom).nElem() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
