// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_BACKWARDSSTEP_X1
#define XFIELD2D_BACKWARDSSTEP_X1

#include "Field/XFieldArea.h"

namespace SANS
{

// cubic-source bump channel flow
// triangle grid in a 6x1 channel with 4 sides as separate boundary-edge groups
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// area elements in 1 group
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 6 groups: BottomInlet, Step, BottomOutlet, Top, Inlet, Outlet
//
// all edges are straight (X1)

class XField2D_BackwardsStep_X1 : public XField<PhysD2,TopoD2>
{
public:
  XField2D_BackwardsStep_X1( mpi::communicator& comm, int power,
                              const Real xUpstream = 1, const Real xDownstream = 1,
                              const Real yUpstream = 1 );

  static const int iBottomInlet,iStep,iBottomOutlet,iOutlet,iTop,iInlet;

protected:
  void generateGrid(mpi::communicator& comm,
                    const std::vector<Real>& xvec_L, const std::vector<Real>& xvec_R,
                    const std::vector<Real>& yvec_U, const std::vector<Real>& yvec_D );
};


}

#endif // XFIELD2D_BACKWARDSSTEP_X1
