// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Field/output_Tecplot.h"

#include <algorithm> // std::sort

#include "XField2D_SymAirfoil_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define VOID void
#define ANSI_DECLARATORS
#define TRILIBRARY
#define REAL double
extern "C"
{
#include <triangle.h>
}

namespace SANS
{

namespace //private to this file
{

void centroid( DLA::VectorS<2,Real>* DOF, std::vector<int>& nodes, double centroid[2] )
{
  centroid[0] = centroid[1] = 0;
  for ( std::size_t i = 0; i < nodes.size(); i++ )
  {
    centroid[0] += DOF[nodes[i]][0];
    centroid[1] += DOF[nodes[i]][1];
  }

  centroid[0] /= nodes.size();
  centroid[1] /= nodes.size();
}

void edgeNormal( DLA::VectorS<2,Real>* DOF, std::vector<int>& edge, double normal[2] )
{
  double tx, ty;

  tx = DOF[edge[1]][0]-DOF[edge[0]][0];
  ty = DOF[edge[1]][1]-DOF[edge[0]][1];

  normal[0] =  ty;
  normal[1] = -tx;
}

double dot( double v1[2], double v2[2] )
{
  return v1[0]*v2[0] + v1[1]*v2[1];
}

//Returns the edge number based on the point markers of a triangle segment. Edge number of 0 indicates an interior segment
int segmentEdgeNumber( const int pointmarkerlist[], const std::vector<int>& tri, const int seg )
{
  const int (*TraceNodes)[3][2] = &TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  const int trimark[3] = {pointmarkerlist[tri[0]],
                          pointmarkerlist[tri[1]],
                          pointmarkerlist[tri[2]]};

  // Assume interior segment
  int edge = 0;

  // One point on a node, other on an edge
  if ( trimark[(*TraceNodes)[seg][0]] == -1 && trimark[(*TraceNodes)[seg][1]] > 0)
    edge = trimark[(*TraceNodes)[seg][1]];

  // Other point on node, and one an edge
  else if ( trimark[(*TraceNodes)[seg][0]] > 0 && trimark[(*TraceNodes)[seg][1]] == -1)
    edge = trimark[(*TraceNodes)[seg][0]];

  // Both points on an edge (or interior)
  else if ( trimark[(*TraceNodes)[seg][0]] == trimark[(*TraceNodes)[seg][1]] )
    edge = trimark[(*TraceNodes)[seg][0]];

  return edge;
}

//Returns the edge number based on the point markers of a triangle segment. Edge number of 0 indicates an interior segment
int segmentEdgeNumber( const std::vector<int>& trimark, const int seg )
{
  const int (*TraceNodes)[3][2] = &TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  // Assume interior segment
  int edge = 0;

  // One point on a node, other on an edge
  if ( trimark[(*TraceNodes)[seg][0]] == -1 && trimark[(*TraceNodes)[seg][1]] > 0)
    edge = trimark[(*TraceNodes)[seg][1]];

  // Other point on node, and one an edge
  else if ( trimark[(*TraceNodes)[seg][0]] > 0 && trimark[(*TraceNodes)[seg][1]] == -1)
    edge = trimark[(*TraceNodes)[seg][0]];

  // Both points on an edge (or interior)
  else if ( trimark[(*TraceNodes)[seg][0]] == trimark[(*TraceNodes)[seg][1]] )
    edge = trimark[(*TraceNodes)[seg][0]];

  return edge;
}

bool sameSegment( const std::vector<int>& triL, const int segL, const std::vector<int>& triR, const int segR )
{
  const int (*TraceNodes)[3][2] = &TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  // Check if the point indexes of triL[segL] matches the indexes of triR[segR]
  return (triL[(*TraceNodes)[segL][0]] == triR[(*TraceNodes)[segR][0]] &&
          triL[(*TraceNodes)[segL][1]] == triR[(*TraceNodes)[segR][1]])
          ||
         (triL[(*TraceNodes)[segL][0]] == triR[(*TraceNodes)[segR][1]] &&
          triL[(*TraceNodes)[segL][1]] == triR[(*TraceNodes)[segR][0]]);
}

void triangulateioInit( triangulateio& io )
{

  io.pointlist = NULL;
  io.pointattributelist = NULL;
  io.pointmarkerlist = NULL;
  io.numberofpoints = 0;
  io.numberofpointattributes = 0;

  io.trianglelist = NULL;
  io.triangleattributelist = NULL;
  io.trianglearealist = NULL;
  io.neighborlist = NULL;
  io.numberoftriangles = 0;
  io.numberofcorners = 0;
  io.numberoftriangleattributes = 0;

  io.segmentlist = NULL;
  io.segmentmarkerlist = NULL;
  io.numberofsegments = 0;

  io.holelist = NULL;
  io.numberofholes = 0;

  io.regionlist = NULL;
  io.numberofregions = 0;

  io.edgelist = NULL;
  io.edgemarkerlist = NULL;
  io.normlist = NULL;
  io.numberofedges = 0;
}

void triangulateioDestroy( triangulateio& io )
{
  trifree( (void*)io.pointlist );
  trifree( (void*)io.pointattributelist );
  trifree( (void*)io.pointmarkerlist );

  trifree( (void*)io.trianglelist );
  trifree( (void*)io.triangleattributelist );
  trifree( (void*)io.trianglearealist );
  trifree( (void*)io.neighborlist );

  trifree( (void*)io.segmentlist );
  trifree( (void*)io.segmentmarkerlist );

  trifree( (void*)io.holelist );

  trifree( (void*)io.regionlist );

  trifree( (void*)io.edgelist );
  trifree( (void*)io.edgemarkerlist );
  trifree( (void*)io.normlist );

  // Set all pointers to NULL and counts to 0
  triangulateioInit( io );
}

} //namespace

XField2D_SymAirfoil::XField2D_SymAirfoil( const std::vector<DLA::VectorS<2,Real>>& airf, const Real rffd, int iistag, int iiwake, int iiffd )
{
  int comm_rank = comm_->rank();

  triangulateio in, out;

  triangulateioInit(in);
  triangulateioInit(out);

  int iiairf = (int)airf.size()-1;

  const int ii = iistag + iiairf + iiwake + iiffd;

  in.numberofpoints = ii;
  in.pointlist = (REAL *) malloc(in.numberofpoints * 2 * sizeof(REAL));
  in.pointmarkerlist = (int *) malloc(in.numberofpoints * sizeof(int));

  in.numberofsegments = in.numberofpoints-1;
  in.segmentlist = (int *) malloc( in.numberofsegments * 2 * sizeof(int));

  // This is needed, otherwise triangle fills in the airfoil
  in.numberofholes = 1;
  in.holelist = (REAL *) malloc(in.numberofholes * 2 * sizeof(REAL));
  in.holelist[0] = 0.5;
  in.holelist[1] = 1e-4;

//  printf(" nPoints = %d\n", in.numberofpoints );

#define xvec( jj ) in.pointlist[2*(jj)+0]
#define yvec( jj ) in.pointlist[2*(jj)+1]

  double xle = 0;
  double xte = 1;

  //double rffd = 3;

  int kupr;

  double x1, y1, x2, y2;

  // airfoil

  for (int i = 0; i <= iiairf; i++)
  {
    kupr = iistag + i;

    xvec(kupr) =  airf[i][0];
    yvec(kupr) =  airf[i][1];
    in.pointmarkerlist[kupr] = iAirfoilBoundaryGroup+1;
    //printf( "naca: kupr = %d  x = %13.6e\n", kupr, xvec(kupr) );
  }
  //Mark LE and TE as 'nodes'
  in.pointmarkerlist[iistag + 0     ] = -1;
  in.pointmarkerlist[iistag + iiairf] = -1;

  // stagnation line: quadratic increments w/ dx1 and dx2 matching airfoil

  x1 = xvec(iistag + 1);
  y1 = yvec(iistag + 1);
  x2 = xvec(iistag + 2);
  y2 = yvec(iistag + 2);

  double dx1 = sqrt( pow((xle - x1), 2) + y1*y1 );
  double dx2 = sqrt( pow(x1 - x2, 2) + pow(y1 - y2, 2) );
  double a, b, c;

  a = (2*((rffd - (xle+xte)/2) - xle) + iistag*((iistag - 3)*dx1 - (iistag - 1)*dx2)) / ((double) (2*iistag*(2 - iistag*(3 - iistag))));
  b = 0.5*(dx2 - dx1) - 3*a;
  c = 0.5*(3*dx1 - dx2) + 2*a;

  for (int i = 0; i < iistag; i++)
  {
    kupr = i;
    int j = iistag - i;
    xvec(kupr) = xle - j*(c + j*(b + j*a));
    yvec(kupr) = 0;
    in.pointmarkerlist[kupr] = 99;
    //printf( "stag: kupr = %d  x = %13.6e\n", kupr, xvec(kupr) );
  }
  //Mark first point as 'node', airfoil marked the last one
  in.pointmarkerlist[0] = -1;

  // wake: quadratic increments w/ dx1 and dx2 matching airfoil

  x1 = xvec(iistag + iiairf - 1);
  x2 = xvec(iistag + iiairf - 2);

  dx1 = xte - x1;
  dx2 = x1 - x2;

  a = (2*((rffd + (xle+xte)/2) - xte) + iiwake*((iiwake - 3)*dx1 - (iiwake - 1)*dx2)) / ((double) (2*iiwake*(2 - iiwake*(3 - iiwake))));
  b = 0.5*(dx2 - dx1) - 3*a;
  c = 0.5*(3*dx1 - dx2) + 2*a;

  for (int i = 0; i < iiwake; i++)
  {
    kupr = iistag + iiairf + 1 + i;
    int j = i+1;
    xvec(kupr) = xte + j*(c + j*(b + j*a));
    yvec(kupr) = 0;
    in.pointmarkerlist[kupr] = 99;
    //printf( "wake: kupr = %d  x = %13.6e\n", kupr, xvec(kupr) );
  }
  //Mark last point as 'node', the first was marked by the airfoil
  in.pointmarkerlist[iistag + iiairf + 1 + iiwake-1] = -1;

  // farfield

  double dth = PI/((double) iiffd);

  for (int i = 1; i < iiffd; i++)
  {
    int k = iistag + iiairf + iiwake + i;
    xvec(k) = (xle+xte)/2 + rffd * cos(i*dth);
    yvec(k) =               rffd * sin(i*dth);
    in.pointmarkerlist[k] = iFarfieldBoundaryGroup+1;
    //printf( "ff: kupr = %d  x = %13.6e\n", k, xvec(k) );
  }
  //No need to mark points as 'nodes'


  // Create the segments, which follow the order of the nodes
  for (int i = 0; i < in.numberofsegments; i++)
  {
    in.segmentlist[2*i+0] = i+0;
    in.segmentlist[2*i+1] = i+1;
  }

  // -p Triangulates a Planar Straight Line Graph
  // -c Encloses the convex hull with segments.
  // -e Outputs a list of edges of the triangulation.
  // -z Numbers all items starting from zero (rather than one). This switch is useful when calling Triangle from another program.
  // -n Outputs a list of triangles neighboring each triangle.
  // -Y Prohibits the insertion of Steiner points on the mesh boundary.
  //    If specified twice (-YY), it prohibits the insertion of Steiner points on any segment, including internal segments.
  // -V Verbose: Gives detailed information about what Triangle is doing. Add more `V's for increasing amount of detail.
  // -Q Quiet: Suppresses all explanation of what Triangle is doing, unless an error occurs.
  // -q Quality mesh generation with no angles smaller than 20 degrees. An alternate minimum angle may be specified after the `q'.
  triangulate((char*)"pceznYYQq33", &in, &out, NULL);

  in.holelist = NULL; //pointer has been moved to out
  triangulateioDestroy(in);

  //=============================
  // Copy over the triangle data to XField, and duplicate for symmetry
  //=============================

  // Add the LE point to the stagnation line count
  iistag++;

  // Add TE point to the wake node count
  iiwake++;

  // Set iiarif to the first point on the wakr that is not duplicated
  iiairf = iistag + iiairf - 1;

  // Stagnation and wake points are not duplicated
  resizeDOF( out.numberofpoints*2 - iistag - iiwake );

  //Create the element groups
  resizeInteriorTraceGroups(2); // Interior, stagnation line + wake
  resizeBoundaryTraceGroups(2); // Airfoil, Farfield
  resizeCellGroups(1);

  // Copy over the upper set of points
  for ( int i = 0; i < out.numberofpoints; i++ )
    DOF(i) = {out.pointlist[2*i+0], out.pointlist[2*i+1]};

  // Duplicate points on the airfoil to the lower half
  for ( int i = iistag; i < iiairf; i++ )
    DOF(i+out.numberofpoints - iistag) = {out.pointlist[2*i+0], -out.pointlist[2*i+1]};

  // Duplicate points in the volume to the lower half
  for ( int i = iiairf + iiwake; i < out.numberofpoints; i++ )
    DOF(i+out.numberofpoints - iistag - iiwake) = {out.pointlist[2*i+0], -out.pointlist[2*i+1]};


  nElem_ = 2*out.numberoftriangles;

  // area field associativity variable
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nElem_ );

  //element area associativity of the upper triangles
  for ( int i = 0; i < out.numberoftriangles; i++ )
    fldAssocCell.setAssociativity( i ).setNodeGlobalMapping( out.trianglelist + 3*i, 3 );

  //element area associativity of the lower triangles
  for ( int i = 0; i < out.numberoftriangles; i++ )
  {
    std::vector<int> tri = {out.trianglelist[3*i+0],
                            out.trianglelist[3*i+1],
                            out.trianglelist[3*i+2]};

    for ( int k = 0; k < 3; k++ )
      if ( tri[k] >= iistag && tri[k] < iiairf)
        tri[k] += out.numberofpoints - iistag;
      else if (tri[k] >= iiairf + iiwake )
        tri[k] += out.numberofpoints - iistag - iiwake;

    //Swap two nodes to get a positive area after mirroring the triangle
    std::swap(tri[0], tri[1]);

    fldAssocCell.setAssociativity( i+out.numberoftriangles ).setRank( comm_rank );
    fldAssocCell.setAssociativity( i+out.numberoftriangles ).setNodeGlobalMapping( tri );
  }


  // Count the total number of interior edges on the upper half of the domain
  int nInteriorTrace = 0;
  for ( int i = 0; i < out.numberoftriangles; i++ )
    for ( int k = 0; k < 3; k++ )
      if ( out.neighborlist[3*i + k] > i )
        nInteriorTrace++;


  // Create the mapping, BCelem, so elements on BCs are ordered based on the vertex number
  std::vector< std::vector< std::pair<int,int> > > elemBC(2);
  std::vector<int> groupSegBC(2, 0);

  // Construct the interior segments
  const int (*TraceNodes)[3][2] = &TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  // Count the total number of boundary segments
  for ( int iTriL = 0; iTriL < out.numberoftriangles; iTriL++ )
  {

    std::vector<int> triL = {out.trianglelist[3*iTriL+0],
                             out.trianglelist[3*iTriL+1],
                             out.trianglelist[3*iTriL+2]};

    for ( int segL = 0; segL < 3; segL++ )
    {
      const int edge = segmentEdgeNumber(out.pointmarkerlist, triL, segL);

      // Don't count the satagnation line or wake
      if (edge == 99) continue;

      std::vector<int> segnodes = {triL[(*TraceNodes)[segL][0]], triL[(*TraceNodes)[segL][1]]};

      const int iTriR = out.neighborlist[3*iTriL + segL];
      if ( edge > 0 && iTriR < 0 )
      {
        double centroidL[2];
        centroid(DOF_, triL, centroidL);

        double normal[2], centroidE[2], centroidVector[2];
        edgeNormal(DOF_, segnodes, normal );
        centroid(DOF_, segnodes, centroidE);

        // Create a vector from the edge centroid to the right triangle centroid
        centroidVector[0] = centroidL[0] - centroidE[0];
        centroidVector[1] = centroidL[1] - centroidE[1];

        // if the dot is negative, the normal points from left to right. Otherwise swap the nodes.
        const int lessernode = dot( centroidVector, normal ) < 0 ? segnodes[0] : segnodes[1];

        const int group = edge-1;

        elemBC[group].emplace_back( lessernode, groupSegBC[group] );
        groupSegBC[group]++;

        //Negate the node so the sorting is continuous
        elemBC[group].emplace_back( -lessernode, groupSegBC[group] );
        groupSegBC[group]++;
      }
    }
  }

  const int nStagElem = iistag - 1;
  const int nWakeElem = iiwake - 1;

  //The interior group constructor
  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, 2*nInteriorTrace );
  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocStagWake( BasisFunctionLineBase::HierarchicalP1, nStagElem + nWakeElem);
  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocBedge( 2 );

  // Set interior group connectivity
  fldAssocIedge.setGroupLeft( 0 );
  fldAssocIedge.setGroupRight( 0 );

  fldAssocStagWake.setGroupLeft( 0 );
  fldAssocStagWake.setGroupRight( 0 );

  //Resize the number of elements on all BC groups
  for (int group = 0; group < 2; group++ )
  {
    fldAssocBedge[group].resize( BasisFunctionLineBase::HierarchicalP1, groupSegBC[group] );
    fldAssocBedge[group].setGroupLeft( 0 );

    groupSegBC[group] = 0;

    //Sort all the elements based on the node index for the element
    //Use a lambda function for the sorting
    std::sort(elemBC[group].begin(), elemBC[group].end(),
        [](const std::pair<int,int>& a, const std::pair<int,int>& b) -> bool { return a.first < b.first; });
  }

  // Create the sorting map for BC's
  std::vector< std::vector<int> > segSortMap(2);
  for (int group = 0; group < 2; group++ )
  {
    int nseg = (int)elemBC[group].size();
    segSortMap[group].resize(elemBC[group].size());
    for ( int seg = 0; seg < nseg; seg++ )
      segSortMap[group][elemBC[group][seg].second] = seg;
  }

  int interiorSeg = 0;
  int stagSeg = 0;
  // Create the interior segment associations and cell-to-cell connectivity
  for ( int iTriL = 0; iTriL < out.numberoftriangles; iTriL++ )
  {
    fldAssocCell.setAssociativity( iTriL ).setRank( comm_rank );

    //element area associativity of the triangles
    fldAssocCell.setAssociativity( iTriL ).setNodeGlobalMapping( out.trianglelist + 3*iTriL, 3 );

    // Get the left triangle nodes
    std::vector<int> triL = {out.trianglelist[3*iTriL+0],
                             out.trianglelist[3*iTriL+1],
                             out.trianglelist[3*iTriL+2]};

    double centroidL[2];
    centroid(DOF_, triL, centroidL);

    for ( int segL = 0; segL < 3; segL++ )
    {
      const int edge = segmentEdgeNumber(out.pointmarkerlist, triL, segL);
      const int iTriR = out.neighborlist[3*iTriL + segL];

      if ( iTriR > iTriL ) //Right triangle always has larger index
      {

        std::vector<int> triR = {out.trianglelist[3*iTriR+0],
                                 out.trianglelist[3*iTriR+1],
                                 out.trianglelist[3*iTriR+2]};

        std::vector<int> segnodes = {triL[(*TraceNodes)[segL][0]], triL[(*TraceNodes)[segL][1]]};

        double normal[2], centroidE[2], centroidVector[2];
        edgeNormal(DOF_, segnodes, normal );
        centroid(DOF_, segnodes, centroidE);

        // Create a vector from the edge centroid to the left triangle centroid
        centroidVector[0] = centroidL[0] - centroidE[0];
        centroidVector[1] = centroidL[1] - centroidE[1];

        // Find the nodes for an segment on both triangles
        for (int segR = 0; segR < 3; segR++ )
        {
          if ( sameSegment(triL, segL, triR, segR) )
          {
            // do not increment the segment count there, that will be done for the mirrored triangles
            const int seg = interiorSeg;

            fldAssocIedge.setAssociativity( seg ).setRank( comm_rank );

            fldAssocIedge.setElementLeft( iTriL, seg );
            fldAssocIedge.setElementRight( iTriR, seg );

            fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(segL,  1), seg );
            fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(segR, -1), seg );

            // L is +1, and R is -1
            fldAssocCell.setAssociativity( iTriL ).setEdgeSign( +1, segL );
            fldAssocCell.setAssociativity( iTriR ).setEdgeSign( -1, segR );

            // if the dot is negative, the normal points from left to right. Otherwise swap the nodes.
            if ( dot( centroidVector, normal ) < 0 )
              fldAssocIedge.setAssociativity( seg ).setNodeGlobalMapping( {segnodes[0], segnodes[1]} );
            else
              fldAssocIedge.setAssociativity( seg ).setNodeGlobalMapping( {segnodes[1], segnodes[0]} );

            break;
          }
        }

        //Triangles on the lower mirrored half have the two first nodes swapped
        std::swap(triL[0], triL[1]);
        std::swap(triR[0], triR[1]);

        //mirror the centroid
        centroidL[1] = -centroidL[1];

        // Find the nodes for an segment on both triangles
        for ( int segL = 0; segL < 3; segL++ )
        {
          for (int segR = 0; segR < 3; segR++ )
          {
            if ( sameSegment(triL, segL, triR, segR) )
            {
              std::vector<int> segnodes = {triL[(*TraceNodes)[segL][0]], triL[(*TraceNodes)[segL][1]]};

              // offset the node count for the mirrored side if they are not on the stagnation line or wake
              for ( int k = 0; k < 2; k++ )
                if ( segnodes[k] >= iistag && segnodes[k] < iiairf)
                  segnodes[k] += out.numberofpoints - iistag;
                else if (segnodes[k] >= iiairf + iiwake )
                  segnodes[k] += out.numberofpoints - iistag - iiwake;


              double normal[2], centroidE[2], centroidVector[2];
              edgeNormal(DOF_, segnodes, normal );
              centroid(DOF_, segnodes, centroidE);

              // Create a vector from the edge centroid to the left triangle centroid
              centroidVector[0] = centroidL[0] - centroidE[0];
              centroidVector[1] = centroidL[1] - centroidE[1];

              // Get the mirrored segment number and increment it
              const int seg = interiorSeg + nInteriorTrace;
              interiorSeg++;

              fldAssocIedge.setAssociativity( seg ).setRank( comm_rank );

              fldAssocIedge.setElementLeft( iTriL+out.numberoftriangles, seg );
              fldAssocIedge.setElementRight( iTriR+out.numberoftriangles, seg );

              fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(segL,  1), seg );
              fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(segR, -1), seg );

              // L is +1, and R is -1
              fldAssocCell.setAssociativity( iTriL+out.numberoftriangles ).setEdgeSign( +1, segL );
              fldAssocCell.setAssociativity( iTriR+out.numberoftriangles ).setEdgeSign( -1, segR );

              // if the dot is negative, the normal points from left to right. Otherwise swap the nodes.
              if ( dot( centroidVector, normal ) < 0 )
                fldAssocIedge.setAssociativity( seg ).setNodeGlobalMapping( {segnodes[0], segnodes[1]} );
              else
                fldAssocIedge.setAssociativity( seg ).setNodeGlobalMapping( {segnodes[1], segnodes[0]} );

              break;
            }
          }
        }

        //Swap back the left triangle for the next segment
        std::swap(triL[0], triL[1]);

        //mirror the centroid back again
        centroidL[1] = -centroidL[1];
      }
      else if ( edge == 99 ) // Stagnation line or wake
      {
        std::vector<int> segnodes = {triL[(*TraceNodes)[segL][0]], triL[(*TraceNodes)[segL][1]]};

        double normal[2], centroidE[2], centroidVector[2];
        edgeNormal(DOF_, segnodes, normal );
        centroid(DOF_, segnodes, centroidE);

        // Create a vector from the edge centroid to the left triangle centroid
        centroidVector[0] = centroidL[0] - centroidE[0];
        centroidVector[1] = centroidL[1] - centroidE[1];

        std::vector<int> triR = triL;

        // Update the node index for the mirrored triangle
        for ( int k = 0; k < 3; k++ )
          if ( triR[k] >= iistag && triR[k] < iiairf)
            triR[k] += out.numberofpoints - iistag;
          else if (triR[k] >= iiairf + iiwake )
            triR[k] += out.numberofpoints - iistag - iiwake;

        // Swap nodes for the mirrored side
        std::swap(triR[0], triR[1]);

        // Set the right triangle index
        const int iTriR = iTriL + out.numberoftriangles;

        // Find the nodes for an segment on both triangles
        for (int segR = 0; segR < 3; segR++ )
        {
          if ( sameSegment(triL, segL, triR, segR) )
          {
            const int seg = stagSeg;
            stagSeg++;

            fldAssocStagWake.setAssociativity( seg ).setRank( comm_rank );

            fldAssocStagWake.setElementLeft( iTriL, seg );
            fldAssocStagWake.setElementRight( iTriR, seg );

            fldAssocStagWake.setCanonicalTraceLeft(  CanonicalTraceToCell(segL,  1), seg );
            fldAssocStagWake.setCanonicalTraceRight( CanonicalTraceToCell(segR, -1), seg );

            // L is +1, and R is -1
            fldAssocCell.setAssociativity( iTriL ).setEdgeSign( +1, segL );
            fldAssocCell.setAssociativity( iTriR ).setEdgeSign( -1, segR );

            // if the dot is negative, the normal points from left to right. Otherwise swap the nodes.
            if ( dot( centroidVector, normal ) < 0 )
              fldAssocStagWake.setAssociativity( seg ).setNodeGlobalMapping( {segnodes[0], segnodes[1]} );
            else
              fldAssocStagWake.setAssociativity( seg ).setNodeGlobalMapping( {segnodes[1], segnodes[0]} );

            break;
          }
        }
      }
      else if ( edge > 0 && iTriR < 0 ) // boundary edge
      {
        std::vector<int> segnodes = {triL[(*TraceNodes)[segL][0]], triL[(*TraceNodes)[segL][1]]};

        double normal[2], centroidE[2], centroidVector[2];
        edgeNormal(DOF_, segnodes, normal );
        centroid(DOF_, segnodes, centroidE);

        // Create a vector from the edge centroid to the left triangle centroid
        centroidVector[0] = centroidL[0] - centroidE[0];
        centroidVector[1] = centroidL[1] - centroidE[1];

        const int group = edge-1;

        int seg = segSortMap[group][groupSegBC[group]];
        groupSegBC[group]++;

        fldAssocBedge[group].setAssociativity( seg ).setRank( comm_rank );

        fldAssocBedge[group].setElementLeft( iTriL, seg );
        fldAssocBedge[group].setCanonicalTraceLeft(  CanonicalTraceToCell(segL,  1), seg );

        fldAssocCell.setAssociativity( iTriL ).setEdgeSign( +1, segL );

        // if the dot is negative, the normal points from left to right. Otherwise swap the nodes.
        if ( dot( centroidVector, normal ) < 0 )
          fldAssocBedge[group].setAssociativity( seg ).setNodeGlobalMapping( {segnodes[0], segnodes[1]} );
        else
          fldAssocBedge[group].setAssociativity( seg ).setNodeGlobalMapping( {segnodes[1], segnodes[0]} );


        // Create the mirrored triangle
        std::vector<int> triM = triL;

        // Update the node index for the mirrored triangle
        for ( int k = 0; k < 3; k++ )
          if ( triM[k] >= iistag && triM[k] < iiairf )
            triM[k] += out.numberofpoints - iistag;
          else if ( triM[k] >= iiairf + iiwake )
            triM[k] += out.numberofpoints - iistag - iiwake;

        // Swap nodes for the mirrored side
        std::swap(triM[0], triM[1]);

        // Set the mirrored triangle index
        const int iTriM = iTriL + out.numberoftriangles;

        std::vector<int> trimarkL = {out.pointmarkerlist[triL[0]],
                                     out.pointmarkerlist[triL[1]],
                                     out.pointmarkerlist[triL[2]]};
        std::vector<int> trimarkM = trimarkL;
        std::swap(trimarkM[0], trimarkM[1]);

        //mirror the centroid
        centroidL[1] = -centroidL[1];

        // Find the nodes for an segment on the boundary of the mirrored triangle
        for (int segM = 0; segM < 3; segM++ )
        {
          if ( edge == segmentEdgeNumber(trimarkM, segM) )
          {
            segnodes = {triM[(*TraceNodes)[segM][0]], triM[(*TraceNodes)[segM][1]]};

            double normal[2], centroidE[2], centroidVector[2];
            edgeNormal( DOF_, segnodes, normal );
            centroid( DOF_, segnodes, centroidE );

            // Create a vector from the edge centroid to the left triangle centroid
            centroidVector[0] = centroidL[0] - centroidE[0];
            centroidVector[1] = centroidL[1] - centroidE[1];

            seg = segSortMap[group][groupSegBC[group]];
            groupSegBC[group]++;

            fldAssocBedge[group].setAssociativity( seg ).setRank( comm_rank );

            fldAssocBedge[group].setElementLeft( iTriM, seg );

            fldAssocBedge[group].setCanonicalTraceLeft( CanonicalTraceToCell(segM, 1), seg );

            // L is +1, and R is -1
            fldAssocCell.setAssociativity( iTriM ).setEdgeSign( +1, segM );

            // if the dot is negative, the normal points from left to right. Otherwise swap the nodes.
            if ( dot( centroidVector, normal ) < 0 )
              fldAssocBedge[group].setAssociativity( seg ).setNodeGlobalMapping( {segnodes[0], segnodes[1]} );
            else
              fldAssocBedge[group].setAssociativity( seg ).setNodeGlobalMapping( {segnodes[1], segnodes[0]} );

            break;
          }
        }

        //mirror the centroid back again
        centroidL[1] = -centroidL[1];
      }
    }
  }


  // clean up memory
  triangulateioDestroy(out);


  interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocIedge );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

  interiorTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocStagWake );
  interiorTraceGroups_[1]->setDOF(DOF_, nDOF_);


  for (int group = 0; group < 2; group++ )
  {
    boundaryTraceGroups_[group] = new FieldTraceGroupType<Line>( fldAssocBedge[group] );
    boundaryTraceGroups_[group]->setDOF(DOF_, nDOF_);
  }

  cellGroups_[0] = new FieldCellGroupType<Triangle>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  //Check for any errors in the grid
  checkGrid();

}

} //namespace SANS
