// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_Box_UnionJack_LocalRefine_Triangle_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj (quad) elements
//
// diagonals oriented as in Union Jack flag
// if refineFlag is set, has one refined element at the centre
//
// area elements in 1 group: 2*ii*jj triangles
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 4 groups: lower, right, upper, left

const int XField2D_Box_UnionJack_LocalRefine_Triangle_X1::iBottom = 0;
const int XField2D_Box_UnionJack_LocalRefine_Triangle_X1::iRight  = 1;
const int XField2D_Box_UnionJack_LocalRefine_Triangle_X1::iTop    = 2;
const int XField2D_Box_UnionJack_LocalRefine_Triangle_X1::iLeft   = 3;

XField2D_Box_UnionJack_LocalRefine_Triangle_X1::XField2D_Box_UnionJack_LocalRefine_Triangle_X1(
        int ii, int jj, bool refine, Real xmin, Real xmax, Real ymin, Real ymax )
{
  int comm_rank = comm_->rank();

  SANS_ASSERT_MSG( (ii%2 == 1) && (jj%2 == 1), "coded for odd number of cells in I and J" );
  SANS_ASSERT_MSG( (ii>=3) && (jj>=3), "coded for minimum 3 cells in I and J" );

  int nnode, nelem;
  if (refine == true)
  {
    nnode = (ii + 1)*(jj + 1) + 1; // Plus one for central point
    nelem = 2*ii*jj + 2; // Plus 2 for split central element
  }
  else
  {
    nnode = (ii + 1)*(jj + 1); // unsplit central element
    nelem = 2*ii*jj; // unsplit central element
  }
  int i, j;

  // create the grid-coordinate DOF arrays
  resizeDOF( nnode );

  for (i = 0; i < ii+1; i++)
  {
    for (j = 0; j < jj+1; j++)
    {
      DOF(j*(ii+1) + i)[0] = xmin + (xmax - xmin)*i/Real(ii);
      DOF(j*(ii+1) + i)[1] = ymin + (ymax - ymin)*j/Real(jj);
    }
  }

  if (refine)
  {
    // Additional node at centre of grid
    DOF((ii + 1)*(jj + 1))[0] = xmin + (xmax - xmin)/2;
    DOF((ii + 1)*(jj + 1))[1] = ymin + (ymax - ymin)/2;
  }

  // create the element groups
  int cnt = 1;          // interior: diagonal
  if (ii > 1) cnt++;    // interior: horizontal
  if (jj > 1) cnt++;    // interior: vertical
  resizeInteriorTraceGroups(cnt);

  resizeBoundaryTraceGroups(4);     // bottom, right, upper, left
  resizeCellGroups(1);

  // grid area field variable

  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nelem );

  // area element grid-coordinate DOF associativity (cell-to-node connectivity)
  // edge signs for area elements (L is +, R is -)
  // NOTE: assumes edge orientations set below in interior & boundary-edge field variables

  ElementAssociativityConstructor<TopoD2, Triangle> xDOFAssocArea( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  int n1, n2, n3, n4;

  xDOFAssocArea.setRank( comm_rank );

  // lower, left
  for (j = 0; j < jj/2; j++) // uses c++ truncation to stop before central row
  {
    for (i = 0; i < ii/2; i++) // uses c++ truncation to stop before central column
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  | /|
      n3 = n2 + (ii+1);         //  |/ |
      n4 = n1 + (ii+1);         // n1--n2

      xDOFAssocArea.setNodeGlobalMapping( {n4, n1, n3} );

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n2, n3, n1} );

      xDOFAssocArea.setEdgeSign( -1, 0 );
      xDOFAssocArea.setEdgeSign( +1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );

    }
  }

  // lower, right
  for (j = 0; j < jj/2; j++) // uses c++ truncation to stop before central row
  {
    for (i = ii/2 + 1; i < ii; i++) // uses c++ truncation to start after the central column
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  |\ |
      n3 = n2 + (ii+1);         //  | \|
      n4 = n1 + (ii+1);         // n1--n2

      xDOFAssocArea.setNodeGlobalMapping( {n1, n2, n4} );

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n3, n4, n2} );

      xDOFAssocArea.setEdgeSign( -1, 0 );
      xDOFAssocArea.setEdgeSign( +1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );

    }
  }

  // upper, left
  for (j = jj/2 + 1; j < jj; j++) // uses c++ truncation to start after the central row
  {
    for (i = 0; i < ii/2; i++) // uses c++ truncation to stop before central column
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  |\ |
      n3 = n2 + (ii+1);         //  | \|
      n4 = n1 + (ii+1);         // n1--n2

      xDOFAssocArea.setNodeGlobalMapping( {n1, n2, n4} );

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n3, n4, n2} );

      xDOFAssocArea.setEdgeSign( -1, 0 );
      xDOFAssocArea.setEdgeSign( +1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );

    }
  }

  // upper, right
  for (j = jj/2 + 1; j < jj; j++) // uses c++ truncation to start after the central row
  {
    for (i = ii/2 + 1 ; i < ii; i++) // uses c++ truncation to start after the central column
    {
      n1 = j*(ii+1) + i;        // n4--n3
      n2 = n1 + 1;              //  | /|
      n3 = n2 + (ii+1);         //  |/ |
      n4 = n1 + (ii+1);         // n1--n2

      xDOFAssocArea.setNodeGlobalMapping( {n4, n1, n3} );

      xDOFAssocArea.setEdgeSign( +1, 0 );
      xDOFAssocArea.setEdgeSign( -1, 1 );
      xDOFAssocArea.setEdgeSign( -1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

      xDOFAssocArea.setNodeGlobalMapping( {n2, n3, n1} );

      xDOFAssocArea.setEdgeSign( -1, 0 );
      xDOFAssocArea.setEdgeSign( +1, 1 );
      xDOFAssocArea.setEdgeSign( +1, 2 );

      fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );

    }
  }

  // central column
  i = ii/2;
  for (j = 0; j < jj; j++)
  {
    if (refine && j == jj/2) continue; // skip the central element if refining

    n1 = j*(ii+1) + i;        // n4--n3
    n2 = n1 + 1;              //  | /|
    n3 = n2 + (ii+1);         //  |/ |
    n4 = n1 + (ii+1);         // n1--n2

    xDOFAssocArea.setNodeGlobalMapping( {n4, n1, n3} );

    xDOFAssocArea.setEdgeSign( +1, 0 );
    xDOFAssocArea.setEdgeSign( -1, 1 );
    xDOFAssocArea.setEdgeSign( -1, 2 );

    fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

    xDOFAssocArea.setNodeGlobalMapping( {n2, n3, n1} );

    xDOFAssocArea.setEdgeSign( -1, 0 );
    xDOFAssocArea.setEdgeSign( +1, 1 );
    xDOFAssocArea.setEdgeSign( +1, 2 );

    fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );
  }

  // central row
  j = jj/2;
  for (i = 0; i < ii; i++)
  {
    if ( i == ii/2) continue; // skip the central element (either caught above or by refining)

    n1 = j*(ii+1) + i;        // n4--n3
    n2 = n1 + 1;              //  | /|
    n3 = n2 + (ii+1);         //  |/ |
    n4 = n1 + (ii+1);         // n1--n2

    xDOFAssocArea.setNodeGlobalMapping( {n4, n1, n3} );

    xDOFAssocArea.setEdgeSign( +1, 0 );
    xDOFAssocArea.setEdgeSign( -1, 1 );
    xDOFAssocArea.setEdgeSign( -1, 2 );

    fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

    xDOFAssocArea.setNodeGlobalMapping( {n2, n3, n1} );

    xDOFAssocArea.setEdgeSign( -1, 0 );
    xDOFAssocArea.setEdgeSign( +1, 1 );
    xDOFAssocArea.setEdgeSign( +1, 2 );

    fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );
  }


  if (refine)
  {
    // central element
    //  n4 -- n3
    //  | \  / |
    //  |  n5  |
    //  | /  \ |
    //  n1 -- n2

    i = ii/2; j = jj/2;
    n1 = j*(ii+1) + i;
    n2 = n1 + 1;
    n3 = n2 + (ii+1);
    n4 = n1 + (ii+1);
    int n5 = (ii + 1)*(jj + 1); // central node

    // left triangle
    xDOFAssocArea.setNodeGlobalMapping( {n4, n1, n5} );

    xDOFAssocArea.setEdgeSign( +1, 0 );
    xDOFAssocArea.setEdgeSign( -1, 1 );
    xDOFAssocArea.setEdgeSign( -1, 2 );

    fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i );

    // bottom triangle
    xDOFAssocArea.setNodeGlobalMapping( {n2, n5, n1} );

    xDOFAssocArea.setEdgeSign( -1, 0 );
    xDOFAssocArea.setEdgeSign( +1, 1 );
    xDOFAssocArea.setEdgeSign( +1, 2 );

    fldAssocCell.setAssociativity( xDOFAssocArea, j*(2*ii) + 2*i + 1 );

    // top triangle
    xDOFAssocArea.setNodeGlobalMapping( {n4, n5, n3} );

    xDOFAssocArea.setEdgeSign( +1, 0 );
    xDOFAssocArea.setEdgeSign( -1, 1 );
    xDOFAssocArea.setEdgeSign( +1, 2 );

    fldAssocCell.setAssociativity( xDOFAssocArea, 2*ii*jj ); // penultimate cell (indexed from 0)

    // right triangle
    xDOFAssocArea.setNodeGlobalMapping( {n2, n3, n5} );

    xDOFAssocArea.setEdgeSign( -1, 0 );
    xDOFAssocArea.setEdgeSign( -1, 1 );
    xDOFAssocArea.setEdgeSign( +1, 2 );

    fldAssocCell.setAssociativity( xDOFAssocArea, 2*ii*jj + 1 ); // ultimate cell (indexed from 0)

  }

  // left boundary-edge: oriented down
  for (j = 0; j < jj/2 + 1; j++) // attaches central row
  {
    i = 0;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i ).setEdgeSign( +1, 2 );
  }
  // left boundary-edge: oriented up
  for (j = jj/2 + 1; j < jj; j++) // uses c++ truncation to start after central row
  {
    i = 0;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i ).setEdgeSign( +1, 1 );
  }

  // top boundary-edge: oriented left
  for (i = 0; i < ii/2; i++) // uses c++ truncation to stop before central column
  {
    j = jj-1;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i + 1 ).setEdgeSign( +1, 2 );
  }
  // top boundary-edge: oriented right
  for (i = ii/2; i < ii; i++) // attaches central column
  {
    j = jj-1;
    fldAssocCell.setAssociativity( j*(2*ii) + 2*i ).setEdgeSign( +1, 1 );
  }



  FieldCellGroupType<Triangle>* xfldArea = NULL;
  cellGroups_[0] = xfldArea = new FieldCellGroupType<Triangle>( fldAssocCell );

  xfldArea->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // grid interior-edge field variable

  FieldTraceGroupType<Line>* xfldIedge = NULL;
  ElementAssociativityConstructor<TopoD1, Line> xDOFAssocEdge( BasisFunctionLineBase::HierarchicalP1 );
  int edge, nedge;
  int c1, c2;

  cnt = 0;

  xDOFAssocEdge.setRank( comm_rank );

  // horizontal edges
  if (jj > 1)
  {
    nedge = (jj-1)*ii;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;

    // lower, left
    for (j = 1; j < jj/2; j++)
    {
      for (i = 0; i < ii/2; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(2*ii) + 2*i + 1;
        c2 = (j-1)*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }
    }

    // lower, right
    for (j = 1; j < jj/2; j++)
    {
      for (i = ii/2 + 1; i < ii; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(2*ii) + 2*i;
        c2 = (j-1)*(2*ii) + 2*i + 1;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }
    }

    // upper, left
    for (j = jj/2 + 2; j < jj; j++) // need to leave bisector
    {
      for (i = 0; i < ii/2; i++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(2*ii) + 2*i;
        c2 = (j-1)*(2*ii) + 2*i + 1;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }
    }

    // upper, right
    for (j = jj/2 + 2; j < jj; j++) // + 2 for top of row
    {
      for (i = ii/2 + 1; i < ii; i++) // +1 right of column
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + 1;
        c1 = (j  )*(2*ii) + 2*i + 1;
        c2 = (j-1)*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }
    }

    // central row
    j = jj/2; // along bottom of row
    for (i = 0; i < ii/2+1; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + 1;
      c1 = (j  )*(2*ii) + 2*i + 1;
      c2 = (j-1)*(2*ii) + 2*i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

      edge++;
    }
    j = jj/2 + 1; // along top of row
    for (i = ii/2+1; i < ii; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + 1;
      c1 = (j  )*(2*ii) + 2*i + 1;
      c2 = (j-1)*(2*ii) + 2*i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

      edge++;
    }

    // central column
    i = ii/2;
    for (j = 1; j < jj; j++)
    {
      if (j == jj/2) continue; // skip central element, row took care of it

      n1 = j*(ii+1) + i;
      n2 = n1 + 1;
      c1 = (j  )*(2*ii) + 2*i + 1;
      c2 = (j-1)*(2*ii) + 2*i;
      if ( (j == jj/2 + 1) && refine) // if refining right element changes
        c2 = 2*ii*jj;


      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

      edge++;
    }

    // bisectors
    j = jj/2 + 1; // +1 on top of row
    for (i = 0; i < ii/2; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + 1;
      c1 = (j  )*(2*ii) + 2*i;
      c2 = (j-1)*(2*ii) + 2*i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

      edge++;
    }
    j = jj/2; // on bottom of row
    for (i = ii/2 + 1; i < ii; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + 1;
      c1 = (j  )*(2*ii) + 2*i + 1;
      c2 = (j-1)*(2*ii) + 2*i + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

      edge++;
    }

    interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
    cnt++;

    xfldIedge->setDOF(DOF_, nDOF_);
  }

  // vertical edges
  if (ii > 1)
  {
    nedge = (ii-1)*jj;
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType
      fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

    fldAssocIedge.setGroupLeft( 0 );
    fldAssocIedge.setGroupRight( 0 );

    edge = 0;

    // lower, left
    for (i = 1; i < ii/2; i++)
    {
      for (j = 0; j < jj/2; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }
    }

    // lower, right
    for (i = ii/2 + 2; i < ii; i++) // +2 to skip column
    {
      for (j = 0; j < jj/2; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }
    }

    // upper, left
    for (i = 1; i < ii/2; i++)
    {
      for (j = jj/2 + 1; j < jj; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

        edge++;
      }
    }

    // upper, right
    for (i = ii/2 + 2; i < ii; i++) // + 2 to skip column
    {
      for (j = jj/2 + 1; j < jj; j++)
      {
        n1 = j*(ii+1) + i;
        n2 = n1 + (ii+1);
        c1 = j*(2*ii) + 2*i - 1;
        c2 = j*(2*ii) + 2*i;

        // grid-coordinate DOF association (edge-to-node connectivity)
        xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
        fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

        // edge-to-cell connectivity
        fldAssocIedge.setElementLeft(  c1, edge );
        fldAssocIedge.setElementRight( c2, edge );
        fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
        fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

        edge++;
      }
    }

    // central row
    j = jj/2;
    for (i = 1; i < ii; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*i - 1;
      c2 = j*(2*ii) + 2*i;
      if ( i == ii/2 + 1 && refine) // if refining left element changes
        c1 = 2*ii*jj + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

      edge++;
    }


    // central column
    i = ii/2; // left of column
    for (j = 0; j < jj/2; j++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*i - 1;
      c2 = j*(2*ii) + 2*i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

      edge++;
    }
    i = ii/2 + 1; // right of column
    for (j = jj/2 + 1; j < jj; j++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*i - 1;
      c2 = j*(2*ii) + 2*i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

      edge++;
    }

    // bisectors
    i = ii/2 + 1; // right of column
    for (j = 0; j < jj/2; j++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*i - 1;
      c2 = j*(2*ii) + 2*i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

      edge++;
    }
    i = ii/2; // left of column
    for (j = jj/2 + 1; j < jj; j++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*i - 1;
      c2 = j*(2*ii) + 2*i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(2, -1), edge );

      edge++;
    }

    interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
    cnt++;

    xfldIedge->setDOF(DOF_, nDOF_);
  }

  // diagonal edges

  if (refine)
    nedge = ii*jj + 3;
  else
    nedge = ii*jj;

  FieldTraceGroupType<Line>::FieldAssociativityConstructorType
    fldAssocIedge( BasisFunctionLineBase::HierarchicalP1, nedge );

  fldAssocIedge.setGroupLeft( 0 );
  fldAssocIedge.setGroupRight( 0 );

  edge = 0;

  // lower, left
  for (j = 0; j < jj/2; j++)
  {
    for (i = 0; i < ii/2; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1) + 1;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

      edge++;
    }
  }

  // lower, right
  for (j = 0; j < jj/2; j++)
  {
    for (i = ii/2 + 1; i < ii; i++)
    {
      n1 = j*(ii+1) + i + 1;
      n2 = n1 + (ii+1) - 1;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

      edge++;
    }
  }

  // upper, left
  for (j = jj/2 + 1; j < jj; j++)
  {
    for (i = 0; i < ii/2; i++)
    {
      n1 = j*(ii+1) + i + 1;
      n2 = n1 + (ii+1) - 1;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

      edge++;
    }
  }

  // upper, right
  for (j = jj/2 + 1; j < jj; j++)
  {
    for (i = ii/2 + 1; i < ii; i++)
    {
      n1 = j*(ii+1) + i;
      n2 = n1 + (ii+1) + 1;
      c1 = j*(2*ii) + 2*i;
      c2 = j*(2*ii) + 2*i + 1;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocIedge.setElementLeft(  c1, edge );
      fldAssocIedge.setElementRight( c2, edge );
      fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
      fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

      edge++;
    }
  }

  // central column
  i = ii/2;
  for (int j = 0; j < jj; j++)
  {
    if (refine && j == jj/2) continue; // skip the central element if refining
    n1 = j*(ii+1) + i;
    n2 = n1 + (ii+1) + 1;
    c1 = j*(2*ii) + 2*i;
    c2 = j*(2*ii) + 2*i + 1;

    // grid-coordinate DOF association (edge-to-node connectivity)
    xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
    fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

    // edge-to-cell connectivity
    fldAssocIedge.setElementLeft(  c1, edge );
    fldAssocIedge.setElementRight( c2, edge );
    fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
    fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

    edge++;
  }

  // central row
  j = jj/2;
  for (int i = 0; i < ii; i++)
  {
    if (i == ii/2) continue; // skip the central element

    n1 = j*(ii+1) + i;
    n2 = n1 + (ii+1) + 1;
    c1 = j*(2*ii) + 2*i;
    c2 = j*(2*ii) + 2*i + 1;

    // grid-coordinate DOF association (edge-to-node connectivity)
    xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
    fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

    // edge-to-cell connectivity
    fldAssocIedge.setElementLeft(  c1, edge );
    fldAssocIedge.setElementRight( c2, edge );
    fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
    fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

    edge++;
  }

  // central element
  if (refine)
  {
    // central element
    //  n4 -- n3
    //  | \  / |
    //  |  n5  |
    //  | /  \ |
    //  n1 -- n2

    i = ii/2; j = jj/2;
    n1 = j*(ii+1) + i;
    n2 = n1 + 1;
    n3 = n2 + (ii+1);
    n4 = n1 + (ii+1);
    int n5 = (ii + 1)*(jj + 1); // central node
    c1 = j*(2*ii) + 2*i;
    c2 = j*(2*ii) + 2*i + 1;
    int c3 = 2*ii*jj;
    int c4 = 2*ii*jj + 1;

    // lower, left
    // grid-coordinate DOF association (edge-to-node connectivity)
    xDOFAssocEdge.setNodeGlobalMapping( {n1, n5} );
    fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

    // edge-to-cell connectivity
    fldAssocIedge.setElementLeft(  c1, edge );
    fldAssocIedge.setElementRight( c2, edge );
    fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
    fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

    edge++;

    // upper, right
    // grid-coordinate DOF association (edge-to-node connectivity)
    xDOFAssocEdge.setNodeGlobalMapping( {n5, n3} );
    fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

    // edge-to-cell connectivity
    fldAssocIedge.setElementLeft(  c3, edge );
    fldAssocIedge.setElementRight( c4, edge );
    fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(0,  1), edge );
    fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(0, -1), edge );

    edge++;

    // upper, left
    // grid-coordinate DOF association (edge-to-node connectivity)
    xDOFAssocEdge.setNodeGlobalMapping( {n4, n5} );
    fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

    // edge-to-cell connectivity
    fldAssocIedge.setElementLeft(  c3, edge );
    fldAssocIedge.setElementRight( c1, edge );
    fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
    fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

    edge++;

    // lower, right
    // grid-coordinate DOF association (edge-to-node connectivity)
    xDOFAssocEdge.setNodeGlobalMapping( {n2, n5} );
    fldAssocIedge.setAssociativity( xDOFAssocEdge, edge );

    // edge-to-cell connectivity
    fldAssocIedge.setElementLeft(  c2, edge );
    fldAssocIedge.setElementRight( c4, edge );
    fldAssocIedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
    fldAssocIedge.setCanonicalTraceRight( CanonicalTraceToCell(1, -1), edge );

    edge++;
  }

  interiorTraceGroups_[cnt] = xfldIedge = new FieldTraceGroupType<Line>( fldAssocIedge );
  cnt++;

  xfldIedge->setDOF(DOF_, nDOF_);

  // grid boundary-edge field variables
  // Note: all edges oriented so domain is on its left

  FieldTraceGroupType<Line>* xfldBedge = NULL;

  // lower boundary
  {
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, ii );

    fldAssocBedge.setGroupLeft( 0 );

    for (i = 0; i < ii/2 + 1; i++) // +1 for central column
    {
      n1 = i;
      n2 = i + 1;
      c1 = 2*i + 1;
      edge = i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
    }

    for (i = ii/2 + 1; i < ii; i++) // +1 for central column
    {
      n1 = i;
      n2 = i + 1;
      c1 = 2*i;
      edge = i;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
    }

    boundaryTraceGroups_[iBottom] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  // right boundary
  {
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocBedge.setGroupLeft( 0 );

    for (j = 0; j < jj/2; j++)
    {
      n1 = j*(ii+1) + ii;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*ii - 1;
      edge = j;

      // edge-to-cell connectivity
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
    }

    for (j = jj/2; j < jj; j++)
    {
      n1 = j*(ii+1) + ii;
      n2 = n1 + (ii+1);
      c1 = j*(2*ii) + 2*ii - 1;
      edge = j;

      // edge-to-cell connectivity
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // grid-coordinate DOF association (edge-to-node connectivity)
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
    }

    boundaryTraceGroups_[iRight] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  // upper boundary
  {
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, ii );

    fldAssocBedge.setGroupLeft( 0 );

    for (i = ii-1; i >= ii/2; i--)
    {
      n1 = jj*(ii+1) + i + 1;
      n2 = jj*(ii+1) + i;
      c1 = (jj-1)*(2*ii) + 2*i;
      edge = ii-1 - i;
      // printf( "upper: i = %d  n1 = %d  n2 = %d  c1 = %d\n", i, n1, n2, c1 );

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
    }

    for (i = ii/2 - 1; i >= 0; i--)
    {
      n1 = jj*(ii+1) + i + 1;
      n2 = jj*(ii+1) + i;
      c1 = (jj-1)*(2*ii) + 2*i + 1;
      edge = ii-1 - i;
      // printf( "upper: i = %d  n1 = %d  n2 = %d  c1 = %d\n", i, n1, n2, c1 );

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
    }

    boundaryTraceGroups_[iTop] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  // left boundary
  {
    FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBedge( BasisFunctionLineBase::HierarchicalP1, jj );

    fldAssocBedge.setGroupLeft( 0 );

    for (j = jj-1; j > jj/2; j--)
    {
      n1 = j*(ii+1) + (ii+1);
      n2 = j*(ii+1);
      c1 = j*(2*ii);
      edge = jj-1 - j;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(1,  1), edge );
    }

    for (j = jj/2; j >= 0; j--)
    {
      n1 = j*(ii+1) + (ii+1);
      n2 = j*(ii+1);
      c1 = j*(2*ii);
      edge = jj-1 - j;

      // grid-coordinate DOF association (edge-to-node connectivity)
      xDOFAssocEdge.setNodeGlobalMapping( {n1, n2} );
      fldAssocBedge.setAssociativity( xDOFAssocEdge, edge );

      // edge-to-cell connectivity
      fldAssocBedge.setElementLeft(  c1, edge );
      fldAssocBedge.setCanonicalTraceLeft(  CanonicalTraceToCell(2,  1), edge );
    }

    boundaryTraceGroups_[iLeft] = xfldBedge = new FieldTraceGroupType<Line>( fldAssocBedge );

    xfldBedge->setDOF(DOF_, nDOF_);
  }

  //Check that the grid is correct
  checkGrid();

#if 0
  output_Tecplot( *xfldArea, "tmp/grid.plt" );
#endif
}

}
