// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "unit/UnitGrids/XField3D_ForwardStep_Hex_X1.h"

#include "Field/Partition/XField_Lagrange.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "tools/output_std_vector.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// create hexahedral grid in unit box with ii x jj x kk elements
/*
// volume elements in 1 group: ii x jj x kk hex
// interior-quad elements in 3 groups: x-const, y-const, z-const
// boundary-quad elements in 6 groups: x-min, x-max, y-min, y-max, z-min, z-max

// Node ordering of the original hex (not canonical ordering)
//
//         y
//  2----------3
//  |\     ^   |\
//  | \    |   | \
//  |  \   |   |  \
//  |   6------+---7
//  |   |  +-- |-- | -> x
//  0---+---\--1   |
//   \  |    \  \  |
//    \ |     \  \ |
//     \|      z  \|
//      4----------5
*/

const int XField3D_ForwardStep_Hex_X1::iXmin = 0;
const int XField3D_ForwardStep_Hex_X1::iXmax = 1;
const int XField3D_ForwardStep_Hex_X1::iYmin = 2;
const int XField3D_ForwardStep_Hex_X1::iYmax = 3;
const int XField3D_ForwardStep_Hex_X1::iZmin = 4;
const int XField3D_ForwardStep_Hex_X1::iZmax = 5;
//---------------------------------------------------------------------------//
XField3D_ForwardStep_Hex_X1::
XField3D_ForwardStep_Hex_X1( mpi::communicator comm,
                     const int ii, const int jj, const int kk,
                     Real xmin, Real xmax,
                     Real ymin, Real ymax,
                     Real zmin, Real zmax,
                     Real stepHeight, Real stepOffset ) : XField<PhysD3,TopoD3>(comm)
{
  SANS_ASSERT( ii > 0 && jj > 0 && kk > 0);
  //SANS_ASSERT_MSG( static_cast<int>(stepHeight/(ymax - ymin)*jj) == (stepHeight/(ymax - ymin)*jj), "Inputs wont form conforming mesh");
  //SANS_ASSERT_MSG( static_cast<int>(stepOffset/(xmax - xmin)*ii) == (stepOffset/(xmax - xmin)*ii), "Inputs wont form conforming mesh");

  XField_Lagrange<PhysD3> xfldin(comm);

  int order = 1;

  Real rLower = stepOffset/(xmax - xmin)*ii;
  int iLower = static_cast<int>(ceil(rLower));
  int jLower = stepHeight/(ymax - ymin)*jj;
  
  int iUpper = ii;
  int jUpper = jj - jLower;

  int nnode = (iUpper + 1) * (jUpper + 1) * (kk + 1) + (iLower+1) * (jLower) * (kk + 1);
  std::cout << "iLower: " << iLower << std::endl;
  std::cout << "jLower: " << jLower << std::endl;
  std::cout << "iUpper: " << iUpper << std::endl;
  std::cout << "jUpper: " << jUpper << std::endl;
  int nCell = iUpper * jUpper * kk + iLower * jLower * kk;
  std::cout << "nnode: " << nnode << std::endl;
  std::cout << "nCell: " << nCell << std::endl;

  // create the grid-coordinate DOF arrays
  xfldin.sizeDOF( nnode );

  // Add DOFs to rank 0
  if (comm.rank() == 0)
  {
    int counter = 0;
    Real x, y, z;
    for (int k = 0; k < kk + 1; k++)
    {
      for (int j = 0; j < jLower; j++)
      {
        for (int i = 0; i < iLower + 1; i++)
        {
          x = xmin + (xmax - xmin)*i/Real(ii);
          y = ymin + (ymax - ymin)*j/Real(jj);
          z = zmin + (zmax - zmin)*k/Real(kk);
          std:: cout << "x: " << x << " y: " << y << " z: " << z << std::endl;
          xfldin.addDOF( {x, y, z} );
          counter++;
        }
      }
      for (int j = jLower; j < jj + 1; j++)
      {
        for (int i = 0; i < ii + 1; i++)
        {
          x = xmin + (xmax - xmin)*i/Real(ii);
          y = ymin + (ymax - ymin)*j/Real(jj);
          z = zmin + (zmax - zmin)*k/Real(kk);
          std:: cout << "x: " << x << " y: " << y << " z: " << z << std::endl;
          xfldin.addDOF( {x, y, z} );
          counter++;
        }
      }
    }
    std::cout << "counter: " << counter << std::endl;
  }

  // Start the process of adding cells
  xfldin.sizeCells(nCell);

  // Add elements to rank 0
  if (comm.rank() == 0)
  {
    int group = 0;
    int counter = 0;
    int koffset = (iUpper + 1) * (jUpper + 1) + (iLower + 1) * (jLower);
    int n0 = 0;
    for (int k = 0; k < kk; k++)
    {
      int joffset = (iLower + 1);
      for (int j = 0; j < jLower; j++)
      {
        for (int i = 0; i < iLower; i++)
        {
          //All the nodes that make up an individial hex
          std::vector<int> hexnodes = { n0 + 0,
                                        n0 + 1,
                                        n0 + joffset + 1,
                                        n0 + joffset + 0,

                                        n0 + koffset + 0,
                                        n0 + koffset + 1,
                                        n0 + koffset + joffset + 1,
                                        n0 + koffset + joffset + 0 };

          // add the indices to the group
          //std::cout << "hexnodes1 : " << hexnodes << std::endl;
          xfldin.addCell(group, eHex, order, hexnodes);
          counter++;
          n0 += 1;
        }
        n0 += 1;
      }
      joffset = (ii + 1);
      for (int j = jLower; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          //All the nodes that make up an individial hex
          std::vector<int> hexnodes = { n0 + 0,
                                        n0 + 1,
                                        n0 + joffset + 1,
                                        n0 + joffset + 0,

                                        n0 + koffset + 0,
                                        n0 + koffset + 1,
                                        n0 + koffset + joffset + 1,
                                        n0 + koffset + joffset + 0 };

          // add the indices to the group
          //std::cout << "hexnodes2: " << hexnodes << std::endl;
          xfldin.addCell(group, eHex, order, hexnodes);counter++;
          n0 += 1;
        }
        n0 += 1;
      }
      n0 += ii+1;
    }
    std::cout << "counter: " << counter << std::endl;
  } // rank == 0


  // Start the process of adding boundary trace elements
  xfldin.sizeBoundaryTrace(2*iUpper*jUpper + 2*iLower*jLower + 2*jj*kk + 2*ii*kk);
  std::cout << "nTraces: " << 2*iUpper*jUpper + 2*iLower*jLower + 2*jj*kk + 2*ii*kk << std::endl;

  // Add elements to rank 0
  if (comm.rank() == 0)
  {
    const int (*TraceNodes)[Quad::NTrace] = TraceToCellRefCoord<Quad, TopoD3, Hex>::TraceNodes;

    int faceL;

    std::vector<int> faceNodes(Quad::NNode);

    // x-min boundary
    {
      int koffset = (iUpper + 1) * (jUpper + 1) + (iLower + 1) * (jLower);
      int n0 = 0;
      for (int k = 0; k < kk; k++)
      {
        int joffset = (iLower + 1);
        for (int j = 0; j < jLower; j++)
        {
          for (int i = 0; i < iLower; i++)
          {
            //All the nodes that make up an individial hex
            std::vector<int> hexnodes = { n0 + 0,
                                          n0 + 1,
                                          n0 + joffset + 1,
                                          n0 + joffset + 0,

                                          n0 + koffset + 0,
                                          n0 + koffset + 1,
                                          n0 + koffset + joffset + 1,
                                          n0 + koffset + joffset + 0 };

            if (i == 0)
            {
              // We're on the LHS
              int group = iXmin;
              // add the indices to the group
              faceL = 4; //Hex s-min
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }
            
            if (i == iLower - 1)
            {
              // We're on the RHS
              int group = iYmin;
              // add the indices to the group
              faceL = 2; //Hex s-max
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }
            
            if (j == 0)
            {
              // We're on the bottom
              int group = iYmin;
              // add the indices to the group
              faceL = 1; //Hex t-min
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }
            
            // This loop doesn't get to j-max
            
            if (k == 0)
            {
              // We're on the zMin
              int group = iZmin;
              // add the indices to the group
              faceL = 0; //Hex u-min
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }
            
            if (k == kk - 1)
            {
              // We're on the zMax
              int group = iZmax;
              // add the indices to the group
              faceL = 5; //Hex u-max
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }

            n0 += 1;
          }
          n0 += 1;
        }
        joffset = (ii + 1);
        for (int j = jLower; j < jj; j++)
        {
          for (int i = 0; i < ii; i++)
          {
            //All the nodes that make up an individial hex
            std::vector<int> hexnodes = { n0 + 0,
                                          n0 + 1,
                                          n0 + joffset + 1,
                                          n0 + joffset + 0,

                                          n0 + koffset + 0,
                                          n0 + koffset + 1,
                                          n0 + koffset + joffset + 1,
                                          n0 + koffset + joffset + 0 };


            if (i == 0)
            {
              // We're on the LHS
              int group = iXmin;
              // add the indices to the group
              faceL = 4; //Hex s-min
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }
            
            if (i == ii - 1)
            {
              // We're on the RHS
              int group = iXmax;
              // add the indices to the group
              faceL = 2; //Hex s-max
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }
            
            if (j == jLower && i >= iLower)
            {
              // We're on the bottom
              int group = iYmin;
              // add the indices to the group
              faceL = 1; //Hex t-min
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }
            
            if (j == jj - 1)
            {
              // We're on the top
              int group = iYmax;
              // add the indices to the group
              faceL = 3; //Hex t-min
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }
            
            if (k == 0)
            {
              // We're on the zMin
              int group = iZmin;
              // add the indices to the group
              faceL = 0; //Hex u-min
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }
            
            if (k == kk - 1)
            {
              // We're on the zMax
              int group = iZmax;
              // add the indices to the group
              faceL = 5; //Hex u-max
              for (int n = 0; n < Quad::NNode; n++)
                faceNodes[n] = hexnodes[ TraceNodes[faceL][n] ];

              //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
              xfldin.addBoundaryTrace( group, eQuad, faceNodes );
            }

            n0 += 1;
          }
          n0 += 1;
        }
        n0 += ii+1;
      }
    }
  }
  // Finalize the grid construction
  std::cout << "Constructing grid" << std::endl;
  this->buildFrom( xfldin );
  std::cout << "Constructed grid" << std::endl;
}

}
