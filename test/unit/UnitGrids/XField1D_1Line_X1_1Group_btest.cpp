// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField1D_1Line_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField1D_1Line_X1_1Group.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_1Line_X1_1Group_test )
{
  XField1D_1Line_X1_1Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 2 );

  BOOST_CHECK_THROW( xfld.nDOFCellGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFInteriorTraceGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFBoundaryTraceGroup(0), DeveloperException );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );

  // line field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField1D_1Line_X1_1Group::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField1D_1Line_X1_1Group::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField1D_1Line_X1_1Group::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(), +1 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
