// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <string>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_FlatPlate_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/output_Tecplot.h"
#include "Field/output_grm.h"
#include "Field/output_fluent.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_FlatPlate_X1_test )
{
//  int power = 0;

  // Check is called at the end of the constructor, hence this is actually tested

  mpi::communicator world;

  for (int i = 0; i < 1; i++)
  {
    XField2D_FlatPlate_X1 xfld1( world, i );

#if 0
      string filename = "tmp/flatplate";
      filename += to_string(world.rank());
      filename += "_X";
      filename += to_string(i);
      filename += ".plt";
      output_Tecplot(xfld1, filename);

//      filename += ".cas";
//      output_fluent(xfld1, filename);
//


//      filename += ".grm";
//      output_grm(xfld1, filename);
#endif
  }


}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
