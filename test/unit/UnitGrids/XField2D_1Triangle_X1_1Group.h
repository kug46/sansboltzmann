// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_1TRIANGLE_X1_1GROUP
#define XFIELD2D_1TRIANGLE_X1_1GROUP

#include "Field/XFieldArea.h"

namespace SANS
{
/*
   A unit grid that consists of one triangle within a single group

    2
    |\
    | \
    |  \
    |   \
    |    \
    | (0) \
    |      \
    0 ----- 1
*/

class XField2D_1Triangle_X1_1Group : public XField<PhysD2,TopoD2>
{
public:
  XField2D_1Triangle_X1_1Group();
};

}

#endif //XFIELD2D_1TRIANGLE_X1_1GROUP
