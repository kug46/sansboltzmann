// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <string>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/output_Tecplot.h"
//#include "Field/output_grm.h"
//#include "Field/output_fluent.h"
#include "XField2D_GilesPierceNozzle_HACK.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_GilesPierceNozzle_HACK_test )
{
//  int power = 0;

  // Check is called at the end of the constructor, hence this is actually tested

  mpi::communicator world;

  for (int i = 0; i < 1; i++)
  {
    Real ii = 10000;
    Real jj = 10;
    Real xmin = -1;
    Real xmax = 1;
    Real ymin = 0;
    Real ymax = 2;
    bool isCurved = true;
    XField2D_GilesPierceNozzle_HACK xfld1( world, ii, jj, xmin, xmax, ymin, ymax, isCurved );
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
