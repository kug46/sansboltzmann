// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD3D_8TET_X1_1GROUP
#define XFIELD3D_8TET_X1_1GROUP

#include "Field/XFieldVolume.h"

namespace SANS
{
// A unit grid that consists of eight tetrahedra within a single group,
// obtained by uniformly refining a single tet by splitting its edges.
//

class XField3D_8Tet_X1_1Group : public XField<PhysD3,TopoD3>
{
public:
  XField3D_8Tet_X1_1Group();
};

}

#endif //XFIELD3D_8TET_X1_1GROUP
