// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD4D_2PTOPE_X1_1GROUP_H
#define XFIELD4D_2PTOPE_X1_1GROUP_H

#include "Field/XFieldSpacetime.h"

namespace SANS
{
/*
 * A unit grid that consists of two pentatopes within a single group.
 *
 *          2
 *         /|\
 *        / | \
 *       / .|  \
 *      /   |   \
 *     /    |    \            ----> in another dimension: (4)
 *    /   . |     \                  (to the tune of the Wolfmother song)
 *   /      |      \                 and also to be shared by both elements
 *  5-------0-------1
 *   .   . /     .
 *    .   /   .
 *     . / .
 *      3
 *
 */


class XField4D_2Ptope_X1_1Group : public XField<PhysD4, TopoD4>
{
public:
  XField4D_2Ptope_X1_1Group();
};

}

#endif // XFIELD4D_2PTOPE_X1_1GROUP_H
