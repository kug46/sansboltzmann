// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField1D_2Line_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
/*
       (0)     (1)
    0 ----- 1 ----- 2
*/

XField1D_2Line_X1_1Group::XField1D_2Line_X1_1Group()
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(3);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(2);
  resizeCellGroups(1);

  // nodal coordinates for the two lines.
  DOF(0) = {0};
  DOF(1) = {1};
  DOF(2) = {2};

  // area field variable
  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, 2 );

  //element processor ranks
  fldAssocCell.setAssociativity( 0 ).setRank(0);
  fldAssocCell.setAssociativity( 1 ).setRank(0);

  //element cell associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
  fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( {1, 2} );

  cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // interior-trace field variable

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode( BasisFunctionNodeBase::P0, 1 );

  // node processor rank
  fldAssocInode.setAssociativity( 0 ).setRank( comm_rank );

  // node-element associativity
  fldAssocInode.setAssociativity( 0 ).setNodeGlobalMapping( {1} );
  fldAssocInode.setAssociativity( 0 ).setNormalSignL(  1 );
  fldAssocInode.setAssociativity( 0 ).setNormalSignR( -1 );

  // node-to-cell connectivity
  fldAssocInode.setGroupLeft( 0 );
  fldAssocInode.setGroupRight( 0 );
  fldAssocInode.setElementLeft( 0, 0 );
  fldAssocInode.setElementRight( 1, 0 );
  fldAssocInode.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );
  fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), 0 );

  FieldTraceGroupType<Line>* xfldInode = NULL;
  interiorTraceGroups_[0] = xfldInode = new FieldTraceGroupType<Line>( fldAssocInode );

  xfldInode->setDOF(DOF_, nDOF_);

  // boundary-trace field variable

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode0( BasisFunctionNodeBase::P0, 1 );
  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode1( BasisFunctionNodeBase::P0, 1 );

  // boundary node processor ranks
  fldAssocBnode0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBnode1.setAssociativity( 0 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
  fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {2} );

  fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
  fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

  // edge-to-cell connectivity
  fldAssocBnode0.setGroupLeft( 0 );
  fldAssocBnode0.setElementLeft( 0, 0 );
  fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

  fldAssocBnode1.setGroupLeft( 0 );
  fldAssocBnode1.setElementLeft( 1, 0 );
  fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
