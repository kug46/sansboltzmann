// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_2QUAD_X1_1GROUP
#define XFIELD2D_2QUAD_X1_1GROUP

#include "Field/XFieldArea.h"

namespace SANS
{
// A unit grid that consists of two quads within a single cell group, one interior trace group, and one boundary trace group
/*
  3-------2-------5
  |       |       |
  |  (0)  |  (1)  |
  |       |       |
  0 ------1-------4
*/
class XField2D_2Quad_X1_1Group : public XField<PhysD2,TopoD2>
{
public:
  XField2D_2Quad_X1_1Group();
};

}

#endif //XFIELD2D_2QUAD_X1_1GROUP
