// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField3D_1Tet_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
/*
        2
        |\
        | \
        |  \
        |   \
        |    \
        | (0) \
        |      \
        0 ----- 1
       /     .
      /   .
     / .
     3
 */

XField3D_1Tet_X1_1Group::XField3D_1Tet_X1_1Group()
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(4);

  //Create the element groups
  resizeInteriorTraceGroups(0);
  resizeBoundaryTraceGroups(4);
  resizeCellGroups(1);

  // nodal coordinates for the two triangles.
  DOF(0) = {0, 0, 0};
  DOF(1) = {1, 0, 0};
  DOF(2) = {0, 1, 0};
  DOF(3) = {0, 0, 1};

  // volume field variable
  FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1, 1 );

  // set the cell processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );

  //element volume associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2, 3} );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 0 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 1 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 2 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 3 );


  cellGroups_[0] = new FieldCellGroupType<Tet>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // interior-edge field variable

  // none

  // boundary-edge field variable

  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBface0( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );
  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBface1( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );
  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBface2( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );
  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBface3( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );

  // boundary face processor ranks
  fldAssocBface0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBface1.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBface2.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBface3.setAssociativity( 0 ).setRank( comm_rank );

  // face-element associativity
  fldAssocBface0.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2, 3} );
  fldAssocBface1.setAssociativity( 0 ).setNodeGlobalMapping( {0, 3, 2} );
  fldAssocBface2.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 3} );
  fldAssocBface3.setAssociativity( 0 ).setNodeGlobalMapping( {0, 2, 1} );

  // face-to-cell connectivity
  fldAssocBface0.setGroupLeft( 0 );
  fldAssocBface1.setGroupLeft( 0 );
  fldAssocBface2.setGroupLeft( 0 );
  fldAssocBface3.setGroupLeft( 0 );
  fldAssocBface0.setElementLeft( 0, 0 );
  fldAssocBface1.setElementLeft( 0, 0 );
  fldAssocBface2.setElementLeft( 0, 0 );
  fldAssocBface3.setElementLeft( 0, 0 );
  fldAssocBface0.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocBface1.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );
  fldAssocBface2.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
  fldAssocBface3.setCanonicalTraceLeft( CanonicalTraceToCell(3, 1), 0 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Triangle>( fldAssocBface0 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Triangle>( fldAssocBface1 );
  boundaryTraceGroups_[2] = new FieldTraceGroupType<Triangle>( fldAssocBface2 );
  boundaryTraceGroups_[3] = new FieldTraceGroupType<Triangle>( fldAssocBface3 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[3]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
