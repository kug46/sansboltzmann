// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD3D_CORNERFLATPLATE_X1
#define XFIELD3D_CORNERFLATPLATE_X1

#include "Field/XFieldVolume.h"

namespace SANS
{

class XField3D_CornerFlatPlate_X1 : public XField<PhysD3,TopoD3>
{
public:
  XField3D_CornerFlatPlate_X1( mpi::communicator& comm, const int power  );

//  static const int iSlipIn, iPlate, iSlipOut, iRight, iTop, iLeft, iFront, iBack;
  static const int iSlipIn, iPlate, iSlipOut, iRight, iTop, iLeft, iBack, iFrontSlipIn, iFrontPlate, iFrontSlipOut;
};


}

#endif // XFIELD3D_FLATPLATE_X1
