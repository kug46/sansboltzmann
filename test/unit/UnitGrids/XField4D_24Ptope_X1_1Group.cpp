// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField4D_24Ptope_X1_1Group.h"

#include "Field/Partition/XField_Lagrange.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
/*
 * A unit grid that consists of a 24 pentatopes arranged into a unit tessaract.
 *
 * <<salvador_dali.jpg>>
 *
 */

XField4D_24Ptope_X1_1Group::XField4D_24Ptope_X1_1Group()
{
  int comm_rank= comm_->rank();

  const int nDim= 4;
  const int nPentatope= 24;
  const int nVertex= 16;
  const int nBoundaries= 8;
  const int nBoundTet= 6;

  // these were the vertices that were
  // dumped out of XField_KuhnFreudenthal
  const int DOFarray[nVertex][nDim]=
      {{0, 0, 0, 0}, {0, 0, 0, 1}, {0, 0, 1, 0}, {0, 0, 1, 1},
       {0, 1, 0, 0}, {0, 1, 0, 1}, {0, 1, 1, 0}, {0, 1, 1, 1},
       {1, 0, 0, 0}, {1, 0, 0, 1}, {1, 0, 1, 0}, {1, 0, 1, 1},
       {1, 1, 0, 0}, {1, 1, 0, 1}, {1, 1, 1, 0}, {1, 1, 1, 1}};

  // these were the vertices that were
  // dumped out of XField_KuhnFreudenthal
  const int simplexVertices[nPentatope][nDim + 1]=
      {{0, 8, 10, 15, 14}, {0, 8, 10, 11, 15}, {0, 8, 9, 13, 15},
       {0, 8, 9, 15, 11}, {0, 2, 10, 14, 15}, {0, 2, 10, 15, 11},
       {0, 8, 12, 14, 15}, {0, 8, 12, 15, 13}, {0, 4, 12, 15, 14},
       {0, 4, 12, 13, 15}, {0, 4, 6, 14, 15}, {0, 4, 6, 15, 7},
       {0, 4, 5, 15, 13}, {0, 4, 5, 7, 15}, {0, 2, 6, 15, 14},
       {0, 2, 6, 7, 15}, {0, 2, 3, 11, 15}, {0, 2, 3, 15, 7},
       {0, 1, 9, 15, 13}, {0, 1, 9, 11, 15}, {0, 1, 5, 13, 15},
       {0, 1, 5, 15, 7}, {0, 1, 3, 15, 11}, {0, 1, 3, 7, 15}};

  // these were the boundary trace groups that were
  // dumped out of XField_KuhnFreudenthal
  const int boundaryTraceGroups[nBoundaries][nBoundTet][nDim]=
      {{{0, 4, 6, 7}, {0, 4, 7, 5}, {0, 2, 7, 6},
        {0, 2, 3, 7}, {0, 1, 5, 7}, {0, 1, 7, 3}},
       {{8, 10, 14, 15}, {8, 10, 15, 11}, {8, 9, 15, 13},
        {8, 9, 11, 15}, {8, 12, 15, 14}, {8, 12, 13, 15}},
       {{0, 8, 11, 10}, {0, 8, 9, 11}, {0, 2, 10, 11},
        {0, 2, 11, 3}, {0, 1, 11, 9}, {0, 1, 3, 11}},
       {{4, 12, 14, 15}, {4, 12, 15, 13}, {4, 6, 15, 14},
        {4, 6, 7, 15}, {4, 5, 13, 15}, {4, 5, 15, 7}},
       {{0, 8, 13, 9}, {0, 8, 12, 13}, {0, 4, 13, 12},
        {0, 4, 5, 13}, {0, 1, 9, 13}, {0, 1, 13, 5}},
       {{2, 10, 15, 14}, {2, 10, 11, 15}, {2, 6, 14, 15},
        {2, 6, 15, 7}, {2, 3, 15, 11}, {2, 3, 7, 15}},
       {{0, 8, 10, 14}, {0, 2, 14, 10}, {0, 8, 14, 12},
        {0, 4, 12, 14}, {0, 4, 14, 6}, {0, 2, 6, 14}},
       {{1, 9, 13, 15}, {1, 9, 15, 11}, {1, 5, 15, 13},
        {1, 5, 7, 15}, {1, 3, 11, 15}, {1, 3, 15, 7}}};

  // get XField_Lagrange object
  XField_Lagrange<PhysD4> xfld(*comm_);
  typename XField_Lagrange<PhysD4>::VectorX X;

  // size the vertices in the XField_Lagrange
  xfld.sizeDOF(nVertex);

  // add the DOFs to the XField_Lagrange
  if (comm_rank == 0)
  {
    for (int k= 0; k < 16; k++)
    {
      for (int d= 0; d < 4; d++)
        X[d]= DOFarray[k][d];

      xfld.addDOF(X);
    }
  }

  // mesh details
  int cellGroup= 0;
  int order= 1;

  // size the elements in the XField_Lagrange
  xfld.sizeCells(24);

  // add the 5-simplices to the XField_Lagrange
  if (comm_rank == 0)
  {
    std::vector<int> simplex(nDim + 1, 0);
    for (int k= 0; k < 24; k++)
    {
      for (int j= 0; j < nDim + 1; j++)
        simplex[j]= simplexVertices[k][j];

      xfld.addCell(cellGroup, ePentatope, order, simplex);
    }
  }

  // size the boundary traces facets in the XField_Lagrange
  xfld.sizeBoundaryTrace(nBoundaries*nBoundTet);

  int group= 0;

  // add the 4-simplices to the appropriate boundaries
  if (comm_rank == 0)
  {
    std::vector<int> facet(nDim);

    for (int boundary= 0; boundary < nBoundaries; boundary++)
    {
      for (int j= 0; j < nBoundTet; j++)
      {
        for (int i= 0; i < nDim; i++)
          facet[i]= boundaryTraceGroups[boundary][j][i];

        xfld.addBoundaryTrace(group, eTet, facet);
      }
    }

  }

  // finally, construct the XField4D from the XField_Lagrange
  this->buildFrom(xfld);

  // check that the grid is correct
  checkGrid();

}

}
