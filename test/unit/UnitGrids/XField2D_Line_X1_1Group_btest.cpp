// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// XField2D_Line_X1_1Group_btest.cpp

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Field/Element/TraceUnitNormal.h"

#include "XField2D_Line_X1_1Group.h"

//#include "Field/output_Tecplot.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_2Line_X1_1Group_test )
{
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldCell;
  typedef ElementXField<PhysD2, TopoD0, Node> ElementXFieldTrace;
  typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
  typedef typename ElementXFieldCell::RefCoordType RefCoordCellType;
  typedef DLA::VectorS<2,Real> VectorX;

  const Real tol = 1.e-13;

  // ---------- Set up a 2-element line grid ----------
  const int ii = 2;  // number of elements
  std::vector<DLA::VectorS<2,Real>> coordinates(ii+1);
  coordinates[0] = {0, 0};
  coordinates[1] = {1, 0};
  coordinates[2] = {1, 1};

  XField2D_Line_X1_1Group xfld(coordinates);

  BOOST_CHECK_EQUAL( xfld.nDOF(), ii+1 );

  // ---------- Check node values ----------
  for ( int i = 0; i < ii+1; i++)
  {
    BOOST_CHECK_CLOSE( xfld.DOF(i)[0], coordinates[i][0], tol );
    BOOST_CHECK_CLOSE( xfld.DOF(i)[1], coordinates[i][1], tol );
  }

  // ---------- Check line field variable ----------
  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField2D_Line_X1_1Group::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2]; // container for node map of an element

  VectorX Xs0, Xs1;

  // element 1
  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  ElementXFieldCell xElem0(1, BasisFunctionCategory_Hierarchical);
  xfldCell.getElement( xElem0, 0 );
  xElem0.unitTangent( 0.5, Xs0 );

  SANS_CHECK_CLOSE( 1, Xs0[0], tol, tol);
  SANS_CHECK_CLOSE( 0, Xs0[1], tol, tol);

  // element 2
  xfldCell.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  ElementXFieldCell xElem1(1, BasisFunctionCategory_Hierarchical);
  xfldCell.getElement( xElem1, 1 );
  xElem1.unitTangent( 0.5, Xs1 );

  SANS_CHECK_CLOSE( 0, Xs1[0], tol, tol);
  SANS_CHECK_CLOSE( 1, Xs1[1], tol, tol);

  // ---------- Check interior-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Node) );

  const XField2D_Line_X1_1Group::FieldTraceGroupType<Node>& xfldInode = xfld.getInteriorTraceGroup<Node>(0);

  // interior trace 1
  // node map
  xfldInode.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  // normals
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignR(), -1 );

  // interior trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(0).trace, 1 );

  ElementXFieldTrace xTraceI0(0,BasisFunctionCategory_Legendre);
  xfldInode.getElement( xTraceI0, 0 );
  const CanonicalTraceToCell canonicalTraceL = xfldInode.getCanonicalTraceLeft(0);
  const CanonicalTraceToCell canonicalTraceR = xfldInode.getCanonicalTraceRight(0);

  RefCoordTraceType sRefTrace = Node::centerRef;
  RefCoordCellType sRefL, sRefR;

  VectorX N, NL, NR;
  VectorX e0L, e0R;

  TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalTraceL, sRefTrace, sRefL );
  TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalTraceR, sRefTrace, sRefR );

  SANS_CHECK_CLOSE( 0, sRefTrace[0], tol, tol);
  SANS_CHECK_CLOSE( 1, sRefL[0], tol, tol);
  SANS_CHECK_CLOSE( 0, sRefR[0], tol, tol);

  VectorX XTraceI;
  xTraceI0.coordinates(sRefTrace,XTraceI);
  SANS_CHECK_CLOSE( 1, XTraceI[0], tol, tol);
  SANS_CHECK_CLOSE( 0, XTraceI[1], tol, tol);

  xElem0.unitTangent( sRefL, e0L );
  xElem1.unitTangent( sRefR, e0R );

  SANS_CHECK_CLOSE( 1, e0L[0], tol, tol);
  SANS_CHECK_CLOSE( 0, e0L[1], tol, tol);
  SANS_CHECK_CLOSE( 0, e0R[0], tol, tol);
  SANS_CHECK_CLOSE( 1, e0R[1], tol, tol);

  SANS_CHECK_CLOSE(  1, xTraceI0.conormalSignL(sRefTrace), tol, tol);
  SANS_CHECK_CLOSE( -1, xTraceI0.conormalSignR(sRefTrace), tol, tol);

  traceUnitNormal( xTraceI0, sRefTrace, xElem0, sRefL, xElem1, sRefR, N);

  SANS_CHECK_CLOSE( sqrt(0.5), N[0], tol, tol);
  SANS_CHECK_CLOSE( sqrt(0.5), N[1], tol, tol);

  // ---------- Check boundary-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField2D_Line_X1_1Group::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField2D_Line_X1_1Group::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  // boundary trace 1
  // node map
  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(),  0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  // boundary trace 2
  // node map
  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(), 0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );

//  output_Tecplot( xfld, "tmp/test.dat" );  // used for testing output_tecplot for 2D manifolds
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Line_X1_1Group_NACA0004TxtInput_test )
{
  // ----- check grid input ----- //
  const std::string filename = "UnitGrids/IO/NACA0004.txt"; // exported from XFoil (without label)
  const int nnodeTrue = 160;  // true number of nodes

  std::vector<DLA::VectorS<2,Real>> coordinates;

  XField2D_Line_X1_1Group::readXFoilGrid(filename, coordinates);

  // ---------- Check XFoilGrid reader ----------
  // 1st node
  BOOST_CHECK_EQUAL( coordinates[0][0], 1. );
  BOOST_CHECK_EQUAL( coordinates[0][1],  .000420 );
  // 9th node
  BOOST_CHECK_EQUAL( coordinates[8][0], 0.902135 );
  BOOST_CHECK_EQUAL( coordinates[8][1], 0.004737 );
  // 160th node
  BOOST_CHECK_EQUAL( coordinates[159][0], 1. );
  BOOST_CHECK_EQUAL( coordinates[159][1], -.000420 );

  // ----- check XField ----- //
  XField2D_Line_X1_1Group xfld(coordinates);

  // ---------- Check node values ----------
  BOOST_CHECK_EQUAL( xfld.nDOF(), nnodeTrue );

  // 1st node
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 1. );
  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  .000420 );
  // 9th node
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0], 0.902135 );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[1], 0.004737 );
  // 160th node
  BOOST_CHECK_EQUAL( xfld.DOF(159)[0], 1. );
  BOOST_CHECK_EQUAL( xfld.DOF(159)[1], -.000420 );


  // ---------- Check line field variable ----------
  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField2D_Line_X1_1Group::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2]; // container for node map of an element

  // element 1
  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  // element 158
  xfldCell.associativity(158).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 158 );
  BOOST_CHECK_EQUAL( nodeMap[1], 159 );

  // ---------- Check interior-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Node) );

  const XField2D_Line_X1_1Group::FieldTraceGroupType<Node>& xfldInode = xfld.getInteriorTraceGroup<Node>(0);

  // interior trace 1
  // node map
  xfldInode.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  // normals
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignR(), -1 );

  // interior trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(0).trace, 1 );

  // interior trace 158
  // node map
  xfldInode.associativity(157).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 158 );

  // normals
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignR(), -1 );

  // interior trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementLeft( 157 ), 157 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight( 157 ), 158 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft( 157 ).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight( 157 ).trace, 1 );

  // ---------- Check boundary-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );

  // boundary trace group 1
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );

  const XField2D_Line_X1_1Group::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);

  // node map
  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(),  0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  // boundary trace group 2
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );
  const XField2D_Line_X1_1Group::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  // node map
  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 159 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(), 1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(), 0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 158 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_4Line_X1_1Group_unstructured_test )
{
  typedef ElementXField<PhysD2, TopoD1, Line> ElementXFieldCell;
  typedef ElementXField<PhysD2, TopoD0, Node> ElementXFieldTrace;
  typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
  typedef typename ElementXFieldCell::RefCoordType RefCoordCellType;
  typedef DLA::VectorS<2,Real> VectorX;

  const Real tol = 1.e-13;

  // ---------- Set up a 2-element line grid ----------
  const int ii = 4;  // number of elements
  std::vector<DLA::VectorS<2,Real>> coordinates(ii+1);
  coordinates[0] = {0, 0};
  coordinates[1] = {1, 0};
  coordinates[2] = {1, 1};
  coordinates[3] = {2, 1};
  coordinates[4] = {2, 2};

  XField2D_Line_X1_1Group_unstructured xfld(coordinates);

  BOOST_CHECK_EQUAL( xfld.nDOF(), ii+1 );

  // ---------- Check node values ----------
  for ( int i = 0; i < ii+1; i++)
  {
    BOOST_CHECK_CLOSE( xfld.DOF(i)[0], coordinates[i][0], tol );
    BOOST_CHECK_CLOSE( xfld.DOF(i)[1], coordinates[i][1], tol );
  }

  // ---------- Check line field variable ----------
  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField2D_Line_X1_1Group_unstructured::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2]; // container for node map of an element

  VectorX Xs0, Xs1, Xs2, Xs3;

  // element 1
  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  ElementXFieldCell xElem0(1, BasisFunctionCategory_Hierarchical);
  xfldCell.getElement( xElem0, 0 );
  xElem0.unitTangent( 0.5, Xs0 );

  SANS_CHECK_CLOSE( 1, Xs0[0], tol, tol);
  SANS_CHECK_CLOSE( 0, Xs0[1], tol, tol);

  // element 2
  xfldCell.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  ElementXFieldCell xElem1(1, BasisFunctionCategory_Hierarchical);
  xfldCell.getElement( xElem1, 1 );
  xElem1.unitTangent( 0.5, Xs1 );

  SANS_CHECK_CLOSE( 0, Xs1[0], tol, tol);
  SANS_CHECK_CLOSE( -1, Xs1[1], tol, tol);

  // element 3
  xfldCell.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  ElementXFieldCell xElem2(1, BasisFunctionCategory_Hierarchical);
  xfldCell.getElement( xElem2, 2 );
  xElem2.unitTangent( 0.5, Xs2 );

  SANS_CHECK_CLOSE( 1, Xs2[0], tol, tol);
  SANS_CHECK_CLOSE( 0, Xs2[1], tol, tol);

  // element 4
  xfldCell.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  ElementXFieldCell xElem3(1, BasisFunctionCategory_Hierarchical);
  xfldCell.getElement( xElem3, 3 );
  xElem3.unitTangent( 0.5, Xs3 );

  SANS_CHECK_CLOSE( 0, Xs3[0], tol, tol);
  SANS_CHECK_CLOSE( -1, Xs3[1], tol, tol);

  // ---------- Check interior-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Node) );

  const XField2D_Line_X1_1Group_unstructured::FieldTraceGroupType<Node>& xfldInode = xfld.getInteriorTraceGroup<Node>(0);

  // interior trace 1
  // node map
  xfldInode.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  // normals
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignL(), 1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignR(), 1 );

  // interior trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(0).trace, 0 );

  ElementXFieldTrace xTraceI0(0,BasisFunctionCategory_Legendre);
  xfldInode.getElement( xTraceI0, 0 );
  CanonicalTraceToCell canonicalTraceL = xfldInode.getCanonicalTraceLeft(0);
  CanonicalTraceToCell canonicalTraceR = xfldInode.getCanonicalTraceRight(0);

  RefCoordTraceType sRefTrace = Node::centerRef;
  RefCoordCellType sRefL, sRefR;

  VectorX N, NL, NR;
  VectorX e0L, e0R;

  TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalTraceL, sRefTrace, sRefL );
  TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalTraceR, sRefTrace, sRefR );

  SANS_CHECK_CLOSE( 0, sRefTrace[0], tol, tol);
  SANS_CHECK_CLOSE( 1, sRefL[0], tol, tol);
  SANS_CHECK_CLOSE( 1, sRefR[0], tol, tol);

  xElem0.unitTangent( sRefL, e0L );
  xElem1.unitTangent( sRefR, e0R );

  SANS_CHECK_CLOSE( 1, e0L[0], tol, tol);
  SANS_CHECK_CLOSE( 0, e0L[1], tol, tol);
  SANS_CHECK_CLOSE( 0, e0R[0], tol, tol);
  SANS_CHECK_CLOSE( -1, e0R[1], tol, tol);

  SANS_CHECK_CLOSE( 1, xTraceI0.conormalSignL(sRefTrace), tol, tol);
  SANS_CHECK_CLOSE( 1, xTraceI0.conormalSignR(sRefTrace), tol, tol);

  traceUnitNormal( xTraceI0, sRefTrace, xElem0, sRefL, xElem1, sRefR, N);

  SANS_CHECK_CLOSE( sqrt(0.5), N[0], tol, tol);
  SANS_CHECK_CLOSE( sqrt(0.5), N[1], tol, tol);


  // interior trace 2
  // node map
  xfldInode.associativity(1).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  // normals
  BOOST_CHECK_EQUAL( xfldInode.associativity(1).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(1).normalSignR(), -1 );

  // interior trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(1), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(1), 2 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(1).trace, 1 );

  ElementXFieldTrace xTraceI1(0,BasisFunctionCategory_Legendre);
  xfldInode.getElement( xTraceI1, 1 );
  canonicalTraceL = xfldInode.getCanonicalTraceLeft(1);
  canonicalTraceR = xfldInode.getCanonicalTraceRight(1);

  TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalTraceL, sRefTrace, sRefL );
  TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalTraceR, sRefTrace, sRefR );

  SANS_CHECK_CLOSE( 0, sRefTrace[0], tol, tol);
  SANS_CHECK_CLOSE( 0, sRefL[0], tol, tol);
  SANS_CHECK_CLOSE( 0, sRefR[0], tol, tol);

  xElem1.unitTangent( sRefL, e0L );
  xElem2.unitTangent( sRefR, e0R );

  SANS_CHECK_CLOSE( 0, e0L[0], tol, tol);
  SANS_CHECK_CLOSE( -1, e0L[1], tol, tol);
  SANS_CHECK_CLOSE( 1, e0R[0], tol, tol);
  SANS_CHECK_CLOSE( 0, e0R[1], tol, tol);

  SANS_CHECK_CLOSE( -1, xTraceI1.conormalSignL(sRefTrace), tol, tol);
  SANS_CHECK_CLOSE( -1, xTraceI1.conormalSignR(sRefTrace), tol, tol);

  traceUnitNormal( xTraceI0, sRefTrace, xElem0, sRefL, xElem1, sRefR, N);

  SANS_CHECK_CLOSE( sqrt(0.5), N[0], tol, tol);
  SANS_CHECK_CLOSE( sqrt(0.5), N[1], tol, tol);

  // ---------- Check boundary-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Node) );

  const XField2D_Line_X1_1Group_unstructured::FieldTraceGroupType<Node>& xfldBnode0 = xfld.getBoundaryTraceGroup<Node>(0);
  const XField2D_Line_X1_1Group_unstructured::FieldTraceGroupType<Node>& xfldBnode1 = xfld.getBoundaryTraceGroup<Node>(1);

  // boundary trace 1
  // node map
  xfldBnode0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode0.associativity(0).normalSignR(),  0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBnode0.getCanonicalTraceLeft(0).trace, 1 );

  // boundary trace 2
  // node map
  xfldBnode1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );

  // normals
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignL(), -1 );
  BOOST_CHECK_EQUAL( xfldBnode1.associativity(0).normalSignR(),  0 );

  // boundary trace-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBnode1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBnode1.getElementLeft(0), 3 );
  BOOST_CHECK_EQUAL( xfldBnode1.getCanonicalTraceLeft(0).trace, 1 );

//  output_Tecplot( xfld, "tmp/XField2D_Line_X1_1Group_unstructured.dat" );  // used for testing output_tecplot for 2D manifolds
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
