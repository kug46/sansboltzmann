// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_1Line_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
/*
       (0)
    0 ----- 1
*/

XField2D_1Line_X1_1Group::XField2D_1Line_X1_1Group( VectorX X1, VectorX X2 )
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(2);

  //Create the element groups
  resizeInteriorTraceGroups(0);
  resizeBoundaryTraceGroups(2);
  resizeCellGroups(1);

  // nodal coordinates for one line
  DOF(0) = X1;
  DOF(1) = X2;

  // line element field variable
  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, 1 );

  //set the processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );

  //element cell associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );

  cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );

  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // interior-trace field variable

  // none

  // boundary-trace field variable

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode0( BasisFunctionNodeBase::P0, 1 );
  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode1( BasisFunctionNodeBase::P0, 1 );

  // set the boundary node processor rank
  fldAssocBnode0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBnode1.setAssociativity( 0 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
  fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {1} );

  fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
  fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

  // edge-to-cell connectivity
  fldAssocBnode0.setGroupLeft( 0 );
  fldAssocBnode0.setElementLeft( 0, 0 );
  fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

  fldAssocBnode1.setGroupLeft( 0 );
  fldAssocBnode1.setElementLeft( 0, 0 );
  fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
