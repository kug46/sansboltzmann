// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <fstream>
#include <iomanip>

#include "XField2D_Line_Xq_1Group.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

namespace SANS
{

// -----------------------------------------------------
XField2D_Line_Xq_1Group::
XField2D_Line_Xq_1Group( const XField2D_Line_X1_1Group& xfld, const int order,
                         const ArrayVector& coordinates )
    : XField<PhysD2,TopoD1>( xfld, order )
{
  SANS_ASSERT_MSG( (order > 1), "Geometric order Q must be greater than 1." );
  SANS_ASSERT_MSG( (coordinates.size() == static_cast<unsigned int>(nNode(order, xfld.nElem()))),
                   "The number of nodes in input coordinates does not match what is required." );

  typedef FieldCellGroupType<Line> FieldCellGroupTypeClass;
  typedef FieldCellGroupTypeClass::template ElementType<> ElementType;
  typedef DLA::VectorS<2, Real> VectorX;

  FieldCellGroupTypeClass& xfldCell = this->getCellGroup<Line>(0);
  ElementType xfldElem( xfldCell.basis() );

  SANS_ASSERT_MSG( (xfldElem.nDOF() == order+1), "The number of DOFs per element does not match (order+1)." );

  const int nDOFelem = xfldElem.nDOF();    // number of total DOFs per element

  DLA::VectorD<Real> sref(nDOFelem);          // element reference coordinates
  DLA::VectorD<Real> bx(nDOFelem);
  DLA::VectorD<Real> by(nDOFelem);            // Cartesian coordinates evaluated at sref
  DLA::VectorD<Real> cx(nDOFelem);
  DLA::VectorD<Real> cy(nDOFelem);            // grid element field DOFs
  DLA::MatrixD<Real> A(nDOFelem,nDOFelem);    // A*cx = bx, A*cy = by
  DLA::VectorD<Real> phi(nDOFelem);           // each basis function evaluated at a point
  VectorX X;                                  // to store (x,y) coordinates

  // reference coordinates sref: equally spaced from 0 to 1
  for ( int i = 0; i < nDOFelem; i++ )
    sref[i] = static_cast<Real>(i) / static_cast<Real>(order);

  // construct Vandermonde matrix
  for ( int i = 0; i < nDOFelem; i++ )    // loop over rows <--> sref coordinates
  {
    xfldElem.basis()->evalBasis(sref[i], phi.data(), phi.size());
    for ( int j = 0; j < nDOFelem; j++ )  // loop over columns <--> DOFs/basis fcns
      A(i,j) = phi[j];
  }
  // invert matrix (with column pivoting)
  DLA::MatrixD<Real> Ainv = DLA::InverseLUP::Inverse(A);

  // solve for nodal DOFs by looping over line elements
  for (int elem = 0; elem < xfldCell.nElem(); elem++)
  {
    xfldCell.getElement( xfldElem, elem );

    bx = 0; by = 0; cx = 0; cy = 0;

    // assign b with input coordinates
    for (int i = 0; i < nDOFelem; i++)
    {
      bx[i] = coordinates[elem*order+i][0];
      by[i] = coordinates[elem*order+i][1];
    }

    // solve for DOFs
    cx = Ainv*bx;
    cy = Ainv*by;

    // assign DOFs
    for ( int j = 0; j < nDOFelem; j++)
    {
      xfldElem.DOF(j)(0) = cx[j];
      xfldElem.DOF(j)(1) = cy[j];
    }

    xfldCell.setElement( xfldElem, elem );

#if 0 // print coordinates
    std::cout << std::setprecision(6) << std::scientific;
    std::cout << "Elem = " << elem << std::endl;
    for (int i = 0; i < nDOFelem; i++)
    {
      X = xfldElem.eval(sref[i]);
      std::cout << X[0] << " " << X[1] << " VS "
          << coordinates[elem*order+i][0] << " " << coordinates[elem*order+i][1] << " VS "
          << bx[i] << " " << by[i] << std::endl;
    }

    if (i==(nDOFelem)) // reach the last grid element
      std::cout << std::endl;
#endif
  }

  //Check that the grid is correct
  checkGrid();
}

// -----------------------------------------------------
XField2D_Line_Xq_1Group_unstructured::
XField2D_Line_Xq_1Group_unstructured( const XField2D_Line_X1_1Group_unstructured& xfld,
                                      const int order,
                                      const ArrayVector& coordinates )
    : XField<PhysD2,TopoD1>( xfld, order )
{
  SANS_ASSERT_MSG( (order > 1), "Geometric order Q must be greater than 1." );
  SANS_ASSERT_MSG( (coordinates.size() == static_cast<unsigned int>(nNode(order, xfld.nElem()))),
                   "The number of nodes in input coordinates does not match what is required." );

  typedef FieldCellGroupType<Line> FieldCellGroupTypeClass;
  typedef FieldCellGroupTypeClass::template ElementType<> ElementType;
  typedef DLA::VectorS<2, Real> VectorX;

  FieldCellGroupTypeClass& xfldCell = this->getCellGroup<Line>(0);
  ElementType xfldElem( xfldCell.basis() );

  SANS_ASSERT_MSG( (xfldElem.nDOF() == order+1), "The number of DOFs per element does not match (order+1)." );
  const int nDOFelem = xfldElem.nDOF();    // number of total DOFs per element

  DLA::VectorD<Real> sref(nDOFelem);          // element reference coordinates
  DLA::VectorD<Real> bx(nDOFelem);
  DLA::VectorD<Real> by(nDOFelem);            // nodal coordinates evaluated at sref
  DLA::VectorD<Real> cx(nDOFelem);
  DLA::VectorD<Real> cy(nDOFelem);            // grid element field DOFs
  DLA::MatrixD<Real> A1(nDOFelem,nDOFelem);   // A*cx = bx, A*cy = by
  DLA::MatrixD<Real> A2(nDOFelem,nDOFelem);   // A*cx = bx, A*cy = by
  DLA::VectorD<Real> phi(nDOFelem);           // each basis function evaluated at a point
  VectorX X;                                  // to store (x,y) coordinates

  // reference coordinates sref: equally spaced from 0 to 1
  for ( int i = 0; i < nDOFelem; i++ )
    sref[i] = static_cast<Real>(i) / static_cast<Real>(order);

  // construct Vandermonde matrices: A1 and A2 correspond to different sRef directions
  for ( int i = 0; i < nDOFelem; i++ )    // loop over rows <--> sref coordinates
  {
    xfldElem.basis()->evalBasis(sref[i], phi.data(), phi.size());
    for ( int j = 0; j < nDOFelem; j++ )  // loop over columns <--> DOFs/basis fcns
      A1(i,j) = phi[j];
  }

  for ( int i = 0; i < nDOFelem; i++ )    // loop over rows <--> sref coordinates
  {
    for ( int j = 0; j < nDOFelem; j++ )  // loop over columns <--> DOFs/basis fcns
      A2(i,j) = A1(nDOFelem-1-i,j);       // flipping A1 upside down gives A2
  }

  // invert matrix (with column pivoting)
  DLA::MatrixD<Real> A1inv = DLA::InverseLUP::Inverse(A1);
  DLA::MatrixD<Real> A2inv = DLA::InverseLUP::Inverse(A2);

  // solve for nodal DOFs by looping over line elements
  for (int elem = 0; elem < xfldCell.nElem(); elem++)
  {

    DLA::MatrixD<Real> Ainv(nDOFelem,nDOFelem);
    if ( (elem % 2) == 0 )
      Ainv = A1inv;
    else
      Ainv = A2inv;

    xfldCell.getElement( xfldElem, elem );

    bx = 0; by = 0; cx = 0; cy = 0;

    // assign b with input coordinates
//    for (int i = 1; i < order; i++)
    for (int i = 0; i < nDOFelem; i++)
    {
      bx[i] = coordinates[elem*order+i][0];
      by[i] = coordinates[elem*order+i][1];
    }

    // solve for DOFs
    cx = Ainv*bx;
    cy = Ainv*by;

    // assign DOFs
    for ( int j = 0; j < nDOFelem; j++)
    {
      xfldElem.DOF(j)(0) = cx[j];
      xfldElem.DOF(j)(1) = cy[j];
    }

    xfldCell.setElement( xfldElem, elem );

    // print coordinates
#if 0
    for (int i = 0; i < nDOFelem; i++)
    {
      X = xfldElem.eval(sref[i]);
      std::cout << "sRef = " << sref[i] << " : " << X[0] << " " << X[1] << " VS "
          << coordinates[elem*order+i][0] << " " << coordinates[elem*order+i][1] << " VS "
          << bx[i] << " " << by[i] << std::endl;
    }
#endif

  }

  //Check that the grid is correct
  checkGrid();
}

}  // namespace SANS
