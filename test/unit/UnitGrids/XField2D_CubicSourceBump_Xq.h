// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_CUBICSOURCEBUMP_XQ
#define XFIELD2D_CUBICSOURCEBUMP_XQ

#include "XField2D_Box_UnionJack_Triangle_X1.h"

namespace SANS
{

// cubic-source bump channel flow
// triangle grid in a 3x1 channel with 4 sides as separate boundary-edge groups
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// area elements in 1 group
// interior-edge elements in 3 groups: horizontal, vertical, diagonal
// boundary-edge elements in 4 groups: lower, right, upper, left
//
// all edges are straight (X1)

class XField2D_CubicSourceBump_Xq : public XField2D_Box_UnionJack_Triangle_X1
{
public:
  XField2D_CubicSourceBump_Xq( const int ii, const int jj, Real tau );
  XField2D_CubicSourceBump_Xq( const int ii, const int jj, Real tau, const int order );

protected:
  void init_X1(const int ii, const int jj, Real tau);
};


}

#endif // XFIELD2D_CUBICSOURCEBUMP_XQ
