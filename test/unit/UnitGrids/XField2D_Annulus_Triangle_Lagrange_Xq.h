// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_ANNULUS_TRIANGLE_LAGRANGE_XQ
#define XFIELD2D_ANNULUS_TRIANGLE_LAGRANGE_XQ

#include <vector>
#include <functional>

#include "XField2D_Box_Triangle_Lagrange_X1.h"

namespace SANS
{
// triangle grid in a unit box with 4 sides as separate boundary-edge groups
//
// generates grid with ii x jj (quad) elements, split into 2*ii*jj triangles;
// area elements in 1 group
// boundary-edge elements in 4 groups: lower, right, upper, left
//
class XField2D_Annulus_Triangle_Lagrange_Xq : public XField2D_Box_Triangle_Lagrange_X1
{
public:
  XField2D_Annulus_Triangle_Lagrange_Xq( mpi::communicator comm, int ii, int jj, int order, Real thetamin, Real thetamax,
          std::function<Real(Real)> Rlower, std::function<Real(Real)> Rupper);
};

}

#endif // XFIELD2D_ANNULUS_TRIANGLE_LAGRANGE_XQ
