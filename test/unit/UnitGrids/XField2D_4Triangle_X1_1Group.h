// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_4TRIANGLE_X1_1GROUP
#define XFIELD2D_4TRIANGLE_X1_1GROUP

#include "Field/XFieldArea.h"

namespace SANS
{
// A unit grid that consists of four triangles within a single group
//
// 1st triangle is surrounded by 3 triangles; this can be used for testing
// interior operators
//
//  4 ----- 2 ----- 3
//   \      |\      |
//    \ (2) | \ (1) |
//     \    |  \    |
//      \   |   \   |
//       \  |    \  |
//        \ | (0) \ |
//         \|      \|
//          0 ----- 1
//           \      |
//            \ (3) |
//             \    |
//              \   |
//               \  |
//                \ |
//                  5

class XField2D_4Triangle_X1_1Group : public XField<PhysD2,TopoD2>
{
public:
  explicit XField2D_4Triangle_X1_1Group(BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical);
};

}

#endif //XFIELD2D_4TRIANGLE_X1_1GROUP
