// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/linspace.h"

#include "Field/output_Tecplot.h"

#include "XField4D_Box_Ptope_X1.h"

//#include "unit/Field/XField4D_CheckTraceCoord3D_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField4D_Box_Ptope_X1_static_test )
{
  BOOST_CHECK_EQUAL(XField4D_Box_Ptope_X1::iXmin, 3);
  BOOST_CHECK_EQUAL(XField4D_Box_Ptope_X1::iXmax, 4);
  BOOST_CHECK_EQUAL(XField4D_Box_Ptope_X1::iYmin, 2);
  BOOST_CHECK_EQUAL(XField4D_Box_Ptope_X1::iYmax, 5);
  BOOST_CHECK_EQUAL(XField4D_Box_Ptope_X1::iZmin, 0);
  BOOST_CHECK_EQUAL(XField4D_Box_Ptope_X1::iZmax, 7);
  BOOST_CHECK_EQUAL(XField4D_Box_Ptope_X1::iWmin, 1);
  BOOST_CHECK_EQUAL(XField4D_Box_Ptope_X1::iWmax, 6);
}

#ifdef SANS_AVRO

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField4D_Box_Ptope_X1_test1 )
{
  // tests trivial single element XField4D

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm= world.split(world.rank());

  const int ii= 1;
  const int jj= 1;
  const int kk= 1;
  const int mm= 1;

  // simply creating the grid triggers connectivity and positive volume checks
  XField4D_Box_Ptope_X1 xfld(comm, ii, jj, kk, mm);

  BOOST_CHECK_EQUAL((ii + 1)*(jj + 1)*(kk + 1)*(mm + 1), xfld.nDOF());
  BOOST_CHECK_EQUAL(24*ii*jj*kk*mm, xfld.nElem());

  BOOST_CHECK_THROW(xfld.nDOFCellGroup(0), DeveloperException);
  BOOST_CHECK_THROW(xfld.nDOFInteriorTraceGroup(0), DeveloperException);
  BOOST_CHECK_THROW(xfld.nDOFBoundaryTraceGroup(0), DeveloperException);

  // volume field variable

  BOOST_CHECK_EQUAL(1, xfld.nCellGroups());
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  // interior-face field variable

  BOOST_CHECK_EQUAL(1, xfld.nInteriorTraceGroups());
  for (int n= 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // boundary-face field variable

  BOOST_CHECK_EQUAL(8, xfld.nBoundaryTraceGroups());
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Tet));
}

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( XField4D_Box_Tet_X1_test2 )
{
  // tests two element XField4D with two elements in x direction

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm= world.split(world.rank());

  const int ii= 2;
  const int jj= 1;
  const int kk= 1;
  const int mm= 1;

  // simply creating the grid triggers connectivity and positive volume checks
  XField4D_Box_Ptope_X1 xfld(comm, ii, jj, kk, mm);

  BOOST_CHECK_EQUAL((ii + 1)*(jj + 1)*(kk + 1)*(mm + 1), xfld.nDOF());
  BOOST_CHECK_EQUAL(24*ii*jj*kk*mm, xfld.nElem());

  // volume field variable

  BOOST_CHECK_EQUAL(1, xfld.nCellGroups());
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  // interior-face field variable

  BOOST_CHECK_EQUAL(1, xfld.nInteriorTraceGroups());
  for (int n= 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // boundary-face field variable

  BOOST_CHECK_EQUAL(8, xfld.nBoundaryTraceGroups());
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Tet));
}

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( XField4D_Box_Tet_X1_test3 )
{
  // tests two element XField4D with two elements in y direction

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm= world.split(world.rank());

  const int ii= 1;
  const int jj= 2;
  const int kk= 1;
  const int mm= 1;

  // simply creating the grid triggers connectivity and positive volume checks
  XField4D_Box_Ptope_X1 xfld(comm, ii, jj, kk, mm);

  BOOST_CHECK_EQUAL((ii + 1)*(jj + 1)*(kk + 1)*(mm + 1), xfld.nDOF());
  BOOST_CHECK_EQUAL(24*ii*jj*kk*mm, xfld.nElem());

  // volume field variable

  BOOST_CHECK_EQUAL(1, xfld.nCellGroups());
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  // interior-face field variable

  BOOST_CHECK_EQUAL(1, xfld.nInteriorTraceGroups());
  for (int n= 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // boundary-face field variable

  BOOST_CHECK_EQUAL(8, xfld.nBoundaryTraceGroups());
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Tet));
}

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( XField4D_Box_Tet_X1_test4 )
{
  // tests two element XField4D with two elements in z direction

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm= world.split(world.rank());

  const int ii= 1;
  const int jj= 1;
  const int kk= 2;
  const int mm= 1;

  // simply creating the grid triggers connectivity and positive volume checks
  XField4D_Box_Ptope_X1 xfld(comm, ii, jj, kk, mm);

  BOOST_CHECK_EQUAL((ii + 1)*(jj + 1)*(kk + 1)*(mm + 1), xfld.nDOF());
  BOOST_CHECK_EQUAL(24*ii*jj*kk*mm, xfld.nElem());

  // volume field variable

  BOOST_CHECK_EQUAL(1, xfld.nCellGroups());
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  // interior-face field variable

  BOOST_CHECK_EQUAL(1, xfld.nInteriorTraceGroups());
  for (int n= 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // boundary-face field variable

  BOOST_CHECK_EQUAL(8, xfld.nBoundaryTraceGroups());
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Tet));
}

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( XField4D_Box_Tet_X1_test5 )
{
  // tests two element XField4D with two elements in "w" direction

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm= world.split(world.rank());

  const int ii= 1;
  const int jj= 1;
  const int kk= 1;
  const int mm= 2;

  // simply creating the grid triggers connectivity and positive volume checks
  XField4D_Box_Ptope_X1 xfld(comm, ii, jj, kk, mm);

  BOOST_CHECK_EQUAL((ii + 1)*(jj + 1)*(kk + 1)*(mm + 1), xfld.nDOF());
  BOOST_CHECK_EQUAL(24*ii*jj*kk*mm, xfld.nElem());

  // volume field variable

  BOOST_CHECK_EQUAL(1, xfld.nCellGroups());
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  // interior-face field variable

  BOOST_CHECK_EQUAL(1, xfld.nInteriorTraceGroups());
  for (int n= 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // boundary-face field variable

  BOOST_CHECK_EQUAL(8, xfld.nBoundaryTraceGroups());
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Tet));
}

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( XField4D_Box_Tet_X1_test6 )
{
  // ensures each direction of the grid is assigned and builds its sizes correctly

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm= world.split(world.rank());

  const int ii= 2;
  const int jj= 3;
  const int kk= 4;
  const int mm= 1;

  // simply creating the grid triggers connectivity and positive volume checks
  XField4D_Box_Ptope_X1 xfld(comm, ii, jj, kk, mm);

  BOOST_CHECK_EQUAL((ii + 1)*(jj + 1)*(kk + 1)*(mm + 1), xfld.nDOF());
  BOOST_CHECK_EQUAL(24*ii*jj*kk*mm, xfld.nElem());

  // volume field variable

  BOOST_CHECK_EQUAL(1, xfld.nCellGroups());
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  // interior-face field variable

  BOOST_CHECK_EQUAL(1, xfld.nInteriorTraceGroups());
  for (int n= 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // boundary-face field variable

  BOOST_CHECK_EQUAL(8, xfld.nBoundaryTraceGroups());
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Tet));
}

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( XField4D_Box_Tet_X1_test7 )
{
  // tests boundary generation on a 2x2x2x2 XField4D

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm= world.split(world.rank());

  const int ii= 2;
  const int jj= 2;
  const int kk= 2;
  const int mm= 2;

  // simply creating the grid triggers connectivity and positive volume checks
  XField4D_Box_Ptope_X1 xfld(comm, ii, jj, kk, mm);

  BOOST_CHECK_EQUAL((ii + 1)*(jj + 1)*(kk + 1)*(mm + 1), xfld.nDOF());
  BOOST_CHECK_EQUAL(24*ii*jj*kk*mm, xfld.nElem());

  // volume field variable

  BOOST_CHECK_EQUAL(1, xfld.nCellGroups());
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  // interior-face field variable

  BOOST_CHECK_EQUAL(1, xfld.nInteriorTraceGroups());
  for (int n= 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // boundary-face field variable

  BOOST_CHECK_EQUAL(8, xfld.nBoundaryTraceGroups());
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // make sure boundary faces are correct

  // loop over the boundaries
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
  {
    const XField4D_Box_Ptope_X1::FieldTraceGroupType<Tet> &xfldBedgen= xfld.getBoundaryTraceGroup<Tet>(n);

    // number of tets on a given boundary
    const int nTet= xfldBedgen.nElem();

    int nodeMap[5];

    // loop over the tets on this boundary
    for (int k= 0; k < nTet; k++)
    {
      // grab the global nodes
      xfldBedgen.associativity(k).getNodeGlobalMapping(nodeMap, 4);

      bool hasBoundary= false;

      if (n == xfld.iXmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[0] == 0)
            hasBoundary= true;
      }
      else if (n == xfld.iXmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[0] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iYmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[1] == 0)
            hasBoundary= true;
      }
      else if (n == xfld.iYmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[1] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iZmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[2] == 0)
            hasBoundary= true;
      }
      else if (n == xfld.iZmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[2] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iWmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[3] == 0)
            hasBoundary= true;
      }
      else if (n == xfld.iWmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[3] == 1)
            hasBoundary= true;
      }

      BOOST_CHECK(hasBoundary);

    }

  }



}

BOOST_AUTO_TEST_CASE( XField4D_Box_Tet_X1_test8 )
{
  // tests XField4D construction using min/max specification of mesh nodes and boundaries

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm= world.split(world.rank());

  const int ii= 2;
  const int jj= 2;
  const int kk= 2;
  const int mm= 2;

  const int xmin= 1;
  const int xmax= 3;
  const int ymin= 1;
  const int ymax= 3;
  const int zmin= 1;
  const int zmax= 3;
  const int wmin= 1;
  const int wmax= 3;

  XField4D_Box_Ptope_X1 xfld(comm, ii, jj, kk, mm, xmin, xmax, ymin, ymax, zmin, zmax, wmin, wmax);

  BOOST_CHECK_EQUAL((ii + 1)*(jj + 1)*(kk + 1)*(mm + 1), xfld.nDOF());
  BOOST_CHECK_EQUAL(24*ii*jj*kk*mm, xfld.nElem());

  // volume field variable

  BOOST_CHECK_EQUAL(1, xfld.nCellGroups());
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  // interior-face field variable

  BOOST_CHECK_EQUAL(1, xfld.nInteriorTraceGroups());
  for (int n= 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // boundary-face field variable

  BOOST_CHECK_EQUAL(8, xfld.nBoundaryTraceGroups());
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // make sure boundary faces are correct

  // loop over the boundaries
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
  {
    const XField4D_Box_Ptope_X1::FieldTraceGroupType<Tet> &xfldBedgen= xfld.getBoundaryTraceGroup<Tet>(n);

    // number of tets on a given boundary
    const int nTet= xfldBedgen.nElem();

    int nodeMap[5];

    // loop over the tets on this boundary
    for (int k= 0; k < nTet; k++)
    {
      // grab the global nodes
      xfldBedgen.associativity(k).getNodeGlobalMapping(nodeMap, 4);

      bool hasBoundary= false;

      if (n == xfld.iXmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[0] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iXmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[0] == 3)
            hasBoundary= true;
      }
      else if (n == xfld.iYmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[1] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iYmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[1] == 3)
            hasBoundary= true;
      }
      else if (n == xfld.iZmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[2] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iZmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[2] == 3)
            hasBoundary= true;
      }
      else if (n == xfld.iWmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[3] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iWmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[3] == 3)
            hasBoundary= true;
      }

      BOOST_CHECK(hasBoundary);

    }

  }

}

BOOST_AUTO_TEST_CASE( XField4D_Box_Tet_X1_test9 )
{
  // tests XField4D using vectorial input of mesh nodes and boundaries

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm= world.split(world.rank());

  const int ii= 2;
  const int jj= 2;
  const int kk= 2;
  const int mm= 2;

  const int xmin= 1;
  const int xmax= 3;
  const int ymin= 1;
  const int ymax= 3;
  const int zmin= 1;
  const int zmax= 3;
  const int wmin= 1;
  const int wmax= 3;

  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);
  std::vector<Real> zvec(kk+1);
  std::vector<Real> wvec(mm+1);

  // construct vectors
  for (unsigned int i = 0; i < ii+1; i++)
    xvec[i] = xmin + (xmax - xmin)*i/Real(ii);

  for (unsigned int j = 0; j < jj+1; j++)
    yvec[j] = ymin + (ymax - ymin)*j/Real(jj);

  for (unsigned int k = 0; k < kk+1; k++)
    zvec[k] = zmin + (zmax - zmin)*k/Real(kk);

  for (unsigned int m = 0; m < mm+1; m++)
    wvec[m] = wmin + (wmax - wmin)*m/Real(mm);

  XField4D_Box_Ptope_X1 xfld(comm, xvec, yvec, zvec, wvec);

  BOOST_CHECK_EQUAL((ii + 1)*(jj + 1)*(kk + 1)*(mm + 1), xfld.nDOF());
  BOOST_CHECK_EQUAL(24*ii*jj*kk*mm, xfld.nElem());

  // volume field variable

  BOOST_CHECK_EQUAL(1, xfld.nCellGroups());
  BOOST_REQUIRE(xfld.getCellGroupBase(0).topoTypeID() == typeid(Pentatope));

  // interior-face field variable

  BOOST_CHECK_EQUAL(1, xfld.nInteriorTraceGroups());
  for (int n= 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // boundary-face field variable

  BOOST_CHECK_EQUAL(8, xfld.nBoundaryTraceGroups());
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE(xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Tet));

  // make sure boundary faces are correct

  // loop over the boundaries
  for (int n= 0; n < xfld.nBoundaryTraceGroups(); n++)
  {
    const XField4D_Box_Ptope_X1::FieldTraceGroupType<Tet> &xfldBedgen= xfld.getBoundaryTraceGroup<Tet>(n);

    // number of tets on a given boundary
    const int nTet= xfldBedgen.nElem();

    int nodeMap[5];

    // loop over the tets on this boundary
    for (int k= 0; k < nTet; k++)
    {
      // grab the global nodes
      xfldBedgen.associativity(k).getNodeGlobalMapping(nodeMap, 4);

      bool hasBoundary= false;

      if (n == xfld.iXmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[0] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iXmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[0] == 3)
            hasBoundary= true;
      }
      else if (n == xfld.iYmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[1] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iYmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[1] == 3)
            hasBoundary= true;
      }
      else if (n == xfld.iZmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[2] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iZmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[2] == 3)
            hasBoundary= true;
      }
      else if (n == xfld.iWmin)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[3] == 1)
            hasBoundary= true;
      }
      else if (n == xfld.iWmax)
      {
        for (int i= 0; i < 4; i++)
          if (xfld.DOF(nodeMap[i])[3] == 3)
            hasBoundary= true;
      }

      BOOST_CHECK(hasBoundary);

    }

  }

}

#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
