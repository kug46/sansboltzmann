// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField1D_1Group_Periodic

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField1D_1Group_Periodic.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_1Group_Periodic_test )
{
  XField1D_1Group_Periodic xfld(1);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 2 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );

  // line field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField1D_1Group_Periodic::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2];

  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );

  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Node) );

  const XField1D_1Group_Periodic::FieldTraceGroupType<Node>& xfldInode = xfld.getInteriorTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( xfldInode.nElem(), 1 );

  xfldInode.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  //Normals
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignR(), -1 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getGroupRight(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(0).trace, 1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_1Group_Periodic_3line_test )
{
  XField1D_1Group_Periodic xfld(3, -1, 2);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], -1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  2.0 );

  // line field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField1D_1Group_Periodic::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldCell.nElem(), 3 );

  int nodeMap[2];

  xfldCell.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldCell.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldCell.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );

  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Node) );

  const XField1D_1Group_Periodic::FieldTraceGroupType<Node>& xfldInode = xfld.getInteriorTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( xfldInode.nElem(), 3 );

  xfldInode.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  xfldInode.associativity(1).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  xfldInode.associativity(2).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  //Normals
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(0).normalSignR(), -1 );

  BOOST_CHECK_EQUAL( xfldInode.associativity(1).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(1).normalSignR(), -1 );

  BOOST_CHECK_EQUAL( xfldInode.associativity(2).normalSignL(),  1 );
  BOOST_CHECK_EQUAL( xfldInode.associativity(2).normalSignR(), -1 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(0), 2 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(1), 0 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(1), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(1).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(1).trace, 1 );

  BOOST_CHECK_EQUAL( xfldInode.getElementLeft(2), 1 );
  BOOST_CHECK_EQUAL( xfldInode.getElementRight(2), 2 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft(2).trace, 0 );
  BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight(2).trace, 1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 0 );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
