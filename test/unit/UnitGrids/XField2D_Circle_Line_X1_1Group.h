// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UNIT_UNITGRIDS_XFIELD2D_CIRCLE_LINE_X1_1GROUP_H_
#define UNIT_UNITGRIDS_XFIELD2D_CIRCLE_LINE_X1_1GROUP_H_

#include "Field/XFieldLine.h"

namespace SANS
{
// line grid on a unit circle
//
// generate grid with ii elements
// line elements in 1 group
// interior traces in 1 group
// no boundary trace

class XField2D_Circle_Line_X1_1Group : public XField<PhysD2,TopoD1>
{
public:
  typedef std::vector<DLA::VectorS<2,Real>> ArrayGrid;

  explicit XField2D_Circle_Line_X1_1Group(const int ii);
};

}



#endif /* UNIT_UNITGRIDS_XFIELD2D_CIRCLE_LINE_X1_1GROUP_H_ */
