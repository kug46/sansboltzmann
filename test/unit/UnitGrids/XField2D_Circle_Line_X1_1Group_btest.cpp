// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_Circle_Line_X1_1Group.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Circle_Line_X1_1Group_test )
{
  // ---------- Set up a circular line grid ----------
  const int ii = 8;  // number of elements

  XField2D_Circle_Line_X1_1Group xfld(ii);

  // ---------- Check node values ----------
  BOOST_CHECK_EQUAL( xfld.nDOF(), ii );

  const Real small_tol = 1.e-14;  // absolute tolerance compared to zero
  const Real close_tol = 1.e-12;  // relative tolerance

  SANS_CHECK_CLOSE( 1.0, xfld.DOF(0)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  .0, xfld.DOF(0)[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( sqrt(0.5), xfld.DOF(1)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( sqrt(0.5), xfld.DOF(1)[1], small_tol, close_tol );

  SANS_CHECK_CLOSE(  .0, xfld.DOF(2)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( 1.0, xfld.DOF(2)[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( -sqrt(0.5), xfld.DOF(3)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(  sqrt(0.5), xfld.DOF(3)[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( -1.0, xfld.DOF(4)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE(   .0, xfld.DOF(4)[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( -sqrt(0.5), xfld.DOF(5)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( -sqrt(0.5), xfld.DOF(5)[1], small_tol, close_tol );

  SANS_CHECK_CLOSE( - .0, xfld.DOF(6)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( -1.0, xfld.DOF(6)[1], small_tol, close_tol );

  SANS_CHECK_CLOSE(  sqrt(0.5), xfld.DOF(7)[0], small_tol, close_tol );
  SANS_CHECK_CLOSE( -sqrt(0.5), xfld.DOF(7)[1], small_tol, close_tol );

  // ---------- Check line field variable ----------
  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField2D_Circle_Line_X1_1Group::FieldCellGroupType<Line>& xfldCell = xfld.getCellGroup<Line>(0);

  int nodeMap[2]; // container for node map of an element

  for ( int i = 0; i < ii-1; i++)
  {
    xfldCell.associativity(i).getNodeGlobalMapping( nodeMap, 2 );
    BOOST_CHECK_EQUAL( nodeMap[0], i );    // left node
    BOOST_CHECK_EQUAL( nodeMap[1], i+1 );  // right node
  }
  xfldCell.associativity(ii-1).getNodeGlobalMapping( nodeMap, 2 );  // the last element is connected to the first element
  BOOST_CHECK_EQUAL( nodeMap[0], ii-1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );


  // ---------- Check interior-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Node) );

  const XField2D_Circle_Line_X1_1Group::FieldTraceGroupType<Node>& xfldInode = xfld.getInteriorTraceGroup<Node>(0);

  // node map
  for ( int i = 0; i < ii; i++)
  {
    xfldInode.associativity(i).getNodeGlobalMapping( nodeMap, 1 );
    BOOST_CHECK_EQUAL( nodeMap[0], i );
  }

  // normals
  for ( int i = 0; i < ii; i++)
  {
    BOOST_CHECK_EQUAL( xfldInode.associativity(i).normalSignL(),  1 );
    BOOST_CHECK_EQUAL( xfldInode.associativity(i).normalSignR(), -1 );
  }

  // interior trace-to-cell connectivity
  for ( int i = 0; i < ii; i++)
  {
    BOOST_CHECK_EQUAL( xfldInode.getGroupLeft(), 0 );
    if ( i > 0 )
      BOOST_CHECK_EQUAL( xfldInode.getElementLeft( i ), i-1 );
    else
      BOOST_CHECK_EQUAL( xfldInode.getElementLeft( i ), ii-1 );

    BOOST_CHECK_EQUAL( xfldInode.getElementRight( i ), i );
    BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceLeft( i ).trace, 0 );
    BOOST_CHECK_EQUAL( xfldInode.getCanonicalTraceRight( i ).trace, 1 );
  }

  // ---------- Check boundary-trace field variable ----------
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 0 );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
