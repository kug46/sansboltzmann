// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_Box_UnionJack_Triangle_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_UnionJack_Triangle_X1_test )
{
  const int ii = 4;
  const int jj = 4;

  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

  BOOST_CHECK_EQUAL( xfld.iBottom, 0 );
  BOOST_CHECK_EQUAL( xfld.iRight,  1 );
  BOOST_CHECK_EQUAL( xfld.iTop,    2 );
  BOOST_CHECK_EQUAL( xfld.iLeft,   3 );

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );

  // area field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField2D_Box_UnionJack_Triangle_X1::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);

  // check consistency of DOF associtivity

  int nodeCount[(ii+1)*(jj+1)];
  int nodeMap[3];
  int nelem;
  ElementAssociativityConstructor<TopoD2, Triangle> xDOFAssocArea( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );

  for (int n = 0; n < (ii+1)*(jj+1); n++)
    nodeCount[n] = 0;

  nelem = xfldArea.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    xfldArea.associativity( elem ).getNodeGlobalMapping( nodeMap, 3 );
    for (int n = 0; n < 3; n++)
      nodeCount[nodeMap[n]]++;
  }

  BOOST_CHECK_EQUAL( 2, nodeCount[0] );
  BOOST_CHECK_EQUAL( 2, nodeCount[ii] );
  BOOST_CHECK_EQUAL( 2, nodeCount[jj*(ii+1)] );
  BOOST_CHECK_EQUAL( 2, nodeCount[jj*(ii+1) + ii] );

  for (int i = 1; i < ii/2; i++)
  {
    BOOST_CHECK_EQUAL( 3, nodeCount[i] );
    BOOST_CHECK_EQUAL( 3, nodeCount[jj*(ii+1) + i] );
  }
  for (int i = ii/2 + 1; i < ii; i++)
  {
    BOOST_CHECK_EQUAL( 3, nodeCount[i] );
    BOOST_CHECK_EQUAL( 3, nodeCount[jj*(ii+1) + i] );
  }
  BOOST_CHECK_EQUAL( 2, nodeCount[ii/2] );
  BOOST_CHECK_EQUAL( 2, nodeCount[jj*(ii+1) + ii/2] );

  for (int j = 1; j < jj/2; j++)
  {
    BOOST_CHECK_EQUAL( 3, nodeCount[j*(ii+1)] );
    BOOST_CHECK_EQUAL( 3, nodeCount[j*(ii+1) + ii] );
  }
  for (int j = jj/2 + 1; j < jj; j++)
  {
    BOOST_CHECK_EQUAL( 3, nodeCount[j*(ii+1)] );
    BOOST_CHECK_EQUAL( 3, nodeCount[j*(ii+1) + ii] );
  }
  BOOST_CHECK_EQUAL( 2, nodeCount[(jj/2)*(ii+1)] );
  BOOST_CHECK_EQUAL( 2, nodeCount[(jj/2)*(ii+1) + ii] );

  for (int j = 1; j < jj/2; j++)
  {
    for (int i = 1; i < ii/2; i++)
      BOOST_CHECK_EQUAL( 6, nodeCount[j*(ii+1) + i] );
    for (int i = ii/2 + 1; i < ii; i++)
      BOOST_CHECK_EQUAL( 6, nodeCount[j*(ii+1) + i] );
  }
  for (int j = jj/2 + 1; j < jj; j++)
  {
    for (int i = 1; i < ii/2; i++)
      BOOST_CHECK_EQUAL( 6, nodeCount[j*(ii+1) + i] );
    for (int i = ii/2 + 1; i < ii; i++)
      BOOST_CHECK_EQUAL( 6, nodeCount[j*(ii+1) + i] );
  }
  BOOST_CHECK_EQUAL( 8, nodeCount[(jj/2)*(ii+1) + ii/2] );

  // check all elements have positive area

  Real x0, x1, x2;
  Real y0, y1, y2;
  for (int elem = 0; elem < nelem; elem++)
  {
    xfldArea.associativity( elem ).getNodeGlobalMapping( nodeMap, 3 );
    x0 = xfldArea.DOF(nodeMap[0])[0];  y0 = xfldArea.DOF(nodeMap[0])[1];
    x1 = xfldArea.DOF(nodeMap[1])[0];  y1 = xfldArea.DOF(nodeMap[1])[1];
    x2 = xfldArea.DOF(nodeMap[2])[0];  y2 = xfldArea.DOF(nodeMap[2])[1];

    BOOST_CHECK( ((x1 - x0)*(y2 - y1) - (x2 - x1)*(y1 - y0)) > 0 );
  }

  // check edge signs for area elements (L is +, R is -)

  typedef std::array<int,3> Int3;

  Int3 edgeSign;

  // lower
  for (int j = 0; j < jj/2; j++)
  {
    for (int i = 0; i < ii/2; i++)
    {
      edgeSign = xfldArea.associativity( j*(2*ii) + 2*i ).edgeSign();

      if (i == 0)
      {
        BOOST_CHECK_EQUAL( edgeSign[0], +1 );
        BOOST_CHECK_EQUAL( edgeSign[1], -1 );
        BOOST_CHECK_EQUAL( edgeSign[2], +1 );
      }
      else
      {
        BOOST_CHECK_EQUAL( edgeSign[0], +1 );
        BOOST_CHECK_EQUAL( edgeSign[1], -1 );
        BOOST_CHECK_EQUAL( edgeSign[2], -1 );
      }

      edgeSign = xfldArea.associativity( j*(2*ii) + 2*i + 1 ).edgeSign();

      BOOST_CHECK_EQUAL( edgeSign[0], -1 );
      BOOST_CHECK_EQUAL( edgeSign[1], +1 );
      BOOST_CHECK_EQUAL( edgeSign[2], +1 );
    }

    for (int i = ii/2; i < ii; i++)
    {
      edgeSign = xfldArea.associativity( j*(2*ii) + 2*i ).edgeSign();

      BOOST_CHECK_EQUAL( edgeSign[0], +1 );
      BOOST_CHECK_EQUAL( edgeSign[1], -1 );
      BOOST_CHECK_EQUAL( edgeSign[2], +1 );

      edgeSign = xfldArea.associativity( j*(2*ii) + 2*i + 1 ).edgeSign();

      BOOST_CHECK_EQUAL( edgeSign[0], -1 );
      BOOST_CHECK_EQUAL( edgeSign[1], +1 );
      BOOST_CHECK_EQUAL( edgeSign[2], -1 );
    }
  }

  // upper
  for (int j = jj/2; j < jj; j++)
  {
    for (int i = 0; i < ii/2; i++)
    {
      edgeSign = xfldArea.associativity( j*(2*ii) + 2*i ).edgeSign();

      if (i == 0)
      {
        BOOST_CHECK_EQUAL( edgeSign[0], +1 );
        BOOST_CHECK_EQUAL( edgeSign[1], +1 );
        BOOST_CHECK_EQUAL( edgeSign[2], +1 );
      }
      else
      {
        BOOST_CHECK_EQUAL( edgeSign[0], +1 );
        BOOST_CHECK_EQUAL( edgeSign[1], -1 );
        BOOST_CHECK_EQUAL( edgeSign[2], +1 );
      }

      edgeSign = xfldArea.associativity( j*(2*ii) + 2*i + 1 ).edgeSign();

      if (j == jj-1)
      {
        BOOST_CHECK_EQUAL( edgeSign[0], -1 );
        BOOST_CHECK_EQUAL( edgeSign[1], +1 );
        BOOST_CHECK_EQUAL( edgeSign[2], +1 );
      }
      else
      {
        BOOST_CHECK_EQUAL( edgeSign[0], -1 );
        BOOST_CHECK_EQUAL( edgeSign[1], +1 );
        BOOST_CHECK_EQUAL( edgeSign[2], -1 );
      }
    }

    for (int i = ii/2; i < ii; i++)
    {
      edgeSign = xfldArea.associativity( j*(2*ii) + 2*i ).edgeSign();

      if (j == jj-1)
      {
        BOOST_CHECK_EQUAL( edgeSign[0], +1 );
        BOOST_CHECK_EQUAL( edgeSign[1], +1 );
        BOOST_CHECK_EQUAL( edgeSign[2], -1 );
      }
      else
      {
        BOOST_CHECK_EQUAL( edgeSign[0], +1 );
        BOOST_CHECK_EQUAL( edgeSign[1], -1 );
        BOOST_CHECK_EQUAL( edgeSign[2], -1 );
      }

      edgeSign = xfldArea.associativity( j*(2*ii) + 2*i + 1 ).edgeSign();

      BOOST_CHECK_EQUAL( edgeSign[0], -1 );
      BOOST_CHECK_EQUAL( edgeSign[1], +1 );
      BOOST_CHECK_EQUAL( edgeSign[2], +1 );
    }
  }

  // interior-edge field variable

  BOOST_CHECK_EQUAL( 3, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < 3; n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Line) );

  ElementAssociativityConstructor<TopoD1, Line> xDOFAssocEdge( BasisFunctionLineBase::HierarchicalP1 );

  // check consistency of DOF associtivity

  for (int n = 0; n < (ii+1)*(jj+1); n++)
    nodeCount[n] = 0;

  for (int group = 0; group < 3; group++)
  {
    const XField2D_Box_UnionJack_Triangle_X1::FieldTraceGroupType<Line>& xfldIedge = xfld.getInteriorTraceGroup<Line>(group);

    nelem = xfldIedge.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      xfldIedge.associativity( elem ).getNodeGlobalMapping( nodeMap, 2 );
      for (int n = 0; n < 2; n++)
        nodeCount[nodeMap[n]]++;
    }
  }

  BOOST_CHECK_EQUAL( 1, nodeCount[0] );
  BOOST_CHECK_EQUAL( 1, nodeCount[ii] );
  BOOST_CHECK_EQUAL( 1, nodeCount[jj*(ii+1)] );
  BOOST_CHECK_EQUAL( 1, nodeCount[jj*(ii+1) + ii] );

  for (int i = 1; i < ii/2; i++)
  {
    BOOST_CHECK_EQUAL( 2, nodeCount[i] );
    BOOST_CHECK_EQUAL( 2, nodeCount[jj*(ii+1) + i] );
  }
  for (int i = ii/2 + 1; i < ii; i++)
  {
    BOOST_CHECK_EQUAL( 2, nodeCount[i] );
    BOOST_CHECK_EQUAL( 2, nodeCount[jj*(ii+1) + i] );
  }
  BOOST_CHECK_EQUAL( 1, nodeCount[ii/2] );
  BOOST_CHECK_EQUAL( 1, nodeCount[jj*(ii+1) + ii/2] );

  for (int j = 1; j < jj/2; j++)
  {
    BOOST_CHECK_EQUAL( 2, nodeCount[j*(ii+1)] );
    BOOST_CHECK_EQUAL( 2, nodeCount[j*(ii+1) + ii] );
  }
  for (int j = jj/2 + 1; j < jj; j++)
  {
    BOOST_CHECK_EQUAL( 2, nodeCount[j*(ii+1)] );
    BOOST_CHECK_EQUAL( 2, nodeCount[j*(ii+1) + ii] );
  }
  BOOST_CHECK_EQUAL( 1, nodeCount[(jj/2)*(ii+1)] );
  BOOST_CHECK_EQUAL( 1, nodeCount[(jj/2)*(ii+1) + ii] );

  for (int j = 1; j < jj/2; j++)
  {
    for (int i = 1; i < ii/2; i++)
      BOOST_CHECK_EQUAL( 6, nodeCount[j*(ii+1) + i] );
    for (int i = ii/2 + 1; i < ii; i++)
      BOOST_CHECK_EQUAL( 6, nodeCount[j*(ii+1) + i] );
  }
  for (int j = jj/2 + 1; j < jj; j++)
  {
    for (int i = 1; i < ii/2; i++)
      BOOST_CHECK_EQUAL( 6, nodeCount[j*(ii+1) + i] );
    for (int i = ii/2 + 1; i < ii; i++)
      BOOST_CHECK_EQUAL( 6, nodeCount[j*(ii+1) + i] );
  }
  BOOST_CHECK_EQUAL( 8, nodeCount[(jj/2)*(ii+1) + ii/2] );

  // check consistency of edge-to-cell connectivity

  int cellCount[2*ii*jj];
  int canonicalCount[2*ii*jj];
  int cell, canonical;

  for (int n = 0; n < 2*ii*jj; n++)
  {
    cellCount[n] = 0;
    canonicalCount[n] = 0;
  }

  for (int group = 0; group < 3; group++)
  {
    const XField2D_Box_UnionJack_Triangle_X1::FieldTraceGroupType<Line>& xfldIedge = xfld.getInteriorTraceGroup<Line>(group);

    nelem = xfldIedge.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      cell      = xfldIedge.getElementLeft( elem );
      canonical = xfldIedge.getCanonicalTraceLeft( elem ).trace;
      cellCount[cell]++;
      canonicalCount[cell] += pow(10, canonical);

      cell      = xfldIedge.getElementRight( elem );
      canonical = xfldIedge.getCanonicalTraceRight( elem ).trace;
      cellCount[cell]++;
      canonicalCount[cell] += pow(10, canonical);
    }
  }

  for (int i = 0; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 2, cellCount[2*i + 1] );
  for (int i = ii/2; i < ii; i++)
    BOOST_CHECK_EQUAL( 2, cellCount[2*i] );

  for (int i = 1; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 3, cellCount[2*i] );
  for (int i = ii/2; i < ii-1; i++)
    BOOST_CHECK_EQUAL( 3, cellCount[2*i + 1] );

  for (int i = 0; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 2, cellCount[(jj-1)*(2*ii) + 2*i + 1] );
  for (int i = ii/2; i < ii; i++)
    BOOST_CHECK_EQUAL( 2, cellCount[(jj-1)*(2*ii) + 2*i] );

  for (int i = 1; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 3, cellCount[(jj-1)*(2*ii) + 2*i] );
  for (int i = ii/2; i < ii-1; i++)
    BOOST_CHECK_EQUAL( 3, cellCount[(jj-1)*(2*ii) + 2*i + 1] );

  for (int j = 0; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 2, cellCount[j*(2*ii)] );
  for (int j = jj/2; j < jj; j++)
    BOOST_CHECK_EQUAL( 2, cellCount[j*(2*ii)] );

  for (int j = 1; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 3, cellCount[j*(2*ii) + 1] );
  for (int j = jj/2; j < jj-1; j++)
    BOOST_CHECK_EQUAL( 3, cellCount[j*(2*ii) + 1] );

  for (int j = 0; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 2, cellCount[j*(2*ii) + 2*ii - 1] );
  for (int j = jj/2; j < jj; j++)
    BOOST_CHECK_EQUAL( 2, cellCount[j*(2*ii) + 2*ii - 1] );

  for (int j = 1; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 3, cellCount[j*(2*ii) + 2*ii - 2] );
  for (int j = jj/2; j < jj-1; j++)
    BOOST_CHECK_EQUAL( 3, cellCount[j*(2*ii) + 2*ii - 2] );

  for (int i = 1; i < ii-1; i++)
  {
    for (int j = 1; j < jj-1; j++)
    {
      BOOST_CHECK_EQUAL( 3, cellCount[j*(2*ii) + 2*i] );
      BOOST_CHECK_EQUAL( 3, cellCount[j*(2*ii) + 2*i + 1] );
    }
  }


  for (int i = 0; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 101, canonicalCount[2*i + 1] );
  for (int i = ii/2; i < ii; i++)
    BOOST_CHECK_EQUAL( 11, canonicalCount[2*i] );

  for (int i = 1; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 111, canonicalCount[2*i] );
  for (int i = ii/2; i < ii-1; i++)
    BOOST_CHECK_EQUAL( 111, canonicalCount[2*i + 1] );

  for (int i = 0; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 11, canonicalCount[(jj-1)*(2*ii) + 2*i + 1] );
  for (int i = ii/2; i < ii; i++)
    BOOST_CHECK_EQUAL( 101, canonicalCount[(jj-1)*(2*ii) + 2*i] );

  for (int i = 1; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 111, canonicalCount[(jj-1)*(2*ii) + 2*i] );
  for (int i = ii/2; i < ii-1; i++)
    BOOST_CHECK_EQUAL( 111, canonicalCount[(jj-1)*(2*ii) + 2*i + 1] );

  for (int j = 0; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 11, canonicalCount[j*(2*ii)] );
  for (int j = jj/2; j < jj; j++)
    BOOST_CHECK_EQUAL( 101, canonicalCount[j*(2*ii)] );

  for (int j = 1; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 1] );
  for (int j = jj/2; j < jj-1; j++)
    BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 1] );

  for (int j = 0; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 101, canonicalCount[j*(2*ii) + 2*ii - 1] );
  for (int j = jj/2; j < jj; j++)
    BOOST_CHECK_EQUAL( 11, canonicalCount[j*(2*ii) + 2*ii - 1] );

  for (int j = 1; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 2*ii - 2] );
  for (int j = jj/2; j < jj-1; j++)
    BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 2*ii - 2] );

  for (int j = 1; j < jj/2; j++)
  {
    for (int i = 1; i < ii/2; i++)
    {
      BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 2*i] );
      BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 2*i + 1] );
    }
    for (int i = ii/2 + 1; i < ii-1; i++)
    {
      BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 2*i] );
      BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 2*i + 1] );
    }
  }
  for (int j = jj/2 + 1; j < jj-1; j++)
  {
    for (int i = 1; i < ii/2; i++)
    {
      BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 2*i] );
      BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 2*i + 1] );
    }
    for (int i = ii/2 + 1; i < ii-1; i++)
    {
      BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 2*i] );
      BOOST_CHECK_EQUAL( 111, canonicalCount[j*(2*ii) + 2*i + 1] );
    }
  }

#if 0
  printf( "cellCount =     " );
  for (int n = 0; n < 2*ii*jj; n++)
    printf( " %d", cellCount[n] );
  printf( "\n" );
  printf( "canonicalCount =" );
  for (int n = 0; n < 2*ii*jj; n++)
    printf( " %d", canonicalCount[n] );
  printf( "\n" );
#endif


  // boundary-edge field variable

  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < 4; n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Line) );

  // check consistency of DOF associtivity

  for (int n = 0; n < (ii+1)*(jj+1); n++)
    nodeCount[n] = 0;

  for (int group = 0; group < 4; group++)
  {
    const XField2D_Box_UnionJack_Triangle_X1::FieldTraceGroupType<Line>& xfldBedge = xfld.getBoundaryTraceGroup<Line>(group);

    nelem = xfldBedge.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      xfldBedge.associativity( elem ).getNodeGlobalMapping( nodeMap, 2 );
      for (int n = 0; n < 2; n++)
        nodeCount[nodeMap[n]]++;
    }
  }

  BOOST_CHECK_EQUAL( 2, nodeCount[0] );
  BOOST_CHECK_EQUAL( 2, nodeCount[ii] );
  BOOST_CHECK_EQUAL( 2, nodeCount[jj*(ii+1)] );
  BOOST_CHECK_EQUAL( 2, nodeCount[jj*(ii+1) + ii] );

  for (int i = 1; i < ii; i++)
  {
    BOOST_CHECK_EQUAL( 2, nodeCount[i] );
    BOOST_CHECK_EQUAL( 2, nodeCount[jj*(ii+1) + i] );
  }
  for (int j = 1; j < jj; j++)
  {
    BOOST_CHECK_EQUAL( 2, nodeCount[j*(ii+1)] );
    BOOST_CHECK_EQUAL( 2, nodeCount[j*(ii+1) + ii] );
  }
  for (int i = 1; i < ii; i++)
  {
    for (int j = 1; j < jj; j++)
    {
      BOOST_CHECK_EQUAL( 0, nodeCount[j*(ii+1) + i] );
    }
  }

  // check consistency of edge-to-cell connectivity

  for (int n = 0; n < 2*ii*jj; n++)
  {
    cellCount[n] = 0;
    canonicalCount[n] = 0;
  }

  for (int group = 0; group < 4; group++)
  {
    const XField2D_Box_UnionJack_Triangle_X1::FieldTraceGroupType<Line>& xfldBedge = xfld.getBoundaryTraceGroup<Line>(group);

    nelem = xfldBedge.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      cell      = xfldBedge.getElementLeft( elem );
      canonical = xfldBedge.getCanonicalTraceLeft( elem ).trace;
      cellCount[cell]++;
      canonicalCount[cell] += pow(10, canonical);
    }
  }

  for (int i = 0; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 1, cellCount[2*i + 1] );
  for (int i = ii/2; i < ii; i++)
    BOOST_CHECK_EQUAL( 1, cellCount[2*i] );

  for (int i = 1; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 0, cellCount[2*i] );
  for (int i = ii/2; i < ii-1; i++)
    BOOST_CHECK_EQUAL( 0, cellCount[2*i + 1] );

  for (int i = 0; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 1, cellCount[(jj-1)*(2*ii) + 2*i + 1] );
  for (int i = ii/2; i < ii; i++)
    BOOST_CHECK_EQUAL( 1, cellCount[(jj-1)*(2*ii) + 2*i] );

  for (int i = 1; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 0, cellCount[(jj-1)*(2*ii) + 2*i] );
  for (int i = ii/2; i < ii-1; i++)
    BOOST_CHECK_EQUAL( 0, cellCount[(jj-1)*(2*ii) + 2*i + 1] );

  for (int j = 0; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 1, cellCount[j*(2*ii)] );
  for (int j = jj/2; j < jj; j++)
    BOOST_CHECK_EQUAL( 1, cellCount[j*(2*ii)] );

  for (int j = 1; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 0, cellCount[j*(2*ii) + 1] );
  for (int j = jj/2; j < jj-1; j++)
    BOOST_CHECK_EQUAL( 0, cellCount[j*(2*ii) + 1] );

  for (int j = 0; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 1, cellCount[j*(2*ii) + 2*ii - 1] );
  for (int j = jj/2; j < jj; j++)
    BOOST_CHECK_EQUAL( 1, cellCount[j*(2*ii) + 2*ii - 1] );

  for (int j = 1; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 0, cellCount[j*(2*ii) + 2*ii - 2] );
  for (int j = jj/2; j < jj-1; j++)
    BOOST_CHECK_EQUAL( 0, cellCount[j*(2*ii) + 2*ii - 2] );

  for (int i = 1; i < ii-1; i++)
  {
    for (int j = 1; j < jj-1; j++)
    {
      BOOST_CHECK_EQUAL( 0, cellCount[j*(2*ii) + 2*i] );
      BOOST_CHECK_EQUAL( 0, cellCount[j*(2*ii) + 2*i + 1] );
    }
  }


  for (int i = 0; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 10, canonicalCount[2*i + 1] );
  for (int i = ii/2; i < ii; i++)
    BOOST_CHECK_EQUAL( 100, canonicalCount[2*i] );

  for (int i = 1; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 0, canonicalCount[2*i] );
  for (int i = ii/2; i < ii-1; i++)
    BOOST_CHECK_EQUAL( 0, canonicalCount[2*i + 1] );

  for (int i = 0; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 100, canonicalCount[(jj-1)*(2*ii) + 2*i + 1] );
  for (int i = ii/2; i < ii; i++)
    BOOST_CHECK_EQUAL( 10, canonicalCount[(jj-1)*(2*ii) + 2*i] );

  for (int i = 1; i < ii/2; i++)
    BOOST_CHECK_EQUAL( 0, canonicalCount[(jj-1)*(2*ii) + 2*i] );
  for (int i = ii/2; i < ii-1; i++)
    BOOST_CHECK_EQUAL( 0, canonicalCount[(jj-1)*(2*ii) + 2*i + 1] );

  for (int j = 0; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 100, canonicalCount[j*(2*ii)] );
  for (int j = jj/2; j < jj; j++)
    BOOST_CHECK_EQUAL( 10, canonicalCount[j*(2*ii)] );

  for (int j = 1; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 0, canonicalCount[j*(2*ii) + 1] );
  for (int j = jj/2; j < jj-1; j++)
    BOOST_CHECK_EQUAL( 0, canonicalCount[j*(2*ii) + 1] );

  for (int j = 0; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 10, canonicalCount[j*(2*ii) + 2*ii - 1] );
  for (int j = jj/2; j < jj; j++)
    BOOST_CHECK_EQUAL( 100, canonicalCount[j*(2*ii) + 2*ii - 1] );

  for (int j = 1; j < jj/2; j++)
    BOOST_CHECK_EQUAL( 0, canonicalCount[j*(2*ii) + 2*ii - 2] );
  for (int j = jj/2; j < jj-1; j++)
    BOOST_CHECK_EQUAL( 0, canonicalCount[j*(2*ii) + 2*ii - 2] );

  for (int i = 1; i < ii-1; i++)
  {
    for (int j = 1; j < jj-1; j++)
    {
      BOOST_CHECK_EQUAL( 0, canonicalCount[j*(2*ii) + 2*i] );
      BOOST_CHECK_EQUAL( 0, canonicalCount[j*(2*ii) + 2*i + 1] );
    }
  }

#if 0
  printf( "cellCount =     " );
  for (int n = 0; n < 2*ii*jj; n++)
    printf( " %d", cellCount[n] );
  printf( "\n" );
  printf( "canonicalCount =" );
  for (int n = 0; n < 2*ii*jj; n++)
    printf( " %d", canonicalCount[n] );
  printf( "\n" );
#endif

#if 0
  xfld.dump();
#endif
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
