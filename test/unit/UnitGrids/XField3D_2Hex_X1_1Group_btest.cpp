// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField3D_2Hex_X1_1Group_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_2Hex_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_2Hex_X1_1Group_test )
{
  typedef std::array<int,6> Int6;

  XField3D_2Hex_X1_1Group xfld;

  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 12 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 2 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0], 2 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[2], 0 );

  BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0], 2 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[2], 1 );

  BOOST_CHECK_EQUAL( xfld.DOF( 9)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF( 9)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF( 9)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(10)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(10)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(10)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(11)[0], 2 );  BOOST_CHECK_EQUAL( xfld.DOF(11)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(11)[2], 1 );

  // cell field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Hex) );

  const XField3D_2Hex_X1_1Group::FieldCellGroupType<Hex>& xfldVolume = xfld.getCellGroup<Hex>(0);

  int nodeMap[8];

  xfldVolume.associativity(0).getNodeGlobalMapping( nodeMap, 8 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );
  BOOST_CHECK_EQUAL( nodeMap[4], 6 );
  BOOST_CHECK_EQUAL( nodeMap[5], 7 );
  BOOST_CHECK_EQUAL( nodeMap[6], 10 );
  BOOST_CHECK_EQUAL( nodeMap[7], 9 );

  xfldVolume.associativity(1).getNodeGlobalMapping( nodeMap, 8 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );
  BOOST_CHECK_EQUAL( nodeMap[4], 7 );
  BOOST_CHECK_EQUAL( nodeMap[5], 8 );
  BOOST_CHECK_EQUAL( nodeMap[6], 11 );
  BOOST_CHECK_EQUAL( nodeMap[7], 10 );

  Int6 faceSign;

  faceSign = xfldVolume.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );
  BOOST_CHECK_EQUAL( faceSign[4], +1 );
  BOOST_CHECK_EQUAL( faceSign[5], +1 );

  faceSign = xfldVolume.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );
  BOOST_CHECK_EQUAL( faceSign[4], -1 );
  BOOST_CHECK_EQUAL( faceSign[5], +1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Quad) );
  const XField3D_2Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldIface = xfld.getInteriorTraceGroup<Quad>(0);

  xfldIface.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 10 );
  BOOST_CHECK_EQUAL( nodeMap[3], 7 );

  BOOST_CHECK_EQUAL( xfldIface.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIface.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldIface.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldIface.getElementRight(0), 1 );

  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(0).trace, 4 );

  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldIface.getCanonicalTraceRight(0).orientation, -1 );

  // boundary-edge field variable

  BOOST_REQUIRE_EQUAL( xfld.nBoundaryTraceGroups(), 6 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(2).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(3).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(4).topoTypeID() == typeid(Quad) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(5).topoTypeID() == typeid(Quad) );

  const XField3D_2Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBface0 = xfld.getBoundaryTraceGroup<Quad>(0);
  const XField3D_2Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBface1 = xfld.getBoundaryTraceGroup<Quad>(1);
  const XField3D_2Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBface2 = xfld.getBoundaryTraceGroup<Quad>(2);
  const XField3D_2Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBface3 = xfld.getBoundaryTraceGroup<Quad>(3);
  const XField3D_2Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBface4 = xfld.getBoundaryTraceGroup<Quad>(4);
  const XField3D_2Hex_X1_1Group::FieldTraceGroupType<Quad>& xfldBface5 = xfld.getBoundaryTraceGroup<Quad>(5);

  // x-min
  xfldBface0.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 9 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );


  // x-max
  xfldBface1.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 11 );
  BOOST_CHECK_EQUAL( nodeMap[3], 8 );


  // y-min
  xfldBface2.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );
  BOOST_CHECK_EQUAL( nodeMap[3], 6 );

  xfldBface2.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );
  BOOST_CHECK_EQUAL( nodeMap[3], 7 );


  // y-max
  xfldBface3.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 9 );
  BOOST_CHECK_EQUAL( nodeMap[2], 10 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );

  xfldBface3.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 11 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );


  // z-min
  xfldBface4.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );
  BOOST_CHECK_EQUAL( nodeMap[3], 1 );

  xfldBface4.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );
  BOOST_CHECK_EQUAL( nodeMap[3], 2 );


  // z-max
  xfldBface5.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 10 );
  BOOST_CHECK_EQUAL( nodeMap[3], 9 );

  xfldBface5.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );
  BOOST_CHECK_EQUAL( nodeMap[2], 11 );
  BOOST_CHECK_EQUAL( nodeMap[3], 10 );


  // boundary face-to-cell connectivity

  BOOST_CHECK_EQUAL( xfldBface0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBface1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBface2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBface3.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBface4.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBface5.getGroupLeft(), 0 );

  // x-min
  BOOST_CHECK_EQUAL( xfldBface0.getElementLeft(0), 0 );

  // x-max
  BOOST_CHECK_EQUAL( xfldBface1.getElementLeft(0), 1 );

  // y-min
  BOOST_CHECK_EQUAL( xfldBface2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBface2.getElementLeft(1), 1 );

  // y-max
  BOOST_CHECK_EQUAL( xfldBface3.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBface3.getElementLeft(1), 1 );

  // z-min
  BOOST_CHECK_EQUAL( xfldBface4.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBface4.getElementLeft(1), 1 );

  // z-max
  BOOST_CHECK_EQUAL( xfldBface5.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBface5.getElementLeft(1), 1 );


  // x-min
  BOOST_CHECK_EQUAL( xfldBface0.getCanonicalTraceLeft(0).trace, 4 );

  // x-max
  BOOST_CHECK_EQUAL( xfldBface1.getCanonicalTraceLeft(0).trace, 2 );

  // y-min
  BOOST_CHECK_EQUAL( xfldBface2.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBface2.getCanonicalTraceLeft(1).trace, 1 );

  // y-max
  BOOST_CHECK_EQUAL( xfldBface3.getCanonicalTraceLeft(0).trace, 3 );
  BOOST_CHECK_EQUAL( xfldBface3.getCanonicalTraceLeft(1).trace, 3 );

  // z-min
  BOOST_CHECK_EQUAL( xfldBface4.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBface4.getCanonicalTraceLeft(1).trace, 0 );

  // z-max
  BOOST_CHECK_EQUAL( xfldBface5.getCanonicalTraceLeft(0).trace, 5 );
  BOOST_CHECK_EQUAL( xfldBface5.getCanonicalTraceLeft(1).trace, 5 );


  // x-min
  BOOST_CHECK_EQUAL( xfldBface0.getCanonicalTraceLeft(0).orientation, 1 );

  // x-max
  BOOST_CHECK_EQUAL( xfldBface1.getCanonicalTraceLeft(0).orientation, 1 );

  // y-min
  BOOST_CHECK_EQUAL( xfldBface2.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBface2.getCanonicalTraceLeft(1).orientation, 1 );

  // y-max
  BOOST_CHECK_EQUAL( xfldBface3.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBface3.getCanonicalTraceLeft(1).orientation, 1 );

  // z-min
  BOOST_CHECK_EQUAL( xfldBface4.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBface4.getCanonicalTraceLeft(1).orientation, 1 );

  // z-max
  BOOST_CHECK_EQUAL( xfldBface5.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBface5.getCanonicalTraceLeft(1).orientation, 1 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
