// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_Box_Triangle_Ghost_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Triangle_Ghost_X1_SENW_GroupCount_test )
{
  // This calls checkGrid internally
  for (int ii=1; ii <= 2; ii++)
  {
    for (int jj=1; jj <= 2; jj++)
    {
      XField2D_Box_Triangle_Ghost_X1 xfld( ii, jj );
      BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOF() );

      BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
      BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
      BOOST_CHECK_EQUAL( xfld.nGhostBoundaryTraceGroups(), 2 );

      if ( ii==1 && jj==1 )
        BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
      else if ( ii==1 || jj==1 )
        BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 2 );
      else if ( ii>1 && jj>1 )
        BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 3 );
      else
        SANS_DEVELOPER_EXCEPTION("Invalid triangle mesh because ii and/or jj are/is unexpected");
    }
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
