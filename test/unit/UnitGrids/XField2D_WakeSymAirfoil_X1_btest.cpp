// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_WakeSymAirfoil_X1.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/output_Tecplot.h"

using namespace SANS;

namespace
{
//----------------------------------------------------------------------------//
// NACA 4-digit rescaled via Rumsey; gives sharp TE at x = 1
//
//  t = nominal thickness (e.g. 0.12 gives NACA0012)

double naca( double x, double t )
{
  double y;

  y = 4.955743175*t*( 0.298222773*sqrt(x) - 0.127125232*x - 0.357907906*x*x + 0.291984971*x*x*x - 0.105174606*x*x*x*x );
  return y;
}

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_WakeSymAirfoil_X1_NACA0024_test )
{

  Real xle = 0.0;
  Real xte = 1.0;
  Real t = 0.24;

  int iiairf = 8;
  std::vector< DLA::VectorS<2,Real> > airf(iiairf+1);

  // airfoil: cosine distribution
  Real x,y;
  for ( int i = 0; i <= iiairf; i++)
  {
    x = xle + (xte - xle)*(0.5 - 0.5*cos(PI*i/((double) iiairf)));
    y = naca( x, t );
    if (i == iiairf)    // needed for roundoff
    {
      x = xte;
      y = 0;
    }

    airf[i][0] =  x;
    airf[i][1] =  y;
  }

  // Check is called at the end of the constructor, hence this is actually tested
  XField2D_WakeSymAirfoil xfld( airf );

  //output_Tecplot(xfld, "tmp/naca0024.dat");
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
