// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD4D_LIBRARY_H
#define XFIELD4D_LIBRARY_H

#include <memory>
#include <vector>

#include "Field/XFieldSpacetime.h"

namespace SANS
{

class XField4D_6Ptope_X1_1Group_NegativeTraceOrientation : public XField<PhysD4,TopoD4>
{
public:
  XField4D_6Ptope_X1_1Group_NegativeTraceOrientation( mpi::communicator& comm );

private:
  void generate( mpi::communicator& comm );

};

}

#endif // XFIELD3D_BOX_TET_LAGRANGE_X1_H
