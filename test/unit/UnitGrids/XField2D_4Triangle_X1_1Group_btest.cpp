// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField2D_4Triangle_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_4Triangle_X1_1Group_test )
{
  typedef std::array<int,3> Int3;

  XField2D_4Triangle_X1_1Group xfld;

  BOOST_CHECK_EQUAL( xfld.nDOF(), 6 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], -1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], -1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField2D_4Triangle_X1_1Group::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);

  int nodeMap[3];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  xfldArea.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );

  xfldArea.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  Int3 edgeSign;

  edgeSign = xfldArea.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea.associativity(2).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], -1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea.associativity(3).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], -1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField2D_4Triangle_X1_1Group::FieldTraceGroupType<Line>& xfldIedge = xfld.getInteriorTraceGroup<Line>(0);

  xfldIedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldIedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  xfldIedge.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldIedge.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(0).trace, 0 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(1), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(1), 2 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(1).trace, 1 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(2), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(2), 3 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(2).trace, 2 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(2).trace, 2 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField2D_4Triangle_X1_1Group::FieldTraceGroupType<Line>& xfldBedge = xfld.getBoundaryTraceGroup<Line>(0);

  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );

  xfldBedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldBedge.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBedge.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldBedge.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldBedge.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  BOOST_CHECK_EQUAL( xfldBedge.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(0), 3 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(0), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(0).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(1), 3 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(1), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(1).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(2), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(2), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(2).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(2).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(3), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(3), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(3).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(3).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(4), 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(4), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(4).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(4).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(5), 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(5), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(5).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(5).trace, -1 );

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
