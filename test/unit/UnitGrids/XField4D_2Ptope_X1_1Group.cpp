// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField4D_2Ptope_X1_1Group.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
/*
 * A unit grid that consists of two pentatopes within a single group.
 *
 *          2
 *         /|\
 *        / | \
 *       / .|  \
 *      /   |   \
 *     /    |    \            ----> in another dimension: (4)
 *    /   . |     \                  (to the tune of the Wolfmother song)
 *   /      |      \                 and also to be shared by both elements
 *  5-------0-------1
 *   .   . /     .
 *    .   /   .
 *     . / .
 *      3
 *
 */

XField4D_2Ptope_X1_1Group::XField4D_2Ptope_X1_1Group()
{
  int comm_rank= comm_->rank();

  // create the DOF arrays
  resizeDOF(6);

  // create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(8);
  resizeCellGroups(1);

  // nodal coordinates for the two tetrahedra
  DOF(0)= {0, 0, 0, 0};
  DOF(1)= {1, 0, 0, 0};
  DOF(2)= {0, 1, 0, 0};
  DOF(3)= {0, 0, 1, 0};
  DOF(4)= {0, 0, 0, 1};
  DOF(5)= {-1, 0, 0, 0};

  // volume field variable
  FieldCellGroupType<Pentatope>::FieldAssociativityConstructorType fldAssocCell(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1, 2);

  // set the cell processor rank
  fldAssocCell.setAssociativity(0).setRank(comm_rank);
  fldAssocCell.setAssociativity(1).setRank(comm_rank);

  // element volume associativity
  fldAssocCell.setAssociativity(0).setNodeGlobalMapping({0, 1, 2, 3, 4});
  fldAssocCell.setAssociativity(1).setNodeGlobalMapping({0, 5, 2, 4, 3});

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity(0).setFaceSign(+1, 0);
  fldAssocCell.setAssociativity(0).setFaceSign(+1, 1);
  fldAssocCell.setAssociativity(0).setFaceSign(+1, 2);
  fldAssocCell.setAssociativity(0).setFaceSign(+1, 3);
  fldAssocCell.setAssociativity(0).setFaceSign(+1, 4);
  fldAssocCell.setAssociativity(1).setFaceSign(+1, 0);
  fldAssocCell.setAssociativity(1).setFaceSign(-1, 1);
  fldAssocCell.setAssociativity(1).setFaceSign(+1, 2);
  fldAssocCell.setAssociativity(1).setFaceSign(+1, 3);
  fldAssocCell.setAssociativity(1).setFaceSign(+1, 4);

  cellGroups_[0]= new FieldCellGroupType<Pentatope>(fldAssocCell);
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_= 2;

  // interior-edge field variable

  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocIface(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);

  fldAssocIface.setAssociativity(0).setRank(comm_rank);

  fldAssocIface.setAssociativity(0).setNodeGlobalMapping({0, 2, 3, 4});

  fldAssocIface.setGroupLeft(0);
  fldAssocIface.setGroupRight(0);

  fldAssocIface.setElementLeft(0, 0);
  fldAssocIface.setElementRight(1, 0);

  fldAssocIface.setCanonicalTraceLeft(CanonicalTraceToCell(1, 1), 0);
  fldAssocIface.setCanonicalTraceRight(CanonicalTraceToCell(1, -1), 0);

  interiorTraceGroups_[0]= new FieldTraceGroupType<Tet>(fldAssocIface);
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);

  // boundary-edge field variables

  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface0(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface1(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface2(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface3(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface4(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface5(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface6(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);
  FieldTraceGroupType<Tet>::FieldAssociativityConstructorType fldAssocBface7(BasisFunctionVolumeBase<Tet>::LagrangeP1, 1);

  // boundary face processor ranks
  fldAssocBface0.setAssociativity(0).setRank(comm_rank);
  fldAssocBface1.setAssociativity(0).setRank(comm_rank);
  fldAssocBface2.setAssociativity(0).setRank(comm_rank);
  fldAssocBface3.setAssociativity(0).setRank(comm_rank);

  fldAssocBface4.setAssociativity(0).setRank(comm_rank);
  fldAssocBface5.setAssociativity(0).setRank(comm_rank);
  fldAssocBface6.setAssociativity(0).setRank(comm_rank);
  fldAssocBface7.setAssociativity(0).setRank(comm_rank);

  // face-element associativity
  fldAssocBface0.setAssociativity(0).setNodeGlobalMapping({1, 2, 4, 3});
  fldAssocBface1.setAssociativity(0).setNodeGlobalMapping({0, 1, 4, 3});
  fldAssocBface2.setAssociativity(0).setNodeGlobalMapping({0, 1, 2, 4});
  fldAssocBface3.setAssociativity(0).setNodeGlobalMapping({0, 1, 3, 2});

  fldAssocBface4.setAssociativity(0).setNodeGlobalMapping({5, 2, 3, 4});
  fldAssocBface5.setAssociativity(0).setNodeGlobalMapping({0, 5, 3, 4});
  fldAssocBface6.setAssociativity(0).setNodeGlobalMapping({0, 5, 2, 3});
  fldAssocBface7.setAssociativity(0).setNodeGlobalMapping({0, 5, 4, 2});

  // face-to-cell connectivity
  fldAssocBface0.setGroupLeft(0);
  fldAssocBface1.setGroupLeft(0);
  fldAssocBface2.setGroupLeft(0);
  fldAssocBface3.setGroupLeft(0);

  fldAssocBface4.setGroupLeft(0);
  fldAssocBface5.setGroupLeft(0);
  fldAssocBface6.setGroupLeft(0);
  fldAssocBface7.setGroupLeft(0);

  fldAssocBface0.setElementLeft(0, 0);
  fldAssocBface1.setElementLeft(0, 0);
  fldAssocBface2.setElementLeft(0, 0);
  fldAssocBface3.setElementLeft(0, 0);

  fldAssocBface4.setElementLeft(1, 0);
  fldAssocBface5.setElementLeft(1, 0);
  fldAssocBface6.setElementLeft(1, 0);
  fldAssocBface7.setElementLeft(1, 0);

  fldAssocBface0.setCanonicalTraceLeft(CanonicalTraceToCell(0, 1), 0);
  fldAssocBface1.setCanonicalTraceLeft(CanonicalTraceToCell(2, 1), 0);
  fldAssocBface2.setCanonicalTraceLeft(CanonicalTraceToCell(3, 1), 0);
  fldAssocBface3.setCanonicalTraceLeft(CanonicalTraceToCell(4, 1), 0);

  fldAssocBface4.setCanonicalTraceLeft(CanonicalTraceToCell(0, 1), 0);
  fldAssocBface5.setCanonicalTraceLeft(CanonicalTraceToCell(2, 1), 0);
  fldAssocBface6.setCanonicalTraceLeft(CanonicalTraceToCell(3, 1), 0);
  fldAssocBface7.setCanonicalTraceLeft(CanonicalTraceToCell(4, 1), 0);

  boundaryTraceGroups_[0]= new FieldTraceGroupType<Tet>(fldAssocBface0);
  boundaryTraceGroups_[1]= new FieldTraceGroupType<Tet>(fldAssocBface1);
  boundaryTraceGroups_[2]= new FieldTraceGroupType<Tet>(fldAssocBface2);
  boundaryTraceGroups_[3]= new FieldTraceGroupType<Tet>(fldAssocBface3);
  boundaryTraceGroups_[4]= new FieldTraceGroupType<Tet>(fldAssocBface4);
  boundaryTraceGroups_[5]= new FieldTraceGroupType<Tet>(fldAssocBface5);
  boundaryTraceGroups_[6]= new FieldTraceGroupType<Tet>(fldAssocBface6);
  boundaryTraceGroups_[7]= new FieldTraceGroupType<Tet>(fldAssocBface7);

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[3]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[4]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[5]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[6]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[7]->setDOF(DOF_, nDOF_);

  // check that the grid is correct
  checkGrid();

}

}
