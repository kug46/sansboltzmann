// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD1D_4LINE_X1_2GROUP
#define XFIELD1D_4LINE_X1_2GROUP

#include "Field/XFieldLine.h"

namespace SANS
{
/*
   A unit grid that consists of four lines within 2 groups

      (0,0)   (0,1) | (1,0)   (1,1)
    0 ----- 1 ----- 2 ----- 3 ----- 4
*/

class XField1D_4Line_X1_2Group : public XField<PhysD1,TopoD1>
{
public:
  XField1D_4Line_X1_2Group();
};

}

#endif //XFIELD1D_4LINE_X1_2GROUP
