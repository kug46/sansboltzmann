// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/linspace.h"

#include "Field/output_Tecplot.h"

#include "XField3D_ForwardStep_Hex_X1.h"

#include "unit/Field/XField3D_CheckTraceCoord3D_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_ForwardStep_Hex_X1_static_test )
{
  BOOST_CHECK_EQUAL( XField3D_ForwardStep_Hex_X1::iXmin, 0 );
  BOOST_CHECK_EQUAL( XField3D_ForwardStep_Hex_X1::iXmax, 1 );
  BOOST_CHECK_EQUAL( XField3D_ForwardStep_Hex_X1::iYmin, 2 );
  BOOST_CHECK_EQUAL( XField3D_ForwardStep_Hex_X1::iYmax, 3 );
  BOOST_CHECK_EQUAL( XField3D_ForwardStep_Hex_X1::iZmin, 4 );
  BOOST_CHECK_EQUAL( XField3D_ForwardStep_Hex_X1::iZmax, 5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_ForwardStep_Hex_X1_test1 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  const int ii = 2;
  const int jj = 4;
  const int kk = 2;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_ForwardStep_Hex_X1 xfld( comm, ii, jj, kk, 0, 1, 0, 1, 0, 1, 0.5, 0.5 );

  BOOST_CHECK_EQUAL( 39, xfld.nDOF() );
  BOOST_CHECK_EQUAL( 12, xfld.nElem() );

  BOOST_CHECK_THROW( xfld.nDOFCellGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFInteriorTraceGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFBoundaryTraceGroup(0), DeveloperException );

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Hex) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Quad) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Quad) );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
      CheckInteriorTraceCoordinates3D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  std::vector<Real> xTrue = {0, 0.5, 0, 0.5, 0, 0.5, 1.0, 0, 0.5, 1.0, 0, 0.5, 1.0,
                             0, 0.5, 0, 0.5, 0, 0.5, 1.0, 0, 0.5, 1.0, 0, 0.5, 1.0,
                             0, 0.5, 0, 0.5, 0, 0.5, 1.0, 0, 0.5, 1.0, 0, 0.5, 1.0};
  std::vector<Real> yTrue = {0, 0, 0.25, 0.25, 0.5, 0.5, 0.5, 0.75, 0.75, 0.75, 1, 1, 1,
                             0, 0, 0.25, 0.25, 0.5, 0.5, 0.5, 0.75, 0.75, 0.75, 1, 1, 1,
                             0, 0, 0.25, 0.25, 0.5, 0.5, 0.5, 0.75, 0.75, 0.75, 1, 1, 1};
  std::vector<Real> zTrue = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                             0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
                             1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
                             
  for (int i = 0; i < xfld.nDOF(); i++)
  {
    BOOST_CHECK_EQUAL( xfld.DOF(i)[0], xTrue[i] );
    BOOST_CHECK_EQUAL( xfld.DOF(i)[1], yTrue[i] );
    BOOST_CHECK_EQUAL( xfld.DOF(i)[2], zTrue[i] );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
