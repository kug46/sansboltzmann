// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_Sphere_Triangle_X1.h"

#include "Field/output_Tecplot.h"

#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Sphere_Triangle_X1_test )
{

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-12;

  const int ii = 6;
  const int jj = 7;

  Real thetamin = 10;
  Real thetamax = 170;

  XField3D_Sphere_Triangle_X1 xfld( ii, jj, thetamin, thetamax );

  BOOST_CHECK_EQUAL( (ii+1)*jj, xfld.nDOF() );

  thetamin *= PI/180.;
  thetamax *= PI/180.;

  Real phimin = 0;
  Real phimax = 2*PI;
  Real r = 1;

  for (int j = 0; j < jj; j++)
  {
    Real phi = phimin + (phimax - phimin)*j/Real(jj);
    for (int i = 0; i < ii+1; i++)
    {
      Real theta = thetamin + (thetamax - thetamin)*i/Real(ii);
      SANS_CHECK_CLOSE( r*sin(theta)*cos(phi), xfld.DOF(j*(ii+1) + i)[0], small_tol, close_tol );
      SANS_CHECK_CLOSE( r*sin(theta)*sin(phi), xfld.DOF(j*(ii+1) + i)[1], small_tol, close_tol );
      SANS_CHECK_CLOSE( r*cos(theta)           , xfld.DOF(j*(ii+1) + i)[2], small_tol, close_tol );
    }
  }

#if 0
  output_Tecplot( xfld, "tmp/sphere.dat");
#endif

  // area field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( 3, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < 3; n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Line) );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( 2, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < 2; n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Line) );

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
