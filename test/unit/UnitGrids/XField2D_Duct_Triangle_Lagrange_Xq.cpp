// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_Duct_Triangle_Lagrange_Xq.h"

#include <iostream>

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

namespace SANS
{

XField2D_Duct_Triangle_Lagrange_Xq::
XField2D_Duct_Triangle_Lagrange_Xq(mpi::communicator comm, int ii, int jj, int order, Real xmin, Real xmax,
          std::function<Real(Real)> flower, std::function<Real(Real)> fupper)
   : XField2D_Box_Triangle_Lagrange_X1(comm, ii, jj, 0, 1, 0, 1)
{

/* Creates a triangular mesh based on a duct with the given parameters.
 * Arguments:
 *     Real x0 - Left boundary coordinate of physical domain (where xi is 0).
 *     Real x1 - Right boundary coordinate of physical domain (where xi is 1).
 *     function flower - Function that returns the lower surface's height given
 *         horizontal coordinate x.
 *     function fupper - Function that returns the upper surface's height given
 *         horizontal coordinate x.
 */

    if (comm.rank() == 0)
    {

        SANS_ASSERT_MSG( (order >= 1), "Geometric order Q must be equal or greater than 1." );

        if (order > 1)
        {
        XField2D_Box_Triangle_Lagrange_X1 xfld_linear(comm, ii, jj, 0, 1, 0, 1);
        buildFrom(xfld_linear, order);
        }

        Real s;
        Real t;
        Real x;
        Real y;

        // In Lagrange, the DOFs are the s and t coordinates
        typedef FieldCellGroupType<Triangle> FieldCellGroupTypeClass;
        typedef FieldCellGroupTypeClass::template ElementType<> ElementType;
        FieldCellGroupTypeClass& xfldCell = this->getCellGroup<Triangle>(0);
        ElementType xfldElem( xfldCell.basis() );

        const int nDOF     = this->nDOF();       // total number of DOFs in mesh
        const int nDOFelem = xfldElem.nDOF();    // number of total DOFs per element

        cout << "nDOF: " << nDOF << endl;
        cout << "nDOFelem: " << nDOFelem << endl;
        for (int i = 0; i < this->nDOF(); i++)
        {
            s = this->DOF(i)[0];
            t = this->DOF(i)[1];
            //cout << "DOF " << i << ": " << s << "," << t << endl;
            x = xmin + s*(xmax-xmin);
            y = flower(x) + t*(fupper(x) - flower(x));
            this->DOF(i)[0] = x;
            this->DOF(i)[1] = y;
        }

    }
}


}
