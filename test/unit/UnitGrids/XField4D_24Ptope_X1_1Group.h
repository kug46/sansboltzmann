// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD4D_24PTOPE_X1_1GROUP_H
#define XFIELD4D_24PTOPE_X1_1GROUP_H

#include "Field/XFieldSpacetime.h"

namespace SANS
{
/*
 * A unit grid that consists of a 24 pentatopes arranged into a unit tessaract.
 *
 * <<salvador_dali.jpg>>
 *
 */


class XField4D_24Ptope_X1_1Group : public XField<PhysD4, TopoD4>
{
public:
  XField4D_24Ptope_X1_1Group();
};

}

#endif // XFIELD4D_24PTOPE_X1_1GROUP_H
