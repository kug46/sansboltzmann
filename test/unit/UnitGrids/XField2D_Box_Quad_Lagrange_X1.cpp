// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_Box_Quad_Lagrange_X1.h"

#include "Field/Partition/XField_Lagrange.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// create triangle grid in a box with ii x jj x 2 (triangle) elements

const int XField2D_Box_Quad_Lagrange_X1::iBottom = 0;
const int XField2D_Box_Quad_Lagrange_X1::iRight  = 1;
const int XField2D_Box_Quad_Lagrange_X1::iTop    = 2;
const int XField2D_Box_Quad_Lagrange_X1::iLeft   = 3;

//---------------------------------------------------------------------------//
XField2D_Box_Quad_Lagrange_X1::
XField2D_Box_Quad_Lagrange_X1( mpi::communicator comm,
                               const int ii, const int jj,
                               const std::array<bool,2> periodic ) :
  XField2D_Box_Quad_Lagrange_X1(comm, ii, jj, 0,1, 0,1, periodic)
{
}

//---------------------------------------------------------------------------//
XField2D_Box_Quad_Lagrange_X1::
XField2D_Box_Quad_Lagrange_X1(mpi::communicator comm, int ii, int jj,
                              Real xmin, Real xmax,
                              Real ymin, Real ymax,
                              const std::array<bool,2> periodic ) : XField<PhysD2,TopoD2>(comm)
{
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = xmin + (xmax - xmin)*i/Real(ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = ymin + (ymax - ymin)*j/Real(jj);

  generateGrid(comm, xvec, yvec, periodic);
}

//---------------------------------------------------------------------------//
XField2D_Box_Quad_Lagrange_X1::
XField2D_Box_Quad_Lagrange_X1( mpi::communicator comm,
                               const std::vector<Real>& xvec,
                               const std::vector<Real>& yvec,
                               const std::array<bool,2> periodic ) : XField<PhysD2,TopoD2>(comm)
{
 std::size_t ii = xvec.size() - 1;
 std::size_t jj = yvec.size() - 1;

 //Check ascending order
 for (std::size_t i = 0; i < ii; i++)
   SANS_ASSERT( xvec[i] < xvec[i+1] );

 for (std::size_t j = 0; j < jj; j++)
   SANS_ASSERT( yvec[j] < yvec[j+1] );

 generateGrid(comm, xvec, yvec, periodic);
}

//---------------------------------------------------------------------------//
void
XField2D_Box_Quad_Lagrange_X1::
generateGrid( mpi::communicator comm,
             const std::vector<Real>& xvec,
             const std::vector<Real>& yvec,
             const std::array<bool,2> periodic )
{
  SANS_ASSERT( xvec.size() > 0 && yvec.size() > 0 );

  int order = 1;

  int ii = (int) xvec.size() - 1;
  int jj = (int) yvec.size() - 1;

  int nnode = (ii*order + 1)*(jj*order + 1);
  int nCell = ii*jj;

  XField_Lagrange<PhysD2> xfldin(comm);

  // create the grid-coordinate DOF arrays
  xfldin.sizeDOF( nnode );

  const int joffset = (ii*order + 1);

  if (comm.rank() == 0)
  {
    for (int j = 0; j < (jj*order + 1); j++)
    {
      for (int i = 0; i < (ii*order + 1); i++)
      {
        xfldin.addDOF( { xvec[i], yvec[j] } );
      }
    }
  }

  // Start the process of adding cells
  xfldin.sizeCells(nCell);

  // Add elements to rank 0
  if (comm.rank() == 0)
  {
    int group = 0;
    for (int j = 0; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        /* Node indexes in the grid

              3-----------2
              |           |
              |           |
              |           |
              |           |
              |           |
              0-----------1
         */

        int n0 = (j*order)*joffset + i*order;
        int n1 = n0 + 1;
        int n2 = n1 + joffset;
        int n3 = n0 + joffset;

        // add the indices to the group
        xfldin.addCell(group, eQuad, order, {n0, n1, n2, n3});
      }
    }
  }

  // Start the process of adding boundary trace elements
  xfldin.sizeBoundaryTrace(2*ii + 2*jj);

  // Add elements to rank 0
  if (comm.rank() == 0)
  {
    // lower boundary
    {
      int group = iBottom;

      int j = 0;
      for (int i = 0; i < ii; i++)
      {
        int n0 = (j*order)*joffset + i*order;
        int n1 = n0 + 1;
        //int n2 = n1 + (ii*order + 1);
        //int n3 = n0 + (ii*order + 1);

        xfldin.addBoundaryTrace( group, eLine, {n0, n1} );
      }
    }

    // right boundary
    {
      int group = iRight;

      int i = ii-1;
      for (int j = 0; j < jj; j++)
      {
        int n0 = (j*order)*joffset + i*order;
        int n1 = n0 + 1;
        int n2 = n1 + joffset;
        //int n3 = n0 + (ii*order + 1);

        xfldin.addBoundaryTrace( group, eLine, {n1, n2} );
      }
    }

    // upper boundary
    {
      int group = iTop;

      int j = jj - 1;
      for (int i = ii-1; i >= 0; i--)
      {
        int n0 = (j*order)*joffset + i*order;
        int n1 = n0 + 1;
        int n2 = n1 + joffset;
        int n3 = n0 + joffset;

        xfldin.addBoundaryTrace( group, eLine, {n2, n3} );
      }
    }

    // left boundary
    {
      int group = iLeft;

      int i = 0;
      for (int j = jj-1; j >= 0; j--)
      {
        int n0 = (j*order)*joffset + i*order;
        //int n1 = n0 + 1;
        //int n2 = n1 + joffset;
        int n3 = n0 + joffset;

        xfldin.addBoundaryTrace( group, eLine, {n3, n0} );
      }
    }
  }


  std::vector<PeriodicBCNodeMap> periodicity;

  // Construct periodic information if requested
  if (comm.rank() == 0)
  {
    // x-min to x-max periodicity
    if (periodic[0])
    {
      int groupR = iLeft;
      int groupL = iRight;

      periodicity.emplace_back(groupL, groupR);
      PeriodicBCNodeMap& period = periodicity.back();

      for (int j = 0; j < jj+1; j++)
      {
        int iR = 0;
        int iL = ii;
        {
          const int nL = (j*order)*joffset + iL;
          const int nR = (j*order)*joffset + iR;

          period.mapLtoR[nL] = nR;
        }
      }
    }

    // y-min to y-max periodicity
    if (periodic[1])
    {
      int groupR = iBottom;
      int groupL = iTop;

      periodicity.emplace_back(groupL, groupR);
      PeriodicBCNodeMap& period = periodicity.back();

      int jR = 0;
      int jL = jj;
      {
        for (int i = 0; i < ii+1; i++)
        {
          const int nL = (jL*order)*joffset + i;
          const int nR = (jR*order)*joffset + i;

          period.mapLtoR[nL] = nR;
        }
      }
    }
  }

  // Create the boundary periodicity
  xfldin.addBoundaryPeriodicity(periodicity);

  this->buildFrom( xfldin );
}

} //namespace SANS
