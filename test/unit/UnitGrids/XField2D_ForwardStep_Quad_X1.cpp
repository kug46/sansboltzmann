// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "unit/UnitGrids/XField2D_ForwardStep_Quad_X1.h"

#include "Field/Partition/XField_Lagrange.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "tools/output_std_vector.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

const int XField2D_ForwardStep_Quad_X1::iXmin = 0;
const int XField2D_ForwardStep_Quad_X1::iXmax = 1;
const int XField2D_ForwardStep_Quad_X1::iYmin = 2;
const int XField2D_ForwardStep_Quad_X1::iYmax = 3;
//---------------------------------------------------------------------------//
XField2D_ForwardStep_Quad_X1::
XField2D_ForwardStep_Quad_X1( mpi::communicator comm,
                     const int ii, const int jj,
                     Real xmin, Real xmax,
                     Real ymin, Real ymax,
                     Real stepHeight, Real stepOffset ) : XField<PhysD2,TopoD2>(comm)
{
  SANS_ASSERT( ii > 0 && jj > 0);
  //SANS_ASSERT_MSG( static_cast<int>(stepHeight/(ymax - ymin)*jj) == (stepHeight/(ymax - ymin)*jj), "Inputs wont form conforming mesh");
  //SANS_ASSERT_MSG( static_cast<int>(stepOffset/(xmax - xmin)*ii) == (stepOffset/(xmax - xmin)*ii), "Inputs wont form conforming mesh");

  XField_Lagrange<PhysD2> xfldin(comm);

  int order = 1;

  Real rLower = stepOffset/(xmax - xmin)*ii;
  int iLower = static_cast<int>(ceil(rLower));
  int jLower = stepHeight/(ymax - ymin)*jj;
  
  int iUpper = ii;
  int jUpper = jj - jLower;

  int nnode = (iUpper + 1) * (jUpper + 1) + (iLower+1) * (jLower);
  std::cout << "iLower: " << iLower << std::endl;
  std::cout << "jLower: " << jLower << std::endl;
  std::cout << "iUpper: " << iUpper << std::endl;
  std::cout << "jUpper: " << jUpper << std::endl;
  int nCell = iUpper * jUpper + iLower * jLower;
  std::cout << "nnode: " << nnode << std::endl;
  std::cout << "nCell: " << nCell << std::endl;

  // create the grid-coordinate DOF arrays
  xfldin.sizeDOF( nnode );

  // Add DOFs to rank 0
  if (comm.rank() == 0)
  {
    int counter = 0;
    Real x, y;
    for (int j = 0; j < jLower; j++)
    {
      for (int i = 0; i < iLower + 1; i++)
      {
        x = xmin + (xmax - xmin)*i/Real(ii);
        y = ymin + (ymax - ymin)*j/Real(jj);
        //std:: cout << "x: " << x << " y: " << y << std::endl;
        xfldin.addDOF( {x, y} );
        counter++;
      }
    }
    for (int j = jLower; j < jj + 1; j++)
    {
      for (int i = 0; i < ii + 1; i++)
      {
        x = xmin + (xmax - xmin)*i/Real(ii);
        y = ymin + (ymax - ymin)*j/Real(jj);
        //std:: cout << "x: " << x << " y: " << y << std::endl;
        xfldin.addDOF( {x, y} );
        counter++;
      }
    }
    std::cout << "counter: " << counter << std::endl;
  }

  // Start the process of adding cells
  xfldin.sizeCells(nCell);

  // Add elements to rank 0
  if (comm.rank() == 0)
  {
    int group = 0;
    int counter = 0;
    int n0 = 0;
    int joffset = (iLower + 1);
    for (int j = 0; j < jLower; j++)
    {
      for (int i = 0; i < iLower; i++)
      {
        //All the nodes that make up an individial Quad
        std::vector<int> Quadnodes = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 1,
                                      n0 + joffset + 0 };

        // add the indices to the group
        //std::cout << "Quadnodes1 : " << Quadnodes << std::endl;
        xfldin.addCell(group, eQuad, order, Quadnodes);
        counter++;
        n0 += 1;
      }
      n0 += 1;
    }
    joffset = (ii + 1);
    for (int j = jLower; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        //All the nodes that make up an individial Quad
        std::vector<int> Quadnodes = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 1,
                                      n0 + joffset + 0};

        // add the indices to the group
        //std::cout << "Quadnodes2: " << Quadnodes << std::endl;
        xfldin.addCell(group, eQuad, order, Quadnodes);
        counter++;
        n0 += 1;
      }
      n0 += 1;
    }
    std::cout << "counter: " << counter << std::endl;
  } // rank == 0


  // Start the process of adding boundary trace elements
  xfldin.sizeBoundaryTrace(2*(ii + jj));
  std::cout << "nTraces: " << 2*(ii + jj) << std::endl;

  // Add elements to rank 0
  if (comm.rank() == 0)
  {
    const int (*TraceNodes)[Line::NTrace] = TraceToCellRefCoord<Line, TopoD2, Quad>::TraceNodes;

    int faceL;

    std::vector<int> faceNodes(Line::NNode);

    // x-min boundary
    {
      int counter = 0;
      int n0 = 0;
      int joffset = (iLower + 1);
      for (int j = 0; j < jLower; j++)
      {
        for (int i = 0; i < iLower; i++)
        {
          //All the nodes that make up an individial Quad
          std::vector<int> Quadnodes = { n0 + 0,
                                        n0 + 1,
                                        n0 + joffset + 1,
                                        n0 + joffset + 0 };

          if (i == 0)
          {
            // We're on the LHS
            int group = iXmin;
            // add the indices to the group
            faceL = 3; //Quad s-min
            for (int n = 0; n < Line::NNode; n++)
              faceNodes[n] = Quadnodes[ TraceNodes[faceL][n] ];

            //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
            xfldin.addBoundaryTrace( group, eLine, faceNodes );
            counter++;
          }
          
          if (i == iLower - 1)
          {
            // We're on the RHS
            int group = iYmin;
            // add the indices to the group
            faceL = 1; //Quad s-max
            for (int n = 0; n < Line::NNode; n++)
              faceNodes[n] = Quadnodes[ TraceNodes[faceL][n] ];

            //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
            xfldin.addBoundaryTrace( group, eLine, faceNodes );
            counter++;
          }
          
          if (j == 0)
          {
            // We're on the bottom
            int group = iYmin;
            // add the indices to the group
            faceL = 0; //Quad t-min
            for (int n = 0; n < Line::NNode; n++)
              faceNodes[n] = Quadnodes[ TraceNodes[faceL][n] ];

            //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
            xfldin.addBoundaryTrace( group, eLine, faceNodes );
            counter++;
          }
          
          // This loop doesn't get to j-max

          n0 += 1;
        }
        n0 += 1;
      }
      joffset = (ii + 1);
      for (int j = jLower; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          //All the nodes that make up an individial Quad
          std::vector<int> Quadnodes = { n0 + 0,
                                        n0 + 1,
                                        n0 + joffset + 1,
                                        n0 + joffset + 0};


          if (i == 0)
          {
            // We're on the LHS
            int group = iXmin;
            // add the indices to the group
            faceL = 3; //Quad s-min
            for (int n = 0; n < Line::NNode; n++)
              faceNodes[n] = Quadnodes[ TraceNodes[faceL][n] ];

            //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
            xfldin.addBoundaryTrace( group, eLine, faceNodes );
            counter++;
          }
          
          if (i == ii - 1)
          {
            // We're on the RHS
            int group = iXmax;
            // add the indices to the group
            faceL = 1; //Quad s-max
            for (int n = 0; n < Line::NNode; n++)
              faceNodes[n] = Quadnodes[ TraceNodes[faceL][n] ];

            //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
            xfldin.addBoundaryTrace( group, eLine, faceNodes );
            counter++;
          }
          
          if (j == jLower && i >= iLower)
          {
            // We're on the bottom
            int group = iYmin;
            // add the indices to the group
            faceL = 0; //Quad t-min
            for (int n = 0; n < Line::NNode; n++)
              faceNodes[n] = Quadnodes[ TraceNodes[faceL][n] ];

            //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
            xfldin.addBoundaryTrace( group, eLine, faceNodes );
            counter++;
          }
          
          if (j == jj - 1)
          {
            // We're on the top
            int group = iYmax;
            // add the indices to the group
            faceL = 2; //Quad t-min
            for (int n = 0; n < Line::NNode; n++)
              faceNodes[n] = Quadnodes[ TraceNodes[faceL][n] ];

            //std::cout << "Trace " << group << ": " << faceNodes << std::endl;
            xfldin.addBoundaryTrace( group, eLine, faceNodes );
            counter++;
          }
          n0 += 1;
        }
        n0 += 1;
      }
      std::cout << "counter: " << counter << std::endl;
    }
  }
  // Finalize the grid construction
  std::cout << "Constructing grid" << std::endl;
  this->buildFrom( xfldin );
  std::cout << "Constructed grid" << std::endl;
}

}
