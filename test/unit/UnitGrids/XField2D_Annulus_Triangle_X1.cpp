// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField2D_Annulus_Triangle_X1.h"

#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// triangle grids of a portion of an annulus
//
// configuration inherited from XField2D_Box_Quad_X1

XField2D_Annulus_Triangle_X1::XField2D_Annulus_Triangle_X1( const int ii, const int jj,
                                                            const Real R1, const Real h,
                                                            const Real thetamin, const Real thetamax )
 : XField2D_Box_Triangle_X1(ii, jj)
{
//  SANS_ASSERT_MSG( jj == 1, "jj == 1 is required (single-layered quad grid)" );
  SANS_ASSERT_MSG( thetamin < thetamax, "thetamin < thetamax is required" );

  // theta-distribution of nodes
  Real dtheta = (thetamax - thetamin)*PI/180.0 / static_cast<Real>(ii);  // increment
  Real dh = h / static_cast<Real>(jj);

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      DOF(j*(ii+1) + i)[0] = (R1+h - j*dh)*cos( thetamin + i * dtheta );
      DOF(j*(ii+1) + i)[1] = (R1+h - j*dh)*sin( thetamin + i * dtheta );
    }
  }

//#undef USE_GGP
#ifdef USE_GGP
  std::fstream fout( "tmp/cubic.ggp", std::fstream::out );
  fout.precision(16);
  for (j = 0; j < jj+1; j++)
  {
    fout << "j" << j << std::endl;
    fout << "i j x y" << std::endl;
    for (i = 0; i < ii+1; i++)
      fout << i+1 << " " << j+1 << " " << DOF(j*(ii+1) + i)[0] << " " << DOF(j*(ii+1) + i)[1] << std::endl;
    fout << "*EOF" << std::endl;
  }
#endif  // USE_GGP


  //Check that the grid is correct
  checkGrid();
}


}  // namespace SANS
