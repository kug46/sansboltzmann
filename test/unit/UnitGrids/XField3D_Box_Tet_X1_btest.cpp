// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/linspace.h"

#include "Field/output_Tecplot.h"

#include "XField3D_Box_Tet_X1.h"

#include "unit/Field/XField3D_CheckTraceCoord3D_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_static_test )
{
  BOOST_CHECK_EQUAL( XField3D_Box_Tet_X1::iXmin, 0 );
  BOOST_CHECK_EQUAL( XField3D_Box_Tet_X1::iXmax, 1 );
  BOOST_CHECK_EQUAL( XField3D_Box_Tet_X1::iYmin, 2 );
  BOOST_CHECK_EQUAL( XField3D_Box_Tet_X1::iYmax, 3 );
  BOOST_CHECK_EQUAL( XField3D_Box_Tet_X1::iZmin, 4 );
  BOOST_CHECK_EQUAL( XField3D_Box_Tet_X1::iZmax, 5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_test1 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  const int ii = 1;
  const int jj = 1;
  const int kk = 1;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1 xfld( comm, ii, jj, kk );

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );

  BOOST_CHECK_THROW( xfld.nDOFCellGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFInteriorTraceGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFBoundaryTraceGroup(0), DeveloperException );

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_test2 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  const int ii = 2;
  const int jj = 1;
  const int kk = 1;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1 xfld( comm, ii, jj, kk );

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_test3 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  const int ii = 1;
  const int jj = 2;
  const int kk = 1;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1 xfld( comm, ii, jj, kk );

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_test4 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  const int ii = 1;
  const int jj = 1;
  const int kk = 2;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1 xfld( comm, ii, jj, kk );

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_test5 )
{
  // global communicator
  mpi::communicator world;

  const int ii = 2;
  const int jj = 3;
  const int kk = 4;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );
  //output_Tecplot(xfld, "tmp/xfld_rank" + std::to_string(world.rank()) + ".dat");

#ifndef SANS_MPI
  BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );

  const int joffset = (ii+1);
  const int koffset = (ii+1)*(jj+1);

  for (int k = 0; k < kk+1; k++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      for (int i = 0; i < ii+1; i++)
      {
        SANS_CHECK_CLOSE( xfld.DOF(k*koffset + j*joffset + i)[0], i/Real(ii), 1e-13, 1e-13);
        SANS_CHECK_CLOSE( xfld.DOF(k*koffset + j*joffset + i)[1], j/Real(jj), 1e-13, 1e-13);
        SANS_CHECK_CLOSE( xfld.DOF(k*koffset + j*joffset + i)[2], k/Real(kk), 1e-13, 1e-13);
      }
    }
  }
#endif

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_Periodic_test1 )
{
  // global communicator
  mpi::communicator world;

  const int ii = 1; //Having 1 here tests corner situations
  const int jj = 4;
  const int kk = 5;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk,
                            0, 1,
                            0, 1,
                            0, 1,
                            {{true, false, false}} );

#ifndef SANS_MPI
  BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );
#endif

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // check the left cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iXmin).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iXmax).nElem() );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
      CheckInteriorTraceCoordinates3D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_Periodic_test2 )
{
  // global communicator
  mpi::communicator world;

  const int ii = 3;
  const int jj = 4;
  const int kk = 5;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk,
                            0, 1,
                            0, 1,
                            0, 1,
                            {{false, true, false}} );

#ifndef SANS_MPI
  BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );
#endif

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // check the left cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iYmin).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iYmax).nElem() );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
      CheckInteriorTraceCoordinates3D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_Periodic_test3 )
{
  // global communicator
  mpi::communicator world;

  const int ii = 3;
  const int jj = 4;
  const int kk = 5;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk,
                            0, 1,
                            0, 1,
                            0, 1,
                            {{false, false, true}} );

#ifndef SANS_MPI
  BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );
#endif

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // check the left cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iZmin).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iZmax).nElem() );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
      CheckInteriorTraceCoordinates3D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_Periodic_test4 )
{
  // global communicator
  mpi::communicator world;

  const int ii = 5;
  const int jj = 3;
  const int kk = 4;

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk,
                            {{true, true, true}} );

#ifndef SANS_MPI
  BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );
#endif

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // check the left cell group
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iXmin).getGroupRight() );
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iYmin).getGroupRight() );
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iZmin).getGroupRight() );

  // the periodic BC should have no elements
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iXmax).nElem() );
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iYmax).nElem() );
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iZmax).nElem() );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
      CheckInteriorTraceCoordinates3D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_Periodic_test5 )
{
  // global communicator
  mpi::communicator world;

  for (int power = 1; power <= 3; power++)
  {
    const int ii = pow(2,power);
    const int jj = ii;
    const int kk = ii;

    // Simply creating the grid triggers connectivity and positive volume checks
    XField3D_Box_Tet_X1 xfld( world, ii, jj, kk,
                              {{true, true, true}} );

#ifndef SANS_MPI
    BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
    BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );
#endif

    // volume field variable

    BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
    BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

    // interior-face field variable

    BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
    for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
      BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

    // boundary-face field variable

    BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
    for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
      BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

    // check the left cell group
    BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iXmin).getGroupRight() );
    BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iYmin).getGroupRight() );
    BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iZmin).getGroupRight() );

    // the periodic BC should have no elements
    BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iXmax).nElem() );
    BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iYmax).nElem() );
    BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(xfld.iZmax).nElem() );


    const Real small_tol = 1e-11;
    const Real close_tol = 1e-11;

    // check that grid trace is identical for adjacent elements
    for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
        CheckInteriorTraceCoordinates3D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
        CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

    //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_X1_TensorProduct_test1 )
{
  // global communicator
  mpi::communicator world;

  std::vector<Real> xvec = {1.0, 1.5, 2.5, 2.75, 4.0};
  std::vector<Real> yvec = {0.3, 0.9, 1.1, 1.5};
  std::vector<Real> zvec = {-1.0, -0.5, 0.5};

  // Simply creating the grid triggers connectivity and positive volume checks
  XField3D_Box_Tet_X1 xfld( world, xvec, yvec, zvec );

#ifndef SANS_MPI
  const int ii = (int) xvec.size() - 1;
  const int jj = (int) yvec.size() - 1;
  const int kk = (int) zvec.size() - 1;

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1)*(kk+1), xfld.nDOF() );
  BOOST_CHECK_EQUAL( 6*ii*jj*kk, xfld.nElem() );

  BOOST_CHECK_THROW( xfld.nDOFCellGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFInteriorTraceGroup(0), DeveloperException );
  BOOST_CHECK_THROW( xfld.nDOFBoundaryTraceGroup(0), DeveloperException );

  const int joffset = (ii+1);
  const int koffset = (ii+1)*(jj+1);

  for (int i = 0; i < ii+1; i++)
  {
    for (int j = 0; j < jj+1; j++)
    {
      for (int k = 0; k < kk+1; k++)
      {
        SANS_CHECK_CLOSE( xfld.DOF(k*koffset + j*joffset + i)[0], xvec[i], 1e-13, 1e-13);
        SANS_CHECK_CLOSE( xfld.DOF(k*koffset + j*joffset + i)[1], yvec[j], 1e-13, 1e-13);
        SANS_CHECK_CLOSE( xfld.DOF(k*koffset + j*joffset + i)[2], zvec[k], 1e-13, 1e-13);
      }
    }
  }
#endif

  // volume field variable

  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  // interior-face field variable

  BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  // boundary-face field variable

  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
