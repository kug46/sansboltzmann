// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "XField3D_5Tet_X1_1Group_AllOrientations.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

/*
  See XField3D_5Tet_X1_1Group_AllOrientations.png for image of mesh
*/

void
XField3D_5Tet_X1_1Group_AllOrientations::XField3D_5Tet_X1_1Group_AllOrientations_Negative(int orient)
{
  //Note: The center tet always has orientation +1, but the outer tets will have an orientation <orient>.

  SANS_ASSERT_MSG( orient >= -3 && orient <= -1, "Invalid face orientation. Supported negative orientations are -1, -2 or -3.");

  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(8);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(1);
  resizeCellGroups(1);

  // nodal coordinates for the 5 tets.
  DOF(0) = {0, 0, 0};
  DOF(1) = {1, 0, 0};
  DOF(2) = {0, 1, 0};
  DOF(3) = {0, 0, 1};

  DOF(4) = { 1, 1, 1};
  DOF(5) = {-1, 0, 0};
  DOF(6) = { 0,-1, 0};
  DOF(7) = { 0, 0,-1};


  // volume field variable
  FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1, 5 );

  //Original configuration: center tet has orientation +1, all neighbors have orientation -1
  const int tets_orient1[5][4] = { {0, 1, 2, 3},
                                   {1, 2, 3, 4},
                                   {0, 3, 2, 5},
                                   {0, 1, 3, 6},
                                   {0, 2, 1, 7}};

  int tets[5][4];
  tets[0][0] = 0; tets[0][1] = 1; tets[0][2] = 2; tets[0][3] = 3;

  //Rotate neighbors so that they all have orientation <orient>
  for (int i=1; i<5; i++)
  {
    for (int j=0; j<3; j++)
    {
      tets[i][j] = tets_orient1[i][(j-orient-1)%3];
    }

    tets[i][3] = tets_orient1[i][3];
  }

  // set the cell processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 3 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 4 ).setRank( comm_rank );

  //element volume associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( tets[0], 4 );
  fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( tets[1], 4 );
  fldAssocCell.setAssociativity( 2 ).setNodeGlobalMapping( tets[2], 4 );
  fldAssocCell.setAssociativity( 3 ).setNodeGlobalMapping( tets[3], 4 );
  fldAssocCell.setAssociativity( 4 ).setNodeGlobalMapping( tets[4], 4 );

  nElem_ = fldAssocCell.nElem();

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  // interior-face field variable

  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocIface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 4 );

  fldAssocIface.setGroupLeft( 0 );
  fldAssocIface.setGroupRight( 0 );

  // face-cell associativity
  int faceNodes[3];
  int elemL, elemR;
  int faceL, faceR;

  // Face between center and front tets - (orientation of face from front tet = -3)
  elemL = 0; faceL = 0;
  elemR = 1; faceR = 3;
  for (int i = 0; i < 3; i++)
  {
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];
//    std::cout<<faceNodes[i]<<std::endl;
  }

  fldAssocIface.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 0 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 0 );
  fldAssocIface.setElementRight( elemR, 0 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 0 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 0 );

//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).orientation <<std::endl;
//
//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).orientation <<std::endl;

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(0).orientation, faceR );


  // Face between center and left tets - (orientation of face from left tet = -1)
  elemL = 0; faceL = 1;
  elemR = 2; faceR = 3;
  for (int i = 0; i < 3; i++)
  {
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];
//    std::cout<<faceNodes[i]<<std::endl;
  }

  fldAssocIface.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 1 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 1 );
  fldAssocIface.setElementRight( elemR, 1 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 1 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 1 );

//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).orientation <<std::endl;
////
//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).orientation <<std::endl;

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(1).orientation, faceR );


  // Face between center and bottom tets - (orientation of face from bottom tet = -2)
  elemL = 0; faceL = 2;
  elemR = 3; faceR = 3;
  for (int i = 0; i < 3; i++)
  {
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];
//    std::cout<<faceNodes[i]<<std::endl;
  }

  fldAssocIface.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 2 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 2 );
  fldAssocIface.setElementRight( elemR, 2 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 2 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 2 );

//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).orientation <<std::endl;
//
//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).orientation <<std::endl;

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(2).orientation, faceR );


  // Face between center and rear tets - (orientation of face from rear tet = -3)
  elemL = 0; faceL = 3;
  elemR = 4; faceR = 3;
  for (int i = 0; i < 3; i++)
  {
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];
//    std::cout<<faceNodes[i]<<std::endl;
  }

  fldAssocIface.setAssociativity( 3 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 3 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 3 );
  fldAssocIface.setElementRight( elemR, 3 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 3 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 3 );

//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).orientation <<std::endl;
//
//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).orientation <<std::endl;

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(3).orientation, faceR );


  //Create the interior face group
  interiorTraceGroups_[0] = new FieldTraceGroupType<Triangle>( fldAssocIface );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);


  // boundary-face field variable
  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 12 );

  fldAssocBface.setGroupLeft( 0 );

  int faceB = 0;

  for (int elemL=1; elemL<5; elemL++)
  {
    for (int faceL=0; faceL<3; faceL++)
    {

      for (int i = 0; i < 3; i++)
        faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

      fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
      fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
      fldAssocBface.setElementLeft( elemL, faceB );
      fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

      // face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
      faceB++;

    }
  }

  //Must be 12 boundary faces
  SANS_ASSERT( faceB == 12 );

  //Create the boundary group
  boundaryTraceGroups_[0] = new FieldTraceGroupType<Triangle>( fldAssocBface );
  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);

  //Finally create the cell groups now that all Face signs have been set
  cellGroups_[0] = new FieldCellGroupType<Tet>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}


void
XField3D_5Tet_X1_1Group_AllOrientations::XField3D_5Tet_X1_1Group_AllOrientations_Positive(int orient)
{
  //Note: The outer tets always have orientation +1, but now the center tet will have an orientation -<orient>.

  SANS_ASSERT_MSG( orient >= 1 && orient <= 3, "Invalid face orientation. Supported positive orientations are +1, +2 or +3.");

  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(8);

  //Create the element groups
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(1);
  resizeCellGroups(1);

  // nodal coordinates for the 5 tets.
  DOF(0) = {0, 0, 0};
  DOF(1) = {1, 0, 0};
  DOF(2) = {0, 1, 0};
  DOF(3) = {0, 0, 1};

  DOF(4) = { 1, 1, 1};
  DOF(5) = {-1, 0, 0};
  DOF(6) = { 0,-1, 0};
  DOF(7) = { 0, 0,-1};


  // volume field variable
  FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1, 5 );

  //Original configuration: center tet has orientation -1, all neighbors have orientation +1
  const int tets_orient1[5][4] = { {0, 1, 2, 3},
                                   {1, 2, 3, 4},
                                   {0, 3, 2, 5},
                                   {0, 1, 3, 6},
                                   {0, 2, 1, 7}};

  int tets[5][4];
  tets[0][0] = 0; tets[0][1] = 1; tets[0][2] = 2; tets[0][3] = 3;

  //Rotate neighbors so that the center tet has orientation <orient>
  for (int i=1; i<5; i++)
  {
    for (int j=0; j<3; j++)
    {
      tets[i][j] = tets_orient1[i][(j+orient-1)%3];
    }

    tets[i][3] = tets_orient1[i][3];
  }

  // set the cell processor rank
  fldAssocCell.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 3 ).setRank( comm_rank );
  fldAssocCell.setAssociativity( 4 ).setRank( comm_rank );

  //element volume associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( tets[0], 4 );
  fldAssocCell.setAssociativity( 1 ).setNodeGlobalMapping( tets[1], 4 );
  fldAssocCell.setAssociativity( 2 ).setNodeGlobalMapping( tets[2], 4 );
  fldAssocCell.setAssociativity( 3 ).setNodeGlobalMapping( tets[3], 4 );
  fldAssocCell.setAssociativity( 4 ).setNodeGlobalMapping( tets[4], 4 );

  nElem_ = 5;

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  // interior-face field variable

  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocIface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 4 );

  fldAssocIface.setGroupLeft( 0 );
  fldAssocIface.setGroupRight( 0 );

  // face-cell associativity
  int faceNodes[3];
  int elemL, elemR;
  int faceL, faceR;

  // Face between center and front tets - (orientation of face from front tet = -3)
  elemL = 1; faceL = 3;
  elemR = 0; faceR = 0;
  for (int i = 0; i < 3; i++)
  {
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];
//    std::cout<<faceNodes[i]<<std::endl;
  }

  fldAssocIface.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 0 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 0 );
  fldAssocIface.setElementRight( elemR, 0 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 0 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 0 );

//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).orientation <<std::endl;
//
//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).orientation <<std::endl;

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(0).orientation, faceR );


  // Face between center and left tets - (orientation of face from left tet = -1)
  elemL = 2; faceL = 3;
  elemR = 0; faceR = 1;
  for (int i = 0; i < 3; i++)
  {
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];
//    std::cout<<faceNodes[i]<<std::endl;
  }

  fldAssocIface.setAssociativity( 1 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 1 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 1 );
  fldAssocIface.setElementRight( elemR, 1 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 1 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 1 );

//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).orientation <<std::endl;
//
//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).orientation <<std::endl;

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(1).orientation, faceR );

  // Face between center and bottom tets - (orientation of face from bottom tet = -2)
  elemL = 3; faceL = 3;
  elemR = 0; faceR = 2;
  for (int i = 0; i < 3; i++)
  {
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];
//    std::cout<<faceNodes[i]<<std::endl;
  }

  fldAssocIface.setAssociativity( 2 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 2 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 2 );
  fldAssocIface.setElementRight( elemR, 2 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 2 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 2 );

//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).orientation <<std::endl;
//
//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).orientation <<std::endl;

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(2).orientation, faceR );


  // Face between center and rear tets - (orientation of face from rear tet = -3)
  elemL = 4; faceL = 3;
  elemR = 0; faceR = 3;
  for (int i = 0; i < 3; i++)
  {
    faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];
//    std::cout<<faceNodes[i]<<std::endl;
  }

  fldAssocIface.setAssociativity( 3 ).setRank( comm_rank );
  fldAssocIface.setAssociativity( 3 ).setNodeGlobalMapping( faceNodes, 3 );
  fldAssocIface.setElementLeft( elemL, 3 );
  fldAssocIface.setElementRight( elemR, 3 );
  fldAssocIface.setCanonicalTraceLeft(  TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4), 3 );
  fldAssocIface.setCanonicalTraceRight( TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4), 3 );

//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemL], 4).orientation <<std::endl;
//
//  std::cout<< TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).trace << "," <<
//              TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(faceNodes, 3, tets[elemR], 4).orientation <<std::endl;

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
  fldAssocCell.setAssociativity( elemR ).setFaceSign( fldAssocIface.getCanonicalTraceRight(3).orientation, faceR );


  //Create the interior face group
  interiorTraceGroups_[0] = new FieldTraceGroupType<Triangle>( fldAssocIface );
  interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);


  // boundary-face field variable
  FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType fldAssocBface( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 12 );

  fldAssocBface.setGroupLeft( 0 );

  int faceB = 0;

  for (int elemL=1; elemL<5; elemL++)
  {
    for (int faceL=0; faceL<3; faceL++)
    {

      for (int i = 0; i < 3; i++)
        faceNodes[i] = tets[elemL][TraceNodes[faceL][i]];

      fldAssocBface.setAssociativity( faceB ).setRank( comm_rank );
      fldAssocBface.setAssociativity( faceB ).setNodeGlobalMapping( faceNodes, 3 );
      fldAssocBface.setElementLeft( elemL, faceB );
      fldAssocBface.setCanonicalTraceLeft( CanonicalTraceToCell(faceL, 1), faceB );

      // face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( elemL ).setFaceSign( +1, faceL );
      faceB++;

    }
  }

  //Must be 12 boundary faces
  SANS_ASSERT( faceB == 12 );

  //Create the boundary group
  boundaryTraceGroups_[0] = new FieldTraceGroupType<Triangle>( fldAssocBface );
  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);

  //Finally create the cell groups now that all Face signs have been set
  cellGroups_[0] = new FieldCellGroupType<Tet>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}


}
