// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <string>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "XField3D_GaussianBump_Xq.h"

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_GaussianBump_Xq_test )
{
  const int ii = 8;
  const int jj = 4;
  const int kk = 4;

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  // Check is called at the end of the constructor, hence this is actually tested
  XField3D_GaussianBump_Xq xfld( comm, ii, jj, kk );

  int order = 2;
  XField3D_GaussianBump_Xq xfld2( comm, ii, jj, kk, order );

  order = 3;
  XField3D_GaussianBump_Xq xfld3( comm, ii, jj, kk, order );

  order = 4;
  XField3D_GaussianBump_Xq xfld4( comm, ii, jj, kk, order );
}

BOOST_AUTO_TEST_CASE( XField3D_GaussianBump_Xq_Series_test )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  //test high order constructor
  for (int order = 1; order < 4; order++)
  {
    const int ii = 8;
    const int jj = 4;
    const int kk = 4;

    // Check is called at the end of the constructor, hence this is actually tested
    XField3D_GaussianBump_Xq xfld(comm, ii, jj, kk, order);

#if 0
    string filename = "tmp/Gaussian_Q";
    filename += to_string(order);
    filename += "_";
    filename += to_string(ii);
    filename += "x";
    filename += to_string(jj);
    filename += "x";
    filename += to_string(kk);
    filename += ".grm";
    output_Tecplot(xfld, filename);
#endif

  }

}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
