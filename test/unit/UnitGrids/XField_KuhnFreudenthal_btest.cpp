// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// UnitGrids_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/linspace.h"

#include "XField_KuhnFreudenthal.h"
#include "XField4D_Box_Ptope_X1.h"

#include "unit/Field/XField1D_CheckTraceCoord1D_btest.h"
#include "unit/Field/XField2D_CheckTraceCoord2D_btest.h"
#include "unit/Field/XField3D_CheckTraceCoord3D_btest.h"
#include "unit/Field/XField4D_CheckTraceCoord4D_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( UnitGrids_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_KuhnFreudenthal_test_2d )
{
  #ifdef SANS_AVRO
  mpi::communicator world;
  std::vector<int> dims(2,5);

  XField_KuhnFreudenthal<PhysD2,TopoD2> mesh( world , dims );
  #endif
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_KuhnFreudenthal_test_3d )
{
  #ifdef SANS_AVRO
  mpi::communicator world;
  std::vector<int> dims(3,5);

  XField_KuhnFreudenthal<PhysD3,TopoD3> mesh( world , dims );
  #endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_KuhnFreudenthal_test_4d )
{
  #ifdef SANS_AVRO
  mpi::communicator world;
  std::vector<int> dims(4,2);

  XField_KuhnFreudenthal<PhysD4,TopoD4> mesh( world , dims );
  #endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_KuhnFreudenthal_test_4d_boundaries )
{
  #ifdef SANS_AVRO
  mpi::communicator world;
  std::vector<int> dims(4,6);

  typedef typename XField<PhysD4,TopoD4>::template FieldTraceGroupType<Tet> XFieldTraceGroupType;

  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld(world,dims);

  // loop through the boundary group and makes sure we are at x = 0
  {
    int imin = XField4D_Box_Ptope_X1::iXmin;
    const XFieldTraceGroupType& bgroup_xmin = xfld.template getBoundaryTraceGroup<Tet>(imin);
    for (int itrace=0;itrace<bgroup_xmin.nElem();itrace++)
    {
      // retrieve the indices of this trace
      std::vector<int> traceDOF( Tet::NNode );
      bgroup_xmin.associativity(itrace).getNodeGlobalMapping( traceDOF.data() , traceDOF.size() );

      // ensure the dof for these indices are actually at xmin
      for (std::size_t idof=0;idof<traceDOF.size();idof++)
      {
        BOOST_CHECK_CLOSE( bgroup_xmin.DOF(traceDOF[idof])[0] , 0 , 1e-12 ) ;
      }
    }
  }

  // loop through the boundary group and makes sure we are at x = 1
  {
    int imax = XField4D_Box_Ptope_X1::iXmax;
    const XFieldTraceGroupType& bgroup_xmax = xfld.template getBoundaryTraceGroup<Tet>(imax);
    for (int itrace=0;itrace<bgroup_xmax.nElem();itrace++)
    {
      // retrieve the indices of this trace
      std::vector<int> traceDOF( Tet::NNode );
      bgroup_xmax.associativity(itrace).getNodeGlobalMapping( traceDOF.data() , traceDOF.size() );

      // ensure the dof for these indices are actually at xmin
      for (int idof=0;idof<(int)traceDOF.size();idof++)
      {
        BOOST_CHECK_CLOSE( bgroup_xmax.DOF(traceDOF[idof])[0] , 1 , 1e-12 ) ;
      }
    }
  }

  // loop through the boundary group and makes sure we are at y = 0
  {
    int imin = XField4D_Box_Ptope_X1::iYmin;
    const XFieldTraceGroupType& bgroup_ymin = xfld.template getBoundaryTraceGroup<Tet>(imin);
    for (int itrace=0;itrace<bgroup_ymin.nElem();itrace++)
    {
      // retrieve the indices of this trace
      std::vector<int> traceDOF( Tet::NNode );
      bgroup_ymin.associativity(itrace).getNodeGlobalMapping( traceDOF.data() , traceDOF.size() );

      // ensure the dof for these indices are actually at xmin
      for (int idof=0;idof<(int)traceDOF.size();idof++)
      {
        BOOST_CHECK_CLOSE( bgroup_ymin.DOF(traceDOF[idof])[1] , 0 , 1e-12 ) ;
      }
    }
  }

  // loop through the boundary group and makes sure we are at y = 1
  {
    int imax = XField4D_Box_Ptope_X1::iYmax;
    const XFieldTraceGroupType& bgroup_ymax = xfld.template getBoundaryTraceGroup<Tet>(imax);
    for (int itrace=0;itrace<bgroup_ymax.nElem();itrace++)
    {
      // retrieve the indices of this trace
      std::vector<int> traceDOF( Tet::NNode );
      bgroup_ymax.associativity(itrace).getNodeGlobalMapping( traceDOF.data() , traceDOF.size() );

      // ensure the dof for these indices are actually at ymax
      for (int idof=0;idof<(int)traceDOF.size();idof++)
      {
        BOOST_CHECK_CLOSE( bgroup_ymax.DOF(traceDOF[idof])[1] , 1 , 1e-12 ) ;
      }
    }
  }

  // loop through the boundary group and makes sure we are at z = 0
  {
    int imin = XField4D_Box_Ptope_X1::iZmin;
    const XFieldTraceGroupType& bgroup_zmin = xfld.template getBoundaryTraceGroup<Tet>(imin);
    for (int itrace=0;itrace<bgroup_zmin.nElem();itrace++)
    {
      // retrieve the indices of this trace
      std::vector<int> traceDOF( Tet::NNode );
      bgroup_zmin.associativity(itrace).getNodeGlobalMapping( traceDOF.data() , traceDOF.size() );

      // ensure the dof for these indices are actually at zmin
      for (int idof=0;idof<(int)traceDOF.size();idof++)
      {
        BOOST_CHECK_CLOSE( bgroup_zmin.DOF(traceDOF[idof])[2] , 0 , 1e-12 ) ;
      }
    }
  }

  // loop through the boundary group and makes sure we are at z = 1
  {
    int imax = XField4D_Box_Ptope_X1::iZmax;
    const XFieldTraceGroupType& bgroup_zmax = xfld.template getBoundaryTraceGroup<Tet>(imax);
    for (int itrace=0;itrace<bgroup_zmax.nElem();itrace++)
    {
      // retrieve the indices of this trace
      std::vector<int> traceDOF( Tet::NNode );
      bgroup_zmax.associativity(itrace).getNodeGlobalMapping( traceDOF.data() , traceDOF.size() );

      // ensure the dof for these indices are actually at zmax
      for (int idof=0;idof<(int)traceDOF.size();idof++)
      {
        BOOST_CHECK_CLOSE( bgroup_zmax.DOF(traceDOF[idof])[2] , 1 , 1e-12 ) ;
      }
    }
  }

  // loop through the boundary group and makes sure we are at t = 0
  {
    int imin = XField4D_Box_Ptope_X1::iWmin;
    const XFieldTraceGroupType& bgroup_tmin = xfld.template getBoundaryTraceGroup<Tet>(imin);
    for (int itrace=0;itrace<bgroup_tmin.nElem();itrace++)
    {
      // retrieve the indices of this trace
      std::vector<int> traceDOF( Tet::NNode );
      bgroup_tmin.associativity(itrace).getNodeGlobalMapping( traceDOF.data() , traceDOF.size() );

      // ensure the dof for these indices are actually at tmin
      for (int idof=0;idof<(int)traceDOF.size();idof++)
      {
        BOOST_CHECK_CLOSE( bgroup_tmin.DOF(traceDOF[idof])[3] , 0 , 1e-12 ) ;
      }
    }
  }

  // loop through the boundary group and makes sure we are at t = 1
  {
    int imax = XField4D_Box_Ptope_X1::iWmax;
    const XFieldTraceGroupType& bgroup_tmax = xfld.template getBoundaryTraceGroup<Tet>(imax);
    for (int itrace=0;itrace<bgroup_tmax.nElem();itrace++)
    {
      // retrieve the indices of this trace
      std::vector<int> traceDOF( Tet::NNode );
      bgroup_tmax.associativity(itrace).getNodeGlobalMapping( traceDOF.data() , traceDOF.size() );

      // ensure the dof for these indices are actually at tmax
      for (int idof=0;idof<(int)traceDOF.size();idof++)
      {
        BOOST_CHECK_CLOSE( bgroup_tmax.DOF(traceDOF[idof])[3] , 1 , 1e-12 ) ;
      }
    }
  }

  #endif

}

BOOST_AUTO_TEST_CASE(XField_KuhnFreudenthal_CheckTrace_2D)
{

  #ifdef SANS_AVRO

  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;

  // global comm
  mpi::communicator world;

  // mesh specification
  int ii= 3;
  int jj= 3;
  std::vector<int> dims= {ii, jj};

  XField_KuhnFreudenthal<PhysDim, TopoDim> xfld(world, dims);

  // make sure that we have a real triangulation
  BOOST_CHECK_EQUAL(xfld.nCellGroups(), 1);
  if (xfld.getCellGroupBase(0).nElem() > 1)
    BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 1);
  else
    BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 0);
  BOOST_CHECK_EQUAL(xfld.nBoundaryTraceGroups(), 2*PhysDim::D); // 2*d

  const Real small_tol= 1e-11;
  const Real close_tol= 1e-11;

  // check that the grid trace is identical for adjascent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoDim>::apply(
        CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0, xfld.nInteriorTraceGroups() - 1)), xfld, xfld);

  for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0, xfld.nBoundaryTraceGroups() - 1)), xfld, xfld);

#endif

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(XField_KuhnFreudenthal_CheckTrace_3D)
{

  #ifdef SANS_AVRO

  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;

  // global comm
  mpi::communicator world;

  // mesh specification
  int ii= 3;
  int jj= 3;
  int kk= 3;
  std::vector<int> dims= {ii, jj, kk};

  XField_KuhnFreudenthal<PhysDim, TopoDim> xfld(world, dims);

  // make sure that we have a real triangulation
  BOOST_CHECK_EQUAL(xfld.nCellGroups(), 1);
  if (xfld.getCellGroupBase(0).nElem() > 1)
    BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 1);
  else
    BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 0);
  BOOST_CHECK_EQUAL(xfld.nBoundaryTraceGroups(), 2*PhysDim::D); // 2*d

  const Real small_tol= 1e-11;
  const Real close_tol= 1e-11;

  // check that the grid trace is identical for adjascent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoDim>::apply(
        CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0, xfld.nInteriorTraceGroups() - 1)), xfld, xfld);

  for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0, xfld.nBoundaryTraceGroups() - 1)), xfld, xfld);

#endif

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(XField_KuhnFreudenthal_CheckTrace_4D)
{

  #ifdef SANS_AVRO

  typedef PhysD4 PhysDim;
  typedef TopoD4 TopoDim;

  // global comm
  mpi::communicator world;

  // mesh specification
  int ii= 3;
  int jj= 3;
  int kk= 3;
  int mm= 3;
  std::vector<int> dims= {ii, jj, kk, mm};

  XField_KuhnFreudenthal<PhysDim, TopoDim> xfld(world, dims);

  // make sure that we have a real triangulation
  BOOST_CHECK_EQUAL(xfld.nCellGroups(), 1);
  if (xfld.getCellGroupBase(0).nElem() > 1)
    BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 1);
  else
    BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 0);
  BOOST_CHECK_EQUAL(xfld.nBoundaryTraceGroups(), 2*PhysDim::D); // 2*d

  const Real small_tol= 1e-11;
  const Real close_tol= 1e-11;

  // check that the grid trace is identical for adjascent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoDim>::apply(
        CheckInteriorTraceCoordinates4D(small_tol, close_tol, linspace(0, xfld.nInteriorTraceGroups() - 1)), xfld, xfld);

  for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
      CheckBoundaryTraceCoordinates4D(small_tol, close_tol, linspace(0, xfld.nBoundaryTraceGroups() - 1)), xfld, xfld);

#endif

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
