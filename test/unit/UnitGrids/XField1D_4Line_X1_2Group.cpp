// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField1D_4Line_X1_2Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
/*
      (0,0)   (0,1) | (1,0)   (1,1)
    0 ----- 1 ----- 2 ----- 3 ----- 4
*/

XField1D_4Line_X1_2Group::XField1D_4Line_X1_2Group()
{
  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(5);

  //Create the element groups
  resizeInteriorTraceGroups(3);
  resizeBoundaryTraceGroups(2);
  resizeCellGroups(2);

  // nodal coordinates for the two lines.
  DOF(0) = {0};
  DOF(1) = {1};
  DOF(2) = {2};
  DOF(3) = {3};
  DOF(4) = {4};

  // area field variable
  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell0( BasisFunctionLineBase::HierarchicalP1, 2 );

  //element processor ranks
  fldAssocCell0.setAssociativity( 0 ).setRank(0);
  fldAssocCell0.setAssociativity( 1 ).setRank(0);

  //element cell associativity
  fldAssocCell0.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
  fldAssocCell0.setAssociativity( 1 ).setNodeGlobalMapping( {1, 2} );

  cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell0 );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell1( BasisFunctionLineBase::HierarchicalP1, 2 );

  //element processor ranks
  fldAssocCell1.setAssociativity( 0 ).setRank(0);
  fldAssocCell1.setAssociativity( 1 ).setRank(0);

  //element cell associativity
  fldAssocCell1.setAssociativity( 0 ).setNodeGlobalMapping( {2, 3} );
  fldAssocCell1.setAssociativity( 1 ).setNodeGlobalMapping( {3, 4} );

  cellGroups_[1] = new FieldCellGroupType<Line>( fldAssocCell1 );
  cellGroups_[1]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell0.nElem() + fldAssocCell1.nElem();

  // interior-trace field variable

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode0( BasisFunctionNodeBase::P0, 1 );
  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode1( BasisFunctionNodeBase::P0, 1 );
  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode2( BasisFunctionNodeBase::P0, 1 );

  // node processor rank
  fldAssocInode0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocInode1.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocInode2.setAssociativity( 0 ).setRank( comm_rank );

  // node-element associativity
  fldAssocInode0.setAssociativity( 0 ).setNodeGlobalMapping( {2} );
  fldAssocInode0.setAssociativity( 0 ).setNormalSignL(  1 );
  fldAssocInode0.setAssociativity( 0 ).setNormalSignR( -1 );

  fldAssocInode1.setAssociativity( 0 ).setNodeGlobalMapping( {1} );
  fldAssocInode1.setAssociativity( 0 ).setNormalSignL(  1 );
  fldAssocInode1.setAssociativity( 0 ).setNormalSignR( -1 );

  fldAssocInode2.setAssociativity( 0 ).setNodeGlobalMapping( {3} );
  fldAssocInode2.setAssociativity( 0 ).setNormalSignL(  1 );
  fldAssocInode2.setAssociativity( 0 ).setNormalSignR( -1 );

  // node-to-cell connectivity
  fldAssocInode0.setGroupLeft( 0 );
  fldAssocInode0.setGroupRight( 1 );
  fldAssocInode0.setElementLeft( 1, 0 );
  fldAssocInode0.setElementRight( 0, 0 );
  fldAssocInode0.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );
  fldAssocInode0.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), 0 );

  fldAssocInode1.setGroupLeft( 0 );
  fldAssocInode1.setGroupRight( 0 );
  fldAssocInode1.setElementLeft( 0, 0 );
  fldAssocInode1.setElementRight( 1, 0 );
  fldAssocInode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );
  fldAssocInode1.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), 0 );

  fldAssocInode2.setGroupLeft( 1 );
  fldAssocInode2.setGroupRight( 1 );
  fldAssocInode2.setElementLeft( 0, 0 );
  fldAssocInode2.setElementRight( 1, 0 );
  fldAssocInode2.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );
  fldAssocInode2.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), 0 );

  FieldTraceGroupType<Line>* xfldInode0 = NULL;
  FieldTraceGroupType<Line>* xfldInode1 = NULL;
  FieldTraceGroupType<Line>* xfldInode2 = NULL;
  interiorTraceGroups_[0] = xfldInode0 = new FieldTraceGroupType<Line>( fldAssocInode0 );
  interiorTraceGroups_[1] = xfldInode1 = new FieldTraceGroupType<Line>( fldAssocInode1 );
  interiorTraceGroups_[2] = xfldInode2 = new FieldTraceGroupType<Line>( fldAssocInode2 );

  xfldInode0->setDOF(DOF_, nDOF_);
  xfldInode1->setDOF(DOF_, nDOF_);
  xfldInode2->setDOF(DOF_, nDOF_);

  // boundary-trace field variable

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode0( BasisFunctionNodeBase::P0, 1 );
  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode1( BasisFunctionNodeBase::P0, 1 );

  // boundary node processor ranks
  fldAssocBnode0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBnode1.setAssociativity( 0 ).setRank( comm_rank );

  // edge-element associativity
  fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
  fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {4} );

  fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
  fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

  // edge-to-cell connectivity
  fldAssocBnode0.setGroupLeft( 0 );
  fldAssocBnode0.setElementLeft( 0, 0 );
  fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

  fldAssocBnode1.setGroupLeft( 1 );
  fldAssocBnode1.setElementLeft( 1, 0 );
  fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
