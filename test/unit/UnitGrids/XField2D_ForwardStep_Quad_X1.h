// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_FORWARDSTEP_QUAD_X1
#define XFIELD2D_FORWARDSTEP_QUAD_X1

#include "Field/XFieldArea.h"

#include <vector>
#include <array>

namespace SANS
{
//----------------------------------------------------------------------------//
// hexahedron grid in a unit box with 6 sides as separate boundary-edge groups
//
// generates grid with ii x jj x kk (hex) elements
// volume elements in 1 group
// boundary-triangle elements in 6 groups: x-min, x-max, y-min, y-max, z-min, z-max

class XField2D_ForwardStep_Quad_X1 : public XField<PhysD2,TopoD2>
{
public:
  XField2D_ForwardStep_Quad_X1( mpi::communicator comm,
                                const int ii, const int jj,
                                Real xmin, Real xmax,
                                Real ymin, Real ymax,
                                Real stepHeight = 0.2, Real stepOffset = 0.6 );

  static const int iXmin, iXmax, iYmin, iYmax;
};

}

#endif // XFIELD2D_FORWARDSTEP_QUAD_X1
