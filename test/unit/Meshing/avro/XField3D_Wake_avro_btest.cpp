// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Adaptation/MeshAdapter.h"

#include "Meshing/avro/XField3D_Wake_avro.h"
#include "Meshing/avro/MesherInterface_Wake_avro.h"

#include "Meshing/EGADS/EGPlane.h"
#include "Meshing/EGADS/EGApproximate.h"
#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGTess/EGTessModel.h"
#include "Meshing/EGADS/Airfoils/NACA4.h"

#include "Meshing/EGADS/EGNode.h"
#include "Meshing/EGADS/EGPlane.h"
#include "Meshing/EGADS/EGLoop.h"
#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGADS/rotate.h"
#include "Meshing/EGADS/extrude.h"
#include "Meshing/EGADS/isSame.h"
#include "Meshing/EGADS/solidBoolean.h"
#include "Meshing/EGADS/EGIntersection.h"

#include "Meshing/EGTess/EGTessModel.h"
#include "Meshing/EGTess/makeWakedAirfoil.h"
#include "Meshing/EGTess/intersectWake.h"

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"

#include "Field/FieldVolume_CG_Cell.h"
#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include <mesh/boundary.h>
#endif

#include <vector>

namespace SANS
{

}

using namespace SANS;
using namespace SANS::EGADS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XField3D_Wake_avro_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WedgeWake_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context(CreateContext(), 0);

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});


  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);


  EGLoop<Dim> wingloop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (wingloop, 1) );

  //Create the wing
  EGBody<Dim> wing = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  //Increase the tess resolution on the wing
  std::vector< EGFace<Dim> > wingfaces = wing.getFaces();
  for ( auto face = wingfaces.begin(); face != wingfaces.end(); face++ )
    face->addAttribute(".tParams", {0.2,0.001,15.});


  //Nodes outside the box
  EGNode<Dim> TEnodeL(context, {0.50, 0.5, -2.});
  EGNode<Dim> TEnodeR(context, {0.50, 0.5,  2.});

  //Edges that extend from the wing tips outside the box
  EGEdge<Dim> edgeTipL (TEnode1, TEnodeL);
  EGEdge<Dim> edgeTipR (TEnodeR, TEnode0);

  EGLoop<Dim>::edge_vector wakeEdges;

  if ( LEnode0[2] < 0 && LEnode1[2] > 1 )
    wakeEdges = {(edgeTE,1)};
  else if ( LEnode0[2] > 0 && LEnode1[2] > 1 )
    wakeEdges = {(edgeTipL,1),(edgeTE,1)};
  else if ( LEnode0[2] < 0 && LEnode1[2] < 1 )
    wakeEdges = {(edgeTE,1),(edgeTipR,1)};
  else if ( LEnode0[2] > 0 && LEnode1[2] < 1 )
    wakeEdges = {(edgeTipL,1),(edgeTE,1),(edgeTipR,1)};

  Real len = 5;
  DLA::VectorS<3,Real> dir = {1, 0.05, 0};

  //Extrude the wake including artificial faces
  EGBody<Dim> wakecutter = extrude( EGLoop<Dim>(context, wakeEdges, OPEN), len, dir );

  //Extrude just the wake it self without extra cutting surfaces
  EGBody<Dim> wakeextrude = extrude( EGLoop<Dim>((edgeTE,1), OPEN), len, dir );

  //EGModel<Dim> wingboxModel = subtract(box, wing);
  //wingboxModel.save( "tmp/wingwake.egads");

  EGBody<Dim> wingbox = subtract(box, wing).getBodies()[0].clone();

  EGBody<Dim> wingboxscribed(EGIntersection<Dim>(wingbox, wakecutter));

  //This needs the most recent version of ESP
  EGBody<Dim> wake = intersect( EGModel<Dim>(wakeextrude), wingbox ).getBodies()[0].clone();

  {
    std::vector< EGFace<3> > wakeFaces = wake.getFaces();
    for ( EGFace<3>& face : wakeFaces )
      face.addAttribute("BCName", "Wing_Wake"); // Mark the wake as a BC

    std::vector< EGEdge<3> > wakeEdges = wake.getEdges();
    std::vector< EGEdge<3> > wingboxEdges = wingboxscribed.getEdges();

    for ( EGEdge<3>& edge : wakeEdges )
    {
      std::vector<EGNode<3>> nodes = edge.getNodes();

      typedef EGNode<3>::CartCoord CartCoord;

      if (nodes.size() == 1 ) continue;

      if ( (((CartCoord)nodes[0])[0] > 0.4 && ((CartCoord)nodes[0])[0] < 0.6) &&
           (((CartCoord)nodes[1])[0] > 0.4 && ((CartCoord)nodes[1])[0] < 0.6) )
      {
        for ( EGEdge<3>& sedge : wingboxEdges )
        {
          if (isSame(edge,sedge))
          {
            sedge.addAttribute(XField3D_Wake::WAKESHEET, "Wing_Wake"); // Mark the edge that is used for Kutta condition integrals
            break;
          }
        }
      }
      else if ( fabs( ((CartCoord)nodes[0])[0] - ((CartCoord)nodes[1])[0] ) < 0.1 )
      {
        for ( EGEdge<3>& sedge : wingboxEdges )
        {
          if (isSame(edge,sedge))
          {
            sedge.addAttribute(XField3D_Wake::TREFFTZ, "Wing_Wake"); // Mark the edge that is used for Trefftz plane integrals
            break;
          }
        }
      }
    }
  }


  // Use the 'Triangle' mesh generator for the wake
  //wake.addAttribute("Triangle", 20.);

  EGModel<Dim> model({wake, wingboxscribed});

  //model.save("tmp/test.egads");

  EGTessModel tessModel(model, {0.5, 0.001, 15.});

  //tessModel.outputTecplot("tmp/wedge.dat");

  // create the avro model from the EGTessModel
  XField3D_Wake_avro xfld( tessModel );

#if 1
  // try just going back and forth between data structures
  avro::Mesh<avro::Simplex> mesh1(3, 3);
  xfld.convert(mesh1);

  XField3D_Wake_avro xfld2( xfld, XFieldEmpty() );
  xfld2.import(mesh1);

  avro::Mesh<avro::Simplex> mesh2(3, 3);
  xfld2.convert(mesh2);

  XField3D_Wake_avro xfld3( xfld2, XFieldEmpty() );
  xfld3.import(mesh2);
#endif

#if 1
  // try adapting
  typedef DLA::MatrixSymS<3, Real> MatrixSym;
  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 15*M0;
  metric_request = M0;
  int iter = 0;
  PyDict d;
  d[avroParams::params.WriteMesh] = false;
  MesherInterface<PhysD3, TopoD3, avroWakeMesher> mesher(iter, d);
  std::shared_ptr< XField3D_Wake > adapted_xfld = mesher.adapt(metric_request,xfld);

  //output_Tecplot(*adapted_xfld, "tmp/avroWakeAdapt.dat");
#endif

  //Delete files written by avro
  std::remove("bnd_debug1.mesh");
  std::remove("tmp/bnd_debug1.mesh");
  std::remove("tmp/input_mesh_0.mesh");
  std::remove("tmp/mesh_0.mesh");
  std::remove("tmp/mesh_0.json");
  std::remove("tmp/properties_0.json");
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Wedge1Wake_floating_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context(CreateContext(), 0);

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  std::vector< EGFace<3> > boxfaces = box.getFaces();
  boxfaces[1].addAttribute(EGTessModel::TRIANGLE, 33.); // Use triangle mesh generator on the outflow

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});


  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);


  EGLoop<Dim> wingloop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (wingloop, 1) );

  //Create the wing
  EGBody<Dim> wing = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  //Increase the tess resolution on the wing
  std::vector< EGFace<Dim> > wingfaces = wing.getFaces();
  for ( auto face = wingfaces.begin(); face != wingfaces.end(); face++ )
    face->addAttribute(".tParams", {0.5,0.001,15.});

  //Nodes outside the box
  EGNode<Dim> TEnodeL(context, {0.50, 0.5, -2.});
  EGNode<Dim> TEnodeR(context, {0.50, 0.5,  2.});

  //Edges that extend from the wing tips outside the box
  EGEdge<Dim> edgeTipL (TEnode1, TEnodeL);
  EGEdge<Dim> edgeTipR (TEnodeR, TEnode0);

  Real len = 5;
  DLA::VectorS<3,Real> dir = {1, 0.05, 0};

  //Extrude just the wake it self without extra cutting surfaces
  EGBody<Dim> wakeextrude = extrude( EGLoop<Dim>((edgeTE,1), OPEN), len, dir );

  //EGModel<Dim> wingboxModel = subtract(box, wing);
  //wingboxModel.save( "tmp/wingwake.egads");

  EGBody<Dim> wingbox = subtract(box, wing).getBodies()[0].clone();

  //This needs the most recent version of ESP
  EGBody<Dim> wake = intersect( EGModel<Dim>(wakeextrude), wingbox ).getBodies()[0].clone();

  {
    std::vector< EGFace<3> > wingboxFaces = wingbox.getFaces();
    EGFace<3> outflowFace(wingboxFaces[0]);
    for (auto face = wingboxFaces.begin(); face != wingboxFaces.end(); face++)
      if (face->hasAttribute(EGTessModel::TRIANGLE))
      {
        outflowFace = *face;
        break;
      }

    std::vector< EGFace<3> > wakeFaces = wake.getFaces();
    for ( EGFace<3>& face : wakeFaces )
      face.addAttribute("BCName", "Wing_Wake"); // Mark the wake as a BC

    std::vector< EGEdge<3> > wakeEdges = wake.getEdges();
    std::vector< EGEdge<3> > wingboxEdges = wingbox.getEdges();

    for ( EGEdge<3>& edge : wakeEdges )
    {
      std::vector<EGNode<3>> nodes = edge.getNodes();

      typedef EGNode<3>::CartCoord CartCoord;

      if (nodes.size() == 1 ) continue;

      if ( (((CartCoord)nodes[0])[0] > 0.4 && ((CartCoord)nodes[0])[0] < 0.6) &&
           (((CartCoord)nodes[1])[0] > 0.4 && ((CartCoord)nodes[1])[0] < 0.6) )
      {
        for ( EGEdge<3>& sedge : wingboxEdges )
        {
          if (isSame(edge,sedge))
          {
            sedge.addAttribute(XField3D_Wake::WAKESHEET, "Wing_Wake"); // Mark the edge that is used for Kutta condition integrals
            break;
          }
        }
      }
      else if ( fabs( ((CartCoord)nodes[0])[0] - ((CartCoord)nodes[1])[0] ) < 0.1 )
      {
        edge.addAttribute(XField3D_Wake::TREFFTZ, "Wing_Wake"); // Mark the edge that is used for Trefftz plane integrals
        edge.addAttribute(EGTessModel_FLOATINGEDGE, {1, wingbox.getBodyIndex(outflowFace)});
      }
    }
  }

  //EGModel<Dim> model({wake, wingbox2});
  EGModel<Dim> model({wingbox, wake});
  //EGModel<Dim> model({wingbox2});

  //model.save( "tmp/tmp.egads");

  // The coarse
  EGTessModel tessModel( model, {0.5, 0.001, 15.} );

  // create the avro model from the EGTessModel
  XField3D_Wake_avro xfld(tessModel);

  //output_Tecplot(xfld, "tmp/wing3.dat");

  typedef DLA::MatrixSymS<3, Real> MatrixSym;
  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 15*M0;
  metric_request = M0;
  int iter = 0;
  PyDict d;
  d[avroParams::params.WriteMesh] = false;
  MesherInterface<PhysD3, TopoD3, avroWakeMesher> mesher(iter, d);
  std::shared_ptr< XField3D_Wake > adapted_xfld = mesher.adapt(metric_request,xfld);

  //output_Tecplot(*adapted_xfld, "tmp/avroWakeAdapt.dat");

  //Delete files written by avro
  std::remove("bnd_debug1.mesh");
  std::remove("tmp/bnd_debug1.mesh");
  std::remove("tmp/input_mesh_0.mesh");
  std::remove("tmp/mesh_0.mesh");
  std::remove("tmp/mesh_0.json");
  std::remove("tmp/properties_0.json");
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Wedge2Wake_floating_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context(CreateContext(), 0);

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  std::vector< EGFace<3> > boxfaces = box.getFaces();
  boxfaces[1].addAttribute(EGTessModel::TRIANGLE, 33.); // Use triangle mesh generator on the outflow

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});


  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);

  EGLoop<Dim> wingloop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (wingloop, 1) );

  //Create the wing
  EGBody<Dim> wing1 = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  //Create the 2nd wing
  EGBody<Dim> wing2 = wing1.clone({-0.1, 0.25, 0});

  //Increase the tess resolution on wing1
  std::vector< EGFace<Dim> > wing1faces = wing1.getFaces();
  for ( auto face = wing1faces.begin(); face != wing1faces.end(); face++ )
    face->addAttribute(".tParams", {0.5,0.001,15.});

  //Set coarse tessellation on wing2 to test a triangle that includes two EGADS nodes
  std::vector< EGFace<Dim> > wing2faces = wing2.getFaces();
  for ( auto face = wing2faces.begin(); face != wing2faces.end(); face++ )
    face->addAttribute(".tParams", {0.5,0.001,15.});


  typedef EGNode<Dim>::CartCoord CartCoord;
  std::vector< EGEdge<Dim> > wing2edges = wing2.getEdges();
  EGEdge<Dim> edgeTE2 = wing2edges[0];
  for ( auto edge2 = wing2edges.begin(); edge2 != wing2edges.end(); edge2++ )
  {
    std::vector<EGNode<Dim>> nodes = edge2->getNodes();

    if ( ((CartCoord)nodes[0])[0] > 0.39 && ((CartCoord)nodes[1])[0] > 0.39 )
    {
      edgeTE2 = *edge2;
      break;
    }
  }

  //Nodes outside the box
  EGNode<Dim> TEnodeL(context, {0.50, 0.5, -2.});
  EGNode<Dim> TEnodeR(context, {0.50, 0.5,  2.});

  //Edges that extend from the wing tips outside the box
  EGEdge<Dim> edgeTipL (TEnode1, TEnodeL);
  EGEdge<Dim> edgeTipR (TEnodeR, TEnode0);

  //Nodes outside the box
  EGNode<Dim> TEnodeL2(context, {0.40, 0.75, -2.});
  EGNode<Dim> TEnodeR2(context, {0.40, 0.75,  2.});

  std::vector<EGNode<Dim>> TE2nodes = edgeTE2.getNodes();
  EGNode<Dim> TEnode02 = TE2nodes[0];
  EGNode<Dim> TEnode12 = TE2nodes[1];


  if ( ((CartCoord)TEnode02)[2] < ((CartCoord)TEnode12)[2] )
  {
    TEnode12 = TE2nodes[0];
    TEnode02 = TE2nodes[1];
  }

  //Edges that extend from the wing tips outside the box
  EGEdge<Dim> edgeTipL2 (TEnode12, TEnodeL2);
  EGEdge<Dim> edgeTipR2 (TEnodeR2, TEnode02);

  Real len = 5;
  DLA::VectorS<3,Real> dir = {1, 0.05, 0};

  //Extrude just the wake it self without extra cutting surfaces
  EGBody<Dim> wakeextrude1 = extrude( EGLoop<Dim>((edgeTE,1), OPEN), len, dir );
  EGBody<Dim> wakeextrude2 = extrude( EGLoop<Dim>((edgeTE2,1), OPEN), len, dir );

  //EGModel<Dim> wingboxModel = subtract(box, wing);
  //wingboxModel.save( "tmp/wingwake.egads");


  EGBody<Dim> wingbox1 = subtract(box, wing1).getBodies()[0].clone();
  EGBody<Dim> wingbox2 = subtract(wingbox1, wing2).getBodies()[0].clone();

  EGBody<Dim> wake1 = intersectWake(wingbox2, wakeextrude1);
  EGBody<Dim> wake2 = intersectWake(wingbox2, wakeextrude2);


#if 1
  wake1.getFaces()[0].addAttribute("BCName", "Wing_Wake");
  wake1.getFaces()[0].addAttribute(".tParams", {0.15,0.001,15.});

  wake2.getFaces()[0].addAttribute("BCName", "Wing_Wake");
  wake2.getFaces()[0].addAttribute(".tParams", {0.15,0.001,15.});

  std::vector< EGFace<3> > wingboxFaces = wingbox2.getFaces();
  EGFace<3> outflowFace(wingboxFaces[0]);
  for (auto face = wingboxFaces.begin(); face != wingboxFaces.end(); face++)
    if (face->hasAttribute(EGTessModel::TRIANGLE))
    {
      outflowFace = *face;
      break;
    }

  std::vector< EGEdge<3> > WakeEdges1 = wake1.getEdges();
  std::vector< EGEdge<3> > WakeEdges2 = wake2.getEdges();
  std::vector< EGEdge<3> > wingboxEdges = wingbox2.getEdges();

  for ( auto edge = WakeEdges1.begin(); edge != WakeEdges1.end(); edge++ )
  {
    std::vector<EGNode<3>> nodes = edge->getNodes();

    typedef EGNode<3>::CartCoord CartCoord;

    if (nodes.size() == 1 ) continue;

    if ( (((CartCoord)nodes[0])[0] > 0.4 && ((CartCoord)nodes[0])[0] < 0.6) &&
         (((CartCoord)nodes[1])[0] > 0.4 && ((CartCoord)nodes[1])[0] < 0.6) )
    {
      for ( auto sedge = wingboxEdges.begin(); sedge != wingboxEdges.end(); sedge++ )
      {
        if (isSame(*edge,*sedge))
        {
          sedge->addAttribute(XField3D_Wake::WAKESHEET, "Wing_Wake"); // Mark the edge that is used for Kutta condition integrals
          break;
        }
      }
    }
    else if ( fabs( ((CartCoord)nodes[0])[0] - ((CartCoord)nodes[1])[0] ) < 0.1 )
    {
      edge->addAttribute(XField3D_Wake::TREFFTZ, "Wing_Wake"); // Mark the edge that is used for Trefftz plane integrals
      edge->addAttribute(EGTessModel_FLOATINGEDGE, {1, wingbox2.getBodyIndex(outflowFace)});
    }
  }

  for ( auto edge = WakeEdges2.begin(); edge != WakeEdges2.end(); edge++ )
  {
    std::vector<EGNode<3>> nodes = edge->getNodes();

    typedef EGNode<3>::CartCoord CartCoord;

    if (nodes.size() == 1 ) continue;

    if ( (((CartCoord)nodes[0])[0] > 0.3 && ((CartCoord)nodes[0])[0] < 0.5) &&
         (((CartCoord)nodes[1])[0] > 0.3 && ((CartCoord)nodes[1])[0] < 0.5) )
    {
      for ( auto sedge = wingboxEdges.begin(); sedge != wingboxEdges.end(); sedge++ )
      {
        if (isSame(*edge,*sedge))
        {
          sedge->addAttribute(XField3D_Wake::WAKESHEET, "Wing_Wake"); // Mark the edge that is used for Kutta condition integrals
          break;
        }
      }
    }
    else if ( fabs( ((CartCoord)nodes[0])[0] - ((CartCoord)nodes[1])[0] ) < 0.1 )
    {
      edge->addAttribute(XField3D_Wake::TREFFTZ, "Wing_Wake"); // Mark the edge that is used for Trefftz plane integrals
      edge->addAttribute(EGTessModel_FLOATINGEDGE, {1, wingbox2.getBodyIndex(outflowFace)});
    }
  }
#endif

  //EGModel<Dim> model({wake, wingbox2});
  EGModel<Dim> model({wingbox2, wake1, wake2});
  //EGModel<Dim> model({wingbox2});

  //model.save( "tmp/tmp.egads");

  // The coarse
  EGTessModel tessModel( model, {0.5, 0.001, 15.} );

  // create the avro model from the EGTessModel
  XField3D_Wake_avro xfld(tessModel);

  //output_Tecplot(xfld, "tmp/wing3.dat");

  typedef DLA::MatrixSymS<3, Real> MatrixSym;
  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 10*M0;
  metric_request = M0;

  // set a smaller request on the duplicated points
  //M0 = 5*M0;
  //for (int i = xfld.dupPointOffset_; i < xfld.nDOF(); i++)
  //  metric_request.DOF(i) = M0;

  int iter = 0;
  PyDict d;
  d[avroParams::params.WriteMesh] = false;
  MesherInterface<PhysD3, TopoD3, avroWakeMesher> mesher(iter, d);
  std::shared_ptr< XField3D_Wake > adapted_xfld = mesher.adapt(metric_request,xfld);

  //output_Tecplot(*adapted_xfld, "tmp/avroWakeAdapt.dat");

  //Delete files written by avro
  std::remove("bnd_debug1.mesh");
  std::remove("tmp/bnd_debug1.mesh");
  std::remove("tmp/input_mesh_0.mesh");
  std::remove("tmp/mesh_0.mesh");
  std::remove("tmp/mesh_0.json");
  std::remove("tmp/properties_0.json");
}
#endif


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WingWake_NACA_floating_test )
{
  EGContext context(CreateContext(), 0);

  EGApproximate<3> NACA_spline = NACA4<3>(context, 0.24, 0.4, 0);

  // Mark the wake or not as a wake
  bool withWake = true;
  //bool withWake = false;
  const int nChord = 10;
  const int nSpan = 10;
  const int nWake = 10;
  const Real span = 1.;

  std::vector<double> EGADSparams = {2.0, 0.001, 15.0};

  std::vector<int> TrefftzFrames;

  EGModel<3> model = makeWakedAirfoil(NACA_spline, TrefftzFrames, span, {-1,-2,-1, 3,3,2}, nChord, nSpan, nWake, EGADSparams, withWake);
  //model.save( "tmp/NACAWake3D.egads");

  // The coarse
  EGTessModel tessModel( model, {0.5, 0.001, 15.} );

  // create the avro model from the EGTessModel
  XField3D_Wake_avro xfld(tessModel);

  //output_Tecplot(xfld, "tmp/wing3.dat");

  typedef DLA::MatrixSymS<3, Real> MatrixSym;
  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 5*M0;
  metric_request = M0;

  // set a smaller request on the duplicated points
  M0 = 10*M0;
  for (int i = xfld.dupPointOffset_; i < xfld.nDOF(); i++)
    metric_request.DOF(i) = M0;

  int iter = 0;
  PyDict d;
  d[avroParams::params.WriteMesh] = false;
  MesherInterface<PhysD3, TopoD3, avroWakeMesher> mesher(iter, d);
  std::shared_ptr< XField3D_Wake > adapted_xfld = mesher.adapt(metric_request,xfld);

  //output_Tecplot(*adapted_xfld, "tmp/avroWakeAdapt.dat");

  //Delete files written by avro
  std::remove("bnd_debug1.mesh");
  std::remove("tmp/bnd_debug1.mesh");
  std::remove("tmp/input_mesh_0.mesh");
  std::remove("tmp/mesh_0.mesh");
  std::remove("tmp/mesh_0.json");
  std::remove("tmp/properties_0.json");
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
