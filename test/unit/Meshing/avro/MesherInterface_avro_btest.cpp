// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MesherInterface_avro_btest
// testing of the MesherInterface class for avro

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <cstdio>
#include <iostream>
#include <fstream>

#include "Meshing/avro/MesherInterface_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_BackwardsStep_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "Meshing/XField1D/XField1D.h"
#include "Meshing/EGADS/EGModel.h"

#include "Meshing/avro/XField_avro.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

// avro EGADS wrappers
#include <geometry/builder.h>

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template class MesherInterface<PhysD1, TopoD1, avroMesher>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MesherInterface_avro_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_avro_2d_test )
{
  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  PyDict d;
  d[avroParams::params.DisableCall] = true;
  avroParams::checkInputs(d);

  mpi::communicator world;

  using avro::coord_t;
  using avro::index_t;

  coord_t number = 2;

  std::vector<Real> lens(number,1.);
  std::vector<index_t> dims(number,5);

  avro::library::CubeMesh mesh( lens , dims );

  avro::Context context;

  Real xc[3] = {.5,.5,0.};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( &context, xc, lens[0], lens[1] );
  mesh.vertices().findGeometry(*pbody);

  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>( &context , "model" );

  model->addBody(pbody,true);

  XField_avro<PhysD2,TopoD2> domain(world,model);

  domain.import(mesh);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric_request(domain , 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 200*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD2, TopoD2, avroMesher> mesher(iter, d);

  std::shared_ptr< XField<PhysD2,TopoD2> > adapted_xfld = mesher.adapt(metric_request,domain);

  //WriteMesh_libMeshb(*adapted_xfld,"tmp/avro_adapted.mesh");

  //Delete files written by avro
  std::remove("bnd_debug1.mesh");
  std::remove("tmp/bnd_debug1.mesh");
  std::remove("tmp/input_mesh_0.mesh");
  std::remove("tmp/mesh_0.mesh");
  std::remove("tmp/mesh_0.json");
  std::remove("tmp/properties_0.json");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_avro_3d_test )
{
  typedef DLA::MatrixSymS<3, Real> MatrixSym;

  PyDict d;
  d[avroParams::params.DisableCall] = true;
  d[avroParams::params.WriteMesh] = false;
  avroParams::checkInputs(d);

  mpi::communicator world;

  using avro::coord_t;
  using avro::index_t;

  coord_t number = 3;

  std::vector<Real> lens(number,1.);
  std::vector<index_t> dims(number,5);

  avro::library::CubeMesh mesh( lens , dims );

  avro::Context context;

  Real xc[3] = {0.,0.,0.};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>( &context, xc, lens.data() );
  mesh.vertices().findGeometry(*pbody);

  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>( &context , "model" );
  model->addBody(pbody,true);

  XField_avro<PhysD3,TopoD3> domain(world,model);

  domain.import(mesh);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(domain , 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 10*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD3, TopoD3, avroMesher> mesher(iter, d);

  std::shared_ptr< XField<PhysD3,TopoD3> > adapted_xfld =
    mesher.adapt(metric_request,domain);

  //Delete files written by avro
  std::remove("bnd_debug1.mesh");
  std::remove("tmp/bnd_0.mesh");
  std::remove("tmp/input_mesh_0.mesh");
  std::remove("tmp/mesh_0.mesh");
  std::remove("tmp/mesh_0.json");
  std::remove("tmp/properties_0.json");
}
#endif
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( xfld_Test_2D )
{
  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  using avro::coord_t;
  using avro::index_t;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  coord_t number = 2;

  avro::Context context;

  std::vector<avro::real> lens(number,1.);
  avro::real xc[3] = {.5,.5,0.};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( &context, xc, lens[0], lens[1] );
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(&context, "cube");
  model->addBody(pbody,true);

  // construct XField_avro from an existing xfld and attach the xfld to the model
  XField2D_Box_Triangle_Lagrange_X1 xfld0( comm, 5 , 5 );
  XField_avro<PhysD2,TopoD2> xfld1(xfld0, model);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric_request(xfld1, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 200*M0;
  metric_request = M0;

  PyDict d;
  d[avroParams::params.DisableCall] = true;
  d[avroParams::params.WriteMesh] = false;
  avroParams::checkInputs(d);

  int iter = 0;
  MesherInterface<PhysD2, TopoD2, avroMesher> mesher(iter, d);

  std::shared_ptr< XField<PhysD2,TopoD2> > adapted_xfld = mesher.adapt(metric_request,xfld1);

  //WriteMesh_libMeshb(*adapted_xfld,"tmp/avro_adapted.mesh");
  //Delete files written by avro
  std::remove("bnd_debug1.mesh");
  std::remove("tmp/bnd_0.mesh");
  std::remove("tmp/input_mesh_0.mesh");
  std::remove("tmp/mesh_0.mesh");
  std::remove("tmp/mesh_0.json");
  std::remove("tmp/properties_0.json");
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGADSConstruction_2D )
{
  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  using avro::coord_t;
  using avro::index_t;

  mpi::communicator world;

  // create the context
  std::shared_ptr<avro::Context> context;
  context = std::make_shared<avro::Context>();

  // set up the egads nodes
  avro::real x0[3] = {-1, 0,0};
  avro::real x1[3] = { 0, 0,0}; // the re-entrant corner
  avro::real x2[3] = { 0,-1,0};
  avro::real x3[3] = { 1,-1,0};
  avro::real x4[3] = { 1, 1,0};
  avro::real x5[3] = {-1,1, 0};
  std::vector<avro::real*> X = {x0,x1,x2,x3,x4,x5};

  std::vector<avro::EGADSNode> nodes;
  for (index_t k=0;k<X.size();k++)
    nodes.emplace_back( avro::EGADSNode(context.get(),X[k]) );

  // make the edges
  // very important! boundary conditions need to be specified in this order
  // you can use EngSketchPad's vGeom on the flatplate.egads file below
  // to visualize the order of the edges in the EGADS model
  std::vector<avro::EGADSEdge> edges;
  for (index_t k=0;k<nodes.size();k++)
  {
    avro::EGADSNode* n0 = &nodes[k];
    avro::EGADSNode* n1;
    if (k<nodes.size()-1)
      n1 = &nodes[k+1];
    else
      n1 = &nodes[0];
    edges.emplace_back( avro::EGADSEdge(context.get(),*n0,*n1) );
  }

  // close the geometry
  avro::EGADSEdgeLoop loop(CLOSED);
  for (index_t k=0;k<edges.size();k++)
    loop.add( edges[k] , SFORWARD );
  loop.make(context.get());

  // build the body from the set of edges
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::EGADSWireBody>(context.get(),loop);
  pbody->buildHierarchy();

  // make the model in the context with a name
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model> ( context.get(), "Re-entrant Corner" );
  model->addBody(pbody,false); // add the body above, false = it is not an interior
  model->finalize(); // clean everything up

  // save the model
  //EG_saveModel(model->object(),"tmp/corner.egads");


  // construct the mesh that goes along with the above model
  const Real domSize = 1.0;
  XField2D_BackwardsStep_X1 xfld0( world, 0, domSize, domSize, domSize );

  XField_avro<PhysD2,TopoD2> xfld1(xfld0, model);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric_request(xfld1, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 200*M0;
  metric_request = M0;

  PyDict d;
  d[avroParams::params.DisableCall] = true;
  d[avroParams::params.WriteMesh] = false;
  avroParams::checkInputs(d);

  int iter = 0;
  MesherInterface<PhysD2, TopoD2, avroMesher> mesher(iter, d);

  std::shared_ptr< XField<PhysD2,TopoD2> > adapted_xfld = mesher.adapt(metric_request,xfld1);

  //WriteMesh_libMeshb(*adapted_xfld,"tmp/avro_adapted.mesh");
  //Delete files written by avro
  std::remove("bnd_debug1.mesh");
  std::remove("tmp/bnd_0.mesh");
  std::remove("tmp/input_mesh_0.mesh");
  std::remove("tmp/mesh_0.mesh");
  std::remove("tmp/mesh_0.json");
  std::remove("tmp/properties_0.json");
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
