// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

// #include <iostream>
// #include <cstdio>
#include <set>

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGTess/EGTessModel.h"
#include "Meshing/EGTess/makeWakedAirfoil.h"
#include "Meshing/EGADS/Airfoils/NACA4.h"

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/libMeshb/XField_libMeshb.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include <geometry/entity.h>
#include <mesh/boundary.h>
#include "Meshing/avro/XField_avro.h"

#include <vector>

namespace SANS
{
}

using namespace SANS;
using namespace SANS::EGADS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XField_avro_test_suite )

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_avro_Test_2D )
{
  EGContext context((CreateContext()));

  EGApproximate<2> NACA_spline = NACA4<2>(context);

  int nHalfAirfoil = 31;
  int nWake = 11;
  int nFarField = 11;

  EGBody<2> AirfoilBody = makeWakedAirfoil(NACA_spline, nHalfAirfoil, nWake, nFarField, 3.);

  EGModel<2> model(AirfoilBody);

  avro::Context avro_context;
  avro::Model model0(&avro_context,model,"naca");

  mpi::communicator world;
  XField_avro<PhysD2,TopoD2> domain(world,model,model0);

  domain.fill();
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Domain_Test_from_avro_2D )
{
  using avro::coord_t;
  using avro::index_t;

  coord_t number = 2;

  std::vector<Real> lens(number,1.);
  std::vector<index_t> dims(number,5);

  avro::library::CubeMesh mesh( lens , dims );

  avro::Context context;

  Real xc[3] = {.5,.5,0.};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( &context, xc, lens[0], lens[1] );
  mesh.vertices().findGeometry(*pbody);

  // create an avro model with the same ego as the SANS one
  // this is needed to lookup the entities when attaching them to vertices
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>( &context , "model" );
  model->addBody(pbody,true);

  mpi::communicator world;
  XField_avro<PhysD2,TopoD2> domain(world,model);

  // ensure the boundary is equal to the original one
  avro::Boundary<avro::Simplex> boundary0( mesh.topology(0) );
  boundary0.extract();
  for (index_t k=0;k<boundary0.nb();k++)
    BOOST_CHECK_EQUAL( boundary0.volume(k) , 1. );

  domain.import(mesh);


  // re-create an avro mesh from the domain
  avro::Mesh<avro::Simplex> mesh2( number , number );
  domain.convert(mesh2);

  // ask avro to get the boundary
  avro::Boundary<avro::Simplex> boundary( mesh2.topology(0) );
  boundary.extract();

  // ensure the boundary is equal to the original one
  // this checks the topology and the geometry of the mesh
  for (index_t k=0;k<boundary.nb();k++)
    BOOST_CHECK_EQUAL( boundary.volume(k) , 1. );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Domain_Test_from_avro_3D )
{
  using avro::coord_t;
  using avro::index_t;

  coord_t number = 3;

  std::vector<Real> lens(number,1.);
  std::vector<index_t> dims(number,5);

  avro::library::CubeMesh mesh( lens , dims );

  avro::Context context;

  Real xc[3] = {0.,0.,0.};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>( &context, xc, lens.data() );
  mesh.vertices().findGeometry(*pbody);

  // create an avro model with the same ego as the SANS one
  // this is needed to lookup the entities when attaching them to vertices
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>( &context , "model" );
  model->addBody(pbody,true);

  // ensure the boundary is equal to the original one
  avro::Boundary<avro::Simplex> boundary0( mesh.topology(0) );
  boundary0.extract();
  for (index_t k=0;k<boundary0.nb();k++)
    BOOST_CHECK_EQUAL( boundary0.volume(k) , 1. );

  mpi::communicator world;
  XField_avro<PhysD3,TopoD3> domain(world,model);

  domain.import(mesh);

  // re-create an avro mesh from the domain
  avro::Mesh<avro::Simplex> mesh2( number , number );
  domain.convert(mesh2);

  // ask avro to get the boundary
  avro::Boundary<avro::Simplex> boundary( mesh2.topology(0) );
  boundary.extract();

  // ensure the boundary is equal to the original one
  // this checks the topology and the geometry of the mesh
  for (index_t k=0;k<boundary.nb();k++)
    BOOST_CHECK_EQUAL( boundary.volume(k) , 1. );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Domain_Test_from_fill_2D )
{
  using avro::coord_t;
  using avro::index_t;

  coord_t number = 2;

  std::vector<Real> lens(number,1.);
  std::vector<index_t> dims(number,5);

  avro::Context context;

  Real xc[3] = {.5,.5,0.};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( &context, xc, lens[0], lens[1] );

  EGADS::EGBody<2> body( EGObject(pbody->object()) );
  std::vector<EGADS::EGEdge<2>> edges = body.getEdges();
  std::vector<double> rPos = {0.25, 0.5, 0.75};
  for (EGADS::EGEdge<2>& edge : edges)
    edge.addAttribute(".rPos", rPos);

  // create an avro model with the same ego as the SANS one
  // this is needed to lookup the entities when attaching them to vertices
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>( &context , "model" );
  model->addBody(pbody,true);

  mpi::communicator world;
  XField_avro<PhysD2,TopoD2> domain(world,model);

  domain.fill({});
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Domain_Test_from_fill_3D )
{
  using avro::coord_t;
  using avro::index_t;

  coord_t number = 3;

  std::vector<Real> lens(number,1.);
  std::vector<index_t> dims(number,5);

  avro::Context context;

  Real xc[3] = {0.,0.,0.};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>( &context, xc, lens.data() );

  // create an avro model with the same ego as the SANS one
  // this is needed to lookup the entities when attaching them to vertices
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>( &context , "model" );
  model->addBody(pbody,true);

  mpi::communicator world;
  XField_avro<PhysD3,TopoD3> domain(world,model);

  domain.fill({0.5,     // Maximum segment length
               0.001,   // Maximum curvature
               15.0} ); // Angle between triangle face normals
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( xfld_Test_2D )
{
  using avro::coord_t;
  using avro::index_t;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  coord_t number = 2;

  avro::Context context;

  std::vector<avro::real> lens(number,1.);
  avro::real xc[3] = {.5,.5,0.};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( &context, xc, lens[0], lens[1] );
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(&context, "cube");
  model->addBody(pbody,true);

  // construct XField_avro from an existing xfld and attach the xfld to the model
  XField2D_Box_Triangle_Lagrange_X1 xfld0( comm, 5 , 5 );
  XField_avro<PhysD2,TopoD2> xfld1(xfld0, model);

  for (int group = 0; group < xfld1.nBoundaryTraceGroups(); group++)
  {
    const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& bndTrace = xfld1.getBoundaryTraceGroup<Line>(group);

    std::vector<int> DOFmap( bndTrace.nBasis() );

    for (int elem = 0; elem < bndTrace.nElem(); elem++)
    {
      bndTrace.associativity(elem).getGlobalMapping( DOFmap.data(), DOFmap.size() );

      // make sure that all boundary vertexes are on geometry
      for (std::size_t i = 0; i < DOFmap.size(); i++)
        BOOST_CHECK_NE(-1, xfld1.vertexOnGeometry()[DOFmap[i]]);
    }
  }


  // create an avro mesh from the xfld
  avro::Mesh<avro::Simplex> mesh( number , number );
  xfld1.convert(mesh);

  // construct a mesh again from the avro mesh
  XField_avro<PhysD2,TopoD2> xfld2(xfld1, XFieldEmpty());
  xfld2.import( mesh );


  // make sure the two meshes are "equivalent"
  BOOST_REQUIRE_EQUAL(xfld1.nDOF(), xfld2.nDOF());
  for (int n = 0; n < xfld1.nDOF(); n++)
  {
    BOOST_CHECK_EQUAL(xfld1.DOF(n)[0], xfld2.DOF(n)[0]);
    BOOST_CHECK_EQUAL(xfld1.DOF(n)[1], xfld2.DOF(n)[1]);
  }

  BOOST_REQUIRE_EQUAL(xfld1.vertexOnGeometry().size(), xfld2.vertexOnGeometry().size());
  for (std::size_t n = 0; n < xfld1.vertexOnGeometry().size(); n++)
    BOOST_CHECK_EQUAL(xfld1.vertexOnGeometry()[n], xfld2.vertexOnGeometry()[n]);

  BOOST_REQUIRE_EQUAL(xfld1.nCellGroups(), xfld2.nCellGroups());
  for (int group = 0; group < xfld1.nCellGroups(); group++)
  {
    const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& cellGroup1 = xfld1.getCellGroup<Triangle>(group);
    const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& cellGroup2 = xfld2.getCellGroup<Triangle>(group);

    std::vector<int> DOFmap1( cellGroup1.nBasis() );
    std::vector<int> DOFmap2( cellGroup2.nBasis() );

    std::set<int> cell1;
    std::set<int> cell2;

    BOOST_REQUIRE_EQUAL(cellGroup1.nElem(), cellGroup2.nElem());
    for (int elem = 0; elem < cellGroup1.nElem(); elem++)
    {
      cellGroup1.associativity(elem).getGlobalMapping( DOFmap1.data(), DOFmap1.size() );
      cellGroup2.associativity(elem).getGlobalMapping( DOFmap2.data(), DOFmap2.size() );

      // gather all the boundary dofs
      cell1.insert(DOFmap1.begin(), DOFmap1.end());
      cell2.insert(DOFmap2.begin(), DOFmap2.end());
    }

    // make sure the count is the same
    BOOST_REQUIRE_EQUAL(cell1.size(), cell2.size());

    // make sure all DOFs are in the same boundary
    for (const int dof1 : cell1)
      BOOST_CHECK( cell2.find(dof1) != cell2.end() );
  }

  BOOST_REQUIRE_EQUAL(xfld1.nBoundaryTraceGroups(), xfld2.nBoundaryTraceGroups());
  for (int group = 0; group < xfld1.nBoundaryTraceGroups(); group++)
  {
    const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& bndTrace1 = xfld1.getBoundaryTraceGroup<Line>(group);
    const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& bndTrace2 = xfld2.getBoundaryTraceGroup<Line>(group);

    std::vector<int> DOFmap1( bndTrace1.nBasis() );
    std::vector<int> DOFmap2( bndTrace2.nBasis() );

    std::set<int> bnd1;
    std::set<int> bnd2;

    BOOST_REQUIRE_EQUAL(bndTrace1.nElem(), bndTrace2.nElem());
    for (int elem = 0; elem < bndTrace1.nElem(); elem++)
    {
      bndTrace1.associativity(elem).getGlobalMapping( DOFmap1.data(), DOFmap1.size() );
      bndTrace2.associativity(elem).getGlobalMapping( DOFmap2.data(), DOFmap2.size() );

      // gather all the boundary dofs
      bnd1.insert(DOFmap1.begin(), DOFmap1.end());
      bnd2.insert(DOFmap2.begin(), DOFmap2.end());
    }

    // make sure the count is the same
    BOOST_REQUIRE_EQUAL(bnd1.size(), bnd2.size());

    // make sure all DOFs are in the same boundary
    for (const int dof1 : bnd1)
      BOOST_CHECK( bnd2.find(dof1) != bnd2.end() );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( xfld_Test_3D )
{
  using avro::coord_t;
  using avro::index_t;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  coord_t number = 3;

  avro::Context context;

  std::vector<avro::real> x0(number,0.);
  std::vector<avro::real> lens(number,1.);
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>( &context, x0.data(), lens.data() );
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(&context,"cube");
  model->addBody(pbody,true);

  // construct XField_avro from an existing xfld and attach the xfld to the model
  XField3D_Box_Tet_X1 xfld0( comm, 3 , 3 , 3 );
  XField_avro<PhysD3,TopoD3> xfld1(xfld0, model);

  for (int group = 0; group < xfld1.nBoundaryTraceGroups(); group++)
  {
    const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& bndTrace = xfld1.getBoundaryTraceGroup<Triangle>(group);

    std::vector<int> DOFmap( bndTrace.nBasis() );

    for (int elem = 0; elem < bndTrace.nElem(); elem++)
    {
      bndTrace.associativity(elem).getGlobalMapping( DOFmap.data(), DOFmap.size() );

      // make sure that all boundary vertexes are on geometry
      for (std::size_t i = 0; i < DOFmap.size(); i++)
        BOOST_CHECK_NE(-1, xfld1.vertexOnGeometry()[DOFmap[i]]);
    }
  }


  // create an avro mesh from the xfld
  avro::Mesh<avro::Simplex> mesh( number , number );
  xfld1.convert(mesh);

  // construct a mesh again from the avro mesh
  XField_avro<PhysD3,TopoD3> xfld2(xfld1, XFieldEmpty());
  xfld2.import( mesh );


  // make sure the two meshes are "equivalent"
  BOOST_REQUIRE_EQUAL(xfld1.nDOF(), xfld2.nDOF());
  for (int n = 0; n < xfld1.nDOF(); n++)
  {
    BOOST_CHECK_EQUAL(xfld1.DOF(n)[0], xfld2.DOF(n)[0]);
    BOOST_CHECK_EQUAL(xfld1.DOF(n)[1], xfld2.DOF(n)[1]);
  }

  BOOST_REQUIRE_EQUAL(xfld1.vertexOnGeometry().size(), xfld2.vertexOnGeometry().size());
  for (std::size_t n = 0; n < xfld1.vertexOnGeometry().size(); n++)
    BOOST_CHECK_EQUAL(xfld1.vertexOnGeometry()[n], xfld2.vertexOnGeometry()[n]);

  BOOST_REQUIRE_EQUAL(xfld1.nCellGroups(), xfld2.nCellGroups());
  for (int group = 0; group < xfld1.nCellGroups(); group++)
  {
    const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& cellGroup1 = xfld1.getCellGroup<Tet>(group);
    const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& cellGroup2 = xfld2.getCellGroup<Tet>(group);

    std::vector<int> DOFmap1( cellGroup1.nBasis() );
    std::vector<int> DOFmap2( cellGroup2.nBasis() );

    std::set<int> cell1;
    std::set<int> cell2;

    BOOST_REQUIRE_EQUAL(cellGroup1.nElem(), cellGroup2.nElem());
    for (int elem = 0; elem < cellGroup1.nElem(); elem++)
    {
      cellGroup1.associativity(elem).getGlobalMapping( DOFmap1.data(), DOFmap1.size() );
      cellGroup2.associativity(elem).getGlobalMapping( DOFmap2.data(), DOFmap2.size() );

      // gather all the boundary dofs
      cell1.insert(DOFmap1.begin(), DOFmap1.end());
      cell2.insert(DOFmap2.begin(), DOFmap2.end());
    }

    // make sure the count is the same
    BOOST_REQUIRE_EQUAL(cell1.size(), cell2.size());

    // make sure all DOFs are in the same boundary
    for (const int dof1 : cell1)
      BOOST_CHECK( cell2.find(dof1) != cell2.end() );
  }

  BOOST_REQUIRE_EQUAL(xfld1.nBoundaryTraceGroups(), xfld2.nBoundaryTraceGroups());
  for (int group = 0; group < xfld1.nBoundaryTraceGroups(); group++)
  {
    const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& bndTrace1 = xfld1.getBoundaryTraceGroup<Triangle>(group);
    const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& bndTrace2 = xfld2.getBoundaryTraceGroup<Triangle>(group);

    std::vector<int> DOFmap1( bndTrace1.nBasis() );
    std::vector<int> DOFmap2( bndTrace2.nBasis() );

    std::set<int> bnd1;
    std::set<int> bnd2;

    BOOST_REQUIRE_EQUAL(bndTrace1.nElem(), bndTrace2.nElem());
    for (int elem = 0; elem < bndTrace1.nElem(); elem++)
    {
      bndTrace1.associativity(elem).getGlobalMapping( DOFmap1.data(), DOFmap1.size() );
      bndTrace2.associativity(elem).getGlobalMapping( DOFmap2.data(), DOFmap2.size() );

      // gather all the boundary dofs
      bnd1.insert(DOFmap1.begin(), DOFmap1.end());
      bnd2.insert(DOFmap2.begin(), DOFmap2.end());
    }

    // make sure the count is the same
    BOOST_REQUIRE_EQUAL(bnd1.size(), bnd2.size());

    // make sure all DOFs are in the same boundary
    for (const int dof1 : bnd1)
      BOOST_CHECK( bnd2.find(dof1) != bnd2.end() );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WriteMesh_libMeshb_Triangle_avro_Load )
{

#if defined(SANS_AVRO)

  std::shared_ptr<avro::Context> context;

  using avro::coord_t;
  using avro::index_t;

  context = std::make_shared<avro::Context>();

  coord_t number = 2;

  Real length = 1.0, offset = 1e-5;
  Real xc[3] = {length*.5+offset,length*.5+offset, 0};
  std::vector<avro::real> lens(number,1.);
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( context.get() , xc , lens[0] , lens[1] );
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(),"cube");
  model->addBody(pbody,true);

  mpi::communicator world;

  XField2D_Box_Triangle_Lagrange_X1 xfld_X1(world,2,2,offset,lens[0]+offset,offset,lens[1]+offset );

  XField_avro<PhysD2,TopoD2> xfld_avro(xfld_X1, model);

  //Test ASCII files
  std::string filename1 = "IO/mesh_X1.mesh";
  std::string filename2 = "IO/mesh_X1_rewrite.mesh";

  WriteMesh_libMeshb(xfld_avro,filename1);

  XField_libMeshb<PhysD2, TopoD2> xfld_readin_X1(world, filename1);

  XField_avro<PhysD2,TopoD2> xfld_readin_avro(xfld_readin_X1, model);

  WriteMesh_libMeshb(xfld_readin_avro,filename2);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    mapped_file_source f1(filename1);
    mapped_file_source f2(filename2);

    //Check if the contents of file1 and file2 are equal
    BOOST_CHECK( f1.size() == f2.size() && std::equal(f1.data(), f1.data() + f1.size(), f2.data()) );

    //Delete the written files
    std::remove(filename1.c_str());
    std::remove(filename2.c_str());
  }

  //Test binary files
  std::string filename1b = "IO/mesh_X1.meshb";
  std::string filename2b = "IO/mesh_X1_rewrite.meshb";

  WriteMesh_libMeshb(xfld_X1,filename1b);

  XField_libMeshb<PhysD2, TopoD2> xfld_readin_X1b(world, filename1b);

  XField_avro<PhysD2,TopoD2> xfld_readin_avro_b(xfld_readin_X1b, model);

  WriteMesh_libMeshb(xfld_readin_avro_b,filename2b);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    mapped_file_source f1b(filename1b);
    mapped_file_source f2b(filename2b);

    //Check if the contents of file1b and file2b are equal
    BOOST_CHECK( f1b.size() == f2b.size() && std::equal(f1b.data(), f1b.data() + f1b.size(), f2b.data()) );

    //Delete the written files
    std::remove(filename1b.c_str());
    std::remove(filename2b.c_str());
  }

#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
