// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MesherInterface_FeFloa_btest
// testing of the MesherInterface class for FeFloa

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <cstdio>
#include <iostream>
#include <fstream>

#include "Meshing/FeFloa/MesherInterface_FeFloa.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template class MesherInterface<PhysD2, TopoD2, FeFloa>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MesherInterface_FeFloa_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_FeFloa_Script2D_Test )
{
  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  PyDict d;
  d[FeFloaParams::params.DisableCall] = true;
  FeFloaParams::checkInputs(d);

  mpi::communicator world;

  XField2D_Box_Triangle_Lagrange_X1 xfld(world,20,20);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 100*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD2, TopoD2, FeFloa> mesher(iter, d);

  mesher.adapt(metric_request);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if mesh and metric request were exported
    std::string filename_mesh = "tmp/mesh_a0.mesh";
    std::string filename_metric = "tmp/metric_request_a0.sol";
    BOOST_REQUIRE( std::ifstream(filename_mesh).good() == true );
    BOOST_REQUIRE( std::ifstream(filename_metric).good() == true );

    //Delete the written files
    std::remove(filename_mesh.c_str());
    std::remove(filename_metric.c_str());
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_FeFloa_Script3D_Test )
{
  typedef DLA::MatrixSymS<3, Real> MatrixSym;

  PyDict d;
  d[FeFloaParams::params.DisableCall] = true;
  FeFloaParams::checkInputs(d);

  mpi::communicator world;

  XField3D_Box_Tet_X1 xfld(world,5,5,5);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 100*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD3, TopoD3, FeFloa> mesher(iter, d);

  mesher.adapt(metric_request);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if mesh and metric request were exported
    std::string filename_mesh = "tmp/mesh_a0.mesh";
    std::string filename_metric = "tmp/metric_request_a0.sol";
    BOOST_REQUIRE( std::ifstream(filename_mesh).good() == true );
    BOOST_REQUIRE( std::ifstream(filename_metric).good() == true );

    //Delete the written files
    std::remove(filename_mesh.c_str());
    std::remove(filename_metric.c_str());
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
