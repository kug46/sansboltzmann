// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGNode.h"
#include "Meshing/EGADS/EGPlane.h"
#include "Meshing/EGADS/EGLoop.h"
#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGADS/rotate.h"
#include "Meshing/EGADS/extrude.h"
#include "Meshing/EGADS/isSame.h"
#include "Meshing/EGADS/solidBoolean.h"
#include "Meshing/EGADS/EGIntersection.h"

#include "Meshing/EGTess/EGTessModel.h"
#ifdef SANS_AFLR
#include "Meshing/EGTess/AFLR4.h"
#include "Python/InputException.h"
#endif

#include <vector>
#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

#ifdef SANS_AFLR
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Prarameter_test )
{

  {
    PyDict d;
    d[AFLR4Params::params.min_ncell] = 3;

    d[AFLR4Params::params.ff_cdfr] = 1.4;
    d[AFLR4Params::params.abs_min_scale] = 1e-4;
    d[AFLR4Params::params.BL_thickness] = 0.1;
    d[AFLR4Params::params.curv_factor] = 0.2;
    d[AFLR4Params::params.length_ratio] = 1.01;
    d[AFLR4Params::params.max_scale] = 0.1;
    d[AFLR4Params::params.min_scale] = 0.01;

    d[AFLR4Params::params.ref_len] = 3.14;

    AFLR4Params::checkInputs(d);
  }

  {
    PyDict d;
    // ref_len must be set by the user
    BOOST_CHECK_THROW( AFLR4Params::checkInputs(d), NoDefaultException );
  }

  {
    PyDict d;
    d[AFLR4Params::params.ref_len] = 3.14;

    AFLR4Params::checkInputs(d);

    // make sure default values are extracted from AFLR4
    // (i.e. they are not the dummy -1 values)
    BOOST_CHECK_NE(d.get(AFLR4Params::params.ff_cdfr), -1);
    BOOST_CHECK_NE(d.get(AFLR4Params::params.min_ncell), -1);

    BOOST_CHECK_NE(d.get(AFLR4Params::params.abs_min_scale), -1);
    BOOST_CHECK_NE(d.get(AFLR4Params::params.BL_thickness), -1);
    BOOST_CHECK_NE(d.get(AFLR4Params::params.curv_factor), -1);
    BOOST_CHECK_NE(d.get(AFLR4Params::params.length_ratio), -1);
    BOOST_CHECK_NE(d.get(AFLR4Params::params.max_scale), -1);
    BOOST_CHECK_NE(d.get(AFLR4Params::params.min_scale), -1);
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTessModel_SingleBody_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});

  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);

  EGLoop<Dim> loop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (loop, 1) );

  //Create the wing
  EGBody<Dim> wing = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  EGModel<Dim> model = subtract(box, wing);

  EGTessModel tessModel(model, {0.5, 0.001, 15.});


  BOOST_CHECK_EQUAL( model.nFaces(), tessModel.nFaces() );


  // Check bodies
  std::vector< EGBody<3> > tessBodies = tessModel.getBodies();
  std::vector< EGBody<3> > modelBodies =  model.getBodies();

  BOOST_CHECK_EQUAL( modelBodies.size(), tessBodies.size() );
  for (std::size_t ibody = 0; ibody < modelBodies.size(); ibody++)
    BOOST_CHECK_EQUAL( (ego)modelBodies[ibody], (ego)tessBodies[ibody] ); // Check pointer equality

  const std::vector< int >& modelSolidBodyIndex = tessModel.modelSolidBodyIndex();
  for (std::size_t ibody = 0; ibody < modelSolidBodyIndex.size(); ibody++)
    BOOST_CHECK_EQUAL( ibody, modelSolidBodyIndex[ibody] );

  const std::vector< int >& modelSheetBodyIndex = tessModel.modelSheetBodyIndex();
  BOOST_CHECK_EQUAL( 0, modelSheetBodyIndex.size() );


  BOOST_REQUIRE_EQUAL( 1, modelBodies.size() );


  // Check nodes
  std::vector< EGNode<3> > tessNodes = tessModel.getUniqueNodes();
  std::vector< EGNode<3> > modelNodes =  modelBodies[0].getNodes();

  BOOST_CHECK_EQUAL( modelNodes.size(), tessNodes.size() );
  for (std::size_t inode = 0; inode < modelNodes.size(); inode++)
    BOOST_CHECK_EQUAL( (ego)modelNodes[inode], (ego)modelNodes[inode] ); // Check pointer equality

  const std::vector< std::vector< int > >& modelSolidNodeIndex = tessModel.modelSolidNodeIndex();
  for (std::size_t ibody = 0; ibody < modelSolidNodeIndex.size(); ibody++)
    for (std::size_t inode = 0; inode < modelSolidNodeIndex.size(); inode++)
      BOOST_CHECK_EQUAL( inode, modelSolidNodeIndex[ibody][inode] );

  const std::vector< std::vector< int > >& modelSheetNodeIndex = tessModel.modelSheetNodeIndex();
  BOOST_CHECK_EQUAL( 0, modelSheetNodeIndex.size() );


  // Check edges
  std::vector< EGEdge<3> > tessEdges = tessModel.getUniqueEdges();
  std::vector< EGEdge<3> > modelEdges =  modelBodies[0].getEdges();

  BOOST_CHECK_EQUAL( modelEdges.size(), tessEdges.size() );
  for (std::size_t iedge = 0; iedge < modelEdges.size(); iedge++)
    BOOST_CHECK_EQUAL( (ego)modelEdges[iedge], (ego)modelEdges[iedge] ); // Check pointer equality

  const std::vector< std::vector< int > >& modelSolidEdgeIndex = tessModel.modelSolidEdgeIndex();
  for (std::size_t ibody = 0; ibody < modelSolidEdgeIndex.size(); ibody++)
    for (std::size_t iedge = 0; iedge < modelSolidEdgeIndex.size(); iedge++)
      BOOST_CHECK_EQUAL( iedge, modelSolidEdgeIndex[ibody][iedge] );

  const std::vector< std::vector< int > >& modelSheetEdgeIndex = tessModel.modelSheetEdgeIndex();
  BOOST_CHECK_EQUAL( 0, modelSheetEdgeIndex.size() );


  // Check faces
  std::vector< EGFace<3> > tessFaces = tessModel.getUniqueFaces();
  std::vector< EGFace<3> > modelFaces =  modelBodies[0].getFaces();

  BOOST_CHECK_EQUAL( modelFaces.size(), tessFaces.size() );
  for (std::size_t iface = 0; iface < modelFaces.size(); iface++)
    BOOST_CHECK_EQUAL( (ego)modelFaces[iface], (ego)modelFaces[iface] ); // Check pointer equality

  const std::vector< std::vector< int > >& modelSolidFaceIndex = tessModel.modelSolidFaceIndex();
  for (std::size_t ibody = 0; ibody < modelSolidFaceIndex.size(); ibody++)
    for (std::size_t iface = 0; iface < modelSolidFaceIndex.size(); iface++)
      BOOST_CHECK_EQUAL( iface, modelSolidFaceIndex[ibody][iface] );

  const std::vector< std::vector< int > >& modelSheetFaceIndex = tessModel.modelSheetFaceIndex();
  BOOST_CHECK_EQUAL( 0, modelSheetFaceIndex.size() );


  // mapping from all egos to unique should be identity for a single body
  std::map<ego,ego> egomap = tessModel.alltoUniqueMap();
  BOOST_CHECK_EQUAL(modelNodes.size() + modelEdges.size() + modelFaces.size(), egomap.size());
  for (const std::pair<ego,ego>& egos : egomap)
    BOOST_CHECK_EQUAL(egos.first, egos.second);  // Check pointer equality


  // check that uv coordinates are consistent with xyz coordinates
  const std::vector< std::array<double,3> >& xyzs = tessModel.xyzs();
  const std::vector< std::array<double,2> >& uvt = tessModel.uvt();

  BOOST_REQUIRE_EQUAL(xyzs.size(), uvt.size());
  Real small_tol = 1e-11;

  for (std::size_t i = 0; i < xyzs.size(); i++)
  {
    EGTopologyBase topo = tessModel.modelTopoFromVertex(i);
    Real close_tol = topo.getTolerance();

    if (topo.objectClass() == NODE)
    {
      EGNode<3> node(topo);
      EGNode<3>::CartCoord Xtrue = node;

      SANS_CHECK_CLOSE( Xtrue[0], xyzs[i][0], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[1], xyzs[i][1], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[2], xyzs[i][2], small_tol, close_tol );
    }
    else if (topo.objectClass() == EDGE)
    {
      EGEdge<3> edge(topo);
      EGEdge<3>::ParamCoord U = uvt[i][0];
      EGEdge<3>::CartCoord Xtrue = edge(U);

      SANS_CHECK_CLOSE( Xtrue[0], xyzs[i][0], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[1], xyzs[i][1], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[2], xyzs[i][2], small_tol, close_tol );
    }
    else if (topo.objectClass() == FACE)
    {
      EGFace<3> face(topo);
      EGFace<3>::ParamCoord U = {uvt[i][0], uvt[i][1]};
      EGFace<3>::CartCoord Xtrue = face(U);

      SANS_CHECK_CLOSE( Xtrue[0], xyzs[i][0], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[1], xyzs[i][1], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[2], xyzs[i][2], small_tol, close_tol );
    }
    else
      BOOST_REQUIRE(false);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTessModel_WedgeWake_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context(CreateContext(), 0);

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});


  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);


  EGLoop<Dim> wingloop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (wingloop, 1) );

  //Create the wing
  EGBody<Dim> wing = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  //Increase the tess resolution on the wing
  std::vector< EGFace<Dim> > wingfaces = wing.getFaces();
  for ( auto face = wingfaces.begin(); face != wingfaces.end(); face++ )
    face->addAttribute(".tParams", {0.05,0.001,15.});



  //Nodes outside the box
  EGNode<Dim> TEnodeL(context, {0.50, 0.5, -2.});
  EGNode<Dim> TEnodeR(context, {0.50, 0.5,  2.});

  //Edges that extend from the wing tips outside the box
  EGEdge<Dim> edgeTipL (TEnode1, TEnodeL);
  EGEdge<Dim> edgeTipR (TEnodeR, TEnode0);

  EGLoop<Dim>::edge_vector wakeEdges;

  if ( LEnode0[2] < 0 && LEnode1[2] > 1 )
    wakeEdges = {(edgeTE,1)};
  else if ( LEnode0[2] > 0 && LEnode1[2] > 1 )
    wakeEdges = {(edgeTipL,1),(edgeTE,1)};
  else if ( LEnode0[2] < 0 && LEnode1[2] < 1 )
    wakeEdges = {(edgeTE,1),(edgeTipR,1)};
  else if ( LEnode0[2] > 0 && LEnode1[2] < 1 )
    wakeEdges = {(edgeTipL,1),(edgeTE,1),(edgeTipR,1)};

  Real len = 5;
  DLA::VectorS<3,Real> dir = {1, 0.05, 0};

  //Extrude the wake including artificial faces
  EGBody<Dim> wakecutter = extrude( EGLoop<Dim>(context, wakeEdges, OPEN), len, dir );

  //Extrude just the wake it self without extra cutting surfaces
  EGBody<Dim> wakeextrude = extrude( EGLoop<Dim>((edgeTE,1), OPEN), len, dir );

  //EGModel<Dim> wingboxModel = subtract(box, wing);
  //wingboxModel.save( "tmp/wingwake.egads");

  EGBody<Dim> wingbox = subtract(box, wing).getBodies()[0].clone();

  EGBody<Dim> wingboxscribed(EGIntersection<Dim>(wingbox, wakecutter));

  //This needs the most recent version of ESP
  EGBody<Dim> wake = intersect( EGModel<Dim>(wakeextrude), wingbox ).getBodies()[0].clone();

  // Use the 'Triangle' mesh generator for the wake
  wake.addAttribute("Triangle", 20.);

  EGModel<Dim> model({wake, wingboxscribed});

  EGTessModel tessModel(model, {0.2, 0.001, 15.});


  BOOST_CHECK_EQUAL( model.nFaces(), tessModel.nFaces() );

  // check the mapping from all to unique entities
  std::map<ego,ego> egomap = tessModel.alltoUniqueMap();
  BOOST_CHECK_EQUAL(tessModel.getAllNodes().size() +
                    tessModel.getAllEdges().size() +
                    tessModel.getAllFaces().size(), egomap.size());


  // Check bodies
  std::vector< EGBody<3> > tessBodies = tessModel.getBodies();
  std::vector< EGBody<3> > modelBodies =  model.getBodies();

  BOOST_CHECK_EQUAL( modelBodies.size(), tessBodies.size() );
  for (std::size_t ibody = 0; ibody < modelBodies.size(); ibody++)
    BOOST_CHECK_EQUAL( (ego)modelBodies[ibody], (ego)tessBodies[ibody] ); // Check pointer equality

  const std::vector< int >& modelSolidBodyIndex = tessModel.modelSolidBodyIndex();
  const std::vector< int >& modelSheetBodyIndex = tessModel.modelSheetBodyIndex();

  BOOST_REQUIRE_EQUAL( 1, modelSolidBodyIndex.size() );
  BOOST_REQUIRE_EQUAL( 1, modelSheetBodyIndex.size() );

  BOOST_CHECK( tessBodies[modelSolidBodyIndex[0]].isSolid() );
  BOOST_CHECK( tessBodies[modelSheetBodyIndex[0]].isSheet() );

  const std::vector< std::vector< int > >& modelSolidEdgeIndex = tessModel.modelSolidEdgeIndex();
  const std::vector< std::vector< int > >& modelSheetEdgeIndex = tessModel.modelSheetEdgeIndex();

  // Find the trailing edge in the wing box
  std::vector< EGEdge<3> > wingboxedges = wingboxscribed.getEdges();
  EGEdge<3> wingboxTE = edgeTE;
  bool foundSame = false;
  for (std::size_t iedge = 0; iedge < wingboxedges.size(); iedge++)
    if (isSame(edgeTE,wingboxedges[iedge]))
    {
      wingboxTE = wingboxedges[iedge];
      BOOST_REQUIRE_EQUAL( iedge+1, wingboxscribed.getBodyIndex(wingboxTE) );

      // the ego map should map to the solid
      BOOST_REQUIRE_EQUAL( (ego)wingboxedges[iedge], egomap.at(wingboxedges[iedge]) );

      // save the actual edgeTE from the wake for the pointer check below
      edgeTE = wingboxedges[iedge];
      foundSame = true;
    }
  BOOST_REQUIRE( foundSame );

  // Find the wake leading edge that is equivalent to the trailing edge of the wedge
  std::vector< EGEdge<3> > wakeedges = wake.getEdges();
  EGEdge<3> wakeLE = edgeTE;
  foundSame = false;
  for (std::size_t iedge = 0; iedge < wakeedges.size(); iedge++)
    if (isSame(edgeTE,wakeedges[iedge]))
    {
      wakeLE = wakeedges[iedge];
      BOOST_REQUIRE_EQUAL( iedge+1, wake.getBodyIndex(wakeLE) );

      // check that the mapping is correct
      BOOST_REQUIRE_EQUAL( (ego)edgeTE, egomap.at(wakeLE) );
      BOOST_REQUIRE_EQUAL( (ego)edgeTE, egomap.at(edgeTE) );
      foundSame = true;
    }
  BOOST_REQUIRE( foundSame );

  // Check that they have the same model index
  BOOST_CHECK_EQUAL( modelSolidEdgeIndex[0][wingboxscribed.getBodyIndex(wingboxTE)-1],
                     modelSheetEdgeIndex[0][wake.getBodyIndex(wakeLE)-1] );


  const std::vector< std::vector< int > >& modelSolidNodeIndex = tessModel.modelSolidNodeIndex();
  const std::vector< std::vector< int > >& modelSheetNodeIndex = tessModel.modelSheetNodeIndex();

  std::vector< EGNode<3> > wingboxTEnodes = wingboxTE.getNodes();
  std::vector< EGNode<3> > wakeLEnodes = wakeLE.getNodes();

  std::vector<EGNode<3>> modelNodes = tessModel.getAllNodes();

  if (isSame(wingboxTEnodes[0],wakeLEnodes[0]))
  {
    // Check that they have the same model index
    BOOST_CHECK_EQUAL( modelSolidNodeIndex[0][wingboxscribed.getBodyIndex(wingboxTEnodes[0])-1],
                       modelSheetNodeIndex[0][          wake.getBodyIndex(wakeLEnodes[0])-1] );

    BOOST_CHECK_EQUAL( modelSolidNodeIndex[0][wingboxscribed.getBodyIndex(wingboxTEnodes[1])-1],
                       modelSheetNodeIndex[0][          wake.getBodyIndex(wakeLEnodes[1])-1] );

    {
      EGNode<3> SolidNode = modelNodes[modelSolidNodeIndex[0][wingboxscribed.getBodyIndex(wingboxTEnodes[0])-1]];
      EGNode<3> SheetNode = modelNodes[modelSheetNodeIndex[0][          wake.getBodyIndex(wakeLEnodes[0])-1]];

      BOOST_REQUIRE_EQUAL( (ego)SolidNode, egomap.at(SheetNode) );
    }
    {
      EGNode<3> SolidNode = modelNodes[modelSolidNodeIndex[0][wingboxscribed.getBodyIndex(wingboxTEnodes[1])-1]];
      EGNode<3> SheetNode = modelNodes[modelSheetNodeIndex[0][          wake.getBodyIndex(wakeLEnodes[1])-1]];

      BOOST_REQUIRE_EQUAL( (ego)SolidNode, egomap.at(SheetNode) );
    }
  }
  else
  {
    // Check that they have the same model index
    BOOST_CHECK_EQUAL( modelSolidNodeIndex[0][wingboxscribed.getBodyIndex(wingboxTEnodes[0])-1],
                       modelSheetNodeIndex[0][          wake.getBodyIndex(wakeLEnodes[1])-1] );

    BOOST_CHECK_EQUAL( modelSolidNodeIndex[0][wingboxscribed.getBodyIndex(wingboxTEnodes[0])-1],
                       modelSheetNodeIndex[0][          wake.getBodyIndex(wakeLEnodes[1])-1] );

    {
      EGNode<3> SolidNode = modelNodes[modelSolidNodeIndex[0][wingboxscribed.getBodyIndex(wingboxTEnodes[0])-1]];
      EGNode<3> SheetNode = modelNodes[modelSheetNodeIndex[0][          wake.getBodyIndex(wakeLEnodes[1])-1]];

      BOOST_REQUIRE_EQUAL( (ego)SolidNode, egomap.at(SheetNode) );
    }
    {
      EGNode<3> SolidNode = modelNodes[modelSolidNodeIndex[0][wingboxscribed.getBodyIndex(wingboxTEnodes[0])-1]];
      EGNode<3> SheetNode = modelNodes[modelSheetNodeIndex[0][          wake.getBodyIndex(wakeLEnodes[1])-1]];

      BOOST_REQUIRE_EQUAL( (ego)SolidNode, egomap.at(SheetNode) );
    }
  }



  // check that uv coordinates are consistent with xyz coordinates
  const std::vector< std::array<double,3> >& xyzs = tessModel.xyzs();
  const std::vector< std::array<double,2> >& uvt = tessModel.uvt();

  BOOST_REQUIRE_EQUAL(xyzs.size(), uvt.size());
  Real small_tol = 1e-11;

  for (std::size_t i = 0; i < xyzs.size(); i++)
  {
    EGTopologyBase topo = tessModel.modelTopoFromVertex(i);
    Real close_tol = topo.getTolerance();

    if (topo.objectClass() == NODE)
    {
      EGNode<3> node(topo);
      EGNode<3>::CartCoord Xtrue = node;

      SANS_CHECK_CLOSE( Xtrue[0], xyzs[i][0], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[1], xyzs[i][1], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[2], xyzs[i][2], small_tol, close_tol );
    }
    else if (topo.objectClass() == EDGE)
    {
      EGEdge<3> edge(topo);
      EGEdge<3>::ParamCoord U = uvt[i][0];
      EGEdge<3>::CartCoord Xtrue = edge(U);

      SANS_CHECK_CLOSE( Xtrue[0], xyzs[i][0], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[1], xyzs[i][1], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[2], xyzs[i][2], small_tol, close_tol );
    }
    else if (topo.objectClass() == FACE)
    {
      EGFace<3> face(topo);
      EGFace<3>::ParamCoord U = {uvt[i][0], uvt[i][1]};
      EGFace<3>::CartCoord Xtrue = face(U);

      SANS_CHECK_CLOSE( Xtrue[0], xyzs[i][0], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[1], xyzs[i][1], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[2], xyzs[i][2], small_tol, close_tol );
    }
    else
      BOOST_REQUIRE(false);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTessModel_Wedge1Wake_floating_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context(CreateContext(), 0);

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  std::vector< EGFace<3> > boxfaces = box.getFaces();
  boxfaces[1].addAttribute(EGTessModel::TRIANGLE, 33.); // Use triangle mesh generator on the outflow

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});


  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);


  EGLoop<Dim> wingloop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (wingloop, 1) );

  //Create the wing
  EGBody<Dim> wing = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  //Increase the tess resolution on the wing
  std::vector< EGFace<Dim> > wingfaces = wing.getFaces();
  for ( auto face = wingfaces.begin(); face != wingfaces.end(); face++ )
    face->addAttribute(".tParams", {0.5,0.001,15.});

  //Nodes outside the box
  EGNode<Dim> TEnodeL(context, {0.50, 0.5, -2.});
  EGNode<Dim> TEnodeR(context, {0.50, 0.5,  2.});

  //Edges that extend from the wing tips outside the box
  EGEdge<Dim> edgeTipL (TEnode1, TEnodeL);
  EGEdge<Dim> edgeTipR (TEnodeR, TEnode0);

  Real len = 5;
  DLA::VectorS<3,Real> dir = {1, 0.05, 0};

  //Extrude just the wake it self without extra cutting surfaces
  EGBody<Dim> wakeextrude = extrude( EGLoop<Dim>((edgeTE,1), OPEN), len, dir );

  //EGModel<Dim> wingboxModel = subtract(box, wing);
  //wingboxModel.save( "tmp/wingwake.egads");

  EGBody<Dim> wingbox = subtract(box, wing).getBodies()[0].clone();

  //This needs the most recent version of ESP
  EGBody<Dim> wake = intersect( EGModel<Dim>(wakeextrude), wingbox ).getBodies()[0].clone();

  {
    std::vector< EGFace<3> > wingboxFaces = wingbox.getFaces();
    EGFace<3> outflowFace(wingboxFaces[0]);
    for (auto face = wingboxFaces.begin(); face != wingboxFaces.end(); face++)
      if (face->hasAttribute(EGTessModel::TRIANGLE))
      {
        outflowFace = *face;
        break;
      }

    std::vector< EGFace<3> > wakeFaces = wake.getFaces();
    for ( EGFace<3>& face : wakeFaces )
      face.addAttribute("BCName", "Wing_Wake"); // Mark the wake as a BC

    std::vector< EGEdge<3> > wakeEdges = wake.getEdges();
    std::vector< EGEdge<3> > wingboxEdges = wingbox.getEdges();

    for ( EGEdge<3>& edge : wakeEdges )
    {
      std::vector<EGNode<3>> nodes = edge.getNodes();

      typedef EGNode<3>::CartCoord CartCoord;

      if (nodes.size() == 1 ) continue;

      if ( (((CartCoord)nodes[0])[0] > 0.4 && ((CartCoord)nodes[0])[0] < 0.6) &&
           (((CartCoord)nodes[1])[0] > 0.4 && ((CartCoord)nodes[1])[0] < 0.6) )
      {
        for ( EGEdge<3>& sedge : wingboxEdges )
        {
          if (isSame(edge,sedge))
          {
            break;
          }
        }
      }
      else if ( fabs( ((CartCoord)nodes[0])[0] - ((CartCoord)nodes[1])[0] ) < 0.1 )
      {
        edge.addAttribute(EGTessModel_FLOATINGEDGE, {1, wingbox.getBodyIndex(outflowFace)});
      }
    }
  }

  //EGModel<Dim> model({wake, wingbox2});
  EGModel<Dim> model({wingbox, wake});
  //EGModel<Dim> model({wingbox2});

  //model.save( "tmp/tmp.egads");

  // The coarse
  EGTessModel tessModel( model, {0.2, 0.001, 15.} );
  //tessModel.outputTecplot("tmp/tmp.dat");


  // check that uv coordinates are consistent with xyz coordinates
  const std::vector< std::array<double,3> >& xyzs = tessModel.xyzs();
  const std::vector< std::array<double,2> >& uvt = tessModel.uvt();

  BOOST_REQUIRE_EQUAL(xyzs.size(), uvt.size());
  Real small_tol = 1e-11;

  for (std::size_t i = 0; i < xyzs.size(); i++)
  {
    EGTopologyBase topo = tessModel.modelTopoFromVertex(i);
    Real close_tol = topo.getTolerance();

    if (topo.objectClass() == NODE)
    {
      EGNode<3> node(topo);
      EGNode<3>::CartCoord Xtrue = node;

      SANS_CHECK_CLOSE( Xtrue[0], xyzs[i][0], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[1], xyzs[i][1], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[2], xyzs[i][2], small_tol, close_tol );
    }
    else if (topo.objectClass() == EDGE)
    {
      EGEdge<3> edge(topo);
      EGEdge<3>::ParamCoord U = uvt[i][0];
      EGEdge<3>::CartCoord Xtrue = edge(U);

      SANS_CHECK_CLOSE( Xtrue[0], xyzs[i][0], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[1], xyzs[i][1], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[2], xyzs[i][2], small_tol, close_tol );
    }
    else if (topo.objectClass() == FACE)
    {
      EGFace<3> face(topo);
      EGFace<3>::ParamCoord U = {uvt[i][0], uvt[i][1]};
      EGFace<3>::CartCoord Xtrue = face(U);

      SANS_CHECK_CLOSE( Xtrue[0], xyzs[i][0], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[1], xyzs[i][1], small_tol, close_tol );
      SANS_CHECK_CLOSE( Xtrue[2], xyzs[i][2], small_tol, close_tol );
    }
    else
      BOOST_REQUIRE(false);
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTessModel_Sphere_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context(CreateContext(), 0);

  EGBody<Dim> sphere = context.makeSolidBody( SPHERE, {0, 0, 0, 1} );

  EGModel<Dim> model({sphere});

  //model.save( "tmp/tmp.egads");

  PyDict AFLR4dict;
  AFLR4dict[EGTessModelParams::params.SurfaceMesher.Name] = EGTessModelParams::params.SurfaceMesher.AFLR4;
  AFLR4dict[AFLR4Params::params.ref_len] = 1;


  PyDict dict;
  dict[EGTessModelParams::params.SurfaceMesher] = AFLR4dict;

  //EGTessModel tessModel(model, dict );
  EGTessModel tessModel( model, {0.2, 0.001, 15.} );
  tessModel.outputTecplot("tmp/EGADS_quad_sphere.dat");
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
