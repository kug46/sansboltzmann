// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGTess/EGTessEdge.h"

#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTessEdge_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim>::CartCoord X0({0.1,0.2}), X1({0.9,0.7});

  EGNode<Dim> n0(context, X0);
  EGNode<Dim> n1(context, X1);

  EGEdge<Dim> edge(n0, n1);

  const int n = 5;
  std::vector< EGTessEdge<Dim>::CartCoord > X(n);
  EGTessEdge<Dim> etess(edge, n, &X[0]);

  for ( int i = 0; i < n; i++ )
  {
    BOOST_CHECK_CLOSE( X[i][0], (X1[0]-X0[0])/(n-1)*i + X0[0], 1e-11 );
    BOOST_CHECK_CLOSE( X[i][1], (X1[1]-X0[1])/(n-1)*i + X0[1], 1e-11 );

    BOOST_CHECK_CLOSE( etess.getVertex(i)[0], X[i][0], 1e-11 );
    BOOST_CHECK_CLOSE( etess.getVertex(i)[1], X[i][1], 1e-11 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTessEdge_evaluate )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));


  EGNode<Dim>::CartCoord X0({0.0,0.0}), X1({1.0,0.0});

  EGNode<Dim> n0(context, X0);
  EGNode<Dim> n1(context, X1);

  EGEdge<Dim> edge(n0, n1);

  const int n = 5;
  std::vector< EGTessEdge<Dim>::CartCoord > X(n);
  EGTessEdge<Dim> etess(edge, n, &X[0]);

  for ( int i = 0; i < n; i++ )
  {
    BOOST_CHECK_CLOSE( X[i][0], (X1[0]-X0[0])/(n-1)*i + X0[0], 1e-11 );
    BOOST_CHECK_CLOSE( X[i][1], (X1[1]-X0[1])/(n-1)*i + X0[1], 1e-11 );

    BOOST_CHECK_CLOSE( etess.getVertex(i)[0], X[i][0], 1e-11 );
    BOOST_CHECK_CLOSE( etess.getVertex(i)[1], X[i][1], 1e-11 );
  }

  std::vector<double> t(n-2);
  t[0] = 0.1;
  t[1] = 0.3;
  t[2] = 0.8;

  //Move the nodes to a non-uniform distribution
  etess.evaluate(t.size(), &t[0]);

  //Check that everything is updated
  for ( int i = 0; i < n; i++ )
  {
    BOOST_CHECK_CLOSE( etess.getVertex(i)[0], X[i][0], 1e-11 );
    BOOST_CHECK_CLOSE( etess.getVertex(i)[1], X[i][1], 1e-11 );
  }

  BOOST_CHECK_SMALL( etess.getVertex(0)[0], 1e-11 );
  BOOST_CHECK_SMALL( etess.getVertex(0)[1], 1e-11 );
  BOOST_CHECK_SMALL( etess.getVertex(0).getParam(), 1e-11 );

  BOOST_CHECK_CLOSE( etess.getVertex(1)[0], 0.1, 1e-11 );
  BOOST_CHECK_SMALL( etess.getVertex(1)[1], 1e-11 );
  BOOST_CHECK_CLOSE( etess.getVertex(1).getParam(), 0.1, 1e-11 );

  BOOST_CHECK_CLOSE( etess.getVertex(2)[0], 0.3, 1e-11 );
  BOOST_CHECK_SMALL( etess.getVertex(2)[1], 1e-11 );
  BOOST_CHECK_CLOSE( etess.getVertex(2).getParam(), 0.3, 1e-11 );

  BOOST_CHECK_CLOSE( etess.getVertex(3)[0], 0.8, 1e-11 );
  BOOST_CHECK_SMALL( etess.getVertex(3)[1], 1e-11 );
  BOOST_CHECK_CLOSE( etess.getVertex(3).getParam(), 0.8, 1e-11 );

  BOOST_CHECK_CLOSE( etess.getVertex(4)[0], 1.0, 1e-11 );
  BOOST_CHECK_SMALL( etess.getVertex(4)[1], 1e-11 );
  BOOST_CHECK_CLOSE( etess.getVertex(4).getParam(), 1.0, 1e-11 );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
