// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGTess/EGTessellation.h"
#include "Meshing/EGADS/sewFaces.h"

#include "Field/output_Tecplot.h"

#include <vector>
#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTesselation_Body_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {1.0, 0.0});
  EGNode<Dim> n2(context, {1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGEdge<Dim> circle( EGCircle<Dim>(context, {0.5,0.5}, 0.25) );

  EGLoop<Dim> loop0(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGLoop<Dim> loop1((circle,1), CLOSED);

  EGBody<Dim> facebody(context, {(loop0,1), (loop1,-1)});

  int nedge;
  ego *pedges;

  //Extract all edges of the body and count the number of elements
  EG_STATUS( EG_getBodyTopos(facebody, NULL, EDGE, &nedge, &pedges) );
  EG_free(pedges);

  std::vector<int> edge_vertex_count(nedge, 9);

  std::vector<int> face_vertex_count(1, 9*9);

  EGTessellation<Dim> tess(facebody, edge_vertex_count, face_vertex_count);

  //output_Tecplot( (const XField2DArea<Triangle>&)tess.getElementAreaGroup(0), "testTess.dat" );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTesselation_TwoFaceBody )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {1.0, 0.0});
  EGNode<Dim> n2(context, {2.0, 0.0});
  EGNode<Dim> n3(context, {2.0, 1.0});
  EGNode<Dim> n4(context, {1.0, 1.0});
  EGNode<Dim> n5(context, {0.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n4);
  EGEdge<Dim> edge2(n4, n5);
  EGEdge<Dim> edge3(n5, n0);

  EGLoop<Dim>::edge_vector edges1 = {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)};

  EGEdge<Dim> edge4(n1, n2);
  EGEdge<Dim> edge5(n2, n3);
  EGEdge<Dim> edge6(n3, n4);
  EGEdge<Dim> edge7(n4, n1);

  EGLoop<Dim>::edge_vector edges2 = {(edge4, 1), (edge5, 1), (edge6, 1), (edge7, 1)};

  EGLoop<Dim> loop1(context, edges1, CLOSED);
  EGLoop<Dim> loop2(context, edges2, CLOSED);

  std::vector< EGBody<Dim> > Faces;
  Faces.emplace_back(context, (loop1,1));
  Faces.emplace_back(context, (loop2,1));

  EGModel<Dim> model = sewFaces(Faces, 0.0, NonManifold);
  std::vector< EGBody<Dim> > facebody = model.getBodies();

  int nedge;
  ego *pedges;

  //Extract all edges of the body and count the number of elements
  EG_STATUS( EG_getBodyTopos(facebody[0], NULL, EDGE, &nedge, &pedges) );
  EG_free(pedges);

  std::vector<int> edge_vertex_count(nedge, 1);

  std::vector<int> face_vertex_count(2, 1);

  EGTessellation<Dim> tess(facebody[0], edge_vertex_count, face_vertex_count);

  //output_Tecplot( tess, "tmp/testTess.dat" );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
