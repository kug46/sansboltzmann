// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Python/PyDict.h"

#include "Meshing/refine/XField_refine.h"
#include "Meshing/refine/MesherInterface_refine.h"
#include "Meshing/refine/MultiScale_metric.h"

#include "Field/output_Tecplot.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

#include <vector>
#include <iostream>
#include <fstream>
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( refine3D_tesit_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_refine_Script3D_Test )
{
  typedef DLA::MatrixSymS<3, Real> MatrixSym;

  PyDict d;
  d[refineParams::params.DisableCall] = true;
#ifdef SANS_REFINE
  d[refineParams::params.CallMethod] = refineParams::params.CallMethod.system;
#endif

  refineParams::checkInputs(d);

  mpi::communicator world;

  XField3D_Box_Tet_X1 xfld(world,5,5,5);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 100*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD3, TopoD3, refine> mesher(iter, d);

  mesher.adapt(metric_request);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if mesh and metric request were exported
    std::string filename_mesh = "tmp/mesh_a0.meshb";
    std::string filename_metric = "tmp/metric_request_a0.solb";
    BOOST_REQUIRE( std::ifstream(filename_mesh).good() == true );
    BOOST_REQUIRE( std::ifstream(filename_metric).good() == true );

    //Delete the written files
    std::remove(filename_mesh.c_str());
    std::remove(filename_metric.c_str());
  }
}

#ifdef SANS_REFINE

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Box_stationary_test )
{
  typedef XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef DLA::MatrixSymS<PhysD2::D, Real> MatrixSym;

  int ii = 4, jj = 4;
  XField2D_Box_UnionJack_Triangle_X1 xfld_box(ii, jj);

  // Get the cell group and create an element for it
  XFieldCellGroupType& xfldCellGroup = xfld_box.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem( xfldCellGroup.basis() );

  // Grab just one element from the cell group
  xfldCellGroup.getElement(xfldElem,0);

  // Compute the implied metric of the element
  MatrixSym M;
  xfldElem.impliedMetric(M);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric(xfld_box, 1, BasisFunctionCategory_Hierarchical);

  // Set a uniform metric field consistent with the mesh, i.e. a stationary condition
  metric = M;

  refine xfld(metric);
  //output_Tecplot(xfld, "tmp/box.dat");

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_REQUIRE_EQUAL(xfld.nDOF(), xfld_box.nDOF());
  for (int i = 0; i < xfld.nDOF(); i++)
  {
    SANS_CHECK_CLOSE( xfld_box.DOF(i)[0], xfld.DOF(i)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( xfld_box.DOF(i)[1], xfld.DOF(i)[1], small_tol, close_tol );
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Box_scaled_Coarse_to_Fine_test )
{
  typedef XField<PhysD3,TopoD3>::FieldCellGroupType<Tet> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef DLA::MatrixSymS<PhysD3::D, Real> MatrixSym;

  typedef XField<PhysD3, TopoD3> XFieldType;
  typedef std::shared_ptr<XFieldType> MeshPtr;

  // global communicator
  mpi::communicator world;

  PyDict paramsDict;
  paramsDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;

  int adapt_iter = 0;

  int ii = 2, jj = 2, kk = 2;
  XField3D_Box_Tet_X1 xfld_box(world, ii, jj, kk);

  // Get the cell group and create an element for it
  XFieldCellGroupType& xfldCellGroup = xfld_box.getCellGroup<Tet>(0);
  ElementXFieldClass xfldElem( xfldCellGroup.basis() );

  // Grab just one element from the cell group
  xfldCellGroup.getElement(xfldElem,0);

  // Compute the implied metric of the element
  MatrixSym M;
  xfldElem.impliedMetric(M);

  // Refine the mesh

  // Reduce the size of the elements in the metric
  M *= pow(2.,3);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric(xfld_box, 1, BasisFunctionCategory_Hierarchical);

  // Set a uniform metric field that should increase mesh size
  metric = M;

  MesherInterface<PhysD3, TopoD3, refine> mesher(adapt_iter, paramsDict);
  MeshPtr pxfld = mesher.adapt(metric);
  //output_Tecplot(*pxfld, "tmp/box.dat");

  // Coarsen the mesh

  // Generate a another mesh with an even coarser metric
  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric2(*pxfld, 1, BasisFunctionCategory_Hierarchical);

  M *= pow(1./4.,3);

  metric2 = M;

  MeshPtr pxfld2 = mesher.adapt(metric2);
  //output_Tecplot(xfld2, "tmp/box2.dat");


  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric3(*pxfld2, 1, BasisFunctionCategory_Hierarchical);

  int nnode_metric = metric.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric = boost::mpi::all_reduce(*metric.comm(), nnode_metric, std::plus<int>());
#endif

  int nnode_metric2 = metric2.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric2 = boost::mpi::all_reduce(*metric2.comm(), nnode_metric2, std::plus<int>());
#endif

  int nnode_metric3 = metric3.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric3 = boost::mpi::all_reduce(*metric3.comm(), nnode_metric3, std::plus<int>());
#endif

  BOOST_CHECK_GT( nnode_metric2, nnode_metric );

  BOOST_CHECK_LT( nnode_metric3, nnode_metric2 );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Box_scaled_Fine_to_Coarse_test )
{
  typedef XField<PhysD3,TopoD3>::FieldCellGroupType<Tet> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef DLA::MatrixSymS<PhysD3::D, Real> MatrixSym;

  typedef XField<PhysD3, TopoD3> XFieldType;
  typedef std::shared_ptr<XFieldType> MeshPtr;

  // global communicator
  mpi::communicator world;

  PyDict paramsDict;
  paramsDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;

  int adapt_iter = 0;

  int ii = 8, jj = 8, kk = 8;
  XField3D_Box_Tet_X1 xfld_box(world, ii, jj, kk);

  // Get the cell group and create an element for it
  XFieldCellGroupType& xfldCellGroup = xfld_box.getCellGroup<Tet>(0);
  ElementXFieldClass xfldElem( xfldCellGroup.basis() );

  // Grab just one element from the cell group
  xfldCellGroup.getElement(xfldElem,0);

  // Compute the implied metric of the element
  MatrixSym M;
  xfldElem.impliedMetric(M);

  // Coarsen the mesh

  // Increase the size of the elements in the metric
  M *= pow(1./2.,3);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric(xfld_box, 1, BasisFunctionCategory_Hierarchical);

  // Set a uniform metric field that should increase mesh size
  metric = M;

  MesherInterface<PhysD3, TopoD3, refine> mesher(adapt_iter, paramsDict);
  MeshPtr pxfld = mesher.adapt(metric);
  //
  // output_Tecplot( *pxfld, "tmp/pxfld.dat" );
  //
  // WriteMesh_libMeshb(*pxfld,"tmp/xfld.mesh");

  // refine the mesh
  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric2(*pxfld, 1, BasisFunctionCategory_Hierarchical);

  xfldElem.impliedMetric(M);
  M *= pow(2.,3);

  metric2 = M;

  // output_Tecplot( *pxfld, "tmp/pxfld.plt" );

  MeshPtr pxfld2 = mesher.adapt(metric2);
  //output_Tecplot(xfld2, "tmp/box2.dat");

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric3(*pxfld2, 1, BasisFunctionCategory_Hierarchical);

  int nnode_metric = metric.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric = boost::mpi::all_reduce(*metric.comm(), nnode_metric, std::plus<int>());
#endif

  int nnode_metric2 = metric2.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric2 = boost::mpi::all_reduce(*metric2.comm(), nnode_metric2, std::plus<int>());
#endif

  int nnode_metric3 = metric3.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric3 = boost::mpi::all_reduce(*metric3.comm(), nnode_metric3, std::plus<int>());
#endif

  BOOST_CHECK_LT( nnode_metric2, nnode_metric );

  BOOST_CHECK_GT( nnode_metric3, nnode_metric2 );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Box_MultiScale_test )
{
  typedef ScalarFunction3D_Tanh3 SolutionExact;
  typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;

  typedef DLA::MatrixSymS<PhysD3::D, Real> MatrixSym;

  typedef XField<PhysD3, TopoD3> XFieldType;
  typedef std::shared_ptr<XFieldType> MeshPtr;

  // global communicator
  mpi::communicator world;

  NDSolutionExact solnExact;

  PyDict paramsDict;
  paramsDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;

  int ii = 8, jj = 8, kk = 8;
  MeshPtr pxfld = std::make_shared<XField3D_Box_Tet_X1>(world, ii, jj, kk);

  for (int adapt_iter = 0; adapt_iter < 1; adapt_iter++)
  {
    Field_CG_Cell<PhysD3, TopoD3, Real> sfld(*pxfld, 1, BasisFunctionCategory_Hierarchical);

    std::map<int,int> native2local;
    for (int i = 0; i < pxfld->nDOF(); i++)
      native2local[pxfld->local2nativeDOFmap(i)] = i;

    for (int i = 0; i < pxfld->nDOF(); i++)
      sfld.DOF(i) = solnExact(pxfld->DOF(native2local.at(sfld.local2nativeDOFmap(i))));

    int p = 2;
    Real gradation = 5;
    Real complexity = 1e2;

    // compute the multi-scale metric
    Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric(*pxfld, 1, BasisFunctionCategory_Hierarchical);
    MultiScale_metric(refine_recon_Kexact, p, gradation, complexity, sfld, metric);

    MesherInterface<PhysD3, TopoD3, refine> mesher(adapt_iter, paramsDict);
    pxfld = mesher.adapt(metric);

    //Field_CG_Cell<PhysD3, TopoD3, Real> sfld2(*pxfld, 1, BasisFunctionCategory_Hierarchical);
    //sfld2 = 1;
    //output_Tecplot( sfld2, "tmp/sfld2_a" + std::to_string(adapt_iter) + ".dat" );
  }
  //
  // WriteMesh_libMeshb(*pxfld,"tmp/xfld.mesh");
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cube_cylinder_test )
{
  typedef XField<PhysD3,TopoD3>::FieldCellGroupType<Tet> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef DLA::MatrixSymS<PhysD3::D, Real> MatrixSym;

  PyDict paramsDict;
  paramsDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;

  paramsDict[refineParams::params.CADfilename] = "tmp/cube-cylinder.egads";
  paramsDict[refineParams::params.GASFilename] = "tmp/cube-cylinder.gas";

  XField_libMeshb<PhysD3, TopoD3> xfld_box("tmp/cube-cylinder.meshb");

  // Get the cell group and create an element for it
  XFieldCellGroupType& xfldCellGroup = xfld_box.getCellGroup<Tet>(0);
  ElementXFieldClass xfldElem( xfldCellGroup.basis() );

  // Grab just one element from the cell group
  xfldCellGroup.getElement(xfldElem,0);

  // Compute the implied metric of the element
  MatrixSym M;
  xfldElem.impliedMetric(M);

  // Reduce the size of the elements in the metric
  //M *= pow(4.,2);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric(xfld_box, 1, BasisFunctionCategory_Hierarchical);

  // Set a uniform metric field that should increase mesh size
  metric = M;

  refine xfld(metric, paramsDict);
  output_Tecplot(xfld, "tmp/box.dat");

  BOOST_CHECK_GT( xfld.nDOF(), xfld_box.nDOF() );
}
#endif

#endif //SANS_REFINE

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
