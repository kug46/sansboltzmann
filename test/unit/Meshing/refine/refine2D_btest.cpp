// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Python/PyDict.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Meshing/refine/XField_refine.h"
#include "Meshing/refine/MesherInterface_refine.h"
#include "Meshing/refine/MultiScale_metric.h"

#include "Meshing/EGADS/EGContext.h"
#include "Meshing/EGADS/EGNode.h"
#include "Meshing/EGADS/EGEdge.h"
#include "Meshing/EGADS/EGLoop.h"
#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGADS/Airfoils/NACA4.h"

#include "Field/output_Tecplot.h"
#include "Field/FieldArea_CG_Cell.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

#include <vector>
#include <iostream>
#include <fstream>
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( refine2D_tesit_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_refine_Script2D_Test )
{
  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  PyDict d;
  d[refineParams::params.DisableCall] = true;
#ifdef SANS_REFINE
  d[refineParams::params.CallMethod] = refineParams::params.CallMethod.system;
#endif

  refineParams::checkInputs(d);

  mpi::communicator world;

  XField2D_Box_Triangle_Lagrange_X1 xfld(world,5,5);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 100*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD2, TopoD2, refine> mesher(iter, d);

  mesher.adapt(metric_request);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if mesh and metric request were exported
    std::string filename_mesh = "tmp/mesh_a0.meshb";
    std::string filename_metric = "tmp/metric_request_a0.solb";
    BOOST_REQUIRE( std::ifstream(filename_mesh).good() == true );
    BOOST_REQUIRE( std::ifstream(filename_metric).good() == true );

    //Delete the written files
    std::remove(filename_mesh.c_str());
    std::remove(filename_metric.c_str());
  }
}

#ifdef SANS_REFINE

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Box_stationary_test )
{
  typedef XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef DLA::MatrixSymS<PhysD2::D, Real> MatrixSym;

  int ii = 4, jj = 4;
  XField2D_Box_UnionJack_Triangle_X1 xfld_box(ii, jj);

  // Get the cell group and create an element for it
  XFieldCellGroupType& xfldCellGroup = xfld_box.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem( xfldCellGroup.basis() );

  // Grab just one element from the cell group
  xfldCellGroup.getElement(xfldElem,0);

  // Compute the implied metric of the element
  MatrixSym M;
  xfldElem.impliedMetric(M);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric(xfld_box, 1, BasisFunctionCategory_Hierarchical);

  // Set a uniform metric field consistent with the mesh, i.e. a stationary condition
  metric = M;

  refine xfld(metric);
  //output_Tecplot(xfld, "tmp/box.dat");

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_REQUIRE_EQUAL(xfld.nDOF(), xfld_box.nDOF());
  for (int i = 0; i < xfld.nDOF(); i++)
  {
    SANS_CHECK_CLOSE( xfld_box.DOF(i)[0], xfld.DOF(i)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( xfld_box.DOF(i)[1], xfld.DOF(i)[1], small_tol, close_tol );
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Box_scaled_Coarse_to_Fine_test )
{
  typedef XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef DLA::MatrixSymS<PhysD2::D, Real> MatrixSym;

  typedef XField<PhysD2, TopoD2> XFieldType;
  typedef std::shared_ptr<XFieldType> MeshPtr;

  // global communicator
  mpi::communicator world;

  PyDict paramsDict;
  paramsDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;

  int adapt_iter = 0;

  int ii = 4, jj = 4;
  XField2D_Box_Triangle_Lagrange_X1 xfld_box(world, ii, jj);

  // Get the cell group and create an element for it
  XFieldCellGroupType& xfldCellGroup = xfld_box.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem( xfldCellGroup.basis() );

  // Grab just one element from the cell group
  xfldCellGroup.getElement(xfldElem,0);

  // Compute the implied metric of the element
  MatrixSym M;
  xfldElem.impliedMetric(M);

  // Refine the mesh

  // Reduce the size of the elements in the metric
  M *= pow(2.,2);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric(xfld_box, 1, BasisFunctionCategory_Hierarchical);

  // Set a uniform metric field that should increase mesh size
  metric = M;

  MesherInterface<PhysD2, TopoD2, refine> mesher(adapt_iter, paramsDict);
  MeshPtr pxfld = mesher.adapt(metric);
  //output_Tecplot(*pxfld, "tmp/box.dat");

  // Coarsen the mesh

  // Generate a another mesh with an even coarser metric
  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric2(*pxfld, 1, BasisFunctionCategory_Hierarchical);

  M *= pow(1./4.,2);

  metric2 = M;

  MeshPtr pxfld2 = mesher.adapt(metric2);
  //output_Tecplot(xfld2, "tmp/box2.dat");


  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric3(*pxfld2, 1, BasisFunctionCategory_Hierarchical);

  int nnode_metric = metric.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric = boost::mpi::all_reduce(*metric.comm(), nnode_metric, std::plus<int>());
#endif

  int nnode_metric2 = metric2.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric2 = boost::mpi::all_reduce(*metric2.comm(), nnode_metric2, std::plus<int>());
#endif

  int nnode_metric3 = metric3.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric3 = boost::mpi::all_reduce(*metric3.comm(), nnode_metric3, std::plus<int>());
#endif

  BOOST_CHECK_GT( nnode_metric2, nnode_metric );

  BOOST_CHECK_LT( nnode_metric3, nnode_metric2 );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Box_scaled_Fine_to_Coarse_test )
{
  typedef XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef DLA::MatrixSymS<PhysD2::D, Real> MatrixSym;

  typedef XField<PhysD2, TopoD2> XFieldType;
  typedef std::shared_ptr<XFieldType> MeshPtr;

  // global communicator
  mpi::communicator world;

  PyDict paramsDict;
  paramsDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;

  int adapt_iter = 0;

  int ii = 8, jj = 8;
  XField2D_Box_Triangle_Lagrange_X1 xfld_box(world, ii, jj);

  // Get the cell group and create an element for it
  XFieldCellGroupType& xfldCellGroup = xfld_box.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem( xfldCellGroup.basis() );

  // Grab just one element from the cell group
  xfldCellGroup.getElement(xfldElem,0);

  // Compute the implied metric of the element
  MatrixSym M;
  xfldElem.impliedMetric(M);

  // Coarsen the mesh

  // Increase the size of the elements in the metric
  M *= pow(1./2.,2);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric(xfld_box, 1, BasisFunctionCategory_Hierarchical);

  // Set a uniform metric field that should increase mesh size
  metric = M;

  MesherInterface<PhysD2, TopoD2, refine> mesher(adapt_iter, paramsDict);
  MeshPtr pxfld = mesher.adapt(metric);
  //
  // output_Tecplot( *pxfld, "tmp/pxfld.dat" );
  //
  // WriteMesh_libMeshb(*pxfld,"tmp/xfld.mesh");

  // refine the mesh
  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric2(*pxfld, 1, BasisFunctionCategory_Hierarchical);

  xfldElem.impliedMetric(M);
  M *= pow(4.,2);

  metric2 = M;

  // output_Tecplot( *pxfld, "tmp/pxfld.plt" );

  MeshPtr pxfld2 = mesher.adapt(metric2);
  //output_Tecplot(xfld2, "tmp/box2.dat");

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric3(*pxfld2, 1, BasisFunctionCategory_Hierarchical);

  int nnode_metric = metric.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric = boost::mpi::all_reduce(*metric.comm(), nnode_metric, std::plus<int>());
#endif

  int nnode_metric2 = metric2.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric2 = boost::mpi::all_reduce(*metric2.comm(), nnode_metric2, std::plus<int>());
#endif

  int nnode_metric3 = metric3.nDOFpossessed();
#ifdef SANS_MPI
  nnode_metric3 = boost::mpi::all_reduce(*metric3.comm(), nnode_metric3, std::plus<int>());
#endif

  BOOST_CHECK_LT( nnode_metric2, nnode_metric );

  BOOST_CHECK_GT( nnode_metric3, nnode_metric2 );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Box_MultiScale_test )
{
  typedef ScalarFunction2D_Pringle SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;

  typedef DLA::MatrixSymS<PhysD2::D, Real> MatrixSym;

  typedef XField<PhysD2, TopoD2> XFieldType;
  typedef std::shared_ptr<XFieldType> MeshPtr;

  // global communicator
  mpi::communicator world;

  NDSolutionExact solnExact;

  PyDict paramsDict;
  paramsDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;

  int ii = 8, jj = 8;
  MeshPtr pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>(world, ii, jj);

  for (int adapt_iter = 0; adapt_iter < 3; adapt_iter++)
  {
    Field_CG_Cell<PhysD2, TopoD2, Real> sfld(*pxfld, 1, BasisFunctionCategory_Hierarchical);

    std::map<int,int> native2local;
    for (int i = 0; i < pxfld->nDOF(); i++)
      native2local[pxfld->local2nativeDOFmap(i)] = i;

    for (int i = 0; i < pxfld->nDOF(); i++)
      sfld.DOF(i) = solnExact(pxfld->DOF(native2local.at(sfld.local2nativeDOFmap(i))));

    //output_Tecplot( sfld, "tmp/sfld_a" + std::to_string(adapt_iter) + ".dat" );

    int p = 2;
    Real gradation = 5;
    Real complexity = 1e2;

    // compute the multi-scale metric
    Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric(*pxfld, 1, BasisFunctionCategory_Hierarchical);
    MultiScale_metric(refine_recon_Kexact, p, gradation, complexity, sfld, metric);

    MesherInterface<PhysD2, TopoD2, refine> mesher(adapt_iter, paramsDict);
    pxfld = mesher.adapt(metric);

    //Field_CG_Cell<PhysD2, TopoD2, Real> sfld2(*pxfld, 1, BasisFunctionCategory_Hierarchical);
    //sfld2 = 1;
    //output_Tecplot( sfld2, "tmp/sfld2_a" + std::to_string(adapt_iter) + ".dat" );
  }
  //
  // WriteMesh_libMeshb(*pxfld,"tmp/xfld.mesh");
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( NACA0012_test )
{
  typedef XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;

  typedef DLA::MatrixSymS<PhysD2::D, Real> MatrixSym;
  typedef Field_CG_Cell<PhysD2,TopoD2,MatrixSym>::FieldCellGroupType<Triangle> MFieldCellGroupType;
  typedef MFieldCellGroupType::ElementType<> ElementMFieldClass;

  typedef XField<PhysD2, TopoD2> XFieldType;
  typedef std::shared_ptr<XFieldType> MeshPtr;

  // global communicator
  mpi::communicator world;

  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  std::string egadsfile = "tmp/naca.egads";

  if (world.rank() == 0)
  {
    EGADS::EGContext context((EGADS::CreateContext()));

    EGADS::EGNode<Dim> n0(context, {-10.0,-10.0});
    EGADS::EGNode<Dim> n1(context, { 10.0,-10.0});
    EGADS::EGNode<Dim> n2(context, { 10.0, 10.0});
    EGADS::EGNode<Dim> n3(context, {-10.0, 10.0});

    EGADS::EGEdge<Dim> edge0(n0, n1);
    EGADS::EGEdge<Dim> edge1(n1, n2);
    EGADS::EGEdge<Dim> edge2(n2, n3);
    EGADS::EGEdge<Dim> edge3(n3, n0);

    EGADS::EGLoop<Dim>::edge_vector square_edges = {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)};

    EGADS::EGLoop<Dim> square_loop(context, square_edges, CLOSED);


    EGADS::EGApproximate<Dim> NACA_spline = EGADS::NACA4<Dim>(context);

    double xle = 0;
    double xte = 1;

    // Assume the airfoil t-space between 0 and 1 and has a closed trailing edge
    EGADS::EGNode<Dim> LE(context, {xle,0});
    EGADS::EGNode<Dim> TE(context, {xte,0});

    Real LEt = NACA_spline(LE);

    //Create the upper and lower edges on the airfoil
    EGADS::EGEdge<Dim> Upper(NACA_spline, {0,LEt}, TE, LE);
    EGADS::EGEdge<Dim> Lower(NACA_spline, {LEt,1}, LE, TE);

    EGADS::EGLoop<Dim>::edge_vector NACA_edges = {(Lower, 1), (Upper, 1)};

    EGADS::EGLoop<Dim> NACA_loop(context, NACA_edges, CLOSED);

    EGADS::EGBody<Dim> facebody(context, {(square_loop,1), (NACA_loop,-1)} );

    EGADS::EGModel<Dim> model(facebody);

    std::remove(egadsfile.c_str());
    model.save(egadsfile);
  }
  world.barrier();

  PyDict paramsDict;
  paramsDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;

  MeshPtr pxfld = std::make_shared<XField_refine<PhysD2, TopoD2>>(world, egadsfile);
  std::string MeshCADfilename = static_cast<XField_refine<PhysD2, TopoD2>*>(pxfld.get())->getMeshCADfilename();

  {
    // test the restart API
    MeshPtr pxfld2 = std::make_shared<XField_refine<PhysD2, TopoD2>>(world, egadsfile, MeshCADfilename);
  }

  MatrixSym M;
  int adapt_max = 2;

  for (int adapt_iter = 0; adapt_iter < adapt_max; adapt_iter++)
  {
    Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric(*pxfld, 1, BasisFunctionCategory_Hierarchical);

    MFieldCellGroupType& mfldCellGroup = metric.getCellGroup<Triangle>(0);
    ElementMFieldClass mfldElem( mfldCellGroup.basis() );

    // Get the cell group and create an element for it
    XFieldCellGroupType& xfldCellGroup = pxfld->getCellGroup<Triangle>(0);
    ElementXFieldClass xfldElem( xfldCellGroup.basis() );

    // construct a poor implied metric
    for (int elem = 0; elem < xfldCellGroup.nElem(); elem++)
    {
      xfldCellGroup.getElement(xfldElem,elem);

      // Compute the implied metric of the element
      xfldElem.impliedMetric(M);

      for (int n = 0; n < mfldElem.nDOF(); n++)
        mfldElem.DOF(n) = M;

      mfldCellGroup.setElement(mfldElem, elem);
    }

    MesherInterface<PhysD2, TopoD2, refine> mesher(adapt_iter, paramsDict);
    pxfld = mesher.adapt(metric);
  }

  if (world.rank() == 0)
  {
    std::remove(egadsfile.c_str());
    std::remove(MeshCADfilename.c_str());

    std::string filename_base = paramsDict.get(refineParams::params.FilenameBase);
    for (int adapt_iter = 0; adapt_iter < adapt_max; adapt_iter++)
    {
      MeshCADfilename = filename_base + "refine_a" + stringify(adapt_iter) + ".meshb";
      std::remove(MeshCADfilename.c_str());
    }
  }
  world.barrier();
}
#endif

#endif //SANS_REFINE

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
