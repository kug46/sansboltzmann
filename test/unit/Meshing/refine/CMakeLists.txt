INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

RUN_WITH_MPIEXEC( ${MPIEXEC_MAX_NUMPROCS} )

IF( USE_REFINE )
  INCLUDE_DIRECTORIES( ${REFINE_INCLUDE_DIRS} )
ENDIF()

GenerateUnitTests( refineLib 
                   UnitGridsLib
                   libMeshbLib
                   ${LIBMESHB_LIBRARIES} 
                   FieldLib 
                   EGADSLib
                   BasisFunctionLib 
                   QuadratureLib 
                   DenseLinAlgLib 
                   PythonLib 
                   TopologyLib 
                   MPILib
                   toolsLib )

IF (BUILD_TESTING AND ${XDIR_FOLDER})
  SET_TESTS_PROPERTIES(refine2D_btest PROPERTIES TIMEOUT 360)
  SET_TESTS_PROPERTIES(refine3D_btest PROPERTIES TIMEOUT 360)
ENDIF()
