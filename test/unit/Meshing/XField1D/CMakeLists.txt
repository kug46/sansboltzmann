INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

RUN_WITH_MPIEXEC( ${MPIEXEC_MAX_NUMPROCS} )

GenerateUnitTests( XField1DLib
                   FieldLib
                   BasisFunctionLib 
                   QuadratureLib 
                   DenseLinAlgLib
                   TopologyLib
                   toolsLib )

