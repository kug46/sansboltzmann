// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/AFLR/AFLR2.h"

#include "Field/output_Tecplot.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include <vector>
#include <iostream>
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( AFLR2_tesit_suite )
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Box_stationary_test )
{
  typedef XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef DLA::MatrixSymS<PhysD2::D, Real> MatrixSym;

  int ii = 4, jj = 4;
  XField2D_Box_UnionJack_Triangle_X1 xfld_box(ii, jj);

  // Get the cell group and create an element for it
  XFieldCellGroupType& xfldCellGroup = xfld_box.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem( xfldCellGroup.basis() );

  // Grab just one element from the cell group
  xfldCellGroup.getElement(xfldElem,0);

  // Compute the implied metric of the element
  MatrixSym M;
  xfldElem.impliedMetric(M);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric(xfld_box, 1, BasisFunctionCategory_Hierarchical);

  // Set a uniform metric field consistent with the mesh, i.e. a stationary condition
  metric = M;

  AFLR2 xfld(metric);
  //output_Tecplot(xfld, "tmp/box.dat");

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  BOOST_REQUIRE_EQUAL(xfld.nDOF(), xfld_box.nDOF());
  for (int i = 0; i < xfld.nDOF(); i++)
  {
    SANS_CHECK_CLOSE( xfld_box.DOF(i)[0], xfld.DOF(i)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( xfld_box.DOF(i)[1], xfld.DOF(i)[1], small_tol, close_tol );
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Box_scaled_test )
{
  typedef XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> ElementXFieldClass;
  typedef DLA::MatrixSymS<PhysD2::D, Real> MatrixSym;

  int ii = 4, jj = 4;
  XField2D_Box_UnionJack_Triangle_X1 xfld_box(ii, jj);

  // Get the cell group and create an element for it
  XFieldCellGroupType& xfldCellGroup = xfld_box.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem( xfldCellGroup.basis() );

  // Grab just one element from the cell group
  xfldCellGroup.getElement(xfldElem,0);

  // Compute the implied metric of the element
  MatrixSym M;
  xfldElem.impliedMetric(M);

  // Reduce the size of the elements in the metric
  M *= pow(4.,2);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric(xfld_box, 1, BasisFunctionCategory_Hierarchical);

  // Set a uniform metric field that should increase mesh size
  metric = M;

  AFLR2 xfld(metric);
  //output_Tecplot(xfld, "tmp/box.dat");

  BOOST_CHECK_GT( xfld.nDOF(), xfld_box.nDOF() );

  // Generate a nother mesh with an even finer metric
  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric2(xfld, 1, BasisFunctionCategory_Hierarchical);

  M *= pow(2.,2);

  metric2 = M;

  AFLR2 xfld2(metric2);
  //output_Tecplot(xfld2, "tmp/box2.dat");

  BOOST_CHECK_GT( xfld2.nDOF(), xfld.nDOF() );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
