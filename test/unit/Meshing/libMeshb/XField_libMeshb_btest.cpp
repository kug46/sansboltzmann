// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// ReadMesh_libMeshb_btest
// testing of the ReadMesh class for libMeshb

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <cstdio>
#include <iostream>

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ReadMesh_libMeshb_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadMesh_libMeshb_4Triangle)
{
  mpi::communicator world;

  XField2D_4Triangle_X1_1Group xfld_orig;

  //Test ASCII files
  std::string filename1 = "IO/mesh.mesh";

  if (world.rank() == 0)
    WriteMesh_libMeshb(xfld_orig, filename1);

  world.barrier();

  XField_libMeshb<PhysD2, TopoD2> xfld(world.split(world.rank()), filename1);

  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 6 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], -1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], -1 );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);

  BOOST_REQUIRE_EQUAL( xfldArea.nElem(), 4 );

  int nodeMap[3];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  xfldArea.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );

  xfldArea.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  // interior-edge field variable

  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge = xfld.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( xfldBedge.nElem(), 6 );

  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );

  xfldBedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldBedge.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBedge.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldBedge.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldBedge.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  BOOST_CHECK_EQUAL( xfldBedge.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(0), 3 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(0), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(0).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(1), 3 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(1), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(1).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(2), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(2), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(2).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(2).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(3), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(3), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(3).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(3).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(4), 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(4), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(4).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(4).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(5), 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(5), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(5).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(5).trace, -1 );

  world.barrier();

  //Test ASCII files
  std::string filename2 = "IO/mesh2.mesh";

  if (world.rank() == 0)
  {
    WriteMesh_libMeshb(xfld,filename2); //Export the reconstructed mesh

    mapped_file_source f1(filename1);
    mapped_file_source f2(filename2);

    //The contents of both exported mesh files should be equal
    BOOST_CHECK( f1.size() == f2.size() && std::equal(f1.data(), f1.data() + f1.size(), f2.data()) );

    //Delete the written files
    std::remove(filename1.c_str());
    std::remove(filename2.c_str());
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadMesh_libMeshb_Box_UnionJack_Triangle)
{
  mpi::communicator world;

  XField2D_Box_UnionJack_Triangle_X1 xfld_orig(2,2);

  //Test ASCII files
  std::string filename1 = "IO/mesh.mesh";

  if (world.rank() == 0)
    WriteMesh_libMeshb(xfld_orig,filename1);

  world.barrier();

  XField_libMeshb<PhysD2, TopoD2> xfld(world.split(world.rank()), filename1);

  Real tol = 1e-12;

  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 9 );

  //Check the node values
  for (int n = 0; n < xfld.nDOF(); n++)
  {
    SANS_CHECK_CLOSE( xfld.DOF(n)[0], xfld_orig.DOF(n)[0], tol, tol );
    SANS_CHECK_CLOSE( xfld.DOF(n)[1], xfld_orig.DOF(n)[1], tol, tol );
  }

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);
  BOOST_CHECK_EQUAL( xfldArea.nElem(), 8 );

  const XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle>& xfldAreaTrue = xfld_orig.getCellGroup<Triangle>(0);

  int nodeMap[3];
  int nodeMapTrue[3];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  xfldAreaTrue.associativity(0).getNodeGlobalMapping( nodeMapTrue, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );
  BOOST_CHECK_EQUAL( nodeMap[2], nodeMapTrue[2] );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  xfldAreaTrue.associativity(1).getNodeGlobalMapping( nodeMapTrue, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );
  BOOST_CHECK_EQUAL( nodeMap[2], nodeMapTrue[2] );

  xfldArea.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  xfldAreaTrue.associativity(2).getNodeGlobalMapping( nodeMapTrue, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );
  BOOST_CHECK_EQUAL( nodeMap[2], nodeMapTrue[2] );

  xfldArea.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  xfldAreaTrue.associativity(3).getNodeGlobalMapping( nodeMapTrue, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );
  BOOST_CHECK_EQUAL( nodeMap[2], nodeMapTrue[2] );

  xfldArea.associativity(4).getNodeGlobalMapping( nodeMap, 3 );
  xfldAreaTrue.associativity(4).getNodeGlobalMapping( nodeMapTrue, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );
  BOOST_CHECK_EQUAL( nodeMap[2], nodeMapTrue[2] );

  xfldArea.associativity(5).getNodeGlobalMapping( nodeMap, 3 );
  xfldAreaTrue.associativity(5).getNodeGlobalMapping( nodeMapTrue, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );
  BOOST_CHECK_EQUAL( nodeMap[2], nodeMapTrue[2] );

  xfldArea.associativity(6).getNodeGlobalMapping( nodeMap, 3 );
  xfldAreaTrue.associativity(6).getNodeGlobalMapping( nodeMapTrue, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );
  BOOST_CHECK_EQUAL( nodeMap[2], nodeMapTrue[2] );

  xfldArea.associativity(7).getNodeGlobalMapping( nodeMap, 3 );
  xfldAreaTrue.associativity(7).getNodeGlobalMapping( nodeMapTrue, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );
  BOOST_CHECK_EQUAL( nodeMap[2], nodeMapTrue[2] );

  // interior-edge field variable

  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(3).topoTypeID() == typeid(Line) );

  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge3 = xfld.getBoundaryTraceGroup<Line>(3);
  BOOST_REQUIRE_EQUAL( xfldBedge0.nElem(), 2 );
  BOOST_REQUIRE_EQUAL( xfldBedge1.nElem(), 2 );
  BOOST_REQUIRE_EQUAL( xfldBedge2.nElem(), 2 );
  BOOST_REQUIRE_EQUAL( xfldBedge3.nElem(), 2 );

  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedgeTrue0 = xfld_orig.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedgeTrue1 = xfld_orig.getBoundaryTraceGroup<Line>(1);
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedgeTrue2 = xfld_orig.getBoundaryTraceGroup<Line>(2);
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedgeTrue3 = xfld_orig.getBoundaryTraceGroup<Line>(3);


  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  xfldBedgeTrue0.associativity(0).getNodeGlobalMapping( nodeMapTrue, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );

  xfldBedge0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  xfldBedgeTrue0.associativity(1).getNodeGlobalMapping( nodeMapTrue, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );

  BOOST_CHECK_EQUAL( xfldBedge0.getGroupLeft() ,  0 );
  BOOST_CHECK_EQUAL( xfldBedge0.getGroupRight(), -1 );

  int elem = 0;
  BOOST_CHECK_EQUAL( xfldBedge0.getElementLeft (elem),  1 );
  BOOST_CHECK_EQUAL( xfldBedge0.getElementRight(elem), -1 );
  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft (elem).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceRight(elem).trace, -1 );

  elem = 1;
  BOOST_CHECK_EQUAL( xfldBedge0.getElementLeft (elem),  2 );
  BOOST_CHECK_EQUAL( xfldBedge0.getElementRight(elem), -1 );
  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft (elem).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceRight(elem).trace, -1 );


  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  xfldBedgeTrue1.associativity(0).getNodeGlobalMapping( nodeMapTrue, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );

  xfldBedge1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  xfldBedgeTrue1.associativity(1).getNodeGlobalMapping( nodeMapTrue, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );

  BOOST_CHECK_EQUAL( xfldBedge1.getGroupLeft() ,  0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getGroupRight(), -1 );

  elem = 0;
  BOOST_CHECK_EQUAL( xfldBedge1.getElementLeft (elem),  3 );
  BOOST_CHECK_EQUAL( xfldBedge1.getElementRight(elem), -1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft (elem).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceRight(elem).trace, -1 );

  elem = 1;
  BOOST_CHECK_EQUAL( xfldBedge1.getElementLeft (elem),  7 );
  BOOST_CHECK_EQUAL( xfldBedge1.getElementRight(elem), -1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft (elem).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceRight(elem).trace, -1 );


  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  xfldBedgeTrue2.associativity(0).getNodeGlobalMapping( nodeMapTrue, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );

  xfldBedge2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  xfldBedgeTrue2.associativity(1).getNodeGlobalMapping( nodeMapTrue, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );

  BOOST_CHECK_EQUAL( xfldBedge2.getGroupLeft() ,  0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getGroupRight(), -1 );

  elem = 0;
  BOOST_CHECK_EQUAL( xfldBedge2.getElementLeft (elem),  6 );
  BOOST_CHECK_EQUAL( xfldBedge2.getElementRight(elem), -1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft (elem).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceRight(elem).trace, -1 );

  elem = 1;
  BOOST_CHECK_EQUAL( xfldBedge2.getElementLeft (elem),  5 );
  BOOST_CHECK_EQUAL( xfldBedge2.getElementRight(elem), -1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft (elem).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceRight(elem).trace, -1 );


  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  xfldBedgeTrue3.associativity(0).getNodeGlobalMapping( nodeMapTrue, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );

  xfldBedge3.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  xfldBedgeTrue3.associativity(1).getNodeGlobalMapping( nodeMapTrue, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], nodeMapTrue[0] );
  BOOST_CHECK_EQUAL( nodeMap[1], nodeMapTrue[1] );

  BOOST_CHECK_EQUAL( xfldBedge3.getGroupLeft() ,  0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getGroupRight(), -1 );

  elem = 0;
  BOOST_CHECK_EQUAL( xfldBedge3.getElementLeft (elem),  4 );
  BOOST_CHECK_EQUAL( xfldBedge3.getElementRight(elem), -1 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceLeft (elem).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceRight(elem).trace, -1 );

  elem = 1;
  BOOST_CHECK_EQUAL( xfldBedge3.getElementLeft (elem),  0 );
  BOOST_CHECK_EQUAL( xfldBedge3.getElementRight(elem), -1 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceLeft (elem).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge3.getCanonicalTraceRight(elem).trace, -1 );

  world.barrier();

  //Test ASCII files
  std::string filename2 = "IO/mesh2.mesh";

  if (world.rank() == 0)
  {
    WriteMesh_libMeshb(xfld,filename2); //Export the reconstructed mesh

    mapped_file_source f1(filename1);
    mapped_file_source f2(filename2);

    //The contents of both exported mesh files should be equal
    BOOST_CHECK( f1.size() == f2.size() && std::equal(f1.data(), f1.data() + f1.size(), f2.data()) );

    //Delete the written files
    std::remove(filename1.c_str());
    std::remove(filename2.c_str());
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadMesh_libMeshb_6Tet)
{
  mpi::communicator world;

  XField3D_6Tet_X1_1Group xfld_orig;

  //Test ASCII files
  std::string filename1 = "IO/mesh.mesh";

  if (world.rank() == 0)
    WriteMesh_libMeshb(xfld_orig,filename1);

  world.barrier();

  XField_libMeshb<PhysD3, TopoD3> xfld(world.split(world.rank()), filename1);

  //Note that the node ordering of the reconstructed mesh is different to that of XField3D_6Tet_X1_1Group
  BOOST_CHECK_EQUAL( xfld.nDOF(), 8 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[2], 1 );

  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  const XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>& xfldVol = xfld.getCellGroup<Tet>(0);

  int nodeMap[4];
  int elemL;
  int faceL;

  // Index table for each tet in the hex
  const int tets[6][4] = { {0, 1, 2, 4},
                           {1, 4, 3, 2},
                           {6, 2, 3, 4},

                           {1, 4, 5, 3},
                           {4, 6, 5, 3},
                           {7, 6, 3, 5} };

  // Left prism
  xfldVol.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[0][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[0][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[0][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[0][3] );

  xfldVol.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[1][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[1][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[1][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[1][3] );

  xfldVol.associativity(2).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[2][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[2][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[2][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[2][3] );

  // Right prism
  xfldVol.associativity(3).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[3][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[3][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[3][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[3][3] );

  xfldVol.associativity(4).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[4][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[4][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[4][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[4][3] );

  xfldVol.associativity(5).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[5][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[5][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[5][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[5][3] );

  // interior-face field variable
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldIface = xfld.getInteriorTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldIface.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIface.getGroupRight(), 0 );

  // interior face-to-cell connectivity

  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct

  // boundary-face field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBface = xfld.getBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldBface.getGroupLeft(), 0 );

  int faceB = 0;

  elemL = 0; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 0; faceL = 2;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 0; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 1; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 2; faceL = 2;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 2; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 3; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 3; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 4; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 5; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 5; faceL = 2;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 5; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  BOOST_CHECK_EQUAL( 12, faceB );

  world.barrier();

  //Test ASCII files
  std::string filename2 = "IO/mesh2.mesh";

  if (world.rank() == 0)
  {
    WriteMesh_libMeshb(xfld,filename2); //Export the reconstructed mesh

    mapped_file_source f1(filename1);
    mapped_file_source f2(filename2);

    //The contents of both exported mesh files should be equal
    BOOST_CHECK( f1.size() == f2.size() && std::equal(f1.data(), f1.data() + f1.size(), f2.data()) );

    //Delete the written files
    std::remove(filename1.c_str());
    std::remove(filename2.c_str());
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
