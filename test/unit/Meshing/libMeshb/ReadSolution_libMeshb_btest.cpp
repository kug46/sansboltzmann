// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// ReadSolution_libMeshb_btest
// testing of the ReadSolution_libMeshb function

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <cstdio>
#include <iostream>

#include "Meshing/libMeshb/WriteSolution_libMeshb.h"
#include "Meshing/libMeshb/ReadSolution_libMeshb.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ReadSolution_libMeshb_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Write_2D_Vector_P1 )
{
  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;

  typedef DLA::VectorS<2,Real> ArrayQ;

  mpi::communicator world;

  int ii = 4, jj = 5;
  XField2D_Box_Triangle_Lagrange_X1 xfld(world,ii,jj);

  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld_out(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld_in (xfld, 1, BasisFunctionCategory_Hierarchical);

  // set the field based on the global native node numbering
  ArrayQ q0 = { 0.2, 0.3 };
  for (int n = 0; n < qfld_out.nDOF(); n++)
    qfld_out.DOF(n) = q0*(qfld_out.local2nativeDOFmap(n)+1);

  //Test ASCII files
  std::string sol_filename = "IO/testRead_libMeshb.sol";

  WriteSolution_libMeshb(qfld_out, sol_filename);
  ReadSolution_libMeshb(qfld_in, sol_filename);

  for (int i = 0; i < qfld_out.nDOF(); i++)
  {
    ArrayQ qTrue = qfld_out.DOF(i);
    ArrayQ q     = qfld_in.DOF(i);

    SANS_CHECK_CLOSE( qTrue[0], q[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( qTrue[1], q[1], small_tol, close_tol );
  }

  //Delete the written files
  if (world.rank() == 0)
    std::remove(sol_filename.c_str());


  //Test bindary files
  std::string sol_filenameb = "IO/testRead_libMeshb.solb";

  qfld_in = 0;

  WriteSolution_libMeshb(qfld_out, sol_filenameb);
  ReadSolution_libMeshb(qfld_in, sol_filenameb);

  for (int i = 0; i < qfld_out.nDOF(); i++)
  {
    ArrayQ qTrue = qfld_out.DOF(i);
    ArrayQ q     = qfld_in.DOF(i);

    SANS_CHECK_CLOSE( qTrue[0], q[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( qTrue[1], q[1], small_tol, close_tol );
  }

  //Delete the written files
  if (world.rank() == 0)
    std::remove(sol_filenameb.c_str());
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Write_2D_SymmetricMatrix_P1 )
{
  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;

  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  mpi::communicator world;

  int ii = 4, jj = 5;
  XField2D_Box_Triangle_Lagrange_X1 xfld(world,ii,jj);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> mfld_out(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> mfld_in (xfld, 1, BasisFunctionCategory_Hierarchical);

  // set the field based on the global native node numbering
  MatrixSym M0 = {{ 0.2},{-0.5, 0.3}};
  for (int n = 0; n < mfld_out.nDOF(); n++)
    mfld_out.DOF(n) = M0*(mfld_out.local2nativeDOFmap(n)+1);

  //Test ASCII files
  std::string sol_filename = "IO/testRead_libMeshb.sol";

  WriteSolution_libMeshb(mfld_out, sol_filename);
  ReadSolution_libMeshb(mfld_in, sol_filename);


  for (int i = 0; i < mfld_out.nDOF(); i++)
  {
    MatrixSym MTrue = mfld_out.DOF(i);
    MatrixSym M     = mfld_in.DOF(i);

    // check that the matrix is identical as that written
    SANS_CHECK_CLOSE( MTrue(0,0), M(0,0), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(1,0), M(1,0), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(1,1), M(1,1), small_tol, close_tol );
  }

  //Delete the written files
  if (world.rank() == 0)
    std::remove(sol_filename.c_str());


  //Test bindary files
  std::string sol_filenameb = "IO/testRead_libMeshb.solb";

  mfld_in = 0;

  WriteSolution_libMeshb(mfld_out, sol_filenameb);
  ReadSolution_libMeshb(mfld_in, sol_filenameb);

  for (int i = 0; i < mfld_out.nDOF(); i++)
  {
    MatrixSym MTrue = mfld_out.DOF(i);
    MatrixSym M     = mfld_in.DOF(i);

    // check that the matrix is identical as that written
    SANS_CHECK_CLOSE( MTrue(0,0), M(0,0), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(1,0), M(1,0), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(1,1), M(1,1), small_tol, close_tol );
  }

  //Delete the written files
  if (world.rank() == 0)
    std::remove(sol_filenameb.c_str());
}

// libMeshb is working on higher-order solution elements
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WriteMeshMetric_FeFloa_2D_SymmetricMatrix_P2 )
{
  typedef DLA::MatrixSymS<2, Real> ArrayQ;

  XField2D_4Triangle_X1_1Group xfld;

  int order = 2;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  qfld.DOF( 9) = {{ 0.2},{-0.5, 0.3}};
  qfld.DOF(10) = {{ 1.2},{ 0.4, 0.6}};
  qfld.DOF(11) = {{ 2.0},{ 0.0, 1.0}};
  qfld.DOF(12) = {{ 0.8},{-0.1, 1.7}};
  qfld.DOF(13) = {{ 1.1},{ 0.2, 0.9}};
  qfld.DOF(14) = {{ 1.0},{ 0.0, 1.0}};

  //Test ASCII files
  std::string mesh_filename = "IO/testRead_libMeshb.mesh";
  std::string sol_filename = "IO/testRead_libMeshb.sol";

  WriteMesh_libMeshb(xfld,mesh_filename);

  WriteSolution_libMeshb(qfld, sol_filename);

  libMeshb_Reader<PhysD2> reader(sol_filename);

  int nSol, nTypes, nReals, Types[2];

  nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

  BOOST_CHECK_EQUAL( nSol, 6);
  BOOST_CHECK_EQUAL( nTypes, 1);
  BOOST_CHECK_EQUAL( nReals, 3);
  BOOST_CHECK_EQUAL( Types[0], GmfSymMat);

  reader.gotoKeyword( GmfSolAtVertices );

  Real tol = 1e-13;

  for (int i = 0; i < nSol; i++)
  {
    Real data[3];
    reader.getLine( GmfSolAtVertices, &data[0], &data[1], &data[2] );

    SANS_CHECK_CLOSE( data[0], qfld.DOF(9+i)(0,0), tol, tol );
    SANS_CHECK_CLOSE( data[1], qfld.DOF(9+i)(1,0), tol, tol );
    SANS_CHECK_CLOSE( data[2], qfld.DOF(9+i)(1,1), tol, tol );
  }

  //Delete the written files
  std::remove(mesh_filename.c_str());
  std::remove(sol_filename.c_str());
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Write_3D_Vector_P1 )
{
  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;

  typedef DLA::VectorS<3,Real> ArrayQ;

  mpi::communicator world;

  int ii = 2, jj = 3, kk = 4;
  XField3D_Box_Tet_X1 xfld(world,ii,jj,kk);

  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld_out(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld_in (xfld, 1, BasisFunctionCategory_Hierarchical);

  // set the field based on the global native node numbering
  ArrayQ q0 = {0.2, 0.3,-0.5};
  for (int n = 0; n < qfld_out.nDOF(); n++)
    qfld_out.DOF(n) = q0*(qfld_out.local2nativeDOFmap(n)+1);

  //Test ASCII files
  std::string sol_filename = "IO/testRead_libMeshb.sol";

  WriteSolution_libMeshb(qfld_out, sol_filename);
  ReadSolution_libMeshb(qfld_in, sol_filename);

  for (int i = 0; i < qfld_out.nDOF(); i++)
  {
    ArrayQ qTrue = qfld_out.DOF(i);
    ArrayQ q     = qfld_in.DOF(i);

    SANS_CHECK_CLOSE( qTrue[0], q[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( qTrue[1], q[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( qTrue[2], q[2], small_tol, close_tol );
  }

  //Delete the written files
  if (world.rank() == 0)
    std::remove(sol_filename.c_str());


  //Test binary files
  std::string sol_filenameb = "IO/testRead_libMeshb.solb";

  qfld_in = 0;

  WriteSolution_libMeshb(qfld_out, sol_filenameb);
  ReadSolution_libMeshb(qfld_in, sol_filenameb);

  for (int i = 0; i < qfld_out.nDOF(); i++)
  {
    ArrayQ qTrue = qfld_out.DOF(i);
    ArrayQ q     = qfld_in.DOF(i);

    SANS_CHECK_CLOSE( qTrue[0], q[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( qTrue[1], q[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( qTrue[2], q[2], small_tol, close_tol );
  }

  //Delete the written files
  if (world.rank() == 0)
    std::remove(sol_filenameb.c_str());
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Write_3D_SymmetricMatrix_P1 )
{
  const Real close_tol = 1e-13;
  const Real small_tol = 1e-13;

  typedef DLA::MatrixSymS<3, Real> MatrixSym;

  mpi::communicator world;

  int ii = 2, jj = 3, kk = 4;
  XField3D_Box_Tet_X1 xfld(world,ii,jj,kk);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> mfld_out(xfld, 1, BasisFunctionCategory_Hierarchical);
  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> mfld_in (xfld, 1, BasisFunctionCategory_Hierarchical);

  // set the field based on the global native node numbering
  MatrixSym M0 = {{ 0.2},{-0.5, 0.3},{ 0.1, 0.6, 1.2}};
  for (int n = 0; n < mfld_out.nDOF(); n++)
    mfld_out.DOF(n) = M0*(mfld_out.local2nativeDOFmap(n)+1);

  //Test ASCII files
  std::string sol_filename = "IO/testRead_libMeshb.sol";

  WriteSolution_libMeshb(mfld_out, sol_filename);
  ReadSolution_libMeshb(mfld_in, sol_filename);

  for (int i = 0; i < mfld_out.nDOF(); i++)
  {
    MatrixSym MTrue = mfld_out.DOF(i);
    MatrixSym M     = mfld_in.DOF(i);

    // check that the matrix is identical as that written
    SANS_CHECK_CLOSE( MTrue(0,0), M(0,0), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(1,0), M(1,0), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(1,1), M(1,1), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(2,0), M(2,0), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(2,1), M(2,1), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(2,2), M(2,2), small_tol, close_tol );
  }

  //Delete the written files
  if (world.rank() == 0)
    std::remove(sol_filename.c_str());


  //Test binary files
  std::string sol_filenameb = "IO/testRead_libMeshb.solb";

  mfld_in = 0;

  WriteSolution_libMeshb(mfld_out, sol_filenameb);
  ReadSolution_libMeshb(mfld_in, sol_filenameb);

  for (int i = 0; i < mfld_out.nDOF(); i++)
  {
    MatrixSym MTrue = mfld_out.DOF(i);
    MatrixSym M     = mfld_in.DOF(i);

    // check that the matrix is identical as that written
    SANS_CHECK_CLOSE( MTrue(0,0), M(0,0), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(1,0), M(1,0), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(1,1), M(1,1), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(2,0), M(2,0), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(2,1), M(2,1), small_tol, close_tol );
    SANS_CHECK_CLOSE( MTrue(2,2), M(2,2), small_tol, close_tol );
  }

  //Delete the written files
  if (world.rank() == 0)
    std::remove(sol_filenameb.c_str());
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
