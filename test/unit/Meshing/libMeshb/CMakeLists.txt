INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

RUN_WITH_MPIEXEC( 4 )

GenerateUnitTests( libMeshbLib
                   UnitGridsLib 
                   FieldLib 
                   DenseLinAlgLib 
                   BasisFunctionLib 
                   QuadratureLib 
                   TopologyLib
                   PythonLib 
                   toolsLib
                   ${LIBMESHB_LIBRARIES}
                  )
