// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// WriteMesh_libMeshb_btest
// testing of the WriteMesh class for FeFloa

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <cstdio>
#include <iostream>

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/libMeshb/XField_libMeshb.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( WriteMesh_libMeshb_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WriteMesh_libMeshb_Triangle )
{
  mpi::communicator world;

  XField2D_Box_Triangle_Lagrange_X1 xfld_X1(world,4,4);

  //Test ASCII files
  std::string filename1 = "IO/mesh_X1.mesh";
  std::string filename2 = "IO/mesh_X1_rewrite.mesh";

  WriteMesh_libMeshb(xfld_X1,filename1);

  XField_libMeshb<PhysD2, TopoD2> xfld_readin_X1(world, filename1);

  WriteMesh_libMeshb(xfld_readin_X1,filename2);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    mapped_file_source f1(filename1);
    mapped_file_source f2(filename2);

    //Check if the contents of file1 and file2 are equal
    BOOST_CHECK( f1.size() == f2.size() && std::equal(f1.data(), f1.data() + f1.size(), f2.data()) );

    //Delete the written files
    std::remove(filename1.c_str());
    std::remove(filename2.c_str());
  }


  //Test binary files
  std::string filename1b = "IO/mesh_X1.meshb";
  std::string filename2b = "IO/mesh_X1_rewrite.meshb";

  WriteMesh_libMeshb(xfld_X1,filename1b);

  XField_libMeshb<PhysD2, TopoD2> xfld_readin_X1b(world, filename1b);

  WriteMesh_libMeshb(xfld_readin_X1b,filename2b);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    mapped_file_source f1b(filename1b);
    mapped_file_source f2b(filename2b);

    //Check if the contents of file1b and file2b are equal
    BOOST_CHECK( f1b.size() == f2b.size() && std::equal(f1b.data(), f1b.data() + f1b.size(), f2b.data()) );

    //Delete the written files
    std::remove(filename1b.c_str());
    std::remove(filename2b.c_str());
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WriteMesh_libMeshb_Tet )
{
  mpi::communicator world;

  XField3D_Box_Tet_X1 xfld_X1(world,2,3,4);

  //Test ASCII files
  std::string filename1 = "IO/mesh_X1.mesh";
  std::string filename2 = "IO/mesh_X2.mesh";

  WriteMesh_libMeshb(xfld_X1,filename1);

  XField_libMeshb<PhysD3, TopoD3> xfld_readin_X1(world, filename1);

  WriteMesh_libMeshb(xfld_readin_X1,filename2);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    mapped_file_source f1(filename1);
    mapped_file_source f2(filename2);

    //Check if the contents of file1 and file2 are equal
    BOOST_CHECK( f1.size() == f2.size() && std::equal(f1.data(), f1.data() + f1.size(), f2.data()) );

    //Delete the written files
    std::remove(filename1.c_str());
    std::remove(filename2.c_str());
  }

  //Test binary files
  std::string filename1b = "IO/mesh_X1.meshb";
  std::string filename2b = "IO/mesh_X2.meshb";

  WriteMesh_libMeshb(xfld_X1,filename1b);

  XField_libMeshb<PhysD3, TopoD3> xfld_readin_X1b(world, filename1b);

  WriteMesh_libMeshb(xfld_readin_X1b,filename2b);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    mapped_file_source f1b(filename1b);
    mapped_file_source f2b(filename2b);

    //Check if the contents of file1b and file2b are equal
    BOOST_CHECK( f1b.size() == f2b.size() && std::equal(f1b.data(), f1b.data() + f1b.size(), f2b.data()) );

    //Delete the written files
    std::remove(filename1b.c_str());
    std::remove(filename2b.c_str());
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
