// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// WriteSolution_libMeshb_btest
// testing of the WriteSolution_libMeshb function

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <cstdio>
#include <iostream>

#include "Meshing/libMeshb/libMeshb_Interface.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/libMeshb/WriteSolution_libMeshb.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( WriteSolution_libMeshb_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Write_2D_Vector_P1 )
{
  typedef DLA::VectorS<2,Real> ArrayQ;

  mpi::communicator world;

  int ii = 4, jj = 5;
  int nNode = (ii+1)*(jj+1);
  XField2D_Box_Triangle_Lagrange_X1 xfld(world,ii,jj);

  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  // set the field based on the global native node numbering
  ArrayQ q0 = { 0.2, 0.3};
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = q0*(qfld.local2nativeDOFmap(n)+1);

  //Test ASCII files
  std::string sol_filename = "IO/testWrite_libMeshbWrite_libMeshb.sol";

  WriteSolution_libMeshb(qfld, sol_filename);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    libMeshb_Reader<PhysD2> reader(sol_filename);

    int nSol, nTypes, nReals, Types[2];

    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    BOOST_CHECK_EQUAL( nSol, nNode );
    BOOST_CHECK_EQUAL( nTypes, 1 );
    BOOST_CHECK_EQUAL( nReals, 2 );
    BOOST_CHECK_EQUAL( Types[0], GmfVec );

    reader.gotoKeyword( GmfSolAtVertices );

    Real tol = 1e-13;

    for (int i = 0; i < nSol; i++)
    {
      Real data[2];
      reader.getLine( GmfSolAtVertices, &data[0], &data[1] );

      ArrayQ qTrue = q0*(i+1);

      SANS_CHECK_CLOSE( qTrue[0], data[0], tol, tol );
      SANS_CHECK_CLOSE( qTrue[1], data[1], tol, tol );
    }

    //Delete the written files
    std::remove(sol_filename.c_str());
  }


  //Test bindary files
  std::string sol_filenameb = "IO/testWrite_libMeshbWrite_libMeshb.solb";

  WriteSolution_libMeshb(qfld, sol_filenameb);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    libMeshb_Reader<PhysD2> reader(sol_filenameb);

    int nSol, nTypes, nReals, Types[2];

    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    BOOST_CHECK_EQUAL( nSol, nNode );
    BOOST_CHECK_EQUAL( nTypes, 1 );
    BOOST_CHECK_EQUAL( nReals, 2 );
    BOOST_CHECK_EQUAL( Types[0], GmfVec );

    reader.gotoKeyword( GmfSolAtVertices );

    Real tol = 1e-13;

    for (int i = 0; i < nSol; i++)
    {
      Real data[2];
      reader.getLine( GmfSolAtVertices, &data[0], &data[1] );

      ArrayQ qTrue = q0*(i+1);

      SANS_CHECK_CLOSE( qTrue[0], data[0], tol, tol );
      SANS_CHECK_CLOSE( qTrue[1], data[1], tol, tol );
    }

    //Delete the written files
    std::remove(sol_filenameb.c_str());
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Write_2D_SymmetricMatrix_P1 )
{
  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  mpi::communicator world;

  int ii = 4, jj = 5;
  int nNode = (ii+1)*(jj+1);
  XField2D_Box_Triangle_Lagrange_X1 xfld(world,ii,jj);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> mfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  // set the field based on the global native node numbering
  MatrixSym M0 = {{ 0.2},{-0.5, 0.3}};
  for (int n = 0; n < mfld.nDOF(); n++)
    mfld.DOF(n) = M0*(mfld.local2nativeDOFmap(n)+1);

  //Test ASCII files
  std::string sol_filename = "IO/testWrite_libMeshbWrite_libMeshb.sol";

  WriteSolution_libMeshb(mfld, sol_filename);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    libMeshb_Reader<PhysD2> reader(sol_filename);

    int nSol, nTypes, nReals, Types[2];

    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    BOOST_CHECK_EQUAL( nSol, nNode );
    BOOST_CHECK_EQUAL( nTypes, 1 );
    BOOST_CHECK_EQUAL( nReals, 3 );
    BOOST_CHECK_EQUAL( Types[0], GmfSymMat );

    reader.gotoKeyword( GmfSolAtVertices );

    Real tol = 1e-13;

    for (int i = 0; i < nSol; i++)
    {
      Real data[3];
      reader.getLine( GmfSolAtVertices, &data[0], &data[1], &data[2] );

      MatrixSym MTrue = M0*(i+1);

      // check that the matrix was written as lower triangular
      SANS_CHECK_CLOSE( MTrue(0,0), data[0], tol, tol );
      SANS_CHECK_CLOSE( MTrue(1,0), data[1], tol, tol );
      SANS_CHECK_CLOSE( MTrue(1,1), data[2], tol, tol );
    }

    //Delete the written files
    std::remove(sol_filename.c_str());
  }


  //Test bindary files
  std::string sol_filenameb = "IO/testWrite_libMeshb.solb";

  WriteSolution_libMeshb(mfld, sol_filenameb);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    libMeshb_Reader<PhysD2> reader(sol_filenameb);

    int nSol, nTypes, nReals, Types[2];

    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    BOOST_CHECK_EQUAL( nSol, nNode );
    BOOST_CHECK_EQUAL( nTypes, 1 );
    BOOST_CHECK_EQUAL( nReals, 3 );
    BOOST_CHECK_EQUAL( Types[0], GmfSymMat );

    reader.gotoKeyword( GmfSolAtVertices );

    Real tol = 1e-13;

    for (int i = 0; i < nSol; i++)
    {
      Real data[3];
      reader.getLine( GmfSolAtVertices, &data[0],
                                        &data[1], &data[2] );

      MatrixSym MTrue = M0*(i+1);

      // check that the matrix was written as lower triangular
      SANS_CHECK_CLOSE( MTrue(0,0), data[0], tol, tol );
      SANS_CHECK_CLOSE( MTrue(1,0), data[1], tol, tol );
      SANS_CHECK_CLOSE( MTrue(1,1), data[2], tol, tol );
    }

    //Delete the written files
    std::remove(sol_filenameb.c_str());
  }
}

// libMeshb is working on higher-order solution elements
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WriteMeshMetric_FeFloa_2D_SymmetricMatrix_P2 )
{
  typedef DLA::MatrixSymS<2, Real> ArrayQ;

  XField2D_4Triangle_X1_1Group xfld;

  int order = 2;
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  qfld.DOF( 9) = {{ 0.2},{-0.5, 0.3}};
  qfld.DOF(10) = {{ 1.2},{ 0.4, 0.6}};
  qfld.DOF(11) = {{ 2.0},{ 0.0, 1.0}};
  qfld.DOF(12) = {{ 0.8},{-0.1, 1.7}};
  qfld.DOF(13) = {{ 1.1},{ 0.2, 0.9}};
  qfld.DOF(14) = {{ 1.0},{ 0.0, 1.0}};

  //Test ASCII files
  std::string mesh_filename = "IO/testWrite_libMeshb.mesh";
  std::string sol_filename = "IO/testWrite_libMeshb.sol";

  WriteMesh_libMeshb(xfld,mesh_filename);

  WriteSolution_libMeshb(qfld, sol_filename);

  libMeshb_Reader<PhysD2> reader(sol_filename);

  int nSol, nTypes, nReals, Types[2];

  nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

  BOOST_CHECK_EQUAL( nSol, 6);
  BOOST_CHECK_EQUAL( nTypes, 1);
  BOOST_CHECK_EQUAL( nReals, 3);
  BOOST_CHECK_EQUAL( Types[0], GmfSymMat);

  reader.gotoKeyword( GmfSolAtVertices );

  Real tol = 1e-13;

  for (int i = 0; i < nSol; i++)
  {
    Real data[3];
    reader.getLine( GmfSolAtVertices, &data[0], &data[1], &data[2] );

    SANS_CHECK_CLOSE( data[0], qfld.DOF(9+i)(0,0), tol, tol );
    SANS_CHECK_CLOSE( data[1], qfld.DOF(9+i)(1,0), tol, tol );
    SANS_CHECK_CLOSE( data[2], qfld.DOF(9+i)(1,1), tol, tol );
  }

  //Delete the written files
  std::remove(mesh_filename.c_str());
  std::remove(sol_filename.c_str());
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Write_3D_Vector_P1 )
{
  typedef DLA::VectorS<3,Real> ArrayQ;

  mpi::communicator world;

  int ii = 2, jj = 3, kk = 4;
  int nNode = (ii+1)*(jj+1)*(kk+1);
  XField3D_Box_Tet_X1 xfld(world,ii,jj,kk);

  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  // set the field based on the global native node numbering
  ArrayQ q0 = {0.2, 0.3,-0.5};
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = q0*(qfld.local2nativeDOFmap(n)+1);

  //Test ASCII files
  std::string sol_filename = "IO/testWrite_libMeshb.sol";

  WriteSolution_libMeshb(qfld, sol_filename);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    libMeshb_Reader<PhysD3> reader(sol_filename);

    int nSol, nTypes, nReals, Types[2];

    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    BOOST_CHECK_EQUAL( nSol, nNode );
    BOOST_CHECK_EQUAL( nTypes, 1 );
    BOOST_CHECK_EQUAL( nReals, 3 );
    BOOST_CHECK_EQUAL( Types[0], GmfVec );

    reader.gotoKeyword( GmfSolAtVertices );

    Real tol = 1e-13;

    for (int i = 0; i < nSol; i++)
    {
      Real data[3];
      reader.getLine( GmfSolAtVertices, &data[0], &data[1], &data[2] );

      ArrayQ qTrue = q0*(i+1);

      SANS_CHECK_CLOSE( qTrue[0], data[0], tol, tol );
      SANS_CHECK_CLOSE( qTrue[1], data[1], tol, tol );
      SANS_CHECK_CLOSE( qTrue[2], data[2], tol, tol );
    }

    //Delete the written files
    std::remove(sol_filename.c_str());
  }


  //Test binary files
  std::string sol_filenameb = "IO/testWrite_libMeshb.solb";

  WriteSolution_libMeshb(qfld, sol_filenameb);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    libMeshb_Reader<PhysD3> reader(sol_filenameb);

    int nSol, nTypes, nReals, Types[2];

    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    BOOST_CHECK_EQUAL( nSol, nNode );
    BOOST_CHECK_EQUAL( nTypes, 1 );
    BOOST_CHECK_EQUAL( nReals, 3 );
    BOOST_CHECK_EQUAL( Types[0], GmfVec );

    reader.gotoKeyword( GmfSolAtVertices );

    Real tol = 1e-13;

    for (int i = 0; i < nSol; i++)
    {
      Real data[3];
      reader.getLine( GmfSolAtVertices, &data[0], &data[1], &data[2] );

      ArrayQ qTrue = q0*(i+1);

      SANS_CHECK_CLOSE( qTrue[0], data[0], tol, tol );
      SANS_CHECK_CLOSE( qTrue[1], data[1], tol, tol );
      SANS_CHECK_CLOSE( qTrue[2], data[2], tol, tol );
    }

    //Delete the written files
    std::remove(sol_filenameb.c_str());
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Write_6D_Vector_P1 )
{
  typedef DLA::VectorS<6,Real> ArrayQ;

  mpi::communicator world;

  int ii = 2, jj = 3, kk = 4;
  int nNode = (ii+1)*(jj+1)*(kk+1);
  XField3D_Box_Tet_X1 xfld(world,ii,jj,kk);

  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  // set the field based on the global native node numbering
  ArrayQ q0 = {0.2, 0.3,-0.5, 0.1, -0.02, 0.05};
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = q0*(qfld.local2nativeDOFmap(n)+1);

  //Test ASCII files
  std::string sol_filename = "IO/testWrite_libMeshb.sol";

  WriteSolution_libMeshb(qfld, sol_filename);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    libMeshb_Reader<PhysD3> reader(sol_filename);

    int nSol, nTypes, nReals, Types[2];

    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    BOOST_CHECK_EQUAL( nSol, nNode );
    BOOST_CHECK_EQUAL( nTypes, 1 );
    BOOST_CHECK_EQUAL( nReals, 6 );
    BOOST_CHECK_EQUAL( Types[0], GmfSymMat );

    reader.gotoKeyword( GmfSolAtVertices );

    Real tol = 1e-13;

    for (int i = 0; i < nSol; i++)
    {
      Real data[6];
      reader.getLine( GmfSolAtVertices, &data[0], &data[1], &data[2], &data[3], &data[4], &data[5]);
      ArrayQ qTrue = q0*(i+1);

      SANS_CHECK_CLOSE( qTrue[0], data[0], tol, tol );
      SANS_CHECK_CLOSE( qTrue[1], data[1], tol, tol );
      SANS_CHECK_CLOSE( qTrue[2], data[2], tol, tol );
      SANS_CHECK_CLOSE( qTrue[3], data[3], tol, tol );
      SANS_CHECK_CLOSE( qTrue[4], data[4], tol, tol );
      SANS_CHECK_CLOSE( qTrue[5], data[5], tol, tol );
    }

    //Delete the written files
    std::remove(sol_filename.c_str());
  }


  //Test binary files
  std::string sol_filenameb = "IO/testWrite_libMeshb.solb";

  WriteSolution_libMeshb(qfld, sol_filenameb);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    libMeshb_Reader<PhysD3> reader(sol_filenameb);

    int nSol, nTypes, nReals, Types[2];

    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    BOOST_CHECK_EQUAL( nSol, nNode );
    BOOST_CHECK_EQUAL( nTypes, 1 );
    BOOST_CHECK_EQUAL( nReals, 6 );
    BOOST_CHECK_EQUAL( Types[0], GmfSymMat );

    reader.gotoKeyword( GmfSolAtVertices );

    Real tol = 1e-13;

    for (int i = 0; i < nSol; i++)
    {
      Real data[6];
      reader.getLine( GmfSolAtVertices, &data[0], &data[1], &data[2], &data[3], &data[4], &data[5] );

      ArrayQ qTrue = q0*(i+1);

      SANS_CHECK_CLOSE( qTrue[0], data[0], tol, tol );
      SANS_CHECK_CLOSE( qTrue[1], data[1], tol, tol );
      SANS_CHECK_CLOSE( qTrue[2], data[2], tol, tol );
      SANS_CHECK_CLOSE( qTrue[3], data[3], tol, tol );
      SANS_CHECK_CLOSE( qTrue[4], data[4], tol, tol );
      SANS_CHECK_CLOSE( qTrue[5], data[5], tol, tol );
    }

    //Delete the written files
    std::remove(sol_filenameb.c_str());
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Write_3D_SymmetricMatrix_P1 )
{
  typedef DLA::MatrixSymS<3, Real> MatrixSym;

  mpi::communicator world;

  int ii = 2, jj = 3, kk = 4;
  int nNode = (ii+1)*(jj+1)*(kk+1);
  XField3D_Box_Tet_X1 xfld(world,ii,jj,kk);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> mfld(xfld, 1, BasisFunctionCategory_Hierarchical);

  // set the field based on the global native node numbering
  MatrixSym M0 = {{ 0.2},{-0.5, 0.3},{ 0.1, 0.6, 1.2}};
  for (int n = 0; n < mfld.nDOF(); n++)
    mfld.DOF(n) = M0*(mfld.local2nativeDOFmap(n)+1);

  //Test ASCII files
  std::string sol_filename = "IO/testWrite_libMeshb.sol";

  WriteSolution_libMeshb(mfld, sol_filename);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    libMeshb_Reader<PhysD3> reader(sol_filename);

    int nSol, nTypes, nReals, Types[2];

    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    BOOST_CHECK_EQUAL( nSol, nNode );
    BOOST_CHECK_EQUAL( nTypes, 1 );
    BOOST_CHECK_EQUAL( nReals, 6 );
    BOOST_CHECK_EQUAL( Types[0], GmfSymMat );

    reader.gotoKeyword( GmfSolAtVertices );

    Real tol = 1e-13;

    for (int i = 0; i < nSol; i++)
    {
      Real data[6];
      reader.getLine( GmfSolAtVertices, &data[0],
                                        &data[1], &data[2],
                                        &data[3], &data[4], &data[5] );

      MatrixSym MTrue = M0*(i+1);

      // check that the matrix was written as lower triangular
      SANS_CHECK_CLOSE( MTrue(0,0), data[0], tol, tol );
      SANS_CHECK_CLOSE( MTrue(1,0), data[1], tol, tol );
      SANS_CHECK_CLOSE( MTrue(1,1), data[2], tol, tol );
      SANS_CHECK_CLOSE( MTrue(2,0), data[3], tol, tol );
      SANS_CHECK_CLOSE( MTrue(2,1), data[4], tol, tol );
      SANS_CHECK_CLOSE( MTrue(2,2), data[5], tol, tol );
    }

    //Delete the written files
    std::remove(sol_filename.c_str());
  }


  //Test binary files
  std::string sol_filenameb = "IO/testWrite_libMeshb.solb";

  WriteSolution_libMeshb(mfld, sol_filenameb);

  // only rank 0 checks the files
  if (world.rank() == 0)
  {
    libMeshb_Reader<PhysD3> reader(sol_filenameb);

    int nSol, nTypes, nReals, Types[2];

    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    BOOST_CHECK_EQUAL( nSol, nNode );
    BOOST_CHECK_EQUAL( nTypes, 1 );
    BOOST_CHECK_EQUAL( nReals, 6 );
    BOOST_CHECK_EQUAL( Types[0], GmfSymMat );

    reader.gotoKeyword( GmfSolAtVertices );

    Real tol = 1e-13;

    for (int i = 0; i < nSol; i++)
    {
      Real data[6];
      reader.getLine( GmfSolAtVertices, &data[0],
                                        &data[1], &data[2],
                                        &data[3], &data[4], &data[5] );

      MatrixSym MTrue = M0*(i+1);

      // check that the matrix was written as lower triangular
      SANS_CHECK_CLOSE( MTrue(0,0), data[0], tol, tol );
      SANS_CHECK_CLOSE( MTrue(1,0), data[1], tol, tol );
      SANS_CHECK_CLOSE( MTrue(1,1), data[2], tol, tol );
      SANS_CHECK_CLOSE( MTrue(2,0), data[3], tol, tol );
      SANS_CHECK_CLOSE( MTrue(2,1), data[4], tol, tol );
      SANS_CHECK_CLOSE( MTrue(2,2), data[5], tol, tol );
    }

    //Delete the written files
    std::remove(sol_filenameb.c_str());
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
