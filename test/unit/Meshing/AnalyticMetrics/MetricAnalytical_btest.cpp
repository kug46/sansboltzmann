// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/AnalyticMetrics/CornerSingularity.h"
#include "Meshing/AnalyticMetrics/Euclidean.h"
#include "Meshing/AnalyticMetrics/XField.h"

#include "Meshing/EGADS/EGEdge.h"

#include <iostream>
using namespace SANS;
using namespace SANS::Metric;

namespace SANS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( MetricField )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Euclidean_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  Euclidean<Dim> M(1);

  DLA::VectorS<Dim,Real> X = {0.5, 0.45};

  //Evaluate the metric
  DLA::MatrixSymS<Dim,Real> Mx = M(X);

  BOOST_CHECK_CLOSE(1, Mx(0,0), 1e-12);
  BOOST_CHECK_SMALL(   Mx(1,0), 1e-12);
  BOOST_CHECK_CLOSE(1, Mx(1,1), 1e-12);

  DLA::VectorS<Dim,Real> dX = {0.1, 0.2};

  Real dexcat = sqrt( pow(dX[0],2) + pow(dX[1],2) );

  Real d = sqrt( Transpose(dX)*M(X)*dX );

  BOOST_CHECK_CLOSE(d, dexcat, 1e-12);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CornerSingularity_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGADS::EGContext context((EGADS::CreateContext()));

  EGADS::EGNode<Dim>::CartCoord X0({0.1,0.0}), X1({2.1,0.0});

  EGADS::EGNode<Dim> n0(context, X0);
  EGADS::EGNode<Dim> n1(context, X1);

  EGADS::EGEdge<Dim> edge(n0, n1);

  Real alpha = 2;
  int p = 2;
  int nElem = 10;
  Real k = 1 - (alpha + 1.)/(p + 2.);
  Real Int = (pow(X1[0], -k+1) - pow(X0[0], -k+1))/(-k+1);
  Real C = pow(nElem/Int, 2);

  Metric::CornerSingularity< Dim > M(alpha, p, C);

  DLA::VectorS<Dim,Real> X = {0.5, 0.45};

  Real r = sqrt(dot(X,X));
  DLA::MatrixSymS<Dim, Real> I = DLA::Identity();

  DLA::MatrixSymS<Dim,Real> exact = C*pow(r, -2*k)*I;

  //Evaluate the metric
  DLA::MatrixSymS<Dim,Real> Mx = M(X);

  BOOST_CHECK_CLOSE(exact(0,0), Mx(0,0), 1e-12);
  BOOST_CHECK_SMALL(            Mx(1,0), 1e-12);
  BOOST_CHECK_CLOSE(exact(1,1), Mx(1,1), 1e-12);

  DLA::VectorS<Dim,Real> dX = {0.1, 0.2};

  Real dexcat = sqrt( exact(0,0)*(pow(dX[0],2) + pow(dX[1],2)) );

  Real d = sqrt( Transpose(dX)*M(X)*dX );

  BOOST_CHECK_CLOSE(d, dexcat, 1e-12);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  XField<Dim> M(0.95, 1);

  DLA::VectorS<Dim,SurrealS<Dim> > X = {0.0, 0.0};

  //Evaluate the metric
  DLA::MatrixSymS<Dim,SurrealS<Dim> > Mx = M(X);

  BOOST_CHECK_CLOSE(400, Mx(0,0).value(), 1e-12);
  BOOST_CHECK_SMALL(     Mx(1,0).value(), 1e-12);
  BOOST_CHECK_CLOSE(400, Mx(1,1).value(), 1e-12);

  X = {-1, -1};
  Mx = M(X);

  BOOST_CHECK_CLOSE( 200.8454212497098, Mx(0,0).value(), 1e-12);
  BOOST_CHECK_CLOSE(-199.1545787502895, Mx(1,0).value(), 1e-12);
  BOOST_CHECK_CLOSE( 200.8454212497098, Mx(1,1).value(), 1e-12);

  X = { 1, -1};
  Mx = M(X);

  BOOST_CHECK_CLOSE( 200.8454212497098, Mx(0,0).value(), 1e-12);
  BOOST_CHECK_CLOSE( 199.1545787502895, Mx(1,0).value(), 1e-12);
  BOOST_CHECK_CLOSE( 200.8454212497098, Mx(1,1).value(), 1e-12);

  X = { 1,  1};
  Mx = M(X);

  BOOST_CHECK_CLOSE( 200.8454212497098, Mx(0,0).value(), 1e-12);
  BOOST_CHECK_CLOSE(-199.1545787502895, Mx(1,0).value(), 1e-12);
  BOOST_CHECK_CLOSE( 200.8454212497098, Mx(1,1).value(), 1e-12);

  X = {-1,  1};
  Mx = M(X);

  BOOST_CHECK_CLOSE( 200.8454212497098, Mx(0,0).value(), 1e-12);
  BOOST_CHECK_CLOSE( 199.1545787502895, Mx(1,0).value(), 1e-12);
  BOOST_CHECK_CLOSE( 200.8454212497098, Mx(1,1).value(), 1e-12);
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
