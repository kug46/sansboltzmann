// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGLoop.h"

#include <vector>
#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGLoop_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {1.0, 0.0});
  EGNode<Dim> n2(context, {1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim>::edge_vector edges = {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)};

  EGLoop<Dim> square(context, edges, CLOSED);

  EGLoop<Dim> square2(square);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
