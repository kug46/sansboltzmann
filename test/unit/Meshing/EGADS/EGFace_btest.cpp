// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGFace.h"
#include "Meshing/EGADS/EGPlane.h"
#include "Meshing/EGADS/isEquivalent.h"

#include <vector>
#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_2D_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {1.0, 0.0});
  EGNode<Dim> n2(context, {1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim>::edge_vector edges = {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)};

  EGLoop<Dim> loop(context, edges, CLOSED);
  EGFace<Dim> face(context, (loop, 1), SREVERSE);

  BOOST_CHECK_CLOSE( face.getArea(), 1., 1e-12 );
  BOOST_CHECK( face.isPlane() );

  EGNode<Dim> n4(context, {-1.0,-1.0});
  EGNode<Dim> n5(context, { 2.0,-1.0});
  EGNode<Dim> n6(context, { 2.0, 2.0});
  EGNode<Dim> n7(context, {-1.0, 2.0});

  EGEdge<Dim> edge4(n4, n5);
  EGEdge<Dim> edge5(n5, n6);
  EGEdge<Dim> edge6(n6, n7);
  EGEdge<Dim> edge7(n7, n4);

  EGLoop<Dim>::edge_vector edges2 = {(edge4, 1), (edge5, 1), (edge6, 1), (edge7, 1)};

  EGLoop<Dim> loop0(context, edges, CLOSED);
  EGLoop<Dim> loop1(context, edges2, CLOSED);

  EGFace<Dim>::loop_vector loops = {(loop0,-1), (loop1, 1)};

  EGFace<Dim> face2(context, loops);

  BOOST_CHECK_CLOSE( face2.getArea(), 9.-1., 1e-12 );
  BOOST_CHECK( face2.isPlane() );

  EGFace<Dim> face3((EGObject)face2);

  BOOST_CHECK_THROW( EGFace<Dim> face3((EGObject)n0);, SANSException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_2D_getLoops )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {1.0, 0.0});
  EGNode<Dim> n2(context, {1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGNode<Dim> n4(context, {-1.0,-1.0});
  EGNode<Dim> n5(context, { 2.0,-1.0});
  EGNode<Dim> n6(context, { 2.0, 2.0});
  EGNode<Dim> n7(context, {-1.0, 2.0});

  EGEdge<Dim> edge4(n4, n5);
  EGEdge<Dim> edge5(n5, n6);
  EGEdge<Dim> edge6(n6, n7);
  EGEdge<Dim> edge7(n7, n4);

  EGLoop<Dim> loop0(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGLoop<Dim> loop1(context, {(edge4, 1), (edge5, 1), (edge6, 1), (edge7, 1)}, CLOSED);

  EGFace<Dim> face(context, {(loop0,-1), (loop1, 1)});

  std::vector< EGLoop<Dim> > loops = face.getLoops();
  BOOST_CHECK(isEquivalent(loop0, loops[0]));
  BOOST_CHECK(isEquivalent(loop1, loops[1]));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_2D_evaluation )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGEdge<Dim>::CartCoord X;
  EGEdge<Dim>::CartCoord X0 = {0.0,0.0};
  EGEdge<Dim>::CartCoord X1 = {1.0,0.0};
  EGEdge<Dim>::CartCoord X2 = {1.0,1.0};
  EGEdge<Dim>::CartCoord X3 = {0.0,1.0};

  EGNode<Dim> n0(context, X0);
  EGNode<Dim> n1(context, X1);
  EGNode<Dim> n2(context, X2);
  EGNode<Dim> n3(context, X3);

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim> loop(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGFace<Dim> face(context, (loop,1));

  EGFace<Dim>::ParamRange u_range, v_range;
  EGFace<Dim>::ParamCoord uv;

  u_range = face.getURange();
  v_range = face.getVRange();

  //This should be X0
  uv = {u_range[0], v_range[0]};
  X = face( uv );

  BOOST_CHECK_CLOSE( X[0], X0[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X0[1], 1e-12 );

  //This should be X1
  uv = {u_range[1], v_range[0]};
  X = face( uv );

  BOOST_CHECK_CLOSE( X[0], X1[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X1[1], 1e-12 );

  //This should be X2
  uv = {u_range[1], v_range[1]};
  X = face( uv );

  BOOST_CHECK_CLOSE( X[0], X2[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X2[1], 1e-12 );

  //This should be X3
  uv = {u_range[0], v_range[1]};
  X = face( uv );

  BOOST_CHECK_CLOSE( X[0], X3[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X3[1], 1e-12 );

  //This should be the center
  uv = {(u_range[0]+u_range[1])/2, (v_range[0]+v_range[1])/2};
  X = face( uv );

  BOOST_CHECK_CLOSE( X[0], 0.5, 1e-12 );
  BOOST_CHECK_CLOSE( X[1], 0.5, 1e-12 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_2D_J )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  //Use a square face
  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {1.0, 0.0});
  EGNode<Dim> n2(context, {1.5, 2.0});
  EGNode<Dim> n3(context, {0.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim> loop(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGFace<Dim> face(context, (loop,1));

  EGFace<Dim>::ParamRange u_range = face.getURange();
  EGFace<Dim>::ParamRange v_range = face.getVRange();

  Real du = (u_range[1]+u_range[0])/2 - u_range[0];
  Real dv = (v_range[1]+v_range[0])/2 - v_range[0];

  EGFace<Dim>::ParamCoord uv0, uv1;
  EGFace<Dim>::CartCoord X0, X1, dX;
  EGFace<Dim>::JType JTrue, J;

  uv0[0] = u_range[0];
  uv0[1] = v_range[0];

  X0 = face(uv0);

  uv1 = uv0;
  uv1[0] += du;

  //Evaluate the face at the perturbed u value
  X1 = face(uv1);

  //Compute deltas
  dX = X1 - X0;

  for (int i = 0; i < Dim; i++)
    JTrue(i,0) = dX[i]/du;

  uv1 = uv0;
  uv1[1] += dv;

  //Evaluate the face at the perturbed u value
  X1 = face(uv1);

  //Compute deltas
  dX = X1 - X0;

  for (int i = 0; i < Dim; i++)
    JTrue(i,1) = dX[i]/dv;


  // The face is square and linear, so jacobian should be constant
  J = face.J(uv1);

  for (int i = 0; i < Dim; i++)
    for (int j = 0; j < 2; j++)
      SANS_CHECK_CLOSE( JTrue(i,j), J(i,j), 1e-12, 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_2D_Jinv_1 )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  //Use a square face
  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {1.0, 0.0});
  EGNode<Dim> n2(context, {1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim> loop(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGFace<Dim> face(context, (loop,1));

  EGFace<Dim>::ParamRange u_range, v_range;

  EGFace<Dim>::ParamCoord uv;
  EGFace<Dim>::CartCoord X, X2, dX;

  u_range = face.getURange();
  v_range = face.getVRange();

  uv[0] = (u_range[1]+u_range[0])/2;
  uv[1] = (v_range[1]+v_range[0])/2;

  //This increment is along the line
  dX[0] = 0.1; dX[1] = 0.1;

  //This should half way between the two nodes
  X = face(uv);

  //The dX increment is along the line, so the new parametric coordinate
  //should compute an X that is equal to X2
  X2 = X + dX;

  //Move the parametric coordinate
  uv = uv + face.Jinv(uv)*dX;

  //This should be equal to X2 now
  X = face(uv);

  BOOST_CHECK_CLOSE( X[0], X2[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X2[1], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_2D_Jinv_2 )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  //This face is not square
  EGNode<Dim> n0(context, { 0.0, 0.0});
  EGNode<Dim> n1(context, { 2.0, 0.0});
  EGNode<Dim> n2(context, { 1.5, 1.5});
  EGNode<Dim> n3(context, {-0.5, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim> loop(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGFace<Dim> face(context, (loop,1));

  EGFace<Dim>::ParamRange u_range, v_range;

  EGFace<Dim>::ParamCoord uv;
  EGFace<Dim>::CartCoord X, X2, dX;

  u_range = face.getURange();
  v_range = face.getVRange();

  uv[0] = (u_range[1]+u_range[0])/2;
  uv[1] = (v_range[1]+v_range[0])/2;

  //This increment is along the line
  dX[0] = 0.1; dX[1] = 0.3;

  //This should half way between the two nodes
  X = face(uv);

  //The dX increment is along the line, so the new parametric coordinate
  //should compute an X that is equal to X2
  X2 = X + dX;

  //Move the parametric coordinate
  uv = uv + face.Jinv(uv)*dX;

  //This should be equal to X2 now
  X = face(uv);

  BOOST_CHECK_CLOSE( X[0], X2[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X2[1], 1e-12 );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_3D_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  EGPlane<Dim> plane(context, {0,0,1.0}, {1,0,0}, {0,1,0});

  EGNode<Dim> n0(context, {0.0, 0.0, 1.0});
  EGNode<Dim> n1(context, {1.0, 0.0, 1.0});
  EGNode<Dim> n2(context, {1.0, 1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim>::edge_vector edges = {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)};

  EGLoop<Dim> loop(context, edges, CLOSED);
  EGFace<Dim> face(plane, (loop, 1), SREVERSE);

  BOOST_CHECK_CLOSE( face.getArea(), 1., 1e-12 );

  EGNode<Dim> n4(context, {-1.0,-1.0, 1.0});
  EGNode<Dim> n5(context, { 2.0,-1.0, 1.0});
  EGNode<Dim> n6(context, { 2.0, 2.0, 1.0});
  EGNode<Dim> n7(context, {-1.0, 2.0, 1.0});

  EGEdge<Dim> edge4(n4, n5);
  EGEdge<Dim> edge5(n5, n6);
  EGEdge<Dim> edge6(n6, n7);
  EGEdge<Dim> edge7(n7, n4);

  EGLoop<Dim>::edge_vector edges2 = {(edge4, 1), (edge5, 1), (edge6, 1), (edge7, 1)};

  EGLoop<Dim> loop0(context, edges, CLOSED);
  EGLoop<Dim> loop1(context, edges2, CLOSED);

  EGFace<Dim>::loop_vector loops = {(loop0,-1), (loop1, 1)};

  EGFace<Dim> face2(plane, loops);

  BOOST_CHECK_CLOSE( face2.getArea(), 9.-1., 1e-12 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_3D_evaluation )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  EGPlane<Dim> plane(context, {0,0,1.0}, {1,0,0}, {0,1,0});

  EGEdge<Dim>::CartCoord X;
  EGEdge<Dim>::CartCoord X0 = {0.0,0.0,1.0};
  EGEdge<Dim>::CartCoord X1 = {1.0,0.0,1.0};
  EGEdge<Dim>::CartCoord X2 = {1.0,1.0,1.0};
  EGEdge<Dim>::CartCoord X3 = {0.0,1.0,1.0};

  EGNode<Dim> n0(context, X0);
  EGNode<Dim> n1(context, X1);
  EGNode<Dim> n2(context, X2);
  EGNode<Dim> n3(context, X3);

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim> loop(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGFace<Dim> face(plane, (loop, 1), SREVERSE);

  EGFace<Dim>::ParamRange u_range, v_range;
  EGFace<Dim>::ParamCoord uv;

  u_range = face.getURange();
  v_range = face.getVRange();

  //This should be X0
  uv = {u_range[0], v_range[0]};
  X = face( uv );

  BOOST_CHECK_CLOSE( X[0], X0[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X0[1], 1e-12 );
  BOOST_CHECK_CLOSE( X[2], X0[2], 1e-12 );

  //This should be X1
  uv = {u_range[1], v_range[0]};
  X = face( uv );

  BOOST_CHECK_CLOSE( X[0], X1[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X1[1], 1e-12 );
  BOOST_CHECK_CLOSE( X[2], X1[2], 1e-12 );

  //This should be X2
  uv = {u_range[1], v_range[1]};
  X = face( uv );

  BOOST_CHECK_CLOSE( X[0], X2[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X2[1], 1e-12 );
  BOOST_CHECK_CLOSE( X[2], X2[2], 1e-12 );

  //This should be X3
  uv = {u_range[0], v_range[1]};
  X = face( uv );

  BOOST_CHECK_CLOSE( X[0], X3[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X3[1], 1e-12 );
  BOOST_CHECK_CLOSE( X[2], X3[2], 1e-12 );

  //This should be the center
  uv = {(u_range[0]+u_range[1])/2, (v_range[0]+v_range[1])/2};
  X = face( uv );

  BOOST_CHECK_CLOSE( X[0], 0.5, 1e-12 );
  BOOST_CHECK_CLOSE( X[1], 0.5, 1e-12 );
  BOOST_CHECK_CLOSE( X[2], 1.0, 1e-12 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_3D_getLoops )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  EGPlane<Dim> plane(context, {0,0,1.0}, {1,0,0}, {0,1,0});

  EGNode<Dim> n0(context, {0.0, 0.0, 1.0});
  EGNode<Dim> n1(context, {1.0, 0.0, 1.0});
  EGNode<Dim> n2(context, {1.0, 1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGNode<Dim> n4(context, {-1.0,-1.0, 1.0});
  EGNode<Dim> n5(context, { 2.0,-1.0, 1.0});
  EGNode<Dim> n6(context, { 2.0, 2.0, 1.0});
  EGNode<Dim> n7(context, {-1.0, 2.0, 1.0});

  EGEdge<Dim> edge4(n4, n5);
  EGEdge<Dim> edge5(n5, n6);
  EGEdge<Dim> edge6(n6, n7);
  EGEdge<Dim> edge7(n7, n4);

  EGLoop<Dim> loop0(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGLoop<Dim> loop1(context, {(edge4, 1), (edge5, 1), (edge6, 1), (edge7, 1)}, CLOSED);

  EGFace<Dim> face(plane, {(loop0,-1), (loop1, 1)});

  std::vector< EGLoop<Dim> > loops = face.getLoops();
  BOOST_CHECK(isEquivalent(loop0, loops[0]));
  BOOST_CHECK(isEquivalent(loop1, loops[1]));
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_3D_unitNormal )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  //Unit sized face
  EGNode<Dim> n0(context, { 0.0, 0.0, 0.0});
  EGNode<Dim> n1(context, { 1.0, 0.0, 0.0});
  EGNode<Dim> n2(context, { 1.0, 1.0, 0.0});
  EGNode<Dim> n3(context, { 0.0, 1.0, 0.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGPlane<Dim> plane(context, { 0.5, 0.5, 0.0 }, {1, 0, 0}, {0, 1, 0} );

  EGLoop<Dim> loop(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGFace<Dim> face(plane, (loop,1));

  EGFace<Dim>::ParamRange u_range, v_range;

  EGFace<Dim>::ParamCoord uv;

  u_range = face.getURange();
  v_range = face.getVRange();

  uv[0] = (u_range[1]+u_range[0])/2;
  uv[1] = (v_range[1]+v_range[0])/2;

  EGFace<Dim>::CartCoord N1 = face.unitNormal(uv);

  BOOST_CHECK_SMALL( N1[0],      1e-12 );
  BOOST_CHECK_SMALL( N1[1],      1e-12 );
  BOOST_CHECK_CLOSE( N1[2], 1.0, 1e-12 );

  EGFace<Dim>::CartCoord X = {0.5, 0.5, 0};

  EGFace<Dim>::CartCoord N2 = face.unitNormal(X);

  BOOST_CHECK_SMALL( N2[0],      1e-12 );
  BOOST_CHECK_SMALL( N2[1],      1e-12 );
  BOOST_CHECK_CLOSE( N2[2], 1.0, 1e-12 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_3D_J )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  EGPlane<Dim> plane(context, { 0.5, 0.5, 1.0 }, {1, 0, 0}, {0, 1, 0} );

  //Use a square face
  EGNode<Dim> n0(context, {0.0, 0.0, 1.0});
  EGNode<Dim> n1(context, {1.0, 0.0, 1.0});
  EGNode<Dim> n2(context, {1.5, 2.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim> loop(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGFace<Dim> face(plane, (loop,1));

  EGFace<Dim>::ParamRange u_range = face.getURange();
  EGFace<Dim>::ParamRange v_range = face.getVRange();

  Real du = (u_range[1]+u_range[0])/2 - u_range[0];
  Real dv = (v_range[1]+v_range[0])/2 - v_range[0];

  EGFace<Dim>::ParamCoord uv0, uv1;
  EGFace<Dim>::CartCoord X0, X1, dX;
  EGFace<Dim>::JType JTrue, J;

  uv0[0] = u_range[0];
  uv0[1] = v_range[0];

  X0 = face(uv0);

  uv1 = uv0;
  uv1[0] += du;

  //Evaluate the face at the perturbed u value
  X1 = face(uv1);

  //Compute deltas
  dX = X1 - X0;

  for (int i = 0; i < Dim; i++)
    JTrue(i,0) = dX[i]/du;

  uv1 = uv0;
  uv1[1] += dv;

  //Evaluate the face at the perturbed u value
  X1 = face(uv1);

  //Compute deltas
  dX = X1 - X0;

  for (int i = 0; i < Dim; i++)
    JTrue(i,1) = dX[i]/dv;


  // The face is square and linear, so jacobian should be constant
  J = face.J(uv1);

  for (int i = 0; i < Dim; i++)
    for (int j = 0; j < 2; j++)
      SANS_CHECK_CLOSE( JTrue(i,j), J(i,j), 1e-12, 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_3D_Jinv_1 )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  EGPlane<Dim> plane(context, { 0.5, 0.5, 1.0 }, {1, 0, 0}, {0, 1, 0} );

  //Use a square face
  EGNode<Dim> n0(context, {0.0, 0.0, 1.0});
  EGNode<Dim> n1(context, {1.0, 0.0, 1.0});
  EGNode<Dim> n2(context, {1.0, 1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim> loop(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGFace<Dim> face(plane, (loop,1));

  EGFace<Dim>::ParamRange u_range, v_range;

  EGFace<Dim>::ParamCoord uv;
  EGFace<Dim>::CartCoord X, X2, dX;

  u_range = face.getURange();
  v_range = face.getVRange();

  uv[0] = (u_range[1]+u_range[0])/2;
  uv[1] = (v_range[1]+v_range[0])/2;

  //This increment is along the line
  dX[0] = 0.1; dX[1] = 0.1; dX[2] = 0.0;

  //This should half way between the two nodes
  X = face(uv);

  //The dX increment is along the line, so the new parametric coordinate
  //should compute an X that is equal to X2
  X2 = X + dX;

  //Move the parametric coordinate
  uv = uv + face.Jinv(uv)*dX;

  //This should be equal to X2 now
  X = face(uv);

  BOOST_CHECK_CLOSE( X[0], X2[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X2[1], 1e-12 );
  BOOST_CHECK_CLOSE( X[2], X2[2], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGFace_3D_Jinv_2 )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  EGPlane<Dim> plane(context, { 0.5, 0.5, 1.0 }, {1, 1, 0}, {-1, 1, 0} );

  //This face is not square
  EGNode<Dim> n0(context, { 0.0, 0.0, 1.0});
  EGNode<Dim> n1(context, { 2.0, 0.0, 1.0});
  EGNode<Dim> n2(context, { 1.5, 1.5, 1.0});
  EGNode<Dim> n3(context, {-0.5, 1.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim> loop(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGFace<Dim> face(plane, (loop,1));

  EGFace<Dim>::ParamCoord uv;
  EGFace<Dim>::CartCoord X, X2, dX;

  EGFace<Dim>::ParamRange u_range = face.getURange();
  EGFace<Dim>::ParamRange v_range = face.getVRange();

  uv[0] = (u_range[1]+u_range[0])/2;
  uv[1] = (v_range[1]+v_range[0])/2;

  //This increment is along the line
  dX[0] = 0.1; dX[1] = 0.3; dX[2] = 0.0;

  //This should half way between the two nodes
  X = face(uv);

  //The dX increment is along the line, so the new parametric coordinate
  //should compute an X that is equal to X2
  X2 = X + dX;

  //Move the parametric coordinate
  uv = uv + face.Jinv(uv)*dX;

  //This should be equal to X2 now
  X = face(uv);

  BOOST_CHECK_CLOSE( X[0], X2[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X2[1], 1e-12 );
  BOOST_CHECK_CLOSE( X[2], X2[2], 1e-12 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
