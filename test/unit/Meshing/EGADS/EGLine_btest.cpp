// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGLine.h"

#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGLine_2D_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {0.5, 0.5});

  EGLine<Dim> line1(n0, n1);

  EGLine<Dim>::CartCoord X, dX;

  X = {0,0};
  dX = {1,1};

  EGLine<Dim> line2(context, X, dX);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGLine_3D_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0.0, 0.0, 0.0});
  EGNode<Dim> n1(context, {0.5, 0.5, 0.5});

  EGLine<Dim> line1(n0, n1);

  EGLine<Dim>::CartCoord X, dX;

  X = {0,0,0};
  dX = {1,1,1};

  EGLine<Dim> line2(context, X, dX);
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
