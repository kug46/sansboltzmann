// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGPlane.h"
#include "Meshing/EGADS/isEquivalent.h"

#include <vector>
#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGBody_2D_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {1.0, 0.0});
  EGNode<Dim> n2(context, {1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim>::edge_vector edges = {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)};

  EGLoop<Dim> loop(context, edges, CLOSED);

  EGFace<Dim> face(context, (loop, 1), SREVERSE);
  EGBody<Dim> facebody(context, (loop, 1), SREVERSE);

  BOOST_CHECK( facebody.isFace() );

  EGBody<Dim> facebody2(face);

  BOOST_CHECK( facebody2.isFace() );

  EGNode<Dim> n4(context, {-1.0,-1.0});
  EGNode<Dim> n5(context, { 2.0,-1.0});
  EGNode<Dim> n6(context, { 2.0, 2.0});
  EGNode<Dim> n7(context, {-1.0, 2.0});

  EGEdge<Dim> edge4(n4, n5);
  EGEdge<Dim> edge5(n5, n6);
  EGEdge<Dim> edge6(n6, n7);
  EGEdge<Dim> edge7(n7, n4);

  EGLoop<Dim> loop0(context, edges, CLOSED);
  EGLoop<Dim> loop1(context, {(edge4, 1), (edge5, 1), (edge6, 1), (edge7, 1)}, CLOSED);

  EGBody<Dim>::loop_vector loops = {(loop0,-1), (loop1, 1)};

  EGBody<Dim> facebody3(context, loops, SREVERSE);

  EGBody<Dim> facebody4((EGObject)facebody3);

  BOOST_CHECK_THROW( EGBody<Dim> facebody5((EGObject)n0);, SANSException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGBody_2D_getTopos )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {1.0, 0.0});
  EGNode<Dim> n2(context, {1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim>::edge_vector edges = {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)};

  EGLoop<Dim> loop(context, edges, CLOSED);

  EGBody<Dim> facebody(context, (loop, 1), SREVERSE);

  // Retrieve all the topos from the face body
  std::vector< EGFace<Dim> > facebody_faces = facebody.getFaces();
  BOOST_CHECK_EQUAL( facebody_faces.size(), facebody.nFaces() );

  for (std::size_t iface = 0; iface < facebody_faces.size(); iface++ )
    BOOST_CHECK_EQUAL( iface+1, facebody.getBodyIndex(facebody_faces[iface]) );

  std::vector< EGLoop<Dim> > facebody_loops = facebody.getLoops();
  BOOST_CHECK_EQUAL( facebody_loops.size(), facebody.nLoops() );

  for (std::size_t iloop = 0; iloop < facebody_loops.size(); iloop++ )
    BOOST_CHECK_EQUAL( iloop+1, facebody.getBodyIndex(facebody_loops[iloop]) );

  std::vector< EGEdge<Dim> > facebody_edges = facebody.getEdges();
  BOOST_CHECK_EQUAL( facebody_edges.size(), facebody.nEdges() );

  for (std::size_t iedge = 0; iedge < facebody_edges.size(); iedge++ )
    BOOST_CHECK_EQUAL( iedge+1, facebody.getBodyIndex(facebody_edges[iedge]) );

  std::vector< EGNode<Dim> > facebody_nodes = facebody.getNodes();
  BOOST_CHECK_EQUAL( facebody_nodes.size(), facebody.nNodes() );

  for (std::size_t inode = 0; inode < facebody_nodes.size(); inode++ )
    BOOST_CHECK_EQUAL( inode+1, facebody.getBodyIndex(facebody_nodes[inode]) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGBody_2D_getTopos_ref )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0.0, 0.0});
  EGNode<Dim> n1(context, {1.0, 0.0});
  EGNode<Dim> n2(context, {1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGNode<Dim> n4(context, {-1.0,-1.0});
  EGNode<Dim> n5(context, { 2.0,-1.0});
  EGNode<Dim> n6(context, { 2.0, 2.0});
  EGNode<Dim> n7(context, {-1.0, 2.0});

  EGEdge<Dim> edge4(n4, n5);
  EGEdge<Dim> edge5(n5, n6);
  EGEdge<Dim> edge6(n6, n7);
  EGEdge<Dim> edge7(n7, n4);

  EGLoop<Dim> loop0(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGLoop<Dim> loop1(context, {(edge4, 1), (edge5, 1), (edge6, 1), (edge7, 1)}, CLOSED);

  EGFace<Dim> face(context, {(loop0,-1), (loop1, 1)}, SREVERSE);

  EGBody<Dim> facebody(face);

  // Retrieve topos connected to the provided reference type
  std::vector< EGFace<Dim> > facebody_faces = facebody.getFaces(n0);
  BOOST_REQUIRE_EQUAL( 1, facebody_faces.size() );
  BOOST_CHECK(isEquivalent(facebody_faces[0], face));

  std::vector< EGLoop<Dim> > facebody_loops = facebody.getLoops(n0);
  BOOST_REQUIRE_EQUAL( 1, facebody_loops.size() );
  BOOST_CHECK(isEquivalent(facebody_loops[0], loop0));

  std::vector< EGEdge<Dim> > facebody_edges = facebody.getEdges(n0);
  BOOST_REQUIRE_EQUAL( 2, facebody_edges.size() );
  BOOST_CHECK(isEquivalent(facebody_edges[0], edge0));
  BOOST_CHECK(isEquivalent(facebody_edges[1], edge3));

  std::vector< EGNode<Dim> > facebody_nodes = facebody.getNodes(edge0);
  BOOST_REQUIRE_EQUAL( 2, facebody_nodes.size() );
  BOOST_CHECK(isEquivalent(facebody_nodes[0], n0));
  BOOST_CHECK(isEquivalent(facebody_nodes[1], n1));
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGBody_3D_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  EGPlane<Dim> plane(context, {0, 0, 1.0}, {1, 0, 0}, {0, 1, 0});

  EGNode<Dim> n0(context, {0.0, 0.0, 1.0});
  EGNode<Dim> n1(context, {1.0, 0.0, 1.0});
  EGNode<Dim> n2(context, {1.0, 1.0, 1.0});
  EGNode<Dim> n3(context, {0.0, 1.0, 1.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  EGLoop<Dim>::edge_vector edges = {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)};

  EGLoop<Dim> loop(context, edges, CLOSED);
  EGFace<Dim> face(plane, (loop, 1), SREVERSE);

  EGBody<Dim> wirebody(loop);

  BOOST_CHECK( wirebody.isWire() );

  EGBody<Dim> facebody(face);

  BOOST_CHECK( facebody.isFace() );

  EGBody<Dim> facebody2(plane, (loop, 1), SREVERSE);

  BOOST_CHECK( facebody2.isFace() );

  EGNode<Dim> n4(context, {-1.0,-1.0, 1.0});
  EGNode<Dim> n5(context, { 2.0,-1.0, 1.0});
  EGNode<Dim> n6(context, { 2.0, 2.0, 1.0});
  EGNode<Dim> n7(context, {-1.0, 2.0, 1.0});

  EGEdge<Dim> edge4(n4, n5);
  EGEdge<Dim> edge5(n5, n6);
  EGEdge<Dim> edge6(n6, n7);
  EGEdge<Dim> edge7(n7, n4);

  EGLoop<Dim> loop0(context, edges, CLOSED);
  EGLoop<Dim> loop1(context, {(edge4, 1), (edge5, 1), (edge6, 1), (edge7, 1)}, CLOSED);

  EGBody<Dim> facebody3(plane, {(loop0,-1), (loop1, 1)}, SREVERSE);

  EGBody<Dim> facebody4((EGObject)facebody3);

  BOOST_CHECK_THROW( EGBody<Dim> facebody5((EGObject)n0);, SANSException );

  //[x, y, z] [dx, dy, dz]
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  BOOST_CHECK( box.isSolid() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
