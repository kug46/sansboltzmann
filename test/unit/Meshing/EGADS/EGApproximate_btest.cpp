// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGApproximate.h"

#include <iostream>
#include <fstream>
#include <iomanip>
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

template class EGApproximate<2>;
template class EGApproximate<3>;
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGApproximate_2D_circle_file_ctor )
{
  EGContext context((CreateContext()));

  std::string filename = "IO/EGApproximate_test.txt";

  std::ofstream file(filename);
  // Set a high precision for numbers
  file << std::setprecision(16) << std::scientific;

  int nnode = 101;
  file << nnode << std::endl;

  for (int i = 0; i < nnode; i++)
  {
    Real t = i/((Real)nnode-1);
    file << cos(2*PI*t) << " " << sin(2*PI*t) << std::endl;
  }

  file.close();

  EGApproximate<2> approx_circle(context, EGApproximate<2>::Slope, 1e-6, filename);

  //Remove the test file from the file system
  std::remove(filename.c_str());

  EGApproximate<2>::CartCoord X;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-3;

  // limit the last t value because x(t=0) == x(t=1) && y(t=0) == y(t=1)
  for (int i = 0; i < nnode-1; i++)
  {
    Real t = i/((Real)nnode-1);

    X = approx_circle(t);

    SANS_CHECK_CLOSE( cos(2*PI*t), X[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( sin(2*PI*t), X[1], small_tol, close_tol );

    Real t_apprx = approx_circle({cos(2*PI*t), sin(2*PI*t)});

    SANS_CHECK_CLOSE( t, t_apprx, small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGApproximate_2D_circle_vector_ctor )
{
  EGContext context((CreateContext()));

  int nnode = 101;
  std::vector<EGApproximate<2>::CartCoord> pts(nnode);

  for (int i = 0; i < nnode; i++)
  {
    Real t = i/((Real)nnode-1);
    pts[i] = {cos(2*PI*t), sin(2*PI*t)};
  }

  EGApproximate<2> approx_circle(context, EGApproximate<2>::Slope, 1e-6, pts);

  EGApproximate<2>::CartCoord X;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-3;

  // limit the last t value because x(t=0) == x(t=1) && y(t=0) == y(t=1)
  for (int i = 0; i < nnode-1; i++)
  {
    Real t = i/((Real)nnode-1);

    X = approx_circle(t);

    SANS_CHECK_CLOSE( cos(2*PI*t), X[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( sin(2*PI*t), X[1], small_tol, close_tol );

    Real t_apprx = approx_circle({cos(2*PI*t), sin(2*PI*t)});

    SANS_CHECK_CLOSE( t, t_apprx, small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGApproximate_3D_circle_file_ctor )
{
  EGContext context((CreateContext()));

  std::string filename = "IO/EGApproximate_test.txt";

  std::ofstream file(filename);
  // Set a high precision for numbers
  file << std::setprecision(16) << std::scientific;

  Real z = 0.001;

  int nnode = 101;
  file << nnode << std::endl;

  for (int i = 0; i < nnode; i++)
  {
    Real t = i/((Real)nnode-1);
    file << cos(2*PI*t) << " " << sin(2*PI*t) << " " << z*sin(2*PI*t) << std::endl;
  }

  file.close();

  EGApproximate<3> approx_circle(context, EGApproximate<3>::Slope, 1e-6, filename);

  //Remove the test file from the file system
  std::remove(filename.c_str());

  EGApproximate<3>::CartCoord X;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-3;

  // limit the last t value because x(t=0) == x(t=1) && y(t=0) == y(t=1)
  for (int i = 0; i < nnode-1; i++)
  {
    Real t = i/((Real)nnode-1);

    X = approx_circle(t);

    SANS_CHECK_CLOSE(   cos(2*PI*t), X[0], small_tol, close_tol );
    SANS_CHECK_CLOSE(   sin(2*PI*t), X[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( z*sin(2*PI*t), X[2], small_tol, close_tol );

    Real t_apprx = approx_circle({cos(2*PI*t), sin(2*PI*t), z*sin(2*PI*t)});

    SANS_CHECK_CLOSE( t, t_apprx, small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGApproximate_3D_circle_vector_ctor )
{
  EGContext context((CreateContext()));

  int nnode = 101;
  std::vector<EGApproximate<3>::CartCoord> pts(nnode);

  Real z = 0.001;

  for (int i = 0; i < nnode; i++)
  {
    Real t = i/((Real)nnode-1);
    pts[i] = {cos(2*PI*t), sin(2*PI*t), z*sin(2*PI*t)};
  }

  EGApproximate<3> approx_circle(context, EGApproximate<3>::Slope, 1e-6, pts);

  EGApproximate<3>::CartCoord X;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-3;

  // limit the last t value because x(t=0) == x(t=1) && y(t=0) == y(t=1)
  for (int i = 0; i < nnode-1; i++)
  {
    Real t = i/((Real)nnode-1);

    X = approx_circle(t);

    SANS_CHECK_CLOSE(   cos(2*PI*t), X[0], small_tol, close_tol );
    SANS_CHECK_CLOSE(   sin(2*PI*t), X[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( z*sin(2*PI*t), X[2], small_tol, close_tol );

    Real t_apprx = approx_circle({cos(2*PI*t), sin(2*PI*t), z*sin(2*PI*t)});

    SANS_CHECK_CLOSE( t, t_apprx, small_tol, close_tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
