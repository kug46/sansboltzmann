// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGContext.h"
#include "Meshing/EGADS/EGBody.h"

#include <iostream>
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGContext_ctor )
{
  //This tests if a context is properly created and destroyed
  EGContext context((CreateContext()));
  BOOST_CHECK( (ego)context != NULL );
  BOOST_CHECK( (ego)const_cast<const EGContext&>(context) != NULL );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGContext_Equality)
{
  int outLevel = 0;
  EGContext context1((CreateContext()));
  EGContext context2(CreateContext(), outLevel);

  BOOST_CHECK_EQUAL(context1, context1);
  BOOST_CHECK( !(context1 == context2) );

  BOOST_CHECK( context1.isReference() == false );
  BOOST_CHECK( context2.isReference() == false );

  EGContext context3(context2);

  BOOST_CHECK_EQUAL(context2, context3);

  BOOST_CHECK( context3.isReference() == true );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGContext_makeSolidBody)
{
  EGContext context((CreateContext()));

  //[x, y, z] [dx, dy, dz]
  EGBody<3> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  //Center [x, y, z] [radius]
  EGBody<3> sphere = context.makeSolidBody( SPHERE, {0, 0, 0, 1} );

  //apex [x, y, z], base center [x, y, z], [radius]
  EGBody<3> cone = context.makeSolidBody( CONE, {0, 0, 1, 0, 0, 0, 1} );

  // 2 axis points [x, y, z], [radius]
  EGBody<3> cylinder = context.makeSolidBody( CYLINDER, {0, 0, 0, 1, 0, 0, 1} );

  //[x,y,z] of center, direction of rotation, then major radius and minor radius
  EGBody<3> torus = context.makeSolidBody( TORUS, {0, 0, 0, 1, 1, 1, 1, 0.5} );

  // Check that an error is thrown for an unkown type
  BOOST_CHECK_THROW( context.makeSolidBody( TORUS+1, {} );, DeveloperException );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
