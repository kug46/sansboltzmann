// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGObject.h"

#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGObject_attr )
{
  EGContext context((CreateContext()));

  double xyz[3] = { 0, 0, 0 };

  ego obj_;
  EG_STATUS( EG_makeTopology(context, NULL, NODE, 0, xyz, 0, NULL, NULL, &obj_) );

  ego obj2_;
  EG_STATUS( EG_makeTopology(context, NULL, NODE, 0, xyz, 0, NULL, NULL, &obj2_) );

  EGObject obj(obj_);
  EGObject obj2(obj2_);

  BOOST_CHECK_EQUAL( 0, obj.numAttributes() );

  obj.addAttribute("Int", int(42));
  obj.addAttribute("Real", double(43));
  obj.addAttribute("Ints", std::vector<int>{42, 43});
  obj.addAttribute("Reals", std::vector<double>{42., 43.});
  obj.addAttribute("String", "Hello EGADS!");

  BOOST_CHECK_EQUAL( 5, obj.numAttributes() );

  int Int;
  double real;
  std::vector<int> Ints;
  std::vector<double> Reals;
  std::string str;

  obj.getAttribute("Int", Int);
  obj.getAttribute("Real", real);
  obj.getAttribute("Ints", Ints);
  obj.getAttribute("Reals", Reals);
  obj.getAttribute("String", str);

  BOOST_CHECK_EQUAL( 42, Int );
  BOOST_CHECK_EQUAL( 43, real );
  BOOST_CHECK_EQUAL(  2, Ints.size() );
  BOOST_CHECK_EQUAL( 42, Ints[0] );
  BOOST_CHECK_EQUAL( 43, Ints[1] );
  BOOST_CHECK_EQUAL(  2, Reals.size() );
  BOOST_CHECK_EQUAL( 42., Reals[0] );
  BOOST_CHECK_EQUAL( 43., Reals[1] );
  BOOST_CHECK_EQUAL( "Hello EGADS!", str );

  BOOST_CHECK_EQUAL( 1           , obj.getAttributeSize("Int") );
  BOOST_CHECK_EQUAL( 1           , obj.getAttributeSize("Real") );
  BOOST_CHECK_EQUAL( Ints.size() , obj.getAttributeSize("Ints") );
  BOOST_CHECK_EQUAL( Reals.size(), obj.getAttributeSize("Reals") );
  BOOST_CHECK_EQUAL( str.size()  , obj.getAttributeSize("String") );

  BOOST_CHECK_EQUAL( 1           , obj.getAttributeSize(1) );
  BOOST_CHECK_EQUAL( 1           , obj.getAttributeSize(2) );
  BOOST_CHECK_EQUAL( Ints.size() , obj.getAttributeSize(3) );
  BOOST_CHECK_EQUAL( Reals.size(), obj.getAttributeSize(4) );
  BOOST_CHECK_EQUAL( str.size()  , obj.getAttributeSize(5) );

  Int = -1;
  real = -1;
  Ints = {-1};
  Reals = {-1};
  str = "";

  std::string name("");

  obj.getAttribute(1, name, Int);
  BOOST_CHECK_EQUAL( 42, Int );
  BOOST_CHECK_EQUAL( "Int", name );

  obj.getAttribute(2, name, real);
  BOOST_CHECK_EQUAL( 43, real );
  BOOST_CHECK_EQUAL( "Real", name );

  obj.getAttribute(3, name, Ints);
  BOOST_CHECK_EQUAL(  2, Ints.size() );
  BOOST_CHECK_EQUAL( 42, Ints[0] );
  BOOST_CHECK_EQUAL( 43, Ints[1] );
  BOOST_CHECK_EQUAL( "Ints", name );

  obj.getAttribute(4, name, Reals);
  BOOST_CHECK_EQUAL(  2, Reals.size() );
  BOOST_CHECK_EQUAL( 42., Reals[0] );
  BOOST_CHECK_EQUAL( 43., Reals[1] );
  BOOST_CHECK_EQUAL( "Reals", name );

  obj.getAttribute(5, name, str);
  BOOST_CHECK_EQUAL( "Hello EGADS!", str );
  BOOST_CHECK_EQUAL( "String", name );

  BOOST_CHECK(obj.hasAttribute("Int"));
  BOOST_CHECK(!obj.hasAttribute("Foo"));

  BOOST_CHECK_EQUAL(ATTRINT   , obj.getAttributeType("Int"));
  BOOST_CHECK_EQUAL(ATTRREAL  , obj.getAttributeType("Real"));
  BOOST_CHECK_EQUAL(ATTRINT   , obj.getAttributeType("Ints"));
  BOOST_CHECK_EQUAL(ATTRREAL  , obj.getAttributeType("Reals"));
  BOOST_CHECK_EQUAL(ATTRSTRING, obj.getAttributeType("String"));

  BOOST_CHECK_EQUAL(ATTRINT   , obj.getAttributeType(1));
  BOOST_CHECK_EQUAL(ATTRREAL  , obj.getAttributeType(2));
  BOOST_CHECK_EQUAL(ATTRINT   , obj.getAttributeType(3));
  BOOST_CHECK_EQUAL(ATTRREAL  , obj.getAttributeType(4));
  BOOST_CHECK_EQUAL(ATTRSTRING, obj.getAttributeType(5));


  Int = -1;
  real = -1;
  Ints = {-1};
  Reals = {-1};
  str = "";

  obj2.dupAttributes(obj);

  BOOST_CHECK(obj2.hasAttribute("Int"));
  BOOST_CHECK(!obj2.hasAttribute("Foo"));

  obj2.getAttribute("Int", Int);
  obj2.getAttribute("Real", real);
  obj2.getAttribute("Ints", Ints);
  obj2.getAttribute("Reals", Reals);
  obj2.getAttribute("String", str);

  BOOST_CHECK_EQUAL( 42, Int );
  BOOST_CHECK_EQUAL( 43, real );
  BOOST_CHECK_EQUAL(  2, Ints.size() );
  BOOST_CHECK_EQUAL( 42, Ints[0] );
  BOOST_CHECK_EQUAL( 43, Ints[1] );
  BOOST_CHECK_EQUAL(  2, Reals.size() );
  BOOST_CHECK_EQUAL( 42., Reals[0] );
  BOOST_CHECK_EQUAL( 43., Reals[1] );
  BOOST_CHECK_EQUAL( "Hello EGADS!", str );

  BOOST_CHECK_THROW( obj.getAttribute("Ints", Int), EGADSAttributeException );
  BOOST_CHECK_THROW( obj.getAttribute("Reals", real), EGADSAttributeException );
  BOOST_CHECK_THROW( obj.getAttribute(3, name, Int), EGADSAttributeException );
  BOOST_CHECK_THROW( obj.getAttribute(4, name, real), EGADSAttributeException );

  BOOST_CHECK_THROW( obj.getAttribute("Real", Int), EGADSAttributeException );
  BOOST_CHECK_THROW( obj.getAttribute("Int", real), EGADSAttributeException );
  BOOST_CHECK_THROW( obj.getAttribute(2, name, Int), EGADSAttributeException );
  BOOST_CHECK_THROW( obj.getAttribute(1, name, real), EGADSAttributeException );

  BOOST_CHECK_THROW( obj.getAttribute("Not found", Int), EGADSAttributeException );


  obj.delAttribute("Int");
  BOOST_CHECK_EQUAL( 4, obj.numAttributes() );


}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
