// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGNode.h"

#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGNode_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  DLA::VectorS<Dim, Real> X;
  X[0] = 0.5;
  X[1] = 0.5;

  EGNode<Dim> node0(context, {0.5, 0.5});
  EGNode<Dim> node1(context, X);

  BOOST_CHECK_CLOSE( 0.5, node0[0], 1e-13 );
  BOOST_CHECK_CLOSE( 0.5, node0[1], 1e-13 );

  BOOST_CHECK_CLOSE( 0.5, const_cast<const EGNode<Dim>&>(node0)[0], 1e-13 );
  BOOST_CHECK_CLOSE( 0.5, const_cast<const EGNode<Dim>&>(node0)[1], 1e-13 );

  DLA::VectorS<Dim, Real> X2 = node0;

  BOOST_CHECK_CLOSE( 0.5, X2[0], 1e-13 );
  BOOST_CHECK_CLOSE( 0.5, X2[1], 1e-13 );

  DLA::VectorS<Dim, Real> X3 = const_cast<const EGNode<Dim>&>(node0);

  BOOST_CHECK_CLOSE( 0.5, X3[0], 1e-13 );
  BOOST_CHECK_CLOSE( 0.5, X3[1], 1e-13 );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
