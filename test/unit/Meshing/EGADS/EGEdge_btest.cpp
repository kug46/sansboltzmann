// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGEdge.h"
#include "Meshing/EGADS/EGCircle.h"
#include "Meshing/EGADS/isEquivalent.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGEdge_2D_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0,0});
  EGNode<Dim> n1(context, {0.5,0.5});

  EGEdge<Dim> edge1(n0, n1);
  BOOST_CHECK_EQUAL( 2, edge1.nNode() );
  BOOST_CHECK( edge1.isTwoNode() );

  EGEdge<Dim> edge2(edge1);
  BOOST_CHECK_EQUAL( 2, edge2.nNode() );
  BOOST_CHECK( edge2.isTwoNode() );

  EGEdge<Dim> edge3((EGObject)edge1);
  BOOST_CHECK_EQUAL( 2, edge3.nNode() );
  BOOST_CHECK( edge3.isTwoNode() );

  EGCircle<Dim> circle(context, {0,0}, 1); //Unit circle at {0,0}

  EGEdge<Dim> edge4(circle);
  BOOST_CHECK_EQUAL( 1, edge4.nNode() );
  BOOST_CHECK( edge4.isOneNode() );

  int nnode = 101;
  std::vector<EGApproximate<2>::CartCoord> pts(nnode);

  for (int i = 0; i < nnode; i++)
  {
    Real t = i/((Real)nnode-1);
    pts[i] = {cos(2*PI*t), sin(2*PI*t)};
  }

  EGApproximate<Dim> approx_circle(context, EGApproximate<Dim>::Slope, 1e-6, pts);

  // Automatically add a node to create the edge
  EGEdge<Dim> edge5(approx_circle);
  BOOST_CHECK_EQUAL( 1, edge5.nNode() );
  BOOST_CHECK( edge5.isOneNode() );


  EGNode<Dim> n2(context, {1.0,0.0});

  // Explicitly provide the node to create the edge
  EGEdge<Dim> edge6(approx_circle, n2);
  BOOST_CHECK_EQUAL( 1, edge6.nNode() );
  BOOST_CHECK( edge6.isOneNode() );


  EGNode<Dim> n3(context, {0.0,1.0});

  // Create an edge based on two nodes
  EGEdge<Dim> edge7(approx_circle, n2, n3);
  BOOST_CHECK_EQUAL( 2, edge7.nNode() );
  BOOST_CHECK( edge7.isTwoNode() );


  // Create an edge with two nodes based on a t-range
  EGEdge<Dim> edge8(approx_circle, {0, 0.25});
  BOOST_CHECK_EQUAL( 2, edge8.nNode() );
  BOOST_CHECK( edge8.isTwoNode() );

  EGEdge<Dim>::ParamRange range;

  range = edge8.getParamRange();

  BOOST_CHECK_EQUAL(    0, range[0] );
  BOOST_CHECK_EQUAL( 0.25, range[1] );

  Real t = (range[1]+range[0])/2;

  BOOST_CHECK_EQUAL( t, edge8.midRange() );

  // Create an edge based on two nodes and a t-range (they should be consistent)
  EGEdge<Dim> edge9(approx_circle, {0, 0.25}, n2, n3);
  BOOST_CHECK_EQUAL( 2, edge9.nNode() );
  BOOST_CHECK( edge9.isTwoNode() );

  BOOST_CHECK_THROW( EGEdge<Dim> no_edge((EGObject)n0);, SANSException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGEdge_2D_getNodes )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0,0});
  EGNode<Dim> n1(context, {0.5,0.5});

  EGEdge<Dim> edge(n0, n1);

  std::vector< EGNode<Dim> > nodes = edge.getNodes();
  BOOST_CHECK(isEquivalent(n0, nodes[0]));
  BOOST_CHECK(isEquivalent(n1, nodes[1]));
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGEdge_2D_getArcLength )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0,0});
  EGNode<Dim> n1(context, {0.5,0.5});

  EGEdge<Dim> edge(n0, n1);

  BOOST_CHECK_CLOSE( sqrt(0.5*0.5*2), edge.getArcLength(), 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGEdge_ExtractCartCoord )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {1,2});

  EGEdge<Dim>::CartCoord X0 = n0;

  BOOST_CHECK_EQUAL( X0[0], 1 );
  BOOST_CHECK_EQUAL( X0[1], 2 );
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGEdge_evaluation )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGEdge<Dim>::CartCoord X;
  EGEdge<Dim>::CartCoord X0 = {0,0};
  EGEdge<Dim>::CartCoord X1 = {0.5,0.5};

  EGNode<Dim> n0(context, X0);
  EGNode<Dim> n1(context, X1);

  EGEdge<Dim> edge(n0, n1);

  EGEdge<Dim>::ParamRange range;

  range = edge.getParamRange();

  //This should be X0
  X = edge(range[0]);

  BOOST_CHECK_CLOSE( X[0], X0[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X0[1], 1e-12 );

  //This should be X1
  X = edge(range[1]);

  BOOST_CHECK_CLOSE( X[0], X1[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X1[1], 1e-12 );

  //This should half way between the two nodes
  X = edge((range[1]-range[0])/2);

  BOOST_CHECK_CLOSE( X[0], (X1[0]-X0[0])/2. + X0[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], (X1[1]-X0[1])/2. + X0[1], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGEdge_CartIncrement2Param )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGEdge<Dim>::CartCoord X, dX, X2;
  EGEdge<Dim>::CartCoord X0 = {0,0};
  EGEdge<Dim>::CartCoord X1 = {0.5,0.5};

  EGNode<Dim> n0(context, X0);
  EGNode<Dim> n1(context, X1);

  EGEdge<Dim> edge(n0, n1);

  EGEdge<Dim>::ParamRange range;

  range = edge.getParamRange();

  Real t = (range[1]+range[0])/2;

  //This increment is along the line
  dX[0] = 0.1; dX[1] = 0.1;

  //This should half way between the two nodes
  X = edge(t);

  //The dX increment is along the line, so the new parametric coordinate
  //should compute an X that is equal to X2
  X2 = X + dX;

  //Move the parametric coordinate
  t = edge.CartIncrement2Param(t, dX);

  //This should be equal to X2 now
  X = edge(t);

  BOOST_CHECK_CLOSE( X[0], X2[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X2[1], 1e-12 );

  //------------//

  t = (range[1]+range[0])/2;

  //This increment does not conform to the line
  dX[0] = 0.2; dX[1] = 0.1;

  //This should half way between the two nodes
  X = edge(t);

  //Define the normalized tangent vector to the line
  EGEdge<Dim>::CartCoord Tangent = X1/sqrt(dot(X1,X1));

  //Increment with dX along the tangent line. The new t should
  //should compute an X that is equal to X2
  X2 = X + Tangent*dot(dX,Tangent);

  //Move the parametric coordinate
  t = edge.CartIncrement2Param(t, dX);

  //This should be equal to X2 now
  X = edge(t);

  BOOST_CHECK_CLOSE( X[0], X2[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X2[1], 1e-12 );


  //------------//

  t = (range[1]+range[0])/2;

  //This increment moves beyond the line
  dX = X1;

  //Move the parametric coordinate
  t = edge.CartIncrement2Param(t, dX);

  //This should be equal to X1 now
  X = edge(t);

  BOOST_CHECK_CLOSE( X[0], X1[0], 1e-12 );
  BOOST_CHECK_CLOSE( X[1], X1[1], 1e-12 );

  //------------//

  t = (range[1]+range[0])/2;

  //This increment moves beyond the line
  dX = -X1;

  //Move the parametric coordinate
  t = edge.CartIncrement2Param(t, dX);

  //This should be equal to X0 now
  X = edge(t);

  BOOST_CHECK_SMALL( X[0], 1e-12 );
  BOOST_CHECK_SMALL( X[1], 1e-12 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGEdge_2D_J )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 2;

  EGContext context((CreateContext()));

  EGEdge<Dim>::CartCoord X, dX, X2;
  EGEdge<Dim>::CartCoord X0 = {0,0};
  EGEdge<Dim>::CartCoord X1 = {0.5,0.5};

  EGNode<Dim> n0(context, X0);
  EGNode<Dim> n1(context, X1);

  EGEdge<Dim> edge(n0, n1);

  EGEdge<Dim>::ParamRange range = edge.getParamRange();

  Real t = (range[1]+range[0])/2;

  Real dt = range[1] - range[0];
  dX = X1 - X0;

  EGEdge<Dim>::JType dXdt = edge.J(t);
  EGEdge<Dim>::JType dXdtTrue = dX/dt;

  BOOST_CHECK_CLOSE( dXdtTrue(0,0), dXdt(0,0), 1e-12 );
  BOOST_CHECK_CLOSE( dXdtTrue(1,0), dXdt(1,0), 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGEdge_3D_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  EGNode<Dim> n0(context, {0,0,0});
  EGNode<Dim> n1(context, {0.5,0.5,0.5});

  EGEdge<Dim> edge(n0, n1);
  BOOST_CHECK_EQUAL( 2, edge.nNode() );

  EGEdge<Dim> edge2(edge);
  BOOST_CHECK_EQUAL( 2, edge2.nNode() );

  EGCircle<Dim> circle(context, {0, 0, 0}, {1, 0, 0}, {0, 1, 0}, 1); //Unit circle at {0,0,0}

  EGEdge<Dim> edge3(circle);
  BOOST_CHECK_EQUAL( 1, edge3.nNode() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
