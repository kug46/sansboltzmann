INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

IF ( USE_TETGEN )

  RUN_WITH_MPIEXEC( 1 )

  GenerateUnitTests( TetGenLib 
                     EGTessLib
                     EGADSLib 
                     FieldLib 
                     BasisFunctionLib 
                     QuadratureLib 
                     DenseLinAlgLib 
                     PythonLib
                     TopologyLib 
                     toolsLib )
ENDIF()
