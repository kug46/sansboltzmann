// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGNode.h"
#include "Meshing/EGADS/EGPlane.h"
#include "Meshing/EGADS/EGIntersection.h"
#include "Meshing/EGADS/EGLoop.h"
#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGTess/EGTessModel.h"
#include "Meshing/EGADS/rotate.h"
#include "Meshing/EGADS/extrude.h"
#include "Meshing/EGADS/solidBoolean.h"
#include "Meshing/EGADS/isSame.h"
#include "Meshing/TetGen/EGTetGen.h"

#include "Field/output_Tecplot.h"

#include <vector>
#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTetGen_Box_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );
  EGModel<Dim> model(box);

  //model.save( "box.egads");

  EGTessModel tessModel(model, {0.5, 0.001, 15.});

  EGTetGen xfld(tessModel);

  //output_Tecplot(xfld, "tmp/box.dat");
}
#endif
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTetGen_Wedge_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});

  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);

  EGLoop<Dim> loop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (loop, 1) );

  //Create the wing
  EGBody<Dim> wing = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  //Increase the tess resolution on the wing
  std::vector< EGFace<Dim> > wingfaces = wing.getFaces();
  for ( auto face = wingfaces.begin(); face != wingfaces.end(); face++ )
    face->addAttribute(".tParams", {0.05,0.001,15.});

  EGModel<Dim> wingboxModel = subtract(box, wing);

  //model.save( "Wedge.egads");
  //output_Tecplot(xfld, "tmp/wing.dat");

  EGTessModel tessModel(wingboxModel, {0.5, 0.001, 15.});

  EGTetGen xfld(tessModel);

  //output_Tecplot(xfld, "tmp/wedge.dat");
}
#endif
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTetGen_WedgeWake_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context(CreateContext(), 0);

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});


  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);

  EGLoop<Dim> wingloop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (wingloop, 1) );

  //Create the wing
  EGBody<Dim> wing = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  //Increase the tess resolution on the wing
  std::vector< EGFace<Dim> > wingfaces = wing.getFaces();
  for ( auto face = wingfaces.begin(); face != wingfaces.end(); face++ )
    face->addAttribute(".tParams", {0.15,0.001,15.});

  //Nodes outside the box
  EGNode<Dim> TEnodeL(context, {0.50, 0.5, -2.});
  EGNode<Dim> TEnodeR(context, {0.50, 0.5,  2.});

  //Edges that extend from the wing tips outside the box
  EGEdge<Dim> edgeTipL (TEnode1, TEnodeL);
  EGEdge<Dim> edgeTipR (TEnodeR, TEnode0);

  EGLoop<Dim>::edge_vector wakeEdges;

  if ( LEnode0[2] < 0 && LEnode1[2] > 1 )
    wakeEdges = {(edgeTE,1)};
  else if ( LEnode0[2] > 0 && LEnode1[2] > 1 )
    wakeEdges = {(edgeTipL,1),(edgeTE,1)};
  else if ( LEnode0[2] < 0 && LEnode1[2] < 1 )
    wakeEdges = {(edgeTE,1),(edgeTipR,1)};
  else if ( LEnode0[2] > 0 && LEnode1[2] < 1 )
    wakeEdges = {(edgeTipL,1),(edgeTE,1),(edgeTipR,1)};

  Real len = 5;
  DLA::VectorS<3,Real> dir = {1, 0.05, 0};

  //Extrude the wake including artificial faces
  EGBody<Dim> wakecutter = extrude( EGLoop<Dim>(context, wakeEdges, OPEN), len, dir );

  //Extrude just the wake it self without extra cutting surfaces
  EGBody<Dim> wakeextrude = extrude( EGLoop<Dim>((edgeTE,1), OPEN), len, dir );

  //EGModel<Dim> wingboxModel = subtract(box, wing);
  //wingboxModel.save( "tmp/wingwake.egads");

  EGBody<Dim> wingbox = subtract(box, wing).getBodies()[0].clone();

  EGBody<Dim> wingboxscribed(EGIntersection<Dim>(wingbox, wakecutter));

  //This needs the most recent version of ESP
  EGBody<Dim> wake = intersect( EGModel<Dim>(wakeextrude), wingbox ).getBodies()[0].clone();

  EGModel<Dim> model({wake, wingboxscribed});

  wake.getFaces()[0].addAttribute("BCName", "Wing_Wake");
  wake.getFaces()[0].addAttribute(".tParams", {0.15,0.001,15.});

  std::vector< EGEdge<3> > WakeEdges = wake.getEdges();
  std::vector< EGEdge<3> > wingboxEdges = wingboxscribed.getEdges();
  for ( auto edge = WakeEdges.begin(); edge != WakeEdges.end(); edge++ )
  {
    std::vector<EGNode<3>> nodes = edge->getNodes();

    typedef EGNode<3>::CartCoord CartCoord;

    if (nodes.size() == 1 ) continue;

    if ( (((CartCoord)nodes[0])[0] > 0.4 && ((CartCoord)nodes[0])[0] < 0.6) &&
         (((CartCoord)nodes[1])[0] > 0.4 && ((CartCoord)nodes[1])[0] < 0.6) )
    {
      for ( auto sedge = wingboxEdges.begin(); sedge != wingboxEdges.end(); sedge++ )
      {
        if (isSame(*edge,*sedge))
        {
          sedge->addAttribute(EGTetGen::WAKESHEET, "Wing_Wake"); // Mark the edge that is used for Kutta condition integrals
          break;
        }
      }
    }
    else if ( fabs( ((CartCoord)nodes[0])[0] - ((CartCoord)nodes[1])[0] ) < 0.1 )
    {
      for ( auto sedge = wingboxEdges.begin(); sedge != wingboxEdges.end(); sedge++ )
      {
        if (isSame(*edge,*sedge))
        {
          sedge->addAttribute(EGTetGen::TREFFTZ, "Wing_Wake"); // Mark the edge that is used for Trefftz plane integrals
          break;
        }
      }
    }
  }

  //model.save( "tmp/wingwake.egads");

  EGTessModel tessModel( model, {0.5, 0.001, 15.});

  EGTetGen xfld(tessModel);

  //output_Tecplot(xfld, "tmp/wing.dat");
}
#endif
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGTetGen_Wedge2Wake_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context(CreateContext(), 0);

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});


  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);

  EGLoop<Dim> wingloop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (wingloop, 1) );

  //Create the wing
  EGBody<Dim> wing1 = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  //Create the 2nd wing
  EGBody<Dim> wing2 = wing1.clone({-0.1, 0.25, 0});

  //Increase the tess resolution on wing1
  std::vector< EGFace<Dim> > wing1faces = wing1.getFaces();
  for ( auto face = wing1faces.begin(); face != wing1faces.end(); face++ )
    face->addAttribute(".tParams", {0.15,0.001,15.});

  //Set coarse tessellation on wing2 to test a triangle that includes two EGADS nodes
  std::vector< EGFace<Dim> > wing2faces = wing2.getFaces();
  for ( auto face = wing2faces.begin(); face != wing2faces.end(); face++ )
    face->addAttribute(".tParams", {0.5,0.001,15.});


  typedef EGNode<Dim>::CartCoord CartCoord;
  std::vector< EGEdge<Dim> > wing2edges = wing2.getEdges();
  EGEdge<Dim> edgeTE2 = wing2edges[0];
  for ( auto edge2 = wing2edges.begin(); edge2 != wing2edges.end(); edge2++ )
  {
    std::vector<EGNode<Dim>> nodes = edge2->getNodes();

    if ( ((CartCoord)nodes[0])[0] > 0.39 && ((CartCoord)nodes[1])[0] > 0.39 )
    {
      edgeTE2 = *edge2;
      break;
    }
  }

  //Nodes outside the box
  EGNode<Dim> TEnodeL(context, {0.50, 0.5, -2.});
  EGNode<Dim> TEnodeR(context, {0.50, 0.5,  2.});

  //Edges that extend from the wing tips outside the box
  EGEdge<Dim> edgeTipL (TEnode1, TEnodeL);
  EGEdge<Dim> edgeTipR (TEnodeR, TEnode0);

  EGLoop<Dim>::edge_vector wakeEdges1;

  if ( LEnode0[2] < 0 && LEnode1[2] > 1 )
    wakeEdges1 = {(edgeTE,1)};
  else if ( LEnode0[2] > 0 && LEnode1[2] > 1 )
    wakeEdges1 = {(edgeTipL,1),(edgeTE,1)};
  else if ( LEnode0[2] < 0 && LEnode1[2] < 1 )
    wakeEdges1 = {(edgeTE,1),(edgeTipR,1)};
  else if ( LEnode0[2] > 0 && LEnode1[2] < 1 )
    wakeEdges1 = {(edgeTipL,1),(edgeTE,1),(edgeTipR,1)};

  Real len = 5;
  DLA::VectorS<3,Real> dir = {1, 0.05, 0};

  //Extrude the wake including artificial faces
  EGBody<Dim> wakecutter1 = extrude( EGLoop<Dim>(context, wakeEdges1, OPEN), len, dir );


  //Nodes outside the box
  EGNode<Dim> TEnodeL2(context, {0.40, 0.75, -2.});
  EGNode<Dim> TEnodeR2(context, {0.40, 0.75,  2.});

  std::vector<EGNode<Dim>> TE2nodes = edgeTE2.getNodes();
  EGNode<Dim> TEnode02 = TE2nodes[0];
  EGNode<Dim> TEnode12 = TE2nodes[1];


  if ( ((CartCoord)TEnode02)[2] < ((CartCoord)TEnode12)[2] )
  {
    TEnode12 = TE2nodes[0];
    TEnode02 = TE2nodes[1];
  }

  //Edges that extend from the wing tips outside the box
  EGEdge<Dim> edgeTipL2 (TEnode12, TEnodeL2);
  EGEdge<Dim> edgeTipR2 (TEnodeR2, TEnode02);

  EGLoop<Dim>::edge_vector wakeEdges2;

  if ( TEnode02[2] < 0 && TEnode12[2] > 1 )
    wakeEdges2 = {(edgeTE2,1)};
  else if ( TEnode02[2] > 0 && TEnode12[2] > 1 )
    wakeEdges2 = {(edgeTipL2,1),(edgeTE2,1)};
  else if ( TEnode02[2] < 0 && TEnode12[2] < 1 )
    wakeEdges2 = {(edgeTE2,1),(edgeTipR2,1)};
  else if ( TEnode02[2] > 0 && TEnode12[2] < 1 )
    wakeEdges2 = {(edgeTipL2,1),(edgeTE2,1),(edgeTipR2,1)};

  //Extrude the wake including artificial faces
  EGBody<Dim> wakecutter2 = extrude( EGLoop<Dim>(context, wakeEdges2, OPEN), len, dir );



  //Extrude just the wake it self without extra cutting surfaces
  EGBody<Dim> wakeextrude1 = extrude( EGLoop<Dim>((edgeTE,1), OPEN), len, dir );
  EGBody<Dim> wakeextrude2 = extrude( EGLoop<Dim>((edgeTE2,1), OPEN), len, dir );

  //EGModel<Dim> wingboxModel = subtract(box, wing);
  //wingboxModel.save( "tmp/wingwake.egads");


  EGBody<Dim> boxscribed1( EGIntersection<Dim>(box, wakecutter1) );
  EGBody<Dim> boxscribed2( EGIntersection<Dim>(boxscribed1, wakecutter2) );

  EGBody<Dim> wingbox1 = subtract(boxscribed2, wing1).getBodies()[0].clone();
  EGBody<Dim> wingbox2 = subtract(wingbox1, wing2).getBodies()[0].clone();

  //This needes the most recent version of ESP
  EGBody<Dim> wake1 = intersect( EGModel<Dim>(wakeextrude1), wingbox2 ).getBodies()[0].clone();
  EGBody<Dim> wake2 = intersect( EGModel<Dim>(wakeextrude2), wingbox2 ).getBodies()[0].clone();

  //EGModel<Dim> model({wake, wingbox2});
  EGModel<Dim> model({wingbox2, wake1, wake2});

#if 1
  wake1.getFaces()[0].addAttribute("BCName", "Wing_Wake");
  wake1.getFaces()[0].addAttribute(".tParams", {0.15,0.001,15.});

  wake2.getFaces()[0].addAttribute("BCName", "Wing_Wake");
  wake2.getFaces()[0].addAttribute(".tParams", {0.15,0.001,15.});


  std::vector< EGEdge<3> > WakeEdges1 = wake1.getEdges();
  std::vector< EGEdge<3> > WakeEdges2 = wake2.getEdges();
  std::vector< EGEdge<3> > wingboxEdges = wingbox2.getEdges();

  for ( auto edge = WakeEdges1.begin(); edge != WakeEdges1.end(); edge++ )
  {
    std::vector<EGNode<3>> nodes = edge->getNodes();

    typedef EGNode<3>::CartCoord CartCoord;

    if (nodes.size() == 1 ) continue;

    if ( (((CartCoord)nodes[0])[0] > 0.4 && ((CartCoord)nodes[0])[0] < 0.6) &&
         (((CartCoord)nodes[1])[0] > 0.4 && ((CartCoord)nodes[1])[0] < 0.6) )
    {
      for ( auto sedge = wingboxEdges.begin(); sedge != wingboxEdges.end(); sedge++ )
      {
        if (isSame(*edge,*sedge))
        {
          sedge->addAttribute(EGTetGen::WAKESHEET, "Wing_Wake"); // Mark the edge that is used for Kutta condition integrals
          break;
        }
      }
    }
    else if ( fabs( ((CartCoord)nodes[0])[0] - ((CartCoord)nodes[1])[0] ) < 0.1 )
    {
      for ( auto sedge = wingboxEdges.begin(); sedge != wingboxEdges.end(); sedge++ )
      {
        if (isSame(*edge,*sedge))
        {
          sedge->addAttribute(EGTetGen::TREFFTZ, "Wing_Wake"); // Mark the edge that is used for Trefftz plane integrals
          break;
        }
      }
    }
  }

  for ( auto edge = WakeEdges2.begin(); edge != WakeEdges2.end(); edge++ )
  {
    std::vector<EGNode<3>> nodes = edge->getNodes();

    typedef EGNode<3>::CartCoord CartCoord;

    if (nodes.size() == 1 ) continue;

    if ( (((CartCoord)nodes[0])[0] > 0.3 && ((CartCoord)nodes[0])[0] < 0.5) &&
         (((CartCoord)nodes[1])[0] > 0.3 && ((CartCoord)nodes[1])[0] < 0.5) )
    {
      for ( auto sedge = wingboxEdges.begin(); sedge != wingboxEdges.end(); sedge++ )
      {
        if (isSame(*edge,*sedge))
        {
          sedge->addAttribute(EGTetGen::WAKESHEET, "Wing_Wake"); // Mark the edge that is used for Kutta condition integrals
          break;
        }
      }
    }
    else if ( fabs( ((CartCoord)nodes[0])[0] - ((CartCoord)nodes[1])[0] ) < 0.1 )
    {
      for ( auto sedge = wingboxEdges.begin(); sedge != wingboxEdges.end(); sedge++ )
      {
        if (isSame(*edge,*sedge))
        {
          sedge->addAttribute(EGTetGen::TREFFTZ, "Wing_Wake"); // Mark the edge that is used for Trefftz plane integrals
          break;
        }
      }
    }
  }
#endif
  //model.save( "tmp/tmp.egads");

  // The coarse
  EGTessModel tessModel( model, {0.5, 0.001, 15.});

  EGTetGen xfld(tessModel);

  //output_Tecplot(xfld, "tmp/wing2.dat");

}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
