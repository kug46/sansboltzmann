// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGPlane.h"
#include "Meshing/EGADS/EGApproximate.h"
#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGTess/EGTessModel.h"
#include "Meshing/EGTess/makeWakedAirfoil.h"
#include "Meshing/EGTess/EGTriangle.h"
#include "Meshing/TetGen/EGTetGen.h"
#include "Meshing/EGADS/extrude.h"
#include "Meshing/EGADS/solidBoolean.h"

#include "Meshing/EGADS/Airfoils/NACA4.h"

#include "Field/output_Tecplot.h"

#include <vector>
#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( makeWakedAirfoil2D_test )
{
  EGContext context((CreateContext()));

  EGApproximate<2> NACA_spline = NACA4<2>(context);

  int nHalfAirfoil = 31;
  int nWake = 11;
  int nFarField = 11;

  EGBody<2> AirfoilBody = makeWakedAirfoil(NACA_spline, nHalfAirfoil, nWake, nFarField, 3.);

  EGModel<2> model(AirfoilBody);

  EGTriangle xfld(AirfoilBody);

  //model.save("tmp/nacawake.egads");

  //output_Tecplot(xfld, "tmp/naca.dat");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( makeWakedAirfoil3D_test )
{
  EGContext context((CreateContext()));

  EGApproximate<3> NACA_spline = NACA4<3>(context, 0.24, 0.4, 0);

  // Mark the wake or not as a wake
  bool withWake = true;
  //bool withWake = false;
  const int nChord = 10;
  const int nSpan = 10;
  const int nWake = 10;
  const Real span = 1.;

  std::vector<double> EGADSparams = {2.0, 0.001, 15.0};

  std::vector<int> TrefftzFrames;

  EGModel<3> model = makeWakedAirfoil(NACA_spline, TrefftzFrames, span, {-2,-2,-4, 4,4,8}, nChord, nSpan, nWake, EGADSparams, withWake);
  //model.save( "tmp/NACAWake3D.egads");

  EGTessModel tessModel( model, EGADSparams);

  EGTetGen xfld(tessModel);

  //output_Tecplot(xfld, "tmp/NACAWake3D.dat");
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
