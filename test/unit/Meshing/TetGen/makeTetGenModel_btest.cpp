// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGNode.h"
#include "Meshing/EGADS/EGPlane.h"
#include "Meshing/EGADS/EGLoop.h"
#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGADS/rotate.h"
#include "Meshing/EGADS/solidBoolean.h"
#include "Meshing/TetGen/makeTetGenModel.h"

#include <vector>
#include <iostream>
using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( makeTetGenModel_old_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});

  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);

  EGLoop<Dim> loop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (loop, 1) );

  //Create the wing
  EGBody<Dim> wing = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  EGModel<Dim> wingboxModel = subtract(box, wing);

  std::vector<int> modelBodyIndex[2];
  std::map<int, std::set<int> > duplicatePoints;

  tetgenio grid;
  makeTetGenModel(wingboxModel, modelBodyIndex, duplicatePoints, grid);

  //std::cout << grid.numberoftrifaces << std::endl;

  //model.save( "square.egads");

}
#endif
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( makeTetGenModel_test )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  //Create the farfield box
  EGBody<Dim> box = context.makeSolidBody( BOX, {0, 0, 0, 1, 1, 1} );

  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  EGNode<Dim> LEnode0(context, {0.25, 0.5, 0.25});
  EGNode<Dim> LEnode1(context, {0.25, 0.5, 0.75});
  EGNode<Dim> TEnode0(context, {0.50, 0.5, 0.75});
  EGNode<Dim> TEnode1(context, {0.50, 0.5, 0.25});

  EGEdge<Dim> edgeLE(LEnode0, LEnode1);
  EGEdge<Dim> edgeR (LEnode1, TEnode0);
  EGEdge<Dim> edgeTE(TEnode0, TEnode1);
  EGEdge<Dim> edgeL (TEnode1, LEnode0);

  EGLoop<Dim> loop(context, {(edgeLE,1),(edgeR,1),(edgeTE,1),(edgeL,1)}, CLOSED);

  EGPlane<Dim> wingplane(context, { 0.5, 0.5, 0.5 }, {1, 0, 0}, {0, 0, 1} );

  EGFace<Dim> wingFace(wingplane, (loop, 1) );

  //Create the wing
  EGBody<Dim> wing = rotate(wingFace, 5, {0.5, 0.5, 0.25, 0, 0, 1} );

  EGModel<Dim> wingboxModel = subtract(box, wing);

  std::set<int> duplicatePoints;
  std::vector<bool> modelFaceKutta;

  EGTessModel tessModel(wingboxModel, {0.5, 0.001, 15.});

  tetgenio grid;
  makeTetGenModel(tessModel, duplicatePoints, modelFaceKutta, grid);

  //std::cout << grid.numberoftrifaces << std::endl;

  //model.save( "square.egads");
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()

#if 0
int main(int argc, char *argv[])
{
  int        i, j, status, oclass, mtype, nbody, nvert, ntriang, nface, *triang;
  char       filename[20];
  const char *OCCrev;
  float      arg;
  double     size, *verts;
  FILE       *fp;
  ego        context, model, geom, tess, *faces;
  double dum[3];


  //Build up a wedge (i.e. a wing with really really poor aerodynamic properties...)

  ego TELine; //Trailing line
  double TELineData[] = {0.5, 0.5, 0.25, 0, 0, 1};
  status = EG_makeGeometry(context, CURVE, LINE, NULL, NULL, TELineData, &TELine);


  ego LELine; //Leading line
  double LELineData[] = {0.25, 0.5, 0.25, 0, 0, 1};
  status = EG_makeGeometry(context, CURVE, LINE, NULL, NULL, LELineData, &LELine);


  ego TipLine; //Tip line
  double TipLineData[] = {0.25, 0.5, 0.25, 1, 0, 0};
  status = EG_makeGeometry(context, CURVE, LINE, NULL, NULL, TipLineData, &TipLine);


  ego RootLine; //Tip line
  double RootLineData[] = {0.25, 0.5, 0.75, 1, 0, 0};
  status = EG_makeGeometry(context, CURVE, LINE, NULL, NULL, RootLineData, &RootLine);


  double LExyz0[3] = {0.25, 0.5, 0.25};
  double LExyz1[3] = {0.25, 0.5, 0.75};
  double TExyz0[3] = {0.50, 0.5, 0.25};
  double TExyz1[3] = {0.50, 0.5, 0.75};

  ego LEnode0;
  status = EG_makeTopology(context, NULL, NODE, 0, LExyz0, 0, NULL, NULL, &LEnode0);

  ego LEnode1;
  status = EG_makeTopology(context, NULL, NODE, 0, LExyz1, 0, NULL, NULL, &LEnode1);

  ego TEnode0;
  status = EG_makeTopology(context, NULL, NODE, 0, TExyz0, 0, NULL, NULL, &TEnode0);

  ego TEnode1;
  status = EG_makeTopology(context, NULL, NODE, 0, TExyz1, 0, NULL, NULL, &TEnode1);


  double TErange[2];

  //Get the parametric range_ of the line
  status = EG_invEvaluate(TELine, TExyz0, &TErange[0], dum);
  status = EG_invEvaluate(TELine, TExyz1, &TErange[1], dum);

  ego TEnodes[2] = {TEnode0, TEnode1};

  ego TEedge;
  status = EG_makeTopology(context, TELine, EDGE, TWONODE, TErange, 2, TEnodes, NULL, &TEedge);



  double LErange[2];

  //Get the parametric range_ of the line
  status = EG_invEvaluate(LELine, LExyz0, &LErange[0], dum);
  status = EG_invEvaluate(LELine, LExyz1, &LErange[1], dum);

  ego LEnodes[2] = {LEnode0, LEnode1};

  ego LEedge;
  status = EG_makeTopology(context, LELine, EDGE, TWONODE, LErange, 2, LEnodes, NULL, &LEedge);


  double Tiprange[2];

  //Get the parametric range_ of the line
  status = EG_invEvaluate(TipLine, LExyz0, &Tiprange[0], dum);
  status = EG_invEvaluate(TipLine, TExyz0, &Tiprange[1], dum);

  ego Tipnodes[2] = {LEnode0, TEnode0};

  ego Tipedge;
  status = EG_makeTopology(context, TipLine, EDGE, TWONODE, Tiprange, 2, Tipnodes, NULL, &Tipedge);


  double Rootrange[2];

  //Get the parametric range_ of the line
  status = EG_invEvaluate(RootLine, LExyz1, &Rootrange[0], dum);
  status = EG_invEvaluate(RootLine, TExyz1, &Rootrange[1], dum);

  ego Rootnodes[2] = {LEnode1, TEnode1};

  ego Rootedge;
  status = EG_makeTopology(context, RootLine, EDGE, TWONODE, Rootrange, 2, Rootnodes, NULL, &Rootedge);


  ego wingedges[] = {LEedge, Tipedge, TEedge, Rootedge};
  int senses[] = {SREVERSE, SFORWARD, SFORWARD, SREVERSE};

  ego wingloop;
  status = EG_makeTopology(context, NULL, LOOP, CLOSED, NULL, 4, wingedges, senses, &wingloop);



  double wingPlaneData[9] = { 0.5, 0.5, 0.5,
                                1,   0,   0,
                                0,   0,   1 };

  ego wingplane;
  status = EG_makeGeometry(context, SURFACE, PLANE, NULL, NULL, wingPlaneData, &wingplane);


  ego wingLoops[] = {wingloop};
  int wingLoopsenses[] = {SFORWARD};

  //Create the wing face
  ego wingFace;
  status = EG_makeTopology(context, wingplane, FACE, SFORWARD, NULL, 1, wingLoops, wingLoopsenses, &wingFace);

  //Create the wing
  ego wing;
  status = EG_rotate(wingFace, 5, TELineData, &wing);


  ego wakeLE;
  status = EG_makeTopology(context, NULL, LOOP, OPEN, NULL, 1, &TEedge, wingLoopsenses, &wakeLE);

  double wakedir[] = {1.0, 0.0, 0.0};
  ego sheetwake;
  status = EG_extrude(wakeLE, 1.0, wakedir, &sheetwake);


  ego wingboxModel;
  EG_solidBoolean(box, wing, SUBTRACTION, &wingboxModel);

  ego *wingboxs;
  status = EG_getTopology(wingboxModel, &geom, &oclass, &mtype, NULL, &nbody, &wingboxs, &triang);
  ego wingbox = *wingboxs;

/*
  double xyPlaneData[9] = { 0.0, 0.0, 0.0,
                              1,   0,   0,
                              0,   1,   0 };

  ego xyPlane;
  EG_STATUS( EG_makeGeometry(context, SURFACE, PLANE, NULL, NULL, xyPlaneData, &xyPlane) );

  double face1Data[4] = { 0.0, 1.0,
                          0.0, 1.0};

  //EG_getRange( xyPlane, face1Data, &status );

  ego face1;
  EG_STATUS( EG_makeFace(xyPlane, SFORWARD, face1Data, &face1) );

  double face2Data[4] = { 1.0, 2.0,
                          0.0, 1.0};

  ego face2;
  EG_STATUS( EG_makeFace(xyPlane, SFORWARD, face2Data, &face2) );

  ego faces2[] = {face1, face2};
  ego facesModel;
  EG_STATUS( EG_sewFaces(2, faces2, 0.0, 1, &facesModel) );

  ego *facesbodies;
  status = EG_getTopology(facesModel, &geom, &oclass, &mtype, NULL, &nbody, &facesbodies, &triang);
  ego facesbody = *facesbodies;
*/

  double params[] = {sqrt(2*0.5*0.5), 0.9, 0};
  //double params[] = {0.15, 0.9, 0};

  EG_STATUS( EG_makeTessBody(facesbody, params, &tess) );
}

#endif
