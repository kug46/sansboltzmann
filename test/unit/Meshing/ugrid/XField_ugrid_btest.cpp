// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include "tools/linspace.h"

#include "Field/XFieldVolume.h"
#include "Meshing/ugrid/XField_ugrid.h"

#include "unit/Field/XField3D_CheckTraceCoord3D_btest.h"

#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "tools/output_std_vector.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XField_ugrid_test_suite )

//===========================================================================//
void Check_Tet_Q1_2Elem(const XField<PhysD3, TopoD3>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 5 );

  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2],  0.0 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check node mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>& xfldArea = xfld.getCellGroup<Tet>(0);
  int nodeMap[4];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );
  BOOST_CHECK_EQUAL( nodeMap[3], 0 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );
  BOOST_CHECK_EQUAL( nodeMap[3], 2 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 6 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge0 = xfld.getBoundaryTraceGroup<Triangle>(0);
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge1 = xfld.getBoundaryTraceGroup<Triangle>(1);
  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge2 = xfld.getBoundaryTraceGroup<Triangle>(2);
  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(3).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge3 = xfld.getBoundaryTraceGroup<Triangle>(3);
  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(4).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge4 = xfld.getBoundaryTraceGroup<Triangle>(4);
  xfldBedge4.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(5).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge5 = xfld.getBoundaryTraceGroup<Triangle>(5);
  xfldBedge5.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-11;

  // Check that interior and boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
      CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Tet_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  {
    std::string meshName = "IO/Meshes/Tet_Q1_2Elem.ugrid";
    XField_ugrid<PhysD3, TopoD3> xfld( comm, meshName );
    Check_Tet_Q1_2Elem(xfld);
  }

  {
    std::string meshName = "IO/Meshes/Tet_Q1_2Elem.b8.ugrid";
    XField_ugrid<PhysD3, TopoD3> xfld( comm, meshName );
    Check_Tet_Q1_2Elem(xfld);
  }

  {
    std::string meshName = "IO/Meshes/Tet_Q1_2Elem.lb8.ugrid";
    XField_ugrid<PhysD3, TopoD3> xfld( comm, meshName );
    Check_Tet_Q1_2Elem(xfld);
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Tet_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  // Write grid
  // Read back in
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Tet_Q1_2Elem.ugrid";
  XField_gmsh<PhysD3, TopoD3> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Tet_Q1_2Elem.ugrid";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Tet_Q1_2Elem(xfld);
}
#endif

//===========================================================================//
void Check_Hex_Q1_2Elem(const XField<PhysD3, TopoD3>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 12 );

  BOOST_CHECK_EQUAL( xfld.DOF( 0)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 0)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 0)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 1)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 1)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 1)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 2)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 2)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 2)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 3)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 3)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 3)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 4)[0],  2.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 4)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 4)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 5)[0],  2.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 5)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 5)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 6)[0],  2.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 6)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 6)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 7)[0],  2.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 7)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 7)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 8)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 8)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 8)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 9)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 9)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 9)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(10)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(10)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(10)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(11)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(11)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(11)[2],  1.0 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Hex) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the element
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD3, TopoD3>::FieldCellGroupType<Hex>& xfldCell = xfld.getCellGroup<Hex>(0);
  BOOST_REQUIRE_EQUAL( xfldCell.nElem(), 2 );

  ElementXField<PhysD3,TopoD3,Hex> xfldElem( xfldCell.basis() );
  ElementXField<PhysD3,TopoD3,Hex>::VectorX X;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  xfldCell.getElement(xfldElem, 0);

  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*xfldCell.basis()->order()+1;
  for (int k = 0; k < npoints; k++)
  {
    for (int j = 0; j < npoints; j++)
    {
      for (int i = 0; i < npoints; i++)
      {
        // Reference coordinates
        Real s = i*1/Real(npoints+1);
        Real t = j*1/Real(npoints+1);
        Real u = k*1/Real(npoints+1);

        // Interpolated reference coordinate
        xfldElem.eval( s, t, u, X );

        // Interpolation should match (just rotated)
        SANS_CHECK_CLOSE( s, X[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( t, X[2], small_tol, close_tol );
        SANS_CHECK_CLOSE( u, X[0], small_tol, close_tol );
      }
    }
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Quad) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 6 );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Quad) );

  // Check that interior and boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
      CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Hex_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  {
    std::string meshName = "IO/Meshes/Hex_Q1_2Elem.ugrid";
    XField_ugrid<PhysD3, TopoD3> xfld( comm, meshName );
    Check_Hex_Q1_2Elem(xfld);
  }


  const Real small_tol = 1e-12;
  const Real close_tol = 5e-11;

  {
    std::string meshName = "IO/Meshes/Hex_Q1_8Elem.ugrid";
    XField_ugrid<PhysD3, TopoD3> xfld( comm, meshName );

    // Check that interior and boundary trace coordinates match
    for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
        CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
        CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
  }

  {
    std::string meshName = "IO/Meshes/Hex_Q1_8Elem.b8.ugrid";
    XField_ugrid<PhysD3, TopoD3> xfld( comm, meshName );

    // Check that interior and boundary trace coordinates match
    for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
        CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
        CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
  }

  {
    std::string meshName = "IO/Meshes/Hex_Q1_8Elem.lb8.ugrid";
    XField_ugrid<PhysD3, TopoD3> xfld( comm, meshName );

    // Check that interior and boundary trace coordinates match
    for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
        CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
        CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Hex_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Hex_Q1_2Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Hex_Q1_2Elem.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Hex_Q1_2Elem(xfld);
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
