// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MetricOps_btest
// testing of the MetricOps class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/Metric/MetricOps.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_SVD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS_InnerProduct.h"

#include <iostream>
using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
namespace DLA
{

}
}

using namespace SANS::DLA;
using namespace SANS::Metric;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MetricOps_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( StepMatrix1D )
{
  typedef PhysD1 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;
  typedef MatrixSymS<PhysDim::D,Real> StepMatrix;

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Metric M0 = {{1.2}};
  Metric M1 = {{4.9}};

  StepMatrix S = computeStepMatrix( M0, M0 );
  SANS_CHECK_CLOSE( 0.0, S(0,0), small_tol, close_tol );

  S = computeStepMatrix( M1, M1 );
  SANS_CHECK_CLOSE( 0.0, S(0,0), small_tol, close_tol );

  S = computeStepMatrix( M0, M1 );
  SANS_CHECK_CLOSE( 1.406913648322627e+00, S(0,0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( StepMatrix2D )
{
  typedef PhysD2 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;
  typedef MatrixSymS<PhysDim::D,Real> StepMatrix;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Metric M0 = {{1.2}, {-0.3, 1.5}};
  Metric M1 = {{4.9}, { 0.7, 2.3}};

  StepMatrix S = computeStepMatrix( M0, M0 );
  SANS_CHECK_CLOSE( 0.0, S(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(1,1), small_tol, close_tol );

  S = computeStepMatrix( M1, M1 );
  SANS_CHECK_CLOSE( 0.0, S(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(1,1), small_tol, close_tol );

  S = computeStepMatrix( M0, M1 );
  SANS_CHECK_CLOSE( 1.411167994606660e+00, S(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 4.424956858156294e-01, S(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 4.300312003596226e-01, S(1,1), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( StepMatrix3D )
{
  typedef PhysD3 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;
  typedef MatrixSymS<PhysDim::D,Real> StepMatrix;

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-12;

  Metric M0 = {{ 1.2}, {-0.3, 1.5}, { 0.2, 0.4, 4.6}};
  Metric M1 = {{ 4.9}, { 0.7, 2.3}, {-0.1, 1.4, 6.9}};

  StepMatrix S = computeStepMatrix( M0, M0 );
  SANS_CHECK_CLOSE( 0.0, S(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(2,2), small_tol, close_tol );

  S = computeStepMatrix( M1, M1 );
  SANS_CHECK_CLOSE( 0.0, S(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, S(2,2), small_tol, close_tol );

  S = computeStepMatrix( M0, M1 );
  SANS_CHECK_CLOSE( 1.413086046118749e+00, S(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 4.742662721749254e-01, S(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 3.699244516052694e-01, S(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE(-1.529200794620964e-01, S(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2.107772278635405e-01, S(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 3.609045104397143e-01, S(2,2), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( RotateRefStepMatrix2D )
{
  typedef PhysD2 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> StepMatrix;
  typedef MatrixS<PhysDim::D,PhysDim::D,Real> Matrix;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Matrix J0 = {{1.2, 0.7}, {-0.3, 1.5}};
  Matrix U, VT;
  VectorS<2,Real> sigma;

  SVD(J0,U,sigma,VT);

  StepMatrix S_eq = {{4.9}, { 0.7, 2.3}};

  StepMatrix S = rotateRefStepMatrix( S_eq, U, VT );
  SANS_CHECK_CLOSE( 5.042340168878169e+00, S(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE(-3.156815440289512e-01, S(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2.157659831121834e+00, S(1,1), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ExponentialMap1D )
{
  typedef PhysD1 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;
  typedef MatrixSymS<PhysDim::D,Real> StepMatrix;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Metric M0 = {{1.2}};
  Metric M1 = {{4.9}};
  StepMatrix S = 0; //zero step matrix should return the original metrics

  Metric M = computeExponentialMap( M0, S );
  SANS_CHECK_CLOSE( M0(0,0), M(0,0), small_tol, close_tol );

  M = computeExponentialMap( M1, S );
  SANS_CHECK_CLOSE( M1(0,0), M(0,0), small_tol, close_tol );

  S = M1;
  M = computeExponentialMap( M0, S );
  SANS_CHECK_CLOSE( 1.611477356219226e+02, M(0,0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ExponentialMap2D )
{
  typedef PhysD2 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;
  typedef MatrixSymS<PhysDim::D,Real> StepMatrix;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Metric M0 = {{1.2}, {-0.3, 1.5}};
  Metric M1 = {{4.9}, { 0.7, 2.3}};
  StepMatrix S = 0; //zero step matrix should return the original metrics

  Metric M = computeExponentialMap( M0, S );
  SANS_CHECK_CLOSE( M0(0,0), M(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(1,0), M(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(1,1), M(1,1), small_tol, close_tol );

  M = computeExponentialMap( M1, S );
  SANS_CHECK_CLOSE( M1(0,0), M(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M1(1,0), M(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M1(1,1), M(1,1), small_tol, close_tol );

  S = M1;
  M = computeExponentialMap( M0, S );
  SANS_CHECK_CLOSE( 1.689081334052914e+02, M(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2.413597922593946e+01, M(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 1.700908086773891e+01, M(1,1), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ExponentialMap3D )
{
  typedef PhysD3 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;
  typedef MatrixSymS<PhysDim::D,Real> StepMatrix;

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-12;

  Metric M0 = {{ 1.2}, {-0.3, 1.5}, { 0.2, 0.4, 4.6}};
  Metric M1 = {{ 4.9}, { 0.7, 2.3}, {-0.1, 1.4, 6.9}};
  StepMatrix S = 0; //zero step matrix should return the original metrics

  Metric M = computeExponentialMap( M0, S );
  SANS_CHECK_CLOSE( M0(0,0), M(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(1,0), M(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(1,1), M(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(2,0), M(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(2,1), M(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(2,2), M(2,2), small_tol, close_tol );

  M = computeExponentialMap( M1, S );
  SANS_CHECK_CLOSE( M1(0,0), M(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M1(1,0), M(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M1(1,1), M(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( M1(2,0), M(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M1(2,1), M(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( M1(2,2), M(2,2), small_tol, close_tol );

  S = M1;
  M = computeExponentialMap( M0, S );
  SANS_CHECK_CLOSE( 1.726490934783477e+02, M(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 5.983043096419737e+01, M(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 3.006076786298605e+02, M(1,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2.015235300285815e+02, M(2,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 1.367457574566643e+03, M(2,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( 6.459690392949982e+03, M(2,2), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AffineInvariantInterpolation1D )
{
  typedef PhysD1 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Metric M0 = {{1.2}};
  Metric M1 = {{4.9}};

  Metric M = computeAffineInvInterp( M0, M1, 0.0 );
  SANS_CHECK_CLOSE( M(0,0), M0(0,0), small_tol, close_tol );

  M = computeAffineInvInterp( M0, M1, 1.0 );
  SANS_CHECK_CLOSE( M(0,0), M1(0,0), small_tol, close_tol );

  M = computeAffineInvInterp( M0, M1, 0.5 );
  SANS_CHECK_CLOSE( M(0,0), 2.424871130596428e+00, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AffineInvariantInterpolation2D )
{
  typedef PhysD2 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-12;

  Metric M0 = {{1.2}, {-0.3, 1.5}};
  Metric M1 = {{4.9}, { 0.7, 2.3}};

  Metric M = computeAffineInvInterp( M0, M1, 0.0 );
  SANS_CHECK_CLOSE( M(0,0), M0(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,0), M0(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,1), M0(1,1), small_tol, close_tol );

  M = computeAffineInvInterp( M0, M1, 1.0 );
  SANS_CHECK_CLOSE( M(0,0), M1(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,0), M1(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,1), M1(1,1), small_tol, close_tol );

  M = computeAffineInvInterp( M0, M1, 0.5 );
  SANS_CHECK_CLOSE( M(0,0), 2.366615793012786e+00, small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,0),-1.592266995431222e-02, small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,1), 1.814284214256512e+00, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LogEuclideanAverage1D )
{
  typedef PhysD1 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Metric M0 = {{1.2}};
  Metric M1 = {{4.9}};
  Metric M2 = {{3.3}};

  std::vector<Metric> Mvec1 = {M0, M1};
  std::vector<Real> wvec1 = {1./2., 1./2.};

  Metric M = computeLogEuclideanAvg( Mvec1, wvec1 );
  SANS_CHECK_CLOSE( M(0,0), 2.424871130596428e+00, small_tol, close_tol );

  std::vector<Metric> Mvec2 = {M0, M1, M2};
  std::vector<Real> wvec2 = {1./3., 1./3., 1./3.};

  M = computeLogEuclideanAvg( Mvec2, wvec2 );
  SANS_CHECK_CLOSE( M(0,0), 2.687182042787896e+00, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LogEuclideanAverage2D )
{
  typedef PhysD2 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;

  const Real small_tol = 1e-12;
  const Real close_tol = 2e-12;

  Metric M0 = {{1.2}, {-0.3, 1.5}};
  Metric M1 = {{4.9}, { 0.7, 2.3}};
  Metric M2 = {{3.3}, {-0.6, 0.8}};

  std::vector<Metric> Mvec1 = {M0, M1};
  std::vector<Real> wvec1 = {1./2., 1./2.};

  Metric M = computeLogEuclideanAvg( Mvec1, wvec1 );
  SANS_CHECK_CLOSE( M(0,0), 2.371905084379876e+00, small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,0),-2.117104296214726e-02, small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,1), 1.810320482211671e+00, small_tol, close_tol );

  std::vector<Metric> Mvec2 = {M0, M1, M2};
  std::vector<Real> wvec2 = {1./3., 1./3., 1./3.};

  M = computeLogEuclideanAvg( Mvec2, wvec2 );
  SANS_CHECK_CLOSE( M(0,0), 2.629000289982262e+00, small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,0),-2.389267534314713e-01, small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,1), 1.344204590033252e+00, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AffineInvariantAverage1D )
{
  typedef PhysD1 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  Metric M0 = {{1.2}};
  Metric M1 = {{4.9}};
  Metric M2 = {{3.3}};

  std::vector<Metric> Mvec1 = {M0, M1};
  std::vector<Real> wvec1 = {1./2., 1./2.};

  Metric M = computeAffineInvAvg( Mvec1, wvec1 );
  SANS_CHECK_CLOSE( M(0,0), 2.424871130596428e+00, small_tol, close_tol );

  std::vector<Metric> Mvec2 = {M0, M1, M2};
  std::vector<Real> wvec2 = {1./3., 1./3., 1./3.};

  M = computeAffineInvAvg( Mvec2, wvec2 );
  SANS_CHECK_CLOSE( M(0,0), 2.687182042787896e+00, small_tol, close_tol );

  std::vector<Metric> Mvec3 = {M0, M0, M0, M0};
  std::vector<Real> wvec3 = {1./4., 1./4., 1./4., 1./4.};

  M = computeAffineInvAvg( Mvec3, wvec3 );
  SANS_CHECK_CLOSE( M(0,0), M0(0,0), small_tol, close_tol );

  M = computeAffineInvAvg( Mvec3 );
  SANS_CHECK_CLOSE( M(0,0), M0(0,0), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AffineInvariantAverage2D )
{
  typedef PhysD2 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;

  const Real small_tol = 1e-8;
  const Real close_tol = 1e-8;

  Metric M0 = {{1.2}, {-0.3, 1.5}};
  Metric M1 = {{4.9}, { 0.7, 2.3}};
  Metric M2 = {{3.3}, {-0.6, 0.8}};

  std::vector<Metric> Mvec1 = {M0, M1};
  std::vector<Real> wvec1 = {1./2., 1./2.};

  Metric M = computeAffineInvAvg( Mvec1, wvec1 );
  SANS_CHECK_CLOSE( M(0,0), 2.366615793012786e+00, small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,0),-1.592266995431222e-02, small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,1), 1.814284214256512e+00, small_tol, close_tol );

  std::vector<Metric> Mvec2 = {M0, M1, M2};
  std::vector<Real> wvec2 = {1./3., 1./3., 1./3.};

  M = computeAffineInvAvg( Mvec2, wvec2 );
  SANS_CHECK_CLOSE( M(0,0), 2.612363436406706e+00, small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,0),-0.231724470612202e+00, small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,1), 1.351467580875473e+00, small_tol, close_tol );

  std::vector<Metric> Mvec3 = {M0, M0, M0, M0};
  std::vector<Real> wvec3 = {1./4., 1./4., 1./4., 1./4.};

  M = computeAffineInvAvg( Mvec3, wvec3 );
  SANS_CHECK_CLOSE( M(0,0), M0(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,0), M0(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,1), M0(1,1), small_tol, close_tol );

  M = computeAffineInvAvg( Mvec3 );
  SANS_CHECK_CLOSE( M(0,0), M0(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,0), M0(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M(1,1), M0(1,1), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Intersection2D )
{
  typedef PhysD2 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  Real h0 = 1;
  Real h1 = 2;

  Metric M0 = {{1/(h0*h0)}, {0.0, 1/(h0*h0)}};
  Metric M1 = {{1/(h1*h1)}, {0.0, 1/(h1*h1)}};
  Metric M2;

  // M0 is the smaller circle, so it should be chosen in the intersection
  M2 = intersection(M0, M1);
  SANS_CHECK_CLOSE( M0(0,0), M2(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(1,0), M2(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(1,1), M2(1,1), small_tol, close_tol );

  // M0 is the smaller circle, so it should be chosen in the intersection
  M2 = intersection(M1, M0);
  SANS_CHECK_CLOSE( M0(0,0), M2(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(1,0), M2(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(1,1), M2(1,1), small_tol, close_tol );

  h0 = 3;
  h1 = 4;

  M0 = {{1.0}, {0.0, 1/(h0*h0)}};
  M1 = {{1/(h1*h1)}, {0.0, 1.0}};

  // Tall oval with wide oval will intersect with a unit metric
  M2 = intersection(M1, M0);
  SANS_CHECK_CLOSE( 1.0, M2(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, M2(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 1.0, M2(1,1), small_tol, close_tol );

  M2 = intersection(M0, M1);
  SANS_CHECK_CLOSE( 1.0, M2(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.0, M2(1,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 1.0, M2(1,1), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( InverseLengthTensor1D )
{
  typedef PhysD1 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;
  typedef VectorS<PhysDim::D,Real> VectorX;

  const Real small_tol = 1e-11, close_tol = 1e-11;

  Metric M0 = { { 2 } };


  VectorX x0 = { 1 };
  VectorX e0 = { 1 }; // direction

  std::vector<VectorX> edges;
  int nEdge = 5;
  for ( int n = 0; n < nEdge; n++)
  {
    VectorX e = e0; // set the direction
    e /= sqrt(SANS::DLA::InnerProduct(e,M0)); // make unit length under the metric

    edges.push_back( e ); // add the edge
  }

  Metric M1 = computeInverseLengthDistributionTensor( edges );

  // should perfectly recover the initial metric tensor
  SANS_CHECK_CLOSE( M0(0,0), M1(0,0), small_tol, close_tol );
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( InverseLengthTensor2D )
{
  typedef PhysD2 PhysDim;
  typedef MatrixSymS<PhysDim::D,Real> Metric;
  typedef MatrixS<PhysDim::D,PhysDim::D,Real> Matrix;
  typedef VectorS<PhysDim::D,Real> VectorX;

  const Real small_tol = 1e-11, close_tol = 1e-11;

  Metric M0;
  // M0(0,0) = 2.1; M0(0,1) = 0.2; M0(1,1) = 0.7;
  M0(0,0) = 2; M0(1,0) = 0; M0(1,1) = 1;

  // std::cout<< "M0 = " << std::endl << M0 << std::endl;

  // M0 *= 2;

  VectorX e0 = { 1, 0 }; // initial direction

  std::vector<VectorX> edges{};
  int nTheta = 4;
  for ( int n = 0; n < nTheta; n++)
  {
    Real theta = ((Real)n/nTheta)*2*PI;
    // compute a rotation matrix
    Matrix Q = {{ cos(theta), - sin(theta) },
                { sin(theta),   cos(theta) }};

    VectorX e = Q*e0; // set the direction

    // VectorX e;
    // e = { -1, -1};
    e /= sqrt(SANS::DLA::InnerProduct(e,M0)); // make unit length under the metric

    edges.push_back( e ); // add the edge
    // std::cout<< "e = " << e << ", length = " << SANS::DLA::InnerProduct(e,M0) << std::endl;

    // e = {1,0};
    // e /= sqrt(SANS::DLA::InnerProduct(e,M0)); // make unit length under the metric
    //
    // edges.push_back( e ); // add the edge
    // std::cout<< "e = " << e << ", length = " << SANS::DLA::InnerProduct(e,M0) << std::endl;
    //
    // e = {0,1};
    // e /= sqrt(SANS::DLA::InnerProduct(e,M0)); // make unit length under the metric
    //
    // edges.push_back( e ); // add the edge
    // std::cout<< "e = " << e << ", length = " << SANS::DLA::InnerProduct(e,M0) << std::endl;
  }

  Metric M1 = computeInverseLengthDistributionTensor( edges );

  // should perfectly recover the initial metric tensor
  SANS_CHECK_CLOSE( M0(0,0), M1(0,0), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(0,1), M1(0,1), small_tol, close_tol );
  SANS_CHECK_CLOSE( M0(1,1), M1(1,1), small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
