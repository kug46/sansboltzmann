function [p,D] = mkmesh1d( p0 , m0 , D0 )

if (nargin==0)
    % example
    clc;
    p0 = 0:10;
    m0 = .25*ones(length(p0),1);
    D0 = [];
elseif (nargin==2)
    % we weren't provided D0
    D0 = [];
elseif (nargin==1)
    % implied metric!
    m0 = [];
    np = length(p0);
    D0 = ones(np,np) -diag(ones(np,1));
end
% p0: nodal coordinates, np x nd (np = # points, nd = dimension)
% m0: metric defined at nodes, np x 1

np = length(p0);
ne = np -1;

% reorder as continguous edges
[p0,idx] = sort(p0);
if ~isempty(m0)
    m0 = m0(idx);
end
    
% evaluate the length of every edge under the metric
h0 = zeros(ne,1);

for k=1:ne
    % edge "vector"
    e = norm( p0(k+1) -p0(k) );
    
    h1 = 1./sqrt(m0(k));
    h2 = 1./sqrt(m0(k+1));
    l1 = e/h1;
    
    a = h2/h1;
    
    % alauzet's 2010 paper, pp. 4-5 (you can use gauss quad if you want...)
    if ( abs(a-1)<1e-12 )
        h0(k) = l1;
    else
        h0(k) = l1*(a -1.)/(a*log(a)); 
    end
end

if isempty(D0)
    D0 = zeros(np,np);
    for i=1:np
        for j=i+1:np
            D0(i,j) = D0(i,j-1) +h0(j-1);
        end
    end
    D0 = (D0 +D0'); % we only computed the symmetric part
    disp(D0);
end

% embed into a "higher" dimensional space (not really)
u0 = embed( D0 );

% uniformly tessellate
[u,D] = uniformly_tessellate( u0 );

% extract
p = extract_mesh( p0 , u0 , u );

disp(D)
end

function u = embed(D)
% isometric embedding by stretching, the first point will start at 0
u = D(1,:)';
end

function [u,D] = uniformly_tessellate( u0 )

% measure how many edges we should have
L  = sum( u0(2:end) -u0(1:end-1) );
ne = int64(L);

fprintf('L = %g, ne = %d\n',L,ne);

% if only the ccvt were always this easy...
u = linspace(0,L,ne+1);

% shift
u = u +u0(1);

% compute the distance matrix
np = length(u);
D  = zeros(np,np);
for i=1:np
    for j=i+1:np
        D(i,j) = u(j) -u(i);
    end
end
D = (D +D');
end

function p = extract_mesh( p0 , u0 , u )
% use linear interpolation on the background mesh
assert(length(p0)==length(u0));

np = length(u);
p  = zeros(np,1);

% if only boundary conformity were also this easy...
p(1)  = p0(1);
p(np) = p0(end);

for i=2:np-1
    
    % find the enclosing edge in the background (embedded) mesh
    found = false;
    for j=2:length(u0)
        if ( u(i)>u0(j-1) && u(i)<u0(j) )
            s = ( u(i)-u0(j-1) )/( u0(j) -u0(j-1) );
            p(i) = p0(j-1) +s*(p0(j) -p0(j-1));
            found = true;
            break;
        end
    end
    if ~found
        error('failed barycentric interpolation');
    end
end

end