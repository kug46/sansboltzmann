// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// XField_gmsh_btest
// Test gmsh reader

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include "tools/linspace.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"
#include "Meshing/gmsh/XField_gmsh.h"
#include "Meshing/gmsh/WriteMesh_gmsh.h"

#include "unit/Field/XField2D_CheckTraceCoord2D_btest.h"
#include "unit/Field/XField3D_CheckTraceCoord3D_btest.h"

#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "tools/output_std_vector.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XField_gmsh_test_suite )

//===========================================================================//
void Check_Quad_Q1_1Elem(const XField_gmsh<PhysD2, TopoD2>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 4 );

  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  1 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check node mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD2, TopoD2>::FieldCellGroupType<Quad>& xfldArea = xfld.getCellGroup<Quad>(0);
  int nodeMap[4];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);
  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(3).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge3 = xfld.getBoundaryTraceGroup<Line>(3);
  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Bottom"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Right"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Top"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Left"][0], 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Quad_Q1_1Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Quad_Q1_1Elem.msh";
  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Quad_Q1_1Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Quad_Q1_1Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  // Write grid
  // Read back in
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Quad_Q1_1Elem.msh";
  XField_gmsh<PhysD2, TopoD2> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Quad_Q1_1Elem.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Quad_Q1_1Elem(xfld);
}

//===========================================================================//
void Check_Quad_Q1_2Elem(const XField_gmsh<PhysD2, TopoD2>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 6 );

  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  2 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  2 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1],  1 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check node mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD2, TopoD2>::FieldCellGroupType<Quad>& xfldArea = xfld.getCellGroup<Quad>(0);
  int nodeMap[4];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  xfldBedge0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);
  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  xfldBedge2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(3).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge3 = xfld.getBoundaryTraceGroup<Line>(3);
  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Bottom"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Right"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Top"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Left"][0], 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Quad_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Quad_Q1_2Elem.msh";
  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Quad_Q1_2Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Quad_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  // Write grid
  // Read back in
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Quad_Q1_2Elem.msh";
  XField_gmsh<PhysD2, TopoD2> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Quad_Q1_2Elem.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Quad_Q1_2Elem(xfld);
}

//===========================================================================//
void Check_Quad_Q2_2Elem(const XField_gmsh<PhysD2, TopoD2>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 15 );

  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  -1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0.0     );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  -1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0.4     );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  -1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  0.8     );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  -0.75 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  0.0     );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0],  -0.75 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  0.4     );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  -0.75 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1],  0.8     );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0],   0.0  );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1],  0.0625  );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0],   0.0  );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1],  0.43125 );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0],   0.0  );  BOOST_CHECK_EQUAL( xfld.DOF(8)[1],  0.8     );
  BOOST_CHECK_EQUAL( xfld.DOF(9)[0],   0.75 );  BOOST_CHECK_EQUAL( xfld.DOF(9)[1],  0.0     );
  BOOST_CHECK_EQUAL( xfld.DOF(10)[0],  0.75 );  BOOST_CHECK_EQUAL( xfld.DOF(10)[1], 0.4     );
  BOOST_CHECK_EQUAL( xfld.DOF(11)[0],  0.75 );  BOOST_CHECK_EQUAL( xfld.DOF(11)[1], 0.8     );
  BOOST_CHECK_EQUAL( xfld.DOF(12)[0],  1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(12)[1], 0.0     );
  BOOST_CHECK_EQUAL( xfld.DOF(13)[0],  1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(13)[1], 0.4     );
  BOOST_CHECK_EQUAL( xfld.DOF(14)[0],  1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(14)[1], 0.8     );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  //////////////////////////////////////////////////////////////////////////////////////
  // Check node mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD2, TopoD2>::FieldCellGroupType<Quad>& xfldArea = xfld.getCellGroup<Quad>(0);
  int nodeMap[4];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );
  BOOST_CHECK_EQUAL( nodeMap[3], 2 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 12 );
  BOOST_CHECK_EQUAL( nodeMap[2], 14 );
  BOOST_CHECK_EQUAL( nodeMap[3], 8 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 12 );
  BOOST_CHECK_EQUAL( nodeMap[1], 14 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);
  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  xfldBedge2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6  );
  BOOST_CHECK_EQUAL( nodeMap[1], 12 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(3).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge3 = xfld.getBoundaryTraceGroup<Line>(3);
  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  xfldBedge3.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 14 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8  );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Inflow"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Outflow"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["LowerWall"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["UpperWall"][0], 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Quad_Q2_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/SmoothBump_quad_ref0_Q2.msh";
  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Quad_Q2_2Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Quad_Q2_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/SmoothBump_quad_ref0_Q2.msh";
  XField_gmsh<PhysD2, TopoD2> xfld1( comm, meshName );

  std::string meshNameTest = "IO/SmoothBump_quad_ref0_Q2.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Quad_Q2_2Elem(xfld);
}

//===========================================================================//
void Check_Quad_Q3_2Elem(const XField_gmsh<PhysD2, TopoD2>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 28 );

  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  -1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0.0       );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  -1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  8.0/30.0  );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  -1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  16.0/30.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  -1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(4)[0],  -1.0  );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  0.0       );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  -1.0  );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1],  8.0/30.0  );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0],  -1.0  );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1],  16.0/30.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0],  -1.0  );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1],  0.8       );

  Real xBase = 0.0625*exp(-25.0*0.5*0.5);
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0],  -0.5  );  BOOST_CHECK_EQUAL( xfld.DOF(8)[1],  xBase     );
  BOOST_CHECK_EQUAL( xfld.DOF(9)[0],  -0.5  );  BOOST_CHECK_EQUAL( xfld.DOF(9)[1],  xBase + 1./3. * (0.8 - xBase) );
  BOOST_CHECK_EQUAL( xfld.DOF(10)[0], -0.5  );  BOOST_CHECK_EQUAL( xfld.DOF(10)[1], xBase + 2./3. * (0.8 - xBase) );
  BOOST_CHECK_EQUAL( xfld.DOF(11)[0], -0.5  );  BOOST_CHECK_EQUAL( xfld.DOF(11)[1], 0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(12)[0],  0.0  );  BOOST_CHECK_EQUAL( xfld.DOF(12)[1], 0.0625    );
  BOOST_CHECK_EQUAL( xfld.DOF(13)[0],  0.0  );  BOOST_CHECK_EQUAL( xfld.DOF(13)[1], 0.0625 + 1./3. * (0.8 - 0.0625) );
  BOOST_CHECK_EQUAL( xfld.DOF(14)[0],  0.0  );  BOOST_CHECK_EQUAL( xfld.DOF(14)[1], 0.0625 + 2./3. * (0.8 - 0.0625) );
  BOOST_CHECK_EQUAL( xfld.DOF(15)[0],  0.0  );  BOOST_CHECK_EQUAL( xfld.DOF(15)[1], 0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(16)[0],  0.5  );  BOOST_CHECK_EQUAL( xfld.DOF(16)[1], xBase      );
  BOOST_CHECK_EQUAL( xfld.DOF(17)[0],  0.5  );  BOOST_CHECK_EQUAL( xfld.DOF(17)[1], xBase + 1./3. * (0.8 - xBase) );
  BOOST_CHECK_EQUAL( xfld.DOF(18)[0],  0.5  );  BOOST_CHECK_EQUAL( xfld.DOF(18)[1], xBase + 2./3. * (0.8 - xBase) );
  BOOST_CHECK_EQUAL( xfld.DOF(19)[0],  0.5  );  BOOST_CHECK_EQUAL( xfld.DOF(19)[1], 0.8        );

  BOOST_CHECK_EQUAL( xfld.DOF(20)[0],  1.0  );  BOOST_CHECK_EQUAL( xfld.DOF(20)[1], 0.0       );
  BOOST_CHECK_EQUAL( xfld.DOF(21)[0],  1.0  );  BOOST_CHECK_EQUAL( xfld.DOF(21)[1], 8.0/30.0  );
  BOOST_CHECK_EQUAL( xfld.DOF(22)[0],  1.0  );  BOOST_CHECK_EQUAL( xfld.DOF(22)[1], 16.0/30.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(23)[0],  1.0  );  BOOST_CHECK_EQUAL( xfld.DOF(23)[1], 0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(24)[0],  1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(24)[1], 0.0       );
  BOOST_CHECK_EQUAL( xfld.DOF(25)[0],  1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(25)[1], 8.0/30.0  );
  BOOST_CHECK_EQUAL( xfld.DOF(26)[0],  1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(26)[1], 16.0/30.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(27)[0],  1.5  );  BOOST_CHECK_EQUAL( xfld.DOF(27)[1], 0.8       );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  //////////////////////////////////////////////////////////////////////////////////////
  // Check node mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD2, TopoD2>::FieldCellGroupType<Quad>& xfldArea = xfld.getCellGroup<Quad>(0);
  int nodeMap[4];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0  );
  BOOST_CHECK_EQUAL( nodeMap[1], 12 );
  BOOST_CHECK_EQUAL( nodeMap[2], 15 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3  );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 12 );
  BOOST_CHECK_EQUAL( nodeMap[1], 24 );
  BOOST_CHECK_EQUAL( nodeMap[2], 27 );
  BOOST_CHECK_EQUAL( nodeMap[3], 15 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 24 );
  BOOST_CHECK_EQUAL( nodeMap[1], 27 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);
  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 12 );
  xfldBedge2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 12  );
  BOOST_CHECK_EQUAL( nodeMap[1], 24 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(3).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge3 = xfld.getBoundaryTraceGroup<Line>(3);
  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 15 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3  );
  xfldBedge3.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 27 );
  BOOST_CHECK_EQUAL( nodeMap[1], 15 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Inflow"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Outflow"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["LowerWall"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["UpperWall"][0], 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Quad_Q3_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/SmoothBump_quad_ref0_Q3.msh";
  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Quad_Q3_2Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Quad_Q3_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/SmoothBump_quad_ref0_Q3.msh";
  XField_gmsh<PhysD2, TopoD2> xfld1( comm, meshName );

  std::string meshNameTest = "IO/SmoothBump_quad_ref0_Q3.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Quad_Q3_2Elem(xfld);
}

#if 0
BOOST_AUTO_TEST_CASE( Read_Quad_Q4_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/SmoothBump_quad_ref0_Q4.msh";
  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 45 );

  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  -1.5    );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0.0       );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  -1.5    );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0.2       );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  -1.5    );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  0.4       );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  -1.5    );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  0.6       );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0],  -1.5    );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  -1.125  );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1],  0.0       );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0],  -1.125  );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1],  0.2       );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0],  -1.125  );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1],  0.4       );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0],  -1.125  );  BOOST_CHECK_EQUAL( xfld.DOF(8)[1],  0.6       );
  BOOST_CHECK_EQUAL( xfld.DOF(9)[0],  -1.125  );  BOOST_CHECK_EQUAL( xfld.DOF(9)[1],  0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(10)[0], -0.75   );  BOOST_CHECK_EQUAL( xfld.DOF(10)[1], 0.0       );
  BOOST_CHECK_EQUAL( xfld.DOF(11)[0], -0.75   );  BOOST_CHECK_EQUAL( xfld.DOF(11)[1], 0.2       );
  BOOST_CHECK_EQUAL( xfld.DOF(12)[0], -0.75   );  BOOST_CHECK_EQUAL( xfld.DOF(12)[1], 0.4       );
  BOOST_CHECK_EQUAL( xfld.DOF(13)[0], -0.75   );  BOOST_CHECK_EQUAL( xfld.DOF(13)[1], 0.6       );
  BOOST_CHECK_EQUAL( xfld.DOF(14)[0], -0.75   );  BOOST_CHECK_EQUAL( xfld.DOF(14)[1], 0.8       );

  Real xBase = 0.0625*exp(-25.0*0.375*0.375);
  BOOST_CHECK_EQUAL( xfld.DOF(15)[0], -0.375  );  BOOST_CHECK_EQUAL( xfld.DOF(15)[1], xBase     );
  BOOST_CHECK_EQUAL( xfld.DOF(16)[0], -0.375  );  BOOST_CHECK_EQUAL( xfld.DOF(16)[1], xBase + 1./4. * (0.8 - xBase) );
  BOOST_CHECK_EQUAL( xfld.DOF(17)[0], -0.375  );  BOOST_CHECK_EQUAL( xfld.DOF(17)[1], xBase + 2./4. * (0.8 - xBase) );
  BOOST_CHECK_EQUAL( xfld.DOF(18)[0], -0.375  );  BOOST_CHECK_EQUAL( xfld.DOF(18)[1], xBase + 3./4. * (0.8 - xBase) );
  BOOST_CHECK_EQUAL( xfld.DOF(19)[0], -0.375  );  BOOST_CHECK_EQUAL( xfld.DOF(19)[1], 0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(20)[0],  0.0    );  BOOST_CHECK_EQUAL( xfld.DOF(20)[1], 0.0625    );
  BOOST_CHECK_EQUAL( xfld.DOF(21)[0],  0.0    );  BOOST_CHECK_EQUAL( xfld.DOF(21)[1], 0.0625 + 1./4. * (0.8 - 0.0625) );
  BOOST_CHECK_EQUAL( xfld.DOF(22)[0],  0.0    );  BOOST_CHECK_EQUAL( xfld.DOF(22)[1], 0.0625 + 2./4. * (0.8 - 0.0625) );
  BOOST_CHECK_EQUAL( xfld.DOF(23)[0],  0.0    );  BOOST_CHECK_EQUAL( xfld.DOF(23)[1], 0.0625 + 3./4. * (0.8 - 0.0625) );
  BOOST_CHECK_EQUAL( xfld.DOF(24)[0],  0.0    );  BOOST_CHECK_EQUAL( xfld.DOF(24)[1], 0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(25)[0],  0.375  );  BOOST_CHECK_EQUAL( xfld.DOF(25)[1], xBase     );
  BOOST_CHECK_EQUAL( xfld.DOF(26)[0],  0.375  );  BOOST_CHECK_EQUAL( xfld.DOF(26)[1], xBase + 1./4. * (0.8 - xBase) );
  BOOST_CHECK_EQUAL( xfld.DOF(27)[0],  0.375  );  BOOST_CHECK_EQUAL( xfld.DOF(27)[1], xBase + 2./4. * (0.8 - xBase) );
  BOOST_CHECK_EQUAL( xfld.DOF(28)[0],  0.375  );  BOOST_CHECK_EQUAL( xfld.DOF(28)[1], xBase + 3./4. * (0.8 - xBase) );
  BOOST_CHECK_EQUAL( xfld.DOF(29)[0],  0.375  );  BOOST_CHECK_EQUAL( xfld.DOF(29)[1], 0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(30)[0],  0.75   );  BOOST_CHECK_EQUAL( xfld.DOF(30)[1], 0.0       );
  BOOST_CHECK_EQUAL( xfld.DOF(31)[0],  0.75   );  BOOST_CHECK_EQUAL( xfld.DOF(31)[1], 0.2       );
  BOOST_CHECK_EQUAL( xfld.DOF(32)[0],  0.75   );  BOOST_CHECK_EQUAL( xfld.DOF(32)[1], 0.4       );
  BOOST_CHECK_EQUAL( xfld.DOF(33)[0],  0.75   );  BOOST_CHECK_EQUAL( xfld.DOF(33)[1], 0.6       );
  BOOST_CHECK_EQUAL( xfld.DOF(34)[0],  0.75   );  BOOST_CHECK_EQUAL( xfld.DOF(34)[1], 0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(35)[0],  1.125  );  BOOST_CHECK_EQUAL( xfld.DOF(35)[1],  0.0       );
  BOOST_CHECK_EQUAL( xfld.DOF(36)[0],  1.125  );  BOOST_CHECK_EQUAL( xfld.DOF(36)[1],  0.2       );
  BOOST_CHECK_EQUAL( xfld.DOF(37)[0],  1.125  );  BOOST_CHECK_EQUAL( xfld.DOF(37)[1],  0.4       );
  BOOST_CHECK_EQUAL( xfld.DOF(38)[0],  1.125  );  BOOST_CHECK_EQUAL( xfld.DOF(38)[1],  0.6       );
  BOOST_CHECK_EQUAL( xfld.DOF(39)[0],  1.125  );  BOOST_CHECK_EQUAL( xfld.DOF(39)[1],  0.8       );

  BOOST_CHECK_EQUAL( xfld.DOF(40)[0],  1.5    );  BOOST_CHECK_EQUAL( xfld.DOF(40)[1],  0.0       );
  BOOST_CHECK_EQUAL( xfld.DOF(41)[0],  1.5    );  BOOST_CHECK_EQUAL( xfld.DOF(41)[1],  0.2       );
  BOOST_CHECK_EQUAL( xfld.DOF(42)[0],  1.5    );  BOOST_CHECK_EQUAL( xfld.DOF(42)[1],  0.4       );
  BOOST_CHECK_EQUAL( xfld.DOF(43)[0],  1.5    );  BOOST_CHECK_EQUAL( xfld.DOF(43)[1],  0.6       );
  BOOST_CHECK_EQUAL( xfld.DOF(44)[0],  1.5    );  BOOST_CHECK_EQUAL( xfld.DOF(44)[1],  0.8       );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Quad) );

  //////////////////////////////////////////////////////////////////////////////////////
  // Check node mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  XField<PhysD2, TopoD2>::FieldCellGroupType<Quad>& xfldArea = xfld.getCellGroup<Quad>(0);
  int nodeMap[4];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0  );
  BOOST_CHECK_EQUAL( nodeMap[1], 20 );
  BOOST_CHECK_EQUAL( nodeMap[2], 24 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4  );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 20 );
  BOOST_CHECK_EQUAL( nodeMap[1], 40 );
  BOOST_CHECK_EQUAL( nodeMap[2], 44 );
  BOOST_CHECK_EQUAL( nodeMap[3], 24 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge = xfld.getBoundaryTraceGroup<Line>(0);
  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  xfldBedge = xfld.getBoundaryTraceGroup<Line>(1);
  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 40 );
  BOOST_CHECK_EQUAL( nodeMap[1], 44 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );
  xfldBedge = xfld.getBoundaryTraceGroup<Line>(2);
  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 20 );
  xfldBedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 20  );
  BOOST_CHECK_EQUAL( nodeMap[1], 40 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(3).topoTypeID() == typeid(Line) );
  xfldBedge = xfld.getBoundaryTraceGroup<Line>(3);
  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 24 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4  );
  xfldBedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 44 );
  BOOST_CHECK_EQUAL( nodeMap[1], 24 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Inflow"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Outflow"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["LowerWall"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["UpperWall"][0], 3 );
}
#endif

//===========================================================================//
void Check_Tri_Q1_2Elem(const XField_gmsh<PhysD2, TopoD2>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 4 );

  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  1 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check node mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);
  int nodeMap[3];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);
  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(3).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge3 = xfld.getBoundaryTraceGroup<Line>(3);
  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Bottom"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Right"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Top"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Left"][0], 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Tri_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Tri_Q1_2Elem.msh";
  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Tri_Q1_2Elem(xfld);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Tri_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  // Write grid
  // Read back in
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Tri_Q1_2Elem.msh";
  XField_gmsh<PhysD2, TopoD2> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Tri_Q1_2Elem.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Tri_Q1_2Elem(xfld);
}

//===========================================================================//
void Check_Tri_Q2_2Elem(const XField_gmsh<PhysD2, TopoD2>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 9 );

  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1],  0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1],  0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[1],  0.5 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check node mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);
  int nodeMap[3];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);
  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(3).topoTypeID() == typeid(Line) );
  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge3 = xfld.getBoundaryTraceGroup<Line>(3);
  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Bottom"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Right"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Top"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Left"][0], 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Tri_Q2_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Tri_Q2_2Elem.msh";
  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Tri_Q2_2Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Tri_Q2_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  // Write grid
  // Read back in
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Tri_Q2_2Elem.msh";
  XField_gmsh<PhysD2, TopoD2> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Tri_Q2_2Elem.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD2, TopoD2> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Tri_Q2_2Elem(xfld);
}

//===========================================================================//
void Check_Tet_Q1_2Elem(const XField_gmsh<PhysD3, TopoD3>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 5 );

  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2],  1.0 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check node mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>& xfldArea = xfld.getCellGroup<Tet>(0);
  int nodeMap[4];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 6 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge0 = xfld.getBoundaryTraceGroup<Triangle>(0);
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge1 = xfld.getBoundaryTraceGroup<Triangle>(1);
  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge2 = xfld.getBoundaryTraceGroup<Triangle>(2);
  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(3).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge3 = xfld.getBoundaryTraceGroup<Triangle>(3);
  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(4).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge4 = xfld.getBoundaryTraceGroup<Triangle>(4);
  xfldBedge4.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(5).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge5 = xfld.getBoundaryTraceGroup<Triangle>(5);
  xfldBedge5.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["iMin"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["iMax"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["jMin"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["jMax"][0], 3 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["kMin"][0], 4 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["kMax"][0], 5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Tet_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Tet_Q1_2Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Tet_Q1_2Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Tet_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  // Write grid
  // Read back in
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Tet_Q1_2Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Tet_Q1_2Elem.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Tet_Q1_2Elem(xfld);
}

//===========================================================================//
void Check_Tet_Q2_1Elem(const XField_gmsh<PhysD3, TopoD3>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 10 );

  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[2],  0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[2],  0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(9)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(9)[1],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(9)[2],  0.5 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check node mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>& xfldArea = xfld.getCellGroup<Tet>(0);
  int nodeMap[4];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge0 = xfld.getBoundaryTraceGroup<Triangle>(0);
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge1 = xfld.getBoundaryTraceGroup<Triangle>(1);
  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge2 = xfld.getBoundaryTraceGroup<Triangle>(2);
  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(3).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBedge3 = xfld.getBoundaryTraceGroup<Triangle>(3);
  xfldBedge3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["face1"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["face2"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["face3"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["face4"][0], 3 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Tet_Q2_1Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Tet_Q2_1Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Tet_Q2_1Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Tet_Q2_1Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Tet_Q2_1Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Tet_Q2_1Elem.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Tet_Q2_1Elem(xfld);
}

//===========================================================================//
void Check_Hex_Q1_2Elem(const XField_gmsh<PhysD3, TopoD3>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 12 );

  BOOST_CHECK_EQUAL( xfld.DOF( 0)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 0)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 0)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 1)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 1)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 1)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 2)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 2)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 2)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 3)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 3)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 3)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 4)[0],  2.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 4)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 4)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 5)[0],  2.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 5)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 5)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 6)[0],  2.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 6)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 6)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 7)[0],  2.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 7)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 7)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 8)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 8)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 8)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF( 9)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 9)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF( 9)[2],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(10)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(10)[1],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(10)[2],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(11)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(11)[1],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(11)[2],  1.0 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Hex) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the element
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD3, TopoD3>::FieldCellGroupType<Hex>& xfldCell = xfld.getCellGroup<Hex>(0);
  BOOST_REQUIRE_EQUAL( xfldCell.nElem(), 2 );

  ElementXField<PhysD3,TopoD3,Hex> xfldElem( xfldCell.basis() );
  ElementXField<PhysD3,TopoD3,Hex>::VectorX X;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  xfldCell.getElement(xfldElem, 0);

  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*xfldCell.basis()->order()+1;
  for (int k = 0; k < npoints; k++)
  {
    for (int j = 0; j < npoints; j++)
    {
      for (int i = 0; i < npoints; i++)
      {
        // Reference coordinates
        Real s = i*1/Real(npoints+1);
        Real t = j*1/Real(npoints+1);
        Real u = k*1/Real(npoints+1);

        // Interpolated reference coordinate
        xfldElem.eval( s, t, u, X );

        // Interpolation should match (just rotated)
        SANS_CHECK_CLOSE( s, X[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( t, X[2], small_tol, close_tol );
        SANS_CHECK_CLOSE( u, X[0], small_tol, close_tol );
      }
    }
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Quad) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 6 );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Quad) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Xmax"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Xmin"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Ymax"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Ymin"][0], 3 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Zmax"][0], 4 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Zmin"][0], 5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Hex_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Hex_Q1_2Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Hex_Q1_2Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Hex_Q1_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Hex_Q1_2Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Hex_Q1_2Elem.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Hex_Q1_2Elem(xfld);
}

//===========================================================================//
void Check_Hex_Q2_2Elem(const XField_gmsh<PhysD3, TopoD3>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 45 );

  std::vector<XField_gmsh<PhysD3, TopoD3>::VectorX> DOFs = {
      {0.0, 0.0, 0.0},
      {0.0, 0.0, 1.0},
      {0.0, 1.0, 0.0},
      {2.0, 0.0, 1.0},
      {0.0, 1.0, 1.0},
      {2.0, 0.0, 0.0},
      {2.0, 1.0, 0.0},
      {2.0, 1.0, 1.0},
      {0.0, 0.0, 0.5},
      {0.5, 0.0, 0.0},
      {1.0, 0.0, 0.0},
      {1.5, 0.0, 0.0},
      {0.0, 0.5, 0.0},
      {0.5, 0.0, 1.0},
      {1.0, 0.0, 1.0},
      {1.5, 0.0, 1.0},
      {0.0, 0.5, 1.0},
      {0.0, 1.0, 0.5},
      {0.5, 1.0, 0.0},
      {1.0, 1.0, 0.0},
      {1.5, 1.0, 0.0},
      {0.5, 1.0, 1.0},
      {1.0, 1.0, 1.0},
      {1.5, 1.0, 1.0},
      {2.0, 0.0, 0.5},
      {2.0, 0.5, 0.0},
      {2.0, 0.5, 1.0},
      {2.0, 1.0, 0.5},
      {2.0, 0.5, 0.5},
      {0.0, 0.5, 0.5},
      {0.5, 1.0, 0.5},
      {1.0, 1.0, 0.5},
      {1.5, 1.0, 0.5},
      {0.5, 0.0, 0.5},
      {1.0, 0.0, 0.5},
      {1.5, 0.0, 0.5},
      {0.5, 0.5, 1.0},
      {1.0, 0.5, 1.0},
      {1.5, 0.5, 1.0},
      {0.5, 0.5, 0.0},
      {1.0, 0.5, 0.0},
      {1.5, 0.5, 0.0},
      {0.5, 0.5, 0.5},
      {1.0, 0.5, 0.5},
      {1.5, 0.5, 0.5}};


  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
  {
    BOOST_CHECK_EQUAL( xfld.DOF(n)[0], DOFs[n][0] );
    BOOST_CHECK_EQUAL( xfld.DOF(n)[1], DOFs[n][1] );
    BOOST_CHECK_EQUAL( xfld.DOF(n)[2], DOFs[n][2] );
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Hex) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the element
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD3, TopoD3>::FieldCellGroupType<Hex>& xfldCell = xfld.getCellGroup<Hex>(0);
  BOOST_REQUIRE_EQUAL( xfldCell.nElem(), 2 );

  ElementXField<PhysD3,TopoD3,Hex> xfldElem( xfldCell.basis() );
  ElementXField<PhysD3,TopoD3,Hex>::VectorX X;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  xfldCell.getElement(xfldElem, 0);

  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*xfldCell.basis()->order()+1;
  for (int k = 0; k < npoints; k++)
  {
    for (int j = 0; j < npoints; j++)
    {
      for (int i = 0; i < npoints; i++)
      {
        // Reference coordinates
        Real s = i*1/Real(npoints+1);
        Real t = j*1/Real(npoints+1);
        Real u = k*1/Real(npoints+1);

        // Interpolated reference coordinate
        xfldElem.eval( s, t, u, X );

        // Interpolation should match (just rotated)
        SANS_CHECK_CLOSE( s, X[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( t, X[2], small_tol, close_tol );
        SANS_CHECK_CLOSE( u, X[0], small_tol, close_tol );
      }
    }
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Quad) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 6 );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Quad) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Xmax"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Xmin"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Ymax"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Ymin"][0], 3 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Zmax"][0], 4 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Zmin"][0], 5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Hex_Q2_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Hex_Q2_2Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Hex_Q2_2Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Hex_Q2_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Hex_Q2_2Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Hex_Q2_2Elem.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Hex_Q2_2Elem(xfld);
}

//===========================================================================//
void Check_Hex_Q3_2Elem(const XField_gmsh<PhysD3, TopoD3>& xfld)
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 112 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Hex) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check the element
  ////////////////////////////////////////////////////////////////////////////////////////
  const XField<PhysD3, TopoD3>::FieldCellGroupType<Hex>& xfldCell = xfld.getCellGroup<Hex>(0);
  BOOST_REQUIRE_EQUAL( xfldCell.nElem(), 2 );

  ElementXField<PhysD3,TopoD3,Hex> xfldElem( xfldCell.basis() );
  ElementXField<PhysD3,TopoD3,Hex>::VectorX X;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  xfldCell.getElement(xfldElem, 0);

  // Use Lagrange coordinates as DOFs, and interpolate on the reference element in between Lagrange nodes.
  // The interpolated values should match the coordinates where they are evaluated.
  int npoints = 2*xfldCell.basis()->order()+1;
  for (int k = 0; k < npoints; k++)
  {
    for (int j = 0; j < npoints; j++)
    {
      for (int i = 0; i < npoints; i++)
      {
        // Reference coordinates
        Real s = i*1/Real(npoints+1);
        Real t = j*1/Real(npoints+1);
        Real u = k*1/Real(npoints+1);

        // Interpolated reference coordinate
        xfldElem.eval( s, t, u, X );

        // Interpolation should match (just rotated)
        SANS_CHECK_CLOSE( s, X[1], small_tol, close_tol );
        SANS_CHECK_CLOSE( t, X[2], small_tol, close_tol );
        SANS_CHECK_CLOSE( u, X[0], small_tol, close_tol );
      }
    }
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check interior trace mappings
  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Quad) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 6 );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Quad) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Xmax"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Xmin"][0], 1 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Ymax"][0], 2 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Ymin"][0], 3 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Zmax"][0], 4 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["Zmin"][0], 5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Hex_Q3_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Hex_Q3_2Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Hex_Q3_2Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_Hex_Q3_2Elem )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/Hex_Q3_2Elem.msh";
  XField_gmsh<PhysD3, TopoD3> xfld1( comm, meshName );

  std::string meshNameTest = "IO/Hex_Q3_2Elem.msh";
  WriteMesh_gmsh( xfld1, meshNameTest, xfld1.getBCBoundaryGroups() );

  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Check_Hex_Q3_2Elem(xfld);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ReadWrite_ParallelTest )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/ParallelTest.msh";
  XField_gmsh<PhysD3, TopoD3> xfld( world, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups = xfld.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups["farfield"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups["wall"][0], 1 );

  std::string meshNameTest = "IO/ParallelTest.msh";
  WriteMesh_gmsh( xfld, meshNameTest, xfld.getBCBoundaryGroups() );

  XField_gmsh<PhysD3, TopoD3> xfld1( world, meshNameTest );
  std::remove(meshNameTest.c_str());

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups1 = xfld1.getBCBoundaryGroups();
  BOOST_CHECK_EQUAL( BCBoundaryGroups1["farfield"][0], 0 );
  BOOST_CHECK_EQUAL( BCBoundaryGroups1["wall"][0], 1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_Periodic )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 1;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/periodicBox_gmsh.msh";
  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 14 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check periodic BC group adjacency
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(5).getGroupRight() );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check periodic BCs have no elements
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(4).nElem() );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check that grid trace is identical for adjacent elements
  ////////////////////////////////////////////////////////////////////////////////////////
  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;
  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
      CheckInteriorTraceCoordinates3D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Read_PeriodicParallel )
{
  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Use 1 core only
  ////////////////////////////////////////////////////////////////////////////////////////
  int size = 2;
  int color = world.rank() < size ? 0 : 1;
  // split the world into two groups
  mpi::communicator comm = world.split(color);
  // only use group 0 for testing
  if (color == 1) return;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string meshName = "IO/Meshes/periodicBox_gmsh.msh";
  XField_gmsh<PhysD3, TopoD3> xfld( comm, meshName );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check nodes
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 14 );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check Cells
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check boundary mappings
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );
  for (int n = 0; n < xfld.nBoundaryTraceGroups(); n++)
    BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(n).topoTypeID() == typeid(Triangle) );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check periodic BC group adjacency
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(5).getGroupRight() );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check periodic BCs have no elements
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK_EQUAL( 0, xfld.getBoundaryTraceGroupBase(4).nElem() );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check that grid trace is identical for adjacent elements
  ////////////////////////////////////////////////////////////////////////////////////////
  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;
  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
      CheckInteriorTraceCoordinates3D(small_tol, close_tol, xfld.getInteriorTraceGroupsGlobal({0}) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
