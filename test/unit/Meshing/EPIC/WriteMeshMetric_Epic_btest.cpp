// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// WriteMeshMetric_Epic_btest
// testing of the WriteMeshMetric class for Epic

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <cstdio>
#include <iostream>
#include <fstream>

#include "Meshing/EPIC/XField_PX.h"
#include "Meshing/EPIC/WriteMeshMetric_Epic.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( WriteMeshMetric_Epic_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WriteMeshMetric_Epic_2D_SymmetricMatrix_P1 )
{
  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  mpi::communicator world;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int ii = 3, jj = 4;
  int nNodeGlobal = (ii+1)*(jj+1);

  XField2D_Box_Triangle_Lagrange_X1 xfld(world,ii,jj);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);

  MatrixSym M0 = {{ 0.2},{-0.5, 0.3}};

  // initialize the parallel metric field using the native node numbering
  for (int n = 0; n < metric_request.nDOF(); n++)
    metric_request.DOF(n) = (metric_request.local2nativeDOFmap(n)+1)*M0;

  std::string mesh_filename = "IO/test.grm";
  std::string sol_filename = "IO/test.sol";
  WriteMeshMetric_Epic(metric_request, mesh_filename, sol_filename);

  // test reading back in the mesh
  XField_PX<PhysD2, TopoD2> xfld_readin(world, mesh_filename);

  // test reading back in the metric file (only rank 0)
  if (world.rank() == 0)
  {
    std::ifstream fsol(sol_filename);

    int nNode, Dim;
    fsol >> nNode >> Dim;

    BOOST_CHECK_EQUAL( nNodeGlobal, nNode );
    BOOST_CHECK_EQUAL( 2, Dim );

    for (int n = 0; n < nNode; n++)
    {
      int ind;
      MatrixSym M;
      fsol >> ind;
      BOOST_CHECK_EQUAL( ind, n+1 );

      MatrixSym MTrue = ind*M0;
      Real data[MatrixSym::SIZE];
      for (int i = 0; i < MatrixSym::SIZE; i++)
        fsol >> data[i];

      // check that the matrix was written as upper triangular
      SANS_CHECK_CLOSE( MTrue(0,0), data[0], small_tol, close_tol );
      SANS_CHECK_CLOSE( MTrue(0,1), data[1], small_tol, close_tol );
      SANS_CHECK_CLOSE( MTrue(1,1), data[2], small_tol, close_tol );
    }

    //Delete the written file
    std::remove(mesh_filename.c_str());
    std::remove(sol_filename.c_str());
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WriteMeshMetric_Epic_3D_SymmetricMatrix_P1 )
{
  typedef DLA::MatrixSymS<3, Real> MatrixSym;

  mpi::communicator world;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  int ii = 3, jj = 4, kk = 5;
  int nNodeGlobal = (ii+1)*(jj+1)*(kk+1);

  XField3D_Box_Tet_X1 xfld(world, ii, jj, kk);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);

  MatrixSym M0 = {{ 0.2},{-0.5, 0.3},{1.5, 0.1, 0.7}};

  // initialize the parallel metric field using the native node numbering
  for (int n = 0; n < metric_request.nDOF(); n++)
    metric_request.DOF(n) = (metric_request.local2nativeDOFmap(n)+1)*M0;

  std::string mesh_filename = "IO/test.grm";
  std::string sol_filename = "IO/test.sol";
  WriteMeshMetric_Epic(metric_request, mesh_filename, sol_filename);

  // test reading back in the mesh
  XField_PX<PhysD3, TopoD3> xfld_readin(world, mesh_filename);

  // test reading back in the metric file (only rank 0)
  if (world.rank() == 0)
  {
    std::ifstream fsol(sol_filename);

    int nNode, Dim;
    fsol >> nNode >> Dim;

    BOOST_CHECK_EQUAL( nNodeGlobal, nNode );
    BOOST_CHECK_EQUAL( 3, Dim );

    for (int n = 0; n < nNode; n++)
    {
      int ind;
      MatrixSym M;
      fsol >> ind;
      BOOST_CHECK_EQUAL( ind, n+1 );

      MatrixSym MTrue = ind*M0;
      Real data[MatrixSym::SIZE];
      for (int i = 0; i < MatrixSym::SIZE; i++)
        fsol >> data[i];

      // check that the matrix was written as upper triangular
      SANS_CHECK_CLOSE( MTrue(0,0), data[0], small_tol, close_tol );
      SANS_CHECK_CLOSE( MTrue(0,1), data[1], small_tol, close_tol );
      SANS_CHECK_CLOSE( MTrue(0,2), data[2], small_tol, close_tol );
      SANS_CHECK_CLOSE( MTrue(1,1), data[3], small_tol, close_tol );
      SANS_CHECK_CLOSE( MTrue(1,2), data[4], small_tol, close_tol );
      SANS_CHECK_CLOSE( MTrue(2,2), data[5], small_tol, close_tol );
    }

    //Delete the written file
    std::remove(mesh_filename.c_str());
    std::remove(sol_filename.c_str());
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
