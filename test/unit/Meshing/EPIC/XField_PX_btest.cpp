// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include "tools/linspace.h"

#include "Field/XFieldArea.h"
#include "Meshing/EPIC/XField_PX.h"

#include "unit/Field/XField2D_CheckTraceCoord2D_btest.h"
#include "unit/Field/XField3D_CheckTraceCoord3D_btest.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Annulus_Quad_X1.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "Field/output_grm.h"
#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XField_PX_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WriteMesh_Grm_Triangle )
{
  mpi::communicator world;

  XField2D_Box_Triangle_Lagrange_X1 xfld_X1(world, 4,4);

  XField<PhysD2,TopoD2> xfld_X2(xfld_X1,2,BasisFunctionCategory_Lagrange); //Construct Q2 mesh from linear mesh
  XField<PhysD2,TopoD2> xfld_X3(xfld_X1,3,BasisFunctionCategory_Lagrange); //Construct Q3 mesh from linear mesh
  XField<PhysD2,TopoD2> xfld_X4(xfld_X1,4,BasisFunctionCategory_Lagrange); //Construct Q4 mesh from linear mesh

  //Test ASCII files
  std::string filename_X1  = "IO/mesh_X1.grm";
  std::string filename_X1r = "IO/mesh_X1_rewrite.grm";

  std::string filename_X2  = "IO/mesh_X2.grm";
  std::string filename_X2r = "IO/mesh_X2_rewrite.grm";

  std::string filename_X3  = "IO/mesh_X3.grm";
  std::string filename_X3r = "IO/mesh_X3_rewrite.grm";

  std::string filename_X4  = "IO/mesh_X4.grm";
  std::string filename_X4r = "IO/mesh_X4_rewrite.grm";

  WriteMeshGrm(xfld_X1,filename_X1);
  WriteMeshGrm(xfld_X2,filename_X2);
  WriteMeshGrm(xfld_X3,filename_X3);
  WriteMeshGrm(xfld_X4,filename_X4);

  //Read in the written mesh and re-write to a different file
  XField_PX<PhysD2, TopoD2> xfld_readin_X1(world, filename_X1);
  XField_PX<PhysD2, TopoD2> xfld_readin_X2(world, filename_X2);
  XField_PX<PhysD2, TopoD2> xfld_readin_X3(world, filename_X3);
  XField_PX<PhysD2, TopoD2> xfld_readin_X4(world, filename_X4);

  WriteMeshGrm(xfld_readin_X1,filename_X1r);
  WriteMeshGrm(xfld_readin_X2,filename_X2r);
  WriteMeshGrm(xfld_readin_X3,filename_X3r);
  WriteMeshGrm(xfld_readin_X4,filename_X4r);

  // only rank 0 will check and remove the files
  if (world.rank() == 0)
  {
    mapped_file_source f_X1 (filename_X1 );
    mapped_file_source f_X1r(filename_X1r);

    mapped_file_source f_X2 (filename_X2 );
    mapped_file_source f_X2r(filename_X2r);

    mapped_file_source f_X3 (filename_X3 );
    mapped_file_source f_X3r(filename_X3r);

    mapped_file_source f_X4 (filename_X4 );
    mapped_file_source f_X4r(filename_X4r);

    //Check if the contents of all the files are equal
    BOOST_CHECK( f_X1.size() == f_X1r.size() && std::equal(f_X1.data(), f_X1.data() + f_X1.size(), f_X1r.data()) );
    BOOST_CHECK( f_X2.size() == f_X2r.size() && std::equal(f_X2.data(), f_X2.data() + f_X2.size(), f_X2r.data()) );
    BOOST_CHECK( f_X3.size() == f_X3r.size() && std::equal(f_X3.data(), f_X3.data() + f_X3.size(), f_X3r.data()) );
    BOOST_CHECK( f_X4.size() == f_X4r.size() && std::equal(f_X4.data(), f_X4.data() + f_X4.size(), f_X4r.data()) );

    //Delete the written files
    std::remove(filename_X1.c_str());
    std::remove(filename_X1r.c_str());

    std::remove(filename_X2.c_str());
    std::remove(filename_X2r.c_str());

    std::remove(filename_X3.c_str());
    std::remove(filename_X3r.c_str());

    std::remove(filename_X4.c_str());
    std::remove(filename_X4r.c_str());
  }
}

#if 0 // Missing unit tests for LagrangeDOFMap<Quad>::getTracesCanonicalNodes
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WriteMesh_Grm_Annulus_Quad )
{
  const int ii = 10;
  const int jj = 10;
  const Real R1 = 1.0;         // R_1: inner radius
  const Real h = 1.0;          // thickness. Must be 1
  const Real thetamin = 0.0;   // degree angle
  const Real thetamax = 135.0;  // degree angle

  XField2D_Annulus_Quad_X1 xfld_X1( ii, jj, R1, h, thetamin, thetamax );

  //Test ASCII files
  std::string filename1 = "IO/mesh_X1.grm";
  std::string filename2 = "IO/mesh_X1_rewrite.grm";

  WriteMeshGrm(xfld_X1,filename1);

  //Read in the written mesh and re-write to a different file
  XField_PX<PhysD2, TopoD2> xfld_readin(world,filename1);
  WriteMeshGrm(xfld_readin,filename2);

  mapped_file_source f1(filename1);
  mapped_file_source f2(filename2);

  //Check if the contents of all the files are equal
  BOOST_CHECK( f1.size() == f2.size() && std::equal(f1.data(), f1.data() + f1.size(), f2.data()) );

  //Delete the written files
  std::remove(filename1.c_str());
  std::remove(filename2.c_str());
}
#endif


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WriteMesh_Grm_Tet )
{
  // global communicator
  mpi::communicator world;

  XField3D_Box_Tet_X1   xfld_X1(world, 2,2,2);
  XField<PhysD3,TopoD3> xfld_X2(xfld_X1,2,BasisFunctionCategory_Lagrange); //Construct Q2 mesh from linear mesh
  XField<PhysD3,TopoD3> xfld_X3(xfld_X1,3,BasisFunctionCategory_Lagrange); //Construct Q3 mesh from linear mesh
  XField<PhysD3,TopoD3> xfld_X4(xfld_X1,4,BasisFunctionCategory_Lagrange); //Construct Q4 mesh from linear mesh

  //Test ASCII files
  std::string filename_X1  = "IO/mesh_X1.grm";
  std::string filename_X1r = "IO/mesh_X1_rewrite.grm";

  std::string filename_X2  = "IO/mesh_X2.grm";
  std::string filename_X2r = "IO/mesh_X2_rewrite.grm";

  std::string filename_X3  = "IO/mesh_X3.grm";
  std::string filename_X3r = "IO/mesh_X3_rewrite.grm";

  std::string filename_X4  = "IO/mesh_X4.grm";
  std::string filename_X4r = "IO/mesh_X4_rewrite.grm";

  WriteMeshGrm(xfld_X1,filename_X1);
  WriteMeshGrm(xfld_X2,filename_X2);
  WriteMeshGrm(xfld_X3,filename_X3);
  WriteMeshGrm(xfld_X4,filename_X4);

  //Read in the written mesh and re-write to a different file
  XField_PX<PhysD3, TopoD3> xfld_readin_X1(world, filename_X1);
  XField_PX<PhysD3, TopoD3> xfld_readin_X2(world, filename_X2);
  XField_PX<PhysD3, TopoD3> xfld_readin_X3(world, filename_X3);
  XField_PX<PhysD3, TopoD3> xfld_readin_X4(world, filename_X4);

  WriteMeshGrm(xfld_readin_X1,filename_X1r);
  WriteMeshGrm(xfld_readin_X2,filename_X2r);
  WriteMeshGrm(xfld_readin_X3,filename_X3r);
  WriteMeshGrm(xfld_readin_X4,filename_X4r);

  // only rank 0 will check and remove the files
  if (world.rank() == 0)
  {
    mapped_file_source f_X1 (filename_X1 );
    mapped_file_source f_X1r(filename_X1r);

    mapped_file_source f_X2 (filename_X2 );
    mapped_file_source f_X2r(filename_X2r);

    mapped_file_source f_X3 (filename_X3 );
    mapped_file_source f_X3r(filename_X3r);

    mapped_file_source f_X4 (filename_X4 );
    mapped_file_source f_X4r(filename_X4r);

    //Check if the contents of all the files are equal
    BOOST_CHECK( f_X1.size() == f_X1r.size() && std::equal(f_X1.data(), f_X1.data() + f_X1.size(), f_X1r.data()) );
    BOOST_CHECK( f_X2.size() == f_X2r.size() && std::equal(f_X2.data(), f_X2.data() + f_X2.size(), f_X2r.data()) );
    BOOST_CHECK( f_X3.size() == f_X3r.size() && std::equal(f_X3.data(), f_X3.data() + f_X3.size(), f_X3r.data()) );
    BOOST_CHECK( f_X4.size() == f_X4r.size() && std::equal(f_X4.data(), f_X4.data() + f_X4.size(), f_X4r.data()) );

    //Delete the written files
    std::remove(filename_X1.c_str());
    std::remove(filename_X1r.c_str());

    std::remove(filename_X2.c_str());
    std::remove(filename_X2r.c_str());

    std::remove(filename_X3.c_str());
    std::remove(filename_X3r.c_str());

    std::remove(filename_X4.c_str());
    std::remove(filename_X4r.c_str());
  }
}

#ifndef SANS_MPI
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_4Triangle_grm )
{
  XField2D_4Triangle_X1_1Group xfld_orig;

  //Test ASCII files
  std::string filename1 = "IO/mesh.grm";

  output_grm(xfld_orig,filename1);

  mpi::communicator world;

  XField_PX<PhysD2, TopoD2> xfld(world, filename1);

  //typedef std::array<int,3> Int3;

  BOOST_REQUIRE_EQUAL( xfld.nDOF(), 6 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], -1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], -1 );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);

  int nodeMap[3];

  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  xfldArea.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );

  xfldArea.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  // interior-edge field variable

  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>& xfldBedge = xfld.getBoundaryTraceGroup<Line>(0);

  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );

  xfldBedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldBedge.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBedge.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldBedge.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldBedge.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  BOOST_CHECK_EQUAL( xfldBedge.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(0), 3 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(0), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(0).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(1), 3 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(1), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(1).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(2), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(2), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(2).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(2).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(3), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(3), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(3).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(3).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(4), 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(4), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(4).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(4).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(5), 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(5), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(5).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(5).trace, -1 );


  //Test ASCII files
  std::string filename2 = "IO/mesh2.grm";

  output_grm(xfld,filename2);

  mapped_file_source f1(filename1);
  mapped_file_source f2(filename2);

  //The contents of both exported mesh files should be equal
  BOOST_CHECK( f1.size() == f2.size() && std::equal(f1.data(), f1.data() + f1.size(), f2.data()) );

  //Delete the written files
  std::remove(filename1.c_str());
  std::remove(filename2.c_str());
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_rae2822_q2_grm )
{
  mpi::communicator world;

  XField_PX<PhysD2,TopoD2> xfld(world, "IO/Adaptation/rae2822_q2.grm");

  BOOST_CHECK_EQUAL( 2, xfld.nCellGroups() );
  BOOST_CHECK_EQUAL( 3, xfld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 5, xfld.nBoundaryTraceGroups() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // Check that interior and boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  // Write and read the grid again
  std::string testfile = "IO/test.grm";
  WriteMeshGrm(xfld, testfile);

  XField_PX<PhysD2,TopoD2> xfld2(world, testfile);

  // Check that interior and boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld2.nInteriorTraceGroups()-1) ), xfld2, xfld2 );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld2.nBoundaryTraceGroups()-1) ), xfld2, xfld2 );

  //Delete the written file
  std::remove(testfile.c_str());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_rae2822_q3_gri )
{
  mpi::communicator world;

  XField_PX<PhysD2,TopoD2> xfld(world, "IO/Adaptation/rae2822_q3.gri");

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  BOOST_CHECK_EQUAL( 2, xfld.nCellGroups() );
  BOOST_CHECK_EQUAL( 3, xfld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 5, xfld.nBoundaryTraceGroups() );

  // Check that interior and boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  // Write and read the grid again
  std::string testfile = "IO/test.grm";
  WriteMeshGrm(xfld, testfile);

  XField_PX<PhysD2,TopoD2> xfld2(world, testfile);

  // Check that interior and boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld2.nInteriorTraceGroups()-1) ), xfld2, xfld2 );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld2.nBoundaryTraceGroups()-1) ), xfld2, xfld2 );

  //Delete the written file
  std::remove(testfile.c_str());
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_box_q2_grm )
{
  mpi::communicator world;

  XField_PX<PhysD2,TopoD2> xfld(world, "IO/Adaptation/box_q2.grm");

  BOOST_CHECK_EQUAL( 3, xfld.nCellGroups() );
#ifndef SANS_MPI //Interior traces do not have the same count across processors
  BOOST_CHECK_EQUAL( 6, xfld.nInteriorTraceGroups() );
#endif
  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // Check that interior and boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  // Write and read the grid again
  std::string testfile = "IO/test.grm";
  WriteMeshGrm(xfld, testfile);

  XField_PX<PhysD2,TopoD2> xfld2(world, testfile);

  // Check that interior and boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld2.nInteriorTraceGroups()-1) ), xfld2, xfld2 );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld2.nBoundaryTraceGroups()-1) ), xfld2, xfld2 );

  //Delete the written file
  std::remove(testfile.c_str());
}

#ifndef SANS_MPI
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_6Tet_grm)
{
  mpi::communicator world;

  XField3D_6Tet_X1_1Group xfld_orig;

  //Test ASCII files
  std::string filename1 = "IO/mesh.grm";
  WriteMeshGrm(xfld_orig, filename1);

  XField_PX<PhysD3, TopoD3> xfld(world, filename1);

  //Note that the node ordering of the reconstructed mesh is different to that of XField3D_6Tet_X1_1Group
  BOOST_CHECK_EQUAL( xfld.nDOF(), 8 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[2], 1 );

  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  const XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>& xfldVol = xfld.getCellGroup<Tet>(0);

  int nodeMap[4];
  int elemL;
  int faceL;

  // Index table for each tet in the hex
  const int tets[6][4] = { {0, 1, 2, 4},
                           {1, 4, 3, 2},
                           {6, 2, 3, 4},

                           {1, 4, 5, 3},
                           {4, 6, 5, 3},
                           {7, 6, 3, 5} };

  // Left prism
  xfldVol.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[0][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[0][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[0][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[0][3] );

  xfldVol.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[1][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[1][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[1][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[1][3] );

  xfldVol.associativity(2).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[2][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[2][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[2][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[2][3] );

  // Right prism
  xfldVol.associativity(3).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[3][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[3][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[3][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[3][3] );

  xfldVol.associativity(4).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[4][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[4][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[4][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[4][3] );

  xfldVol.associativity(5).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], tets[5][0] );
  BOOST_CHECK_EQUAL( nodeMap[1], tets[5][1] );
  BOOST_CHECK_EQUAL( nodeMap[2], tets[5][2] );
  BOOST_CHECK_EQUAL( nodeMap[3], tets[5][3] );


  // interior-face field variable
  BOOST_REQUIRE_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldIface = xfld.getInteriorTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldIface.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIface.getGroupRight(), 0 );

  // interior face-to-cell connectivity

  // Interior trace order is highly dependent on the trace connection algorithm
  // Rely on checkGrid to make sure interior traces are correct

  // boundary-face field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3, TopoD3>::FieldTraceGroupType<Triangle>& xfldBface = xfld.getBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldBface.getGroupLeft(), 0 );

  int faceB = 0;

  elemL = 0; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 0; faceL = 2;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 0; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 1; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 2; faceL = 2;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 2; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 3; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 3; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 4; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;


  elemL = 5; faceL = 1;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 5; faceL = 2;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  elemL = 5; faceL = 3;

  xfldBface.associativity(faceB).getNodeGlobalMapping( nodeMap, 3 );
  for (int i = 0; i < 3; i++)
    BOOST_CHECK_EQUAL( nodeMap[i], (tets[elemL][TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[faceL][i]]) );

  BOOST_CHECK_EQUAL( xfldBface.getElementLeft(faceB), elemL );
  BOOST_CHECK_EQUAL( xfldBface.getCanonicalTraceLeft(faceB).trace, faceL );

  BOOST_CHECK_EQUAL( xfldVol.associativity(elemL).faceSign()[faceL], +1 );
  faceB++;

  BOOST_CHECK_EQUAL( 12, faceB );


  //Test ASCII files
  std::string filename2 = "IO/mesh2.grm";

  WriteMeshGrm(xfld, filename2); //Export the reconstructed mesh

  mapped_file_source f1(filename1);
  mapped_file_source f2(filename2);

  //The contents of both exported mesh files should be equal
  BOOST_CHECK( f1.size() == f2.size() && std::equal(f1.data(), f1.data() + f1.size(), f2.data()) );

  //Delete the written files
  std::remove(filename1.c_str());
  std::remove(filename2.c_str());
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
