// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MesherInterface_Epic_btest
// testing of the MesherInterface class for Epic

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <cstdio>
#include <iostream>
#include <fstream>

#include "Meshing/EPIC/MesherInterface_Epic.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template class MesherInterface<PhysD2, TopoD2, Epic>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MesherInterface_Epic_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_Epic_Default_Parameter_Test )
{
  PyDict d;
  EpicParams::checkInputs(d);

  BOOST_CHECK_EQUAL( d.get(EpicParams::params.Version), "madcap_devel" );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.FilenameBase), "tmp/" );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.SaveInputs), false );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.nAdaptIter), 25 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.Shell), "None" );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.BoundaryObject), "None" );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.CSF), "" );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.GeometryList2D), "" );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.GeometryFile3D), "" );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.SymmetricSurf).empty(), true );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.DisabledSurf).empty(), true );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.SlipSurf).empty(), true );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.ViscSurf).empty(), true );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.minH), -1 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.maxH), -1 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.minGeom), -1 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.maxGeom), -1 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.nPointGeom), 24 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.FoldedFaceAngle), 70.0 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.CornerEdgeAngle), 70.0);
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.NormSize), -1 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.CurvedQOrder), 2 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.ProjectionMethod), EpicParams::params.ProjectionMethod.Elastic );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.CurvingMethod), EpicParams::params.CurvingMethod.Distortion );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.CurvingGeometry), EpicParams::params.CurvingGeometry.CADGeometry );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.Reconnection), true );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.NodeMovement), true );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.BoundaryNodeMovement), true );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.CellQualityRatio),  1 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.CellInitialQualityTol), -1.0 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.FaceQualityRatio), -1 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.GrowthLimit), 0 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.nGrowthIter), 0 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.nBLayer), 0 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.BLayerGrowth), 1.0 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.BLayerAnisoCutOff), 5.0 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.RemoveTrapped), true );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.MetricNorm), EpicParams::params.MetricNorm.L2 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.QualityMode), EpicParams::params.QualityMode.Step );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.QualityCombineMode), EpicParams::params.QualityCombineMode.ObjFunc );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.BackgroundMesh), true );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.MaxAnisotropy), 1e6 );
  BOOST_CHECK_EQUAL( d.get(EpicParams::params.nThread), 1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_Epic_Script2D_NoGeom_Test )
{
  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  std::string filenamebase = "IO/Adaptation/";

  PyDict d;
  d[EpicParams::params.DisableCall] = true;
  d[EpicParams::params.FilenameBase] = filenamebase;
  EpicParams::checkInputs(d);

  mpi::communicator world;

  XField2D_Box_Triangle_Lagrange_X1 xfld(world,20,20);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 100*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD2, TopoD2, Epic> mesher(iter, d);

  mesher.adapt(metric_request);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if the script was correctly generated
    std::string filename_script = filenamebase + "/epic_script.com";
    std::string filename_example = "IO/Adaptation/epic_script_NoGeom_2D.com";

    mapped_file_source f1b(filename_script);
    mapped_file_source f2b(filename_example);

    //Check if the contents of the new script and the example are equal
    BOOST_CHECK( f1b.size() == f2b.size() && std::equal(f1b.data(), f1b.data() + f1b.size(), f2b.data()) );

    //Check if mesh and metric request were exported
    std::string filename_mesh = filenamebase + "mesh_a0_in.grm";
    std::string filename_metric = filenamebase + "metric_request_a0.bnmetric";
    BOOST_REQUIRE( std::ifstream(filename_mesh).good() == true );
    BOOST_REQUIRE( std::ifstream(filename_metric).good() == true );

    //Delete the written files
    std::remove(filename_script.c_str());
    std::remove(filename_mesh.c_str());
    std::remove(filename_metric.c_str());
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_Epic_Script2D_GeomQ1_Test )
{
  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  mpi::communicator world;

  std::string filenamebase = "IO/Adaptation/";

  std::string CSF = "caseGeom.csf";
  std::string Geometry = "case.igs";

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //create the dummy surface and geometry files
    std::ofstream CSFfile(CSF);           CSFfile << "Some surface stuff";
    std::ofstream Geometryfile(Geometry); Geometryfile << "Some geometry stuff";
  }
  // make sure the file is created before continuing
  world.barrier();

  PyDict d;
  d[EpicParams::params.DisableCall] = true;
  d[EpicParams::params.CSF] = CSF;
  d[EpicParams::params.GeometryList2D] = Geometry;
  d[EpicParams::params.nBLayer] = 2;
  d[EpicParams::params.FilenameBase] = filenamebase;
  d[EpicParams::params.SymmetricSurf] = {0,1};
  d[EpicParams::params.SlipSurf] = {2,3};
  d[EpicParams::params.ViscSurf] = {4,5,6};
  d[EpicParams::params.CurvedQOrder] = 1;
  EpicParams::checkInputs(d);

  // make sure all processors get to check that file files exist
  world.barrier();

  XField2D_Box_Triangle_Lagrange_X1 xfld(world,20,20);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 100*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD2, TopoD2, Epic> mesher(iter, d);

  mesher.adapt(metric_request);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if the script was correctly generated
    std::string filename_script = filenamebase + "epic_script.com";
    std::string filename_example = "IO/Adaptation/epic_script_GeomQ1_2D.com";

    mapped_file_source f1b(filename_script);
    mapped_file_source f2b(filename_example);

    //Check if the contents of the new script and the example are equal
    BOOST_CHECK( f1b.size() == f2b.size() && std::equal(f1b.data(), f1b.data() + f1b.size(), f2b.data()) );

    //Check if mesh and metric request were exported
    std::string filename_mesh = filenamebase + "mesh_a0_in.grm";
    std::string filename_metric = filenamebase + "metric_request_a0.bnmetric";
    BOOST_REQUIRE( std::ifstream(filename_mesh).good() == true );
    BOOST_REQUIRE( std::ifstream(filename_metric).good() == true );

    //Delete the written files
    std::remove(filename_script.c_str());
    std::remove(filename_mesh.c_str());
    std::remove(filename_metric.c_str());
    std::remove(CSF.c_str());
    std::remove(Geometry.c_str());
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_Epic_Script2D_GeomQ2_Test )
{
  typedef DLA::MatrixSymS<2, Real> MatrixSym;

  mpi::communicator world;

  std::string filenamebase = "IO/Adaptation/";

  std::string CSF = "caseGeom.csf";
  std::string Geometry = "case.igs";

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //create the dummy surface and geometry files
    std::ofstream CSFfile(CSF);           CSFfile << "Some surface stuff";
    std::ofstream Geometryfile(Geometry); Geometryfile << "Some geometry stuff";
  }
  // make sure the file is created before continuing
  world.barrier();

  PyDict d;
  d[EpicParams::params.DisableCall] = true;
  d[EpicParams::params.CSF] = CSF;
  d[EpicParams::params.GeometryList2D] = Geometry;
  d[EpicParams::params.nBLayer] = 2;
  d[EpicParams::params.FilenameBase] = filenamebase;
  d[EpicParams::params.SymmetricSurf] = {0,1};
  d[EpicParams::params.SlipSurf] = {2,3};
  d[EpicParams::params.ViscSurf] = {4,5,6};
  d[EpicParams::params.CurvedQOrder] = 2;
  EpicParams::checkInputs(d);

  // make sure all processors get to check that file files exist
  world.barrier();

  XField2D_Box_Triangle_Lagrange_X1 xfld(world,20,20);

  Field_CG_Cell<PhysD2, TopoD2, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 100*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD2, TopoD2, Epic> mesher(iter, d);

  mesher.adapt(metric_request);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if the script was correctly generated
    std::string filename_script = filenamebase + "epic_script.com";
    std::string filename_example = "IO/Adaptation/epic_script_GeomQ2_2D.com";

    mapped_file_source f1b(filename_script);
    mapped_file_source f2b(filename_example);

    //Check if the contents of the new script and the example are equal
    BOOST_CHECK( f1b.size() == f2b.size() && std::equal(f1b.data(), f1b.data() + f1b.size(), f2b.data()) );

    //Check if mesh and metric request were exported
    std::string filename_mesh = filenamebase + "mesh_a0_in.grm";
    std::string filename_metric = filenamebase + "metric_request_a0.bnmetric";
    BOOST_REQUIRE( std::ifstream(filename_mesh).good() == true );
    BOOST_REQUIRE( std::ifstream(filename_metric).good() == true );

    //Delete the written files
    std::remove(filename_script.c_str());
    std::remove(filename_mesh.c_str());
    std::remove(filename_metric.c_str());
    std::remove(CSF.c_str());
    std::remove(Geometry.c_str());
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_Epic_Script3D_NoGeom_Test )
{
  typedef DLA::MatrixSymS<3, Real> MatrixSym;

  std::string filenamebase = "IO/Adaptation/";

  PyDict d;
  d[EpicParams::params.DisableCall] = true;
  d[EpicParams::params.FilenameBase] = filenamebase;
  EpicParams::checkInputs(d);

  mpi::communicator world;

  XField3D_Box_Tet_X1 xfld(world,5,5,5);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 100*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD3, TopoD3, Epic> mesher(iter, d);

  mesher.adapt(metric_request);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if the script was correctly generated
    std::string filename_script = filenamebase + "epic_script.com";
    std::string filename_example = "IO/Adaptation/epic_script_NoGeom_3D.com";

    mapped_file_source f1b(filename_script);
    mapped_file_source f2b(filename_example);

    //Check if the contents of the new script and the example are equal
    BOOST_CHECK( f1b.size() == f2b.size() && std::equal(f1b.data(), f1b.data() + f1b.size(), f2b.data()) );

    //Check if mesh and metric request were exported
    std::string filename_mesh = filenamebase + "mesh_a0_in.grm";
    std::string filename_metric = filenamebase + "metric_request_a0.bnmetric";
    BOOST_REQUIRE( std::ifstream(filename_mesh).good() == true );
    BOOST_REQUIRE( std::ifstream(filename_metric).good() == true );

    //Delete the written files
    std::remove(filename_script.c_str());
    std::remove(filename_mesh.c_str());
    std::remove(filename_metric.c_str());
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_Epic_Script3D_GeomQ1_Test )
{
  typedef DLA::MatrixSymS<3, Real> MatrixSym;

  mpi::communicator world;

  std::string filenamebase = "IO/Adaptation/";

  std::string CSF = "caseGeom.csf";
  std::string Geometry = "case.igs";

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //create the dummy surface and geometry files
    std::ofstream CSFfile(CSF);           CSFfile << "Some surface stuff";
    std::ofstream Geometryfile(Geometry); Geometryfile << "Some geometry stuff";
  }
  // make sure the file is created before continuing
  world.barrier();

  PyDict d;
  d[EpicParams::params.DisableCall] = true;
  d[EpicParams::params.CSF] = CSF;
  d[EpicParams::params.Shell] = "Shell";
  d[EpicParams::params.BoundaryObject] = "BoundaryObject";
  d[EpicParams::params.GeometryFile3D] = Geometry;
  d[EpicParams::params.nBLayer] = 2;
  d[EpicParams::params.FilenameBase] = filenamebase;
  d[EpicParams::params.SymmetricSurf] = {0,1};
  d[EpicParams::params.SlipSurf] = {2,3};
  d[EpicParams::params.ViscSurf] = {4,5,6};
  d[EpicParams::params.CurvedQOrder] = 1;
  EpicParams::checkInputs(d);

  // make sure all processors get to check that file files exist
  world.barrier();

  XField3D_Box_Tet_X1 xfld(world,5,5,5);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 100*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD3, TopoD3, Epic> mesher(iter, d);

  mesher.adapt(metric_request);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if the script was correctly generated
    std::string filename_script = filenamebase + "epic_script.com";
    std::string filename_example = "IO/Adaptation/epic_script_GeomQ1_3D.com";

    mapped_file_source f1b(filename_script);
    mapped_file_source f2b(filename_example);

    //Check if the contents of the new script and the example are equal
    BOOST_CHECK( f1b.size() == f2b.size() && std::equal(f1b.data(), f1b.data() + f1b.size(), f2b.data()) );

    //Check if mesh and metric request were exported
    std::string filename_mesh = filenamebase + "mesh_a0_in.grm";
    std::string filename_metric = filenamebase + "metric_request_a0.bnmetric";
    BOOST_REQUIRE( std::ifstream(filename_mesh).good() == true );
    BOOST_REQUIRE( std::ifstream(filename_metric).good() == true );

    //Delete the written files
    std::remove(filename_script.c_str());
    std::remove(filename_mesh.c_str());
    std::remove(filename_metric.c_str());
    std::remove(CSF.c_str());
    std::remove(Geometry.c_str());
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_Epic_Script3D_GeomQ2_Test )
{
  typedef DLA::MatrixSymS<3, Real> MatrixSym;

  mpi::communicator world;

  std::string filenamebase = "IO/Adaptation/";

  std::string CSF = "caseGeom.csf";
  std::string Geometry = "case.igs";

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //create the dummy surface and geometry files
    std::ofstream CSFfile(CSF);           CSFfile << "Some surface stuff";
    std::ofstream Geometryfile(Geometry); Geometryfile << "Some geometry stuff";
  }
  // make sure the file is created before continuing
  world.barrier();

  PyDict d;
  d[EpicParams::params.DisableCall] = true;
  d[EpicParams::params.CSF] = CSF;
  d[EpicParams::params.Shell] = "Shell";
  d[EpicParams::params.BoundaryObject] = "BoundaryObject";
  d[EpicParams::params.GeometryFile3D] = Geometry;
  d[EpicParams::params.nBLayer] = 2;
  d[EpicParams::params.FilenameBase] = filenamebase;
  d[EpicParams::params.SymmetricSurf] = {0,1};
  d[EpicParams::params.SlipSurf] = {2,3};
  d[EpicParams::params.ViscSurf] = {4,5,6};
  d[EpicParams::params.CurvedQOrder] = 2;
  EpicParams::checkInputs(d);

  // make sure all processors get to check that file files exist
  world.barrier();

  XField3D_Box_Tet_X1 xfld(world,5,5,5);

  Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(xfld, 1, BasisFunctionCategory_Hierarchical);
  MatrixSym M0 = DLA::Identity();
  M0 = 100*M0;
  metric_request = M0;

  int iter = 0;
  MesherInterface<PhysD3, TopoD3, Epic> mesher(iter, d);

  mesher.adapt(metric_request);

  // only rank 0 does file IO
  if (world.rank() == 0)
  {
    //Check if the script was correctly generated
    std::string filename_script = filenamebase + "epic_script.com";
    std::string filename_example = "IO/Adaptation/epic_script_GeomQ2_3D.com";

    mapped_file_source f1b(filename_script);
    mapped_file_source f2b(filename_example);

    //Check if the contents of the new script and the example are equal
    BOOST_CHECK( f1b.size() == f2b.size() && std::equal(f1b.data(), f1b.data() + f1b.size(), f2b.data()) );

    //Check if mesh and metric request were exported
    std::string filename_mesh = filenamebase + "mesh_a0_in.grm";
    std::string filename_metric = filenamebase + "metric_request_a0.bnmetric";
    BOOST_REQUIRE( std::ifstream(filename_mesh).good() == true );
    BOOST_REQUIRE( std::ifstream(filename_metric).good() == true );

    //Delete the written files
    std::remove(filename_script.c_str());
    std::remove(filename_mesh.c_str());
    std::remove(filename_metric.c_str());
    std::remove(CSF.c_str());
    std::remove(Geometry.c_str());
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
