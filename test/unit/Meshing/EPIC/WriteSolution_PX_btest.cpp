// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// WriteSolution_libMeshb_btest
// testing of the WriteSolution_libMeshb function

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <cstdio>
#include <iostream>

#include "../../../../src/Meshing/EPIC/SolutionField_PX.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( WriteSolution_PX_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Write_2D_Vector_P1 )
{
  Real tol = 1e-10;
  typedef DLA::VectorS<2,Real> ArrayQ;

  mpi::communicator world;

  int ii = 4, jj = 5;
  XField2D_Box_Triangle_Lagrange_X1 xfld(world,ii,jj);

  {
    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, 1, BasisFunctionCategory_Hierarchical);
    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld2(xfld, 1, BasisFunctionCategory_Hierarchical);

    // set the field based on the global native node numbering
    ArrayQ q0 = { 0.2, 0.3};
    for (int n = 0; n < qfld.nDOF(); n++)
      qfld.DOF(n) = q0*(qfld.local2nativeDOFmap(n)+1);

    //Test CG mesh
    std::string sol_filename = "IO/testWrite_CG_PX.ssol";

    if (world.rank() == 0)
    {
      std::cout << "writing...";
    }
    WriteSolution_PX(qfld, sol_filename);
    if (world.rank() == 0)
    {
      std::cout << "done\n";
    }


    if (world.rank() == 0)
    {
      std::cout << "reading...";
    }
    ReadSolution_PX(sol_filename, qfld2);

    if (world.rank() == 0)
    {
      std::cout << "done\n";
    }

    for (int n = 0; n < qfld.nDOF(); n++)
    {
      SANS_CHECK_CLOSE( qfld.DOF(n)[0], qfld2.DOF(n)[0], tol, tol );
      SANS_CHECK_CLOSE( qfld.DOF(n)[1], qfld2.DOF(n)[1], tol, tol );
    }

    std::remove(sol_filename.c_str());
  }

  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, 1, BasisFunctionCategory_Hierarchical);
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld2(xfld, 1, BasisFunctionCategory_Hierarchical);

    // set the field based on the global native node numbering
    ArrayQ q0 = { 0.2, 0.3};
    for (int n = 0; n < qfld.nDOF(); n++)
      qfld.DOF(n) = q0*(qfld.local2nativeDOFmap(n)+1);

    //Test CG mesh
    std::string sol_filename = "IO/testWrite_HODG_PX.ssol";

    WriteSolution_PX(qfld, sol_filename);
    ReadSolution_PX(sol_filename, qfld2);

    for (int n = 0; n < qfld.nDOF(); n++)
    {
      SANS_CHECK_CLOSE( qfld.DOF(n)[0], qfld2.DOF(n)[0], tol, tol );
      SANS_CHECK_CLOSE( qfld.DOF(n)[1], qfld2.DOF(n)[1], tol, tol );
    }

    std::remove(sol_filename.c_str());
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
