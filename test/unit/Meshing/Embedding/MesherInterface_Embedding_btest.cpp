// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// MesherInterface_FeFloa_btest
// testing of the MesherInterface class for FeFloa

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/iostreams/device/mapped_file.hpp>
using boost::iostreams::mapped_file_source;

#include <cstdio>
#include <iostream>
#include <fstream>

#include "Meshing/Embedding/MesherInterface_Embedding.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_CG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;


//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template class MesherInterface<PhysD1, TopoD1, EmbeddingMesher>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( MesherInterface_Embedding_test_suite )

void
createMetric( const int npt , const XField1D& xfld ,
              std::vector<DLA::MatrixSymS<1,Real>>& metrics )
{
  metrics.resize(npt);

  for (int i=0;i<npt;i++)
  {
    DLA::MatrixSymS<1,Real> m;
    m(0,0) = 100;
    metrics[i] = m;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( MesherInterface_Embedding_1D_Test_Uniform )
{
  Real tol = 1e-12;

  int nElements = 10;
  Real xL = -0.22669;
  Real xR = 3.0734;
  XField1D xfld( nElements, xL, xR );

  Field_CG_Cell<PhysD1,TopoD1,DLA::MatrixSymS<1,Real>>
    metric_request(xfld,1,BasisFunctionCategory_Hierarchical);

  int n = xfld.nDOF();

  std::vector<DLA::MatrixSymS<1,Real>> metrics;
  createMetric( n , xfld , metrics );

  // save the metric into the CG field
  for (int i=0;i<n;i++)
    metric_request.DOF(i) = metrics[i];

  // create the mesher
  PyDict dict;
  MesherInterface<PhysD1,TopoD1,EmbeddingMesher> mesher( 1 , dict );

  // generate the addapted mesh
  std::shared_ptr< XField<PhysD1,TopoD1> >
    xfld2 = mesher.adapt( metric_request );

  BOOST_CHECK_EQUAL( xfld2->nDOF() , 34 );
  BOOST_CHECK_CLOSE( xfld2->DOF(0)[0] , xL , tol );
  BOOST_CHECK_CLOSE( xfld2->DOF(33)[0] , xR , tol );
}

Real
meshSize( const Real s )
{
  return s*(1. -s) +.01;
}

BOOST_AUTO_TEST_CASE( MesherInterface_Embedding_1D_Test_NonUniform )
{
  // initial requested mesh spacing
  // we need to do this first to get an estimate of the domain size
  std::vector<Real> h;
  Real s = 0.;
  Real ds = 0.1;
  Real hi, L = 0.;
  while (s < 1)
  {
    hi = meshSize(s);
    h.push_back(hi);
    L  += hi;
    s  += ds;
  }

  int nElements = h.size();
  Real xL = -.5;
  Real xR = xL +L;

  printf("domain = [%g,%g]\n",xL,xR);

  // initial uniform mesh
  std::shared_ptr< XField<PhysD1,TopoD1> >
    xfld = std::make_shared< XField1D >( nElements , xL , xR );

  // create the mesher
  PyDict dict;
  MesherInterface<PhysD1,TopoD1,EmbeddingMesher> mesher( 1 , dict );

  // adapt the mesh to conform to the metric
  for (int iter=0;iter<20;iter++)
  {
    // metric request
    Field_CG_Cell<PhysD1,TopoD1,DLA::MatrixSymS<1,Real>>
      metric_request(*xfld,1,BasisFunctionCategory_Hierarchical);

    nElements = xfld->nDOF() -1;

    // create the metrics
    for (int i=0;i<nElements+1;i++)
    {
      DLA::MatrixSymS<1,Real> m;

      // get the parameter value and evaluate mesh size
      Real s = (xfld->DOF(i)[0] -xL)/(xR -xL);
      hi = meshSize( s );

      // create the metric
      m(0,0) = 1./(hi*hi);
      metric_request.DOF(i) = m;
    }

    std::shared_ptr< XField<PhysD1,TopoD1> >
      xfld2 = mesher.adapt( metric_request );

    // set the new mesh
    xfld = xfld2;
  }

#if 0
  // Analytic 1D edge length integrals
  std::vector<Real> xtruth = {
    -5.000000000000e-01,
    -4.866810130670e-01,
    -4.634767936437e-01,
    -4.236479021986e-01,
    -3.570083497301e-01,
    -2.501774563640e-01,
    -9.025928547251e-02,
     1.258009182158e-01,
     3.800000000000e-01,
     6.341990817842e-01,
     8.502592854725e-01,
     1.010177456364e+00,
     1.117008349730e+00,
     1.183647902199e+00,
     1.223476793644e+00,
     1.246681013067e+00,
     1.260000000000e+00 };
#else
  // Gauss quadrature
  std::vector<Real> xtruth = {
    -5.000000000000e-01,
    -4.866808226734e-01,
    -4.634761832397e-01,
    -4.236465216955e-01,
    -3.570058386798e-01,
    -2.501738139024e-01,
    -9.025537384027e-02,
     1.258034937407e-01,
     3.800000000000e-01,
     6.341965062593e-01,
     8.502553738403e-01,
     1.010173813902e+00,
     1.117005838680e+00,
     1.183646521696e+00,
     1.223476183240e+00,
     1.246680822673e+00,
     1.260000000000e+00 };
#endif

  //for (int i=0;i<xfld->nDOF();i++)
  //  std::cout << std::scientific << std::setprecision(12) << xfld->DOF(i)[0] << "," << std::endl;

  // strictly check the mesh vertex locations
  for (int i=0;i<xfld->nDOF();i++)
    BOOST_CHECK_CLOSE( xfld->DOF(i)[0] , xtruth[i] , 1e-10 );

  // loosely check the mesh sizes against the requested field
  std::vector<Real> H( xfld->nDOF()-1 );
  for (int i=0;i<xfld->nDOF()-1;i++)
  {
    Real x1 = xfld->DOF(i+1)[0];
    Real x0 = xfld->DOF(i)[0];
    Real xm = .5*( x0 +x1 );
    Real s = ( xm -xL )/( xR -xL );
    H[i] = x1 -x0;
    hi = meshSize( s );
    //printf("H[%d] = %g, hrequest = %g\n",i,H[i],hi);

    // check that they are somewhat close, ask for 3.5 %
    BOOST_CHECK_CLOSE( H[i] , hi , 3.5 );
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
