// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//This must be included first
#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "tools/linspace.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( linspace_int_test )
{
  std::vector<int> v = linspace(3,6);

  BOOST_CHECK_EQUAL(4, v.size());
  BOOST_CHECK_EQUAL(3, v[0]);
  BOOST_CHECK_EQUAL(4, v[1]);
  BOOST_CHECK_EQUAL(5, v[2]);
  BOOST_CHECK_EQUAL(6, v[3]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( linspace_real_test )
{
  std::vector<Real> v = linspace(1, 0, 5);

  BOOST_CHECK_EQUAL(5, v.size());
  BOOST_CHECK_EQUAL(1.00, v[0]);
  BOOST_CHECK_EQUAL(0.75, v[1]);
  BOOST_CHECK_EQUAL(0.50, v[2]);
  BOOST_CHECK_EQUAL(0.25, v[3]);
  BOOST_CHECK_EQUAL(0.00, v[4]);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
