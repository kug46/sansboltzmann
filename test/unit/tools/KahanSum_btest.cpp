// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "tools/KahanSum.h"

#include <iostream>
#include <iomanip> // std::setprecision
#include <numeric>

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

// Test taken from
// http://codereview.stackexchange.com/questions/56532/kahan-summation

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( KahanSum_test )
{
  const Real closetol = 1e-12;

  const Real initial = 1e3;
  const Real addend = 1e-15;
  const int count = 1e7;
  const Real multiplier = 1e19;

  std::vector<Real> d;

  d.push_back(initial);

  std::fill_n(std::back_inserter(d), count, addend);

  // Simple sum
  Real result = (std::accumulate(d.begin(), d.end(), 0.0) - initial) * multiplier;

  // Kahan summation to account for floating point precision
  KahanSum<Real> sum;
  for (auto begin = d.begin(); begin != d.end(); ++begin)
    sum += *begin;

  Real result2 = (sum - initial) * multiplier;

  // The single expression works for gnu, but intel re-arranges it and the test fails
#ifdef __INTEL_COMPILER
  Real reference = initial + count * addend;
  reference -= initial;
  reference *= multiplier;
#else
  // Reference computed by multiplying the small value rather than summing
  Real reference = ((initial + count * addend) - initial) * multiplier;
#endif

#ifndef __INTEL_COMPILER
  // Intel compiler computes something other than 0, but its wrong
  BOOST_CHECK_EQUAL(0, result);
#endif
  BOOST_CHECK_CLOSE( reference, result2, closetol );

#if 0
  std::cout << "   simple: " << std::setprecision(20) << result << "\n";
  std::cout << "    Kahan: " << std::setprecision(20) << result2 << "\n";
  std::cout << "Reference: " << std::setprecision(20) << reference << "\n";
#endif

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( KahanSum_initial_test )
{
  const Real closetol = 1e-12;

  const Real initial = 1e3;
  const Real addend = 1e-15;
  const int count = 1e7;
  const Real multiplier = 1e19;

  // The single expression works for gnu, but intel re-arranges it and the test fails
#ifdef __INTEL_COMPILER
  Real reference = initial + count * addend;
  reference -= initial;
  reference *= multiplier;
#else
  // Reference computed by multiplying the small value rather than summing
  Real reference = ((initial + count * addend) - initial) * multiplier;
#endif

  // Kahan summation to account for floating point precision with a non-zero initial value
  KahanSum<Real> sum(initial);
  for (int i = 0; i < count; ++i)
    sum += addend;

  Real result2 = (sum - initial) * multiplier;

  BOOST_CHECK_CLOSE( reference, result2, closetol );

#if 0
  std::cout << "      sum: " << std::setprecision(20) << sum << "\n";
  std::cout << "    Kahan: " << std::setprecision(20) << result2 << "\n";
  std::cout << "Reference: " << std::setprecision(20) << reference << "\n";
#endif

  // Reset the summation and compute it again
  sum = initial;
  for (int i = 0; i < count; ++i)
    sum += addend;

  result2 = (sum - initial) * multiplier;

  BOOST_CHECK_CLOSE( reference, result2, closetol );

#if 0
  std::cout << "      sum: " << std::setprecision(20) << sum << "\n";
  std::cout << "    Kahan: " << std::setprecision(20) << result2 << "\n";
  std::cout << "Reference: " << std::setprecision(20) << reference << "\n";
#endif
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
