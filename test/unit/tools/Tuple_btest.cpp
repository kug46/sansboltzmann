// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//This must be included first
#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "tools/SANSTraitsPOD.h"
#include "tools/Tuple.h"

#include <iostream>

using namespace SANS;
using namespace SANS::detail;

template<class L, class R, class >
class DummyTuple;

//----------------------------------------------------------------------------//
// A class for storing any number of DOF types
//
// template parameters:
//   DofTypeL, DofTypeR  DOF types
//----------------------------------------------------------------------------//
template<class L, class R, int Level >
class DummyTuple<L,R,TupleClass<Level>>
{
public:
  DummyTuple( const L& left, const R& right ) : left_(left), right_(right) {}

  //Generic interfaces
  L& left() { return left_; }
  R& right() { return right_; }

  const L& left() const { return left_; }
  const R& right() const { return right_; }

protected:
  L left_;
  R right_;
};

//############################################################################//
//cppcheck-suppress unknownMacro
BOOST_AUTO_TEST_SUITE( tools )

typedef MakeTuple< DummyTuple, int, float, double, long int >::type TestTuple;

typedef int Type0;
typedef DummyTuple<Type0,float,TupleClass<>> Type1;
typedef DummyTuple<Type1,double,TupleClass<>> Type2;
typedef DummyTuple<Type2,long int,TupleClass<>> Type3;
typedef DummyTuple<Type3,bool,TupleClass<>> Type4;

static_assert( std::is_same<TestTuple,
                            DummyTuple< DummyTuple< DummyTuple<int,float,TupleClass<>>, double, TupleClass<>>, long int, TupleClass<>>
                           >::value, "MakeTuple failed." );

static const int Level0 = 0;

static_assert( TupleSize<TestTuple,Level0>::size == 4, "Incorrect tuple size" );

// Test implementation level
static_assert( std::is_same< TupleType_impl<0,3,Type3>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<0,2,Type2>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<0,1,Type1>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<0,0,Type0>::type, int >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType_impl<1,3,Type3>::type, float >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<1,2,Type2>::type, float >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<1,1,Type1>::type, float >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType_impl<2,3,Type3>::type, double >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<2,2,Type2>::type, double >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType_impl<3,3,Type3>::type, long int >::value, "Incorrect TypleType." );

// Test implementation level
static_assert( std::is_same< TupleType_impl<0,3,Type3,Level0>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<0,2,Type2,Level0>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<0,1,Type1,Level0>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<0,0,Type0,Level0>::type, int >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType_impl<1,3,Type3,Level0>::type, float >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<1,2,Type2,Level0>::type, float >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<1,1,Type1,Level0>::type, float >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType_impl<2,3,Type3,Level0>::type, double >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<2,2,Type2,Level0>::type, double >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType_impl<3,3,Type3,Level0>::type, long int >::value, "Incorrect TypleType." );

// Test simple interface to retrieve the type in a tuple
static_assert( std::is_same< TupleType<0,Type3,Level0>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<0,Type2,Level0>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<0,Type1,Level0>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<0,Type0,Level0>::type, int >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType<1,Type3,Level0>::type, float >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<1,Type2,Level0>::type, float >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<1,Type1,Level0>::type, float >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType<2,Type3,Level0>::type, double >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<2,Type2,Level0>::type, double >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType<3,Type3,Level0>::type, long int >::value, "Incorrect TypleType." );

// Test simple interface to retrieve the type in a tuple
static_assert( std::is_same< TupleType<0,Type3>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<0,Type2>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<0,Type1>::type, int >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<0,Type0>::type, int >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType<1,Type3>::type, float >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<1,Type2>::type, float >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<1,Type1>::type, float >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType<2,Type3>::type, double >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<2,Type2>::type, double >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType<3,Type3>::type, long int >::value, "Incorrect TypleType." );



typedef MakeTupleN< DummyTuple, int, 4 >::type TestTupleN;

static_assert( std::is_same<TestTupleN,
                            DummyTuple< DummyTuple< DummyTuple<int,int,TupleClass<>>, int,TupleClass<>>, int,TupleClass<>>
                           >::value, "MakeTupleN failed." );

//----------------------------------------------------------------------------//
// Test creating tuples of tuples

static const int Level1 = 1;

typedef DummyTuple<Type1,Type2,TupleClass<Level1>> TupleOfTuple0;
typedef DummyTuple<TupleOfTuple0,Type3,TupleClass<Level1>> TupleOfTuple1;

static_assert( TupleSize<TupleOfTuple1,Level1>::size == 3, "Incorrect tuple size" );

// Test implementation level
static_assert( std::is_same< TupleType_impl<0,2,TupleOfTuple1,Level1>::type, Type1 >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<0,1,TupleOfTuple0,Level1>::type, Type1 >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<0,0,Type1        ,Level1>::type, Type1 >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType_impl<1,2,TupleOfTuple1,Level1>::type, Type2 >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType_impl<1,1,TupleOfTuple0,Level1>::type, Type2 >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType_impl<2,2,TupleOfTuple1,Level1>::type, Type3 >::value, "Incorrect TypleType." );

// Test simple interface to retrieve the type in a tuple of tuple
static_assert( std::is_same< TupleType<0,TupleOfTuple1,Level1>::type, Type1 >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<0,TupleOfTuple0,Level1>::type, Type1 >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<0,Type1        ,Level1>::type, Type1 >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType<1,TupleOfTuple1,Level1>::type, Type2 >::value, "Incorrect TypleType." );
static_assert( std::is_same< TupleType<1,TupleOfTuple0,Level1>::type, Type2 >::value, "Incorrect TypleType." );

static_assert( std::is_same< TupleType<2,TupleOfTuple1,Level1>::type, Type3 >::value, "Incorrect TypleType." );

static const int Level2 = 2;
typedef DummyTuple<TupleOfTuple1,TupleOfTuple0,TupleClass<Level2>> TupleOfTupleOfTuple0;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tuple_get_test )
{
  Type0 t0(1);
  Type1 t1(t0, 2.5);
  Type2 t2(t1, 3.5);
  Type3 t3(t2, 6);
  Type4 t4(t3, true);

  BOOST_CHECK_EQUAL( get<0>(t4), 1    );
  BOOST_CHECK_EQUAL( get<1>(t4), 2.5  );
  BOOST_CHECK_EQUAL( get<2>(t4), 3.5  );
  BOOST_CHECK_EQUAL( get<3>(t4), 6    );
  BOOST_CHECK_EQUAL( get<4>(t4), true );

  BOOST_CHECK_EQUAL( get<-5>(t4), 1    );
  BOOST_CHECK_EQUAL( get<-4>(t4), 2.5  );
  BOOST_CHECK_EQUAL( get<-3>(t4), 3.5  );
  BOOST_CHECK_EQUAL( get<-2>(t4), 6    );
  BOOST_CHECK_EQUAL( get<-1>(t4), true );


  BOOST_CHECK_EQUAL( get<int>(t4)     , 1    );
  BOOST_CHECK_EQUAL( get<float>(t4)   , 2.5  );
  BOOST_CHECK_EQUAL( get<double>(t4)  , 3.5  );
  BOOST_CHECK_EQUAL( get<long int>(t4), 6    );
  BOOST_CHECK_EQUAL( get<bool>(t4)    , true );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TupleOfTuple_get_test )
{
  Type0 t0(1);
  Type1 t1(t0, 2.5);
  Type2 t2(t1, 3.5);
  Type3 t3(t2, 6);

  TupleOfTuple0 tot0(t1,t2);
  TupleOfTuple1 tot1(tot0,t3);


  const Type1& u1 = get<0>(tot1);
  const Type2& u2 = get<1>(tot1);
  const Type3& u3 = get<2>(tot1);

  BOOST_CHECK_EQUAL( get<0>(u1), 1    );
  BOOST_CHECK_EQUAL( get<1>(u1), 2.5  );

  BOOST_CHECK_EQUAL( get<0>(u2), 1    );
  BOOST_CHECK_EQUAL( get<1>(u2), 2.5  );
  BOOST_CHECK_EQUAL( get<2>(u2), 3.5  );

  BOOST_CHECK_EQUAL( get<0>(u3), 1    );
  BOOST_CHECK_EQUAL( get<1>(u3), 2.5  );
  BOOST_CHECK_EQUAL( get<2>(u3), 3.5  );
  BOOST_CHECK_EQUAL( get<3>(u3), 6    );


  const Type1& v1 = get<-3>(tot1);
  const Type2& v2 = get<-2>(tot1);
  const Type3& v3 = get<-1>(tot1);

  BOOST_CHECK_EQUAL( get<0>(v1), 1    );
  BOOST_CHECK_EQUAL( get<1>(v1), 2.5  );

  BOOST_CHECK_EQUAL( get<0>(v2), 1    );
  BOOST_CHECK_EQUAL( get<1>(v2), 2.5  );
  BOOST_CHECK_EQUAL( get<2>(v2), 3.5  );

  BOOST_CHECK_EQUAL( get<0>(v3), 1    );
  BOOST_CHECK_EQUAL( get<1>(v3), 2.5  );
  BOOST_CHECK_EQUAL( get<2>(v3), 3.5  );
  BOOST_CHECK_EQUAL( get<3>(v3), 6    );

  {
    const Type1& w1 = get<Type1>(tot1);
    const Type2& w2 = get<Type2>(tot1);
    const Type3& w3 = get<Type3>(tot1);

    BOOST_CHECK_EQUAL( get<0>(w1), 1    );
    BOOST_CHECK_EQUAL( get<1>(w1), 2.5  );

    BOOST_CHECK_EQUAL( get<0>(w2), 1    );
    BOOST_CHECK_EQUAL( get<1>(w2), 2.5  );
    BOOST_CHECK_EQUAL( get<2>(w2), 3.5  );

    BOOST_CHECK_EQUAL( get<0>(w3), 1    );
    BOOST_CHECK_EQUAL( get<1>(w3), 2.5  );
    BOOST_CHECK_EQUAL( get<2>(w3), 3.5  );
    BOOST_CHECK_EQUAL( get<3>(w3), 6    );
  }

  { // testing get<Type> from Tuple< Tuple<..., ..., TupleClass<Level1> >, Tuple<..., ..., TupleClass<Level1> >TupleClass<Level2> >
    Type0 t0(1);
    Type1 t1(t0, 2.5);
    Type2 t2(t1, 3.5);
    Type3 t3(t2, 6);

    Type0 s0(2);
    Type1 s1(s0, 7.5);
    Type2 s2(s1, 0.5);

    TupleOfTuple0 tos0(s1,s2);
    TupleOfTuple1 tos1(tot0,t3);

    TupleOfTupleOfTuple0 totos0(tos1,tos0);

    const Type1& z1 = get<Type1>(totos0);
    const Type2& z2 = get<Type2>(totos0);
    const Type3& z3 = get<Type3>(totos0);

    BOOST_CHECK_EQUAL( get<0>(z1), 1    );
    BOOST_CHECK_EQUAL( get<1>(z1), 2.5  );

    BOOST_CHECK_EQUAL( get<0>(z2), 1    );
    BOOST_CHECK_EQUAL( get<1>(z2), 2.5  );
    BOOST_CHECK_EQUAL( get<2>(z2), 3.5  );

    BOOST_CHECK_EQUAL( get<0>(z3), 1    );
    BOOST_CHECK_EQUAL( get<1>(z3), 2.5  );
    BOOST_CHECK_EQUAL( get<2>(z3), 3.5  );
    BOOST_CHECK_EQUAL( get<3>(z3), 6    );
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
