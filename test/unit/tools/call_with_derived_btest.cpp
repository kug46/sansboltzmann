// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//This must be included first
#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"
#include "tools/call_with_derived.h"

#include <iostream>

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

struct Base
{
  virtual ~Base() {}
  virtual const std::type_info& derivedTypeID() const = 0;

  template<class Derived>
  Derived& cast()
  {
    BOOST_REQUIRE( typeid(*this) == derivedTypeID() );
    return static_cast<Derived&>(*this);
  }
};

struct Derived0 : public Base { virtual ~Derived0() {} virtual const std::type_info& derivedTypeID() const { return typeid(*this); } };
struct Derived1 : public Base { virtual ~Derived1() {} virtual const std::type_info& derivedTypeID() const { return typeid(*this); } };
struct Derived2 : public Base { virtual ~Derived2() {} virtual const std::type_info& derivedTypeID() const { return typeid(*this); } };

struct testCalls
{
  testCalls() : calls{false, false, false} {}
  testCalls(const testCalls& t) = delete;

  bool calls[3];

  void operator()( Derived0& ) { calls[0] = true; }
  void operator()( Derived1& ) { calls[1] = true; }
  void operator()( Derived2& ) { calls[2] = true; }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( call_derived_test )
{
  typedef boost::mpl::vector<Derived0,Derived1,Derived2> DerivedVector3;

  testCalls calltester;

  std::vector< std::shared_ptr<Base> > vect;

  vect.push_back( std::shared_ptr<Base>( new Derived0 ) );
  vect.push_back( std::shared_ptr<Base>( new Derived1 ) );
  vect.push_back( std::shared_ptr<Base>( new Derived2 ) );

  call_with_derived<DerivedVector3, testCalls&>(calltester, *vect[0]);
  BOOST_CHECK( calltester.calls[0] == true  );
  BOOST_CHECK( calltester.calls[1] == false );
  BOOST_CHECK( calltester.calls[2] == false );

  call_with_derived<DerivedVector3, testCalls&>(calltester, *vect[1]);
  BOOST_CHECK( calltester.calls[0] == true  );
  BOOST_CHECK( calltester.calls[1] == true  );
  BOOST_CHECK( calltester.calls[2] == false );

  call_with_derived<DerivedVector3, testCalls&>(calltester, *vect[2]);
  BOOST_CHECK( calltester.calls[0] == true  );
  BOOST_CHECK( calltester.calls[1] == true  );
  BOOST_CHECK( calltester.calls[2] == true  );

  typedef boost::mpl::vector<Derived0,Derived1> DerivedVector2;

  BOOST_CHECK_THROW( (call_with_derived<DerivedVector2, testCalls&>(calltester, *vect[2])), DeveloperException );

#if DOCUMENTION && 0
  // call_with_derived effectively expands out to the following function
  // However, this is done automatically using template meta programming
  void call_with_derived(testCalls& calltester, Base base)
  {
    // If the current type matches the derived type, then call f with the base class casted
    if (typeid(Derived0) == base.derivedTypeID())
    {
      calltester(base.template cast<Derived0>());
    }
    else if (typeid(Derived1) == base.derivedTypeID())
    {
      calltester(base.template cast<Derived1>());
    }
    else if (typeid(Derived2) == base.derivedTypeID())
    {
      calltester(base.template cast<Derived2>());
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown derived type");
  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
