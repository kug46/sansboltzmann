// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"

#include "tools/output_std_vector.h"

#include <iostream>

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( output_vector_Real_test )
{
  output_test_stream output;
  std::vector<Real> v = {1, 2, 3};

  output << v;
  BOOST_CHECK( output.is_equal( "1, 2, 3" ) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( output_vector_vector_Real_test )
{
  output_test_stream output;
  std::vector<std::vector<Real>> v = {{1, 2}, {3, 4}, {5, 6}};

  output << v;
  BOOST_CHECK( output.is_equal( "1, 2, 3, 4, 5, 6" ) );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
