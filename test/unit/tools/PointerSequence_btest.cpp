// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/PointerSequence.h"

#include <vector>

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PointerSequence_test )
{
  const int size = 4;
  const int arg = 5;
  PointerSequence< std::vector<int> > ps(size, arg);

  BOOST_REQUIRE_EQUAL(size, ps.size());

  for (int i = 0; i < size; i++)
    BOOST_CHECK_EQUAL(arg, ps[i].size());

  ps[0][0] = 0;
  ps[1][0] = 1;
  ps[2][0] = 2;
  ps[3][0] = 3;

  ps.rotate();

  BOOST_CHECK_EQUAL(3, ps[0][0]);
  BOOST_CHECK_EQUAL(0, ps[1][0]);
  BOOST_CHECK_EQUAL(1, ps[2][0]);
  BOOST_CHECK_EQUAL(2, ps[3][0]);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
