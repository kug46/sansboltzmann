// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//This must be included first
#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/vector.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/lambda.hpp>
#include <boost/mpl/equal.hpp>

#include "tools/SANSnumerics.h"
#include "tools/add_decorator.h"

#include <iostream>

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( add_decorator_test_suite )

struct test1 {};
struct test2 {};

template<class Base>
struct decorator : public Base {};

// Define a test vector for the example
typedef boost::mpl::vector<test1, test2> testVector;

// Transform the test vector into boost::mpl::vector< decorator<test1>, decorator<test2> >
typedef boost::mpl::transform<testVector, add_decorator<decorator,boost::mpl::_1> >::type decoratedVector;

// boost::mpl::equal checks that each entry in two Sequences are is_same
static_assert( boost::mpl::equal< decoratedVector, boost::mpl::vector< decorator<test1>, decorator<test2> > >::value, "These should match" );

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( empty_test )
{
  // An empty test so this file can be compiled independently
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
