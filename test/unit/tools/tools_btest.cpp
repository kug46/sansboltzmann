// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "tools/SANSTraitsPOD.h"
#include "tools/stringify.h"
#include "tools/Type2String.h"

#include <iostream>

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SANSDummyType_test )
{
  int i = 1;
  SANSDummyType dummy;

  //The case to int of the dummy type is simply there to prevent
  //compiler errors. It should never be called, and hence should
  //always throw an exception if it is called.
  BOOST_CHECK_EQUAL( i, 1 ); //Need to check i to suppress warnings
  // cppcheck-suppress unreadVariable
  BOOST_CHECK_THROW( i = (int)dummy, AssertionException ); //Need the i to suppress warnings
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( stringify_test )
{

  BOOST_CHECK_EQUAL( stringify(true), "true" );
  BOOST_CHECK_EQUAL( stringify(false), "false" );

  BOOST_CHECK_EQUAL( stringify(std::string("string")), "string" );
  BOOST_CHECK_EQUAL( stringify("const char*"), "const char*" );

  BOOST_CHECK_EQUAL( stringify(10), "10" );
  BOOST_CHECK_EQUAL( stringify(1.), "1" );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Type2String_test )
{

  BOOST_CHECK_EQUAL( Type2String<int>::str(), "int" );
  BOOST_CHECK_EQUAL( Type2String<float>::str(), "float" );
  BOOST_CHECK_EQUAL( Type2String<double>::str(), "double" );
  BOOST_CHECK_EQUAL( Type2String<bool>::str(), "bool" );
  BOOST_CHECK_EQUAL( Type2String<std::string>::str(), "string" );
  BOOST_CHECK_EQUAL( Type2String<PyDict>::str(), "dict" );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
