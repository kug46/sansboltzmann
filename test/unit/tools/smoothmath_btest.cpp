// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//This must be included first
#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/smoothmath.h"

#include <iostream>

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( smoothabs0_test )
{
  Real closetol = 1e-12;
  Real eps = 1e-3;
  BOOST_CHECK_CLOSE( smoothabs0( 10., eps ), 100. / (10. + eps), closetol );
  BOOST_CHECK_CLOSE( smoothabs0(-10., eps ), 100. / (10. + eps), closetol );

  BOOST_CHECK_CLOSE( smoothabs0( eps, eps ), eps*eps / (eps + eps), closetol );
  BOOST_CHECK_CLOSE( smoothabs0(-eps, eps ), eps*eps / (eps + eps), closetol );

  BOOST_CHECK_SMALL( smoothabs0(0., eps ), closetol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( smoothabsP_test )
{
  Real closetol = 1e-12;
  Real eps = 1e-3;
  BOOST_CHECK_CLOSE( smoothabsP( 10., eps ), 10, closetol );
  BOOST_CHECK_CLOSE( smoothabsP(-10., eps ), 10, closetol );

  BOOST_CHECK_CLOSE( smoothabsP( eps/2., eps ), 0.5*(eps + pow(eps/2.,2)/eps), closetol );
  BOOST_CHECK_CLOSE( smoothabsP(-eps/2., eps ), 0.5*(eps + pow(eps/2.,2)/eps), closetol );

  BOOST_CHECK_CLOSE( smoothabsP(0., eps ), 0.5*eps, closetol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( smoothminmax_test )
{
  Real closetol = 1e-12;
  Real alpha = 20;
  BOOST_CHECK_EQUAL( smoothmax(-20.,  10., alpha ), 10. );
  BOOST_CHECK_EQUAL( smoothmax( 10., -20., alpha ), 10. );
  BOOST_CHECK_EQUAL( smoothmax(-1.e6, 1.e7, alpha ),  1.e7 );
  BOOST_CHECK_CLOSE( smoothmax( 1000000., 1000000.1, alpha ),  1000000.1 + log1p( exp(alpha*(-0.1)) )/alpha, closetol );

  BOOST_CHECK_EQUAL( smoothmin( 20., -10., alpha ), -10. );
  BOOST_CHECK_EQUAL( smoothmin(-10.,  20., alpha ), -10. );
  BOOST_CHECK_EQUAL( smoothmin(-1.e6, 1.e7, alpha ), -1.e6 );
  BOOST_CHECK_CLOSE( smoothmin( 1000000., 1000000.1, alpha ),  1000000. - log1p( exp(-alpha*(0.1)) )/alpha, closetol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( smoothminmaxC1_test )
{
  Real closetol = 1e-12;
  Real eps = 1e-2;
  BOOST_CHECK_EQUAL( smoothmaxC1(-20.,  10., eps ), 10. );
  BOOST_CHECK_EQUAL( smoothmaxC1( 10., -20., eps ), 10. );
  BOOST_CHECK_EQUAL( smoothmaxC1(-1.e6, 1.e7, eps ),  1.e7 );
  BOOST_CHECK_CLOSE( smoothmaxC1( 1000000., 1000000.1, eps ), 1000000.1, closetol );
  BOOST_CHECK_CLOSE( smoothmaxC1( 100., 100.001, eps), 100 + pow(eps + 2*0.001,2)/(8*eps) , closetol);

  BOOST_CHECK_EQUAL( smoothminC1( 20., -10., eps ), -10. );
  BOOST_CHECK_EQUAL( smoothminC1(-10.,  20., eps ), -10. );
  BOOST_CHECK_EQUAL( smoothminC1(-1.e6, 1.e7, eps ), -1.e6 );
  BOOST_CHECK_CLOSE( smoothminC1( 1000000., 1000000.1, eps ), 1000000.0, closetol );
  BOOST_CHECK_CLOSE( smoothminC1( 100., 100.001, eps), -(-100.001 + pow(eps + 2*0.001,2)/(8*eps)) , closetol);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( smoothminmaxC2_test )
{
  Real closetol = 1e-12;
  Real eps = 1e-2;
  BOOST_CHECK_EQUAL( smoothmaxC2(-20.,  10., eps ), 10. );
  BOOST_CHECK_EQUAL( smoothmaxC2( 10., -20., eps ), 10. );
  BOOST_CHECK_EQUAL( smoothmaxC2(-1.e6, 1.e7, eps ),  1.e7 );
  BOOST_CHECK_CLOSE( smoothmaxC2( 1000000., 1000000.1, eps ), 1000000.1, closetol );
  BOOST_CHECK_CLOSE( smoothmaxC2( 100., 100.001, eps), 100 + (pow(eps+2*1e-3,3)*(3*eps - 2*1e-3))/(32*pow(eps,3)) , closetol);

  BOOST_CHECK_EQUAL( smoothminC2( 20., -10., eps ), -10. );
  BOOST_CHECK_EQUAL( smoothminC2(-10.,  20., eps ), -10. );
  BOOST_CHECK_EQUAL( smoothminC2(-1.e6, 1.e7, eps ), -1.e6 );
  BOOST_CHECK_CLOSE( smoothminC2( 1000000., 1000000.1, eps ), 1000000.0, closetol );
  BOOST_CHECK_CLOSE( smoothminC2( 100., 100.001, eps), -(-100.001 + (pow(eps+2*1e-3,3)*(3*eps - 2*1e-3))/(32*pow(eps,3)) ) , closetol);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( smoothRamp0_test )
{
  Real closetol = 1e-12;
  Real eps = 1e-3;
  BOOST_CHECK_CLOSE( smoothRamp0( 10., eps ), 100. / (10. + eps), closetol );
  BOOST_CHECK_CLOSE( smoothRamp0(-10., eps ), 0.0, closetol );

  BOOST_CHECK_SMALL( smoothRamp0(0., eps ), closetol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( smoothactivation_test )
{
  Real small_tol = 1e-12;
  Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( smoothActivation_sine ( -0.123 ), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( smoothActivation_cubic( -0.123 ), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( smoothActivation_tanh ( -0.123 ), 0.5*(1 + tanh(4*(-0.123-0.5))), small_tol, close_tol );

  SANS_CHECK_CLOSE( smoothActivation_sine ( 0.5 ), 0.5, small_tol, close_tol );
  SANS_CHECK_CLOSE( smoothActivation_cubic( 0.5 ), 0.5, small_tol, close_tol );
  SANS_CHECK_CLOSE( smoothActivation_tanh ( 0.5 ), 0.5, small_tol, close_tol );

  SANS_CHECK_CLOSE( smoothActivation_sine ( 1.123 ), 1.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( smoothActivation_cubic( 1.123 ), 1.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( smoothActivation_tanh ( 1.123 ), 0.5*(1 + tanh(4*(1.123-0.5))), small_tol, close_tol );

  Real x = 0.25;
  SANS_CHECK_CLOSE( smoothActivation_sine ( x ), 0.5*(1.0 + sin(PI*(x-0.5))), small_tol, close_tol );
  SANS_CHECK_CLOSE( smoothActivation_cubic( x ), 3.0*x*x - 2.0*x*x*x, small_tol, close_tol );
  SANS_CHECK_CLOSE( smoothActivation_tanh ( x ), 0.5*(1 + tanh(4*(x-0.5))), small_tol, close_tol );

  x = 0.657;
  SANS_CHECK_CLOSE( smoothActivation_sine ( x ), 0.5*(1.0 + sin(PI*(x-0.5))), small_tol, close_tol );
  SANS_CHECK_CLOSE( smoothActivation_cubic( x ), 3.0*x*x - 2.0*x*x*x, small_tol, close_tol );
  SANS_CHECK_CLOSE( smoothActivation_tanh ( x ), 0.5*(1 + tanh(4*(x-0.5))), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( smoothactivation_exp_test )
{
  Real small_tol = 1e-14;
  Real close_tol = 1e-12;

  Real alpha = 500;
  Real eps = 1e-3;

  SANS_CHECK_CLOSE( eps, smoothActivation_exp ( 0., alpha, eps), small_tol, close_tol );
  SANS_CHECK_CLOSE( 1.0, smoothActivation_exp ( 2., alpha, eps), small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
