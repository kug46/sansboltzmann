// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//This must be included first
#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "pyrite_fstream.h"

#include <iostream>
#include <cstdio>

using namespace SANS;

//############################################################################//
//cppcheck-suppress unknownMacro
BOOST_AUTO_TEST_SUITE( tools )

template<class stream>
void DummyFileWrites( stream& out )
{
  out << 1 << std::endl;
  out << 3.14 << std::endl;
  out << "Hello_World!" << std::endl;
  out << std::string("Hello_Other_World!") << std::endl;

  // Temporarily change the close tolerance
  out.close(1e-5) << 3.145 << std::endl;
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pyrite_file_test )
{

  std::string filename = "IO/pyrite_file_test.txt";

  pyrite_file_stream file(filename, 1e-10, 1e-10, pyrite_file_stream::generate);

  //Create the file
  DummyFileWrites(file);

  BOOST_CHECK_THROW( file << " ", AssertionException );
  BOOST_CHECK_THROW( file << "\n", AssertionException );
  BOOST_CHECK_THROW( file << "\r", AssertionException );

  pyrite_file_stream file2(filename, 1e-10, 1e-10, pyrite_file_stream::check);

  //This time we are reading back in the file and making sure what is written matches what is in the file
  DummyFileWrites(file2);

  //Remove the test file from the file system
  std::remove(filename.c_str());
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
