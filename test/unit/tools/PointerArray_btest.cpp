// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/PointerArray.h"

#include <vector>

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PointerArray_rotate_test )
{
  const int size = 4;
  PointerArray< std::vector<int> > pa(size);

  BOOST_REQUIRE_EQUAL(size, pa.size());

  for (int i = 0; i < size; i++)
  {
    pa[i] = new std::vector<int>(5);
  }

  (*pa[0])[0] = 0;
  (*pa[1])[0] = 1;
  (*pa[2])[0] = 2;
  (*pa[3])[0] = 3;

  pa.rotate();

  BOOST_CHECK_EQUAL(3, (*pa[0])[0]);
  BOOST_CHECK_EQUAL(0, (*pa[1])[0]);
  BOOST_CHECK_EQUAL(1, (*pa[2])[0]);
  BOOST_CHECK_EQUAL(2, (*pa[3])[0]);

  BOOST_CHECK_EQUAL(3, (*const_cast<const PointerArray< std::vector<int> >&>(pa)[0])[0]);
  BOOST_CHECK_EQUAL(0, (*const_cast<const PointerArray< std::vector<int> >&>(pa)[1])[0]);
  BOOST_CHECK_EQUAL(1, (*const_cast<const PointerArray< std::vector<int> >&>(pa)[2])[0]);
  BOOST_CHECK_EQUAL(2, (*const_cast<const PointerArray< std::vector<int> >&>(pa)[3])[0]);

}

#ifndef __clang_analyzer__
// clang analyzer gets confused by this test
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PointerArray_resize_test )
{
  const int size = 4;
  PointerArray< std::vector<int> > pa(size);

  BOOST_REQUIRE_EQUAL(size, pa.size());

  for (int i = 0; i < size; i++)
  {
    pa[i] = new std::vector<int>(5);
  }

  BOOST_REQUIRE_EQUAL(4, pa.size());

  (*pa[0])[0] = 0;
  (*pa[1])[0] = 1;
  (*pa[2])[0] = 2;
  (*pa[3])[0] = 3;

  pa.resize(size - 1);

  BOOST_REQUIRE_EQUAL(3, pa.size());

  BOOST_CHECK_EQUAL(0, (*pa[0])[0]);
  BOOST_CHECK_EQUAL(1, (*pa[1])[0]);
  BOOST_CHECK_EQUAL(2, (*pa[2])[0]);

  pa.resize(size + 1);

  BOOST_REQUIRE_EQUAL(5, pa.size());

  BOOST_CHECK_EQUAL(0, (*pa[0])[0]);
  BOOST_CHECK_EQUAL(1, (*pa[1])[0]);
  BOOST_CHECK_EQUAL(2, (*pa[2])[0]);
  BOOST_CHECK(nullptr == pa[3]);
  BOOST_CHECK(nullptr == pa[4]);

  pa.resize(0);

  BOOST_REQUIRE_EQUAL(0, pa.size());
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
