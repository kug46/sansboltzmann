// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//This must be included first
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/file_extension.h"

#include <iostream>

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( file_extension )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( file_extension_tests )
{
  std::string filename1 = "airfoil.gri";
  std::string ext1 = get_file_extension(filename1);
  BOOST_CHECK_EQUAL( ext1 , "gri" );

  std::string filename2 = "readme";
  std::string ext2 = get_file_extension(filename2);
  BOOST_CHECK_EQUAL( ext2.empty() , true );

  std::string filename3 = "naca.0012.airfoil.grm";
  std::string ext3 = get_file_extension(filename3);
  BOOST_CHECK_EQUAL( ext3 , "grm" );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
