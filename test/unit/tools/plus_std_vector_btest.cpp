// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"

#include "tools/plus_std_vector.h"

//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( plus_vector_Real_test )
{
  std::plus< std::vector<Real> > Op;
  std::vector<Real> v0 = {1, 2, 3};
  std::vector<Real> v1 = {2, 3, 4};

  std::vector<Real> add = Op(v0, v1);
  BOOST_CHECK_EQUAL(3, add[0]);
  BOOST_CHECK_EQUAL(5, add[1]);
  BOOST_CHECK_EQUAL(7, add[2]);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( plus_vector_vector_Real_test )
{
  std::plus< std::vector<std::vector<Real>> > Op;
  std::vector<std::vector<Real>> v0 = {{1, 2}, {3, 4}};
  std::vector<std::vector<Real>> v1 = {{4, 5}, {6, 7}};

  std::vector<std::vector<Real>> add = Op(v0, v1);
  BOOST_CHECK_EQUAL(5, add[0][0]);
  BOOST_CHECK_EQUAL(7, add[0][1]);
  BOOST_CHECK_EQUAL(9, add[1][0]);
  BOOST_CHECK_EQUAL(11, add[1][1]);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
