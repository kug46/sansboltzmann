// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"

#include "tools/split_cat_std_vector.h"

//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( split_vector_Real_test )
{
  std::vector<Real> v0 = {0,1,2,3};

  std::vector<std::vector<Real>> v1 = SANS::split(v0);
//  std::vector<std::vector<Real>> v1;
//  for (auto it = v0.begin(); it != v0.end(); ++it)
//    v1.push_back(std::vector<Real>(1,*it));

  BOOST_CHECK_EQUAL( 0, v1[0][0] );
  BOOST_CHECK_EQUAL( 1, v1[1][0] );
  BOOST_CHECK_EQUAL( 2, v1[2][0] );
  BOOST_CHECK_EQUAL( 3, v1[3][0] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cat_vector_Real_test )
{
  std::vector<Real> v0 = {0,1,2,3}, v1 = {4,5,6};
  std::vector<std::vector<Real>> v2 = {v0,v1};

  std::vector<Real> v3 = SANS::cat(v2);

  BOOST_CHECK_EQUAL( 0, v3[0] );
  BOOST_CHECK_EQUAL( 1, v3[1] );
  BOOST_CHECK_EQUAL( 2, v3[2] );
  BOOST_CHECK_EQUAL( 3, v3[3] );
  BOOST_CHECK_EQUAL( 4, v3[4] );
  BOOST_CHECK_EQUAL( 5, v3[5] );
  BOOST_CHECK_EQUAL( 6, v3[6] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( split_vector_int_test )
{
  std::vector<int> v0 = {0,1,2,3};

  std::vector<std::vector<int>> v1 = SANS::split(v0);

  BOOST_CHECK_EQUAL( 0, v1[0][0] );
  BOOST_CHECK_EQUAL( 1, v1[1][0] );
  BOOST_CHECK_EQUAL( 2, v1[2][0] );
  BOOST_CHECK_EQUAL( 3, v1[3][0] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( cat_vector_int_test )
{
  std::vector<int> v0 = {0,1,2,3}, v1 = {4,5,6};
  std::vector<std::vector<int>> v2 = {v0,v1};

  std::vector<int> v3 = SANS::cat(v2);

  BOOST_CHECK_EQUAL( 0, v3[0] );
  BOOST_CHECK_EQUAL( 1, v3[1] );
  BOOST_CHECK_EQUAL( 2, v3[2] );
  BOOST_CHECK_EQUAL( 3, v3[3] );
  BOOST_CHECK_EQUAL( 4, v3[4] );
  BOOST_CHECK_EQUAL( 5, v3[5] );
  BOOST_CHECK_EQUAL( 6, v3[6] );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
