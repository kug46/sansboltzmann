// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//This must be included first
#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"
#include "tools/call_derived_functor.h"

#include <iostream>

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( tools )

struct BaseFunctor
{
  virtual ~BaseFunctor() {}
  virtual const std::type_info& derivedTypeID() const = 0;

  template<class Derived>
  Derived& cast()
  {
    BOOST_REQUIRE( typeid(*this) == derivedTypeID() );
    return static_cast<Derived&>(*this);
  }
};

struct DerivedFunctor0 : public BaseFunctor
{
  virtual ~DerivedFunctor0() {}
  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  void operator()(int& i) const
  {
    i = 0;
  }
};

struct DerivedFunctor1 : public BaseFunctor
{
  virtual ~DerivedFunctor1() {}
  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  void operator()(int& i) const
  {
    i = 1;
  }
};

struct DerivedFunctor2 : public BaseFunctor
{
  virtual ~DerivedFunctor2() {}
  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  void operator()(int& i) const
  {
    i = 2;
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( call_derived_functor_test )
{
  // Test a version of call_with_dervied that caches the sequence index to avoid virtual function calls
  typedef boost::mpl::vector<DerivedFunctor0,DerivedFunctor1,DerivedFunctor2> DerivedVector3;

  std::vector< std::shared_ptr<BaseFunctor> > vect;

  vect.push_back( std::shared_ptr<BaseFunctor>( new DerivedFunctor0 ) );
  vect.push_back( std::shared_ptr<BaseFunctor>( new DerivedFunctor1 ) );
  vect.push_back( std::shared_ptr<BaseFunctor>( new DerivedFunctor2 ) );

  int i;
  call_derived_functor<DerivedVector3, BaseFunctor> call0(*vect[0]);
  call0(i);
  BOOST_CHECK_EQUAL( 0, i );

  call_derived_functor<DerivedVector3, BaseFunctor> call1(*vect[1]);
  call1(i);
  BOOST_CHECK_EQUAL( 1, i );

  call_derived_functor<DerivedVector3, BaseFunctor> call2(*vect[2]);
  call2(i);
  BOOST_CHECK_EQUAL( 2, i );

  typedef boost::mpl::vector<DerivedFunctor0,DerivedFunctor1> DerivedVector2;
  typedef call_derived_functor<DerivedVector2, BaseFunctor>  call_derived_functor2;

  BOOST_CHECK_THROW( (call_derived_functor2(*vect[2])), DeveloperException );

#if DOCUMENTION && 0
  // call_with_derived effectively expands out to the following function
  // However, this is done automatically using template meta programming
  call_with_derived_cache(Base base)
  {
    // Save off the index based on the derived type
    if (typeid(Derived0) == base.derivedTypeID())
    {
      index_ = 0;
    }
    else if (typeid(Derived1) == base.derivedTypeID())
    {
      index_ = 1;
    }
    else if (typeid(Derived2) == base.derivedTypeID())
    {
      index_ = 1;
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown derived type");
  }

  template<class ...Args>
  void call_with_derived::operator()(Args ..args) const
  {
    // Based on the cached index, call f with the base class casted
    if (index_ == 0)
    {
      base.template cast<Derived0>().operator()(args...);
    }
    else if (index_ == 1)
    {
      base.template cast<Derived1>().operator()(args...);
    }
    else if (index_ == 2)
    {
      base.template cast<Derived2>().operator()(args...);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown derived type");
  }
#endif
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
