// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/python/dict.hpp> //include boost/python file first to remove _POSIX_C_SOURCE warning
#include <boost/python/extract.hpp>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "Python/Parameter.h"
#include "Python/PyDict.h"
#include "Python/InputException.h"

#include <iostream>
#include <fstream>
#include <set>
#include <cstdio> // std::remove of file

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Python )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_ctor )
{
  PyDict d1;
  BOOST_CHECK_EQUAL( d1.getParent(), "" );
  BOOST_CHECK_EQUAL( d1.length(), 0 );

  boost::python::dict bd;
  PyDict d2(bd);
  BOOST_CHECK_EQUAL( d2.getParent(), "" );
  BOOST_CHECK_EQUAL( d2.length(), 0 );

  PyDict d3(bd, "Args", "Key");
  BOOST_CHECK_EQUAL( d3.getParent(), "Args.Key" );
  BOOST_CHECK_EQUAL( d3.length(), 0 );

  PyDict d4(bd, "Args", 0);
  BOOST_CHECK_EQUAL( d4.getParent(), "Args[0]" );
  BOOST_CHECK_EQUAL( d4.length(), 0 );

  PyDict d5(d3);
  BOOST_CHECK_EQUAL( d5.getParent(), "Args.Key" );
  BOOST_CHECK_EQUAL( d5.length(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_access )
{
  PyDict d1;

  BOOST_CHECK_EQUAL( d1.has_key("gamma"), false );

  d1["gamma"] = 1.4;
  BOOST_CHECK( d1.has_key("gamma") );
  BOOST_CHECK_EQUAL( boost::python::extract<double>( d1["gamma"] ), 1.4 );
  BOOST_CHECK_EQUAL( boost::python::extract<double>( const_cast<const PyDict&>(d1)["gamma"] ), 1.4 );
  BOOST_CHECK_EQUAL( d1.length(), 1 );

  d1[0] = 6;
  BOOST_CHECK( d1.has_key(0) );
  BOOST_CHECK_EQUAL( boost::python::extract<int>( d1[0] ), 6 );
  BOOST_CHECK_EQUAL( boost::python::extract<int>( const_cast<const PyDict&>(d1)[0] ), 6 );
  BOOST_CHECK_EQUAL( d1.length(), 2 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_assign )
{
  boost::python::dict pyd;
  std::string ParentString = "GasModel";
  std::string ParentKey = "Test";

  PyDict d1(pyd, ParentString, ParentKey);

  d1["gamma"] = 1.4;

  BOOST_CHECK_EQUAL( d1.getParent(), "GasModel.Test" );
  BOOST_CHECK_EQUAL( boost::python::extract<double>( d1["gamma"] ), 1.4 );

  PyDict d2;
  d2 = d1;

  BOOST_CHECK_EQUAL( d2.getParent(), "GasModel.Test" );
  BOOST_CHECK_EQUAL( boost::python::extract<double>( d2["gamma"] ), 1.4 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_stringKeys )
{
  PyDict d1;
  d1["test1"] = 1.4;
  d1["test2"] = false;
  d1["test3"] = 1;

  bool checks[] = {false, false, false};
  std::string truths[] = {"test1", "test2", "test3"};

  std::vector<std::string> keys = d1.stringKeys();

  BOOST_CHECK_EQUAL( keys.size(), 3 );

  // Keys are not guaranteed to be in any order, so just check that all are found
  for (std::size_t k = 0; k < 3; k++)
    for (std::size_t i = 0; i < keys.size(); i++)
      if (keys[i] == truths[k])
        checks[k] = true;

  BOOST_CHECK( checks[0] );
  BOOST_CHECK( checks[1] );
  BOOST_CHECK( checks[2] );

  // Add a key that is not a string, and you shuld get an exception
  d1[0] = 1;
  BOOST_CHECK_THROW( d1.stringKeys(), KeyTypeException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDictKeyPair )
{
  PyDict d1;
  std::string key("gamma");

  DictKeyPair pair(d1, key);

  BOOST_CHECK_EQUAL( pair.has_key(0), false );
  BOOST_CHECK_EQUAL( pair.has_key(pair), false );

  ((PyDict&)pair)[pair] = 1.4;
  BOOST_CHECK( pair.has_key(pair) );
  BOOST_CHECK_EQUAL( boost::python::extract<double>( ((PyDict&)pair)[pair] ), 1.4 );
  BOOST_CHECK_EQUAL( boost::python::extract<double>( const_cast<const PyDict&>((PyDict&)pair)[pair] ), 1.4 );
  BOOST_CHECK_EQUAL( pair.length(), 1 );

  ((PyDict&)pair)[0] = 6;
  BOOST_CHECK( pair.has_key(0) );
  BOOST_CHECK_EQUAL( boost::python::extract<int>( ((PyDict&)pair)[0] ), 6 );
  BOOST_CHECK_EQUAL( boost::python::extract<int>( const_cast<const PyDict&>((PyDict&)pair)[0] ), 6 );
  BOOST_CHECK_EQUAL( pair.length(), 2 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_ParamsNone_test )
{
  PyDict d, d2;

  // Should have nothing in the dict
  ParamsNone::checkInputs(d);

  //Test setting a valid value
  d["Something"] = "A thing";

  //Expects an empty dictionary
  BOOST_CHECK_THROW( ParamsNone::checkInputs(d), UnknownInputsException );
}

//----------------------------------------------------------------------------//
struct GMRESParam : noncopyable
{
  const ParameterNumeric<int> nOuter{"nOuter", 10        , 0, NO_LIMIT, "Number of Outer Iterations"};
  const ParameterNumeric<int> nInner{"nInner", NO_DEFAULT, 0, NO_LIMIT, "Number of Inner Iterations"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.nOuter));
    allParams.push_back(d.checkInputs(params.nInner));
    d.checkUnknownInputs(allParams);
  }

  static GMRESParam params;
};
GMRESParam GMRESParam::params;

struct BiCGStabParam : noncopyable
{
  const ParameterNumeric<int> nIter{"nIter", NO_DEFAULT, 0, NO_LIMIT, "Number of Iterations"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.nIter));
    d.checkUnknownInputs(allParams);
  }

  static BiCGStabParam params;
};
BiCGStabParam BiCGStabParam::params;

//----------------------------------------------------------------------------//
struct PyDictParam : noncopyable
{
  const ParameterNumeric<Real> gamma{"gamma", 1.4       , 1       , 5./3.   , "Specific heat ratio"};
  const ParameterNumeric<int>  nIter{"nIter", NO_DEFAULT, 0       , NO_LIMIT, "Number of Iterations"};
  const ParameterNumeric<Real> Angle{"Angle", NO_DEFAULT, NO_RANGE          , "An angle"};
  const ParameterNumeric<Real> Beta {"Beta" , 0         , -10     , 0       , "A limited thing"};

  const ParameterBool Print {"Print", true, "To print or not to print."};

  struct BasisFunctionOptions
  {
    typedef std::string ExtractType;
    const std::string Lagrange = "Lagrange";
    const std::string Legendre = "Legendre";

    const std::vector<std::string> options{Lagrange,Legendre};
  };
  const ParameterOption<BasisFunctionOptions> BasisFunction{"BasisFunction", "Lagrange", "Polynomial Basis Function"};

 struct PreconditionerOptions
 {
   typedef std::string ExtractType;
   const std::string ILU0 = "ILU0";

   const std::vector<std::string> options{ILU0};
 };
 const ParameterOption<PreconditionerOptions> Preconditioner{"Preconditioner", NO_DEFAULT, "Preconitioner for the iterative linear solver"};


 struct OrderOptions
 {
   typedef int ExtractType;
   enum Options
   {
     i0, i1, i2, i3
   };
   const std::vector<int> options{i0, i1, i2, i3};
 };
 const ParameterOption<OrderOptions> Order{"Order", NO_DEFAULT, "Polynomial Order"};

 struct RKOrderOptions
 {
   typedef int ExtractType;
   enum Options
   {
     i1 = 1,
     i2 = 2,
     i4 = 4
   };
   const std::vector<int> options{i1, i2, i4};
 };
 const ParameterOption<RKOrderOptions> RKOrder{"RKOrder", 4, "Runge-Kutta Order"};

  const ParameterString FileName{"FileName", NO_DEFAULT, "A file name"};
  const ParameterString Suffix{"Suffix", ".dat", "File suffix"};

  struct LinearSolverOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name = ParameterString("Name", "GMRES", "Iterative Solver Name" );
    const ParameterString& key = Name;

    const DictOption GMRES   {"GMRES", GMRESParam::checkInputs};
    const DictOption BiCGStab{"BiCGStab", BiCGStabParam::checkInputs};

    const std::vector<DictOption> options{GMRES,BiCGStab};
  };
  const ParameterOption<LinearSolverOptions> LinearSolver{"LinearSolver", NO_DEFAULT, "Iterative Linear Solver"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.gamma));
    allParams.push_back(d.checkInputs(params.nIter));
    allParams.push_back(d.checkInputs(params.Angle));
    allParams.push_back(d.checkInputs(params.Beta));

    allParams.push_back(d.checkInputs(params.Print));

    allParams.push_back(d.checkInputs(params.BasisFunction));
    allParams.push_back(d.checkInputs(params.Preconditioner));

    allParams.push_back(d.checkInputs(params.Order));
    allParams.push_back(d.checkInputs(params.RKOrder));

    allParams.push_back(d.checkInputs(params.FileName));
    allParams.push_back(d.checkInputs(params.Suffix));

    allParams.push_back(d.checkInputs(params.LinearSolver));

    d.checkUnknownInputs(allParams);
  }

  static PyDictParam params;
};
PyDictParam PyDictParam::params;


struct Test2Param : noncopyable
{
  const ParameterDict GasModel{"GasModel", NO_DEFAULT, PyDictParam::checkInputs, "Gas model dict"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.GasModel));
    d.checkUnknownInputs(allParams);
  }

  static Test2Param params;
};
Test2Param Test2Param::params;


struct OptionalParam : noncopyable
{
  const ParameterDict Optional{"Optional", EMPTY_DICT, Test2Param::checkInputs, "A default empty dict"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.Optional));
    d.checkUnknownInputs(allParams);
  }

  static OptionalParam params;
};
OptionalParam OptionalParam::params;


struct NumericListParam : noncopyable
{
  const ParameterNumericList<int> ListPos{"ListPos", NO_DEFAULT, 0, NO_LIMIT, "List of positive numbers"};
  const ParameterNumericList<int> ListNeg{"ListNeg", NO_DEFAULT, NO_LIMIT, 0, "List of positive numbers"};

  const ParameterNumericList<Real> ListReal{"ListReal", NO_DEFAULT, NO_RANGE, "List of real numbers"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.ListPos));
    allParams.push_back(d.checkInputs(params.ListNeg));
    allParams.push_back(d.checkInputs(params.ListReal));
    d.checkUnknownInputs(allParams);
  }

  static NumericListParam params;
};
NumericListParam NumericListParam::params;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_gamma_test )
{
  PyDict d, d2;

  //Test setting a valid value
  d[PyDictParam::params.gamma] = 1.5;
  BOOST_CHECK_EQUAL( d.get(PyDictParam::params.gamma), 1.5 );

  //Test default value
  BOOST_CHECK_EQUAL( d2.get(PyDictParam::params.gamma), 1.4 );

  //Test a value out of range
  d[PyDictParam::params.gamma] = 0.5;
  BOOST_CHECK_THROW( d.get(PyDictParam::params.gamma), NumericOutOfRangeException );
  d[PyDictParam::params.gamma] = 2;
  BOOST_CHECK_THROW( d.get(PyDictParam::params.gamma), NumericOutOfRangeException );

  d[PyDictParam::params.gamma] = "This is not a double";
  BOOST_CHECK_THROW( d.get(PyDictParam::params.gamma), DictValueTypeException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_nIter_test )
{
  PyDict d, d2;

  //Test setting a valid value
  d[PyDictParam::params.nIter] = 10;
  BOOST_CHECK_EQUAL( d.get(PyDictParam::params.nIter), 10 );

  //Test default value
  BOOST_CHECK_THROW( d2.get(PyDictParam::params.nIter), NoDefaultException );

  //Test a value out of range
  d[PyDictParam::params.nIter] = -1;
  BOOST_CHECK_THROW( d.get(PyDictParam::params.nIter), NumericOutOfRangeException );

  d[PyDictParam::params.nIter] = 1.1;
  BOOST_CHECK_THROW( d.get(PyDictParam::params.nIter), DictValueTypeException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_Angle_test )
{
  PyDict d, d2;

  //Test setting a valid value
  d[PyDictParam::params.Angle] = 10;
  BOOST_CHECK_EQUAL( d.get(PyDictParam::params.Angle), 10 );

  //Test default value
  BOOST_CHECK_THROW( d2.get(PyDictParam::params.Angle), NoDefaultException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_Print_test )
{
  PyDict d, d2;

  //Test setting a valid value
  d[PyDictParam::params.Print] = false;
  BOOST_CHECK_EQUAL( d.get(PyDictParam::params.Print), false );

  //Test default value
  BOOST_CHECK_EQUAL( d2.get(PyDictParam::params.Print), true );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_BasisFunction_test )
{
  PyDict d, d2;

  //Test setting a valid value
  d[PyDictParam::params.BasisFunction] = "Legendre";
  BOOST_CHECK_EQUAL( d.get(PyDictParam::params.BasisFunction), PyDictParam::params.BasisFunction.Legendre );

  //Test default value
  BOOST_CHECK_EQUAL( d2.get(PyDictParam::params.BasisFunction), PyDictParam::params.BasisFunction.Lagrange );

  d[PyDictParam::params.BasisFunction] = 1; //This is not a string
  BOOST_CHECK_THROW( d.get(PyDictParam::params.BasisFunction), DictValueTypeException );

  d[PyDictParam::params.BasisFunction] = "Not a Solver"; //This is not one of the options
  BOOST_CHECK_THROW( d.get(PyDictParam::params.BasisFunction), OptionException<std::string> );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_Order_test )
{
  PyDict d, d2;

  //Test setting a valid value
  d[PyDictParam::params.Order] = 1;
  BOOST_CHECK_EQUAL( d.get(PyDictParam::params.Order), PyDictParam::params.Order.i1 );

  //Test default value
  BOOST_CHECK_THROW( d2.get(PyDictParam::params.Order), NoDefaultException );

  d[PyDictParam::params.Order] = "1"; //This is not an int
  BOOST_CHECK_THROW( d.get(PyDictParam::params.Order), DictValueTypeException );

  d[PyDictParam::params.Order] = 5; //This is not one of the options
  BOOST_CHECK_THROW( d.get(PyDictParam::params.Order), OptionException<int> );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_FileName_test )
{
  PyDict d, d2;

  //Test setting a valid value
  d[PyDictParam::params.FileName] = "SaveFile.txt";
  BOOST_CHECK_EQUAL( d.get(PyDictParam::params.FileName), "SaveFile.txt" );

  //Test default value
  BOOST_CHECK_THROW( d2.get(PyDictParam::params.FileName), NoDefaultException );

  d[PyDictParam::params.FileName] = 1; //This is not a string
  BOOST_CHECK_THROW( d.get(PyDictParam::params.FileName), DictValueTypeException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_GasModel_test )
{
  PyDict d, d2, d3;

  PyDict bd;
  bd["test"] = 1;
  d[Test2Param::params.GasModel] = bd;
  boost::python::extract<int> test(d.get(Test2Param::params.GasModel)["test"]);
  BOOST_REQUIRE( test.check() );
  BOOST_CHECK_EQUAL( static_cast<int>(test), 1 );


  // Test copying over a python dictionary
  d3[Test2Param::params.GasModel] = d[Test2Param::params.GasModel];
  boost::python::extract<int> test2(d3.get(Test2Param::params.GasModel)["test"]);
  BOOST_REQUIRE( test2.check() );
  BOOST_CHECK_EQUAL( static_cast<int>(test2), 1 );


  //Test default value
  BOOST_CHECK_THROW( d2.get(Test2Param::params.GasModel), NoDefaultException );

  d[Test2Param::params.GasModel] = 1; //This is not a dict
  BOOST_CHECK_THROW( d.get(Test2Param::params.GasModel), DictValueTypeException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_LinearSolver_test )
{
  PyDict d; //, d2;
  PyDict LinearSolver;

  LinearSolver[PyDictParam::params.LinearSolver.Name] = PyDictParam::params.LinearSolver.GMRES;
  LinearSolver[GMRESParam::params.nOuter] = 5;
  LinearSolver[GMRESParam::params.nInner] = 20;

  //Test setting a valid value
  d[PyDictParam::params.LinearSolver] = LinearSolver;
  //(d2, Solver) = d.get(PyDictParam::params.LinearSolver);
  DictKeyPair d2 = d.get(PyDictParam::params.LinearSolver);
  BOOST_CHECK_EQUAL( d2.length(), 3 );
  BOOST_CHECK_EQUAL( (std::string)d2, PyDictParam::params.LinearSolver.GMRES );

  BOOST_CHECK_EQUAL( d2.get(GMRESParam::params.nOuter), 5 );
  BOOST_CHECK_EQUAL( d2.get(GMRESParam::params.nInner), 20 );

  LinearSolver[PyDictParam::params.LinearSolver.Name] = "Not a Solver";
  BOOST_CHECK_THROW( d.get(PyDictParam::params.LinearSolver), OptionException<DictOption> );

  d[PyDictParam::params.LinearSolver] = 1; //This is not a dict
  BOOST_CHECK_THROW( d.get(PyDictParam::params.LinearSolver), DictValueTypeException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_Optional_test )
{

  PyDict d2, LinearSolver_in;

  //Test default value
  BOOST_CHECK_EQUAL( d2.get(OptionalParam::params.Optional).length(), 0 );

  PyDict GasModel_in;
  GasModel_in[PyDictParam::params.gamma] = 1.5;
  GasModel_in[PyDictParam::params.nIter] = 10;
  GasModel_in[PyDictParam::params.Angle] = 5;
  GasModel_in[PyDictParam::params.Order] = 0;
  GasModel_in[PyDictParam::params.Preconditioner] = PyDictParam::params.Preconditioner.ILU0;
  GasModel_in[PyDictParam::params.FileName] = "SaveFile";
  GasModel_in[PyDictParam::params.LinearSolver] = LinearSolver_in;

  LinearSolver_in[GMRESParam::params.nInner] = 100;

  PyDict Optional_in;
  Optional_in[Test2Param::params.GasModel] = GasModel_in;

  PyDict Inputs;
  Inputs[OptionalParam::params.Optional] = Optional_in;

  PyDict Optional = Inputs.get(OptionalParam::params.Optional);
  BOOST_CHECK_EQUAL( Optional.length(), 1 );
  BOOST_CHECK_EQUAL( Optional.getParent(), "Optional" );

  PyDict GasModel = Optional.get(Test2Param::params.GasModel);
  BOOST_CHECK_EQUAL( GasModel.length(), 7 );
  BOOST_CHECK_EQUAL( GasModel.getParent(), "Optional.GasModel" );

  BOOST_CHECK_EQUAL( GasModel.get(PyDictParam::params.gamma), 1.5 );
  BOOST_CHECK_EQUAL( GasModel.get(PyDictParam::params.nIter), 10 );
  BOOST_CHECK_EQUAL( GasModel.get(PyDictParam::params.Angle), 5 );
  BOOST_CHECK_EQUAL( GasModel.get(PyDictParam::params.BasisFunction), PyDictParam::params.BasisFunction.Lagrange );
  BOOST_CHECK_EQUAL( GasModel.get(PyDictParam::params.Order), 0 );
  BOOST_CHECK_EQUAL( GasModel.get(PyDictParam::params.FileName), "SaveFile" );

  DictKeyPair LinearSolver = GasModel.get(PyDictParam::params.LinearSolver);

  BOOST_CHECK_EQUAL( (std::string)LinearSolver, PyDictParam::params.LinearSolver.GMRES );
  BOOST_CHECK_EQUAL( LinearSolver.get(GMRESParam::params.nOuter), 10);
  BOOST_CHECK_EQUAL( LinearSolver.get(GMRESParam::params.nInner), 100);

  //This checks that all the inputs. Should not throw any exceptions for these inputs
  OptionalParam::checkInputs(Inputs);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_NumericList_test )
{
  PyDict d, d2;

  d[NumericListParam::params.ListPos] = {0, 1, 2, 4};
  d[NumericListParam::params.ListNeg] = {0, -1, -2, -4};

  d[NumericListParam::params.ListReal] = {0.1, 3.14, 1.23, 2.56};

  std::vector<int> pos = d.get(NumericListParam::params.ListPos);
  BOOST_REQUIRE_EQUAL(4, pos.size());
  BOOST_CHECK_EQUAL(0, pos[0]);
  BOOST_CHECK_EQUAL(1, pos[1]);
  BOOST_CHECK_EQUAL(2, pos[2]);
  BOOST_CHECK_EQUAL(4, pos[3]);

  std::vector<int> neg = d.get(NumericListParam::params.ListNeg);
  BOOST_REQUIRE_EQUAL(4, neg.size());
  BOOST_CHECK_EQUAL( 0, neg[0]);
  BOOST_CHECK_EQUAL(-1, neg[1]);
  BOOST_CHECK_EQUAL(-2, neg[2]);
  BOOST_CHECK_EQUAL(-4, neg[3]);

  std::vector<Real> real = d.get(NumericListParam::params.ListReal);
  BOOST_REQUIRE_EQUAL(4, real.size());
  BOOST_CHECK_EQUAL(  0.1, real[0]);
  BOOST_CHECK_EQUAL( 3.14, real[1]);
  BOOST_CHECK_EQUAL( 1.23, real[2]);
  BOOST_CHECK_EQUAL( 2.56, real[3]);

  //Test default value
  BOOST_CHECK_THROW( d2.get(NumericListParam::params.ListPos), NoDefaultException );

  d[NumericListParam::params.ListPos] = 1; //This is not a list
  BOOST_CHECK_THROW( d.get(NumericListParam::params.ListPos), DictValueTypeException );

  d[NumericListParam::params.ListPos] = {0, 1, -2, 4}; //Negative number
  BOOST_CHECK_THROW( d.get(NumericListParam::params.ListPos), NumericOutOfRangeException );

  d[NumericListParam::params.ListNeg] = {0, 1, -2, -4};
  BOOST_CHECK_THROW( d.get(NumericListParam::params.ListNeg), NumericOutOfRangeException );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_CheckInput_test )
{
  PyDict Optional_in;
  PyDict GasModel_in;
  PyDict Inputs;
  PyDict LinearSolver;

  //No default for Gas model
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NoDefaultException );
  Inputs[OptionalParam::params.Optional] = Optional_in;

  //Still no default for gas model
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NoDefaultException );
  Optional_in[Test2Param::params.GasModel] = GasModel_in;

  //No default for nIter
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NoDefaultException );
  GasModel_in[PyDictParam::params.nIter] = -1;

  //nIter is out of range
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NumericOutOfRangeException );
  GasModel_in[PyDictParam::params.nIter] = 10;

  //No default for Angle
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NoDefaultException );
  GasModel_in[PyDictParam::params.Angle] = 5;

  //No default for order
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NoDefaultException );
  GasModel_in[PyDictParam::params.Order] = 5;

  //No default for Preconditioner
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NoDefaultException );
  GasModel_in[PyDictParam::params.Preconditioner] = PyDictParam::params.Preconditioner.ILU0;

  //order is out of range
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), OptionException<int> );
  GasModel_in[PyDictParam::params.Order] = 0;

  //No default for FileName
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NoDefaultException );
  GasModel_in[PyDictParam::params.FileName] = 1;

  //File name was not a string
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), DictValueTypeException );
  GasModel_in[PyDictParam::params.FileName] = "SaveFile";

  //No default for linear solver
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NoDefaultException );
  GasModel_in[PyDictParam::params.LinearSolver] = LinearSolver;

  //nInner is missing in the LinearSolver
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NoDefaultException );
  LinearSolver[GMRESParam::params.nInner] = 100;

  //Should not have any more exceptions thrown now
  OptionalParam::checkInputs(Inputs);

  //Change the linear solver and and missing nIter now
  LinearSolver[PyDictParam::params.LinearSolver.Name] = PyDictParam::params.LinearSolver.BiCGStab;
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), NoDefaultException );

  //Fix nIter, but nInner is no longer used
  LinearSolver[BiCGStabParam::params.nIter] = 10;
  BOOST_CHECK_THROW( OptionalParam::checkInputs(Inputs), UnknownInputsException );

  //Remove nInner
  LinearSolver[GMRESParam::params.nInner].del();

  //This should fix the last error
  OptionalParam::checkInputs(Inputs);

  //gamma has a default, so it was not needed
  GasModel_in[PyDictParam::params.gamma] = 1.5;
}

//---------------------------------------------------------------------------//
struct IdealGasParam : noncopyable
{
  const ParameterNumeric<Real> gamma{"gamma", 1.4    , 1, 5./3.   , "Specific heat ratio"};
  const ParameterNumeric<Real> R    {"R"    , 287.04 , 0, NO_LIMIT, "Gas Constant"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.gamma));
    allParams.push_back(d.checkInputs(params.R));
    d.checkUnknownInputs(allParams);
  }

  static IdealGasParam params;
};
IdealGasParam IdealGasParam::params;


struct SutherlandParam : noncopyable
{
  const ParameterNumeric<Real> RefTemp {"RefTemp" , 7.14285714286, 0, NO_LIMIT , "Sutherland reference temperature"};
  const ParameterNumeric<Real> Constant{"Constant", 2.72627343620, NO_RANGE    , "Sutherland Constant"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.RefTemp));
    allParams.push_back(d.checkInputs(params.Constant));
    d.checkUnknownInputs(allParams);
  }

  static SutherlandParam params;
};
SutherlandParam SutherlandParam::params;


struct ConstantViscosityParam : noncopyable
{
  const ParameterNumeric<Real> Constant{"Constant", 2e-7, 0, NO_LIMIT, "Viscosity"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.Constant));
    d.checkUnknownInputs(allParams);
  }

  static ConstantViscosityParam params;
};
ConstantViscosityParam ConstantViscosityParam::params;


struct NavierStokesParam : noncopyable
{
  struct GasModelOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Equation{"Equation", "Ideal", "Gas Model Equation"};
    const ParameterString& key = Equation;

    const DictOption Ideal{"Ideal", IdealGasParam::checkInputs};

    const std::vector<DictOption> options{Ideal};
  };
  const ParameterOption<GasModelOptions> GasModel{"GasModel", EMPTY_DICT, "Gas Model Dictionary"};

  struct ViscosityOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Equation{"Equation", "Constant", "Viscosity Equation"};
    const ParameterString& key = Equation;

    const DictOption Constant  {"Constant"  , ConstantViscosityParam::checkInputs};
    const DictOption Sutherland{"Sutherland", SutherlandParam::checkInputs};

    const std::vector<DictOption> options{Constant,Sutherland};
  };
  const ParameterOption<ViscosityOptions> Viscosity{"Viscosity", NO_DEFAULT, "Viscosity Dictionary"};


//  PARAMETER_DICT_OPTION( Viscosity, ("Viscosity", NO_DEFAULT, "Viscosity Dictionary"),
//                         Equation, ("Equation", "Constant", "Viscosity Equation"),
//                         ((Constant, ("Constant", ConstantViscosityParam::checkInputs)))
//                         ((Sutherland, ("Sutherland", SutherlandParam::checkInputs))) );


  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.GasModel));
    allParams.push_back(d.checkInputs(params.Viscosity));
    d.checkUnknownInputs(allParams);
  }

  static NavierStokesParam params;
};
NavierStokesParam NavierStokesParam::params;


//----------------------------------------------------------------------------//
struct DirichletParam : noncopyable
{
  const ParameterNumeric<Real> q{"q", NO_DEFAULT, NO_RANGE, "Dirichlet value"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.q));
    d.checkUnknownInputs(allParams);
  }

  static constexpr const char* BCName{"Dirichlet"};
  struct Option
  {
    const DictOption Dirichlet{DirichletParam::BCName, DirichletParam::checkInputs};
  };

  static DirichletParam params;
};
DirichletParam DirichletParam::params;

struct NeumannParam : noncopyable
{
  const ParameterNumeric<Real> qx{"qx", NO_DEFAULT, NO_RANGE, "Neumann value"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.qx));
    d.checkUnknownInputs(allParams);
  }

  static constexpr const char* BCName{"Neumann"};
  struct Option
  {
    const DictOption Neumann{NeumannParam::BCName, NeumannParam::checkInputs};
  };

  static NeumannParam params;
};
NeumannParam NeumannParam::params;

struct RobinParam : noncopyable
{
  const ParameterNumeric<Real> q{"q", NO_DEFAULT, NO_RANGE, "Value"};
  const ParameterNumeric<Real> qx{"qx", NO_DEFAULT, NO_RANGE, "Derivative"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.q));
    allParams.push_back(d.checkInputs(params.qx));
    d.checkUnknownInputs(allParams);
  }

  static constexpr const char* BCName{"Robin"};
  struct Option
  {
    const DictOption Robin{RobinParam::BCName, RobinParam::checkInputs};
  };

  static RobinParam params;
};
RobinParam RobinParam::params;

struct BCParam : noncopyable
{
  struct BCOptions : DirichletParam::Option, NeumannParam::Option, RobinParam::Option
  {
    typedef DictKeyPair ExtractType;
    const ParameterString BCType = ParameterString("BCType", NO_DEFAULT, "The Boundary Condition type" );
    const ParameterString& key = BCType;

    const std::vector<DictOption> options{Dirichlet,Neumann,Robin};
  };
  const ParameterOption<BCOptions> BC{"BC", NO_DEFAULT, "BC Dictionary"};


  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;

    // Extract the keys from the dictionary
    std::vector<std::string> keys = d.stringKeys();

    for (std::size_t i = 0; i < keys.size(); i++)
    {
      const ParameterOption<BCOptions> BC{keys[i], NO_DEFAULT, "Boundary Condition Dictionary"};

      allParams.push_back(d.checkInputs(BC));
    }

    d.checkUnknownInputs(allParams);
  }

  static BCParam params;
};
BCParam BCParam::params;

//----------------------------------------------------------------------------//
struct SolverParam : noncopyable
{
  struct PDEOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Equation = ParameterString("Equation", NO_DEFAULT, "PDE Equation" );
    const ParameterString& key = Equation;

    const DictOption Navier_Stokes{"Navier_Stokes", NavierStokesParam::checkInputs};

    const std::vector<DictOption> options{Navier_Stokes};
  };
  const ParameterOption<PDEOptions> PDE{"PDE", NO_DEFAULT, "PDE Dictionary"};

  const ParameterDict BCs{"BCs", NO_DEFAULT, BCParam::checkInputs, "Dictionary of Boundary Conditions"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.PDE));
    allParams.push_back(d.checkInputs(params.BCs));
    d.checkUnknownInputs(allParams);
  }

  static SolverParam params;
};
SolverParam SolverParam::params;


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_BCError_test )
{
  PyDict Left;
  Left[BCParam::params.BC.BCType] = BCParam::params.BC.Dirichlet;
  Left[DirichletParam::params.q] = 3.14;

  PyDict Right;
  Right[BCParam::params.BC.BCType] = BCParam::params.BC.Neumann;
  Right[NeumannParam::params.qx] = 18.19;

  PyDict Lower;
  Lower[BCParam::params.BC.BCType] = BCParam::params.BC.Robin;
  Lower[RobinParam::params.q] = 0.123;
  Lower[RobinParam::params.qx] = 1.58;

  PyDict Upper;
  Upper[BCParam::params.BC.BCType] = BCParam::params.BC.Robin;
  Upper[RobinParam::params.q] = 2.589;
  Upper[RobinParam::params.qx] = -42;

  PyDict BCList;
  BCList["Left"]  = Left;
  BCList["Right"] = Right;
  BCList["Lower"] = Lower;
  BCList[0] = Upper;

  //Check the error handling
  BOOST_CHECK_THROW( BCParam::checkInputs(BCList), KeyTypeException );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_Presentation_test )
{

  PyDict IdealGas;
  IdealGas[NavierStokesParam::params.GasModel.Equation] = NavierStokesParam::params.GasModel.Ideal;
  IdealGas[IdealGasParam::params.gamma] = 1.4;
  IdealGas[IdealGasParam::params.R] = 1;

  PyDict SutherlandViscosity;
  SutherlandViscosity[NavierStokesParam::params.Viscosity.Equation] = NavierStokesParam::params.Viscosity.Sutherland;
  SutherlandViscosity[SutherlandParam::params.RefTemp] = 44.6;
  SutherlandViscosity[SutherlandParam::params.Constant] = 17.1;

  PyDict ConstantViscosity;
  ConstantViscosity[NavierStokesParam::params.Viscosity.Equation] = NavierStokesParam::params.Viscosity.Constant;
  ConstantViscosity[ConstantViscosityParam::params.Constant] = 2e-7;

  PyDict Left;
  Left[BCParam::params.BC.BCType] = BCParam::params.BC.Dirichlet;
  Left[DirichletParam::params.q] = 3.14;

  PyDict Right;
  Right[BCParam::params.BC.BCType] = BCParam::params.BC.Neumann;
  Right[NeumannParam::params.qx] = 18.19;

  PyDict Lower;
  Lower[BCParam::params.BC.BCType] = BCParam::params.BC.Robin;
  Lower[RobinParam::params.q] = 0.123;
  Lower[RobinParam::params.qx] = 1.58;

  PyDict Upper;
  Upper[BCParam::params.BC.BCType] = BCParam::params.BC.Robin;
  Upper[RobinParam::params.q] = 2.589;
  Upper[RobinParam::params.qx] = -42;

  PyDict BCList;
  BCList["Left"]  = Left;
  BCList["Right"] = Right;
  BCList["Lower"] = Lower;
  BCList["Upper"] = Upper;

  PyDict NS;
  NS[SolverParam::params.PDE.Equation] = SolverParam::params.PDE.Navier_Stokes;
  NS[NavierStokesParam::params.Viscosity] = SutherlandViscosity;
  //NS[NavierStokesParam::params.Viscosity] = ConstantViscosity;
  NS[NavierStokesParam::params.GasModel] = IdealGas;

  PyDict Input;
  Input[SolverParam::params.PDE] = NS;
  Input[SolverParam::params.BCs] = BCList;

  //This checks that all the inputs. Should not throw any exceptions for these inputs
  SolverParam::checkInputs(Input);

  DictKeyPair PDE = Input.get(SolverParam::params.PDE);

  //Make sure the dict is the NS dict
  if ( PDE == SolverParam::params.PDE.Navier_Stokes )
  {
    DictKeyPair GasModel = PDE.get(NavierStokesParam::params.GasModel);

    //Check the inputes depending on the model
    if ( GasModel == NavierStokesParam::params.GasModel.Ideal )
    {
      BOOST_CHECK_EQUAL( GasModel.get(IdealGasParam::params.gamma), 1.4 );
      BOOST_CHECK_EQUAL( GasModel.get(IdealGasParam::params.R), 1 );
    }
    else
      BOOST_CHECK( false );

    DictKeyPair Viscosity = PDE.get(NavierStokesParam::params.Viscosity);

    //Check the type of viscosity dict
    if ( Viscosity == NavierStokesParam::params.Viscosity.Sutherland )
    {
      BOOST_CHECK_EQUAL( Viscosity.get(SutherlandParam::params.RefTemp), 44.6 );
      BOOST_CHECK_EQUAL( Viscosity.get(SutherlandParam::params.Constant), 17.1 );
    }
    else if ( Viscosity == NavierStokesParam::params.Viscosity.Constant )
    {
      BOOST_CHECK_EQUAL( Viscosity.get(ConstantViscosityParam::params.Constant), 2e-7 );
    }
    else
      BOOST_CHECK( false );
  }
  else
    BOOST_CHECK( false );


  PyDict BCs = Input.get(SolverParam::params.BCs);

  boost::python::list keys = BCs.keys();

  for (int i = 0; i < boost::python::len(keys); i++)
  {
    boost::python::extract<std::string> key(keys[i]);
    BOOST_REQUIRE( key.check() );

    const ParameterOption<BCParam::BCOptions> BCParam{(std::string)key, NO_DEFAULT, "BC Dictionary"};

    DictKeyPair BC = BCs.get(BCParam);

    if ( BC == BCParam::params.BC.Dirichlet )
    {
      BOOST_CHECK_EQUAL( BC.get(DirichletParam::params.q), 3.14 );
    }
    else if ( BC == BCParam::params.BC.Neumann )
    {
      BOOST_CHECK_EQUAL( BC.get(NeumannParam::params.qx), 18.19 );
    }
    else if ( BC == BCParam::params.BC.Robin )
    {
      if ((std::string)key == "Lower")
      {
        BOOST_CHECK_EQUAL( BC.get(RobinParam::params.q), 0.123 );
        BOOST_CHECK_EQUAL( BC.get(RobinParam::params.qx), 1.58 );
      }
      else if ((std::string)key == "Upper")
      {
        BOOST_CHECK_EQUAL( BC.get(RobinParam::params.q), 2.589 );
        BOOST_CHECK_EQUAL( BC.get(RobinParam::params.qx), -42 );
      }
      else
        BOOST_CHECK( false );
    }
    else
      BOOST_CHECK( false );
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_Transfer_test )
{
  PyDict Left;
  Left[BCParam::params.BC.BCType] = BCParam::params.BC.Dirichlet;
  Left[DirichletParam::params.q] = 3.14;

  PyDict Right;
  Right[BCParam::params.BC.BCType] = BCParam::params.BC.Neumann;
  Right[NeumannParam::params.qx] = 18.19;

  PyDict BCList1;
  BCList1["Left"]  = Left;
  BCList1["Right"]  = Right;

  BCParam::checkInputs(BCList1);

  PyDict BCList2;
  BCList2["Left"]  = BCList1["Left"];
  BCList2["Right"]  = BCList1["Right"];

  BCParam::checkInputs(BCList2);
}


struct FileNameParam : noncopyable
{
  const ParameterFileName FileIn{"FileIn", std::ios_base::in, NO_DEFAULT, "An input file"};
  const ParameterFileName FileOut{"FileOut", std::ios_base::out, "IO/DefaultFile.txt", "An output file"};

  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.FileIn));
    allParams.push_back(d.checkInputs(params.FileOut));
    d.checkUnknownInputs(allParams);
  }

  static FileNameParam params;
};
FileNameParam FileNameParam::params;


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_ParamFileName_test )
{
  std::string inputfilename = "IO/InputFile.txt";
  std::string outputfilename = "IO/OutputFile.txt";

  // create the input file
  std::fstream inputfile(inputfilename, std::ios_base::out);
  inputfile << "something in the file";
  inputfile.close();

  PyDict Files;
  Files[FileNameParam::params.FileIn] = inputfilename;
  Files[FileNameParam::params.FileOut] = outputfilename;

  // should not produce and error
  FileNameParam::checkInputs(Files);

  //The file name is not a string
  Files[FileNameParam::params.FileOut] = 2;
  BOOST_CHECK_THROW( FileNameParam::checkInputs(Files), DictValueTypeException );

  //The output file name is invalid
  Files[FileNameParam::params.FileOut] = "Invalid/file/name/to/directory/that/is/non/existent";
  BOOST_CHECK_THROW( FileNameParam::checkInputs(Files), FileNameException );

  //The output is a valid path, but no file name
  Files[FileNameParam::params.FileOut] = "IO/";
  BOOST_CHECK_THROW( FileNameParam::checkInputs(Files), FileNameException );

  // restore the valid output file
  Files[FileNameParam::params.FileOut] = outputfilename;

  // delete the input file
  std::remove(inputfilename.c_str());

  //The input file does not exist which causes and error
  BOOST_CHECK_THROW( FileNameParam::checkInputs(Files), FileNameException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CheckBCInputs_test )
{

  // Create the boundary groups
  std::map<std::string, std::vector<int> > BoundaryGroups;
  BoundaryGroups["Wall"] = {6,7};
  BoundaryGroups["Inflow"] = {0};
  BoundaryGroups["Outflow"] = {1,2,3,4,5};

  // Create the dictionaries of boundary types
  PyDict BCWallDict;
  BCWallDict["BCType"] = "Wall";

  PyDict BCOutflowDict;
  BCOutflowDict["BCType"] = "Neumann";
  BCOutflowDict["gradqn"] = 0;

  PyDict BCInflowDict;
  BCInflowDict["BCType"] = "Dirichlet";
  BCInflowDict["q"] = 0;

  PyDict PyBCList;
  PyBCList["Wall"] = BCWallDict;
  PyBCList["Inflow"] = BCInflowDict;
  PyBCList["Outflow"] = BCOutflowDict;

  // Keys are consistent, check should pass without exceptions
  checkBCInputs(PyBCList, BoundaryGroups);

  // Add another boundary group missing a BC dictionary type
  BoundaryGroups["Nozzle"] = {8};

  BOOST_CHECK_THROW( checkBCInputs(PyBCList, BoundaryGroups), BCException );

  // Add another boundary type missing boundary group
  PyDict BCFooDict;
  BCFooDict["BCType"] = "Foo";
  PyBCList["Funny"] = BCFooDict;

  BOOST_CHECK_THROW(checkBCInputs(PyBCList, BoundaryGroups), BCException );

  // Now only an extra BC type exists
  BoundaryGroups.erase("Nozzle");

  BOOST_CHECK_THROW(checkBCInputs(PyBCList, BoundaryGroups), BCException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_json_test )
{
  PyDict Left;
  Left[BCParam::params.BC.BCType] = BCParam::params.BC.Dirichlet;
  Left[DirichletParam::params.q] = 3.14;

  PyDict Lower;
  Lower[BCParam::params.BC.BCType] = BCParam::params.BC.Robin;
  Lower[RobinParam::params.q] = 0.123;
  Lower[RobinParam::params.qx] = 1.58;

  PyDict Lists;
  Lists[NumericListParam::params.ListPos] = {0, 1, 2, 4};
  Lists[NumericListParam::params.ListNeg] = {0, -1, -2, -4};
  Lists[NumericListParam::params.ListReal] = {0.1, 3.14, 1.23, 2.56};

  //std::cout << Left.json() << std::endl;
  //std::cout << Lower.json() << std::endl;
  //std::cout << Lists.json() << std::endl;

  PyDict Left2;
  Left2.json_parse(Left.json());

  BOOST_CHECK_EQUAL( Left2.get(BCParam::params.BC.BCType), "Dirichlet" );
  BOOST_CHECK_EQUAL( Left2.get(DirichletParam::params.q), 3.14 );

  PyDict Lower2;
  Lower2.json_parse(Lower.json());

  BOOST_CHECK_EQUAL( Lower2.get(BCParam::params.BC.BCType), "Robin" );
  BOOST_CHECK_EQUAL( Lower2.get(RobinParam::params.q), 0.123 );
  BOOST_CHECK_EQUAL( Lower2.get(RobinParam::params.qx), 1.58 );

  PyDict Lists2;
  Lists2.json_parse(Lists.json());

  std::vector<int> pos = Lists2.get(NumericListParam::params.ListPos);
  BOOST_REQUIRE_EQUAL(4, pos.size());
  BOOST_CHECK_EQUAL(0, pos[0]);
  BOOST_CHECK_EQUAL(1, pos[1]);
  BOOST_CHECK_EQUAL(2, pos[2]);
  BOOST_CHECK_EQUAL(4, pos[3]);

  std::vector<int> neg = Lists2.get(NumericListParam::params.ListNeg);
  BOOST_REQUIRE_EQUAL(4, neg.size());
  BOOST_CHECK_EQUAL( 0, neg[0]);
  BOOST_CHECK_EQUAL(-1, neg[1]);
  BOOST_CHECK_EQUAL(-2, neg[2]);
  BOOST_CHECK_EQUAL(-4, neg[3]);

  std::vector<Real> real = Lists2.get(NumericListParam::params.ListReal);
  BOOST_REQUIRE_EQUAL(4, real.size());
  BOOST_CHECK_EQUAL(  0.1, real[0]);
  BOOST_CHECK_EQUAL( 3.14, real[1]);
  BOOST_CHECK_EQUAL( 1.23, real[2]);
  BOOST_CHECK_EQUAL( 2.56, real[3]);

  PyDict Lists4;
  Lists4.json_parse("{\"ListReal\":[1, -1.1, 2, 2.56]}");
  real = Lists4.get(NumericListParam::params.ListReal);
  BOOST_REQUIRE_EQUAL(4, real.size());
  BOOST_CHECK_EQUAL(    1, real[0]);
  BOOST_CHECK_EQUAL( -1.1, real[1]);
  BOOST_CHECK_EQUAL(    2, real[2]);
  BOOST_CHECK_EQUAL( 2.56, real[3]);

  {
  PyDict PyBCList;
  PyBCList["Left"] = Left;
  PyBCList["Lower"] = Lower;
  //std::cout << PyBCList.json() << std::endl;

  PyDict PyBCList2;
  PyBCList2.json_parse(PyBCList.json());

  const ParameterOption<BCParam::BCOptions> BCParamLeft{"Left", NO_DEFAULT, "BC Dictionary"};
  DictKeyPair BCLeft = PyBCList.get(BCParamLeft);
  PyDict Left2 = BCLeft;

  BOOST_CHECK_EQUAL( Left2.get(BCParam::params.BC.BCType), "Dirichlet" );
  BOOST_CHECK_EQUAL( Left2.get(DirichletParam::params.q), 3.14 );

  const ParameterOption<BCParam::BCOptions> BCParamLower{"Lower", NO_DEFAULT, "BC Dictionary"};
  DictKeyPair BCLower = PyBCList.get(BCParamLower);
  PyDict Lower2 = BCLower;

  BOOST_CHECK_EQUAL( Lower2.get(BCParam::params.BC.BCType), "Robin" );
  BOOST_CHECK_EQUAL( Lower2.get(RobinParam::params.q), 0.123 );
  BOOST_CHECK_EQUAL( Lower2.get(RobinParam::params.qx), 1.58 );
  }
}

//----------------------------------------------------------------------------//
// Turn on the below test cases to see examples of error messages
#if 0

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error1 )
{
  PyDict d;

  //Test a value out of range
  d[PyDictParam::params.gamma] = 0.5;
  d.get(PyDictParam::params.gamma);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error2 )
{
  PyDict d;
  d[PyDictParam::params.gamma] = 2;
  d.get(PyDictParam::params.gamma);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error3 )
{
  PyDict d;
  d[PyDictParam::params.gamma] = "This is not a double";
  d.get(PyDictParam::params.gamma);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error4 )
{
  PyDict d;
  //Test no default value
  d.get(PyDictParam::params.nIter);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error5 )
{
  PyDict d;
  //Test a value out of range
  d[PyDictParam::params.nIter] = -1;
  d.get(PyDictParam::params.nIter);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error6 )
{
  PyDict d;
  //Wron type
  d[PyDictParam::params.nIter] = 1.1;
  d.get(PyDictParam::params.nIter);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error7 )
{
  PyDict d;
  //This is not one of the options
  d[PyDictParam::params.LinearSolver] = "Not a Solver";
  d.get(PyDictParam::params.LinearSolver);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error8 )
{
  PyDict d;
  //This is not one of the options
  d[PyDictParam::params.Order] = 5;
  d.get(PyDictParam::params.Order);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error9 )
{

  PyDict GasModel_in;
  GasModel_in[PyDictParam::params.gamma] = "This is not a double";

  PyDict Optional_in;
  Optional_in[Test2Param::params.GasModel] = GasModel_in;

  PyDict Inputs;
  Inputs[OptionalParam::params.Optional] = Optional_in;

  PyDict Optional = Inputs.get(OptionalParam::params.Optional);
  BOOST_CHECK_EQUAL( Optional.length(), 1 );
  BOOST_CHECK_EQUAL( Optional.getParent(), "Optional" );

  PyDict GasModel = Optional.get(Test2Param::params.GasModel);
  BOOST_CHECK_EQUAL( GasModel.length(), 1 );
  BOOST_CHECK_EQUAL( GasModel.getParent(), "Optional.GasModel" );

  GasModel.get(PyDictParam::params.gamma);

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error10 )
{
  PyDict d;
  //Unknown extra parameters specified
  d["What is this?"] = "I don't know";
  d["Beats me."] = "Oh well.";
  d[BiCGStabParam::params.nIter] = 5;
  BiCGStabParam::checkInputs( d );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error11 )
{
  PyDict d;
  //Unknown extra parameters specified
  d["One parameter"] = 1;
  d["Two parameter"] = 2;
  d["Three!"]        = 3;
  d[PyDictParam::params.nIter] = 5;
  d[PyDictParam::params.Angle] = 2*PI;
  d[PyDictParam::params.Preconditioner] = PyDictParam::params.Preconditioner.ILU0;
  d[PyDictParam::params.Order] = 1;
  d[PyDictParam::params.FileName] = "MyFile";

  PyDict LinearSolver;
  LinearSolver[PyDictParam::params.LinearSolver.Name] = PyDictParam::params.LinearSolver.GMRES;
  LinearSolver[GMRESParam::params.nOuter] = 5;
  LinearSolver[GMRESParam::params.nInner] = 20;
  d[PyDictParam::params.LinearSolver] = LinearSolver;

  PyDictParam::checkInputs( d );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PyDict_show_error12 )
{

  PyDict IdealGas;
  IdealGas[NavierStokesParam::params.GasModel.Equation] = NavierStokesParam::params.GasModel.Ideal;
  IdealGas[IdealGasParam::params.gamma] = "What is this???";
  IdealGas[IdealGasParam::params.R] = 1;

  PyDict SutherlandViscosity;
  SutherlandViscosity[NavierStokesParam::params.Viscosity.Equation] = NavierStokesParam::params.Viscosity.Sutherland;
  SutherlandViscosity[SutherlandParam::params.RefTemp] = 44.6;
  SutherlandViscosity[SutherlandParam::params.Constant] = 17.1;

  PyDict ConstantViscosity;
  ConstantViscosity[NavierStokesParam::params.Viscosity.Equation] = NavierStokesParam::params.Viscosity.Constant;
  ConstantViscosity[ConstantViscosityParam::params.Constant] = 2e-7;

  PyDict NS;
  NS[SolverParam::params.PDE.Equation] = SolverParam::params.PDE.Navier_Stokes;
  NS[NavierStokesParam::params.Viscosity] = SutherlandViscosity;
  //NS[NavierStokesParam::params.Viscosity] = ConstantViscosity;
  NS[NavierStokesParam::params.GasModel] = IdealGas;

  PyDict Input;
  Input[SolverParam::params.PDE] = NS;
}


#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
