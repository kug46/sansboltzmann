// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "Python/Parameter.h"
#include "Python/PyDict.h"

#include <boost/mpl/assert.hpp>
#include <boost/type_traits/is_same.hpp>
#include <iostream>

namespace SANS
{
  //Explicitly instantiate for coverage information
  template struct ParameterNumeric<int>;

  struct EmptyOptions
  {
    typedef std::string ExtractType;
    const std::vector<std::string> options{};
  };
  template struct ParameterOption<EmptyOptions>;
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Python )

namespace // private to this file
{

struct GMRES1Param : noncopyable
{
  const ParameterNumeric<int> nOuter{"nOuter", 10 , 0, NO_LIMIT, "Number of Outer Iterations"};
  const ParameterNumeric<int> nInner{"nInner", 100, 0, NO_LIMIT, "Number of Inner Iterations"};

  //cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //Intentionally left void to decouple from PyDict
  }
};

struct BiCGStab1Param : noncopyable
{
  const ParameterNumeric<int> nIter{"nIter", NO_DEFAULT, 0, NO_LIMIT, "Number of Iterations"};

  //cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //Intentionally left void to decouple from PyDict
  }
};


struct IdealGasTestParam : noncopyable
{
  const ParameterNumeric<Real> gamma{"gamma", 1.4    , 1, 5./3.   , "Specific heat ratio"};
  const ParameterNumeric<Real> R    {"R"    , 287.04 , 0, NO_LIMIT, "Gas Constant"};

  //cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //Intentionally left void to decouple from PyDict
  }
};


struct Test1Param : noncopyable
{
  const ParameterNumeric<int>  Num0{"Num0"  , 50        , 10      , 100     , "Num0 help"};
  const ParameterNumeric<int>  Num1{"Num1"  , 50        , NO_LIMIT, 100     , "Num1 help"};
  const ParameterNumeric<int>  Num2{"Num2"  , 50        , 10      , NO_LIMIT, "Num2 help"};

  const ParameterNumeric<int>  Num3{"Num3"  , NO_DEFAULT, 10      , 100     , "Num3 help"};
  const ParameterNumeric<int>  Num4{"Num4"  , NO_DEFAULT, NO_LIMIT, 100     , "Num4 help"};
  const ParameterNumeric<int>  Num5{"Num5"  , NO_DEFAULT, 10      , NO_LIMIT, "Num5 help"};

  const ParameterNumeric<int>  Num6{"Num6"  , 50        , NO_RANGE, "Num6 help"};
  const ParameterNumeric<int>  Num7{"Num7"  , NO_DEFAULT, NO_RANGE, "Num7 help"};

  const ParameterNumeric<Real> gamma{"gamma", 1.4       , 1       , 5./3.   , "Specific heat ratio"};
  const ParameterNumeric<int>  nIter{"nIter", NO_DEFAULT, 0       , NO_LIMIT, "Number of Iterations"};
  const ParameterNumeric<Real> Angle{"Angle", NO_DEFAULT, NO_RANGE          , "An angle"};
  const ParameterNumeric<Real> Beta {"Beta" , 0         , -10     , 0       , "A limited thing"};

  const ParameterString FileName{"FileName", NO_DEFAULT, "A file name"};
  const ParameterString Suffix {"Suffix", ".dat", "File suffix"};

  const ParameterBool bool1{"bool1", NO_DEFAULT, "A boolean 1"};
  const ParameterBool bool2{"bool2", true      , "A boolean 2"};

  struct BasisFunctionOptions
  {
    typedef std::string ExtractType;
    const std::string Lagrange = "Lagrange";
    const std::string Legendre = "Legendre";

    const std::vector<std::string> options{Lagrange,Legendre};
  };
  const ParameterOption<BasisFunctionOptions> BasisFunction{"BasisFunction", "Lagrange", "Polynomial Basis Function"};

  struct PreconditionerOptions
  {
    typedef std::string ExtractType;
    const std::string ILU0 = "ILU0";

    const std::vector<std::string> options{ILU0};
  };
  const ParameterOption<PreconditionerOptions> Preconditioner{"Preconditioner", NO_DEFAULT, "Preconitioner for the iterative linear solver"};


  struct OrderOptions
  {
    typedef int ExtractType;
    enum Options
    {
      i0, i1, i2, i3
    };
    const std::vector<int> options{i0, i1, i2, i3};
  };
  const ParameterOption<OrderOptions> Order =
   ParameterOption<OrderOptions>("Order", 0, "Polynomial Order");

  struct RKOrderOptions
  {
    typedef int ExtractType;
    enum Options
    {
      i1 = 1,
      i2 = 2,
      i4 = 4
    };
    const std::vector<int> options{i1, i2, i4};
  };
  const ParameterOption<RKOrderOptions> RKOrder{"RKOrder", NO_DEFAULT, "Runge-Kutta Order"};

  struct LinearSolverOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Iterative Solver Name"};
    const ParameterString& key = Name;

    const DictOption GMRES   {"GMRES", GMRES1Param::checkInputs};
    const DictOption BiCGStab{"BiCGStab", BiCGStab1Param::checkInputs};

    const std::vector<DictOption> options{GMRES,BiCGStab};
  };
  const ParameterOption<LinearSolverOptions> LinearSolver{"LinearSolver", NO_DEFAULT, "Iterative Linear Solver"};

  struct GasModelOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Equation{"Equation", "Ideal", "Gas Model Equation"};
    const ParameterString& key = Equation;

    const DictOption Ideal = DictOption("Ideal", IdealGasTestParam::checkInputs);

    const std::vector<DictOption> options{Ideal};
  };
  const ParameterOption<GasModelOptions> GasModel{"GasModel", EMPTY_DICT, "Gas Model Dictionary"};

  /*
  PARAMETER_DICT_OPTION( 11, LinearSolver, NO_DEFAULT,
                         Name, "GMRES", "Iterative Solver Name",
                         ( (GMRES)(GMRESParam) )
                         ( (BiCGStab)(BiCGStabParam) )
                         , "The iterative linear solver" );
*/
  //cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //Intentionally left void to decouple from PyDict
  }

  static Test1Param params;
};

Test1Param Test1Param::params;

struct DictParam : noncopyable
{
  //PARAMETER_DICT( 0, Test, NO_DEFAULT, Test1Param, "A dict of params" );
  const ParameterDict Test{"Test", NO_DEFAULT, Test1Param::checkInputs, "A dict of params"};

  static DictParam params;
};
DictParam DictParam::params;


struct NumericList : noncopyable
{
  const ParameterNumericList<int> List1{"List1", EMPTY_LIST,       -1,        1, "List1 help"};
  const ParameterNumericList<int> List2{"List2", EMPTY_LIST, NO_LIMIT,        1, "List2 help"};
  const ParameterNumericList<int> List3{"List3", EMPTY_LIST,       -1, NO_LIMIT, "List3 help"};

  const ParameterNumericList<int> List4{"List4", NO_DEFAULT,       -1,        1, "List4 help"};
  const ParameterNumericList<int> List5{"List5", NO_DEFAULT, NO_LIMIT,        1, "List5 help"};
  const ParameterNumericList<int> List6{"List6", NO_DEFAULT,       -1, NO_LIMIT, "List6 help"};

  const ParameterNumericList<int> List7{"List7", EMPTY_LIST, NO_RANGE, "List7 help"};
  const ParameterNumericList<int> List8{"List8", NO_DEFAULT, NO_RANGE, "List8 help"};

  const ParameterNumericList<double> List9{"List9", {0.025, 0.001, 15.0}, 0, NO_LIMIT, "List9 help"};

  static NumericList params;
};
NumericList NumericList::params;


struct FileNameParam : noncopyable
{
  const ParameterFileName FileIn{"FileIn", std::ios_base::in, NO_DEFAULT, "An input file"};
  const ParameterFileName FileOut{"FileOut", std::ios_base::out, "DefaultFile.txt", "An output file"};

  static FileNameParam params;
};
FileNameParam FileNameParam::params;

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Test1Param_test )
{
  BOOST_CHECK( "gamma" == Test1Param::params.gamma ); //For coverage
  BOOST_CHECK( std::string("gamma") == (const char*)Test1Param::params.gamma ); //For coverage

  BOOST_CHECK_EQUAL( Test1Param::params.Num0, "Num0" );
  BOOST_CHECK_EQUAL( Test1Param::params.Num0.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num0.Default, 50 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num0.hasMin, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num0.min, 10 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num0.hasMax, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num0.max, 100 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num0.help, "Num0 help" );

  BOOST_CHECK_EQUAL( Test1Param::params.Num1, "Num1" );
  BOOST_CHECK_EQUAL( Test1Param::params.Num1.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num1.Default, 50 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num1.hasMin, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num1.min, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num1.hasMax, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num1.max, 100 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num1.help, "Num1 help" );

  BOOST_CHECK_EQUAL( Test1Param::params.Num2, "Num2" );
  BOOST_CHECK_EQUAL( Test1Param::params.Num2.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num2.Default, 50 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num2.hasMin, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num2.min, 10 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num2.hasMax, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num2.max, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num2.help, "Num2 help" );


  BOOST_CHECK_EQUAL( Test1Param::params.Num3, "Num3" );
  BOOST_CHECK_EQUAL( Test1Param::params.Num3.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num3.Default, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num3.hasMin, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num3.min, 10 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num3.hasMax, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num3.max, 100 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num3.help, "Num3 help" );

  BOOST_CHECK_EQUAL( Test1Param::params.Num4, "Num4" );
  BOOST_CHECK_EQUAL( Test1Param::params.Num4.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num4.Default, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num4.hasMin, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num4.min, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num4.hasMax, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num4.max, 100 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num4.help, "Num4 help" );

  BOOST_CHECK_EQUAL( Test1Param::params.Num5, "Num5" );
  BOOST_CHECK_EQUAL( Test1Param::params.Num5.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num5.Default, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num5.hasMin, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num5.min, 10 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num5.hasMax, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num5.max, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num5.help, "Num5 help" );


  BOOST_CHECK_EQUAL( Test1Param::params.Num6, "Num6" );
  BOOST_CHECK_EQUAL( Test1Param::params.Num6.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Num6.Default, 50 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num6.hasMin, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num6.min, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num6.hasMax, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num6.max, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num6.help, "Num6 help" );

  BOOST_CHECK_EQUAL( Test1Param::params.Num7, "Num7" );
  BOOST_CHECK_EQUAL( Test1Param::params.Num7.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num7.Default, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num7.hasMin, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num7.min, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num7.hasMax, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Num7.max, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Num7.help, "Num7 help" );


  BOOST_CHECK_EQUAL( Test1Param::params.gamma, "gamma" );
  BOOST_CHECK_EQUAL( Test1Param::params.gamma.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.gamma.Default, 1.4 );
  BOOST_CHECK_EQUAL( Test1Param::params.gamma.hasMin, true );
  BOOST_CHECK_EQUAL( Test1Param::params.gamma.min, 1 );
  BOOST_CHECK_EQUAL( Test1Param::params.gamma.hasMax, true );
  BOOST_CHECK_EQUAL( Test1Param::params.gamma.max, 5./3. );
  BOOST_CHECK_EQUAL( Test1Param::params.gamma.help, "Specific heat ratio" );

  BOOST_CHECK_EQUAL( Test1Param::params.nIter, "nIter" );
  BOOST_CHECK_EQUAL( Test1Param::params.nIter.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.nIter.Default, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.nIter.hasMin, true );
  BOOST_CHECK_EQUAL( Test1Param::params.nIter.min, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.nIter.hasMax, false );
  BOOST_CHECK_EQUAL( Test1Param::params.nIter.max, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.nIter.help, "Number of Iterations" );

  BOOST_CHECK_EQUAL( Test1Param::params.Angle, "Angle" );
  BOOST_CHECK_EQUAL( Test1Param::params.Angle.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Angle.Default, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Angle.hasMin, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Angle.min, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Angle.hasMax, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Angle.max, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Angle.help, "An angle" );

  BOOST_CHECK_EQUAL( Test1Param::params.Beta, "Beta" );
  BOOST_CHECK_EQUAL( Test1Param::params.Beta.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Beta.Default, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Beta.hasMin, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Beta.min, -10 );
  BOOST_CHECK_EQUAL( Test1Param::params.Beta.hasMax, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Beta.max, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Beta.help, "A limited thing" );



  BOOST_CHECK_EQUAL( Test1Param::params.FileName, "FileName" );
  BOOST_CHECK_EQUAL( Test1Param::params.FileName.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.FileName.Default, "" );
  BOOST_CHECK_EQUAL( Test1Param::params.FileName.help, "A file name" );

  BOOST_CHECK_EQUAL( Test1Param::params.Suffix, "Suffix" );
  BOOST_CHECK_EQUAL( Test1Param::params.Suffix.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Suffix.Default, ".dat" );
  BOOST_CHECK_EQUAL( Test1Param::params.Suffix.help, "File suffix" );



  BOOST_CHECK_EQUAL( Test1Param::params.bool1, "bool1" );
  BOOST_CHECK_EQUAL( Test1Param::params.bool1.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.bool1.Default, false );
  BOOST_CHECK_EQUAL( Test1Param::params.bool1.help, "A boolean 1" );

  BOOST_CHECK_EQUAL( Test1Param::params.bool2, "bool2" );
  BOOST_CHECK_EQUAL( Test1Param::params.bool2.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.bool2.Default, true );
  BOOST_CHECK_EQUAL( Test1Param::params.bool2.help, "A boolean 2" );



  BOOST_CHECK_EQUAL( Test1Param::params.BasisFunction, "BasisFunction" );
  BOOST_CHECK_EQUAL( Test1Param::params.BasisFunction.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.BasisFunction.Default, "Lagrange" );
  BOOST_CHECK_EQUAL( Test1Param::params.BasisFunction.Lagrange, "Lagrange" );
  BOOST_CHECK_EQUAL( Test1Param::params.BasisFunction.Legendre, "Legendre" );

  BOOST_CHECK_EQUAL( Test1Param::params.BasisFunction.options.size(), std::size_t(2) );
  BOOST_CHECK_EQUAL( Test1Param::params.BasisFunction.options[0], "Lagrange" );
  BOOST_CHECK_EQUAL( Test1Param::params.BasisFunction.options[1], "Legendre" );
  BOOST_CHECK_EQUAL( Test1Param::params.BasisFunction.help, "Polynomial Basis Function" );

  BOOST_CHECK_EQUAL( Test1Param::params.Preconditioner, "Preconditioner" );
  BOOST_CHECK_EQUAL( Test1Param::params.Preconditioner.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.Preconditioner.Default, "" );
  BOOST_CHECK_EQUAL( Test1Param::params.Preconditioner.ILU0, "ILU0" );

  BOOST_CHECK_EQUAL( Test1Param::params.Preconditioner.options.size(), std::size_t(1) );
  BOOST_CHECK_EQUAL( Test1Param::params.Preconditioner.options[0], "ILU0" );
  BOOST_CHECK_EQUAL( Test1Param::params.Preconditioner.help, "Preconitioner for the iterative linear solver" );


  BOOST_CHECK_EQUAL( Test1Param::params.Order, "Order" );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.Default, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.i0, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.i1, 1 );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.i2, 2 );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.i3, 3 );

  BOOST_CHECK_EQUAL( Test1Param::params.Order.options.size(), std::size_t(4) );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.options[0], 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.options[1], 1 );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.options[2], 2 );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.options[3], 3 );
  BOOST_CHECK_EQUAL( Test1Param::params.Order.help, "Polynomial Order" );

  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder, "RKOrder" );
  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder.Default, 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder.i1, 1 );
  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder.i2, 2 );
  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder.i4, 4 );

  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder.options.size(), std::size_t(3) );
  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder.options[0], 1 );
  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder.options[1], 2 );
  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder.options[2], 4 );
  BOOST_CHECK_EQUAL( Test1Param::params.RKOrder.help, "Runge-Kutta Order" );


  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver, "LinearSolver" );
  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver.hasDefault, false );
  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver.Default.length(), 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver.Default, std::string() );
  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver.Name, "Name" );
  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver.key, "Name" );
  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver.GMRES, "GMRES" );
  BOOST_CHECK( "BiCGStab" == Test1Param::params.LinearSolver.BiCGStab ); //Reversed order for coverage

  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver.options.size(), std::size_t(2) );
  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver.options[0], "GMRES" );
  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver.options[1], "BiCGStab" );
  BOOST_CHECK_EQUAL( Test1Param::params.LinearSolver.help, "Iterative Linear Solver" );

  BOOST_CHECK_EQUAL( Test1Param::params.GasModel, "GasModel" );
  BOOST_CHECK_EQUAL( Test1Param::params.GasModel.hasDefault, true );
  BOOST_CHECK_EQUAL( Test1Param::params.GasModel.Default.length(), 0 );
  BOOST_CHECK_EQUAL( Test1Param::params.GasModel.Default, std::string() );
  BOOST_CHECK_EQUAL( Test1Param::params.GasModel.Equation, "Equation" );
  BOOST_CHECK_EQUAL( Test1Param::params.GasModel.key, "Equation" );
  BOOST_CHECK_EQUAL( Test1Param::params.GasModel.Ideal, "Ideal" );

  BOOST_CHECK_EQUAL( Test1Param::params.GasModel.options.size(), std::size_t(1) );
  BOOST_CHECK_EQUAL( Test1Param::params.GasModel.options[0], "Ideal" );
  BOOST_CHECK_EQUAL( Test1Param::params.GasModel.help, "Gas Model Dictionary" );


  BOOST_CHECK_EQUAL( NumericList::params.List1, "List1" );
  BOOST_CHECK_EQUAL( NumericList::params.List1.hasDefault, true );
  BOOST_CHECK_EQUAL( NumericList::params.List1.hasMin, true );
  BOOST_CHECK_EQUAL( NumericList::params.List1.min, -1 );
  BOOST_CHECK_EQUAL( NumericList::params.List1.hasMax, true );
  BOOST_CHECK_EQUAL( NumericList::params.List1.max,  1 );
  BOOST_CHECK_EQUAL( NumericList::params.List1.help, "List1 help" );

  BOOST_CHECK_EQUAL( NumericList::params.List2, "List2" );
  BOOST_CHECK_EQUAL( NumericList::params.List2.hasDefault, true );
  BOOST_CHECK_EQUAL( NumericList::params.List2.hasMin, false );
  BOOST_CHECK_EQUAL( NumericList::params.List2.min, 0 );
  BOOST_CHECK_EQUAL( NumericList::params.List2.hasMax, true );
  BOOST_CHECK_EQUAL( NumericList::params.List2.max, 1 );
  BOOST_CHECK_EQUAL( NumericList::params.List2.help, "List2 help" );

  BOOST_CHECK_EQUAL( NumericList::params.List3, "List3" );
  BOOST_CHECK_EQUAL( NumericList::params.List3.hasDefault, true );
  BOOST_CHECK_EQUAL( NumericList::params.List3.hasMin, true );
  BOOST_CHECK_EQUAL( NumericList::params.List3.min, -1 );
  BOOST_CHECK_EQUAL( NumericList::params.List3.hasMax, false );
  BOOST_CHECK_EQUAL( NumericList::params.List3.max, 0 );
  BOOST_CHECK_EQUAL( NumericList::params.List3.help, "List3 help" );


  BOOST_CHECK_EQUAL( NumericList::params.List4, "List4" );
  BOOST_CHECK_EQUAL( NumericList::params.List4.hasDefault, false );
  BOOST_CHECK_EQUAL( NumericList::params.List4.hasMin, true );
  BOOST_CHECK_EQUAL( NumericList::params.List4.min, -1 );
  BOOST_CHECK_EQUAL( NumericList::params.List4.hasMax, true );
  BOOST_CHECK_EQUAL( NumericList::params.List4.max,  1 );
  BOOST_CHECK_EQUAL( NumericList::params.List4.help, "List4 help" );

  BOOST_CHECK_EQUAL( NumericList::params.List5, "List5" );
  BOOST_CHECK_EQUAL( NumericList::params.List5.hasDefault, false );
  BOOST_CHECK_EQUAL( NumericList::params.List5.hasMin, false );
  BOOST_CHECK_EQUAL( NumericList::params.List5.min, 0 );
  BOOST_CHECK_EQUAL( NumericList::params.List5.hasMax, true );
  BOOST_CHECK_EQUAL( NumericList::params.List5.max, 1 );
  BOOST_CHECK_EQUAL( NumericList::params.List5.help, "List5 help" );

  BOOST_CHECK_EQUAL( NumericList::params.List6, "List6" );
  BOOST_CHECK_EQUAL( NumericList::params.List6.hasDefault, false );
  BOOST_CHECK_EQUAL( NumericList::params.List6.hasMin, true );
  BOOST_CHECK_EQUAL( NumericList::params.List6.min, -1 );
  BOOST_CHECK_EQUAL( NumericList::params.List6.hasMax, false );
  BOOST_CHECK_EQUAL( NumericList::params.List6.max, 0 );
  BOOST_CHECK_EQUAL( NumericList::params.List6.help, "List6 help" );


  BOOST_CHECK_EQUAL( NumericList::params.List7, "List7" );
  BOOST_CHECK_EQUAL( NumericList::params.List7.hasDefault, true );
  BOOST_CHECK_EQUAL( NumericList::params.List7.hasMin, false );
  BOOST_CHECK_EQUAL( NumericList::params.List7.min, 0 );
  BOOST_CHECK_EQUAL( NumericList::params.List7.hasMax, false );
  BOOST_CHECK_EQUAL( NumericList::params.List7.max, 0 );
  BOOST_CHECK_EQUAL( NumericList::params.List7.help, "List7 help" );

  BOOST_CHECK_EQUAL( NumericList::params.List8, "List8" );
  BOOST_CHECK_EQUAL( NumericList::params.List8.hasDefault, false );
  BOOST_CHECK_EQUAL( NumericList::params.List8.hasMin, false );
  BOOST_CHECK_EQUAL( NumericList::params.List8.min, 0 );
  BOOST_CHECK_EQUAL( NumericList::params.List8.hasMax, false );
  BOOST_CHECK_EQUAL( NumericList::params.List8.max, 0 );
  BOOST_CHECK_EQUAL( NumericList::params.List8.help, "List8 help" );


  BOOST_CHECK_EQUAL( NumericList::params.List9, "List9" );
  BOOST_CHECK_EQUAL( NumericList::params.List9.hasDefault, true );
  BOOST_CHECK_EQUAL( NumericList::params.List9.hasMin, true );
  BOOST_CHECK_EQUAL( NumericList::params.List9.min, 0 );
  BOOST_CHECK_EQUAL( NumericList::params.List9.hasMax, false );
  BOOST_CHECK_EQUAL( NumericList::params.List9.max, 0 );
  BOOST_CHECK_EQUAL( NumericList::params.List9.help, "List9 help" );
  BOOST_REQUIRE_EQUAL( NumericList::params.List9.Default.size(), 3 );
  BOOST_CHECK_EQUAL( NumericList::params.List9.Default[0], 0.025 );
  BOOST_CHECK_EQUAL( NumericList::params.List9.Default[1], 0.001 );
  BOOST_CHECK_EQUAL( NumericList::params.List9.Default[2], 15.00 );
  BOOST_CHECK_EQUAL( NumericList::params.List9.default_str(), "[0.025, 0.001, 15]" );

  BOOST_CHECK_EQUAL( FileNameParam::params.FileIn, "FileIn" );
  BOOST_CHECK_EQUAL( FileNameParam::params.FileIn.hasDefault, false );
  BOOST_CHECK_EQUAL( FileNameParam::params.FileIn.mode, std::ios_base::in );
  BOOST_CHECK_EQUAL( FileNameParam::params.FileIn.Default, "" );
  BOOST_CHECK_EQUAL( FileNameParam::params.FileIn.help, "An input file" );

  BOOST_CHECK_EQUAL( FileNameParam::params.FileOut, "FileOut" );
  BOOST_CHECK_EQUAL( FileNameParam::params.FileOut.hasDefault, true );
  BOOST_CHECK_EQUAL( FileNameParam::params.FileOut.mode, std::ios_base::out );
  BOOST_CHECK_EQUAL( FileNameParam::params.FileOut.Default, "DefaultFile.txt" );
  BOOST_CHECK_EQUAL( FileNameParam::params.FileOut.help, "An output file" );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
