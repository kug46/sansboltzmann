// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Quaternion_btest
// testing of Quaternion class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"  // Real
#include "Surreal/SurrealS.h"    // Surreal
#include "Quaternion/Quaternion.h"

#include <ostream>

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class Quaternion<Real>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Quaternion_test_suite )

typedef DLA::VectorS<3,Real> Vector;
typedef DLA::VectorS<4,Real> Vector4;
typedef DLA::MatrixS<3,3,Real> Matrix3;
typedef DLA::MatrixS<4,4,Real> Matrix4;
typedef Quaternion<Real> QReal;
typedef Quaternion< SurrealS<1> > QSurreal;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ctors_access )
{
  Real wTrue = 1;
  Real xTrue = 2;
  Real yTrue = 3;
  Real zTrue = 4;
  Real thetaTrue = 1./2.;

  Vector v1   = { xTrue, yTrue, zTrue };
  Real   v2[] = { xTrue, yTrue, zTrue };

  QReal q1( wTrue, xTrue, yTrue, zTrue );
  QReal q2( q1 );
  QReal q3 = q1;
  QReal q4( wTrue, v1 );
  QReal q5( wTrue, v2, 3 );
  QReal q6( v1, thetaTrue );
  QReal q7( v2, 3, thetaTrue );

  // accessors
  BOOST_CHECK_EQUAL( wTrue, q1.w() );
  BOOST_CHECK_EQUAL( xTrue, q1.x() );
  BOOST_CHECK_EQUAL( yTrue, q1.y() );
  BOOST_CHECK_EQUAL( zTrue, q1.z() );
  BOOST_CHECK( q1.isUnit() == false );

  BOOST_CHECK_EQUAL( wTrue, q2.w() );
  BOOST_CHECK_EQUAL( xTrue, q2.x() );
  BOOST_CHECK_EQUAL( yTrue, q2.y() );
  BOOST_CHECK_EQUAL( zTrue, q2.z() );
  BOOST_CHECK( q1.isUnit() == false );

  BOOST_CHECK_EQUAL( wTrue, q3.w() );
  BOOST_CHECK_EQUAL( xTrue, q3.x() );
  BOOST_CHECK_EQUAL( yTrue, q3.y() );
  BOOST_CHECK_EQUAL( zTrue, q3.z() );
  BOOST_CHECK( q1.isUnit() == false );

  BOOST_CHECK_EQUAL( wTrue, q4.w() );
  BOOST_CHECK_EQUAL( xTrue, q4.x() );
  BOOST_CHECK_EQUAL( yTrue, q4.y() );
  BOOST_CHECK_EQUAL( zTrue, q4.z() );
  BOOST_CHECK( q1.isUnit() == false );

  BOOST_CHECK_EQUAL( wTrue, q5.w() );
  BOOST_CHECK_EQUAL( xTrue, q5.x() );
  BOOST_CHECK_EQUAL( yTrue, q5.y() );
  BOOST_CHECK_EQUAL( zTrue, q5.z() );
  BOOST_CHECK( q1.isUnit() == false );

  // q6 and q7 are quaternions constructed from an angle about an axis
  Real vNorm = sqrt( pow(xTrue,2) + pow(yTrue,2) + pow(zTrue,2) );
  wTrue = cos( thetaTrue/2 );
  xTrue = sin( thetaTrue/2 ) * xTrue/vNorm;
  yTrue = sin( thetaTrue/2 ) * yTrue/vNorm;
  zTrue = sin( thetaTrue/2 ) * zTrue/vNorm;

  BOOST_CHECK_EQUAL( wTrue, q6.w() );
  BOOST_CHECK_EQUAL( xTrue, q6.x() );
  BOOST_CHECK_EQUAL( yTrue, q6.y() );
  BOOST_CHECK_EQUAL( zTrue, q6.z() );
  BOOST_CHECK( q6.isUnit() == true );

  BOOST_CHECK_EQUAL( wTrue, q7.w() );
  BOOST_CHECK_EQUAL( xTrue, q7.x() );
  BOOST_CHECK_EQUAL( yTrue, q7.y() );
  BOOST_CHECK_EQUAL( zTrue, q7.z() );
  BOOST_CHECK( q7.isUnit() == true );

  // SHOULD I CHECK WITH SURREALS????? YES, EVENTUALLY...

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( member_functions )
{
  Real tol = 2e-13;

  Real w = 1, x = 2, y = 3, z = 4;
  QReal q1( w, x, y, z );
  QReal q2( q1 );

  Real magnitude = sqrt( pow(w,2) + pow(x,2) + pow(y,2) + pow(z,2) );

  Real wTrue = w/magnitude;
  Real xTrue = x/magnitude;
  Real yTrue = y/magnitude;
  Real zTrue = z/magnitude;

  q1.normalize();

  BOOST_CHECK_EQUAL( wTrue, q1.w() );
  BOOST_CHECK_EQUAL( xTrue, q1.x() );
  BOOST_CHECK_EQUAL( yTrue, q1.y() );
  BOOST_CHECK_EQUAL( zTrue, q1.z() );
  BOOST_CHECK( q1.isUnit() == true );

  Vector r = { -1, 4, 2 };

  BOOST_CHECK_THROW( q2.rotate(r); , AssertionException ); // q2 is non-unit

  // Use rotation matrix form to check quaternion conjugation implemented in rotate method
  Matrix3 Tbar = {{ 1-2*(pow(yTrue,2)+pow(zTrue,2)),  2*(xTrue*yTrue-wTrue*zTrue)   ,  2*(xTrue*zTrue+wTrue*yTrue)    },
                  {  2*(xTrue*yTrue+wTrue*zTrue)   , 1-2*(pow(xTrue,2)+pow(zTrue,2)),  2*(yTrue*zTrue-wTrue*xTrue)    },
                  {  2*(xTrue*zTrue-wTrue*yTrue)   ,  2*(yTrue*zTrue+wTrue*xTrue)   , 1-2*(pow(xTrue,2)+pow(yTrue,2)) }};

  Vector rRotTrue = Tbar*r;
  Vector rRot     = q1.rotate(r);

  SANS_CHECK_CLOSE( rRotTrue[0], rRot[0], tol, tol );
  SANS_CHECK_CLOSE( rRotTrue[1], rRot[1], tol, tol );
  SANS_CHECK_CLOSE( rRotTrue[2], rRot[2], tol, tol );

  BOOST_CHECK_THROW( q2.invRotate(r); , AssertionException );

  Vector rRotInv = q1.invRotate(rRot);

  SANS_CHECK_CLOSE( r[0], rRotInv[0], tol, tol );
  SANS_CHECK_CLOSE( r[1], rRotInv[1], tol, tol );
  SANS_CHECK_CLOSE( r[2], rRotInv[2], tol, tol );

  x = 2./7., y = 3./7., z = 6./7.;
  Vector axis = { x, y, z };
  Real theta = 1;
  QReal qAngleAxis( axis, theta );

  SANS_CHECK_CLOSE( theta, qAngleAxis.getAngle(), tol, tol );
  SANS_CHECK_CLOSE( axis[0] , qAngleAxis.getAxis()[0] , tol, tol );
  SANS_CHECK_CLOSE( axis[1] , qAngleAxis.getAxis()[1] , tol, tol );
  SANS_CHECK_CLOSE( axis[2] , qAngleAxis.getAxis()[2] , tol, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( quaternion_operations )
{
  Real tol = 2e-13;

  Real w = 1, x = 2, y = 3, z = 4;
  QReal qTrue( w, x, y, z );

  QReal q1 = unit(qTrue);

  qTrue.normalize();

  SANS_CHECK_CLOSE( qTrue.w(), q1.w(), tol, tol );
  SANS_CHECK_CLOSE( qTrue.x(), q1.x(), tol, tol );
  SANS_CHECK_CLOSE( qTrue.y(), q1.y(), tol, tol );
  SANS_CHECK_CLOSE( qTrue.z(), q1.z(), tol, tol );
  BOOST_CHECK( q1.isUnit() == true );

  QReal q2( w, x, y, z );

  QReal q1Conj = conj(q1); // Unit conjugate
  QReal q2Conj = conj(q2); // Non-unit conjugate

  SANS_CHECK_CLOSE(  qTrue.w(), q1Conj.w(), tol, tol );
  SANS_CHECK_CLOSE( -qTrue.x(), q1Conj.x(), tol, tol );
  SANS_CHECK_CLOSE( -qTrue.y(), q1Conj.y(), tol, tol );
  SANS_CHECK_CLOSE( -qTrue.z(), q1Conj.z(), tol, tol );
  BOOST_CHECK( q1Conj.isUnit() == true );

  SANS_CHECK_CLOSE(  w, q2Conj.w(), tol, tol );
  SANS_CHECK_CLOSE( -x, q2Conj.x(), tol, tol );
  SANS_CHECK_CLOSE( -y, q2Conj.y(), tol, tol );
  SANS_CHECK_CLOSE( -z, q2Conj.z(), tol, tol );
  BOOST_CHECK( q2Conj.isUnit() == false );


}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( vector_operations )
{
  Real tol = 2e-13;

  Real w = 1, x = 2, y = 3, z = 4;
  QReal q1( w,  x, y,  z );
  QReal q2( x, -y, z, -w );

  Real magTrue = sqrt( pow(w,2) + pow(x,2) + pow(y,2) + pow(z,2) );
  Real mag1    = magnitude( q1 );
  Real mag2    = magnitude( q2 );

  SANS_CHECK_CLOSE( magTrue, mag1, tol, tol );
  SANS_CHECK_CLOSE( magTrue, mag2, tol, tol );

  Real dtTrue = w*x - x*y + y*z - z*w;
  Real dt1    = dot( q1, q2 );
  Real dt2    = dot( q2, q1 );

  SANS_CHECK_CLOSE( dtTrue, dt1, tol, tol );
  SANS_CHECK_CLOSE( dtTrue, dt2, tol, tol );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( binary_operators )
{
  Real tol = 2e-13;

  Real w = 1, x = 2, y = 3, z = 4;
  QReal q1( w,  x, y,  z );
  QReal q2( x, -y, z, -w );

  Real wTrue = w+x;
  Real xTrue = x-y;
  Real yTrue = y+z;
  Real zTrue = z-w;

  QReal q3 = q1 + q2;

  SANS_CHECK_CLOSE( wTrue, q3.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, q3.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, q3.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, q3.z(), tol, tol );
  BOOST_CHECK( q3.isUnit() == false );

  wTrue = w-x;
  xTrue = x+y;
  yTrue = y-z;
  zTrue = z+w;

  QReal q4 = q1 - q2;

  SANS_CHECK_CLOSE( wTrue, q4.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, q4.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, q4.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, q4.z(), tol, tol );
  BOOST_CHECK( q4.isUnit() == false );

  // Use matrix multiplication form to check explicit element assignment implemented in operator*
  Matrix4 Q1 = {{ w, -x, -y, -z },
                { x,  w, -z,  y },
                { y,  z,  w, -x },
                { z, -y,  x,  w }};
  Vector4 Q2 = { x, -y, z, -w };

  Vector4 q1q2True = Q1*Q2;

  QReal q1q2 = q1*q2;

  SANS_CHECK_CLOSE( q1q2True[0], q1q2.w(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[1], q1q2.x(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[2], q1q2.y(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[3], q1q2.z(), tol, tol );
  BOOST_CHECK( q1q2.isUnit() == false );

  q1q2True *= 2;

  QReal q1q2double1 = q1q2 * 2.0;
  QReal q1q2double2 = 2.0  * q1q2;
  QReal q1q2int1    = q1q2 * 2;
  QReal q1q2int2    = 2    * q1q2;

  SANS_CHECK_CLOSE( q1q2True[0], q1q2double1.w(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[1], q1q2double1.x(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[2], q1q2double1.y(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[3], q1q2double1.z(), tol, tol );
  BOOST_CHECK( q1q2double1.isUnit() == false );

  SANS_CHECK_CLOSE( q1q2True[0], q1q2double2.w(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[1], q1q2double2.x(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[2], q1q2double2.y(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[3], q1q2double2.z(), tol, tol );
  BOOST_CHECK( q1q2double2.isUnit() == false );

  SANS_CHECK_CLOSE( q1q2True[0], q1q2int1.w(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[1], q1q2int1.x(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[2], q1q2int1.y(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[3], q1q2int1.z(), tol, tol );
  BOOST_CHECK( q1q2int1.isUnit() == false );

  SANS_CHECK_CLOSE( q1q2True[0], q1q2int2.w(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[1], q1q2int2.x(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[2], q1q2int2.y(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[3], q1q2int2.z(), tol, tol );
  BOOST_CHECK( q1q2int2.isUnit() == false );

  // Test unit flag with unit quaternions and multiplying by 1
  Real magnitude = sqrt( pow(w,2) + pow(x,2) + pow(y,2) + pow(z,2) );

  Q1 /= magnitude;
  Q2 /= magnitude;
  q1q2True = Q1*Q2;

  q1.normalize();
  q2.normalize();
  q1q2 = q1*q2;

  SANS_CHECK_CLOSE( q1q2True[0], q1q2.w(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[1], q1q2.x(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[2], q1q2.y(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[3], q1q2.z(), tol, tol );
  BOOST_CHECK( q1q2.isUnit() == true );

  q1q2double1 = q1q2 * 1.0;
  q1q2double2 = 1.0  * q1q2;
  q1q2int1    = q1q2 * 1;
  q1q2int2    = 1    * q1q2;

  SANS_CHECK_CLOSE( q1q2True[0], q1q2double1.w(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[1], q1q2double1.x(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[2], q1q2double1.y(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[3], q1q2double1.z(), tol, tol );
  BOOST_CHECK( q1q2double1.isUnit() == true );

  SANS_CHECK_CLOSE( q1q2True[0], q1q2double2.w(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[1], q1q2double2.x(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[2], q1q2double2.y(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[3], q1q2double2.z(), tol, tol );
  BOOST_CHECK( q1q2double2.isUnit() == true );

  SANS_CHECK_CLOSE( q1q2True[0], q1q2int1.w(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[1], q1q2int1.x(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[2], q1q2int1.y(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[3], q1q2int1.z(), tol, tol );
  BOOST_CHECK( q1q2int1.isUnit() == true );

  SANS_CHECK_CLOSE( q1q2True[0], q1q2int2.w(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[1], q1q2int2.x(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[2], q1q2int2.y(), tol, tol );
  SANS_CHECK_CLOSE( q1q2True[3], q1q2int2.z(), tol, tol );
  BOOST_CHECK( q1q2int2.isUnit() == true );

  q1q2double1 = q1q2 * 2.0;
  q1q2double2 = 2.0  * q1q2;
  q1q2int1    = q1q2 * 2;
  q1q2int2    = 2    * q1q2;

  BOOST_CHECK( q1q2double1.isUnit() == false );
  BOOST_CHECK( q1q2double2.isUnit() == false );
  BOOST_CHECK( q1q2int1.isUnit() == false );
  BOOST_CHECK( q1q2int2.isUnit() == false );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( relational_operators )
{
  Real w = 1, x = 2, y = 3, z = 4;
  QReal q1( w, x, y, z );
  QReal q2( q1 );

  BOOST_CHECK( (q1 == q2) == true );
  BOOST_CHECK( (q1 != q2) == false );

  QReal q3( w, x, y, y );
  BOOST_CHECK( (q1 == q3) == false );
  BOOST_CHECK( (q1 != q3) == true );

  Real magnitude = sqrt( pow(w,2) + pow(x,2) + pow(y,2) + pow(z,2) );

  QReal q4( w/magnitude, x/magnitude, y/magnitude, z/magnitude ); // Unit quat, but no unit flag
  q1.normalize();
  BOOST_CHECK( (q1 == q4) == false );
  BOOST_CHECK( (q1 != q4) == true );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( exp_tests )
{
  Real tol = 2e-13;

  Real w = 1, x = 2, y = 3, z = 4;
  QReal q1( w, x, y, z );
  QReal q1N( q1 );
  q1N.normalize();
  QReal q2( z, 0, 0, 0 );

  Real magnitude = sqrt( pow(w,2) + pow(x,2) + pow(y,2) + pow(z,2) );
  Real vMag = sqrt( pow(x,2) + pow(y,2) + pow(z,2) );

  Real wTrue = exp( w ) * cos( vMag );
  Real xTrue = exp( w ) * sin( vMag ) * x/vMag;
  Real yTrue = exp( w ) * sin( vMag ) * y/vMag;
  Real zTrue = exp( w ) * sin( vMag ) * z/vMag;

  QReal expq1 = exp( q1 );

  SANS_CHECK_CLOSE( wTrue, expq1.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, expq1.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, expq1.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, expq1.z(), tol, tol );
  BOOST_CHECK( expq1.isUnit() == false );

  Real wN = w/magnitude;
  Real xN = x/magnitude;
  Real yN = y/magnitude;
  Real zN = z/magnitude;
  Real vNmag = sqrt( pow(xN,2) + pow(yN,2) + pow(zN,2) );

  wTrue = exp( wN ) * cos( vNmag );
  xTrue = exp( wN ) * sin( vNmag ) * xN/vNmag;
  yTrue = exp( wN ) * sin( vNmag ) * yN/vNmag;
  zTrue = exp( wN ) * sin( vNmag ) * zN/vNmag;

  QReal expq1N = exp( q1N );

  SANS_CHECK_CLOSE( wTrue, expq1N.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, expq1N.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, expq1N.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, expq1N.z(), tol, tol );
  BOOST_CHECK( expq1N.isUnit() == false );

  wTrue = exp( z );
  xTrue = 0;
  yTrue = 0;
  zTrue = 0;

  QReal expq2 = exp( q2 );

  SANS_CHECK_CLOSE( wTrue, expq2.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, expq2.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, expq2.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, expq2.z(), tol, tol );
  BOOST_CHECK( expq2.isUnit() == false );

  QReal zeros( 0, 0, 0, 0 );
  QReal exp0 = exp( zeros );

  wTrue = 1;
  xTrue = 0;
  yTrue = 0;
  zTrue = 0;

  SANS_CHECK_CLOSE( wTrue, exp0.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, exp0.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, exp0.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, exp0.z(), tol, tol );
  BOOST_CHECK( exp0.isUnit() == true );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( log_tests )
{
  Real tol = 2e-13;

  Real w = 1, x = 2, y = 3, z = 4;
  QReal q1( w, x, y, z );
  QReal q1N( q1 );
  q1N.normalize();

  Real qMag = sqrt( pow(w,2) + pow(x,2) + pow(y,2) + pow(z,2) );
  Real vMag = sqrt( pow(x,2) + pow(y,2) + pow(z,2) );
  Real halfTheta = atan2( vMag, w );

  Real wTrue = log(qMag);
  Real xTrue = x/vMag * halfTheta;
  Real yTrue = y/vMag * halfTheta;
  Real zTrue = z/vMag * halfTheta;

  QReal logq1 = log( q1 );

  SANS_CHECK_CLOSE( wTrue, logq1.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, logq1.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, logq1.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, logq1.z(), tol, tol );
  BOOST_CHECK( logq1.isUnit() == false );

  Real wN = w/qMag;
  Real xN = x/qMag;
  Real yN = y/qMag;
  Real zN = z/qMag;
  Real vNmag = sqrt( pow(xN,2) + pow(yN,2) + pow(zN,2) );
  Real halfThetaN = atan2( vNmag, wN );

  wTrue = 0;
  xTrue = xN/vNmag * halfThetaN;
  yTrue = yN/vNmag * halfThetaN;
  zTrue = zN/vNmag * halfThetaN;

  QReal logq1N = log( q1N );

  SANS_CHECK_CLOSE( wTrue, logq1N.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, logq1N.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, logq1N.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, logq1N.z(), tol, tol );
  BOOST_CHECK( logq1N.isUnit() == false );

  QReal q2 = exp( log(q1N) );
  QReal q3 = log( exp(q1N) );

  SANS_CHECK_CLOSE( q1N.w(), q2.w(), tol, tol );
  SANS_CHECK_CLOSE( q1N.x(), q2.x(), tol, tol );
  SANS_CHECK_CLOSE( q1N.y(), q2.y(), tol, tol );
  SANS_CHECK_CLOSE( q1N.z(), q2.z(), tol, tol );
  BOOST_CHECK( q2.isUnit() == false );

  SANS_CHECK_CLOSE( q1N.w(), q3.w(), tol, tol );
  SANS_CHECK_CLOSE( q1N.x(), q3.x(), tol, tol );
  SANS_CHECK_CLOSE( q1N.y(), q3.y(), tol, tol );
  SANS_CHECK_CLOSE( q1N.z(), q3.z(), tol, tol );
  BOOST_CHECK( q3.isUnit() == false );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( inverse_tests )
{
  Real tol = 2e-13;

  Real w = 1, x = 2, y = 3, z = 4;
  QReal q( w, x, y, z );
  QReal qN( q );
  qN.normalize();

  Real qMag = sqrt( pow(w,2) + pow(x,2) + pow(y,2) + pow(z,2) );

  Real wTrue =  w / pow(qMag,2);
  Real xTrue = -x / pow(qMag,2);
  Real yTrue = -y / pow(qMag,2);
  Real zTrue = -z / pow(qMag,2);

  QReal qInv = inverse( q );

  SANS_CHECK_CLOSE( wTrue, qInv.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, qInv.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, qInv.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, qInv.z(), tol, tol );
  BOOST_CHECK( qInv.isUnit() == false );

  wTrue =  w / qMag;
  xTrue = -x / qMag;
  yTrue = -y / qMag;
  zTrue = -z / qMag;

  QReal qNInv = inverse( qN );

  SANS_CHECK_CLOSE( wTrue, qNInv.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, qNInv.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, qNInv.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, qNInv.z(), tol, tol );
  BOOST_CHECK( qNInv.isUnit() == true );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( pow_tests )
{
  Real tol = 4e-13;

  Real x = 2./7., y = 3./7., z = 6./7.;
  Vector axis = { x, y, z };
  Real theta = 1;
  QReal q( axis, theta );

  // Check zero power
  QReal q0 = pow(q,0);
  SANS_CHECK_CLOSE( 1, q0.w(), tol, tol );
  SANS_CHECK_CLOSE( 0, q0.x(), tol, tol );
  SANS_CHECK_CLOSE( 0, q0.y(), tol, tol );
  SANS_CHECK_CLOSE( 0, q0.z(), tol, tol );
  BOOST_CHECK( q0.isUnit() == true );

  // Check identity
  QReal qIdent = pow(q,1);
  SANS_CHECK_CLOSE( q.w(), qIdent.w(), tol, tol );
  SANS_CHECK_CLOSE( q.x(), qIdent.x(), tol, tol );
  SANS_CHECK_CLOSE( q.y(), qIdent.y(), tol, tol );
  SANS_CHECK_CLOSE( q.z(), qIdent.z(), tol, tol );
  BOOST_CHECK( qIdent.isUnit() == true );

  // Check inverse
  QReal qInvTrue = inverse( q );

  QReal qInv = pow(q,-1);

  SANS_CHECK_CLOSE( qInvTrue.w(), qInv.w(), tol, tol );
  SANS_CHECK_CLOSE( qInvTrue.x(), qInv.x(), tol, tol );
  SANS_CHECK_CLOSE( qInvTrue.y(), qInv.y(), tol, tol );
  SANS_CHECK_CLOSE( qInvTrue.z(), qInv.z(), tol, tol );
  BOOST_CHECK( qInv.isUnit() == true );

  // Check arbitrary power

  Real p   = 3;
  int pInt = 3;
  Real wTrue =     cos( p*theta/2 );
  Real xTrue = x * sin( p*theta/2 );
  Real yTrue = y * sin( p*theta/2 );
  Real zTrue = z * sin( p*theta/2 );

  QReal qPow = pow(q,p);

  SANS_CHECK_CLOSE( wTrue, qPow.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, qPow.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, qPow.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, qPow.z(), tol, tol );
  BOOST_CHECK( qPow.isUnit() == false );

  qPow = pow(q,pInt);

  SANS_CHECK_CLOSE( wTrue, qPow.w(), tol, tol );
  SANS_CHECK_CLOSE( xTrue, qPow.x(), tol, tol );
  SANS_CHECK_CLOSE( yTrue, qPow.y(), tol, tol );
  SANS_CHECK_CLOSE( zTrue, qPow.z(), tol, tol );
  BOOST_CHECK( qPow.isUnit() == false );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
