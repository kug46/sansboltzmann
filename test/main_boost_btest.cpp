// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/python/detail/prefix.hpp> //include boost/python file first to remove _POSIX_C_SOURCE warning
#include <boost/version.hpp>

#define BOOST_TEST_MODULE SANS_unit_test
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/test/debug.hpp>

#include "SANS_btest.h"

#ifdef SANS_TRAP_FLOATINGPOINT
#include <fenv.h>

#if defined(__APPLE__) && defined(__MACH__)

// This fixes the missing feenbableexcept on MacOS.  The fix reuses in verbatim a code available on
// https://github.com/ArduPilot/ardupilot/blob/master/libraries/AP_Common/missing/fenv.h.

// Public domain polyfill for feenableexcept on OS X
// http://www-personal.umich.edu/~williams/archive/computation/fe-handling-example.c

int feenableexcept(unsigned int excepts)
{
  static fenv_t fenv;
  unsigned int new_excepts = excepts & FE_ALL_EXCEPT;
  // previous masks
  unsigned int old_excepts;

  if (fegetenv(&fenv))
    return -1;

  old_excepts = fenv.__control & FE_ALL_EXCEPT;

  // unmask
  fenv.__control &= ~new_excepts;
  fenv.__mxcsr   &= ~(new_excepts << 7);

  return fesetenv(&fenv) ? -1 : old_excepts;
}

#if 0
inline int fedisableexcept(unsigned int excepts)
{
  static fenv_t fenv;
  unsigned int new_excepts = excepts & FE_ALL_EXCEPT;
  // all previous masks
  unsigned int old_excepts;

  if (fegetenv(&fenv))
    return -1;

  old_excepts = fenv.__control & FE_ALL_EXCEPT;

  // mask
  fenv.__control |= new_excepts;
  fenv.__mxcsr   |= new_excepts << 7;

  return fesetenv(&fenv) ? -1 : old_excepts;
}
#endif
#endif
#endif

struct GlobalSANSFixture
{
  int narg;
  char** argv;

  GlobalSANSFixture() : narg(0), argv(0)
  {
    boost::debug::detect_memory_leaks(false);

#ifdef SANS_TRAP_FLOATINGPOINT
    feenableexcept(FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO);
#endif

    //Python is initialized in PyGIL.h if it's needed
  }

  ~GlobalSANSFixture()
  {
    // Close down Python as cleanly as possible
    Py_Finalize();
  }
};

BOOST_GLOBAL_FIXTURE( GlobalSANSFixture )
// Boost 1.59 removed the ';' in the above macro, but simply adding it for all versions gives compiler warnings for ';;'
#if BOOST_VERSION >= 105900
;
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EmptyCoverageTest )
{
  //This is an empty test that can be used to generate coverage information without running any test
  //This allows system tests to be included in the coverage information but not contribute to line coverage

}
