// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include <boost/version.hpp>

#ifdef SANS_PETSC
#include <petscsys.h>
#else
#include <boost/mpi/environment.hpp>
#endif

//---------------------------------------------------------------------------//
//
// A fixture to initialize and terminate MPI in unit tests
//
//---------------------------------------------------------------------------//

struct MPI_unit_fixture
{
  int narg;
  char** argv;

#ifdef SANS_PETSC
  MPI_unit_fixture() : narg(0), argv(nullptr)
  {
    PetscInitialize(&narg, &argv, (char *)0, (char *)0);
  }

  ~MPI_unit_fixture()
  {
    PetscFinalize();
  }
#else
  //This will handle starting up the MPI environment
  boost::mpi::environment env;

  MPI_unit_fixture() : narg(0), argv(nullptr), env(narg,argv,false)
  {
  }

  ~MPI_unit_fixture()
  {
  }
#endif
};


BOOST_GLOBAL_FIXTURE( MPI_unit_fixture )
// Boost 1.59 removed the ';' in the above macro, but simply adding it for all versions gives compiler warnings for ';;'
#if BOOST_VERSION >= 105900
;
#endif
