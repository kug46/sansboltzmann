// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PYRITE_FILE_STREAM_H
#define PYRITE_FILE_STREAM_H

#include <boost/test/unit_test.hpp>

#include "SANS_CHECK_CLOSE_btest.h"

#include <fstream>
#include <iostream>
#include <iomanip> // setprecision

#include "tools/SANSnumerics.h"
#include "tools/SANSException.h"

namespace SANS
{

namespace detail
{
  struct pyrite_file_stream_impl;
}


class pyrite_file_stream
{
public:
  typedef boost::test_tools::predicate_result result_type;

  enum testmode
  {
    check,
    generate,
    panning  // Temporarily disable checking, but creates one error to prevent a passing test
  };

  pyrite_file_stream( const std::string& filename,
                      const Real small_tol, const Real close_tol,
                      testmode mode = check,
                      const bool verbose = false )
    : small_tol_(small_tol), close_tol_(close_tol), mode_(mode), verbose_(verbose)
  {
    //Open the file for read or write
    if ( mode_ == check )
    {
      pyritefile_.open(filename.c_str(), std::ios::in );

      if (verbose_)
      {
        std::cout << "...set to check pyrite file..." << std::endl;
        std::cout << "  pyrite file name: " << filename << std::endl << std::endl;
      }
    }
    else if ( mode_ == generate )
    {
      pyritefile_.open(filename.c_str(), std::ios::out );

      // Default to a high precision for numbers
      pyritefile_ << std::setprecision(16) << std::scientific;

      if (verbose_)
      {
        std::cout << "...set to generate pyrite file..." << std::endl;
        std::cout << "  pyrite file name: " << filename << std::endl << std::endl;
      }

    }
    else if ( mode_ == panning ) // Intentionally create an error
      BOOST_ERROR( "pyrite panning: not checking or generating pyrite files" );
    else
      BOOST_FAIL( "unknown pyrite test mode" );
  }

  inline detail::pyrite_file_stream_impl close( const Real close_tol );

  template<class T>
  inline detail::pyrite_file_stream_impl operator<<( const T& actual );

  inline pyrite_file_stream&
  operator<<(std::ostream& (*__pf)(std::ostream&));

  inline detail::pyrite_file_stream_impl
  operator<<( const char* actual );

protected:
  std::fstream pyritefile_;
  const Real small_tol_;
  const Real close_tol_;
  const testmode mode_;
  const bool verbose_;
};

//=============================================================================
namespace detail
{
  // This secondary stream class allows for temporarily changing tolerances
  struct pyrite_file_stream_impl
  {
    pyrite_file_stream_impl( std::fstream& pyritefile, const Real small_tol, const Real close_tol, pyrite_file_stream::testmode mode )
      : pyritefile_(pyritefile), small_tol_(small_tol), close_tol_(close_tol), mode_(mode)
    {}

    template<class T>
    inline pyrite_file_stream_impl& operator<<( const T& actual );

    inline pyrite_file_stream_impl&
    operator<<(std::ostream& (*__pf)(std::ostream&));

    inline pyrite_file_stream_impl&
    operator<<( const char* actual );

  protected:
    std::fstream& pyritefile_;
    const Real small_tol_;
    const Real close_tol_;
    const pyrite_file_stream::testmode mode_;
  };


  template<class T>
  inline pyrite_file_stream_impl&
  pyrite_file_stream_impl::operator<<( const T& actual )
  {
    if ( mode_ == pyrite_file_stream::check || mode_ == pyrite_file_stream::panning )
    {
      //This should be things like std::setprecision(16) or std::scientific, so nothing to do.
    }
    else
    {
      pyritefile_ << actual;
    }

    return *this;
  }

  template<>
  inline pyrite_file_stream_impl&
  pyrite_file_stream_impl::operator<< <int>( const int& actual )
  {
    if ( mode_ == pyrite_file_stream::check  )
    {
      int expected;
      pyritefile_ >> expected;
      BOOST_CHECK( pyritefile_.good() );
      BOOST_CHECK_EQUAL( expected, actual );
    }
    else if ( mode_ == pyrite_file_stream::generate )
    {
      pyritefile_ << actual << " ";
    }

    return *this;
  }

  inline pyrite_file_stream_impl&
  pyrite_file_stream_impl::operator<<(std::ostream& (*__pf)(std::ostream&))
  {
    if ( mode_ == pyrite_file_stream::check )
    {
      //This is likely std::cout, so nothing to do.
    }
    else if ( mode_ == pyrite_file_stream::generate )
    {
      __pf(pyritefile_);
    }

    return *this;
  }

  template<>
  inline pyrite_file_stream_impl&
  pyrite_file_stream_impl::operator<< <std::string>( const std::string& actual )
  {
    if ( mode_ == pyrite_file_stream::check )
    {
      std::string expected;
      pyritefile_ >> expected;
      BOOST_CHECK( pyritefile_.good() );
      BOOST_CHECK_EQUAL( expected, actual );
    }
    else if ( mode_ == pyrite_file_stream::generate )
    {
      SANS_ASSERT_MSG( actual.find(' ') == std::string::npos &&
                       actual.find('\n') == std::string::npos &&
                       actual.find('\r') == std::string::npos,
                       "\nStrings in pyrite files cannot contain white characters\nFailed to write '%s'", actual.c_str() );
      pyritefile_ << actual;
    }

    return *this;
  }

  inline pyrite_file_stream_impl&
  pyrite_file_stream_impl::operator<<( const char* actual )
  {
    *this << std::string(actual);
    return *this;
  }

  template<>
  inline pyrite_file_stream_impl&
  pyrite_file_stream_impl::operator<< <Real>( const Real& actual )
  {
    if ( mode_ == pyrite_file_stream::check )
    {
      Real expected;
      pyritefile_ >> expected;
      BOOST_CHECK( pyritefile_.good() );
      SANS_CHECK_CLOSE( expected, actual, small_tol_, close_tol_ );
    }
    else if ( mode_ == pyrite_file_stream::generate )
    {
      pyritefile_ << actual << " ";
    }

    return *this;
  }
}

//=============================================================================
inline detail::pyrite_file_stream_impl
pyrite_file_stream::close( const Real close_tol )
{
  detail::pyrite_file_stream_impl pystream(pyritefile_, small_tol_, close_tol, mode_);

  return pystream;
}

template<class T>
inline detail::pyrite_file_stream_impl
pyrite_file_stream::operator<<( const T& actual )
{
  detail::pyrite_file_stream_impl pystream(pyritefile_, small_tol_, close_tol_, mode_);

  pystream << actual;

  return pystream;
}

inline pyrite_file_stream&
pyrite_file_stream::operator<<(std::ostream& (*__pf)(std::ostream&))
{
  if ( mode_ == check )
  {
    //This is likely std::cout, so nothing to do.
  }
  else if ( mode_ == generate )
  {
    __pf(pyritefile_);
  }

  return *this;
}


inline detail::pyrite_file_stream_impl
pyrite_file_stream::operator<<( const char* actual )
{
  detail::pyrite_file_stream_impl pystream(pyritefile_, small_tol_, close_tol_, mode_);

  pystream << std::string(actual);
  return pystream;
}

}

#endif //PYRITE_FILE_STREAM_H
