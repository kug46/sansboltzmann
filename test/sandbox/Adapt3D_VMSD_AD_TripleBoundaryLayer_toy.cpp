// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

// #define BOUNDARYOUTPUT
//#define WHOLEPATCH

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/FunctionalCell_VMSD.h"
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"

#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_EG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#endif

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_AD_VMSD_TripleBoundaryLayer_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_AD_VMSD_TripleBoundaryLayer )
{
  typedef ScalarFunction3D_TripleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputNDConvertSpace<PhysD3, OutputCell_SolutionErrorSquared<PDEClass, SolutionExact>> L2ErrorClass;
  typedef IntegrandCell_Galerkin_Output<L2ErrorClass> L2ErrorIntegrandClass;

#ifndef BOUNDARYOUTPUT
  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_VMSD_Output<NDOutputClass,NDPDEClass> OutputIntegrandClass;
#else
  typedef OutputAdvectionDiffusion3D_WeightedResidual OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, VMSD> OutputIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_VMSD<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

// #define USE_AVRO 0

  int orderL = 1, orderH = 3;
  int powerL = 2, powerH = 7;
  std::string mesher = "EPIC";

  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc == 4)
  {
    orderL = orderH = std::stoi(argv[1]);
    powerL = powerH = std::stoi(argv[2]);
    mesher = std::string(argv[3]);
  }
  else if (argc == 3)
  {
    orderL = orderH = std::stoi(argv[1]);
    powerL = powerH = std::stoi(argv[2]);
  }
  else if (argc == 2)
  {
    orderL = orderH = std::stoi(argv[1]);
  }




#if USE_AVRO
  mesher = "avro";
#endif

  // global communicator
  mpi::communicator world;

  // Grid
  int ii = 5;
  int jj = ii;
  int kk = ii;

  Real a = 1.0;
  Real b = 1.0;
  Real c = 1.0;
  Real nu = 0.01;

  // PDE
  AdvectiveFlux3D_Uniform adv( a, b, c );

  ViscousFlux3D_Uniform visc( nu, 0, 0, 0, nu, 0, 0, 0, nu );

  Source3D_UniformGrad source(0,0,0,0);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function.Name] =
           BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function.TripleBL;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.c] = c;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;
  solnArgs[NDSolutionExact::ParamsType::params.offset] = 1;
  solnArgs[NDSolutionExact::ParamsType::params.scale] = -1;

  NDSolutionExact solnExact( solnArgs );

  NDPDEClass pde( adv, visc, source );

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Upwind] = true;

  PyDict BCSoln_Robin;
  BCSoln_Robin[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln_Robin[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  BCSoln_Robin[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType.Robin;
  BCSoln_Robin[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Upwind] = true;

  PyDict PyBCList;
  PyBCList["BCSoln_Dirichlet"] = BCSoln_Dirichlet;
  PyBCList["BCSoln_Robin"] = BCSoln_Robin;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSoln_Dirichlet"] = {XField3D_Box_Tet_X1::iXmax,
                                          XField3D_Box_Tet_X1::iYmax,
                                          XField3D_Box_Tet_X1::iZmax};
  BCBoundaryGroups["BCSoln_Robin"] = {XField3D_Box_Tet_X1::iXmin,
                                      XField3D_Box_Tet_X1::iYmin,
                                      XField3D_Box_Tet_X1::iZmin};

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Galerkin Stabilization
  DiscretizationVMSD stab;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-10, 1e-10, 1e-10};
  //Output functional
#ifndef BOUNDARYOUTPUT
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, true);
  const Real trueOutput = 0.015073625; // From Mathematica
#else
  // Residual weighted boundary output
  NDOutputClass fcnOutput(1.);
  OutputIntegrandClass outputIntegrand( fcnOutput, {XField3D_Box_Tet_X1::iXmax} );
  const Real trueOutput = 0.0199; // From Mathematica
#endif

  // L2 Error output functional
  L2ErrorClass L2ErrorOutput(solnExact);
  L2ErrorIntegrandClass L2ErrorIntegrand(L2ErrorOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 )
      std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 3;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDict_adjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-20;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  PyDict MesherDict;
  if (mesher == "refine")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.refine;
    MesherDict[refineParams::params.DumpRefineDebugFiles] = false;
#ifdef SANS_REFINE
    MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;
    //MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.system;
#endif
  }
  else if (mesher == "EPIC")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
    std::vector<int> allBC;
    allBC.insert(allBC.end(), BCBoundaryGroups["BCNone"].begin(), BCBoundaryGroups["BCNone"].end());
    MesherDict[EpicParams::params.SymmetricSurf] = allBC;
    MesherDict[EpicParams::params.nThread] = 4;
  }
  else if (mesher == "fefloa")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.FeFloa;
  }
#ifdef SANS_AVRO
  else if (mesher == "avro")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.avro;
    MesherDict[avroParams::params.Curved] = false;
  }
#endif
  else
    BOOST_REQUIRE_MESSAGE(false, "Unknown mesh generator.");

  std::cout << "mesher = " << mesher << std::endl;

  const int maxIter = 30;
  const bool DGCONV = true; // convert to DG element counts

  //--------ADAPTATION LOOP--------
  for (int order = orderL; order <= orderH; order++)
  {
    stab.setNitscheOrder(order);

    for (int power = powerL; power <= powerH; power++ )
    {
      int nk = pow(2,power);
      int targetCost = 1000*nk;

      if (DGCONV)
      {
        Real nDOFperCell_DG = (order+1)*(order+2)*(order+3)/6;

        Real nDOFperCell_CG = nDOFperCell_DG;
        nDOFperCell_CG -= (4 - 1./5); // the node dofs are shared by 20
        nDOFperCell_CG -= (6 - 1)*std::max(0,(order-1)); // if there are edge dofs they are shared by 6
        nDOFperCell_CG -= (4 - 2)*std::max(0,(order-1)*(order-2)/2); // if there are face dofs they are shared by 2

        targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
      }

      const int string_pad = 6;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);

      std::string filename_base = "tmp/TripleBoundaryLayer/" + mesher + "_";
      filename_base += "VMSD_NEW2_";

      filename_base += int_pad + "_P" + std::to_string(order) + "/";

      std::cout<< filename_base << std::endl;

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;
      MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction] = 1.0;
      MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
//      MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.ImpliedMetric.SANSparallel;

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.dumpStepMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.dumpRateMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.hasTrueOutput] = true;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TrueOutput] = trueOutput;

      MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

      boost::filesystem::path base_dir(filename_base);
      if ( not boost::filesystem::exists(base_dir) )
        boost::filesystem::create_directories(base_dir);

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist;
      if ( world.rank() == 0 )
      {
        fadapthist.open( adapthist_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
      }

      std::string output_filename = filename_base + "output.dat";
      fstream foutputhist;
      if ( world.rank() == 0 )
      {
        foutputhist.open( output_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + output_filename);
      }

      if (world.rank() == 0 )
      {
        // write the header to the output file
        foutputhist << "VARIABLES="
                    << std::setw(5)  << "\"Iter\""
                    << std::setw(10) << "\"DOF\""
                    << std::setw(20) << "\"Elements\""
                    << std::setw(20) << "\"u<sup>2</sup>\""
                    << std::setw(20) << "\"u<sup>2</sup> Error\""
                    << std::setw(20) << "\"Estimate\""
                    << std::setw(20) << "\"L<sup>2</sup> Error\""
                    << std::endl;

        foutputhist << "ZONE T=\"MOESS " << mesher << " " << nk << "k\"" << std::endl;
      }

      MesherDict[refineParams::params.FilenameBase] = filename_base;
      MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

#if defined(SANS_AVRO) && USE_AVRO

      using avro::coord_t;
      using avro::index_t;

      avro::Context context;

      coord_t number = 3;
      Real x0[3] = {0.,0.,0.};
      std::vector<Real> lens(number,1.);
      std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>( &context , x0 , lens.data() );
      std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(&context,"cube");
      model->addBody(pbody,true);

      // initialize the mesh
      XField3D_Box_Tet_X1 xfld0( world, ii, jj, kk ); //pts , pts , pts );
      // copy the mesh into the domain and attach the geometry
      std::shared_ptr< XField<PhysD3,TopoD3> > pxfld = std::make_shared< XField_avro<PhysD3,TopoD3> >(xfld0,model);

#else
      std::shared_ptr<XField<PhysD3, TopoD3>> pxfld = std::make_shared<XField3D_Box_Tet_X1>( world, ii, jj, kk ); //pts, pts, pts );
#endif

      std::vector<int> cellGroups = {0};

      const int quadOrder = 2*(order + 1);

      //const Real iCost = pInterface->getCost();

      int iter = 0;
      while (true)
      {
        if (world.rank() == 0)
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        std::vector<int> interiorTraceGroups;
        for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        std::shared_ptr<SolutionClass> pGlobalSol;
        pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, stab, order, order, order+1, order+1,
                                                     BasisFunctionCategory_Lagrange,
                                                     BasisFunctionCategory_Lagrange,
                                                     BasisFunctionCategory_Lagrange,
                                                     active_boundaries);

        //Perform L2 projection from solution on previous mesh
        pGlobalSol->setSolution(1);

        std::shared_ptr<SolverInterfaceClass> pInterface;
        pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                            cellGroups, interiorTraceGroups,
                                                            PyBCList, BCBoundaryGroups,
                                                            SolverContinuationDict, LinearSolverDict,
                                                            outputIntegrand);

        pInterface->solveGlobalPrimalProblem();

        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter)
                                                  + "_P" + std::to_string(order) +
                                                  + ".plt";
        if ( iter == maxIter )
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        pInterface->solveGlobalAdjointProblem();

        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter)
                                                    + "_P" + std::to_string(order) +
                                                    + ".plt";
        if ( iter == maxIter )
          output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

#ifdef SANS_MPI
        int nDOFtotal = 0;
        boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );

        // count the number of elements possessed by this processor
        int nElem = 0;
        for (int elem = 0; elem < pxfld->nElem(); elem++ )
          if (pxfld->getCellGroupGlobal<Tet>(0).associativity(elem).rank() == world.rank())
            nElem++;

        int nElemtotal = 0;
        boost::mpi::reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>(), 0 );
#else
        int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
        int nElemtotal = pxfld->nElem();
#endif

        //Compute error estimates
        pInterface->computeErrorEstimates();

        Real u2 = pInterface->getOutput();
        Real globalEstimate  = pInterface->getGlobalErrorEstimate();


        QuadratureOrder quadrule(*pxfld, quadOrder);

        Real L2error = 0;
        IntegrateCellGroups<TopoD3>::integrate( FunctionalCell_Galerkin( L2ErrorIntegrand, L2error ),
                                                *pxfld, pGlobalSol->primal.qfld,
                                                quadrule.cellOrders.data(), quadrule.cellOrders.size() );
        L2error = sqrt(L2error);

        if (world.rank() == 0 )
        {
          foutputhist << std::setw(5) << iter
                      << std::setw(10) << nDOFtotal
                      << std::setw(10) << nElemtotal
                      << std::setw(20) << std::setprecision(10) << std::scientific << u2
                      << std::setw(20) << std::setprecision(10) << std::scientific << fabs(u2-trueOutput)
                      << std::setw(20) << std::setprecision(10) << std::scientific << globalEstimate
                      << std::setw(20) << std::setprecision(10) << std::scientific << L2error
                      << std::endl;
        }
        if ( iter == maxIter )
        {
          break;

          std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter) + ".plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

          std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter) + ".plt";
          output_Tecplot( pInterface->getAdjField(), adjfld_filename );
        }
        //const Real tCost = iCost + (targetCost - iCost)*std::min(1.0, pow((iter+1.0)/(maxIter-3.0),1) );
        //mesh_adapter.adjustTargetCost(tCost);

        //Perform local sampling and adapt mesh
#if defined(SANS_AVRO) && USE_AVRO
        pxfld = mesh_adapter.adapt_domain(*pxfld,*pxfld,cellGroups, *pInterface, iter);
#else
        pxfld = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);
#endif
        iter++;
      }


    }
    //timehist.close();
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
