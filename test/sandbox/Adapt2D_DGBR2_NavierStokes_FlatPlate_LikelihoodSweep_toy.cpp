// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_NavierStokes_FlatPlate_btest
// Testing of the MOESS framework on a 2D Navier-Stokes problem


#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include <iostream>
#include <random>

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "tools/linspace.h"

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/OutputNavierStokes2D.h"
#include "pde/NS/ForcingFunctionEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#ifdef SANS_AVRO
#include <geometry/builder.h>
#include <library/cube.h>
#include "Meshing/avro/XField_avro.h"
#endif

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_FlatPlate_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_NavierStokes_FlatPlate_MCMC_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_NavierStokes_FlatPlate_MCMC_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  // typedef OutputNavierStokes2D_TotalHeatFlux<PDEClass> OutputClass;
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif
  // For setting temperature sensor arrays
  typedef OutputEuler2D_TemperatureProbe<PDEClass> TemperatureOutputClass;
  typedef OutputNDConvertSpace<PhysD2, TemperatureOutputClass> NDTemperatureOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDTemperatureOutputClass> TemperatureOutputIntegrandClass;

  // Heat Flux and Drag independent of the adaptation
  typedef OutputNavierStokes2D_TotalHeatFlux<PDEClass> HeatFluxOutputClass;
  typedef OutputNDConvertSpace<PhysD2, HeatFluxOutputClass> NDHeatFluxOutputClass;
  typedef OutputEuler2D_Force<PDEClass> ForceOutputClass;
  typedef OutputNDConvertSpace<PhysD2, ForceOutputClass> NDForceOutputClass;

  typedef NDVectorCategory<boost::mpl::vector2<NDHeatFluxOutputClass,NDForceOutputClass>, NDHeatFluxOutputClass::Category> NDOutVec2Cat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVec2Cat, DGBR2> DataOutputIntegrandBoundaryClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // For the data arrays
  typedef typename DLA::VectorS<8,Real> Vector8;
  Vector8 output(0.0);

#define USE_AVRO 1
#define PRINT 1

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1.0e-9, 1.0e-9};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.3;
  const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real tRef = 1;                              // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                      // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // Entropy fix
  const RoeEntropyFix entropyFix = eVanLeer;

  // Forcing Function

  Real xF = 0.5, yF = 1.0, str = 0*1e-1, width = 0.01; // initial point
  typedef ForcingFunction2D_HeatSource<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(xF,yF,str,width) );

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  // NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, entropyFix, forcingptr );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  // BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipIsothermal_mitState;
  BCNoSlip[BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDEClass>::ParamsType::params.Twall] = 0.5;

#if 0

  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_mitState;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VxSpec] = uRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VySpec] = vRef;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;

#else

  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  Real aSpec = atan(vRef/uRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
#endif

  // std::cout<< "V = " << BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] << std::endl;


  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  // return;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2,4};
  BCBoundaryGroups["BCNoSlip"] = {1};
  BCBoundaryGroups["BCOut"] = {3};
  BCBoundaryGroups["BCIn"] = {5};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Temperature probes
  std::vector<std::vector<Real>> xSensor = { {1.25}, {1.25}, {1.25}, {1.25}, {1.25}};
  std::vector<std::vector<Real>> ySensor = { {0.05}, {0.10}, {0.20}, {0.40}, {0.80}};

  // Outputs
  NDTemperatureOutputClass tempSensor_0(pde,xSensor[0],ySensor[0],1e-2);
  NDTemperatureOutputClass tempSensor_1(pde,xSensor[1],ySensor[1],1e-2);
  NDTemperatureOutputClass tempSensor_2(pde,xSensor[2],ySensor[2],1e-2);
  NDTemperatureOutputClass tempSensor_3(pde,xSensor[3],ySensor[3],1e-2);
  NDTemperatureOutputClass tempSensor_4(pde,xSensor[4],ySensor[4],1e-2);
  NDTemperatureOutputClass allTempSensor(pde,cat(xSensor),cat(ySensor),1e-1);

  // Integrands
  TemperatureOutputIntegrandClass outputIntegrandSensor_0( tempSensor_0, {0} );
  TemperatureOutputIntegrandClass outputIntegrandSensor_1( tempSensor_1, {0} );
  TemperatureOutputIntegrandClass outputIntegrandSensor_2( tempSensor_2, {0} );
  TemperatureOutputIntegrandClass outputIntegrandSensor_3( tempSensor_3, {0} );
  TemperatureOutputIntegrandClass outputIntegrandSensor_4( tempSensor_4, {0} );
  TemperatureOutputIntegrandClass outputIntegrandAllTemps( allTempSensor, {0} );

  NDHeatFluxOutputClass heatFluxOutputFcn(pde); // Heat flux output
  DataOutputIntegrandBoundaryClass heatFluxOutputIntegrand( heatFluxOutputFcn, {1} );
  NDForceOutputClass dragOutputFcn(pde,1.0,0.0); // Drag output
  DataOutputIntegrandBoundaryClass dragOutputIntegrand( dragOutputFcn, {1} );


#ifndef BOUNDARYOUTPUT
  // Temp probes output
  NDOutputClass fcnOutput(pde,cat(xSensor),cat(ySensor),1e-1);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  // const HNormConstant = Cp*tRef*rhoref*sqrt(uRef*uRef + vRef*vRef);
  // NDOutputClass outputFcn(pde); // Heat flux output
  NDOutputClass outputFcn(pde, 1., 0.);  // Drag output
  OutputIntegrandClass outputIntegrand( outputFcn, {3} );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  // PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-12;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

#if 1
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 50;
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 1;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  // Grid
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;
#if USE_AVRO && defined(SANS_AVRO)
  using avro::coord_t;
  using avro::index_t;
  std::shared_ptr<avro::Context> context;

  // create the context
  context = std::make_shared<avro::Context>();

  // setup the EGADS nodes
  avro::real x0[3] = {-1,0,0};
  avro::real x1[3] = {0,0,0};
  avro::real x2[3] = {1,0,0};
  avro::real x3[3] = {2,0,0};
  avro::real x4[3] = {2,2,0};
  avro::real x5[3] = {-1,2,0};
  std::vector<avro::real*> X = {x0,x1,x2,x3,x4,x5};

  std::vector<avro::EGADSNode> nodes;
  for (index_t k=0;k<X.size();k++)
    nodes.emplace_back( avro::EGADSNode(context.get(),X[k]) );

  // make the edges
  // very important! boundary conditions need to be specified in this order
  // you can use EngSketchPad's vGeom on the flatplate.egads file below
  // to visualize the order of the edges in the EGADS model
  std::vector<avro::EGADSEdge> edges;
  for (index_t k=0;k<nodes.size();k++)
  {
    avro::EGADSNode* n0 = &nodes[k];
    avro::EGADSNode* n1;
    if (k<nodes.size()-1)
      n1 = &nodes[k+1];
    else
      n1 = &nodes[0];
    edges.emplace_back( avro::EGADSEdge(context.get(),*n0,*n1) );
  }

  avro::EGADSEdgeLoop loop(CLOSED);
  for (index_t k=0;k<edges.size();k++)
    loop.add( edges[k] , SFORWARD );
  loop.make(context.get());

  std::shared_ptr<avro::Body> body = std::make_shared<avro::EGADSWireBody>(context.get(),loop);
  body->buildHierarchy();

  std::shared_ptr<avro::Model> amodel = std::make_shared<avro::Model>(context.get(),"flatplate");
  amodel->addBody(body,false); // false = body is not interior
  amodel->finalize();

  // setup the initial mesh
  index_t nx = 4;
  index_t ny = 5;
  avro::library::CubeMesh mesh( {1,1} , {nx,ny} );

  // transform the vertices
  for (index_t k=0;k<mesh.vertices().nb();k++)
  {
    avro::real x = mesh.vertices()[k][0];
    avro::real y = mesh.vertices()[k][1];

    mesh.vertices()[k][0] = (x3[0] -x0[0])*x +x0[0];
    mesh.vertices()[k][1] = (x5[1] -x0[1])*y +x0[1];
  }

  mesh.vertices().findGeometry(*amodel);

  std::shared_ptr<XField_avro<PhysD2, TopoD2>> pxfld_avro( new XField_avro<PhysD2,TopoD2>(world,amodel) );
  pxfld_avro->import(mesh);

  pxfld = pxfld_avro;
#endif

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  int order=3, power=0;
  if (argc == 3)
  {
    order = std::stoi(argv[1]);
    power = std::stoi(argv[2]);
  }

  std::cout << "order = " << order << ", power = " << power << std::endl;

#endif

  // const int power = 0, order = 2;

  //--------Initial ADAPTATION LOOP--------
  /*
    Give the mesh an initial adaptation to no flame, in order to get a reasonable start
  */

  // Real xF = 0.92, yF = 0.24, str = 0*1e-1, width = 0.01;

  const int maxIter = 50;
  Real targetCost = 250.0*pow(2,power);

  // to make sure folders have a consistent number of zero digits
  const int string_pad = 5;
  std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
  std::string filename_base = "tmp/NS_FP_Re1000_MCMC/DG_Likelihood_" + int_pad + "_P" + std::to_string(order) + "/";

  boost::filesystem::create_directories(filename_base);

  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist;
  if (world.rank() == 0)
  {
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }
  std::string outhist_filename = filename_base + "out.dat";
  fstream fouthist;
  if (world.rank() == 0)
  {
    fouthist.open( outhist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fouthist.good(), "Error opening file: " + outhist_filename);
    fouthist << "T0, T1, T2, T3, T4, Tall, qH, qD" << std::endl;
  }

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Optimizer_MaxEval] = 6000;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;
  MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction] = 0.25;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

  PyDict MesherDict;
#if USE_AVRO && defined(SANS_AVRO)
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;
#else
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
#endif

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  const int quadOrder = 3*(order+1); //2*(order + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrand);

  //Set initial solution
  pGlobalSol->setSolution(q0);

#if PRINT
  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );
#endif

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

#if PRINT
  std::string qfld_filename = filename_base + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

  std::string adjfld_filename = filename_base + "adjfld_a0.plt";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
#endif

  //Compute error estimates
  pInterface->computeErrorEstimates();

#if PRINT
  std::string efld_filename = filename_base + "efld_a0.plt";
  pInterface->output_EField(efld_filename);
#endif

  for (int iter = 0; iter < maxIter+1; iter++)
  {
    timer tic;
    if ( world.rank() == 0 ) std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

#if PRINT
    // std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    // output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );
#endif

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();
    //Compute error estimates
    pInterface->computeErrorEstimates();

#if PRINT
    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

    // std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".plt";
    // pInterface->output_EField(efld_filename);
#endif

     QuadratureOrder quadratureOrder( *pxfld, quadOrder );

     // Calculate temperature sensor outputs
     // std::valarray<Real,8> output(0.0);
     output = 0;
     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_0,output[0]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
        quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_1,output[1]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_2,output[2]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_3,output[3]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_4,output[4]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandAllTemps,output[5]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
         FunctionalBoundaryTrace_Dispatch_DGBR2(heatFluxOutputIntegrand,
           pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
           quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(),
         output[6] ) );
     pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
         FunctionalBoundaryTrace_Dispatch_DGBR2(dragOutputIntegrand,
           pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
           quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(),
         output[7] ) );

 #ifdef SANS_MPI
     output = boost::mpi::all_reduce( *pxfld->comm(), output,  std::plus<Vector8>() );
 #endif

    if (world.rank() == 0)
    {
      std::cout << "time elapsed = " << tic.elapsed() << " s"<< std::endl;

      fouthist  << std::setprecision(16) << output[0] << ", "
                << std::setprecision(16) << output[1] << ", "
                << std::setprecision(16) << output[2] << ", "
                << std::setprecision(16) << output[3] << ", "
                << std::setprecision(16) << output[4] << ", "
                << std::setprecision(16) << output[5] << ", "
                << std::setprecision(16) << output[6] << ", "
                << std::setprecision(16) << output[7] << std::endl;
    }
  }

  if (world.rank() == 0)
    fadapthist.close();

  // return; // Generating the data

//----------------------------- SWEEP -----------------------------//
/*
  The basic mesh is now ok. Start doing the actual MCMC

  Uniform prior for xF and yF on the region
*/
  // // Initialize the Random number generator - Only actually used on processor 0
  // // obtain a seed from the system clock:
  // unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
  // std::default_random_engine generator(seed1);
  // // std::default_random_engine generator;
  // std::uniform_real_distribution<Real> distribution(0.0,1.0);

  // boundary of the domain
  Real xMin = -1, xMax = 2, yMin = 0, yMax = 2;

  // const int chain = 1e3;
  std::cout<< std::setprecision(4); // So all the outputs look the same

  std::vector<Real> xVec = linspace(xMin,xMax,101);
  std::vector<Real> yVec = linspace(yMin,yMax,101);

  std::vector<Real> xOut, yOut, dOut;


  // Filestream for output
  fstream foutputhist;
  if (world.rank() ==  0)
  {
    std::string outputFileName = filename_base + "output.dat";
    foutputhist.open( outputFileName, fstream::out );
    BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + outputFileName);
    foutputhist << "X, Y, output" << std::endl;
    foutputhist << std::setprecision(16);
  }

  str = 1e-1; // turn the heat source back on

  timer tic;
  // Start the chain having got an initial mesh
  for (std::size_t ix = 0; ix < xVec.size(); ix++)
    for (std::size_t iy = 0; iy < yVec.size(); iy++)
    {
      xF = xVec[ix]; yF = yVec[iy];

      forcingptr->set(xF,yF,str,width); // set the values for the heat source

      // Solve the primal on the current grid
      pInterface->solveGlobalPrimalProblem();

      // Compute the outputs
      QuadratureOrder quadratureOrder( *pxfld, quadOrder );

      output = 0.0;
       // Calculate temperature sensor outputs
       IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_0,output[0]),
         *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
          quadratureOrder.cellOrders.size() );

       IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_1,output[1]),
         *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
         quadratureOrder.cellOrders.size() );

       IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_2,output[2]),
         *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
         quadratureOrder.cellOrders.size() );

       IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_3,output[3]),
         *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
         quadratureOrder.cellOrders.size() );

       IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_4,output[4]),
         *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
         quadratureOrder.cellOrders.size() );

       IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandAllTemps,output[5]),
         *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
         quadratureOrder.cellOrders.size() );

       pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
           FunctionalBoundaryTrace_Dispatch_DGBR2(heatFluxOutputIntegrand,
             pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
             quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(),
           output[6] ) );
       pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
           FunctionalBoundaryTrace_Dispatch_DGBR2(dragOutputIntegrand,
             pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
             quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(),
           output[7] ) );

   #ifdef SANS_MPI
       output = boost::mpi::all_reduce( *pxfld->comm(), output,  std::plus<Vector8>() );
   #endif


      // // Calculate the acceptance probability
      // if ( world.rank() == 0 )
      // {
      //   likelihood = 0.0;
      //   for (int i = 0; i < 8; i++)
      //     likelihood += -precM(i)*pow(output(i) - data(i),2)/2;
      //
      //   likelihood = (1.0/pow(2*PI/precM,0.5))*exp(likelihood); // Compute the likelihood of the proposed point
      //
      //   xOut.push_back(xF); yOut.push_back(yF); dOut.push_back(likelihood);
      // }
// #ifdef SANS_MPI
//       boost::mpi::broadcast(world, likelihood, 0 );
// #endif

      if (world.rank() == 0)
      {
        foutputhist << std::setprecision(8) << xF <<", "
                    << std::setprecision(8) << yF <<", "
                    << std::setprecision(8) << output << std::endl;
      }

    }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
