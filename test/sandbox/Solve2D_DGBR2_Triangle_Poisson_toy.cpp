// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "tools/linspace.h"
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Topology/Dimension.h"
#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/BCParameters.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/output_Tecplot.h"

#include "Meshing/EPIC/XField_PX.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

// Issue: The file seems to work fine, but for the box case doesn't seem to output accurate results
// Might be because I am not using the normalization, Need to add Normalization
// Need to find a way to access the derivative information from arrayQ
// Putting Distance Function converter in the ScalarFunction2D.h file

using namespace std;
using namespace SANS;

//#######################################################################//
BOOST_AUTO_TEST_SUITE (Solve2D_DGBR2_Poisson_test_suite)

//-----------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Poisson )
{
  typedef PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform, Source2D_UniformGrad> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
          AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2> > PrimalEquationSetClass;

  //typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType( 1.0 ) );  //Need to use opposite sign, ie. 1 instead of -1
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // Newton Solver Set Up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  // Check inputs
  NewtonSolverParam::checkInputs( NewtonSolverDict );

  //PDE

  AdvectiveFlux2D_Uniform adv( 0.0, 0.0 );
  ViscousFlux2D_Uniform visc( 1., 0., 0., 1. );   /// Think this is kxx,kxy,kyx,kyy
  Source2D_UniformGrad source( 0.0, 0.0, 0.0 );    // Might want to make this a forcing function instead of a source term
  NDPDEClass pde( adv, visc, source, forcingptr );
  //NDPDEClass pde( adv, visc, source);

  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  // Bug Testing Grid

  //BC

  int iB = XField2D_Box_Triangle_X1::iBottom;
  int iR = XField2D_Box_Triangle_X1::iRight;
  int iT = XField2D_Box_Triangle_X1::iTop;
  int iL = XField2D_Box_Triangle_X1::iLeft;

  PyDict BCWall;   /// Check to see if I need to call this from an existing Python Directory (Solved, this should be fine)
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;   //Dirichle
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCFar;
  BCFar[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 0;
  BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 1;
  BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCAdia;
  BCAdia[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 0;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 1;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict PyBCList;

  PyBCList["BCWall"] = BCWall;
  PyBCList["BCFar"] = BCFar;
  PyBCList["BCAdia"] = BCAdia;


  BCBoundaryGroups["BCWall"] =
  { iB};
  BCBoundaryGroups["BCFar"] =
  { iT};
  BCBoundaryGroups["BCAdia"] =
  { iR, iL};
  //BCBoundaryGroups["BCWall"] = {0};
  //BCBoundaryGroups["BCFar"] = {1,2};
  BCParams::checkInputs( PyBCList );

  const std::vector<int> BoundaryGroups;

  int ordermin = 2;
  int ordermax = 4;

  for (int order = ordermin; order <= ordermax; order++)
  {
    int powermin = 2;
    int powermax = 4;

    for (int power = powermin; power <= powermax; power++)
    {

      int ii = 5;
      int jj = ii;
      int xmin = 0.0;
      int xmax = 1.;
      int ymin = 0.0;
      int ymax = 1.;

      XField2D_Box_Triangle_X1 xfld( ii, jj, xmin, xmax, ymin, ymax );

      // Mesh Read in
#if 0

      //string filein = "grids/Airfoil_with_BCs_spline_experimentv5";
      string filein = "grids/Joukowski_Laminar_Classic_tri_ref0_Q3";
      filein += ".grm";
      XField_PX<PhysD2, TopoD2> xfld(filein);
      //const std::vector<int> BoundaryGroups = {0,1,2};
#endif

      // BR2 discretization
      Real viscousEtaParameter = 6;  // Not sure what this should be set to, need to investigate
      DiscretizationDGBR2 disc( 0, viscousEtaParameter );

      //Intergration
      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = { 1e-11, 1e-11 };

      // lifting operators
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld( xfld, order, BasisFunctionCategory_Hierarchical );
      rfld = 0.0;

      //Create Solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld( xfld, order, BasisFunctionCategory_Hierarchical );

      qfld = 0.1;

      //Lagrange Multipliers

      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups ) );
      lgfld = 0;

      // Spatial Discritization

      PrimalEquationSetClass PrimalEqSet( xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                          ResidualNorm_Default, tol, { 0 }, { 0, 1, 2 }, PyBCList, BCBoundaryGroups );
      NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );
      // set initial condition from current solution in solution fields
      SystemVectorClass sln0( PrimalEqSet.vectorStateSize() );
      PrimalEqSet.fillSystemVector( sln0 );

      // nonlinear solve
      SystemVectorClass sln( PrimalEqSet.vectorStateSize() );
      SolveStatus status = nonlinear_solver.solve( sln0, sln );
      BOOST_CHECK( status.converged );

      // Conversion to Distance

      // Final Conditions

      string filename = "tmp/Solve2D_DGBR2_Poisson";
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );

    } //grid refinment loop
  } //order loop
//########################################################################################

  BOOST_AUTO_TEST_SUITE_END()

}
