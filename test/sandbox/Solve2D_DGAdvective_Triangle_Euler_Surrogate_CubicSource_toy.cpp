// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGAdvective_Triangle_Euler_Surrogate_CubicSource_btest
// testing of 2-D DG Advective for Euler on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGAdvective_Triangle_Euler_Surrogate_CubicSource_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGAdvective_Triangle_Bump10 )
{
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputEntropyErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquaredClass> NDOutputEntropyErrorSquaredClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputEntropyErrorSquaredClass> IntegrandSquareErrorClass;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE
  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Momentum);
  int nSol = NDPDEClass::N;

  // BC
  // Create a BC dictionary
  PyDict BCSymmetry;

#if 1
// PX STYLE BC:
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;
#else
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry;
#endif

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt;

  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.sSpec] = 0.0;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.HSpec] = 3.563;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.qtSpec] = 0;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure>::params.pSpec] = 1.0;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2}; //bottom and top
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {3};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  //typedef DLA::VectorS<PhysD2::D, ArrayQ> GradArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  Real sExact = 0.0;
  NDOutputEntropyErrorSquaredClass outEntropyError( pde, sExact );
  IntegrandSquareErrorClass fcnErr( outEntropyError, {0} );

  // norm data
//  Real normVec[10];   // L2 error
//  int indx = 0;

//  fstream fout( "tmp/cubicsource_results.txt", fstream::out );

  int ordermin = 1;
  int ordermax = 1;

  for (int order = ordermin; order <= ordermax; order++)
  {
    //indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
    int powermax = 2;

    for (int power = powermin; power <= powermax; power++)
    {

      int order0 = 0;

      jj = pow( 2, power );
      ii = 3*jj; //small grid!
      Real tau = 0.1; //10% bump
      int fieldorder = 3;

      // grid: 2D Cubic Source Bump w/ Q2 mesh
      XField2D_CubicSourceBump_Xq xfld(ii, jj, tau, fieldorder);

      // DG solution field
      //solution at inlet is (1, 0.1, 0, 1);
      ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(1, 0.354964787, 0, 1) );

      //////////////////////
      // SET UP P0 solve
      ///////////////////////

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld0(xfld, order0, BasisFunctionCategory_Legendre);

      const int nDOFPDE0 = qfld0.nDOF();

      // Set the initial condition
      qfld0 = q0;

      // Lagrange multiplier:
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
        lgfld0( xfld, order0, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      lgfld0 = 0;

      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-11, 1e-11};

      ////////////
      //SOLVE P0 SYSTEM
      ////////////

      PrimalEquationSetClass PrimalEqSet0(xfld, qfld0, lgfld0, pde, quadratureOrder,
                                          ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> Solver0( PrimalEqSet0, NewtonSolverDict );

      SystemVectorClass ini0(PrimalEqSet0.vectorStateSize());
      SystemVectorClass sln0(PrimalEqSet0.vectorStateSize());
      SystemVectorClass slnchk0(PrimalEqSet0.vectorStateSize());
      SystemVectorClass rsd0(PrimalEqSet0.vectorEqSize());
      rsd0 = 0;

      PrimalEqSet0.fillSystemVector(ini0);
      sln0 = ini0;

      Real rsdPDEnrm0[4] = {0,0,0,0};


      bool solved;
      solved = Solver0.solve(ini0,sln0).converged;
      BOOST_REQUIRE(solved);
      PrimalEqSet0.setSolutionField(sln0);

      rsd0 = 0;
      PrimalEqSet0.residual(sln0, rsd0);

      // check that the residual is zero

      for (int j = 0; j < nSol; j++)
        rsdPDEnrm0[j] = 0;

      for (int n = 0; n < nDOFPDE0; n++)
        for (int j = 0; j < nSol; j++)
          rsdPDEnrm0[j] += pow(rsd0[0][n][j],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm0[0]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm0[1]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm0[2]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm0[3]), 1e-12 );
#if 0
      // Tecplot dump grid
      string filename0 = "tmp/pseqDG_EulerSurrogateBump_P";
      filename0 += to_string(order);
      filename0 += "_Q";
      filename0 += to_string(fieldorder);
      filename0 += "_";
      filename0 += to_string(ii);
      filename0 += "x";
      filename0 += to_string(jj);
      filename0 += ".plt";
      output_Tecplot( qfld0, filename0 );
#endif

      ////////////////////////////////////
      // PROJECT P0 SOLUTION TO HIGHER P
      ///////////////////////////////////

      if (order !=0)
      {
        Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

        const int nDOFPDE = qfld.nDOF();

        // Set the initial condition
        qfld0.projectTo(qfld);

        // Lagrange multiplier:
        Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
          lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
        lgfld = 0;

        //////////////////////////
        //SOLVE HIGHER P SYSTEM
        //////////////////////////

        PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                           ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

        NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

        SystemVectorClass ini(PrimalEqSet.vectorStateSize());
        SystemVectorClass sln(PrimalEqSet.vectorStateSize());
        SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
        SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
        rsd = 0;

        PrimalEqSet.fillSystemVector(ini);
        sln = ini;

        Real rsdPDEnrm[4] = {0,0,0,0};

        solved = Solver.solve(ini,sln).converged;
        BOOST_REQUIRE(solved);
        PrimalEqSet.setSolutionField(sln);

        // check that the residual is zero

        for (int j = 0; j < nSol; j++)
          rsdPDEnrm[j] = 0;

        for (int n = 0; n < nDOFPDE; n++)
          for (int j = 0; j < nSol; j++)
            rsdPDEnrm[j] += pow(rsd[0][n][j],2);

        BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[0]), 1e-12 );
        BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[1]), 1e-12 );
        BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[2]), 1e-12 );
        BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[3]), 1e-12 );

        // Monitor Entropy Error
        Real EntropySquareError = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( fcnErr, EntropySquareError ),
            xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
#define SANS_VERBOSE
#ifdef SANS_VERBOSE
        cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( EntropySquareError );
//        if (indx > 1)
//          cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << endl;
#endif
#if 0
        // Tecplot dump grid
        string filename = "tmp/slnDG_EulerSurrogateBump_P";
        filename += to_string(order);
        filename += "_Q";
        filename += to_string(fieldorder);
        filename += "_";
        filename += to_string(ii);
        filename += "x";
        filename += to_string(jj);
        filename += ".plt";
        output_Tecplot( qfld, filename );
#endif
      }
      else
      {
        // Monitor Entropy Error
        Real EntropySquareError = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( fcnErr, EntropySquareError ),
            xfld, qfld0, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
#define SANS_VERBOSE
#ifdef SANS_VERBOSE
        cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( EntropySquareError );
//        if (indx > 1)
//          cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << endl;
#endif
#if 0
        // Tecplot dump grid
        string filename0 = "tmp/slnDG_EulerSurrogateBump_P";
        filename0 += to_string(order);
        filename0 += "_Q";
        filename0 += to_string(fieldorder);
        filename0 += "_";
        filename0 += to_string(ii);
        filename0 += "x";
        filename0 += to_string(jj);
        filename0 += ".plt";
        output_Tecplot( qfld0, filename0 );
#endif
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
