// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "tools/linspace.h"
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Topology/Dimension.h"
#include "pde/Distance/BCEikonal_Distance2D.h"
#include "pde/Distance/PDEEikonal_Distance2D.h"

#include "pde/BCParameters.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/output_Tecplot.h"

#include "Meshing/EPIC/XField_PX.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/Direct/MKL_PARDISO/MKL_PARDISOSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#include "Field/DistanceFunction/DistanceFunction.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

//Instantiate
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"


using namespace std;
using namespace SANS;

//#######################################################################//
BOOST_AUTO_TEST_SUITE (Solve2D_DGBR2_Eikonal_test_suite)

//-----------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Eikonal )
{
  typedef PDEEikonal_Distance2D PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef BCEional2DVector BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
          AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2> > PrimalEquationSetClass;

  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // Newton Solver Set Up
  PyDict NewtonSolverDict, LineUpdateDict;
#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-12;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  // Check inputs
  NewtonSolverParam::checkInputs( NewtonSolverDict );

  //PDE

  NDPDEClass pde;

  //BC

  //int iB = XField2D_Box_Triangle_X1::iBottom;
  //int iR = XField2D_Box_Triangle_X1::iRight;
  //int iT = XField2D_Box_Triangle_X1::iTop;
  //int iL = XField2D_Box_Triangle_X1::iLeft;

#if 1
  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1; //Value
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCFar;
  //BCFar[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  //BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  //BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  //BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;
  BCFar[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCAdia;
  BCAdia[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 0;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 1;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 1;
#else
  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitState>::params.qB] = 0;

  PyDict BCFar;
  BCFar[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitState>::params.qB] = 0;

  PyDict BCAdia;
  BCAdia[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitState>::params.qB] = 0;
#endif

  PyDict PyBCList;

  PyBCList["BCWall"] = BCWall;
  PyBCList["BCFar"] = BCFar;
  PyBCList["BCAdia"] = BCAdia;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["BCWall"] = { 0 }; //iB, iR, iL, iT };
  BCBoundaryGroups["BCFar"]  = { 1, 2 }; //iT };
  BCBoundaryGroups["BCAdia"] = {  };

  BCParams::checkInputs( PyBCList );

  int ordermin = 2;
  int ordermax = 2;

  for (int order = ordermin; order <= ordermax; order++)
  {
    int powermin = 5;
    int powermax = 5;

    for (int power = powermin; power <= powermax; power++)
    {

      int ii = 50;
#if 0
      int jj = ii;
      int xmin = 0.0;
      int xmax = 1.;
      int ymin = 0.0;
      int ymax = 1.;

      XField2D_Box_Triangle_X1 xfld( ii, jj, xmin, xmax, ymin, ymax );
      std::vector<int> interiorTracGroups = {0,1,2};
#else
      // Mesh Read in
      mpi::communicator world;

      //string filein = "grids/Airfoil_with_BCs_spline_experimentv5";
      string filein = "grids/Joukowski_Laminar_Challenge_tri_ref0_Q3";
      filein += ".grm";
      XField_PX<PhysD2, TopoD2> xfld(world, filein);
      std::vector<int> interiorTracGroups = {0};

#endif

      // BR2 discretization
      Real viscousEtaParameter = 9;
      DiscretizationDGBR2 disc( 0, viscousEtaParameter );

      //Intergration
      QuadratureOrder quadratureOrder( xfld, 3*order );
      std::vector<Real> tol = { 1e-8, 1e-8 };

      // lifting operators
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld( xfld, order, BasisFunctionCategory_Legendre );
      rfld = 0.0;

      //Create Solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld( xfld, order, BasisFunctionCategory_Legendre);

      qfld = 0.1;

      //Lagrange Multipliers

      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                            BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups ) );
      lgfld = 0;

      // Spatial Discritization
      PrimalEquationSetClass PrimalEqSet( xfld, qfld, rfld, lgfld, pde, disc,
                                          quadratureOrder, ResidualNorm_Default, tol,
                                          { 0 }, interiorTracGroups, PyBCList, BCBoundaryGroups );
      NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );
      // set initial condition from current solution in solution fields
      SystemVectorClass sln0( PrimalEqSet.vectorStateSize() );
      PrimalEqSet.fillSystemVector( sln0 );

      // nonlinear solve
      SystemVectorClass sln( PrimalEqSet.vectorStateSize() );
      SolveStatus status = nonlinear_solver.solve( sln0, sln );
      BOOST_CHECK( status.converged );

#if 0
      typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
      PrimalEqSet.jacobian(nz);

      SystemMatrixClass jac(nz);
      PrimalEqSet.jacobian(jac);

      WriteMatrixMarketFile(jac, "tmp/jac.mtx");
#endif

      // Final Conditions

      //Create distance field
      Field_DG_Cell<PhysD2, TopoD2, Real> distfld( xfld, order, BasisFunctionCategory_Lagrange );

      //Compute distance field
      DistanceFunction(distfld, {0});

#if 0
      std::ifstream eig_vec("tmp/eig_vecL.csv");

      for (int i = 0; i < distfld.nDOF(); i++)
        eig_vec >> distfld.DOF(i);
#endif

      {
        string filename = "tmp/Solve2D_DGBR2_Eikonal_Distance_Box";
        filename += "_Q" + std::to_string(order);
        filename += "_" + to_string( ii );
        filename += ".plt";
        cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( qfld, filename );
      }

      {
        string filename = "tmp/Solve2D_DGBR2_Distance_Box";
        filename += "_Q" + std::to_string(order);
        filename += "_" + to_string( ii );
        filename += ".plt";
        cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( distfld, filename );
      }

    } //grid refinment loop
  } //order loop
//########################################################################################

  BOOST_AUTO_TEST_SUITE_END()

}
