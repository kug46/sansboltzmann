// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_Burgers_ArtificialViscosity_ST_toy
// Testing of the MOESS framework on a space-time Buckley-Leverett problem

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/Burgers/PDEBurgers_ArtificialViscosity1D.h"
#include "pde/Burgers/BCmitAVSensorBurgers1D.h"
#include "pde/Burgers/BurgersConservative1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "pde/OutputCell_SolutionSquared.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2_Block2x2.h"
#include "Discretization/DG/SolutionData_DGBR2_Block2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_DGBR2_Burgers_ArtificialViscosity_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_Burgers_ArtificialViscosity_ST_Triangle )
{
  typedef BurgersConservative1D QType;
  typedef ViscousFlux1D_Uniform DiffusionModel;
  typedef Source1D_UniformGrad SourceModel;

  typedef PDEBurgers<PhysD1,
                     QType,
                     DiffusionModel,
                     SourceModel > PDEClass;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;

  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         QType,
                                         DiffusionModel,
                                         SourceModel > PDEClass_Primal;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_Primal> NDPDEClass_Primal;

//  typedef ScalarFunction1D_PiecewiseLinear SolutionExact;
//  typedef SolnNDConvertSpaceTime<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCmitAVSensorBurgers1DVector<QType, DiffusionModel, SourceModel> BCVectorPrimal;

  typedef BCParameters<BCVectorPrimal> BCParamsPrimal;

//  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef Sensor_AdvectiveFlux1D_Uniform Sensor_Advection;

//  typedef Sensor_ViscousFlux1D_Uniform Sensor_Diffusion;
//  typedef Sensor_ViscousFlux1D_GridDependent Sensor_Diffusion;
  typedef Sensor_ViscousFlux1D_GenHScale Sensor_Diffusion;

  typedef PDESensorParameter<PhysD1,
                             SensorParameterTraits<PhysD1>,
                             Sensor_Advection,
                             Sensor_Diffusion,
                             Source_JumpSensor > PDEClass_Sensor;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_Sensor> NDPDEClass_Sensor;

  typedef BCSensorParameter1DVector<Sensor_Advection, Sensor_Diffusion> BCVectorSensor;

  typedef BCParameters<BCVectorSensor> BCParamsSensor;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_ArtificialViscosity ParamBuilderType;

  //Solution data
  typedef SolutionData_DGBR2_Block2<PhysD2, TopoD2, NDPDEClass_Primal, NDPDEClass_Sensor, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType0 ParamFieldType0;
  typedef typename SolutionClass::ParamFieldType1 ParamFieldType1;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Primal, BCNDConvertSpaceTime, BCVectorPrimal,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType0> PrimalEquationSetClass_Primal;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Sensor, BCNDConvertSpaceTime, BCVectorSensor,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType1> PrimalEquationSetClass_Sensor;


  typedef SolverInterface_DGBR2_Block2x2<PhysD2, TopoD2,
                                         NDPDEClass_Primal, BCNDConvertSpaceTime, BCVectorPrimal,
                                         NDPDEClass_Sensor, BCNDConvertSpaceTime, BCVectorSensor,
                                         ParamBuilderType, SolutionClass,
                                         PrimalEquationSetClass_Primal, PrimalEquationSetClass_Sensor,
                                         OutputIntegrandClass> SolverInterfaceClass;


  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  int ii = 20;
  int jj = ii;
  Real Tend = 2;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, jj, -1, 4, 0, Tend, false );

  int order_primal = 1;
  int order_sensor = 1;


  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Burgers PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  Real nu = 0.0;

  DiffusionModel diffusion(nu);
  SourceModel source(0.0, 0.0);

  NDPDEClass pde(diffusion, source);
  NDPDEClass_Primal pde_primal(order_primal, pde);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict SolnDict;
  SolnDict[BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params.Function.Name]
           = BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params.Function.PiecewiseLinear;

  PyDict BCDirichletB;
  BCDirichletB[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.FunctionLinearRobin_sansLG;
  BCDirichletB[BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params.Function] = SolnDict;

  PyDict BCNone;
  BCNone[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.None;

#if 1
  PyDict BCDirichletL;
  BCDirichletL[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.Dirichlet_mitState;
  BCDirichletL[BCmitAVSensorBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam>::params.qB] = 2.0;

  PyDict BCDirichletR;
  BCDirichletR[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.Dirichlet_mitState;
  BCDirichletR[BCmitAVSensorBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam>::params.qB] = 1.0;
#else
  PyDict BCDirichletL;
  BCDirichletL[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.LinearRobin_sansLG;
  BCDirichletL[BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCDirichletL[BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCDirichletL[BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 2.0;

  PyDict BCDirichletR;
  BCDirichletR[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.LinearRobin_sansLG;
  BCDirichletR[BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCDirichletR[BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCDirichletR[BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;
#endif

  PyDict PyBCList_Primal;
  PyBCList_Primal["DirichletB"] = BCDirichletB;
  PyBCList_Primal["DirichletR"] = BCDirichletR;
  PyBCList_Primal["DirichletL"] = BCDirichletL;
  PyBCList_Primal["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Primal;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_Primal["DirichletB"] = {0}; //Bottom boundary
  BCBoundaryGroups_Primal["DirichletR"] = {1}; //Bottom and right boundary
  BCBoundaryGroups_Primal["DirichletL"] = {3}; //Left boundary
  BCBoundaryGroups_Primal["None"] = {2}; //Top boundary

  //Check the BC dictionary
  BCParamsPrimal::checkInputs(PyBCList_Primal);

  std::vector<int> active_boundaries_primal = BCParamsPrimal::getLGBoundaryGroups(PyBCList_Primal, BCBoundaryGroups_Primal);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our sensor parameter PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  Sensor sensor(pde_primal);
  Sensor_Advection sensor_adv(0.0);
  Sensor_Diffusion sensor_visc(1.0);
  Source_JumpSensor sensor_source(order_primal, sensor);

  NDPDEClass_Sensor pde_sensor(sensor_adv, sensor_visc, sensor_source);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict BCSensorRobin;
  BCSensorRobin[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorNeumann;
  BCSensorNeumann[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorNeumann[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 0.0;
  BCSensorNeumann[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCSensorNeumann[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorNone;
  BCSensorNone[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.None;

  PyDict PyBCList_Sensor;
  PyBCList_Sensor["SensorRobin"] = BCSensorRobin;
//  PyBCList_Sensor["SensorNeumann"] = BCSensorNeumann;
//  PyBCList_Sensor["SensorNone"] = BCSensorNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Sensor;
  BCBoundaryGroups_Sensor["SensorRobin"] = {0,1,2,3}; // Bottom, right, left
//  BCBoundaryGroups_Sensor["SensorNeumann"] = {2}; // Bottom, right, left
//  BCBoundaryGroups_Sensor["SensorNone"] = {2}; // Top

  //Check the BC dictionary
  BCParamsSensor::checkInputs(PyBCList_Sensor);

  std::vector<int> active_boundaries_sensor = BCParamsSensor::getLGBoundaryGroups(PyBCList_Sensor, BCBoundaryGroups_Sensor);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create discretisation parameters
  ////////////////////////////////////////////////////////////////////////////////////////
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol0 = {1e-12, 1e-12};
  std::vector<Real> tol1 = {1e-7, 1e-7};

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create output functional to adapt with
  ////////////////////////////////////////////////////////////////////////////////////////
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);


  ////////////////////////////////////////////////////////////////////////////////////////
  // Adaptation parameters
  ////////////////////////////////////////////////////////////////////////////////////////
  int maxIter = 20;
  Real targetCost = 10000;

  std::string adapthist_filename = "tmp/test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create solver interface
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef SolutionData_DGBR2_Block2<PhysD2, TopoD2, NDPDEClass_Primal, NDPDEClass_Sensor, ParamBuilderType> SolutionClass;
  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde_primal, pde_sensor,
                                               order_primal, order_sensor, order_primal+1, order_sensor+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries_primal, active_boundaries_sensor, disc, disc );

  const int quadOrder = 2*(order_primal + 1);

  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, ResNormType, tol0, tol1, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList_Primal, PyBCList_Sensor,
                                                      BCBoundaryGroups_Primal, BCBoundaryGroups_Sensor,
                                                      SolverContinuationDict, outputIntegrand);


  ////////////////////////////////////////////////////////////////////////////////////////
  // Set Initial condition
  ////////////////////////////////////////////////////////////////////////////////////////
//  pGlobalSol->setSolution0(solnExact, cellGroups);
  pGlobalSol->setSolution(0.5, 0.0);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output Initial Condition
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string qfld0_init_filename = "tmp/qfld0_init_a0.plt";
  std::string qfld1_init_filename = "tmp/qfld1_init_a0.plt";
  output_Tecplot( pGlobalSol->primal0.qfld, qfld0_init_filename );
  output_Tecplot( pGlobalSol->primal1.qfld, qfld1_init_filename );


  ////////////////////////////////////////////////////////////////////////////////////////
  // Solve Primal and output
  ////////////////////////////////////////////////////////////////////////////////////////
  pInterface->solveGlobalPrimalProblem();
  std::string qfld0_filename = "tmp/qfld0_a0.plt";
  std::string qfld1_filename = "tmp/qfld1_a0.plt";
  output_Tecplot( pGlobalSol->primal0.qfld, qfld0_filename );
  output_Tecplot( pGlobalSol->primal1.qfld, qfld1_filename );


  ////////////////////////////////////////////////////////////////////////////////////////
  // Solve Dual and output
  ////////////////////////////////////////////////////////////////////////////////////////
  pInterface->solveGlobalAdjointProblem();
  std::string adjfld0_filename = "tmp/adjfld0_a0.plt";
  std::string adjfld1_filename = "tmp/adjfld1_a0.plt";
  output_Tecplot( pGlobalSol->adjoint0.qfld, adjfld0_filename );
  output_Tecplot( pGlobalSol->adjoint1.qfld, adjfld1_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<std::endl<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde_primal, pde_sensor,
                                                    order_primal, order_sensor, order_primal+1, order_sensor+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries_primal, active_boundaries_sensor, disc, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, ResNormType,
                                                           tol0, tol1, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList_Primal, PyBCList_Sensor,
                                                           BCBoundaryGroups_Primal, BCBoundaryGroups_Sensor,
                                                           SolverContinuationDict, outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

//    hfld_filename = "tmp/hfld_a" + std::to_string(iter+1) + ".plt";
//    output_Tecplot( get<0>(pGlobalSol->paramfld0), hfld_filename );

    qfld0_init_filename = "tmp/qfld0_init_a" + std::to_string(iter+1) + ".plt";
    qfld1_init_filename = "tmp/qfld1_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal0.qfld, qfld0_init_filename );
    output_Tecplot( pGlobalSol->primal1.qfld, qfld1_init_filename );

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    qfld0_filename = "tmp/qfld0_a" + std::to_string(iter+1) + ".plt";
    qfld1_filename = "tmp/qfld1_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal0.qfld, qfld0_filename );
    output_Tecplot( pGlobalSol->primal1.qfld, qfld1_filename );

    adjfld0_filename = "tmp/adjfld0_a" + std::to_string(iter+1) + ".plt";
    adjfld1_filename = "tmp/adjfld1_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint0.qfld, adjfld0_filename );
    output_Tecplot( pGlobalSol->adjoint1.qfld, adjfld1_filename );
  }

  fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
