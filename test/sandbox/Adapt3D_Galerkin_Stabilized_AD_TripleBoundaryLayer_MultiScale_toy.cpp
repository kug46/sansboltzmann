// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_MultiScale_btest
// Testing of the MOESS framework on the advection-diffusion pde

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"

#include "Adaptation/callMesher.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"

#include "Meshing/refine/MultiScale_metric.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/libMeshb/WriteSolution_libMeshb.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_AD_TripleBoundaryLayer_MultiScale_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_AD_TripleBoundaryLayer_MultiScale_Triangle )
{
  typedef ScalarFunction3D_TripleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputNDConvertSpace<PhysD3, OutputCell_SolutionErrorSquared<PDEClass, SolutionExact>> L2ErrorClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<L2ErrorClass,NDPDEClass> L2ErrorIntegrandClass;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass,NDPDEClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  #define USE_AVRO 0

  // global communicator
  mpi::communicator world;

  // Grid
  int ii = 5;
  int jj = ii;
  int kk = ii;

  Real a = 1.0;
  Real b = 1.0;
  Real c = 1.0;
  Real nu = 0.01;

  // PDE
  AdvectiveFlux3D_Uniform adv( a, b, c );

  ViscousFlux3D_Uniform visc( nu, 0, 0, 0, nu, 0, 0, 0, nu );

  Source3D_UniformGrad source(0,0,0,0);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function.Name] =
           BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function.TripleBL;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.c] = c;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;
  solnArgs[NDSolutionExact::ParamsType::params.offset] = 1;
  solnArgs[NDSolutionExact::ParamsType::params.scale] = -1;

  NDSolutionExact solnExact( solnArgs );

  NDPDEClass pde( adv, visc, source );

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Upwind] = true;

  PyDict PyBCList;
  PyBCList["BCSoln_Dirichlet"] = BCSoln_Dirichlet;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSoln_Dirichlet"] = {0,1,2,3,4,5};

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Galerkin Stabilization
  StabilizationMatrix stab(StabilizationType::Unstabilized);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-8, 1e-8};

  //Output functional
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, stab);
  const Real trueOutput = 0.015073625; // From Mathematica

  // L2 Error output functional
  L2ErrorClass L2ErrorOutput(solnExact);
  L2ErrorIntegrandClass L2ErrorIntegrand(pde, L2ErrorOutput, {0}, stab);

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 )
      std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-20;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDict_adjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-20;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  int powerL = 3, powerH = 8;
  std::string mesher = "refine";
  std::string recon = "kexact";

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc == 4)
  {
    powerL = powerH = std::stoi(argv[1]);
    mesher = std::string(argv[2]);
    recon = std::string(argv[3]);
  }

  std::cout << "powerL = " << powerL << ", powerH = " << powerH << std::endl;
#endif

#if USE_AVRO
  mesher = "avro";
#endif

  PyDict MesherDict;
  if (mesher == "refine")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.refine;
    MesherDict[refineParams::params.DumpRefineDebugFiles] = false;
  #ifdef SANS_REFINE
    //MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;
    MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.system;
  #endif
  }
  else if (mesher == "EPIC")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
    std::vector<int> allBC = {0,1,2,3,4,5};
    MesherDict[EpicParams::params.SymmetricSurf] = allBC;
  }
  else if (mesher == "fefloa")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.FeFloa;
  }
#ifdef SANS_AVRO
  else if (mesher == "avro")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.avro;
    MesherDict[avroParams::params.Curved] = false;
  }
#endif
  else
    BOOST_REQUIRE_MESSAGE(false, "Unknown mesh generator.");

  const int order = 1;
  const int order_adj = order+1;

  const int maxIter = 30;
  const bool DGCONV = true; // convert to DG element counts

  //--------ADAPTATION LOOP--------
  for (int power = powerL; power <= powerH; power++ )
  {
    int nk = pow(2,power);
    int targetCost = 1000*nk;

    if (DGCONV)
    {
      Real nDOFperCell_DG = (order+1)*(order+2)*(order+3)/6;

      Real nDOFperCell_CG = nDOFperCell_DG;
      nDOFperCell_CG -= (4 - 1./5); // the node dofs are shared by 20
      nDOFperCell_CG -= (6 - 1)*std::max(0,(order-1)); // if there are edge dofs they are shared by 6
      nDOFperCell_CG -= (4 - 2)*std::max(0,(order-1)*(order-2)/2); // if there are face dofs they are shared by 2

      targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
    }

    const int string_pad = 6;
    std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);

    std::string filename_base = "tmp/TripleBoundaryLayer/" + mesher + "_";

    if ( stab.getStabType() == StabilizationType::Adjoint )
    {
      filename_base += "VMS_";
    }
    else if ( stab.getStabType() == StabilizationType::Unstabilized )
    {
      filename_base += "CG_";
    }
    else if ( stab.getStabType() == StabilizationType::GLS )
    {
      filename_base += "GLS_";
    }
    else if ( stab.getStabType() == StabilizationType::SUPG )
    {
      filename_base += "SUPG_";
    }

    filename_base += "MultiScale_" + int_pad + "_P" + std::to_string(order) + "/";

    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directories(base_dir);

    std::string adapthist_filename = filename_base + "test.adapthist";
    fstream fadapthist( adapthist_filename, fstream::out );
    if ( world.rank() == 0 )
    {
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
    }

    std::string output_filename = filename_base + "output.dat";
    fstream foutputhist( output_filename, fstream::out );
    if ( world.rank() == 0 )
    {
      BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + output_filename);
    }

    MesherDict[EpicParams::params.FilenameBase] = filename_base;

    if (world.rank() == 0 )
    {
      // write the header to the output file
      foutputhist << "VARIABLES="
                  << std::setw(5)  << "\"Iter\""
                  << std::setw(10) << "\"DOF\""
                  << std::setw(20) << "\"Elements\""
                  << std::setw(20) << "\"u<sup>2</sup>\""
                  << std::setw(20) << "\"u<sup>2</sup> Error\""
                  << std::setw(20) << "\"Estimate\""
                  << std::setw(20) << "\"L<sup>2</sup> Error\""
                  << std::endl;

      foutputhist << "ZONE T=\"MultiScale " << mesher << " " << nk << "k\"" << std::endl;
    }


    std::shared_ptr<XField<PhysD3, TopoD3>> pxfld = std::make_shared<XField3D_Box_Tet_X1>( world, ii, jj, kk );

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Solution data
    std::shared_ptr<SolutionClass> pGlobalSol;
    pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order_adj,
                                                 BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                 active_boundaries, stab );

    const int quadOrder = 2*(order + 1);

    //Create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, LinearSolverDict,
                                                        outputIntegrand);

    //Set initial solution
    pGlobalSol->setSolution(0.0);

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    //Compute error estimates
    pInterface->computeErrorEstimates();

    for (int iter = 0; iter < maxIter+1; iter++)
    {
      if (world.rank() == 0)
        std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

      //Perform local sampling and adapt mesh
      std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew;
      try // Call refine to perform multiscale metric adaptation
      {
#ifdef SANS_REFINE
      refine_recon_reconstructions recons;

      if (recon == "L2")
        recons = refine_recon_L2projection;
      else if (recon == "kexact")
        recons = refine_recon_Kexact;
      else
        SANS_DEVELOPER_EXCEPTION("Unknown reconstruction: %s", recon.c_str());

      const int p = 2;
      const Real gradation = 5;

      const Real nDOFperCell = 4;
      const Real equiVol = sqrt(2.0)/12.0;
      const Real targetComp = targetCost*equiVol/nDOFperCell;

      typedef DLA::MatrixSymS<PhysD3::D, Real> MatrixSym;

      // compute the multi-scale metric
      Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_req(*pxfld, 1, BasisFunctionCategory_Hierarchical);
      MultiScale_metric(recons, p, gradation, targetComp, pGlobalSol->primal.qfld, metric_req);

      // pass metric along to the mesher
#if defined(SANS_AVRO) && USE_AVRO
      MesherInterface<PhysD3, TopoD3, avro> mesher(iter, MesherDict);
      pxfldNew = mesher.adapt(metric_req);
#else
      pxfldNew = callMesher<PhysD3, TopoD3>::call(*pxfld, metric_req, iter, MesherDict).first;
#endif

#else
        BOOST_REQUIRE_MESSAGE(false, "Must be compiled with cmake -DUSE_REFINE=ON");
#endif
      }
      catch (...)
      {
        WriteMesh_libMeshb( *pxfld , filename_base+"/refine_debug_"+std::to_string(iter)+".meshb" );
        WriteSolution_libMeshb( pGlobalSol->primal.qfld , filename_base+"/refine_debug_"+std::to_string(iter)+".solb" );
        throw;
      }

      interiorTraceGroups.clear();
      for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<SolutionClass> pGlobalSolNew;
      pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order_adj,
                                                      BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                      active_boundaries, stab);

      //Perform L2 projection from solution on previous mesh
      pGlobalSolNew->setSolution(*pGlobalSol);

      std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
      pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                             cellGroups, interiorTraceGroups,
                                                             PyBCList, BCBoundaryGroups,
                                                             SolverContinuationDict, LinearSolverDict,
                                                             outputIntegrand);

      //Update pointers to the newest problem (this deletes the previous mesh and solutions)
      pxfld = pxfldNew;
      pGlobalSol = pGlobalSolNew;
      pInterface = pInterfaceNew;
      //
      // std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
      // output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

#if 0
      WriteMesh_libMeshb( *pxfld , filename_base+"/refine_a"+std::to_string(iter)+".meshb" );
      WriteSolution_libMeshb( pGlobalSol->primal.qfld, filename_base+"/refine_primal_a"+std::to_string(iter)+".solb" );
      WriteSolution_libMeshb( pInterface->getAdjField() , filename_base+"/refine_adjoint_a"+std::to_string(iter)+".solb" );
#endif

      //Compute error estimates
      pInterface->computeErrorEstimates();

#ifdef SANS_MPI
      int nDOFtotal = 0;
      boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );

      // count the number of elements possessed by this processor
      int nElem = 0;
      for (int elem = 0; elem < pxfld->nElem(); elem++ )
        if (pxfld->getCellGroupGlobal<Tet>(0).associativity(elem).rank() == world.rank())
          nElem++;

      int nElemtotal = 0;
      boost::mpi::reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>(), 0 );
#else
      int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
      int nElemtotal = pxfld->nElem();
#endif

      Real output = pInterface->getOutput();
      Real globalEstimate  = pInterface->getGlobalErrorEstimate();

      QuadratureOrder quadrule(*pxfld, quadOrder);

      Real L2error = 0;
      IntegrateCellGroups<TopoD3>::integrate( FunctionalCell_Galerkin( L2ErrorIntegrand, L2error ),
                                              *pxfld, pGlobalSol->primal.qfld,
                                              quadrule.cellOrders.data(), quadrule.cellOrders.size() );

#ifdef SANS_MPI
      L2error = boost::mpi::all_reduce( *pxfld->comm(), L2error, std::plus<Real>() );
#endif
      L2error = sqrt(L2error);

      if (world.rank() == 0 )
      {
        foutputhist << std::setw(5) << iter
                    << std::setw(10) << nDOFtotal
                    << std::setw(10) << nElemtotal
                    << std::setw(20) << std::setprecision(10) << std::scientific << output
                    << std::setw(20) << std::setprecision(10) << std::scientific << fabs(output-trueOutput)
                    << std::setw(20) << std::setprecision(10) << std::scientific << globalEstimate
                    << std::setw(20) << std::setprecision(10) << std::scientific << L2error
                    << std::endl;
      }
    }

    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(maxIter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(maxIter+1) + ".plt";
    output_Tecplot( pInterface->getAdjField(), adjfld_filename );

    foutputhist.close();
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
