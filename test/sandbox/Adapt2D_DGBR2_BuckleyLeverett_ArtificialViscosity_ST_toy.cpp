// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_BuckleyLeverett_ST_btest
// Testing of the MOESS framework on a space-time Buckley-Leverett problem with artificial viscosity

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverettArtificialViscosity.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"
#include "pde/PorousMedia/OutputTwoPhase.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_BuckleyLeverett_ArtificialViscosity_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_BuckleyLeverett_ArtificialViscosity_ST_Triangle )
{
  typedef QTypePrimitive_Sw QType;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef TraitsModelBuckleyLeverett<QType, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEBL_ExtendedState;
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeBuckleyLeverettArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeBuckleyLeverettArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QType, PDEBL_ExtendedState> ExactSolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef BCBuckleyLeverett_ArtificialViscosity1DVector<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputTwoPhase_SaturationPower<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_GenH_DG ParamBuilderType;
  typedef GenHField_DG<PhysD2, TopoD2> GenHFieldType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  timer clock;
  mpi::communicator world;

  // Grid
  int ii = 50;
  int jj = 25;

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, jj, 0, 50, 0, 25, true );
//  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField_libMeshb<PhysD2, TopoD2>>( world, "tmp/mesh_a5.mesh" );

  int iB = XField2D_Box_Triangle_X1::iBottom;
  int iR = XField2D_Box_Triangle_X1::iRight;
  int iT = XField2D_Box_Triangle_X1::iTop;
  int iL = XField2D_Box_Triangle_X1::iLeft;

  int order = 1;

  // Primal PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 0.0;
  CapillaryModel cap_model(pc_max);

  //Create original BL PDE with extended state vector
  PDEBL_ExtendedState pdeBL(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  //Create artificial viscosity PDE
  bool hasSpaceTimeDiffusionTwoPhase = false;
  PDEBaseClass pde_BLAV(order, hasSpaceTimeDiffusionTwoPhase, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  SensorAdvectiveFlux sensor_adv(0.0);
  bool hasSpaceTimeDiffusionAV = true;
  const Real diffusionConstantAV = 3.0;
  SensorViscousFlux sensor_visc(order, hasSpaceTimeDiffusionAV, 1.0, diffusionConstantAV);
  const Real Sk_min = -3;
  const Real Sk_max = -1;
  SensorSource sensor_source(pde_BLAV, Sk_min, Sk_max);

  // AV PDE with sensor equation
  bool isSteady = true;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady,
                 order, hasSpaceTimeDiffusionTwoPhase, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 1.0;
  const Real SwR = 0.1;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(pdeBL, SwL, SwR, xinit);

  const Real SwInit = SwR;
  const Real nuInit = 1.0;
  ArrayQ qInit = {SwInit, nuInit};
//  AVVariable<PressureNonWet_SaturationWet,Real> qdata({pwInit, SwInit}, sensorInit);
//  pde.setDOFFrom( qInit, qdata );

  // Primal PDE BCs
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitStateSpaceTime,
                                BCBuckleyLeverett1DParams<BCTypeTimeIC>> BCParamsAV_TimeIC;
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitStateSpaceTime,
                                BCBuckleyLeverett1DParams<BCTypeDirichlet_mitStateSpaceTime>> BCParamsAV_Dirichlet_mitStateST;

  // Create a Solution dictionary
//  PyDict SolnDict;
//  SolnDict[BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.Name]
//           = BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.PiecewiseConst;
//  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.a0] = SwL;
//  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.a1] = SwR;
//  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.x0] = xinit;

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

//  PyDict BCDirichletB;
//  BCDirichletB[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
//  BCDirichletB[BCParamsAV_Sol::params.Function] = SolnDict;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  TimeIC[BCParamsAV_TimeIC::params.qB] = SwR;
  TimeIC[BCParamsAV_TimeIC::params.Cdiff] = diffusionConstantAV;

  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitStateSpaceTime;
  BCDirichletL[BCParamsAV_Dirichlet_mitStateST::params.qB] = SwL;
  BCDirichletL[BCParamsAV_Dirichlet_mitStateST::params.Cdiff] = diffusionConstantAV;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitStateSpaceTime;
  BCDirichletR[BCParamsAV_Dirichlet_mitStateST::params.qB] = SwR;
  BCDirichletR[BCParamsAV_Dirichlet_mitStateST::params.Cdiff] = diffusionConstantAV;
  BCDirichletR[BCParamsAV_Dirichlet_mitStateST::params.isOutflow] = true;

  PyDict PyBCList;
  PyBCList["TimeIC"] = TimeIC;
  PyBCList["DirichletR"] = BCDirichletR;
  PyBCList["DirichletL"] = BCDirichletL;
  PyBCList["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TimeIC"] = {iB};
  BCBoundaryGroups["DirichletR"] = {iR};
  BCBoundaryGroups["DirichletL"] = {iL};
  BCBoundaryGroups["None"] = {iT};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-10, 1e-10};

  //Output functional
  NDOutputClass fcnOutput(pde, 0, 2);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.0;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchResiduals] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);


  //Adaptation parameters
  int maxIter = 20;
  Real targetCost = 10000;

  std::string filename_base = "tmp/";
  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
//  MOESSDict[MOESSParams::params.UniformRefinement] = false;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Compute generalized log H-tensor field
  std::shared_ptr<GenHFieldType> pHfld = std::make_shared<GenHFieldType>(*pxfld);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>((*pHfld, *pxfld), pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

  const int quadOrder = 2*(order + 2);

  //Set initial solution
#if 1
//  pGlobalSol->setSolution(qInit);
  pGlobalSol->setSolution(solnExact, {0});
#else
  //Read solution
  std::fstream fqfld("tmp/qfld_init_a10.txt", std::fstream::in);
  for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
  {
    for (int j = 0; j < ArrayQ::M; j++)
      fqfld >> pGlobalSol->primal.qfld.DOF(i)[j];
//    pGlobalSol->primal.qfld.DOF(i)[1] = 0.0;
  }
  fqfld.close();
#endif

  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, {"Sw", "nu"}  );

#if 0
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  QuadratureOrder quadrule(*pxfld, quadOrder);

  PrimalEquationSetClass AlgEqSet(pGlobalSol->paramfld, pGlobalSol->primal, pGlobalSol->pliftedQuantityfld, pde, quadrule,
                                  ResNormType, tol, cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups);

  // residual
  SystemVectorClass q(AlgEqSet.vectorStateSize());
  AlgEqSet.fillSystemVector(q);

  SystemVectorClass rsd(AlgEqSet.vectorEqSize());
  rsd = 0;
  AlgEqSet.residual(q, rsd);

  std::cout << std::setprecision(5) << std::scientific << AlgEqSet.residualNorm(rsd) << std::endl;

  // jacobian nonzero pattern
  SystemNonZeroPattern nz(AlgEqSet.matrixSize());
  AlgEqSet.jacobian(q, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  jac = 0;
  AlgEqSet.jacobian(q, jac);

  fstream fout( "tmp/jac.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
  return;
#endif

  for (int iter = 0; iter < maxIter+1; iter++)
  {
    if (world.rank() == 0)
      std::cout << "\n" << std::string(25,'-') + "Adaptation Iteration " << iter << std::string(25,'-') << "\n\n";

    //Create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, LinearSolverDict,
                                                        outputIntegrand);
    pInterface->setBaseFilePath(filename_base);

#if 1 //Dump initial solution to file
    {
      std::fstream fqfld("tmp/qfld_init_a" + std::to_string(iter) + ".txt", std::fstream::out);
      fqfld << std::scientific << std::setprecision(15);
      for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
        for (int j = 0; j < ArrayQ::M; j++)
          fqfld << pGlobalSol->primal.qfld.DOF(i)[j] << std::endl;
      fqfld.close();
    }
#endif

    pInterface->solveGlobalPrimalProblem();
    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename, {"Sw", "nu"} );

    std::string liftedfld_filename = filename_base + "liftedfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( *(pGlobalSol->pliftedQuantityfld), liftedfld_filename, {"s"} );

#if 1 //Dump solution to file
    {
      std::fstream fqfld("tmp/qfld_a" + std::to_string(iter) + ".txt", std::fstream::out);
      fqfld << std::scientific << std::setprecision(15);
      for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
        for (int j = 0; j < ArrayQ::M; j++)
          fqfld << pGlobalSol->primal.qfld.DOF(i)[j] << std::endl;
      fqfld.close();
    }
#endif

    if (world.rank() == 0)
      std::cout << "Output: " << std::scientific << std::setprecision(12) << pInterface->getOutput() << std::endl;

    pInterface->solveGlobalAdjointProblem();
    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename, {"psi", "psinu"} );

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Compute generalized log H-tensor field on new mesh
    pHfld = std::make_shared<GenHFieldType>(*pxfldNew);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>((*pHfld,*pxfldNew), pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    pGlobalSolNew->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;

    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, {"Sw", "nu"}  );

    if (world.rank() == 0)
      std::cout<<"Time elapsed: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl << std::endl;
  }

  if (world.rank() == 0)
    fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
