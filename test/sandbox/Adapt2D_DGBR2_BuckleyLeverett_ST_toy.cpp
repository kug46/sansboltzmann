// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_BuckleyLeverett_ST_btest
// Testing of the MOESS framework on a space-time Buckley-Leverett problem

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_BuckleyLeverett_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_BuckleyLeverett_ST_Triangle )
{
  typedef QTypePrimitive_Sw QType;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef TraitsModelBuckleyLeverett<QType, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QType, PDEClass> ExactSolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef BCBuckleyLeverett1DVector<PDEClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Grid
  int ii = 20;
  int jj = 10;

#if 0
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);

//  Real r = 1.09576;
  Real r = 1.058456;

  for (int i = 0; i < ii+1; i++)
  {
    xvec[i] = 0.01*(1.0 - pow(r, i))/(1.0 - r);
//    std::cout << i <<"," << xvec[i] << std::endl;
  }
  xvec[ii] = 50.0;

  for (int j = 0; j < jj+1; j++)
    yvec[j] = (Real)j/jj*25.0;

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_Box_Triangle_X1( xvec, yvec, true ) );
#endif
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, jj, 0, 50, 0, 25, true );
//  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_Box_UnionJack_Triangle_X1( ii, jj, 0, 50, 0, 25 ) );

  int iB = XField2D_Box_Triangle_X1::iBottom;
  int iR = XField2D_Box_Triangle_X1::iRight;
  int iT = XField2D_Box_Triangle_X1::iTop;
  int iL = XField2D_Box_Triangle_X1::iLeft;

  int order = 1;

#if 0
    //Temporary: for visualizing smallest left/right eigenvectors
    Field_DG_Cell<PhysD2, TopoD2, Real> Vfld (*pxfld, order, BasisFunctionCategory_Hierarchical);
    Field_DG_Cell<PhysD2, TopoD2, Real> VTfld(*pxfld, order, BasisFunctionCategory_Hierarchical);

    std::fstream fVmin("tmp/Vmin.txt", std::fstream::in); //right-eigenvector
    std::fstream fVTmin("tmp/VTmin.txt", std::fstream::in); //left-eigenvector

    for (int i = 0; i < Vfld.nDOF(); i++)
    {
      fVmin  >> Vfld .DOF(i);
      fVTmin >> VTfld.DOF(i);
    }
    fVmin .close();
    fVTmin.close();

    output_Tecplot( Vfld, "tmp/Vfld.plt" );
    output_Tecplot( VTfld, "tmp/VTfld.plt" );
#endif

  // PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 5.0;
  CapillaryModel cap_model(pc_max);

  NDPDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 1.0;
  const Real SwR = 0.1;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(pde, SwL, SwR, xinit);

  // BCs

  // Create a Solution dictionary
  PyDict SolnDict;
  SolnDict[BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.Name]
           = BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.PiecewiseConst;
  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.a0] = SwL;
  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.a1] = SwR;
  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.x0] = xinit;

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC_Function;
  TimeIC[BCBuckleyLeverett1DParams<BCTypeTimeIC_Function>::params.Function] = SolnDict;

#if 1
  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params.qB] = SwL;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params.qB] = SwR;
#else
  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.A] = 1;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.B] = 0;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.bcdata] = SwL;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.A] = 1;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.B] = 0;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.bcdata] = SwR;
#endif

  PyDict PyBCList;
  PyBCList["TimeIC"] = TimeIC;
  PyBCList["DirichletR"] = BCDirichletR;
  PyBCList["DirichletL"] = BCDirichletL;
  PyBCList["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TimeIC"] = {iB};
  BCBoundaryGroups["DirichletR"] = {iR};
  BCBoundaryGroups["DirichletL"] = {iL};
  BCBoundaryGroups["None"] = {iT};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-11, 1e-11};

  //Output functional
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 50;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);

  //--------ADAPTATION LOOP--------

  int maxIter = 20;
  Real targetCost = 10000;

  std::string filename_base = "tmp/";
  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  const int quadOrder = 2*(order + 2);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrand);

  //Set initial solution
//  pGlobalSol->setSolution(solnExact, cellGroups);
  pGlobalSol->setSolution(0.1);

#if 0
  std::fstream fqfld_in("tmp/qfld_KBKB_sol.txt", std::fstream::in);

  for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
    fqfld_in >> pGlobalSol->primal.qfld.DOF(i);

  fqfld_in.close();
#endif

  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename = filename_base + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

  std::string adjfld_filename = filename_base + "adjfld_a0.plt";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


#if 0
  std::fstream fqfld("tmp/qfld_sol.txt", std::fstream::out);
  fqfld << std::scientific << std::setprecision(15);

  for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
    fqfld << pGlobalSol->primal.qfld.DOF(i) << std::endl;

  fqfld.close();
#endif


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
  }

  fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
