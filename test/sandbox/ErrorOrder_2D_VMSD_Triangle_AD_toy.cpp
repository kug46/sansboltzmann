// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//#define SANS_FULLTEST
//#define BOUNDARYOUTPUT
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "pyrite_fstream.h"
#include "Field/output_Tecplot.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_Solution.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"

#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "Discretization/VMSD/FunctionalCell_VMSD.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "Field/Function/WeightedFunctionIntegral.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "tools/timer.h"

#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//#define VMSD0
// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_CG_Triangle_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_CG_Triangle_AD )
{
//  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  //EXACT SOLN
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_DoubleBL> SolutionExact;

  //ADJOINT TYPEDEFS
  typedef ScalarFunction2D_ForcingFunction<NDPDEClass> WeightFunctional;
  typedef SolnNDConvertSpace<PhysD2, WeightFunctional>       NDWeightFunctional;

#ifndef BOUNDARYOUTPUT
  typedef OutputCell_WeightedSolution<PDEClass, WeightFunctional> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDWeightOutputClass;
  typedef IntegrandCell_VMSD_Output<NDWeightOutputClass,NDPDEClass> OutputIntegrandClass;

#else
  typedef ScalarFunction2D_Quadratic BoundaryWeight;
  typedef OutputAdvectionDiffusion2D_FunctionWeightedResidual<BoundaryWeight> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;
#endif
  //additional output functionals
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> AdjSolutionExact;
  typedef ForcingFunction2D_MMS<PDEClass> ForcingAdjType;
  typedef OutputCell_SolutionErrorSquared<PDEClass, AdjSolutionExact> AdjErrorClass;
  typedef OutputNDConvertSpace<PhysD2, AdjErrorClass> AdjNDErrorClass;
  typedef IntegrandCell_VMSD_Output<AdjNDErrorClass, NDPDEClass> AdjErrorIntegrandClassL2;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_VMSD_Output<NDErrorClass, NDPDEClass> ErrorIntegrandClassL2;

  typedef ParamType_None ParamBuilderType;
  typedef SolutionData_VMSD<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;

#ifdef VMSD0
  typedef AlgebraicEquationSet_VMSD_Linear<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
#else
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

#endif
  typedef PrimalEquationSetClass::BCParams BCParams;

//  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, ErrorIntegrandClassOutput> SolverInterfaceClass;
  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  // PDE
  Real a = 0.6;
  Real b = 0.8;
  Real nu = 1./50.;

  AdvectiveFlux2D_Uniform adv( a, b );
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_None source;

  // Create a solution dictionary
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;
  DoubleBL[SolutionExact::ParamsType::params.offset] = 1;
  DoubleBL[SolutionExact::ParamsType::params.scale] = -1;

  SolutionExact solnExact( DoubleBL );

  NDPDEClass pde( adv, visc, source );


  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] = "Dirichlet";
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-10, 1e-10, 1e-10};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  // integrands
  NDErrorClass fcnErrorL2(solnExact);
  ErrorIntegrandClassL2 errorIntegrand(pde, fcnErrorL2, {0}, true);

#ifndef BOUNDARYOUTPUT
  AdjSolutionExact adjExact(1.,1.,0.,1.);

  AdvectiveFlux2D_Uniform adv_adj( -a, -b );

  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact));
  NDPDEClass pdeadj( adv_adj, visc, source, forcingadjptr );

  NDWeightFunctional volumeFcn( pdeadj );
  NDWeightOutputClass weightOutput( volumeFcn );
  OutputIntegrandClass outputIntegrand(pde, weightOutput, {0}, true );

  Real trueOutput = 0.08033955918095300;

//  AdjSolutionExact adjsolnExact(advx, advy, nu);
  AdjNDErrorClass adjfcnErrorL2(adjExact);
  AdjErrorIntegrandClassL2 adjerrorIntegrandL2(pde, adjfcnErrorL2, {0}, true);
#else

  // Drag output
  PyDict boundaryWeight;
  boundaryWeight[BoundaryWeight::ParamsType::params.a0] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.ax] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.ay] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.axx] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.ayy] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.axy] = 1;

  NDOutputClass outputFcn(boundaryWeight);
  OutputIntegrandClass outputIntegrand( outputFcn, {0,1,2,3} );

//  const Real trueOutput = (450. - 899*exp(30) + 29. *exp(60))/(450.*pow(exp(30)-1,2));
  const Real trueOutput = 0.04040277777771624;
#endif

  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;
#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

//  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 1;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.PreconditionerSide] = SLA::PreconditionerASMParam::params.PreconditionerSide.Right;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
//  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-15;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 4000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 200;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;

  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  string filename_base = "tmp/AD2DSTRUC/VMSD0/";
  boost::filesystem::create_directories(filename_base);

  string filename2 = filename_base + "Errors.txt";
  fstream foutsol;

//  std::vector<Real> targetCosts = {100, 200, 400, 800, 1600, 3200, 6400};

  int ordermin = 1; int ordermax = 3;
  for (int order = ordermin; order <= ordermax; order++)
  {

    // loop over grid resolution: 2^power
    int powermin = 2; int powermax = 7;
    for (int power = powermin; power <= powermax; power++)
    {

      foutsol.open( filename2, fstream::app );

      int ii = pow( 2, power );
      int jj = ii;

      // grid:

//      XField2D_Box_Triangle_X1 xfld( ii, jj );
//      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );
//      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField_PX<PhysD2, TopoD2>(world, gridfile) );
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_Box_Triangle_Lagrange_X1( world, ii, jj ));


      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

#ifdef VMSD0
      DiscretizationVMSD stab(VMSD0, true, order);
#else
      DiscretizationVMSD stab(VMSDp, true, order);
#endif
      std::shared_ptr<SolutionClass> pGlobalSol;

#ifdef VMSD0
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, stab, order, order, order+1, order+1,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Legendre,
                                                   BasisFunctionCategory_Lagrange,
                                                   active_boundaries);
#else
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, stab, order, order, order+1, order+1,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   active_boundaries);

#endif

      const int quadOrder = 2*(order + 1);
      QuadratureOrder quadratureOrder( *pxfld, 2*(order+1) );

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrand);

      pGlobalSol->setSolution(0.0);

      timer clock;
      pInterface->solveGlobalPrimalProblem();
      Real elapsed = clock.elapsed();

      pInterface->solveGlobalAdjointProblem();

      Real output = pInterface->getOutput();

      pInterface->computeErrorEstimates();
      Real outputErrEstimate = pInterface->getGlobalErrorEstimate();

      Real outputErr = (output - trueOutput);

      Real errEstErr = outputErr - outputErrEstimate;

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_VMSD( errorIntegrand, SquareError ),
          *pxfld, (pGlobalSol->primal.qfld, pGlobalSol->primal.qpfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real L2err = sqrt(SquareError);

      Real AdjSquareError = 0;

#ifndef BOUNDARYOUTPUT
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_VMSD( adjerrorIntegrandL2, AdjSquareError ),
          *pxfld, (pInterface->getAdjField(), pInterface->getAdjPField()), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
#endif
      Real adjError = sqrt(AdjSquareError);

      // Tecplot dump
      std::string filename_base2 = filename_base + "P";
      filename_base2 += to_string(order);
      filename_base2 += "_";
      filename_base2 += to_string(ii);

      std::string filenameq = filename_base2 + "_qfld.plt";
      std::string filenameqp = filename_base2 + "_qpfld.plt";
      std::string filenameadj  = filename_base2 + "_adjfld.plt";
      std::string filenameadjp  = filename_base2 + "_adjpfld.plt";
      std::string filenameE  = filename_base2 + "_efld.plt";

      output_Tecplot( pGlobalSol->primal.qfld, filenameq );
      output_Tecplot( pGlobalSol->primal.qpfld, filenameqp );
      output_Tecplot( pInterface->getAdjField(), filenameadj );
      output_Tecplot( pInterface->getAdjPField(), filenameadjp );
      pInterface->output_EField(filenameE);

      int nDOF = pGlobalSol->primal.qfld.nDOF();

      foutsol << order << " " << to_string(ii) << " " << nDOF << std::setprecision(15);
      foutsol << " " << L2err;
      foutsol << " " << adjError;
      foutsol << " " << fabs(outputErr);
      foutsol << " " << fabs(errEstErr);
      foutsol << " " << elapsed << "\n";

      foutsol.close();

    }



  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
