// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve3D_CG_NACA_LIP_btest
// testing of 3-D CG Linarized Incompressible Potential on a NACA airfoil

#undef SANS_FULLTEST
#undef TIMING

//#define LG_WAKE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "Surreal/SurrealS.h"
#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"
#include "pde/FullPotential/BCLinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/LinearizedIncompressiblePotential3D_Trefftz.h"
#include "pde/FullPotential/IntegrandBoundary3D_LIP_Force.h"
#include "pde/FullPotential/IntegrandFunctorBoundary3D_LIP_Vn2.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"

#include "Discretization/Galerkin/FunctionalBoundaryTrace_Galerkin.h"

#include "Discretization/Potential_Drela/ResidualCell_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/ResidualBoundaryTrace_CG_Potential_Drela.h"
#include "Discretization/LIP_HACK/ResidualBoundaryFrame_Galerkin_Darmofal.h"

#include "Discretization/Potential_Drela/JacobianCell_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianBoundaryTrace_CG_Potential_Drela.h"
#include "Discretization/LIP_HACK/JacobianBoundaryFrame_Galerkin_Darmofal.h"

#include "Discretization/LIP_HACK/IntegrandCell_Galerkin_Darmofal.h"

#include "Discretization/Potential_Drela/ResidualBoundaryTrace_WakeCut_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianBoundaryTrace_WakeCut_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/IntegrandBoundaryTrace_WakeCut_CG_LIP_Drela.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldVolume.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryFrame.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/Direct/MKL_PARDISO/MKL_PARDISOSolver.h"

#include "Meshing/EGTess/makeWakedAirfoil.h"
#include "Meshing/EGADS/Airfoils/NACA4.h"
#include "Meshing/TetGen/EGTetGen.h"

#include "Meshing/AFLR/AFLR3.h"

#include "unit/UnitGrids/XField3D_Box_Hex_X1_WakeCut.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1_WakeCut.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;
using namespace EGADS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_CG_NACA_LIP_Darmofal_test_suite )

#if 1
//----------------------------------------------------------------------------//
// solution: Hierarchical P1
// wake included with other BC regions
BOOST_AUTO_TEST_CASE( Solve3D_NACA_Test_Working )
{
  return; //Don't do any testing because memcheck takes too long

  typedef SurrealS<1> SurrealClass;

  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D<Real>> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  typedef BCNDConvertSpace<PhysD3, BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Real> > BCDirichlet;
  typedef BCNDConvertSpace<PhysD3, BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Real> > BCNeumann;
  typedef BCNDConvertSpace<PhysD3, BCLinearizedIncompressiblePotential3D<BCTypeWall, Real> > BCWall;

  typedef NDVectorCategory<boost::mpl::vector1<BCDirichlet>, BCDirichlet::Category> NDBCDirichletVecCat;
  typedef NDVectorCategory<boost::mpl::vector1<BCNeumann>, BCNeumann::Category> NDBCNeumannVecCat;
  typedef NDVectorCategory<boost::mpl::vector1<BCWall>, BCWall::Category> NDBCWallVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCDirichletVecCat, Galerkin> IntegrandBCDirichlet;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCNeumannVecCat, Galerkin> IntegrandBCNeumann;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCWallVecCat, Galerkin> IntegrandBCWall;

  typedef NDVectorCategory<boost::mpl::vector1<BC_WakeCut_Potential_Drela>, BC_WakeCut_Potential_Drela::Category> NDBCWakeCutVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCWakeCutVecCat, Galerkin > IntegrandWake;
  typedef IntegrandCell_Galerkin_Darmofal<PDEClass> IntegrandKutta;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandCellClass;

  timer totaltime;

  timer meshtime;

  Real span = 4;

  //int factor = 1;

  // Mark the wake or not as a wake
  bool withWake = true;
  //bool withWake = false;
  const int nChord = 11;
  const int nSpan  = 11; //span*nChord+1;
  const int nWake  = 11; //nChord;

#if 1
  EGContext context((CreateContext()));

  //EGApproximate<3> NACA_spline = NACA4<3>(context, 0.001, 0.3, 0);
  EGApproximate<3> NACA_spline = NACA4<3>(context, 0.12, 0.3, 0);

  // EGADS mesh parameters
  std::vector<double> EGADSParams = {50., 0.1, 30.0};
  std::vector<double> outflowParams = {2.5, 0.001, 15.0};

  std::vector<int> TrefftzFrames;

  //{32.,6.,320.} {1.5,0.75,2.}
  EGModel<3> model = makeWakedAirfoil(NACA_spline, TrefftzFrames, span, {32.,6.,320.}, nChord, nSpan, nWake, outflowParams, withWake);
  //model.save( "tmp/NACAWake3D.egads");

  //std::vector<int> TrefftzFrames = {2,3};

  //std::vector<int> TrefftzFrames = {4,5,6,7};


#if 0
  EGModel<3> model(context);
  model.load("tmp/glider_new.egads");

  Real size = model.getSize();
  std::cout << "Model size: " << size << std::endl;

  EGADSParams[0] = 0.25*size;
  EGADSParams[1] = 0.001*size;


#if 1 //glider
/*
  std::vector<EGEdge<3>> wake1_edges = model.getBodies()[0].getEdges();
  std::vector<EGEdge<3>> wake2_edges = model.getBodies()[1].getEdges();

  const int nwk = 801;
  std::vector<double> rwk(nwk-2);
  for ( int i = 1; i < nwk-1; i++ )
    rwk[i-1] = i/Real(nwk-1);

  wake1_edges[11-1].addAttribute(".rPos", rwk);
  wake1_edges[8-1].addAttribute(".rPos", rwk);

  wake2_edges[6-1].addAttribute(".rPos", rwk);
  wake2_edges[12-1].addAttribute(".rPos", rwk);
*/
  std::vector<double> tParam{1.322882/4., 0.05, 10.};

  std::vector<EGBody<3>> bodies = model.getBodies();
  for (auto body = bodies.begin(); body != bodies.end(); body++)
  {
    std::vector<EGEdge<3>> edges = body->getEdges();
    for (auto edge = edges.begin(); edge != edges.end(); edge++)
      if ( edge->hasAttribute(".rPos") )
        edge->delAttribute(".rPos");
  }

  //bodies.pop_back(); // Remove the solid body

  for (auto body = bodies.begin(); body != bodies.end(); body++)
  {
    std::vector<EGFace<3>> faces = body->getFaces();
    for (auto face = faces.begin(); face != faces.end(); face++)
    {
      if ( face->hasAttribute(".tParams") )
      {
        face->delAttribute(".tParams");
        face->addAttribute(".tParam", tParam);
      }
    }
  }
#endif

#if 0 //swept wing
  std::vector<EGEdge<3>> solid_edges = model.getBodies()[1].getEdges();

  solid_edges[25-1].delAttribute(".rPos");
  solid_edges[26-1].delAttribute(".rPos");
  solid_edges[23-1].delAttribute(".rPos");
  solid_edges[22-1].delAttribute(".rPos");

  std::vector<EGBody<3>> bodies = model.getBodies();

  for (auto body = bodies.begin(); body != bodies.end(); body++)
  {
    std::vector<EGFace<3>> faces = body->getFaces();
    for (auto face = faces.begin(); face != faces.end(); face++)
      if ( face->hasAttribute(".tParams") )
        face->delAttribute(".tParams");
  }

#endif

#if 0 //RAE
  std::vector<EGEdge<3>> wake1_edges = model.getBodies()[0].getEdges();
  std::vector<EGEdge<3>> wake2_edges = model.getBodies()[1].getEdges();
  std::vector<EGEdge<3>> solid_edges = model.getBodies()[2].getEdges();

  std::vector<EGFace<3>> wake1_faces = model.getBodies()[0].getFaces();
  std::vector<EGFace<3>> wake2_faces = model.getBodies()[1].getFaces();

  wake1_faces[1-1].addAttribute(".tParams", {634.278358/2., 12.685567, 15.000000});
  wake2_faces[1-1].addAttribute(".tParams", {634.278358/2., 12.685567, 15.000000});

  std::vector<EGFace<3>> solid_faces = model.getBodies()[2].getFaces();

  solid_faces[8-1].delAttribute(".tParams");
  solid_faces[9-1].delAttribute(".tParams");
  solid_faces[10-1].delAttribute(".tParams");
  solid_faces[11-1].delAttribute(".tParams");

  std::vector<double> rSpan(nSpan-2);
  for ( int i = 1; i < nSpan-1; i++ )
    //rSpan[i-1] = i/Real(nSpan-1);
    rSpan[i-1] = 1-0.5*(1+cos(PI*i/Real(nSpan-1)));


  std::vector<double> rChord(nChord-2);
  for ( int i = 1; i < nChord-1; i++ )
    //rCos[i-1] = i/Real(nCos-1);
    rChord[i-1] = 1-0.5*(1+cos(PI*i/Real(nChord-1)));

  // LE
  solid_edges[66-1].addAttribute(".rPos", rSpan);
  solid_edges[60-1].addAttribute(".rPos", rSpan);

  // TE
  solid_edges[68-1].addAttribute(".rPos", rSpan);
  solid_edges[63-1].addAttribute(".rPos", rSpan);

  // Airfoils
  solid_edges[55-1].addAttribute(".rPos", rChord);
  solid_edges[53-1].addAttribute(".rPos", rChord);
  solid_edges[56-1].addAttribute(".rPos", rChord);
  solid_edges[52-1].addAttribute(".rPos", rChord);
  solid_edges[67-1].addAttribute(".rPos", rChord);
  solid_edges[64-1].addAttribute(".rPos", rChord);
  solid_edges[69-1].addAttribute(".rPos", rChord);
  solid_edges[62-1].addAttribute(".rPos", rChord);

  const int iiwake = MAX(nWake, 3);
#if 0
  // stagnation line: quadratic increments w/ dx1 and dx2 matching airfoil
  double x1 = rChord[rChord.size() - 1];
  double x2 = rChord[rChord.size() - 2];

  double xle = 0;
  double xte = 1;

  double dx1 = xte - x1;
  double dx2 = x1 - x2;

  double rffd = 2 - xte/2.;

  double a = (2*((rffd + (xle+xte)/2) - xte) + iiwake*((iiwake - 3)*dx1 - (iiwake - 1)*dx2)) / ((double) (2*iiwake*(2 - iiwake*(3 - iiwake))));
  double b = 0.5*(dx2 - dx1) - 3*a;
  double c = 0.5*(3*dx1 - dx2) + 2*a;

  int iim = iiwake-1;
  std::vector<double> rwk(iiwake-2);
  for (int i = 1; i < iiwake-1; i++)
    rwk[i-1] = i*(c + i*(b + i*a))/(iim*(c + iim*(b + iim*a)));
#endif

#if 1 //Wakes
  std::vector<double> rwk(iiwake-2);
  for (int i = 1; i < iiwake-1; i++)
    rwk[i-1] = (1-0.5*(1+cos(0.5*PI*i/Real(iiwake-1))))/0.5;


  std::vector<double> rwkinv(iiwake-2);
  for (int i = 0; i < iiwake-2; i++)
    rwkinv[i] = 1-rwk[iiwake-3-i];


  wake1_edges[1-1].addAttribute(".rPos", rSpan);
  //wake1_edges[5-1].addAttribute(".rPos", rSpan);
  wake1_edges[8-1].addAttribute(".rPos", rwkinv);
  wake1_edges[9-1].addAttribute(".rPos", rwk);
  wake2_edges[1-1].addAttribute(".rPos", rSpan);
  //wake2_edges[3-1].addAttribute(".rPos", rSpan);
  wake2_edges[8-1].addAttribute(".rPos", rwk);
  wake2_edges[9-1].addAttribute(".rPos", rwkinv);
#endif
#endif
#endif

  EGTessModel tessModel( model, EGADSParams);

  // grid:
#if SANS_AFLR
  AFLR3 xfld(tessModel);
#else
  // TetGen mesh parameters
  Real maxRadiusEdgeRatio = 1.1;//*2/1.1;
  Real minDihedralAngle = 25;//*0;

  EGTetGen xfld(tessModel, maxRadiusEdgeRatio, minDihedralAngle);
#endif

  std::cout << "Meshing Time: " << meshtime.elapsed() << std::endl;

  output_Tecplot( xfld, "tmp/RAE.dat" );
  //output_Tecplot( xfld, "tmp/NACA3D.plt" );

  //return;

  // Find the BC faces
  //std::vector<int> WallFaces, InflowFaces, OutflowFaces, WakeFaces, LGFaces;
  std::map<std::string, std::vector<int> > BCFaces;
  std::vector<int> LGFaces;

  {
    int BCFace = 0;
    std::vector< EGBody<3> > bodies = model.getBodies();
    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ ) //TODO: This should first go over solid bodies then sheet bodies...
    {
      if (not bodies[ibody].isSolid()) continue;
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        if ( faces[i].hasAttribute("BCName") )
        {
          std::string BCName;
          faces[i].getAttribute("BCName", BCName);
          if ( BCName == "Lateral") BCName = "Outflow";
          //if ( BCName == "Lateral") BCName = "Inflow";
          BCFaces[BCName].push_back(BCFace);
          LGFaces.push_back(BCFace);
          BCFace++;
        }
      }
    }

    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ ) //TODO: This should first go over solid bodies then sheet bodies...
    {
      if (bodies[ibody].isSolid()) continue;
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        SANS_ASSERT ( faces[i].hasAttribute("BCName") );

        std::string BCName;
        faces[i].getAttribute("BCName", BCName);
        BCFaces[BCName].push_back(BCFace);
        BCFace++;

      }
    }
  }

  // quadrature rule
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 2);    // max
  std::vector<int> quadratureOrderMin(xfld.nBoundaryTraceGroups(), 0);     // min

#else

  XField3D_Box_Tet_X1_WakeCut xfld( nChord, nSpan, span, nWake );

  std::map<std::string, std::vector<int> > BCFaces;

  BCFaces["Wall"] = {6,7};
  BCFaces["Inflow"] = {0};
  BCFaces["Outflow"] = {1,2,3,4,5};
  BCFaces["Wing_Wake"] = {8};
  std::vector<int> LGFaces = {0,1,2,3,4,5,6,7};

  std::vector<int> TrefftzFrames = {1};

  // quadrature rule
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 2);    // max
  std::vector<int> quadratureOrderMin(xfld.nBoundaryTraceGroups(), 0);     // min

#endif

  // PDE
  Real Vinf = 1;

  Real alphamin = 5; //0.01*180/PI;
  int nalpha = 1;
  Real alphamax = 5; //0.01*180/PI;

  Real outdata = 0;
  Real bcdata = 0;

  PDEClass pde( 1, 0, 0 );

  // BC

  BCDirichlet bcInflow( bcdata );
  BCNeumann bcOutflow( outdata );
  BCWall bcwall( pde );

  // integrands
  int order = 1;
  StabilizationNitsche stab(order);
  IntegrandCellClass fcnCell( pde, {0} );
  IntegrandBCDirichlet fcnBCInflow( pde, bcInflow, BCFaces.at("Inflow"), stab );
  IntegrandBCNeumann fcnBCOutflow( pde, bcOutflow, BCFaces.at("Outflow"), stab );
  IntegrandBCWall fcnBCwall( pde, bcwall, BCFaces.at("Wall"), stab );
  IntegrandWake fcnWake(pde, BCFaces.at("Wing_Wake"), xfld.dupPointOffset_, xfld.KuttaPoints_);

  IntegrandKutta fcnKutta(pde, xfld.dupPointOffset_, xfld.KuttaPoints_);

  std::cout << "Kutta point size: " << xfld.KuttaPoints_.size() << std::endl;


  // solution: Hierarchical, C0
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical),
                                        qfldnew(xfld, order, BasisFunctionCategory_Hierarchical);

//  Field_CG_Cell<PhysD3, TopoD3, DLA::VectorS<1,Real> > rsdfld(xfld, order, BasisFunctionCategory_Hierarchical);

  qfld = 0;

  std::vector<int> sortedKuttaPoints(xfld.KuttaPoints_);

  //Sort all the kutta points
  //Use a lambda function for the sorting
  std::sort(sortedKuttaPoints.begin(), sortedKuttaPoints.end(),
      [&](const int& a, const int& b) -> bool { return xfld.DOF(a)[1] < xfld.DOF(b)[1]; });


  //output_Tecplot_LIP( pde, qfld, "tmp/NACA_mesh.dat" );

  const int nDOFPDE = qfld.nDOF();

  std::cout << " nDOFPDE = " << nDOFPDE << std::endl;
  std::cout << " Duplicates after " << xfld.dupPointOffset_ << std::endl;

 //Dummy Lagrange multipliers for now...
#ifdef LG_WAKE
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#else
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical, LGFaces );
#endif

  lgfld = 0;

#if 0
  cout << "nDOFPDE = " << nDOFPDE << endl;
  cout << "  InflowFaces = " << InflowFaces.size()
       << "  OutflowFaces = " << OutflowFaces.size()
       << "  WallFaces = " << WallFaces.size()
       << "  WakeFaces = " << WakeFaces.size() << endl;
  cout << "  lgfld.nBoundaryTraceGroups() = " << lgfld.nBoundaryTraceGroups() << endl;
#endif


  // linear system setup

  typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
  typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
  typedef SparseMatrixClass               SystemMatrixClass;
  typedef SparseVectorClass               SystemVectorClass;

  typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

  NonZeroPatternClass nzPDE_q(nDOFPDE, nDOFPDE);

  IntegrateCellGroups<TopoD3>::integrate( JacobianCell_CG_Potential_Drela<SurrealClass>(fcnCell, nzPDE_q, xfld.dupPointOffset_, xfld.invPointMap_),
                                          xfld, qfld, &quadratureOrderMin[0], xfld.nCellGroups() );
#if 1
  IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      JacobianBoundaryTrace_CG_Potential_Drela<SurrealClass>(fcnBCInflow, nzPDE_q, xfld.dupPointOffset_, xfld.invPointMap_),
                                                        xfld, qfld, &quadratureOrderMin[0], quadratureOrderMin.size() );
  IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      JacobianBoundaryTrace_CG_Potential_Drela<SurrealClass>(fcnBCOutflow, nzPDE_q, xfld.dupPointOffset_, xfld.invPointMap_),
                                                        xfld, qfld, &quadratureOrderMin[0], quadratureOrderMin.size() );
  IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      JacobianBoundaryTrace_CG_Potential_Drela<SurrealClass>(fcnBCwall, nzPDE_q, xfld.dupPointOffset_, xfld.invPointMap_),
                                                        xfld, qfld, &quadratureOrderMin[0], quadratureOrderMin.size() );
#endif


  if ( withWake )
  {

    JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate( fcnWake, qfld,
                                                                                      &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                                      nzPDE_q );

    JacobianBoundaryFrame_Galerkin_Darmofal<SurrealClass,TopoD3>::integrate(fcnKutta, xfld, qfld, &quadratureOrderMin[0], xfld.nBoundaryFrameGroups(),
                                                                   nzPDE_q );
  }

  unsigned int maxRow = 0, maxRowSize = 0;
  for ( int i = 0; i < nzPDE_q.m(); i++ )
    if ( nzPDE_q.rowSize(i) > maxRowSize )
    {
      maxRow=i;
      maxRowSize = nzPDE_q.rowSize(i);
    }

  std::cout << "maxRow = " << maxRow << " maxRowSize = " << maxRowSize << std::endl;

  // Solution and residual vector and jacobian matrix
  SystemVectorClass sln(nDOFPDE);
  SystemVectorClass rsd(nDOFPDE), rsdnew(nDOFPDE);
  SystemMatrixClass jacPDE_q(nzPDE_q);


//#ifdef INTEL_MKL
//    SLA::MKL_PARDISO<SystemMatrixClass> solver;
//#else
   //SLA::UMFPACK<SystemMatrixClass> solver;
//#endif

  for ( int na = 0; na < nalpha; na++)
  {
    Real alpha = alphamin + na*(alphamax-alphamin)/std::max(1,nalpha-1);

    std::cout << "-----------------" << std::endl;
    std::cout << "alpha          = " << alpha << std::endl;
#if 0
    Real U = Vinf*cos(alpha*PI/180), V = Vinf*sin(alpha*PI/180), W = Vinf*0;
    DLA::VectorS<3, Real> Ddir = { U, V, W};
    DLA::VectorS<3, Real> Ldir = {-V, U, W};
#else
    Real U = Vinf*cos(alpha*PI/180), V = Vinf*0, W = Vinf*sin(alpha*PI/180);
    DLA::VectorS<3, Real> Ddir = { U, V, W};
    DLA::VectorS<3, Real> Ldir = {-W, V, U};
#endif
    //Update the angle of attack
    pde.setFreestream(Ddir);

    Ddir /= Vinf;
    Ldir /= Vinf;

    qfld = 0;
#if 0
    for (std::size_t i = 0; i < sortedKuttaPoints.size(); i++)
    {
      int n = xfld.invPointMap_[ sortedKuttaPoints[i] - xfld.dupPointOffset_ ];
      qfld.DOF(n) = (i % 2 == 0 ? 1 : -1);
      std::cout << "qfld.DOF(" << n << ") = " << qfld.DOF(n) << std::endl;
    }

    for (std::size_t i = 0; i < sortedKuttaPoints.size(); i++)
    {
      int n = sortedKuttaPoints[i];
      qfld.DOF(n) = (i % 2 == 0 ? 1 : -1);
      std::cout << "qfld.DOF(" << n << ") = " << qfld.DOF(n) << std::endl;
    }
#endif
    for (int newton = 0; newton < 100; newton++)
    {
      rsd = 0;
      jacPDE_q = 0;

      timer rsdtime;

      // residual

      IntegrateCellGroups<TopoD3>::integrate( ResidualCell_CG_Potential_Drela(fcnCell, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                              xfld, qfld, &quadratureOrder[0], xfld.nCellGroups() );

      IntegrateBoundaryTraceGroups<TopoD3>::integrate(
          ResidualBoundaryTrace_CG_Potential_Drela(fcnBCInflow, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
      IntegrateBoundaryTraceGroups<TopoD3>::integrate(
          ResidualBoundaryTrace_CG_Potential_Drela(fcnBCOutflow, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
      IntegrateBoundaryTraceGroups<TopoD3>::integrate(
          ResidualBoundaryTrace_CG_Potential_Drela(fcnBCwall, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

      std::cout << "Residual Time: " << rsdtime.elapsed() << std::endl;

      timer jactime;

      IntegrateCellGroups<TopoD3>::integrate(
          JacobianCell_CG_Potential_Drela<SurrealClass>(fcnCell, jacPDE_q, xfld.dupPointOffset_, xfld.invPointMap_),
                                                  xfld, qfld, &quadratureOrder[0], xfld.nCellGroups() );
    #if 1
      IntegrateBoundaryTraceGroups<TopoD3>::integrate(
          JacobianBoundaryTrace_CG_Potential_Drela<SurrealClass>(fcnBCInflow, jacPDE_q, xfld.dupPointOffset_, xfld.invPointMap_),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
      IntegrateBoundaryTraceGroups<TopoD3>::integrate(
          JacobianBoundaryTrace_CG_Potential_Drela<SurrealClass>(fcnBCOutflow, jacPDE_q, xfld.dupPointOffset_, xfld.invPointMap_),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
      IntegrateBoundaryTraceGroups<TopoD3>::integrate(
          JacobianBoundaryTrace_CG_Potential_Drela<SurrealClass>(fcnBCwall, jacPDE_q, xfld.dupPointOffset_, xfld.invPointMap_),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
    #endif
      std::cout << "Jacobian Time: " << jactime.elapsed() << std::endl;

      if ( withWake )
      {
        JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate( fcnWake, qfld, &quadratureOrder[0], quadratureOrder.size(),
                                                                                       jacPDE_q );

        JacobianBoundaryFrame_Galerkin_Darmofal<SurrealClass,TopoD3>::integrate(fcnKutta, xfld, qfld,
                                                                       &quadratureOrder[0], xfld.nBoundaryFrameGroups(), jacPDE_q );

        ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfld,
                                                                          &quadratureOrder[0], quadratureOrder.size(), rsd );

        ResidualBoundaryFrame_Galerkin_Darmofal<TopoD3>::integrate(fcnKutta, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), rsd );
      }

#if 0
      std::cout << std::setprecision(16) << "rsd = {";
      for (int n = 0; n < nDOFPDE; n++)
      {
        Real z = rsd[n];
        int exponent = z == 0 ? 0 : floor( log10( std::abs(z) ) );
        Real base     = z / pow(10., exponent);
        std::cout << base << " 10^" << exponent;
        if (n < nDOFPDE-1)
          std::cout << ", ";
      }
      std::cout << "};" << endl;
#endif

#if 0
      std::cout << std::scientific << std::setprecision(8);
      for (std::size_t i = 0; i < sortedKuttaPoints.size(); i++)
      {
        int n = xfld.invPointMap_[ sortedKuttaPoints[i] - xfld.dupPointOffset_ ];
        std::cout << "rsd[" << n << "] = " << rsd[n] << std::endl;
      }

      for (std::size_t i = 0; i < sortedKuttaPoints.size(); i++)
      {
        int n = sortedKuttaPoints[i];
        std::cout << "rsd[" << n << "] = " << rsd[n] << std::endl;
      }
#endif

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[n],2);

      std::cout << "Newton " << newton << " rsd = " << sqrt(rsdPDEnrm) << std::endl;

#if 0
      WriteMatrixMarketFile( jacPDE_q, "tmp/NACA0.mtx" );

      for ( int i = 0; i < jacPDE_q.m(); i++ )
      {
        Real diag = jacPDE_q.diag(i);
        bool smalldiag = false;
        Real rowSum = 0;
        for ( int k = 0; k < jacPDE_q.rowNonZero(i); k++)
        {
          if ( diag < jacPDE_q.sparseRow(i,k) ) smalldiag = true;
          rowSum += jacPDE_q.sparseRow(i,k);
        }

        if ( true ) //smalldiag || i >= xfld.dupPointOffset_  ) //rowSum > 1e-10 ||
        {
          std::vector<Real> AJac;
          std::vector<int> iAJac;
          std::cout << "-----------------" << std::endl;
          std::cout << "row = " << i << " sum = " << rowSum << " smalldiag = " << smalldiag << std::endl;
          std::cout << "diag = " << jacPDE_q.diag(i) << std::endl;
          for ( int k = 0; k < jacPDE_q.rowNonZero(i); k++)
            if ( abs(jacPDE_q.sparseRow(i,k)) > 1e-12)
            {
              AJac.push_back(jacPDE_q.sparseRow(i,k));
              std::cout << jacPDE_q.sparseRow(i,k) << ", ";
            }
          std::cout << std::endl;
          for ( int k = 0; k < jacPDE_q.rowNonZero(i); k++)
            if ( abs(jacPDE_q.sparseRow(i,k)) > 1e-12)
            {
              iAJac.push_back(jacPDE_q.get_col_ind()[jacPDE_q.get_row_ptr()[i] + k]);
              std::cout << jacPDE_q.get_col_ind()[jacPDE_q.get_row_ptr()[i] + k] << ", ";
            }
          std::cout << std::endl;

          for ( int k = 0; k < qfld.nDOF(); k++)
            qfldnew.DOF(k) = qfld.DOF(k);


          std::vector<Real> diffJac;
          std::vector<int> idiffJac;
          for ( int k = 0; k < qfld.nDOF(); k++)
          {

            rsdnew = 0;
            qfldnew.DOF(k) += 1;

            IntegrateCellGroups<TopoD3>::integrate( ResidualCell_CG_Potential_Drela(fcnCell, rsdnew, xfld.dupPointOffset_, xfld.invPointMap_),
                                                    xfld, qfldnew, &quadratureOrder[0], xfld.nCellGroups() );

            IntegrateBoundaryTraceGroups<TopoD3>::integrate(
                ResidualBoundaryTrace_CG_Potential_Drela(fcnBCInflow, rsdnew, xfld.dupPointOffset_, xfld.invPointMap_),
                                                                  xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );
            IntegrateBoundaryTraceGroups<TopoD3>::integrate(
                ResidualBoundaryTrace_CG_Potential_Drela(fcnBCOutflow, rsdnew, xfld.dupPointOffset_, xfld.invPointMap_),
                                                                  xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );
            IntegrateBoundaryTraceGroups<TopoD3>::integrate(
                ResidualBoundaryTrace_CG_Potential_Drela(fcnBCwall, rsdnew, xfld.dupPointOffset_, xfld.invPointMap_),
                                                                  xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );

            ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfldnew,
                                                                            &quadratureOrder[0], quadratureOrder.size(), rsdnew );

            ResidualBoundaryFrame_Galerkin_Darmofal<TopoD3>::integrate(fcnKutta, xfld, qfldnew, &quadratureOrder[0],
                                                              xfld.nBoundaryFrameGroups(), rsdnew );

            qfldnew.DOF(k) -= 1;

            Real diff = rsdnew[i] - rsd[i];
            if ( abs(diff) > 1e-12 )
            {
              diffJac.push_back(diff);
              idiffJac.push_back(k);
            }
          }
          std::cout << "FD" << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << diffJac[k] << ", ";
          std::cout << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << idiffJac[k] << ", ";
          std::cout << std::endl;

          bool error = false;
          std::cout << "A-FD" << std::endl;
          BOOST_REQUIRE_EQUAL(AJac.size(), diffJac.size());
          for ( std::size_t k = 0; k < diffJac.size(); k++)
          {
            if ( abs(AJac[k]-diffJac[k]) > 1e-12)
            {
              error = true;
              std::cout << "AJac[" << k << "]=" << AJac[k]
                        << " diffJac[" << k << "]=" << diffJac[k] << std::endl;
            }
          }
          BOOST_REQUIRE(!error);
        }
        //BOOST_CHECK_LT( rowSum, 1e-10 );
      }
#endif


#if 0
      std::cout << "Writing tmp/FieldKutta.mtx" << std::endl;
      std::fstream file("tmp/FieldKutta.mtx", std::ios::out);

      int nKuttNNZ = 0;
      for (std::size_t i = 0; i < xfld.KuttaPoints_.size(); i++)
      {
        int row = xfld.invPointMap_[ xfld.KuttaPoints_[i]-xfld.dupPointOffset_ ];
        nKuttNNZ += jacPDE_q.rowNonZero(row);
        row = xfld.KuttaPoints_[i];
        nKuttNNZ += jacPDE_q.rowNonZero(row);
      }

      //Write the banner
      file << "%%MatrixMarket matrix coordinate real general" << std::endl;
      file << jacPDE_q.m() << " " << jacPDE_q.n() << " " << nKuttNNZ << std::endl;

      int* row_ptr = jacPDE_q.get_row_ptr();
      int* col_ind = jacPDE_q.get_col_ind();

      for (std::size_t i = 0; i < xfld.KuttaPoints_.size(); i++)
      {
        Real rowSum = 0;

        //Write out the matrix data
        //Add one to the row and column index as the file format is 1-based
        int row = xfld.invPointMap_[ xfld.KuttaPoints_[i]-xfld.dupPointOffset_ ];
        for ( int col = 0; col < jacPDE_q.rowNonZero(row); col++ )
        {
          file << std::setprecision( 16 ) << row+1 << " " << col_ind[row_ptr[row] + col]+1 << " " << jacPDE_q.sparseRow(row,col) << std::endl;
          rowSum += jacPDE_q.sparseRow(row,col);
        }
        std::cout << "row = " << row << " sum = " << rowSum << std::endl;
      }

      for (std::size_t i = 0; i < xfld.KuttaPoints_.size(); i++)
      {
        Real rowSum = 0;

        //Write out the matrix data
        //Add one to the row and column index as the file format is 1-based
        int row = xfld.KuttaPoints_[i];
        for ( int col = 0; col < jacPDE_q.rowNonZero(row); col++ )
        {
          file << std::setprecision( 16 ) << row+1 << " " << col_ind[row_ptr[row] + col]+1 << " " << jacPDE_q.sparseRow(row,col) << std::endl;
          rowSum += jacPDE_q.sparseRow(row,col);

          if ( xfld.DOF( col_ind[row_ptr[row] + col] )[0] > 1 && abs(xfld.DOF( col_ind[row_ptr[row] + col] )[2]) < 0.01 )
          {
            SANS_ASSERT_MSG( jacPDE_q.sparseRow(row,col) == 0, "%d %e", col_ind[row_ptr[row] + col], jacPDE_q.sparseRow(row,col) );
          }

        }
        std::cout << "row = " << row << " sum = " << rowSum << std::endl;
      }
#endif

      if (rsdPDEnrm < 1e-10) break;

      timer solvetime;

      SANS_DEVELOPER_EXCEPTION("Need to update solver!!");
      //sln = solver.inverse(jacPDE_q)*rsd;

#if 0
      std::cout << "sln = {";
      for (int n = 0; n < nDOFPDE; n++)
      {
        Real z = sln[n];
        int exponent = floor( log10( std::abs(z) ) );
        Real base     = z / pow(10, exponent);
        std::cout << base << " 10^" << exponent;
        if (n < nDOFPDE-1)
          std::cout << ", ";
      }
      std::cout << "};" << endl;
#endif

      std::cout << "Solve Time: " << solvetime.elapsed() << std::endl;

      // updated solution
      Real eps = 1;
      Real rsdPDEnrmnew = rsdPDEnrm+1;
      while (sqrt(rsdPDEnrmnew) > sqrt(rsdPDEnrm))
      {
        for (int k = 0; k < nDOFPDE; k++)
          qfldnew.DOF(k) = qfld.DOF(k) - eps*sln[k];

        rsd = 0;

        IntegrateCellGroups<TopoD3>::integrate( ResidualCell_CG_Potential_Drela(fcnCell, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                                xfld, qfldnew, &quadratureOrder[0], xfld.nCellGroups() );

        IntegrateBoundaryTraceGroups<TopoD3>::integrate(
            ResidualBoundaryTrace_CG_Potential_Drela(fcnBCInflow, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                                              xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );
        IntegrateBoundaryTraceGroups<TopoD3>::integrate(
            ResidualBoundaryTrace_CG_Potential_Drela(fcnBCOutflow, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                                              xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );
        IntegrateBoundaryTraceGroups<TopoD3>::integrate(
            ResidualBoundaryTrace_CG_Potential_Drela(fcnBCwall, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                                              xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );

        if ( withWake )
        {
          ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfldnew,
                                                                          &quadratureOrder[0], quadratureOrder.size(), rsd );

          ResidualBoundaryFrame_Galerkin_Darmofal<TopoD3>::integrate(fcnKutta, xfld, qfldnew, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), rsd );
        }

        rsdPDEnrmnew = 0;
        for (int n = 0; n < nDOFPDE; n++)
          rsdPDEnrmnew += pow(rsd[n],2);

        std::cout << "rsdPDEnrmnew = " << sqrt(rsdPDEnrmnew) << " rsdPDEnrm = " << sqrt(rsdPDEnrm) << std::endl;
//        break;

        if (sqrt(rsdPDEnrmnew) > sqrt(rsdPDEnrm)) eps /= 2;
        if (eps < 1e-6)
        {
          std::cout << "Line search failed..." << std::endl;
          break;
        }

      }

      // Update the solution
      for (int k = 0; k < nDOFPDE; k++)
        qfld.DOF(k) = qfldnew.DOF(k);

      if (eps < 1e-6) break;
      //break;
    }

#if 0
    rsd = 0;
    for (std::size_t i = 0; i < sortedKuttaPoints.size(); i++)
    {
      int n = xfld.invPointMap_[ sortedKuttaPoints[i] - xfld.dupPointOffset_ ];
      rsd[n] = (i % 2 == 0 ? 1 : -1);
    }

    for (std::size_t i = 0; i < sortedKuttaPoints.size(); i++)
    {
      int n = sortedKuttaPoints[i];
      rsd[n] = (i % 2 == 0 ? 1 : -1);
    }

    sln = jacPDE_q*rsd;

    for (std::size_t i = 0; i < sortedKuttaPoints.size(); i++)
    {
      int n = xfld.invPointMap_[ sortedKuttaPoints[i] - xfld.dupPointOffset_ ];
      std::cout << "sln[" << n << "] = " << sln[n] << std::endl;
    }

    for (std::size_t i = 0; i < sortedKuttaPoints.size(); i++)
    {
      int n = sortedKuttaPoints[i];
      std::cout << "sln[" << n << "] = " << sln[n] << std::endl;
    }
#endif

    // check that the residual is zero
#if 1
    rsd = 0;

    IntegrateCellGroups<TopoD3>::integrate( ResidualCell_CG_Potential_Drela(fcnCell, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                            xfld, qfld, &quadratureOrder[0], xfld.nCellGroups() );

    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        ResidualBoundaryTrace_CG_Potential_Drela(fcnBCInflow, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                                          xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        ResidualBoundaryTrace_CG_Potential_Drela(fcnBCOutflow, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                                          xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        ResidualBoundaryTrace_CG_Potential_Drela(fcnBCwall, rsd, xfld.dupPointOffset_, xfld.invPointMap_),
                                                          xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

    if ( withWake )
    {
      ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfld,
                                                                      &quadratureOrder[0], quadratureOrder.size(), rsd );

      ResidualBoundaryFrame_Galerkin_Darmofal<TopoD3>::integrate(fcnKutta, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), rsd );
    }

    Real rsdPDEnrm = 0;
    for (int n = 0; n < nDOFPDE; n++)
    {
//      rsdfld.DOF(n) = rsd[n];
      rsdPDEnrm += pow(rsd[n],2);
    }

    BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );
#endif
    Real Drag = 0;
    Real TrefftzLift = 0;
    DLA::VectorS<3,Real> PressureForce = 0;
    Real Vn2 = 0;
    quadratureOrder[0] = 2;
    quadratureOrder[1] = 2;

    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzInducedDrag, Real> fcnTrefftzDrag(pde, TrefftzFrames);
    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Real> fcnTrefftzLift(pde, Ldir, TrefftzFrames);
    IntegrandBoundary3D_LIP_Force<Real> fcnBodyForce(pde, BCFaces.at("Wall"));
    IntegrandBoundary3D_LIP_Vn2<Real> fcnVn(pde, BCFaces.at("Wall"));

    if ( withWake )
    {
      FunctionalTrefftz::integrate(fcnTrefftzDrag, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), Drag);
      FunctionalTrefftz::integrate(fcnTrefftzLift, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), TrefftzLift);
    }
    //FunctionalBoundaryTrace_sansLG<TopoD3>::integrate(fcnBodyForce, qfld, &quadratureOrder[0], quadratureOrder.size(), PressureForce);
    //FunctionalBoundaryTrace_sansLG<TopoD3>::integrate(fcnVn, qfld, &quadratureOrder[0], quadratureOrder.size(), Vn2);
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( fcnBodyForce, PressureForce ), xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( fcnVn, Vn2 ), xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

    Real rho = 1;
    Real Vinf2 = U*U + V*V + W*W;

    // Simple NACA hershey bar
    Real Sref = span*1;

    // The RAE model is scaled by 100
    //Real Lref = (228.6 + 76.2)/2.*100;
    //Real Sref = 457.2*2*Lref*100;

    // Swept wing
    //Real Lref = (7.77964496612548828 - 4);
    //Real Sref = 13.2287569046020508*2*Lref;

    Drag /= (0.5*rho*Vinf2*Sref);
    TrefftzLift /= (0.5*rho*Vinf2*Sref);
    PressureForce /= (0.5*rho*Vinf2*Sref);

    std::cout << std::setprecision(16);
    std::cout << "Trefftz Drag   = " << Drag << std::endl;
    std::cout << "Trefftz Lift   = " << TrefftzLift << std::endl;
    std::cout << "Pressure Force = " << PressureForce << std::endl;
    std::cout << "Pressure Drag  = " << dot(PressureForce,Ddir) << std::endl;
    std::cout << "Pressure Lift  = " << dot(PressureForce,Ldir) << std::endl;
    std::cout << "L2 Vn  = " << sqrt(Vn2) << std::endl;

  }

  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;

#if 1
  // Tecplot dump
  string filename = "tmp/NACA0_LIP.dat";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot_LIP( pde, qfld, filename );
#endif

#if 0
  for (int n = 0; n < nDOFPDE; n++)
    rsdfld.DOF(n) = rsd[n];
  output_Tecplot( rsdfld, "tmp/NACA0_LIPrsd.dat" );
#endif

#if 0
  // Tecplot dump
  string filenameLG = "tmp/NACA0_LG_LIP.dat";
  cout << "calling output_Tecplot: filename = " << filenameLG << endl;
  output_Tecplot_LIP( pde, lgfld, filenameLG );
#endif

  cout << "1st finished:" << endl;
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
