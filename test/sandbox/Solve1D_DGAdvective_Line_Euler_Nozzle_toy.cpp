// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve1D_DGAdvective_Line_Euler_Nozzle_toy


#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/BCEuler1D.h"
#include "pde/NS/SolutionFunction_Euler1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
//#include "pde/AnalyticFunction/SSMEInletConditions.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "tools/output_std_vector.h"

using namespace std;
using namespace SANS;

namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGAdvective_Line_Euler_Nozzle_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGAdvective_Line_Euler_Nozzle )
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // MPI comm world
  ////////////////////////////////////////////////////////////////////////////////////////
  mpi::communicator world;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for Euler1D
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for discretization
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;
  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD1, TopoD1> > PDEPrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD1, TopoD1> > AlgebraicEquationSet_PTCClass;

  typedef PDEPrimalEquationSetClass::BCParams BCParams;
  typedef PDEPrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PDEPrimalEquationSetClass::SystemVector SystemVectorClass;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Solution order
  ////////////////////////////////////////////////////////////////////////////////////////
  int order_pde = 1;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Area function for the nozzle contour
  ////////////////////////////////////////////////////////////////////////////////////////
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  // Gas Model
  Real gamma = 1.4;
  Real R = 1.0;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  NDPDEClass pde(gas, PDEClass::Euler_ResidInterp_Momentum, area);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Define boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  Real rho = 1.0;
  Real t =  1.0;
  Real M = 1.0;
  Real a = sqrt(gamma*R*t);
  Real u = M * a;
  Real p = gas.pressure(rho, t);

  PyDict State;
  State[NSVariableType1DParams::params.StateVector.Variables] = NSVariableType1DParams::params.StateVector.PrimitiveTemperature;
  State[DensityVelocityTemperature1DParams::params.rho] = rho;
  State[DensityVelocityTemperature1DParams::params.u] = u;
  State[DensityVelocityTemperature1DParams::params.t] = t;

  // Create a BC dictionary
  PyDict BCInflowSupersonic;
  typedef BCEuler1D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCParameters< BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> > BCParams;
  BCInflowSupersonic[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCInflowSupersonic[BCClass::ParamsType::params.Characteristic] = true;
  BCInflowSupersonic[BCClass::ParamsType::params.StateVector] = State;

  // Define BC list
  PyDict PyPDEBCList;
  PyPDEBCList["Inflow"] = BCInflowSupersonic;

#define PRESSURE_EXIT 0
#if PRESSURE_EXIT
  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = p * pr;
  std::cout << "Outflow pressure: " << p * pr << " ("  << p << ")" << std::endl;
  PyPDEBCList["Outflow"] = BCOut;
#else
  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.None;
  PyPDEBCList["Outflow"] = BCOut;
#endif
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  int ii = 30;
  XField1D xfld( ii, 0, 1 );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Get boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;
  PDEBCBoundaryGroups["Inflow"] = {0}; // Left
  PDEBCBoundaryGroups["Outflow"] = {1}; // Right
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check BCs
  ////////////////////////////////////////////////////////////////////////////////////////
  BCParams::checkInputs(PyPDEBCList);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate our Euler PDE field
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> pde_lgfld( xfld, order_pde, BasisFunctionCategory_Legendre,
                                                            BCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups) );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure1D<Real>(rho, 2.0*a, p) );
  pde_qfld = q0;
  pde_lgfld = 0;
  ////////////////////////////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////////////////////
  // Output IC gnuplot file
  ////////////////////////////////////////////////////////////////////////////////////////
  string filenameStem_IC = "tmp/SSME_" + Type2String<QType>::str() + "_P" + std::to_string(order_pde) + "_IC." + std::to_string(world.rank());
  output_gnuplot( pde_qfld, filenameStem_IC + ".gplt" );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // AES Settings
  ////////////////////////////////////////////////////////////////////////////////////////
  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {3.e-3, 1e-9};
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our AES to solve our PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  PDEPrimalEquationSetClass PrincipalEqSet(xfld, pde_qfld, pde_lgfld, pde, quadratureOrder, ResidualNorm_L2, tol, {0},
                                           {0}, PyPDEBCList, PDEBCBoundaryGroups);

  AlgebraicEquationSet_PTCClass PrincipalEqSetPTC(xfld, pde_qfld, pde, quadratureOrder, {0}, PrincipalEqSet);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Construct Newton Solver
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);
  NewtonSolver<SystemMatrixClass> Solver( PrincipalEqSet, NewtonSolverDict );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial guess from initial condition
  ////////////////////////////////////////////////////////////////////////////////////////
  SystemVectorClass ini(PrincipalEqSet.vectorStateSize());
  SystemVectorClass sln(PrincipalEqSet.vectorStateSize());

  PrincipalEqSet.fillSystemVector(ini);
  sln = ini;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // SOLVE
  ////////////////////////////////////////////////////////////////////////////////////////
#define USE_NEWTON_SOLVE 1
#if USE_NEWTON_SOLVE
  SolveStatus status = Solver.solve(ini, sln);
  BOOST_CHECK( status.converged );
#else
  bool converged = false;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  PyDict PseudoTimeDict;
  PseudoTimeDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  PseudoTimeDict[PseudoTimeParam::params.invCFL] = 1;
  PseudoTimeDict[PseudoTimeParam::params.invCFL_max] = 100;
  PseudoTimeDict[PseudoTimeParam::params.invCFL_min] = 0.0;

  PseudoTimeDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.5;//0.5;
  PseudoTimeDict[PseudoTimeParam::params.CFLIncreaseFactor] = 10;
  PseudoTimeDict[PseudoTimeParam::params.Verbose] = true;

  PseudoTime<SystemMatrixClass> PTC(PseudoTimeDict, PrincipalEqSetPTC);
  converged = PTC.iterate(100);
  BOOST_CHECK(converged);
#endif


  ////////////////////////////////////////////////////////////////////////////////////////
  // Output Tecplot file
  ////////////////////////////////////////////////////////////////////////////////////////
  string filenameStem = "tmp/EulerNozzle_" + Type2String<QType>::str() + "_P" + std::to_string(order_pde) + "." + std::to_string(world.rank());
  output_Tecplot( pde_qfld, filenameStem + ".plt" );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output gnuplot file
  ////////////////////////////////////////////////////////////////////////////////////////
  output_gnuplot( pde_qfld, filenameStem + ".gplt" );
  output_gnuplot( pde_qfld, filenameStem + ".txt" );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Get residual after 0 iterations
  ////////////////////////////////////////////////////////////////////////////////////////
  SystemVectorClass q(PrincipalEqSet.vectorStateSize());
  PrincipalEqSet.fillSystemVector(q);
  SystemVectorClass rsd(PrincipalEqSet.vectorEqSize());
  rsd = 0.0;
  PrincipalEqSet.residual(q, rsd);
  PrincipalEqSet.setSolutionField(rsd);
  ////////////////////////////////////////////////////////////////////////////////////////
  // Output file
  ////////////////////////////////////////////////////////////////////////////////////////
  filenameStem = "tmp/EulerNozzle_" + Type2String<QType>::str() + "_P" + std::to_string(order_pde) + "." + std::to_string(world.rank()) + "_residual";
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output gnuplot file
  ////////////////////////////////////////////////////////////////////////////////////////
  output_gnuplot( pde_qfld, filenameStem + ".gplt" );
  output_gnuplot( pde_qfld, filenameStem + ".txt" );
  ////////////////////////////////////////////////////////////////////////////////////////
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
