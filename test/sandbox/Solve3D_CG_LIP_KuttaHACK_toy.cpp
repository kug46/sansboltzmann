// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#undef SANS_FULLTEST
#undef TIMING

//#define LG_WAKE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "Surreal/SurrealS.h"
#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"
#include "pde/FullPotential/BCLinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/LinearizedIncompressiblePotential3D_Trefftz.h"
#include "pde/FullPotential/IntegrandBoundary3D_LIP_Force.h"
#include "pde/FullPotential/IntegrandFunctorBoundary3D_LIP_Vn2.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"

#include "Discretization/Galerkin/FunctionalBoundaryTrace_Galerkin.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
//#include "Discretization/Galerkin/ResidualBoundaryTrace_Galerkin_LG.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Galerkin.h"
#include "Discretization/LIP_HACK/ResidualBoundaryFrame_Galerkin.h"
#include "Discretization/LIP_HACK/ResidualBoundaryFrame_Galerkin_Circ.h"
#include "Discretization/LIP_HACK/ResidualCell_Galerkin_Kutta.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
//#include "Discretization/Galerkin/JacobianBoundaryTrace_Galerkin.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Galerkin.h"
#include "Discretization/LIP_HACK/JacobianBoundaryFrame_Galerkin.h"
#include "Discretization/LIP_HACK/JacobianBoundaryFrame_Galerkin_Circ.h"
#include "Discretization/LIP_HACK/JacobianCell_Galerkin_Kutta.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#ifdef LG_WAKE
#include "Discretization/LIP_HACK/ResidualBoundaryTrace_Galerkin_WakeCut_LIP.h"
#include "Discretization/LIP_HACK/JacobianBoundaryTrace_Galerkin_WakeCut_LIP.h"
#else
#include "Discretization/Potential_Drela/ResidualBoundaryTrace_WakeCut_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianBoundaryTrace_WakeCut_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/IntegrandBoundaryTrace_WakeCut_CG_LIP_Drela.h"
#endif

#include "Field/XFieldLine.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryFrame.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/Field_DG_BoundaryFrame.h"

#include "Field/output_Tecplot.h"


#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/Direct/MKL_PARDISO/MKL_PARDISOSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/EGTess/makeWakedAirfoil.h"
#include "Meshing/EGADS/Airfoils/NACA4.h"
#include "Meshing/TetGen/EGTetGen.h"

#include "unit/UnitGrids/XField3D_Box_Hex_X1_WakeCut.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1_WakeCut.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;
using namespace EGADS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

namespace // private to this file
{

#if 0
//----------------------------------------------------------------------------//
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_LIP_KuttaHACK : public AlgebraicEquationSet_Debug<NDPDEClass, Traits>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandCellClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_Dispatch;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDVectorCategory<boost::mpl::vector1<BC_WakeCut_Potential_Drela>, BCCategory::WakeCut_Potential_Drela>,
                                 Galerkin> IntegrandWake;
  typedef IntegrandBoundaryFrame_CG_Potential_Drela<NDPDEClass> IntegrandKutta;

  template< class... BCArgs >
  AlgebraicEquationSet_LIP_KuttaHACK(const XFieldType& xfld,
                          Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                          Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                          const NDPDEClass& pde,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& WakeGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args )
   : DebugBaseType(pde, {1e-12, 1e-12}),
     fcnCell_(pde, CellGroups),
     fcnWake_(pde, WakeGroups, get<-1>(xfld).dupPointOffset_, get<-1>(xfld).KuttaPoints_),
     fcnKutta_(pde, get<-1>(xfld).dupPointOffset_, get<-1>(xfld).KuttaPoints_),
     BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)), dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups),
     xfld_(xfld), qfld_(qfld), lgfld_(lgfld), pde_(pde)
  {
    quadratureOrder_.resize(std::max(std::max(xfld.nCellGroups(),
                                              xfld.getXField().nBoundaryTraceGroups()),
                                              get<-1>(xfld).nBoundaryFrameGroups()), -1);
    quadratureOrderMin_.resize(quadratureOrder_.size(), 0);
  }

  virtual ~AlgebraicEquationSet_LIP_KuttaHACK() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVector& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrix& mtx       ) override { jacobian<        SystemMatrix&>(mtx, quadratureOrder_ );    }
  virtual void jacobian(SystemNonZeroPattern& nz) override { jacobian<SystemNonZeroPattern&>( nz, quadratureOrderMin_ ); }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrix& mtxT       ) override { jacobian(Transpose(mtxT), quadratureOrder_ );    }
  virtual void jacobianTranspose(SystemNonZeroPattern& nzT) override { jacobian(Transpose( nzT), quadratureOrderMin_ ); }


  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVector& rsd ) const override
  {
    const int nDOFPDE = qfld_.nDOF();
    const int nDOFBC = lgfld_.nDOF();
    const int nMon = pde_.nMonitor();

    DLA::VectorD<Real> rsdPDEtmp(nMon);
    DLA::VectorD<Real> rsdBCtmp(nMon);

    rsdPDEtmp = 0;
    rsdBCtmp = 0;

    std::vector<std::vector<Real>> rsdNorm(2, std::vector<Real>(nMon, 0));

    //compute residual norm
    //HACKED Simple L2 norm
    //TODO: Allow for non-L2 norms
    for (int n = 0; n < nDOFPDE; n++)
    {
      pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] += pow(rsdPDEtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);

    //BC residual
    for (int n = 0; n < nDOFBC; n++)
    {
      pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

      for (int j = 0; j < nMon; j++)
        rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

    return rsdNorm;

  }

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override
  {
    titles = {"PDE : ",
              "BC  : "};
    idx = {iPDE, iBC};
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVector& q) override;

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVector& q) const override;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVector& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  // Indexes to order the equations and the solution vectors
  static const int iPDE = 0;
  static const int iBC = 1;
  static const int iq = 0;
  static const int ilg = 1;

protected:
  template<class SparseMatrixType>
  void jacobian(SparseMatrixType mtx, const std::vector<int>& quadratureOrder );

  IntegrandCellClass fcnCell_;
  IntegrandWake fcnWake_;
  IntegrandKutta fcnKutta_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  const XFieldType& xfld_;
  Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const NDPDEClass& pde_;

  std::vector<int> quadratureOrder_;
  std::vector<int> quadratureOrderMin_;
};

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residual(SystemVector& rsd)
{
#if 0
  IntegrateCellGroups<TopoDim>::integrate(
      ResidualCell_CG_Potential_Drela(fcnCell_, rsd(iq), get<-1>(xfld_).dupPointOffset_, get<-1>(xfld_).invPointMap_),
      xfld_, qfld_, &quadratureOrder_[0], xfld_.nCellGroups() );

  ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake_, qfld_,
                                                                    &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(), rsd(iq) );

  ResidualBoundaryFrame_CG_Potential_Drela<TopoD3>::integrate( fcnKutta_,
                                                           get<-1>(xfld_), qfld_, &quadratureOrder_[0],
                                                           get<-1>(xfld_).nBoundaryFrameGroups(), rsd(iq) );

  dispatchBC_.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_LIP( xfld_, qfld_, lgfld_,
                                                         &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                         rsd(iq), rsd(ilg) ),
      ResidualBoundaryTrace_Dispatch_CG_Potential_Drela( xfld_, qfld_,
                                                          &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                          rsd(iq) ) );
#endif
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const std::vector<int>& quadratureOrder)
{
  SANS_ASSERT(jac.m() == 2);
  SANS_ASSERT(jac.n() == 2);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDE,iq);
  Matrix jacPDE_lg = jac(iPDE,ilg);
  Matrix jacBC_q   = jac(iBC,iq);
  Matrix jacBC_lg  = jac(iBC,ilg);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

#if 0
  IntegrateCellGroups<TopoDim>::integrate( JacobianCell_CG_Potential_Drela<SurrealClass>(fcnCell_, jacPDE_q,
                                                                                   get<-1>(xfld_).dupPointOffset_, get<-1>(xfld_).invPointMap_),
                                           xfld_, qfld_, &quadratureOrder[0], xfld_.nCellGroups() );

  JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate( fcnWake_, qfld_, &quadratureOrder[0],
                                                                                 xfld_.getXField().nBoundaryTraceGroups(),
                                                                                 jacPDE_q );

  JacobianBoundaryFrame_CG_Potential_Drela<SurrealClass,TopoD3>::integrate(fcnKutta_, get<-1>(xfld_), qfld_, &quadratureOrder[0],
                                                                       get<-1>(xfld_).nBoundaryFrameGroups(),
                                                                       jacPDE_q );

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_LIP<SurrealClass>( xfld_, qfld_, lgfld_,
                                                                       &quadratureOrder[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                                       jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg ),
      JacobianBoundaryTrace_Dispatch_CG_Potential_Drela<SurrealClass>( xfld_, qfld_,
                                                                        &quadratureOrder[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                                        jacPDE_q ) );
#endif
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::setSolutionField(const SystemVector& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOF();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld_.DOF(k) = q[iq][k];

  const int nDOFBC = lgfld_.nDOF();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    lgfld_.DOF(k) = q[ilg][k];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::fillSystemVector(SystemVector& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld_.nDOF();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld_.DOF(k);

  const int nDOFBC = lgfld_.nDOF();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    q[ilg][k] = lgfld_.DOF(k);
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  // Create the size that represents the equations linear algebra vector
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  return {nDOFPDEpos,
          nDOFBCpos};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorStateSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFPDE = qfld_.nDOF();
  const int nDOFBC = lgfld_.nDOF();

  return {nDOFPDE,
          nDOFBC};
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");
  static_assert(iq == 0,"");
  static_assert(ilg == 1,"");

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  const int nDOFPDE = qfld_.nDOF();
  const int nDOFBC = lgfld_.nDOF();

  return {{ {nDOFPDEpos, nDOFPDE}, {nDOFPDEpos, nDOFBC} },
          { { nDOFBCpos, nDOFPDE}, { nDOFBCpos, nDOFBC} }};
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
bool
AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
isValidStateSystemVector(SystemVector& q)
{
  bool isValidState = true;

  // Update the solution field
  setSolutionField(q);

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell(pde_, fcnCell_.cellGroups(), isValidState),
                                             xfld_, qfld_, &quadratureOrder_[0], xfld_.nCellGroups() );
  dispatchBC_.dispatch(
      isValidStateBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, lgfld_,
                                                &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(), isValidState ),
      isValidStateBoundaryTrace_sansLG_Dispatch( pde_, xfld_, qfld_,
                                                 &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(), isValidState )
    );

  return isValidState;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
int
AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
nResidNorm() const
{
  return 2;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_LIP_KuttaHACK<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);

//  std::string filename_iBC = filenamebase + "_iBC_iter" + std::to_string(nonlinear_iter) + ".plt";
//  this->dumpLinesearchField(lgfld_, *pStepData, iBC, filename_iBC);
}
#endif

}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_CG_LIP_KuttaHACK_test_suite )

#if 0
//----------------------------------------------------------------------------//
// solution: Hierarchical P1
// wake included with other BC regions
BOOST_AUTO_TEST_CASE( Solve3D_KuttaHACK_Test )
{
  return; //Don't do any testing because memcheck takes too long

  typedef SurrealS<1> SurrealClass;

  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D<Real>> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;

  typedef BCNDConvertSpace<PhysD3, BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Real> > BCDirichlet;
  typedef BCNDConvertSpace<PhysD3, BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Real> > BCNeumann;
  typedef BCNDConvertSpace<PhysD3, BCLinearizedIncompressiblePotential3D<BCTypeWall, Real> > BCWall;

  typedef NDVectorCategory<boost::mpl::vector1<BCDirichlet>, BCDirichlet::Category> NDBCDirichletVecCat;
  typedef NDVectorCategory<boost::mpl::vector1<BCNeumann>, BCNeumann::Category> NDBCNeumannVecCat;
  typedef NDVectorCategory<boost::mpl::vector1<BCWall>, BCWall::Category> NDBCWallVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCDirichletVecCat, Galerkin> IntegrandBCDirichlet;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCNeumannVecCat, Galerkin> IntegrandBCNeumann;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCWallVecCat, Galerkin> IntegrandBCWall;

  typedef NDVectorCategory<boost::mpl::vector1<BC_WakeCut_Potential_Drela>, BC_WakeCut_Potential_Drela::Category> NDBCWakeCutVecCat;

  typedef IntegrandBoundaryTrace<PDEClass, NDBCWakeCutVecCat, Galerkin > IntegrandWake;
  typedef IntegrandBoundaryFrame_Galerkin_Circ<PDEClass> IntegrandCirc;
  typedef IntegrandCell_Galerkin_Kutta_LIP<PDEClass> IntegrandKutta;

  typedef IntegrandCell_Galerkin<PDEClass> IntegrandCellClass;

  timer totaltime;

  timer meshtime;

  Real span = 4;

  // Mark the wake or not as a wake
  bool withWake = true;
  //bool withWake = false;
  const int nChord = 3;
  const int nSpan = 2; //span*nChord;
  const int nWake = 2; //nChord;
#if 0
  EGContext context((CreateContext()));

  //EGApproximate<3> NACA_spline = NACA4<3>(context, 0.001, 0.3, 0);
  EGApproximate<3> NACA_spline = NACA4<3>(context, 0.12, 0.3, 0);

  // EGADS mesh parameters
#if 1
  //std::vector<double> airfoLIParams = {0.05,0.001*100000,15.};
  //std::vector<double> wakeParams = {0.05,0.001*100000,15.};
  std::vector<double> EGADSParams = {2.5, 0.001, 15.0};
  std::vector<double> outflowParams = {2.5, 0.001, 15.0};
  // TetGen mesh parameters
  Real maxRadiusEdgeRatio = 2;//1.75;
  Real minDihedralAngle = 17.5;
#else
  std::vector<double> airfoLIParams = {0.2,0.001,15.};//{0.075,0.001,15.};
  std::vector<double> wakeParams = {0.2,0.001,15.};//{0.075,0.001,15.};
  std::vector<double> EGADSParams = {0.3, 0.001, 15.0};
  // TetGen mesh parameters
  Real maxRadiusEdgeRatio = 2;//1.075;
  Real minDihedralAngle = 0;
#endif

  EGModel<3> model = makeWakedAirfoil(NACA_spline, span, {16.,2000*4./200.,1.5}, nChord, nSpan, nWake, outflowParams, withWake);
  //model.save( "tmp/NACAWake3D.egads");

  //EGModel<3> model(context);
  //model.load("tmp/domain.egads");

  // grid:
  EGTetGen xfld(model, EGADSParams, maxRadiusEdgeRatio, minDihedralAngle);

  std::cout << "Meshing Time: " << meshtime.elapsed() << std::endl;

  //output_Tecplot( xfld, "tmp/RAE.plt" );
  //output_Tecplot( xfld, "tmp/NACA3D.plt" );

  //return;

  // Find the BC faces
  std::vector<int> WallFaces, InflowFaces, OutflowFaces, WakeFaces, LGFaces;

  {
    int BCFace = 0;
    std::vector< EGBody<3> > bodies = model.getBodies();
    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ ) //TODO: This should first go over solid bodies then sheet bodies...
    {
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        if ( faces[i].hasAttribute("Wall") )
        {
          WallFaces.push_back(BCFace);
          LGFaces.push_back(BCFace);
          BCFace++;
        }
        else if ( faces[i].hasAttribute("Inflow") || faces[i].hasAttribute("Lateral") )
        {
          InflowFaces.push_back(BCFace);
          LGFaces.push_back(BCFace);
          BCFace++;
        }
        else if ( faces[i].hasAttribute("Outflow") )
        {
          OutflowFaces.push_back(BCFace);
          LGFaces.push_back(BCFace);
          BCFace++;
        }
      }
    }

    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ ) //TODO: This should first go over solid bodies then sheet bodies...
    {
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        if ( faces[i].hasAttribute("Wake") )
        {
          WakeFaces.push_back(BCFace);
          BCFace++;
        }
      }
    }
  }

#else
  XField3D_Box_Hex_X1_WakeCut xfld( nChord, nSpan, span, nWake );

  std::vector<int> WallFaces = {6,7};
  std::vector<int> InflowFaces = {0};
  std::vector<int> OutflowFaces = {1,2,3,4,5};
  std::vector<int> WakeFaces = {8};
  std::vector<int> LGFaces = {0,1,2,3,4,5,6,7};

  //const int joint0 = 0;
  //const int joint1 = nSpan+1;
#endif

  // quadrature rule
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 2);    // max
  std::vector<int> quadratureOrderMin(xfld.nBoundaryTraceGroups(), 0);     // min

  std::vector<int> quadratureOrderWake(xfld.nBoundaryFrameGroups(), 2);    // max
  std::vector<int> quadratureOrderKutta(xfld.nBoundaryFrameGroups(), 2);    // max

  // PDE
  Real Vinf = 1;

  Real alphamin = 0.01*180/PI;
  int nalpha = 1;
  Real alphamax = 0.01*180/PI;

  Real outdata = 0;
  Real bcdata = 0;

  PDEClass pde( 1, 0, 0 );

  // BC

  BCDirichlet bcInflow( bcdata );
  BCNeumann bcOutflow( outdata );
  BCWall bcwall( pde );


  std::vector<int> frameKutta = {1};

  // integrands
  IntegrandCellClass fcnCell( pde, {0} );
  IntegrandBCDirichlet fcnBCInflow( pde, bcInflow, InflowFaces );
  IntegrandBCNeumann fcnBCOutflow( pde, bcOutflow, OutflowFaces );
  IntegrandBCWall fcnBCwall( pde, bcwall, WallFaces );
  IntegrandWake fcnWake(pde, WakeFaces, 0*xfld.dupPointOffset_, {});

  IntegrandCirc fcnCirc(pde, frameKutta, 0*xfld.dupPointOffset_);
  IntegrandKutta fcnKutta(pde, frameKutta, {1}, {2});

  int order = 1;

  // solution: Hierarchical, C0
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical),
                                        qfldnew(xfld, order, BasisFunctionCategory_Hierarchical);


  Field_DG_BoundaryFrame<PhysD3, TopoD3, ArrayQ> Gfld(xfld,0,BasisFunctionCategory_Legendre,frameKutta),
      Gfldnew(xfld,0,BasisFunctionCategory_Legendre,frameKutta);

  qfld = 0;

  const int nDOFPDE = qfld.nDOF();
  const int nDOFKutta = Gfld.nDOF();

  std::cout << " nDOFPDE=" << nDOFPDE << std::endl;
  std::cout << " nDOFKutta=" << nDOFKutta << std::endl;
  std::cout << " Duplicates after " << xfld.dupPointOffset_ << std::endl;

 //Dummy Lagrange multipliers for now...
#ifdef LG_WAKE
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#else
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical, LGFaces );
#endif

  lgfld = 0;


#if 0
  cout << "nDOFPDE = " << nDOFPDE << endl;
  cout << "  InflowFaces = " << InflowFaces.size()
       << "  OutflowFaces = " << OutflowFaces.size()
       << "  WallFaces = " << WallFaces.size()
       << "  WakeFaces = " << WakeFaces.size() << endl;
  cout << "  lgfld.nBoundaryTraceGroups() = " << lgfld.nBoundaryTraceGroups() << endl;
#endif




  // linear system setup

  typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
  typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
  typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
  typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;

  typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

  DLA::MatrixD<NonZeroPatternClass> nz = { {{nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFKutta}},
                                           {{nDOFKutta, nDOFPDE}, {nDOFKutta,nDOFKutta}} };


  NonZeroPatternClass& nzPDE_q = nz(0,0);
  NonZeroPatternClass& nzPDE_G = nz(0,1);
  NonZeroPatternClass& nzKutta_q = nz(1,0);
  NonZeroPatternClass& nzKutta_G = nz(1,1);

  IntegrateCellGroups<TopoD3>::integrate( JacobianCell_Galerkin(fcnCell, nzPDE_q),
                                          xfld, qfld, &quadratureOrderMin[0], fcnCell.nCellGroups() );
#if 1
  IntegrateBoundaryTraceGroups<TopoD3>::integrate( JacobianBoundaryTrace_Galerkin<SurrealClass>(fcnBCInflow, nzPDE_q),
                                                        xfld, qfld, &quadratureOrderMin[0], quadratureOrderMin.size() );
  IntegrateBoundaryTraceGroups<TopoD3>::integrate( JacobianBoundaryTrace_Galerkin<SurrealClass>(fcnBCOutflow, nzPDE_q),
                                                        xfld, qfld, &quadratureOrderMin[0], quadratureOrderMin.size() );
  IntegrateBoundaryTraceGroups<TopoD3>::integrate( JacobianBoundaryTrace_Galerkin<SurrealClass>(fcnBCwall, nzPDE_q),
                                                        xfld, qfld, &quadratureOrderMin[0], quadratureOrderMin.size() );
#endif



  if ( withWake )
  {

    JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate( fcnWake, qfld,
                                                                                      &quadratureOrderMin[0], quadratureOrderMin.size(),
                                                                                      nzPDE_q );

    JacobianBoundaryFrame_Galerkin_Circ<SurrealClass,TopoD3>::integrate(fcnCirc, xfld, qfld, Gfld,
                                                                        &quadratureOrderMin[0], xfld.nBoundaryFrameGroups(),
                                                                        nzPDE_q, nzPDE_G, nzKutta_q, nzKutta_G );

    JacobianCell_Galerkin_Kutta<SurrealClass,TopoD3>::integrate(fcnKutta, xfld, qfld, Gfld,
                                                                &quadratureOrderMin[0], qfld.nCellGroups(),
                                                                nzKutta_q );

    //Gamma_Hack_WakeCut_LIP(joint0, Gfld.nDOF()  , nzKutta_G );
    //Gamma_Hack_WakeCut_LIP(joint1, Gfld.nDOF()+1, nzKutta_G );
  }

  unsigned int maxRow = 0, maxRowSize = 0;
  for ( int i = 0; i < nzPDE_q.m(); i++ )
    if ( nzPDE_q.rowSize(i) > maxRowSize )
    {
      maxRow=i;
      maxRowSize = nzPDE_q.rowSize(i);
    }

  std::cout << "maxRow =" << maxRow << " maxRowSize=" << maxRowSize << std::endl;

  // Solution and residual vector and jacobian matrix
  SystemVectorClass sln = { {nDOFPDE}, {nDOFKutta} };
  SystemVectorClass rsd = { {nDOFPDE}, {nDOFKutta} }, rsdnew = { {nDOFPDE}, {nDOFKutta} };
  SystemMatrixClass jac(nz);

  SparseMatrixClass& jacPDE_q = jac(0,0);
  SparseMatrixClass& jacPDE_G = jac(0,1);
  SparseMatrixClass& jacKutta_q = jac(1,0);
  SparseMatrixClass& jacKutta_G = jac(1,1);

//#ifdef INTEL_MKL
//    SLA::MKL_PARDISO<SystemMatrixClass> solver;
//#else
   //SLA::UMFPACK<SystemMatrixClass> solver;
//#endif

  for ( int na = 0; na < nalpha; na++)
  {
    Real alpha = alphamin + na*(alphamax-alphamin)/std::max(1,nalpha-1);

    //alpha = 0.01*180/PI;

    std::cout << "-----------------" << std::endl;
    std::cout << "alpha          = " << alpha << std::endl;
#if 0
    Real U = Vinf*cos(alpha*PI/180), V = Vinf*sin(alpha*PI/180), W = Vinf*0;
    DLA::VectorS<3, Real> Ddir = { U, V, W};
    DLA::VectorS<3, Real> Ldir = {-V, U, W};
#else
    Real U = Vinf*cos(alpha*PI/180), V = Vinf*0, W = Vinf*sin(alpha*PI/180);
    DLA::VectorS<3, Real> Ddir = { U, V, W};
    DLA::VectorS<3, Real> Ldir = {-W, V, U};
#endif

    //Update the angle of attack
    pde.setFreestream(Ddir);

    Ddir /= Vinf;
    Ldir /= Vinf;

    qfld = 0;
    Gfld = 0;
    for (int newton = 0; newton < 1; newton++)
    {
      rsd = 0;
      jac = 0;

      timer rsdtime;

      // residual

      IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnCell, rsd[0]), xfld, qfld, &quadratureOrder[0], fcnCell.nCellGroups() );

      IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCInflow, rsd[0]),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
      IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCOutflow, rsd[0]),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
      IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCwall, rsd[0]),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

      std::cout << "Residual Time: " << rsdtime.elapsed() << std::endl;

      timer jactime;

      IntegrateCellGroups<TopoD3>::integrate( JacobianCell_Galerkin(fcnCell, jacPDE_q),
                                              xfld, qfld, &quadratureOrder[0], fcnCell.nCellGroups() );
    #if 1
      IntegrateBoundaryTraceGroups<TopoD3>::integrate( JacobianBoundaryTrace_Galerkin<SurrealClass>(fcnBCInflow, jacPDE_q),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
      IntegrateBoundaryTraceGroups<TopoD3>::integrate( JacobianBoundaryTrace_Galerkin<SurrealClass>(fcnBCOutflow, jacPDE_q),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
      IntegrateBoundaryTraceGroups<TopoD3>::integrate( JacobianBoundaryTrace_Galerkin<SurrealClass>(fcnBCwall, jacPDE_q),
                                                            xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
    #endif
      std::cout << "Jacobian Time: " << jactime.elapsed() << std::endl;

      if ( withWake )
      {
        JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate( fcnWake, qfld,
                                                                                          &quadratureOrderWake[0], quadratureOrder.size(),
                                                                                          jacPDE_q );

        JacobianBoundaryFrame_Galerkin_Circ<SurrealClass,TopoD3>::integrate(fcnCirc, xfld, qfld, Gfld,
                                                                            &quadratureOrderKutta[0], xfld.nBoundaryFrameGroups(),
                                                                            jacPDE_q, jacPDE_G, jacKutta_q, jacKutta_G );

        JacobianCell_Galerkin_Kutta<SurrealClass,TopoD3>::integrate(fcnKutta, xfld, qfld, Gfld,
                                                                    &quadratureOrder[0], qfld.nCellGroups(),
                                                                    jacKutta_q );

        //Gamma_Hack_WakeCut_LIP(joint0, Gfld.nDOF()  , jacKutta_G );
        //Gamma_Hack_WakeCut_LIP(joint1, Gfld.nDOF()+1, jacKutta_G );

        ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfld,
                                                                             &quadratureOrderWake[0], quadratureOrder.size(), rsd[0] );

        ResidualBoundaryFrame_Galerkin_Circ<TopoD3>::integrate(fcnCirc, xfld, qfld, Gfld,
                                                               &quadratureOrderKutta[0], xfld.nBoundaryFrameGroups(), rsd[0], rsd[1] );

        ResidualCell_Galerkin_Kutta<TopoD3>::integrate( fcnKutta, xfld, qfld, Gfld, &quadratureOrder[0], qfld.nCellGroups(), rsd[1] );
      }

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      Real rsdKuttanrm = 0;
      for (int n = 0; n < nDOFKutta; n++)
        rsdKuttanrm += pow(rsd[1][n],2);

      std::cout << "Newton " << newton << " rsd[0] = " << sqrt(rsdPDEnrm) << " rsd[1] = " << sqrt(rsdKuttanrm) << std::endl;

#if 1
      WriteMatrixMarketFile( jac, "tmp/NACA0.mtx" );

#if 1
      for ( int i = 0; i < jacPDE_q.m(); i++ )
      {
        Real diag = jacPDE_q.diag(i);
        bool smalldiag = false;
        Real rowSum = 0;
        for ( int k = 0; k < jacPDE_q.rowNonZero(i); k++)
        {
          if ( diag < jacPDE_q.sparseRow(i,k) ) smalldiag = true;
          rowSum += jacPDE_q.sparseRow(i,k);
        }

        if ( smalldiag || i >= xfld.dupPointOffset_  ) //rowSum > 1e-10 ||
        {
          std::vector<Real> AJac;
          std::vector<int> iAJac;
          std::cout << "-----------------" << std::endl;
          std::cout << "row = " << i << " sum = " << rowSum << " smalldiag = " << smalldiag << std::endl;
          std::cout << "diag = " << jacPDE_q.diag(i) << std::endl;
          for ( int k = 0; k < jacPDE_q.rowNonZero(i); k++)
            if ( abs(jacPDE_q.sparseRow(i,k)) > 1e-12)
            {
              AJac.push_back(jacPDE_q.sparseRow(i,k));
              std::cout << jacPDE_q.sparseRow(i,k) << ", ";
            }
          std::cout << std::endl;
          for ( int k = 0; k < jacPDE_q.rowNonZero(i); k++)
            if ( abs(jacPDE_q.sparseRow(i,k)) > 1e-12)
            {
              iAJac.push_back(jacPDE_q.get_col_ind()[jacPDE_q.get_row_ptr()[i] + k]);
              std::cout << jacPDE_q.get_col_ind()[jacPDE_q.get_row_ptr()[i] + k] << ", ";
            }
          std::cout << std::endl;

          for ( int k = 0; k < qfld.nDOF(); k++)
            qfldnew.DOF(k) = qfld.DOF(k);

          for ( int k = 0; k < Gfld.nDOF(); k++)
            Gfldnew.DOF(k) = Gfld.DOF(k);

          std::vector<Real> diffJac;
          std::vector<int> idiffJac;
          for ( int k = 0; k < qfld.nDOF(); k++)
          {

            rsdnew = 0;
            qfldnew.DOF(k) += 1;

            IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnCell, rsdnew[0]),
                                                    xfld, qfldnew, &quadratureOrder[0], fcnCell.nCellGroups() );

            IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCInflow, rsdnew[0]),
                                                                  xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );
            IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCOutflow, rsdnew[0]),
                                                                  xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );
            IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCwall, rsdnew[0]),
                                                                  xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );


            ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfldnew,
                                                                            &quadratureOrderWake[0], quadratureOrder.size(), rsdnew[0] );

            ResidualBoundaryFrame_Galerkin_Circ<TopoD3>::integrate(fcnCirc, xfld, qfldnew, Gfldnew,
                                                                   &quadratureOrderKutta[0], xfld.nBoundaryFrameGroups(), rsdnew[0], rsdnew[1] );

            ResidualCell_Galerkin_Kutta<TopoD3>::integrate( fcnKutta, xfld, qfldnew, Gfldnew, &quadratureOrder[0], qfld.nCellGroups(), rsdnew[1] );

            qfldnew.DOF(k) -= 1;

            Real diff = rsdnew[0][i] - rsd[0][i];
            if ( abs(diff) > 1e-12 )
            {
              diffJac.push_back(diff);
              idiffJac.push_back(k);
            }
          }
          std::cout << "FD" << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << diffJac[k] << ", ";
          std::cout << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << idiffJac[k] << ", ";
          std::cout << std::endl;

          bool error = false;
          std::cout << "A-FD" << std::endl;
          BOOST_REQUIRE_EQUAL(AJac.size(), diffJac.size());
          for ( std::size_t k = 0; k < diffJac.size(); k++)
          {
            if ( abs(AJac[k]-diffJac[k]) > 1e-12)
            {
              error = true;
              std::cout << "AJac[" << k << "]=" << AJac[k]
                        << " diffJac[" << k << "]=" << diffJac[k] << std::endl;
            }
          }
          BOOST_REQUIRE(!error);
        }
      }
#endif
#if 1
      for ( int i = 0; i < jacPDE_G.m(); i++ )
      {
        std::vector<Real> AJac;
        std::vector<int> iAJac;
        if ( jacPDE_G.rowNonZero(i) > 0 )
        {
          std::cout << "-----------------" << std::endl;
          std::cout << "row = " << i << std::endl;
          //std::cout << "diag = " << jacPDE_G.diag(i) << std::endl;
          for ( int k = 0; k < jacPDE_G.rowNonZero(i); k++)
            if ( abs(jacPDE_G.sparseRow(i,k)) > 1e-12)
            {
              AJac.push_back(jacPDE_G.sparseRow(i,k));
              std::cout << jacPDE_G.sparseRow(i,k) << ", ";
            }
          std::cout << std::endl;
          for ( int k = 0; k < jacPDE_G.rowNonZero(i); k++)
            if ( abs(jacPDE_G.sparseRow(i,k)) > 1e-12)
            {
              iAJac.push_back(jacPDE_G.get_col_ind()[jacPDE_G.get_row_ptr()[i] + k]);
              std::cout << jacPDE_G.get_col_ind()[jacPDE_G.get_row_ptr()[i] + k] << ", ";
            }
          std::cout << std::endl;
        }

        for ( int k = 0; k < qfld.nDOF(); k++)
          qfldnew.DOF(k) = qfld.DOF(k);

        for ( int k = 0; k < Gfld.nDOF(); k++)
          Gfldnew.DOF(k) = Gfld.DOF(k);

        std::vector<Real> diffJac;
        std::vector<int> idiffJac;
        for ( int k = 0; k < Gfld.nDOF(); k++)
        {

          rsdnew = 0;
          Gfldnew.DOF(k) += 1;

          IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnCell, rsdnew[0]),
                                                  xfld, qfldnew, &quadratureOrder[0], fcnCell.nCellGroups() );

          IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCInflow, rsdnew[0]),
                                                                xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );
          IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCOutflow, rsdnew[0]),
                                                                xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );
          IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCwall, rsdnew[0]),
                                                                xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );

          ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfldnew,
                                                                          &quadratureOrderWake[0], quadratureOrder.size(), rsdnew[0] );

          ResidualBoundaryFrame_Galerkin_Circ<TopoD3>::integrate(fcnCirc, xfld, qfldnew, Gfldnew,
                                                                 &quadratureOrderKutta[0], xfld.nBoundaryFrameGroups(), rsdnew[0], rsdnew[1] );

          ResidualCell_Galerkin_Kutta<TopoD3>::integrate( fcnKutta, xfld, qfldnew, Gfldnew, &quadratureOrder[0], qfld.nCellGroups(), rsdnew[1] );

          Gfldnew.DOF(k) -= 1;

          Real diff = rsdnew[0][i] - rsd[0][i];
          if ( abs(diff) > 1e-12 )
          {
            diffJac.push_back(diff);
            idiffJac.push_back(k);
          }
        }
        if ( diffJac.size() > 0 ||  jacPDE_G.rowNonZero(i) > 0 )
        {
          std::cout << "FD" << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << diffJac[k] << ", ";
          std::cout << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << idiffJac[k] << ", ";
          std::cout << std::endl;

          bool error = false;
          std::cout << "A-FD" << std::endl;
          BOOST_REQUIRE_EQUAL(AJac.size(), diffJac.size());
          for ( std::size_t k = 0; k < diffJac.size(); k++)
          {
            if ( abs(AJac[k]-diffJac[k]) > 1e-12)
            {
              error = true;
              std::cout << "AJac[" << k << "]=" << AJac[k]
                        << " diffJac[" << k << "]=" << diffJac[k] << std::endl;
            }
          }
          BOOST_REQUIRE(!error);
        }
      }
#endif
#if 1
      for ( int i = 0; i < jacKutta_q.m(); i++ )
      {
        std::vector<Real> AJac;
        std::vector<int> iAJac;
        if ( jacKutta_q.rowNonZero(i) > 0 )
        {
          std::cout << "-----------------" << std::endl;
          std::cout << "row = " << i << std::endl;
          //std::cout << "diag = " << jacKutta_q.diag(i) << std::endl;
          for ( int k = 0; k < jacKutta_q.rowNonZero(i); k++)
            if ( abs(jacKutta_q.sparseRow(i,k)) > 1e-12)
            {
              AJac.push_back(jacKutta_q.sparseRow(i,k));
              std::cout << jacKutta_q.sparseRow(i,k) << ", ";
            }
          std::cout << std::endl;
          for ( int k = 0; k < jacKutta_q.rowNonZero(i); k++)
            if ( abs(jacKutta_q.sparseRow(i,k)) > 1e-12)
            {
              iAJac.push_back(jacKutta_q.get_col_ind()[jacKutta_q.get_row_ptr()[i] + k]);
              std::cout << jacKutta_q.get_col_ind()[jacKutta_q.get_row_ptr()[i] + k] << ", ";
            }
          std::cout << std::endl;
        }

        for ( int k = 0; k < qfld.nDOF(); k++)
          qfldnew.DOF(k) = qfld.DOF(k);

        for ( int k = 0; k < Gfld.nDOF(); k++)
          Gfldnew.DOF(k) = Gfld.DOF(k);

        std::vector<Real> diffJac;
        std::vector<int> idiffJac;
        for ( int k = 0; k < qfld.nDOF(); k++)
        {

          rsdnew = 0;
          qfldnew.DOF(k) += 1;

          IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnCell, rsdnew[0]),
                                                  xfld, qfldnew, &quadratureOrder[0], fcnCell.nCellGroups() );

          IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCInflow, rsdnew[0]),
                                                                xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );
          IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCOutflow, rsdnew[0]),
                                                                xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );
          IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCwall, rsdnew[0]),
                                                                xfld, qfldnew, &quadratureOrder[0], quadratureOrder.size() );

          ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfldnew,
                                                                          &quadratureOrderWake[0], quadratureOrder.size(), rsdnew[0] );

          ResidualBoundaryFrame_Galerkin_Circ<TopoD3>::integrate(fcnCirc, xfld, qfldnew, Gfldnew,
                                                                 &quadratureOrderKutta[0], xfld.nBoundaryFrameGroups(), rsdnew[0], rsdnew[1] );

          ResidualCell_Galerkin_Kutta<TopoD3>::integrate( fcnKutta, xfld, qfldnew, Gfldnew, &quadratureOrder[0], qfld.nCellGroups(), rsdnew[1] );

          qfldnew.DOF(k) -= 1;

          Real diff = rsdnew[1][i] - rsd[1][i];
          if ( abs(diff) > 1e-12 )
          {
            diffJac.push_back(diff);
            idiffJac.push_back(k);
          }
        }
        if ( diffJac.size() > 0 ||  jacKutta_q.rowNonZero(i) > 0 )
        {
          std::cout << "FD" << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << diffJac[k] << ", ";
          std::cout << std::endl;
          for ( std::size_t k = 0; k < diffJac.size(); k++)
            std::cout << idiffJac[k] << ", ";
          std::cout << std::endl;

          bool error = false;
          std::cout << "A-FD" << std::endl;
          BOOST_REQUIRE_EQUAL(AJac.size(), diffJac.size());
          for ( std::size_t k = 0; k < diffJac.size(); k++)
          {
            if ( abs(AJac[k]-diffJac[k]) > 1e-12)
            {
              error = true;
              std::cout << "AJac[" << k << "]=" << AJac[k]
                        << " diffJac[" << k << "]=" << diffJac[k] << std::endl;
            }
          }
          BOOST_REQUIRE(!error);
        }
      }
#endif
//      if (rsdPDEnrm < 1e-10) break;
#endif

      timer solvetime;

      SANS_DEVELOPER_EXCEPTION("Need to update the solver!");
      //sln = solver.inverse(jac)*rsd;

      std::cout << "Solve Time: " << solvetime.elapsed() << std::endl;

      // updated solution
      for (int k = 0; k < nDOFPDE; k++)
        qfld.DOF(k) -= sln[0][k];

      for (int k = 0; k < Gfld.nDOF(); k++)
        Gfld.DOF(k) -= sln[1][k];

    }
    // check that the residual is zero
#if 1
    rsd = 0;

    IntegrateCellGroups<TopoD3>::integrate( ResidualCell_Galerkin(fcnCell, rsd[0]), xfld, qfld, &quadratureOrder[0], fcnCell.nCellGroups() );

    IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCInflow, rsd[0]),
                                                          xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCOutflow, rsd[0]),
                                                          xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate( ResidualBoundaryTrace_Galerkin(fcnBCwall, rsd[0]),
                                                          xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

    if ( withWake )
    {
      ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake, qfld,
                                                                      &quadratureOrderWake[0], quadratureOrder.size(), rsd[0] );

      ResidualBoundaryFrame_Galerkin_Circ<TopoD3>::integrate(fcnCirc, xfld, qfld, Gfld,
                                                             &quadratureOrderKutta[0], xfld.nBoundaryFrameGroups(), rsd[0], rsd[1] );

      ResidualCell_Galerkin_Kutta<TopoD3>::integrate( fcnKutta, xfld, qfld, Gfld, &quadratureOrder[0], qfld.nCellGroups(), rsd[1] );
    }

#if 1
    std::cout << "Kutta Frame map = ";
    for (int n = 0; n < xfld.getBoundaryFrameGroup(frameKutta[0]).nElem(); n++)
    {
      int nodeMap[2];
      xfld.getBoundaryFrameGroup(frameKutta[0]).associativity(n).getNodeGlobalMapping(nodeMap, 2);
      std::cout << "{ " << nodeMap[0] << ", " << nodeMap[1] << " }";
      if ( n < xfld.getBoundaryFrameGroup(frameKutta[0]).nElem()-1 )
        std::cout << ", ";
    }
    std::cout << std::endl;
#endif

#if 1
    std::cout << "Kutta Frame Field map = ";
    for (int n = 0; n < Gfld.getBoundaryFrameGroupGlobal(frameKutta[0]).nElem(); n++)
    {
      int nodeMap[1];
      Gfld.getBoundaryFrameGroupGlobal(frameKutta[0]).associativity(n).getGlobalMapping(nodeMap, 1);
      std::cout << "{ " << nodeMap[0] << " }";
      if ( n < Gfld.getBoundaryFrameGroupGlobal(frameKutta[0]).nElem()-1 )
        std::cout << ", ";
    }
    std::cout << std::endl;
#endif
#if 0
    std::cout << "Kutta Coordinates = ";
    for (int n = 0; n < xfld.getBoundaryFrameGroup(frameKutta[0]).nElem(); n++)
    {
      int nodeMap[2];
      xfld.getBoundaryFrameGroup(frameKutta[0]).associativity(n).getNodeGlobalMapping(nodeMap, 2);
      std::cout << "{ (" << xfld.DOF(nodeMap[0]) << "), (" << xfld.DOF(nodeMap[1]) << ") }";
      if ( n < xfld.getBoundaryFrameGroup(frameKutta[0]).nElem()-1 )
        std::cout << ", ";
    }
    std::cout << std::endl;
#endif

    std::cout << std::setprecision(16);
    const char xyz[] = "xyz";
    for (int d = 0; d < 3; d++)
    {
      std::cout << xyz[d] << " = { ";
      for (int n = 0; n < xfld.getBoundaryFrameGroup(frameKutta[0]).nElem(); n++)
      {
        int nodeMap[2];
        xfld.getBoundaryFrameGroup(frameKutta[0]).associativity(n).getNodeGlobalMapping(nodeMap, 2);
        std::cout << xfld.DOF(nodeMap[0])[d] << ", " << xfld.DOF(nodeMap[1])[d];
        if ( n < xfld.getBoundaryFrameGroup(frameKutta[0]).nElem()-1 )
          std::cout << ", ";
      }
      std::cout << " };" << std::endl;
    }


    std::cout << "gamma = { ";
    for (int n = 0; n < Gfld.getBoundaryFrameGroupGlobal(frameKutta[0]).nElem(); n++)
    {
      int nodeMap[1];
      Gfld.getBoundaryFrameGroupGlobal(frameKutta[0]).associativity(n).getGlobalMapping(nodeMap, 1);
      std::cout << Gfld.DOF(nodeMap[0]) << ", " << Gfld.DOF(nodeMap[0]);
      if ( n < Gfld.getBoundaryFrameGroupGlobal(frameKutta[0]).nElem()-1 ) std::cout << ", ";
    }
    std::cout << " };" << std::endl;


    std::cout << std::setprecision(8);
    Real rsdPDEnrm = 0;
    for (int n = 0; n < nDOFPDE; n++)
    {
//      rsdfld.DOF(n) = rsd[n];
      rsdPDEnrm += pow(rsd[0][n],2);
    }

    Real rsdKuttanrm = 0;
    //std::cout << "rsdKutta = { ";
    for (int n = 0; n < nDOFKutta; n++)
    {
      //std::cout << rsd[1][n];
      //if ( n < nDOFKutta-1 )std::cout << ", ";
      rsdKuttanrm += pow(rsd[1][n],2);
    }
    //std::cout << " }" << std::endl;

    BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );
    BOOST_CHECK_SMALL( sqrt(rsdKuttanrm), 1e-12 );
#endif
    Real Drag = 0;
    Real TrefftzLift = 0;
    DLA::VectorS<3,Real> PressureForce = 0;
    Real Vn2 = 0;
    quadratureOrder[0] = 2;
    quadratureOrder[1] = 2;

    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzInducedDrag, Real> fcnTrefftzDrag(pde, {0});
    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Real> fcnTrefftzLift(pde, Ldir, {0});
    IntegrandBoundary3D_LIP_Force<Real> fcnBodyForce(pde, WallFaces);
    IntegrandBoundary3D_LIP_Vn2<Real> fcnVn(pde, WallFaces);

    if ( withWake )
    {
      FunctionalTrefftz::integrate(fcnTrefftzDrag, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), Drag);
      FunctionalTrefftz::integrate(fcnTrefftzLift, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), TrefftzLift);
    }
    //FunctionalBoundaryTrace_sansLG<TopoD3>::integrate(fcnBodyForce, qfld, &quadratureOrder[0], quadratureOrder.size(), PressureForce);
    //FunctionalBoundaryTrace_sansLG<TopoD3>::integrate(fcnVn, qfld, &quadratureOrder[0], quadratureOrder.size(), Vn2);
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( fcnBodyForce, PressureForce ), xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( fcnVn, Vn2 ), xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

    Real rho = 1;
    Real Vinf2 = U*U + V*V + W*W;

    Drag /= (0.5*rho*Vinf2*span);
    TrefftzLift /= (0.5*rho*Vinf2*span);
    PressureForce /= (0.5*rho*Vinf2*span);

    std::cout << "Trefftz Drag   = " << Drag << std::endl;
    std::cout << "Trefftz Lift   = " << TrefftzLift << std::endl;
    std::cout << "Pressure Force = " << PressureForce << std::endl;
    std::cout << "Pressure Drag  = " << dot(PressureForce,Ddir) << std::endl;
    std::cout << "Pressure Lift  = " << dot(PressureForce,Ldir) << std::endl;
    std::cout << "L2 Vn  = " << sqrt(Vn2) << std::endl;

  }

  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;

#if 1
  // Tecplot dump
  string filename = "tmp/NACA0_LIP.dat";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot_LIP( pde, qfld, filename );
#endif

#if 0
  Field_CG_Cell<PhysD3, TopoD3, DLA::VectorS<1,Real> > rsdfld(xfld, order);
  for (int n = 0; n < nDOFPDE; n++)
    rsdfld.DOF(n) = rsd[n];
  output_Tecplot( rsdfld, "tmp/NACA0_LIPrsd.dat" );
#endif

#if 0
  // Tecplot dump
  string filenameLG = "tmp/NACA0_LG_LIP.dat";
  cout << "calling output_Tecplot: filename = " << filenameLG << endl;
  output_Tecplot_LIP( pde, lgfld, filenameLG );
#endif

  cout << "1st finished:" << endl;
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
