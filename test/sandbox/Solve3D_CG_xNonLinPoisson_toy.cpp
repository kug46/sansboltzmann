// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_3D_DGBR2_NonLinPoisson_btest
// testing of 3-D DG with a non-linear Poisson problem
//
// The exact solution is: ???
//
// Q(x) = 1/lam * ln( (A + B*x + C*y + D*z)/ E)
//
// where A, B, C, E and D are arbitrary coefficients determined completely by the BC's.

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"    //Real
#include "tools/linspace.h"
#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
//#include "pde/ForcingFunction_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"
#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h" // Added for Transient

//CG Discretization
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryFrame.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/FieldBase.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"
#include "unit/UnitGrids/XField3D_Box_Hex_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//#############################################################################
BOOST_AUTO_TEST_SUITE( Solve3D_CG_NonLinPoisson_test_suite )

BOOST_AUTO_TEST_CASE( Solve3D_CG_NonLinPoisson )
{

  typedef PDEAdvectionDiffusion<PhysD3, AdvectiveFlux3D_None, ViscousFlux3D_Poly, Source3D_None> PDEClass;

  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_None, ViscousFlux3D_Poly> BCVector;

  typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector, AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // global communicator
  mpi::communicator world;

  // PDE
  Real A = 13.1, B = 0;

  AdvectiveFlux3D_None adv;
  Source3D_None source;
  ViscousFlux3D_Poly visc( A, B );

  NDPDEClass pde( adv, visc, source );

  // BC
  PyDict BCNeumann;
  BCNeumann[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCNeumann[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.A] = 0;
  BCNeumann[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.B] = 1;
  BCNeumann[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCDirichlet_high;
  BCDirichlet_high[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichlet_high[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCDirichlet_high[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCDirichlet_high[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.bcdata] = 1000;

  PyDict BCDirichlet_low;
  BCDirichlet_low[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichlet_low[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCDirichlet_low[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCDirichlet_low[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.bcdata] = 200;

  PyDict PyBCList;
  PyBCList["BCDirichlet_high"] = BCDirichlet_high;
  PyBCList["BCDirichlet_low"] = BCDirichlet_low;
  PyBCList["BCNeumann"] = BCNeumann;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define Boundary Groups for each Boundary Condition
  BCBoundaryGroups["BCNeumann"] = { XField3D_Box_Tet_X1::iZmin,
                                    XField3D_Box_Tet_X1::iZmax,
                                    XField3D_Box_Tet_X1::iYmin,
                                    XField3D_Box_Tet_X1::iYmax};
  BCBoundaryGroups["BCDirichlet_high"] = { XField3D_Box_Tet_X1::iXmin};
  BCBoundaryGroups["BCDirichlet_low"]  = { XField3D_Box_Tet_X1::iXmax};
  int order = 3;

  // loop over grid resolution: 2^power
  int ii, jj, kk;
  int power = 2;
  ii = pow( 2, power );
  jj = kk = ii;

  // grid:
  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk, -1, 1, -1, 1, -10, 10 );
  //XField3D_Box_Hex_X1 xfld( world, ii, jj, kk, -1, 1, -1, 1, -10, 10 );

  // Solution: Hierarchical
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld( xfld, order, BasisFunctionCategory_Lagrange );
  // Initialize
  qfld = 300;

  //
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Lagrange,
                                                        BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups ) );
  lgfld = 0;

  // Integration :: Quadrature order
  int quadOrder = 8; //3*order+2
  QuadratureOrder quadratureOrder( xfld, quadOrder );
  std::vector<Real> tol = { 1e-9, 1e-9 };

  // Spatial Discretization
  StabilizationNitsche stab(order);
  PrimalEquationSetClass PrimalEqSet( xfld, qfld, lgfld, pde, stab, quadratureOrder, ResidualNorm_L2,
                                      tol, { 0 }, // { 0,1,2,3 },
                                      PyBCList, BCBoundaryGroups );

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // Create the solver object
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //NewtonSolver<SystemMatrixClass,SystemNonZeroPattern> nonlinear_solver( PrimalEqSet, NewtonSolverDict );
  NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );
  // set initial condition from current solution in solution fields
  SystemVectorClass sln0(PrimalEqSet.vectorStateSize());
  PrimalEqSet.fillSystemVector(sln0);

  // nonlinear solve
  SystemVectorClass sln(PrimalEqSet.vectorStateSize());
  SolveStatus status = nonlinear_solver.solve(sln0, sln);
  BOOST_CHECK( status.converged );

  // Tecplot Output
  string filename = "tmp/solnCG_xNonLinPoisson";
  filename += "_P" + to_string( order );
  filename += ".plt";
  output_Tecplot( qfld, filename );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
