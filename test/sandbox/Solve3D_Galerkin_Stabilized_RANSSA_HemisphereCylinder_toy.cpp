// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA3D.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCRANSSA3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/OutputEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_EG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_BoundaryTrace.h"

#include "Field/FieldVolume_CG_Cell.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Meshing/EPIC/XField_PX.h"
#include "Meshing/ugrid/XField_ugrid.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

//----------------------------------------------------------------------------//
//template <template <class> class PDETraitsSize, class PDETraitsModel>
//void
//output_Tecplot_RANS_Wall( const PDERANSSA3D<PDETraitsSize, PDETraitsModel>& pde,
//                          const Field< PhysD3, TopoD3, DLA::VectorS<6,Real>>& qfld,
//                          const FieldLift< PhysD3, TopoD3, DLA::VectorS<PhysD3::D,DLA::VectorS<6,Real>> >& rfld,
//                          const DiscretizationDGBR2& disc,
//                          const std::string& filename,
//                          const bool partitioned = false);

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_VMSDBR2_RANS_HemisphereCylinder_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_VMSDBR2_RANS_HemisphereCylinder_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  //typedef QTypePrimitiveSurrogate QType;
//  typedef QTypeConservative QType;

  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

#ifndef BOUNDARYOUTPUT
  typedef ScalarFunction3D_Const WeightFcn; // Change this to change output weighting type
  typedef OutputCell_WeightedSolution<PDEClass, WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputForce;
  typedef IntegrandCell_DGBR2_Output<NDOutputForce> OutputIntegrandClass;
#else
  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Force<PDEClass>> NDOutputForce;
  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Moment<PDEClass>> NDOutputMoment;
  typedef NDVectorCategory<boost::mpl::vector2<NDOutputForce, NDOutputMoment>, NDOutputForce::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;
#endif

  typedef ParamType_Distance ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  int iref = 4;
  int aoa = 0;

  int orderMax = 3;
  bool isRestart = false;
  std::string restartFileBase = "";

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    orderMax = std::stoi(argv[1]);
  if (argc >= 3)
    iref = std::stoi(argv[2]);
  if (argc >= 4)
    aoa = std::stoi(argv[3]);
  if (argc >= 5)
  {
    restartFileBase = argv[4];
    isRestart = true;
  }

  if (world.rank() == 0)
  {
    std::cout << ", order = " << orderMax;
    std::cout << ", iRef = " << iref << ", aoa: " << aoa << std::endl;
  }
#endif

  std::string filename_base = "tmp/hemisphere-cylinder/fixed/aoa" + std::to_string(aoa) + "/";

  boost::filesystem::create_directories(filename_base);

  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld =
     std::make_shared<XField_ugrid<PhysD3, TopoD3>>(world, "tmp/hemisphere-cylinder/fixed/hc_tetra." + std::to_string(iref) + ".lb8.ugrid" );


  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // Sutherland viscosity
  const Real tSuth = 198.6/540;     // R/R
  //const Real tRef  = 491.6/540;     // R/R

  // reference state (freestream)
  const Real Mach = 0.6;
  const Real Reynolds = 0.35e6;
  //const Real Mach = 0.1;
  //const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real MAC = 10;                              // mean aerodynamic chord for moments
  const Real SRef = 5;                              // area scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = Real(aoa)*PI/180.;            // angle of attack (radians)

#if 1
  const Real tRef = 1;          // temperature
  const Real pRef = rhoRef*R*tRef;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;
#endif

#if 0
  const Real qRef = 1;                              // velocity scale
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = 0;
  const Real wRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef + wRef*wRef);

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Entropy );

  // initial condition
  ArrayQ q0, p0;
  pde.setDOFFrom( q0, SAnt3D<DensityVelocityPressure3D<Real>>({rhoRef, uRef, vRef, wRef, pRef, ntRef}) );
  pde.setDOFFrom( p0, SAnt3D<DensityVelocityPressure3D<Real>>({rhoRef, 0, 0, 0, pRef, ntRef}) );

  // DGBR2 discretization
//  Real viscousEtaParameter = 2*Tet::NFace;
  StabilizationMatrix stab(StabilizationType::SUPG, TauType::Glasby);

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict StateVector;
  StateVector[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  StateVector[Conservative3DParams::params.rho] = rhoRef;
  StateVector[Conservative3DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative3DParams::params.rhov] = rhoRef*vRef;
  StateVector[Conservative3DParams::params.rhow] = rhoRef*wRef;
  StateVector[Conservative3DParams::params.rhoE] = rhoRef*ERef;
  StateVector["rhont"]  = rhoRef*ntRef;

  PyDict BCFarField;
  BCFarField[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.StateVector] = StateVector;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.Characteristic] = true;

  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflow[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;


  PyDict PyBCList;
  PyBCList["BCFarField"] = BCFarField;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCOutflow"]  = BCOutflow;
  PyBCList["BCWall"]     = BCWall;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
#if 0
  BCBoundaryGroups["BCFarField"] = {0};
  BCBoundaryGroups["BCSymmetry"] = {1};
  BCBoundaryGroups["BCOutflow"]  = {2};
  BCBoundaryGroups["BCWall"]     = {3,4};
#else
  BCBoundaryGroups["BCFarField"] = {2};
  BCBoundaryGroups["BCSymmetry"] = {3};
  BCBoundaryGroups["BCOutflow"]  = {1};
  BCBoundaryGroups["BCWall"]     = {0};
#endif

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_L2_DOFWeighted;
  std::vector<Real> tol = {1e-10, 1e-10};

#ifndef BOUNDARYOUTPUT
  //Output functional
  Real weight = 1.0;
  WeightFcn weightFcn(weight);
  NDOutputForce fcnOutput(weightFcn);
  OutputIntegrandClass outputIntegrandDrag(fcnOutput, {0});
#else
  // Residual weighted boundary output
  Real dynpRef = 0.5*rhoRef*(uRef*uRef + vRef*vRef + wRef*wRef);

  NDOutputForce outputFcnDrag(pde, cos(aoaRef)/(SRef*dynpRef), 0., sin(aoaRef)/(SRef*dynpRef));
  OutputIntegrandClass outputIntegrandDrag( outputFcnDrag, BCBoundaryGroups["BCWall"] );

  NDOutputForce outputFcnLift(pde, -sin(aoaRef)/(SRef*dynpRef), 0., cos(aoaRef)/(SRef*dynpRef));
  OutputIntegrandClass outputIntegrandLift( outputFcnLift, BCBoundaryGroups["BCWall"] );

  NDOutputMoment outputFcnMoment(pde, 0, 0, 0, 0, 1, 0, MAC);
  OutputIntegrandClass outputIntegrandMoment( outputFcnMoment, BCBoundaryGroups["BCWall"] );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0)
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-8;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-18;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 50;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict), PreconditionerILUAdjoint(PreconditionerILU);

  PreconditionerILUAdjoint[SLA::PreconditionerILUParam::params.FillLevel] = 2;

  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILUAdjoint;

  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;
  PETScDictAdjoint[SLA::PETScSolverParam::params.ResidualHistoryFile] = filename_base + "PETSc_adjoint_residual.dat";

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 3;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
  //NewtonSolverDict[NewtonSolverParam::params.MaxChangeFraction] = 0.1;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 10000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
  NonlinearSolverDict[PseudoTimeParam::params.SteveLogic] = false;
  NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_residual.dat";
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  fstream foutputhist;
  if (world.rank() == 0)
  {
    std::string outputhist_filename = filename_base + "output" + std::to_string(orderMax) + "_X" + std::to_string(iref) + ".adapthist";
    foutputhist.open( outputhist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + outputhist_filename);
  }

  int orderDist = 1;

  PyDict ParamDict;

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Compute distance field
  std::shared_ptr<Field_CG_Cell<PhysD3, TopoD3, Real>>
    pdistfld = std::make_shared<Field_CG_Cell<PhysD3, TopoD3, Real>>(*pxfld, orderDist, BasisFunctionCategory_Lagrange);
  DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCWall"));

  //Solution data
//  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol, pGlobalSolNew;
  std::shared_ptr<SolverInterfaceClass> pInterface;

  int orderMin = 1;

  if (isRestart == true) orderMin = orderMax;

  for (int order = orderMin; order <= orderMax; order++)
  {
    pGlobalSolNew = std::make_shared<SolutionClass>((*pdistfld, *pxfld), pde,  order, order, //+1
                                                    BasisFunctionCategory_Lagrange,
                                                    BasisFunctionCategory_Lagrange,
                                                    active_boundaries, ParamDict, stab );

    // Adjoint equation is integrated with 3 times adjoint order
    const int quadOrder = 2*(order+1);

    //Create solver interface
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, LinearSolverDict,
                                                        outputIntegrandDrag);

    QuadratureOrder quadrule(*pxfld, quadOrder);

    Real outputDragP0 = 0;
    Real outputLiftP0 = 0;
    pGlobalSolNew->setSolution(p0);

    outputLiftP0 = 0;
    pInterface->AlgEqnSet().dispatchBC().dispatchSANSFT(
      FunctionalBoundaryTrace_Dispatch_Galerkin(outputIntegrandLift, pGlobalSolNew->paramfld,
                                                pGlobalSolNew->primal.qfld,
                                             quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputLiftP0 ) );
  #ifdef SANS_MPI
    outputLiftP0 = boost::mpi::all_reduce( *pxfld->comm(), outputLiftP0, std::plus<Real>() );
  #endif

    outputDragP0 = 0;
    pInterface->AlgEqnSet().dispatchBC().dispatchSANSFT(
        FunctionalBoundaryTrace_Dispatch_Galerkin(outputIntegrandDrag, pGlobalSolNew->paramfld,
                                                  pGlobalSolNew->primal.qfld,
                                             quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputDragP0 ) );
  #ifdef SANS_MPI
    outputDragP0 = boost::mpi::all_reduce( *pxfld->comm(), outputDragP0, std::plus<Real>() );
  #endif

    //Set initial solution
    if (isRestart == true)
    {
      std::shared_ptr<XField<PhysD3, TopoD3>> prestartxfld;
      std::shared_ptr<Field_CG_Cell<PhysD3,TopoD3,ArrayQ>> prestartfld;

      std::string file_initial_xfld = restartFileBase + "restartxfld_P" + std::to_string(order) + "_X" + std::to_string(iref + 1) + ".grm";
      std::string file_initial_qfld = restartFileBase + "restartqfld_P" + std::to_string(order) + "_X" + std::to_string(iref + 1) + ".qrm";
      prestartxfld = std::make_shared<XField_PX<PhysD3, TopoD3>>(world, file_initial_xfld);
      prestartfld = std::make_shared<Field_CG_Cell<PhysD3,TopoD3,ArrayQ>>(*prestartxfld, order, BasisFunctionCategory_Lagrange, EmbeddedCGField);

      ReadSolution_PX(file_initial_qfld, *prestartfld);

      pGlobalSolNew->setSolution(q0);
      //prestartfld->projectTo(pGlobalSolNew->primal.qfld);
      //
      ProjectGlobalField(*prestartfld, pGlobalSolNew->primal.qfld);
    }
    else if (order != orderMin)
    {
      pGlobalSolNew->setSolution(*pGlobalSol);
    }
    else
    {
      pGlobalSolNew->setSolution(q0);
    }
    pGlobalSol = pGlobalSolNew;

    //Set initial solution
    std::string qfld_init_filename = filename_base + "qfld_init_a0_P" + std::to_string(order)
                                                   + "_rank" + std::to_string(world.rank()) + ".dat";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();

    //----------//

    Real outputLift = 0;
    pInterface->AlgEqnSet().dispatchBC().dispatchSANSFT(
        FunctionalBoundaryTrace_Dispatch_Galerkin(outputIntegrandLift, pGlobalSol->paramfld,
                                               pGlobalSol->primal.qfld,
                                             quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputLift ) );


#ifdef SANS_MPI
    outputLift = boost::mpi::all_reduce( *pxfld->comm(), outputLift, std::plus<Real>() );
#endif

    Real outputMoment = 0;
    pInterface->AlgEqnSet().dispatchBC().dispatchSANSFT(
        FunctionalBoundaryTrace_Dispatch_Galerkin(outputIntegrandMoment, pGlobalSol->paramfld,
                                               pGlobalSol->primal.qfld,
                                             quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputMoment ) );
#ifdef SANS_MPI
    outputMoment = boost::mpi::all_reduce( *pxfld->comm(), outputMoment, std::plus<Real>() );
#endif

    if (pxfld->comm()->rank() == 0)
    {
      if (order == 0)
        foutputhist << std::setw(5)  << "Iter"
                    << std::setw(5)  << "P"
                    << std::setw(20) << "Lift"
                    << std::setw(20) << "Drag"
                    << std::setw(20) << "Drag P0"
                    << std::setw(20) << "Drag Net"
                    << std::setw(20) << "Moment"
                    << std::endl;

      foutputhist << std::setw(5) << 0
                  << std::setw(5) << order
                  << std::setw(20) << std::setprecision(10) << std::scientific << outputLift - outputLiftP0
                  << std::setw(20) << std::setprecision(10) << std::scientific << pInterface->getOutput()
                  << std::setw(20) << std::setprecision(10) << std::scientific << outputDragP0
                  << std::setw(20) << std::setprecision(10) << std::scientific << pInterface->getOutput() - outputDragP0
                  << std::setw(20) << std::setprecision(10) << std::scientific << outputMoment
                  << std::endl;
    }
    //----------//

    std::string qfld_filename = filename_base + "qfld_a0_P" + std::to_string(order)
                                              + "_rank" + std::to_string(world.rank()) + ".dat";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string restartxfld_filename = filename_base + "restartxfld_P" + std::to_string(order) + "_X" + std::to_string(iref) + ".grm";
    std::string restartqfld_filename = filename_base + "restartqfld_P" + std::to_string(order) + "_X" + std::to_string(iref) + ".qrm";

    pInterface->writeRestart(restartxfld_filename, restartqfld_filename);

    std::string wallfld_filename = filename_base + "wallfld_a0_P" + std::to_string(order)
                                                 + "_rank" + std::to_string(world.rank()) + ".dat";

//    output_Tecplot_RANS_Wall( pde, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld, disc, wallfld_filename );
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
