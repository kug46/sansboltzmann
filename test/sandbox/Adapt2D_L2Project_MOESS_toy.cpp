// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Testing of the MOESS framework with L2-Projection

// #define WHOLEPATCH
//#define INNERPATCH

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <fstream>
#include <typeinfo>

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/FunctionNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "Adaptation/MOESS/SolverInterface_L2Project.h"
#include "Adaptation/MeshAdapter.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"

#include "Field/output_Tecplot.h"
#include "Field/output_grm.h"
#include "Meshing/EPIC/XField_PX.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_L2Project_MOESS_test_suite )

//----------------------------------------------------------------------------//

template< class SolutionExact,  template<class,class,class> class FieldConstructor >
void
adapt( const PyDict& solnArgs,
       const int order, const int power, const int maxIter,
       const string mesher, const bool dumpFields, const string file_tag )
{

  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;

  // DG_element_match, divide the CG dof so that you get the same element count as DG
  // file_tag is the string that comes after the G_ bit and before the _0001000_PX bit.

  const bool use_CG = (typeid(FieldConstructor<PhysDim,TopoDim,Real>) == typeid(Field_CG_Cell<PhysDim,TopoDim,Real>));

  string func;
  if (typeid(SolutionExact) == typeid(SolnNDConvertSpace<PhysD2,ScalarFunction2D_DoubleBL>))
    func = "DoubleBL";
  else if (typeid(SolutionExact) == typeid(SolnNDConvertSpace<PhysD2,ScalarFunction2D_CornerSingularity>))
    func = "Corner";
  else if (typeid(SolutionExact) == typeid(SolnNDConvertSpace<PhysD2,ScalarFunction2D_BoundaryLayer>))
    func = "BL";
  else if (typeid(SolutionExact) == typeid(SolnNDConvertSpace<PhysD2,ScalarFunction2D_PolarGauss>))
    func = "PolarGauss";

  mpi::communicator world;

  typedef SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact, FieldConstructor> SolverInterfaceClass;

  SolutionExact solnExact(solnArgs);

  std::array<Real,1> tol = {{1e-11}};

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;

  if (use_CG)
    MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;

  MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;
  // MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.UniformWeighted;

  PyDict MOESS_DoNothing_Dict(MOESSDict);
  MOESS_DoNothing_Dict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.None;
  // MOESSDict[MOESSParams::params.ImpliedMetric_MaxEval] = (int)1e4;
  // MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  // MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.UniformWeighted;
  // MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.InverseLengthTensor;

  PyDict MesherDict;
  if (mesher == "refine")
  {
    MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.refine;
    MesherDict[refineParams::params.DumpRefineDebugFiles] = false;
#ifdef SANS_REFINE
    MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;
    //MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.system;
#endif
  }
  else if (mesher == "EPIC")
  {
    MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
    std::vector<int> allBC = {0,1,2,3};
    MesherDict[EpicParams::params.SymmetricSurf] = allBC;
    MesherDict[EpicParams::params.nThread] = 1;
  }
  else if (mesher == "fefloa")
  {
    MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;
  }
#ifdef SANS_AVRO
  else if (mesher == "avro")
  {
    MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;
    MesherDict[avroParams::params.Curved] = false;
    MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
    MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files
  }
#endif
  else
    BOOST_REQUIRE_MESSAGE(false, "Unknown mesh generator.");


  //--------ADAPTATION LOOP--------

  timer totalTime;

  // const int maxIter = 50;
  const int nk = pow(2,power);
  int targetCost = 125*nk;

  const int string_pad = 6;
  string int_pad = string(string_pad - std::to_string(targetCost).length(), '0') + std::to_string(targetCost);

  if ( use_CG ) // Modify the dof counts to match the DG element counts - Should be much faster to run and show the same
  {
    Real nDOFperCell_DG = (order+1)*(order+2)/2;
    Real nDOFperCell_CG = nDOFperCell_DG;
    nDOFperCell_CG -= (3 - 1./2); // the node dofs are shared by 6
    nDOFperCell_CG -= (3 - 3./2)* MAX(0,(order - 1 )); // if there are edge dofs they are shared

    targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
  }

  // targetCost *= (609./665); // avro correction for corner at 4000 DOF P1
  // targetCost *= (667./757.); // EPIC correction for corner at 4000 DOF P1

  string filename_base = "tmp/L2Project_2D/" + func + "/" + mesher + "_";

  BasisFunctionCategory basis_category;

  if ( use_CG &&  (typeid(FieldConstructor<PhysDim,TopoDim,Real>) == typeid(Field_CG_Cell<PhysDim,TopoDim,Real>)) )
  {
    filename_base += "CG_";
    basis_category = BasisFunctionCategory_Hierarchical;
  }
  else if (typeid(FieldConstructor<PhysDim,TopoDim,Real>) == typeid(Field_DG_Cell<PhysDim,TopoDim,Real>))
  {
    filename_base += "DG_";
    basis_category = BasisFunctionCategory_Legendre;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Invalid choice of solution field!");

  if (file_tag.size() > 0 && file_tag != "_")
    filename_base += file_tag + "_"; // the additional file name bits kept especially

  filename_base += int_pad + "_P" + std::to_string(order) + "/";

  boost::filesystem::path base_dir(filename_base);
  if ( not boost::filesystem::exists(base_dir) )
    boost::filesystem::create_directories(filename_base);

  string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist, fdummyhist;
  if ( world.rank() == 0 )
  {
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
    fdummyhist.open( filename_base + "dummy.adapthist", fstream::out );
  }

  string output_filename = filename_base + "output.dat";
  fstream foutputhist;
  if ( world.rank() == 0 )
  {
    foutputhist.open( output_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + output_filename);
  }

  if (world.rank() == 0 )
  {
    // write the header to the output file
    foutputhist << "VARIABLES="
                << std::setw(5)  << "\"Iter\""
                << std::setw(10) << "\"DOF\""
                << std::setw(20) << "\"Elements\""
                << std::setw(20) << "\"L<sup>2</sup> Error\""
                << std::endl;

    foutputhist << "ZONE T=\"MOESS " << mesher << " " << nk << "k\"" << std::endl;
  }

  MesherDict[refineParams::params.FilenameBase] = filename_base;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = dumpFields;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpMetric] = true;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpImpliedMetric] = true;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpOptimizationInfo] = false;

  PyDict Adapt_DoNothing_Dict(AdaptDict);
  Adapt_DoNothing_Dict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESS_DoNothing_Dict;
  Adapt_DoNothing_Dict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);
  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(Adapt_DoNothing_Dict);

  MeshAdapter<PhysDim, TopoDim> mesh_adapter(AdaptDict, fadapthist);
  MeshAdapter<PhysDim, TopoDim> mesh_DoNothing_adapter(Adapt_DoNothing_Dict, fdummyhist);

  std::shared_ptr<XField<PhysDim, TopoDim>> pxfld;

#if defined(SANS_AVRO)
  std::shared_ptr<avro::Context> context;
  if (mesher == "avro")
  {
    using avro::coord_t;
    using avro::index_t;

    context = std::make_shared<avro::Context>();

    coord_t number = 2;
    Real xc[3] = {.5,.5,0.};
    std::vector<avro::real> lens(number,1.);
    std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( context.get() , xc , lens[0] , lens[1] );
    std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(),"cube");
    model->addBody(pbody,true);

    try
    {
      std::string xfld_filename = filename_base + "xfld_initial.grm";
      XField_PX<PhysD2,TopoD2> xfld0(world,xfld_filename);
      // copy the mesh into the domain and attach the geometry
      pxfld = std::make_shared< XField_avro<PhysDim,TopoDim> >(xfld0, model);
    }
    catch (...)
    {
      const int ii = 4;
      XField2D_Box_Triangle_Lagrange_X1 xfld0( world, ii, ii );
      // copy the mesh into the domain and attach the geometry
      pxfld = std::make_shared< XField_avro<PhysDim,TopoDim> >(xfld0, model);
    }
  }
  else
#endif
  {
    try
    {
      std::string xfld_filename = filename_base + "xfld_initial.grm";
      pxfld = std::make_shared<XField_PX<PhysD2,TopoD2>>(world,xfld_filename);
    }
    catch (...)
    {
      const int ii = 4;
      pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, ii );
    }
  }

  std::vector<int> cellGroups = {0};
  int iter = 0;
  while (true)
  {
    std::cout << "-----Adaptation Iteration " << iter << "-----" << std::endl;

    FieldConstructor<PhysDim,TopoDim,Real> qfld(*pxfld,order,basis_category);

    const int quadOrder = 2*(order+1);

    //Perform L2 projection from solution on previous mesh
    qfld = 0;

    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(qfld, quadOrder,
                                                        cellGroups, tol, solnExact,
                                                        LinearSolverDict,true);

    pInterface->solveGlobalPrimalProblem();

    if (dumpFields)
    {
      std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
      std::string xfld_filename = filename_base + "xfld_a" + std::to_string(iter+1) + ".grm";

      output_Tecplot( qfld, qfld_filename );
      output_grm( *pxfld, xfld_filename);
    }
    else if ( iter == maxIter )
    {
      std::string qfld_filename = filename_base + "qfld_final.plt";
      output_Tecplot( qfld, qfld_filename );

      std::string xfld_filename = filename_base + "xfld_final.grm";
      output_grm( *pxfld, xfld_filename);
    }

    if (dumpFields)
    {
      std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
      output_Tecplot( qfld, qfld_filename );
    }

#ifdef SANS_MPI
    int nDOFtotal = 0;
    boost::mpi::reduce(*pxfld->comm(), qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );

    // count the number of elements possessed by this processor
    int nElem = 0;
    for (int elem = 0; elem < pxfld->nElem(); elem++ )
      if (pxfld->getCellGroupGlobal<Triangle>(0).associativity(elem).rank() == world.rank())
        nElem++;

    int nElemtotal = 0;
    boost::mpi::reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>(), 0 );
#else
    int nDOFtotal = qfld.nDOFpossessed();
    int nElemtotal = pxfld->nElem();
#endif

    //Compute error estimates
    pInterface->computeErrorEstimates();

    Real L2error = pInterface->getOutput();

    // if (dumpFields)
    // {
    //   std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".plt";
    //   pInterface->output_EField(efld_filename);
    // }

    if (world.rank() == 0 )
    {
      foutputhist << std::setw(5) << iter
                  << std::setw(10) << nDOFtotal
                  << std::setw(10) << nElemtotal
                  << std::setw(20) << std::setprecision(10) << std::scientific << L2error
                  << std::endl;
    }
    if ( iter == maxIter )
    {
      if (func == "Corner")
      {
        std::cout << "Writing element distribution to file" << std::endl;
        //Writing out element distribution...
        typedef PhysD2 PhysDim;
        typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

        typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
        typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
        typedef ElementXFieldClass::VectorX ArrayX;

        XFieldCellGroupType xfldCellGroup = pxfld->getCellGroup<Triangle>(0);
        ElementXFieldClass xfldElem( xfldCellGroup.basis() );

        string distfile = filename_base + "h_distribution.txt";
        fstream fdist( distfile, fstream::out );

        for (int elem = 0; elem < xfldCellGroup.nElem(); elem++)
        {
          xfldCellGroup.getElement(xfldElem, elem);

          ArrayX centroid;
          xfldElem.coordinates(Triangle::centerRef, centroid);

          Real r = sqrt(centroid[0]*centroid[0] + centroid[1]*centroid[1]);

          MatrixSym M = 0;
          xfldElem.impliedMetric(M);

          Real h = pow(DLA::Det(M),-0.25);

          fdist << r << "," << h << std::endl;
        }
        fdist.close();
      }


      if (func == "BL")
      {
        std::cout << "Writing element and aspect Ratio distribution to file" << std::endl;
        //Writing out element distribution...
        typedef PhysD2 PhysDim;
        typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

        typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
        typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
        typedef ElementXFieldClass::VectorX ArrayX;

        XFieldCellGroupType xfldCellGroup = pxfld->getCellGroup<Triangle>(0);
        ElementXFieldClass xfldElem( xfldCellGroup.basis() );

        string distfile = filename_base + "h_distribution.txt";
        string ARfile = filename_base + "AR_distribution.txt";
        fstream fdist( distfile, fstream::out );
        fstream fAR( ARfile, fstream::out );

        for (int elem = 0; elem < xfldCellGroup.nElem(); elem++)
        {
          xfldCellGroup.getElement(xfldElem, elem);

          ArrayX centroid;
          xfldElem.coordinates(Triangle::centerRef, centroid);

          // Real r = sqrt(centroid[0]*centroid[0] + centroid[1]*centroid[1]);
          Real x = centroid[0];

          MatrixSym M = 0;
          xfldElem.impliedMetric(M);

          Real h1 = pow(M(0,0),-0.5);
          Real h2 = pow(M(1,1),-0.5);
          Real AR = h2/h1;

          fdist << x << "," << h1 << std::endl;
          fAR   << x << "," << AR << std::endl;
        }
        fdist.close();
      }


      break;
    }

    //Perform local sampling and adapt mesh
    pxfld = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    // std::shared_ptr<SolverInterfaceClass> pInterfaceSmooth;
    // FieldConstructor<PhysDim,TopoDim,Real> qfldSmooth(*pxfld,order,basis_category);
    // qfldSmooth = 0;
    // pInterfaceSmooth = std::make_shared<SolverInterfaceClass>(qfldSmooth, quadOrder,
    //                                                           cellGroups, tol, solnExact,
    //                                                           LinearSolverDict,true);
    //
    // pInterfaceSmooth->solveGlobalPrimalProblem();
    // pInterfaceSmooth->computeErrorEstimates();
    // //Perform local sampling and adapt mesh -- SMOOTH OUT THE Metric
    // pxfld = mesh_DoNothing_adapter.adapt(*pxfld, cellGroups, *pInterfaceSmooth, iter);


    iter++;
  }
  if (world.rank() == 0)
    fadapthist << "\n\nTotal Time elapsed: " << totalTime.elapsed() << "s" << std::endl;

  fadapthist.close();
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_L2_test )
{
  // default parameters

  string func = "PolarGauss";
  string mesher = "avro";
  string file_tag = "";
  bool use_CG = true;
  bool dumpFields = false;
  int orderL = 1, orderH = 3;
  int powerL = 0, powerH = 7;
  int maxIter = 50;

  // read command-line args if any
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc >= 2)
    func = string(argv[1]);
  if (argc >= 3)
    mesher = string(argv[2]);
  if (argc >= 4)
    file_tag = string(argv[3]);
  if (argc >= 5)
    use_CG = std::stoi(argv[4]);
  if (argc >= 6)
    dumpFields = std::stoi(argv[5]);
  if (argc >= 7)
    orderL = orderH = std::stoi(argv[6]);
  if (argc >= 8)
    powerL = powerH = std::stoi(argv[7]);
  if (argc >= 9)
    maxIter = std::stoi(argv[8]);

  if (file_tag == "none" )
    file_tag = "";

  std::cout << "func: " << func << ", mesher: " << mesher << ", file_tag: " << file_tag << ", disc: ";
  if (use_CG)
    std::cout << "CG, ";
  else
    std::cout << "DG, ";
  std::cout << "dumpFields: " << dumpFields << ", orderL,H: " << orderL << "," << orderH << ", powerL,H: " << powerL << "," << powerH << std::endl;

  for (int order = orderL; order <= orderH; order++)
    for (int power = powerH; power >= powerL; power--)
    {
      // set up the dictionary based on the params
      if (func=="DoubleBL")
      {
        PyDict solnArgs;
        typedef SolnNDConvertSpace<PhysD2,ScalarFunction2D_DoubleBL> SolutionExact;
        solnArgs[SolutionExact::ParamsType::params.a] = 3.0/5.0;
        solnArgs[SolutionExact::ParamsType::params.b] = 4.0/5.0;
        solnArgs[SolutionExact::ParamsType::params.nu] = 1.0/50.0;
        solnArgs[SolutionExact::ParamsType::params.offset] = 1.0;
        solnArgs[SolutionExact::ParamsType::params.scale] = -1.0;

        if (use_CG)
        {
          adapt<SolutionExact,Field_CG_Cell>(solnArgs,order,power,maxIter,mesher,dumpFields,file_tag);
        }
        else
        {
          adapt<SolutionExact,Field_DG_Cell>(solnArgs,order,power,maxIter,mesher,dumpFields,file_tag);
        }
      }
      else if (func=="Corner")
      {
        PyDict solnArgs;
        typedef SolnNDConvertSpace<PhysD2,ScalarFunction2D_CornerSingularity> SolutionExact;
        solnArgs[SolutionExact::ParamsType::params.alpha] = 2.0/3.0;
        solnArgs[SolutionExact::ParamsType::params.theta0] = PI/2;

        if (use_CG)
        {
          adapt<SolutionExact,Field_CG_Cell>(solnArgs,order,power,maxIter,mesher,dumpFields,file_tag);
        }
        else
        {
          adapt<SolutionExact,Field_DG_Cell>(solnArgs,order,power,maxIter,mesher,dumpFields,file_tag);
        }
      }
      else if (func=="BL")
      {
        PyDict solnArgs;
        typedef SolnNDConvertSpace<PhysD2,ScalarFunction2D_BoundaryLayer> SolutionExact;
        solnArgs[SolutionExact::ParamsType::params.epsilon] = 0.01;
        solnArgs[SolutionExact::ParamsType::params.beta] = pow(2,orderL+1);
        solnArgs[SolutionExact::ParamsType::params.p] = orderL;
        BOOST_REQUIRE_MESSAGE(orderL == orderH, "Need to run boundary layer case with fixed polynomial order");

        if (use_CG)
        {
          adapt<SolutionExact,Field_CG_Cell>(solnArgs,order,power,maxIter,mesher,dumpFields,file_tag);
        }
        else
        {
          adapt<SolutionExact,Field_DG_Cell>(solnArgs,order,power,maxIter,mesher,dumpFields,file_tag);
        }
      }
      else if (func=="PolarGauss")
      {
        PyDict solnArgs;
        typedef SolnNDConvertSpace<PhysD2,ScalarFunction2D_PolarGauss> SolutionExact;
        solnArgs[SolutionExact::ParamsType::params.k1] = 1000;
        solnArgs[SolutionExact::ParamsType::params.r0] = 0.5;

        if (use_CG)
        {
          adapt<SolutionExact,Field_CG_Cell>(solnArgs,order,power,maxIter,mesher,dumpFields,file_tag);
        }
        else
        {
          adapt<SolutionExact,Field_DG_Cell>(solnArgs,order,power,maxIter,mesher,dumpFields,file_tag);
        }
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION("bad function");
      }
    }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
