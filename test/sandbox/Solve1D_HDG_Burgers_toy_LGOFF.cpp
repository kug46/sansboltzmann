// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_HDG_Burgers_toy
// testing of 1-D HDG with Burgers

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_HDG_Burgers_toy_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_HDG_Burgers1D )
{

  typedef PDEAdvectionDiffusion<PhysD1,
                                LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> NDPDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  typedef ScalarFunction1D_Tanh SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCAdvectionDiffusion<PhysD1,BCType> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  typedef BCAdvectionDiffusion1DVector<LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> AlgebraicEquationSet_HDGClass;
  typedef AlgebraicEquationSet_HDGClass::BCParams BCParams;

  Real nu = 0.005;
  Real b =  0.2;

  ScalarFunction1D_Tanh soln(nu);
  NDSolutionExact solnExact(nu);

  Real param = 1.0;
  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Const> ICType3;
  ICType3 IC3(param);

  // PDE
  LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers> adv;
  ViscousFlux1D_Uniform visc(nu);
  ScalarFunction1D_Tanh MMS_soln(nu);
//  ForcingFunction1D_MMS<ScalarFunction1D_Tanh> forcing(MMS_soln);
  Source1D_Uniform source(b);


  typedef ForcingFunction1D_MMS<PDEAdvectionDiffusion1D, ScalarFunction1D_Tanh> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(MMS_soln) );

  NDPDEClass pde( adv, visc, source, forcingptr ); // solution as argument means a forcing function is generated

  // BC
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(nu) ) ;
  BCClass bc( solnExactPtr, visc );
  // Create a Solution dictionary
  PyDict Tanh;
  Tanh[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Tanh;
  Tanh[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = Tanh;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef AlgebraicEquationSet_HDGClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_HDGClass::SystemVector SystemVectorClass;
  typedef AlgebraicEquationSet_HDGClass::SystemNonZeroPattern SystemNonZeroPattern;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  typedef IntegrandCell_HDG<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, BCClass, BCClass::Category, HDG> IntegrandBoundClass;
  typedef IntegrandCell_SquareError<NDPDEClass,NDSolutionExact> IntegrandSquareErrorClass;

  const std::vector<int> BoundaryGroups = {0,1};

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnInt( pde, disc, {0} );
  IntegrandBoundClass fcnBC( pde, bc, BoundaryGroups, disc );
  IntegrandSquareErrorClass fcnErr( solnExact, {0} );

  int order = 0;
  // loop over grid resolution: 2^power
  int ii  = 65;

  // grid:
  XField1D xfld( ii, -1, 1 );

  // HDG solution field
  // cell solution
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufld(xfld, order, BasisFunctionCategory_Legendre);
  // auxiliary variable
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  // interface solution
  Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> uIfld(xfld, order, BasisFunctionCategory_Legendre);
  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroups);

  const int nDOFPDE = ufld.nDOF();
  const int nDOFAu  = qfld.nDOF();
  const int nDOFInt = uIfld.nDOF();
  const int nDOFBC  = lgfld.nDOF();

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-12, 1e-12, 1e-12, 1e-12};

  AlgebraicEquationSet_HDGClass AlgEqSet(xfld, ufld, qfld, uIfld, lgfld, pde, disc, quadratureOrder, tol,
                                         {0}, {0}, PyBCList, BCBoundaryGroups );

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, ScalarGoldenSearchLineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  ScalarGoldenSearchLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] =
      LineUpdateParam::params.LineUpdate.ScalarGoldenSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = ScalarGoldenSearchLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);
  // Create the solver object
  NewtonSolver<SystemMatrixClass> Solver( AlgEqSet, NewtonSolverDict );
  typedef NewtonSolver<SystemMatrixClass>::SolveStatus SolveStatus;

  // residual vectors
  SystemVectorClass q0(AlgEqSet.vectorStateSize());
  SystemVectorClass sln(AlgEqSet.vectorStateSize());
  SystemVectorClass rsd(sln.size());

  // set initial condition
  q0 = 0.0;
  sln = 0.0;

  for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(IC3, {0}), (xfld, ufld) );
  // Solve
  sln = q0;

#ifdef SANS_VERBOSE
  // Tecplot dump
  string filename4 = "tmp/icHDG_P";
  filename4 += to_string(order);
  filename4 += "_";
  filename4 += to_string(ii);
  filename4 += ".plt";
  output_Tecplot( ufld, filename4 );
#endif

  // updated solution
  for (int k = 0; k < nDOFPDE; k++)
    sln[0][k] = ufld.DOF(k);

  for (int k = 0; k < nDOFPDE; k++)
    for (int d = 0; d < PhysD1::D; d++)
      sln[1][PhysD1::D*k+d] = qfld.DOF(k)[d] ;

  for (int k = 0; k < nDOFInt; k++)
    sln[2][k] = uIfld.DOF(k);

  for (int k = 0; k < nDOFBC; k++)
    sln[3][k] = lgfld.DOF(k);

  q0 = sln;

  // Get initial residual
  SystemVectorClass rsdInit(AlgEqSet.vectorEqSize());
  rsdInit = 0.0;
  AlgEqSet.residual(q0, rsdInit);
  Real nrmRSDInit = norm(rsdInit, 2);

  // Solve
  SolveStatus status = Solver.solve(q0, sln);
  BOOST_CHECK( status.converged );

  AlgEqSet.setSolutionField(sln);

  // check that the residual is zero
  Real rsdPDEnrm = 0;
  for (int n = 0; n < nDOFPDE; n++)
    rsdPDEnrm += pow(rsd[0][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1.1e-12 );

  Real rsdAunrm = 0;
  for (int n = 0; n < nDOFAu; n++)
    rsdAunrm += pow(rsd[1][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdAunrm), 1.1e-12 );

  Real rsdIntnrm = 0;
  for (int n = 0; n < nDOFInt; n++)
    rsdIntnrm += pow(rsd[2][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdIntnrm), 1.1e-12 );

  Real rsdBCnrm = 0;
  for (int n = 0; n < nDOFBC; n++)
    rsdBCnrm += pow(rsd[3][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1.1e-12 );

  if (status.converged)
  {
    cout << "Converged   ";
  }
  else
  {
    cout << "Unconverged ";
  }

  // Get final residual
  SystemVectorClass rsdFinal(AlgEqSet.vectorEqSize());
  rsdFinal = 0.0;
  AlgEqSet.residual(sln, rsdFinal);
  Real nrmRSDFinal = norm(rsdFinal, 2);
  std::cout << " " << order << "  " << ii << " " << nrmRSDInit << " " << nrmRSDFinal << std::endl;

#ifdef SANS_VERBOSE
  // Tecplot dump
  string filename = "tmp/slnHDG_P";
  filename += to_string(order);
  filename += "_";
  filename += to_string(ii);
  filename += ".plt";
  output_Tecplot( ufld, filename );
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
