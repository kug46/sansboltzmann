// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_VMSD_HOW5_VI2_toy
// Trying to build the HOW5 VI2 case


//#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <chrono>
#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Output_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_WeightedResidual_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "Discretization/isValidState/SetValidStateCell.h"

#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "Discretization/VMSD/IntegrandCell_VMSD.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_OutputWeightRsd_VMSD.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_Flux_mitState_VMSD.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"
#include "Adaptation/MOESS/MOESSParams.h"
#include "Adaptation/MeshAdapter.h"
#include "Field/Function/FunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot_PDE.h"
#include "Field/HField/GenHFieldArea_CG.h"
#include "Field/HField/GenHFieldArea_DG.h"

#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_VMSD_HOW5_VI2_toy )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_VMSD_HOW5_VI2_toy )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;
//  typedef QTypeEntropy QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up integrand for stagnation pressure
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputEntropyErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquaredClass> NDOutputEntropyErrorSquaredClass;
  typedef IntegrandCell_VMSD_Output<NDOutputEntropyErrorSquaredClass, NDPDEClass> OutputIntegrandClass;
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef ParamType_None ParamBuilderType;
  typedef SolutionData_VMSD<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // Used to compute the volume of the domain
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Const> NDScalarConst;

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2_DOFWeighted;
  std::vector<Real> tol = {1.0e-10, 1.0e-10, 1.0e-10};

  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler PDE sans AV
  ////////////////////////////////////////////////////////////////////////////////////////
  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R = 1;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Define boundary conditions - P0
  ////////////////////////////////////////////////////////////////////////////////////////
  // reference state (freestream)
  const Real Mach = 0.5;
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 0.0 * PI / 180.0;                          // angle of attack (radians)

  const Real pRef = 1.0;
  const Real tRef = pRef/rhoRef/R;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  // Lets calculate our inflow enthalpy since this should be constant
  const Real inflowEntropy = log( pRef / pow(rhoRef,gamma) );
  const Real TtSpec = tRef * (1 + (gamma-1)/2 * Mach*Mach);
  const Real PtSpec = pRef * pow(TtSpec/ tRef, gamma/(gamma-1));

  std::cout << "rho:  " << rhoRef << "[kg/m3]" << std::endl
            << "u:    " << uRef   << "[m/s]"   << std::endl
            << "v:    " << vRef   << "[m/s]"   << std::endl
            << "T:    " << tRef   << "[K]"     << std::endl
            << "Tt:   " << TtSpec << "[K]"     << std::endl
            << "Pt:   " << PtSpec << "[K]"     << std::endl
            << "sRef: " << inflowEntropy << "[-]"     << std::endl;

  // Define inflow BC
  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
  BCInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
  BCInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aoaRef;

  // Define wall BC
  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  // Define outflow BC
  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflow[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
//  BCOutflow[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = 0.9 * pRef;

  // Define BC list
  PyDict PyBCList;
  PyBCList["FarfieldOutflow"] = BCOutflow;
  PyBCList["Wall"] = BCWall;
  PyBCList["FarfieldInflow"] = BCInflow;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["FarfieldInflow"] = {0};
  BCBoundaryGroups["FarfieldOutflow"] = {1};
  BCBoundaryGroups["Wall"] = {2, 3};
  ////////////////////////////////////////////////////////////////////////////////////////

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;
  PyDict ParamDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-6;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] =  true;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type] =
                  SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver]    = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLDecreaseFactor]  = 0.1;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor]  = 2;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create tecplot file to put total enthalpy errors
////////////////////////////////////////////////////////////////////////////////////////
  std::string filename_base = "tmp/HOW5_VI2/NoAV_NL/";
  boost::filesystem::create_directories(filename_base);
  std::ofstream resultFile(filename_base + "Entropy.plt", std::ios::out);
  resultFile << "VARIABLES=";
  resultFile << "\"Order\"";
  resultFile << "\"DOF\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"Solve Time /ms\"";
  resultFile << std::endl;
  resultFile << std::setprecision(16) << std::scientific;
  ////////////////////////////////////////////////////////////////////////////////////////

  //--------ADAPTATION LOOP--------
  for (int grid_index = 0; grid_index <= 4; grid_index++)
  {
    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directory(base_dir);

    NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_residual"
        + std::to_string(grid_index) + ".dat";

    // Grid
    std::string meshName = "grids/HOW5_VI2/SmoothBump_tri_ref" + std::to_string(grid_index) + "_Q4.grm";
    if (world.rank() == 0) std::cout << "Opening: " << meshName << std::endl;
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfld =
        std::make_shared<XField_PX<PhysD2,TopoD2>>(world, meshName);
    if (world.rank() == 0) std::cout << "Grid opened" << std::endl;

    std::vector<int> cellGroups;
    for ( int i = 0; i < pxfld->nCellGroups(); i++)
      cellGroups.push_back(i);
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Check the BC dictionary
    BCParams::checkInputs(PyBCList);

    std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

    //Solution data
    typedef SolutionData_VMSD<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

    std::shared_ptr<SolutionClass> pGlobalSol;
    std::shared_ptr<SolutionClass> pGlobalSolOld;
    std::shared_ptr<SolverInterfaceClass> pInterface;

    for (int order = 1; order <= 1; order++)
    {
      for (int porder = order; porder <= order; porder++)
      {
        NDPDEClass pde(gas, interp, eHarten);

        DiscretizationVMSD stab;
        stab.setNitscheOrder(order);

        NDOutputEntropyErrorSquaredClass fcnOutput( pde, 1.0 );
        OutputIntegrandClass outputIntegrand( pde, fcnOutput, cellGroups );

        const int quadOrder = 3*(order + 2)-1; // 14 at order=3

//        const int porder = order;
        //Solution data
        pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, stab,
                                                     order, porder, order+1, porder,
                                                     BasisFunctionCategory_Lagrange,
                                                     BasisFunctionCategory_Lagrange,
                                                     BasisFunctionCategory_Lagrange,
                                                     active_boundaries, ParamDict);

        //Create solver interface
        std::shared_ptr<SolverInterfaceClass> pInterface;
        pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                            cellGroups, interiorTraceGroups,
                                                            PyBCList, BCBoundaryGroups,
                                                            SolverContinuationDict, LinearSolverDict,
                                                            outputIntegrand);

        //Set initial solution
        if (order == 1)
        {
          // initial condition
          ArrayQ q0 = pde.setDOFFrom( DensityVelocityTemperature2D<Real>({rhoRef, uRef, vRef, tRef}) );

          pGlobalSol->setSolution(q0);
        }
        else
        {
          pGlobalSol->setSolution(*pGlobalSolOld);
        }

        pGlobalSolOld = pGlobalSol;

        auto t1 = std::chrono::high_resolution_clock::now();
        pInterface->solveGlobalPrimalProblem();
        auto t2 = std::chrono::high_resolution_clock::now();
        std::cout << "P solve: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << "ms" << std::endl;

        {
          string filename = "HOW5_VI2_Primal_"
                         + Type2String<QType>::str()
                         + "_P"
                         + std::to_string(order)
                         + "_Pp"
                         + std::to_string(porder)
                         + "_G"
                         + std::to_string(grid_index)
                         + ".dat";
          output_Tecplot( pde, *pxfld, pGlobalSol->primal.qfld, filename_base + filename );
        }

        {
          ////////////////////////////////////////////////////////////////////////////////////////
          // \bar{q} + \hat{q}
          ////////////////////////////////////////////////////////////////////////////////////////
          int orderCombined = std::max(order,porder);
          Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldCombined(*pxfld, orderCombined, BasisFunctionCategory_Lagrange);
          Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qpfld_temp(*pxfld, orderCombined, BasisFunctionCategory_Lagrange);
          pGlobalSol->primal.qfld.projectTo(qfldCombined);
          pGlobalSol->primal.qpfld.projectTo(qpfld_temp);
          for (int i = 0; i < qfldCombined.nDOF(); i++)
          {
            qfldCombined.DOF(i) += qpfld_temp.DOF(i);
          }
          ////////////////////////////////////////////////////////////////////////////////////////
          string filename = "HOW5_VI2_Combined_"
                         + Type2String<QType>::str()
                         + "_P"
                         + std::to_string(order)
                         + "_Pp"
                         + std::to_string(porder)
                         + "_G"
                         + std::to_string(grid_index)
                         + ".dat";
          output_Tecplot( pde, *pxfld, qfldCombined, filename_base + filename );
        }

        {
          string filename = "HOW5_VI2_Perturbation_"
                         + Type2String<QType>::str()
                         + "_P"
                         + std::to_string(order)
                         + "_Pp"
                         + std::to_string(porder)
                         + "_G"
                         + std::to_string(grid_index)
                         + ".dat";
          output_Tecplot( pGlobalSol->primal.qpfld, filename_base + filename );
        }

        ////////////////////////////////////////////////////////////////////////////////////////
        // Calculate the volume of the domain
        ////////////////////////////////////////////////////////////////////////////////////////
        NDScalarConst constFcn(1);

        Real localDomainArea = 0;
        Real domainArea = 0;
        for_each_CellGroup<TopoD2>::apply( FunctionIntegral(constFcn, localDomainArea, world.rank(), cellGroups),
                                           *pxfld );
#ifdef SANS_MPI
        boost::mpi::reduce(world, localDomainArea, domainArea, std::plus<Real>(), 0);
#else
        domainArea = localDomainArea;
#endif
        ////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // Calculate Enthalpy error
        ////////////////////////////////////////////////////////////////////////////////////////
        QuadratureOrder quadratureOrder( *pxfld, -1 );
        NDOutputEntropyErrorSquaredClass outEnthalpyError( pde, 1.0 );
        OutputIntegrandClass fcnErr( pde, outEnthalpyError, cellGroups );

        Real EntropySquareError = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_VMSD( fcnErr, EntropySquareError ),
            *pxfld, (pGlobalSol->primal.qfld, pGlobalSol->primal.qpfld),
            quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
        std::cout << "RMS Entropy Error: " << sqrt(EntropySquareError/domainArea) << std::endl;
        ////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // Output Error to tecplot file
        ////////////////////////////////////////////////////////////////////////////////////////
        resultFile << " " << order;
        resultFile << " " << pGlobalSol->primal.qfld.nDOF();
        resultFile << " " << 1.0/sqrt(pGlobalSol->primal.qfld.nDOF());
        resultFile << " " << sqrt(EntropySquareError/domainArea) / (cRef*cRef);
        resultFile << " " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
        resultFile << std::endl;
        ////////////////////////////////////////////////////////////////////////////////////////
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
