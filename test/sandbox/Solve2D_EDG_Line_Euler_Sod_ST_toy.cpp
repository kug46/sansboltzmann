// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_EDG_Line_Euler_Sod_ST_toy
// 2D+1 Riemann problem

#undef SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"

#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/SolutionFunction_Euler2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime2D.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE // HACK: I need to work out how to sort out homotopy based stuff
#include "Discretization/HDG/AlgebraicEquationSet_HDG_impl.h"

#include "pde/BCParameters.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_CG_Trace.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectConstCell.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

namespace SANS
{
template<> struct Type2String<QTypeEntropy>              { static std::string str() { return "Entropy";      } };
template<> struct Type2String<QTypeConservative>         { static std::string str() { return "Conservative"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure> { static std::string str() { return "Primitive";    } };
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_EDG_Line_Euler_Sod_test_suite )

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( Solve1D_EDG_Line_Euler_Sod )
{
//  typedef QTypeEntropy QType;
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpaceTime<PhysD2, PDEClass> NDPDEClass;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef SolutionFunction_Euler2D_Riemann<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD2, SolutionClass> SolutionNDClass;

  typedef BCTypeFunction_mitState BCInflowIC;
  typedef BCEuler2D<BCInflowIC, PDEClass> BCSolnClass;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> PrimalEquationSetClass;

  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // global communicator
  mpi::communicator world;

  // Gas Model
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);
  // PDE
  EulerResidualInterpCategory interp = Euler_ResidInterp_Momentum;
  NDPDEClass pde(gas, interp);

  // Set up IC with Sod's shock tube problem
  //  See: "Riemann Solvers and Numerical Methods for Fluid Dynamics" - Toro (Section 4.3.3 Table 4.1 in my copy)
  PyDict solnArgs;
  solnArgs[BCSolnClass::ParamsType::params.Function.Name] = BCSolnClass::ParamsType::params.Function.Riemann;
  solnArgs[SolutionClass::ParamsType::params.interface] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.rhoL] = 1.0;
  solnArgs[SolutionClass::ParamsType::params.uL] = 0.0;
  solnArgs[SolutionClass::ParamsType::params.vL] = 0.0;
  solnArgs[SolutionClass::ParamsType::params.pL] = 1.0;
  solnArgs[SolutionClass::ParamsType::params.rhoR] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.uR] = 0.0;
  solnArgs[SolutionClass::ParamsType::params.vR] = 0.0;
  solnArgs[SolutionClass::ParamsType::params.pR] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionNDClass solnExact(solnArgs);
  Real Tend = 0.25;

  // BC
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCReflect;
  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCTypeSpaceTimeInitialCondition;
  BCTypeSpaceTimeInitialCondition[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCTypeSpaceTimeInitialCondition[BCSolnClass::ParamsType::params.Function] = solnArgs;
  BCTypeSpaceTimeInitialCondition[BCSolnClass::ParamsType::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCNone"] = BCNone;
  PyBCList["BCReflect"] = BCReflect;
  PyBCList["BCIn"] = BCTypeSpaceTimeInitialCondition;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNone"] = {5}; //t-max
  BCBoundaryGroups["BCReflect"] = {0,1,2,3}; //x-min, x-max, y-min, y-max
  BCBoundaryGroups["BCIn"] = {4}; //t-min

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

//  bool converged = false;

  int ii = 100; //grid size - using timesteps = grid size
  int jj = 10;
  int tt = 100;
  int order = 1;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, tt, 0, 1, 0, 1, 0, Tend );

  // DG solution field
//  ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(1.1, 0, 0, 1.1) );
  ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(0.25, 0, 0, 0.6) );
  // cell solution
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld = q0;
  //for_each_CellGroup<TopoD3>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );

  Field_DG_Cell<PhysD3, TopoD3, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Hierarchical);
  afld = 0;

  Field_CG_Trace<PhysD3, TopoD3, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qIfld = q0;

  // lagrange multipliers not actually used either
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ>
    lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

#if 0
  // Tecplot dump grid
  string filename2 = "tmp/icEDG_ST_EulerSod_P";
  filename2 += to_string(order);
  filename2 += "_";
  filename2 += to_string(ii);
  filename2 += "_";
  filename2 += to_string(tt);
  filename2 += ".plt";
  output_Tecplot( qfld, filename2 );
#endif

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                     {0}, {0,1,2,3}, PyBCList, BCBoundaryGroups);

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1.0e-12;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);
  NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

  SystemVectorClass ini(PrimalEqSet.vectorStateSize());
  SystemVectorClass sln(PrimalEqSet.vectorStateSize());

  PrimalEqSet.fillSystemVector(ini);
  sln = ini;

  Solver.solve(ini,sln);
//  BOOST_CHECK(converged);
  PrimalEqSet.setSolutionField(sln);

#if 1
  cout << std::setprecision(16) << std::scientific;
  cout << "P = " << order << " ii = " << " jj = " << jj << " tt = " << tt;// << ": L2 solution error = " << sqrt( EntropySquareError );
  cout << endl;
#endif
#if 1
  // Tecplot dump grid
  string filename = "tmp/slnEDG_ST_EulerSod";
  filename += Type2String<QType>::str();
  filename += "_P";
  filename += to_string(order);
  filename += "_";
  filename += to_string(ii);
  filename += "_";
  filename += to_string(jj);
  filename += "_";
  filename += to_string(tt);
  filename += ".plt";
  output_Tecplot( qfld, filename );
#endif
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
