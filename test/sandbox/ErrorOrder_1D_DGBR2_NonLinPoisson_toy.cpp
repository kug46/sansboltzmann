// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_DGBR2_NonLinPoisson_btest
// testing of 1-D DG with a non-linear poisson problem
//
// The exact solution is:
//
// Q(x) = 1/lam * ln( (A + B*x)/ D)
//
// where A, B, and D are arbitrary coefficients determined completely by the BC's.

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Meshing/XField1D/XField1D.h"

// Since this is the only place where the Non-Linear Poisson problem is solved, just instantiate here
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder1D_DGBR2_NonLinPoisson_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorOrder1D_DGBR2_NonLinPoisson )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_None,
                                ViscousFlux1D_Exp,
                                Source1D_None > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ScalarFunction1D_NonLinPoisson SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_None, ViscousFlux1D_Exp> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  //typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef OutputCell_Solution<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  // PDE
  AdvectiveFlux1D_None adv;

  Real lam = 1;

  ViscousFlux1D_Exp visc( lam );

  Source1D_None source;

  Real A = 1;
  Real B = 50;
  Real D = 1;

  //Exact solution
  NDSolutionExact solnExact(lam, A, B, D);

  NDPDEClass pde(adv, visc, source);

  // BC
  PyDict NonLinPoisson;
  NonLinPoisson[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
                BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.NonLinPoisson;
  NonLinPoisson[SolutionExact::ParamsType::params.lam] = lam;
  NonLinPoisson[SolutionExact::ParamsType::params.A] = A;
  NonLinPoisson[SolutionExact::ParamsType::params.B] = B;
  NonLinPoisson[SolutionExact::ParamsType::params.D] = D;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function] = NonLinPoisson;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
      BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;

  // integrands
  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // integrands
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});


  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-18;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 60;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = true;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_1D_DGBR2.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_DGBR2_NonLinPoisson_FullTest.txt", 1e-9, 1e-9, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_DGBR2_NonLinPoisson_MinTest.txt", 1e-9, 1e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "Running full test..." << endl;
#endif
  int ordermax = 7;
#else
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii;
    int powermin = 4;
#ifdef SANS_FULLTEST
    int powermax = 7;
#else
    int powermax = 7;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );

      // grid:
      XField1D xfld( ii );

      // solution: Hierarchical, C0
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      qfld = 3.5;

      const int nDOFPDE = qfld.nDOF();

      // lifting operators
      FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      rfld = 0;

      // Lagrange multiplier: Hierarchical, C0 (also at corners)

#if 1
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#elif 1
      std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#else
      QField2D_DG_BoundaryEdge<NDPDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#endif

      lgfld = 0;

      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-11, 1e-11};
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

      // Create the solver object
      NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );

      // residual
      SystemVectorClass q0(PrimalEqSet.vectorStateSize());

      PrimalEqSet.fillSystemVector(q0);

#if 0
      // jacobian nonzero pattern
      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
      PrimalEqSet.jacobian(nz);

      // jacobian
      SystemMatrixClass jac(nz);
      PrimalEqSet.jacobian(jac);

      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#if 0
      fstream fout00( "tmp/jac00.mtx", fstream::out );
      cout << "btest: global jac00" << endl;  WriteMatrixMarketFile( jac(0,0), fout00 );
      fstream fout10( "tmp/jac10.mtx", fstream::out );
      cout << "btest: global jac10" << endl;  WriteMatrixMarketFile( jac(1,0), fout10 );
      fstream fout01( "tmp/jac01.mtx", fstream::out );
      cout << "btest: global jac01" << endl;  WriteMatrixMarketFile( jac(0,1), fout01 );
#endif
#endif


      // nonlinear solve
      SystemVectorClass q(q0.size());
      SolveStatus status = nonlinear_solver.solve(q0, q);
      BOOST_REQUIRE( status.converged );

      // L2 solution error
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_DGBR2( errorIntegrand, SquareError ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

#if 1
      SystemVectorClass rhs(q.size());
      rhs = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell_DGBR2<SurrealClass>( outputIntegrand, rhs(PrimalEqSet.iPDE) ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);

      SystemVectorClass adj(q.size());

      solverAdj.solve(rhs, adj);

      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> mufld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

      PrimalEqSet.setAdjointField(adj, wfld, sfld, mufld);

      // Tecplot dump
      string filename = "tmp/adjDG_P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( wfld, filename );

#if 0
      for (int k = 0; k < 2; k++)
      {

        typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ>::template FieldCellGroupType<Line> WFieldCellGroupType;
        typedef FieldLift_DG_Cell< PhysD1, TopoD1, VectorArrayQ>::template FieldCellGroupType<Line> SFieldCellGroupType;

        typedef WFieldCellGroupType::template ElementType<> ElementWFieldClass;
        typedef ElementLift<VectorArrayQ, TopoD1, Line> ElementSFieldClass;

              WFieldCellGroupType& wfldCell = wfld.getCellGroup<Line>(0);
        const SFieldCellGroupType& sfldCell = sfld.getCellGroup<Line>(0);

        ElementWFieldClass wfldElem( wfldCell.basis() );
        ElementSFieldClass sfldElems( sfldCell.basis() );

        const int nelem = wfldCell.nElem();
        for (int elem = 0; elem < nelem; elem++)
        {
          sfldCell.getElement( sfldElems, elem );
          for (int i = 0; i < wfldElem.nDOF(); i++)
            wfldElem.DOF(i) = sfldElems[k].DOF(i)[0];

          wfldCell.setElement( wfldElem, elem );
        }

        filename = "tmp/adjDG_P";
        filename += stringify(order);
        filename += "_";
        filename += stringify(ii);
        filename += "_";
        filename += stringify(k);
        filename += ".plt";
        cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( wfld, filename );
      }
#endif

#endif


#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
      {
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        Real slope = (log(normVec[indx-1]) - log(normVec[indx-2]))/(log(hVec[indx-1]) - log(hVec[indx-2]));
        cout << "  (slope = " << slope << ")";
      }
      cout << endl;
#endif

#if 1
      {
        // Tecplot dump
        string filename = "tmp/slnDG_P";
        filename += to_string(order);
        filename += "_";
        filename += to_string(ii);
        filename += ".plt";
        cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( qfld, filename );
      }
#endif

    } //grid refinement loop

    // Tecplot output
    resultFile << "ZONE T=\"DG BR2 P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  } //order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
