// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#define BOUNDARYOUTPUT

#include <chrono>
#include <ctime>
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"
#include "pyrite_fstream.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "pyrite_fstream.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/NS/SolutionFunction_NavierStokes3D.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DConservative.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/OutputEuler3D.h"
#include "pde/NS/Q3DConservative.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
//#include "pde/NS/Q3DEntropy.h"

#include "pde/AdvectionDiffusion/ForcingFunction3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
//#include "pde/OutputCell_SolutionErrorSquared.h"
//#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"
#include "Discretization/ResidualNormType.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "NonLinearSolver/NonLinearSolver.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"

#include "unit/UnitGrids/XField3D_Box_Hex_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

//Instantiate
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

using namespace std;
using namespace SANS;

//#############################################################################
BOOST_AUTO_TEST_SUITE( Solve3D_DGBR2_Hex_ADPeriodic_test_suite )

BOOST_AUTO_TEST_CASE( Solve3D_DGBR2_Hex_ADPeriodic)
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD3, TopoD3>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolutionFunction_NavierStokes3D_TaylorGreen<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolutionClass;
  typedef SolnNDConvertSpace<PhysD3, SolutionClass> SolutionNDClass;

  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> RKClass;

  // global communicator
  mpi::communicator world;

  //Solver Settings
 // Set up NEWTON solver:
  PyDict NonLinearSolverDict, NewtonSolverDict, NewtonLineUpdateDict, LinSolverDict, UMFPACKDict, LineUpdateDict;

#ifdef SANS_PETSC
  // Take the Preconditioners from my Tandem Sphere case and just copy paste them here

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  LinSolverDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  LinSolverDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  LinSolverDict[SLA::PETScSolverParam::params.MaxIterations] = 900;
  LinSolverDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  LinSolverDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  LinSolverDict[SLA::PETScSolverParam::params.Verbose] = true;
  LinSolverDict[SLA::PETScSolverParam::params.Timing] = true;
  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  LinSolverDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  if ( world.rank( )== 0 )
    std::cout << "Linear solver: PETSc w/ Timing" << std::endl;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = true;

  NewtonSolverParam::checkInputs( NewtonSolverDict );
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#else

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  std::cout << "Linear solver: UMFPACK" << std::endl;
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  //AdjSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#endif

  // gas model
  const Real gamma = 1.4;
  const Real R = 1; // 2.964e-4;        // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R / (gamma - 1);
  const Real Cp = gamma * Cv;

  // reference state (freestream)
  const Real Mach = 0.10;
  const Real Reynolds = 1600;
  const Real Prandtl = 0.71;
  const Real lRef = 1; // 2*PI;
  const Real rhoRef = 1;                        // density scale
  //const Real aoaRef = 0;                      // angle of attack (radians)
  const Real tRef = 1;                          //temperature
  const Real pRef = rhoRef * R * tRef;           // pressure

  const Real cRef = sqrt( gamma * R * tRef );
  const Real qRef = cRef * Mach;                              // velocity scale

  //const Real uRef = qRef * cos( aoaRef );             // velocity
  //const Real vRef = qRef * sin( aoaRef );
  //const Real wRef = 0;
  //const Real ERef = Cv * tRef + 0.5 * (uRef * uRef + vRef * vRef);
  const Real muRef = rhoRef * qRef * lRef / Reynolds;
  if ( world.rank( )== 0 )
    std::cout << "Viscosity = " << muRef << std::endl;

  // PDE
  GlobalTime time;
  Real Tperiod, dt, t_char;
  int nsteps;

  time = 0.;
  t_char = lRef / qRef;
  dt = 0.001 * t_char;
  Tperiod = dt * 2;

  nsteps = int( Tperiod / dt );

  // PDE
  GasModel gas( gamma, R );
  ViscosityModelType visc( muRef );
  ThermalConductivityModel tcond( Prandtl, Cp );
  NDPDEClass pde( gas, visc, tcond, Euler_ResidInterp_Raw );

  // BCs (none -> periodic)
  PyDict BCSymmetry, PyBCList;

  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;
  PyBCList["BCSymmetry"] = BCSymmetry;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0, 1, 2, 3, 4, 5}; //body and symmetry plane


  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Set up initial field
  PyDict solnArgs;
  GlobalTime temp_time;
  temp_time = 0.25;
  SolutionNDClass solnExact(gas, rhoRef, qRef, pRef, lRef);

  if (world.rank()==0)
    std::cout<<"rho:"<<rhoRef<<" V0:"<<qRef<<" p0:"<<pRef<<std::endl;

  // DGBR2 Discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc( 0, viscousEtaParameter );

  int RKorder = 4;
  int order = 3;

  int ii, jj, kk;
  //int power = 4;
  ii =  2; // pow( 2, power);
  jj = ii;
  kk = ii;

  // Grid
  const std::array<bool,3> periodicity = {{false, false, false}};
  XField3D_Box_Hex_X1 xfld( world, ii, jj, kk, -PI,  PI, -PI,  PI, -PI,  PI, periodicity);

  // Solution: Lagrange P0 or P1 only*
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld( xfld, order, BasisFunctionCategory_Lagrange );

  // Initialize
  for_each_CellGroup<TopoD3>::apply( ProjectSolnCell_Discontinuous( solnExact, { 0 } ), (xfld, qfld) );
  //qfld = 100;

  // Lifting operators
  FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld( xfld, order, BasisFunctionCategory_Lagrange );
  rfld = 0;

  // Boundary Trace: None in effect
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Lagrange,
                                                        BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups ) );
  lgfld = 0;

  // Integration quadrature
  QuadratureOrder quadratureOrder( xfld, 3*order+2 );
  std::vector<Real> tol = { 1e-9, 1e-9 };

  std::vector<int> interiorTraceGroups = linspace(0,xfld.nInteriorTraceGroups()-1);
  std::vector<int> cellGroups = linspace(0,xfld.nCellGroups()-1);

  // Spatial Discretization
  PrimalEquationSetClass PrimalEqSet( xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder, ResidualNorm_Default, tol,
                                      cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, time );

  // Temporal Discretization
  int RKtype = 0;
  int RKstages = RKorder;
  RKClass RK( RKorder, RKstages, RKtype, dt, time, xfld, qfld, NonLinearSolverDict, pde,
              quadratureOrder, { tol[0] }, { 0 }, PrimalEqSet );

  // Tecplot Output
  int FieldOrder = order;
  string filename = "tmp/testTaylorGreenslnDG_3D_Hex_P";
  filename += to_string( order );
  filename += "_Q";
  filename += to_string( FieldOrder );
  filename += "_quadOrder";
  filename += to_string( 3*order+2 );
  filename += "_Time_";
  filename += to_string( 0 );
  output_Tecplot( qfld, filename );

#if 1
  for (int step = 0; step < nsteps; step++)
  {
    if ( world.rank( )== 0 )
      std::cout << "Time Step: " << step + 1 << "\n time is " << time << std::endl;
    // Advance solution
    RK.march( 1 );
    // Tecplot Output
#if 0
    int FieldOrder = order;
    string filename = "tmp/testTaylorGreenslnDG_3D_Hex_P";
    filename += to_string( order );
    filename += "_Q";
    filename += to_string( FieldOrder );
    filename += "_quadOrder";
    filename += to_string( 11 );
    filename += "_Time_";
    //int f_time = time * 10;
    filename += to_string( step + 1 );

    auto start = std::chrono::system_clock::now();
    output_Tecplot( qfld, filename );
    auto end = std::chrono::system_clock::now();

    std::chrono::duration<double> ETA = end-start;
    std::cout<<"********************* Time to Print FILE @ Step: "<<step<<" -> "<< ETA.count()<<std::endl;
#endif

  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
