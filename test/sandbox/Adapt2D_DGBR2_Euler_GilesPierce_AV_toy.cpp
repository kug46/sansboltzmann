// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_Line_Euler_Nozzle_ArtificialViscosity_toy
// Inviscid 2D nozzle with shock and artificial viscosity


#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <chrono>
#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Output_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_Dispatch_DGBR2.h"

#include "Discretization/isValidState/SetValidStateCell.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/Function/FunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"


#include "../unit/UnitGrids/XField2D_GilesPierceNozzle_HACK.h"

#include "tools/linspace.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Line_Euler_Nozzle_ArtificialViscosity_toy )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Line_Euler_Nozzle_ArtificialViscosity_toy )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> BCVector;

#ifdef BOUNDARYOUTPUT
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#else

  typedef OutputEuler2D_Pressure<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

//  typedef OutputEuler2D_totalEnthalpyErrorSquare<PDEClass> OutputClass;
//  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
//  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_CG ParamBuilderType;
  typedef GenHField_CG<PhysD2, TopoD2> GenHFieldType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Real xmin = -1;
  Real xmax = 1;
  Real ymin = 0;
  Real ymax = 2;
  Real ii = 100;
  Real tt = 1;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_GilesPierceNozzle_HACK>( world, ii, tt, xmin, xmax, ymin, ymax, true );
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld_linear = std::make_shared<XField2D_GilesPierceNozzle_HACK>( world, ii, tt, xmin, xmax, ymin, ymax,
                                                                                                            false );
  std::shared_ptr<GenHFieldType> phfld = std::make_shared<GenHFieldType>(*pxfld);
  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler pde_principal
  ////////////////////////////////////////////////////////////////////////////////////////
  // gas model
  const Real gamma = 1.4;
  const Real R     = .286;
  GasModel gas(gamma, R);

  bool hasSpaceTimeDiffusion = false;

  int orderPrimal = 1;
  int orderAdjoint = orderPrimal + 1;
  bool isSteady = true;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  PDEBaseClass pdeEulerAV(orderPrimal, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  // Chamber conditions (no velocity)
  const Real tc = 4;
  const Real pc = 2;
  const Real rhoc = pc / (R * tc);

  // Define inflow BC
  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = tc;
  BCInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = pc;
  BCInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = 0;

  Real pa = pc;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSupersonic_Pressure_mitState>::params.pSpec] = pa;
  //std::cout << "Outflow pressure ratio: " << pa/pc << " ("  << pa/pc << ")" << std::endl;

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;


  // Define BC list
  PyDict PyBCList;
  PyBCList["BCInflow"] = BCInflow;
  PyBCList["BCOut"] = BCOut;
  PyBCList["BCWall"] = BCWall;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Define the BoundaryGroups for each boundary condition
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["BCInflow"] = {3}; //left
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCWall"] = {0,2};

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our sensor parameter PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(orderPrimal);
  SensorSource sensor_source(orderPrimal, sensor);

  // AV PDE with sensor equation
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, orderPrimal, hasSpaceTimeDiffusion,
                 EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp);

  PyDict ParamDict;

  // DGBR2 discretization
  Real viscousEtaParameter = 2*Triangle::NEdge;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);


  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pxfld), pde, orderPrimal, orderAdjoint,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, ParamDict, disc);
  pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Lower back pressure
  ////////////////////////////////////////////////////////////////////////////////////////
  int maxI = 0;
  for (int i = 0; i<=maxI; i++)
  {
    //pa = pc*pow(0.9, i);
    pa = 0.09393263*pc;
    //pa = 1.98;
    //pa = 1.6;
    BCOut[BCEuler2DParams<BCTypeOutflowSupersonic_Pressure_mitState>::params.pSpec] = pa;
    std::cout << std::endl << "----- Pressure Iteration " << i << "-----" << std::endl;
    std::cout << "Outflow pressure ratio: " << pa/pc << " ("  << pa/pc << ")" << std::endl;

    // Define BC list
    PyDict PyBCList;
    PyBCList["BCInflow"] = BCInflow;
    PyBCList["BCOut"] = BCOut;
    PyBCList["BCWall"] = BCWall;

    //No exceptions should be thrown
    BCParams::checkInputs(PyBCList);

    // Define the BoundaryGroups for each boundary condition
    std::map<std::string, std::vector<int>> BCBoundaryGroups;
    BCBoundaryGroups["BCInflow"] = {3}; //left
    BCBoundaryGroups["BCOut"] = {1};
    BCBoundaryGroups["BCWall"] = {0,2};

    std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

    ////////////////////////////////////////////////////////////////////////////////////////
    // Create parameters
    ////////////////////////////////////////////////////////////////////////////////////////
    enum ResidualNormType ResNormType = ResidualNorm_L2;
    std::vector<Real> tol = {1e-10, 1.0e-11};

    ////////////////////////////////////////////////////////////////////////////////////////
    // Output Functional
    ////////////////////////////////////////////////////////////////////////////////////////
    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int j = 0; j < pxfld->nInteriorTraceGroups(); j++)
      interiorTraceGroups.push_back(j);
#ifdef BOUNDARYOUTPUT
    NDOutputClass outputFcn(pde, 1., 0.);
    OutputIntegrandClass outputIntegrand( outputFcn, {1} );
#else
    NDOutputClass fcnOutput(pde);
    OutputIntegrandClass outputIntegrand(fcnOutput, cellGroups);
#endif
    //std::vector<int> cellGroups;
    //cellGroups = {0};

    ////////////////////////////////////////////////////////////////////////////////////////
    // Newton Solver
    ////////////////////////////////////////////////////////////////////////////////////////
    PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

  #ifdef INTEL_MKL
    std::cout << "Using MKL\n";
    PyDict MKL_PARDISODict;
    MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
  #else
    PyDict UMFPACKDict;
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  #endif

    LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

    NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
    NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
    NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

  #if 0
    NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  #else
    //PTC
    NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type] =
                    SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
    NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]    = NewtonSolverDict;
    NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 1;
    NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
    NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 100;
    NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 50000;
    NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  #endif

    SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

    // Check inputs
    SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set up pyrite file
    ////////////////////////////////////////////////////////////////////////////////////////
    pyrite_file_stream pyriteFile( "IO/Adaptation/Adapt2D_Euler_GPNozzle_toy.txt",
                                   1e-10, 1e-10, pyrite_file_stream::check );
    pyriteFile << std::setprecision( 16 ) << std::scientific;

    ////////////////////////////////////////////////////////////////////////////////////////
    // Adaptation parameters
    ////////////////////////////////////////////////////////////////////////////////////////
    int maxIter;
    if (i== maxI)
    {
      maxIter = 30;
    }
    else
    {
      maxIter = -1;
    }
    Real targetCost = 10000;

    std::string filename_base = "tmp/Nozzle/2D/";
    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directory(base_dir);

    std::string adapthist_filename = "test.adapthist";
    fstream fadapthist( filename_base + adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE( fadapthist.good(), "Error opening file: " + adapthist_filename );

    PyDict MOESSDict;
    MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
    MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
    MOESSDict[MOESSParams::params.UniformRefinement] = false;
    MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

    PyDict MesherDict;
    MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
    //MesherDict[EpicParams::params.Version]="madcap_devel-10.1";
    //MesherDict[EpicParams::params.QualityMode] = EpicParams::params.QualityMode.Shape0;
    MesherDict[EpicParams::params.CurvedQOrder] = 3; //Mesh order
    MesherDict[EpicParams::params.CSF] = "grids/GPNozzle0_X0.csf"; //Geometry file
    MesherDict[EpicParams::params.GeometryList2D] = "nozzleall"; //Geometry list
    MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;
    MesherDict[EpicParams::params.NodeMovement] = true;


    PyDict AdaptDict;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;

    MeshAdapterParams<PhysD2, TopoD2>::checkInputs( AdaptDict );

    MeshAdapter<PhysD2, TopoD2> mesh_adapter( AdaptDict, fadapthist );

    //std::vector<int> interiorTraceGroups;
    //for ( int j = 0; j < pxfld->nInteriorTraceGroups(); j++)
      //interiorTraceGroups.push_back(j);

    ////////////////////////////////////////////////////////////////////////////////////////
    // Create ProblemStatement
    ////////////////////////////////////////////////////////////////////////////////////////
    std::shared_ptr<SolverInterfaceClass> pInterface;
    const int quadOrder = -1;
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, LinearSolverDict,
                                                        outputIntegrand);




    ////////////////////////////////////////////////////////////////////////////////////////
    // Set Initial condition
    ////////////////////////////////////////////////////////////////////////////////////////
    if (i == 0)
    {
      ArrayQ q0 = pde.setDOFFrom( AVVariable<DensityVelocityPressure2D, Real>({{rhoc, 0.0, 0.0, pc}, 0.0}) );
      pGlobalSol->setSolution(q0);
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Output Initial Condition
    ////////////////////////////////////////////////////////////////////////////////////////
    string filenameIC = "NozzleIC_"
                       + Type2String<QType>::str()
                       + "_i"
                       + std::to_string(i)
                       + "_P"
                       + std::to_string(orderPrimal)
                       + "_G"
                       + std::to_string(0)
                       + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, filename_base + filenameIC );

    ////////////////////////////////////////////////////////////////////////////////////////
    // Solve Primal and output
    ////////////////////////////////////////////////////////////////////////////////////////
    //pGlobalSolOld = pGlobalSol;
    auto t1 = std::chrono::high_resolution_clock::now();
    pInterface->solveGlobalPrimalProblem();
    auto t2 = std::chrono::high_resolution_clock::now();
    std::cout << "P solve: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << "ms" << std::endl;

    string filename = "Nozzle_"
                       + Type2String<QType>::str()
                       + "_i"
                       + std::to_string(i)
                       + "_P"
                       + std::to_string(orderPrimal)
                       + "_G"
                       + std::to_string(0)
                       + ".plt";
     output_Tecplot( pGlobalSol->primal.qfld, filename_base + filename );

     string sol_filename = "NozzleDerived_"
                       + Type2String<QType>::str()
                       + "_i"
                       + std::to_string(i)
                       + "_P"
                       + std::to_string(orderPrimal)
                       + "_G"
                       + std::to_string(0)
                       + ".plt";
     //output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld,
                    //pGlobalSol->primal.rfld, filename_base + sol_filename);

    string gsol_filename = "Nozzle_"
                       + Type2String<QType>::str()
                       + "_i"
                       + std::to_string(i)
                       + "_P"
                       + std::to_string(orderPrimal)
                       + "_G"
                       + std::to_string(0)
                       + ".gplt";
    //output_gnuplot( pGlobalSol->primal.qfld, filename_base + gsol_filename );

    ////////////////////////////////////////////////////////////////////////////////////////
    // Solve Dual and Adaptation
    ////////////////////////////////////////////////////////////////////////////////////////
    pInterface->solveGlobalAdjointProblem();
    string filename_a = "Nozzle_adjoint_i"
                            + std::to_string(i)
                            + "_P"
                            + std::to_string(orderPrimal)
                            + "_G0"
                            + ".plt";
         output_Tecplot( pGlobalSol->adjoint.qfld, filename_base + filename_a );

    for (int iter = 0; iter < maxIter+1; iter++)
    {
      std::cout << std::endl << "-----Adaptation Iteration " << iter << "-----" << std::endl;

      //Compute error estimates
      pInterface->computeErrorEstimates();

      //Perform local sampling and adapt mesh
      MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;
      //std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
      ptr_pair = mesh_adapter.adapt( *pxfld_linear, *pxfld, cellGroups, *pInterface, iter );
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew_linear = ptr_pair.first;
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew = ptr_pair.second;
      phfld = std::make_shared<GenHFieldType>(*pxfldNew);

      interiorTraceGroups.clear();
      for (int j = 0; j < pxfldNew->nInteriorTraceGroups(); j++)
        interiorTraceGroups.push_back( j );
      std::shared_ptr<SolutionClass> pGlobalSolNew;
      pGlobalSolNew = std::make_shared<SolutionClass>((*phfld, *pxfldNew), pde, orderPrimal, orderAdjoint,
                                                        BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                        active_boundaries, ParamDict, disc);
      pGlobalSolNew->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

      //Perform L2 projection from solution on previous mesh
      pGlobalSolNew->setSolution( *pGlobalSol );

      std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
      pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                              cellGroups, interiorTraceGroups,
                                                              PyBCList, BCBoundaryGroups,
                                                              SolverContinuationDict, LinearSolverDict,
                                                              outputIntegrand);

      //Update pointers to the newest problem (this deletes the previous mesh and solutions)
      pxfld_linear = pxfldNew_linear;
      pxfld = pxfldNew;  //New;
      pGlobalSol = pGlobalSolNew;
      pInterface = pInterfaceNew;

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      // Write files
      string filename = "Nozzle_"
                         + Type2String<QType>::str()
                         + "_i"
                         + std::to_string(i)
                         + "_P"
                         + std::to_string(orderPrimal)
                         + "_G"
                         + std::to_string(iter+1)
                         + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, filename_base + filename );

      string filename_a = "Nozzle_adjoint_i"
                         + std::to_string(i)
                         + "_P"
                         + std::to_string(orderPrimal)
                         + "_G"
                         + std::to_string(iter+1)
                         + ".plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, filename_base + filename_a );
    }
    fadapthist.close();
    string filenamegrid = "tmp/Nozzle/2D/GilesPierce_2D.plt";
    output_Tecplot(*pxfld, filenamegrid);
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
