// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adjoint1D_DGAdvective_BuckleyLeverett_btest
// testing of 1-D DG adjoint with Buckley-Leverett

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Adjoint1D_DGAdvective_BuckleyLeverett_ST_test_suite )

//----------------------------------------------------------------------------//
// solution: P1 thru P3
// Lagrange multiplier: continuous P1 thru P3
// exact solution: double BL
BOOST_AUTO_TEST_CASE( Adjoint1D_DGAdvective_BuckleyLeverett_ST )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_None CapillaryModel;
  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QTypePrimitive_Sw, PDEClass> ExactSolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef BCBuckleyLeverett1DVector<PDEClass> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  // PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.2;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel cap_model;

  NDPDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 0.8;
  const Real SwR = 0.3;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(pde, SwL, SwR, xinit);

  // BCs

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params.qB] = SwL;

  PyDict BCInit;
  BCInit[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  BCInit[BCBuckleyLeverett1DParams<BCTypeTimeIC>::params.qB] = SwR;

  PyDict PyBCList;
  PyBCList["DirichletL"] = BCDirichletL;
  PyBCList["BCInit"] = BCInit;
  PyBCList["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletL"] = {3}; //Left boundary
  BCBoundaryGroups["BCInit"] = {0}; //Bottom boundary
  BCBoundaryGroups["None"] = {1,2}; //Right and top boundary

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // norm data
//  Real hVec[10];
//  Real hDOFVec[10];   // 1/sqrt(DOF)
//  Real normVec[10];   // L2 error
//  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/Solve1D_DGAdvective_BuckleyLeverett.plt", std::ios::out);
  //pyrite_file_stream pyriteFile("IO/Solve/Solve1D_DGAdvective_BuckleyLeverett_FullTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  //pyrite_file_stream pyriteFile("IO/Solve/Solve1D_DGAdvective_BuckleyLeverett_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
//  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 0;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 0;
#else
  int ordermax = 0;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
//    indx = 0;

    // loop over grid resolution: 30*factor
    int ii, jj;
    int factormin = 1;
#ifdef SANS_FULLTEST
    int factormax = 1;
#else
    int factormax = 1;
#endif
    for (int factor = factormin; factor <= factormax; factor++)
    {
      ii = 20*factor;
      jj = 20*factor;

      // grid:
      XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 50, 0, 25, true );
//      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, 50, 0, 25);

//      XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 1, 0, 1, true );
//      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );
//      xfld.dump(3,std::cout);

      // Initial solution
      ArrayQ qinit;
      pde.setDOFFrom( qinit, 0.5, "Sw" );

      // solution: Hierarchical, C0
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_adj(xfld, order, BasisFunctionCategory_Legendre);
      qfld = qinit;
      qfld_adj = 0.0;

//      qfld.DOF(0) = 0.735706925392150879;
//      qfld.DOF(1) = 0.182240694761276245;
//      qfld.DOF(2) = 0.112710796296596527;
//      qfld.DOF(3) = 0.1;
//      qfld.DOF(4) = 0.1;
//      qfld.DOF(5) = 0.1;
//      qfld.DOF(6) = 0.854126989841461182;
//      qfld.DOF(7) = 0.722093820571899414;
//      qfld.DOF(8) = 0.631096184253692627;
//      qfld.DOF(9) = 0.327745407819747925;
//      qfld.DOF(10) = 0.163223594427108765;
//      qfld.DOF(11) = 0.1;

      // Or, use the projection of the exact solution as an initial solution
      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
//      output_Tecplot( qfld, "tmp/qfld_init.plt" );
      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 0
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups)  );
#elif 0
      std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
      std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, active_BGroup_list);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_adj(xfld, order, BasisFunctionCategory_Legendre, active_BGroup_list);
#endif

      lgfld = 0.0;
      lgfld_adj = 0.0;
      const int nDOFBC = lgfld.nDOF();

      std::cout<<"DOF: "<<(nDOFPDE + nDOFBC)<<std::endl;

      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = { 1e-10, 1e-10 };

      //--------PRIMAL SOLVE------------

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups);

      // Create the solver object
      NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );

      // set initial condition from current solution in solution fields
      SystemVectorClass sln0(PrimalEqSet.vectorStateSize());
      PrimalEqSet.fillSystemVector(sln0);

#if 0
      // jacobian nonzero pattern
      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
      PrimalEqSet.jacobian(sln0, nz);

      // jacobian
      SystemMatrixClass jac(nz);
      jac = 0.0;
      PrimalEqSet.jacobian(sln0, jac);

      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

      // nonlinear solve
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SolveStatus status = nonlinear_solver.solve(sln0, sln);
      BOOST_CHECK( status.converged );

      //--------ADJOINT SOLVE------------

      PrimalEquationSetClass AdjointEqSet(xfld, qfld_adj, lgfld_adj, pde, quadratureOrder,
                                          ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups);
      AdjointEqSet.setSolutionField(sln);

      // jacobian nonzero pattern transposed
      SystemNonZeroPattern nzT(AdjointEqSet.matrixSize());
      AdjointEqSet.jacobianTranspose(sln, nzT);

      // jacobian transposed
      SystemMatrixClass jacT(nzT);
      jacT = 0.0;
      AdjointEqSet.jacobianTranspose(sln, jacT);

      // adjoint (right hand side)
      SystemVectorClass rhs(AdjointEqSet.vectorEqSize());
      rhs = 0.0;

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_Galerkin( outputIntegrand, rhs(0) ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      rhs(0) = rhs(0)/phi; //The Buckley-Leverett equation is divided by phi

      // adjoint solve
      SLA::UMFPACK<SystemMatrixClass> solver(AdjointEqSet, SLA::TransposeSolve);
      SystemVectorClass adj_sln(AdjointEqSet.vectorStateSize());
      solver.solve(rhs, adj_sln);

      AdjointEqSet.setSolutionField(adj_sln);

#if 0
      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jacT, fout );
#endif

//      cout << sqrt(rsdPDEnrm) << "\t" << sqrt(rsdBCnrm) <<endl;

      // L2 solution error
//      int quadratureOrder[2] = {-1, -1};    // max
//      ArrayQ SquareError = 0;
//      FunctionalCell<TopoD1>::integrate( fcnErr, qfld, quadratureOrder, 1, SquareError );
//      Real norm = SquareError;

//      hVec[indx] = 1./ii;
//      hDOFVec[indx] = 1./sqrt(nDOFPDE);
//      normVec[indx] = sqrt( norm );
//      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;
#endif

#if 1
      // Tecplot dump
      string filename = "tmp/adjoint_DGAdv_BuckleySpaceTime_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld_adj, filename );
      output_Tecplot( qfld, "tmp/primal.plt" );
#endif

    } //grid refinement loop

#if 0
    // Tecplot output
    resultFile << "ZONE T=\"DG BR2 P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }
#endif

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
//    std::cout<<resultFile.str()<<std::endl;
  } //order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
