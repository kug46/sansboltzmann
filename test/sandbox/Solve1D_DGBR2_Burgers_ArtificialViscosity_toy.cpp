// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_DGBR2_Burgers_ArtificialViscosity_toy
// testing of 1-D DG with Burgers

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/vector_c.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"
#include "pde/Burgers/PDEBurgers_ArtificialViscosity1D.h"
#include "pde/Burgers/BCmitAVSensorBurgers1D.h"
#include "pde/Burgers/BurgersConservative1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/ForcingFunction1D_MMS.h"
#include "pde/BCParameters.h"

#include "Discretization/JacobianParam.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/HField/GenHFieldLine_CG.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGBR2_Burgers_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGBR2_Burgers )
{
  typedef BurgersConservative1D QType;
  typedef PDEBurgers<PhysD1,
                     QType,
                     ViscousFlux1D_Uniform,
                     Source1D_UniformGrad > PDEBurgers1D;
  typedef PDENDConvertSpace<PhysD1, PDEBurgers1D> NDPDEClass;
  typedef PDEBurgers_ArtificialViscosity<PhysD1,
                                         QType,
                                         ViscousFlux1D_Uniform,
                                         Source1D_UniformGrad> ArtificialViscosity;
  typedef PDENDConvertSpace<PhysD1,     ArtificialViscosity> NDArtificialViscosity;
  typedef NDArtificialViscosity::template ArrayQ<Real> ArrayQ;
  typedef NDArtificialViscosity::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDArtificialViscosity::template MatrixParam<Real> PDEMatrixParam;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD1,
                             SensorParameterTraits<PhysD1>,
                             Sensor_AdvectiveFlux1D_Uniform,
                             Sensor_ViscousFlux1D_GenHScale,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD1, SensorPDEClass> SensorNDPDEClass;
  typedef SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;
  typedef SensorNDPDEClass::template VectorArrayQ<Real> SensorVectorArrayQ;
  typedef SensorNDPDEClass::template MatrixParam<Real> SensorMatrixParam;

  typedef ScalarFunction1D_Tanh SolutionExact;

  typedef BCTypeFunction_mitState<QType,ViscousFlux1D_Uniform, Source1D_UniformGrad> BCType;

  typedef typename DLA::MatrixSymS<PhysD1::D,Real> HType;

  typedef typename MakeTuple<FieldTuple, Field<PhysD1, TopoD1, HType>,
                                         Field<PhysD1, TopoD1, SensorArrayQ>,
                                         XField<PhysD1, TopoD1>>::type AVParamField;

  typedef BCmitAVSensorBurgers1DVector<QType, ViscousFlux1D_Uniform, Source1D_UniformGrad> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDArtificialViscosity, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, AVParamField> PDEPrimalEquationSetClass;
  typedef BCmitAVSensorBurgers<PhysD1, BCType> BCSolnClass;
  typedef PDEPrimalEquationSetClass::BCParams PDEBCParams;

  typedef PDEPrimalEquationSetClass::SystemMatrix SystemMatrixClass;

  typedef BCSensorParameter1DVector<Sensor_AdvectiveFlux1D_Uniform, Sensor_ViscousFlux1D_GenHScale> SensorBCVector;

  typedef typename MakeTuple<FieldTuple, Field<PhysD1, TopoD1, HType>,
                                         Field<PhysD1, TopoD1, ArrayQ>,
                                         XField<PhysD1, TopoD1>>::type ParamField;
  typedef AlgebraicEquationSet_DGBR2<SensorNDPDEClass, BCNDConvertSpace, SensorBCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamField> SensorPrimalEquationSetClass;
  typedef SensorPrimalEquationSetClass::BCParams SensorBCParams;

  typedef SensorPrimalEquationSetClass::SystemMatrix SensorSystemMatrixClass;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Burgers parameter PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  Real nu = 0.0;
  Real A = 2.0;
  Real B = 0.0;
  Real b =  0.2;

  PyDict Tanh;
  Tanh[BCSolnClass::ParamsType::params.Function.Name] = BCSolnClass::ParamsType::params.Function.Tanh;
  Tanh[SolutionExact::ParamsType::params.nu] = nu;
  Tanh[SolutionExact::ParamsType::params.A] = A;
  Tanh[SolutionExact::ParamsType::params.B] = B;

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Tanh> NDExactSolutionClass;
  NDExactSolutionClass solnExact( Tanh );

  ViscousFlux1D_Uniform visc( nu );
  ScalarFunction1D_Tanh MMS_soln( Tanh );
  typedef ForcingFunction1D_MMS<PDEBurgers1D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(MMS_soln) );
  Source1D_UniformGrad source(b, 0.0);

  NDPDEClass pde(visc, source, forcingptr);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict PDEBCSoln;
  PDEBCSoln[PDEBCParams::params.BC.BCType] = PDEBCParams::params.BC.Function_mitState;
  PDEBCSoln[BCSolnClass::ParamsType::params.Function] = Tanh;

  PyDict PyPDEBCList;
  PyPDEBCList["PDEBCSoln"] = PDEBCSoln;

  std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  PDEBCBoundaryGroups["PDEBCSoln"] = {0, 1};

  //Check the BC dictionary
  PDEBCParams::checkInputs(PyPDEBCList);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict BCSensorRobin, PySensorBCList;
  BCSensorRobin[SensorBCParams::params.BC.BCType] = SensorBCParams::params.BC.LinearRobin_sansLG;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 0.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["LinearRobin_sansLG"] = {0, 1}; // Both sides

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "Running full test..." << endl;
#endif
  int ordermax = 5;
#else
  int ordermax = 1;//2;
#endif
  for (int order_pde = ordermin; order_pde <= ordermax; order_pde++)
  {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Create our PDE mit AV
    ////////////////////////////////////////////////////////////////////////////////////////
    NDArtificialViscosity avpde(order_pde, pde);

    ////////////////////////////////////////////////////////////////////////////////////////
    // Create our sensor parameter PDE
    ////////////////////////////////////////////////////////////////////////////////////////
    Sensor sensor(avpde);
    Sensor_AdvectiveFlux1D_Uniform sensor_adv(0.0);
    Sensor_ViscousFlux1D_GenHScale sensor_visc(10.0);
    Source_JumpSensor sensor_source(order_pde, sensor);

    SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

    // loop over grid resolution
    int factormin = 1;
#ifdef SANS_FULLTEST
    int factormax = 6;
#else
    int factormax = 1;//4;
#endif
    for (int factor = factormin; factor <= factormax; factor++)
    {
      int ii = 100*factor;
      XField1D xfld( ii, -1, 1 );
      GenHField_CG<PhysD1,TopoD1> hfld(xfld);

      ////////////////////////////////////////////////////////////////////////////////////////
      // Allocate our Burgers parameter field
      ////////////////////////////////////////////////////////////////////////////////////////
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> pde_rfld(xfld, order_pde, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> pde_lgfld(xfld, order_pde, BasisFunctionCategory_Legendre,
                                                               PDEBCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups));
      pde_qfld = 0;
      pde_rfld = 0;
      pde_lgfld = 0;

      ////////////////////////////////////////////////////////////////////////////////////////
      // Allocate our sensor parameter field
      ////////////////////////////////////////////////////////////////////////////////////////
      int order_sensor = 1;

      Field_DG_Cell<PhysD1, TopoD1, SensorArrayQ> sensorfld(xfld, order_sensor, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD1, TopoD1, SensorVectorArrayQ> sensor_rfld(xfld, order_sensor, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD1, TopoD1, SensorArrayQ> sensor_lgfld(xfld, order_sensor, BasisFunctionCategory_Legendre,
                                                                        SensorBCParams::getLGBoundaryGroups(PySensorBCList, BCBoundaryGroups));
      sensorfld = 0;
      sensor_rfld = 0;
      sensor_lgfld = 0;

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our AES to solve the Burgers PDE
      ////////////////////////////////////////////////////////////////////////////////////////
//      std::vector<Real> tol = {1e-12, 1e-12};
//      Real viscousEtaParameter = 2;
//      DiscretizationDGBR2 disc(0, viscousEtaParameter);
//      PDEPrimalEquationSetClass PDEPrimalEqSet(xfld, pde_qfld, pde_rfld, pde_lgfld, pde, disc, tol, {0}, {0}, PyPDEBCList, PDEBCBoundaryGroups);

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our AES to solve our PDE mit AV
      ////////////////////////////////////////////////////////////////////////////////////////
      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-12, 1e-12};
      Real viscousEtaParameter = 0;
      DiscretizationDGBR2 disc(0, viscousEtaParameter);
      AVParamField avparamfld = (hfld, sensorfld, xfld);
      PDEPrimalEquationSetClass PDEPrimalEqSet(avparamfld, pde_qfld, pde_rfld, pde_lgfld, avpde,
                                               disc, quadratureOrder, ResidualNorm_Default, tol,
                                               {0}, {0}, PyPDEBCList, PDEBCBoundaryGroups);

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our AES to solve the sensor PDE
      ////////////////////////////////////////////////////////////////////////////////////////
      ParamField paramfield = (hfld, pde_qfld, xfld);
      SensorPrimalEquationSetClass SensorPrimalEqSet(paramfield, sensorfld, sensor_rfld, sensor_lgfld, sensorpde,
                                                     disc, quadratureOrder, ResidualNorm_Default, tol,
                                                     {0}, {0}, PySensorBCList, BCBoundaryGroups);

      ////////////////////////////////////////////////////////////////////////////////////////
      // Generate off diagonal parameter jaocians
      ////////////////////////////////////////////////////////////////////////////////////////
      typedef PDEMatrixParam MatrixQ01;
      typedef SensorMatrixParam MatrixQ10;

      typedef SLA::SparseMatrix_CRS<MatrixQ01>   MatrixClass01;
      typedef SLA::SparseMatrix_CRS<MatrixQ10>   MatrixClass10;

      typedef DLA::MatrixD<MatrixClass01> SystemMatrix01;
      typedef DLA::MatrixD<MatrixClass10> SystemMatrix10;

      const int iPDE_SensorParam = 1; // Sensor parameter in PDE index
      const int iSensor_PDEParam = 1; // PDE solution parameter in sensor index

      typedef JacobianParam<boost::mpl::vector1_c<int,1>,PDEPrimalEquationSetClass> JacobianParam_PDE;
      typedef JacobianParam<boost::mpl::vector1_c<int,1>,SensorPrimalEquationSetClass> JacobianParam_Sensor;

      std::map<int,int> PDEMap;
      PDEMap[iPDE_SensorParam] = SensorPrimalEqSet.iq; //The parameter in the PDE

      std::map<int,int> SensorMap;
      SensorMap[iSensor_PDEParam] = PDEPrimalEqSet.iq;

      JacobianParam_PDE JP01(PDEPrimalEqSet, PDEMap);
      JacobianParam_Sensor JP10(SensorPrimalEqSet, SensorMap);

      ////////////////////////////////////////////////////////////////////////////////////////
      // Generate 2x2 Block system
      ////////////////////////////////////////////////////////////////////////////////////////
      typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass, SystemMatrix01,
                                            SystemMatrix10, SensorSystemMatrixClass> AlgEqSetType_2x2;

      AlgEqSetType_2x2 BlockAES(PDEPrimalEqSet, JP01,
                                JP10, SensorPrimalEqSet);

#define BLOCK_SOLVE

#ifdef BLOCK_SOLVE
      typedef AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
      typedef AlgEqSetType_2x2::SystemVector BlockVectorClass;
#endif


      ////////////////////////////////////////////////////////////////////////////////////////
      // Newton Solver
      ////////////////////////////////////////////////////////////////////////////////////////
      PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
      UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
      LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

      NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
      NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
      NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
      NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1000;
      NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

      NewtonSolverParam::checkInputs(NewtonSolverDict);
#ifdef BLOCK_SOLVE
      NewtonSolver<BlockMatrixClass> SolverBlock( BlockAES, NewtonSolverDict );
#else
      NewtonSolver<SystemMatrixClass> SolverBurgers( PDEPrimalEqSet, NewtonSolverDict );
#endif

      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial guess
      ////////////////////////////////////////////////////////////////////////////////////////

      // Or, use the projection of the exact solution as an initial solution
      for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );
      output_gnuplot( pde_qfld, "tmp/qfld_init_pde.plt" );

#ifdef BLOCK_SOLVE
      BlockVectorClass q(BlockAES.vectorStateSize());
      BlockVectorClass sln(BlockAES.vectorStateSize());
      BlockAES.fillSystemVector(q);
      sln = q;
#else
      SystemVectorClass q(PDEPrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PDEPrimalEqSet.vectorStateSize());
      PDEPrimalEqSet.fillSystemVector(q);
      sln = q;
#endif

      ////////////////////////////////////////////////////////////////////////////////////////
      // SOLVE
      ////////////////////////////////////////////////////////////////////////////////////////
#ifdef BLOCK_SOLVE

      NewtonSolver<SystemMatrixClass> SolverSensor( SensorPrimalEqSet, NewtonSolverDict );
      SolveStatus status = SolverSensor.solve(q.v1, sln.v1);
      BOOST_CHECK( status.converged );

      q.v1 = sln.v1;
      output_gnuplot( sensorfld, "tmp/qfld_init_sensor.plt" );

      status = SolverBlock.solve(q, sln);
      BOOST_CHECK( status.converged );
#else
      SolveStatus status = SolverBurgers.solve(q, sln);
      BOOST_CHECK( status.converged );
#endif

#if 1
      // gnuplot dump
      string gfilename = "tmp/slnDG_P";
      gfilename += to_string(order_pde);
      gfilename += "_";
      gfilename += to_string(ii);
      gfilename += ".plt";
      output_gnuplot( pde_qfld, gfilename );
#endif
#if 1
      // gnuplot dump
      string gfilenameS = "tmp/sensorDG_P";
      gfilenameS += to_string(order_pde);
      gfilenameS += "_";
      gfilenameS += to_string(ii);
      gfilenameS += ".plt";
      output_gnuplot( sensorfld, gfilenameS );
#endif

    } //grid refinement loop
  } //order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
