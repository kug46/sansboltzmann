// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGAdvective_Triangle_AD_btest
// testing of 2-D DG Advective on triangle Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"
#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#define PERIODIC 1
#define PARALLEL 1
#if PERIODIC
#if PARALLEL
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#else
#include "unit/UnitGrids/XField2D_BoxPeriodic_Triangle_X1.h"
#endif
#else
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGAdvective_AD_RK_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGAdvective_AD_RK )
{

  // Set up Newton Solver
    PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict, LinSolverDict;
#ifdef SANS_PETSC
  // Take the Preconditioners from my Tandem Sphere case and just copy paste them here

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  LinSolverDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  LinSolverDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  LinSolverDict[SLA::PETScSolverParam::params.MaxIterations] = 2500;
  LinSolverDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  LinSolverDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  LinSolverDict[SLA::PETScSolverParam::params.Verbose] = true;
  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  LinSolverDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  std::cout << "Linear solver: PETSc" << std::endl;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  NewtonSolverParam::checkInputs( NewtonSolverDict );
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#else

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  std::cout << "Linear solver: UMFPACK" << std::endl;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  //AdjSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#endif

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> AlgebraicEquationSet_DGClass;

  typedef AlgebraicEquationSet_DGClass::BCParams BCParams;

  typedef ScalarFunction2D_SineSineSineUnsteady SolutionClass;
  typedef SolnNDConvertSpace<PhysD2, SolutionClass> SolutionNDClass;

  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> RKClass;
  //typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> BDFClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;


  // PDE
  GlobalTime time;
  Real Tperiod, dt;
  int nsteps;

  time = 0.;
  Tperiod = 1.3;
  dt = 0.1;

  nsteps = int (Tperiod/dt);

  AdvectiveFlux2D_Uniform adv(1., 0.);
  ViscousFlux2D_None visc;
  Source2D_None source;

  NDPDEClass pde(time, adv, visc, source);

#if PERIODIC
  // BC
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups = {};

  BCParams::checkInputs(PyBCList);

  //const std::vector<int> BoundaryGroups = {};
#else
  // BC
  PyDict BCNeumann;
  BCNeumann[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 0;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 1;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCDirichlet_high;
  BCDirichlet_high[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichlet_high[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCDirichlet_high[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCDirichlet_high[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCDirichlet_low;
  BCDirichlet_low[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichlet_low[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCDirichlet_low[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCDirichlet_low[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict PyBCList;

   PyBCList["BCDirichlet_high"] = BCDirichlet_high;
   PyBCList["BCDirichlet_low"] = BCDirichlet_low;
   PyBCList["BCNeumann"] = BCNeumann;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;



   // Define Boundary Groups for each Boundary Condition
   BCBoundaryGroups["BCNeumann"] = {0,1};

   BCBoundaryGroups["BCDirichlet_high"] ={2};  //XField3D_Box_Tet_X1::iXmin};
   BCBoundaryGroups["BCDirichlet_low"] ={3};  //XField3D_Box_Tet_X1::iXmax};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};
#endif

  //Set up IC sine waves
  PyDict solnArgs;
  GlobalTime temp_time;
  temp_time = 0.25; //HACK to avoid initialization to zero
  SolutionNDClass solnExact(temp_time, solnArgs);

  int RKtype;
  int RKmin, RKmax;

  RKtype = 0;

  RKmin = 2;
  RKmax = 2;

  for (int RKorder = RKmin; RKorder <= RKmax; RKorder++)  //temporal order
  {
    int RKstages = RKorder;

    int ordermin = 1;
    int ordermax = 1;

    for (int order = ordermin; order <= ordermax; order++) //spatial order
    {
      int ii, jj;
      int powermin = 3;
      int powermax = 3;

      for (int power = powermin; power <= powermax; power++) //grid resolution
      {
        ii = pow( 2, power );
        jj = ii;

        Real gscale;
        Real xmin, xmax, ymin, ymax;

        xmin = -1.;
        xmax = 1.;
        ymin = -1.;
        ymax = 1.;

        gscale = 1.;

        xmin = xmin*gscale;
        xmax = xmax*gscale;
        ymin = ymin*gscale;
        ymax = ymax*gscale;


        //create grid: 2D periodic mesh
#if PERIODIC
#if PARALLEL
        // global communicator
        mpi::communicator world;
        XField2D_Box_Triangle_Lagrange_X1 xfld(world,ii,jj,xmin,xmax,ymin,ymax);
#else
        XField2D_BoxPeriodic_Triangle_X1 xfld(ii, jj, xmin, xmax, ymin, ymax);
#endif
#else
        // global communicator
        mpi::communicator world;
        XField2D_Box_Triangle_Lagrange_X1 xfld(world,ii,jj, xmin,xmax,ymin,ymax);

        std::cout<<"****************************World Rank:"<< world.rank() << std::endl;

#endif
        //XField2D_BoxPeriodic_Triangle_X1 xfld(ii, jj);

        std::vector<int> interiorTraceGroups = linspace(0,xfld.nInteriorTraceGroups()-1);

        //integration
        QuadratureOrder quadratureOrder(xfld, -1);
        std::vector<Real> tol = {1e-9, 1e-9};

        //create solution
        Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

        // Set the initial condition
        for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );

        // Lagrange multiplier:
        Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
          lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
        lgfld = 0;

        //spatial discretization
        AlgebraicEquationSet_DGClass AlgEqSetSpace(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                                   {0}, interiorTraceGroups, PyBCList, BCBoundaryGroups, time);

        //temporal discretiation
        RKClass RK(RKorder, RKstages, RKtype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace);
        //BDFClass RK(RKorder, dt, time, xfld, qfld, NonLinearSolverDict, pde, {0}, AlgEqSetSpace);

#if 1
        //See initial condition
        int fieldorder = 3;
#if PERIODIC
        string filename = "tmp/solnDG_AD_P";
#else
        string filename = "tmp/nonPeriodic_solnDG_AD_P";
#endif
        filename += to_string(order);
        filename += "_Q";
        filename += to_string(fieldorder);
        filename += "_";
        filename += to_string(ii);
        filename += "x";
        filename += to_string(jj);
        filename += ".plt";
        output_Tecplot( qfld, filename );
#endif

        std::cout<< "Steps " << nsteps << std::endl;

        // Advance the solution
        RK.march(5);

#if 1
        // See final condition
        int fieldorder_aft = 3;
#if PERIODIC
        string filename_aft = "tmp/solnDG_aft_AD_P";
#else
        string filename_aft = "tmp/nonPeriodic_solnDG_aft_AD_P";
#endif
        filename_aft += to_string(order);
        filename_aft += "_Q";
        filename_aft += to_string(fieldorder_aft);
        filename_aft += "_";
        filename_aft += to_string(ii);
        filename_aft += "x";
        filename_aft += to_string(jj);
        filename_aft += ".plt";
        output_Tecplot( qfld, filename_aft );
#endif

      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
