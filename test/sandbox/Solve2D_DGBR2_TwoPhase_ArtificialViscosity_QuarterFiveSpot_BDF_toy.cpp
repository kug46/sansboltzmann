// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_DGBR2_TwoPhase_QuarterFiveSpot_BDF_btest
// Testing of the MOESS framework on a time-marching two-phase problem

#define USE_PETSC_SOLVER

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"
#include "tools/timer.h"

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/OutputTwoPhase.h"
#include "pde/PorousMedia/Sensor_TwoPhase.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/VectorFunction2D.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_Lagrange_X1.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#include "Field/output_Tecplot_PDE.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_TwoPhase_QuarterFiveSpot_BDF_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_TwoPhase_QuarterFiveSpot_BDF )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelTwoPhaseClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                            TraitsModelTwoPhaseClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTwoPhaseArtificialViscosity2DVector<TraitsSizeTwoPhaseArtificialViscosity,
                                                TraitsModelAV> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhaseArtificialViscosity,
                                                TraitsModelTwoPhaseClass> SourceOutflowClass;
  typedef SourceOutflowClass::ParamsType SourceOutflowParamClass;

  typedef OutputTwoPhase2D_Flowrate<SourceOutflowClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_GenH_DG ParamBuilderType;
  typedef GenHField_DG<PhysD2, TopoD2> GenHFieldType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, ParamFieldType> BDFClass;

  mpi::communicator world;

  GlobalTime time(0);

  timer clock;

  const Real Lx = 3000.0; //ft
  const Real Ly = Lx; //ft
  const Real T = 4000.0; //days

  int order = 1;

  const int Nx = 33;
  const int Ny = Nx;
  const int Nt = 312;

  //BDF parameters
  Real dt = T/Nt;
  int BDForder = order + 1;

  bool exportSolutions = false;
//  std::string filename_base = "tmp/twophase_hetero_AV_BDF/L3k_T2_5k_P" + to_string(order)
//                              + "_BDF" + to_string(BDForder) + "_Nx" + to_string(Nx) + "_Nt" + to_string(Nt) + "/";
  std::string filename_base = "tmp/";

  std::ofstream fwellhist(filename_base + "wellrate1.dat");
  BOOST_REQUIRE_MESSAGE(fwellhist.good(), "Error opening file: " + filename_base + "wellrate1.dat");
  if (world.rank() == 0)
    fwellhist << std::scientific << std::setprecision(15);

  // Grid
  typedef XField2D_Box_Quad_Lagrange_X1 XFieldClass;
  XFieldClass xfld( world, Nx, Ny, 0, Lx, 0, Ly );

  const int iXmin = XFieldClass::iLeft;
  const int iXmax = XFieldClass::iRight;
  const int iYmin = XFieldClass::iBottom;
  const int iYmax = XFieldClass::iTop;

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200; //mD

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kref, 0.0}, {0.0, Kref}};

#if 1 //Heterogeneous block
//  RockPermModel::QuadBlock quadblock0 = {{RockPermModel::Coord({{ 800.0, 1800.0}}),
//                                          RockPermModel::Coord({{1800.0,  800.0}}),
//                                          RockPermModel::Coord({{2200.0, 1200.0}}),
//                                          RockPermModel::Coord({{1200.0, 2200.0}})}};

    RockPermModel::QuadBlock quadblock0 = {{RockPermModel::Coord({{2000.0, 1000.0}}),
                                            RockPermModel::Coord({{2300.0, 1300.0}}),
                                            RockPermModel::Coord({{1100.0, 2200.0}}),
                                            RockPermModel::Coord({{ 800.0, 1400.0}})}};

  RockPermModel K(Kref_mat, {quadblock0}, {Kref_mat/100.0});
#elif 0 //Cosine-wave
  const Real Kref_cos = conversion*100; //mD
  const Real Kratio = 100.0;
  const Real amp = 0.05;
  const Real freq = 2.0;
  const Real width = 0.05;
  const Real yoffset = 0.5;
  RockPermModel K(Kref_cos, Kratio, Lx, Ly, amp, freq, width, yoffset);

  if (world.rank() == 0)
  {
    std::cout << "Kref_cos: " << Kref_cos / conversion << " mD" << std::endl;
    std::cout << "Kratio: " << Kratio << std::endl;
    std::cout << "Amplitude: " << amp << std::endl;
    std::cout << "Frequency: " << freq << std::endl;
    std::cout << "Width: " << width << std::endl;
    std::cout << "y-offset: " << yoffset << std::endl << std::endl;
  }
#else
  RockPermModel K(Kref_mat);
#endif

  const Real pc_max = 0.0;
  CapillaryModel pc(pc_max);

  // Set up source term PyDicts

  Real R_bore = 1.0/6.0; //ft
  Real R_well = 100.0; //ft
  Real L_offset = 500.0; //ft

  Real pnIn = 4000.0; //psi
  Real SwIn = 1.0;
  Real pnOut = 2000.0; //psi

  Real pwInit = 3000;
  Real SwInit = 0.1;
  Real nuInit = 1.0;

  const int nWellParam = 6;

  PyDict well_in;
  typedef SourceTwoPhase2DType_FixedPressureInflow_Param SourceInflowParamClass;
  well_in[SourceInflowParamClass::params.pB] = pnIn;
  well_in[SourceInflowParamClass::params.Sw] = SwIn;
  well_in[SourceInflowParamClass::params.Rwellbore] = R_bore;
  well_in[SourceInflowParamClass::params.nParam] = nWellParam;
  well_in[SourceInflowParamClass::params.WellModel] = SourceInflowParamClass::params.WellModel.Polynomial;
  well_in[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureInflow;

  PyDict well_out;
  well_out[SourceOutflowParamClass::params.pB] = pnOut;
  well_out[SourceOutflowParamClass::params.Rwellbore] = R_bore;
  well_out[SourceOutflowParamClass::params.nParam] = nWellParam;
  well_out[SourceOutflowParamClass::params.WellModel] = SourceInflowParamClass::params.WellModel.Polynomial;
  well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

  PyDict source_injection;
  source_injection[SourceTwoPhase2DParam::params.Source] = well_in;
  source_injection[SourceTwoPhase2DParam::params.xcentroid] = L_offset;
  source_injection[SourceTwoPhase2DParam::params.ycentroid] = L_offset;
  source_injection[SourceTwoPhase2DParam::params.R] = R_well;
  source_injection[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_injection[SourceTwoPhase2DParam::params.Tmax] = T;
  source_injection[SourceTwoPhase2DParam::params.smoothLr] = 0.0*R_well;
  source_injection[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_injection[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_production;
  source_production[SourceTwoPhase2DParam::params.Source] = well_out;
  source_production[SourceTwoPhase2DParam::params.xcentroid] = Lx - L_offset;
  source_production[SourceTwoPhase2DParam::params.ycentroid] = Ly - L_offset;
  source_production[SourceTwoPhase2DParam::params.R] = R_well;
  source_production[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_production[SourceTwoPhase2DParam::params.Tmax] = T;
  source_production[SourceTwoPhase2DParam::params.smoothLr] = 0.0*R_well;
  source_production[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_production[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_list;
  source_list["injection_well"] = source_injection;
  source_list["production_well"] = source_production;

  SourceTwoPhase2DListParam::checkInputs(source_list);

  // TwoPhase PDE with AV
  bool hasSpaceTimeDiffusionTwoPhase = false;
  PDEBaseClass pdeTwoPhaseAV(order, hasSpaceTimeDiffusionTwoPhase, rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list);

  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  bool hasSpaceTimeDiffusionAV = false;
  const Real diffusionConstantAV = 3.0;
  SensorViscousFlux sensor_visc(order, hasSpaceTimeDiffusionAV, 1.0, diffusionConstantAV);
  const Real Sk_min = -3;
  const Real Sk_max = -1;
  SensorSource sensor_source(pdeTwoPhaseAV, Sk_min, Sk_max);

  // AV PDE with sensor equation
  bool isSteadyAV = true;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteadyAV,
                 order, hasSpaceTimeDiffusionTwoPhase, rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list);

  // Initial solution
  ArrayQ qInit;
  AVVariable<PressureNonWet_SaturationWet,Real> qdata({pwInit, SwInit}, nuInit);
  pde.setDOFFrom( qInit, qdata );

  typedef BCmitAVSensor2DParams<BCTypeFlux_mitState, BCTwoPhase2DParams<BCTypeNoFlux>> BCParamsNoFlux;

  PyDict BCNoFlux;
  BCNoFlux[BCParams::params.BC.BCType] = BCParams::params.BC.NoFlux;
  BCNoFlux[BCParamsNoFlux::params.Cdiff] = diffusionConstantAV;

  PyDict PyBCList;
  PyBCList["BCNoFlux"] = BCNoFlux;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoFlux"] = {iXmin,iXmax,iYmin,iYmax};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 8;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  std::vector<Real> tol = {1e-10, 1e-10};

  //Output functional
  bool computeMassFlow = false;
  SourceOutflowClass source(source_production, rhow, rhon, krw, krn, muw, mun, K, pc, computeMassFlow);
  std::vector<SourceOutflowClass> sourceList = { source };
  int phase = 1;
  NDOutputClass fcnOutput(sourceList, phase);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LineUpdateDict;

#if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+1); //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  if (world.rank() == 0) std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.25;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  // Check inputs
  NonLinearSolverParam::checkInputs(NonlinearSolverDict);

  const int quadOrder = 2*(order + 2);

  //Print case summary
  if (world.rank() == 0)
  {
    std::cout << "Case parameters" << std::endl;
    std::cout << "---------------" << std::endl;
    std::cout << "p-order: " << order << std::endl;
    std::cout << "Processors: " << world.size() << std::endl;
    std::cout << "pc_max: " << pc_max << std::endl;
    std::cout << "nWellParam: " << nWellParam << std::endl;
    std::cout << "R_bore: " << R_bore << std::endl;
    std::cout << "R_well: " << R_well << std::endl;
    std::cout << "pnIn: " << pnIn << std::endl;
    std::cout << "pnOut: " << pnOut << std::endl;
    std::cout << "hasSpaceTimeDiffusionTwoPhase: " << hasSpaceTimeDiffusionTwoPhase << std::endl;
    std::cout << "hasSpaceTimeDiffusionAV: " << hasSpaceTimeDiffusionAV << std::endl;
    std::cout << "diffusionConstantAV: " << diffusionConstantAV << std::endl;
    std::cout << "Sk_min, Sk_max: " << Sk_min << ", " << Sk_max << std::endl;
    std::cout << "isSteadyAV: " << isSteadyAV << std::endl;
    std::cout << "viscousEta: " << viscousEtaParameter << std::endl;
    std::cout << "Residual tol: " << tol[0] << ", " << tol[1] << std::endl;
    std::cout << "Quadrature order: " << quadOrder << std::endl;
    std::cout << "maxResGrowthFactor: " << LineUpdateDict.get(HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor) << std::endl;
#if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
    std::cout << "ILU fill level: " << PreconditionerILU.get(SLA::PreconditionerILUParam::params.FillLevel) << std::endl;
    std::cout << "ILU ordering: " << PreconditionerILU.get(SLA::PreconditionerILUParam::params.Ordering) << std::endl;
    std::cout << "GMRES restart: " << PETScDict.get(SLA::PETScSolverParam::params.GMRES_Restart) << std::endl;
    std::cout << "PETSc RelativeTolerance: " << PETScDict.get(SLA::PETScSolverParam::params.RelativeTolerance) << std::endl;
    std::cout << "PETSc MaxIterations: " << PETScDict.get(SLA::PETScSolverParam::params.MaxIterations) << std::endl;
#endif
    std::cout << "---------------" << std::endl << std::endl;
  }

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Compute generalized log H-tensor field
  GenHFieldType Hfld(xfld);

  //Solution data
  SolutionClass sol((Hfld, xfld), pde, order, 0,
                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre, active_boundaries, disc);

  //Create a field to store the lifted quantity required for shock-capturing
  sol.createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

  //Set initial condition
  sol.setSolution(qInit);

  QuadratureOrder quadratureOrder( xfld, quadOrder );

  // Create AlgebraicEquationSets
  PrimalEquationSetClass AlgEqSetSpace(sol.paramfld, sol.primal, sol.pliftedQuantityfld, pde, disc,
                                       quadratureOrder, ResidualNorm_Default, tol,
                                       cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, time);

  // The BDF class
  BDFClass BDF( BDForder, dt, time, sol.paramfld, sol.primal.qfld, NonlinearSolverDict, pde, quadratureOrder, cellGroups, AlgEqSetSpace );

#if 0 //Eigenvector dump
  Field_DG_Cell<PhysD2,TopoD2,ArrayQ> Vfld(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2,TopoD2,ArrayQ> VTfld(xfld, order, BasisFunctionCategory_Hierarchical);
  std::fstream fVfld("tmp/Vmin.txt", std::fstream::in);
  std::fstream fVTfld("tmp/VTmin.txt", std::fstream::in);
  for (int i = 0; i < Vfld.nDOF(); i++)
    for (int j = 0; j < ArrayQ::M; j++)
    {
      fVfld >> Vfld.DOF(i)[j];
      fVTfld >> VTfld.DOF(i)[j];
    }
  fVfld.close();
  fVTfld.close();

  output_Tecplot( Vfld, "tmp/Vfld.plt" );
  output_Tecplot( VTfld, "tmp/VTfld.plt" );
#endif

  std::vector<Real> output_hist(Nt+1, 0.0);

  // Set IC
  time = 0.0;

#if 0
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  // residual
  SystemVectorClass q(AlgEqSetSpace.vectorStateSize());
  AlgEqSetSpace.fillSystemVector(q);

  SystemVectorClass rsd(AlgEqSetSpace.vectorEqSize());
  rsd = 0;
  AlgEqSetSpace.residual(rsd);

  for (int i = 0; i < sol.primal.qfld.nDOF(); i++)
  {
    std::cout << "i: " << i << ", " << rsd(0)[i] << std::endl;
  }

  std::cout << std::setprecision(5) << std::scientific << AlgEqSetSpace.residualNorm(rsd) << std::endl;

#if 0
  // jacobian nonzero pattern
  SystemNonZeroPattern nz(AlgEqSetSpace.matrixSize());
  AlgEqSetSpace.jacobian(q, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  jac = 0;
  AlgEqSetSpace.jacobian(q, jac);


  fstream fout( "tmp/jac.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif
#endif

#if 1
  for (int step = 0; step < Nt; step++)
  {
    if (world.rank() == 0)
      std::cout << "Step: " << step << ", Time = " << time << std::endl;

    if (exportSolutions && (step % 50 == 0))
    {
      // Tecplot dump grid
      string filename = filename_base + "timeDGBR2_TwoPhaseAV_P";
      filename += to_string(order);
      filename += "_n";
      filename += to_string(step);
      filename += ".plt";
//      output_Tecplot(pde, sol.paramfld, sol.primal.qfld, sol.primal.rfld, filename, {"pn", "Sw", "nu"});
      output_Tecplot( sol.primal.qfld, filename, {"pn", "Sw", "nu"} );

      string filename_liftedquantity = filename_base + "liftedfld_n" + to_string(step) + ".plt";
      output_Tecplot( *sol.pliftedQuantityfld, filename_liftedquantity );
    }

    //Write well data
    if (world.rank() == 0)
      fwellhist << time << ", " << output_hist[step] << std::endl;

    // March the solution in time
    BDF.march(1);

    //Compute output
    IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2( outputIntegrand, output_hist[step+1] ),
                                            xfld, (sol.primal.qfld, sol.primal.rfld),
                                            quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#ifdef SANS_MPI
    output_hist[step+1] = boost::mpi::all_reduce( *get<-1>(xfld).comm(), output_hist[step+1], std::plus<Real>() );
#endif
  }

  if (world.rank() == 0)
    std::cout << "Time = " << time << std::endl;

  if (exportSolutions)
  {
    // Tecplot dump grid
    string filename = filename_base + "timeDGBR2_TwoPhaseAV_P";
    filename += to_string(order);
    filename += "_n";
    filename += to_string(Nt);
    filename += ".plt";
//    output_Tecplot(pde, sol.paramfld, sol.primal.qfld, sol.primal.rfld, filename, {"pn", "Sw", "nu"});
    output_Tecplot( sol.primal.qfld, filename, {"pn", "Sw", "nu"} );

    string filename_liftedquantity = filename_base + "liftedfld_n" + to_string(Nt) + ".plt";
    output_Tecplot( *sol.pliftedQuantityfld, filename_liftedquantity );
  }

  //Write well data
  if (world.rank() == 0)
  {
    fwellhist << time << ", " << output_hist[Nt] << std::endl;
    fwellhist.close();

    //Integrate flowrates to obtain output volume
    Real output_vol = 0.0;
    for (int n = 1; n <= Nt; n++)
      output_vol += output_hist[n]*dt;

    std::cout << std::scientific << std::setprecision(10);
    std::cout << "Volume produced (ft^3) = " << output_vol << std::endl;

    Real runtime = clock.elapsed();
    std::cout<<"Time elapsed: " << std::fixed << std::setprecision(3) << runtime << "s" << std::endl;

    std::cout << std::scientific << std::setprecision(10);
    std::cout << Nx << ", " << Ny << ", " << Nt << ", " << output_vol << ", " << runtime << std::endl;
  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
