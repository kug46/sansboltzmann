// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_DGBR2_RANS_ArtificialViscosity_ONERA_M6_toy

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/TraitsRANSSAArtificialViscosity.h"
#include "pde/NS/PDERANSSA3D.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/PDEEulermitAVDiffusion3D.h"
#include "pde/NS/BCRANSSA3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/BCEulermitAVSensor3D.h"
#include "pde/NS/Fluids3D_Sensor.h"

#include "pde/NS/OutputEuler3D.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field/FieldVolume_CG_Cell.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

template <class T>
using SAnt3D_rhovP = SAnt3D<DensityVelocityPressure3D<T>>;


}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_DGBR2_RANS_ArtificialViscosity_ONERA_M6_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_DGBR2_RANS_ArtificialViscosity_ONERA_M6_test )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;

#if 0
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEClass PDEBaseClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef ParamType_Distance ParamBuilderType;
#else

//  typedef PDERANSSA3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass> PDEBaseClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD3, PDEBaseClass> Sensor;
  //typedef AVSensor_Source3D_PressureGrad<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;
  typedef AVSensor_Source3D_Jump<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCRANSSAmitAVDiffusion3DVector<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> BCVector;

  typedef ParamType_GenH_DG_Distance ParamBuilderType;
#endif

  typedef BCParameters<BCVector> BCParams;

  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Force<PDEClass>> NDOutputForce;
  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Moment<PDEClass>> NDOutputMoment;
  typedef NDVectorCategory<boost::mpl::vector2<NDOutputForce, NDOutputMoment>, NDOutputForce::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;

  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef SolutionData_DGBR2<PhysDim, TopoDim, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;


  mpi::communicator world;

  // initial Grid
#if 1
  std::string filename_base = "tmp/OneraM6/EPIC/";
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld =
      std::make_shared<XField_PX<PhysD3, TopoD3>>(world, "tmp/OneraM6/oneram6.q2.grm" );
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld_linear =
      std::make_shared<XField_PX<PhysD3, TopoD3>>(world, "tmp/OneraM6/oneram6.q1.grm" );
#else
  std::string filename_base = "tmp/OneraM6/wing_tetra_5/";
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld =
      std::make_shared<XField_libMeshb<PhysD3, TopoD3>>(world, "tmp/OneraM6/wing_tetra_5/wing_tetra.5.meshb" );
#endif

  int orderMax = 1;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // Sutherland viscosity
  const Real tSuth = 198.6/540;     // R/R
  //const Real tRef  = 491.6/540;     // R/R

  // reference state (freestream)
  const Real Mach = 0.84;
  const Real Reynolds = 1.46e7;
  //const Real Mach = 0.1;
  //const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                         // length scale
  const Real MAC = 0.801672958512342;          // mean aerodynamic chord for moments
  const Real SRef = 1.15315084119231;          // area scale
  const Real rhoRef = 1;                       // density scale
  const Real aoaRef = 3.06 * PI / 180.0;       // angle of attack (radians)

#if 1
  const Real tRef = 1;          // temperature
  const Real pRef = rhoRef*R*tRef;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;
#endif

#if 0
  const Real qRef = 1;                              // velocity scale
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = 0;
  const Real wRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef + wRef*wRef);

  const Real sensorRef = 0.0;

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);

  // DGBR2 discretization
  Real viscousEtaParameter = 2*Tet::NFace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  typedef BCRANSSA3D<BCTypeFullState_mitState, BCEuler3D<BCTypeFullState_mitState, PDEBaseClass>> BCBase;
  typedef BCmitAVSensor3D<BCTypeFlux_mitState, BCBase> BCClassFullState;

  PyDict StateVector;
  StateVector[SAVariableType3DParams::params.StateVector.Variables] = SAVariableType3DParams::params.StateVector.Conservative;
  StateVector[Conservative3DParams::params.rho] = rhoRef;
  StateVector[Conservative3DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative3DParams::params.rhov] = rhoRef*vRef;
  StateVector[Conservative3DParams::params.rhow] = rhoRef*wRef;
  StateVector[Conservative3DParams::params.rhoE] = rhoRef*ERef;
  StateVector[SAnt3DParams<Conservative3DParams>::params.rhont]  = rhoRef*ntRef;

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;
  //BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCFarField;
  BCFarField[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFarField[BCClassFullState::ParamsType::params.StateVector] = StateVector;
#if 1
  BCFarField[BCClassFullState::ParamsType::params.sensor] = sensorRef;
#endif
  BCFarField[BCClassFullState::ParamsType::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCWall"]     = BCWall;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCFarField"] = BCFarField;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
#if 1
  BCBoundaryGroups["BCWall"]     = {2,3,4,5,6,7,8,9,10,11};
  BCBoundaryGroups["BCSymmetry"] = {1};
  BCBoundaryGroups["BCFarField"] = {0};
#else
  BCBoundaryGroups["BCWall"]     = {0};
  BCBoundaryGroups["BCSymmetry"] = {1};
  BCBoundaryGroups["BCFarField"] = {2};
#endif

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {5e-7, 5e-7};

  // Residual weighted boundary output
  const Real dynpRef = 0.5*rhoRef*(uRef*uRef + vRef*vRef + wRef*wRef);

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0)
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-9;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = true;
  NewtonSolverDict[NewtonSolverParam::params.MaxChangeFraction] = 0.1;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;

  NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_Euler_residual.dat";

#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  //--------ADAPTATION LOOP--------

  int maxIter = 30;
  int targetCost = 5e5;

  fstream fadapthist, foutputhist;
  if (world.rank() == 0)
  {
    std::string adapthist_filename = filename_base + "test.adapthist";
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

    std::string outputhist_filename = filename_base + "output.adapthist";
    foutputhist.open( outputhist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + outputhist_filename);
  }

  int orderDist = 2;

  PyDict ParamDict;

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

  PyDict MesherDict;
#if defined(SANS_REFINE) && USE_REFINE
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.refine;
  MesherDict[refineParams::params.FilenameBase] = filename_base;
  //MesherDict[refineParams::params.CADfilename] = filename_base + "hemisph-cyl.egads";
  //MesherDict[refineParams::params.GASFilename] = filename_base + "hsc01.gas";
  //MesherDict[refineParams::params.DumpRefineDebugFiles] = true;
#else
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
  MesherDict[EpicParams::params.Version] = "madcap_devel-10.2";
  MesherDict[EpicParams::params.CurvedQOrder] = 2; //Mesh order
  MesherDict[EpicParams::params.CSF] = "tmp/OneraM6/onera-m6-sharp-te_cf.csf"; //madcap surface file
  MesherDict[EpicParams::params.GeometryFile3D] = "tmp/OneraM6/onera-m6-sharp-te_cf.igs"; //CAD file
  MesherDict[EpicParams::params.Shell] = "SHELL";
  MesherDict[EpicParams::params.BoundaryObject] = "BDIST";
  MesherDict[EpicParams::params.nAdaptIter] = 15;
  //MesherDict[EpicParams::params.minGeom] = 1e-6;
  //MesherDict[EpicParams::params.maxGeom] = 2000;
  //MesherDict[EpicParams::params.nPointGeom] = -1;
  MesherDict[EpicParams::params.NodeMovement] = false;
  MesherDict[EpicParams::params.SymmetricSurf] = BCBoundaryGroups.at("BCSymmetry");
  MesherDict[EpicParams::params.ViscSurf] = BCBoundaryGroups.at("BCWall");
  MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;
#endif

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

  MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Compute generalized H-tensor field
  std::shared_ptr<GenHField_CG<PhysD3, TopoD3>> pHfld = std::make_shared<GenHField_CG<PhysD3, TopoD3>>(*pxfld);

  //Compute distance field
  std::shared_ptr<Field_CG_Cell<PhysD3, TopoD3, Real>>
    pdistfld = std::make_shared<Field_CG_Cell<PhysD3, TopoD3, Real>>(*pxfld, 1, BasisFunctionCategory_Lagrange);
  DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCWall"));

  std::string distfld_filename = filename_base + "distfld_a0_P"
                                               + std::to_string(orderDist)
                                               + "_rank" + std::to_string(world.rank()) + ".dat";
  output_Tecplot( *pdistfld, distfld_filename );

  //Solution data
//  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol, pGlobalSolNew;
  std::shared_ptr<SolverInterfaceClass> pInterface;

  bool isSensorSteady = false;

  for (int order = 0; order <= orderMax; order++)
  {

    // PDE
#if 0
    NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

    ArrayQ q0;
    pde.setDOFFrom( q0, SAnt3D<DensityVelocityPressure3D<Real>>({rhoRef, uRef, vRef, wRef, pRef, ntRef}) );
#else
    PDEBaseClass pdeRANSAV(order, gas, visc, tcond, Euler_ResidInterp_Raw );

    Sensor sensor(pdeRANSAV);
    SensorSource sensor_source(order, sensor, 1.0);

    SensorAdvectiveFlux sensor_adv(0.0, 0.0, 0.0);
    SensorViscousFlux sensor_visc(order);

    // AV PDE with sensor equation
    NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSensorSteady,
                   order, gas, visc, tcond, Euler_ResidInterp_Raw);

    // initial condition
    AVVariable<SAnt3D_rhovP,Real> qdata({rhoRef, uRef, vRef, wRef, pRef, ntRef}, sensorRef);
    ArrayQ q0;
    pde.setDOFFrom( q0, qdata );
#endif

    NDOutputForce outputFcnDrag(pde, cos(aoaRef)/(SRef*dynpRef), 0., sin(aoaRef)/(SRef*dynpRef));
    OutputIntegrandClass outputIntegrandDrag( outputFcnDrag, BCBoundaryGroups.at("BCWall") );

    NDOutputForce outputFcnLift(pde, -sin(aoaRef)/(SRef*dynpRef), 0., cos(aoaRef)/(SRef*dynpRef));
    OutputIntegrandClass outputIntegrandLift( outputFcnLift, BCBoundaryGroups.at("BCWall") );

    NDOutputMoment outputFcnMoment(pde, 0, 0, 0, 0, 1, 0, MAC);
    OutputIntegrandClass outputIntegrandMoment( outputFcnMoment, BCBoundaryGroups.at("BCWall") );


    pGlobalSolNew = std::make_shared<SolutionClass>((*pHfld, *pdistfld, *pxfld), pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, ParamDict, disc);
    pGlobalSolNew->createLiftedQuantityField(order, BasisFunctionCategory_Legendre);

    //Set initial solution
    if (order == 0)
    {
      pGlobalSol = pGlobalSolNew;
      pGlobalSol->setSolution(q0);
    }
    else
    {
      pGlobalSolNew->setSolution(*pGlobalSol);
      pGlobalSol = pGlobalSolNew;
    }

    // Adjoint equation is integrated with 3 times adjoint order
    const int quadOrder = std::min(13, 3*order+3);

    //Create solver interface
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, LinearSolverDict,
                                                        outputIntegrandDrag);

    //Set initial solution
    std::string qfld_init_filename = filename_base + "qfld_init_a0_P" + std::to_string(order)
                                                   + "_rank" + std::to_string(world.rank()) + ".dat";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();

    //----------//
    QuadratureOrder quadrule(*pxfld, quadOrder);

    Real outputLift = 0;
    pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
      FunctionalBoundaryTrace_Dispatch_DGBR2(outputIntegrandLift, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
                                             quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputLift ) );

#ifdef SANS_MPI
    outputLift = boost::mpi::all_reduce( *pxfld->comm(), outputLift, std::plus<Real>() );
#endif

    Real outputMoment = 0;
    pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
      FunctionalBoundaryTrace_Dispatch_DGBR2(outputIntegrandMoment, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
                                             quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputMoment ) );

#ifdef SANS_MPI
    outputMoment = boost::mpi::all_reduce( *pxfld->comm(), outputMoment, std::plus<Real>() );
#endif

    if (pxfld->comm()->rank() == 0)
    {
      if (order == 0)
        foutputhist << std::setw(5)  << "Iter"
                    << std::setw(5)  << "P"
                    << std::setw(20) << "Lift"
                    << std::setw(20) << "Drag"
                    << std::setw(20) << "Moment"
                    << std::endl;

      foutputhist << std::setw(5) << 0
                  << std::setw(5) << order
                  << std::setw(20) << std::setprecision(10) << std::scientific << outputLift
                  << std::setw(20) << std::setprecision(10) << std::scientific << pInterface->getOutput()
                  << std::setw(20) << std::setprecision(10) << std::scientific << outputMoment
                  << std::endl;
    }
    //----------//

    std::string qfld_filename = filename_base + "qfld_a0_P" + std::to_string(order)
                                              + "_rank" + std::to_string(world.rank()) + ".dat";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
  }

  pInterface->solveGlobalAdjointProblem();

  std::string adjfld_filename = filename_base + "adjfld_a0_P" + std::to_string(orderMax)
                                              + "_rank" + std::to_string(world.rank()) + ".dat";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    if (world.rank() == 0 )
      std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
#if 0
    MeshAdapter<PhysD3, TopoD3>::MeshPtrPair ptr_pair;
    ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew_linear = ptr_pair.first;
    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew = ptr_pair.second;
#else
    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);
#endif

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Compute generalized H-tensor field
    pHfld = std::make_shared<GenHField_CG<PhysD3, TopoD3>>(*pxfldNew);

    //Compute distance field
    pdistfld = std::make_shared<Field_CG_Cell<PhysD3, TopoD3, Real>>(*pxfldNew, orderDist, BasisFunctionCategory_Lagrange);
    DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCWall"));

    for (int order = 1; order <= orderMax; order++)
    {
      // PDE
#if 0
      NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

      ArrayQ q0;
      pde.setDOFFrom( q0, SAnt3D<DensityVelocityPressure3D<Real>>({rhoRef, uRef, vRef, wRef, pRef, ntRef}) );
#else
       PDEBaseClass pdeRANSAV(order, gas, visc, tcond, Euler_ResidInterp_Raw );

       Sensor sensor(pdeRANSAV);
       SensorSource sensor_source(order, sensor);

       SensorAdvectiveFlux sensor_adv(0.0, 0.0, 0.0);
       SensorViscousFlux sensor_visc(order);

       // AV PDE with sensor equation
       NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSensorSteady,
                      order, gas, visc, tcond, Euler_ResidInterp_Raw);
#endif

      NDOutputForce outputFcnDrag(pde, cos(aoaRef)/(SRef*dynpRef), 0., sin(aoaRef)/(SRef*dynpRef));
      OutputIntegrandClass outputIntegrandDrag( outputFcnDrag, BCBoundaryGroups.at("BCWall") );

      NDOutputForce outputFcnLift(pde, -sin(aoaRef)/(SRef*dynpRef), 0., cos(aoaRef)/(SRef*dynpRef));
      OutputIntegrandClass outputIntegrandLift( outputFcnLift, BCBoundaryGroups.at("BCWall") );

      NDOutputMoment outputFcnMoment(pde, 0, 0, 0, 0, 1, 0, MAC);
      OutputIntegrandClass outputIntegrandMoment( outputFcnMoment, BCBoundaryGroups.at("BCWall") );




      pGlobalSolNew = std::make_shared<SolutionClass>((*pHfld, *pdistfld, *pxfldNew), pde, order, order+1,
                                                      BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                      active_boundaries, ParamDict, disc);
      pGlobalSolNew->createLiftedQuantityField(order, BasisFunctionCategory_Legendre);

      // Adjoint equation is integrated with 3 times adjoint order
      const int quadOrder = std::min(13, 3*order+3);

      //Perform L2 projection from solution on previous mesh
      pGlobalSolNew->setSolution(*pGlobalSol);
      pGlobalSol = pGlobalSolNew;

      std::string distfld_filename = filename_base + "distfld_a" + std::to_string(iter+1)
                                                   + "_P" + std::to_string(orderDist)
                                                   + "_rank" + std::to_string(world.rank()) + ".dat";
      output_Tecplot( pGlobalSol->parambuilder.distfld, distfld_filename );

      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrandDrag);

      std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1)
                                                     + "_P" + std::to_string(order) +
                                                     + "_rank" + std::to_string(world.rank()) + ".dat";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

      pInterface->solveGlobalPrimalProblem();


      //----------//
      QuadratureOrder quadrule(*pxfld, quadOrder);

      Real outputLift = 0;
      pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
        FunctionalBoundaryTrace_Dispatch_DGBR2(outputIntegrandLift, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
                                               quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputLift ) );

  #ifdef SANS_MPI
      outputLift = boost::mpi::all_reduce( *pxfld->comm(), outputLift, std::plus<Real>() );
  #endif

      Real outputMoment = 0;
      pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
        FunctionalBoundaryTrace_Dispatch_DGBR2(outputIntegrandMoment, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
                                               quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputMoment ) );

  #ifdef SANS_MPI
      outputMoment = boost::mpi::all_reduce( *pxfld->comm(), outputMoment, std::plus<Real>() );
  #endif

      if (pxfld->comm()->rank() == 0)
      {
        foutputhist << std::setw(5) << iter+1
                    << std::setw(5) << order
                    << std::setw(20) << std::setprecision(10) << std::scientific << outputLift
                    << std::setw(20) << std::setprecision(10) << std::scientific << pInterface->getOutput()
                    << std::setw(20) << std::setprecision(10) << std::scientific << outputMoment
                    << std::endl;
      }
      //----------//


      std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1)
                                                + "_P" + std::to_string(order) +
                                                + "_rank" + std::to_string(world.rank()) + ".dat";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
    }

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    //pxfld_linear = pxfldNew_linear;
    pxfld = pxfldNew;

    pInterface->solveGlobalAdjointProblem();

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1)
                                                + "_P" + std::to_string(orderMax) +
                                                + "_rank" + std::to_string(world.rank()) + ".dat";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
