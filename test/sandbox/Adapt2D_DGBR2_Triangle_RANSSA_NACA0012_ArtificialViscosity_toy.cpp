// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adapt2D_DGBR2_Triangle_RANSSA_Joukowski_ArtificialViscosity_toy

//#define SANS_FULLTEST
//#define SANS_VERBOSE
#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>

#include <boost/filesystem.hpp> // to automagically make the directories

#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsRANSSAArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"
#include "pde/NS/EulerArtificialViscosityType.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/DistanceFunction/DistanceFunction.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/EPIC/XField_PX.h"
#include "Field/output_grm.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


using namespace std;

namespace SANS
{

template <class T>
using SAnt2D_rhovP = SAnt2D<DensityVelocityPressure2D<T>>;

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Triangle_RANSSA_Joukowski_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Triangle_RANSSA_Joukowski )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;

  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCRANSSAmitAVDiffusion2DVector<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputRANSSA2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
#if 0
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef IntegrandCell_Galerkin_Output_DGBR2<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#endif
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_DG_Distance ParamBuilderType;
  typedef GenHField_DG<PhysD2, TopoD2> GenHFieldType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  std::string file_tag;
  bool dumpField = false;
  int order = 1;
  int targetCost = 32e3;
  int maxIter = 50;

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- file_tag dumpField order targetCost maxIter
  if (argc >= 2)
    file_tag = std::string(argv[1]);
  if (argc >= 3)
    dumpField = std::stoi(argv[2]);
  if (argc >= 4)
    order = std::stoi(argv[3]);
  if (argc >= 5)
    targetCost = std::stoi(argv[4]);
  if (argc >= 6)
    maxIter = std::stoi(argv[5]);

  if (world.rank() == 0)
  {
    std::cout << "file_tag: " << file_tag << ", dumpField: " << dumpField;
    std::cout << "order: " << order << ", targetCost: " << targetCost;
    std::cout << ", maxIter: " << maxIter << std::endl;
  }
#endif


  // to make sure folders have a consistent number of zero digits
  const int string_pad= 7;
  std::string int_pad= std::string(string_pad - std::to_string((int) targetCost).length(), '0') + std::to_string((int) targetCost);

  std::string filename_base;
  filename_base= "tmp/RANS_NACA0012/transonic/DGBR2_";

  if (file_tag.size() > 0 && file_tag != "_")
    filename_base += file_tag + "_"; // the additional file name bits kept especially

  filename_base += int_pad + "_P" + std::to_string(order) + "/";

  boost::filesystem::path base_dir(filename_base);
  if ( not boost::filesystem::exists(base_dir) )
    boost::filesystem::create_directories(filename_base);


  Real viscousEtaParameter = 2*Triangle::NEdge;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {8e-7, 5e-7};

  RoeEntropyFix entropyFix = eVanLeer;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = false;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const Real tRef = 1.0;          // temperature
  const Real pRef = 1.0;          // pressure
  const Real lRef = 1;            // length scale

  const Real Mach = 0.8;
  const Real Reynolds = 1e5;
  const Real Prandtl = 0.72;
  const Real chiRef = 3;

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;

//  const Real lRef = 1;                            // length scale
  const Real rhoRef = 1;                            // density scale
//  const Real qRef = 0.1;                          // velocity scale
  const Real aoaRef = 1.25 * M_PI / 180.0;          // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  PDEBaseClass pdeRANSSAAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                                 gas, visc, tcond, interp, entropyFix);
  // Sensor equation terms
  Sensor sensor(pdeRANSSAAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(order);
  SensorSource sensor_source(order, sensor);
  // AV PDE with sensor equation
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order,
                 hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                 gas, visc, tcond, interp, entropyFix);

  // initial condition
  AVVariable<SAnt2D_rhovP,Real> qdata({rhoRef, uRef, vRef, pRef, ntRef}, 0.0);
  ArrayQ q0;
  pde.setDOFFrom( q0, qdata );

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;
  StateVector["rhont"]  = rhoRef*ntRef; //TODO: not sure about this, was previously just "nt" which gave a Python dictionary error

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCWall;
  PyBCList["BCIn"] = BCIn;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0,1};
  BCBoundaryGroups["BCIn"] = {2,3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else

  Real SRef = 1.0; // unit length chord
  Real dynpRef = 0.5*rhoRef*(uRef*uRef + vRef*vRef);

  // Drag coefficient output
  NDOutputClass outputFcn(pde, cos(aoaRef)/(SRef*dynpRef), sin(aoaRef)/(SRef*dynpRef));
  OutputIntegrandClass outputIntegrand( outputFcn, {0,1} );

  // // DRAG INTEGRAND
  // typedef BCRANSSA2D<BCTypeWall_mitState, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  // typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  // typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  // typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;
  //
  // NDBCClass bc(pde);
  // IntegrandBCClass fcnBC( pde, bc, BCBoundaryGroups["BCNoSlip"], disc );
  //
  // NDOutputClass outputFcn2(pde, -sin(aoaRef)/(SRef*dynpRef), cos(aoaRef)/(SRef*dynpRef));
  // OutputIntegrandClass outputIntegrand2( outputFcn2, BCBoundaryGroups["BCNoSlip"] );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;
  PETScDictAdjoint[SLA::PETScSolverParam::params.ResidualHistoryFile] = filename_base + "PETSc_adjoint_residual.dat";

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
    std::cout << "Linear solver: MKL_PARDISO" << std::endl;
    PyDict MKL_PARDISODict;
    MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;

#else
    std::cout << "Linear solver: UMFPACK" << std::endl;
    PyDict UMFPACKDict;
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 3;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
//  NewtonSolverDict[NewtonSolverParam::params.MaxChangeFraction] = 0.1;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 1e2;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;
  NonlinearSolverDict[PseudoTimeParam::params.SteveLogic] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  PyDict ParamDict;

  // Grid
  std::string file_initial_mesh= "grids/naca0012/naca0012_q3.grm";
  std::string file_initial_linear_mesh = "grids/naca0012/naca0012_q1.grm";

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField_PX<PhysD2, TopoD2>(world, file_initial_mesh) );
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld_linear( new XField_PX<PhysD2, TopoD2>(world, file_initial_linear_mesh) );

  //--------ADAPTATION LOOP--------
  int startIter= 0;

  std::string adapthist_filename = filename_base + "test.adapthist";

  // Graceful restart capability
  const bool RESTART = true;
  // read backwards from the end and look for the last file
  if (RESTART)
  {
    bool foundFile = false;
    std::string xfld_filename, xfld_linear_filename;
    if (world.rank() == 0)
      for (int read = maxIter; read >=-1; read--)
      {
        xfld_filename = filename_base + "mesh_a" + std::to_string(read) + "_outQ3.grm";
        xfld_linear_filename = filename_base + "mesh_a" + std::to_string(read) + "_outQ1.grm";
        foundFile = boost::filesystem::exists(xfld_filename) && boost::filesystem::exists(xfld_linear_filename);
        if (foundFile )
        {
          startIter = read+1;
          break; // found the file, exit the loop
        }
      }
#ifdef SANS_MPI
    boost::mpi::broadcast( world, xfld_filename, 0 ); // get the file to be read off of rank 0
    boost::mpi::broadcast( world, xfld_linear_filename, 0 ); // get the file to be read off of rank 0
    boost::mpi::broadcast( world, startIter, 0); // get the start iteration from rank 0
    boost::mpi::broadcast( world, foundFile, 0); // get the start iteration from rank 0
#endif

    // read the old grid back in
    if (foundFile )
    {
      // std::cout<< "rank: " << world.rank() << ", loading q1 grid" << std::endl;
      pxfld_linear = std::make_shared<XField_PX<PhysD2,TopoD2>>(world, xfld_linear_filename);
      // std::cout<< "rank: " << world.rank() << ", loaded q1 grid" << std::endl;
      // world.barrier();
      // std::cout<< "rank: " << world.rank() << ", loading q3 grid" << std::endl;
      pxfld = std::make_shared<XField_PX<PhysD2,TopoD2>>(world, xfld_filename);
      // std::cout<< "rank: " << world.rank() << ", loaded q3 grid" << std::endl;
    }
    else
    {
      // pxfld = pxfld_coarse;
    }
  }
  if (world.rank() == 0)
  {
    if (startIter == 0)
      std::cout << "\nStarting from Initial mesh\n" << std::endl;
    else
      std::cout << "\nRestarting from mesh " << std::to_string(startIter-1) << std::endl << std::endl;
  }


  fstream fadapthist;
  if (world.rank() == 0)
  {
    std::string adapthist_filename = filename_base + "test.adapthist";
    fadapthist.open( adapthist_filename, startIter > 0 ? fstream::app : fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;
  MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Element;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;
  MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
  MesherDict[EpicParams::params.CurvedQOrder] = 3; //Mesh order
  MesherDict[EpicParams::params.CSF]= "grids/naca0012/naca0012.csf"; // geometry file
  MesherDict[EpicParams::params.GeometryList2D]= "body2 freestream outflow"; // geometry list
  MesherDict[EpicParams::params.minGeom] = 1e-6;
  // MesherDict[EpicParams::params.maxGeom] = 0.5;
  MesherDict[EpicParams::params.nPointGeom] = -1;
  MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;
  MesherDict[EpicParams::params.nThread]= 3;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpMetric] = false;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpImpliedMetric] = true;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  // // Smooth out the initial grid a whole lot
  // MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;
  // for (int iter = 0; iter < 200; iter++)
  // {
  //   // // adapt to the implied metric!
  //   ptr_pair = mesh_adapter.adapt_implied_metric(*pxfld_linear, *pxfld, cellGroups, iter);
  //   pxfld_linear = ptr_pair.first;
  //   pxfld = ptr_pair.second;
  // }
  //
  // return;

  //Compute distance field
  std::shared_ptr<Field_CG_Cell<PhysD2, TopoD2, Real>>
    pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfld, 4, BasisFunctionCategory_Lagrange);
  DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));
  // H field
  std::shared_ptr<GenHFieldType> phfld = std::make_shared<GenHFieldType>(*pxfld);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;


  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pdistfld, *pxfld), pde, 0, 0+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, ParamDict, disc);

  //Set initial solution
  pGlobalSol->setSolution(q0);

  pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

  const int quadOrder = 3*(order + 1); // Is this high enough?

  std::shared_ptr<SolverInterfaceClass> pInterface;
  // p sequencing
  for (int orderSeq = 0; orderSeq <= order; orderSeq++)
  {
    if (world.rank() == 0) std::cout << "\n Psequencing: p = " << orderSeq << "\n\n";
    std::shared_ptr<SolutionClass> pGlobalSolSeq;
    pGlobalSolSeq = std::make_shared<SolutionClass>((*phfld, *pdistfld, *pxfld), pde, orderSeq, orderSeq+1,
                                                 BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                 active_boundaries, ParamDict, disc);
    pGlobalSolSeq->setSolution(*pGlobalSol); // initialize with the previous guess
    pGlobalSolSeq->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSolSeq, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, LinearSolverDict,
                                                        outputIntegrand);

    pInterface->solveGlobalPrimalProblem();
    pGlobalSol = pGlobalSolSeq; // take ownership of this solution
  }

  pInterface->solveGlobalAdjointProblem();
  //Compute error estimates
  pInterface->computeErrorEstimates();

  if (dumpField)
  {
    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(startIter) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(startIter) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

    std::string efld_filename = filename_base + "efld_a" + std::to_string(startIter) + ".plt";
    pInterface->output_EField(efld_filename);
  }

  for (int iter = startIter; iter < maxIter+1; iter++)
  {
    if (world.rank() == 0) std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Perform local sampling and adapt mesh
    MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;
    ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew_linear = ptr_pair.first;
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew = ptr_pair.second;

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Compute distance field
    pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfldNew, 4, BasisFunctionCategory_Lagrange);
    DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));
    // H field
    std::shared_ptr<GenHFieldType> phfld = std::make_shared<GenHFieldType>(*pxfldNew);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>((*phfld, *pdistfld, *pxfldNew), pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, ParamDict, disc);
    pGlobalSolNew->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    try
    {
      if (world.rank() == 0) std::cout << "\nUsing P1 solution transfer as initial guess.\n" << std::endl;
      // Do solution transfer with a p1 solution then prolongate instead.
      std::shared_ptr<SolutionClass> pGlobalSol_P1;
      pGlobalSol_P1 = std::make_shared<SolutionClass>((*phfld, *pdistfld, *pxfldNew), pde, 1, 1,
                                                       BasisFunctionCategory_Legendre,
                                                       BasisFunctionCategory_Legendre,
                                                       active_boundaries, ParamDict, disc);
      pGlobalSol_P1->setSolution(*pGlobalSol);
      pGlobalSolNew->setSolution(*pGlobalSol_P1);

      pInterfaceNew->solveGlobalPrimalProblem();
    }
    catch (...)
    {
      if (world.rank() == 0) std::cout << "\nUsing freestream as initial guess.\n" << std::endl;

      pGlobalSolNew->setSolution(q0);
      // p sequence it too
      for (int orderSeq = 0; orderSeq <= order; orderSeq++)
      {
        if (world.rank() == 0) std::cout << "\n Psequencing: p = " << orderSeq << "\n\n";
        std::shared_ptr<SolutionClass> pGlobalSolSeq;
        pGlobalSolSeq = std::make_shared<SolutionClass>((*phfld, *pdistfld, *pxfldNew), pde, orderSeq, orderSeq+1,
                                                     BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                     active_boundaries, ParamDict, disc);
        pGlobalSolSeq->setSolution(*pGlobalSolNew); // initialize with the previous guess
        pGlobalSolSeq->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolSeq, ResNormType, tol, quadOrder,
                                                                cellGroups, interiorTraceGroups,
                                                                PyBCList, BCBoundaryGroups,
                                                                SolverContinuationDict, LinearSolverDict,
                                                                outputIntegrand);

        pInterfaceNew->solveGlobalPrimalProblem();
        pGlobalSol = pGlobalSolSeq; // take ownership of this solution
      }
    }

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld_linear = pxfldNew_linear;
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    pInterface->solveGlobalAdjointProblem();
    //Compute error estimates
    pInterface->computeErrorEstimates();

    // Dump the drag coefficient to screen
    if (world.rank() == 0)
    {
      Real Cd = pInterface->getOutput();
      cout << "P = " << order << ", Cd = " << Cd
          << ", estimate = " << pInterface->getGlobalErrorEstimate();
      cout << endl;
    }

    if (dumpField)
    {
      std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

      std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".plt";
      pInterface->output_EField(efld_filename);
    }
    else if ( iter == maxIter )
    {
      std::string qfld_filename = filename_base + "qfld_final.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_final.plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

      std::string efld_filename = filename_base + "efld_final.plt";
      pInterface->output_EField(efld_filename);
    }

    // std::string xfld_linear_filename = filename_base + "xfld_linear_a" + std::to_string(iter+1) + ".grm";
    // std::string xfld_filename = filename_base + "xfld_a" + std::to_string(iter+1) + ".grm";
    //
    // output_grm( *pxfld_linear, xfld_linear_filename);
    // output_grm( *pxfld, xfld_filename);
  }

  fadapthist.close();
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
