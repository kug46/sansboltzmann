// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve1D_DGBR2_Line_Euler_Nozzle_ArtificialViscosity_toy
// Inviscid quasi-1D nozzle with shock and artificial viscosity


//#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <chrono>
#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include "../../src/pde/NS/PDEEuler1D_Source.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler1D.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/BCEulermitAVSensor1D.h"
#include "pde/NS/Fluids1D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Output_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_Dispatch_DGBR2.h"

#include "Discretization/isValidState/SetValidStateCell.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/Function/FunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/output_Tecplot_PDE.h"
#include "Field/output_gnuplot.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Meshing/XField1D/XField1D.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGBR2_Line_Euler_Nozzle_AV_BDF_toy )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGBR2_Line_Euler_Nozzle_AV_BDF_toy )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
//  typedef PDEEuler1D_Source<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD1, PDEBaseClass> Sensor;
  typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
//  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEulermitAVSensor1DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> BCVector;


#ifndef BOUNDARYOUTPUT
  typedef OutputEuler1D_WindowedPressureSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEulerQ1D_TotalPressure<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_CG ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD1, TopoD1, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  //steady solver
  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  //unsteady solver
  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, ParamFieldType> BDFClass;

  //RK solver?

  // Used to compute the volume of the domain
  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Const> NDScalarConst;

  GlobalTime time(0);

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1.0e-9, 1.0e-9};

  PDEBaseClass::EulerResidualInterpCategory interp = PDEBaseClass::Euler_ResidInterp_Raw;
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = true;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)

  // reference state (freestream)
  const Real pr = 1.4;
  const Real Pc = 1.0;
  const Real Tc = 1.0;
  const Real rhoc = 1;                   // density scale

  const Real pback = Pc/pr;

  //Real Pte = 0.998615682125091553;

  // exact-ish initial condition
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);

  typedef SolutionFunction_Euler1D_ArtShock<TraitsSizeEulerArtificialViscosity, TraitsModelAV> ICSolnClass;
  PyDict ICSoln;
  ICSoln[BCEuler1DParams<BCTypeFunction_mitState>::params.Function.Name] = "ArtShock";
  ICSoln[ICSolnClass::ParamsType::params.xThroat] = 0.5;
  ICSoln[ICSolnClass::ParamsType::params.xShock] = 0.8150186197870202;
  ICSoln[ICSolnClass::ParamsType::params.pt1] = Pc;
  ICSoln[ICSolnClass::ParamsType::params.pt2] = 0.947561264038085938;
  ICSoln[ICSolnClass::ParamsType::params.Tt] = Tc;
  ICSoln[ICSolnClass::ParamsType::params.pe] = pback;
  ICSoln[ICSolnClass::ParamsType::params.gasModel] = gasModelDict;

  SolnNDConvertSpace<PhysD1, ICSolnClass> solnExact(ICSoln);


  // DGBR2 discretization
  Real viscousEtaParameter = 2*Line::NEdge;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDScalarConst constFcn(1);

  typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDEClass> BCBase;
  typedef BCmitAVSensor1D<BCTypeFlux_mitState, BCBase> BCClass;

  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTt_mitState;
  BCInflow[BCClass::ParamsType::params.TtSpec] = Tc;
  BCInflow[BCClass::ParamsType::params.PtSpec] = Pc;

#if 0
  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_SpaceTime_mitState;
  BCOut[BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState>::params.pSpec] = Pc / pr;
#else

  typedef SolutionFunction_Euler1D_SineBackPressure<TraitsSizeEuler, TraitsModelEulerClass> BackPressureSolnClass;
  PyDict OutSoln;
  OutSoln[BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_mitState>::params.Function.Name] = "SineBackPressure";
  OutSoln[BackPressureSolnClass::ParamsType::params.rho] = rhoc;
  OutSoln[BackPressureSolnClass::ParamsType::params.u] = 0.0;
  OutSoln[BackPressureSolnClass::ParamsType::params.p] = pback;
  OutSoln[BackPressureSolnClass::ParamsType::params.dp] = 0.05;
  OutSoln[BackPressureSolnClass::ParamsType::params.dt] = 1.0;
  OutSoln[BackPressureSolnClass::ParamsType::params.gasModel] = gasModelDict;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_PressureFunction_mitState;
  BCOut[BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_mitState>::params.Function] = OutSoln;
#endif

  // Define BC list
  PyDict PyBCList;
  PyBCList["BCInflow"] = BCInflow;
  PyBCList["BCOut"] = BCOut;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Define the BoundaryGroups for each boundary condition
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["BCInflow"] = {0}; //left
  BCBoundaryGroups["BCOut"] = {1}; // right


  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;
  PyDict ParamDict;

//#ifdef INTEL_MKL
//  std::cout << "Using MKL\n";
//  PyDict MKL_PARDISODict;
//  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
//  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
//  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
//#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
//#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.2;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] =  false;

  //--------Grid Density LOOP--------
  for (int grid_index = 3; grid_index <= 3; grid_index++)
  {

    // Grid
    int ii = 100*pow(2,grid_index);
    std::shared_ptr<XField<PhysD1, TopoD1>> pxfld = std::make_shared<XField1D>( ii, 0, 1 );

    std::vector<int> cellGroups;
    for ( int i = 0; i < pxfld->nCellGroups(); i++)
      cellGroups.push_back(i);

    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<GenHField_CG<PhysD1,TopoD1>> phfld = std::make_shared<GenHField_CG<PhysD1,TopoD1>>(*pxfld);

    //Check the BC dictionary
    BCParams::checkInputs(PyBCList);

    std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);


    std::shared_ptr<SolutionClass> pGlobalSol;
    std::shared_ptr<SolverInterfaceClass> pInterface;
    for (int order = 2; order <= 2; order++)
    {
      std::string filename_base = "tmp/NozzleBDF_P";
      filename_base += std::to_string(order);
      filename_base += "_X";
      filename_base += std::to_string(ii);
      filename_base += "/";

      boost::filesystem::path base_dir(filename_base);
      if ( not boost::filesystem::exists(base_dir) )
        boost::filesystem::create_directory(base_dir);

      int BDForder = order+1;

      std::shared_ptr<ScalarFunction1D_ArtNozzle> area( new ScalarFunction1D_ArtNozzle() );
      PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, area);
//      PDEBaseClass pdeEulerAV(order, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, area);
      // Sensor equation terms
      Sensor sensor(pdeEulerAV);
      SensorAdvectiveFlux sensor_adv(0.0);
      SensorViscousFlux sensor_visc(order);
      SensorSource sensor_source(order, sensor);
      // AV PDE with sensor equation
      NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order, hasSpaceTimeDiffusion,
                     EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, area);

#ifndef BOUNDARYOUTPUT
      // Enthalpy error output

      Real t1 = 9.0;
      Real t2 = 14.0;
      NDOutputClass fcnOutput(time, pde, t1, t2, pback + 0.0426738463799453);
      OutputIntegrandClass outputIntegrand(fcnOutput, cellGroups);
#else
      // Pressure output
      NDOutputClass fcnOutput(pde, 1e-3, -0.5, 0.);
      IntegrandOutputPressure outputIntegrand( disc, fcnOutput, BCBoundaryGroups["Wall"] );
#endif

      Real Tend = 15.;
      Real Nsteps = 200*pow(2,grid_index)*Tend;

      Real dt = Tend / Nsteps;

      // solution:
      QuadratureOrder quadratureOrder( *pxfld, 3*order+1 );
      const int quadOrder = 3*order+1;

      std::vector<Real> tol = { 1e-11, 1e-11 };

      PyDict ParamDict;
      SolutionClass sol( (*phfld, *pxfld), pde, order, order,
                         BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                         active_boundaries, ParamDict, disc);
      sol.createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

//      ArrayQ q0 = pde.setDOFFrom( AVVariable<DensityVelocityPressure1D, Real>({{rhoc, 0.1, Pc}, 0.0}) );
//      sol.setSolution(q0);
      sol.setSolution( solnExact, {0});

      NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type] =
                      SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
      NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]    = NewtonSolverDict;
      NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
      NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
      NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1000;
      NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
      //NonlinearSolverDict[PseudoTimeParam::params.CFLDecreaseFactor]  = 0.1;
      //NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor]  = 2;
      NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
      SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;
      SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

      //Create solver interface
      pInterface = std::make_shared<SolverInterfaceClass>(sol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      pInterface->solveGlobalPrimalProblem();

      // Tecplot dump grid
      string filename = filename_base + "NozzleInit_P";
      filename += to_string(order);
      filename += "_X";
      filename += to_string(ii);
      filename += ".plt";
      output_Tecplot( sol.primal.qfld, filename );

      Real entropy = 0.0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_DGBR2( outputIntegrand, entropy ), sol.paramfld, (sol.primal.qfld, sol.primal.rfld),
          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      cout <<  std::to_string( time ) << "  "  << std::setprecision(16) << entropy << "\n";

      PyDict NonlinearSolverDict2;
      NonlinearSolverDict2[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

      // Create AlgebraicEquationSets
      PrimalEquationSetClass AlgEqSetSpace( sol.paramfld, sol.primal, sol.pliftedQuantityfld,
                                            pde, disc, quadratureOrder, ResNormType, tol,
                                            cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, time);

      // The BDF class
      BDFClass BDF( BDForder, dt, time, sol.paramfld, sol.primal.qfld, NonlinearSolverDict2, pde, quadratureOrder, {0}, AlgEqSetSpace );

      // Set IC
      time = -dt*Real(BDForder);
      int stepstart = 0;
      for (int j = BDForder-1; j >= 0; j--)
      {
        time += dt;
//        stepstart++;
        BDF.setqfldPast(j, sol.primal.qfld);
      }

      string filenameout = filename_base + "NozzleBDF_pressure_P";
      filenameout += to_string(order);
      filenameout += "_X";
      filenameout += to_string(ii);
      filenameout += ".txt";
      fstream fout( filenameout, fstream::out );

      fout << "time  windowedPressure\n";

      fout <<  std::to_string( time ) << "  "  << std::setprecision(16) << entropy << "\n";

      auto time1 = std::chrono::high_resolution_clock::now();
      for (int step = stepstart; step < Nsteps; step++)
      {
        // March the solution in time
        BDF.march(1);

        entropy = 0.0;
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_DGBR2( outputIntegrand, entropy ), sol.paramfld, (sol.primal.qfld, sol.primal.rfld),
            quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        fout <<  std::to_string( time ) << "  "  << std::setprecision(16) << entropy << "\n";

        if (step % 10 == 0)
        {

          std::cout << "t = " << std::to_string( time ) << ", entropy= " << std::setprecision(16) << entropy << "\n";

          // Tecplot dump grid
          string filename = filename_base + "NozzleBDF_P";
          filename += to_string(order);
          filename += "_X";
          filename += to_string(ii);
          filename += "_t";
          filename += to_string(step);
          filename += ".plt";
          output_Tecplot( sol.primal.qfld, filename );
        }

      }
      fout.close();

      auto time2 = std::chrono::high_resolution_clock::now();

      string filenameout2 = filename_base + "NozzleBDF_time_P";
      filenameout2 += to_string(order);
      filenameout2 += "_X";
      filenameout2 += to_string(ii);
      filenameout2 += ".txt";
      fstream fout2( filenameout2, fstream::out );
      fout2 << "P solve: " << std::chrono::duration_cast<std::chrono::milliseconds>(time2-time1).count() << "ms" << std::endl;
      fout2.close();


//      string sol_filename = "ArtNozzleDerived_"
//                     + Type2String<QType>::str()
//                     + "_P"
//                     + std::to_string(order)
//                     + "_G"
//                     + std::to_string(grid_index)
//                     + ".plt";
//      output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld,
//                     pGlobalSol->primal.rfld, filename_base + sol_filename);


    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
