// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA3D.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCRANSSA3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/OutputEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"

#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"

#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Field/HField/HFieldVolume_DG.h"

#include "Field/DistanceFunction/DistanceFunction.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Meshing/EPIC/XField_PX.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize, class PDETraitsModel>
void
output_Tecplot_RANS_Wall( const PDERANSSA3D<PDETraitsSize, PDETraitsModel>& pde,
                          const Field< PhysD3, TopoD3, DLA::VectorS<6,Real>>& qfld,
                          const FieldLift< PhysD3, TopoD3, DLA::VectorS<PhysD3::D,DLA::VectorS<6,Real>> >& rfld,
                          const DiscretizationDGBR2& disc,
                          const std::string& filename,
                          const bool partitioned = false);

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_DGBR2_RANS_HemisphereCylinder_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_DGBR2_RANS_HemisphereCylinder_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  //typedef QTypePrimitiveSurrogate QType;
//  typedef QTypeConservative QType;

  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Force<PDEClass>> NDOutputForce;
  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Moment<PDEClass>> NDOutputMoment;
  typedef NDVectorCategory<boost::mpl::vector2<NDOutputForce, NDOutputMoment>, NDOutputForce::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;

  typedef FieldTuple<Field<PhysD3, TopoD3, Real>, XField<PhysD3, TopoD3>, TupleClass<>> ParamFieldType;

  typedef AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamFieldType> AlgebraicEquationSet_PTCClass;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;

  mpi::communicator world;

//  int iref = 6;
  int aoa = 19;

  std::string filename_base = "tmp/hemisphere-cylinder/EPIC_P1Q1_500k_aoa19/aoa" + std::to_string(aoa) + "/";

  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld =
     std::make_shared<XField_PX<PhysD3, TopoD3>>(world, "tmp/hemisphere-cylinder/EPIC_P1Q1_500k_aoa19/hc_tetra.28k.grm" );

  int orderMax = 1;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // Sutherland viscosity
  const Real tSuth = 198.6/540;     // R/R
  //const Real tRef  = 491.6/540;     // R/R

  // reference state (freestream)
  const Real Mach = 0.6;
  const Real Reynolds = 0.35e6;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real MAC = 10;                              // mean aerodynamic chord for moments
  const Real SRef = 5;                              // area scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = Real(aoa)*PI/180.;            // angle of attack (radians)

#if 1
  const Real tRef = 1;          // temperature
  const Real pRef = rhoRef*R*tRef;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;
#endif

#if 0
  const Real qRef = 1;                              // velocity scale
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = 0;
  const Real wRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef + wRef*wRef);

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // DGBR2 discretization
  Real viscousEtaParameter = 2*Tet::NTrace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  StabilizationMatrix stab(StabilizationType::SUPG, TauType::Glasby);

  // initial condition
  ArrayQ q0, p0;
  pde.setDOFFrom( q0, SAnt3D<DensityVelocityPressure3D<Real>>({rhoRef, uRef, vRef, wRef, pRef, ntRef}) );
  pde.setDOFFrom( p0, SAnt3D<DensityVelocityPressure3D<Real>>({rhoRef, 0, 0, 0, pRef, ntRef}) );


  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  PyDict StateVector;
  StateVector[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  StateVector[Conservative3DParams::params.rho] = rhoRef;
  StateVector[Conservative3DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative3DParams::params.rhov] = rhoRef*vRef;
  StateVector[Conservative3DParams::params.rhow] = rhoRef*wRef;
  StateVector[Conservative3DParams::params.rhoE] = rhoRef*ERef;
  StateVector["rhont"]  = rhoRef*ntRef;

  PyDict BCFarField;
  BCFarField[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.StateVector] = StateVector;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.Characteristic] = true;

  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflow[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;


  PyDict PyBCList;
  PyBCList["BCFarField"] = BCFarField;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCOutflow"]  = BCOutflow;
  PyBCList["BCWall"]     = BCWall;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
#if 1
  BCBoundaryGroups["BCFarField"] = {0};
  BCBoundaryGroups["BCSymmetry"] = {1};
  BCBoundaryGroups["BCOutflow"]  = {2};
  BCBoundaryGroups["BCWall"]     = {3,4};
#else
  BCBoundaryGroups["BCFarField"] = {2};
  BCBoundaryGroups["BCSymmetry"] = {3};
  BCBoundaryGroups["BCOutflow"]  = {1};
  BCBoundaryGroups["BCWall"]     = {0};
#endif

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-7, 1e-7};

  typedef BCRANSSA3D<BCTypeWall_mitState, BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  typedef BCNDConvertSpace<PhysD3, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  NDBCClass bc(pde);
  IntegrandBCClass fcnWallBC( pde, bc, BCBoundaryGroups["BCWall"], disc );

  // Residual weighted boundary output
  Real dynpRef = 0.5*rhoRef*(uRef*uRef + vRef*vRef + wRef*wRef);

  NDOutputForce outputFcnDrag(pde, cos(aoaRef)/(SRef*dynpRef), 0., sin(aoaRef)/(SRef*dynpRef));
  OutputIntegrandClass outputIntegrandDrag( outputFcnDrag, BCBoundaryGroups["BCWall"] );

  NDOutputForce outputFcnLift(pde, -sin(aoaRef)/(SRef*dynpRef), 0., cos(aoaRef)/(SRef*dynpRef));
  OutputIntegrandClass outputIntegrandLift( outputFcnLift, BCBoundaryGroups["BCWall"] );

  NDOutputMoment outputFcnMoment(pde, 0, 0, 0, 0, 1, 0, MAC);
  OutputIntegrandClass outputIntegrandMoment( outputFcnMoment, BCBoundaryGroups["BCWall"] );

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC) && 0
  if (world.rank() == 0)
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 900;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict), PreconditionerILUAdjoint(PreconditionerILU);

  PreconditionerILUAdjoint[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 1;
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILUAdjoint;

  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;
  PETScDictAdjoint[SLA::PETScSolverParam::params.ResidualHistoryFile] = filename_base + "PETSc_adjoint_residual.dat";

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
  //NewtonSolverDict[NewtonSolverParam::params.MaxChangeFraction] = 0.1;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
  NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_residual.dat";
#endif

  PseudoTimeParam::checkInputs(NonlinearSolverDict);

  fstream foutputhist;
  if (world.rank() == 0)
  {
    std::string outputhist_filename = filename_base + "output.adapthist";
    foutputhist.open( outputhist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + outputhist_filename);
  }

  int orderDist = 2;

  PyDict ParamDict;

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Compute distance field
  std::shared_ptr<Field_CG_Cell<PhysD3, TopoD3, Real>>
    pdistfld = std::make_shared<Field_CG_Cell<PhysD3, TopoD3, Real>>(*pxfld, orderDist, BasisFunctionCategory_Lagrange);
  DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCWall"));


  Real outputDragP0 = 0;
  Real outputLiftP0 = 0;

  std::shared_ptr<Field_CG_Cell<PhysD3, TopoD3, ArrayQ>> pqfld;

  for (int order = 1; order <= orderMax; order++)
  {
    stab.setStabOrder(order);
    stab.setNitscheOrder(order);
    ParamFieldType paramfld = (*pdistfld, *pxfld);

    // Adjoint equation is integrated with 3 times adjoint order
    const int quadOrder = 3*(order+1);

    std::shared_ptr<Field_CG_Cell<PhysD3, TopoD3, ArrayQ>>
      pqfldNew = std::make_shared<Field_CG_Cell<PhysD3, TopoD3, ArrayQ>>(*pxfld, order, BasisFunctionCategory_Lagrange);
    FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ>
      rfld(*pxfld, order, BasisFunctionCategory_Lagrange);
    Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ>
      lgfld(*pxfld, order, BasisFunctionCategory_Lagrange, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

    QuadratureOrder quadrule(*pxfld, quadOrder);

    //Set initial solution
    if (order == 1)
    {
      pqfld = pqfldNew;

      std::string distfld_filename = filename_base + "distfld_a0_P"
                                                   + std::to_string(orderDist)
                                                   + "_rank" + std::to_string(world.rank()) + ".dat";
      output_Tecplot( *pdistfld, distfld_filename );

      // compute the lift and drag based on just the reference pressure
      *pqfld = p0;
      rfld = 0;

      outputLiftP0 = 0;
      IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_DGBR2( outputIntegrandLift, fcnWallBC, outputLiftP0 ),
        paramfld, (*pqfld, rfld), quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size() );

#ifdef SANS_MPI
      outputLiftP0 = boost::mpi::all_reduce( *pxfld->comm(), outputLiftP0, std::plus<Real>() );
#endif

      outputDragP0 = 0;
      IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_DGBR2( outputIntegrandDrag, fcnWallBC, outputDragP0 ),
        paramfld, (*pqfld, rfld), quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size() );

#ifdef SANS_MPI
      outputDragP0 = boost::mpi::all_reduce( *pxfld->comm(), outputDragP0, std::plus<Real>() );
#endif

      //Set initial solution
      *pqfld = q0;
    }
    else
    {
      //Set initial solution
      pqfld->projectTo(*pqfldNew);
      pqfld = pqfldNew;
    }

    // dump initial solution
    std::string qfld_init_filename = filename_base + "qfld_init_a0_P" + std::to_string(order)
                                                   + "_rank" + std::to_string(world.rank()) + ".dat";
    output_Tecplot( *pqfld, qfld_init_filename );


    //////////////////////////
    //SOLVE SYSTEM
    //////////////////////////
    PrimalEquationSetClass PrimalEqSet(paramfld, *pqfld, rfld, lgfld, pde, stab, quadrule, ResNormType,
                                       tol, {0}, PyBCList, BCBoundaryGroups);
    AlgebraicEquationSet_PTCClass AlgEqSetPTC(paramfld, *pqfld, pde, quadrule, {0}, PrimalEqSet);

    // Create the pseudo time continuation class
    PseudoTime<SystemMatrixClass> PTC(NonlinearSolverDict, AlgEqSetPTC);

    BOOST_CHECK( PTC.iterate(1000) );

    //----------//

    Real outputLift = 0;
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      FunctionalBoundaryTrace_DGBR2( outputIntegrandLift, fcnWallBC, outputLift ),
      paramfld, (*pqfld, rfld), quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size() );

#ifdef SANS_MPI
    outputLift = boost::mpi::all_reduce( *pxfld->comm(), outputLift, std::plus<Real>() );
#endif

    Real outputDrag = 0;
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      FunctionalBoundaryTrace_DGBR2( outputIntegrandDrag, fcnWallBC, outputDrag ),
      paramfld, (*pqfld, rfld), quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size() );

#ifdef SANS_MPI
    outputDrag = boost::mpi::all_reduce( *pxfld->comm(), outputDrag, std::plus<Real>() );
#endif

    Real outputMoment = 0;
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      FunctionalBoundaryTrace_DGBR2( outputIntegrandMoment, fcnWallBC, outputMoment ),
      paramfld, (*pqfld, rfld), quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size() );

#ifdef SANS_MPI
    outputMoment = boost::mpi::all_reduce( *pxfld->comm(), outputMoment, std::plus<Real>() );
#endif

    if (pxfld->comm()->rank() == 0)
    {
      if (order == 0)
        foutputhist << std::setw(5)  << "Iter"
                    << std::setw(5)  << "P"
                    << std::setw(20) << "Lift"
                    << std::setw(20) << "Drag"
                    << std::setw(20) << "Drag P0"
                    << std::setw(20) << "Drag Net"
                    << std::setw(20) << "Moment"
                    << std::endl;

      foutputhist << std::setw(5) << 0
                  << std::setw(5) << order
                  << std::setw(20) << std::setprecision(10) << std::scientific << outputLift - outputLiftP0
                  << std::setw(20) << std::setprecision(10) << std::scientific << outputDrag
                  << std::setw(20) << std::setprecision(10) << std::scientific << outputDragP0
                  << std::setw(20) << std::setprecision(10) << std::scientific << outputDrag - outputDragP0
                  << std::setw(20) << std::setprecision(10) << std::scientific << outputMoment
                  << std::endl;
    }
    //----------//

    std::string qfld_filename = filename_base + "qfld_a0_P" + std::to_string(order)
                                              + "_rank" + std::to_string(world.rank()) + ".dat";
    output_Tecplot( *pqfld, qfld_filename );
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
