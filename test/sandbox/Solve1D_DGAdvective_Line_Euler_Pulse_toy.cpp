// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_DGAdvective_Line_Euler_Pulse_toy
// testing of 1-D DGAdvective with Timestepping

#undef SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler1D.h"
#include "pde/NS/BCEuler1D.h"
#include "pde/NS/SolutionFunction_Euler1D.h"
#include "pde/NS/OutputEuler1D.h"
#include "pde/BCParameters.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGAdvective_Line_Euler_Sod_test_suite )

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( Solve1D_DGAdvective_Line_Euler_Sod )
{
#if 0
//  typedef QTypeEntropy QType;
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  //this will be handled in BC class eventually...
  typedef BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD1, TopoD1> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolutionFunction_Euler1D_DensityPulse<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolnNDConvertSpace<PhysD1, SolutionClass> SolutionNDClass;

  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> BDFClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  GlobalTime time(0);

  // Gas Model
  Real gamma = 1.4;
  Real R = 0.4;
  Real cv = R / (gamma - 1.0);
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);
  // PDE
  NDPDEClass pde(time, gas, PDEClass::Euler_ResidInterp_Momentum);

  typedef OutputEuler1D_Density<PDEClass> OutputDensityClass;
  typedef OutputNDConvertSpace<PhysD1, OutputDensityClass> NDOutputDensityClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputDensityClass> IntegrandDensity;

  NDOutputDensityClass outDensity(time, pde);
  IntegrandDensity fcnDensity( outDensity, {0} );

  Real rho = 0.1;
  Real u = 2.0;
  Real p = 0.1;
  Real t =  p / (R*rho);

  // BC
  // Create a BC dictionary
  PyDict BCInflowSupersonic;

  BCInflowSupersonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSupersonic;
  BCInflowSupersonic[BCEuler1DParams<BCTypeInflowSupersonic>::params.rho] = rho;
  BCInflowSupersonic[BCEuler1DParams<BCTypeInflowSupersonic>::params.rhou] = rho*u;
  BCInflowSupersonic[BCEuler1DParams<BCTypeInflowSupersonic>::params.rhoE] = rho*(cv*t + 0.5*u*u);

  PyDict PyBCList;
  PyBCList["BCInflowSupersonic"] = BCInflowSupersonic;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCInflowSupersonic"] = {0,1}; //left

  // Set up IC with a gaussian pulse
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.center] = 1.0;
  solnArgs[SolutionClass::ParamsType::params.width] = 0.1;
  solnArgs[SolutionClass::ParamsType::params.height] = 0.9;
  solnArgs[SolutionClass::ParamsType::params.rho] = rho;
  solnArgs[SolutionClass::ParamsType::params.u] = u;
  solnArgs[SolutionClass::ParamsType::params.p] = p;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionNDClass solnExact(time, solnArgs);
  Real Tend = 4.0;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1};

  // set BDF and grid parameters for the run
  int BDFmin = 2;
  int BDFmax = 2;

  for (int BDForder = BDFmin; BDForder <= BDFmax; BDForder++)
  {
    int order = BDForder;

    int indx = 0;

    int ii = 300; //grid size - using timesteps = grid size

    XField1D xfld( ii, 0, 15 );

    Real dt;
    int nsteps = ii;
    dt = Tend / (nsteps);

    // DG solution field
    // cell solution
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
    qfld = 0;

    // lagrange multipliers not actually used either
    Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ>
      lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
    lgfld = 0;

    // Initial condition
    typedef typename XField<PhysD1, TopoD1>::FieldCellGroupType<TopoD1> XFieldCellGroupType;
    typedef typename Field< PhysD1, TopoD1, ArrayQ>::FieldCellGroupType<TopoD1> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    ElementXFieldClass xfldElem( xfld.getCellGroup<Line>(0).basis() );
    ElementQFieldClass qfldElem( qfld.getCellGroup<Line>(0).basis() );

#if 0
    // Tecplot dump grid
    string filename2 = "tmp/icDG_EulerPulse_P";
    filename2 += to_string(order);
    filename2 += "_";
    filename2 += to_string(ii);
    filename2 += ".plt";
    output_Tecplot( qfld, filename2 );
#endif

    // Set up Newton Solver
    PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
    NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
    NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

    PyDict NonLinearSolverDict;
    NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
    NonLinearSolverParam::checkInputs(NonLinearSolverDict);

    QuadratureOrder quadratureOrder( xfld, -1 );
    std::vector<Real> tol = {1e-11, 1e-11};

    // Create the spatial discretization
    PrimalEquationSetClass AlgEqSetSpace(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups, time);

    // The BDF class
    BDFClass BDF( BDForder, dt, time, xfld, qfld, NonLinearSolverDict, pde, {0}, AlgEqSetSpace );

#if 1
    // Tecplot dump grid
    string filename2 = "tmp/icDG_EulerPulse_P";
    filename2 += to_string(order);
    filename2 += "_";
    filename2 += to_string(ii);
    filename2 += ".plt";
    output_Tecplot( qfld, filename2 );

    // Gnuplot dump grid
    string gfilename2 = "tmp/icDG_EulerPulse_P";
    gfilename2 += to_string(order);
    gfilename2 += "_";
    gfilename2 += to_string(ii);
    gfilename2 += ".gplt";
    output_gnuplot( qfld, gfilename2 );
#endif

    // Set IC
    time = 0.0;
    int stepstart = 0;
    for (int j = BDForder-1; j >= 0; j--)
    {
      for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
      time += dt;
      stepstart += 1;
      BDF.setqfldPast(j, qfld);
    }

    for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );

    // Monitor Density
    Real iMass = 0.0;
    IntegrateCellGroups<TopoD1>::integrate(
        FunctionalCell_Galerkin( fcnDensity, iMass ),
        xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

    //start clock
    timer solution_time;

    // March the solution in time
    BDF.march(nsteps - stepstart);

#if 1
    const int nDOFPDE = qfld.nDOF();
    const int nDOFBC  = lgfld.nDOF();

    const int nDOFtot = nDOFPDE + nDOFBC;

    // Monitor Density
    Real fMass = 0.0;
    IntegrateCellGroups<TopoD1>::integrate(
        FunctionalCell_Galerkin( fcnDensity, fMass ),
        xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size()  );

    cout << std::setprecision(16) << std::scientific;
    cout << "P = " << order << " ii = " << ii << ": DOF = " << nDOFtot << ": nsteps = " << nsteps << endl;
    cout << " Initial Mass: " << iMass << endl
         << " Final Mass:   " << fMass << endl;
    cout << " CPUTime = " << solution_time.elapsed() << " s";
    cout << endl;
#endif

    indx++;
    BOOST_CHECK_CLOSE( time, Tend, 1e-12 );

#if 1
    // Tecplot dump grid
    string filename = "tmp/slnDG_EulerPulse_P";
    filename += to_string(order);
    filename += "_";
    filename += to_string(ii);
    filename += ".plt";
    output_Tecplot( qfld, filename );

    // gnuplot dump grid
    string gfilename = "tmp/slnDG_EulerPulse_P";
    gfilename += to_string(order);
    gfilename += "_";
    gfilename += to_string(ii);
    gfilename += ".gplt";
    output_gnuplot( qfld, gfilename );
#endif
  }
#endif
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
