// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_NavierStokes_FlatPlate_btest
// Testing of the MOESS framework on a 2D Navier-Stokes problem

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include <iostream>
#include <random>
#include <sstream> // string stream

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "tools/split_cat_std_vector.h"

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/OutputNavierStokes2D.h"
#include "pde/NS/ForcingFunctionEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#ifdef SANS_AVRO
#include <geometry/builder.h>
#include <library/cube.h>
#include "Meshing/avro/XField_avro.h"
#endif

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_FlatPlate_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_NavierStokes_FlatPlate_MCMC_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_NavierStokes_FlatPlate_MCMC_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_TemperatureProbe<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputNavierStokes2D_TotalHeatFlux<PDEClass> OutputClass;
  // typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif
  // For setting temperature sensor arrays
  typedef OutputEuler2D_TemperatureProbe<PDEClass> TemperatureOutputClass;
  typedef OutputNDConvertSpace<PhysD2, TemperatureOutputClass> NDTemperatureOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDTemperatureOutputClass> TemperatureOutputIntegrandClass;

  // Heat Flux and Drag independent of the adaptation
  typedef OutputNavierStokes2D_TotalHeatFlux<PDEClass> HeatFluxOutputClass;
  typedef OutputNDConvertSpace<PhysD2, HeatFluxOutputClass> NDHeatFluxOutputClass;
  typedef OutputEuler2D_Force<PDEClass> ForceOutputClass;
  typedef OutputNDConvertSpace<PhysD2, ForceOutputClass> NDForceOutputClass;

  typedef NDVectorCategory<boost::mpl::vector2<NDHeatFluxOutputClass,NDForceOutputClass>, NDHeatFluxOutputClass::Category> NDOutVec2Cat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVec2Cat, DGBR2> DataOutputIntegrandBoundaryClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // For the data arrays
  typedef typename DLA::VectorS<7,Real> Vector7;
  Vector7 output(0.0);

#define USE_AVRO 1
#define PRINT 1

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1.0e-9, 1.0e-9};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.3;
  const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real tRef = 1;                              // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                      // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum);
  // NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, entropyFix, forcingptr );

  // Temperature Parameters for the chains
  Real tWall=0.5, tInlet=tRef;

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, R*rhoRef*tInlet) );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  // BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipIsothermal_mitState;
  BCNoSlip[BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDEClass>::ParamsType::params.Twall] = tWall;

  Real HRef = Cp*tInlet + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  Real aSpec = atan(vRef/uRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = R*rhoRef*tInlet;


  // std::cout<< "V = " << BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] << std::endl;


  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  // return;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2,4};
  BCBoundaryGroups["BCNoSlip"] = {1};
  BCBoundaryGroups["BCOut"] = {3};
  BCBoundaryGroups["BCIn"] = {5};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Temperature probes
  std::vector<std::vector<Real>> xSensor = { {1.25}, {1.25}, {1.25}, {1.25}, {1.25}};
  std::vector<std::vector<Real>> ySensor = { {0.05}, {0.10}, {0.20}, {0.40}, {0.80}};

  // Outputs
  NDTemperatureOutputClass tempSensor_0(pde,xSensor[0],ySensor[0],1e-2);
  NDTemperatureOutputClass tempSensor_1(pde,xSensor[1],ySensor[1],1e-2);
  NDTemperatureOutputClass tempSensor_2(pde,xSensor[2],ySensor[2],1e-2);
  NDTemperatureOutputClass tempSensor_3(pde,xSensor[3],ySensor[3],1e-2);
  NDTemperatureOutputClass tempSensor_4(pde,xSensor[4],ySensor[4],1e-2);

  // Integrands
  TemperatureOutputIntegrandClass outputIntegrandSensor_0( tempSensor_0, {0} );
  TemperatureOutputIntegrandClass outputIntegrandSensor_1( tempSensor_1, {0} );
  TemperatureOutputIntegrandClass outputIntegrandSensor_2( tempSensor_2, {0} );
  TemperatureOutputIntegrandClass outputIntegrandSensor_3( tempSensor_3, {0} );
  TemperatureOutputIntegrandClass outputIntegrandSensor_4( tempSensor_4, {0} );

  NDHeatFluxOutputClass heatFluxOutputFcn(pde); // Heat flux output
  DataOutputIntegrandBoundaryClass heatFluxOutputIntegrand( heatFluxOutputFcn, {1} );
  NDForceOutputClass dragOutputFcn(pde,1.0,0.0); // Drag output
  DataOutputIntegrandBoundaryClass dragOutputIntegrand( dragOutputFcn, {1} );


#ifndef BOUNDARYOUTPUT
  // Temp probes output
  NDOutputClass fcnOutput(pde,cat(xSensor),cat(ySensor),1e-1);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  // const HNormConstant = Cp*tRef*rhoRef*sqrt(uRef*uRef + vRef*vRef);
  NDOutputClass outputFcn(pde); // Heat flux output
  // NDOutputClass outputFcn(pde, 1., 0.);  // Drag output
  OutputIntegrandClass outputIntegrand( outputFcn, {1} );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  // PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-12;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

#if 1
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 50;
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 1;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
#endif
  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  PyDict PTCContinuationDict, PTCNonlinearSolverDict, PTCNewtonDict(NewtonSolverDict);
  PTCNewtonDict[NewtonSolverParam::params.MaxIterations] = 1;
  PTCNonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  PTCNonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  PTCNonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  PTCNonlinearSolverDict[PseudoTimeParam::params.invCFL] = 1;
  PTCNonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  PTCNonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;

  PTCContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = PTCNonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  // Grid
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;
#if USE_AVRO && defined(SANS_AVRO)
  using avro::coord_t;
  using avro::index_t;
  std::shared_ptr<avro::Context> context;

  // create the context
  context = std::make_shared<avro::Context>();

  // setup the EGADS nodes
  avro::real x0[3] = {-1,0,0};
  avro::real x1[3] = {0,0,0};
  avro::real x2[3] = {1,0,0};
  avro::real x3[3] = {2,0,0};
  avro::real x4[3] = {2,2,0};
  avro::real x5[3] = {-1,2,0};
  std::vector<avro::real*> X = {x0,x1,x2,x3,x4,x5};

  std::vector<avro::EGADSNode> nodes;
  for (index_t k=0;k<X.size();k++)
    nodes.emplace_back( avro::EGADSNode(context.get(),X[k]) );

  // make the edges
  // very important! boundary conditions need to be specified in this order
  // you can use EngSketchPad's vGeom on the flatplate.egads file below
  // to visualize the order of the edges in the EGADS model
  std::vector<avro::EGADSEdge> edges;
  for (index_t k=0;k<nodes.size();k++)
  {
    avro::EGADSNode* n0 = &nodes[k];
    avro::EGADSNode* n1;
    if (k<nodes.size()-1)
      n1 = &nodes[k+1];
    else
      n1 = &nodes[0];
    edges.emplace_back( avro::EGADSEdge(context.get(),*n0,*n1) );
  }

  avro::EGADSEdgeLoop loop(CLOSED);
  for (index_t k=0;k<edges.size();k++)
    loop.add( edges[k] , SFORWARD );
  loop.make(context.get());

  std::shared_ptr<avro::Body> body = std::make_shared<avro::EGADSWireBody>(context.get(),loop);
  body->buildHierarchy();

  std::shared_ptr<avro::Model> amodel = std::make_shared<avro::Model>(context.get(),"flatplate");
  amodel->addBody(body,false); // false = body is not interior
  amodel->finalize();

  // setup the initial mesh
  index_t nx = 4;
  index_t ny = 5;
  avro::library::CubeMesh mesh( {1,1} , {nx,ny} );

  // transform the vertices
  for (index_t k=0;k<mesh.vertices().nb();k++)
  {
    avro::real x = mesh.vertices()[k][0];
    avro::real y = mesh.vertices()[k][1];

    mesh.vertices()[k][0] = (x3[0] -x0[0])*x +x0[0];
    mesh.vertices()[k][1] = (x5[1] -x0[1])*y +x0[1];
  }

  mesh.vertices().findGeometry(*amodel);

  std::shared_ptr<XField_avro<PhysD2, TopoD2>> pxfld_avro( new XField_avro<PhysD2,TopoD2>(world,amodel) );
  pxfld_avro->import(mesh);

  pxfld = pxfld_avro;
#endif


#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  int order=3, power=0, chain = 0;
  if (argc == 4)
  { // Running a chain!
    order = std::stoi(argv[1]);
    power = std::stoi(argv[2]);
    chain = std::stoi(argv[3]);
    // yF = ((Real)std::stoi(argv[4]))/100.0;
  }


  std::cout << "order = " << order << ", power = " << power << ", chain = " << chain << std::endl;

#endif

  // const int power = 0, order = 2;

  //--------Initial ADAPTATION LOOP--------
  /*
    Give the mesh an initial adaptation to the mean value of wall Temp and inlet Temp
  */


  int maxIter;
  if (chain == 0)
  {
    maxIter = 50;
  }
  else
  {
    maxIter = 25;
  }

  Real targetCost = 250.0*pow(2,power);

  // std::string s = stream.str();

  // to make sure folders have a consistent number of zero digits
  const int string_pad = 5;
  std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
  std::string chain_pad = std::string(6 - std::to_string(chain).length(), '0') + std::to_string(chain);
  std::string filename_base;
  filename_base = "tmp/NS_FP_Re1000_MCMC_PlateTemp/DG_" + int_pad + "_P" + std::to_string(order) + "_" + chain_pad + "_expensive/";

  boost::filesystem::create_directories(filename_base);

  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist;
  if (world.rank() == 0)
  {
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }
  std::string outhist_filename = filename_base + "out.dat";
  fstream fouthist;
  if (world.rank() == 0)
  {
    fouthist.open( outhist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fouthist.good(), "Error opening file: " + outhist_filename);
    fouthist << "tWall,tInlet,T0, T1, T2, T3, T4, Tall, qH, qD" << std::endl;
  }

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Optimizer_MaxEval] = 6000;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;
  MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction] = 0.25;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

  PyDict MesherDict;
#if USE_AVRO && defined(SANS_AVRO)
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;
#else
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
#endif

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  const int quadOrder = 3*(order+1); //2*(order + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrand);

  //Set initial solution
  pGlobalSol->setSolution(q0);

#if PRINT
  // std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  // output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );
#endif

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

#if PRINT
  std::string qfld_filename = filename_base + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

  std::string adjfld_filename = filename_base + "adjfld_a0.plt";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
#endif

  //Compute error estimates
  pInterface->computeErrorEstimates();

#if PRINT
  // std::string efld_filename = filename_base + "efld_a0.plt";
  // pInterface->output_EField(efld_filename);
#endif

  for (int iter = 0; iter < maxIter+1; iter++)
  {
    timer tic;
    if ( world.rank() == 0 ) std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

#if PRINT
    // std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    // output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );
#endif

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();
    //Compute error estimates
    pInterface->computeErrorEstimates();

#if PRINT
    if (iter % 5 == 0)
    {
      std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
    }

    // std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".plt";
    // pInterface->output_EField(efld_filename);
#endif

     QuadratureOrder quadratureOrder( *pxfld, quadOrder );

     // Calculate temperature sensor outputs
     // std::valarray<Real,8> output(0.0);
     output = 0;
     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_0,output[0]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
        quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_1,output[1]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_2,output[2]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_3,output[3]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_4,output[4]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
       FunctionalBoundaryTrace_Dispatch_DGBR2(heatFluxOutputIntegrand,
         pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
         quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(),
         output[5] ) );
     pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
         FunctionalBoundaryTrace_Dispatch_DGBR2(dragOutputIntegrand,
           pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
           quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(),
         output[6] ) );

 #ifdef SANS_MPI
     output = boost::mpi::all_reduce( *pxfld->comm(), output,  std::plus<Vector7>() );
 #endif

    if (world.rank() == 0)
    {
      std::cout << "time elapsed = " << tic.elapsed() << " s"<< std::endl;

      fouthist  << std::setprecision(4) << tWall << ", "
                << std::setprecision(4) << tInlet << ", "
                << std::setprecision(16) << output[0] << ", "
                << std::setprecision(16) << output[1] << ", "
                << std::setprecision(16) << output[2] << ", "
                << std::setprecision(16) << output[3] << ", "
                << std::setprecision(16) << output[4] << ", "
                << std::setprecision(16) << output[5] << ", "
                << std::setprecision(16) << output[6] << std::endl;
    }

  }

  if (world.rank() == 0)
  {
    fadapthist.close();
    fouthist.close();
  }

  if (chain == 0)
    return; // Generating data rather than running chains

//----------------------------- MCMC -----------------------------//
/*
  The basic mesh is now ok. Start doing the actual MCMC

  Uniform prior for xF and yF on the region
*/


  /*
    // DETERMINISTIC INLET AND UNCERTAIN WALL

  // // From Twall = 0.8, P1 2000 DOF adapt
  // const Vector7 data = {0.010394140522392,
  //                       0.012813268034498,
  //                       0.015210033310727,
  //                       0.015746072739063,
  //                       0.015789191630460,
  //                       0.007544392580019,
  //                       0.003138960822900};



  */


  // From Twall = 0.8, P5 8000 DOF adapt
  const Vector7 data = { 0.010392877768780,
                         0.012812722345310,
                         0.015210407334742,
                         0.015741589656344,
                         0.015747064802028,
                         0.007538840451495,
                         0.003130634565864};


  // distribution parameters
  // precision of the mesh noise, 1/sigma^2_m
  // const Real precSurf = 1.0/pInterface->getGlobalErrorIndicator();
  // const Vector7 precM = { precSurf,  // T0
  //                         precSurf,  // T1
  //                         precSurf,  // T2
  //                         precSurf,  // T3
  //                         precSurf,  // T4
  //                         precSurf,  // Qheat
  //                         precSurf}; // Qdrag

  const Vector7 precM = { 1e8,  // T0
                          1e8,  // T1
                          1e8,  // T2
                          1e8,  // T3
                          1e8,  // T4
                          1e8,  // Qheat
                          1e8}; // Qdrag

  // For calculating the Outputs for the proposed points
  Vector7 propOutput;

  Real logLikelihood = 0.0, propLogLikelihood; // the log likelihood of the data

  for (int i = 0; i < 7; i++)
    logLikelihood += -precM(i)*pow(output(i) - data(i),2)/2;

  // const int chain = 1e3;
  const int reAdaptIter = 100; // number of acceptances before an adapt
  const int burnIn = MIN(1000,chain/10); // number of samples to get rid of
  std::vector<Real> logTWallSamples(chain), logTInletSamples(chain);
  std::vector<Vector7> outputSamples(chain);

  std::cout<< std::setprecision(4); // So all the outputs look the same

  // Filestream for output
  fstream foutputhist;
  if (world.rank() ==  0)
  {
    std::string outputFileName = filename_base + "output.dat";
    foutputhist.open( outputFileName, fstream::out );
    BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + outputFileName);
    foutputhist << "X, Y, output" << std::endl;
    foutputhist << std::setprecision(16);
  }

  // coefficients in the log normal var
  // logT ~ N( mu, sigma )
  // const Real tWallRef = tWall;
  Real logTWall=log(tWall), logTInlet = log(tInlet);
  Real logTWallProp=log(0.5), logTInletProp=0.0, tWallProp, tInletProp;
  const Real propSigma = 0.5; // sigma^2 for the proposal distribution

  // Initialize the Random number generator - Only actually used on processor 0
  // obtain a seed from the system clock:
  unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed1);
  std::normal_distribution<Real> normalDist(0.0,1.0); // Standard normal distribution
  std::uniform_real_distribution<Real> uniformDist(0.0,1.0); // Standard uniform

  // Uniform prior on 0.5 ->

  timer tic;
  // Start the chain having got an initial mesh
  // int ic = 0;
  unsigned int ACC = 0, REJ = 0;
  std::cout<< "starting, chain = " << chain << std::endl;

  // std::shared_ptr<SolutionClass pGlobalSolChain;
  std::shared_ptr<SolutionClass> pGlobalSolNew;
  for (int ic = 0; ic < chain; ic++)
  {
    // std::cout<< ic <<std::endl;

    bool earlyReject = false;
    // Drawing samples must be done on rank 0
    if (world.rank() == 0 )
    {
      Real n = normalDist(generator); // Draw from N(0,1)

      logTInletProp = logTInlet + propSigma*n; // draw the inlet

      tInletProp = exp(logTInletProp);

      n = normalDist(generator); // draw another N(0,1)

      // Sample log T from the proposal distribution focused on current
      logTWallProp = logTWall + propSigma*n;

      // wall temperature
      tWallProp = exp(logTWallProp);

      std::cout << "tWallProp = " << tWallProp << ": ";
      // Rejection sampling of the temperature
      if (tWallProp > tInletProp || tWallProp<0.25 ) // infeasible
      {
        // std::cout<< "Early Reject!" << std::endl;
        earlyReject = true;
        logTInletSamples[ic] = logTInlet; logTWallSamples[ic] = logTWall; // save the log state off
        outputSamples[ic] = output; // save the output off
        REJ++;
        foutputhist << std::setprecision(16) << logTWall <<", "
                    << std::setprecision(16) << logTInlet <<", "
                    << std::setprecision(16) << output << std::endl;
      }

    }

    // send the logic to the processors
#ifdef SANS_MPI
    boost::mpi::broadcast(world, earlyReject, 0 );
#endif

    if ( earlyReject  ) // if the last sample was rejected above, this will now trigger
      continue;

    // send the values to other processors
#ifdef SANS_MPI
    boost::mpi::broadcast(world, logTWallProp, 0 );
    boost::mpi::broadcast(world, tWallProp, 0 );
    boost::mpi::broadcast(world, logTInletProp, 0 );
    boost::mpi::broadcast(world, tInletProp, 0 );
#endif

    // Update the inlet dictionary
    HRef = Cp*tInletProp + 0.5*(uRef*uRef + vRef*vRef);
    BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;

    // Update the wall dicionary
    BCNoSlip[BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDEClass>::ParamsType::params.Twall] = tWallProp;


    // Create a new solver instance, dont need a new SolutionClass
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

    try
    {
      pInterface->solveGlobalPrimalProblem();
    }
    catch (...) // If the solver fails, its a rejection
    {
      std::cout<< "Solver Reject!" << std::endl;
      // REJECT the point
      REJ++;
      // tInletSamples[ic] = tInlet; tWallSamples[ic] = tWall; // save the state off
      logTInletSamples[ic] = logTInlet; logTWallSamples[ic] = logTWall; // save the log state off
      outputSamples[ic] = output; // save the output off
      continue;
    }
    //
    // pInterface = pInterfaceNew; // Take control of the last pointer
    // pGlobalSol = pGlobalSolNew;

    // Compute the outputs
    QuadratureOrder quadratureOrder( *pxfld, quadOrder );

    propOutput = 0.0;
     // Calculate temperature sensor outputs
     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_0,propOutput[0]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
        quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_1,propOutput[1]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_2,propOutput[2]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_3,propOutput[3]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2(outputIntegrandSensor_4,propOutput[4]),
       *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld), quadratureOrder.cellOrders.data(),
       quadratureOrder.cellOrders.size() );

     pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
       FunctionalBoundaryTrace_Dispatch_DGBR2(heatFluxOutputIntegrand,
         pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
         quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(),
         propOutput[5] ) );
     pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
         FunctionalBoundaryTrace_Dispatch_DGBR2(dragOutputIntegrand,
           pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
           quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(),
         propOutput[6] ) );

 #ifdef SANS_MPI
     propOutput = boost::mpi::all_reduce( *pxfld->comm(), propOutput,  std::plus<Vector7>() );
 #endif


    bool acc=false;
    // Calculate the acceptance probability
    if ( world.rank() == 0 )
    {
      // calculate the priors log pdfs
      // priorWallProp = - 0.5*pow( logTWallProp - muWall,2)/sigWall;
      // priorInletProp = - 0.5*pow( logTInletProp - muInlet,2)/sigInlet;

      // the log likelihood of the data
      propLogLikelihood = 0.0;
      for (int i = 0; i < 7; i++) // which sensors to include
        propLogLikelihood = -precM(i)*pow(propOutput(i) - data(i),2)/2;

      Real alpha = MIN(exp( propLogLikelihood - logLikelihood ),1.0);
      // std::cout << "propLogLikelihood = " << propLogLikelihood
      //           << ", logLikelihood =" << logLikelihood
      //           << ", alpha = " << alpha << std::endl;
      // Real alpha = MIN(exp( priorWallProp + priorInletProp + propLogLikelihood
      //                     - priorWall - priorInlet - logLikelihood ),1.0);

                            // alpha = alpha < 1.0 ? alpha : 1.0 ; // acceptance probability
      acc = uniformDist(generator) < alpha; // is it accepted?
      if ( acc )
      {
        // std::cout<< "Accept! Current Ratio = " << ACC/((Real)(ACC+REJ)) << std::endl;
        // ACCEPT the point
        //tInlet = tInletProp; tWall = tWallProp; // update the state
        logTWall = logTWallProp; logTInlet = logTInletProp; // update the log state
        logLikelihood = propLogLikelihood; // update the log likelihood
        // priorWall = priorWallProp; priorInlet = priorInletProp; // update the log priors
        output = propOutput; // update the output
        ACC++;
      }
      else
      {
        // std::cout<< "Reject!" << std::endl;
        // REJECT the point
        REJ++;
      }
      // tInletSamples[ic] = tInlet; tWallSamples[ic] = tWall; // save the state off
      logTInletSamples[ic] = logTInlet; logTWallSamples[ic] = logTWall; // save the log state off
      outputSamples[ic] = output; // save the output off
    }

    // std::cout << "Rank = " << world.rank() << ", wait for acceptance " << yFprop << std::endl;
#ifdef SANS_MPI
    boost::mpi::broadcast(world, acc, 0 ); // send the accept status to everybody
    boost::mpi::broadcast(world, ACC, 0 ); // current accepted number
#endif
    // std::cout << "Rank = " << world.rank() << ", acc = " << acc << std::endl;

    if ( acc && (ACC%reAdaptIter == 0) & (ic > burnIn) ) // accepted 100 sample -> adapt the mesh
    {
      Real tWallMean=0, tInletMean=0;
      if (world.rank() == 0)
      {
        // Adapt the mesh to the current sample mean
        // sum the current samples
        Real logTWallMean, logTInletMean;

        logTWallMean = std::accumulate(logTWallSamples.begin()+burnIn, logTWallSamples.begin()+ic, 0.0);
        logTInletMean = std::accumulate(logTInletSamples.begin()+burnIn, logTInletSamples.begin()+ic, 0.0);
        logTWallMean /= ic; logTInletMean /= ic;

        tWallMean = exp(logTWallMean); tInletMean = exp(logTInletMean); // take the exponential of the log vars

        std::cout << "Adapt! Accept Ratio = " << ACC/((Real)(ACC+REJ)) << std::endl;
        std::cout << "tWallMean = " << std::setprecision(4) << tWallMean
                  << ", tInletMean = " << std::setprecision(4) << tInletMean << std::endl;
      }

      // std::cout << "Rank = " << world.rank() << ", wait for means " << yFprop << std::endl;
#ifdef SANS_MPI
      boost::mpi::broadcast(world, tWallMean, 0 );
      boost::mpi::broadcast(world, tInletMean, 0 );
#endif
      // std::cout << "Rank = " << world.rank() << ", xFMean = " << xFMean << ", yFMean = " << yFMean << std::endl;

      // Update the inlet dictionary
      HRef = Cp*tInletMean + 0.5*(uRef*uRef + vRef*vRef);
      BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;

      // Update the wall dicionary
      BCNoSlip[BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDEClass>::ParamsType::params.Twall] = tWallMean;

      // Create a new solution instance, don't need to update the fields
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);


      // Solve the primal
      pInterface->solveGlobalPrimalProblem();

      // Solve the adjoint
      pInterface->solveGlobalAdjointProblem();

      //Compute error estimates
      pInterface->computeErrorEstimates();

  #if PRINT
      std::string qfld_filename = filename_base + "qfld_M" + std::to_string(ACC) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      // std::string adjfld_filename = filename_base + "adjfld_M" + std::to_string(ACC) + ".plt";
      // output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
      //
      // std::string efld_filename = filename_base + "efld_M" + std::to_string(ACC) + ".plt";
      // pInterface->output_EField(efld_filename);
  #endif

      std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
      pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, ic);

      interiorTraceGroups.clear();
      for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<SolutionClass> pGlobalSolNew;
      pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                      BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                      active_boundaries, disc);

      //Perform L2 projection from solution on previous mesh
      pGlobalSolNew->setSolution(*pGlobalSol);

      std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
      pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                             cellGroups, interiorTraceGroups,
                                                             PyBCList, BCBoundaryGroups,
                                                             SolverContinuationDict, LinearSolverDict,
                                                             outputIntegrand);

      //Update pointers to the newest problem (this deletes the previous mesh and solutions)
      pxfld = pxfldNew;
      pGlobalSol = pGlobalSolNew;
      pInterface = pInterfaceNew;

      // Do not perform a solve on the new grid. This is only done for the new sample!
    }


    if (world.rank() == 0)
    {
      foutputhist << std::setprecision(16) << logTWall <<", "
                  << std::setprecision(16) << logTInlet <<", "
                  << std::setprecision(16) << output << std::endl;
    }

  }
  foutputhist.close();

  fstream foutputhistFinal;

  // Write the samples to file. Only rank 0 has the correct list!
  if (world.rank() == 0)
  {

    Real logTWallMean = std::accumulate(logTWallSamples.begin()+burnIn, logTWallSamples.end(), 0.0);
    Real logTInletMean = std::accumulate(logTInletSamples.begin()+burnIn, logTInletSamples.end(), 0.0);
    logTWallMean /= logTWallSamples.size()-burnIn; logTInletMean /= logTInletSamples.size()-burnIn;

    Real tWallMean = exp(logTWallMean), tInletMean = exp(logTInletMean);

    std::cout << "Accept Ratio = " << ACC/((Real)(ACC+REJ)) << std::endl;
    std::cout << "tWallMean = " << std::setprecision(4) << tWallMean
              << ", tInletMean = " << std::setprecision(4) << tInletMean << std::endl;

    std::cout << "Time elapsed: " << tic.elapsed() << " s" << std::endl;

    std::string outputFileName = filename_base + "outputFinal.dat";
    foutputhistFinal.open( outputFileName, fstream::out );
    BOOST_REQUIRE_MESSAGE(foutputhistFinal.good(), "Error opening file: " + outputFileName);
    foutputhistFinal << "tWall, tInlet, output" << std::endl;
    for ( int i = 0; i < chain; i++) // loop over the chain
    {
      foutputhistFinal << std::setprecision(16) << logTWallSamples[i] <<", "
                  << std::setprecision(16) << logTInletSamples[i] <<", "
                  << std::setprecision(16) << outputSamples[i] << std::endl;
    }
  }
  // std::cout<< "Rank = " << world.rank() << " closed" << std::endl;
  foutputhistFinal.close();
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
