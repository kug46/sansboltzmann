// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "tools/linspace.h"
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/Distance/BCEikonal_Distance2D.h"
#include "pde/Distance/PDEEikonal_Distance2D.h"

#include "pde/BCParameters.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"


#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "Meshing/EPIC/XField_PX.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#include "Field/DistanceFunction/DistanceFunction.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//#######################################################################//
BOOST_AUTO_TEST_SUITE (Solve2D_CG_Eikonal_test_suite)

//-----------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_CG_Eikonal )
{
  typedef PDEEikonal_Distance2D PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef BCEional2DVector BCVector;

  typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector, AlgEqSetTraits_Sparse,
      XField<PhysD2, TopoD2> > PrimalEquationSetClass;

  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;


  // Newton Solver Set Up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict, NewtonLineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1.0e-48;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  // Check inputs
  NewtonSolverParam::checkInputs( NewtonSolverDict );

  // Create the solver object
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //PDE

  NDPDEClass pde;

  //BC

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //int iB = XField2D_Box_Triangle_X1::iBottom;
  //int iR = XField2D_Box_Triangle_X1::iRight;
  //int iT = XField2D_Box_Triangle_X1::iTop;
  //int iL = XField2D_Box_Triangle_X1::iLeft;

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCFar;
  //BCFar[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  //BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  //BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  //BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;
  BCFar[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCAdia;
  BCAdia[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict PyBCList;

  PyBCList["BCWall"] = BCWall;
  PyBCList["BCFar"] = BCFar;
  PyBCList["BCAdia"] = BCAdia;

  BCBoundaryGroups["BCWall"] = { 0 };
  BCBoundaryGroups["BCFar"]  = { 1, 2 };
  BCBoundaryGroups["BCAdia"] = {  };

  BCParams::checkInputs( PyBCList );

  const std::vector<int> BoundaryGroups;

  int ordermin = 1;
  int ordermax = 1;

  for (int order = ordermin; order <= ordermax; order++)
  {
    int powermin = 2;
    int powermax = 2;

    for (int power = powermin; power <= powermax; power++)
    {

      int ii = 50;
#if 0
      int jj = ii;
      int xmin = 0.0;
      int xmax = 1.;
      int ymin = 0.0;
      int ymax = 1.;

      XField2D_Box_Triangle_X1 xfld( ii, jj, xmin, xmax, ymin, ymax );
#else
      // Mesh Read in
      mpi::communicator world;

      //string filein = "grids/Airfoil_with_BCs_spline_experimentv5";
      string filein = "grids/Joukowski_Laminar_Challenge_tri_ref0_Q3";
      filein += ".grm";
      XField_PX<PhysD2, TopoD2> xfld(world, filein);
#endif

      //Intergration
      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = { 1e-9, 1e-9 };

      //Create Solution
      Field_CG_Cell<PhysD2, TopoD2, Real> qfld( xfld, order, BasisFunctionCategory_Lagrange );

      qfld = 0.1;

      //Lagrange Multipliers

      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Lagrange,
                                                            BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups ) );
      lgfld = 0;

      // Spatial Discretization

      StabilizationNitsche stab(order);
      PrimalEquationSetClass PrimalEqSet( xfld, qfld, lgfld, pde, stab,
          quadratureOrder, ResidualNorm_L2, tol, { 0 }, PyBCList, BCBoundaryGroups );
      NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );
      // set initial condition from current solution in solution fields
      SystemVectorClass sln0( PrimalEqSet.vectorStateSize() );
      PrimalEqSet.fillSystemVector( sln0 );

      // nonlinear solve
      SystemVectorClass sln( PrimalEqSet.vectorStateSize() );
      SolveStatus status = nonlinear_solver.solve( sln0, sln );
      BOOST_CHECK( status.converged );


      //Create distance field
      Field_CG_Cell<PhysD2, TopoD2, Real> distfld( xfld, order, BasisFunctionCategory_Lagrange );

      //Compute distance field
      DistanceFunction(distfld, {0});

      // Final Conditions
      {
        string filename = "tmp/Solve2D_CG_Eikonal_Box";
        filename += "_Q" + to_string( order );
        filename += "_" + to_string( ii );
        filename += ".plt";
        cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( qfld, filename );
      }

      {
        string filename = "tmp/Solve2D_CG_Distance_Box";
        filename += "_Q" + std::to_string(order);
        filename += "_" + to_string( ii );
        filename += ".plt";
        cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( distfld, filename );
      }
      // cout << Dis<<  endl;
    } //grid refinment loop
  } //order loop
//########################################################################################

  BOOST_AUTO_TEST_SUITE_END()

}
