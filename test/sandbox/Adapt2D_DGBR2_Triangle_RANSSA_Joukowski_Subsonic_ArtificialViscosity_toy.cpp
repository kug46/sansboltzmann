// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adapt2D_DGBR2_Triangle_RANSSA_Joukowski_Subsonic_ArtificialViscosity_toy

//#define SANS_FULLTEST
//#define SANS_VERBOSE
#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>

#include <boost/filesystem.hpp> // to automagically make the directories
#include <boost/filesystem/operations.hpp>

#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsRANSSAArtificialViscosity.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"
#include "pde/NS/EulerArtificialViscosityType.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/DistanceFunction/DistanceFunction.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Meshing/EPIC/XField_PX.h"
#include "Field/output_grm.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


using namespace std;

namespace SANS
{

template <class T>
using SAnt2D_rhovP = SAnt2D<DensityVelocityPressure2D<T>>;

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_DGBR2_Triangle_RANSSA_Joukowski_Subsonic_ArtificialViscosity_toy )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_DGBR2_Triangle_RANSSA_Joukowski_Subsonic_ArtificialViscosity_toy )
{
  typedef QTypePrimitiveRhoPressure QType;

  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCRANSSAmitAVDiffusion2DVector<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputRANSSA2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
#if 0
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#endif
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_DG_Distance ParamBuilderType;
  typedef GenHField_DG<PhysD2, TopoD2> GenHFieldType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  Real viscousEtaParameter = 2*Triangle::NEdge;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {8e-7, 5e-7};

  RoeEntropyFix entropyFix = eVanLeer;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = false;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const Real tRef = 1.0;          // temperature
  const Real pRef = 1.0;          // pressure
  const Real lRef = 1;            // length scale

  const Real Mach = 0.5;
  const Real Reynolds = 1e6;
  const Real Prandtl = 0.72;
  const Real chiRef = 3;

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;

//  const Real lRef = 1;                            // length scale
  const Real rhoRef = 1;                            // density scale
//  const Real qRef = 0.1;                          // velocity scale
  const Real aoaRef = 0.00 * M_PI / 180.0;          // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  // Grid

//#define RESTART

#ifndef RESTART
  const int startIter = 0;
//  std::string file_initial_mesh = "grids/joukowski_initial_mesh/ransstart_q3.grm";
//  std::string file_initial_linear_mesh = "grids/joukowski_initial_mesh/ransstart_q1.grm";

  std::string file_initial_mesh = "grids/joukowski_initial_mesh/jouk_q3.grm";
  std::string file_initial_linear_mesh = "grids/joukowski_initial_mesh/jouk_q1_surf_swapped.grm";

//  std::string file_initial_mesh = "tmp/jouk/RANS/Mach1_2/16k_P1/xfld_a" + std::to_string(5) + ".grm";
//  std::string file_initial_linear_mesh = "tmp/jouk/RANS/Mach1_2/16k_P1/xfld_linear_a" + std::to_string(5) + ".grm";

#else
  const int startIter = 4;
  std::string file_initial_mesh = filename_base + "xfld_a" + std::to_string(startIter) + ".grm";
  std::string file_initial_linear_mesh = filename_base + "xfld_linear_a" + std::to_string(startIter) + ".grm";

  std::cout<< "Starting from " << file_initial_mesh << std::endl;
#endif

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField_PX<PhysD2, TopoD2>(world, file_initial_mesh) );
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld_linear( new XField_PX<PhysD2, TopoD2>(world, file_initial_linear_mesh) );

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;
//
//  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-14;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = true;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-14;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-20;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
    std::cout << "Linear solver: MKL_PARDISO" << std::endl;
    PyDict MKL_PARDISODict;
    MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;

#else
    std::cout << "Linear solver: UMFPACK" << std::endl;
    PyDict UMFPACKDict;
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
//  NewtonSolverDict[NewtonSolverParam::params.MaxChangeFraction] = 0.1;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 1e2;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  PyDict ParamDict;

  int orderL = 1, orderH = 3;
  int powerL = 2, powerH = 6;

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc == 3)
  {
    orderL = orderH = std::stoi(argv[1]);
    powerL = powerH = std::stoi(argv[2]);
  }
#endif

  std::cout << "orderL, orderH = " << orderL << ", " << orderH << std::endl;
  std::cout << "powerL, powerH = " << powerL << ", " << powerH << std::endl;

  //--------ADAPTATION LOOP--------
  for (int order = orderL; order <= orderH; order++)
  {
    for (int power = powerL; power <= powerH; power++ )
    {
      int maxIter = 30;

      int nk = pow(2,power);
      int targetCost = 1000*nk;
      std::string filename_base = "tmp/RANSJOUK/DG/SubsonicAV/P" + std::to_string(order) + "/" + std::to_string(nk) + "k/";

      // PDE
      GasModel gas(gamma, R);
      ViscosityModelType visc(muRef);
      ThermalConductivityModel tcond(Prandtl, Cp);

      PDEBaseClass pdeRANSSAAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                                     gas, visc, tcond, interp, entropyFix);
      // Sensor equation terms
      Sensor sensor(pdeRANSSAAV);
      SensorAdvectiveFlux sensor_adv(0.0, 0.0);
      SensorViscousFlux sensor_visc(order);
      SensorSource sensor_source(order, sensor);
      // AV PDE with sensor equation
      NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order,
                     hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                     gas, visc, tcond, interp, entropyFix);

      // initial condition
      AVVariable<SAnt2D_rhovP,Real> qdata({rhoRef, uRef, vRef, pRef, ntRef}, 0.0);
      ArrayQ q0;
      pde.setDOFFrom( q0, qdata );

      PyDict BCWall;
      BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

      PyDict StateVector;
      StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
      StateVector[Conservative2DParams::params.rho] = rhoRef;
      StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
      StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
      const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
      StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;
      StateVector["rhont"]  = rhoRef*ntRef; //TODO: not sure about this, was previously just "nt" which gave a Python dictionary error

      PyDict BCIn;
      BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
      BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
      BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

      PyDict PyBCList;
      PyBCList["BCNoSlip"] = BCWall;
      PyBCList["BCIn"] = BCIn;

      std::map<std::string, std::vector<int>> BCBoundaryGroups;

      // Define the BoundaryGroups for each boundary condition
      BCBoundaryGroups["BCNoSlip"] = {0,1};
      BCBoundaryGroups["BCIn"] = {2,3};

      //Check the BC dictionary
      BCParams::checkInputs(PyBCList);

      std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
      // Entropy output
      NDOutputClass fcnOutput(pde);
      OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
      // Drag output
      NDOutputClass outputFcn(pde, cos(aoaRef), sin(aoaRef));
      OutputIntegrandClass outputIntegrand( outputFcn, {0,1} );

      // DRAG INTEGRAND
      typedef BCRANSSA2D<BCTypeWall_mitState, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
      typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
      typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
      typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

      NDBCClass bc(pde);
      IntegrandBCClass fcnBC( pde, bc, {0,1}, disc );

      NDOutputClass outputFcn2(pde, -sin(aoaRef), cos(aoaRef));
      OutputIntegrandClass outputIntegrand2( outputFcn2, {0,1} );
#endif

      //--------ADAPTATION LOOP--------

      std::string adapthist_filename = filename_base + "test.adapthist";
#ifndef RESTART
      fstream fadapthist( adapthist_filename, fstream::out );
#else
      fstream fadapthist( adapthist_filename, fstream::app );
#endif

      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
      MesherDict[EpicParams::params.CurvedQOrder] = 3; //Mesh order
      MesherDict[EpicParams::params.CSF] = "grids/joukowski_initial_mesh/jouk.csf"; //Geometry file
      MesherDict[EpicParams::params.GeometryList2D] = "Airfoil Farfield"; //Geometry list
      MesherDict[EpicParams::params.minGeom] = 1e-6;
      MesherDict[EpicParams::params.maxGeom] = 0.5;
      MesherDict[EpicParams::params.nPointGeom] = -1;
      MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Compute distance field
      std::shared_ptr<Field_CG_Cell<PhysD2, TopoD2, Real>>
        pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfld, 4, BasisFunctionCategory_Lagrange);
      DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));
      // H field
      std::shared_ptr<GenHFieldType> phfld = std::make_shared<GenHFieldType>(*pxfld);

      //Solution data
      typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pdistfld, *pxfld), pde, order, order+1,
                                                   BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                   active_boundaries, ParamDict, disc);
      pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

#ifndef RESTART
      output_Tecplot( get<1>(pGlobalSol->paramfld), filename_base + "distfld_a0.plt" );
#endif

      const int quadOrder = 3*(order + 1); // Is this high enough?

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      //Set initial solution
      pGlobalSol->setSolution(q0);

#ifndef RESTART
      std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );
#endif

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();
      //Compute error estimates
      pInterface->computeErrorEstimates();

#ifndef RESTART
      std::string qfld_filename = filename_base + "qfld_a0.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_a0.plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

      std::string rfld_filename = filename_base + "rfld_a0.plt";
      std::string sfld_filename = filename_base + "sfld_a0.plt";
      std::string efld_filename = filename_base + "efld_a0.plt";

      if (world.rank() == 0)
      {
        std::cout<< "output e and rflds" <<std::endl;
      }
      pInterface->output_EField(efld_filename);
    //  output_Tecplot_LO( pGlobalSol->primal.rfld, rfld_filename );
    //  output_Tecplot_LO( pGlobalSol->adjoint.rfld, sfld_filename );
#endif

      for (int iter = startIter; iter < maxIter+1; iter++)
      {
        if (world.rank() == 0)
        {
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;
        }


        //Perform local sampling and adapt mesh
        MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;
        ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew_linear = ptr_pair.first;
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew = ptr_pair.second;

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        //Compute distance field
        pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfldNew, 4, BasisFunctionCategory_Lagrange);
        DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));
        // H field
        std::shared_ptr<GenHFieldType> phfld = std::make_shared<GenHFieldType>(*pxfldNew);

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>((*phfld, *pdistfld, *pxfldNew), pde, order, order+1,
                                                        BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                        active_boundaries, ParamDict, disc);
        pGlobalSolNew->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, LinearSolverDict,
                                                               outputIntegrand);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld_linear = pxfldNew_linear;
        pxfld = pxfldNew;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();
        //Compute error estimates
        pInterface->computeErrorEstimates();

        QuadratureOrder quadratureOrder( *pxfld, 3*order+1 );//3*order + 1 );

        // Monitor Drag Error
        Real Lift = pInterface->getOutput();

        Real Cl = Lift / (0.5*rhoRef*qRef*qRef*lRef);
        if (world.rank() == 0)
        {
          cout << "P = " << order << " power = " << targetCost
              << ": Cl = " << std::setprecision(12) << Cl;
          cout << endl;
        }

        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

        std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".plt";
        std::string rfld_filename = filename_base + "rfld_a" + std::to_string(iter+1) + ".plt";
        std::string sfld_filename = filename_base + "sfld_a" + std::to_string(iter+1) + ".plt";

        if (world.rank() == 0)
        {
          std::cout<< "output e and rflds" <<std::endl;
        }
        pInterface->output_EField(efld_filename);
    //    output_Tecplot_LO( pGlobalSol->primal.rfld, rfld_filename );
    //    output_Tecplot_LO( pGlobalSol->adjoint.rfld, sfld_filename );


        std::string xfld_linear_filename = filename_base + "xfld_linear_a" + std::to_string(iter+1) + ".grm";
        std::string xfld_filename = filename_base + "xfld_a" + std::to_string(iter+1) + ".grm";

        output_grm( *pxfld_linear, xfld_linear_filename);
        output_grm( *pxfld, xfld_filename);
      }

      fadapthist.close();
    }
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
