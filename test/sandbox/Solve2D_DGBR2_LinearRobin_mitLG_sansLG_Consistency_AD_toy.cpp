// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGBR2_LinearRobin_mitLG_sansLG_Consistency_AD_btest
// testing of 2-D DG with Advection-Diffusion

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_LinearRobin_mitLG_sansLG_Consistency_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_LinearRobin_mitLG_sansLG_Consistency_AD )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;

  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  const int D = PhysD2::D;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector, AlgEqSetTraits_Sparse,
                                     DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;

  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // PDE
  AdvectiveFlux2D_Uniform adv(0, 0);

  Real nu = 1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0., 0., 0.);
  typedef ForcingFunction2D_Const<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(1));

  NDPDEClass pde( adv, visc, source, forcingptr );

  const Real uL = 0.1;
//  const Real uR = 0.9;
//  const Real uB = 0.3;
//  const Real uT = 0.7;

  // BCs

  // Create a BC dictionary
//  PyDict BCNone;
//  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCDirichletL_mitLG;
  BCDirichletL_mitLG[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCDirichletL_mitLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.A] = 1;
  BCDirichletL_mitLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.B] = 0;
  BCDirichletL_mitLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.bcdata] = uL;

  PyDict BCDirichletL_sansLG;
  BCDirichletL_sansLG[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichletL_sansLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCDirichletL_sansLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCDirichletL_sansLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = uL;

//  PyDict BCDirichletL_sansLG;
//  BCDirichletL_sansLG[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
//  BCDirichletL_sansLG[BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitState>::params.qB] = uL;

//  PyDict BCDirichletR;
//  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
//  BCDirichletR[BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitState>::params.bcdata] = uR;
//
//  PyDict BCDirichletB;
//  BCDirichletB[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
//  BCDirichletB[BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitState>::params.bcdata] = uB;
//
//  PyDict BCDirichletT;
//  BCDirichletT[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
//  BCDirichletT[BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitState>::params.bcdata] = uT;

  PyDict PyBCList_mitLG;
  PyDict PyBCList_sansLG;
//  PyBCList["DirichletB"] = BCDirichletB;
//  PyBCList["DirichletR"] = BCDirichletR;
//  PyBCList["DirichletT"] = BCDirichletT;
  PyBCList_mitLG["DirichletL"] = BCDirichletL_mitLG;
//  PyBCList["None"] = BCNone;

  PyBCList_sansLG["DirichletL"] = BCDirichletL_sansLG;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
//  BCBoundaryGroups["DirichletB"] = {0}; //Bottom boundary
//  BCBoundaryGroups["DirichletR"] = {1}; //Right boundary
//  BCBoundaryGroups["DirichletT"] = {2}; //Top boundary
//  BCBoundaryGroups["DirichletL"] = {3}; //Left boundary
  BCBoundaryGroups["DirichletL"] = {0,1,2,3}; //Left boundary
//  BCBoundaryGroups["None"] = {0};

  //Check the BC dictionaries
  BCParams::checkInputs(PyBCList_mitLG);
  BCParams::checkInputs(PyBCList_sansLG);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  int order = 1;

  // grid:
  int ii = 6;
  int jj = ii;
//  XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 1, 0, 1 );
  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, 1, 0, 1);

  // solution: Hierarchical, C0
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_mitLG(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_sansLG(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld_mitLG = 0.0;
  qfld_sansLG = 0.0;
  const int nDOFPDE = qfld_mitLG.nDOF();

  // lifting operators
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld_mitLG(xfld, order, BasisFunctionCategory_Hierarchical);
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld_sansLG(xfld, order, BasisFunctionCategory_Hierarchical);
  rfld_mitLG = 0.0;
  rfld_sansLG = 0.0;
  const int nDOFLO = D*rfld_mitLG.nDOF();

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 0
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_mitLG( xfld, order, BasisFunctionCategory_Hierarchical,
                                                              BCParams::getLGBoundaryGroups(PyBCList_mitLG, BCBoundaryGroups) );
#elif 0
  std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_mitLG( xfld, order, BasisFunctionCategory_Hierarchical,
                                                                          BCParams::getLGBoundaryGroups(PyBCList_mitLG, BCBoundaryGroups) );
#else
  std::vector<int> active_BGroup_list_mitLG = BCParams::getLGBoundaryGroups(PyBCList_mitLG, BCBoundaryGroups);
  std::vector<int> active_BGroup_list_sansLG = BCParams::getLGBoundaryGroups(PyBCList_sansLG, BCBoundaryGroups);
//  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_mitLG( xfld, order, active_BGroup_list_mitLG );

  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_mitLG(xfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list_mitLG);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_sansLG(xfld, order, BasisFunctionCategory_Legendre, active_BGroup_list_sansLG);
#endif

  lgfld_mitLG = 0.0;
  lgfld_sansLG = 0.0;
  const int nDOFBC = lgfld_mitLG.nDOF();

//  std::cout<<"DOF: "<<(nDOFPDE + D*rfld_mitLG.nDOF() + nDOFBC)<<std::endl;

  //--------PRIMAL SOLVE------------

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-12, 1e-12};
  PrimalEquationSetClass PrimalEqSet_mitLG(xfld, qfld_mitLG, rfld_mitLG, lgfld_mitLG,
                                           pde, disc, quadratureOrder,
                                           ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList_mitLG, BCBoundaryGroups);
  PrimalEquationSetClass PrimalEqSet_sansLG(xfld, qfld_sansLG, rfld_sansLG, lgfld_sansLG,
                                            pde, disc, quadratureOrder,
                                            ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList_sansLG, BCBoundaryGroups);


  // residual
  SystemVectorClass sln_mitLG(PrimalEqSet_mitLG.vectorStateSize());
  SystemVectorClass sln_sansLG(PrimalEqSet_sansLG.vectorStateSize());

  SystemVectorClass rsd_mitLG(PrimalEqSet_mitLG.vectorEqSize());
  SystemVectorClass rsd_sansLG(PrimalEqSet_sansLG.vectorEqSize());

  PrimalEqSet_mitLG.fillSystemVector(sln_mitLG);
  PrimalEqSet_sansLG.fillSystemVector(sln_sansLG);

  rsd_mitLG = 0;
  rsd_sansLG = 0;
  PrimalEqSet_mitLG.residual(sln_mitLG, rsd_mitLG);
  PrimalEqSet_sansLG.residual(sln_sansLG, rsd_sansLG);
#if 0
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  // jacobian nonzero pattern
  SystemNonZeroPattern nz_mitLG(PrimalEqSet_mitLG.matrixSize());
  SystemNonZeroPattern nz_sansLG(PrimalEqSet_sansLG.matrixSize());
  PrimalEqSet_mitLG.jacobian(sln_mitLG, nz_mitLG);
  PrimalEqSet_sansLG.jacobian(sln_sansLG, nz_sansLG);

  // jacobian
  SystemMatrixClass jac_mitLG(nz_mitLG);
  SystemMatrixClass jac_sansLG(nz_sansLG);
  jac_mitLG = 0;
  jac_sansLG = 0;
  PrimalEqSet_mitLG.jacobian(sln_mitLG, jac_mitLG);
  PrimalEqSet_sansLG.jacobian(sln_sansLG, jac_sansLG);

      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac_sansLG, fout );
//      fstream fout00( "tmp/jac00.mtx", fstream::out );
//      cout << "btest: global jac00" << endl;  WriteMatrixMarketFile( jac(0,0), fout00 );
//      fstream fout10( "tmp/jac10.mtx", fstream::out );
//      cout << "btest: global jac10" << endl;  WriteMatrixMarketFile( jac(1,0), fout10 );
//      fstream fout01( "tmp/jac01.mtx", fstream::out );
//      cout << "btest: global jac01" << endl;  WriteMatrixMarketFile( jac(0,1), fout01 );
#endif

  // solve
  SLA::UMFPACK<SystemMatrixClass> solver_mitLG(PrimalEqSet_mitLG);
  SLA::UMFPACK<SystemMatrixClass> solver_sansLG(PrimalEqSet_sansLG);

  SystemVectorClass dsln_mitLG(sln_mitLG.size());
  SystemVectorClass dsln_sansLG(sln_sansLG.size());
  solver_mitLG.solve(rsd_mitLG, dsln_mitLG);
  solver_sansLG.solve(rsd_sansLG, dsln_sansLG);

  // update solutions
  sln_mitLG -= dsln_mitLG;
  sln_sansLG -= dsln_sansLG;
  PrimalEqSet_mitLG.setSolutionField(sln_mitLG);
  PrimalEqSet_sansLG.setSolutionField(sln_sansLG);

//  Real check_tol = 5e-10;

  //Check if both solutions are equal
//  for (int k = 0; k < nDOFLO; k++)
//    SANS_CHECK_CLOSE( sln_mitLG[0][k], sln_sansLG[0][k], check_tol, check_tol );
//
//  for (int k = 0; k < nDOFPDE; k++)
//    SANS_CHECK_CLOSE( sln_mitLG[1][k], sln_sansLG[1][k], check_tol, check_tol );

//  for (int k = 0; k < nDOFBC; k++)
//    SANS_CHECK_CLOSE( sln_mitLG[2][k], sln_sansLG[2][k], check_tol, check_tol );


  // check that the residuals are zero
  rsd_mitLG = 0;
  rsd_sansLG = 0;
  PrimalEqSet_mitLG.residual(sln_mitLG, rsd_mitLG);
  PrimalEqSet_sansLG.residual(sln_sansLG, rsd_sansLG);

  Real rsdLOnrm_mitLG = 0;
  Real rsdLOnrm_sansLG = 0;
  for (int n = 0; n < nDOFLO; n++)
  {
    rsdLOnrm_mitLG += pow(rsd_mitLG[0][n],2);
    rsdLOnrm_sansLG += pow(rsd_sansLG[0][n],2);
  }

  BOOST_CHECK_SMALL( sqrt(rsdLOnrm_mitLG), 1e-12 );
  BOOST_CHECK_SMALL( sqrt(rsdLOnrm_sansLG), 1e-12 );

  Real rsdPDEnrm_mitLG = 0;
  Real rsdPDEnrm_sansLG = 0;
  for (int n = 0; n < nDOFPDE; n++)
  {
    rsdPDEnrm_mitLG += pow(rsd_mitLG[1][n],2);
    rsdPDEnrm_sansLG += pow(rsd_sansLG[1][n],2);
  }

  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm_mitLG), 1e-12 );
  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm_sansLG), 1e-12 );

  Real rsdBCnrm_mitLG = 0;
  for (int n = 0; n < nDOFBC; n++)
    rsdBCnrm_mitLG += pow(rsd_mitLG[2][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdBCnrm_mitLG), 1e-12 );

#if 0
  // Tecplot dump
  string filename = "tmp/slnDGBR2_mitLG_P";
  filename += to_string(order);
  filename += "_";
  filename += to_string(ii);
  filename += "x";
  filename += to_string(jj);
  filename += ".plt";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot( qfld_mitLG, filename );

  filename = "tmp/slnDGBR2_sansLG_P";
  filename += to_string(order);
  filename += "_";
  filename += to_string(ii);
  filename += "x";
  filename += to_string(jj);
  filename += ".plt";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot( qfld_sansLG, filename );
#endif

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
