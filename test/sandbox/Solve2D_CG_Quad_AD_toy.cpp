// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_CG_Quad_AD_toy
// testing of 2-D CG with Advection-Diffusion on quads

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/Element/ElementProjection_L2.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_CG_Quad_AD_test_suite )


//----------------------------------------------------------------------------//
// solution: Hierarchical P1 thru P3
// Lagrange multiplier: continuous Hierarchical P1 thru P3
// exact solution: double BL
BOOST_AUTO_TEST_CASE( Solve_CG_Quad_AD )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCTypeLinearRobin_sansLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> AlgebraicEquationSet_GalerkinClass;
  typedef AlgebraicEquationSet_GalerkinClass::BCParams BCParams;

  // PDE

  AdvectiveFlux2D_Uniform adv( 1, 0 );

  Real nu = 0.001;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_None source;

  typedef ForcingFunction2D_Gaussian<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType);

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC

  PyDict BCDirichlet;
  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = 0.0;

  PyDict BCNeumann;
  BCNeumann[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 0.0;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 1.0;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = 0.0;

  PyDict PyBCList;
  PyBCList["BCDirichlet"] = BCDirichlet;
  PyBCList["BCNeumann"] = BCNeumann;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDirichlet"] = {3};
  BCBoundaryGroups["BCNeumann"] = {0, 1, 2};

  typedef AlgebraicEquationSet_GalerkinClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_GalerkinClass::SystemVector SystemVectorClass;

  typedef OutputCell_WeightedSolution<PDEClass, ScalarFunction2D_Gaussian> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  // integrands
  Real x0 = 0.75;
  Real y0 = 0.5;
  Real sx = 1./12.;
  Real sy = 1./6.;
  Real a = 1. / (2.*PI*sx*sy);
  ScalarFunction2D_Gaussian weighting( a,  x0, y0,  sx, sy );
  NDErrorClass fcnError(weighting);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_2D_CG.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_CG_Quad_AD_FullTest.txt", 1e-10, 1e-7, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_CG_Quad_AD_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  std::cout << std::setprecision(16) << std::scientific;
  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 3;
#else
  int ordermax = 1;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = 10*pow( 2, power );
      jj = ii;

      // grid:

      XField2D_Box_Quad_X1 xfld( ii, jj );

      // solution: Hierarchical, C0
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Lagrange);
      qfld = 0;

      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)

#if 1
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Lagrange );
#elif 0
      QField2D_CG_BoundaryEdge_Independent<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Lagrange );
#else
      QField2D_DG_BoundaryEdge<PDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#endif

      lgfld = 0;

      const int nDOFBC = lgfld.nDOF();

      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-11, 1e-11};

      StabilizationNitsche stab(order);
      AlgebraicEquationSet_GalerkinClass AlgEqSet(xfld, qfld, lgfld, pde, stab, quadratureOrder, ResidualNorm_L2,
                                                  tol, {0}, PyBCList, BCBoundaryGroups );

      // residual vectors

      SystemVectorClass q(AlgEqSet.vectorStateSize());
      SystemVectorClass rsd(AlgEqSet.vectorEqSize());

      AlgEqSet.fillSystemVector(q);

      rsd = 0;

      AlgEqSet.residual(q, rsd);

      // solve

      SLA::UMFPACK<SystemMatrixClass> solver(AlgEqSet);

      SystemVectorClass dq(q.size());

      solver.solve(rsd, dq);

      // updated solution

      q -= dq;

      AlgEqSet.setSolutionField(q);

      // check that the residual is zero

      rsd = 0;
      AlgEqSet.residual(q, rsd);

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

      // L2 solution error

      ArrayQ OutputValue = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_Galerkin( errorIntegrand, OutputValue ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = OutputValue;
      indx++;

#ifdef SANS_VERBOSE
      std::cout << std::setprecision(16) << std::scientific;
      cout << "P = " << order << " ii = " << ii << ": Output = " << OutputValue;
      if (indx > 1)
      {
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        Real slope = (log(normVec[indx-1]) - log(normVec[indx-2]))/(log(hVec[indx-1]) - log(hVec[indx-2]));
        cout << "  (slope = " << slope << ")";
      }
      cout << endl;
#endif

#if 1
      // Tecplot dump
      string filename = "tmp/slnCG_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
#endif

    }

    // Tecplot output
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
