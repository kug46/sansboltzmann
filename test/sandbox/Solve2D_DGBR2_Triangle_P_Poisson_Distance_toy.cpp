// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"
#include "tools/linspace.h"
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "Topology/Dimension.h"
#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/AdvectiveFlux2D.h"
#include "pde/AdvectionDiffusion/ViscousFlux2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/BCParameters.h"
#include "pde/AdvectionDiffusion/SolutionFunction2D.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/output_Tecplot.h"
#include "Meshing/EPIC/XField_PX.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Project.h"
#include "Discretization/Galerkin/IntegrandCell_ProjectDistance.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
//Instantiate
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//#######################################################################//
BOOST_AUTO_TEST_SUITE (Solve2D_DGBR2_Poisson_test_suite)

//-----------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Poisson )
{
  typedef PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_None, ViscousFlux2D_P_Laplacian, Source2D_None> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_None, ViscousFlux2D_P_Laplacian> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector, AlgEqSetTraits_Sparse,
      DGBR2, XField<PhysD2, TopoD2> > PrimalEquationSetClass;

  typedef ForcingFunction2D_Const<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType( 1.0 ) );
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef MakeTuple<FieldTuple, Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>>::type ProjectParamType;
  typedef IntegrandCell_ProjectDistance<PhysD2> IntegrandCellProject;
  typedef AlgebraicEquationSet_Project< ProjectParamType, IntegrandCellProject, TopoD2, AlgEqSetTraits_Sparse > ProjectionEquationSetClass;


  // Newton Solver Set Up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict, NewtonLineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1.0e-48;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  // Check inputs
  NewtonSolverParam::checkInputs( NewtonSolverDict );

  // Create the solver object
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //PDE
  Real P = 2;
  AdvectiveFlux2D_None adv;
  ViscousFlux2D_P_Laplacian visc( P );
  Source2D_None source;
  NDPDEClass pde( adv, visc, source, forcingptr );

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Distance Set Up

  //BC

  int iB = XField2D_Box_Triangle_X1::iBottom;
  int iR = XField2D_Box_Triangle_X1::iRight;
  int iT = XField2D_Box_Triangle_X1::iTop;
  int iL = XField2D_Box_Triangle_X1::iLeft;

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCWall[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCFar;
  BCFar[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCFar[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCAdia;
  BCAdia[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCAdia[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict PyBCList;

  PyBCList["BCWall"] = BCWall;
  PyBCList["BCFar"] = BCFar;
  PyBCList["BCAdia"] = BCAdia;

#if 1
  BCBoundaryGroups["BCWall"] =
  { iB};
  BCBoundaryGroups["BCFar"] =
  { iT};
  BCBoundaryGroups["BCAdia"] =
  { iR, iL};
#endif
  //BCBoundaryGroups["BCWall"] = {0};
  //BCBoundaryGroups["BCFar"] = {1,2};
  BCParams::checkInputs( PyBCList );

  const std::vector<int> BoundaryGroups;

  int ordermin = 3;
  int ordermax = 3;

  for (int order = ordermin; order <= ordermax; order++)
  {
    int powermin = 2;
    int powermax = 2;

    for (int power = powermin; power <= powermax; power++)
    {

      int ii = 20;
      int jj = ii;
      int xmin = 0.0;
      int xmax = 1.;
      int ymin = 0.0;
      int ymax = 1.;

      XField2D_Box_Triangle_X1 xfld( ii, jj, xmin, xmax, ymin, ymax );

      // Mesh Read in
#if 0 //Need to figure out how to assign which BC groups to what edge i want

      //string filein = "grids/Airfoil_with_BCs_spline_experimentv5";
      string filein = "grids/Airfoil_with_BCs_spline_experimentv5";
      filein += ".grm";
      XField_PX<PhysD2, TopoD2> xfld(filein);
      const std::vector<int> BoundaryGroups =
      { 0,1,2};
#endif

      // BR2 discretization
      Real viscousEtaParameter = 6;  // Not sure what this should be set to, need to investigate
      DiscretizationDGBR2 disc( 0, viscousEtaParameter );

      //Intergration
      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = { 1e-9, 1e-9 };

      // lifting operators
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld( xfld, order, BasisFunctionCategory_Hierarchical );
      rfld = 0.0;

      //Create Solution
      Field_DG_Cell<PhysD2, TopoD2, Real> qfld( xfld, order, BasisFunctionCategory_Hierarchical );

      qfld = 0.1;

      //Lagrange Multipliers

      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups ) );
      lgfld = 0;

      // Spatial Discretization

      PrimalEquationSetClass PrimalEqSet( xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                          ResidualNorm_Default, tol, { 0 }, { 0, 1, 2 }, PyBCList, BCBoundaryGroups );
      NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );
      // set initial condition from current solution in solution fields
      SystemVectorClass sln0( PrimalEqSet.vectorStateSize() );
      PrimalEqSet.fillSystemVector( sln0 );

      // nonlinear solve
      SystemVectorClass sln( PrimalEqSet.vectorStateSize() );
      SolveStatus status = nonlinear_solver.solve( sln0, sln );
      BOOST_CHECK( status.converged );

      //distance field
      Field_CG_Cell<PhysD2, TopoD2, Real> distfld( xfld, order, BasisFunctionCategory_Hierarchical );
      distfld = 0;

      IntegrandCellProject fcnCell( P, {0} );
      ProjectParamType paramfld = (qfld, xfld);
      const std::array<Real,1> tol_projection = {{1e-15}};
      ProjectionEquationSetClass ProjEqSet(paramfld, distfld, fcnCell, quadratureOrder, tol_projection );

      SystemVectorClass rhs(ProjEqSet.vectorEqSize());
      SystemVectorClass ddist(ProjEqSet.vectorStateSize());
      SystemVectorClass dist(ProjEqSet.vectorStateSize());

      // copy the field data to the vector
      ProjEqSet.fillSystemVector(dist);

      SLA::UMFPACK<SystemMatrixClass> solver(ProjEqSet);

      rhs = 0;
      ProjEqSet.residual(rhs);

      solver.solve(rhs, ddist);
      dist -= ddist;

      ProjEqSet.setSolutionField(dist);

      // Final Conditions
      {
        string filename = "tmp/Solve2D_DGBR2_P_Poisson_Box_Q_";
        filename += to_string( P );
        filename += "_";
        filename += to_string( ii );
        filename += ".plt";
        cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( qfld, filename );
      }

      {
        string filename = "tmp/Solve2D_DGBR2_P_Poisson_Box_Dist_";
        filename += to_string( P );
        filename += "_";
        filename += to_string( ii );
        filename += ".plt";
        cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( distfld, filename );
      }

      // cout << Dis<<  endl;
    } //grid refinement loop
  } //order loop
//########################################################################################

  BOOST_AUTO_TEST_SUITE_END()

}
