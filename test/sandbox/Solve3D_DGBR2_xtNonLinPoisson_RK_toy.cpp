// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_3D_DGBR2_NonLinPoisson_btest
// testing of 3-D DG with a non-linear Poisson problem
//
// The exact solution is: ???
//
// Q(x) = 1/lam * ln( (A + B*x + C*y + D*z)/ E)
//
// where A, B, C, E and D are arbitrary coefficients determined completely by the BC's.

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"    //Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AdvectionDiffusion/ForcingFunction3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
//#include "pde/OutputCell_SolutionErrorSquared.h"
//#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "NonLinearSolver/NonLinearSolver.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"
#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h" // Added for Transient

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

//Instantiate
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//#############################################################################
BOOST_AUTO_TEST_SUITE( Solve3D_DGBR2_xtNonLinPoisson_test_suite )

BOOST_AUTO_TEST_CASE( Solve3D_DGBR2_xtNonLinPoisson )
{
  typedef PDEAdvectionDiffusion<PhysD3, AdvectiveFlux3D_None, ViscousFlux3D_Poly, Source3D_None> PDEClass;

  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_None, ViscousFlux3D_Poly> BCVector;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector, AlgEqSetTraits_Sparse, DGBR2,
                                                            XField<PhysD3, TopoD3>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> RKClass;

  typedef ScalarFunction3D_Gaussian SolutionClass;
  typedef SolnNDConvertSpace<PhysD3, SolutionClass> SolutionNDClass;

  //typedef ForcingFunction3D_Gaussian<PDEClass> ForcingType;

  // global communicator
  mpi::communicator world;

  // Set up NEWTON solver:
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 60;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);



  // PDE
  GlobalTime time;
  Real Tperiod, dt;
  int nsteps;

  time = 0.;
  Tperiod = 5;
  dt = 0.1;

  nsteps = int (Tperiod/dt);

  AdvectiveFlux3D_None adv;

  Real A = 15.265;
  Real B = 0;//-31.1;
  ViscousFlux3D_Poly visc( A, B );

  Source3D_None source;

  //Define Forcing Function (-ve of the function needed)
  //Real fa,fc,fx0, fsigma, fy0, fz0;
  //fc = 0.5;
  //fa = 2; // should be -2.. but the inverse is needed..
  //fx0 = fy0 = fz0 = 0;
  //fsigma = -2;
  //std::shared_ptr<ForcingType> forcingptr( new ForcingType(fa, fc, fx0, fy0, fz0, fsigma));


  NDPDEClass pde( time, adv, visc, source);//, forcingptr);

  // BC
  PyDict BCNeumann;
  BCNeumann[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCNeumann[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.A] = 0;
  BCNeumann[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.B] = 1;
  BCNeumann[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCDirichlet_high;
  BCDirichlet_high[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichlet_high[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCDirichlet_high[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCDirichlet_high[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict BCDirichlet_low;
  BCDirichlet_low[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichlet_low[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCDirichlet_low[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCDirichlet_low[BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params.bcdata] = 0;

  PyDict PyBCList;
  PyBCList["BCDirichlet_high"] = BCDirichlet_high;
  PyBCList["BCDirichlet_low"] = BCDirichlet_low;
  PyBCList["BCNeumann"] = BCNeumann;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Set up initial field
  PyDict solnArgs;
  Real sigma, x0, y0, z0;
  sigma = 1;//0.03;
  x0 = y0 = z0 = 0;
  SolutionNDClass solnExact(sigma, x0, y0, z0);

  // Define Boundary Groups for each Boundary Condition
  BCBoundaryGroups["BCNeumann"] =
  { XField3D_Box_Tet_X1::iZmin,XField3D_Box_Tet_X1::iXmin,
     XField3D_Box_Tet_X1::iYmin};
  BCBoundaryGroups["BCDirichlet_high"] =
  { };//XField3D_Box_Tet_X1::iXmin};
  BCBoundaryGroups["BCDirichlet_low"] =
  {XField3D_Box_Tet_X1::iZmax, XField3D_Box_Tet_X1::iYmax,
   XField3D_Box_Tet_X1::iXmax };//XField3D_Box_Tet_X1::iXmax};

  //DGBR2 Discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc( 0, viscousEtaParameter );

  int RKorder = 2; // Fix RK Order
  int order = 2; // Fix Field order


  // loop over grid resolution: 2^power
  int ii, jj, kk;
  int power = 3;
  ii = pow( 2, power );
  jj = 1*ii;
  kk = 1*ii;

  // grid:
  Real xmin, xmax, ymin, ymax, zmin, zmax;
  xmin = 0;//-20.;
  xmax = 20.;
  ymin = 0;//-20.;
  ymax = 20.;
  zmin = 0;//-20.;
  zmax = 20.;
  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk, xmin, xmax, ymin, ymax, zmin, zmax);

  // Solution: Hierarchical
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld( xfld, order, BasisFunctionCategory_Hierarchical );
  // Initialize
  for_each_CellGroup<TopoD3>:: apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
  //qfld = 300;

  // lifting operators
  FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld( xfld, order, BasisFunctionCategory_Hierarchical );
  rfld = 0;

  //
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                        BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups ) );
  lgfld = 0;

  // Integration :: Quadrature order
  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = { 1e-9, 1e-9 };

  // Spatial Discretization
  PrimalEquationSetClass PrimalEqSet( xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                      ResidualNorm_Default, tol, { 0 }, { 0,1,2,3 }, PyBCList, BCBoundaryGroups, time );

  int RKtype = 0;

  int RKstages = RKorder;
  // Temporal Discretization:
  //RKClass RK(RKorder, RKstages, dt, time, xfld, qfld, NonLinearSolverDict, pde, {0}, {0,1,2,3}, PrimalEqSet);
  RKClass RK(RKorder, RKstages, RKtype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, PrimalEqSet);

  std::cout << "Total Number of timesteps: " << nsteps << std::endl;
  // Tecplot Output
  /*int FieldOrder = order;
  string filename = "tmp/solnDG_LFxtNonLinPoisson_Gaussian_P";
  filename += to_string( order );
  filename += "_Q";
  filename += to_string( FieldOrder );
  filename += "_TimeStep_";
  filename += "Initial";
  filename += "_test.plt";
  output_Tecplot( qfld, filename );*/
#if 1
  for (int step = 0; step < nsteps; step++)
  {
    std::cout << "Time Step: " << step+1 << "\n value of time is " << time << std::endl;
    // Advance solution
    RK.march(1);
    // Tecplot Output
    int FieldOrder = order;
    string filename = "tmp/solnDG_xtestNonLinPoisson_Gaussian_P";
    filename += to_string( order );
    filename += "_Q";
    filename += to_string( FieldOrder );
    filename += "_Time_";
    int f_time = time*10;
    filename += to_string(f_time);
    filename += "_time.plt";
    output_Tecplot( qfld, filename );

  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
