// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_Euler_ArtificialViscosity_ST_toy
// Testing of the MOESS framework on a space-time Buckley-Leverett problem with artificial viscosity

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/PDEEuler1D_Source.h"
#include "pde/NS/BCEulermitAVSensor1D.h"
#include "pde/NS/SolutionFunction_Euler1D.h"
#include "pde/NS/Fluids1D_Sensor.h"
#include "pde/NS/OutputEuler1D.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MeshAdapter.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"

#include "Meshing/gmsh/WriteMesh_gmsh.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"



// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

using namespace std;

//#define UNSTEADY_FORMULATION

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_Euler_ArtificialViscosity_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_Euler_ArtificialViscosity_ST_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
//  typedef PDEEuler1D_Source<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD1, PDEBaseClass> Sensor;
  typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEulermitAVSensor1DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler1D_WindowedPressureSquared<PDEBaseClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEulerQ1D_TotalPressure<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_CG ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler pde_principal
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 1.0;
  GasModel gas(gasModelDict);
  // pde_principal

  std::shared_ptr<ScalarFunction1D_ArtNozzle> area( new ScalarFunction1D_ArtNozzle() );
  PDEClass::EulerResidualInterpCategory interp = PDEClass::Euler_ResidInterp_Momentum;

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  //  See: "Riemann Solvers and Numerical Methods for Fluid Dynamics" - Toro (Section 4.3.3 Table 4.1 in my copy)
  // gas model
//  const Real gamma = 1.4;
//  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)

  // reference state (freestream)
  const Real pr = 1.4;
  const Real Pc = 1.0;
  const Real Tc = 1.0;
  const Real rhoc = 1;                   // density scale

  const Real pback = Pc / pr;

  PyDict stateVec;
  stateVec[NSVariableType1DParams::params.StateVector.Variables] = NSVariableType1DParams::params.StateVector.PrimitivePressure;
  stateVec[DensityVelocityPressure1DParams::params.rho] = rhoc;
  stateVec[DensityVelocityPressure1DParams::params.u] = 0.0;
  stateVec[DensityVelocityPressure1DParams::params.p] = Pc;

  // BC
  PyDict BCTimeOut;
  BCTimeOut[BCParams::params.BC.BCType] = BCParams::params.BC.TimeOut;

  typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDEClass> BCBase;
  typedef BCmitAVSensor1D<BCTypeFlux_mitState, BCBase> BCClass;

  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTt_SpaceTime_mitState;
  BCInflow[BCClass::ParamsType::params.TtSpec] = Tc;
  BCInflow[BCClass::ParamsType::params.PtSpec] = Pc;

  typedef SolutionFunction_Euler1D_ArtShock<TraitsSizeEuler, TraitsModelEulerClass> ICSolnClass;
  PyDict ICSoln;
  ICSoln[BCEuler1DParams<BCTypeFunction_mitState>::params.Function.Name] = "ArtShock";
  ICSoln[ICSolnClass::ParamsType::params.xThroat] = 0.5;
  ICSoln[ICSolnClass::ParamsType::params.xShock] = 0.8150186197870202;
  ICSoln[ICSolnClass::ParamsType::params.pt1] = Pc;
  ICSoln[ICSolnClass::ParamsType::params.pt2] = 0.947561264038085938;
  ICSoln[ICSolnClass::ParamsType::params.Tt] = Tc;
  ICSoln[ICSolnClass::ParamsType::params.pe] = pback;
  ICSoln[ICSolnClass::ParamsType::params.gasModel] = gasModelDict;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  TimeIC[BCEuler1DParams<BCTypeFunction_mitState>::params.Function] = ICSoln;
  TimeIC[BCEuler1DParams<BCTypeFunction_mitState>::params.Characteristic] = true;


#if 0
  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_SpaceTime_mitState;
  BCOut[BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState>::params.pSpec] = Pc / pr;
#else

  typedef SolutionFunction_Euler1D_SineBackPressure<TraitsSizeEuler, TraitsModelEulerClass> BackPressureSolnClass;
  PyDict OutSoln;
  OutSoln[BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState>::params.Function.Name] = "SineBackPressure";
  OutSoln[BackPressureSolnClass::ParamsType::params.rho] = rhoc;
  OutSoln[BackPressureSolnClass::ParamsType::params.u] = 0.0;
  OutSoln[BackPressureSolnClass::ParamsType::params.p] = pback;
  OutSoln[BackPressureSolnClass::ParamsType::params.dp] = 0.05;
  OutSoln[BackPressureSolnClass::ParamsType::params.dt] = 1.0;
  OutSoln[BackPressureSolnClass::ParamsType::params.gasModel] = gasModelDict;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_PressureFunction_SpaceTime_mitState;
  BCOut[BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState>::params.Function] = OutSoln;
#endif


  // Define BC list
  PyDict PyBCList;
  PyBCList["TimeIC"] = TimeIC;
  PyBCList["BCInflow"] = BCInflow;
  PyBCList["BCOut"] = BCOut;
  PyBCList["None"] = BCTimeOut;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Define the BoundaryGroups for each boundary condition
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["TimeIC"] = {0}; //Bottom boundary
  BCBoundaryGroups["BCOut"] = {1}; // right
  BCBoundaryGroups["BCInflow"] = {3}; //left
  BCBoundaryGroups["None"] = {2}; //Top boundary

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

//  std::string gmsh_filename2 = "tmp/STinit.msh";
//  WriteMesh_gmsh( *pxfld, gmsh_filename2, BCBoundaryGroups );
  ////////////////////////////////////////////////////////////////////////////////////////
  // Create parameters
  ////////////////////////////////////////////////////////////////////////////////////////
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-9, 1e-9};

  ////////////////////////////////////////////////////////////////////////////////////////
  // Newton Solver
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict SolverContinuationDict, NonlinearSolverDict, LinearSolverDict, NewtonSolverDict, LineUpdateDict;
//#if defined(SANS_PETSC)
//  if (world.rank() == 0)
//    std::cout << "Linear solver: PETSc" << std::endl;
//
//  PyDict PreconditionerDict;
//  PyDict PreconditionerILU;
//  PyDict PETScDict;
//
//  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
////  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 1;
//
//  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
//  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
//
//  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-8;
//  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-16;
//  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 20000;
//  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 5000;
//  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
//  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
//  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
////  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
//
//  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict), PreconditionerILUAdjoint(PreconditionerILU);
//
//  PreconditionerILUAdjoint[SLA::PreconditionerILUParam::params.FillLevel] = 0;
//
//  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 1;
//  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILUAdjoint;
//
//  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 10000;
//  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
//  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;
//  PETScDictAdjoint[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
//
//  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
//  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
//#elif defined(INTEL_MKL)
//  if (world.rank() == 0)
//    std::cout << "Using MKL\n";
//
//  PyDict MKL_PARDISODict;
//  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
//  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
//  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
//#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
//#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-12;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.0;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 500;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  PyDict SolverContinuationDict0;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.5;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  SolverContinuationDict0[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict0);


  ////////////////////////////////////////////////////////////////////////////////////////
  // Adaptation parameters
  ////////////////////////////////////////////////////////////////////////////////////////

  for (int power=0; power<3; power++)
  {

  int order = 2;
  int maxIter = 20;
  int target = 5*pow(2,power);
  Real targetCost = target*1000.0;


  ////////////////////////////////////////////////////////////////////////////////////////
  // Create initial grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Real Tend = 15;
  int ii = 10; //grid size - using timesteps = grid size
  int tt = 100;

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, tt, 0, 1, 0, Tend, true );

  std::vector<int> cellGroups;
  for ( int i = 0; i < pxfld->nCellGroups(); i++)
    cellGroups.push_back(i);

  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  std::shared_ptr<GenHField_CG<PhysD2,TopoD2>> phfld =
      std::make_shared<GenHField_CG<PhysD2,TopoD2>>(*pxfld);
  ////////////////////////////////////////////////////////////////////////////////////////

  std::string filebase = "tmp/NozzleAdapt_Windowed_P" + std::to_string(order) + "_" + std::to_string(target) + "k/";

  boost::filesystem::path base_dir(filebase);
  if ( not boost::filesystem::exists(base_dir) )
    boost::filesystem::create_directory(base_dir);


  std::string adapthist_filename = filebase +  "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filebase;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create ProblemStatement
  ////////////////////////////////////////////////////////////////////////////////////////
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, area);
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0);
  SensorViscousFlux sensor_visc(order, true);
  SensorSource sensor_source(order, sensor);
  // AV PDE with sensor equation
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order, hasSpaceTimeDiffusion,
                 EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, area);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output Functional
  ////////////////////////////////////////////////////////////////////////////////////////
  Real t1 = 9.0;
  Real t2 = 14.0;
  NDOutputClass fcnOutput(pdeEulerAV, t1, t2, pback);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  PyDict ParamDict;
  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pxfld), pde, order, order,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, ParamDict, disc);

  pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

  const int quadOrder = 2*(order + 1);

  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict0, LinearSolverDict,
                                                      outputIntegrand);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set Initial condition
  ////////////////////////////////////////////////////////////////////////////////////////
//  pGlobalSol->setSolution0(solnExact, cellGroups);
  ArrayQ q0 = pde.setDOFFrom( AVVariable<DensityVelocityPressure1D, Real>({{rhoc, 0.0, Pc}, 0.0}) );
  pGlobalSol->setSolution(q0);

  ////////////////////////////////////////////////////////////////////////////////////////
  auto starttime = std::chrono::high_resolution_clock::now();
  std::string qfld_init_filename = filebase + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename);

  pInterface->solveGlobalPrimalProblem();

  std::string qfld_filename = filebase + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

  std::string gmsh_filename = filebase + "xfld_a0.msh";
  WriteMesh_gmsh( pGlobalSol->primal.qfld.getXField(), gmsh_filename, BCBoundaryGroups );

  pInterface->solveGlobalAdjointProblem();

  std::string adjfld_filename = filebase + "adjfld_a0.plt";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    //Compute hfld
    std::shared_ptr<GenHField_CG<PhysD2,TopoD2>> phfldNew =
        std::make_shared<GenHField_CG<PhysD2,TopoD2>>(*pxfldNew);

    //    break;

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>((*phfldNew, *pxfldNew), pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, ParamDict, disc);

    pGlobalSolNew->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    phfld = phfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    std::string qfld_init_filename = filebase + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();

    std::string qfld_filename = filebase + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string gmsh_filename = filebase + "xfld_a" + std::to_string(iter+1) + ".msh";
    WriteMesh_gmsh( pGlobalSol->primal.qfld.getXField(), gmsh_filename, BCBoundaryGroups );

    pInterface->solveGlobalAdjointProblem();

    std::string adjfld_filename = filebase + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
  }
  auto stoptime = std::chrono::high_resolution_clock::now();
  std::cout << "P solve: " << std::chrono::duration_cast<std::chrono::milliseconds>(stoptime-starttime).count() << "ms" << std::endl;

  fadapthist.close();
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
