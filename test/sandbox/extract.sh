#!/bin/bash

# Start with an empty array.
DIRS=()

# For each file in /stuff/...
for FILE in *; do
    # If the file is a directory add it to the array. ("&&" is shorthand for
    # if/then.)
    [[ -d $FILE ]] && DIRS+=("$FILE")

    # (Normally variable expansions should have double quotes to preserve
    # whitespace; thanks to bash magic we don't them inside double brackets.
    # [[ ]] has special parsing rules.)
done

echo "${DIRS[@]}"
if [ -d "output" ]
then
	rm -r "output"
fi
mkdir "output" # create new output folder

# For each subfolder in folder
for i in "${DIRS[@]}"
do
	# copy test.adapthist from subfolder to parent folder
	# echo "$i/test.adapthist"
	if [ -f "$i/test.adapthist" ]
	then
		cp "$i/test.adapthist" "$i.adapthist"
		mv "$i.adapthist" "output/."
	fi
done
if [ -f "output.tar.gz" ]
then
	rm "output.tar.gz"
fi

tar -zcf "output.tar.gz" "output"

