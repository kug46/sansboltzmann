// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_DGBR2_TwoPhase_QuarterFiveSpot_BDF_btest
// Testing of the MOESS framework on a time-marching two-phase problem

#define USE_PETSC_SOLVER
#define USE_FV_TPFA

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"
#include "tools/timer.h"

#include "pyrite_fstream.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/PDETwoPhase2D.h"
#include "pde/PorousMedia/BCTwoPhase2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/SolutionFunction_TwoPhase1D.h"
#include "pde/PorousMedia/OutputTwoPhase.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/VectorFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/FV_TPFA/AlgebraicEquationSet_FV_TPFA.h"
#include "Discretization/FV_TPFA/IntegrandCell_FV_TPFA_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"

#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_Lagrange_X1.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_FV_TwoPhase_QuarterFiveSpot_BDF_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_FV_TwoPhase_QuarterFiveSpot_BDF )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTwoPhase2DVector<PDEClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;

  typedef OutputTwoPhase2D_Flowrate<SourceClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_FV_TPFA_Output<NDOutputClass> OutputIntegrandClass;

  typedef AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvertSpace, BCVector,
                                       AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> BDFClass;

  GlobalTime time(0);

  timer clock;

  mpi::communicator world;

  const Real Lx = 3000.0; //ft
  const Real Ly = Lx; //ft
  const Real T = 4000; //2500.0; //days

  const int Nx = 33;
  const int Ny = Nx;
  const int Nt = 312;

  int order = 0;
  BOOST_REQUIRE(order == 0); //Running FV!

  //BDF parameters
  Real dt = T/Nt;
  int BDForder = 1;

  bool exportSolutions = true;
//  std::string filename_base = "tmp/twophase_homo_BDF/FV_poly_m6_ILU0_MDF/L3k_T2_5k_FV_BDF"
//                              + to_string(BDForder) + "_Nx" + to_string(Nx) + "_Nt" + to_string(Nt) + "/";
  std::string filename_base = "tmp/";

  std::ofstream fwellhist(filename_base + "wellrate1.dat");
  BOOST_REQUIRE_MESSAGE(fwellhist.good(), "Error opening file: " + filename_base + "wellrate1.dat");
  if (world.rank() == 0)
    fwellhist << std::scientific << std::setprecision(15);

  // Grid
  typedef XField2D_Box_Quad_Lagrange_X1 XFieldClass;
  XFieldClass xfld( world, Nx, Ny, 0, Lx, 0, Ly );

  const int iXmin = XFieldClass::iLeft;
  const int iXmax = XFieldClass::iRight;
  const int iYmin = XFieldClass::iBottom;
  const int iYmax = XFieldClass::iTop;

  // PDE
  const Real pref = 14.7;

  const Real comp_switch = 1;
  DensityModel rhow(62.4, comp_switch*5.0e-6, pref);
  DensityModel rhon(52.1, comp_switch*1.5e-5, pref);

  PorosityModel phi(0.3, comp_switch*3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200; //mD

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kref, 0.0}, {0.0, Kref}};

#if 1 //Heterogeneous block
//  RockPermModel::QuadBlock quadblock0 = {{RockPermModel::Coord({{ 800.0, 1800.0}}),
//                                          RockPermModel::Coord({{1800.0,  800.0}}),
//                                          RockPermModel::Coord({{2200.0, 1200.0}}),
//                                          RockPermModel::Coord({{1200.0, 2200.0}})}};

  RockPermModel::QuadBlock quadblock0 = {{RockPermModel::Coord({{2000.0, 1000.0}}),
                                          RockPermModel::Coord({{2300.0, 1300.0}}),
                                          RockPermModel::Coord({{1100.0, 2200.0}}),
                                          RockPermModel::Coord({{ 800.0, 1400.0}})}};

  RockPermModel K(Kref_mat, {quadblock0}, {Kref_mat/100.0});
#elif 0 //Cosine-wave
  const Real Kref_cos = conversion*100; //mD
  const Real Kratio = 100.0;
  const Real amp = 0.05;
  const Real freq = 2.0;
  const Real width = 0.05;
  const Real yoffset = 0.5;
  RockPermModel K(Kref_cos, Kratio, Lx, Ly, amp, freq, width, yoffset);

  if (world.rank() == 0)
  {
    std::cout << "Kref_cos: " << Kref_cos / conversion << " mD" << std::endl;
    std::cout << "Kratio: " << Kratio << std::endl;
    std::cout << "Amplitude: " << amp << std::endl;
    std::cout << "Frequency: " << freq << std::endl;
    std::cout << "Width: " << width << std::endl;
    std::cout << "y-offset: " << yoffset << std::endl << std::endl;
  }
#else
  RockPermModel K(Kref_mat);
#endif

  const Real pc_max = 0*5.0;
  CapillaryModel pc(pc_max);

  // Set up source term PyDicts

  Real R_bore = 1.0/6.0; //ft
  Real R_well = 100.0; //ft
  Real L_offset = 500.0; //ft

  Real pnIn = 4000.0; //psi
  Real SwIn = 1.0;
  Real pnOut = 2000.0; //psi

  Real pwInit = 3000;
  Real SwInit = 0.1;

#if 0
  Real Q = 10.0*ft3_per_bbl; //[ft^3/day]
  Real q = Q / (0.25*PI*R_well*R_well*1.0); //[1/day]

  PyDict well_in;
  well_in[SourceTwoPhase2DType_FixedInflowRate_Param::params.Q] = q; // [1/day]
  well_in[SourceTwoPhase2DType_FixedInflowRate_Param::params.pn] = pnIn;
  well_in[SourceTwoPhase2DType_FixedInflowRate_Param::params.Sw] = SwIn;
  well_in[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedInflowRate;

  PyDict well_out;
  well_out[SourceTwoPhase2DType_FixedOutflowRate_Param::params.Q] = q; // [1/day]
  well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedOutflowRate;
#else
  const int nWellParam = 6;

  PyDict well_in;
  typedef SourceTwoPhase2DType_FixedPressureInflow_Param SourceInflowParamClass;
  well_in[SourceInflowParamClass::params.pB] = pnIn;
  well_in[SourceInflowParamClass::params.Sw] = SwIn;
  well_in[SourceInflowParamClass::params.Rwellbore] = R_bore;
  well_in[SourceInflowParamClass::params.nParam] = nWellParam;
  well_in[SourceInflowParamClass::params.WellModel] = SourceInflowParamClass::params.WellModel.Polynomial;
  well_in[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureInflow;

  PyDict well_out;
  typedef SourceTwoPhase2DType_FixedPressureOutflow_Param SourceOutflowParamClass;
  well_out[SourceOutflowParamClass::params.pB] = pnOut;
  well_out[SourceOutflowParamClass::params.Rwellbore] = R_bore;
  well_out[SourceOutflowParamClass::params.nParam] = nWellParam;
  well_out[SourceOutflowParamClass::params.WellModel] = SourceOutflowParamClass::params.WellModel.Polynomial;
  well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;
#endif

  PyDict source_injection;
  source_injection[SourceTwoPhase2DParam::params.Source] = well_in;
  source_injection[SourceTwoPhase2DParam::params.xcentroid] = L_offset;
  source_injection[SourceTwoPhase2DParam::params.ycentroid] = L_offset;
  source_injection[SourceTwoPhase2DParam::params.R] = R_well;
  source_injection[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_injection[SourceTwoPhase2DParam::params.Tmax] = T;
  source_injection[SourceTwoPhase2DParam::params.smoothLr] = 0.0*R_well;
  source_injection[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_injection[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_production;
  source_production[SourceTwoPhase2DParam::params.Source] = well_out;
  source_production[SourceTwoPhase2DParam::params.xcentroid] = Lx - L_offset;
  source_production[SourceTwoPhase2DParam::params.ycentroid] = Ly - L_offset;
  source_production[SourceTwoPhase2DParam::params.R] = R_well;
  source_production[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_production[SourceTwoPhase2DParam::params.Tmax] = T;
  source_production[SourceTwoPhase2DParam::params.smoothLr] = 0.0*R_well;
  source_production[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_production[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_list;
  source_list["injection_well"] = source_injection;
  source_list["production_well"] = source_production;

  SourceTwoPhase2DListParam::checkInputs(source_list);


  NDPDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list);

  // Initial solution
  ArrayQ qInit;
  PressureNonWet_SaturationWet<Real> qdata(pwInit, SwInit);
  pde.setDOFFrom( qInit, qdata );

  PyDict BCStateInit;
  BCStateInit[TwoPhaseVariableTypeParams::params.StateVector.Variables] =
              TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  BCStateInit[PressureNonWet_SaturationWet_Params::params.pn] = pwInit;
  BCStateInit[PressureNonWet_SaturationWet_Params::params.Sw] = SwInit;

  PyDict BCNoFlux;
  BCNoFlux[BCParams::params.BC.BCType] = BCParams::params.BC.NoFlux;

  PyDict PyBCList;
  PyBCList["BCNoFlux"] = BCNoFlux;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoFlux"] = {iXmin,iXmax,iYmin,iYmax};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<Real> tol = {1e-8};

  //Output functional
  bool computeMassFlow = false;
  SourceClass source(source_production, rhow, rhon, krw, krn, muw, mun, K, pc, computeMassFlow);
  std::vector<SourceClass> sourceList = { source };
  int phase = 1;
  NDOutputClass fcnOutput(sourceList, phase);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LineUpdateDict;

#if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+1); //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  if (world.rank() == 0) std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 50;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  // Check inputs
  NonLinearSolverParam::checkInputs(NonlinearSolverDict);

  const int cellQuadOrder = 3; // for integrating source terms

  //Print case summary
  if (world.rank() == 0)
  {
    std::cout << "Case parameters" << std::endl;
    std::cout << "---------------" << std::endl;
    std::cout << "p-order: " << order << std::endl;
    std::cout << "BDF-order: " << BDForder << std::endl;
    std::cout << "Processors: " << world.size() << std::endl;
    std::cout << "compressible_switch: " << comp_switch << std::endl;
    std::cout << "pc_max: " << pc_max << std::endl;
    std::cout << "nWellParam: " << nWellParam << std::endl;
    std::cout << "R_bore: " << R_bore << std::endl;
    std::cout << "R_well: " << R_well << std::endl;
    std::cout << "pnIn: " << pnIn << std::endl;
    std::cout << "pnOut: " << pnOut << std::endl;
    std::cout << "Residual tol: " << tol[0] << std::endl;
    std::cout << "Cell Quadrature order: " << cellQuadOrder << std::endl;
#if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
    std::cout << "ILU fill level: " << PreconditionerILU.get(SLA::PreconditionerILUParam::params.FillLevel) << std::endl;
    std::cout << "ILU ordering: " << PreconditionerILU.get(SLA::PreconditionerILUParam::params.Ordering) << std::endl;
    std::cout << "MDF blocksize: " << PreconditionerILU.get(SLA::PreconditionerILUParam::params.MDFOuterBlockSize) << std::endl;
    std::cout << "GMRES restart: " << PETScDict.get(SLA::PETScSolverParam::params.GMRES_Restart) << std::endl;
    std::cout << "PETSc RelativeTolerance: " << PETScDict.get(SLA::PETScSolverParam::params.RelativeTolerance) << std::endl;
    std::cout << "PETSc MaxIterations: " << PETScDict.get(SLA::PETScSolverParam::params.MaxIterations) << std::endl;
#endif
    std::cout << "---------------" << std::endl << std::endl;
  }


  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef Field_DG_Cell<PhysD2, TopoD2, ArrayQ> QFieldType;
  QFieldType qfld( xfld, order, BasisFunctionCategory_Legendre );

  //Set initial condition
  qfld = qInit;

  QuadratureOrder quadratureOrder( xfld, cellQuadOrder, 0, 0 );

  // Create AlgebraicEquationSets
  PrimalEquationSetClass AlgEqSetSpace(xfld, qfld, pde, quadratureOrder, ResidualNorm_L2, tol,
                                       cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, time);

  // The BDF class
  QuadratureOrder zeroQuadratureOrder( xfld, 0 );
  BDFClass BDF( BDForder, dt, time, xfld, qfld, NonlinearSolverDict, pde, zeroQuadratureOrder, cellGroups, AlgEqSetSpace );

#if 0 //Eigenvector dump
  Field_DG_Cell<PhysD2,TopoD2,ArrayQ> Vfld(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2,TopoD2,ArrayQ> VTfld(xfld, order, BasisFunctionCategory_Hierarchical);
  std::fstream fVfld("tmp/Vmin.txt", std::fstream::in);
  std::fstream fVTfld("tmp/VTmin.txt", std::fstream::in);
  for (int i = 0; i < Vfld.nDOF(); i++)
    for (int j = 0; j < ArrayQ::M; j++)
    {
      fVfld >> Vfld.DOF(i)[j];
      fVTfld >> VTfld.DOF(i)[j];
    }
  fVfld.close();
  fVTfld.close();

  output_Tecplot( Vfld, "tmp/Vfld.plt" );
  output_Tecplot( VTfld, "tmp/VTfld.plt" );
#endif

  std::vector<Real> output_hist(Nt+1, 0.0);

  // Set IC
  time = 0.0;

#if 0
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef BDFClass::AlgebraicEquationSet_TimeClass UnsteadyPrimalAlgEqnSetClass;
  UnsteadyPrimalAlgEqnSetClass& AlgEqSetUnsteady = BDF.getUnsteadyAlgEqnSet();

  // residual
  SystemVectorClass q(AlgEqSetSpace.vectorStateSize());
  AlgEqSetSpace.fillSystemVector(q);

  SystemVectorClass rsd(AlgEqSetSpace.vectorEqSize());
  rsd = 0;
  AlgEqSetSpace.residual(q, rsd);
  std::cout << std::setprecision(10) << std::scientific << AlgEqSetSpace.residualNorm(rsd) << std::endl;

  // jacobian nonzero pattern
  SystemNonZeroPattern nz(AlgEqSetSpace.matrixSize());
  AlgEqSetSpace.jacobian(q, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  jac = 0;
  AlgEqSetSpace.jacobian(q, jac);

#if 1
  fstream fout( "tmp/jac.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif
#endif

  for (int step = 0; step < Nt; step++)
  {
    if (world.rank() == 0)
      std::cout << std::endl << "Step: " << step << ", Time = " << time << std::endl;

    if (exportSolutions && (step % 50 == 0))
    {
      // Tecplot dump grid
      string filename = filename_base + "timeFV_TwophaseQuarterFive_P";
      filename += to_string(order);
      filename += "_n";
      filename += to_string(step);
      filename += ".plt";
      output_Tecplot( qfld, filename );
    }

    //Write well data
    if (world.rank() == 0)
      fwellhist << time << ", " << output_hist[step] << std::endl;

    // March the solution in time
    BDF.march(1);

    //Compute output
    IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_Galerkin( outputIntegrand, output_hist[step+1] ),
                                            xfld, qfld,
                                            quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#ifdef SANS_MPI
    output_hist[step+1] = boost::mpi::all_reduce( *get<-1>(xfld).comm(), output_hist[step+1], std::plus<Real>() );
#endif
  }

  if (world.rank() == 0)
    std::cout << "Time = " << time << std::endl;

  if (exportSolutions)
  {
    // Tecplot dump grid
    string filename = filename_base + "timeFV_TwophaseQuarterFive_P";
    filename += to_string(order);
    filename += "_n";
    filename += to_string(Nt);
    filename += ".plt";
    output_Tecplot( qfld, filename );
  }

  //Write well data
  if (world.rank() == 0)
  {
    fwellhist << time << ", " << output_hist[Nt] << std::endl;
    fwellhist.close();

    //Integrate flowrates to obtain output volume
    Real output_vol = 0.0;
    for (int n = 1; n <= Nt; n++)
      output_vol += output_hist[n]*dt;

    std::cout << std::scientific << std::setprecision(12);
    std::cout << "Volume produced (ft^3) = " << output_vol << std::endl;

    Real runtime = clock.elapsed();
    std::cout<<"Time elapsed: " << std::fixed << std::setprecision(3) << runtime << "s" << std::endl;

    std::cout << std::scientific << std::setprecision(12);
    std::cout << Nx << ", " << Ny << ", " << Nt << ", " << output_vol << ", " << runtime << std::endl;
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
