// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DG_AD_DoubleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_Solution.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"
#include "Meshing/EPIC/XField_PX.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif

#include "tools/linspace.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_DG_AD_DoubleBoundaryLayer_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_AD_DoubleBoundaryLayer_Triangle )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputCell_Solution<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef ScalarFunction2D_Quadratic BoundaryWeight;
  typedef OutputAdvectionDiffusion2D_FunctionWeightedResidual<BoundaryWeight> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;


  mpi::communicator world;

  // Grid
  std::vector<Real> xpts = linspace(0,1,5);
  std::vector<Real> ypts(xpts.begin(),xpts.end());

  Real a = 3./5.;
  Real b = 4./5.;
  Real nu = 1./50;

  // PDE
  AdvectiveFlux2D_Uniform adv( a, b );

  ViscousFlux2D_Uniform visc( nu, 0., 0., nu );

  Source2D_UniformGrad source(0.0, 0.0, 0.0);

  NDPDEClass pde( adv, visc, source );

  // BC

  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;
  DoubleBL[SolutionExact::ParamsType::params.offset] = 1;
  DoubleBL[SolutionExact::ParamsType::params.scale] = -1;

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict BCSoln_Flux;
  BCSoln_Flux[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln_Flux[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln_Flux[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln_Flux[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BC_Dirichlet"] = BCSoln_Dirichlet;
//  PyBCList["BC_Flux"] = BCSoln_Flux;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

/*
const int XField2D_Box_Triangle_Lagrange_X1::iBottom = 0;
const int XField2D_Box_Triangle_Lagrange_X1::iRight  = 1;
const int XField2D_Box_Triangle_Lagrange_X1::iTop    = 2;
const int XField2D_Box_Triangle_Lagrange_X1::iLeft   = 3;
*/

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BC_Dirichlet"] = {
                                      XField2D_Box_Triangle_Lagrange_X1::iBottom,
                                      XField2D_Box_Triangle_Lagrange_X1::iRight,
                                      XField2D_Box_Triangle_Lagrange_X1::iTop,
                                      XField2D_Box_Triangle_Lagrange_X1::iLeft
                                     };
  BCBoundaryGroups["BC_Flux"] = {
                                  // XField2D_Box_Triangle_Lagrange_X1::iBottom,
                                  // XField2D_Box_Triangle_Lagrange_X1::iRight,
                                  // XField2D_Box_Triangle_Lagrange_X1::iTop,
                                  // XField2D_Box_Triangle_Lagrange_X1::iLeft
                                };

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> mitLG_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 2*Triangle::NEdge;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-12, 1e-12};

  //Output functional
//  Real aa = 3.0;
#ifndef BOUNDARYOUTPUT
  // \int u - Volume Output
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // nu = 1./10
  // const Real trueOutput = (99. - 218.*exp(10) + 19*exp(20))/(100. * pow(exp(10)-1.,2) );

  // nu = 1./100
  // const Real trueOutput = 199./10000.;
  // const Real trueOutput = (9999. - 20198.*exp(100) + 199*exp(200))/(10000. * pow(exp(100)-1,2) );

  // nu = 1/30
  // const Real trueOutput = 59./900.; // From Mathematica
  // const Real trueOutput = (899. - 1858.*exp(30) + 59.*exp(60))/(900.* pow(exp(30)-1.,2));
  // const Real trueOutput = 0.065555555555374641511;

  // From mathematica!
  const Real trueOutput = ((-1 + exp(a/nu))*nu*(exp(b/nu)*(b - nu) + nu)
                    - a*(b*(-1 + exp(b/nu)) + exp(a/nu)*(b + nu - exp(b/nu)*nu)))
                    /(a*b*(-1 + exp(a/nu))*(-1 + exp(b/nu)));
#else
  // Quadratic function for weighting the output
  PyDict boundaryWeight;
  boundaryWeight[BoundaryWeight::ParamsType::params.a0] = 1;
  boundaryWeight[BoundaryWeight::ParamsType::params.ax] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.ay] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.axx] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.ayy] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.axy] = 0;

  // Residual weighted boundary output
  NDOutputClass fcnOutput(boundaryWeight);
  OutputIntegrandClass outputIntegrand( fcnOutput, {XField2D_Box_Triangle_Lagrange_X1::iRight} );

  // From mathematica! For w = 1 (on right boundary)
  const Real trueOutput = a - (a*exp((a + b)/nu))/((-1 + exp(a/nu))*(-1 + exp(b/nu))) + (a*exp(a/nu)*nu)/(b*(-1 + exp(a/nu)));

  // From mathematica! For w = xy (on top and right boundaries)
  // const Real trueOutput = -(pow(b,3)*(pow(a,2)*(-1 + exp(a/nu)) + exp(b/nu)*(pow(a,2)
  //   - 2*exp(a/nu)*(a - nu)*nu - 2*pow(nu,2))) + pow(a,3)*(pow(b,2)*(-1 + exp(b/nu))
  //   + exp(a/nu)*(pow(b,2) - 2*exp(b/nu)*(b - nu)*nu - 2*pow(nu,2))))
  //   /(2.*pow(a,2)*pow(b,2)*(-1 + exp(a/nu))*(-1 + exp(b/nu)));
#endif
  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

// #if defined(SANS_PETSC)
//   if (world.rank() == 0 )
//       std::cout << "Linear solver: PETSc" << std::endl;
//
//   PyDict PreconditionerDict;
//   PyDict PreconditionerILU;
//   PyDict PETScDict;
//
//   PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
//   PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//   PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//   PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 3;
//   // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order
//
//   PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
//   PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
//
//   PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//   PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
//   PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0.0;
//   PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
//   PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
//   PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
//   PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
// //  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
//   PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
//   // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
//   // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;
//
//   NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
//
//   //Change parameters for adjoint solve
//   PyDict PreconditionerILU_adjoint = PreconditionerILU;
//   PyDict PreconditionerDict_adjoint = PreconditionerDict;
//   PyDict PETScDict_adjoint = PETScDict;
//   // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
//   PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//   PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
//   PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
//   PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
//   PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
//
//   AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;
//
// #elif defined(INTEL_MKL)
//   std::cout << "Using MKL\n";
//   PyDict MKL_PARDISODict;
//   MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
//   AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
//   NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
// #else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
// #endif


  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::string mesher = "avro";
  std::string file_tag = "";
  bool dumpField = false;
  int maxIter = 40;

  int orderL = 1, orderH = 3;
  int powerL = 0, powerH = 8;

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc >= 2)
    mesher = std::string(argv[1]);
  if (argc >= 3)
    file_tag = std::string(argv[2]);
  if (argc >= 4)
    dumpField = std::stoi(argv[3]);
  if (argc >= 5)
    orderL = orderH = std::stoi(argv[4]);
  if (argc >= 6)
    powerL = powerH = std::stoi(argv[5]);
  if (argc >= 7)
    maxIter = std::stoi(argv[6]);

  std::cout << "mesher: " << mesher << ", file_tag: " << file_tag << ", dumpField: " << dumpField;
  std::cout << ", orderL,H = " << orderL << ", " << orderH << ", powerL,H = " << powerL << ", " << powerH << "maxIter: " << maxIter << std::endl;
#endif

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;

#define CHAIN_ADAPT 1  // Whether to use the lower DOF mesh as the initial condition for the next dof target

  //--------ADAPTATION LOOP--------
  for (int order = orderL; order <= orderH; order++)
  {

#if CHAIN_ADAPT

#ifdef SANS_AVRO
    std::shared_ptr<avro::Context> context;
#endif
    PyDict MesherDict;
    if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic)
    {
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;

      MesherDict[EpicParams::params.nThread] = 1;

      pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, xpts, ypts );
    }
#ifdef SANS_AVRO
    else if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro)
    {
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;

      MesherDict[avroParams::params.Curved] = false; // is the grid curved?
      MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
      MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files

      using avro::coord_t;
      using avro::index_t;

      // create the context
      context = std::make_shared<avro::Context>();

      coord_t number = 2;
      Real xc[3] = {.5,.5,0.};
      std::vector<avro::real> lens(number,1.);
      std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( context.get() , xc , lens[0] , lens[1] );
      std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(),"square");
      model->addBody(pbody,true);

      XField2D_Box_Triangle_Lagrange_X1 xfld0( world, xpts, ypts );
      // copy the mesh into the domain and attach the geometry
      pxfld = std::make_shared< XField_avro<PhysD2,TopoD2> >(xfld0, model);
    }
#endif
    else if (mesher == "fefloa")
    {
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

      // uniform initial
      // pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
      // Initial cross clustering
      pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, xpts, ypts );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown mesher");
#endif

#if CHAIN_ADAPT
    for (int power = powerL; power <= powerH; power++)
#else
    for (int power = powerH; power >= powerL; power--)
#endif
    {
      timer totalTime;
      int nk = pow(2,power);
      int targetCost = 125*nk;

      // to make sure folders have a consistent number of zero digits
      const int string_pad = 6;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
      std::string filename_base = "tmp/AD_2D/" + mesher + "_DG_";

      if (file_tag.size() > 0 && file_tag != "_")
        filename_base += file_tag + "_"; // the additional file name bits kept especially

      filename_base += int_pad + "_P" + std::to_string(order) + "/";

      boost::filesystem::create_directories(filename_base);

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Element;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;
      MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;
      MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;

#if not(CHAIN_ADAPT)
#ifdef SANS_AVRO
      std::shared_ptr<avro::Context> context;
#endif
      PyDict MesherDict;
      if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic)
      {
        MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
        MesherDict[EpicParams::params.nThread] = 1;

        // uniform initial
        // pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
        // Initial cross clustering
        pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, xpts, ypts );
      }
#ifdef SANS_AVRO
      else if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro)
      {
        MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;

        MesherDict[avroParams::params.Curved] = false; // is the grid curved?
        MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
        MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files

        using avro::coord_t;
        using avro::index_t;

        // create the context
        context = std::make_shared<avro::Context>();

        coord_t number = 2;
        Real xc[3] = {.5,.5,0.};
        std::vector<avro::real> lens(number,1.);
        std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( context.get() , xc , lens[0] , lens[1] );
        std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(),"square");
        model->addBody(pbody,true);

        XField2D_Box_Triangle_Lagrange_X1 xfld0( world, xpts, ypts );
        // copy the mesh into the domain and attach the geometry
        pxfld = std::make_shared< XField_avro<PhysD2,TopoD2> >(xfld0, model);
      }
#endif
      else if (mesher == "fefloa")
      {
        MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

        // uniform initial
        // pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
        // Initial cross clustering
        pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, xpts, ypts );
      }
      else
        SANS_DEVELOPER_EXCEPTION("Unknown mesher");
#endif

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpMetric] = false;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.hasTrueOutput] = true;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TrueOutput] = trueOutput;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                   mitLG_boundaries, disc);

      pGlobalSol->setSolution(0.0);

      const int quadOrder = 2*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrand);

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      if (dumpField)
      {
        std::string qfld_filename = filename_base + "qfld_a0.plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
      }

      //Compute error estimates
      pInterface->computeErrorEstimates();

      for (int iter = 0; iter < maxIter+1; iter++)
      {
        if (world.rank() == 0)
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        //Perform local sampling and adapt mesh
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
        pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                        BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                        mitLG_boundaries, disc);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, AdjLinearSolverDict,
                                                               outputIntegrand);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld = pxfldNew;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();

        if (dumpField)
        {
          std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
        }
        else if ( iter == maxIter )
        {
          std::string qfld_filename = filename_base + "qfld_final.plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
        }

        //Compute error estimates
        pInterface->computeErrorEstimates();

      }
      if (world.rank() == 0)
        fadapthist << "\n\nTotal Time elapsed: " << totalTime.elapsed() << "s" << std::endl;

      fadapthist.close();
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
