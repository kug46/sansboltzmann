// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_Galerkin_Stabilized_AD_Pollutant_toy
// Testing of the MOESS framework on the advection-diffusion pde

// #define BOUNDARYOUTPUT

// #define INNERPATCH
// #define WHOLEPATCH

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
 #include "Adaptation/MOESS/SolverInterface_AGLS.h"

#include "Adaptation/MeshAdapter.h"
#include "Meshing/EPIC/XField_PX.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "Field/output_grm.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#include "tools/timer.h"
#include "tools/linspace.h"

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_Galerkin_Stabilized_AD_Pollutant_toy_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_Galerkin_Stabilized_AD_Pollutant )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;
  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_WeightedSolution<PDEClass, ScalarFunction2D_Gaussian> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                   AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, ErrorIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  std::vector<Real> xpts = linspace(0,1,5);
  std::vector<Real> ypts(xpts.begin(),xpts.end());

  Real nu = 0.001;

  // PDE
  AdvectiveFlux2D_Uniform adv( 1.0, 0.0 );
  ViscousFlux2D_Uniform visc( nu, 0., 0., nu );
  Source2D_UniformGrad source(0.0, 0.0, 0.0);

  typedef ForcingFunction2D_Gaussian<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType);

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC

  PyDict BCDirichlet;
//  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
//  BCDirichlet[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
//  BCDirichlet[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;
//  BCDirichlet[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = 0.0;
  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeDirichlet_mitStateParam>::params.qB] = 0.0;

  PyDict BCNeumann;
//  BCNeumann[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
//  BCNeumann[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 0.0;
//  BCNeumann[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 1.0;
//  BCNeumann[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = 0.0;
//  BCNeumann[BCParams::params.BC.BCType] = BCParams::params.BC.Natural;
//  BCNeumann[BCAdvectionDiffusionParams<PhysD2,BCTypeNatural>::params.gB] = 0.0;
  BCNeumann[BCParams::params.BC.BCType] = BCParams::params.BC.Flux;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2,BCTypeFluxParams>::params.bcdata] = 0.0;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2,BCTypeFluxParams>::params.Inflow] = false;

  PyDict PyBCList;
  PyBCList["BCDirichlet"] = BCDirichlet;
  PyBCList["BCNeumann"] = BCNeumann;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDirichlet"] = {3};
  BCBoundaryGroups["BCNeumann"] = {0, 1, 2};

  BCParams::checkInputs(PyBCList);
  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // integrands
  Real x0 = 0.75;
  Real y0 = 0.5;
  Real sx = 1./12.;
  Real sy = 1./6.;
  Real a = 1. / (2.*PI*sx*sy);
  ScalarFunction2D_Gaussian weighting( a,  x0, y0,  sx, sy );
  NDErrorClass fcnError(weighting);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 ) std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 3;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0.0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;
#elif defined(INTEL_MKL)
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::string mesher = "Epic";
  std::string file_tag = "";
  bool dumpField = true;
  int maxIter = 500;

  int orderL = 1, orderH = 1;
  int powerL = 8, powerH = 8;

  StabilizationMatrix stab(StabilizationType::Unstabilized);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-11, 1e-11};

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    mesher = std::string(argv[1]);
  if (argc >= 3)
    file_tag = std::string(argv[2]);
  if (argc >= 4)
    dumpField = std::stoi(argv[3]);
  if (argc >= 5)
    orderL = orderH = std::stoi(argv[4]);
  if (argc >= 6)
    powerL = powerH = std::stoi(argv[5]);
  if (argc >= 7)
    maxIter = std::stoi(argv[6]);

  std::cout << "mesher: " << mesher << ", file_tag: " << file_tag << ", dumpField: " << dumpField;
  std::cout << ", orderL,H = " << orderL << ", " << orderH << ", powerL,H = " << powerL << ", " << powerH << "maxIter: " << maxIter << std::endl;
#endif

  const bool DGCONV = true; // use DG DOF counts. Converts to matching DG for elements

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;

#define CHAIN_ADAPT 0  // Whether to use the lower DOF mesh as the initial condition for the next dof target

  //--------ADAPTATION LOOP--------
  for (int order = orderL; order <= orderH; order++)
  {
    timer totalTime;
    stab.setStabOrder(order);
    stab.setNitscheOrder(order);

    PyDict MesherDict;
    if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic)
    {
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;

      pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, xpts, ypts );
    }
    else if (mesher == "fefloa")
    {
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

      // uniform initial
      // pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
      // Initial cross clustering
      pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, xpts, ypts );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown mesher");

    for (int power = powerH; power >= powerL; power--)
    {
      int nk = pow(2,power);
      int targetCost = 125*nk;

      // to make sure folders have a consistent number of zero digits
      const int string_pad = 6;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);

      if (DGCONV)
      {
        Real nDOFperCell_DG = (order+1)*(order+2)/2;

        Real nDOFperCell_CG = nDOFperCell_DG;
        nDOFperCell_CG -= (3 - 1./2); // the node dofs are shared by 6
        nDOFperCell_CG -= (3 - 3./2)*std::max(0,(order-1)); // if there are edge dofs they are shared by 2

        targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
      }

      std::string filename_base = "tmp/AD_2D/" + mesher + "_";
      if ( stab.getStabType() == StabilizationType::Adjoint )
      {
        filename_base += "VMS_";
      }
      else if ( stab.getStabType() == StabilizationType::Unstabilized )
      {
        filename_base += "CG_";
      }
      else if ( stab.getStabType() == StabilizationType::GLS )
      {
        filename_base += "GLS_";
      }
      else if ( stab.getStabType() == StabilizationType::AGLSPrimal )
      {
        filename_base += "AGLS_";
      }
      else if ( stab.getStabType() == StabilizationType::SUPG )
      {
        filename_base += "SUPG_";
      }

      // down convert to match DG dof counts
      if (DGCONV)
      {
        Real nDOFperCell_DG = (order+1)*(order+2)/2;

        Real nDOFperCell_CG = nDOFperCell_DG;
        nDOFperCell_CG -= (3 - 1./2); // the node dofs are shared by 6
        nDOFperCell_CG -= (3 - 3./2)*std::max(0,(order-1)); // if there are edge dofs they are shared by 2

        targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
      }


      if (file_tag.size() > 0 && file_tag != "_")
        filename_base += file_tag + "_"; // the additional file name bits kept especially

      filename_base += int_pad + "_P" + std::to_string(order) + "/";

      boost::filesystem::create_directories(filename_base);

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;
      MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;
      MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;

      PyDict MesherDict;
      if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic)
      {
        MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
        MesherDict[EpicParams::params.Version] = "madcap_devel-10.2";

        // MesherDict[EpicParams::params.nThread] = 4; // DO NOT USE THIS ON HYPERSONIC

        // uniform initial
        // pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
        // Initial cross clustering
        pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, xpts, ypts );
      }
      else if (mesher == "fefloa")
      {
        MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

        // uniform initial
        // pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
        // Initial cross clustering
        pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, xpts, ypts );
      }
      else
        SANS_DEVELOPER_EXCEPTION("Unknown mesher");

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpMetric] = false;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = true;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.hasTrueOutput] = true;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                   active_boundaries, stab);

      pGlobalSol->setSolution(0.0);

      const int quadOrder = 2*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          errorIntegrand);

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      if (dumpField)
      {
        std::string qfld_filename = filename_base + "qfld_a0.plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
      }

      //Compute error estimates
      pInterface->computeErrorEstimates();

      for (int iter = 0; iter < maxIter+1; iter++)
      {
        if (world.rank() == 0 )
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        //Perform local sampling and adapt mesh
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
        pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        // return;
        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                        BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                        active_boundaries, stab);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, AdjLinearSolverDict,
                                                               errorIntegrand);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld = pxfldNew;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();

        if (dumpField)
        {
          std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
        }
        else if ( iter == maxIter )
        {
          std::string qfld_filename = filename_base + "qfld_final.plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
        }

        //Compute error estimates
        pInterface->computeErrorEstimates();

        // std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".plt";
        // pInterface->output_EField(efld_filename);
      }
      if (world.rank() == 0)
        fadapthist << "\n\nTotal Time elapsed: " << totalTime.elapsed() << "s" << std::endl;

      fadapthist.close();

    // std::string xfld_filename = filename_base + "xfld_2D_UnstructuredExample.grm";
    // WriteMeshGrm( *pxfld, xfld_filename );
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
