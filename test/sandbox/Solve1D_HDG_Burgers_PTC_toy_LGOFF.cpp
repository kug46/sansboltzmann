// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_HDG_Burgers_PTC_btest
// testing of 1-D HDG with Burgers-Advection-Diffusion

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction_MMS.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandFunctor_HDG_PTC.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianCell_HDG_PTC.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "NonLinearSolver/NonLinearConjugateGradientSolver.h"

using namespace std;
using namespace SANS;

BOOST_AUTO_TEST_SUITE( Solve1D_HDG_Burgers_PTC_test_suite )

namespace // no-named namespace to make this private to this file
{

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
typedef SurrealS<1> SurrealClass;

typedef PDEAdvectionDiffusion<PhysD1,
                              LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>,
                              ViscousFlux1D_Uniform,
                              Source1D_Uniform > PDEAdvectionDiffusion1D;
typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
typedef PDEAdvectionDiffusion1D::template MatrixQ<Real> MatrixQ;
typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

typedef ScalarFunction1D_Tanh SolutionExact;
typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

typedef BCTypeFunctionLinearRobin_mitLG BCTypeSoln;
typedef BCAdvectionDiffusion<PhysD1,BCTypeSoln> BCClassRaw;
typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
typedef IntegrandCell_HDG_PTC<PDEClass> IntegrandCellPTCClass;
typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;
typedef IntegrandBoundaryTrace<PDEClass, BCClass, BCClass::Category, HDG> IntegrandBoundClass;
typedef IntegrandCell_SquareError<PDEClass,NDSolutionExact> IntegrandSquareErrorClass;

typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;

typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;


typedef SystemMatrixClass  SparseMatrix;
typedef SystemVectorClass  SparseVector;
typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;
typedef DLA::MatrixD<NonZeroPatternClass> NonZeroPattern;
//typedef SLA::SparseMatrix_CRS<Real>  SparseMatrix;
//typedef SLA::SparseVector<Real>       SparseVector;
typedef MatrixSizeType<SparseMatrix>::type MatrixSizeClass;
typedef VectorSizeType<SparseVector>::type VectorSizeClass;


template<class SystemVector, class SystemMatrix, class NonZeroPattern>
class AlgebraicEquationSetBase
{
public:
  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  virtual ~AlgebraicEquationSetBase() {}

  //Computes the residual
  virtual void residual(const SystemVector& X, SystemVector& rsd) = 0;

  //Computes the jacobian or fills the non-zero pattern of a jacobian
  virtual void jacobian(const SystemVector& X, SystemMatrix& mtx) = 0;
  virtual void jacobian(const SystemVector& X, NonZeroPattern& nz) = 0;
  virtual void jacobianTranspose(const SystemVector& X, SystemMatrix& mtx) = 0;
  virtual void jacobianTranspose(const SystemVector& X, NonZeroPattern& nz) = 0;

  //Limits the updated vector dX
  virtual void lineSearch(const SystemVector& X, SystemVector& dX ) = 0;

  //Used to compute norms of the residual and the update vector
  virtual void normResidual(const SystemVector& rsd, Real& nrmRsd) const = 0;
  virtual void normUpdate(const SystemVector& dX, Real& nrmUpd) const = 0;

  //Translates the system vector into a solution field
  //virtual void setSolutionField(const SystemVector& q) = 0;

  //Translates the solution field into a system vector
  //virtual void fillSystemVector(SystemVector& q) const = 0;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorSize() const = 0;
  virtual MatrixSizeClass matrixSize() const = 0;
};

template<class SparseMatrix, class NonZeroPattern>
class NonLinearSolverBase
{
public:

  typedef typename VectorType<SparseMatrix>::type SparseVector;
  typedef typename VectorType<SparseMatrix>::Viewtype SparseVectorView;

  virtual ~NonLinearSolverBase() {}

  virtual bool solve( const SparseVector& qold, SparseVector& q ) = 0;

protected:
};


struct NewtonSolverParam : noncopyable
{
  const ParameterDict LinearSolver{"LinearSolver", NO_DEFAULT, SLA::LinearSolverParam::checkInputs, "The Linear Solver Options for Newton-Raphson"};

  const ParameterNumeric<int > MinIterations    {"MinIterations"    , 0    , 0, NO_LIMIT, "Minimum Number of Newton Iterations"};
  const ParameterNumeric<int > MaxIterations    {"MaxIterations"    , 100  , 0, NO_LIMIT, "Maximum Number of Newton Iterations"};
  const ParameterNumeric<Real> ResidualTolerance{"ResidualTolerance", 1e-13, 0, NO_LIMIT, "Convergence Tolerance on the Residual Vector"};
  const ParameterNumeric<Real> UpdateTolerance  {"UpdateTolerance"  , 0    , 0, NO_LIMIT, "Convergence Tolerance on the Update Vector"};

  //static void checkInputs(PyDict d);
  static NewtonSolverParam params;
};

#if 0
void NewtonSolverParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.LinearSolver));
  allParams.push_back(d.checkInputs(params.MaxIterations));
  allParams.push_back(d.checkInputs(params.MinIterations));
  allParams.push_back(d.checkInputs(params.ResidualTolerance));
  allParams.push_back(d.checkInputs(params.UpdateTolerance));
  d.checkUnknownInputs(allParams);
}
#endif
NewtonSolverParam NewtonSolverParam::params;

template<class SystemMatrix, class NonZeroPattern>
class NewtonSolver : public NonLinearSolverBase<SystemMatrix, NonZeroPattern>
{
public:
  NewtonSolverParam& params;

  typedef typename VectorType<SystemMatrix>::type SystemVector;
  typedef typename VectorType<SystemMatrix>::Viewtype SystemVectorView;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  typedef AlgebraicEquationSetBase<SystemVector,SystemMatrix,NonZeroPattern> EquationSetBase;

  //The constructor for the non-zero pattern likely needs work
  template<class U>
  NewtonSolver( EquationSetBase& f, const PyDict& d, const std::initializer_list<U>& nz ) :
    params(NewtonSolverParam::params),
    f_(f), nz_(nz),
    LinearSolver_(d.get(params.LinearSolver)),
    MinIterations_(d.get(params.MinIterations)),
    MaxIterations_(d.get(params.MaxIterations)),
    ResidualTolerance_(d.get(params.ResidualTolerance)),
    UpdateTolerance_(d.get(params.UpdateTolerance))
  {
    if ( MaxIterations_ < MinIterations_ )
      MaxIterations_ = MinIterations_;
  }

  //The constructor for the non-zero pattern likely needs work
  NewtonSolver( EquationSetBase& f, const PyDict& d,NonZeroPattern& nz ) :
    params(NewtonSolverParam::params),
    f_(f), nz_(nz),
    LinearSolver_(d.get(params.LinearSolver)),
    MinIterations_(d.get(params.MinIterations)),
    MaxIterations_(d.get(params.MaxIterations)),
    ResidualTolerance_(d.get(params.ResidualTolerance)),
    UpdateTolerance_(d.get(params.UpdateTolerance))
  {
    if ( MaxIterations_ < MinIterations_ )
      MaxIterations_ = MinIterations_;
  }

  bool solve( const SystemVector& qold, SystemVector& q );

  // TODO:
  // Evaluate line search residuals to a file
  // Dump solutions each Newton iteration
  // Absolute and relative drop tolerances (study PETSc options for non-linear solvers)
  // Jacobian update frequency (evaluation as well as construction/destruction frequency)
  // Possibly use a separate class for managing the various controls

protected:
  EquationSetBase& f_;                             //The multivariate non-linear function that is solved
  NonZeroPattern nz_;                              //The non-zero pattern of the sparse matrix
  SLA::LinearSolver< SystemMatrix > LinearSolver_; //The sparse matrix linear solver
  int MinIterations_;                              //Minimum number of Newton iterations
  int MaxIterations_;                              //Maximum number of Newton iterations
  Real ResidualTolerance_;                         //Convergence tolerance on the residual vector
  Real UpdateTolerance_;                           //Convergence tolerance on update vector
};


template<class SystemMatrix, class NonZeroPattern>
bool
NewtonSolver<SystemMatrix,NonZeroPattern>::solve( const SystemVector& qold, SystemVector& q )
{
  SystemVector rsd(qold.size());
  SystemVector dq(qold.size());

//  std::fstream flout( "tmp/history.dat", std::fstream::out | std::fstream::app );
//  flout << std::setprecision(16) << std::scientific;
//  Real nrmRsdIni = 0;

  f_.jacobian(qold, nz_);

  //Should we create just a temporary matrix here? Should we pass in the matrix instead?
  SystemMatrix A(nz_);

  Real nrmRsd = ResidualTolerance_ + 1;
  Real nrmUpd = UpdateTolerance_ + 1;

  q = qold;

  for (int i = 0; i < MaxIterations_; i++)
  {
    rsd = 0;
    A = 0;
    f_.residual(q, rsd);
    f_.jacobian(q, A);

#if 0
    std::string filename = "tmp/jac";
    //filename += std::to_string(i);
    filename += ".mtx";
    std::fstream fout( filename, std::fstream::out );
    WriteMatrixMarketFile( A, fout );
#endif

    dq = LinearSolver_.inverse(A)*rsd;

#if 0
    SystemVector Rd(dq.size());
    SystemVector Qd(dq.size());
    Real fd = 0;
    std::string filename2 = "tmp/line";
    filename2 += std::to_string(i);
    filename2 += ".dat";
    std::fstream flout( filename2, std::fstream::out );
    int N = 1000;
    for (int j = 0; j < N+1; j++)
    {
      Real d = 4.0 * j / N - 2;
      Rd = 0;
      fd = 0;
      Qd = q - d*dq;
      f_.residual(Qd , Rd);
      f_.normResidual(Rd, fd);
      flout << d << " " << fd << std::endl;
    }
#endif

    f_.lineSearch(q, dq);

    q -= dq;

    f_.normResidual(rsd, nrmRsd);
    f_.normUpdate(dq, nrmUpd);

//    if (i == 0) nrmRsdIni = nrmRsd;

//    std::cout << i+1 << ", " << nrmRsd << std::endl;

    if ( (nrmRsd < ResidualTolerance_ || nrmUpd < UpdateTolerance_) && i >= MinIterations_)
    {
//      flout << nrmRsdIni << " " << nrmRsd << std::endl;
      return true;
    }
  }
//  flout << nrmRsdIni << " " << nrmRsd << std::endl;
  return false;
}

class BurgersHDGAlgebraicEquation : public AlgebraicEquationSetBase<SparseVector,SparseMatrix,NonZeroPattern>
{
public:

//  BurgersHDGAlgebraicEquation() {};
  BurgersHDGAlgebraicEquation(XField1D &xfld, int &order, PDEClass &pde, DiscretizationHDG<PDEClass> &disc,
                              BCClass &bc, const std::vector<int>& BoundaryGroups, NDSolutionExact& solnExact) :
    xfld_(xfld),
    order_(order),
    fcnCell_( pde, disc, {0} ),
    fcnInt_( pde, disc, {0} ),
    fcnBC_( pde, bc, BoundaryGroups, disc ),
    fcnErr_( solnExact, {0} ),
    fcnPTC_( pde, disc ),
    ufld_(xfld_, order_, BasisFunctionCategory_Legendre),
    qfld_(xfld_, order_, BasisFunctionCategory_Legendre),
    uIfld_(xfld_, order_, BasisFunctionCategory_Legendre),
    lgfld_(xfld_, order_, BasisFunctionCategory_Legendre, BoundaryGroups)
  {
    nDOFPDE = ufld_.nDOF();
    nDOFAu  = qfld_.nDOF();
    nDOFInt = uIfld_.nDOF();
    nDOFBC  = lgfld_.nDOF();
    CFL = 0.1;
  }

  virtual void residual(const SparseVector& X, SparseVector& rsd)
  {
    rsd = 0;

    // quadrature rule
    int quadratureOrder[2] = {-1, -1};
    int quadratureOrderTrace[2] = {0, 0};

    for (int k = 0; k < nDOFPDE; k++)
      ufld_.DOF(k) = X[0][k];

    for (int k = 0; k < nDOFPDE; k++)
      for (int d = 0; d < PhysD1::D; d++)
        qfld_.DOF(k)[d] = X[1][PhysD1::D*k+d];

    for (int k = 0; k < nDOFInt; k++)
      uIfld_.DOF(k) = X[2][k];

    for (int k = 0; k < nDOFBC; k++)
      lgfld_.DOF(k) = X[3][k];

    IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell_, rsd(0), rsd(1)),
                                            xfld_, (ufld_, qfld_), quadratureOrder, 1 );
    IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt_, rsd(0), rsd(1), rsd(2)),
                                                            xfld_, (ufld_, qfld_), uIfld_, quadratureOrderTrace, 1);
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC_, rsd(0), rsd(1), rsd(3)),
                                                        xfld_, (ufld_, qfld_), lgfld_, quadratureOrderTrace, 2 );
  }

  virtual void jacobian(const SparseVector& X, SparseMatrix& mtx)
  {
    jacobian<SparseMatrix>(X,mtx);
  }

//  virtual void jacobian(const SparseVector& X, SparseMatrixTranspose& mtx)
//  {
//    jacobian<SparseMatrixTranspose>(X,mtx);
//  }

  virtual void jacobian(const SparseVector& X, NonZeroPattern& nz)
  {
    // quadrature rule
    int quadratureOrderMin[2] = {0, 0};

    // Create non-zero sub-patterns
    NonZeroPatternClass& nzPDE_u  = nz(0,0);
    NonZeroPatternClass& nzPDE_q  = nz(0,1);
    NonZeroPatternClass& nzPDE_uI = nz(0,2);
    NonZeroPatternClass& nzPDE_lg = nz(0,3);

    NonZeroPatternClass& nzAu_u  = nz(1,0);
    NonZeroPatternClass& nzAu_q  = nz(1,1);
    NonZeroPatternClass& nzAu_uI = nz(1,2);
    NonZeroPatternClass& nzAu_lg = nz(1,3);

    NonZeroPatternClass& nzInt_u  = nz(2,0);
    NonZeroPatternClass& nzInt_q  = nz(2,1);
    NonZeroPatternClass& nzInt_uI = nz(2,2);
    //NonZeroPatternClass& nzInt_lg = nz(2,3);

    NonZeroPatternClass& nzBC_u  = nz(3,0);
    NonZeroPatternClass& nzBC_q  = nz(3,1);
    //NonZeroPatternClass& nzBC_uI = nz(3,2);
    NonZeroPatternClass& nzBC_lg = nz(3,3);

    IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell_, nzPDE_u, nzPDE_q,
                                                                                     nzAu_u, nzAu_q),
                                            xfld_, (ufld_, qfld_), quadratureOrderMin, 1 );

    IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
        JacobianInteriorTrace_HDG<SurrealClass>(fcnInt_, nzPDE_u, nzPDE_q, nzPDE_uI,
                                                         nzAu_u ,          nzAu_uI ,
                                                         nzInt_u, nzInt_q, nzInt_uI),
        xfld_, (ufld_, qfld_), uIfld_, quadratureOrderMin, 1);

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
        JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC_, nzPDE_u, nzPDE_q, nzPDE_lg,
                                                           nzAu_u , nzAu_q , nzAu_lg ,
                                                           nzBC_u , nzBC_q , nzBC_lg ),
        xfld_, (ufld_, qfld_), lgfld_, quadratureOrderMin, 2);

    JacobianCell_HDG_PTC<SurrealClass,TopoD1>::integrate( fcnPTC_, ufld_, qfld_, quadratureOrderMin, 1, nzPDE_u );
  }

  template<class SparseMatrix_type>
  void jacobian(const SparseVector& qfld, SparseMatrix_type& jac)
  {
    jac = 0;

    for (int k = 0; k < nDOFPDE; k++)
      ufld_.DOF(k) = qfld[0][k];

    for (int k = 0; k < nDOFPDE; k++)
      for (int d = 0; d < PhysD1::D; d++)
        qfld_.DOF(k)[d] = qfld[1][PhysD1::D*k+d];

    for (int k = 0; k < nDOFInt; k++)
      uIfld_.DOF(k) = qfld[2][k];

    for (int k = 0; k < nDOFBC; k++)
      lgfld_.DOF(k) = qfld[3][k];

    // quadrature rule
    int quadratureOrder[2] = {-1, -1};
    int quadratureOrderMin[2] = {0, 0};

    SparseMatrixClass& jacPDE_u  = jac(0,0);
    SparseMatrixClass& jacPDE_q  = jac(0,1);
    SparseMatrixClass& jacPDE_uI = jac(0,2);
    SparseMatrixClass& jacPDE_lg = jac(0,3);

    SparseMatrixClass& jacAu_u  = jac(1,0);
    SparseMatrixClass& jacAu_q  = jac(1,1);
    SparseMatrixClass& jacAu_uI = jac(1,2);
    SparseMatrixClass& jacAu_lg = jac(1,3);

    SparseMatrixClass& jacInt_u  = jac(2,0);
    SparseMatrixClass& jacInt_q  = jac(2,1);
    SparseMatrixClass& jacInt_uI = jac(2,2);
    //SparseMatrixClass& jacInt_lg = jac(2,3);

    SparseMatrixClass& jacBC_u  = jac(3,0);
    SparseMatrixClass& jacBC_q  = jac(3,1);
    //SparseMatrixClass& jacBC_uI = jac(3,2);
    SparseMatrixClass& jacBC_lg = jac(3,3);

    IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell_, jacPDE_u, jacPDE_q,
                                                                                     jacAu_u, jacAu_q),
                                            xfld_, (ufld_, qfld_), quadratureOrder, 1 );

    IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
        JacobianInteriorTrace_HDG<SurrealClass>(fcnInt_, jacPDE_u, jacPDE_q, jacPDE_uI,
                                                        jacAu_u ,           jacAu_uI ,
                                                        jacInt_u, jacInt_q, jacInt_uI),
        xfld_, (ufld_, qfld_), uIfld_, quadratureOrderMin, 1);

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
        JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC_, jacPDE_u, jacPDE_q, jacPDE_lg,
                                                           jacAu_u , jacAu_q , jacAu_lg ,
                                                           jacBC_u , jacBC_q , jacBC_lg ),
        xfld_, (ufld_, qfld_), lgfld_, quadratureOrder, 2);

    Real dx = xfld_.DOF(1)[0] - xfld_.DOF(0)[0];
    Real dt = CFL * dx / 1.0; // HACK 1.0 should be the max wave speed in this case
    fcnPTC_.setTimeStep( dt );
    JacobianCell_HDG_PTC<SurrealClass,TopoD1>::integrate( fcnPTC_, ufld_, qfld_, quadratureOrderMin, 1, jacPDE_u );
    CFL *= 1.2;
  }
  virtual void jacobianTranspose(const SparseVector& X, SparseMatrix& mtx) {}
  virtual void jacobianTranspose(const SparseVector& X, NonZeroPattern& nz) {}

  //Limits the updated vector dX
//  // No limit on dX
//  virtual void lineSearch(const SparseVector& X, SparseVector& dX )
//  {
//  }
  // Golden search
  virtual void lineSearch(const SparseVector& X, SparseVector& dX )
  {
    Real r = (sqrt(5) - 1)/ 2; // Golden search ratio
    Real tol = 1e-7;//1e-15;

    Real a = 0.0, b = 2.0; // HACK: need to actually calculate where the 'other-side' is

    // Interior test points
    Real c = b - r*(b - a);
    Real d = a + r*(b - a);
    SparseVector Rc(dX.size());
    SparseVector Rd(dX.size());
    SparseVector Xc(dX.size());
    SparseVector Xd(dX.size());
    Real fc = 0;
    Real fd = 0;

    // Search
    while (abs(b - a) > tol)
    {
      Rc = 0;
      fc = 0;
      Xc = X - c*dX;
      residual(Xc , Rc);
      normResidual(Rc, fc);
      Rd = 0;
      fd = 0;
      Xd = X - d*dX;
      residual(Xd , Rd);
      normResidual(Rd, fd);

      // update the test point locations
      if (fc < fd)
      {
        b = d;
        d = c;
        c = b - r*(b - a);
      }
      else
      {
        a = c;
        c = d;
        d = a + r*(b - a);
      }
    }
//    cout << " " << (a + b) / 2 << endl;
    Xd = (a + b) / 2 * dX;
    dX = Xd;
    return;
  }

//  // Halving line search
//  virtual void lineSearch(const SparseVector& X, SparseVector& dX )
//  {
//    Real tol = 9.5e-7;
//
//    Real c = 0.0, d = 1.0; // HACK: need to actually calculate where the 'other-side' is
//
//    SparseVector Rc(dX.size());
//    SparseVector Rd(dX.size());
//    SparseVector Xc(dX.size());
//    SparseVector Xd(dX.size());
//    Real fc = 0;
//    Real fd = 0;
//
//    // What value are we trying to decrease from?
//    Rc = 0;
//    fc = 0;
//    Xc = X - c*dX;
//    residual(Xc , Rc);
//    normResidual(Rc, fc);
//
//    // What is our value from pure Newton?
//    Rd = 0;
//    fd = 0;
//    Xd = X - d*dX;
//    residual(Xd , Rd);
//    normResidual(Rd, fd);
//
//    // Search
//    while ((fc < fd) && (d > tol))
//    {
//      // Half our step size
//      d = d / 2;
//      Rd = 0;
//      fd = 0;
//      Xd = X - d*dX;
//      residual(Xd , Rd);
//      normResidual(Rd, fd);
//    }
////    cout << " " << d << endl;
//    Xd = d*dX;
//    dX = Xd;
//    return;
//  }

  //Used to compute norms of the residual and the update vector
  virtual void normResidual(const SparseVector& rsd, Real& nrmRsd) const
  {
    //Simple L2 norm
    nrmRsd = norm(rsd,2);
  }
  virtual void normUpdate(const SparseVector& dX, Real& nrmUpd) const
  {
    //Simple L2 norm
    nrmUpd = norm(dX,2);
  }


  VectorSizeClass vectorSize() const
  {
    // Create the size that represents the size of a linear algebra vector
    const int nDOFPDE = ufld_.nDOF();
    const int nDOFAu  = qfld_.nDOF();
    const int nDOFInt = uIfld_.nDOF();
    const int nDOFBC  = lgfld_.nDOF();

    return {nDOFPDE,nDOFAu,nDOFInt,nDOFBC};
  }

  MatrixSizeClass matrixSize() const
  {
    // Create the size that represents the size of a sparse linear algebra matrix
    const int nDOFPDE = ufld_.nDOF();
    const int nDOFAu  = qfld_.nDOF();
    const int nDOFInt = uIfld_.nDOF();
    const int nDOFBC  = lgfld_.nDOF();

    return {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFAu}, {nDOFPDE, nDOFInt}, {nDOFPDE, nDOFBC} },
            { {nDOFAu , nDOFPDE}, {nDOFAu , nDOFAu}, {nDOFAu , nDOFInt}, {nDOFAu , nDOFBC} },
            { {nDOFInt, nDOFPDE}, {nDOFInt, nDOFAu}, {nDOFInt, nDOFInt}, {nDOFInt, nDOFBC} },
            { {nDOFBC , nDOFPDE}, {nDOFBC , nDOFAu}, {nDOFBC , nDOFInt}, {nDOFBC , nDOFBC} }};
  }

private:
  XField1D& xfld_;
  int order_;
  // integrands
  IntegrandCellClass fcnCell_;
  IntegrandTraceClass fcnInt_;
  IntegrandBoundClass fcnBC_;
  IntegrandSquareErrorClass fcnErr_;
  IntegrandCellPTCClass fcnPTC_;
  // HDG solution field
  // cell solution
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufld_;
  // auxiliary variable
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfld_;
  // interface solution
  Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> uIfld_;
  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld_;
  // Degrees of freedom
  int nDOFPDE;
  int nDOFAu;
  int nDOFInt;
  int nDOFBC;
  Real CFL;
};

}

//############################################################################//

//----------------------------------------------------------------------------//
// solution: Hierarchical P1 thru P3
// Lagrange multiplier: continuous Hierarchical P1 thru P3
// exact solution: -tanh
BOOST_AUTO_TEST_CASE( Solve_HDG_AD_Burgers1D_PTC )
{

  typedef PDEAdvectionDiffusion<PhysD1,
                                LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::template MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  Real nu = 0.05;
  Real b =  0.2;

  typedef ScalarFunction1D_Tanh SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCAdvectionDiffusion<PhysD1,BCType> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  SolutionExact soln(nu);
  NDSolutionExact solnExact(nu);

  // PDE
  LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers> adv;
  ViscousFlux1D_Uniform visc(nu);
  SolutionExact MMS_soln(nu);
//  ForcingFunction1D_MMS<SolutionExact> forcing(MMS_soln);
//  ForcingFunction1D_Tanh forcing(b, nu);
  Source1D_Uniform source(b);

  typedef ForcingFunction1D_MMS<PDEAdvectionDiffusion1D, SolutionExact> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(MMS_soln) );

  PDEClass pde( adv, visc, source, forcingptr );

  // BC
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(nu) );
  BCClass bc( solnExactPtr, visc );

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, BCClass, BCClass::Category, HDG> IntegrandBoundClass;
  typedef IntegrandCell_SquareError<PDEClass,NDSolutionExact> IntegrandSquareErrorClass;

  const std::vector<int> BoundaryGroups = {0,1};

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Local, AugGradient );

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnInt( pde, disc, {0} );
  IntegrandBoundClass fcnBC( pde, bc, BoundaryGroups, disc );
  IntegrandSquareErrorClass fcnErr( solnExact, {0} );

  // norm data
//  Real hVec[10];
//  Real hDOFVec[10];   // 1/sqrt(DOF)
//  Real normVec[10];   // L2 error
//  int indx = 0;

  // convergence flag
  bool converged = false;

  // Tecplot output
//#ifdef SANS_FULLTEST
//  std::ofstream resultFile("tmp/L2_1D_HDG_Burger.plt", std::ios::out);
//  pyrite_file_stream pyriteFile("IO/Solve/Solve1D_HDG_Burgers_FullTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
//#else
//  std::stringstream resultFile;
//  pyrite_file_stream pyriteFile("IO/Solve/Solve1D_HDG_Burgers_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
//#endif
//  resultFile << "VARIABLES=";
//  resultFile << "\"h\"";
//  resultFile << ", \"1/sqrt(DOF)\"";
//  resultFile << ", \"L2 error\"";
//  resultFile << ", \"L2 error rate\"";
//  resultFile << std::endl;

//  resultFile << std::setprecision(16) << std::scientific;
//  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 0;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 7;
#else
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {

    // loop over grid resolution: 2^power
    int ii;
    int powermin = 3;
#ifdef SANS_FULLTEST
    int powermax = 7;
#else
    int powermax = 3;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power ) + 1;
      // grid:
      XField1D xfld( ii, -1, 1 );

      // HDG solution field
      // cell solution
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufld(xfld, order, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> uIfld(xfld, order, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroups);


      const int nDOFPDE = ufld.nDOF();
      const int nDOFAu  = qfld.nDOF();
      const int nDOFInt = uIfld.nDOF();
      const int nDOFBC  = lgfld.nDOF();

      // quadrature rule
      int quadratureOrder[2] = {-1, -1};    // max
      int quadratureOrderTrace[2] = {0, 0};     // min

      // linear system setup
      typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
      typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
      typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
      typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;

      SystemVectorClass rsd = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};

      // jacobian nonzero pattern
      typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

      DLA::MatrixD<NonZeroPatternClass> nz =
          {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFAu}, {nDOFPDE, nDOFInt}, {nDOFPDE, nDOFBC} },
           { {nDOFAu , nDOFPDE}, {nDOFAu , nDOFAu}, {nDOFAu , nDOFInt}, {nDOFAu , nDOFBC} },
           { {nDOFInt, nDOFPDE}, {nDOFInt, nDOFAu}, {nDOFInt, nDOFInt}, {nDOFInt, nDOFBC} },
           { {nDOFBC , nDOFPDE}, {nDOFBC , nDOFAu}, {nDOFBC , nDOFInt}, {nDOFBC , nDOFBC} }};

#if 0
      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

      // generate equation set object
      BurgersHDGAlgebraicEquation Equation(xfld, order, pde, disc, bc, BoundaryGroups, solnExact);

      // generate non-linear solver
      PyDict NewtonSolverDict, LinearSolver, UMFPACK;
      UMFPACK[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
      LinearSolver[SLA::LinearSolverParam::params.LinearSolver] = UMFPACK;
      NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinearSolver;

      NewtonSolver<SystemMatrixClass, DLA::MatrixD<NonZeroPatternClass>> Solver( Equation, NewtonSolverDict, nz );
//      NonLinearConjugateGradientSolver<SystemMatrixClass, DLA::MatrixD<NonZeroPatternClass> > Solver( Equation, NewtonSolverDict, nz );

      // Initial condition
      SystemVectorClass sln = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};
      SystemVectorClass q0  = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};
      q0  = 0;

      // Solve
      sln = q0;
      converged = Solver.solve(q0, sln).converged;
      BOOST_CHECK( converged );

      // updated solution
      for (int k = 0; k < nDOFPDE; k++)
        ufld.DOF(k) = sln[0][k];

      for (int k = 0; k < nDOFPDE; k++)
        for (int d = 0; d < PhysD1::D; d++)
          qfld.DOF(k)[d] = sln[1][PhysD1::D*k+d];

      for (int k = 0; k < nDOFInt; k++)
        uIfld.DOF(k) = sln[2][k];

      for (int k = 0; k < nDOFBC; k++)
        lgfld.DOF(k) = sln[3][k];

      // check that the residual is zero
      rsd = 0;

      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsd(0), rsd(1)),
                                              xfld, (ufld, qfld), quadratureOrder, 1 );
      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(0), rsd(1), rsd(2)),
                                                              xfld, (ufld, qfld), uIfld, quadratureOrderTrace, 1);
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsd(0), rsd(1), rsd(3)),
                                                          xfld, (ufld, qfld), lgfld, quadratureOrderTrace, 2 );

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

      Real rsdAunrm = 0;
      for (int n = 0; n < nDOFAu; n++)
        rsdAunrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdAunrm), 1e-12 );

      Real rsdIntnrm = 0;
      for (int n = 0; n < nDOFInt; n++)
        rsdIntnrm += pow(rsd[2][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdIntnrm), 1e-12 );

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[3][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

      // L2 solution error
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell( fcnErr, SquareError ), xfld, ufld, quadratureOrder, 1 );
      Real norm = SquareError;

//      hVec[indx] = 1./ii;
//      hDOFVec[indx] = 1./sqrt(nDOFPDE);
//      normVec[indx] = sqrt( norm );
//      indx++;

      if (converged)
      {
        cout << "Converged   ";
      }
      else
      {
        cout << "Unconverged ";
      }
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
//      if (indx > 1)
//        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;

#if 0
      // Tecplot dump
      string filename = "tmp/slnHGD_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += ".plt";
//      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( ufld, filename );
#endif

#if 0
      // GNUPLOT dump
      string filename2 = "tmp/slnHGD_P";
      filename2 += to_string(order);
      filename2 += "_";
      filename2 += to_string(ii);
      filename2 += ".dat";
      std::fstream flout( filename2, std::fstream::out );
      for (int n = 0; n < nDOFInt; n++)
      {
        flout << uIfld.DOF(n) << std::endl;
      }
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
#endif

    }

#if 0
    // Tecplot output
    resultFile << "ZONE T=\"HDG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }
#endif

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
