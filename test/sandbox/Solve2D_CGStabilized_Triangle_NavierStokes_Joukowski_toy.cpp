// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGBR2_Triangle_NavierStokes_FlatPlate_toy
// testing of 2-D DGBR2 for NS for flat plate

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include <iostream>
#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/PDENavierStokes_Brenner2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Adaptation/MOESS/SolverInterface_AGLS.h"
#include "Adaptation/MOESS/MOESSParams.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_CGStabilized_Triangle_NavierStokes_Joukowski_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_CGStabilized_Triangle_Joukowski )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;

//#define BRENNER
#ifndef BRENNER
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
#else
  typedef PDENavierStokes_Brenner2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
#endif
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

#ifndef BRENNER
  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;
#else
  typedef BCNavierStokesBrenner2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;
#endif

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                   AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

//  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef SolverInterface_AGLS<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  std::vector<Real> tol = {1e-9, 1e-9};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.5;
  const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real pRef = 1.0;                            // pressure
  const Real tRef = pRef/(rhoRef*R);

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = cRef*Mach;                              // velocity scale

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

#ifndef BRENNER
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Entropy );
#else
  Real rhodiff = 4./3.;
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, rhodiff );
#endif

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

#if 1
  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict BCOut(BCIn);
#else

  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aRef = atan(uRef/vRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aRef;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
#endif

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0};
  BCBoundaryGroups["BCIn"] = {1};
  BCBoundaryGroups["BCOut"] = {2};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde, 1.0);
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, stab);
#else
  // Drag output
  NDOutputClass outputFcn(pde, 1., 0.);
  OutputIntegrandClass outputIntegrand( outputFcn, {0} );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 200;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-12;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 500;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-12;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  PyDict SolverContinuationDict0, NonlinearSolverDict0;

  // Easier PTC for subsequent solves
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);
  //
  int powermin = 0;
  int powermax = 4;
  //
  int ordermin = 1;
  int ordermax = 3;

  StabilizationType stabType = StabilizationType::AGLSPrimal;
  TauType tauType = TauType::Glasby;
  Real cb = 2.0;

  enum ResidualNormType ResNormType = ResidualNorm_L2;

  string filename_base = "tmp/AGLS2_Joukowski_NS";

  string filename2 = filename_base + "_Errors.txt";
  fstream foutsol;


  std::shared_ptr<SolutionClass> pGlobalSol;
  std::shared_ptr<SolutionClass> pGlobalSolOld;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfldOld;

  for (int power = powermin; power <= powermax; power++)
  {

    //      int npow = pow(2, power);
    //      // Read PX Output grid
    string filein = "grids/JOUKOWSKI/Joukowski_Laminar_Challenge_tri_ref";
    filein += to_string(power);
    filein += "_Q4.grm";

    pxfld = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, filein);

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    // loop over grid resolution: 2^power
    //    int power = 0;
    for (int order = ordermin; order <= ordermax; order++)
    {


      if (world.rank() == 0)
      {
        foutsol.open( filename2, fstream::app );
      }

      StabilizationMatrix stab(stabType, tauType, order, cb, order );

      //Solution data
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                   active_boundaries, stab);

      const int quadOrder = 2*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      //Set initial solution
      if (order == ordermin && power == powermin)
        pGlobalSol->setSolution(q0);
      else
        pGlobalSol->setSolution(*pGlobalSolOld);

      //solve
      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      Real output = pInterface->getOutput();

      Real Cd = output / (0.5*rhoRef*qRef*qRef);

      int nDOF = pGlobalSol->primal.qfld.nDOFnative();

      if (world.rank() == 0)
      {
        cout << "P = " << order << " power = " << power <<
            ": Cd = " << std::setprecision(16) << Cd;
        cout << endl;      foutsol << order << " " << power << " " << nDOF << " " << std::setprecision(16) << Cd;
        foutsol << endl;
        foutsol.close();
      }

      pGlobalSolOld = pGlobalSol;
      pxfldOld = pxfld;


#if 1
      // Tecplot dump grid
      string filename = filename_base + "_P";
      filename += to_string(order);
      filename += "_X";
      filename += to_string(power);
      filename += ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, filename );
      std::cout << "Wrote Primal\n";


      // Tecplot dump grid
      string filename2 = filename_base + "_P";
      filename2 += to_string(order);
      filename2 += "_X";
      filename2 += to_string(power);
      filename2 += "_adjoint.plt";
      output_Tecplot( pInterface->getAdjField(), filename2 );
      std::cout << "Wrote Adjoint\n";
#endif

      //       Tecplot dump grid
#if 0
      string filename = "tmp/PXP1_4k_epic.plt";
      output_Tecplot( qfld, filename );
      std::cout << "Wrote Tecplot File\n";
#endif

    }

  }

  //  time_t end;
  //  time(&end);
  //  Real seconds = difftime(end,start);

  //  std::cout << " ELAPSED TIME: " << seconds << " seconds"<< std::endl;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
