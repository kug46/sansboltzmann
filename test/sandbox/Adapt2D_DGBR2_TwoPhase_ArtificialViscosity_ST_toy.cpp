// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_TwoPhase_ArtificialViscosity_ST_btest
// Testing of the MOESS framework on a space-time two-phase problem

#define USE_PETSC_SOLVER

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity1D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity1D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/OutputTwoPhase.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_Lagrange_X1.h"

#include "Field/output_Tecplot_PDE.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_TwoPhase_ArtificialViscosity_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_TwoPhase_ArtificialViscosity_ST_Triangle )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef QTypePrimitive_pnSw QType;
  typedef Q1D<QType, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, Real,
                              CapillaryModel> TraitsModelTwoPhaseClass;

  typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity,
                                            TraitsModelTwoPhaseClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTwoPhaseArtificialViscosity1DVector<TraitsSizeTwoPhaseArtificialViscosity,
                                                TraitsModelAV> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputTwoPhase_SaturationPower<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_GenH_CG ParamBuilderType;
  typedef GenHField_CG<PhysD2, TopoD2> GenHFieldType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  timer clock;
  mpi::communicator world;

  // Grid
#if 1
    int ii = 15;
    int jj = 15;
//    std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, jj, 0, 100, 0, 100, true );
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj, 0, 100, 0, 100 );
//    std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Quad_X1>(ii, jj, 0, 100, 0, 100);
#else
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField_libMeshb<PhysD2, TopoD2>>( world, "tmp/mesh_a1.mesh" );
//  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField_libMeshb<PhysD2, TopoD2>>( world, "tmp/fefloa_out_a16.mesh" );
#endif

  const int iBottom = XField2D_Box_Triangle_Lagrange_X1::iBottom;
  const int iRight = XField2D_Box_Triangle_Lagrange_X1::iRight;
  const int iTop = XField2D_Box_Triangle_Lagrange_X1::iTop;
  const int iLeft = XField2D_Box_Triangle_Lagrange_X1::iLeft;

  int order = 2;

  int maxIter = 20;
  Real targetCost = 10000;
  std::string filename_base = "tmp/";

#if 0 //Eigenvector dump
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_tmp(*pxfld, order, BasisFunctionCategory_Hierarchical);

  std::fstream fqfld("tmp/Vmin.txt", std::fstream::in);
  for (int i = 0; i < qfld_tmp.nDOF(); i++)
    for (int j = 0; j < ArrayQ::M; j++)
      fqfld >> qfld_tmp.DOF(i)[j];
  fqfld.close();
  output_Tecplot( qfld_tmp, "tmp/Vmin.plt" );
  return;
#endif

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(0.0);
  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;

  bool hasSpaceTimeDiffusionTwoPhase = false;
  PDEBaseClass pdeTwoPhaseAV(order, hasSpaceTimeDiffusionTwoPhase, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  SensorAdvectiveFlux sensor_adv(0.0);
  bool hasSpaceTimeDiffusionAV = true;
  const Real diffusionConstantAV = 3.0;
  SensorViscousFlux sensor_visc(order, hasSpaceTimeDiffusionAV, 1.0, diffusionConstantAV);
  const Real Sk_min = -3;
  const Real Sk_max = -1;
  SensorSource sensor_source(pdeTwoPhaseAV, Sk_min, Sk_max);

  // AV PDE with sensor equation
  bool isSteady = true;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady,
                 order, hasSpaceTimeDiffusionTwoPhase, rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  Real pnL = 2500.0;
  Real SwL = 1.0;

  Real pnR = 2470.0;
  Real SwR = 0.1;

  // Initial solution
  Real pwInit = 0.5*(pnL + pnR);
  Real SwInit = SwR;
  Real nuInit = 1.0;

  ArrayQ qInit;
  AVVariable<PressureNonWet_SaturationWet,Real> qdata({pwInit, SwInit}, nuInit);
  pde.setDOFFrom( qInit, qdata );

  typedef BCmitAVSensor1DParams<BCTypeFlux_mitStateSpaceTime, BCTwoPhase1DParams<BCTypeFullStateSpaceTime>> BCParamsFullState;

  // BCs
  PyDict LeftState;
  LeftState[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  LeftState[PressureNonWet_SaturationWet_Params::params.pn] = pnL;
  LeftState[PressureNonWet_SaturationWet_Params::params.Sw] = SwL;

  PyDict RightState;
  RightState[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  RightState[PressureNonWet_SaturationWet_Params::params.pn] = pnR;
  RightState[PressureNonWet_SaturationWet_Params::params.Sw] = SwR;

  PyDict InitState;
  InitState[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  InitState[PressureNonWet_SaturationWet_Params::params.pn] = pwInit;
  InitState[PressureNonWet_SaturationWet_Params::params.Sw] = SwInit;

  PyDict BCLeft;
  BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.FullStateSpaceTime;
  BCLeft[BCParamsFullState::params.StateVector] = LeftState;
  BCLeft[BCParamsFullState::params.Cdiff] = diffusionConstantAV;

  PyDict BCRight;
  BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.FullStateSpaceTime;
  BCRight[BCParamsFullState::params.StateVector] = RightState;
  BCRight[BCParamsFullState::params.Cdiff] = diffusionConstantAV;
  BCRight[BCParamsFullState::params.isOutflow] = true;

#if 0
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitStateSpaceTime, BCTwoPhase1DParams<BCTypeTimeIC>> BCParamsTimeIC;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  TimeIC[BCParamsTimeIC::params.StateVector] = InitState;
  TimeIC[BCParamsTimeIC::params.Cdiff] = diffusionConstantAV;
#else
  typedef SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, TraitsSizeTwoPhaseArtificialViscosity> SolutionType;
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitStateSpaceTime, BCTwoPhase1DParams<BCTypeTimeIC_Function>> BCParamsTimeIC_Function;

  PyDict SolutionInit;
  SolutionInit[BCParamsTimeIC_Function::params.Function.Name] = BCParamsTimeIC_Function::params.Function.ThreeConstSaturation;
  SolutionInit[SolutionType::ParamsType::params.pn] = 0.5*(pnL + pnR);
  SolutionInit[SolutionType::ParamsType::params.Sw_left] = SwL;
  SolutionInit[SolutionType::ParamsType::params.Sw_mid] = SwR;
  SolutionInit[SolutionType::ParamsType::params.Sw_right] = SwR;
  SolutionInit[SolutionType::ParamsType::params.xLeftMid] = 30.0;
  SolutionInit[SolutionType::ParamsType::params.xMidRight] = 80.0;
  SolutionInit[SolutionType::ParamsType::params.Ltransit] = 0*10.0;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC_Function;
  TimeIC[BCParamsTimeIC_Function::params.Function] = SolutionInit;
  TimeIC[BCParamsTimeIC_Function::params.Cdiff] = diffusionConstantAV;
#endif

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCLeft"] = BCLeft;
  PyBCList["BCRight"] = BCRight;
  PyBCList["TimeIC"] = TimeIC;
  PyBCList["BCNone"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TimeIC"] = {iBottom};
  BCBoundaryGroups["BCRight"] = {iRight};
  BCBoundaryGroups["BCNone"] = {iTop};
  BCBoundaryGroups["BCLeft"] = {iLeft};
//  BCBoundaryGroups["BCNone"] = {iBottom, iRight, iTop, iLeft};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {5e-7, 5e-7};

  //Output functional
  NDOutputClass fcnOutput(pde, 0, 2);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, AdjLinearSolverDict;
  PyDict NonlinearSolverDict, NewtonSolverDict, LineUpdateDict;

#if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
    PyDict PreconditionerDict;
    PyDict PreconditionerILU;
    PyDict PETScDict;

    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
    PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 1;
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
    PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)/2; //elemDOF for p=order

    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

    PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
    PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
    PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
    PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
    PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
    PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
    PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
    PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
    PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
    PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

    //Change parameters for adjoint solve
    PyDict PreconditionerILU_adjoint = PreconditionerILU;
    PyDict PreconditionerDict_adjoint = PreconditionerDict;
    PyDict PETScDict_adjoint = PETScDict;
    PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)/2; //elemDOF for p=order+1
    PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
    PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
    PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
    PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
    AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

    if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
  #else
    PyDict UMFPACKDict;
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
    AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
    if (world.rank() == 0) std::cout << "Linear solver: UMFPACK" << std::endl;
  #endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.0;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
//  NewtonSolverDict[NewtonSolverParam::params.ResidualHistoryFile] = "tmp/residual_history.txt";

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);


  //--------ADAPTATION LOOP--------

  fstream fadapthist;
  if (world.rank() == 0)
  {
    std::string adapthist_filename = filename_base + "test.adapthist";
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = true;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Compute generalized log H-tensor field
  std::shared_ptr<GenHFieldType> pHfld = std::make_shared<GenHFieldType>(*pxfld);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>((*pHfld, *pxfld), pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

  const int quadOrder = 2*(order + 2);

  //Set initial solution
  pGlobalSol->setSolution(qInit);

  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, {"pn", "Sw", "nu"}  );

#if 0
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  QuadratureOrder quadrule(*pxfld, quadOrder);

  PrimalEquationSetClass AlgEqSet(pGlobalSol->paramfld, pGlobalSol->primal, pGlobalSol->pliftedQuantityfld, pde, quadrule,
                                  ResNormType, tol, cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups);

  // residual
  SystemVectorClass q(AlgEqSet.vectorStateSize());
  AlgEqSet.fillSystemVector(q);

  SystemVectorClass rsd(AlgEqSet.vectorEqSize());
  rsd = 0;
  AlgEqSet.residual(q, rsd);

//  std::fstream frsd("tmp/rsd_C1.txt", std::fstream::in);
//  frsd << std::scientific << std::setprecision(12);
//  for (int i = 0; i < rsd[0].m(); i++)
//    for (int j = 0; j < ArrayQ::M; j++)
//      frsd >> rsd[0][i][j];
//  frsd.close();

//  std::cout << rsd[0].m() << std::endl;
//
//  for (int i = 0; i < rsd[0].m(); i++)
//    for (int j = 0; j < 2; j++)
//      rsd[0][i][j] = 0.0;

  std::cout << std::setprecision(5) << std::scientific << AlgEqSet.residualNorm(rsd) << std::endl;

//  std::fstream frsd("tmp/rsd_tmp.txt", std::fstream::out);
//  frsd << std::scientific << std::setprecision(12);
//  for (int i = 0; i < rsd[0].m(); i++)
//    for (int j = 0; j < ArrayQ::M; j++)
//      frsd << rsd[0][i][j] << std::endl;
//  frsd.close();
//  return;

#if 1
  // jacobian nonzero pattern
  SystemNonZeroPattern nz(AlgEqSet.matrixSize());
  AlgEqSet.jacobian(q, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  jac = 0;
  AlgEqSet.jacobian(q, jac);

  fstream fout( "tmp/jac.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );

  SystemVectorClass dq(AlgEqSet.vectorStateSize());

#if 1
  // solve
  SLA::UMFPACK<SystemMatrixClass> solver(AlgEqSet);
  solver.solve(rsd, dq);

  for (int i = 0; i < dq[0].m(); i++)
    for (int j = 0; j < 2; j++)
      dq[0][i][j] = 0.0;

  {
    std::fstream fqfld("tmp/dq_tmp.txt", std::fstream::out);
    fqfld << std::scientific << std::setprecision(12);
    for (int i = 0; i < dq[0].m(); i++)
        fqfld << dq[0][i][2] << std::endl;
    fqfld.close();
  }
#else
  {
//    dq = 0;
//    std::fstream fqfld("tmp/dq_matlab.txt", std::fstream::in);
//    for (int i = 0; i < dq[0].m(); i++)
//        fqfld >> dq[0][i][2];
//    fqfld.close();
  }
#endif

  // update solution
  q -= dq;
  AlgEqSet.setSolutionField(q);

  // check that the residual is zero
  rsd = 0;
  AlgEqSet.residual(q, rsd);

  bool converged = AlgEqSet.convergedResidual(rsd);
  if (!converged) AlgEqSet.printDecreaseResidualFailure( AlgEqSet.residualNorm(rsd) );
#endif

//  output_Tecplot( pGlobalSol->primal.qfld, "tmp/qfld_mod.plt", {"pn", "Sw", "nu"}  );
//  output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld, "tmp/qfld_mod.plt", {"pn", "Sw", "nu"});

//  {
//    std::fstream fqfld("tmp/qfldDOF_tmp.txt", std::fstream::out);
//    fqfld << std::scientific << std::setprecision(12);
//    for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
//      for (int j = 0; j < ArrayQ::M; j++)
//        fqfld << pGlobalSol->primal.qfld.DOF(i)[j] << std::endl;
//    fqfld.close();
//  }
  return;
#endif


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    if (world.rank() == 0)
      std::cout << "\n" << std::string(25,'-') + "Adaptation Iteration " << iter << std::string(25,'-') << "\n\n";

    //Create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, AdjLinearSolverDict,
                                                        outputIntegrand);
    pInterface->setBaseFilePath(filename_base);

    pInterface->solveGlobalPrimalProblem();
    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename, {"pn", "Sw", "nu"} );
//    std::string sol_filename = filename_base + "sol_a" + std::to_string(iter) + ".plt";
//    output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld, sol_filename, {"pn", "Sw", "nu"});

    std::string liftedfld_filename = filename_base + "liftedfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( *(pGlobalSol->pliftedQuantityfld), liftedfld_filename, {"s"} );

#if 1 //Dump solution to file
    {
      std::fstream fqfld("tmp/qfld_a" + std::to_string(iter) + ".txt", std::fstream::out);
      fqfld << std::scientific << std::setprecision(15);
      for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
        for (int j = 0; j < ArrayQ::M; j++)
          fqfld << pGlobalSol->primal.qfld.DOF(i)[j] << std::endl;
      fqfld.close();
    }
#endif

    if (world.rank() == 0)
      std::cout << "Output: " << std::scientific << std::setprecision(12) << pInterface->getOutput() << std::endl;

    pInterface->solveGlobalAdjointProblem();
    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename, {"psiw", "psin", "psinu"} );

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Compute generalized log H-tensor field on new mesh
    pHfld = std::make_shared<GenHFieldType>(*pxfldNew);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>((*pHfld,*pxfldNew), pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    pGlobalSolNew->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;

    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, {"pn", "Sw", "nu"}  );

    if (world.rank() == 0)
      std::cout<<"Time elapsed: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl << std::endl;
  }

  if (world.rank() == 0)
    fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
