// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGBR2_Triangle_NavierStokes_FlatPlate_toy
// testing of 2-D DGBR2 for NS for flat plate

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/SolutionFunction_Euler2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_Dispatch_DGBR2.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_FlatPlate_X1.h"
#include "Meshing/EPIC/XField_PX.h"
#include "Meshing/gmsh/XField_gmsh.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Triangle_NavierStokes_NACADuct_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGBR2_Triangle_FlatPlate )
{

  typedef QTypePrimitiveRhoPressure QType;
  //typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  //typedef SurrealS<ArrayQ::M> SurrealClass;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  std::vector<Real> tol = {1e-9, 1e-9};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.1;
  const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real tRef = 1.0;          // temperature
  const Real pRef = rhoRef*R*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                            // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  //const Real cRef = qRef/Mach;                      // speed of sound
  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  const Real TtRef = tRef*Gamma;
  const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // DRAG INTEGRAND
  typedef BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  NDBCClass bc(pde);
  IntegrandBCClass fcnBC( pde, bc, {0}, disc );

  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> IntegrandOutputClass;

  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass outputIntegrand( outputFcn, {0} );

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;


  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;

  PyDict BCIn;
#if 1
  typedef SolutionFunction_Euler2D_Wake<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass2;

  PyDict Function;
  Function[BCClass2::ParamsType::params.Function.Name] = BCClass2::ParamsType::params.Function.Wake;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.Pt] = PtRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.Tt] = TtRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = pRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.wakedepth] = 0.3;

  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_mitState;
  BCIn[BCClass2::ParamsType::params.Function] = Function;

#else
  typedef SolutionFunction_Euler2D_Wake<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass2;

  PyDict Function;
  Function[BCClass2::ParamsType::params.Function.Name] = BCClass2::ParamsType::params.Function.Wake;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = rhoRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = uRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = pRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.wakedepth] = 0.6;

  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
  BCIn[BCClass2::ParamsType::params.Function] = Function;
#endif

#if 0
  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;
#else
  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
#endif


  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"]   = {3};
  BCBoundaryGroups["BCNoSlip"] = {0};
  BCBoundaryGroups["BCOut"]    = {2};
  BCBoundaryGroups["BCIn"]     = {1};

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  //typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, AdjSolverDict, NewtonSolverDict, LinSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;


  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  LinSolverDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-8;
  LinSolverDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  LinSolverDict[SLA::PETScSolverParam::params.MaxIterations] = 2500;
  LinSolverDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  LinSolverDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  LinSolverDict[SLA::PETScSolverParam::params.Verbose] = true;
  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  LinSolverDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "tmp/PETSc_residualhist.dat";
  //LinSolverDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;
  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  std::cout << "Linear solver: MKL" << std::endl;
#else
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  AdjSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;


  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);

//   norm data
//  Real normVec[10];   // L2 error
//  int indx = 0;

//  fstream fout( "tmp/DGBR2_V2_Ec0p3Re1.txt", fstream::out );
//
  int powermin = 3;
  int powermax = 3;
//
  int ordermin = 1;
  int ordermax = 1;

  for (int order = ordermin; order <= ordermax; order++)
  {

  // loop over grid resolution: 2^power
//    int power = 0;
    for (int power = powermin; power <= powermax; power++)
    {

//        string filein = "tmp/flatplate_P1_4k/mesh_a30.mesh";
      string filein = "/home2/NACADUCT20180307/nacanew.msh";
//      string filein = "grids/PXP1_4k_epic.grm";
      XField_gmsh<PhysD2, TopoD2> xfld(world, filein);

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      mufld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

//      const int nDOFPDE = qfld.nDOF();
//      const int nDOFLIFT = rfld.nDOF();

      // Set the uniform initial condition
      ArrayQ q0;
      pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

//      std::cout << rho0 << " " << u0 << " " << v0 << " " << p0 << "\n";

      qfld = q0;
      for (int i=0; i<rfld.nDOF(); i++) rfld.DOF(i) = 0.0;
      lgfld = 0;

      QuadratureOrder quadratureOrder( xfld, 3*(order+1) );

      //////////////////////////
      //SOLVE SYSTEM
      //////////////////////////

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0;

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      Solver.solve(ini,sln);

      PrimalEqSet.setSolutionField(sln);

      // Monitor Drag Error
      Real Drag = 0.0;
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          FunctionalBoundaryTrace_DGBR2( outputIntegrand, fcnBC, Drag ),
          xfld, (qfld, rfld), quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size() );

#ifdef SANS_MPI
      Drag = boost::mpi::all_reduce(world, Drag, std::plus<Real>());
#endif

      // drag coefficient
      Real Cd = Drag / (0.5*rhoRef*qRef*qRef*lRef);

#if 1
      SystemVectorClass rhs(PrimalEqSet.vectorEqSize());
      rhs = 0;

      typedef SurrealS<NDPDEClass::N> SurrealClass;
      PrimalEqSet.dispatchBC().dispatch_DGBR2(
          JacobianFunctionalBoundaryTrace_Dispatch_DGBR2<SurrealClass>(outputIntegrand, xfld, qfld, rfld, sfld,
                                                                       quadratureOrder.boundaryTraceOrders.data(),
                                                                       quadratureOrder.boundaryTraceOrders.size(),
                                                                       rhs(PrimalEqSet.iPDE) ) );

      // adjoint solve
      SLA::LinearSolver<SystemMatrixClass> solverAdj(AdjSolverDict, PrimalEqSet, SLA::TransposeSolve);

      SystemVectorClass adj(PrimalEqSet.vectorStateSize());
      solverAdj.solve(rhs, adj);

      PrimalEqSet.setAdjointField(adj, wfld, sfld, mufld);
#endif

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      cout << "P = " << order << " power = " << power <<
          ": Cd = " << std::setprecision(16) << Cd;
      cout << endl;
#endif

#if 0
      fstream fout( "tmp/flatplate_dgbr2_truth.txt", fstream::out );
      fout << "P = " << order << " power = " << power <<
          ": Cd = " << std::setprecision(16) << Cd;
      fout << endl;
#endif


#if 1
      // Tecplot dump grid
      string filename = "tmp/DGBR2_NACANEW_FBKI";
      filename += "_rank" + to_string(world.rank());
      filename += "_P" + to_string(order);
      filename += "_X" + to_string(power);
      filename += ".plt";
      output_Tecplot( qfld, filename );
      std::cout << "Wrote Tecplot File\n";
#endif

#if 1
      filename = "tmp/DGBR2_NACANEW_FBKI";
      filename += "_rank" + to_string(world.rank());
      filename += "_P" + to_string(order);
      filename += "_X" + to_string(power);
      filename += "_adjoint.plt";
      output_Tecplot( wfld, filename );
      std::cout << "Wrote Tecplot File\n";
#endif

//       Tecplot dump grid
#if 0
      string filename = "tmp/PXP1_4k_epic.plt";
      output_Tecplot( qfld, filename );
      std::cout << "Wrote Tecplot File\n";
#endif

    }

  }


//  time_t end;
//  time(&end);
//  Real seconds = difftime(end,start);

//  std::cout << " ELAPSED TIME: " << seconds << " seconds"<< std::endl;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
