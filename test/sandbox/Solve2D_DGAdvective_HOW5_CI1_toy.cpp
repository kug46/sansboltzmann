// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_DGAdvective_HOW5_CI1_toy
// Trying to build the HOW5 CI1 case


#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <boost/mpl/vector_c.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/JacobianParam.h"
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_Block2x2_PTC.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Field/HField/GenHFieldArea_CG.h"
#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "Meshing/EPIC/XField_PX.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "tools/output_std_vector.h"
#include "tools/linspace.h"

using namespace std;
using namespace SANS;

namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_HOW5_CI1_toy )

////////////////////////////////////////////////////////////////////////////////////////
// Notes:
//  -Built of RAE2282 case
// Default PETSc abstol is 1e-50, 1e-10 is much too big
////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_HOW5_CI1_toy )
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // MPI comm world
  ////////////////////////////////////////////////////////////////////////////////////////
  mpi::communicator world;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for Euler2D sans AV
  ////////////////////////////////////////////////////////////////////////////////////////
//  typedef QTypeEntropy QType;
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for discretization sans AV
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;
  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PDEPrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD2, TopoD2> > AlgebraicEquationSet_PTCClass;

  typedef PDEPrimalEquationSetClass::BCParams BCParams;
  typedef PDEPrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PDEPrimalEquationSetClass::SystemVector SystemVectorClass;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Solution order
  ////////////////////////////////////////////////////////////////////////////////////////
  int order_pde = 0;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler PDE sans AV
  ////////////////////////////////////////////////////////////////////////////////////////
  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  // PDE
  EulerResidualInterpCategory interp = Euler_ResidInterp_Momentum;
  NDPDEClass pde(gas, interp, eHarten);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Define boundary conditions - P0
  ////////////////////////////////////////////////////////////////////////////////////////
  // reference state (freestream)
  const Real Mach = 4.0;

  const Real rhoRef = 1;                            // density scale

#if 1
  const Real tRef = 1;                              // temperature
//  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                      // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
#else
  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif


  PyDict Inflow;
  Inflow[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitiveTemperature;
  Inflow[DensityVelocityTemperature2DParams::params.rho] = rhoRef;
  Inflow[DensityVelocityTemperature2DParams::params.u] = uRef;
  Inflow[DensityVelocityTemperature2DParams::params.v] = vRef;
  Inflow[DensityVelocityTemperature2DParams::params.t] = tRef;

  std::cout << "rho: " << rhoRef << "[kg/m3]" << std::endl
            << "u:   " << uRef   << "[m/s]"   << std::endl
            << "v:   " << vRef   << "[m/s]"   << std::endl
            << "T:   " << tRef   << "[K]"     << std::endl;

  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCClass;

  // Define inflow BC
  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCInflow[BCClass::ParamsType::params.Characteristic] = false;
  BCInflow[BCClass::ParamsType::params.StateVector] = Inflow;

  // Define wall BC
  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  // Define outflow BC
  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  // Define BC list
  PyDict PyPDEBCList;
  PyPDEBCList["Outflow"] = BCOutflow;
  PyPDEBCList["Wall"] = BCWall;
  PyPDEBCList["Inflow"] = BCInflow;
  ////////////////////////////////////////////////////////////////////////////////////////

  for (int grid_index = 9; grid_index <= 9; grid_index++)
  {

    ////////////////////////////////////////////////////////////////////////////////////////
    // Create grid
    ////////////////////////////////////////////////////////////////////////////////////////
    std::string filename_base = "tmp/HOW5_CI1_Inviscid/";
    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directory(base_dir);
    std::string meshName = "tmp/HOW5_CI1/Enthalpy/P3/64k/mesh_a" + std::to_string(grid_index) + "_outQ1.grm";
    if (world.rank() == 0) std::cout << "Openning: " << meshName << std::endl;
    XField_PX<PhysD2,TopoD2> xfld(world, meshName);

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Get boundary group information
    ////////////////////////////////////////////////////////////////////////////////////////
    std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;
    PDEBCBoundaryGroups["Outflow"] = {3,5};
    PDEBCBoundaryGroups["Wall"] = {0,1,2};
    PDEBCBoundaryGroups["Inflow"] = {4};
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Check BCs
    ////////////////////////////////////////////////////////////////////////////////////////
    BCParams::checkInputs(PyPDEBCList);
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Allocate our Euler PDE field - P0
    ////////////////////////////////////////////////////////////////////////////////////////
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> pde_qfld0(xfld, 0, BasisFunctionCategory_Legendre);
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> pde_lgfld0( xfld, 0, BasisFunctionCategory_Legendre,
                                                              BCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups) );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Allocate our Euler PDE field - higher P
    ////////////////////////////////////////////////////////////////////////////////////////
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Legendre);
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> pde_lgfld( xfld, order_pde, BasisFunctionCategory_Legendre,
                                                              BCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups) );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Initial condition for what ever I am doing
    ////////////////////////////////////////////////////////////////////////////////////////
    ArrayQ q0 = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoRef, uRef, vRef, tRef) );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set initial conditions - P0
    ////////////////////////////////////////////////////////////////////////////////////////
    pde_qfld0 = q0;
    pde_lgfld0 = 0;
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // AES Settings
    ////////////////////////////////////////////////////////////////////////////////////////
//    const int quadOrder = 3*(order_pde + 2)-1; // 14 at order=3
    const int quadOrder = 1;
    QuadratureOrder quadratureOrder( xfld, quadOrder );
    std::vector<Real> tol = {1.0e-10, 1.0e-10};
  //  Real viscousEtaParameter = 6;
  //  DiscretizationDGBR2 disc(0, viscousEtaParameter);
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Create our AES to solve our PDE sans AV
    ////////////////////////////////////////////////////////////////////////////////////////
    PDEPrimalEquationSetClass PrincipalEqSet0(xfld, pde_qfld0, pde_lgfld0, pde, quadratureOrder, ResidualNorm_Default, tol, cellGroups,
                                              interiorTraceGroups, PyPDEBCList, PDEBCBoundaryGroups);
    PDEPrimalEquationSetClass PrincipalEqSet(xfld, pde_qfld, pde_lgfld, pde, quadratureOrder, ResidualNorm_Default, tol, cellGroups,
                                             interiorTraceGroups, PyPDEBCList, PDEBCBoundaryGroups);

    AlgebraicEquationSet_PTCClass PrincipalEqSetPTC0(xfld, pde_qfld0, pde, quadratureOrder, cellGroups, PrincipalEqSet0);
    AlgebraicEquationSet_PTCClass PrincipalEqSetPTC(xfld, pde_qfld, pde, quadratureOrder, cellGroups, PrincipalEqSet);
    ////////////////////////////////////////////////////////////////////////////////////////

#define P_SEQUENCE 0
#if P_SEQUENCE
    ////////////////////////////////////////////////////////////////////////////////////////
    // Construct Newton Solver - P0
    ////////////////////////////////////////////////////////////////////////////////////////
    PyDict NewtonSolverDict0, LineUpdateDict0;
#if defined(SANS_PETSC)
    std::cout << "Linear solver: PETSc" << std::endl;

    PyDict PreconditionerDict0;
    PyDict PreconditionerILU0;
    PyDict PETScDict0;

    PreconditionerILU0[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
    PreconditionerILU0[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    PreconditionerILU0[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
    PreconditionerILU0[SLA::PreconditionerILUParam::params.FillLevel] = 4;

    PreconditionerDict0[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict0[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU0;

    PETScDict0[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
    PETScDict0[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
    PETScDict0[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0e-10;
    PETScDict0[SLA::PETScSolverParam::params.MaxIterations] = 2000;
    PETScDict0[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
    PETScDict0[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict0;
    PETScDict0[SLA::PETScSolverParam::params.Verbose] = true;
    PETScDict0[SLA::PETScSolverParam::params.computeSingularValues] = false;
    PETScDict0[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
    //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

    NewtonSolverDict0[NewtonSolverParam::params.LinearSolver] = PETScDict0;

#elif defined(INTEL_MKL)
    std::cout << "Linear solver: MKL_PARDISO" << std::endl;
    PyDict MKL_PARDISODict0;
    MKL_PARDISODict0[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
    NewtonSolverDict0[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict0;
#else
    std::cout << "Linear solver: UMFPACK" << std::endl;
    PyDict UMFPACKDict0;
    UMFPACKDict0[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    NewtonSolverDict0[NewtonSolverParam::params.LinearSolver] = UMFPACKDict0;
#endif

    LineUpdateDict0[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  //  LineUpdateDict0[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;
    LineUpdateDict0[HalvingSearchLineUpdateParam::params.verbose] = true;

    NewtonSolverDict0[NewtonSolverParam::params.LineUpdate] = LineUpdateDict0;
    NewtonSolverDict0[NewtonSolverParam::params.MinIterations] = 0;
    NewtonSolverDict0[NewtonSolverParam::params.MaxIterations] = 100;
    NewtonSolverDict0[NewtonSolverParam::params.Verbose] = true;

    NewtonSolverParam::checkInputs(NewtonSolverDict0);
    NewtonSolver<SystemMatrixClass> Solver0( PrincipalEqSet0, NewtonSolverDict0 );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set initial guess from initial condition - P0
    ////////////////////////////////////////////////////////////////////////////////////////
    SystemVectorClass ini0(PrincipalEqSet0.vectorStateSize());
    SystemVectorClass sln0(PrincipalEqSet0.vectorStateSize());

    PrincipalEqSet0.fillSystemVector(ini0);
    sln0 = ini0;
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // SOLVE - P0
    ////////////////////////////////////////////////////////////////////////////////////////
#define PSEQ_NEWTON_SOLVE 0
#if PSEQ_NEWTON_SOLVE
    SolveStatus status = Solver.solve(ini, sln);
    BOOST_CHECK( status.converged );
  #else
  //  NewtonSolverDict0[NewtonSolverParam::params.MaxIterations] = 20;
    PyDict PseudoTimeDict0;
    PseudoTimeDict0[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict0;
    PseudoTimeDict0[PseudoTimeParam::params.invCFL] = 1;
    PseudoTimeDict0[PseudoTimeParam::params.invCFL_max] = 100;
    PseudoTimeDict0[PseudoTimeParam::params.invCFL_min] = 0.0;

    PseudoTimeDict0[PseudoTimeParam::params.CFLDecreaseFactor] = 0.5;
    PseudoTimeDict0[PseudoTimeParam::params.CFLIncreaseFactor] = 10.0;
    PseudoTimeDict0[PseudoTimeParam::params.Verbose] = true;

#if defined(SANS_PETSC)
    PseudoTimeDict0[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/PTC0_History_PETSc.dat";
#elif defined(INTEL_MKL)
    PseudoTimeDict0[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/PTC0_History_MKL.dat";
#else
    PseudoTimeDict0[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/PTC0_History_UMFPACK.dat";
#endif

    PseudoTime<SystemMatrixClass> PTC0(PseudoTimeDict0, PrincipalEqSetPTC0);
    PTC0.iterate(150);
#endif
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set initial conditions - Higher P
    ////////////////////////////////////////////////////////////////////////////////////////
    pde_qfld0.projectTo(pde_qfld);
    ////////////////////////////////////////////////////////////////////////////////////////

#if 1
    string filename0;
    filename0 = "tmp/HOW5_CI1_"
              + Type2String<QType>::str()
              + "_P"
              + std::to_string(order_pde)
              + "_PSeq."
              + std::to_string(world.rank())
              + ".plt";
    output_Tecplot( pde_qfld, filename0 );
#endif

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set initial guess from initial condition - Higher P
    ////////////////////////////////////////////////////////////////////////////////////////
    SystemVectorClass ini(PrincipalEqSetPTC.vectorStateSize());
    SystemVectorClass sln(PrincipalEqSetPTC.vectorStateSize());

    PrincipalEqSetPTC.fillSystemVector(ini);
    sln = ini;
    ////////////////////////////////////////////////////////////////////////////////////////
#else
    ////////////////////////////////////////////////////////////////////////////////////////
    // Set initial conditions
    ////////////////////////////////////////////////////////////////////////////////////////
    pde_qfld = q0;
    pde_lgfld = 0;
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set initial guess from initial condition - P0
    ////////////////////////////////////////////////////////////////////////////////////////
    SystemVectorClass ini(PrincipalEqSetPTC.vectorStateSize());
    SystemVectorClass sln(PrincipalEqSetPTC.vectorStateSize());

    PrincipalEqSetPTC.fillSystemVector(ini);
    sln = ini;
    ////////////////////////////////////////////////////////////////////////////////////////
#endif

    ////////////////////////////////////////////////////////////////////////////////////////
    // Construct Newton Solver
    ////////////////////////////////////////////////////////////////////////////////////////
    PyDict NewtonSolverDict, LineUpdateDict;
#if defined(SANS_PETSC)
    std::cout << "Linear solver: PETSc" << std::endl;

    PyDict PreconditionerDict;
    PyDict PreconditionerILU;
    PyDict PETScDict;

    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
    PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
    PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

    PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
    PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
    PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0e-10;
    PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
    PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
    PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
    PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
    PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
    PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
    //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
    std::cout << "Linear solver: MKL_PARDISO" << std::endl;
    PyDict MKL_PARDISODict;
    MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
    std::cout << "Linear solver: UMFPACK" << std::endl;
    PyDict UMFPACKDict;
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

    LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
    //LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchResiduals] = true;
    //LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;
    LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

    NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
    NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
    NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

    NewtonSolverParam::checkInputs(NewtonSolverDict);
    NewtonSolver<SystemMatrixClass> Solver( PrincipalEqSetPTC, NewtonSolverDict );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Solve
    ////////////////////////////////////////////////////////////////////////////////////////
#define NEWTON_SOLVE 0
#if NEWTON_SOLVE
    SolveStatus status = Solver.solve(ini, sln);
    BOOST_CHECK( status.converged );
#else
    bool converged = false;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
    PyDict PseudoTimeDict;
    PseudoTimeDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
    PseudoTimeDict[PseudoTimeParam::params.invCFL] = 100;
    PseudoTimeDict[PseudoTimeParam::params.invCFL_max] = 1000;
    PseudoTimeDict[PseudoTimeParam::params.invCFL_min] = 0.0;

  //  PseudoTimeDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.1;
  //  PseudoTimeDict[PseudoTimeParam::params.CFLIncreaseFactor] = 2.0;
    PseudoTimeDict[PseudoTimeParam::params.Verbose] = true;
    PseudoTimeDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_residual"
        + std::to_string(grid_index) + ".dat";
    PseudoTimeDict[PseudoTimeParam::params.LineSearchHistoryFile] = filename_base + "PTC_linesearch"
        + std::to_string(grid_index) + ".dat";

    PseudoTime<SystemMatrixClass> PTC(PseudoTimeDict, PrincipalEqSetPTC);
    converged = PTC.iterate(20000);
#endif
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Output Tecplot file
    ////////////////////////////////////////////////////////////////////////////////////////
    string filename;
    filename = "tmp/HOW5_CI1_Inviscid/HOW5_CI1_Inviscid"
              + Type2String<QType>::str()
              + "_P"
              + std::to_string(order_pde)
              + "_G"
              + std::to_string(grid_index)
              + ".plt";
    output_Tecplot( pde_qfld, filename );
    ////////////////////////////////////////////////////////////////////////////////////////
    BOOST_REQUIRE(converged);
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
