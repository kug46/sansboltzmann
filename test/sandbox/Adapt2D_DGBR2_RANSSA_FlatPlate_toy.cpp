// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_RANSA_FlatPlate_btest
// Testing of the MOESS framework on a 2D Navier-Stokes problem


#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Meshing/gmsh/WriteMesh_gmsh.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_FlatPlate_X1.h"

#include "Field/output_Points.h"
#include "Field/output_Tecplot.h"
#include "tools/minmax.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_RANSA_FlatPlate_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_RANSA_FlatPlate_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_Entropy<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_Distance ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2_DOFWeighted;
  std::vector<Real> tol = {1.0e-11, 1.0e-11};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.189;
  const Real Reynolds = 2.4e6;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale

  // Sutherland viscosity
//  const Real tSuth = 198.6/540;     // R/R
  //const Real tRef  = 491.6/540;     // R/R

#if 1
  const Real tRef = 1;                              // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                      // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
#else
  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  // PDE
  GasModel gas(gamma, R);
//  ViscosityModelType visc(muRef, tRef, tSuth);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntRef}) );

  // DGBR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

#if 1

  const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  const Real TtRef = tRef*Gamma;
  const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  std::cout << "Pt/Pref = " << PtRef/pRef << std::endl;
  std::cout << "Tt/Tref = " << TtRef/tRef << std::endl;

  typedef BCRANSSA2D<BCTypeDirichlet_mitState, BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> >::ParamsType PtTta_Params;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCIn[PtTta_Params::params.TtSpec] = TtRef;
  BCIn[PtTta_Params::params.PtSpec] = PtRef;
  BCIn[PtTta_Params::params.aSpec]  = aoaRef;
  BCIn[PtTta_Params::params.nt]     = ntRef;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;

#elif 0
  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_mitState;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.vtSpec] = vRef;
  BCIn["nt"] = ntRef; // TODO: not happy about this

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;
#else
  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aSpec = atan(vRef/uRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;
  BCIn["nt"] = ntRef; // TODO: not happy about this

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
#endif

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2,4};
  BCBoundaryGroups["BCNoSlip"] = {1};
  BCBoundaryGroups["BCOut"] = {3};
  BCBoundaryGroups["BCIn"] = {5};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  // Drag output
  // (halved because the plate is length 2... sigh)
  NDOutputClass outputFcn(pde, 0.5/(0.5*rhoRef*(uRef*uRef + vRef*vRef)), 0.);
  OutputIntegrandClass outputIntegrand( outputFcn, {1} );
#endif

  int orderL = 2, orderH = 2;
  int powerL = 7, powerH = 7;

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc == 3)
  {
    orderL = orderH = std::stoi(argv[1]);
    powerL = powerH = std::stoi(argv[2]);
  }
#endif

  if (world.rank() == 0 )
  {
    std::cout << "orderL, orderH = " << orderL << ", " << orderH << std::endl;
    std::cout << "powerL, powerH = " << powerL << ", " << powerH << std::endl;
  }

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

  #if defined(SANS_PETSC)
  if (world.rank() == 0 )
      std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;


  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 3;
  // if ( orderL == orderH )
  //   PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (orderL+1)*(orderL+2)*(orderL+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-20;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  //PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.DGMRES;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 6000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-14;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-6;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] =  false;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type] =
                  SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver]    = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLDecreaseFactor]  = 0.1;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor]  = 2;
  NonlinearSolverDict[PseudoTimeParam::params.SteveLogic] = true;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::map<int,std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>> refPoints;

  //{u,v,polyOrder=2,numModes=6}
  refPoints[2].resize(1);
  refPoints[2][0] =  { {0,0},
                       {0.5,0},
                       {1,0},
                       {0,0.5},
                       {0.25,0.5},
                       {0.5,0.5} };
  //{u,v,polyOrder=3,numModes=12}
  refPoints[3].resize(1);
  refPoints[3][0] =  { {0,0},
                       {0.276393202250021,0},
                       {0.7236067977499789,0},
                       {1,0},
                       {0,0.276393202250021},
                       {0.2,0.276393202250021},
                       {0.5236067977499789,0.276393202250021},
                       {0.7236067977499789,0.276393202250021},
                       {0,0.7236067977499789},
                       {0.076393202250021,0.7236067977499789},
                       {0.2,0.7236067977499789},
                       {0.276393202250021,0.7236067977499789} };

  //--------ADAPTATION LOOP--------
  for (int order = orderL; order <= orderH; order++)
  {
#if defined(SANS_PETSC)
    // update the MDF
    // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order
    // PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
    // PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
    // NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
    // NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver]    = NewtonSolverDict;
    // SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

#endif
    timer totalTime;
    for (int power = powerL; power <= powerH; power++ )
    {

      // Grid
      int gridpower = -1;
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_FlatPlate_X1(world, gridpower) );

      // Scale the grid to match the TMR domain size
//      for (int i = 0; i < pxfld->nDOF(); i++ )
//        pxfld->DOF(i) *= 2;

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Optimizer_MaxEval] = 6000;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
      MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction] = 0.5;

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      const int maxIter = 30;

      Real nk = pow(2,power);
      int targetCost = 1000*nk;

      // std::cout<< "nk = " << nk<< ", targetCost = " << targetCost << std::endl;

      const int quadOrder = 3*(order + 2)-1; // 14 at order=3

      // Hugh file system
      // to make sure folders have a consistent number of zero digits
      const int string_pad = 6;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
      std::string filename_base = "tmp/2FP/DG" + int_pad + "_P" + std::to_string(order) + "/";

      // Marshall file system
      // std::string filename_base = "tmp/TMR_FP/P" + std::to_string(order) + "/" + std::to_string(nk) + "k/";
      if (world.rank() == 0)
      {
        boost::filesystem::create_directories(filename_base);
      }

      // Update options for this cost
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      PyDict ParamDict;

      //Compute distance field
      std::shared_ptr<Field_CG_Cell<PhysD2, TopoD2, Real>>
        pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfld, 2, BasisFunctionCategory_Lagrange);
      DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));

      //Solution data
      typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>((*pdistfld, *pxfld), pde, order, order+1,
                                                   BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                   active_boundaries, ParamDict, disc);

      output_Tecplot( get<0>(pGlobalSol->paramfld), filename_base + "distfld_a0.plt" );


      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrand);

      //Set initial solution
      pGlobalSol->setSolution(q0);


      std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, pde.variableInterpreter().stateNames() );

      pInterface->solveGlobalPrimalProblem();

      std::string qfld_filename = filename_base + "qfld_a0.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename, pde.variableInterpreter().stateNames() );

//      std::string pntfld_filename = filename_base + "pntfld_a0";
//      output_Points( pGlobalSol->primal.qfld, pntfld_filename, refPoints[2*order], pde.variableInterpreter().stateNames() );
//
//      std::string gmsh_filename = filename_base + "xfld_a0.msh";
//      WriteMesh_gmsh( pGlobalSol->primal.qfld.getXField(), gmsh_filename, BCBoundaryGroups );

      pInterface->solveGlobalAdjointProblem();

      std::string adjfld_filename = filename_base + "adjfld_a0.plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


      // Initializing it here because then rank 0 will have made the folder correctely.
      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      const Real iCost = pInterface->getCost();

      mesh_adapter.adjustTargetCost( iCost + 0.05*(targetCost-iCost) ); // set initial cost to 5 %

      for (int iter = 0; iter < maxIter+1; iter++)
      {
        if (world.rank() == 0 )
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        //Compute error estimates
        pInterface->computeErrorEstimates();

        // Update targetCost
        // mesh_adapter.adjustTargetCost( iCost + (targetCost-iCost)
        //     *( 3*pow((1.0+iter)/(1+maxIter),2) - 2*pow((1.0+iter)/(1+maxIter),3) ) ); // step up smooth cubicly
        // mesh_adapter.adjustTargetCost( iCost + (targetCost-iCost)*( std::min(pow((1.0+iter)/(1+maxIter-5),2),1.0) ) ); // step up linearly
        // every 5 increase the % of total cost allowed
        if ( iter%5 == 0 )
          mesh_adapter.adjustTargetCost( iCost + (floor(100.0*iter/(maxIter-5))/100.0) *(targetCost-iCost) );


        //Perform local sampling and adapt mesh
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
        pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

        //Compute distance field
        pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfldNew, 2, BasisFunctionCategory_Lagrange);
        DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));

        //    break;

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>((*pdistfld, *pxfldNew), pde, order, order+1,
                                                        BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                        active_boundaries, ParamDict, disc);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, AdjLinearSolverDict,
                                                               outputIntegrand);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld = pxfldNew;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, pde.variableInterpreter().stateNames() );

        pInterface->solveGlobalPrimalProblem();

        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename, pde.variableInterpreter().stateNames() );

//        std::string pntfld_filename = filename_base + "pntfld_a" + std::to_string(iter+1);
//        output_Points( pGlobalSol->primal.qfld, pntfld_filename, refPoints[2*order], pde.variableInterpreter().stateNames() );
//
//        std::string gmsh_filename = filename_base + "xfld_a" + std::to_string(iter+1) + ".msh";
//        WriteMesh_gmsh( pGlobalSol->primal.qfld.getXField(), gmsh_filename, BCBoundaryGroups );

        pInterface->solveGlobalAdjointProblem();

        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
      }

      if (world.rank() == 0 )
      {
        fadapthist << std::endl<< std::endl << "Total Elapsed Time = " << totalTime.elapsed() << " s" <<std::endl;
        fadapthist.close();
      }
    }

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
