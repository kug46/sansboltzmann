// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGBR2_Triangle_NavierStokes_FlatPlate_toy
// testing of 2-D DGBR2 for NS for flat plate

//#define SANS_FULLTEST
//#define SANS_VERBOSE
//#define IMPORTMESH
#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include <iostream>
#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/PDENavierStokes_Brenner2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
//#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_OutputWeightRsd_VMSD.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "../../src/Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "../../src/Discretization/VMSD/SolutionData_VMSD.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"

#include "Adaptation/MOESS/MOESSParams.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/ProjectSoln/ProjectGlobalField.h"

#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_CGStabilized_Triangle_NavierStokes_Joukowski_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_VMSD_Triangle_Joukowski )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;

  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, VMSD> OutputIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_VMSD<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                                   AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

//  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  std::vector<Real> tol = {1.0e-9, 1.0e-9, 1.0e-9};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.5;
  const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real pRef = 1.0;                            // pressure
  const Real tRef = pRef/(rhoRef*R);

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = cRef*Mach;                              // velocity scale

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict BCOut(BCIn);

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

#ifndef IMPORTMESH
  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0};
  BCBoundaryGroups["BCIn"] = {1};
  BCBoundaryGroups["BCOut"] = {2};
#else
  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0,1};
  BCBoundaryGroups["BCIn"] = {2};
  BCBoundaryGroups["BCOut"] = {3};
#endif

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde, 1.0);
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, stab);
#else
  // Drag output
  NDOutputClass outputFcn(pde, 1., 0.);
  OutputIntegrandClass outputIntegrand( outputFcn, BCBoundaryGroups["BCNoSlip"] );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if (defined(SANS_PETSC) && 1 )

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-20;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 200;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-20;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 500;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-12;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  PyDict SolverContinuationDict0, NonlinearSolverDict0;

  // Easier PTC for subsequent solves
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;
  NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor] = 2;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);
  //
#ifndef IMPORTMESH
  int powermin = 0;
  int powermax = 0;
  //
  int ordermin = 1;
  int ordermax = 1;
#else
  int powermin = 0;
  int powermax = 0;
  int ordermin = 2;
  int ordermax = 2;
#endif

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  DiscretizationVMSD stab;

  string filename_base = "tmp/LAMJOUK/VMSD";
  boost::filesystem::create_directories(filename_base);

  filename_base += "/P1";

  string filename2 = filename_base + "_Drag.txt";
  fstream foutsol;


  std::shared_ptr<SolutionClass> pGlobalSol;
  std::shared_ptr<SolutionClass> pGlobalSolOld;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfldOld;

  //  fstream fout( "tmp/joukowski_dgbr2.txt", fstream::out );
  for (int power = powermin; power <= powermax; power++)
  {


#ifndef IMPORTMESH
    //      int npow = pow(2, power);
    //      // Read PX Output grid
    string filein = "grids/JOUKOWSKI/Joukowski_Laminar_Challenge_tri_ref";
    filein += to_string(power);
    filein += "_Q4.grm";

#else
    //      int npow = pow(2, power);
    //      // Read PX Output grid
    string filein = "/home2/CGvsDG/LAMJOUK/AGLS/JOUK_01000_P2/mesh_a25_outQ3.grm";

#endif
    pxfld = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, filein);

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    // loop over grid resolution: 2^power
    //    int power = 0;
    for (int order = ordermin; order <= ordermax; order++)
    {


      foutsol.open( filename2, fstream::app );

      stab.setNitscheOrder(order);

      const int porder = 1;
      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, stab,
                                                    order, porder, order+1, porder,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Legendre,
                                                   BasisFunctionCategory_Lagrange,
                                                   active_boundaries);

      const int quadOrder = 2*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      //Set initial solution
      if (order == ordermin && power == powermin)
        pGlobalSol->setSolution(q0);
      else
        pGlobalSol->setSolution(*pGlobalSolOld);

      pInterface->solveGlobalPrimalProblem();
//      pInterface->solveGlobalAdjointProblem();

      Real output = pInterface->getOutput();

      Real Cd = output / (0.5*rhoRef*qRef*qRef);


#if 1

      int nDOFq = pGlobalSol->primal.qfld.nDOFnative();
      foutsol << order << " " << power << " " << nDOFq << " " << std::setprecision(16) << Cd;
      foutsol << endl;
#endif
      foutsol.close();

      pGlobalSolOld = pGlobalSol;
      pxfldOld = pxfld;




#if 1

      int ordermax = std::max(order, porder);
      std::shared_ptr<SolutionClass> pGlobalSol2;
      pGlobalSol2 = std::make_shared<SolutionClass>(*pxfld, pde, stab,
                                                    ordermax, ordermax, ordermax+1, ordermax+1,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Legendre,
                                                   BasisFunctionCategory_Lagrange,
                                                   active_boundaries);

//      std::shared_ptr<SolutionClass> pGlobalSol3;
//      pGlobalSol2 = std::make_shared<SolutionClass>(*pxfld, pde, stab,
//                                                    order, order, order+1, order+1,
//                                                   BasisFunctionCategory_Lagrange,
//                                                   BasisFunctionCategory_Legendre,
//                                                   BasisFunctionCategory_Lagrange,
//                                                   active_boundaries);


//      std::cout << "420\n";
      pGlobalSol->primal.projectTo(pGlobalSol2->primal);

//      std::cout << "423\n";
//      for (int i=0; i<pGlobalSol2->primal.qpfld.nDOF(); i++ )
//      {
//        std::cout << "i: " << i << "\n";
//        pGlobalSol2->primal.qpfld.DOF(i) += pGlobalSol3->primal.qpfld.DOF(i);
//      }


      {
        // Tecplot dump grid
        string filename = filename_base + "_P";
        filename += to_string(order);
        filename += "_X";
        filename += to_string(power);
        filename += ".plt";
        output_Tecplot( pGlobalSol2->primal.qfld, filename );
        std::cout << "Wrote Primal\n";
      }

      {
        // Tecplot dump grid
        string filename = filename_base + "_P";
        filename += to_string(order);
        filename += "_X";
        filename += to_string(power);
        filename += "_qp.plt";
        output_Tecplot( pGlobalSol2->primal.qpfld, filename );
        std::cout << "Wrote Primal\n";
      }



      // Tecplot dump grid
//      string filename2 = filename_base + "_P";
//      filename2 += to_string(order);
//      filename2 += "_X";
//      filename2 += to_string(power);
//      filename2 += "_adjoint.plt";
//      output_Tecplot( pInterface->getAdjField(), filename2 );
//      std::cout << "Wrote Adjoint\n";
#endif


    }

  }

  //  time_t end;
  //  time(&end);
  //  Real seconds = difftime(end,start);

  //  std::cout << " ELAPSED TIME: " << seconds << " seconds"<< std::endl;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
