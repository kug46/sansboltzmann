// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
//  Solve2D_HDG_Triangle_NavierStokes_Poiseuille_btest
// testing of 2-D HDG for NS on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/BCParameters.h"

#include "pde/OutputCell_SolutionErrorSquared.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/ProjectSoln_HDG.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"
#include "Discretization/HDG/JacobianFunctionalCell_HDG.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectSolnTrace_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Trace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_InteriorFieldTraceGroup.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_HDG_Triangle_NavierStokes_OjedaMMS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_HDG_Triangle_OjedaMMS )
{

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;
  typedef SolutionFunction_NavierStokes2D_OjedaMMS<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolnType;
  typedef SolnNDConvertSpace<PhysD2,SolnType> NDSolnType;
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;

  typedef OutputEuler2D_TotalEnthalpyWindowed<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_HDG_Output<NDOutputClass> IntegrandOutputClass;

  typedef OutputCell_SolutionErrorSquared< PDEClass, SolnType > SolutionErrorClass;
  typedef OutputNDConvertSpace<PhysD2, SolutionErrorClass> NDSolutionErrorClass;
  typedef IntegrandCell_HDG_Output<NDSolutionErrorClass> IntegrandSolutionErrorClass;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_HDG< NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  std::vector<Real> tol = {1e-10, 1e-10, 1e-10};

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  GasModel gas(gamma, R);
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // INFLOW CONDITIONS TO MATCH OJEDA'S MMS

  Real rho0 = 1.0;
  Real u0 = 0.2;
  Real v0 = 0.1;
  Real p0 = 1.0;
  Real t0 = p0/rho0/R;

  //  Real a0 = atan2(v0,u0);

  const Real muRef = 0.02; // Re_L = 10ish
  const Real Prandtl = 0.72;

  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDSolnType soln(gas);
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(soln) );

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, forcingptr );

  // HDG solution field at inlet
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rho0, u0, v0, p0) );

  // L2 error
  NDSolutionErrorClass slnError(soln);
  IntegrandSolutionErrorClass fcnOut( slnError, {0} );

  // Enthalpy Output
  NDOutputClass out(pde);
  IntegrandOutputClass fcnOut2( out, {0} );

#if 1
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient);
#else
  Real refl = 1;
  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient, refl);
#endif


  std::map<std::string, std::vector<int>> BCBoundaryGroups;
#if 1
  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rho0;
  StateVector[Conservative2DParams::params.rhou] = rho0*u0;
  StateVector[Conservative2DParams::params.rhov] = rho0*v0;

  const Real ERef = Cv*t0 + 0.5*(u0*u0+v0*v0);

  StateVector[Conservative2DParams::params.rhoE] = rho0*ERef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;

  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;

  BCBoundaryGroups["BCIn"]  = {0,1,2,3};
#else

  Real s0 = log(  p0/ pow(rho0, gamma) );
  Real H0 =  gas.enthalpy(rho0,t0) + 0.5*(u0*u0+v0*v0);

  PyDict BCIn;

  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = s0;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = H0;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = atan(v0/u0);

  PyDict BCOut;

  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = p0;


  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;
  PyBCList["BCSym"] = BCSymmetry;

  BCBoundaryGroups["BCIn"] = {3};
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCSym"] = {0,2};
#endif

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  // Set up Newton Solver
  PyDict NewtonSolverDict, LinSolverDict, NewtonLineUpdateDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
#else
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
#endif

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] =  true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  string filename_base = "tmp/OjedaStag/EDGh/EDGh";

  string filename2 = filename_base + "_Errors.txt";
  fstream foutsol;


  int ii, jj;
//
  int ordermin = 1;
  int ordermax = 3;

  for (int order = ordermin; order <= ordermax; order++)
  {

    int powermin = 3;
    int powermax = 7;
    if (order == 3)
      powermax = 6;



  // loop over grid resolution: 2^power
    for (int power = powermin; power <= powermax; power++)
    {
      jj = pow( 2, power ) + 1;
      ii = jj; //small grid!

      string filename = filename_base + "_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);

      foutsol.open( filename2, fstream::app );

      // grid
      XField2D_Box_Triangle_X1 xfld(ii, jj);

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);

//#define HDGSOLVE
#ifdef HDGSOLVE //HDG
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
#else //EDG??
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Hierarchical);
#endif
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

      //L2 projection of exact solution
//      ProjectSoln_HDG(qfld, afld, qIfld, soln);


//      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(soln, {0}), (xfld, qfld) );
//      for_each_InteriorFieldTraceGroup<TopoD2>::apply( ProjectSolnTrace_Discontinuous(soln,{0,1,2}), (xfld, qIfld)  );

      // Set the uniform initial condition
      qfld = q0;
      afld = 0.0;
      qIfld = q0;
      lgfld = 0;

      //////////////////////////
      //SOLVE HIGHER P SYSTEM
      //////////////////////////

      QuadratureOrder quadratureOrder( xfld, 3*order+1 );

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                         {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      bool solved = Solver.solve(ini,sln).converged;
      BOOST_REQUIRE(solved);
      PrimalEqSet.setSolutionField(sln);

      // check that the residual is zero

      rsd = 0;
      PrimalEqSet.residual(sln, rsd);
      std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

      bool converged = PrimalEqSet.convergedResidual(rsdNorm);

      if (!converged)
      {
        std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);
        PrimalEqSet.printDecreaseResidualFailure(rsdNorm, std::cout);
      }
      BOOST_CHECK(converged);

      // Monitor Velocity Error
      Real Hout = 0.0;
      ArrayQ L2err = 0.0;
//
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( fcnOut, L2err ), xfld, (qfld, afld),
          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( fcnOut2, Hout ), xfld, (qfld, afld),
          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii
          << ": Integrated Enthalpy = " << std::setprecision(12) <<  Hout << "\n";

      foutsol << order << " " << ii << std::setprecision(15);
      foutsol << " " << sqrt(L2err[0]);
      foutsol << " " << sqrt(L2err[1]);
      foutsol << " " << sqrt(L2err[2]);
      foutsol << " " << sqrt(L2err[3]);
      foutsol << " " << Hout;
      foutsol << " " << Hout << "\n";
#endif

      // adjoint (right hand side)
//      typedef SurrealS<4> SurrealClass;
//      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
//      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, order, BasisFunctionCategory_Legendre);
//
//#ifdef HDGSOLVE //HDG
//      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, order, BasisFunctionCategory_Legendre);
//#else //EDG??
//      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, order, BasisFunctionCategory_Hierarchical);
//#endif
//      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
//      mufld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
//
//      SystemVectorClass rhs(PrimalEqSet.vectorEqSize());
//      rhs = 0;
//
//      IntegrateCellGroups<TopoD2>::integrate(
//          JacobianFunctionalCell_HDG<SurrealClass>( fcnOut2, rhs(PrimalEqSet.iPDE) ),
//          xfld, (qfld, afld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
//
//      // solve
//      SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);
//      SystemVectorClass adj(PrimalEqSet.vectorStateSize());
//      solverAdj.solve(rhs, adj);
//
//      bfld = 0;
//      PrimalEqSet.setAdjointField(adj, wfld, bfld, wIfld, mufld);



      string filenameprimal = filename + ".plt";
//      string filenameadjoint = filename + "_adj.plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filenameprimal );
//      output_Tecplot( wfld, filenameadjoint );

      foutsol.close();

    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
