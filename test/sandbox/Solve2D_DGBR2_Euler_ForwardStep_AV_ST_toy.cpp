// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_DGBR2_Quad_Euler_forwardFacingStep_AV_BDF
// Inviscid 2D forward facing step with shock and artificial viscosity


//#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <chrono>
#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime2D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime2D.h"


#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Output_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_Dispatch_DGBR2.h"

#include "Discretization/isValidState/SetValidStateCell.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/Function/FunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "Field/output_Tecplot_PDE.h"
#include "Field/output_gnuplot.h"

#include "unit/UnitGrids/XField3D_ForwardStep_Hex_X1.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Euler_ForwardStep_AV_ST_toy )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Euler_ForwardStep_AV_ST_toy )
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // Initial Typedefs
  ////////////////////////////////////////////////////////////////////////////////////////
//  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveSurrogate QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> BCVector;
  typedef BCEuler2D<BCTypeFunction_mitState, PDEClass> BCSolnClass;
  typedef SolutionFunction_Euler2D_Const<TraitsSizeEuler, TraitsModelEulerClass> InitialConditionClass;

  typedef OutputEuler2D_Pressure<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_GenH_DG ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // MPI Communicator
  ////////////////////////////////////////////////////////////////////////////////////////
  mpi::communicator world;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Discretization parameters
  ////////////////////////////////////////////////////////////////////////////////////////
  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1.0e-10, 1.0e-10};
  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  bool isSteady = true;
  bool hasSpaceTimeDiffusionEuler = false;
  Real viscousEtaParameter = 2*Hex::NEdge;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our gas model
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict gasModelDict;
  Real gamma = 1.4;
  Real R = 0.4;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Initial Condition
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict TimeIC;
  TimeIC[BCSolnClass::ParamsType::params.Function.Name] = BCSolnClass::ParamsType::params.Function.Const;
  TimeIC[InitialConditionClass::ParamsType::params.rho] = gamma;
  TimeIC[InitialConditionClass::ParamsType::params.u] = 3.0;
  TimeIC[InitialConditionClass::ParamsType::params.v] = 0.0;
  TimeIC[InitialConditionClass::ParamsType::params.p] = 1.0;
  TimeIC[InitialConditionClass::ParamsType::params.gasModel] = gasModelDict;
  Real Tend = 4.0;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCTimeOut;
  BCTimeOut[BCParams::params.BC.BCType] = BCParams::params.BC.TimeOut;

  PyDict Inflow;
  Inflow[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitiveTemperature;
  Inflow[DensityVelocityTemperature2DParams::params.rho] = gamma;
  Inflow[DensityVelocityTemperature2DParams::params.u] = 3.0;
  Inflow[DensityVelocityTemperature2DParams::params.v] = 0.0;
  Inflow[DensityVelocityTemperature2DParams::params.t] = 1.0 / (gamma * R);

  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCBase;
  typedef BCmitAVSensor2D<BCTypeFullState_mitState, BCBase> BCClass;
  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCInflow[BCClass::ParamsType::params.Characteristic] = true;
  BCInflow[BCClass::ParamsType::params.StateVector] = Inflow;

  PyDict BCReflect;
  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  PyDict BCTypeSpaceTimeInitialCondition;
  BCTypeSpaceTimeInitialCondition[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC_Function;
  BCTypeSpaceTimeInitialCondition[BCSolnClass::ParamsType::params.Function] = TimeIC;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Add BCs to BC list
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict PyBCList;
  PyBCList["TimeIC"] = BCTypeSpaceTimeInitialCondition;
  PyBCList["BCReflect"] = BCReflect;
  PyBCList["BCInflow"] = BCInflow;
  PyBCList["None"] = BCNone;
  PyBCList["TimeOut"] = BCTimeOut;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check BCs!
  ////////////////////////////////////////////////////////////////////////////////////////
  BCParams::checkInputs(PyBCList);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Assign BCs to grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["TimeIC"] = {XField3D_ForwardStep_Hex_X1::iZmin};
  BCBoundaryGroups["BCReflect"] = {XField3D_ForwardStep_Hex_X1::iYmin, XField3D_ForwardStep_Hex_X1::iYmax};
  BCBoundaryGroups["BCInflow"] = {XField3D_ForwardStep_Hex_X1::iXmin};
  BCBoundaryGroups["None"] = {XField3D_ForwardStep_Hex_X1::iXmax};
  BCBoundaryGroups["TimeOut"] = {XField3D_ForwardStep_Hex_X1::iZmax};
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up linear solver
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict LinearSolverDict;
#if defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict;

  if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up line update
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict LineUpdateDict;
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.1;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up Newton solver
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict NewtonSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] =  true;
#if defined(SANS_PETSC)
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
#else
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up overall non-linear solver
  //  define USE_PTC to use continuation, otherwise use plain Newton
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict SolverContinuationDict, NonlinearSolverDict;
#define USE_PTC 1
#if USE_PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type] =
                  SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]    = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLDecreaseFactor]  = 0.1;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor]  = 2;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLPartialStepDecreaseFactor]  = 1.0;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#else
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#endif
  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check inputs
  ////////////////////////////////////////////////////////////////////////////////////////
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Grid loop
  ////////////////////////////////////////////////////////////////////////////////////////
  for (int grid_index = 1; grid_index <= 1; grid_index++)
  {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Where all our data gets saved to
    ////////////////////////////////////////////////////////////////////////////////////////
    std::string filename_base = "tmp/ForwardFacingStep/";
    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directory(base_dir);

    NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_residual"
        + std::to_string(grid_index) + ".dat";
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set up grid here
    ////////////////////////////////////////////////////////////////////////////////////////
    int ii = 300*grid_index;
    int jj = 100*grid_index;
    int tt = 40*grid_index;
    std::shared_ptr<XField<PhysD3, TopoD3>> pxfld =
            std::make_shared<XField3D_ForwardStep_Hex_X1>( world, ii, jj, tt, 0, 3, 0, 1, 0, Tend );

    std::vector<int> cellGroups;
    for ( int i = 0; i < pxfld->nCellGroups(); i++)
      cellGroups.push_back(i);

    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<GenHField_DG<PhysD3,TopoD3>> phfld =
        std::make_shared<GenHField_DG<PhysD3,TopoD3>>(*pxfld);
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Get our active boundaries
    ////////////////////////////////////////////////////////////////////////////////////////
    std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Dat structures for transfering solutions for p-seq
    ////////////////////////////////////////////////////////////////////////////////////////
    typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;

    std::shared_ptr<SolutionClass> pGlobalSol;
    std::shared_ptr<SolutionClass> pGlobalSolOld;
    std::shared_ptr<SolverInterfaceClass> pInterface;
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // P-sequencing loop
    ////////////////////////////////////////////////////////////////////////////////////////
    for (int order = 0; order <= 3; order++)
    {
      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our pde objects
      ////////////////////////////////////////////////////////////////////////////////////////
      PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusionEuler, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp);
      Sensor sensor(pdeEulerAV);
      SensorAdvectiveFlux sensor_adv(0.0, 0.0);
      SensorViscousFlux sensor_visc(order, true);
      SensorSource sensor_source(order, sensor);
      // AV PDE with sensor equation
      NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order, hasSpaceTimeDiffusionEuler,
                     EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Pressure output
      ////////////////////////////////////////////////////////////////////////////////////////
      NDOutputClass fcnOutput(pde);
      OutputIntegrandClass outputIntegrand(fcnOutput, cellGroups);
      ////////////////////////////////////////////////////////////////////////////////////////


      ////////////////////////////////////////////////////////////////////////////////////////
      // Set up solution field data structures
      ////////////////////////////////////////////////////////////////////////////////////////
      pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pxfld), pde, order, order,
                                                   BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                   active_boundaries, LinearSolverDict, disc);
      pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      //Create solver interface
      ////////////////////////////////////////////////////////////////////////////////////////
      const int HexOrder = 3*(order + 2)-1; // 14 at order=3
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, HexOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial guess, or transfer solution for p-sequencing
      ////////////////////////////////////////////////////////////////////////////////////////
      if (order == 0)
      {
        // initial condition
        ArrayQ q0 = pde.setDOFFrom( AVVariable<DensityVelocityPressure2D, Real>({{gamma, 3.0, 0.1, 1.0}, 0.0}) );

        pGlobalSol->setSolution(q0);
      }
      else
      {
        pGlobalSol->setSolution(*pGlobalSolOld);
      }
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Transfer solution
      ////////////////////////////////////////////////////////////////////////////////////////
      pGlobalSolOld = pGlobalSol;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Output initial guess
      ////////////////////////////////////////////////////////////////////////////////////////
      string filenameIC = "ForwardFacingStepIC_"
                     + Type2String<QType>::str()
                     + "_P"
                     + std::to_string(order)
                     + "_G"
                     + std::to_string(grid_index)
                     + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, filename_base + filenameIC );
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // SOLVE PRIMAL
      ////////////////////////////////////////////////////////////////////////////////////////
      auto t1 = std::chrono::high_resolution_clock::now();
      pInterface->solveGlobalPrimalProblem();
      auto t2 = std::chrono::high_resolution_clock::now();
      std::cout << "P solve: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << "ms" << std::endl;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Output primal
      ////////////////////////////////////////////////////////////////////////////////////////
      string sol_filename = "ForwardFacingStepPrimal_"
                     + Type2String<QType>::str()
                     + "_P"
                     + std::to_string(order)
                     + "_G"
                     + std::to_string(grid_index)
                     + ".plt";
      output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld,
                     pGlobalSol->primal.rfld, filename_base + sol_filename);
      ////////////////////////////////////////////////////////////////////////////////////////
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
