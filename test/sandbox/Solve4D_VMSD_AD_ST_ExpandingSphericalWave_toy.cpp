// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve4D_VMSD_AD_ST_ExpandingSphericalWave
// testing of 3D+T convergence on an advection diffusion problem
// learning to 4D SANS case for Cory... don't @ me

#include <boost/test/unit_test.hpp>
#include <boost/filesystem/path.hpp>          // to make filesystems
#include <boost/filesystem/operations.hpp>    // to make filesystems
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"               // saw a comment that this gives us Reals

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

// i'm just gonna extrapolate these to 3d assuming the hardware is there
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

// these, I think, give us tools to convert the 2D physical problem to a
// 3D (2D+T) adaptation problem
#include "pde/NDConvert/PDENDConvertSpaceTime3D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime3D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime3D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime3D.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "pde/AdvectionDiffusion/ForcingFunction3D.h"
#include "pde/ForcingFunction3D_MMS.h"

#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_VMSD_BR2.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_Local_VMSD_BR2.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"

#include "Adaptation/MOESS/SolverInterface_VMSD_BR2.h"
#include "Discretization/VMSDBR2/SolutionData_VMSD_BR2.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldSpacetime_DG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"
#include "Field/FieldSpacetime_EG_Cell.h"
#include "Field/FieldSpacetime_DG_BoundaryTrace.h"
#include "Field/FieldSpacetime_CG_BoundaryTrace.h"
#include "Field/FieldLiftSpaceTime_DG_Cell.h"
#include "Field/FieldLiftSpaceTime_DG_BoundaryTrace.h"

// not sure if this is sufficient include...
#include "unit/UnitGrids/XField4D_Box_Ptope_X1.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define REFINE_UNSTRUCTURED
// #undef SANS_PETSC

#if defined(SANS_AVRO) && defined(REFINE_UNSTRUCTURED)
#include "library/tesseract.h"
#include "Meshing/avro/XField_avro.h"
#elif !defined(SANS_AVRO) && defined(REFINE_UNSTRUCTURED)
#undef REFINE_UNSTRUCTURED
#endif

#define BOUNDARY_LAYER
// #define SINUSOIDAL_DECAY
// #define NEUMANN_ON

using namespace std;

// explicitly instantiate the classes for correct coverage info
namespace SANS
{
}

using namespace SANS;

static inline int factorial(int n)
{
  int m;
  if (n <= 1)
    m= 1;
  else
    m= n*factorial(n - 1);

  return m;
}

BOOST_AUTO_TEST_SUITE(Solve4D_VMSD_AD_ST_ExpandingSphericalWave_test_suite)

BOOST_AUTO_TEST_CASE(Solve4D_VMSD_AD_ST_ExpandingSphericalWave_test_case)
{

  // parse inputs

  int argc= boost::unit_test::framework::master_test_suite().argc;
  char ** argv= boost::unit_test::framework::master_test_suite().argv;

  int iiSamples= -1;
  int pSamples= -1;

#if !defined(REFINE_UNSTRUCTURED) || !defined(SANS_AVRO)
  // specify m (m x m x m x m grid) and p
  if (argc == 3)
  {
    iiSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  else if (argc == 2)
  {
    iiSamples= std::stoi(argv[1]);
    pSamples= 1;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  else
  {
    iiSamples= 2;
    pSamples= 2;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
#else
  std::string filename_mesh= "";

  // specify m (m x m x m x m grid) and p
  if (argc == 4)
  {
    iiSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    filename_mesh= argv[3];
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  else if (argc == 3)
  {
    iiSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    filename_mesh= "tmp/unif_meshes/n" + to_string(iiSamples) + "d4.json";
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  else if (argc == 2)
  {
    iiSamples= std::stoi(argv[1]);
    pSamples= 1;
    filename_mesh= "tmp/unif_meshes/n" + to_string(iiSamples) + "d4.json";
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  else
  {
    iiSamples= 2;
    pSamples= 2;
    filename_mesh= "tmp/unif_meshes/n" + to_string(iiSamples) + "d4.json";
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
#endif

  SANS_ASSERT(iiSamples > 0);
  SANS_ASSERT(pSamples > 0);

  // specify physical problem

  // Real a= 1.0;
  // Real b= 1.0;
  // Real c= 1.0;
  // Real nu= 1.0;
  // Real lambda_z= 1.0;
  // Real lambda_t= -1.0;

  Real alpha= 0.1;
  Real k0= 5.0;
  Real k1= 100.0;
  Real r0= 0.3;
  Real V= 0.5;
  Real nu= 0.01;

  std::cout << "\tiiSamples: " << iiSamples << std::endl;
  std::cout << "\tpSamples: " << pSamples << std::endl << std::endl;

  std::cout << "Physical problem:" << std::endl;
  std::cout << "\texpanding spherical wave?" << std::endl;

#ifdef REFINE_UNSTRUCTURED
  std::string filename_base= "tmp/AD4D_ST_VMSD_ESW_unifconv_unstructured/";
#else
  std::string filename_base= "tmp/AD4D_ST_VMSD_ESW_unifconv/";
#endif
  std::string filename_case;

  typedef AdvectiveFlux3D_Radial AdvectionModel;
  typedef ViscousFlux3D_Uniform DiffusionModel;
  typedef Source3D_UniformGrad SourceModel;

  // shortcut for the appropriate PDE object for our 3D physics specified
  typedef PDEAdvectionDiffusion<PhysD3, AdvectionModel, DiffusionModel,
      SourceModel> PDEClass;
  // shortcut for the 3D+T conversion
  typedef PDENDConvertSpaceTime<PhysD3, PDEClass> NDPDEClass;

  typedef ScalarFunction3D_SphericalWaveDecay NDSolutionExact;
  const Real trueOutput= 1.4127415906407418839657;
  // alpha= 0.1, k0= 5.0, k1= 100.0, r0= 0.3, V= 0.5, nu= 0.01
  // const Real trueOutput= 0.43745587919021083832230;
  // // alpha= 0.1, k0= 5.0, k1= 1000.0, r0= 0.3, V= 0.5, nu= 0.01


  // // shortcut to the conversion for the exact solution to spacetime
  // typedef SolnNDConvertSpaceTime<PhysD3, SolutionExact> NDSolutionExact;

  typedef BCAdvectionDiffusion3DVector<AdvectionModel, DiffusionModel> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_VMSD_Output<NDOutputClass, NDPDEClass> OutputIntegrandClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, NDSolutionExact> ErrorClass;
  // typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpaceTime<PhysD3, ErrorClass> NDErrorClass;
  typedef IntegrandCell_VMSD_Output<NDErrorClass, NDPDEClass> ErrorIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  // the solution to the problem
// #define VMSD_SIP
#ifdef VMSD_SIP
  typedef SolutionData_VMSD<PhysD4, TopoD4, NDPDEClass,
      ParamBuilderType> SolutionClass;
#else
  typedef SolutionData_VMSD_BR2<PhysD4, TopoD4, NDPDEClass,
      ParamBuilderType> SolutionClass;
#endif
  // parameters to the solver
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  // algebraic equation set
#ifdef VMSD_SIP
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpaceTime,
      BCVector, AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
#else
  typedef AlgebraicEquationSet_VMSD_BR2<NDPDEClass, BCNDConvertSpaceTime,
      BCVector, AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
#endif
  // solver interface, everything it needs to run the solution
#ifdef VMSD_SIP
  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass,
      OutputIntegrandClass> SolverInterfaceClass;
#else
  typedef SolverInterface_VMSD_BR2<SolutionClass, PrimalEquationSetClass,
      OutputIntegrandClass> SolverInterfaceClass;
#endif

  // mpi
  mpi::communicator world;

  // CREATE ADVECTION DIFFUSION PDE

  AdvectionModel advection(V);
  DiffusionModel diffusion(nu, 0, 0, 0, nu, 0, 0, 0, nu);
  SourceModel source(0.0, 0.0, 0.0, 0.0);

  // create exact solution
  PyDict solnArgs;

  solnArgs[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function.Name]=
      BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function.SphericalWaveDecay;
  solnArgs[NDSolutionExact::ParamsType::params.alpha]= alpha;
  solnArgs[NDSolutionExact::ParamsType::params.k0]= k0;
  solnArgs[NDSolutionExact::ParamsType::params.k1]= k1;
  solnArgs[NDSolutionExact::ParamsType::params.velocity]= V;
  solnArgs[NDSolutionExact::ParamsType::params.radius0]= r0;

  NDSolutionExact solnExact(solnArgs);
  // NDSolutionExact solnExact(alpha, k0, k1, V, r0);

  // create forcing function for MMS
  typedef ForcingFunction3D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));
  // typedef ForcingFunction3D_SphericalWaveDecay<PDEClass> ForcingType;
  // std::shared_ptr<ForcingType> forcingptr(new ForcingType(alpha, k0, k1, V, r0, nu));

  // overwrite pde with forcing function
  NDPDEClass pde(advection, diffusion, source, forcingptr);

  // CREATE BOUNDARY CONDITIONS

  // awaiting implementation
  //  PyDict BCSoln_timeIC;
  //  BCSoln_timeIC[BCParams::params.BC.BCType]= BCParams::params.BC.Function_mitState;

  PyDict BCSoln_timeIC;
  BCSoln_timeIC[BCParams::params.BC.BCType]= BCParams::params.BC.TimeIC_Function;
  BCSoln_timeIC[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;

  PyDict BCSoln_timeOut;
  BCSoln_timeOut[BCParams::params.BC.BCType]= BCParams::params.BC.TimeOut;

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType]= BCParams::params.BC.Function_mitState;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.SolutionBCType]=
      BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Upwind]= false;

#if defined(NEUMANN_ON)
  PyDict BCSoln_Neumann;
  BCSoln_Neumann[BCParams::params.BC.BCType]= BCParams::params.BC.Function_mitState;
  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;
  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.SolutionBCType]=
      BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.SolutionBCType.Neumann;
  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Upwind]= false;
#endif

  //  PyDict BCNone;
  //  BCNone[BCParams::params.BC.BCType]= BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCSoln_timeIC"]= BCSoln_timeIC;
  PyBCList["BCSoln_timeOut"]= BCSoln_timeOut;
  PyBCList["BCSoln_Dirichlet"]= BCSoln_Dirichlet;
#if defined(NEUMANN_ON)
  PyBCList["BCSoln_Neumann"]= BCSoln_Neumann;
#endif
  //  PyBCList["BCNone"]= BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // CREATE DISCRETIZATION PARAMETERS

  // VMSD discretization
  DiscretizationVMSD stab;

  enum ResidualNormType ResNormType= ResidualNorm_L2;
  std::vector<Real> tol= {1.0e-10, 1.0e-10, 1.0e-10};

  // CREATE OUTPUT FUNCTIONAL TO ADAPT WITH

  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, true); // add true parameter at end to sum states

  // CREATE INTEGRAND FOR EXACT SOLN ERROR

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(pde, fcnError, {0}, true); // add true parameter at end to sum states

  // SETUP NONLINEAR SOLVER (PROBABLY UNNEEDED FOR LINEAR PROBLEM)

  // nonlinear solver dicts
  PyDict SolverContinuationDict;
  PyDict NonlinearSolverDict;
  PyDict NewtonSolverDict;
  PyDict AdjLinearSolverDict;
  PyDict LinearSolverDict;
  PyDict LineUpdateDict;
  // PyDict UMFPACKDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 )
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 3;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  //  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
  NewtonSolverDict[NewtonSolverParam::params.Verbose]= true;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver]= SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  // LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method]= LineUpdateParam::params.LineUpdate.HalvingSearch;

  // NewtonSolverDict[NewtonSolverParam::params.LinearSolver]= UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate]= LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations]= 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations]= 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose]= true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]= NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation]= NonlinearSolverDict;

  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);

  // REFINEMENT LOOP
  // uniformly refine in h and in p

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_base);

  // prep output files and data

  // norm data
  Real orderVec;
  Real hDOFVec;
  int nElemVec;
  int nDOFVec;
  Real normVec;

  // output file
  std::string convhist_filename= filename_base + "test.convhist";
  fstream convhist;

  std::vector<int> cellGroups= {0};

  if (world.rank() == 0)
    printf("\nRunning case with N= %d and p= %d.\n\n", iiSamples, pSamples);

  // CREATE INITIAL GRID

  // ?x?x?x?
  const int ii= iiSamples;
  const int jj= ii;
  const int kk= jj;
  const int mm= kk;

#ifndef REFINE_UNSTRUCTURED
  const Real xmin= 0.0;
  const Real xmax= 1.0;
  const Real ymin= 0.0;
  const Real ymax= 1.0;
  const Real zmin= 0.0;
  const Real zmax= 1.0;
  const Real tmin= 0.0;
  const Real tmax= 1.0;

  std::shared_ptr<XField4D_Box_Ptope_X1> pxfld= std::make_shared<XField4D_Box_Ptope_X1>
                                (world, ii, jj, kk, mm, xmin, xmax, ymin, ymax, zmin, zmax, tmin, tmax);
#else
  using avro::coord_t;
  using avro::index_t;

  avro::Context context;

  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld0(world, {ii, jj, kk, mm});

  coord_t number= 4;                    // topological dimension
//  Real x0[4]= {0.5, 0.5, 0.5, 0.5};
//  std::vector<Real> lens(number, 1.);
//  std::shared_ptr<avro::Body> pbody= std::make_shared<avro::library::Tesseract>(x0, lens.data());
  std::shared_ptr<avro::Model> model= std::make_shared<avro::Model>(&context, "tesseract");
  model->addBody(xfld0.body_ptr(), false);
//  model->addBody(pbody,false);

  avro::Mesh<avro::Simplex> mesh(number, number);

  std::shared_ptr<XField_avro<PhysD4, TopoD4>> pxfld_avro= std::make_shared<XField_avro<PhysD4, TopoD4>>(world, model);
  std::shared_ptr<XField<PhysD4, TopoD4>> pxfld= pxfld_avro;

  avro::io::readMesh(filename_mesh, mesh);

  mesh.vertices().findGeometry(*xfld0.body_ptr());
//  mesh.vertices().findGeometry(*pbody);

  //mesh.vertices().print("v", true);

  pxfld_avro->import(mesh);

#endif

  const int order= pSamples;

  const int iXmin= XField4D_Box_Ptope_X1::iXmin;
  const int iXmax= XField4D_Box_Ptope_X1::iXmax;
  const int iYmin= XField4D_Box_Ptope_X1::iYmin;
  const int iYmax= XField4D_Box_Ptope_X1::iYmax;
  const int iZmin= XField4D_Box_Ptope_X1::iZmin;
  const int iZmax= XField4D_Box_Ptope_X1::iZmax;
  const int iTmin= XField4D_Box_Ptope_X1::iWmin;
  const int iTmax= XField4D_Box_Ptope_X1::iWmax;

  // define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSoln_timeIC"]= {iTmin};
  BCBoundaryGroups["BCSoln_timeOut"]= {iTmax};
#if !defined(NEUMANN_ON)
  BCBoundaryGroups["BCSoln_Dirichlet"]= {iXmin, iYmin, iZmin, iXmax, iYmax, iZmax};
#else
  BCBoundaryGroups["BCSoln_Dirichlet"]= {iXmin, iYmin, iZmin};
  BCBoundaryGroups["BCSoln_Neumann"]= {iXmax, iYmax, iZmax};
#endif
  //      BCBoundaryGroups["BCNone"]= {iXmax, iYmax};

  std::vector<int> active_boundaries= BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  std::vector<int> interiorTraceGroups;

  for (int iT= 0; iT < pxfld->nInteriorTraceGroups(); iT++)
    interiorTraceGroups.push_back(iT);

  // set the order in the stabilization
  stab.setNitscheOrder(order);

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol= std::make_shared<SolutionClass>(*pxfld, pde, stab,
                                              order, order,
                                              order + 1, order + 1,
                                              BasisFunctionCategory_Lagrange,
                                              BasisFunctionCategory_Lagrange,
                                              BasisFunctionCategory_Lagrange,
                                              active_boundaries);

  const int quadOrder= 2*(order + 1);    // somehow this means 2*(order + 1)?

  // create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface= std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                     cellGroups, interiorTraceGroups,
                                                     PyBCList, BCBoundaryGroups,
                                                     SolverContinuationDict,
                                                     AdjLinearSolverDict, outputIntegrand);

  // set initial solution
  //  pGlobalSol->setSolution(solnExact, cellGroups);
  pGlobalSol->setSolution(0.0);

  const int dofReq= ii*jj*kk*mm*factorial(order + PhysD4::D)/factorial(order);

  filename_case= filename_base + "dof" + stringify(dofReq) + "p" + stringify(order) + "/";

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_case);

  std::string adapthist_filename= filename_case + "test.adapthist";
  fstream fadapthist;

  if (world.rank() == 0)
  {
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

    fadapthist << std::setw(4)  << "Iter"
        << std::setw(12) << "DOFTrue"
        << std::setw(16) << "DOFEstimate"
        << std::setw(16) << "DOFPrediction"
        << std::setw(20) << "ErrorIndicator"
        << std::setw(20) << "ErrorEstimate"
        << std::setw(20) << "ErrorTrue"
        << std::setw(22) << "Output"
        << std::setw(11) << "EvalCount"
        << std::setw(15) << "ObjReduction"
        << std::endl;
  }

  pInterface->solveGlobalPrimalProblem();
  // pInterface->computeErrorEstimates();

  // compute exact error
  QuadratureOrder quadratureOrder(pGlobalSol->primal.qfld.getXField(), quadOrder);

  Real squareError= 0;

  IntegrateCellGroups<TopoD4>::integrate(
      FunctionalCell_VMSD(errorIntegrand, squareError),
      pGlobalSol->primal.qfld.getXField(),
      (pGlobalSol->primal.qfld, pGlobalSol->primal.qpfld),
      quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size());

#ifdef SANS_MPI
  int nDOFtotal = 0;
  boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );

  // count the number of elements possessed by this processor
  int nElem = 0;
  for (int elem = 0; elem < pxfld->nElem(); elem++ )
    if (pxfld->getCellGroupGlobal<Pentatope>(0).associativity(elem).rank() == world.rank())
      nElem++;

  int nElemtotal = 0;
  boost::mpi::reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>(), 0 );
#else
  int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
  int nElemtotal = pxfld->nElem();
#endif

  Real output= pInterface->getOutput();

  Real norm= squareError;

  orderVec= order;
  nElemVec= nElemtotal;
  nDOFVec= nDOFtotal;
  hDOFVec= 1.0/pow(nDOFtotal, 1.0/((Real) PhysD4::D));
  normVec= sqrt(norm);

  if (world.rank() == 0)
  {

    bool fileStarted= boost::filesystem::exists(convhist_filename);

    convhist.open(convhist_filename, fstream::app);
    BOOST_REQUIRE_MESSAGE(convhist.good(), "Error opening file: " + convhist_filename);

    if (!fileStarted)
    {
      convhist << "VARIABLES=";
      convhist << "\"p\"";
      convhist << ", \"DOFrequested\"";
      convhist << ", \"1/pow(DOF, 1.0/4.0)\"";
      convhist << ", \"Nelem\"";
      convhist << ", \"Ndof\"";
      convhist << ", \"L2 error\"";
      convhist << std::endl;
    }

    convhist << std::setprecision(16) << std::scientific;

    // outputfile
    convhist << orderVec;
    convhist << ", " << ii*jj*kk*mm*factorial(order + PhysD4::D)/factorial(order);
    convhist << ", " << hDOFVec;
    convhist << ", " << nElemVec;
    convhist << ", " << nDOFVec;
    convhist << ", " << normVec;
    convhist << endl;

    fadapthist << std::setw(4)  << 0;
    fadapthist << std::setw(12) << (int) 0;
    fadapthist << std::setw(16) << std::setprecision(3) << std::fixed << 0.0;
    fadapthist << std::setw(16) << std::setprecision(3) << std::fixed << 0.0;
    fadapthist << std::setw(20) << std::setprecision(10) << std::scientific << (output - trueOutput);
    fadapthist << std::setw(20) << std::setprecision(10) << std::scientific << (output - trueOutput);
    fadapthist << std::setw(20) << std::setprecision(10) << std::scientific << (output - trueOutput);
    fadapthist << std::setw(22) << std::setprecision(12) << std::scientific << output;
    fadapthist << std::setw(11) << 0;
    fadapthist << std::setw(15) << std::setprecision(4) << std::scientific << 0.0;
    fadapthist << std::endl;

    fadapthist.close();
    convhist.close();
    std::cout << "Output file written to " << convhist_filename << "." << std::endl;
  }

}

BOOST_AUTO_TEST_SUITE_END()
