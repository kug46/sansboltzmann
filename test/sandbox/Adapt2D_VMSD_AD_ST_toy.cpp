// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_VMSD_AD_ST_toy
// testing of 1D+T adaptation on an advection-diffusion problem

// #define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>               // to make filesystems
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"               // saw a comment that this gives us Reals

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "pde/ForcingFunction1D_MMS.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
// #include "Discretization/VMSDBR2/IntegrandCell_VMSD_BR2_Output.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_VMSD_BR2.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_Local_VMSD_BR2.h"
#include "Discretization/VMSDBR2/IntegrandBoundaryTrace_OutputWeightRsd_VMSD_BR2.h"
#include "Discretization/VMSDBR2/IntegrandBoundaryTrace_Flux_mitState_VMSD_BR2.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"

#include "Adaptation/MOESS/SolverInterface_VMSD_BR2.h"
#include "Discretization/VMSDBR2/SolutionData_VMSD_BR2.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_BoundaryTrace.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
//#include "Meshing/avro/MesherInterface_avro.h"
//#include "Meshing/EGADS/EGModel.h"
#include "Meshing/avro/XField_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#endif


using namespace std;

// explicitly instantiate the classes for correct coverage info
namespace SANS
{

}

using namespace SANS;

// static inline int factorial(int n)
// {
//   int m;
//   if (n <= 1)
//     m= 1;
//   else
//     m= n*factorial(n - 1);
//
//   return m;
// }


BOOST_AUTO_TEST_SUITE(Adapt2D_VMSD_AD_ST_toy_test_suite)

BOOST_AUTO_TEST_CASE(Adapt2D_VMSD_AD_ST_toy_test_case)
{

  // parse inputs

  int argc= boost::unit_test::framework::master_test_suite().argc;
  char ** argv= boost::unit_test::framework::master_test_suite().argv;

  int pSamples= -1;               // p to be used
  int dofReqSamples= -1;          // dof to be requested

  const int lastn= 5;             // adaptation iterations to print

  int maxIter= -1;                // adaptation iterations to do

  // specify dof and p and maxIter
  if (argc == 4)
  {
    dofReqSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    maxIter= std::stoi(argv[3]);
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // just specify dof and p
  else if (argc == 3)
  {
    dofReqSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    maxIter= 20;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // just specify dof
  else if (argc == 2)
  {
    dofReqSamples= std::stoi(argv[1]);
    pSamples= 1;
    maxIter= 20;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // specify nothing
  else
  {
    dofReqSamples = 125;
    pSamples= 1;
    maxIter= 20;
    std::cout << std::endl << "No input parsed! Defaults:" << std::endl;
  }


  bool DG_CONV= false;

  SANS_ASSERT(dofReqSamples > 0);
  SANS_ASSERT(pSamples > 0);
  SANS_ASSERT(maxIter >= 0);
  SANS_ASSERT(maxIter >= lastn);

  Real ax= 0.5;
  Real nu= 0.005;

  Real A_gauss= 1.0;
  Real mu0_gauss= 0.25;
  Real C_gauss= 0.5;
  Real sigma_gauss= 0.025;

  std::cout << "\tdofReqSamples: " << dofReqSamples << std::endl;
  std::cout << "\tpSamples: " << pSamples << std::endl;
  std::cout << "\tmaxIter: " << maxIter << std::endl << std::endl;

  std::cout << "Physical problem:" << std::endl;
  std::cout << "\ta: (" << ax << ")" << std::endl;
  std::cout << "\tnu: " << nu << std::endl << std::endl;


  std::string filename_base= "tmp/AD2D_ST_VMSD_adapconv";
  filename_base += "/";
  std::string filename_case;

  typedef AdvectiveFlux1D_Uniform AdvectionModel;
  typedef ViscousFlux1D_Uniform DiffusionModel;
  typedef Source1D_UniformGrad SourceModel;

  // shortcut for the appropriate PDE object for our 1D physics specified
  typedef PDEAdvectionDiffusion<PhysD1, AdvectionModel, DiffusionModel,
      SourceModel> PDEClass;
  // shortcut for the 1D+T conversion
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;

  typedef ScalarFunction1D_GaussianUnsteady SolutionExact;
  const Real trueOutput= 0.04431134640574236;

  // shortcut to the conversion for the exact solution to spacetime
  typedef SolnNDConvertSpaceTime<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCAdvectionDiffusion1DVector<AdvectionModel, DiffusionModel> BCVector;

  typedef BCParameters<BCVector> BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_VMSD_Output<NDOutputClass, NDPDEClass> OutputIntegrandClass;
  // typedef IntegrandCell_VMSD_BR2_Output<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#else
  typedef OutputAdvectionDiffusion1D_WeightedState<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, VMSDBR2> OutputIntegrandClass;
#endif

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpaceTime<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_VMSD_Output<NDErrorClass, NDPDEClass> ErrorIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  // the solution to the problem
  typedef SolutionData_VMSD_BR2<PhysD2, TopoD2, NDPDEClass,
      ParamBuilderType> SolutionClass;
  // parameters to the solver
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  // algebraic equation set
  typedef AlgebraicEquationSet_VMSD_BR2<NDPDEClass, BCNDConvertSpaceTime,
      BCVector, AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
  // solver interface, everything it needs to run the solution
  typedef SolverInterface_VMSD_BR2<SolutionClass, PrimalEquationSetClass,
      OutputIntegrandClass> SolverInterfaceClass;

  // mpi
  mpi::communicator world;

  // CREATE ADVECTION DIFFUSION PDE

  AdvectionModel advection(ax);
  DiffusionModel diffusion(nu);
  SourceModel source(0.0, 0.0);

  // create exact solution
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.Function.Name]=
      BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.Function.GaussianUnsteady;
  solnArgs[NDSolutionExact::ParamsType::params.A]= A_gauss;
  solnArgs[NDSolutionExact::ParamsType::params.mu0]= mu0_gauss;
  solnArgs[NDSolutionExact::ParamsType::params.C]= C_gauss;
  solnArgs[NDSolutionExact::ParamsType::params.sigma]= sigma_gauss;

  NDSolutionExact solnExact(solnArgs);

  // create forcing function for MMS
  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  // overwrite pde with forcing function
  // NDPDEClass pde(advection, diffusion, source);
  NDPDEClass pde(advection, diffusion, source, forcingptr);

  // CREATE BOUNDARY CONDITIONS

  PyDict BCSoln_timeIC;
  BCSoln_timeIC[BCParams::params.BC.BCType]= BCParams::params.BC.TimeIC_Function;
  BCSoln_timeIC[BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;

  PyDict BCSoln_timeOut;
  BCSoln_timeOut[BCParams::params.BC.BCType]= BCParams::params.BC.TimeOut;

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType]= BCParams::params.BC.Function_mitState;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.SolutionBCType]=
      BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.Upwind]= false;

   PyDict BCSoln_Neumann;
   BCSoln_Neumann[BCParams::params.BC.BCType]= BCParams::params.BC.Function_mitState;
   BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;
   BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.SolutionBCType]=
       BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.SolutionBCType.Neumann;
   BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params.Upwind]= false;

   PyDict BCNone;
   BCNone[BCParams::params.BC.BCType]= BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCSoln_timeIC"]= BCSoln_timeIC;
  PyBCList["BCSoln_timeOut"]= BCSoln_timeOut;
  PyBCList["BCSoln_Dirichlet"]= BCSoln_Dirichlet;
  PyBCList["BCSoln_Neumann"]= BCSoln_Neumann;
  PyBCList["BCNone"]= BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // CREATE DISCRETIZATION PARAMETERS

  // VMSD discretization
  DiscretizationVMSD stab;

  enum ResidualNormType ResNormType= ResidualNorm_L2_DOFWeighted;
  std::vector<Real> tol= {1.0e-08, 1.0e-08, 1.0e-08};

  // SETUP NONLINEAR SOLVER (PROBABLY UNNEEDED FOR LINEAR PROBLEM)

  // nonlinear solver dicts
  PyDict SolverContinuationDict;
  PyDict NonlinearSolverDict;
  PyDict NewtonSolverDict;
  PyDict AdjLinearSolverDict;
  PyDict LinearSolverDict;
  PyDict LineUpdateDict;
  // PyDict UMFPACKDict;

#undef SANS_PETSC
#if defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  //  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 1;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-20;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = true;
  //  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  //  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-20;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 10000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 500;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;
  //  PETScDictAdjoint[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;

  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver]= SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  // LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  // NewtonSolverDict[NewtonSolverParam::params.LinearSolver]= UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]= NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation]= NonlinearSolverDict;

  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);

  // REFINEMENT LOOP
  // adaptively refine in ndof and in p

#ifndef __clang_analyzer__
  Real targetCost= dofReqSamples;
#else
  Real targetCost= 0;
#endif

  if (DG_CONV) // Modify the dof counts to match the DG element counts - Should be much faster to run and show the same
  {
    Real nDOFperCell_DG = (pSamples + 1)*(pSamples + 2)*(pSamples + 3)*(pSamples + 4)/24;
#ifndef __clang_analyzer__
    Real nDOFperCell_CG = nDOFperCell_DG;
#else
    Real nDOFperCell_CG = 0;
#endif

    SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");

    targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
  }

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_base);

  // prep output files and data

  // norm data
  int orderVec = 0;
  Real hDOFVec[lastn] = {0};
  int nElemVec[lastn] = {0};
  int nDOFVec[lastn]  = {0};
  Real normVec[lastn] = {0};

  // output file
  std::string convhist_filename= filename_base + "test.convhist";
  fstream convhist;

  bool fileStarted= boost::filesystem::exists(convhist_filename);

  if (world.rank() == 0)
  {
    convhist.open(convhist_filename, fstream::app);
    BOOST_REQUIRE_MESSAGE(convhist.good(), "Error opening file: " + convhist_filename);

    if (!fileStarted)
    {
      convhist << "VARIABLES=";
      convhist << "\"p\"";
      convhist << ", \"DOFrequested\"";
      convhist << ", \"1/pow(DOF, 1.0/4.0)\"";
      convhist << ", \"Nelem\"";
      convhist << ", \"Ndof\"";
      convhist << ", \"L2 error\"";
      convhist << std::endl;
    }
    convhist << std::setprecision(16) << std::scientific;
  }

  // prep mesh adaption tools
  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction]= 1.0;
  MOESSDict[MOESSParams::params.CostModel]= MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity]= MOESSParams::VerbosityOptions::Progressbar;

  MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;

  MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;

  PyDict MesherDict;
#ifdef SANS_AVRO
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name]= MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;
  MesherDict[avroParams::params.Curved]= false;
#else
  SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");
#endif

  std::vector<int> cellGroups= {0};
  // CREATE INITIAL GRID

  // 3x3
  const int ii= std::max(3, (int) std::floor(1./pSamples*(pow(targetCost, 1./PhysD2::D) - 1)));
  const int jj= ii;

  std::cout << "initial grid created with ii= jj= " << ii << "." << std::endl;

  // create pointer
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;

  // generate grid
#ifdef SANS_AVRO

  using avro::coord_t;
  using avro::index_t;

  avro::Context context;

  XField_KuhnFreudenthal<PhysD2, TopoD2> xfld0(world, {ii, jj});

  std::shared_ptr<avro::Model> model= std::make_shared<avro::Model>(xfld0.context(), "square");
  model->addBody(xfld0.body_ptr(), false);

  // copy the mesh into the domain and attach the geometry
  pxfld = std::make_shared<XField_avro<PhysD2, TopoD2>>(xfld0, model);

#else
  pxfld= std::make_shared<XField2D_Box_Triangle_X1>(world, ii, jj, 0, 1, 0, 1);
#endif

// assign local variables

  const int order= pSamples;

  const int iXmin= XField2D_Box_Triangle_X1::iLeft;
  const int iXmax= XField2D_Box_Triangle_X1::iRight;
  const int iTmin= XField2D_Box_Triangle_X1::iBottom;
  const int iTmax= XField2D_Box_Triangle_X1::iTop;

  // define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSoln_timeIC"]= {iTmin};
  BCBoundaryGroups["BCSoln_timeOut"]= {iTmax};
  BCBoundaryGroups["BCSoln_Dirichlet"]= {iXmin, iXmax};
  BCBoundaryGroups["BCSoln_Neumann"]= {};
  BCBoundaryGroups["BCNone"]= {};

  std::vector<int> active_boundaries= BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  std::vector<int> interiorTraceGroups;

  for (int iTG= 0; iTG < pxfld->nInteriorTraceGroups(); iTG++)
    interiorTraceGroups.push_back(iTG);

  // CREATE OUTPUT FUNCTIONAL TO ADAPT WITH

#ifndef BOUNDARYOUTPUT
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, true);
#else
  NDOutputClass fcnOutput(pde, 1.0);
  OutputIntegrandClass outputIntegrand(fcnOutput, BCBoundaryGroups["BCSoln_timeOut"]);
#endif

  // CREATE INTEGRAND FOR EXACT SOLN ERROR

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(pde, fcnError, {0}, true);

  // set the order in the stabilization
  stab.setNitscheOrder(order);

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol= std::make_shared<SolutionClass>(*pxfld, pde, stab,
                                              order, order,
                                              order + 1, order + 1,
                                              BasisFunctionCategory_Lagrange,
                                              BasisFunctionCategory_Lagrange,
                                              BasisFunctionCategory_Lagrange,
                                              active_boundaries);

  const int quadOrder= 2*(order + 1);    // somehow this means 2*(order + 1)?

  // create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface= std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                     cellGroups, interiorTraceGroups,
                                                     PyBCList, BCBoundaryGroups,
                                                     SolverContinuationDict,
                                                     AdjLinearSolverDict, outputIntegrand);

  // set initial solution
  //  pGlobalSol->setSolution(solnExact, cellGroups);
  pGlobalSol->setSolution(0.0);

  filename_case= filename_base + "dof" + stringify(dofReqSamples) + "p" + stringify(order) + "/";

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_case);

  std::string adapthist_filename = filename_case + "test.adapthist";
  fstream fadapthist;

  if (world.rank() == 0)
  {
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.hasTrueOutput]= true;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TrueOutput]= trueOutput;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::string qfld_init_filename= filename_case + "qfld_init_a0.plt";
  output_Tecplot(pGlobalSol->primal.qfld, qfld_init_filename);
  std::string qpfld_init_filename= filename_case + "qpfld_init_a0.plt";
  output_Tecplot(pGlobalSol->primal.qpfld, qpfld_init_filename);

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename= filename_case + "qfld_a0.plt";
  output_Tecplot(pGlobalSol->primal.qfld, qfld_filename);
  std::string qpfld_filename= filename_case + "qpfld_a0.plt";
  output_Tecplot(pGlobalSol->primal.qpfld, qpfld_filename);

  std::string adjfld_filename= filename_case + "adjfld_a0.plt";
  output_Tecplot(pGlobalSol->adjoint.qfld, adjfld_filename);
  std::string adjpfld_filename= filename_case + "adjpfld_a0.plt";
  output_Tecplot(pGlobalSol->adjoint.qpfld, adjpfld_filename);

  orderVec= order;

  // ADAPTATION LOOP

  int iprinted= 0;

  stab.setNitscheOrder(order);

  for (int iter= 0; iter < maxIter + 1; iter++)
  {

    if (world.rank() == 0)
      std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    // compute error estimates
    pInterface->computeErrorEstimates();

    // perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    // // print data about the new mesh (doesn't do what you think it does...)
    // std::cout << std::endl << std::endl;
    // std::cout << "new mesh metadata:" << std::endl;
    // std::cout << "\tnumber of elements: " << pxfld->nElem();
    // std::cout << std::endl << std::endl;

    // reset interior trace for adapted mesh!
    interiorTraceGroups.clear();
    for ( int iT = 0; iT < pxfldNew->nInteriorTraceGroups(); iT++)
      interiorTraceGroups.push_back(iT);

    // make a new global solution
    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew= std::make_shared<SolutionClass>(*pxfldNew, pde, stab,
                                                   order, order,
                                                   order + 1, order + 1,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   active_boundaries);

    // fill it by performing L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    // generate a new solver interface
    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew= std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict,
                                                          LinearSolverDict, outputIntegrand);

    // update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld= pxfldNew;
    pGlobalSol= pGlobalSolNew;
    pInterface= pInterfaceNew;

    qfld_init_filename= filename_case + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot(pGlobalSol->primal.qfld, qfld_init_filename);
    qpfld_init_filename= filename_case + "qpfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot(pGlobalSol->primal.qpfld, qpfld_init_filename);

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    qfld_filename = filename_case + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot(pGlobalSol->primal.qfld, qfld_filename);
    qpfld_filename = filename_case + "qpfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot(pGlobalSol->primal.qpfld, qpfld_filename);

    adjfld_filename = filename_case + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot(pGlobalSol->adjoint.qfld, adjfld_filename);
    adjpfld_filename = filename_case + "adjpfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot(pGlobalSol->adjoint.qpfld, adjpfld_filename);

    // compute exact error
    QuadratureOrder quadratureOrder(pGlobalSol->primal.qfld.getXField(), 2*(order + 1));

    Real squareError= 0;

    IntegrateCellGroups<TopoD2>::integrate(
        FunctionalCell_VMSD(errorIntegrand, squareError),
        pGlobalSol->primal.qfld.getXField(),
        (pGlobalSol->primal.qfld, pGlobalSol->primal.qpfld),
        quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size());

#ifdef SANS_MPI
    int nDOFtotal = 0;
    boost::mpi::all_reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>());

    // count the number of elements possessed by this processor
    int nElem = 0;
    for (int elem = 0; elem < pxfld->nElem(); elem++ )
      if (pxfld->getCellGroupGlobal<Triangle>(0).associativity(elem).rank() == world.rank())
        nElem++;

    int nElemtotal = 0;
    boost::mpi::all_reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>());
#else
    int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
    int nElemtotal = pxfld->nElem();
#endif

    Real norm= squareError;

    if (world.rank() == 0)
    {
      if (iprinted < lastn)
      {
        cout << "iprinted= " << iprinted << " < lastn= " << lastn << endl;
        hDOFVec[iprinted]= 1.0/pow(nDOFtotal, 1.0/((Real) PhysD2::D));
        nElemVec[iprinted]= nElemtotal;
        nDOFVec[iprinted]= nDOFtotal;
        normVec[iprinted]= sqrt(norm);
        iprinted++;
      }
      else
      {
        for (int k= 1; k < lastn; k++)
        {
          cout << "moving k= " << k << " to (k - 1)= " << k - 1 << endl;
          hDOFVec[k - 1]= hDOFVec[k];
          nElemVec[k - 1]= nElemVec[k];
          nDOFVec[k - 1]= nDOFVec[k];
          normVec[k - 1]= normVec[k];
        }
        cout << "crowning with k= " << lastn - 1 << endl;
        hDOFVec[lastn - 1]= 1.0/pow(nDOFtotal, 1.0/((Real) PhysD2::D));
        nElemVec[lastn - 1]= nElemtotal;
        nDOFVec[lastn - 1]= nDOFtotal;
        normVec[lastn - 1]= sqrt(norm);
      }

      std::cout << std::endl << "INNER LOOP EXIT with " << nDOFtotal << " DOFs and " <<
          nElemtotal << " elements." << std::endl << std::endl;
    }
  }

  if (world.rank() == 0)
  {
    fadapthist.close();

    for (int k= 0; k < iprinted; k++)
    {
      // outputfile
      convhist << orderVec;
      convhist << ", " << (Real) targetCost;
      convhist << ", " << hDOFVec[k];
      convhist << ", " << (Real) nElemVec[k];
      convhist << ", " << (Real) nDOFVec[k];
      convhist << ", " << normVec[k];
      convhist << endl;
    }

    if (world.rank() == 0)
      convhist.close();
  }

} // Adapt2D_VMSD_AD_ST_toy_test_case

BOOST_AUTO_TEST_SUITE_END()
