// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_BuckleyLeverett_ST_btest
// Testing of the MOESS framework on a space-time Buckley-Leverett problem with artificial viscosity

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"
#include "pde/PorousMedia/Sensor_BuckleyLeverett.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/Sensor/PDESensorParameter2D.h"
#include "pde/Sensor/Source2D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2_Block2x2.h"
#include "Discretization/DG/SolutionData_DGBR2_Block2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_BuckleyLeverett_ArtificialViscosity_ST_Block2x2_test_suite )

#define SPACETIME1D_FORMULATION

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_BuckleyLeverett_ArtificialViscosity_ST_Block2x2_Triangle )
{
  typedef QTypePrimitive_Sw QType;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef TraitsModelBuckleyLeverett<QType, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass_noAV;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_noAV> NDPDEClass_noAV;

  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass_Primal;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_Primal> NDPDEClass_Primal;

  typedef Sensor_BuckleyLeverett<PDEClass_noAV> Sensor;

#ifdef SPACETIME1D_FORMULATION
  typedef Sensor_AdvectiveFlux1D_Uniform Sensor_Advection;
  typedef Sensor_ViscousFlux1D_GenHScale Sensor_Diffusion;
  typedef Source1D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor;

  typedef PDESensorParameter<PhysD1,
                             SensorParameterTraits<PhysD1>,
                             Sensor_Advection,
                             Sensor_Diffusion,
                             Source_JumpSensor > PDEClass_Sensor;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_Sensor> NDPDEClass_Sensor;
#else
  typedef Sensor_AdvectiveFlux2D_Uniform Sensor_Advection;
  typedef Sensor_ViscousFlux2D_GenHScale Sensor_Diffusion;
  typedef Source2D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor;

  typedef PDESensorParameter<PhysD2,
                             SensorParameterTraits<PhysD2>,
                             Sensor_Advection,
                             Sensor_Diffusion,
                             Source_JumpSensor > PDEClass_Sensor;
  typedef PDENDConvertSpace<PhysD2, PDEClass_Sensor> NDPDEClass_Sensor;
#endif

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QType, PDEClass_noAV> ExactSolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef BCBuckleyLeverett_ArtificialViscosity1DVector<TraitsSizeBuckleyLeverett, TraitsModelClass> BCVectorPrimal;

  typedef BCParameters<BCVectorPrimal> BCParamsPrimal;

#ifdef SPACETIME1D_FORMULATION
  typedef BCSensorParameter1DVector<Sensor_Advection, Sensor_Diffusion> BCVectorSensor;
#else
  typedef BCSensorParameter2DVector<Sensor_Advection, Sensor_Diffusion> BCVectorSensor;
#endif

  typedef BCParameters<BCVectorSensor> BCParamsSensor;

  typedef OutputCell_SolutionSquared<PDEClass_Primal> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_ArtificialViscosity ParamBuilderType;

  typedef SolutionData_DGBR2_Block2<PhysD2, TopoD2, NDPDEClass_Primal, NDPDEClass_Sensor, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType0 ParamFieldType0;
  typedef typename SolutionClass::ParamFieldType1 ParamFieldType1;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Primal, BCNDConvertSpaceTime, BCVectorPrimal,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType0> PrimalEquationSetClass_Primal;

#ifdef SPACETIME1D_FORMULATION

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Sensor, BCNDConvertSpaceTime, BCVectorSensor,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType1> PrimalEquationSetClass_Sensor;

  typedef SolverInterface_DGBR2_Block2x2<PhysD2, TopoD2,
                                         NDPDEClass_Primal, BCNDConvertSpaceTime, BCVectorPrimal,
                                         NDPDEClass_Sensor, BCNDConvertSpaceTime, BCVectorSensor,
                                         ParamBuilderType, SolutionClass,
                                         PrimalEquationSetClass_Primal, PrimalEquationSetClass_Sensor,
                                         OutputIntegrandClass> SolverInterfaceClass;
#else

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Sensor, BCNDConvertSpace, BCVectorSensor,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType1> PrimalEquationSetClass_Sensor;

  typedef SolverInterface_DGBR2_Block2x2<PhysD2, TopoD2,
                                         NDPDEClass_Primal, BCNDConvertSpaceTime, BCVectorPrimal,
                                         NDPDEClass_Sensor, BCNDConvertSpace,    BCVectorSensor,
                                         ParamBuilderType, SolutionClass,
                                         PrimalEquationSetClass_Primal, PrimalEquationSetClass_Sensor,
                                         OutputIntegrandClass> SolverInterfaceClass;
#endif

  // Grid
  int ii = 10;
  int jj = ii;

//  ReadMesh<PhysD2, TopoD2, FeFloa> reader("tmp/fefloa_out_a9.mesh");
//  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField<PhysD2, TopoD2>( reader ) );

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, jj, 0, 50, 0, 25, true );

  int iB = XField2D_Box_Triangle_X1::iBottom;
  int iR = XField2D_Box_Triangle_X1::iRight;
  int iT = XField2D_Box_Triangle_X1::iTop;
  int iL = XField2D_Box_Triangle_X1::iLeft;

  int order_primal = 1;
  int order_sensor = 1;

  // Primal PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 0.0;
  CapillaryModel cap_model(pc_max);

  NDPDEClass_noAV pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  //Create artificial viscosity PDE
  const bool hasSpaceTimeDiffusion = false;
  NDPDEClass_Primal pde_primal(order_primal, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 1.0;
  const Real SwR = 0.1;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(pde, SwL, SwR, xinit);


  //Sensor PDE
  Sensor sensor(pde_primal);
  Real Cdiff = 3.0;
#ifdef SPACETIME1D_FORMULATION
  Sensor_Advection sensor_adv(0.0); //this assumes a unit temporal velocity
  Sensor_Diffusion sensor_visc(Cdiff);
  Source_JumpSensor sensor_source(order_primal, sensor);

  NDPDEClass_Sensor pde_sensor(sensor_adv, sensor_visc, sensor_source);
#else
  Sensor_Advection sensor_adv(0.0, 0.0);
  Sensor_Diffusion sensor_visc(Cdiff);
  Source_JumpSensor sensor_source(order_primal, sensor);

  NDPDEClass_Sensor pde_sensor(sensor_adv, sensor_visc, sensor_source);
#endif

  // Primal PDE BCs
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitState, BCBuckleyLeverett1DParams<BCTypeFunction_mitState>> BCParamsAV_Sol;
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitState, BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>> BCParamsAV_Dirichlet_mitState;

  // Create a Solution dictionary
  PyDict SolnDict;
  SolnDict[BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.Name]
           = BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.PiecewiseConst;
  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.a0] = SwL;
  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.a1] = SwR;
  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.x0] = xinit;

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.None;

  PyDict BCDirichletB;
  BCDirichletB[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.Function_mitState;
  BCDirichletB[BCParamsAV_Sol::params.Function] = SolnDict;
  BCDirichletB[BCParamsAV_Sol::params.Cdiff] = Cdiff;

#if 1
  PyDict BCDirichletL;
  BCDirichletL[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.Dirichlet_mitState;
  BCDirichletL[BCParamsAV_Dirichlet_mitState::params.qB] = SwL;
  BCDirichletL[BCParamsAV_Dirichlet_mitState::params.Cdiff] = Cdiff;

  PyDict BCDirichletR;
  BCDirichletR[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.Dirichlet_mitState;
  BCDirichletR[BCParamsAV_Dirichlet_mitState::params.qB] = SwR;
  BCDirichletR[BCParamsAV_Dirichlet_mitState::params.Cdiff] = Cdiff;
#else
  PyDict BCDirichletL;
  BCDirichletL[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.LinearRobin;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.A] = 1;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.B] = 0;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.bcdata] = SwL;

  PyDict BCDirichletR;
  BCDirichletR[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.LinearRobin;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.A] = 1;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.B] = 0;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.bcdata] = SwR;
#endif

  PyDict PyBCList_Primal;
//  PyBCList_Primal["DirichletB"] = BCDirichletB;
  PyBCList_Primal["DirichletR"] = BCDirichletR;
  PyBCList_Primal["DirichletL"] = BCDirichletL;
  PyBCList_Primal["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Primal;

  // Define the BoundaryGroups for each boundary condition
//  BCBoundaryGroups_Primal["DirichletB"] = {iB};
  BCBoundaryGroups_Primal["DirichletR"] = {iB};
  BCBoundaryGroups_Primal["DirichletL"] = {iL};
  BCBoundaryGroups_Primal["None"] = {iR,iT};

  //Check the BC dictionary
  BCParamsPrimal::checkInputs(PyBCList_Primal);

  std::vector<int> active_boundaries_primal = BCParamsPrimal::getLGBoundaryGroups(PyBCList_Primal, BCBoundaryGroups_Primal);

  // Sensor BCs
  PyDict BCSensorNeumann;
  BCSensorNeumann[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorNeumann[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 0.0;
  BCSensorNeumann[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCSensorNeumann[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorDirichlet;
  BCSensorDirichlet[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorDirichlet[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCSensorDirichlet[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 0.0;
  BCSensorDirichlet[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorNone;
  BCSensorNone[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.None;

  PyDict PyBCList_Sensor;
  PyBCList_Sensor["SensorNeumann"] = BCSensorNeumann;
  PyBCList_Sensor["SensorDirichlet"] = BCSensorDirichlet;
//  PyBCList_Sensor["SensorNone"] = BCSensorNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Sensor;
  BCBoundaryGroups_Sensor["SensorNeumann"] = {iR,iT};
  BCBoundaryGroups_Sensor["SensorDirichlet"] = {iB,iL};
//    BCBoundaryGroups_Sensor["SensorDirichlet"] = {iB,iR,iT,iL};
//  BCBoundaryGroups_Sensor["SensorNone"] = {iB,iR,iT,iL};

  //Check the BC dictionary
  BCParamsSensor::checkInputs(PyBCList_Sensor);

  std::vector<int> active_boundaries_sensor = BCParamsSensor::getLGBoundaryGroups(PyBCList_Sensor, BCBoundaryGroups_Sensor);


  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol0 = {1e-11, 1e-11};
  std::vector<Real> tol1 = {1e-7, 1e-7};

  //Output functional
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);


  //Adaptation parameters
  int maxIter = 20;
  Real targetCost = 10000;

  std::string adapthist_filename = "tmp/test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
//  MOESSDict[MOESSParams::params.UniformRefinement] = false;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = true;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpError] = true;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2_Block2<PhysD2, TopoD2, NDPDEClass_Primal, NDPDEClass_Sensor, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde_primal, pde_sensor,
                                               order_primal, order_sensor, order_primal+1, order_sensor+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries_primal, active_boundaries_sensor, disc, disc);

  const int quadOrder = -1; //2*(order_primal + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, ResNormType,
                                                      tol0, tol1, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList_Primal, PyBCList_Sensor,
                                                      BCBoundaryGroups_Primal, BCBoundaryGroups_Sensor,
                                                      SolverContinuationDict, outputIntegrand);

  //Set initial solution
  pGlobalSol->setSolution0(solnExact, cellGroups);
//  pGlobalSol->primal0.qfld = SwR;
//  pGlobalSol->primal1.qfld = 0.0;

  //Output h-field
//  std::string hfld_filename = "tmp/hfld_a0.plt";
//  output_Tecplot( get<0>(pGlobalSol->paramfld0), hfld_filename );

  std::string qfld0_init_filename = "tmp/qfld0_init_a0.plt";
  std::string qfld1_init_filename = "tmp/qfld1_init_a0.plt";
  output_Tecplot( pGlobalSol->primal0.qfld, qfld0_init_filename );
  output_Tecplot( pGlobalSol->primal1.qfld, qfld1_init_filename );

  //Solve the sensor equation independently
  pInterface->solveGlobalPrimalProblemIsolated(1);

  pInterface->solveGlobalPrimalProblem();
  std::string qfld0_filename = "tmp/qfld0_a0.plt";
  std::string qfld1_filename = "tmp/qfld1_a0.plt";
  output_Tecplot( pGlobalSol->primal0.qfld, qfld0_filename );
  output_Tecplot( pGlobalSol->primal1.qfld, qfld1_filename );

  pInterface->solveGlobalAdjointProblem();
  std::string adjfld0_filename = "tmp/adjfld0_a0.plt";
  std::string adjfld1_filename = "tmp/adjfld1_a0.plt";
  output_Tecplot( pGlobalSol->adjoint0.qfld, adjfld0_filename );
  output_Tecplot( pGlobalSol->adjoint1.qfld, adjfld1_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<std::endl<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde_primal, pde_sensor,
                                                    order_primal, order_sensor, order_primal+1, order_sensor+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries_primal, active_boundaries_sensor, disc, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, ResNormType,
                                                           tol0, tol1, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList_Primal, PyBCList_Sensor,
                                                           BCBoundaryGroups_Primal, BCBoundaryGroups_Sensor,
                                                           SolverContinuationDict, outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

//    hfld_filename = "tmp/hfld_a" + std::to_string(iter+1) + ".plt";
//    output_Tecplot( get<0>(pGlobalSol->paramfld0), hfld_filename );

    qfld0_init_filename = "tmp/qfld0_init_a" + std::to_string(iter+1) + ".plt";
    qfld1_init_filename = "tmp/qfld1_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal0.qfld, qfld0_init_filename );
    output_Tecplot( pGlobalSol->primal1.qfld, qfld1_init_filename );

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    qfld0_filename = "tmp/qfld0_a" + std::to_string(iter+1) + ".plt";
    qfld1_filename = "tmp/qfld1_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal0.qfld, qfld0_filename );
    output_Tecplot( pGlobalSol->primal1.qfld, qfld1_filename );

    adjfld0_filename = "tmp/adjfld0_a" + std::to_string(iter+1) + ".plt";
    adjfld1_filename = "tmp/adjfld1_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint0.qfld, adjfld0_filename );
    output_Tecplot( pGlobalSol->adjoint1.qfld, adjfld1_filename );
  }

  fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
