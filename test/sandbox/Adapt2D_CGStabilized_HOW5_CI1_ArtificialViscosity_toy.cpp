// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adapt2D_CGStabilized_HOW5_CI1_ArtificialViscosity_toy

//#define SANS_FULLTEST
//#define SANS_VERBOSE
//#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>

#include <boost/filesystem.hpp> // to automagically make the directories

#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Adaptation/MeshAdapter.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/DistanceFunction/DistanceFunction.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Field/Function/FunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Meshing/EPIC/XField_PX.h"
#include "Field/output_grm.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


using namespace std;

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_CGStabilized_HOW5_CI1_ArtificialViscosity_toy )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_CGStabilized_HOW5_CI1_ArtificialViscosity_toy )
{
  typedef QTypePrimitiveRhoPressure QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_PressureGrad<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_totalEnthalpyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_TotalPressure<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_DG ParamBuilderType;
  typedef GenHField_DG<PhysD2, TopoD2> GenHFieldType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                   AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // Used to compute the volume of the domain
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Const> NDScalarConst;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up integrand for Enthalpy
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef OutputEuler2D_totalEnthalpyErrorSquare<NDPDEClass> OutputEntropyErrorSquared;
  typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquared> NDOutputTotalEnthalpyErrorSquared;
  typedef IntegrandCell_Galerkin_Output<NDOutputTotalEnthalpyErrorSquared> IntegrandTotalEnthalpySquareError;
  ////////////////////////////////////////////////////////////////////////////////////////


  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1.0e-9, 1.0e-9};

  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = false;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)

  // reference state (freestream)
  const Real Mach = 4.0;

  const Real rhoRef = 1;                            // density scale

#if 1
  const Real tRef = 1;                              // temperature
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                      // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
#else
  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  // PDE
  GasModel gas(gamma, R);

  const Real inflowTotalEnthalpy = (gas.enthalpy(rhoRef, tRef) + 0.5*qRef*qRef);

  NDScalarConst constFcn(1);

  PyDict Inflow;
  Inflow[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitiveTemperature;
  Inflow[DensityVelocityTemperature2DParams::params.rho] = rhoRef;
  Inflow[DensityVelocityTemperature2DParams::params.u] = uRef;
  Inflow[DensityVelocityTemperature2DParams::params.v] = vRef;
  Inflow[DensityVelocityTemperature2DParams::params.t] = tRef;

  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCBase;
  typedef BCmitAVSensor2D<BCTypeFullState_mitState, BCBase> BCClass;

  // Define inflow BC
  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCInflow[BCClass::ParamsType::params.Characteristic] = false;
  BCInflow[BCClass::ParamsType::params.StateVector] = Inflow;

  // Define wall BC
  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  // Define outflow BC
  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  // Define BC list
  PyDict PyBCList;
  PyBCList["Outflow"] = BCOutflow;
  PyBCList["Wall"] = BCWall;
  PyBCList["Inflow"] = BCInflow;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["Outflow"] = {3,5};
  BCBoundaryGroups["Wall"] = {0,1,2};
  BCBoundaryGroups["Inflow"] = {4};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-6;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] =  true;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type] =
                  SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver]    = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLDecreaseFactor]  = 0.1;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor]  = 2;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  int orderL = 2, orderH = 2;
  int powerL = 2, powerH = 2;

#if 0
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc == 3)
  {
    orderL = orderH = std::stoi(argv[1]);
    powerL = powerH = std::stoi(argv[2]);
  }
#endif

  std::cout << "orderL, orderH = " << orderL << ", " << orderH << std::endl;
  std::cout << "powerL, powerH = " << powerL << ", " << powerH << std::endl;

  //--------ADAPTATION LOOP--------
  for (int orderMax = orderL; orderMax <= orderH; orderMax++)
    for (int power = powerL; power <= powerH; power++ )
    {
      int maxIter = 30;

      int nk = pow(2,power);
      int targetCost = 1000*nk;

#ifdef BOUNDARYOUTPUT
      std::string filename_base = "tmp/HOW5_CI1/P" + std::to_string(orderMax) + "/" + std::to_string(nk) + "k/";
#else
      std::string filename_base = "tmp/HOW5_CI1/P" + std::to_string(orderMax) + "/" + std::to_string(nk) + "k/";
#endif
      boost::filesystem::path base_dir(filename_base);
      if ( not boost::filesystem::exists(base_dir) )
        boost::filesystem::create_directory(base_dir);

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

      NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_residual.dat";

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
      MesherDict[EpicParams::params.CurvedQOrder] = 3; //Mesh order
      MesherDict[EpicParams::params.CSF] = "grids/HOW5_CI1/bowshock.csf"; //Geometry file
      MesherDict[EpicParams::params.GeometryList2D] = "domain"; //Geometry list
      MesherDict[EpicParams::params.minGeom] = 1e-6;
      MesherDict[EpicParams::params.maxGeom] = 0.5;
      MesherDict[EpicParams::params.NodeMovement] = true;
      MesherDict[EpicParams::params.nPointGeom] = -1;
      MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      PyDict ParamDict;

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      // Grid
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld =
          std::make_shared<XField_PX<PhysD2,TopoD2>>(world, "grids/HOW5_CI1/bowshock.q3.gri");
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld_linear =
          std::make_shared<XField_PX<PhysD2,TopoD2>>(world, "grids/HOW5_CI1/bowshock.q1.gri");

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<GenHFieldType> phfld =
          std::make_shared<GenHFieldType>(*pxfld);

      //Solution data
      typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

      std::shared_ptr<SolutionClass> pGlobalSol;
      std::shared_ptr<SolutionClass> pGlobalSolOld;
      std::shared_ptr<SolverInterfaceClass> pInterface;

      std::ofstream resultFile(filename_base+"output.plt", std::ios::out);
      resultFile << "VARIABLES=";
      resultFile << "\"DOF\"";
      resultFile << ", \"1/sqrt(DOF)\"";
      resultFile << ", \"Total Enthalpy L<sup>2</sup> Error\"";
      resultFile << ", \"Total Pressure Error\"";
      resultFile << std::endl;
      resultFile << std::setprecision(16) << std::scientific;


//      for (int order = 0; order <= orderMax; order++)
      int order = orderMax;
      {
        PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
        // Sensor equation terms
        Sensor sensor(pdeEulerAV);
        SensorAdvectiveFlux sensor_adv(0.0, 0.0);
        SensorViscousFlux sensor_visc(order);
        SensorSource sensor_source(order, sensor);
        // AV PDE with sensor equation
        NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order,
                       hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);

        StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby);
        stab.setStabOrder(order);
        stab.setNitscheOrder(order);

#ifndef BOUNDARYOUTPUT
        // Enthalpy error output

        NDOutputClass fcnOutput(pde, inflowTotalEnthalpy);
        OutputIntegrandClass outputIntegrand(pde, fcnOutput, cellGroups, stab);
#else
        // Pressure output
        NDOutputClass fcnOutput(pde, StagPstd, -0.5, 0.);
        IntegrandOutputPressure outputIntegrand( disc, fcnOutput, BCBoundaryGroups["Wall"] );
#endif

        const int quadOrder = 3*(order + 2)-1; // 14 at order=3

        pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pxfld), pde, order, order+1,
                                                     BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                     active_boundaries, ParamDict, stab);

        //Create solver interface
        pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                            cellGroups, interiorTraceGroups,
                                                            PyBCList, BCBoundaryGroups,
                                                            SolverContinuationDict, LinearSolverDict,
                                                            outputIntegrand);

        pInterface->setBaseFilePath( filename_base );

        //Set initial solution
//        if (order == 0)
//        {
//          // initial condition
          ArrayQ q0 = pde.setDOFFrom( AVVariable<DensityVelocityTemperature2D, Real>({{rhoRef, uRef, vRef, tRef}, 0.0}) );

          pGlobalSol->setSolution(q0);
//        }
//        else
//          pGlobalSol->setSolution(*pGlobalSolOld);

        pGlobalSolOld = pGlobalSol;

        output_Tecplot( pGlobalSol->primal.qfld, filename_base + "qfld_init_a0_P"+std::to_string(order)+".plt" );

        pInterface->solveGlobalPrimalProblem();
        output_Tecplot( pGlobalSol->primal.qfld, filename_base + "qfld_a0_P"+std::to_string(order)+".plt" );

        if (order == orderMax)
        {
          Real domainArea = 0;
          for_each_CellGroup<TopoD2>::apply( FunctionIntegral(constFcn, domainArea, world.rank(), cellGroups), *pxfld );

          QuadratureOrder quadratureOrder( *pxfld, -1 );

          NDOutputTotalEnthalpyErrorSquared outEnthalpyError( pde, inflowTotalEnthalpy );
          IntegrandTotalEnthalpySquareError fcnErr( outEnthalpyError, cellGroups );

          Real TotalEnthalpySquareError = 0.0;
          IntegrateCellGroups<TopoD2>::integrate(
              FunctionalCell_Galerkin( fcnErr, TotalEnthalpySquareError ),
              *pxfld, pGlobalSol->primal.qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

          resultFile << " " << pGlobalSol->primal.qfld.nDOF();
          resultFile << " " << 1.0/sqrt(pGlobalSol->primal.qfld.nDOF());
          resultFile << " " << sqrt(TotalEnthalpySquareError/domainArea) / (cRef*cRef);
          resultFile << std::endl;

          // Compute the adjoint
          pInterface->solveGlobalAdjointProblem();
          output_Tecplot( pGlobalSol->adjoint.qfld, filename_base + "adjfld_a0.plt" );
        }
      }


      for (int iter = 0; iter < maxIter+1; iter++)
      {
        std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        //Compute error estimates
        pInterface->computeErrorEstimates();

        //Perform local sampling and adapt mesh
        MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;
        ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew_linear = ptr_pair.first;
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew = ptr_pair.second;

        phfld = std::make_shared<GenHFieldType>(*pxfldNew);

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        //Perform L2 projection from solution on previous mesh

//        for (int order = orderMax; order <= orderMax; order++)
        int order = orderMax;
        {
          PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
          // Sensor equation terms
          Sensor sensor(pdeEulerAV);
          SensorAdvectiveFlux sensor_adv(0.0, 0.0);
          SensorViscousFlux sensor_visc(order);
          SensorSource sensor_source(order, sensor);
          // AV PDE with sensor equation
          NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order,
                         hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);

          StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby);
          stab.setStabOrder(order);
          stab.setNitscheOrder(order);

#ifndef BOUNDARYOUTPUT
          // Enthalpy error output

          NDOutputClass fcnOutput(pde, inflowTotalEnthalpy);
          OutputIntegrandClass outputIntegrand(pde, fcnOutput, cellGroups, stab);
#else
          // Pressure output
          NDOutputClass fcnOutput(pde, StagPstd, -0.5, 0.);
          IntegrandOutputPressure outputIntegrand( disc, fcnOutput, BCBoundaryGroups["Wall"] );
#endif

          const int quadOrder = 3*(order + 2)-1; // 14 at order=3

          pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pxfldNew), pde, order, order+1,
                                                          BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                          active_boundaries, ParamDict, stab);

          // Project/Prolongate the previous solution
          pGlobalSol->setSolution(*pGlobalSolOld);

          QuadratureOrder quadratureOrder( *pxfld, quadOrder );

//          IntegrateCellGroups<TopoD2>::integrate( SetValidStateCell(pde, cellGroups),
//                                                  *pxfldNew, pGlobalSol->primal.qfld,
//                                                  quadratureOrder.cellOrders.data(),
//                                                  quadratureOrder.cellOrders.size() );


          pxfld_linear = pxfldNew_linear;
          pxfld = pxfldNew;
          pGlobalSolOld = pGlobalSol;

          pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                              cellGroups, interiorTraceGroups,
                                                              PyBCList, BCBoundaryGroups,
                                                              SolverContinuationDict, LinearSolverDict,
                                                              outputIntegrand);

          pInterface->setBaseFilePath( filename_base );

          std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1)
                                                         + "_P"+std::to_string(order)+".plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

          pInterface->solveGlobalPrimalProblem();

          std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1)
                                                    + "_P"+std::to_string(order)+".plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

//          if (order == orderMax)
          {
            Real domainArea = 0;
            for_each_CellGroup<TopoD2>::apply( FunctionIntegral(constFcn, domainArea, world.rank(), cellGroups), *pxfld );

            QuadratureOrder quadratureOrder( *pxfld, -1 );

            NDOutputTotalEnthalpyErrorSquared outEnthalpyError( pde, inflowTotalEnthalpy );
            IntegrandTotalEnthalpySquareError fcnErr( outEnthalpyError, cellGroups );

            Real TotalEnthalpySquareError = 0.0;
            IntegrateCellGroups<TopoD2>::integrate(
                FunctionalCell_Galerkin( fcnErr, TotalEnthalpySquareError ),
                *pxfld, pGlobalSol->primal.qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

            resultFile << " " << pGlobalSol->primal.qfld.nDOF();
            resultFile << " " << 1.0/sqrt(pGlobalSol->primal.qfld.nDOF());
            resultFile << " " << sqrt(TotalEnthalpySquareError/domainArea) / (cRef*cRef);
            resultFile << std::endl;

            pInterface->solveGlobalAdjointProblem();

            std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1)+".plt";
            output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
          }
        }
      }

      fadapthist.close();
    }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
