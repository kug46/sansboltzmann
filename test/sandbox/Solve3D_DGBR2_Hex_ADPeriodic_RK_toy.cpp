// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"
#include "pyrite_fstream.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AdvectionDiffusion/ForcingFunction3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
//#include "pde/OutputCell_SolutionErrorSquared.h"
//#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"
#include "Discretization/ResidualNormType.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/Partition/XField_Lagrange.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "NonLinearSolver/NonLinearSolver.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"

#include "unit/UnitGrids/XField3D_Box_Hex_X1.h"

//Instantiate
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

using namespace std;
using namespace SANS;

//#############################################################################
BOOST_AUTO_TEST_SUITE( Solve3D_DGBR2_Hex_ADPeriodic_test_suite )

BOOST_AUTO_TEST_CASE( Solve3D_DGBR2_Hex_ADPeriodic)
{
  typedef PDEAdvectionDiffusion<PhysD3, AdvectiveFlux3D_Uniform,
                                        ViscousFlux3D_Uniform,
                                        Source3D_None> PDEClass;

  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2< NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, XField<PhysD3, TopoD3> > PrimalEquationSetClass;


  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> RKClass;

  typedef ScalarFunction3D_SineSineSineSineUnsteady SolutionClass;
  typedef SolnNDConvertSpace<PhysD3, SolutionClass> SolutionNDClass;

  //Solver Settings

  // Set up NEWTON solver:
  PyDict NonLinearSolverDict, NewtonSolverDict, NewtonLineUpdateDict, LinSolverDict, UMFPACKDict, LineUpdateDict;

  // Set up Newton Solver
#ifdef SANS_PETSC
  // Take the Preconditioners from my Tandem Sphere case and just copy paste them here

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  LinSolverDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  LinSolverDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  LinSolverDict[SLA::PETScSolverParam::params.MaxIterations] = 2500;
  LinSolverDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  LinSolverDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  LinSolverDict[SLA::PETScSolverParam::params.Verbose] = true;
  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  LinSolverDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  std::cout << "Linear solver: PETSc" << std::endl;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  NewtonSolverParam::checkInputs( NewtonSolverDict );
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#else

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  std::cout << "Linear solver: UMFPACK" << std::endl;
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  //AdjSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#endif

  // PDE
  GlobalTime time;
  Real Tperiod, dt;
  int nsteps;

  time = 0.;
  Tperiod = 1;
  dt = 0.1;

  nsteps = int( Tperiod / dt );

  AdvectiveFlux3D_Uniform adv( 0., 0., 1. );
  ViscousFlux3D_Uniform visc(0.005, 0., 0.,
                             0., 0.005, 0.,
                             0., 0., 0.005);
  Source3D_None source;

  NDPDEClass pde( time, adv, visc, source );

  // BCs (none -> periodic)
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCParams::checkInputs(PyBCList);
  const std::vector<int> BoundaryGroups = {};

  // Set up initial field
  PyDict solnArgs;
  GlobalTime temp_time;
  temp_time = 0.25;
  SolutionNDClass solnExact(temp_time);

  // DGBR2 Discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc( 0, viscousEtaParameter );

  int RKorder = 2;
  int order = 1;

  int ii, jj, kk;
  int power = 3;
  ii = pow( 2, power);
  jj = ii;
  kk = ii;

  // global communicator
  mpi::communicator world;

  // Grid
  string fileout = "tmp/Partition";
  int rankk = world.rank();
  fileout += to_string( rankk);
  fileout += "_rank.plt";

  const std::array<bool,3> periodicity = {{true, true, true}};

  XField3D_Box_Hex_X1 xfld( world, fileout, ii, jj, kk, -1, 1, -1, 1, -1, 1, periodicity);
  //string fileout = "tmp/Partition";
   // int rankk = xfld.comm()->rank();
   // fileout += to_string( rankk);
   // fileout += "_rank.plt";
   // XField_Lagrange<PhysD3>::outputPartition(xfld, fileout );

  std::cout << " Finished Construction" << std::endl;
  // Solution: Hierarchical P0 or P1 only*
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld( xfld, order, BasisFunctionCategory_Hierarchical );

  // Initialize
  for_each_CellGroup<TopoD3>::apply( ProjectSolnCell_Discontinuous( solnExact, { 0 } ), (xfld, qfld) );
  //qfld = 100;

  // Lifting operators
  FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld( xfld, order, BasisFunctionCategory_Hierarchical );
  rfld = 0;

  // Boundary Trace: None in effect
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                        BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups ) );
  lgfld = 0;

  // Integration quadrature
  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = { 1e-9, 1e-9 };

  std::vector<int> interiorTraceGroups = linspace(0,xfld.nInteriorTraceGroups()-1);
  std::vector<int> cellGroups = linspace(0,xfld.nCellGroups()-1);

  // Spatial Discretization
  PrimalEquationSetClass PrimalEqSet( xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder, ResidualNorm_Default, tol,
                                      cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, time );

  // Temporal Discretization
  int RKtype = 0;
  int RKstages = RKorder;
  RKClass RK( RKorder, RKstages, RKtype, dt, time, xfld, qfld, NonLinearSolverDict, pde,
              quadratureOrder, { tol[0] }, { 0 }, PrimalEqSet );

  //int printf("TOtal Number of timesteps: %d /n", nsteps);
  // Tecplot Output
  int FieldOrder = order;
  string filename = "tmp/snlDG_3D_Hex_ADPeriodic_P";
  filename += to_string( order );
  filename += "_Q";
  filename += to_string( FieldOrder );
  filename += "_Time_";
  //int f_time = time * 10;
  filename += to_string( 0 );
  filename += "_time.plt";
  output_Tecplot( qfld, filename );

#if 1
  for (int step = 0; step < nsteps; step++)
  {
    std::cout << "Time Step: " << step + 1 << "\n value of time is " << time << std::endl;
    // Advance solution
    RK.march( 1 );
    // Tecplot Output
    int FieldOrder = order;
    string filename = "tmp/snlDG_3D_Hex_ADPeriodic_P";
    filename += to_string( order );
    filename += "_Q";
    filename += to_string( FieldOrder );
    filename += "_Time_";
    //int f_time = time * 10;
    filename += to_string( step + 1 );
    filename += "_time.plt";
    output_Tecplot( qfld, filename );

  }
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
