// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_Galerkin_Stabilized_Poisson_btest
// Testing of the MOESS framework on the advection-diffusion pde

// #define VOLUME_WEIGHTED // Must be here so that includes see the change
// #define WHOLEPATCH
// #define INNERPATCH

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_Solution.h"
#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"


#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#ifdef SANS_AVRO
#include <geometry/builder.h>
#include "Meshing/avro/XField_avro.h"
#endif

// #include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_BackwardsStep_X1.h"
#include "tools/linspace.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_Galerkin_Stabilized_Poisson_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_Poisson_Triangle )
{
  typedef ScalarFunction2D_CornerSingularity  SolutionExact;
  // typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD2,
            AdvectiveFlux2D_Uniform,
            ViscousFlux2D_Uniform,
            Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_Solution<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  // Grid
  Real a = 0.0;
  Real b = 0.0;
  Real nu = 1.0;

  // PDE
  AdvectiveFlux2D_Uniform adv(a,b);

  ViscousFlux2D_Uniform visc( nu, 0., 0., nu );

  Source2D_UniformGrad source(0.0, 0.0, 0.0);

  NDPDEClass pde( adv, visc, source );

  // BC
  PyDict solnArgs;
  solnArgs[SolutionExact::ParamsType::params.alpha] = 2.0/3.0;
  solnArgs[SolutionExact::ParamsType::params.theta0] = PI/2;

  // J(u) = \int u
  const Real domSize = 1.0;
  // from mathematica!
  const Real trueOutput = (9.0/20.0)*(1.0 + pow(2.0,4.0/3.0) );

  solnArgs[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.CornerSingularity;

  PyDict BC_Dirichlet_Soln;
  BC_Dirichlet_Soln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
                    BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;


  PyDict PyBCList;

  PyBCList["BC_Dirichlet_Soln"] = BC_Dirichlet_Soln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["BC_Dirichlet_Soln"] = {
                                            XField2D_BackwardsStep_X1::iBottomInlet,
                                            XField2D_BackwardsStep_X1::iStep,
                                            XField2D_BackwardsStep_X1::iBottomOutlet,
                                            XField2D_BackwardsStep_X1::iOutlet,
                                            XField2D_BackwardsStep_X1::iTop,
                                            XField2D_BackwardsStep_X1::iInlet
                                          };

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // discretization

  StabilizationMatrix stab(StabilizationType::Unstabilized);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-11, 1e-11};

  NDOutputClass fcnOutput;

  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 ) std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 3;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0.0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;
#elif defined(INTEL_MKL)
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::string mesher = "avro";
  std::string file_tag = "";
  bool dumpField = true;
  int maxIter = 20;

  const bool DGCONV = false; // use DG DOF counts. Converts to matching DG for elements

  int orderL = 1, orderH = 3;
  int powerL = 0, powerH = 8;

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    mesher = std::string(argv[1]);
  if (argc >= 3)
    file_tag = std::string(argv[2]);
  if (argc >= 4)
    dumpField = std::stoi(argv[3]);
  if (argc >= 5)
    orderL = orderH = std::stoi(argv[4]);
  if (argc >= 6)
    powerL = powerH = std::stoi(argv[5]);
  if (argc >= 7)
    maxIter = std::stoi(argv[6]);

  std::cout << "mesher: " << mesher << ", file_tag: " << file_tag << ", dumpField: " << dumpField;
  std::cout << ", orderL,H = " << orderL << ", " << orderH << ", powerL,H = " << powerL << ", " << powerH << "maxIter: " << maxIter << std::endl;
#endif

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;

#define CHAIN_ADAPT 1  // Whether to use the lower DOF mesh as the initial condition for the next dof target

  //--------ADAPTATION LOOP--------
  for (int order = orderL; order <= orderH; order++)
  {
    timer totalTime;
    stab.setStabOrder(order);
    stab.setNitscheOrder(order);
    // stab.setNitscheConstant(10*2.0);

#if CHAIN_ADAPT

#ifdef SANS_AVRO
    std::shared_ptr<avro::Context> context;
#endif
    PyDict MesherDict;
    if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic)
    {
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;

      pxfld = std::make_shared<XField2D_BackwardsStep_X1>( world, 0, domSize, domSize, domSize );
    }
#ifdef SANS_AVRO
    else if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro)
    {
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;

      MesherDict[avroParams::params.Curved] = false; // is the grid curved?
      MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
      MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files

      using avro::coord_t;
      using avro::index_t;

      // create the context
      context = std::make_shared<avro::Context>();


      // setup the EGADS nodes
      avro::real x0[3] = {-1, 0,0};
      avro::real x1[3] = { 0, 0,0};
      avro::real x2[3] = { 0,-1,0};
      avro::real x3[3] = { 1,-1,0};
      avro::real x4[3] = { 1, 1,0};
      avro::real x5[3] = {-1, 1,0};
      std::vector<avro::real*> X = {x0,x1,x2,x3,x4,x5};

      std::vector<avro::EGADSNode> nodes;
      for (index_t k=0;k<X.size();k++)
        nodes.emplace_back( avro::EGADSNode(context.get(),X[k]) );

      // make the edges
      // very important! boundary conditions need to be specified in this order
      // you can use EngSketchPad's vGeom on the flatplate.egads file below
      // to visualize the order of the edges in the EGADS model
      std::vector<avro::EGADSEdge> edges;
      for (index_t k=0;k<nodes.size();k++)
      {
        avro::EGADSNode* n0 = &nodes[k];
        avro::EGADSNode* n1;
        if (k<nodes.size()-1)
          n1 = &nodes[k+1];
        else
          n1 = &nodes[0];
        edges.emplace_back( avro::EGADSEdge(context.get(),*n0,*n1) );
      }

      avro::EGADSEdgeLoop loop(CLOSED);
      for (index_t k=0;k<edges.size();k++)
        loop.add( edges[k] , SFORWARD );
      loop.make(context.get());

      std::shared_ptr<avro::Body> body = std::make_shared<avro::EGADSWireBody>(context.get(),loop);
      body->buildHierarchy();

      std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(),"flatplate");
      model->addBody(body,false); // false = body is not interior
      model->finalize();

      // What to do here?
      /*

        // setup the initial mesh
        index_t nx = 4;
        index_t ny = 5;
        avro::library::CubeMesh mesh( {1,1} , {nx,ny} );

        // transform the vertices
        for (index_t k=0;k<mesh.vertices().nb();k++)
        {
          avro::real x = mesh.vertices()[k][0];
          avro::real y = mesh.vertices()[k][1];

          mesh.vertices()[k][0] = (x3[0] -x0[0])*x +x0[0];
          mesh.vertices()[k][1] = (x5[1] -x0[1])*y +x0[1];
        }

      */


      XField2D_BackwardsStep_X1 xfld0( world, 0, domSize, domSize, domSize );
      // copy the mesh into the domain and attach the geometry
      pxfld = std::make_shared< XField_avro<PhysD2,TopoD2> >(xfld0, model);
    }
#endif
    else if (mesher == "fefloa")
    {
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

      pxfld = std::make_shared<XField2D_BackwardsStep_X1>( world, 0, domSize, domSize, domSize );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown mesher");
#endif

    for (int power = powerL; power <= powerH; power++)
    {
      Real nk = pow(2,power);
      Real targetCost = 125*nk;

      // to make sure folders have a consistent number of zero digits
      const int string_pad = 6;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
      std::string filename_base = "tmp/Poisson/CG_" + mesher + "_";

      if (file_tag.size() > 0 && file_tag != "_")
        filename_base += file_tag + "_"; // the additional file name bits kept especially

      filename_base += int_pad + "_P" + std::to_string(order) + "/";

      std::string adapthist_filename = filename_base + "test.adapthist";

      boost::filesystem::create_directories(filename_base);

      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

      // down convert to match the element number for DG
      if (DGCONV)
      {
        Real nDOFperCell_DG = (order+1)*(order+2)/2;

        Real nDOFperCell_CG = nDOFperCell_DG;
        nDOFperCell_CG -= (3 - 1./2); // the node dofs are shared by 6
        nDOFperCell_CG -= (3 - 3./2)*std::max(0,(order-1)); // if there are edge dofs they are shared by 2

        targetCost *= nDOFperCell_CG/nDOFperCell_DG;
      }

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;
      MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;
      MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;
      MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;

#if not(CHAIN_ADAPT)
#ifdef SANS_AVRO
      std::shared_ptr<avro::Context> context;
#endif
      PyDict MesherDict;
      if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic)
      {
        MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
        // MesherDict[EpicParams::params.nThread] = 4; // DO NOT USE THIS ON HYPERSONIC

        MesherDict[EpicParams::params.nThread] = 1;

        // pxfld = std::make_shared<XField_libMeshb<PhysD2, TopoD2>>( world, init_grid_path );
        pxfld = std::make_shared<XField2D_BackwardsStep_X1>( world, 0, domSize, domSize, domSize );
      }
#ifdef SANS_AVRO
      else if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro)
      {
        MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;

        MesherDict[avroParams::params.WriteMesh] = true; // write all the .mesh files
        MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files

        using avro::index_t;

        // create the context
        context = std::make_shared<avro::Context>();

        // set up the egads nodes
        avro::real x0[3] = {-1,0,0};
        avro::real x1[3] = { 0, 0,0}; // the re-entrant corner
        avro::real x2[3] = { 0,-1,0};
        avro::real x3[3] = { 1,-1,0};
        avro::real x4[3] = { 1, 1,0};
        avro::real x5[3] = {-1,1,0};
        std::vector<avro::real*> X = {x0,x1,x2,x3,x4,x5};

        std::vector<avro::EGADSNode> nodes;
        for (index_t k=0;k<X.size();k++)
          nodes.emplace_back( avro::EGADSNode(context.get(),X[k]) );

        // make the edges
        // very important! boundary conditions need to be specified in this order
        // you can use EngSketchPad's vGeom on the flatplate.egads file below
        // to visualize the order of the edges in the EGADS model
        std::vector<avro::EGADSEdge> edges;
        for (index_t k=0;k<nodes.size();k++)
        {
          avro::EGADSNode* n0 = &nodes[k];
          avro::EGADSNode* n1;
          if (k<nodes.size()-1)
            n1 = &nodes[k+1];
          else
            n1 = &nodes[0];
          edges.emplace_back( avro::EGADSEdge(context.get(),*n0,*n1) );
        }

        // close the geometry
        avro::EGADSEdgeLoop loop(CLOSED);
        for (index_t k=0;k<edges.size();k++)
          loop.add( edges[k] , SFORWARD );
        loop.make(context.get());

        // build the body from the set of edges
        std::shared_ptr<avro::Body> pbody = std::make_shared<avro::EGADSWireBody>(context.get(),loop);
        pbody->buildHierarchy();

        // make the model in the context with a name
        std::shared_ptr<avro::Model> model = std::make_shared<avro::Model> ( context.get(), "Re-entrant Corner" );
        model->addBody(pbody,false); // add the body above, false = it is not an interior
        model->finalize(); // clean everything up

        // save the model
        EG_saveModel(model->object(),"tmp/corner.egads");


        // construct the mesh that goes along with the above model
        XField2D_BackwardsStep_X1 xfld0( world, 0, domSize, domSize, domSize );

        // copy the mesh into the domain and attach the mesh to the model
        pxfld = std::make_shared< XField_avro<PhysD2,TopoD2> >(xfld0, model);
      }
#endif
      else if (mesher == "fefloa")
      {
        MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

        // uniform initial
        // pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
        // Initial cross clustering
        pxfld = std::make_shared<XField2D_BackwardsStep_X1>( world, 0, domSize, domSize, domSize );
      }
      else
        SANS_DEVELOPER_EXCEPTION("Unknown mesher");
#endif

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.hasTrueOutput] = true;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TrueOutput] = trueOutput;


      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                   active_boundaries, stab);

      const int quadOrder = 2*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrand);

      pGlobalSol->setSolution(0.0);

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      if (dumpField)
      {
        std::string qfld_filename = filename_base + "qfld_a0.plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename, {"u"} );

        std::string adjfld_filename = filename_base + "adjfld_a0.plt";
        output_Tecplot( pInterface->getAdjField(), adjfld_filename, {"w"} );
      }

      //Compute error estimates
      pInterface->computeErrorEstimates();
      std::string efld_filename = filename_base + "efld_a" + std::to_string(0) + ".plt";

      pInterface->output_EField(efld_filename);

      for (int iter = 0; iter < maxIter+1; iter++)
      {
        if ( world.rank() == 0 )
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        //Perform local sampling and adapt mesh
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
        pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

        if ( world.rank() == 0 )
          std::cout << "adapted!" << std::endl;

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                        BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                        active_boundaries, stab);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, AdjLinearSolverDict,
                                                               outputIntegrand);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld = pxfldNew;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();

        if (dumpField)
        {
          std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename, {"u"} );

          std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
          output_Tecplot( pInterface->getAdjField(), adjfld_filename, {"w"} );
        }
        else if (iter == maxIter)
        {
          std::string qfld_filename = filename_base + "qfld_final.plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename, {"u"} );

          std::string adjfld_filename = filename_base + "adjfld_final.plt";
          output_Tecplot( pInterface->getAdjField(), adjfld_filename, {"w"} );
        }

        //Compute error estimates
        pInterface->computeErrorEstimates();
        // std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".plt";
      }
      if (world.rank() == 0)
        fadapthist << "\n\nTotal Time elapsed: " << totalTime.elapsed() << "s" << std::endl;

      fadapthist.close();
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
