// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_Galerkin_Stabilized_Helmholtz_btest
// Testing of the MOESS framework on the advection-diffusion pde

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/OutputCell_SolutionSquared.h"
//#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Adaptation/MeshAdapter.h"
#include "Meshing/EPIC/XField_PX.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "Field/output_grm.h"


#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_Galerkin_Stabilized_Helmholtz_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_Helmholtz_Triangle )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Grid
  int ii = 2;
  int jj = ii;

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_UnionJack_Triangle_X1>( ii, jj );

  Real a = 0.0;
  Real b = 0.0;
  Real nu = 1;

  // PDE
  AdvectiveFlux2D_Uniform adv(a,b);

  ViscousFlux2D_Uniform visc( nu, 0., 0., nu );

  Real wave_num = 3;
  Source2D_UniformGrad source(pow(wave_num,2), 0.0, 0.0);

  NDPDEClass pde( adv, visc, source );

  // BC
#if 1
//  PyDict BCSoln_Bottom;
//  BCSoln_Bottom[BCParams::params.BC.BCType] = BCParams::params.BC.Natural;
//  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCTypeNatural>::params.gB] = 0;
  PyDict BCSoln_Bottom;
  BCSoln_Bottom[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.A] = 0;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.B] = 1;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.bcdata] = 0;

  PyDict BCSoln_Right;
  BCSoln_Right[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.A] = 1;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.B] = 0;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.bcdata] = 1;

//  PyDict BCSoln_Top;
//  BCSoln_Top[BCParams::params.BC.BCType] = BCParams::params.BC.Natural;
//  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCTypeNatural>::params.gB] = 0;
  PyDict BCSoln_Top;
  BCSoln_Top[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.A] = 0;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.B] = 1;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.bcdata] = 0;

  PyDict BCSoln_Left;
  BCSoln_Left[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.A] = 1;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.B] = 0;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.bcdata] = 0;
#elif 1
  PyDict BCSoln_Bottom;
  BCSoln_Bottom[BCParams::params.BC.BCType] = BCParams::params.BC.Natural;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCTypeNatural>::params.gB] = 0;
//  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.A] = 0;
//  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.B] = 1;
//  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.bcdata] = 0;

  PyDict BCSoln_Right;
  BCSoln_Right[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.A] = 1;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.B] = 0;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.bcdata] = 1;

  PyDict BCSoln_Top;
  BCSoln_Top[BCParams::params.BC.BCType] = BCParams::params.BC.Natural;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCTypeNatural>::params.gB] = 0;

//  BCSoln_Top[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
//  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.A] = 0;
//  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.B] = 1;
//  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.bcdata] = 0;

  PyDict BCSoln_Left;
  BCSoln_Left[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.A] = 1;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.B] = 0;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCTypeLinearRobin_mitLG>::params.bcdata] = 0;

#endif

  PyDict PyBCList;
  PyBCList["BC_Bottom"] = BCSoln_Bottom;
  PyBCList["BC_Right"] = BCSoln_Right;
  PyBCList["BC_Top"] = BCSoln_Top;
  PyBCList["BC_Left"] = BCSoln_Left;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BC_Bottom"] = {0};
  BCBoundaryGroups["BC_Right"] = {1};
  BCBoundaryGroups["BC_Top"] = {2};
  BCBoundaryGroups["BC_Left"] = {3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-11, 1e-11};

  //Output functional
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

//#define XFIELD_IN
//#define XFIELD_OUT

  //--------ADAPTATION LOOP--------
  for (int order = 2; order <= 2; order++)
  {
    stab.setStabOrder(order);
    stab.setNitscheOrder(order);
#ifdef XFIELD_IN
    mpi::communicator world;
    std::string xfld_filename = "tmp/Helmholtz/DG_P" + std::to_string(order) + "/xfld_a0.grm";
    pxfld = std::make_shared<XField_PX<PhysD2,TopoD2>>(world,xfld_filename);
    const Real targetCost = 1000;
#else
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_UnionJack_Triangle_X1>( ii, jj );

    Real targetCost= 400;
#endif

    const int maxIter = 30;

#ifdef XFIELD_IN
    std::string filename_base = "tmp/Helmholtz/CG_DG_P" + std::to_string(order) + "/";
#else
    std::string filename_base = "tmp/Helmholtz/CG_P" + std::to_string(order) + "/";
#endif
    std::string adapthist_filename = filename_base + "test.adapthist";
    fstream fadapthist( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

    PyDict MOESSDict;
    MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
    MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
    MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
    MOESSDict[MOESSParams::params.UniformRefinement] = false;

    PyDict MesherDict;
    MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;

    PyDict AdaptDict;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.hasTrueOutput] = true;
    //  u(x) = (e^kx - e^(-kx))/(e^k - e^(-k)) - \int u
    //  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TrueOutput] = std::tanh(wave_num/2)/wave_num;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TrueOutput] = // u(x) = (e^kx - e^(-kx))/(e^k - e^(-k)) - \int u^2
        (std::cosh(wave_num)/std::sinh(wave_num)  - wave_num*(1/pow(std::sinh(wave_num),2)))/(wave_num*2);

    MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

    MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Solution data
    std::shared_ptr<SolutionClass> pGlobalSol;
    pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                 BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                 active_boundaries, stab);

    const int quadOrder = 2*(order + 1);

    //Create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, LinearSolverDict,
                                                        outputIntegrand);

    std::vector<std::pair<int,int>> trueCost = {};

    pGlobalSol->setSolution(0.0);

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    std::string qfld_filename = filename_base + "qfld_a0.plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a0.plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

    trueCost.push_back(std::make_pair(0,pGlobalSol->primal.qfld.nDOF()));


#ifdef XFIELD_OUT
    std::string xfld_filename = filename_base + "xfld_a0.grm";
    output_grm( *pxfld, xfld_filename);
#endif

    for (int iter = 0; iter < maxIter+1; iter++)
    {
      std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

      //Compute error estimates
      pInterface->computeErrorEstimates();

      //Perform local sampling and adapt mesh
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;

#ifdef XFIELD_IN
      xfld_filename = "tmp/Helmholtz/DG_P" + std::to_string(order) + "/xfld_a" + std::to_string(iter+1) + ".grm";
      pxfldNew = std::make_shared<XField_PX<PhysD2,TopoD2>>(world,xfld_filename);
#else
      pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);
#endif

      interiorTraceGroups.clear();
      for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<SolutionClass> pGlobalSolNew;
      pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                      BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                      active_boundaries, stab);

      //Perform L2 projection from solution on previous mesh
      pGlobalSolNew->setSolution(*pGlobalSol);

      std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
      pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                             cellGroups, interiorTraceGroups,
                                                             PyBCList, BCBoundaryGroups,
                                                             SolverContinuationDict, LinearSolverDict,
                                                             outputIntegrand);

      //Update pointers to the newest problem (this deletes the previous mesh and solutions)
      pxfld = pxfldNew;
      pGlobalSol = pGlobalSolNew;
      pInterface = pInterfaceNew;

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
#ifdef XFIELD_OUT
      std::string xfld_filename = filename_base + "xfld_a" + std::to_string(iter+1) + ".grm";
      output_grm( *pxfld, xfld_filename);
#endif
    }
//    std::cout<< "trueCost" <<std::endl;
//    for (auto it = trueCost.begin(); it != trueCost.end(); ++it)
//      std::cout<< "( " << (*it).first << ", " << (*it).second << " )" << std::endl;

    fadapthist.close();
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
