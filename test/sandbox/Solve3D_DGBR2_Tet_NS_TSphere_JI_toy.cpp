// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve3D_DGBR2_Tet_Navier_Stokes_Cascadeem_TBlade_toy.cpp

#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "NonLinearSolver/NonLinearSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DConservative.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/OutputEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "tools/linspace.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "Field/Partition/XField_Lagrange.cpp"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

#include "Meshing/EPIC/XField_PX.h"
#include "Meshing/gmsh/XField_gmsh.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"
#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h" // Added for Transient

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

//Instantiate
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"
using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE (Solve3D_DGBR2_Tet_NavierStokes_Cascadeem_TBlade_test_suite)

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGBR2_Tet_Cascadeem_TBlade )
{

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD3, TopoD3>> PrimalEquationSetClass;
  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> RKClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  //typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  //typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  mpi::communicator world;

  std::vector<Real> tol = { 1e-7, 1e-7 };

  // gas model
  const Real gamma = 1.4;
  const Real R = 1;        // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R / (gamma - 1);
  const Real Cp = gamma * Cv;

  // reference state (freestream)
  const Real Mach = 0.2;
  const Real Reynolds = 80000;
  const Real Prandtl = 0.72;
  const Real lRef = 1;          // Need to look up in write up                    // length scale
  const Real rhoRef = 1.0;                          // density scale
  const Real aoaRef = 0.5707227;                            // angle of attack (radians)
  const Real tRef = 1.0;                         // pressure
  const Real pRef = rhoRef * R * tRef;

  const Real cRef = sqrt( gamma * R * tRef );
  const Real qRef = cRef * Mach;                              // velocity scale

  const Real uRef = qRef * cos( aoaRef );             // velocity
  const Real vRef = qRef * sin( aoaRef );
  const Real wRef = 0;

  const Real muRef = rhoRef * qRef * lRef / Reynolds;

  // PDE
  GasModel gas( gamma, R );
  ViscosityModelType visc( muRef );
  ThermalConductivityModel tcond( Prandtl, Cp );
  NDPDEClass pde( gas, visc, tcond, Euler_ResidInterp_Raw );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc( 0, viscousEtaParameter );



  // BC Set up

  typedef BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD3, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  NDBCClass bc( pde );
  IntegrandBCClass fcnBC( pde, bc, { 0 }, disc );

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict StateVector;
  StateVector[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  StateVector[Conservative3DParams::params.rho] = rhoRef;
  StateVector[Conservative3DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative3DParams::params.rhov] = rhoRef*vRef;
  StateVector[Conservative3DParams::params.rhow] = rhoRef*wRef;

  const Real ERef = Cv * tRef + 0.5 * (uRef * uRef + vRef * vRef);

  StateVector[Conservative3DParams::params.rhoE] = rhoRef*ERef;

  PyDict BCFarField;
  BCFarField[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.StateVector] = StateVector;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.Characteristic] = true;

  PyDict BCPressureOut;
  BCPressureOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCPressureOut[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;

  PyDict PyBCList;
  PyBCList["BCWall"] = BCNoSlip;
  PyBCList["BCFarField"] = BCFarField;
  PyBCList["BCPressureOut"] = BCPressureOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["BCWall"] = {9};//{ 0,2};        // These group assignments are dependent on what Karthik's numbering ends up being
  BCBoundaryGroups["BCFarField"] = {2};//{ 1 };
  BCBoundaryGroups["BCPressureOut"] = {6};

  BCParams::checkInputs (PyBCList);

  GlobalTime time;
  Real Tperiod, dt;
  int nsteps;

  time = 0.;
  Real tchar = lRef/qRef;
  dt = 0.001*tchar;
  Tperiod = 201*tchar;
  nsteps = int( Tperiod / dt );
  
  // Set up NEWTON solver:
  PyDict NonLinearSolverDict, NewtonSolverDict, NewtonLineUpdateDict, LinSolverDict, UMFPACKDict,LineUpdateDict;


#ifdef SANS_PETSC
  // Set up Newton Solver

  PyDict PreconditionerDict;
  //PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ILU;
  //PreconditionerDict[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  //PreconditionerDict[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.Natural;

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  LinSolverDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-8;
  LinSolverDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  LinSolverDict[SLA::PETScSolverParam::params.MaxIterations] = 2500;
  LinSolverDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  LinSolverDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  LinSolverDict[SLA::PETScSolverParam::params.Verbose] = true;
  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  //LinSolverDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "tmp/PETSc_residualhist.dat";
  //LinSolverDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;
  std::cout << "Linear solver: PETSc" << std::endl;


  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  //LineUpdateDict[HalvingSearchLineUpdateParam::params.MinStepSize] = 1e-11;
  //LineUpdateDict[HalvingSearchLineUpdateParam::params.Verbose] = true;
  //LineUpdateDict[HalvingSearchLineUpdateParam::params.DumpLineSearchField] = false;
#else

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

    NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
    NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
    NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 250;
    NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
    NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
#endif

#if 1
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 500;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;
#endif
  NewtonSolverParam::checkInputs(NewtonSolverDict);
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  //NonLinearSolverParam::checkInputs (NonLinearSolverDict);



  //int ordermin = 1;
  //int ordermax = 1;
  int order = 2;
  //for (int order = ordermin; order <= ordermax; order++)
  //{
  //  int powermin = 2;
  //  int powermax = 2;

  //  for (int power = powermin; power <= powermax; power++)
  //  {

      // Make Partition
      int RKorder = 4;
      string partout = "tmp/Partition_CascadeSphe_";
      partout += to_string(world.rank());
      partout += ".txt";

      // Read in Mesh
      //string filein = "grids/test1.grm";
      std::string filein = "grids/E_baseline.msh"; //"grids/CascadeemTBlades_Tet_P1.grm";
      //XField_PX<PhysD3, TopoD3> xfld( world, filein );

      XField_gmsh<PhysD3, TopoD3> xfld( world, filein );
      XField_Lagrange<PhysD3>::outputPartition(xfld, partout );


      //std::shared_ptr<Field_DG_Cell<PhysD3, TopoD3, ArrayQ>>
      //  qfld( new Field_DG_Cell<PhysD3, TopoD3, ArrayQ>(xfld, order, BasisFunctionCategory_Lagrange) );
      Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld( xfld, order, BasisFunctionCategory_Lagrange );
      FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld( xfld, order, BasisFunctionCategory_Lagrange );
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Lagrange,
          BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups ) );
      lgfld = 0;
      // Set Initial Conditions

      ArrayQ q0;
      pde.setDOFFrom( q0, DensityVelocityPressure3D<Real>(rhoRef, uRef, vRef, wRef, pRef) );

      qfld = q0;
      for (int i=0; i<rfld.nDOF(); i++) rfld.DOF(i) = 0.0;
      lgfld = 0;

      // Solving System

      std::vector<int> CellGroups =
      {0};
      std::vector<int> ITraceGroups = linspace( 0, xfld.nInteriorTraceGroups() - 1);

      //std::cout<<"# Interior Trace Groups: "<<xfld.nInteriorTraceGroups()<<std::endl;
      //std::cout<<"# Groups: "<<group<<std::endl;

      QuadratureOrder quadratureOrder( xfld, 8);


#if 0
  //// THIS WILL Read in THE RESTART FILE
  string restart = "tmp/Restart_CascadeSphe_";
  restart += to_string( world.rank());
  restart += ".txt";
  int nDOF;
  //FILE* fp = fopen( restart.c_str(), "w" );
  ifstream fp;
  fp.open(restart);
  fp >> nDOF;
  BOOST_REQUIRE_EQUAL(nDOF,qfld.nDOF());
  for ( int i = 0; i < qfld.nDOF(); i++)
    for ( int n = 0; n < 5; n++)
      fp >> qfld.DOF(i)[n];

  fp.close();
#endif
      // 0-5 interior faces to integrate on (6 - number of boundary group)
      // { 0 } is the cell groups (value = 1. therefore, 0)
      PrimalEquationSetClass PrimalEqSet( xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                          ResidualNorm_Default, tol, {0}, ITraceGroups, PyBCList, BCBoundaryGroups, time );
#if 0
      {
        NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

        SystemVectorClass ini(PrimalEqSet.vectorStateSize());
        SystemVectorClass sln(PrimalEqSet.vectorStateSize());
        SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
        SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
        rsd = 0;

        PrimalEqSet.fillSystemVector(ini);
        sln = ini;

        Solver.solve(ini,sln);

        PrimalEqSet.setSolutionField(sln);
      }
#endif

      // RK set up

      int RKtype = 0;
      int RKstages = RKorder;
      RKClass RK( RKorder, RKstages, RKtype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, { tol[0] }, { 0 }, PrimalEqSet );
      //LinSolverDict
      //NonLinearSolverDict
      // Tecplot Output
      //int FieldOrder = order;
      string filename = "tmp/CascadeemTBlade_TEST";
      //filename += to_string( order );
      //filename += "_Q";
      //filename += "_Time_";
      //int f_time = time * 10;
      //filename += to_string( 0 );
      filename += "_time.plt";

#if 1
      for (int step = 0; step < nsteps; step++)
      {
        std::cout << "Time Step: " << step + 1 << "\n value of time is " << time << std::endl;
        // Advance solution
        RK.march( 1 );
        // Tecplot Output
        int FieldOrder = order;
        string filename = "tmp/Cascadeem_TBlade";
        filename += to_string( order );
        filename += "_Q";
        filename += to_string( FieldOrder );
        filename += "_Time_";
        //int f_time = time * 10;
        filename += to_string( step + 1 );
        filename += ".plt";
        output_Tecplot( qfld, filename );


        //// THIS WILL DUMP THE RESTART FILE AFTER ALL TIME STEPS
        string restart = "tmp/Restart_CascadeSphe_";
        restart += to_string( world.rank());
        restart += ".txt";

        //FILE* fp = fopen( restart.c_str(), "w" );
        ofstream fp;
        fp.open(restart);
        fp << std::scientific << std::setprecision(16);
        fp << qfld.nDOF();
        fp << std::endl;
        for ( int i = 0; i < qfld.nDOF(); i++)
        {
          for ( int n = 0; n < 5; n++)
            fp << qfld.DOF(i)[n] << " ";
          fp << std::endl;
        }

       fp.close();


      }
#endif

}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
