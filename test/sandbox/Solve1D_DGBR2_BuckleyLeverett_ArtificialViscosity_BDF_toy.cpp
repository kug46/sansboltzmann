// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve1D_DGBR2_BuckleyLeverett_ArtificialViscosity_BDF_btest
// Testing of the MOESS framework on a time-marching BUckley-Leverett problem

//#define USE_PETSC_SOLVER

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"
#include "tools/timer.h"

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverettArtificialViscosity.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"
//#include "pde/PorousMedia/Sensor_BuckleyLeverett.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Meshing/XField1D/XField1D.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#include "Field/output_Tecplot_PDE.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_BuckleyLeverett_ArtificialViscosity_BDF_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_BuckleyLeverett_ArtificialViscosity_BDF )
{
  typedef QTypePrimitive_Sw QType;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelBuckleyLeverett<QType, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEBL_ExtendedState;
  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeBuckleyLeverettArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeBuckleyLeverettArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelAV> PDEClass;

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QType, PDEBL_ExtendedState> ExactSolutionClass;
  typedef SolnNDConvertSpace<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCBuckleyLeverett_ArtificialViscosity1DVector<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef ParamType_GenH_DG ParamBuilderType;
  typedef GenHField_DG<PhysD1, TopoD1> GenHFieldType;

  typedef SolutionData_DGBR2<PhysD1, TopoD1, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;
//  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, ParamFieldType> BDFClass;

  mpi::communicator world;

  GlobalTime time(0);

  timer clock;

  int Nx = 100;
  int Nt = 250;
  const Real L = 50.0;
  const Real T = 25.0;

  int order = 1;
  int BDForder = 2;
  const Real dt = T/Nt;

  bool exportSolutions = true;
  std::string filename_base = "tmp/";

  // Grid
  typedef XField1D XFieldClass;
  XFieldClass xfld( Nx, 0, L );

  const int iL = 0;
  const int iR = 1;

  // Primal PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 0.0;
  CapillaryModel cap_model(pc_max);

  //Create original BL PDE with extended state vector
  PDEBL_ExtendedState pdeBL(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  //Create artificial viscosity PDE
  bool hasSpaceTimeDiffusionBL = false;
  PDEBaseClass pde_BLAV(order, hasSpaceTimeDiffusionBL, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  SensorAdvectiveFlux sensor_adv(0.0);
  bool hasSpaceTimeDiffusionAV = false;
  const Real diffusionConstantAV = 3.0;
  SensorViscousFlux sensor_visc(order, hasSpaceTimeDiffusionAV, 1.0, diffusionConstantAV);
  const Real Sk_min = -3;
  const Real Sk_max = -1;
  SensorSource sensor_source(pde_BLAV, Sk_min, Sk_max);

  // AV PDE with sensor equation
  bool isSteadyAV = true;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteadyAV,
                 order, hasSpaceTimeDiffusionBL, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 1.0;
  const Real SwR = 0.1;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(pdeBL, SwL, SwR, xinit);

  const Real SwInit = SwR;
  const Real nuInit = 1.0;
  ArrayQ qInit = {SwInit, nuInit};

  // Primal PDE BCs
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitState,
                                BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>> BCParamsAV_Dirichlet_mitState;

  // Create a BC dictionary
  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletL[BCParamsAV_Dirichlet_mitState::params.qB] = SwL;
  BCDirichletL[BCParamsAV_Dirichlet_mitState::params.Cdiff] = diffusionConstantAV;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletR[BCParamsAV_Dirichlet_mitState::params.qB] = SwR;
  BCDirichletR[BCParamsAV_Dirichlet_mitState::params.Cdiff] = diffusionConstantAV;
//  BCDirichletR[BCParamsAV_Dirichlet_mitState::params.isOutflow] = true;

  PyDict PyBCList;
  PyBCList["DirichletR"] = BCDirichletR;
  PyBCList["DirichletL"] = BCDirichletL;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletL"] = {iL};
  BCBoundaryGroups["DirichletR"] = {iR};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  std::vector<Real> tol = {1e-10, 1e-10};

  //Output functional
//  NDOutputClass fcnOutput(source_production, rhow, rhon, krw, krn, muw, mun, K, pc, false, false);
//  NDOutputClass fcnOutput(source_production, rhow, rhon, krw, krn, muw, mun, pc, false, false);
//  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LineUpdateDict;

#if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+1); //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  if (world.rank() == 0) std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.25;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  // Check inputs
  NonLinearSolverParam::checkInputs(NonlinearSolverDict);

  const int quadOrder = 2*(order + 2);

  //Print case summary
  if (world.rank() == 0)
  {
    std::cout << "Case parameters" << std::endl;
    std::cout << "---------------" << std::endl;
    std::cout << "p-order: " << order << std::endl;
    std::cout << "BDF-order: " << BDForder << std::endl;
    std::cout << "Processors: " << world.size() << std::endl;
    std::cout << "pc_max: " << pc_max << std::endl;
    std::cout << "hasSpaceTimeDiffusionBL: " << hasSpaceTimeDiffusionBL << std::endl;
    std::cout << "hasSpaceTimeDiffusionAV: " << hasSpaceTimeDiffusionAV << std::endl;
    std::cout << "diffusionConstantAV: " << diffusionConstantAV << std::endl;
    std::cout << "Sk_min, Sk_max: " << Sk_min << ", " << Sk_max << std::endl;
    std::cout << "isSteadyAV: " << isSteadyAV << std::endl;
    std::cout << "viscousEta: " << viscousEtaParameter << std::endl;
    std::cout << "Residual tol: " << tol[0] << ", " << tol[1] << std::endl;
    std::cout << "Quadrature order: " << quadOrder << std::endl;
    std::cout << "maxResGrowthFactor: " << LineUpdateDict.get(HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor) << std::endl;
#if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
    std::cout << "ILU fill level: " << PreconditionerILU.get(SLA::PreconditionerILUParam::params.FillLevel) << std::endl;
    std::cout << "ILU ordering: " << PreconditionerILU.get(SLA::PreconditionerILUParam::params.Ordering) << std::endl;
    std::cout << "GMRES restart: " << PETScDict.get(SLA::PETScSolverParam::params.GMRES_Restart) << std::endl;
    std::cout << "PETSc RelativeTolerance: " << PETScDict.get(SLA::PETScSolverParam::params.RelativeTolerance) << std::endl;
    std::cout << "PETSc MaxIterations: " << PETScDict.get(SLA::PETScSolverParam::params.MaxIterations) << std::endl;
#endif
    std::cout << "---------------" << std::endl << std::endl;
  }

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Compute generalized log H-tensor field
  GenHFieldType Hfld(xfld);

  //Solution data
  SolutionClass sol((Hfld, xfld), pde, order, 0,
                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre, active_boundaries, disc);

  //Create a field to store the lifted quantity required for shock-capturing
  sol.createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

  //Set initial condition
  sol.setSolution(solnExact, {0});
//  sol.setSolution(qInit);

  QuadratureOrder quadratureOrder( xfld, quadOrder );

  // Create AlgebraicEquationSets
  PrimalEquationSetClass AlgEqSetSpace(sol.paramfld, sol.primal, sol.pliftedQuantityfld, pde, disc,
                                       quadratureOrder, ResidualNorm_Default, tol,
                                       cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, time);

  // The BDF class
  BDFClass BDF( BDForder, dt, time, sol.paramfld, sol.primal.qfld, NonlinearSolverDict, pde, quadratureOrder, cellGroups, AlgEqSetSpace );

#if 0 //Eigenvector dump
  Field_DG_Cell<PhysD2,TopoD2,ArrayQ> Vfld(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2,TopoD2,ArrayQ> VTfld(xfld, order, BasisFunctionCategory_Hierarchical);
  std::fstream fVfld("tmp/Vmin.txt", std::fstream::in);
  std::fstream fVTfld("tmp/VTmin.txt", std::fstream::in);
  for (int i = 0; i < Vfld.nDOF(); i++)
    for (int j = 0; j < ArrayQ::M; j++)
    {
      fVfld >> Vfld.DOF(i)[j];
      fVTfld >> VTfld.DOF(i)[j];
    }
  fVfld.close();
  fVTfld.close();

  output_Tecplot( Vfld, "tmp/Vfld.plt" );
  output_Tecplot( VTfld, "tmp/VTfld.plt" );
#endif

  std::vector<Real> output_hist(Nt+1, 0.0);

  // Set IC
  time = 0.0;

#if 0
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  // residual
  SystemVectorClass q(AlgEqSetSpace.vectorStateSize());
  AlgEqSetSpace.fillSystemVector(q);

  SystemVectorClass rsd(AlgEqSetSpace.vectorEqSize());
  rsd = 0;
  AlgEqSetSpace.residual(rsd);

  for (int i = 0; i < sol.primal.qfld.nDOF(); i++)
  {
    std::cout << "i: " << i << ", " << rsd(0)[i] << std::endl;
  }

  std::cout << std::setprecision(5) << std::scientific << AlgEqSetSpace.residualNorm(rsd) << std::endl;

#if 0
  // jacobian nonzero pattern
  SystemNonZeroPattern nz(AlgEqSetSpace.matrixSize());
  AlgEqSetSpace.jacobian(q, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  jac = 0;
  AlgEqSetSpace.jacobian(q, jac);


  fstream fout( "tmp/jac.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif
#endif

  for (int step = 0; step < Nt; step++)
  {
    if (world.rank() == 0)
      std::cout << "Step: " << step << ", Time = " << time << std::endl;

    if (exportSolutions && (step % 10 == 0))
    {
      // Tecplot dump grid
      string filename = filename_base + "timeDGBR2_BLAV_P";
      filename += to_string(order);
      filename += "_n";
      filename += to_string(step);
      filename += ".plt";
//      output_Tecplot(pde, sol.paramfld, sol.primal.qfld, sol.primal.rfld, filename, {"Sw", "nu"});
      output_Tecplot( sol.primal.qfld, filename, {"Sw", "nu"} );

      string filename_liftedquantity = filename_base + "liftedfld_n" + to_string(step) + ".plt";
      output_Tecplot( *sol.pliftedQuantityfld, filename_liftedquantity );
    }

    // March the solution in time
    BDF.march(1);

#if 0
    //Compute output
    IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2( outputIntegrand, output_hist[step+1] ),
                                            xfld, (sol.primal.qfld, sol.primal.rfld),
                                            quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#ifdef SANS_MPI
    output_hist[step+1] = boost::mpi::all_reduce( *get<-1>(xfld).comm(), output_hist[step+1], std::plus<Real>() );
#endif
#endif
  }

  if (world.rank() == 0)
    std::cout << "Time = " << time << std::endl;

  if (exportSolutions)
  {
    // Tecplot dump grid
    string filename = filename_base + "timeDGBR2_BLAV_P";
    filename += to_string(order);
    filename += "_n";
    filename += to_string(Nt);
    filename += ".plt";
//    output_Tecplot(pde, sol.paramfld, sol.primal.qfld, sol.primal.rfld, filename, {"Sw", "nu"});
    output_Tecplot( sol.primal.qfld, filename, {"Sw", "nu"} );

    string filename_liftedquantity = filename_base + "liftedfld_n" + to_string(Nt) + ".plt";
    output_Tecplot( *sol.pliftedQuantityfld, filename_liftedquantity );
  }

  if (world.rank() == 0)
  {
    Real runtime = clock.elapsed();
    std::cout<<"Time elapsed: " << std::fixed << std::setprecision(3) << runtime << "s" << std::endl;
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
