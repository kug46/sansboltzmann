// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_TwoPhase_ST_Symmetric_btest
// Testing of the MOESS framework on a space-time two-phase problem

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/PDETwoPhase1D.h"
#include "pde/PorousMedia/BCTwoPhase1D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/SolutionFunction_TwoPhase1D.h"
#include "pde/PorousMedia/OutputTwoPhase.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//#define INITIAL_MESH_GRM

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_TwoPhase_ST_Symmetric_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_TwoPhase_ST_Symmetric_Triangle )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef QTypePrimitive_pnSw QType;
  typedef Q1D<QType, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, Real,
                              CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTwoPhase1DVector<PDEClass> BCVector;
  typedef BCTwoPhase1D<BCTypeFullState, PDEClass> BCClassFullState;
  typedef BCTwoPhase1D<BCTypeFunction_mitState, PDEClass> BCClassSolution;

  typedef BCParameters<BCVector> BCParams;

  typedef SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, TraitsSizeTwoPhase> SolutionType;
  typedef SolnNDConvertSpaceTime<PhysD1,SolutionType> NDSolutionType;

//  typedef OutputTwoPhase_SaturationPower<PDEClass> OutputClass;
  typedef OutputTwoPhase1D_FixedWellPressure<QInterpreter, DensityModel, DensityModel,
                                             RelPermModel, RelPermModel, ViscModel, ViscModel, CapillaryModel> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  const Real L = 2000.0; //ft
  const Real T = 1000.0; //days
  const Real Ls = 10.0;  //ft - well size

  // Grid
#ifdef INITIAL_MESH_GRM
  ReadMesh<PhysD2, TopoD2, Grm> grm_reader("tmp/SpacetimeMesh_22x25_2000ft_1000d.grm");
//  ReadMesh<PhysD2, TopoD2, Grm> grm_reader("tmp/SpacetimeMesh_bidiag_40x20_2000ft_1000d.grm");
  XField<PhysD2, TopoD2> xfld(grm_reader);
#else
  int ii = 22;
  int jj = 25;
//  XField2D_Box_Triangle_X1 xfld(ii, jj, 0, L, 0, T, true);
//  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, L, 0, T );

  Real dx = L/(ii-2);
  std::vector<Real> dxvec(ii, dx);
  dxvec[ii/2 - 2] = dx - Ls/2.0;
  dxvec[ii/2 - 1] = Ls/2.0;
  dxvec[ii/2    ] = Ls/2.0;
  dxvec[ii/2 + 1] = dx - Ls/2.0;

  std::vector<Real> xvec(ii+1, 0);
  std::vector<Real> yvec(jj+1, 0);

  for (int i = 1; i < ii+1; i++)
    xvec[i] = xvec[i-1] + dxvec[i-1];

  for (int j = 0; j < jj+1; j++)
    yvec[j] = T*j/Real(jj);

//  std::vector<Real> xvec = {0, 500, 995, 1000, 1005, 1500, 2000};
//  std::vector<Real> yvec = {0, 500, 1000};

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_Box_Triangle_X1(xvec, yvec, true) );
#endif

  int order = 1;

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(5);
  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;

  // Set up source term PyDicts
  PyDict well_fixedpressure;
  well_fixedpressure[SourceTwoPhase1DType_FixedWellPressure_Param::params.pB] = 2350.0;
  well_fixedpressure[SourceTwoPhase1DParam::params.Source.SourceType] = SourceTwoPhase1DParam::params.Source.FixedPressure;

  PyDict source_well;
  source_well[SourceTwoPhase1DParam::params.Source] = well_fixedpressure;
  source_well[SourceTwoPhase1DParam::params.xmin] = 0.5*(L - Ls);
  source_well[SourceTwoPhase1DParam::params.xmax] = 0.5*(L + Ls);
  source_well[SourceTwoPhase1DParam::params.Tmin] = 0;
  source_well[SourceTwoPhase1DParam::params.Tmax] = T;
  source_well[SourceTwoPhase1DParam::params.smoothLx] = 5.0;
  source_well[SourceTwoPhase1DParam::params.smoothT] = 0.0;

  PyDict source_list;
  source_list["production_well"] = source_well;

  NDPDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list);

  // Initial solution
  ArrayQ qinit;
  PressureNonWet_SaturationWet<Real> qdata(2400.0, 0.5);
  pde.setDOFFrom( qinit, qdata );

  PyDict initSoln;
  initSoln[BCClassSolution::ParamsType::params.Function.Name] = BCClassSolution::ParamsType::params.Function.ThreeConstSaturation;
  initSoln[SolutionType::ParamsType::params.pn] = 2500.0;
  initSoln[SolutionType::ParamsType::params.Sw_left] = 1.0;
  initSoln[SolutionType::ParamsType::params.Sw_mid] = 0.1;
  initSoln[SolutionType::ParamsType::params.Sw_right] = 1.0;
  initSoln[SolutionType::ParamsType::params.xLeftMid] = 0.25*L;
  initSoln[SolutionType::ParamsType::params.xMidRight] = 0.75*L;
  NDSolutionType NDinitSolution(initSoln, pde.variableInterpreter());

  // BCs
  PyDict BCState;
  BCState[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  BCState[PressureNonWet_SaturationWet_Params::params.pn] = 2500.0;
  BCState[PressureNonWet_SaturationWet_Params::params.Sw] = 1.0;

  PyDict BCBottom;
  BCBottom[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCBottom[BCClassSolution::ParamsType::params.Function] = initSoln;

  PyDict BCLeft;
  BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCLeft[BCClassFullState::ParamsType::params.StateVector] = BCState;

  PyDict BCRight;
  BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCRight[BCClassFullState::ParamsType::params.StateVector] = BCState;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCBottom"] = BCBottom;
  PyBCList["BCLeft"] = BCLeft;
  PyBCList["BCRight"] = BCRight;
  PyBCList["BCNone"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
#ifdef INITIAL_MESH_GRM
  BCBoundaryGroups["BCLeft"] = {0};
  BCBoundaryGroups["BCBottom"] = {1};
  BCBoundaryGroups["BCRight"] = {2};
  BCBoundaryGroups["BCNone"] = {3};
#else
  BCBoundaryGroups["BCBottom"] = {0};
  BCBoundaryGroups["BCRight"] = {1};
  BCBoundaryGroups["BCNone"] = {2};
  BCBoundaryGroups["BCLeft"] = {3};
#endif

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-7, 1e-7};

  //Output functional
//  NDOutputClass fcnOutput(pde, 0, 2);
  NDOutputClass fcnOutput(source_well, rhow, rhon, krw, krn, muw, mun, K, pc);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.None;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);


  //--------ADAPTATION LOOP--------

  int maxIter = 20;
  Real targetCost = 10000;

  std::string filename_base = "tmp/";
  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  const int quadOrder = -1; //2*(order + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrand);

  //Set initial solution
  pGlobalSol->setSolution(NDinitSolution, cellGroups);

  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename = filename_base + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

  std::string adjfld_filename = filename_base + "adjfld_a0.plt";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
  }

  fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
