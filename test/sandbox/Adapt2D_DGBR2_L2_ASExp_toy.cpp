// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_L2_CornerSingularity_btest
// Testing of the MOESS framework on the corner singularity test-case found in Masa's thesis

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include <iostream>

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_Solution.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionError.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "Field/output_Tecplot.h"
#include "Field/output_grm.h"

using namespace std;

//#define TEST_MODE

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_L2_ASExp_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_L2_ASExp_Triangle )
{

  typedef ScalarFunction2D_ASExp SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

//  typedef OutputCell_Solution<PDEClass>                      OutputClass;
//  typedef OutputCell_SolutionSquared<PDEClass>                      OutputClass;
//  typedef OutputCell_SolutionError<PDEClass, SolutionExact> OutputClass;
  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  // for dumping alternative output and error
  typedef OutputCell_Solution<PDEClass>                      OutputSolutionClass;
  typedef OutputNDConvertSpace<PhysD2,OutputSolutionClass>   NDOutputSolutionClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputSolutionClass>  OutputIntegrandSolutionClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputL2ErrorClass;
  typedef OutputNDConvertSpace<PhysD2,OutputL2ErrorClass>   NDOutputL2ErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputL2ErrorClass>  OutputIntegrandL2ErrorClass;

  typedef OutputCell_SolutionError<PDEClass, SolutionExact> OutputL1ErrorClass;
  typedef OutputNDConvertSpace<PhysD2,OutputL1ErrorClass>   NDOutputL1ErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputL1ErrorClass>  OutputIntegrandL1ErrorClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  // Grid
  int ii = 6;
  int jj = ii;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_UnionJack_Triangle_X1>( ii, jj );

  std::vector<int> global_btracegroup_list = {0,1,2,3};

  int order = 1;

  // PDE
  AdvectiveFlux2D_Uniform adv( 0, 0 );

  Real nu = 0;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(1.0, 0, 0);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[SolutionExact::ParamsType::params.a] = 2;
  solnArgs[SolutionExact::ParamsType::params.b] = 1;
  solnArgs[SolutionExact::ParamsType::params.alpha] = 5;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCNone"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNone"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-12, 1e-12};

  NDOutputClass fcnOutput(solnExact);
  // NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  //Set up pyrite file
  pyrite_file_stream pyriteFile("IO/Adaptation/Adaptation2D_CornerSingularity_ErrorEstimate.txt", 1e-10, 1e-10, pyrite_file_stream::check);
  pyriteFile << std::setprecision(16) << std::scientific;

  //--------ADAPTATION LOOP--------

  int maxIter = 20;
  Real targetCost = 3200;

  // to make sure folders have a consistent number of zero digits
  const int string_pad = 6;
  std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
  std::string filename_base = "tmp/L2/ASExp/DG_" + int_pad + "_P" + std::to_string(order) + "/";
  boost::filesystem::create_directories(filename_base);

  // File name for the saving off of the last XField
  std::string xfld_filename = filename_base + "xfld_final_err.grm";

  std::string outputhist_filename = filename_base + "output.adapthist";
  fstream foutputhist( outputhist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + outputhist_filename);

  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
//  MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;


  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  const int quadOrder = 2*(order + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrand);

  pGlobalSol->setSolution(0.0);

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  // Compute additional output functionals
  QuadratureOrder quadratureOrder = pInterface->getQuadOrder();

  NDOutputSolutionClass outputSlnFcn;
  OutputIntegrandSolutionClass integrandOutputSlnFcn(outputSlnFcn,{0});
  NDOutputL2ErrorClass outputL2ErrorFcn(solnExact);
  OutputIntegrandL2ErrorClass integrandOutputL2ErrorFcn(outputL2ErrorFcn,{0});
  NDOutputL1ErrorClass outputL1ErrorFcn(solnExact);
  OutputIntegrandL1ErrorClass integrandOutputL1ErrorFcn(outputL1ErrorFcn,{0});

  // global and local error
  Real globalSln=0, globalL2Error = 0, globalL1Error = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_DGBR2( integrandOutputSlnFcn, globalSln ),
      *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld),
      quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_DGBR2( integrandOutputL2ErrorFcn, globalL2Error ),
      *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld),
      quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_DGBR2( integrandOutputL1ErrorFcn, globalL1Error ),
      *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld),
      quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  foutputhist << std::setw(5)  << "Iter"
              << std::setw(5)  << "P"
              << std::setw(10) << "DOF"
              << std::setw(20) << "Output"
              << std::setw(20) << "Error"
              << std::setw(20) << "L2 Error"
              << std::setw(20) << "L1 Error"
              << std::endl;

  Real error = (11- 1./exp(5./2))/25 - globalSln;
  foutputhist << std::setw(5) << 0
              << std::setw(5) << order
              << std::setw(10) << pGlobalSol->primal.qfld.nDOF()
              << std::setw(20) << std::setprecision(10) << std::scientific << globalSln
              << std::setw(20) << std::setprecision(10) << std::scientific << fabs(error)
              << std::setw(20) << std::setprecision(10) << std::scientific << fabs(globalL2Error)
              << std::setw(20) << std::setprecision(10) << std::scientific << fabs(globalL1Error)
              << std::endl;

  std::string qfld_filename = filename_base + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

  std::string adjfld_filename = filename_base + "adjfld_a0.plt";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    // global and local error
    Real globalSln=0, globalL2Error = 0, globalL1Error = 0;
    IntegrateCellGroups<TopoD2>::integrate(
        FunctionalCell_DGBR2( integrandOutputSlnFcn, globalSln ),
        *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld),
        quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

    IntegrateCellGroups<TopoD2>::integrate(
        FunctionalCell_DGBR2( integrandOutputL2ErrorFcn, globalL2Error ),
        *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld),
        quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

    IntegrateCellGroups<TopoD2>::integrate(
        FunctionalCell_DGBR2( integrandOutputL1ErrorFcn, globalL1Error ),
        *pxfld, (pGlobalSol->primal.qfld,pGlobalSol->primal.rfld),
        quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

    Real error = (11- 1./exp(5./2))/25 - globalSln;
    foutputhist << std::setw(5) << iter+1
                << std::setw(5) << order
                << std::setw(10) << pGlobalSol->primal.qfld.nDOF()
                << std::setw(20) << std::setprecision(10) << std::scientific << globalSln
                << std::setw(20) << std::setprecision(10) << std::scientific << fabs(error)
                << std::setw(20) << std::setprecision(10) << std::scientific << fabs(globalL2Error)
                << std::setw(20) << std::setprecision(10) << std::scientific << fabs(globalL1Error)
                << std::endl;

    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
  }

  output_grm( *pxfld, xfld_filename);

  foutputhist.close();
  fadapthist.close();



}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
