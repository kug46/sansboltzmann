// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve1D_DGBR2_TwoPhaseLinearized_BDF_btest
// Testing on a 1D linearized two-phase problem

#define LINEARIZED 1
#define PERIODIC 0
#define LINEAR_ADVECTION 0

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"
#include "tools/timer.h"

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#if !LINEAR_ADVECTION
#include "pde/PorousMedia/PDETwoPhaseLinearized1D.h"
#include "pde/PorousMedia/BCTwoPhaseLinearized1D.h"

#include "pde/PorousMedia/PDETwoPhase1D.h"
#include "pde/PorousMedia/BCTwoPhase1D.h"
#else
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#endif

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField1D_1Group_Periodic.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_TwoPhaseLinearized_BDF_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_TwoPhaseLinearized_BDF_Line )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef QTypePrimitive_pnSw QType;

#if LINEARIZED
  typedef PDETwoPhaseLinearized1D<QType, RelPermModel, RelPermModel, CapillaryModel> PDEClass;

  typedef BCTwoPhaseLinearized1DVector<PDEClass> BCVector;
  typedef BCTwoPhaseLinearized1D<BCTypeFullState, PDEClass> BCClassFullState;
#elif LINEAR_ADVECTION
  typedef AdvectiveFlux1D_Uniform AdvectionModel;
  typedef ViscousFlux1D_Uniform DiffusionModel;
  typedef Source1D_UniformGrad SourceModel;

  typedef PDEAdvectionDiffusion<PhysD1, AdvectionModel, DiffusionModel, SourceModel > PDEClass;
  typedef BCAdvectionDiffusion1DVector<AdvectionModel, DiffusionModel> BCVector;
  typedef BCAdvectionDiffusionParams<PhysD1, BCTypeDirichlet_mitStateParam> BCClassParams;

#else
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef ViscosityModel_Constant ViscModel;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, Real,
                              CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef BCTwoPhase1DVector<PDEClass> BCVector;
  typedef BCTwoPhase1D<BCTypeFullState, PDEClass> BCClassFullState;
#endif

  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

//  typedef BCParameters<BCVector> BCParams;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> BDFClass;
  typedef BDFClass::AlgebraicEquationSet_TimeClass UnsteadyPrimalAlgEqnSetClass;

  GlobalTime time(0);

  timer clock;

  const Real Lx = 100.0; //ft
  const Real T = 10.0; //days

  const int Nx = 100;
  const int Nt = 10;

  //BDF parameters
  Real dt = T/Nt;
  int BDForder = 1;

  bool exportSolutions = true;
  std::string filename_base = "tmp/";

  // Grid
#if PERIODIC
  XField1D_1Group_Periodic xfld(Nx, 0, Lx);
#else
  XField1D xfld(Nx, 0, Lx);
#endif

  int order = 1;

  // PDE
  Real rhow = 62.4;
  Real rhon = 52.1;
  Real phi = 0.3;

  RelPermModel krw(2);
  RelPermModel krn(2);

  Real muw = 1, mun = 2;

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200; //mD

  CapillaryModel pc(0.0);

  Real pxbar = -0.25;
  Real Swbar = 0.2;

  ArrayQ qInit = 0;

#if !LINEAR_ADVECTION

#if LINEARIZED
  NDPDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, pxbar, Swbar);

  PyDict BCStateL;
  BCStateL[TwoPhaseVariableTypeParams::params.StateVector.Variables] =
           TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  BCStateL[PressureNonWet_SaturationWet_Params::params.pn] = 0.0;
  BCStateL[PressureNonWet_SaturationWet_Params::params.Sw] = 1.0;

  PyDict BCStateR;
  BCStateR[TwoPhaseVariableTypeParams::params.StateVector.Variables] =
           TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  BCStateR[PressureNonWet_SaturationWet_Params::params.pn] = 0.0;
  BCStateR[PressureNonWet_SaturationWet_Params::params.Sw] = 0.0;

  PyDict BCLeft;
  BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCLeft[BCClassFullState::ParamsType::params.StateVector] = BCStateL;

  PyDict BCRight;
  BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCRight[BCClassFullState::ParamsType::params.StateVector] = BCStateR;

#else
  const Real pref = 14.7;

  int comp_switch = 0;
  DensityModel rhow_model(rhow, comp_switch*5.0e-6, pref);
  DensityModel rhon_model(rhon, comp_switch*1.5e-5, pref);

  PorosityModel phi_model(phi, comp_switch*3.0e-6, pref);

  ViscModel muw_model(muw);
  ViscModel mun_model(mun);

  NDPDEClass pde(rhow_model, rhon_model, phi_model, krw, krn, muw_model, mun_model, K, pc);

  PyDict BCStateL;
  BCStateL[TwoPhaseVariableTypeParams::params.StateVector.Variables] =
           TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  BCStateL[PressureNonWet_SaturationWet_Params::params.pn] = 2500.0;
  BCStateL[PressureNonWet_SaturationWet_Params::params.Sw] = Swbar;

  PyDict BCStateR;
  BCStateR[TwoPhaseVariableTypeParams::params.StateVector.Variables] =
           TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  BCStateR[PressureNonWet_SaturationWet_Params::params.pn] = 2500.0 + pxbar*Lx;
  BCStateR[PressureNonWet_SaturationWet_Params::params.Sw] = Swbar;

  PyDict BCLeft;
  BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCLeft[BCClassFullState::ParamsType::params.StateVector] = BCStateL;

  PyDict BCRight;
  BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCRight[BCClassFullState::ParamsType::params.StateVector] = BCStateR;
//  BCRight[BCClassFullState::ParamsType::params.isOutflow] = true;

  // Initial solution
  Real pwInit = 2500.0;
  Real SwInit = Swbar;

  PressureNonWet_SaturationWet<Real> qdata(pwInit, SwInit);
  pde.setDOFFrom( qInit, qdata );
#endif

#else
  Real Lw = pow(Swbar, 2.0) / muw;
  Real Ln = pow(1.0 - Swbar, 2.0) / mun;
  Real Lw_S = 2.0*Swbar / muw;
  Real Ln_S = -2.0*(1.0 - Swbar) / mun;

  Real V = -(Lw_S*Ln - Lw*Ln_S)/(phi*(Lw + Ln)) * K * pxbar;
  Real nu = 0.0;
  std::cout << "V: " << V << std::endl;

  AdvectionModel advection(V);
  DiffusionModel diffusion(nu);
  SourceModel source(0.0,0.0);

  NDPDEClass pde( advection, diffusion, source );

  ArrayQ qInit = 0;

  PyDict BCLeft;
  BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCLeft[BCClassParams::params.qB] = 1.0; //Swbar;

  PyDict BCRight;
  BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCRight[BCClassParams::params.qB] = 0.0;
#endif

  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

#if !PERIODIC
  PyBCList["BCLeft"] = BCLeft;
  PyBCList["BCRight"] = BCRight;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCLeft"] = {0};
  BCBoundaryGroups["BCRight"] = {1};
#endif

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  std::vector<Real> tol = {1e-9, 1e-10};

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinSolverDict, LineUpdateDict;

#if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ILU;
  PreconditionerDict[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerDict[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.Natural;

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//  LinSolverDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  LinSolverDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  LinSolverDict[SLA::PETScSolverParam::params.MaxIterations] = 2500;
  LinSolverDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  LinSolverDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  LinSolverDict[SLA::PETScSolverParam::params.Verbose] = true;
  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  LinSolverDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = filename_base + "PETSc_residualhist.dat";
  LinSolverDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;
  std::cout << "Linear solver: PETSc" << std::endl;
#else
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  // Check inputs
  NonLinearSolverParam::checkInputs(NonlinearSolverDict);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2<PhysD1, TopoD1, NDPDEClass, ParamType_None> SolutionClass;

  SolutionClass sol(xfld, pde, order, 1,
                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre, active_boundaries, disc);


  //Set initial condition
//  qInit = 0;
  sol.setSolution(qInit);

//  for (int elem = 0; elem < 10; elem++)
//    sol.primal.qfld.DOF(elem)[1] = 1.0;

//  for (int elem = 20; elem < 40; elem++)
//  {
//    sol.primal.qfld.DOF(2*elem  )[1] = 1.0;
//    sol.primal.qfld.DOF(2*elem+1)[1] = 0.0;
//  }

//  std::fstream fqfld("tmp/qfld_sol_pxbar0_25_Swbar0_5_P0_N100.txt", std::fstream::in);
//  for (int i = 0; i < sol.primal.qfld.nDOF(); i++)
//    for (int j = 0; j < ArrayQ::M; j++)
//      fqfld >> sol.primal.qfld.DOF(i)[j];
//  fqfld.close();

#if 0 //Eigenvector dump
  Field_DG_Cell<PhysD1,TopoD1,ArrayQ> Vfld(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD1,TopoD1,ArrayQ> VTfld(xfld, order, BasisFunctionCategory_Hierarchical);
  std::fstream fVfld("tmp/V_unstable.txt", std::fstream::in);
  std::fstream fVTfld("tmp/VT_unstable.txt", std::fstream::in);
  for (int i = 0; i < Vfld.nDOF(); i++)
    for (int j = 0; j < ArrayQ::M; j++)
    {
      fVfld >> Vfld.DOF(i)[j];
      fVTfld >> VTfld.DOF(i)[j];
    }
  fVfld.close();
  fVTfld.close();

  output_Tecplot( Vfld, "tmp/Vfld.plt" );
  output_Tecplot( VTfld, "tmp/VTfld.plt" );
#endif

  QuadratureOrder quadratureOrder( xfld, 2*(order + 2) );

  // Create AlgebraicEquationSets
  PrimalEquationSetClass AlgEqSetSpace(xfld, sol.primal.qfld, sol.primal.rfld, sol.primal.lgfld,
                                       pde, disc, quadratureOrder, ResidualNorm_Default, tol,
                                       cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, time);

  // The BDF class
  BDFClass BDF( BDForder, dt, time, xfld, sol.primal.qfld, NonlinearSolverDict, pde, quadratureOrder, cellGroups, AlgEqSetSpace );

  std::vector<Real> output_hist(Nt+1, 0.0);

  // Set IC
  time = 0.0;

  for (int step = 0; step < Nt; step++)
  {
    std::cout << "Step: " << step << ", Time = " << time << std::endl;

    if (exportSolutions && (step % 1 == 0))
    {
      // Tecplot dump grid
      string filename = filename_base + "timeDGBR2_TwophaseQuarterFive_P";
      filename += to_string(order);
      filename += "_n";
      filename += to_string(step);
      filename += ".plt";
      output_Tecplot( sol.primal.qfld, filename );
    }

    // March the solution in time
    BDF.march(1);
  }

  BDF.march(1);

  std::cout << "Time = " << time << std::endl;

  if (exportSolutions)
  {
    // Tecplot dump grid
    string filename = filename_base + "timeDGBR2_TwophaseQuarterFive_P";
    filename += to_string(order);
    filename += "_n";
    filename += to_string(Nt);
    filename += ".plt";
    output_Tecplot( sol.primal.qfld, filename );
  }

//  std::fstream fqfld("tmp/qfld_sol.txt", std::fstream::out);
//  fqfld << std::scientific << std::setprecision(15);
//  for (int i = 0; i < sol.primal.qfld.nDOF(); i++)
//    for (int j = 0; j < ArrayQ::M; j++)
//      fqfld << sol.primal.qfld.DOF(i)[j] << std::endl;
//  fqfld.close();

#if 1
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  // residual
  SystemVectorClass q(AlgEqSetSpace.vectorStateSize());
  AlgEqSetSpace.fillSystemVector(q);

  SystemVectorClass rsd(AlgEqSetSpace.vectorStateSize());
  rsd = 0;
  AlgEqSetSpace.residual(q, rsd);
  std::cout << std::setprecision(5) << std::scientific << AlgEqSetSpace.residualNorm(rsd) << std::endl;

  // jacobian nonzero pattern
  SystemNonZeroPattern nz(AlgEqSetSpace.matrixSize());
  AlgEqSetSpace.jacobian(q, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  jac = 0;
  AlgEqSetSpace.jacobian(q, jac);

#if 1
  fstream fout( "tmp/jac.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

  UnsteadyPrimalAlgEqnSetClass& AlgEqSetUnsteady = BDF.getUnsteadyAlgEqnSet();

  SystemNonZeroPattern nz_unsteady(AlgEqSetUnsteady.matrixSize());
  AlgEqSetUnsteady.jacobian(q, nz_unsteady);

  SystemMatrixClass jac_unsteady(nz_unsteady);
  jac_unsteady = 0;
  AlgEqSetUnsteady.jacobian(q, jac_unsteady);

#if 1
  fstream fout2( "tmp/jac_unsteady.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac_unsteady, fout2 );
#endif

#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
