// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_CG_Triangle_Euler_CubicSource_btest
// testing of 2-D DG Advective for Euler on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
//#include "LinearAlgebra/SparseLinAlg/PETSc/PETScSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

//#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_CG_Triangle_Euler_CubicSource_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_CG_Triangle_Bump10 )
{
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_Galerkin_Stabilized< NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1.0;
  const Real tRef = 1.0;          // temperature
  const Real pRef = 1.0;                  // pressure

  const Real Mach = 0.5;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;

//  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
//  const Real qRef = 0.1;                            // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  //const Real cRef = qRef/Mach;                      // speed of sound
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Momentum);
  int nSol = NDPDEClass::N;

  // BC
  // Create a BC dictionary
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

//  PyDict BCIn;
//  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt;
//
//  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.sSpec] = 0.0;
//  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.HSpec] = 3.505;
//  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.qtSpec] = 0;
//
//  PyDict BCOut;
//  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure;
//  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure>::params.pSpec] = 1.0;

  PyDict BCIn;

  // Entropy, Stagnation Enthalpy, Tangential velocity
  const Real sSpec = log( pRef / pow(rhoRef,gamma) );
  const Real HSpec = gas.enthalpy(rhoRef, tRef) + 0.5*(uRef*uRef+vRef*vRef);

  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sSpec;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HSpec;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aoaRef;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {2,3};
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {0};


  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  //typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  // Set up Newton Solver

  PyDict NewtonSolverDict, NewtonLineUpdateDict, AdjSolverDict, LinSolverDict;
  PyDict HalvingSearchDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
#else
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
#endif
//  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = true;

  AdjSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
//  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  StabilizationMatrix stab(StabilizationType::Adjoint, TauType::Glasby);

#if 1

  typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputEntropyErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquaredClass> NDOutputEntropyErrorSquaredClass;
#if 0
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputEntropyErrorSquaredClass, NDPDEClass> IntegrandSquareErrorClass;

  Real sExact = 0.0;
  NDOutputEntropyErrorSquaredClass outEntropyError( pde, sExact );
  IntegrandSquareErrorClass fcnErr( pde, outEntropyError, {0}, stab );
#else

  typedef IntegrandCell_Galerkin_Output<NDOutputEntropyErrorSquaredClass> IntegrandSquareErrorClass;

  Real sExact = 1.0;
  NDOutputEntropyErrorSquaredClass outEntropyError( pde, sExact );
  IntegrandSquareErrorClass fcnErr( outEntropyError, {0} );
#endif


#else
  // DRAG INTEGRAND
  typedef BCEuler2D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> IntegrandBCClass;

  NDBCClass bc(pde);

  const std::vector<int> WallGroups = {0,2};
  IntegrandBCClass fcnBC( pde, bc, WallGroups );

  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> IntegrandOutputClass;

  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass outputIntegrand( outputFcn, WallGroups );

#endif



  int ordermin = 1;
  int ordermax = 3;
  for (int order = ordermin; order <= ordermax; order++)
  {
    stab.setStabOrder(order);
    stab.setNitscheOrder(order);
    //indx = 0;

    // loop over grid resolution: 2^power
//    int ii, jj;

    int powermin = 0;
    int powermax = 5;

    for (int power = powermin; power <= powermax; power++)
    {

//      jj = pow( 2, power );
//      ii = 3*jj; //small grid!
//      Real tau = 0.1; //10% bump
//
//      // grid: 2D Cubic Source Bump
//      XField2D_CubicSourceBump_Xq xfld( ii, jj, tau, 3 );

      string filein = "grids/SmoothBump/SmoothBump_tri_ref";
      filein += to_string(power);
      filein += "_Q4.grm";

      XField_PX<PhysD2, TopoD2> xfld(world, filein);

      // DG solution field
      //solution at inlet is (1, 0.1, 0, 1);
      ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

      std::shared_ptr<Field_CG_Cell<PhysD2, TopoD2, ArrayQ>>
        qfld( new Field_CG_Cell<PhysD2, TopoD2, ArrayQ>(xfld, order, BasisFunctionCategory_Hierarchical) );

//      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Hierarchical);

      // Set the initial condition
      *qfld = q0;

      const int nDOFPDE = qfld->nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      lgfld = 0;

      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

      QuadratureOrder quadratureOrder( xfld, 3*order+1 );
//      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-11, 1e-11};

      ////////////
      //SOLVE
      ////////////

      PrimalEquationSetClass PrimalEqSet(xfld, *qfld, lgfld, pde, stab, quadratureOrder,
                                         ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );


      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

#if 1

      typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD2, TopoD2>> AlgebraicEquationSet_PTCClass;
      AlgebraicEquationSet_PTCClass AlgEqSetPTC(xfld, *qfld, pde, quadratureOrder, {0}, PrimalEqSet);

      PyDict NonLinearSolverDict;
      NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

      Real invCFL = 100;
      Real invCFL_min = 0.0;
      Real invCFL_max = 1000.0;
      Real CFLDecreaseFactor = 0.5;
      Real CFLIncreaseFactor = 2.0;
      Real CFLPartialStepDecreaseFactor = 0.9;
      // Create the pseudo time continuation class
      PseudoTime<SystemMatrixClass>
      PTC(invCFL, invCFL_max, invCFL_min, CFLDecreaseFactor, CFLIncreaseFactor, CFLPartialStepDecreaseFactor,
          NonLinearSolverDict, AlgEqSetPTC, true);

      bool convgd = PTC.iterate(100);
#else

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      rsd = 0;

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;


      bool convgd = Solver.solve(ini,sln).converged;
      PrimalEqSet.setSolutionField(sln);

#endif

#if 0
      fstream fout( "tmp/jac_eulerDG_new.mtx", fstream::out );
      cout << "btest: global jac" << endl;
      WriteMatrixMarketFile( A, fout );
#endif


      if (convgd)
      {
        rsd = 0;
        PrimalEqSet.residual(rsd);

        // check that the residual is zero

        Real rsdPDEnrm[4] = {0,0,0,0};
        for (int j = 0; j < nSol; j++)
          rsdPDEnrm[j] = 0;

        for (int n = 0; n < nDOFPDE; n++)
          for (int j = 0; j < nSol; j++)
            rsdPDEnrm[j] += pow(rsd[0][n][j],2);

        BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[0]), 1e-11 );
        BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[1]), 1e-11 );
        BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[2]), 1e-11 );
        BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[3]), 1e-11 );

#if 1
        // Monitor Entropy Error
        Real EntropySquareError = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( fcnErr, EntropySquareError ),
            xfld, *qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        cout << "P = " << order << " pow = " << power << " nDOF = " << nDOFPDE << ": Entropy error = " << sqrt(EntropySquareError);
        //      if (indx > 1)
        //        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << endl;

#else
        Real Drag = 0.0;
        IntegrateBoundaryTraceGroups<TopoD2>::integrate(
            FunctionalBoundaryTrace_WeightedResidual_Galerkin( outputIntegrand, fcnBC, Drag ),
            xfld, qfld, quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size() );

        cout << "P = " << order << " ii = " << ii << ": Drag = " << Drag;
        //      if (indx > 1)
        //        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << endl;

        // drag coefficient
        //      Real Cd = Drag / (0.5*rhoRef*qRef*qRef*lRef);
#endif


        // jacobian nonzero pattern transposed
        SystemNonZeroPattern nzT(PrimalEqSet.matrixSize());
        PrimalEqSet.jacobianTranspose(nzT);

        // jacobian transposed
        SystemMatrixClass jacT(nzT);
        PrimalEqSet.jacobianTranspose(jacT);

        SystemVectorClass rhs(PrimalEqSet.vectorEqSize());
        rhs = 0;
        //
        //      PrimalEqSet.dispatchBC().dispatch_DGBR2(
        //          JacobianFunctionalBoundaryTrace_Dispatch_DGBR2<SurrealClass>(outputIntegrand, xfld, qfld, rfld, sfld,
        //                                                                       quadratureOrder.boundaryTraceOrders.data(),
        //                                                                       quadratureOrder.boundaryTraceOrders.size(),
        //                                                                       rhs(PrimalEqSet.iPDE) ) );

        IntegrateCellGroups<TopoD2>::integrate( JacobianFunctionalCell_Galerkin( fcnErr, rhs(PrimalEqSet.iPDE) ),
                                                xfld, *qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        // adjoint solve
        SLA::LinearSolver<SystemMatrixClass> solverAdj(AdjSolverDict, PrimalEqSet, SLA::TransposeSolve);

        SystemVectorClass adj(PrimalEqSet.vectorStateSize());
        solverAdj.solve(rhs, adj);

        PrimalEqSet.setAdjointField(adj, wfld, mufld);

#if 1
        // Tecplot dump grid
        string filename = "tmp/SMOOTHBUMP/VMS_EulerBump_P";
        filename += to_string(order);
//        filename += "_Q";
//        filename += to_string(fieldorder);
        filename += "_ref";
        filename += to_string(power);
        filename += ".plt";
        output_Tecplot( *qfld, filename );

        // Tecplot dump grid
        string filename2 = "tmp/SMOOTHBUMP/VMS_EulerBump_P";
        filename2 += to_string(order);
//        filename += "_Q";
//        filename += to_string(fieldorder);
        filename2 += "_ref";
        filename2 += to_string(power);
        filename2 += "_adjoint.plt";
        cout << "calling output_Tecplot: filename2 = " << filename2 << endl;
        output_Tecplot( wfld, filename2 );
#endif
#if 0
        cout << "dumping lg:" << endl;
        lgfldBoundaryEdgeBase[0]->dump(2);
        lgfldBoundaryEdgeBase[1]->dump(2);
        lgfldBoundaryEdgeBase[2]->dump(2);
        lgfldBoundaryEdgeBase[3]->dump(2);
#endif

      }
      else
      {
        // Tecplot dump grid
        string filename = "tmp/SMOOTHBUMP/VMS_EulerBump_P";
        filename += to_string(order);
//        filename += "_Q";
//        filename += to_string(fieldorder);
        filename += "_ref";
        filename += to_string(power);
        filename += "_UNCONVERGED.plt";
        output_Tecplot( *qfld, filename );

      }
    }

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
