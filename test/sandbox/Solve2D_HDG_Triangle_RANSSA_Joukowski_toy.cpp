// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGBR2_Triangle_RANSSA_FlatPlate_toy
// testing of 2-D DGBR2 for RANS-SA for flat plate

//#define SANS_FULLTEST
#define SANS_VERBOSE
#define FROMADAPT

//#include <fenv.h> // catch NaN

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/HDG/FunctionalBoundaryTrace_Dispatch_HDG.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/ProjectSoln_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_OutputWeightRsd_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Output_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectSolnTrace_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_CG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/DistanceFunction/DistanceFunction.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/EPIC/XField_PX.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Triangle_RANSSA_Joukowski_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGBR2_Triangle_RANSSA_Joukowski )
{
  typedef QTypePrimitiveRhoPressure QType;
  //typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, ParamFieldTupleType> PrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamFieldTupleType> AlgebraicEquationSet_PTCClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;

  mpi::communicator world;

  std::vector<Real> tol = {1e-9, 1e-9, 1e-9};

  //feenableexcept(FE_INVALID | FE_OVERFLOW);

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.5;
  const Real Reynolds = 1e6;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 0.0;                            // angle of attack (radians)

  const Real pRef = 1.0;
  const Real tRef = pRef/rhoRef/R;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  // stagnation temperature, pressure


  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Entropy );


  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient);

  //WEIGHTED RESIDUAL
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, HDG> IntegrandOutputClass;

  NDOutputClass outputFcn(pde, 1., 0.);
#ifdef FROMADAPT
  IntegrandOutputClass outputIntegrand( outputFcn, {0, 1} );
#else
  IntegrandOutputClass outputIntegrand( outputFcn, {0} );
#endif

  //FORCE INTEGRAL
//  typedef OutputEuler2D_ForceEval<PDEClass> OutputClass2;
//  typedef OutputNDConvertSpace<PhysD2, OutputClass2> NDOutputClass2;
//  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass2>, NDOutputClass2::Category> NDOutVecCat2;
//  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat2, HDG> IntegrandOutputClass2;
//
//  PDEClass pde2(gas, visc, tcond, Euler_ResidInterp_Momentum );
//  NDOutputClass2 outputFcn2(pde2, 1.0, 0.0);
//  IntegrandOutputClass2 outputIntegrand2( outputFcn2, {0} );

  //L2 ENTROPY ERROR
//  typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputEntropyErrorSquaredClass;
//  typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquaredClass> NDOutputEntropyErrorSquaredClass;
//  typedef IntegrandCell_HDG_Output<NDOutputEntropyErrorSquaredClass> IntegrandSquareErrorClass;
//
//  Real sExact = 0.0;
//  NDOutputEntropyErrorSquaredClass outEntropyError( pde, sExact );
//  IntegrandSquareErrorClass fcnErr3( outEntropyError, {0} );



  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;
//  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipIsothermal_mitState;
//  BCNoSlip[BCNavierStokes2DParams<BCTypeWallNoSlipIsothermal_mitState>::params.Twall] = tRef;


  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;
  StateVector["rhont"]  = rhoRef*ntRef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

#ifdef FROMADAPT
  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0,1};
  BCBoundaryGroups["BCIn"] = {2,3};
#else
  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0};
  BCBoundaryGroups["BCIn"] = {1,2};
#endif

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

//  const std::vector<int> BoundaryGroups = {0,1,2,3,4,5};

  // Set up Newton Solver
  PyDict NewtonSolverDict, LinSolverDict, NonLinearSolverDict, NewtonLineUpdateDict;

//#ifdef INTEL_MKL
//  std::cout << "Using MKL\n";
//  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
//#else
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
//#endif

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  //  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 10;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] =  true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  Real invCFL = 100;
  Real invCFL_min = 0.0;
  Real invCFL_max = 1000.0;
  Real CFLDecreaseFactor = 0.5;
  Real CFLIncreaseFactor = 2.0;
  Real CFLPartialStepDecreaseFactor = 0.9;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

//   norm data
//  Real normVec[10];   // L2 error
//  int indx = 0;

//  fstream fout( "tmp/DGBR2_V2_Ec0p3Re1.txt", fstream::out );
//
  int powermin = 5;
  int powermax = 6;
//
  int ordermin = 1;
  int ordermax = 1;

  fstream fout;
  string filename_base = "tmp/RANSJOUK/DGMESHES2/";
  boost::filesystem::create_directories(filename_base);

  string filename2 = filename_base + "EDG_Drag_P1.txt";

  for (int power = powermin; power <= powermax; power++)
  {
#ifdef FROMADAPT
    // pcaplan TODO check these have lagrange basis functions...local patch only works with these
    Real targetCost = 500.0*pow(2.0,power);

    // to make sure folders have a consistent number of zero digits
    const int string_pad = 5;
    std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);

    string filein = "/home2/ADAPTCG/RANSJOUK/DG/DG_" + int_pad + "_P1/mesh_a24_outQ3.grm";
#else
    string filein = "grids/Joukowski_RANS_Challenge_tri_ref";
    filein += to_string(power);
    filein += "_Q4.grm";
#endif

//    int order0 = 0;
    XField_PX<PhysD2, TopoD2> xfld(world, filein);

//    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld0(xfld, order0, BasisFunctionCategory_Legendre);
//    Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld0(xfld, order0, BasisFunctionCategory_Legendre);
//    Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qIfld0(xfld, order0, BasisFunctionCategory_Lagrange);
//    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
//    lgfld0(xfld, order0, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

    int dorder = 3; // Distance order
    Field_CG_Cell<PhysD2, TopoD2, Real> distfld(xfld, dorder, BasisFunctionCategory_Lagrange);
    DistanceFunction(distfld, BCBoundaryGroups.at("BCNoSlip"));
    ParamFieldTupleType paramfld = (distfld, xfld);
//
//    QuadratureOrder quadratureOrder0( xfld, 1 );//3*order + 1 );
//
    ArrayQ q0;
    pde.setDOFFrom( q0, SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntRef}) );

//      std::cout << rhoRef << " " << uRef << " " << vRef << " " << pRef << "\n";

//    qfld0 = q0;
//    afld0 = 0.0;
//    qIfld0 = q0;
//    lgfld0 = 0;
//
//
//    QuadratureOrder quadratureOrder0( xfld, 1 );
//
//    PrimalEquationSetClass PrimalEqSet0(paramfld, qfld0, afld0, qIfld0, lgfld0, pde, disc, quadratureOrder0,
//                                        tol, {0}, {0}, PyBCList, BCBoundaryGroups);
//
//    AlgebraicEquationSet_PTCClass AlgEqSetPTC0(paramfld, qfld0, pde, quadratureOrder0, {0}, PrimalEqSet0);
//
//    // Create the pseudo time continuation class
//    PseudoTime<SystemMatrixClass>
//       PTC(invCFL, invCFL_max, invCFL_min, CFLDecreaseFactor, CFLIncreaseFactor, CFLPartialStepDecreaseFactor,
//           NonLinearSolverDict, AlgEqSetPTC0, true);
//
//    BOOST_CHECK( PTC.iterate(100) );

    // loop over grid resolution: 2^power
    //    int power = 0;
    for (int order = ordermin; order <= ordermax; order++)
    {
      if (world.rank() == 0)
      {
        cout << "P = " << order << " power = " << power << std::endl;
        fout.open( filename2, fstream::app );
      }

      cout << "P = " << order << " power = " << power << std::endl;

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Lagrange);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

      qfld = q0;
      afld = 0.0;
      qIfld = q0;
      lgfld = 0;

      QuadratureOrder quadratureOrder( xfld, 2*order+2 );//3*order + 1 );

      //////////////////////////
      //SOLVE SYSTEM
      //////////////////////////

      PrimalEquationSetClass PrimalEqSet(paramfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                         {0}, {0}, PyBCList, BCBoundaryGroups);

      AlgebraicEquationSet_PTCClass AlgEqSetPTC(paramfld, qfld, pde, quadratureOrder, {0}, PrimalEqSet);
#if 1
      // Create the pseudo time continuation class
      PseudoTime<SystemMatrixClass>
         PTC(invCFL, invCFL_max, invCFL_min, CFLDecreaseFactor, CFLIncreaseFactor, CFLPartialStepDecreaseFactor,
             NonLinearSolverDict, AlgEqSetPTC, true);

      BOOST_CHECK( PTC.iterate(500) );

#else
      typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
      typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0;

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      bool solved = Solver.solve(ini,sln).converged;
      BOOST_CHECK(solved);
      PrimalEqSet.setSolutionField(sln);
#endif


#if 1
      // Tecplot dump grid
      string filename2 = "tmp/HDG_RANSJoukowski_P";
      filename2 += to_string(order);
      filename2 += "_X";
      filename2 += to_string(power);
      filename2 += ".plt";
      output_Tecplot( qfld, filename2 );
      std::cout << "Wrote Tecplot File\n";
#endif

      // Monitor Drag Error
      Real Drag = 0;

      PrimalEqSet.dispatchBC().dispatch_HDG(
        FunctionalBoundaryTrace_Dispatch_HDG(outputIntegrand, (distfld, xfld), qfld, afld, qIfld,
                                             quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(), Drag ) );
//
//      Real Drag2 = 0;
//
//      PrimalEqSet.dispatchBC().dispatch_HDG(
//        FunctionalBoundaryTrace_Dispatch_HDG(outputIntegrand2, (distfld, xfld), qfld, afld, qIfld,
//                                             quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(), Drag2 ) );
//
//      // Monitor Entropy Error
//      Real EntropySquareError = 0.0;
//      IntegrateCellGroups<TopoD2>::integrate(
//          FunctionalCell_HDG( fcnErr3, EntropySquareError ), (distfld, xfld), (qfld, afld),
//          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
//
//      // drag coefficient
//      Real Cd = Drag / (0.5*rhoRef*qRef*qRef*lRef);
//      Real Cd2 = Drag2 / (0.5*rhoRef*qRef*qRef*lRef);

      int nDOFq = qIfld.nDOFnative();

      if (world.rank() == 0)
      {
        fout << order << " " << to_string(nDOFq);
        fout << " " << std::setprecision(15) << Drag <<"\n";
        fout.close();
      }


    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
