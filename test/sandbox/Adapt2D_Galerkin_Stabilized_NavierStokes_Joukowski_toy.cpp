// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_NavierStokes_FlatPlate_btest
// Testing of the MOESS framework on a 2D Navier-Stokes problem


#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include <iostream>
#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/PDENavierStokes_Brenner2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Adaptation/MOESS/SolverInterface_AGLS.h"
#include "Adaptation/MOESS/MOESSParams.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_NavierStokes_FlatPlate_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_NavierStokes_FlatPlate_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;

//#define BRENNER
#ifndef BRENNER
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
#else
  typedef PDENavierStokes_Brenner2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
#endif
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

#ifndef BRENNER
  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;
#else
  typedef BCNavierStokesBrenner2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;
#endif

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                   AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

//  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef SolverInterface_AGLS<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;


  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1.0e-10, 1.0e-10};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.5;
  const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 3*(PI/180);                   // angle of attack (radians)
  const Real pRef = 1.0;                            // pressure
  const Real tRef = pRef/(rhoRef*R);

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = cRef*Mach;                      // velocity scale

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

#ifndef BRENNER
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum );
#else
  Real rhodiff = 4./3.;
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, rhodiff );
#endif

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  // Galerkin Stabilization
  StabilizationMatrix stab(StabilizationType::AGLSPrimal, TauType::Glasby);

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

#if 1
  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict BCOut(BCIn);
#else

  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aRef = atan(uRef/vRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aRef;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
#endif

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0,1};
  BCBoundaryGroups["BCIn"] = {2};
  BCBoundaryGroups["BCOut"] = {3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde, 1.0);
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, stab);
#else
  // Drag output
  NDOutputClass outputFcn(pde, 1., 0.);
  OutputIntegrandClass outputIntegrand( outputFcn, {0,1} );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 200;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-20;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 500;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL) && 0
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-12;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  PyDict SolverContinuationDict0, NonlinearSolverDict0;

  // Easier PTC for subsequent solves
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;


  //PTC for
  NonlinearSolverDict0[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict0[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict0[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict0[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict0[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict0[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict0[PseudoTimeParam::params.invCFL_max] = 1e5;

  SolverContinuationDict0[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict0;
  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict0);
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  int orderL = 1, orderH = 3;
  int powerL = 1, powerH = 5;


#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc == 3)
  {
    orderL = orderH = std::stoi(argv[1]);
    powerL = powerH = std::stoi(argv[2]);
  }
  if (argc == 2)
  {
    orderL = orderH = std::stoi(argv[1]);
  }

  std::cout << "orderL = " << orderL << ", orderH = " << orderH << std::endl;
  std::cout << "powerL = " << powerL << ", powerH = " << powerH << std::endl;
#endif

  const bool DGCONV = true;

  //--------ADAPTATION LOOP--------
  int maxIter = 50;

  for ( int order = orderL; order<=orderH; order++)
  {
    stab.setStabOrder(order);
    stab.setNitscheOrder(order);

    for (int power=powerL; power<=powerH; power++)
    {
      timer totalTime;

      // Grid
      std::string file_initial_mesh = "grids/joukowski_initial_mesh/jouk_q3.grm";
      std::string file_initial_linear_mesh = "grids/joukowski_initial_mesh/jouk_q1_surf_swapped.grm";

      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField_PX<PhysD2, TopoD2>(world, file_initial_mesh) );
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld_linear( new XField_PX<PhysD2, TopoD2>(world, file_initial_linear_mesh) );
      // pcaplan TODO check these have lagrange basis functions...local patch only works with these

      Real targetCost = 500.0*pow(2.0,power);

      // to make sure folders have a consistent number of zero digits
      const int string_pad = 5;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);

      if (DGCONV)
      {
        Real nDOFperCell_DG = (order+1)*(order+2)/2;

        Real nDOFperCell_CG = nDOFperCell_DG;
        nDOFperCell_CG -= (3 - 1./2); // the node dofs are shared by 6
        nDOFperCell_CG -= (3 - 3./2)*std::max(0,(order-1)); // if there are edge dofs they are shared by 2

        targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
      }

      std::string filename_base;
      if ( stab.getStabType() == StabilizationType::Adjoint )
      {
        filename_base = "tmp/CGJOUK/VMSK/VMSK_" + int_pad + "_P" + std::to_string(order) + "/";
      }
      else if ( stab.getStabType() == StabilizationType::Unstabilized )
      {
        filename_base = "tmp/CGJOUK/CG_" + int_pad + "_P" + std::to_string(order) + "/";
      }
      else if ( stab.getStabType() == StabilizationType::AGLSPrimal )
      {
        filename_base = "tmp/CGJOUK_RE10K/AGLS_ENTROPY/JOUK_EDGE_SANS_" + int_pad + "_P" + std::to_string(order) + "/";
      }
      else if ( stab.getStabType() == StabilizationType::SUPG )
      {
        filename_base = "tmp/CGJOUK/SUPG_" + int_pad + "_P" + std::to_string(order) + "/";
      }

      boost::filesystem::create_directories(filename_base);


//      filename_base += "DG_";
      Real nDOFperCell_DG = (order+1)*(order+2)/2;

      Real nDOFperCell_CG = nDOFperCell_DG;
      nDOFperCell_CG -= (3 - 1./2); // the node dofs are shared by 6
      nDOFperCell_CG -= (3 - 3./2)*std::max(0,(order-1)); // if there are edge dofs they are shared by 2

      targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist;
      if (world.rank() == 0)
      {
        fadapthist.open( adapthist_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
      }


      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Optimizer_MaxEval] = 6000;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;

      // MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Element;
      MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
      MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;
      MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
      MesherDict[EpicParams::params.CurvedQOrder] = 3; //Mesh order
      MesherDict[EpicParams::params.CSF] = "grids/joukowski_initial_mesh/jouk.csf"; //Geometry file
      MesherDict[EpicParams::params.GeometryList2D] = "Airfoil Farfield"; //Geometry list
      MesherDict[EpicParams::params.minGeom] = 1e-6;
      MesherDict[EpicParams::params.maxGeom] = 0.5;
      MesherDict[EpicParams::params.nPointGeom] = -1;
      MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;
      //MesherDict[EpicParams::params.Version] = "madcap_devel-10.2";

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;


      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                   active_boundaries, stab);

      const int quadOrder = 2*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict0, LinearSolverDict,
                                                          outputIntegrand);

      //Set initial solution
      pGlobalSol->setSolution(q0);

      std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      std::string qfld_filename = filename_base + "qfld_a0.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string delta_adjfld_filename = filename_base + "delta_adjfld_a0.plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, delta_adjfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_a0.plt";
      output_Tecplot( pInterface->getAdjField(), adjfld_filename );

      pInterface->computeErrorEstimates();
      std::string efld_filename = filename_base + "efld_a0.plt";

      pInterface->output_EField(efld_filename);

      for (int iter = 0; iter < maxIter+1; iter++)
      {
        if (world.rank() == 0 )
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;
//        ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld_linear, cellGroups, *pInterface, iter);
        ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew_linear = ptr_pair.first;
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew = ptr_pair.second;

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                        BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                        active_boundaries, stab);

#if 0
        pGlobalSolNew->setSolution(q0);
#else
        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);
#endif

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, LinearSolverDict,
                                                               outputIntegrand);


        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld = pxfldNew;
        pxfld_linear = pxfldNew_linear;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();

        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string delta_adjfld_filename = filename_base + "delta_adjfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, delta_adjfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pInterface->getAdjField(), adjfld_filename );

        //Compute error estimates
        pInterface->computeErrorEstimates();

        std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".plt";
        pInterface->output_EField(efld_filename);

      }

      if (world.rank() == 0)
      {
        fadapthist << "\n\nTotal Time elapsed: " << totalTime.elapsed() << "s" << std::endl;
        fadapthist.close();
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
