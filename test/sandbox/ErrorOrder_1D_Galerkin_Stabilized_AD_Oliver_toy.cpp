// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_CG_AD_btest
// testing of 1-D CG with Advection-Diffusion

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_Solution.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Adaptation/MOESS/SolverInterface_AGLS.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_WeightedResidual_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Field/ProjectSoln/InterpolateFunctionCell_Continuous.h"

#include "Meshing/XField1D/XField1D.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_1D_CG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorOrder_1D_CG_AD )
{

  typedef ScalarFunction1D_Sine SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef ScalarFunction1D_Sine AdjSolutionExact;
  typedef SolnNDConvertSpace<PhysD1, AdjSolutionExact> NDAdjSolutionExact;

  //ADVECTION_DIFFUSION CASE
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_None,
                                ViscousFlux1D_Oliver,
                                Source1D_Oliver > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;


//  typedef BCTypeFunction_mitState<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_None, ViscousFlux1D_Oliver> BCVector;


  typedef ScalarFunction1D_OliverOutputWeight WeightFunctionClass;
  typedef OutputCell_WeightedSolution<PDEClass, WeightFunctionClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> IntegrandOutputClass;

  typedef ParamType_None ParamBuilderType;
  typedef SolutionData_Galerkin_Stabilized<PhysD1, TopoD1, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

//    typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, IntegrandOutputClass> SolverInterfaceClass;
  typedef SolverInterface_AGLS<SolutionClass, PrimalEquationSetClass, IntegrandOutputClass> SolverInterfaceClass;


  // PDE
//  Real a = 1;
  AdvectiveFlux1D_None adv;
  Real nu = 1.0;

  //ADVECTION DIFFUSION
  ViscousFlux1D_Oliver visc( nu );

  Real src = 0.5;
  Source1D_Oliver source(src);

  Real a = 1;
  NDSolutionExact solnExact(a);
  NDAdjSolutionExact adjSolnExact(a);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde(adv, visc, source, forcingptr );
//  NDPDEClass pde(adv, visc, source);

  // BC

  // Create a BC dictionary
  PyDict Sine;
  Sine[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.Sine;
  Sine[SolutionExact::ParamsType::params.a] = a;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function] = Sine;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType] = "Dirichlet";
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Upwind] = false;


  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0,1};
  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Primal Exact Soln
  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // Adjoint Exact Solution
  typedef OutputCell_SolutionErrorSquared<PDEClass, AdjSolutionExact> AdjErrorClass;
  typedef OutputNDConvertSpace<PhysD1, AdjErrorClass> NDAdjErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDAdjErrorClass> ErrorAdjIntegrandClass;

  NDAdjErrorClass fcnAdjError(adjSolnExact);
  ErrorAdjIntegrandClass errorAdjIntegrand(fcnAdjError, {0});

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby);

  WeightFunctionClass weightFcn(nu, src);
  NDOutputClass outputFcn(weightFcn);
  IntegrandOutputClass outputIntegrand(pde, outputFcn, {0}, stab);

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  string filename_base = "tmp/OLIVERRANS/AGLS2/ADS";
  string filename2 = filename_base + "_Errors.txt";

  fstream foutsol( filename2, fstream::out );

  int ordermin = 1;
  int ordermax = 3;
  for (int order = ordermin; order <= ordermax; order++)
  {
    stab.setStabOrder(order);
    stab.setNitscheOrder(order);
    // loop over grid resolution: 2^power
    int powermin = 2;
    int powermax = 8;

    for (int power = powermin; power <= powermax; power++)
    {
      std::cout << "order: " << order << ", power:" << power << "\n";
      int ii = pow( 2, power )+1;

      // grid:

      XField1D xfld( ii );

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(xfld, pde, order, order+1,
                                                   BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                   active_boundaries, stab);

      std::vector<Real> tol = {1e-11, 1e-11};
      const int quadOrder = 3*(order + 1);
      QuadratureOrder quadratureOrder( xfld, quadOrder );

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrand);

      pGlobalSol->setSolution(0.0);

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();
      Real output = pInterface->getOutput();
//      Real outputexact = PI*(2./3. + 0.5*PI);
      Real outputexact = 4.2921004167641579346;
//      Real outputexact = PI*(4./3. + 0.5*PI); // VISCOUS ONLY

      Real outputErr = output - outputexact;

      pInterface->computeErrorEstimates();
      Real outputErrEstimate = pInterface->getGlobalErrorEstimate();

      // L2 solution error

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorIntegrand, SquareError ),
          xfld, pGlobalSol->primal.qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real primalErr = sqrt(SquareError);

      // L2 adjoint solution error
      Real AdjSquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorAdjIntegrand, AdjSquareError ),
          xfld, pInterface->getAdjField(), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real adjError = sqrt(AdjSquareError);

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      foutsol << order << " " << ii << " " << std::setprecision(15) << primalErr;
      foutsol << " " << adjError;
      foutsol << " " << outputErr  << " ";
      foutsol << " " << outputErrEstimate  << "\n";

//      std::cout << std::setprecision(16) << output << "\n";
#endif


#if 1
      // Tecplot dump of solution
      string filenameQ = filename_base + "_P";
      filenameQ += stringify(order);
      filenameQ += "_";
      filenameQ += stringify(ii);
      filenameQ += ".plt";
//      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( pGlobalSol->primal.qfld, filenameQ );

#endif

#if 1
      // Tecplot dump of solution
      string filenameAdj = filename_base + "_P";
      filenameAdj += stringify(order);
      filenameAdj += "_";
      filenameAdj += stringify(ii);
      filenameAdj += "_adj.plt";
//      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( pInterface->getAdjField(), filenameAdj );

#endif

    }

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
