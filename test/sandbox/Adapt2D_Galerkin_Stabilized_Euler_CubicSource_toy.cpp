// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_Galerkin_AD_DoubleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include <iostream>
#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

//#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "Adaptation/MeshAdapter.h"
#include "Meshing/EPIC/XField_PX.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"

#include "Field/output_grm.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_Galerkin_AD_DoubleBoundaryLayer_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_AD_DoubleBoundaryLayer_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;
  typedef BCParameters<BCVector> BCParams;

  typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE
  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // Initial Condition
  //solution at inlet is (1, 0.1, 0, 1);
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(1, 0.1, 0, 1) );

  // BC
  // Create a BC dictionary
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;

  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = 0.0;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = 3.505;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = 0;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = 1.0;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2};
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {3};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Galerkin Stabilization

  StabilizationMatrix stab(StabilizationType::Adjoint, TauType::Glasby);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-9, 1e-9};

  //Output functional
  Real sExact = 0.0;
  NDOutputClass outEntropyError( pde, sExact );
  OutputIntegrandClass outputIntegrand( outEntropyError, {0} );

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-9;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  PETScDict[SLA::PETScSolverParam::params.Timing] = false;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

//#define XFIELD_IN
//#define XFIELD_OUT

  std::vector<Real> targetCosts = {1000};


  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;

  //--------ADAPTATION LOOP--------
  for (int order = 3; order >= 1; order--)
  {
    stab.setStabOrder(order+1);
    stab.setNitscheOrder(order+1);
    // Grid
    int jj = pow(2,1), ii= 3*jj;
    Real tau = 0.1; // small bump

    pxfld = std::make_shared<XField2D_CubicSourceBump_Xq>( ii, jj, tau, 1 );
    for (auto it = targetCosts.begin(); it != targetCosts.end(); ++it)
    {
      Real targetCost = *it;

      const int maxIter = 20;

      // to make sure folders have a consistent number of zero digits
      const int string_pad = 5;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
      std::string filename_base = "tmp/Euler/CG_" + int_pad + "_P" + std::to_string(order) + "/";

      boost::filesystem::create_directories(filename_base);

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                   active_boundaries, stab);

      const int quadOrder = 3*(order + 1);

      // set initial solution
      pGlobalSol->setSolution(q0);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);


      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      std::string qfld_filename = filename_base + "qfld_a0.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_a0.plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

      for (int iter = 0; iter < maxIter+1; iter++)
      {
        std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        //Compute error estimates
        pInterface->computeErrorEstimates();

        //Perform local sampling and adapt mesh
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;

        pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                        BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                        active_boundaries, stab);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, LinearSolverDict,
                                                               outputIntegrand);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld = pxfldNew;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();

        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

      }

      fadapthist.close();
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
