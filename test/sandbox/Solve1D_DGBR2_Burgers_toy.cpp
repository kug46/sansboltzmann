// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_DGBR2_Burgers_toy
// testing of 1-D DG with Burgers

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/vector_c.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"
#include "pde/Burgers/PDEBurgers1D.h"
#include "pde/Burgers/BCBurgers1D.h"
#include "pde/Burgers/BurgersConservative1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/ForcingFunction1D_MMS.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGBR2_Burgers_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGBR2_Burgers )
{
  typedef BurgersConservative1D QType;
  typedef PDEBurgers<PhysD1,
                     QType,
                     ViscousFlux1D_Uniform,
                     Source1D_Uniform > PDEBurgers1D;
  typedef PDENDConvertSpace<PhysD1, PDEBurgers1D> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ScalarFunction1D_Tanh SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCBurgers1DVector<QType, ViscousFlux1D_Uniform, Source1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD1, TopoD1>> PDEPrimalEquationSetClass;
  typedef PDEPrimalEquationSetClass::BCParams PDEBCParams;

  typedef PDEPrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PDEPrimalEquationSetClass::SystemVector SystemVectorClass;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Burgers parameter PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  Real nu = 0.1*2./2.;
  Real A = 2.0;
  Real B = 0.0;
  ViscousFlux1D_Uniform visc( nu );

  ScalarFunction1D_Tanh MMS_soln( nu, A, B );

  typedef ForcingFunction1D_MMS<PDEBurgers1D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(MMS_soln) );

  Real b =  0.2;
  Source1D_Uniform source(b);

  //Exact solution
  NDSolutionExact solnExact( nu, A, B );

  NDPDEClass pde(visc, source, forcingptr);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict Tanh;
  Tanh[BCBurgersParams<PhysD1,BCType>::params.Function.Name] =
      BCBurgersParams<PhysD1,BCType>::params.Function.Tanh;
  Tanh[SolutionExact::ParamsType::params.nu] = nu;
  Tanh[SolutionExact::ParamsType::params.A] = A;
  Tanh[SolutionExact::ParamsType::params.B] = B;

  PyDict PDEBCSoln;
  PDEBCSoln[PDEBCParams::params.BC.BCType] = PDEBCParams::params.BC.FunctionLinearRobin_mitLG;
  PDEBCSoln[BCBurgersParams<PhysD1,BCType>::params.Function] = Tanh;

  PyDict PyPDEBCList;
  PyPDEBCList["PDEBCSoln"] = PDEBCSoln;

  std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  PDEBCBoundaryGroups["PDEBCSoln"] = {0, 1};

  //Check the BC dictionary
  PDEBCParams::checkInputs(PyPDEBCList);

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "Running full test..." << endl;
#endif
  int ordermax = 5;
#else
  int ordermax = 1;//2;
#endif
  for (int order_pde = ordermin; order_pde <= ordermax; order_pde++)
  {

    // loop over grid resolution
    int factormin = 1;
#ifdef SANS_FULLTEST
    int factormax = 6;
#else
    int factormax = 1;//4;
#endif
    Real xL = -2, xR = 2;
    for (int factor = factormin; factor <= factormax; factor++)
    {
      int ii = 20*factor;
      XField1D xfld( ii, -2, 2 );

      ////////////////////////////////////////////////////////////////////////////////////////
      // Allocate our Burgers parameter field
      ////////////////////////////////////////////////////////////////////////////////////////
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> pde_rfld(xfld, order_pde, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> pde_lgfld(xfld, order_pde, BasisFunctionCategory_Legendre,
                                                               PDEBCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups));
      pde_qfld = 0;
      pde_rfld = 0;
      pde_lgfld = 0;

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our AES to solve our PDE
      ////////////////////////////////////////////////////////////////////////////////////////
      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-12, 1e-12};
      Real viscousEtaParameter = 2;
      DiscretizationDGBR2 disc(0, viscousEtaParameter);
      PDEPrimalEquationSetClass PDEPrimalEqSet(xfld, pde_qfld, pde_rfld, pde_lgfld, pde,
                                               disc, quadratureOrder, ResidualNorm_Default, tol,
                                               {0}, {0}, PyPDEBCList, PDEBCBoundaryGroups);


      ////////////////////////////////////////////////////////////////////////////////////////
      // Newton Solver
      ////////////////////////////////////////////////////////////////////////////////////////
      PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
      UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
      LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

      NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
      NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
      NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
      NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1000;
      NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

      NewtonSolverParam::checkInputs(NewtonSolverDict);
      NewtonSolver<SystemMatrixClass> SolverBurgers( PDEPrimalEqSet, NewtonSolverDict );

      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial guess
      ////////////////////////////////////////////////////////////////////////////////////////
      for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );
//      pde_qfld = 0.0;
      output_gnuplot( pde_qfld, "tmp/qfld_init_pde.plt" );

      SystemVectorClass q(PDEPrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PDEPrimalEqSet.vectorStateSize());
      PDEPrimalEqSet.fillSystemVector(q);
      sln = q;

      ////////////////////////////////////////////////////////////////////////////////////////
      // SOLVE
      ////////////////////////////////////////////////////////////////////////////////////////
      SolveStatus status = SolverBurgers.solve(q, sln);
      BOOST_CHECK( status.converged );

      std::cout << "h: " << (xR-xL)/Real(ii) << " nu: " << nu << " Pe1: " << (xR-xL)/Real(ii)*A/2.0/nu/Real(order_pde)
                                                              << " Pe2: " << (xR-xL)/Real(ii)*A/2.0/nu/Real(order_pde+1) << std::endl;
#if 1
      // gnuplot dump
      string gfilename = "tmp/slnDG_P";
      gfilename += to_string(order_pde);
      gfilename += "_";
      gfilename += to_string(ii);
      gfilename += ".plt";
      output_gnuplot( pde_qfld, gfilename );
#endif
    } //grid refinement loop
  } //order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
