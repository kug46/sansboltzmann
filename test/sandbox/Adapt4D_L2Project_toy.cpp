// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// #define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <fstream>

#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AnalyticFunction/ScalarFunction4D.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Adaptation/MOESS/SolverInterface_L2Project.h"
#include "Adaptation/MeshAdapter.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Field/FieldSpacetime_CG_Cell.h"
#include "Field/FieldSpacetime_CG_BoundaryTrace.h"

#include "Field/FieldSpacetime_DG_Cell.h"
#include "Field/FieldSpacetime_DG_BoundaryTrace.h"

#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/NDConvert/OutputNDConvertSpace4D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime3D.h"
#include "pde/NDConvert/FunctionNDConvertSpace4D.h"


#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

#ifdef SANS_AVRO
static inline int factorial(int n)
{
  int m;
  if (n <= 1)
    m= 1;
  else
    m= n*factorial(n - 1);

  return m;
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt4D_L2_test_suite )

#if 1
BOOST_AUTO_TEST_CASE( Adapt4D_L2_test )
{
  #ifdef SANS_AVRO

  #define USE_CG 0

  // parse inputs

  int argc= boost::unit_test::framework::master_test_suite().argc;
  char ** argv= boost::unit_test::framework::master_test_suite().argv;

  int pReq= -1;                   // p to be used
  int dofReq= -1;          // dof to be requested

  const int lastn= 5;             // adaptation iterations to print

  int maxIter= -1;                // adaptation iterations to do

  // specify dof and p and maxIter
  if (argc == 4)
  {
    dofReq= std::stoi(argv[1]);
    pReq= std::stoi(argv[2]);
    maxIter= std::stoi(argv[3]);
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // just specify dof and p
  else if (argc == 3)
  {
    dofReq= std::stoi(argv[1]);
    pReq= std::stoi(argv[2]);
    maxIter= 10;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // just specify dof
  else if (argc == 2)
  {
    dofReq= std::stoi(argv[1]);
    pReq= 1;
    maxIter= 10;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // specify nothing
  else
  {
    dofReq = 128000;
    pReq= 1;
    maxIter= 100;
    std::cout << std::endl << "No input parsed! Defaults:" << std::endl;
  }

  SANS_ASSERT(dofReq > 0);
  SANS_ASSERT(pReq > 0);
  SANS_ASSERT(maxIter >= 0);
  SANS_ASSERT(maxIter >= lastn);

  std::cout << "\tdofReq: " << dofReq << std::endl;
  std::cout << "\tpReq: " << pReq << std::endl;
  std::cout << "\tmaxIter: " << maxIter << std::endl << std::endl;

#if USE_CG
  std::string filename_base= "tmp/L2Project4D_ST_CG_adapconv/";
#else
  std::string filename_base= "tmp/L2Project4D_ST_DG_adapconv/";
#endif
  std::string filename_case;

#if 0
  typedef ScalarFunction3D_SineSineSineSineUnsteady SolutionExact;
  const Real trueOutput= 0.0;

  // shortcut to the conversion for the exact solution to spacetime
  typedef SolnNDConvertSpaceTime<PhysD3, SolutionExact> NDSolutionExact;

  // create exact solution
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.Function.Name]= NDSolutionExact::ParamsType::params.Function.SineSineSineSineUnsteady;
  NDSolutionExact solnExact(solnArgs);
#else
  const Real trueOutput= 0.0;

  typedef SolnNDConvertSpaceTime<PhysD3, ScalarFunction4D_Monomial> NDSolutionExact;
  std::vector<Real> coeffs= {0.1, 0.2, 0.3, 0.4};
  std::vector<int> E= {3, 4, 3, 4};
  NDSolutionExact solnExact(1., coeffs, E);
#endif

#if USE_CG
  typedef SolverInterface_L2Project<PhysD4, TopoD4, NDSolutionExact, Field_CG_Cell> SolverInterfaceClass;
#else
  typedef SolverInterface_L2Project<PhysD4, TopoD4, NDSolutionExact, Field_DG_Cell> SolverInterfaceClass;
#endif

  // mpi
  mpi::communicator world;

  // checking tolerance
  std::array<Real,1> tol= {{1e-11}};

  // nonlinear solver dicts
  PyDict SolverContinuationDict;
  PyDict NonlinearSolverDict;
  PyDict NewtonSolverDict;
  PyDict AdjLinearSolverDict;
  PyDict LinearSolverDict;
  PyDict LineUpdateDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 )
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  //  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
  NewtonSolverDict[NewtonSolverParam::params.Verbose]= false;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method]= LineUpdateParam::params.LineUpdate.HalvingSearch;

  // NewtonSolverDict[NewtonSolverParam::params.LinearSolver]= UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate]= LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations]= 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations]= 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose]= true;

  // SOLVES

  Real targetCost;

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_base);

  // prep output files and data

  // norm data
  Real hDOFVec[lastn]= {0};
  int nElemVec[lastn]= {0};
  int nDOFVec[lastn]= {0};
  Real normVec[lastn]= {0};

  // output file
  std::string convhist_filename= filename_base + "test.convhist";
  fstream convhist;

  bool fileStarted= boost::filesystem::exists(convhist_filename);

  if (world.rank() == 0)
  {
    convhist.open(convhist_filename, fstream::app);
    BOOST_REQUIRE_MESSAGE(convhist.good(), "Error opening file: " + convhist_filename);

    if (!fileStarted)
    {
      convhist << "VARIABLES=";
      convhist << "\"p\"";
      convhist << ", \"DOFrequested\"";
      convhist << ", \"1/pow(DOF, 1.0/4.0)\"";
      convhist << ", \"Nelem\"";
      convhist << ", \"Ndof\"";
      convhist << ", \"L2 error\"";
      convhist << std::endl;
    }
    convhist << std::setprecision(16) << std::scientific;
  }

  // prep mesh adaption tools
  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction]= 1.0;
  MOESSDict[MOESSParams::params.CostModel]= MOESSParams::params.CostModel.LogEuclidean;
  // MOESSDict[MOESSParams::params.Verbosity]= MOESSParams::VerbosityOptions::Detailed;
  MOESSDict[MOESSParams::params.Verbosity]= MOESSParams::VerbosityOptions::Progressbar;

#if USE_CG
  MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
#else
  MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Element;
#endif

  // MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.FrobNorm;
  // MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

  MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD4, TopoD4>::params.Mesher.Name]= MeshAdapterParams<PhysD4, TopoD4>::params.Mesher.avro;
  MesherDict[avroParams::params.Curved]= false;

  std::vector<int> cellGroups= {0};

  // CREATE INITIAL GRID

  // 3x3x3x3 at minimum
  // const int ii= 2;
  const int ii= std::max(3, (int) floor(pow(dofReq*factorial(pReq)/factorial(pReq + PhysD4::D), 1./PhysD4::D)));
  const int jj= ii;
  const int kk= jj;
  const int mm= kk;

  std::cout << "initial grid created with ii= jj= kk= mm= " << ii << "." << std::endl;

  // create pointer
  std::shared_ptr<XField<PhysD4, TopoD4>> pxfld;

  // generate grid

  using avro::coord_t;
  using avro::index_t;

  avro::Context context;

  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld0(world, {ii, jj, kk, mm});

  std::shared_ptr<avro::Model> model= std::make_shared<avro::Model>(xfld0.context(), "tesseract");
  model->addBody(xfld0.body_ptr(), false);

  // copy the mesh into the domain and attach the geometry
  pxfld= std::make_shared<XField_avro<PhysD4,TopoD4>>(xfld0, model);

  // assign local variables

  const int order= pReq;

  // create solution fields
#if USE_CG
  std::shared_ptr<Field_CG_Cell<PhysD4, TopoD4, Real>> pqfld;
  pqfld= make_shared<Field_CG_Cell<PhysD4, TopoD4, Real>>(*pxfld, order, BasisFunctionCategory_Lagrange);
#else
  std::shared_ptr<Field_DG_Cell<PhysD4, TopoD4, Real>> pqfld;
  pqfld= make_shared<Field_DG_Cell<PhysD4, TopoD4, Real>>(*pxfld, order, BasisFunctionCategory_Lagrange);
#endif // USE_CG

  // adjoint equation is integrated with 2 times adjoint order
  const int quadOrder = 2*(order + 1);

  // create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pqfld, quadOrder,
                                                      cellGroups, tol, solnExact,
                                                      LinearSolverDict, false); // true to use elementwise_projection

  // set target cost
  targetCost= dofReq;

  // set initial solution
  *pqfld= 0.0;

  filename_case= filename_base + "dof" + stringify(targetCost) + "p" + stringify(order) + "/";

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_case);

  std::string adapthist_filename = filename_case + "test.adapthist";
  fstream fadapthist;

  if (world.rank() == 0)
  {
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.hasTrueOutput]= true;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.TrueOutput]= trueOutput;

  MeshAdapterParams<PhysD4, TopoD4>::checkInputs(AdaptDict);

  MeshAdapter<PhysD4, TopoD4> mesh_adapter(AdaptDict, fadapthist);

  pInterface->solveGlobalPrimalProblem();

  // ADAPTATION LOOP

  int iprinted= 0;

  for (int iter= 0; iter < maxIter + 1; iter++)
  {

    if (world.rank() == 0)
      std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    // compute error estimates
    pInterface->computeErrorEstimates();

    // perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD4, TopoD4>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    // perform L2 projection from solution on previous mesh

#if USE_CG
    std::shared_ptr<Field_CG_Cell<PhysD4, TopoD4, Real>> pqfldNew;
    pqfldNew= make_shared<Field_CG_Cell<PhysD4, TopoD4, Real>>(*pxfldNew, order, BasisFunctionCategory_Lagrange);
#else
    std::shared_ptr<Field_DG_Cell<PhysD4, TopoD4, Real>> pqfldNew;
    pqfldNew= make_shared<Field_DG_Cell<PhysD4, TopoD4, Real>>(*pxfldNew, order, BasisFunctionCategory_Lagrange);
#endif // USE_CG

    *pqfldNew= 0.0;

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew= std::make_shared<SolverInterfaceClass>(*pqfldNew, quadOrder,
                                                          cellGroups, tol, solnExact,
                                                          LinearSolverDict, false); // true to use elementwise_projection

    // update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld= pxfldNew;
    pqfld= pqfldNew;
    pInterface= pInterfaceNew;

    // solve the adapted problem
    pInterface->solveGlobalPrimalProblem();

    // compute error estimates
    pInterface->computeErrorEstimates();

    Real L2error = pInterface->getOutput();

#ifdef SANS_MPI
    // L2error= boost::mpi::all_reduce(*pxfld->comm(), L2error, std::plus<Real>()); // ???

    int nDOFtotal = 0;
    boost::mpi::all_reduce(*pxfld->comm(), pqfld->nDOFpossessed(), nDOFtotal, std::plus<int>());

    // count the number of elements possessed by this processor
    int nElem = 0;
    for (int elem = 0; elem < pxfld->nElem(); elem++ )
      if (pxfld->getCellGroupGlobal<Pentatope>(0).associativity(elem).rank() == world.rank())
        nElem++;

    int nElemtotal = 0;
    boost::mpi::all_reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>());
#else
    int nDOFtotal = pqfld->nDOFpossessed();
    int nElemtotal = pxfld->nElem();
#endif // SANS_MPI

    if (iprinted < lastn)
    {
      if (world.rank() == 0)
        cout << "iprinted= " << iprinted << " < lastn= " << lastn << endl;
      hDOFVec[iprinted]= 1.0/pow(nDOFtotal, 1.0/((Real) PhysD4::D));
      nElemVec[iprinted]= nElemtotal;
      nDOFVec[iprinted]= nDOFtotal;
      normVec[iprinted]= L2error;
      iprinted++;
    }
    else
    {
      for (int k= 1; k < lastn; k++)
      {
        if (world.rank() == 0)
          cout << "moving k= " << k << " to (k - 1)= " << k - 1 << endl;
        hDOFVec[k - 1]= hDOFVec[k];
        nElemVec[k - 1]= nElemVec[k];
        nDOFVec[k - 1]= nDOFVec[k];
        normVec[k - 1]= normVec[k];
      }
      if (world.rank() == 0)
        cout << "crowning with k= " << lastn - 1 << endl;
      hDOFVec[lastn - 1]= 1.0/pow(nDOFtotal, 1.0/((Real) PhysD4::D));
      nElemVec[lastn - 1]= nElemtotal;
      nDOFVec[lastn - 1]= nDOFtotal;
      normVec[lastn - 1]= L2error;
    }

    std::cout << std::endl << "INNER LOOP EXIT with " << nDOFtotal << " DOFs and " <<
        nElemtotal << " elements." << std::endl << std::endl;

  }

  if (world.rank() == 0)
  {

    fadapthist.close();

    for (int k= 0; k < iprinted; k++)
    {
      // outputfile
      convhist << order;
      convhist << ", " << (Real) dofReq;
      convhist << ", " << hDOFVec[k];
      convhist << ", " << (Real) nElemVec[k];
      convhist << ", " << (Real) nDOFVec[k];
      convhist << ", " << normVec[k];
      convhist << endl;
    }

  }

  if (world.rank() == 0)
    convhist.close();

#endif
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
