// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories

#include "SANS_btest.h"
#include "tools/SANSnumerics.h"     // Real

#include <chrono>
#include <thread>
#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/OutputEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "BasisFunction/LagrangeDOFMap.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"
#include "Adaptation/callMesher.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field/FieldVolume_CG_Cell.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Meshing/EPIC/XField_PX.h"

// For computing the Mach number and writing to libMeshb
#include "Field/Field_NodalView.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup_Cell.h"

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/libMeshb/WriteSolution_libMeshb.h"
#include "Meshing/libMeshb/ReadSolution_libMeshb.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_DGBR2_NavierStokes_DeltaWing_test_suite )


template< class ArrayQ>
class AccumulateStateVector:
    public GroupFunctorCellType<AccumulateStateVector<ArrayQ>>
{
public:

  explicit AccumulateStateVector( const std::vector<int>& cellGroups ) :
    cellGroups_(cellGroups) {}

  std::size_t nCellGroups()          const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  typedef PhysD3 PhysDim;
  // typedef TopoD3 TopoDim;
  // typedef Tet Topology;
  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class Topology>
  void
  apply(
      const typename FieldTuple< Field<PhysDim, typename Topology::TopoDim, ArrayQ>,
                                 Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<Topology>& fldsCell,
      const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field< PhysDim,typename Topology::TopoDim, ArrayQ >::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    // Apportioning the eLfld to efld
    // loop over groups
    const QFieldCellGroupType& DfldCell = get<0>(fldsCell);
          QFieldCellGroupType& CfldCell = const_cast<QFieldCellGroupType&>(get<1>(fldsCell));

    ElementQFieldClass DfldElem( DfldCell.basis() );
    ElementQFieldClass CfldElem( CfldCell.basis() );

    SANS_ASSERT_MSG( DfldCell.basis()->order() == 1, "Mach Number Hessian hack only works for P1 Fields");

    SANS_ASSERT( (DfldCell.basis()->category() == BasisFunctionCategory_Lagrange ||
                  DfldCell.basis()->category() == BasisFunctionCategory_Hierarchical) && DfldCell.basis()->order() == 1 );

    // std::cout<< "starting loop over elems " << std::endl;
    for (int elem = 0; elem < DfldCell.nElem(); elem++)
    {
      DfldCell.getElement(DfldElem,elem);
      CfldCell.getElement(CfldElem,elem);

      for (int n = 0; n < DfldElem.nDOF(); n++)
        CfldElem.DOF(n) += DfldElem.DOF(n);

      CfldCell.setElement(CfldElem,elem);
    }
  }

protected:
  const std::vector<int> cellGroups_;
};



//----------------------------------------------------------------------------//
// Explicitly sets nodes on wall boundaries to zero mach number
template<class PhysDim>
class SetWallZero :
    public GroupFunctorBoundaryCellType<SetWallZero<PhysDim>>
{
public:

  explicit SetWallZero( const std::vector<int>& boundaryTraceGroups ) :
    boundaryTraceGroups_(boundaryTraceGroups) {}

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL>
  void
  apply( const typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL>& mfldCellL,
         const int cellGroupGlobalL,
         const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL> FieldCellGroupTypeL;
    typedef typename FieldCellGroupTypeL::template ElementType<> ElementFieldCellClassL;

    SANS_ASSERT(  mfldCellL.basis()->category() == BasisFunctionCategory_Legendre ||
                 (mfldCellL.basis()->category() == BasisFunctionCategory_Hierarchical && mfldCellL.basis()->order() == 1) );

    // Construct the element
    ElementFieldCellClassL mfldElemL( mfldCellL.basis() );
    int morder = mfldElemL.order();

    // loop over elements
    const int nElem = xfldTrace.nElem();
    for (int elem = 0; elem < nElem; elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global distance DOFs to element
      mfldCellL.getElement( mfldElemL, elemL );

      // set all DOFs on wall boundaries explicitly to zero
      std::vector<int> map = LagrangeDOFMap<TopologyL>::getCanonicalTraceMap( canonicalTraceL.trace, morder );

      for (std::size_t i = 0; i < map.size(); i++)
        mfldElemL.DOF(map[i]) = 0;

      // save the element
      const_cast<FieldCellGroupTypeL&>(mfldCellL).setElement( mfldElemL, elemL );
    }
  }
protected:
  const std::vector<int> boundaryTraceGroups_;
};

template <class PDE, class ArrayQ>
void computeMachField( const PDE& pde, const std::vector<int> wallBTraceGroups,
                       Field_DG_Cell<PhysD3,TopoD3,ArrayQ>& Dfld,
                       const std::string& filename )
{
  const XField<PhysD3,TopoD3>& xfld = Dfld.getXField();

  Field_CG_Cell<PhysD3,TopoD3,ArrayQ> Cfld( xfld, 1, BasisFunctionCategory_Hierarchical );
  Field_CG_Cell<PhysD3,TopoD3,Real>   Mfld( xfld, 1, BasisFunctionCategory_Hierarchical );
  Cfld = 0; Mfld = 0;

  std::vector<int> cellgroups;
  for (int i = 0; i < xfld.nCellGroups(); i++)
    cellgroups.push_back(i);

  // Add all the DG DOFs to the CG DOFs, this is the summation in the average
  for_each_CellGroup<TopoD3>::apply( AccumulateStateVector<ArrayQ>(cellgroups), (Dfld,Cfld) );

  // divides by number of elements attached
  Field_NodalView nodalView( Cfld, cellgroups ); // assumes only one cell group

  int nNode = Cfld.nDOF();
  const std::vector<int>& node_list = nodalView.getNodeDOFList();

  SANS_ASSERT( nNode == (int) node_list.size() );

  Real x=0,y=0,z=0,t=0;
  ArrayQ qx=0,qy=0,qz=0;

  for (int node = 0; node < nNode; node++)
  {
    int global_node = node_list[node];
    Field_NodalView::IndexVector cell_list = nodalView.getCellList(global_node);

    int nCells = (int) cell_list.size();
    Cfld.DOF(global_node) /= (Real)nCells;

    pde.derivedQuantity(0,x,y,z,t,Cfld.DOF(global_node),qx,qy,qz,Mfld.DOF(global_node));
  }

  //Set all Mach numbers on wall boundaries explicitly to zero
  //for_each_BoundaryTraceGroup_Cell<TopoD3>::apply( SetWallZero<PhysD3>(wallBTraceGroups), Mfld );

  //output_Tecplot( Cfld, filename + "_cfld.dat" );
  WriteSolution_libMeshb( Mfld, filename + ".solb" );
  output_Tecplot( Mfld, filename + ".dat" );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_DGBR2_NavierStokes_DeltaWing_test )
{

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D, Real> MatrixSym;

//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Force<PDEClass>> NDOutputForce;
  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Moment<PDEClass>> NDOutputMoment;
  typedef NDVectorCategory<boost::mpl::vector2<NDOutputForce, NDOutputMoment>, NDOutputForce::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

#define USE_REFINE 0
#define USE_FEFLOA 0

  std::string init_grid_path = "grids/delta-wing/initial-grid/delta01.meshb";
  //std::string init_grid_path = "tmp/DeltaWing/xfld_a2.meshb";

  // initial Grid
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld = std::make_shared<XField_libMeshb<PhysD3, TopoD3>>(world, init_grid_path );

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // Sutherland viscosity
  //const Real tSuth = 198.6/540;     // R/R

  // reference state (freestream)
  const Real Mach = 0.3;
  const Real Reynolds = 4000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale

  const Real aoaRef = 12.5*PI/180;                  // angle of attack (radians)

  const Real SRef = 0.133974596;                    // area scale
#if 1
  const Real tRef = 1;          // temperature
  const Real pRef = rhoRef*R*tRef;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;
#endif

#if 0
  const Real qRef = 1;                              // velocity scale
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = 0;
  const Real wRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef + wRef*wRef);

  Real CG[3] = {lRef/0.25, 0, 0};

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure3D<Real>(rhoRef, uRef, vRef, wRef, pRef) );

  // DGBR2 discretization
  Real viscousEtaParameter = 2*Tet::NFace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);


  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipIsothermal_mitState;
  BCWall[BCNavierStokes3DParams<BCTypeWallNoSlipIsothermal_mitState>::params.Twall] = tRef;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict StateVector;
  StateVector[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  StateVector[Conservative3DParams::params.rho] = rhoRef;
  StateVector[Conservative3DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative3DParams::params.rhov] = rhoRef*vRef;
  StateVector[Conservative3DParams::params.rhow] = rhoRef*wRef;
  StateVector[Conservative3DParams::params.rhoE] = rhoRef*ERef;

  PyDict BCFarField;
  BCFarField[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.StateVector] = StateVector;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.Characteristic] = true;

  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflow[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;


  PyDict PyBCList;
  PyBCList["BCFarField"] = BCFarField;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCOutflow"]  = BCOutflow;
  PyBCList["BCWall"]     = BCWall;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  /*
      Boundary conditons (delta01.mapbc):
      1. solid wall top
      2. solid wall bottom
      3. solid wall bevel
      4. solid wall back face
      5. freestream x min
      6. freestream x max
      7. symmetry y 0
      8. freestream y max
      9. freestream z min
      10. freestream z max

      starting from 0

      Boundary conditons (delta01.mapbc):
      0. solid wall top
      1. solid wall bottom
      2. solid wall bevel
      3. solid wall back face
      4. freestream x min
      5. freestream x max
      6. symmetry y 0
      7. freestream y max
      8. freestream z min
      9. freestream z max
   */

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCFarField"] = {4,5,7,8,9};
  BCBoundaryGroups["BCSymmetry"] = {6};
  BCBoundaryGroups["BCOutflow"]  = {};
  BCBoundaryGroups["BCWall"]     = {0,1,2,3};

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-7, 1e-7};

  // Residual weighted boundary output
  Real dynpRef = 0.5*rhoRef*(uRef*uRef + vRef*vRef + wRef*wRef);

  NDOutputForce outputFcnDrag(pde,  cos(aoaRef)/(SRef*dynpRef), 0., sin(aoaRef)/(SRef*dynpRef));
  OutputIntegrandClass outputIntegrandDrag( outputFcnDrag, BCBoundaryGroups["BCWall"] );

  NDOutputForce outputFcnLift(pde, -sin(aoaRef)/(SRef*dynpRef), 0., cos(aoaRef)/(SRef*dynpRef));
  OutputIntegrandClass outputIntegrandLift( outputFcnLift, BCBoundaryGroups.at("BCWall") );

  NDOutputMoment outputFcnMoment(pde, CG[0], CG[1], CG[2], 0, 1, 0, SRef*dynpRef*lRef);
  OutputIntegrandClass outputIntegrandMoment( outputFcnMoment, BCBoundaryGroups.at("BCWall") );



  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
if (world.rank() == 0 )
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;
  const int MDForder = 3;
  PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (MDForder+1)*(MDForder+2)*(MDForder+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = true;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  int orderL = 1, orderH = 1;
  int powerL = 3, powerH = 3;

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc == 3)
  {
    orderL = orderH = std::stoi(argv[1]);
    powerL = powerH = std::stoi(argv[2]);
  }
#endif

  std::cout << "orderL, orderH = " << orderL << ", " << orderH << std::endl;
  std::cout << "powerL, powerH = " << powerL << ", " << powerH << std::endl;

  //--------ADAPTATION LOOP--------

  for (int order = orderL; order <= orderH; order++)
    for (int power = powerL; power <= powerH; power++ )
    {
      int maxIter = 30;

      int nk = pow(2,power);
      int targetCost = 1000*nk;
      Real targetComp = targetCost/(12.0/sqrt(2.0))/4;

      // initial Grid
      std::shared_ptr<XField<PhysD3, TopoD3>> pxfld = std::make_shared<XField_libMeshb<PhysD3, TopoD3>>(world,
                                                                          init_grid_path );
      // to make sure folders have a consistent number of zero digits
      const int string_pad = 7;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
#if USE_REFINE
      std::string filename_base = "tmp/DeltaWing/MultiScale/refine/DG_" + int_pad + "_P" + std::to_string(order) + "/";
#elif USE_FEFLOA
      std::string filename_base = "tmp/DeltaWing/MultiScale/fefloa/DG_" + int_pad + "_P" + std::to_string(order) + "/";
#else
      std::string filename_base = "tmp/DeltaWing/MultiScale/EPIC/DG_" + int_pad + "_P" + std::to_string(order) + "/";
#endif
      boost::filesystem::path base_dir(filename_base);
      if ( not boost::filesystem::exists(base_dir) )
        boost::filesystem::create_directories(filename_base);

#ifdef SANS_PETSC
      PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;
      PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order
#endif

      fstream fadapthist, foutputhist;
      if (world.rank() == 0)
      {
        std::string outputhist_filename = filename_base + "output.dat";
        foutputhist.open( outputhist_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + outputhist_filename);
      }

      PyDict ParamDict;

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;
      MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

      // PETSc MDF ordering Update
#if defined(SANS_PETSC) && USE_PETSC_SOLVER
      // Primal
      PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order
      PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
      PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
      NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

      // Adjoint
      // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
      // PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
      // PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
      // AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;
#endif

      PyDict MesherDict;
#if USE_REFINE
      MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.refine;
      MesherDict[refineParams::params.FilenameBase] = filename_base;
      MesherDict[refineParams::params.CADfilename] = ".egads";
#ifdef SANS_REFINE
      MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.system;
#endif
#elif USE_FEFLOA
      MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.FeFloa;
      MesherDict[FeFloaParams::params.FilenameBase] = filename_base;
#else
      MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
    //  MesherDict[EpicParams::params.Version] = "madcap_devel";
    //  MesherDict[EpicParams::params.CurvedQOrder] = 1; //Mesh order
    //  MesherDict[EpicParams::params.CSF] = "tmp/hemisphere-cylinder/EPIC/hemisphere_cylinder3_cf.csf"; //madcap surface file
    //  MesherDict[EpicParams::params.GeometryFile3D] = "tmp/hemisphere-cylinder/EPIC/hemisphere_cylinder3_cf.igs"; //CAD file
    //  MesherDict[EpicParams::params.Shell] = "SHELL";
    //  MesherDict[EpicParams::params.BoundaryObject] = "BDIST";
    //  MesherDict[EpicParams::params.minGeom] = 1e-6;
    //  MesherDict[EpicParams::params.maxGeom] = 2000;
    //  MesherDict[EpicParams::params.nPointGeom] = -1;
      std::vector<int> allBC;
      allBC.insert(allBC.end(), BCBoundaryGroups["BCFarField"].begin(), BCBoundaryGroups["BCFarField"].end());
      allBC.insert(allBC.end(), BCBoundaryGroups["BCSymmetry"].begin(), BCBoundaryGroups["BCSymmetry"].end());
      allBC.insert(allBC.end(), BCBoundaryGroups["BCOutflow"].begin(), BCBoundaryGroups["BCOutflow"].end());
      allBC.insert(allBC.end(), BCBoundaryGroups["BCWall"].begin(), BCBoundaryGroups["BCWall"].end());

      MesherDict[EpicParams::params.SymmetricSurf] = allBC;

    //  MesherDict[EpicParams::params.ViscSurf] = BCBoundaryGroups["BCWall"];
    //  MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;
      MesherDict[EpicParams::params.FilenameBase] = filename_base;
#endif

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;

      MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                   active_boundaries, ParamDict, disc);

      //Set initial solution
      pGlobalSol->setSolution(q0);

      const int quadOrder = 3*order + 1;

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrandDrag);

      //Set initial solution
      std::string qfld_init_filename = filename_base + "qfld_init_a0.dat";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

      //std::cout << "nDOF = " << pGlobalSol->primal.qfld.nDOFpossessed() << std::endl;
      pInterface->solveGlobalPrimalProblem();

      //std::cout << "Stopping for testing!!!" << std::endl;
      //return;

      std::string qfld_filename = filename_base + "qfld_a0.dat";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      pInterface->solveGlobalAdjointProblem();

      std::string adjfld_filename = filename_base + "adjfld_a0.dat";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

      //Compute error estimates
      pInterface->computeErrorEstimates();

      Real globalEstimate=0,globalIndicator=0,globalOutput=0;
      globalEstimate  = pInterface->getGlobalErrorEstimate();
      globalIndicator = pInterface->getGlobalErrorIndicator();
      globalOutput    = pInterface->getOutput();

      std::string efld_filename = filename_base + "efld_a" + std::to_string(0) + ".dat";

      pInterface->output_EField(efld_filename);


      //----------//
      QuadratureOrder quadrule(*pxfld, quadOrder);

      Real outputLift = 0;
      pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
          FunctionalBoundaryTrace_Dispatch_DGBR2(outputIntegrandLift, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
                                                 quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputLift ) );

#ifdef SANS_MPI
      outputLift = boost::mpi::all_reduce( *pxfld->comm(), outputLift, std::plus<Real>() );
#endif

      Real outputMoment = 0;
      pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
          FunctionalBoundaryTrace_Dispatch_DGBR2(outputIntegrandMoment, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
                                                 quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputMoment ) );

#ifdef SANS_MPI
      outputMoment = boost::mpi::all_reduce( *pxfld->comm(), outputMoment, std::plus<Real>() );
#endif

#ifdef SANS_MPI
      int nDOFtotal = 0;
      boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );
#else
      int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
#endif

      if (world.rank() == 0 )
      {
        // write the header to the output file
        foutputhist << "VARIABLES="
                    << std::setw(5)  << "\"Iter\""
                    << std::setw(10) << "\"DOF\""
                    << std::setw(20) << "\"C<sub>L</sub>\""
                    << std::setw(20) << "\"C<sub>D</sub>\""
                    << std::setw(20) << "\"C<sub>M</sub>\""
                    << std::setw(20) << "\"Estimate\""
                    << std::setw(20) << "\"Indicator\""
                    << std::endl;

        foutputhist << "ZONE T=\"Mach " << nk << "k\"" << std::endl;

        foutputhist << std::setw(5) << 0
                    << std::setw(10) << nDOFtotal
                    << std::setw(20) << std::setprecision(10) << std::scientific << outputLift
                    << std::setw(20) << std::setprecision(10) << std::scientific << globalOutput
                    << std::setw(20) << std::setprecision(10) << std::scientific << outputMoment
                    << std::setw(20) << std::setprecision(10) << std::scientific << globalEstimate
                    << std::setw(20) << std::setprecision(10) << std::scientific << globalIndicator
                    << std::endl;
      }

      for (int iter = 0; iter < maxIter+1; iter++)
      {
        if ( world.rank() == 0 ) std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        //Perform local sampling and adapt mesh
        BOOST_REQUIRE( order == 1);

        std::string machfld_filename = filename_base + "machfld_a"  + std::to_string(iter);
        computeMachField( pde, BCBoundaryGroups["BCWall"],
                          pGlobalSol->primal.qfld, machfld_filename ); // .solb and .dat files are written

        std::string xfld_filename = filename_base + "xfld_a"  + std::to_string(iter) + ".meshb";
        WriteMesh_libMeshb( *pxfld, xfld_filename );

        // manually calling refine must be done serially
        std::string mach_metric_filename = filename_base + "mach_metric_a"  + std::to_string(iter) + ".solb";
        if (world.rank() == 0)
        {
          // Call Refine at the command line to generate the metric field
          // ref_metric_test --lp grid.meshb scalar-mach.solb p gradation complexity output-metric.solb --kexact
          std::cout<< "Generating Metric field based on Mach number using refine" << std::endl;
          std::string ref_mach_call = "ref_metric_test --lp " + xfld_filename + " "
              + machfld_filename + ".solb 2 15 " + std::to_string(targetComp) + " " + mach_metric_filename + " --kexact";

          std::this_thread::sleep_for( std::chrono::milliseconds(500) );
          std::cout << ref_mach_call << std::endl;
          int sys = system(ref_mach_call.c_str());
          BOOST_REQUIRE( sys == 0 );
          std::this_thread::sleep_for( std::chrono::milliseconds(500) );
        }
        world.barrier();

        std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew;
        {
          // Read back in the metric and pass it along to the mesher
          Field_CG_Cell<PhysD3, TopoD3, MatrixSym> pmetric_req(*pxfld, 1, BasisFunctionCategory_Hierarchical );
          ReadSolution_libMeshb(pmetric_req, mach_metric_filename);

          pxfldNew = callMesher<PhysD3, TopoD3>::call(*pxfld, pmetric_req, iter, MesherDict).first;
        }

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                        BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                        active_boundaries, ParamDict, disc);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, AdjLinearSolverDict,
                                                               outputIntegrandDrag);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;
        pxfld = pxfldNew;

        std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".dat";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

        pInterface->solveGlobalPrimalProblem();

        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".dat";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        pInterface->solveGlobalAdjointProblem();

        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".dat";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

        //Compute error estimates
        pInterface->computeErrorEstimates();

        Real globalEstimate=0,globalIndicator=0,globalOutput=0;
        globalEstimate  = pInterface->getGlobalErrorEstimate();
        globalIndicator = pInterface->getGlobalErrorIndicator();
        globalOutput    = pInterface->getOutput();

        std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".dat";

        pInterface->output_EField(efld_filename);

        //----------//
        QuadratureOrder quadrule(*pxfld, quadOrder);

        Real outputLift = 0;
        pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
            FunctionalBoundaryTrace_Dispatch_DGBR2(outputIntegrandLift, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
                                                   quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputLift ) );

#ifdef SANS_MPI
        outputLift = boost::mpi::all_reduce( *pxfld->comm(), outputLift, std::plus<Real>() );
#endif

        Real outputMoment = 0;
        pInterface->AlgEqnSet().dispatchBC().dispatch_DGBR2(
            FunctionalBoundaryTrace_Dispatch_DGBR2(outputIntegrandMoment, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
                                                   quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputMoment ) );

#ifdef SANS_MPI
        outputMoment = boost::mpi::all_reduce( *pxfld->comm(), outputMoment, std::plus<Real>() );
#endif

#ifdef SANS_MPI
        int nDOFtotal = 0;
        boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );
#else
        int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
#endif

        if (world.rank() == 0)
        {
          foutputhist << std::setw(5) << iter+1
                      << std::setw(10) << nDOFtotal
                      << std::setw(20) << std::setprecision(10) << std::scientific << outputLift
                      << std::setw(20) << std::setprecision(10) << std::scientific << globalOutput
                      << std::setw(20) << std::setprecision(10) << std::scientific << outputMoment
                      << std::setw(20) << std::setprecision(10) << std::scientific << globalEstimate
                      << std::setw(20) << std::setprecision(10) << std::scientific << globalIndicator
                      << std::endl;
        }
      }

      fadapthist.close();
      foutputhist.close();
    }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
