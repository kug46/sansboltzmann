// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_VMSD_AD_toy
// testing of 1-D VMSD with advection diffusion

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include <boost/mpl/vector_c.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/BCParameters.h"

#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_EG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_VMSD_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGBR2_Burgers )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef ScalarFunction1D_Quad SolutionClass;
  typedef SolnNDConvertSpace<PhysD1, SolutionClass> SolutionNDClass;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> PDEPrimalEquationSetClass;
  typedef PDEPrimalEquationSetClass::BCParams PDEBCParams;

  typedef PDEPrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PDEPrimalEquationSetClass::SystemVector SystemVectorClass;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our AD parameter PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  Real c = 0.1;
  Real nu = 1.0;

  AdvectiveFlux1D_Uniform adv( c );
  ViscousFlux1D_Uniform visc( nu );
  Source1D_UniformGrad source(.0,.0);

  SolutionNDClass solnExact;

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde(adv, visc, source, forcingptr);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict PDEBCSoln;
  PDEBCSoln[PDEBCParams::params.BC.BCType] = PDEBCParams::params.BC.Dirichlet_mitState;
  PDEBCSoln[BCAdvectionDiffusionParams<PhysD1, BCTypeDirichlet_mitStateParam>::params.qB] = 0.0;

  PyDict PyPDEBCList;
  PyPDEBCList["PDEBCSoln"] = PDEBCSoln;

  std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  PDEBCBoundaryGroups["PDEBCSoln"] = {0, 1};

  //Check the BC dictionary
  PDEBCParams::checkInputs(PyPDEBCList);

  const int ordermin = 1;
  const int ordermax = 1;

  const int factormin = 10;
  const int factormax = 10;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up tecplot output
  ////////////////////////////////////////////////////////////////////////////////////////
  string filename_base = "tmp/AD/VMSD/";
  boost::filesystem::create_directories(filename_base);

  std::cout << std::setprecision(2) << std::scientific;

  for (int order_pde = ordermin; order_pde <= ordermax; order_pde++)
  {
    // loop over grid resolution
    for (int factor = factormin; factor <= factormax; factor++)
    {
      for (int order_pdep = ordermin; order_pdep <= 2; order_pdep++)
      {
        int ii = 2*factor;
        XField1D xfld( ii, 0, 1 );
        std::vector<int> cellGroups = {0};
        std::vector<int> interiorTraceGroup = {0};

        ////////////////////////////////////////////////////////////////////////////////////////
        // Allocate our Burgers parameter field
        ////////////////////////////////////////////////////////////////////////////////////////
        Field_CG_Cell<PhysD1, TopoD1, ArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Lagrange);
        Field_EG_Cell<PhysD1, TopoD1, ArrayQ> pde_qpfld(pde_qfld, order_pdep, BasisFunctionCategory_Lagrange);
        Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> pde_lgfld(xfld, order_pde, BasisFunctionCategory_Lagrange,
                                                                 PDEBCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups));
        pde_qfld = 0;
        pde_qpfld = 0;
        pde_lgfld = 0;
        ///////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // Create our AES to solve our PDE
        ////////////////////////////////////////////////////////////////////////////////////////
        QuadratureOrder quadratureOrder( xfld, - 1 );
        std::vector<Real> tol = {5e-12, 5e-12, 5e-12};
        DiscretizationVMSD stab;
        stab.setNitscheOrder(order_pde);
        PDEPrimalEquationSetClass PDEPrimalEqSet(xfld, pde_qfld, pde_qpfld, pde_lgfld, pde,
                                                 stab, quadratureOrder, ResidualNorm_L2_DOFWeighted, tol,
                                                 cellGroups, interiorTraceGroup, PyPDEBCList, PDEBCBoundaryGroups);
        ///////////////////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////////////////////
        // Newton Solver
        ////////////////////////////////////////////////////////////////////////////////////////
        PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
        UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
        LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

        NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
        NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
        NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
        NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1000;
        NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

        NewtonSolverParam::checkInputs(NewtonSolverDict);
        NewtonSolver<SystemMatrixClass> SolverBurgers( PDEPrimalEqSet, NewtonSolverDict );
        ///////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // Set initial guess
        ////////////////////////////////////////////////////////////////////////////////////////
        for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );

        SystemVectorClass q(PDEPrimalEqSet.vectorStateSize());
        SystemVectorClass sln(PDEPrimalEqSet.vectorStateSize());
        PDEPrimalEqSet.fillSystemVector(q);
        sln = q;
        ///////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // SOLVE
        ////////////////////////////////////////////////////////////////////////////////////////
        SolveStatus status = SolverBurgers.solve(q, sln);
        BOOST_CHECK( status.converged );
        ///////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // Text
        ////////////////////////////////////////////////////////////////////////////////////////
        std::cout << "P = " << order_pde << " ii = " << ii << " Pp = " << order_pdep
                  << " (" << (status.converged ? "true" : "false") << "): \\bar{p}: ";
        ////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // L2 solution error
        ////////////////////////////////////////////////////////////////////////////////////////
        typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionNDClass> ErrorClass;
        typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
        typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

        NDErrorClass fcnError(solnExact);
        ErrorIntegrandClass errorIntegrand(fcnError, cellGroups);

        ArrayQ SquareError = 0;
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_Galerkin( errorIntegrand, SquareError ),
            xfld, pde_qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
        Real norm = sqrt(SquareError);
        std::cout << "L2 = " << norm << " ";
        ////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // Output functional
        ////////////////////////////////////////////////////////////////////////////////////////
        typedef OutputCell_SolutionSquared<PDEClass> OutputSolutionSquared;
        typedef OutputNDConvertSpace<PhysD1, OutputSolutionSquared> NDOutputSolutionSquared;
        typedef IntegrandCell_Galerkin_Output<NDOutputSolutionSquared> IntegrandOutputSolutionSquared;

        NDOutputSolutionSquared outputSolutionSquared;
        IntegrandOutputSolutionSquared fcnOutputSolutionSquared( outputSolutionSquared, cellGroups );

        Real SolutionSquared = 0.0;
        const Real SolutionSquaredExact = 1.2;
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_Galerkin( fcnOutputSolutionSquared, SolutionSquared ),
            xfld, pde_qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
        std::cout << "Output = " << SolutionSquared << " ";
        std::cout << "OutputError = " << SolutionSquared - SolutionSquaredExact << " ";
        ////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // \bar{q} + \hat{q}
        ////////////////////////////////////////////////////////////////////////////////////////
        int orderCombined = std::max(order_pde,order_pdep);
        Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldCombined(xfld, orderCombined, BasisFunctionCategory_Lagrange);
        Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qpfld_temp(xfld, orderCombined, BasisFunctionCategory_Lagrange);
        pde_qfld.projectTo(qfldCombined);
        pde_qpfld.projectTo(qpfld_temp);
        for (int i = 0; i < qfldCombined.nDOF(); i++)
        {
          qfldCombined.DOF(i) += qpfld_temp.DOF(i);
        }
        ////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // Text
        ////////////////////////////////////////////////////////////////////////////////////////
        std::cout << " \\bar{p} + \\hat{p}: ";
        ////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // L2 solution error
        ////////////////////////////////////////////////////////////////////////////////////////
        SquareError = 0;
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_Galerkin( errorIntegrand, SquareError ),
            xfld, qfldCombined, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
        norm = sqrt(SquareError);
        std::cout << "L2 = " << norm << " ";
        ////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////
        // Output functional
        ////////////////////////////////////////////////////////////////////////////////////////
        SolutionSquared = 0.0;
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_Galerkin( fcnOutputSolutionSquared, SolutionSquared ),
            xfld, qfldCombined, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
        std::cout << "Output = " << SolutionSquared << " ";
        std::cout << "OutputError = " << SolutionSquared - SolutionSquaredExact << " ";
        ///////////////////////////////////////////////////////////////////////////////////////

//        {
//          string filename = "Primal_P"
//                         + std::to_string(order_pde)
//                         + "_Pp"
//                         + std::to_string(order_pdep)
//                         + "_G"
//                         + std::to_string(ii)
//                         + ".dat";
//          output_gnuplot( pde_qfld, filename_base + filename );
//        }
//
//        {
//          string filename = "Combined_P"
//                         + std::to_string(order_pde)
//                         + "_Pp"
//                         + std::to_string(order_pdep)
//                         + "_G"
//                         + std::to_string(ii)
//                         + ".dat";
//          output_gnuplot( qfldCombined, filename_base + filename );
//        }
//
//        {
//          string filename = "Perturbation_P"
//                         + std::to_string(order_pde)
//                         + "_Pp"
//                         + std::to_string(order_pdep)
//                         + "_G"
//                         + std::to_string(ii)
//                         + ".dat";
//          output_gnuplot( pde_qpfld, filename_base + filename );
//        }

        std::cout << std::endl;
        pde_qpfld.dump();
        std::cout << std::endl;
      }
      std::cout << std::endl;
    } //grid refinement loop
  } //order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
