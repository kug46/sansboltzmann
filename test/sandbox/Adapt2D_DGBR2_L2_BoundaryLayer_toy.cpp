// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_L2_BoundaryLayer_btest
// Testing of the MOESS framework on the 2D boundary layer test-case found in Masa's thesis

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include <iostream>

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

using namespace std;

//#define TEST_MODE

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_L2_BoundaryLayer_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_L2_BoundaryLayer_Triangle )
{

  typedef ScalarFunction2D_BoundaryLayer SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  // Grid
  int ii = 3;
  int jj = ii;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, jj );

  std::vector<int> global_btracegroup_list = {0,1,2,3};

  int order = 1;

  // PDE
  AdvectiveFlux2D_Uniform adv( 0, 0 );

  Real nu = 0;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(1.0, 0.0, 0.0);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
           BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.BoundaryLayer;
  solnArgs[NDSolutionExact::ParamsType::params.epsilon] = 0.01;
  solnArgs[NDSolutionExact::ParamsType::params.beta] = pow(2.0, order + 1);
  solnArgs[NDSolutionExact::ParamsType::params.p] = order;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;
//  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = solnArgs;

  PyDict PyBCList;
  PyBCList["BCNone"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNone"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-12, 1e-12};

  NDOutputClass fcnOutput(solnExact);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  //Set up pyrite file
  pyrite_file_stream pyriteFile("IO/Adaptation/Adaptation2D_BoundaryLayer.txt", 1e-10, 1e-10, pyrite_file_stream::check);
  pyriteFile << std::setprecision(16) << std::scientific;

  //--------ADAPTATION LOOP--------

  int maxIter = 20;
  Real targetCost = 4000;

  std::string filename_base = "tmp/";
  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  const int quadOrder = -1; //2*(order + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrand);

  pGlobalSol->setSolution(0.0);

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename = filename_base + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

  std::string adjfld_filename = filename_base + "adjfld_a0.plt";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
  }

  fadapthist.close();

  //Writing out element distribution...

  typedef PhysD2 PhysDim;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef ElementXFieldClass::VectorX ArrayX;

  //Read the final mesh
  std::string filename = "tmp/fefloa_out_a" + std::to_string(maxIter) + ".mesh";
  XField_libMeshb<PhysD2, TopoD2> xfld_final(world, filename);

  XFieldCellGroupType xfldCellGroup = xfld_final.getCellGroup<Triangle>(0);
  ElementXFieldClass xfldElem(xfldCellGroup.basis() );

  std::string distfile = "tmp/h_distribution.txt";
  fstream fdist( distfile, fstream::out );

  for (int elem = 0; elem < xfldCellGroup.nElem(); elem++)
  {
    xfldCellGroup.getElement(xfldElem, elem);

    ArrayX centroid;
    xfldElem.coordinates(Triangle::centerRef, centroid);

    MatrixSym M = 0;
    xfldElem.impliedMetric(M);

    Real h0 = pow(M(0,0),-0.5);
    Real h1 = pow(M(1,1),-0.5);

    fdist<<centroid[0]<<","<<centroid[1]<<","<<h0<<","<<h1<<std::endl;

#ifdef TEST_MODE
//    pyriteFile << centroid[0] << centroid[1] << h0 << h1 << std::endl;
#endif
  }

  fdist.close();


#ifdef TEST_MODE
  //Delete the written files

  for (int iter = 0; iter < maxIter+1; iter++ )
  {
    std::string mesh = "tmp/mesh_a" + std::to_string(iter) + ".mesh";
    std::string metric = "tmp/metric_request_a" + std::to_string(iter) + ".sol";
    std::string output_mesh = "tmp/fefloa_out_a" + std::to_string(iter) + ".mesh";
    std::string output_metric = "tmp/fefloa_out_a" + std::to_string(iter) + ".sol";
    std::string output_solb = "tmp/fefloa_out_a" + std::to_string(iter) + ".mesh.qua.solb";
    std::string tecplot = "tmp/qfld_a" + std::to_string(iter) + ".plt";

    std::remove(mesh.c_str());
    std::remove(metric.c_str());
    std::remove(output_mesh.c_str());
    std::remove(output_metric.c_str());
    std::remove(output_solb.c_str());
    std::remove(tecplot.c_str());
  }

  std::remove("tmp/h_distribution.txt");
  std::remove(adapthist_filename.c_str());
  std::remove("tmp/fefloa_log.txt");
#endif

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
