// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
//  Solve2D_DGBR2_Triangle_NavierStokes_Poiseuille_btest
// testing of 2-D DGBR2 for NS on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "Field/ProjectSoln/InterpolateFunctionCell_Continuous.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA3D.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/SolutionFunction_RANSSA3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/BCRANSSA3D.h"
#include "pde/NS/OutputEuler3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/BCParameters.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/VMSD/FunctionalCell_VMSD.h"
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"

#include "Discretization/VMSDBR2/SolutionData_VMSD_BR2.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_VMSD_BR2.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_Local_VMSD_BR2.h"
#include "Adaptation/MOESS/SolverInterface_VMSD_BR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_EG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_BoundaryTrace.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"


#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//#define USEVMSD0
//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_VMSDBR2_Triangle_RANSSA_OjedaMMS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve3D_VMSDBR2_Triangle_OjedaMMS )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_RANSSA3D_OjedaMMS<TraitsSizeRANSSA, TraitsModelRANSSAClass>  SolnType;
  typedef SolnNDConvertSpace<PhysD3,SolnType> NDSolnType;

  typedef BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef FieldTuple<Field<PhysD3, TopoD3, Real>, XField<PhysD3, TopoD3>, TupleClass<>> ParamFieldTupleType;


  typedef AlgebraicEquationSet_VMSD_BR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                       AlgEqSetTraits_Sparse, ParamFieldTupleType> PrimalEquationSetClass;

  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef OutputEuler3D_VelocitySquared<NDPDEClass> OutputClass;
  //typedef OutputEuler3D_Density<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_VMSD_Output<NDOutputClass, NDPDEClass> OutputIntegrandClass;

  typedef ParamType_Distance ParamBuilderType;

  typedef SolutionData_VMSD_BR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef SolverInterface_VMSD_BR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  mpi::communicator world;

  std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;

  GasModel gas(gasModelDict);
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // INFLOW CONDITIONS TO MATCH OJEDA'S MMS

  const Real mu0 = 0.0001; // FOR RE = 100

  Real rho0 = 1.0;
  Real u0 = 0.2;
  Real v0 = 0.1;
  Real w0 = 0.15;
  Real p0 = 1.0;

//  Real ntRef = 1;
  Real ntRef = mu0/rho0;
  Real nt0 = mu0/rho0/ntRef;

  const Real Prandtl = 0.72;

  ViscosityModelType visc(mu0); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  typedef SolnType::ParamsType SlnParams;
  PyDict slnArgs;
  slnArgs[SlnParams::params.gasModel] = gasModelDict;
  slnArgs[SlnParams::params.ntRef] = ntRef;

  std::shared_ptr<NDSolnType> solnptr( new NDSolnType(slnArgs) );

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, ntRef, solnptr );

  //  Velocity Magnitude Error
  NDOutputClass fcnOutput(pde);
  OutputIntegrandClass outputIntegrand( pde, fcnOutput, {0} );

  // DGBR2 discretization
#ifdef USEVMSD0
  DiscretizationVMSD disc(VMSD0);
#else
  DiscretizationVMSD disc;
#endif

  // BC
  slnArgs[BCEuler3D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Function.Name] =
      BCEuler3D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Function.OjedaRANSMMS;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCEuler3D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Function] = slnArgs;
  BCSoln[BCEuler3D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Characteristic] = true;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  PyDict PyBCList;
  PyBCList["BCSoln"] = BCSoln;

  BCBoundaryGroups["BCSoln"] = {0,1,2,3,4,5};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  int ii = 4;
  int jj = 4;
  int kk = 4;
  int order = 1;

  disc.setNitscheOrder(order);

  // READ IN ARGUMENTS
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    order = std::stoi(argv[1]);
  if (argc >= 3)
  {
    ii = jj = std::stoi(argv[2]);
    kk = ii;
  }
  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)


  //PETSC PARAMETERS
  int ordering = 0;
  int fill = 0;
  int krylov = 50;
  if (argc >= 4)
    ordering = std::stoi(argv[3]);
  if (argc >= 5)
    fill = std::stoi(argv[4]);
  if (argc >= 6)
    krylov = std::stoi(argv[5]);

  if (world.rank() == 0 )
  {
    std::cout << ", orderL,H = " << order;
    std::cout << ", powerL,H = " << ii << std::endl;
  }

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

//    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  //  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  if (ordering==0)
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  else if (ordering==1)
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  else
  {
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
//    PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order
  }

  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = fill;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.PreconditionerSide] = SLA::PreconditionerASMParam::params.PreconditionerSide.Right;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-16;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = krylov;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = true;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
//  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-30;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 40000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 4000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  if (world.rank() == 0 )
  {
    std::cout << "Linear solver: PETSc" << std::endl;
  }
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-5;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 10;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  // Easier PTC for subsequent solves
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  string filename_base = "tmp/OJEDARANS3D/VMSDBR20/";

  boost::filesystem::path base_dir(filename_base);
  if ( not boost::filesystem::exists(base_dir) )
    boost::filesystem::create_directories(base_dir);

  string filename2 = filename_base + "VMSD_Errors.txt";
  fstream foutsol;

  string filename = filename_base + "_P";
  filename += to_string(order);
  filename += "_X";
  filename += to_string(ii);

  foutsol.open( filename2, fstream::app );

  // grid:
  XField3D_Box_Tet_X1 pxfld( world, ii, jj, kk );
  ArrayQ q0 = 0;
  pde.setDOFFrom( q0, SAnt3D<DensityVelocityPressure3D<Real>>({rho0, u0, v0, w0, p0, nt0}) );

  Field_CG_Cell<PhysD3, TopoD3, Real> distfld(pxfld, 1, BasisFunctionCategory_Lagrange);
  for (int n = 0; n < distfld.nDOF(); n++)
    distfld.DOF(n) = 0.001;

  const int quadOrder = 2*(order + 1); // Is this high enough?

  //Solution data
  PyDict paramDict;
  std::shared_ptr<SolutionClass> pGlobalSol;

#ifdef USEVMSD0
  pGlobalSol = std::make_shared<SolutionClass>( (distfld, pxfld), pde, disc, order, order, order, order,
                                               BasisFunctionCategory_Lagrange,
                                               BasisFunctionCategory_Legendre,
                                               BasisFunctionCategory_Lagrange,
                                               active_boundaries, paramDict);
#else
  pGlobalSol = std::make_shared<SolutionClass>( (distfld, pxfld), pde, disc, order, order, order, order,
                                               BasisFunctionCategory_Lagrange,
                                               BasisFunctionCategory_Lagrange,
                                               BasisFunctionCategory_Lagrange,
                                               active_boundaries, paramDict);
#endif

  pGlobalSol->setSolution(q0);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrand);

  int nDOFtotal = pGlobalSol->primal.qfld.nDOFnative();


  int nElemTotal = 0;
#ifdef SANS_MPI
  boost::mpi::reduce(*pxfld.comm(), pxfld.nElem(), nElemTotal, std::plus<int>(), 0 );
#else
  nElemTotal = pxfld.nElem();
#endif
  if (world.rank() == 0 )
  {
    std::cout << "nElem: " << nElemTotal;
    std::cout << ", nDOF: " << nDOFtotal << "\n";
    std::cout << "projecting...\n";
  }

  // Sortof L2 projection of exact solution as initial guess
  for_each_CellGroup<TopoD3>::apply(  InterpolateFunctionCell_Continuous(*solnptr, {0}), (pxfld, pGlobalSol->primal.qfld) );

  if (world.rank() == 0 )
    std::cout << "done...\n";

  pInterface->solveGlobalPrimalProblem();

  Real rhoExact = 0.073882304138978247155;
  Real rhoInt = pInterface->getOutput();

  if (world.rank() == 0 )
    std::cout  <<  "Density Error: " << std::setprecision(15) << fabs(rhoInt - rhoExact) << "\n";
#if 0
  std::string filename_q = filename_base + "qfld_P" + std::to_string(order) +"_X" + std::to_string(ii) + ".plt";

  output_Tecplot(pGlobalSol->primal.qfld, filename_q);
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
