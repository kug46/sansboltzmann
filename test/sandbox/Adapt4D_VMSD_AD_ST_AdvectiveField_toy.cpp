// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt4D_VMSD_AD_ST_AdvectiveField_toy
// testing of 3D+T adaptation on a random field advection-diffusion problem


#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>               // to make filesystems
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"               // saw a comment that this gives us Reals

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AnalyticFunction/ScalarFunction4D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime3D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime3D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime3D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime3D.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "pde/ForcingFunction3D_MMS.h"

#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldSpacetime_DG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"
#include "Field/FieldSpacetime_EG_Cell.h"
#include "Field/FieldSpacetime_CG_BoundaryTrace.h"

// not sure if this is sufficient include...
#include "unit/UnitGrids/XField4D_Box_Ptope_X1.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
//#include "Meshing/avro/MesherInterface_avro.h"
//#include "Meshing/EGADS/EGModel.h"
#include "Meshing/avro/XField_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#endif

#define BOUNDARY_LAYER
// #define SINUSOIDAL_DECAY
// #define NEUMANN_ON

using namespace std;

// explicitly instantiate the classes for correct coverage info
namespace SANS
{

}

using namespace SANS;

#if 0
// in development

BOOST_AUTO_TEST_SUITE(Adapt4D_VMSD_AD_ST_AdvectiveField_toy_test_suite)

BOOST_AUTO_TEST_CASE(Adapt4D_VMSD_AD_ST_AdvectiveField_toy_test_case)
{

  // parse inputs

  int argc= boost::unit_test::framework::master_test_suite().argc;
  char ** argv= boost::unit_test::framework::master_test_suite().argv;

  int pSamples= -1;               // p to be used
  int dofReqSamples= -1;          // dof to be requested

  const int lastn= 5;             // adaptation iterations to print

  int maxIter= -1;                // adaptation iterations to do

  // specify dof and p and maxIter
  if (argc == 4)
  {
    dofReqSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    maxIter= std::stoi(argv[3]);
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // just specify dof and p
  else if (argc == 3)
  {
    dofReqSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    maxIter= 10;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // just specify dof
  else if (argc == 2)
  {
    dofReqSamples= std::stoi(argv[1]);
    pSamples= 1;
    maxIter= 10;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // specify nothing
  else
  {
    dofReqSamples = 125;
    pSamples= 1;
    maxIter= 5;
    std::cout << std::endl << "No input parsed! Defaults:" << std::endl;
  }

  bool DG_CONV = false;

  SANS_ASSERT(dofReqSamples > 0);
  SANS_ASSERT(pSamples > 0);
  SANS_ASSERT(maxIter >= 0);
  SANS_ASSERT(maxIter >= lastn);

  Real ax= 0.5; // 0.0;
  Real ay= 0.5; // 0.0;
  Real az= 0.0; // 0.0;
  Real nu= 1.0/64.;
  // Real Lx= 1.0;
  // Real Ly= 1.0;
  // Real Lz= 1.0;

  Real sigma_inflow= 0.025;
  Real x0_inflow= 0.05;
  Real y0_inflow= 0.05;
  Real z0_inflow= 0.;
  Real tMin_inflow= 0.;
  Real tMax_inflow= 1.;
  Real f_inflow= 5.;

  Real sigma_city= 0.01;
  Real x0_city= 0.9;
  Real y0_city= 0.95;
  Real z0_city= 0.;

  std::cout << "\tdofReqSamples: " << dofReqSamples << std::endl;
  std::cout << "\tpSamples: " << pSamples << std::endl;
  std::cout << "\tmaxIter: " << maxIter << std::endl << std::endl;

  std::cout << "Physical problem:" << std::endl;
  std::cout << "\tsquare channel?" << std::endl;

  std::string filename_base= "tmp/AD4D_ST_VMSD_RandomField/";
  std::string filename_case;

  typedef AdvectiveFlux3D_Uniform AdvectionModel;
  typedef ViscousFlux3D_Uniform DiffusionModel;
  typedef Source3D_UniformGrad SourceModel;

  // shortcut for the appropriate PDE object for our 3D physics specified
  typedef PDEAdvectionDiffusion<PhysD3, AdvectionModel, DiffusionModel,
      SourceModel> PDEClass;
  // shortcut for the 3D+T conversion
  typedef PDENDConvertSpaceTime<PhysD3, PDEClass> NDPDEClass;

  typedef ScalarFunction3D_OscillatingGaussian BCFunction;
  typedef ScalarFunction3D_Gaussian WFunction;

  typedef BCAdvectionDiffusion3DVector<AdvectionModel, DiffusionModel> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_WeightedSolution<PDEClass, WFunction> OutputClass;
  // typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_VMSD_Output<NDOutputClass, NDPDEClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  // the solution to the problem
  typedef SolutionData_VMSD<PhysD4, TopoD4, NDPDEClass,
  ParamBuilderType> SolutionClass;
  // parameters to the solver
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  // algebraic equation set
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpaceTime,
  BCVector, AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
  // solver interface, everything it needs to run the solution
  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass,
  OutputIntegrandClass> SolverInterfaceClass;

  // mpi
  mpi::communicator world;

  // CREATE ADVECTION DIFFUSION PDE

  AdvectionModel advection(ax, ay, az);   // temporary concession...
  DiffusionModel diffusion(nu, 0, 0, 0, nu, 0, 0, 0, nu);
  SourceModel source(0.0, 0.0, 0.0, 0.0);

  // create the PDE
  NDPDEClass pde(advection, diffusion, source);

  // create BC solution
  PyDict bcArgs;

  bcArgs[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function.Name]=
      BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function.OscillatingGaussian;
  bcArgs[ScalarFunction3D_OscillatingGaussian::ParamsType::params.sigma]= sigma_inflow;
  bcArgs[ScalarFunction3D_OscillatingGaussian::ParamsType::params.x0]= x0_inflow;
  bcArgs[ScalarFunction3D_OscillatingGaussian::ParamsType::params.y0]= y0_inflow;
  bcArgs[ScalarFunction3D_OscillatingGaussian::ParamsType::params.z0]= z0_inflow;
  bcArgs[ScalarFunction3D_OscillatingGaussian::ParamsType::params.tMin]= tMin_inflow;
  bcArgs[ScalarFunction3D_OscillatingGaussian::ParamsType::params.tMax]= tMax_inflow;
  bcArgs[ScalarFunction3D_OscillatingGaussian::ParamsType::params.f]= f_inflow;

  ScalarFunction3D_OscillatingGaussian bcFcn(bcArgs);

  // create weighting function
  PyDict wArgs;
  wArgs[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function.Name]=
      BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function.Gaussian;
  wArgs[ScalarFunction3D_Gaussian::ParamsType::params.sigma]= sigma_city;
  wArgs[ScalarFunction3D_Gaussian::ParamsType::params.x0]= x0_city;
  wArgs[ScalarFunction3D_Gaussian::ParamsType::params.y0]= y0_city;
  wArgs[ScalarFunction3D_Gaussian::ParamsType::params.z0]= z0_city;

  ScalarFunction3D_Gaussian wFcn(wArgs);

  // CREATE BOUNDARY CONDITIONS

  PyDict BCSoln_timeIC;
  BCSoln_timeIC[BCParams::params.BC.BCType]= BCParams::params.BC.Dirichlet_mitState;
  BCSoln_timeIC[BCAdvectionDiffusionParams<PhysD3, BCTypeDirichlet_mitStateParam>::params.qB]= 0.0;
  // BCSoln_timeIC[BCParams::params.BC.BCType]= BCParams::params.BC.TimeIC_Function;
  // BCSoln_timeIC[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;

  PyDict BCSoln_timeOut;
  BCSoln_timeOut[BCParams::params.BC.BCType]= BCParams::params.BC.TimeOut;

  PyDict BCSoln_DirichletZero;
  BCSoln_DirichletZero[BCParams::params.BC.BCType]= BCParams::params.BC.Dirichlet_mitState;
  BCSoln_DirichletZero[BCAdvectionDiffusionParams<PhysD3, BCTypeDirichlet_mitStateParam>::params.qB]= 0.0;

  PyDict BCSoln_Inflow;
  BCSoln_Inflow[BCParams::params.BC.BCType]= BCParams::params.BC.Function_mitState;
  BCSoln_Inflow[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Function]= bcArgs;
  BCSoln_Inflow[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.SolutionBCType]=
      BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.SolutionBCType.Neumann;
  BCSoln_Inflow[BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params.Upwind]= false;

  PyDict BCSoln_None;
  BCSoln_None[BCParams::params.BC.BCType]= BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCSoln_timeIC"]= BCSoln_timeIC;
  PyBCList["BCSoln_timeOut"]= BCSoln_timeOut;
  PyBCList["BCSoln_DirichletZero"]= BCSoln_DirichletZero;
  PyBCList["BCSoln_Inflow"]= BCSoln_Inflow;
  PyBCList["BCSoln_None"]= BCSoln_None;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // VMSD discretization
  DiscretizationVMSD stab;

  enum ResidualNormType ResNormType= ResidualNorm_L2;
  std::vector<Real> tol= {1.e-10, 1.e-10, 1.e-10};

  // create output functional to adapt with

  NDOutputClass fcnOutput(wFcn);
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, true);

  // SETUP NONLINEAR SOLVER (PROBABLY UNNEEDED FOR LINEAR PROBLEM)

  // nonlinear solver dicts
  PyDict SolverContinuationDict;
  PyDict NonlinearSolverDict;
  PyDict NewtonSolverDict;
  PyDict AdjLinearSolverDict;
  PyDict LinearSolverDict;
  PyDict LineUpdateDict;
  // PyDict UMFPACKDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 )
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  //  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= UMFPACKDict;
#endif

  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver]= SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  // LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  // NewtonSolverDict[NewtonSolverParam::params.LinearSolver]= UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]= NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation]= NonlinearSolverDict;

  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);

  // REFINEMENT LOOP
  // adaptively refine

  Real targetCost= dofReqSamples;

  if (DG_CONV) // Modify the dof counts to match the DG element counts - Should be much faster to run and show the same
  {
    Real nDOFperCell_DG = (pSamples + 1)*(pSamples + 2)*(pSamples + 3)*(pSamples + 4)/24;
    Real nDOFperCell_CG = nDOFperCell_DG;
    // //nDOFperCell_CG -= (4 - 1./5); // the node dofs are shared by 20
    // nDOFperCell_CG -= (5 - 5./90); // the node dofs are shared by 90
    // nDOFperCell_CG -= (6 - 1)*std::max(0,(pSamples-1)); // if there are edge dofs they are shared by 6
    // nDOFperCell_CG -= (4 - 2)*std::max(0,(pSamples-1)*(pSamples-2)/2); // if there are face dofs they are shared by 2

    /*
     * 120 regular pentatopes can share a point so vertex dofs are shared by 120
     * 20 regular pentatopes can share an edge so edge DOFs are shared by 20
     * 6 regular pentatopes can share a triangle so area DOFs are shared by 6
     * 2 regular pentatopes can share a tetrahedron, so face DOFs are shared by 2
     * cell dofs are solely owned
     *
     * 5 nodes,  1/120 dof shares per node, 1/24 dof share per elem,  1 dof per node
     * 10 edges, 1/20 edge shares per node, 1/2 edge share per elem,  (order - 1) dof per edge (interior)
     * 10 areas, 1/6 area share per node,   5/3 area share per elem,  (order - 2)*(order - 1)/2 dofs per area (interior)
     * 5 traces, 1/2 trace share per node,  5/2 trace share per elem, (order - 3)*(order - 2)*(order - 1)/6 dofs per trace (interior)
     */

    nDOFperCell_CG -= ( 5. - 1./24);
    nDOFperCell_CG -= (10. - 1./2)*std::max(0, (pSamples - 1));
    nDOFperCell_CG -= (10. - 5./3)*std::max(0, (pSamples - 1)*(pSamples - 2)/2);
    nDOFperCell_CG -= ( 5. - 5./2)*std::max(0, (pSamples - 1)*(pSamples - 2)*(pSamples - 3)/6);

    targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
  }

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_base);

  // norm data
  int orderVec= 0;
  Real hDOFVec[lastn]= {0.};
  int nElemVec[lastn]= {0};
  int nDOFVec[lastn]= {0};
  Real normVec[lastn]= {0.0};

  // output file
  std::string convhist_filename= filename_base + "test.convhist";
  fstream convhist;

  // prep mesh adaptation tools
  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction]= 1.0;
  MOESSDict[MOESSParams::params.CostModel]= MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity]= MOESSParams::VerbosityOptions::Progressbar;

  MOESSDict[MOESSParams::params.LocalSolve]= MOESSParams::params.LocalSolve.Edge;


  MOESSDict[MOESSParams::params.MetricOptimization]= MOESSParams::params.MetricOptimization.SANS;
  MOESSDict[MOESSParams::params.ImpliedMetric]= MOESSParams::params.ImpliedMetric.Optimized;

  PyDict MesherDict;
#ifdef SANS_AVRO
  MesherDict[MeshAdapterParams<PhysD4, TopoD4>::params.Mesher.Name]= MeshAdapterParams<PhysD4, TopoD4>::params.Mesher.avro;
  MesherDict[avroParams::params.Curved]= false;
#else
  SANS_DEVELOPER_EXCEPTION("IN 4D LAND PHILIP'S CODE IS KING.");
#endif

  std::vector<int> cellGroups= {0};

  // CREATE INITIAL GRID

  // 3x3x3x3
  const int ii= std::max(3, (int) std::floor(1./pSamples*(pow(targetCost, 1./PhysD4::D) - 1)));
  const int jj= ii;
  const int kk= jj;
  const int mm= kk;

  std::cout << "initial grid created with ii= jj= kk= mm= " << ii << "." << std::endl;

  // create pointer
  std::shared_ptr<XField<PhysD4, TopoD4>> pxfld;

  // generate grid
#ifdef SANS_AVRO

  using avro::coord_t;
  using avro::index_t;

  avro::Context context;

  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld0(world, {ii, jj, kk, mm});

  std::shared_ptr<avro::Model> model= std::make_shared<avro::Model>(xfld0.context(), "tesseract");
  model->addBody(xfld0.body_ptr(), false);

  // copy the mesh into the domain and attach the geometry
  pxfld = std::make_shared<XField_avro<PhysD4,TopoD4>>(xfld0, model);

#else
  pxfld= std::make_shared<XField4D_Box_Ptope_X1>(world, ii, jj, kk, mm,
                                                 0, 1,
                                                 0, 1,
                                                 0, 1,
                                                 0, 1);
#endif

  // assign local variables

  const int order= pSamples;

  const int iXmin = XField4D_Box_Ptope_X1::iXmin;
  const int iXmax = XField4D_Box_Ptope_X1::iXmax;
  const int iYmin = XField4D_Box_Ptope_X1::iYmin;
  const int iYmax = XField4D_Box_Ptope_X1::iYmax;
  const int iZmin = XField4D_Box_Ptope_X1::iZmin;
  const int iZmax = XField4D_Box_Ptope_X1::iZmax;
  const int iTmin = XField4D_Box_Ptope_X1::iWmin;
  const int iTmax = XField4D_Box_Ptope_X1::iWmax;

  // define the boundarygroups for each BC
  BCBoundaryGroups["BCSoln_timeIC"]= {iTmin};                 // time inflow
  BCBoundaryGroups["BCSoln_timeOut"]= {iTmax};                // time outflow
  BCBoundaryGroups["BCSoln_DirichletZero"]= {iXmin, iYmin};   // zero contaminant inflow
  BCBoundaryGroups["BCSoln_Inflow"]= {iZmin};                // flux input
  BCBoundaryGroups["BCSoln_None"]= {iXmax, iYmax, iZmax};     // free outflow

  std::vector<int> active_boundaries= BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  std::vector<int> interiorTraceGroups;

  for (int iTG= 0; iTG < pxfld->nInteriorTraceGroups(); iTG++)
    interiorTraceGroups.push_back(iTG);

  // set the order in the stabilization
  stab.setNitscheOrder(order);


  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, stab,
                                               order, order,
                                               order + 1, order + 1,
                                               BasisFunctionCategory_Lagrange,
                                               BasisFunctionCategory_Lagrange,
                                               BasisFunctionCategory_Lagrange,
                                               active_boundaries);

  const int quadOrder= 2*(order + 1);    // somehow this means 2*(order + 1)?

  // create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict,
                                                      AdjLinearSolverDict, outputIntegrand);

  // set initial solution
  //  pGlobalSol->setSolution(solnExact, cellGroups);
  pGlobalSol->setSolution(0.0);

  filename_case= filename_base + "dof" + stringify(dofReqSamples) + "p" + stringify(order) + "/";

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_case);

  std::string adapthist_filename = filename_case + "test.adapthist";
  fstream fadapthist;

  if (world.rank() == 0)
  {
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.hasTrueOutput]= false;

  MeshAdapterParams<PhysD4, TopoD4>::checkInputs(AdaptDict);

  MeshAdapter<PhysD4, TopoD4> mesh_adapter(AdaptDict, fadapthist);

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  orderVec= order;

  // ADAPTATION LOOP

  int iprinted= 0;

  stab.setNitscheOrder(order);

  for (int iter= 0; iter < maxIter + 1; iter++)
  {
    if (world.rank() == 0)
      std::cout << "-----Adaptation Iteration " << iter << "-----" << std::endl;

    // compute error estimates
    pInterface->computeErrorEstimates();

    // perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD4, TopoD4>> pxfldNew;
    pxfldNew= mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    // reset interior trace for adapted mesh!
    interiorTraceGroups.clear();
    for ( int iT = 0; iT < pxfldNew->nInteriorTraceGroups(); iT++)
      interiorTraceGroups.push_back(iT);

    // make a new global solution
    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, stab,
                                                    order, order,
                                                    order + 1, order + 1,
                                                    BasisFunctionCategory_Lagrange,
                                                    BasisFunctionCategory_Lagrange,
                                                    BasisFunctionCategory_Lagrange,
                                                    active_boundaries);

    // fill it by performing L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    // generate a new solver interface
    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict,
                                                           LinearSolverDict, outputIntegrand);

    // update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld= pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    std::string qfld_init_filename = filename_case + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    //        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    Real squareError= 0;

#ifdef SANS_MPI
    int nDOFtotal = 0;
    boost::mpi::all_reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>());

    // count the number of elements possessed by this processor
    int nElem = 0;
    for (int elem = 0; elem < pxfld->nElem(); elem++ )
      if (pxfld->getCellGroupGlobal<Pentatope>(0).associativity(elem).rank() == world.rank())
        nElem++;

    int nElemtotal = 0;
    boost::mpi::all_reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>());
#else
    int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
    int nElemtotal = pxfld->nElem();
#endif

    Real norm= squareError;

    if (world.rank() == 0)
    {
      if (iprinted < lastn)
      {
        cout << "iprinted= " << iprinted << " < lastn= " << lastn << endl;
        hDOFVec[iprinted]= 1.0/pow(nDOFtotal, 1.0/((Real) PhysD4::D));
        nElemVec[iprinted]= nElemtotal;
        nDOFVec[iprinted]= nDOFtotal;
        normVec[iprinted]= sqrt(norm);
        iprinted++;
      }
      else
      {
        for (int k= 1; k < lastn; k++)
        {
          cout << "moving k= " << k << " to (k - 1)= " << k - 1 << endl;
          hDOFVec[k - 1]= hDOFVec[k];
          nElemVec[k - 1]= nElemVec[k];
          nDOFVec[k - 1]= nDOFVec[k];
          normVec[k - 1]= normVec[k];
        }
        cout << "crowning with k= " << lastn - 1 << endl;
        hDOFVec[lastn - 1]= 1.0/pow(nDOFtotal, 1.0/((Real) PhysD4::D));
        nElemVec[lastn - 1]= nElemtotal;
        nDOFVec[lastn - 1]= nDOFtotal;
        normVec[lastn - 1]= sqrt(norm);
      }

      std::cout << std::endl << "INNER LOOP EXIT with " << nDOFtotal << " DOFs and " <<
          nElemtotal << " elements." << std::endl << std::endl;
    }
  }

  // open and write to convhist file
  if (world.rank() == 0)
  {

    bool fileStarted= boost::filesystem::exists(convhist_filename);

    convhist.open(convhist_filename, fstream::app);
    BOOST_REQUIRE_MESSAGE(convhist.good(), "Error opening file: " + convhist_filename);

    if (!fileStarted)
    {
      convhist << "VARIABLES=";
      convhist << "\"p\"";
      convhist << ", \"DOFrequested\"";
      convhist << ", \"1/pow(DOF, 1.0/4.0)\"";
      convhist << ", \"Nelem\"";
      convhist << ", \"Ndof\"";
      convhist << ", \"L2 error\"";
      convhist << std::endl;
    }

    convhist << std::setprecision(16) << std::scientific;

    fadapthist.close();

    for (int k= 0; k < iprinted; k++)
    {
      // outputfile
      convhist << orderVec;
      convhist << ", " << (Real) targetCost;
      convhist << ", " << hDOFVec[k];
      convhist << ", " << (Real) nElemVec[k];
      convhist << ", " << (Real) nDOFVec[k];
      convhist << ", " << normVec[k];
      convhist << endl;
    }

    convhist.close();
  }

}

BOOST_AUTO_TEST_SUITE_END()

#endif
