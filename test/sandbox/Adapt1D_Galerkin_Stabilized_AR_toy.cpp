// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt1D_Galerkin_AD_BoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
//#include "pde/OutputCell_WeightedSolution.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"

#include "Adaptation/MeshAdapter.h"
#include "Meshing/Embedding/MesherInterface_Embedding.h"
#include "Meshing/EPIC/XField_PX.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"

#include "Meshing/XField1D/XField1D.h"

#include "Field/output_grm.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt1D_Galerkin_AD_BoundaryLayer_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt1D_AD_BoundaryLayer_Triangle )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

//  typedef ScalarFunction1D_VarExp3 WeightFcn; // Change this to change output weighting type
//  typedef ScalarFunction1D_BL WeightFcn; // Change this to change output weighting type
  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD1, TopoD1, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Grid
  int ii = 3;

//  std::shared_ptr<XField<PhysD1, TopoD1>> pxfld = std::make_shared<XField1D>( ii , 0 , 1 );

  Real a = 1.0;

  // PDE
  AdvectiveFlux1D_Uniform adv( a );

  ViscousFlux1D_Uniform visc( 0 );

  Source1D_UniformGrad source(5.0, 0.0);

  // Create a solution dictionary
//  PyDict solnArgs;
//  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
//  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
//  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;
//  solnArgs[NDSolutionExact::ParamsType::params.c] = 1.0;
//  solnArgs[NDSolutionExact::ParamsType::params.s] = -1.0;
//
//  NDSolutionExact solnExact( solnArgs );
//
//  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
//  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source );

  // BC


#if 0
  PyDict BL;
  BL[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.Function.BL;
  BL[SolutionExact::ParamsType::params.a] = a;
  BL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln_Bottom;
  BCSoln_Bottom[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.Function] = BL;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.A] = 1.0;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.B] = 0.0;

  PyDict BCSoln_Right;
  BCSoln_Right[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.Function] = BL;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.A] = 1.0;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.B] = 0.0;

  PyDict BCSoln_Top;
  BCSoln_Top[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.Function] = BL;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.A] = 1.0;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.B] = 0.0;

  PyDict BCSoln_Left;
  BCSoln_Left[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.Function] = BL;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.A] = 1.0;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.B] = 0.0;

#elif 0
  PyDict BL;
  BL[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.Function.BL;
  BL[SolutionExact::ParamsType::params.a] = a;
  BL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln_Bottom;
  BCSoln_Bottom[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.Function] = BL;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.A] = 1.0;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0.0;

  PyDict BCSoln_Right;
  BCSoln_Right[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.Function] = BL;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.A] = 1.0;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0.0;

  PyDict BCSoln_Top;
  BCSoln_Top[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.Function] = BL;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.A] = 1.0;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0.0;

  PyDict BCSoln_Left;
  BCSoln_Left[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.Function] = BL;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.A] = 1.0;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0.0;

#else

  PyDict BCSoln_Right;
  BCSoln_Right[BCParams::params.BC.BCType] = BCParams::params.BC.None;
//  BCSoln_Right[BCAdvectionDiffusionParams<PhysD1,BCTypeLinearRobin_mitLG>::params.A] = 1;
//  BCSoln_Right[BCAdvectionDiffusionParams<PhysD1,BCTypeLinearRobin_mitLG>::params.B] = 0;
//  BCSoln_Right[BCAdvectionDiffusionParams<PhysD1,BCTypeLinearRobin_mitLG>::params.bcdata] = 0;

  PyDict BCSoln_Left;
  BCSoln_Left[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD1,BCTypeLinearRobin_mitLG>::params.A] = 1;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD1,BCTypeLinearRobin_mitLG>::params.B] = 0;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD1,BCTypeLinearRobin_mitLG>::params.bcdata] = 1;

#endif

  PyDict PyBCList;
  PyBCList["BC_Right"] = BCSoln_Right;
  PyBCList["BC_Left"] = BCSoln_Left;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BC_Right"] = {1};
  BCBoundaryGroups["BC_Left"] = {0};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Galerkin Stabilization

  StabilizationMatrix stab(StabilizationType::SUPG, TauType::Glasby);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-10, 1e-10};

  //Output functional
//  Real aa = 3.0;
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
//
//  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

//#define XFIELD_IN
//#define XFIELD_OUT

  std::vector<Real> targetCosts = {40};//,500,1000,2000,4000,8000,16000,32000};//,64000,128000,256000,512000};

//  std::vector<Real> targetCosts = {1000};//,64000,128000,256000,512000};

  std::shared_ptr<XField<PhysD1, TopoD1>> pxfld;

  //--------ADAPTATION LOOP--------
  for (int order = 1; order <= 3; order++)
  {
    stab.setStabOrder(order +1);
    stab.setNitscheOrder(order);

    pxfld = std::make_shared<XField1D>( ii );
    for (auto it = targetCosts.begin(); it != targetCosts.end(); ++it)
    {
      Real targetCost;

      targetCost = *it;
#ifdef XFIELD_IN
      mpi::communicator world;
      std::string xfld_filename = "tmp/AD_1D/DG_P" + std::to_string(order) + "/xfld_a0.grm";
      pxfld = std::make_shared<XField_PX<PhysD1,TopoD1>>(world,xfld_filename);
      const Real targetCost = 1000;
#else
      // Comment out if you want to start with the previous meshes
      // i.e. this in effect cost sequences up, the adapts should then converge faster
//      pxfld = std::make_shared<XField1D_Box_UnionJack_Triangle_X1>( ii, jj );
#endif

//      switch (order)
//      {
//      case 1 : targetCost = 2100; break;
//      case 2 : targetCost = 1200; break;
//      case 3 : targetCost = 800; break;
//      break;
//      }

      const int maxIter = 20;

#ifdef XFIELD_IN
      std::string filename_base = "tmp/AD_1D/CG_DG_P" + std::to_string(order) + "/";
#else
      // to make sure folders have a consistent number of zero digits
      const int string_pad = 6;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
      std::string filename_base = "tmp/AD_1D/CG_" + int_pad + "_P" + std::to_string(order) + "/";
#endif
      boost::filesystem::create_directories(filename_base);

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD1, TopoD1>::params.Mesher.Name] = MeshAdapterParams<PhysD1, TopoD1>::params.Mesher.embedding;

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.FilenameBase] = filename_base;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.dumpStepMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.dumpRateMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.hasTrueOutput] = true;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.TrueOutput] = 0.9875;

      MeshAdapterParams<PhysD1, TopoD1>::checkInputs(AdaptDict);

      MeshAdapter<PhysD1, TopoD1> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                   active_boundaries, stab);

      const int quadOrder = 2*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      pGlobalSol->setSolution(0.0);

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      std::string qfld_filename = filename_base + "qfld_a0.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
      //  std::string lgld_filename = filename_base + "lgfld_a0.plt";
      //  output_Tecplot( pGlobalSol->primal.lgfld, lgld_filename );
      //
      //  return;
      std::string adjfld_filename = filename_base + "adjfld_a0.plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

#ifdef XFIELD_OUT
      std::string xfld_filename = filename_base + "xfld_a0.grm";
      output_grm( *pxfld, xfld_filename);
#endif


      for (int iter = 0; iter < maxIter+1; iter++)
      {
        std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        //Compute error estimates
        pInterface->computeErrorEstimates();

        //Perform local sampling and adapt mesh
        std::shared_ptr<XField<PhysD1, TopoD1>> pxfldNew;

#ifdef XFIELD_IN
        pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter); // so that adapthist fills

        xfld_filename = "tmp/AD_1D/DG_P" + std::to_string(order) + "/xfld_a" + std::to_string(iter+1) + ".grm";
        pxfldNew = std::make_shared<XField_PX<PhysD1,TopoD1>>(world,xfld_filename);
#else
        pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);
#endif

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                        BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                        active_boundaries, stab);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, LinearSolverDict,
                                                               outputIntegrand);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld = pxfldNew;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();

        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
#ifdef XFIELD_OUT
        std::string xfld_filename = filename_base + "xfld_a" + std::to_string(iter+1) + ".grm";
        output_grm( *pxfld, xfld_filename);
#endif
      }

      fadapthist.close();
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
