// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_HDG_Triangle_NavierStokes_FlatPlate_toy
// testing of 2-D HDG for NS for flat plate

//#define SANS_FULLTEST
//#define SANS_VERBOSE
//#define IMPORTMESH

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include <boost/filesystem.hpp> // to automagically make the directories

#include "tools/SANSnumerics.h"     // Real

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/HDG/FunctionalBoundaryTrace_Dispatch_HDG.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/ProjectSoln_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_OutputWeightRsd_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectSolnTrace_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_CG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_InteriorFieldTraceGroup.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/EPIC/XField_PX.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_HDG_Triangle_NavierStokes_Joukowski_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_HDG_Triangle_Joukowski )
{

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  // Drag integrand
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, HDG> IntegrandOutputClass;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_HDG< NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD2, TopoD2>> AlgebraicEquationSet_PTCClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  std::vector<Real> tol = {1e-9, 1e-9, 1e-9};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.5;
  const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real pRef = 1.0;                            // pressure
  const Real tRef = pRef/(rhoRef*R);

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = cRef*Mach;                              // velocity scale

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  // stagnation temperature, pressure
  //const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  //const Real TtRef = tRef*Gamma;
  //const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // HDG discretization
#if 1
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient);
#else
  Real refl = 0.1;
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient, refl);
#endif

  // Drag integrand
  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass fcnOut( outputFcn, {0} );

  //BCS

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;

  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);

  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;

  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

//  PyDict BCOut;
//  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
//  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;

  PyDict PyBCList;
//  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
//  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
//  BCBoundaryGroups["BCSymmetry"] = {0,2,4};
#ifdef IMPORTMESH
  BCBoundaryGroups["BCNoSlip"] = {0,1};
  BCBoundaryGroups["BCIn"] = {2,3};
#else
  BCBoundaryGroups["BCNoSlip"] = {0};
  BCBoundaryGroups["BCIn"] = {1,2};
#endif

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
//  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Set up Newton Solver
  PyDict NewtonSolverDict, LinSolverDict, NonLinearSolverDict, NewtonLineUpdateDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
#else
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  //  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 10;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] =  true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  Real invCFL = 10;
  Real invCFL_min = 0.0;
  Real invCFL_max = 1000.0;
  Real CFLDecreaseFactor = 0.5;
  Real CFLIncreaseFactor = 2.0;
  Real CFLPartialStepDecreaseFactor = 0.9;
//#else
  NewtonSolverParam::checkInputs(NewtonSolverDict);
//
  int powermin = 0;
  int powermax = 4;
//
  int ordermin = 1;
  int ordermax = 3;


  fstream fout;
  string filename_base = "tmp/LAMJOUK/EDG";
  boost::filesystem::create_directories(filename_base);

  filename_base += "/EDG_";

  string filename2 = filename_base + "Drag.txt";

  for (int order = ordermin; order <= ordermax; order++)
  {

  // loop over grid resolution: 2^power
//    int power = 0;
    for (int power = powermin; power <= powermax; power++)
    {

#ifndef IMPORTMESH
    string filein = "grids/JOUKOWSKI/Joukowski_Laminar_Challenge_tri_ref";
    filein += to_string(power);
    filein += "_Q4.grm";
#else
    string filein = "/home2/CGvsDG/LAMJOUK/AGLS/JOUK_04000_P2/mesh_a25_outQ3.grm";

#endif

    if (world.rank() == 0)
    {
      fout.open( filename2, fstream::app );
    }

      XField_PX<PhysD2, TopoD2> xfld(world, filein);

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Lagrange);
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Lagrange);
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Lagrange);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      lgfld(xfld, order-1, BasisFunctionCategory_Lagrange, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

//      const int nDOFPDE = qfld.nDOF();
//      const int nDOFAUX = afld.nDOF();
//      const int nDOFINT = qIfld.nDOF();

      // Set the uniform initial condition
      ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

      qfld = q0;
      afld = 0.0;
      qIfld = q0;
      lgfld = 0;

      //////////////////////////
      //SOLVE HIGHER P SYSTEM
      //////////////////////////
      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      QuadratureOrder quadratureOrder( xfld, 2*order+1 );

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                         cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups );
      AlgebraicEquationSet_PTCClass AlgEqSetPTC(xfld, qfld, pde, quadratureOrder, {0}, PrimalEqSet);

#if 1
        // Create the pseudo time continuation class
        PseudoTime<SystemMatrixClass>
        PTC(invCFL, invCFL_max, invCFL_min, CFLDecreaseFactor, CFLIncreaseFactor, CFLPartialStepDecreaseFactor,
            NonLinearSolverDict, AlgEqSetPTC, true);

        BOOST_CHECK( PTC.iterate(1000) );
#else

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0;

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      Solver.solve(ini,sln);

      PrimalEqSet.setSolutionField(sln);
#endif
      // Monitor drag
      Real Drag = 0.0;

      PrimalEqSet.dispatchBC().dispatch_HDG(
        FunctionalBoundaryTrace_Dispatch_HDG(fcnOut, xfld, qfld, afld, qIfld,
                                             quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(), Drag ) );

      // drag coefficient
//      Real Cd = Drag / (0.5*rhoRef*qRef*qRef*lRef);


      int nDOFq = qIfld.nDOFnative();
      if (world.rank() == 0)
      {
        fout << order << " " << to_string(nDOFq);
        fout << " " << std::setprecision(16) << Drag << "\n";
        fout.close();
      }



#if 1
      // Tecplot dump grid
      string filename = filename_base + "_P";
      filename += to_string(order);
      filename += "_X";
      filename += to_string(power);
      filename += ".plt";
      output_Tecplot( qfld, filename );
      std::cout << "Wrote Tecplot File\n";
#endif


    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
