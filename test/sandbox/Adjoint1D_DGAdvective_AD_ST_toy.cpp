// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adjoint1D_DGAdvective_AD_ST_btest
// testing of 1-D DG adjoint with advection-diffusion

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/ForcingFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Adjoint1D_DGAdvective_AD_ST_test_suite )

//----------------------------------------------------------------------------//
// solution: P1 thru P3
// Lagrange multiplier: continuous P1 thru P3
// exact solution: double BL
BOOST_AUTO_TEST_CASE( Adjoint1D_DGAdvective_AD_ST )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_None,
                                Source1D_None > PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  // PDE
  AdvectiveFlux1D_Uniform adv( 1.0 );

  ViscousFlux1D_None visc;
  Source1D_None source;

  NDPDEClass pde(adv, visc, source);

  // BCs

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 0.0;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 1.0;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichletR[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCDirichletR[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 0.0;
  BCDirichletR[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.5;

  PyDict PyBCList;
  PyBCList["DirichletL"] = BCDirichletL;
  PyBCList["DirichletR"] = BCDirichletR;
  PyBCList["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletL"] = {3}; //Left boundary
  BCBoundaryGroups["DirichletR"] = {0}; //Bottom boundary
  BCBoundaryGroups["None"] = {1,2}; //Right and top boundary

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // norm data
//  Real hVec[10];
//  Real hDOFVec[10];   // 1/sqrt(DOF)
//  Real normVec[10];   // L2 error
//  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/Solve1D_DGAdvective_AD_ST.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/Solve/Solve1D_DGAdvective_AD_ST_FullTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/Solve/Solve1D_DGAdvective_AD_ST_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
//  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 1;
#else
  int ordermax = 1;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
//    indx = 0;

    // loop over grid resolution: 30*factor
    int ii, jj;
    int factormin = 1;
#ifdef SANS_FULLTEST
    int factormax = 1;
#else
    int factormax = 1;
#endif
    for (int factor = factormin; factor <= factormax; factor++)
    {
      ii = 4*factor;
      jj = 4*factor;

      // grid:
//      XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 1, 0, 1, true );
      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj);

      // solution: Hierarchical, C0
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_adj(xfld, order, BasisFunctionCategory_Legendre);
      qfld = 0.0;
      qfld_adj = 0.0;
//      output_Tecplot( qfld, "tmp/qfld_init.plt" );
      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 0
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups)  );
#elif 0
      std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
      std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre,active_BGroup_list);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_adj(xfld, order-1, BasisFunctionCategory_Legendre,active_BGroup_list);
//      std::cout<<active_BGroup_list[0]<<","<<active_BGroup_list[1]<<","<<active_BGroup_list[2]<<std::endl;
#endif

      lgfld = 0.0;
      lgfld_adj = 0.0;
      const int nDOFBC = lgfld_adj.nDOF();

      std::cout<<"DOF: "<<(nDOFPDE + nDOFBC)<<std::endl;

      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-11, 1e-11};

      //--------PRIMAL SOLVE------------

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

      // primal solution vector
      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      PrimalEqSet.fillSystemVector(q);

      // residual
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0.0;
      PrimalEqSet.residual(q, rsd);

      // primal solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(PrimalEqSet.vectorStateSize());

      solver.solve(rsd, dq);

      // update primal solution
      q -= dq;
      PrimalEqSet.setSolutionField(q);


      // check that the residual is zero
      rsd = 0;
      PrimalEqSet.residual(q, rsd);

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

//      cout << sqrt(rsdPDEnrm) << "\t" << sqrt(rsdBCnrm) <<endl;


      //--------ADJOINT SOLVE------------

      PrimalEquationSetClass AdjointEqSet(xfld, qfld_adj, lgfld_adj, pde, quadratureOrder,
                                          ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);
      AdjointEqSet.setSolutionField(q);

      // adjoint (right hand side)
      SystemVectorClass rhs(AdjointEqSet.vectorEqSize());
      rhs = 0.0;

      // quadrature rule
      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_Galerkin( outputIntegrand, rhs(0) ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // adjoint solve
      SLA::UMFPACK<SystemMatrixClass> solverAdj(AdjointEqSet, SLA::TransposeSolve);
      SystemVectorClass adj_sln(AdjointEqSet.vectorStateSize());
      solverAdj.solve(rhs, adj_sln);

      AdjointEqSet.setSolutionField(adj_sln);

#if 0
      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jacT, fout );
//      fstream fout00( "tmp/jac00.mtx", fstream::out );
//      cout << "btest: global jac00" << endl;  WriteMatrixMarketFile( jac(0,0), fout00 );
//      fstream fout10( "tmp/jac10.mtx", fstream::out );
//      cout << "btest: global jac10" << endl;  WriteMatrixMarketFile( jac(1,0), fout10 );
//      fstream fout01( "tmp/jac01.mtx", fstream::out );
//      cout << "btest: global jac01" << endl;  WriteMatrixMarketFile( jac(0,1), fout01 );
#endif

      // L2 solution error
//      int quadratureOrder[2] = {-1, -1};    // max
//      ArrayQ SquareError = 0;
//      FunctionalCell<TopoD1>::integrate( fcnErr, qfld, quadratureOrder, 1, SquareError );
//      Real norm = SquareError;

//      hVec[indx] = 1./ii;
//      hDOFVec[indx] = 1./sqrt(nDOFPDE);
//      normVec[indx] = sqrt( norm );
//      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/adjoint_DGAdv_AD_ST_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld_adj, filename );
      output_Tecplot( qfld, "tmp/primal.plt" );
#endif

    } //grid refinement loop

#if 0
    // Tecplot output
    resultFile << "ZONE T=\"DG BR2 P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }
#endif

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
//    std::cout<<resultFile.str()<<std::endl;
  } //order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
