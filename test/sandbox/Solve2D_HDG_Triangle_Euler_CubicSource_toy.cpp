// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_HDG_Triangle_Euler_CubicSource_toy
// testing of 2-D HDG for Euler on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"
#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_HDG_Triangle_Euler_CubicSource_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_HDG_Triangle_Bump10 )
{
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef OutputEuler2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_HDG_Output<NDOutputClass> IntegrandSquareErrorClass;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_HDG< NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE
  const Real gamma = 1.4;
  const Real R = 0.4;

  // reference state (freestream)
  const Real Mach = 0.1;

  // const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure

  // stagnation temperature, pressure
  const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  const Real TtRef = tRef*Gamma;
  const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Momentum);

  // BC
  // Create a BC dictionary
  PyDict BCSymmetry;

#if 1
// PX STYLE BC:
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;
#else
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry;
#endif

  // PyDict BCIn;
  // BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt;

  // BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.sSpec] = 0.0;
  // BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.HSpec] = 3.563;
  // BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.qtSpec] = 0;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aoaRef;


  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure>::params.pSpec] = pRef;


  PyDict state;
  state[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
  state[DensityVelocityPressure2DParams::params.rho] = rhoRef;
  state[DensityVelocityPressure2DParams::params.u] = uRef;
  state[DensityVelocityPressure2DParams::params.v] = vRef;
  state[DensityVelocityPressure2DParams::params.p] = pRef;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCEuler2DFullStateParams<PDEClass::VariableType2DParams>::params.Characteristic] = true;
  BCFullState[BCEuler2DFullStateParams<PDEClass::VariableType2DParams>::params.StateVector] = state;



  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;
  PyBCList["BCFullState"] = BCFullState;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2}; //bottom and top
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {3};
  BCBoundaryGroups["BCFullState"] = {};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  //typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  Real sExact = 1.0;
  NDOutputClass fcnOutput(pde, sExact);
  IntegrandSquareErrorClass fcnErr(fcnOutput, {0});

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );


  // norm data
//  Real normVec[10];   // L2 error
//  int indx = 0;

//  fstream fout( "tmp/cubicsource_results.txt", fstream::out );

  int ii, jj;
  int powermin = 1;
  int powermax = 1;

  int ordermin = 1;
  int ordermax = 1;

  // loop over grid resolution: 2^power
  for (int power = powermin; power <= powermax; power++)
  {

    int order0 = 0;

    jj = pow( 2, power );
    ii = 3*jj; //small grid!
    Real tau = 0.1; //10% bump
    int fieldorder = 3;

    // grid: 2D Cubic Source Bump w/ Q3 mesh
    XField2D_CubicSourceBump_Xq xfld(ii, jj, tau, fieldorder);

    // DG solution field
    //solution at inlet
    ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

    //////////////////////
    // SET UP P0 solve
    ///////////////////////


    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld0(xfld, order0, BasisFunctionCategory_Legendre);
    Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld0(xfld, order0, BasisFunctionCategory_Legendre);
    Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld0(xfld, order0, BasisFunctionCategory_Legendre);
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
    lgfld0(xfld, order0, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

    // Set the initial condition
    qfld0 = q0;
    afld0 = 0;
    qIfld0 = q0;
    lgfld0 = 0;

    ////////////
    //SOLVE P0 SYSTEM
    ////////////

    QuadratureOrder quadratureOrder( xfld, -1 );
    std::vector<Real> tol = {1e-11, 1e-11, 1e-11};

    PrimalEquationSetClass PrimalEqSet0(xfld, qfld0, afld0, qIfld0, lgfld0, pde, disc, quadratureOrder, tol,
                                        {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

    NewtonSolver<SystemMatrixClass> Solver0( PrimalEqSet0, NewtonSolverDict );

    SystemVectorClass ini0(PrimalEqSet0.vectorStateSize());
    SystemVectorClass sln0(PrimalEqSet0.vectorStateSize());
    SystemVectorClass slnchk0(PrimalEqSet0.vectorStateSize());
    SystemVectorClass rsd0(PrimalEqSet0.vectorEqSize());
    rsd0 = 0;

    PrimalEqSet0.fillSystemVector(ini0);
    sln0 = ini0;

#if 1
    PrimalEquationSetClass::SystemNonZeroPattern nz(PrimalEqSet0.matrixSize());
    PrimalEqSet0.jacobian(sln0, nz);
    SystemMatrixClass jac(nz);

    PrimalEqSet0.jacobian(sln0, jac);

    fstream fout( "tmp/jac_HDG_Euler.mtx", fstream::out );
    cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
    //    fstream fout2( "tmp/rsd_HDG_1D.mtx", fstream::out );
    //    for (int i = 0; i<4 ; i++)
    //      for (int k = 0; k<nDOFPDE; k++)
    //        fout2 << rsd[i][k][0] << "\n";
#endif


    bool solved;
    solved = Solver0.solve(ini0,sln0).converged;
    BOOST_CHECK(solved);
    PrimalEqSet0.setSolutionField(sln0);

    rsd0 = 0;
    PrimalEqSet0.residual(sln0, rsd0);
    std::vector<std::vector<Real>> rsdNorm = PrimalEqSet0.residualNorm(rsd0);

    bool converged = PrimalEqSet0.convergedResidual(rsdNorm);

    if (!converged)
    {
      std::vector<std::vector<Real>> rsdNorm = PrimalEqSet0.residualNorm(rsd0);
      PrimalEqSet0.printDecreaseResidualFailure(rsdNorm, std::cout);
    }
    BOOST_CHECK(converged);

#if 0
    // Tecplot dump grid
    string filename0 = "tmp/HDG_EulerBump_P";
    filename0 += to_string(order0);
    filename0 += "_Q";
    filename0 += to_string(fieldorder);
    filename0 += "_";
    filename0 += to_string(ii);
    filename0 += "x";
    filename0 += to_string(jj);
    filename0 += ".plt";
    output_Tecplot( qfld0, filename0 );
    std::cout << "Wrote Tecplot File\n";
#endif

    //LOOP OVER ORDER, with P0 initialization
    for (int order = ordermin; order <= ordermax; order++)
    {

      ////////////////////////////////////
      // PROJECT P0 SOLUTION TO HIGHER P
      ///////////////////////////////////

      if (order > 0)
      {

        Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
        Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
        Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
        Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
        lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

        // Set the initial condition
        qfld0.projectTo(qfld);
        afld0.projectTo(afld);
        qIfld0.projectTo(qIfld);

        // Lagrange multiplier:
        lgfld = 0;

        //////////////////////////
        //SOLVE HIGHER P SYSTEM
        //////////////////////////

        PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                           {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

        NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

        SystemVectorClass ini(PrimalEqSet.vectorStateSize());
        SystemVectorClass sln(PrimalEqSet.vectorStateSize());
        SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
        SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
        rsd = 0;

        PrimalEqSet.fillSystemVector(ini);
        sln = ini;

        solved = Solver.solve(ini,sln).converged;
        BOOST_REQUIRE(solved);
        PrimalEqSet.setSolutionField(sln);

        // check that the residual is zero
        rsd = 0;
        PrimalEqSet.residual(sln, rsd);
        std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

        converged = PrimalEqSet.convergedResidual(rsdNorm);

        if (!converged)
        {
          std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);
          PrimalEqSet.printDecreaseResidualFailure(rsdNorm, std::cout);
        }
        BOOST_CHECK(converged);

        // Monitor Entropy Error
        Real EntropySquareError = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_HDG( fcnErr, EntropySquareError ), xfld, (qfld, afld),
            quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
        cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( EntropySquareError );
        //      if (indx > 1)
        //        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << endl;
#endif
#if 0
        // Tecplot dump grid
        string filename = "tmp/slnDG_EulerBump_P";
        filename += to_string(order);
        filename += "_Q";
        filename += to_string(fieldorder);
        filename += "_";
        filename += to_string(ii);
        filename += "x";
        filename += to_string(jj);
        filename += ".plt";
        output_Tecplot( qfld, filename );
#endif

      }
      else
      {
        // Monitor Entropy Error
        Real EntropySquareError = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_HDG( fcnErr, EntropySquareError ), xfld, (qfld0, afld0),
            quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
        cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( EntropySquareError );
        //      if (indx > 1)
        //        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << endl;
#endif

      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
