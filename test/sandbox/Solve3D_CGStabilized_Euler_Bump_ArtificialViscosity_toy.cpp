// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve3D_CGStabilized_Euler_Bump_ArtificialViscosity_toy

//#define SANS_FULLTEST
//#define SANS_VERBOSE
#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>

#include <boost/filesystem.hpp> // to automagically make the directories

#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q3DConservative.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler3D.h"
#include "pde/NS/PDEEulermitAVDiffusion3D.h"
#include "pde/NS/BCEulermitAVSensor3D.h"
#include "pde/NS/Fluids3D_Sensor.h"
#include "pde/NS/EulerArtificialViscosityType.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField3D_GaussianBump_Xq.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_CGStabilized_Euler_Bump_ArtificialViscosity_toy )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve3D_CGStabilized_Euler_Bump_ArtificialViscosity_toy )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD3, PDEBaseClass> Sensor;
  typedef AVSensor_Source3D_PressureGrad<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEulermitAVSensor3DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler3D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
#if 0
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#endif
#else
  typedef OutputEuler3D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_DG ParamBuilderType;
  typedef GenHField_DG<PhysD3, TopoD3> GenHFieldType;

  typedef SolutionData_Galerkin_Stabilized<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {5e-7, 5e-7};

  RoeEntropyFix entropyFix = eVanLeer;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  bool isSteady = true;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;
  GasModel gas(gamma, R);

  const Real tRef = 1.0;          // temperature
  const Real pRef = 1.0;                  // pressure

  const Real Mach = 0.75;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;

//  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
//  const Real qRef = 0.1;                            // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real wRef = 0.0;

  // Create a BC dictionary
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  // Entropy, Stagnation Enthalpy, Tangential velocity
  const Real TtSpec = tRef * (1 + (gamma-1)/2 * Mach*Mach);
  const Real PtSpec = pRef * pow(TtSpec/ tRef, gamma/(gamma-1));

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCIn[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
  BCIn[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
  BCIn[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec]  = aoaRef;
  BCIn[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.bSpec]  = 0.0;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  const Real p_ratio = 0.9;
  BCOut[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = p_ratio*pRef;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {2,3,4,5};
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {0};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);


  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
if (world.rank() == 0 )
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  NewtonSolverParam::checkInputs(NewtonSolverDict);
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);
  //
  int ordermin = 1;
  int ordermax = 3;

  std::string filename_base = "tmp/Bump3D/";

  if (world.rank() == 0)
  {
    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directories(filename_base);
  }

  fstream foutputhist;
  std::ofstream resultFile;
  if (world.rank() == 0)
  {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Create tecplot file to put total enthalpy errors
    ////////////////////////////////////////////////////////////////////////////////////////
    resultFile.open( filename_base + "DragCG.plt", fstream::out );
    resultFile << "VARIABLES=";
    resultFile << "\"DOF\"";
    resultFile << ", \"1/sqrt(DOF)\"";
    resultFile << ", \"L2 error\"";
    resultFile << std::endl;
    resultFile << std::setprecision(16) << std::scientific;
    ////////////////////////////////////////////////////////////////////////////////////////
  }

  world.barrier();

  for (int order = ordermin; order <= ordermax; order++)
  {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Tecplot zone for this order
    ////////////////////////////////////////////////////////////////////////////////////////
    if (world.rank() == 0)
    {
      resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    }
    ////////////////////////////////////////////////////////////////////////////////////////

    PDEBaseClass pdeEulerAV(order, gas, interp, entropyFix);
    // Sensor equation terms
    Sensor sensor(pdeEulerAV);
    SensorAdvectiveFlux sensor_adv(0.0, 0.0, 0.0);
    SensorViscousFlux sensor_visc(order);
    SensorSource sensor_source(order, sensor);
    // AV PDE with sensor equation
    NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order,
                   gas, interp, entropyFix);

    // initial condition
    ArrayQ q0 = pde.setDOFFrom( AVVariable<DensityVelocityPressure3D, Real>({{rhoRef, uRef, vRef, wRef, pRef}, 0.0}) );

    StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby);

#ifndef BOUNDARYOUTPUT
    // Entropy output
    NDOutputClass fcnOutput(pde, 1.0);
#if 0
    OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
    OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, stab);
#endif
#else
    // Drag output
    NDOutputClass outputFcn(pde, 1., 0., 0.);
    OutputIntegrandClass outputIntegrand( outputFcn, {4} );
#endif

    stab.setStabOrder(order);
    stab.setNitscheOrder(order);

    for (int grid_index = 1; grid_index <= 3; grid_index++)
    {
      int ii = 6*pow(2, grid_index);
      int jj = 2*pow(2, grid_index);
      int kk = 2*pow(2, grid_index);
      const int grid_order = 4;

      std::shared_ptr<XField<PhysD3, TopoD3>> pxfld( new XField3D_GaussianBump_Xq( world, ii, jj, kk, grid_order ) );
      std::shared_ptr<GenHFieldType> phfld = std::make_shared<GenHFieldType>(*pxfld);

      std::vector<int> cellGroups;
      for ( int i = 0; i < pxfld->nCellGroups(); i++)
        cellGroups.push_back(i);
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pxfld), pde, order, order+1,
                                                   BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                   active_boundaries, stab);

      const int quadOrder = 3*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrand);

      pGlobalSol->setSolution(q0);

      pInterface->solveGlobalPrimalProblem();

      // Monitor Drag Error
      Real drag = pInterface->getOutput();


#ifdef SANS_MPI
      int nDOFtotal = 0;
      boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );
#else
      int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
#endif
      if (world.rank() == 0)
        std::cout << "P = " << order << " nDOF = " << nDOFtotal
                  << " : Drag = " << std::setprecision(16) << drag << endl;

      if (world.rank() == 0)
      {
        ////////////////////////////////////////////////////////////////////////////////////////
        // Output Error to tecplot file
        ////////////////////////////////////////////////////////////////////////////////////////
        resultFile << " " << nDOFtotal;
        resultFile << " " << 1.0/sqrt(nDOFtotal);
        resultFile << " " << drag;
        resultFile << std::endl;
        ////////////////////////////////////////////////////////////////////////////////////////
      }

#if 1
      // Tecplot dump grid
      std::string qfld_filename = filename_base + "slnCG_EulerBump_P";
      qfld_filename += to_string(order);
      qfld_filename += "_G";
      qfld_filename += to_string(grid_index);
      qfld_filename += ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
#endif
    }

  }

}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
