// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Read_msh_toy
// Testing of the MOESS framework on a 2D Navier-Stokes problem


#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <set>

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

#include "Topology/Dimension.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/RefCoordSolver.h"
#include "Field/DistanceFunction/DistanceFunction.h"

#include "Field/output_Tecplot.h"

#include "Meshing/EPIC/XField_PX.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/ugrid/XField_ugrid.h"
#include "Meshing/gmsh/XField_gmsh.h"
#include "Meshing/gmsh/WriteMesh_gmsh.h"

#include "BasisFunction/LagrangeDOFMap.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"


#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Convert_test_suite )

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Convert )
{
  mpi::communicator world;


  XField_ugrid<PhysD3, TopoD3> xfld( world, "tmp/hemisphere-cylinder/hc_tetra.4.b8.ugrid" );

  Field_DG_Cell<PhysD3, TopoD3,Real> qfld(xfld, 1, BasisFunctionCategory_Lagrange);

  qfld = 0;

  output_Tecplot(qfld, "tmp/hemisphere-cylinder/hc_tetra.4.dat");
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Convert_meshQ1_to_grmQ2 )
{
  mpi::communicator world;

  SANS_ASSERT(world.size() == 1);

  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;


  if (argc != 2)
  {
    std::cout << "Please specify the filename as the argument" << std::endl;
    return;
  }
  std::string filename = argv[1];

  std::string filebase;
  std::size_t len = filename.length();
  if (filename.substr(len-10) == ".lb8.ugrid" )
    filebase = filename.substr(0, len-10);
  else if (filename.substr(len-9) == ".b8.ugrid" )
    filebase = filename.substr(0, len-9);
  else if (filename.substr(len-6) == ".ugrid" )
    filebase = filename.substr(0, len-6);
  else
  {
    std::cout << "Unkinwn file name extension. Expexting one of .lb8.ugrid, .b8.ugrid, or .ugrid" << std::endl;
    return;
  }

  typedef XField<PhysD3, TopoD3>::VectorX VectorX;

  XField_ugrid<PhysD3, TopoD3> xfldQ1( world, filename );

  {
    Field_DG_Cell<PhysD3, TopoD3,Real> qfldQ1(xfldQ1, 1, BasisFunctionCategory_Lagrange);

    qfldQ1 = 0;

    output_Tecplot(qfldQ1, filebase+".q1.dat");
  }

  // construct a Q2 grid
  XField<PhysD3, TopoD3> xfldQ2(xfldQ1, 2, BasisFunctionCategory_Lagrange);

  std::ifstream rmpfile(filebase + ".rmp");

  XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>& xfldCellQ2 = xfldQ2.getCellGroup<Tet>(0);

  XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>::ElementType<> xfldElemQ2(xfldCellQ2.basis());

  ElementXField<PhysD3,TopoD1,Line> xfldElemLine(1, BasisFunctionCategory_Lagrange);

  RefCoordSolver<PhysD3, TopoD1, Line> solverLine(xfldElemLine);

  ElementXField<PhysD3,TopoD3,Tet>::RefCoordType sRefTet;
  ElementXField<PhysD3,TopoD1,Line>::RefCoordType sRefLine;

  int ntet, nprim, nhex;
  rmpfile >> ntet >> nprim >> nhex;
  SANS_ASSERT(nprim == 0);
  SANS_ASSERT(nhex == 0);
  SANS_ASSERT(ntet == xfldCellQ2.nElem());

  std::vector<DLA::VectorS<TopoD3::D,Real>> sRefLg;          // Lagrange point refernce coordinates
  LagrangeNodes<Tet>::get(2, sRefLg);

  std::vector<Real> phi(xfldCellQ2.nBasis());
  DLA::MatrixD<Real> V(xfldCellQ2.nBasis(), xfldCellQ2.nBasis()); // Vandermode matrix
  DLA::VectorD<VectorX> rhs(xfldCellQ2.nBasis());

#if 0
  ! tet
  !  o : Coarse grid nodes
  !  x : Removed fine-grid nodes
  !
  !                             3
  !                             o
  !                           . . .
  !                          .  .  .
  !                         .   .   .
  !                        .    .    .
  !                       .     .     .
  !                      .      .      .
  !                     .       .       .
  !                    .        .        .
  !                   .         x 9       .
  !                7 .          .          .8
  !                 x           .           x
  !                .            .            .
  !               .             .             .
  !              .              .              .
  !             .               .               .
  !            .                .                .
  !           .                 o 2               .
  !          .               .     .               .
  !         .        6   .             .  5         .
  !        .         x                    x          .
  !       .      .                           .        .
  !      .    .                                 .      .
  !     .                                           .   .
  !   0 o-----------------------x------------------------o 1
  !                            4
#endif

  //                             4  5  6  7  8  9
  std::vector<int> HCHFtoSANS = {9, 6, 7, 8, 5, 4};

  VectorX X, diff;

  for (int itet = 0; itet < ntet; itet++)
  {
    xfldCellQ2.getElement(xfldElemQ2, itet);

#if 0
    for (int n = 0; n < 4; n++)
    {
      xfldElemQ2.evalBasis(sRefLg[n], phi.data(), phi.size());
      for (std::size_t i = 0; i < phi.size(); i++)
        V(n,i) = phi[i];

      rhs[n] = xfldElemQ2.DOF(n);
    }
#endif

#if 1

    for (int n = 0; n < 6; n++)
    {
      rmpfile >> X[0] >> X[1] >> X[2];
      xfldElemQ2.DOF(HCHFtoSANS[n]) = X;
    }

#else

    // DOF 9, Nodes 0-1
    {
      rmpfile >> X[0] >> X[1] >> X[2];

      xfldElemLine.DOF(0) = xfldElemQ2.DOF(0);
      xfldElemLine.DOF(1) = xfldElemQ2.DOF(1);

      sRefLine = Line::centerRef;
      solverLine.solve(X, sRefLine);

      sRefTet[0] = sRefLine[0];
      sRefTet[1] = 0;
      sRefTet[2] = 0;

      //BOOST_CHECK_EQUAL(sRefTet[0], sRefLg[9][0]);
      //BOOST_CHECK_EQUAL(sRefTet[1], sRefLg[9][1]);
      //BOOST_CHECK_EQUAL(sRefTet[2], sRefLg[9][2]);

      xfldElemQ2.evalBasis(sRefTet, phi.data(), phi.size());
      for (std::size_t i = 0; i < phi.size(); i++)
        V(9,i) = phi[i];

      rhs[9] = X; //xfldElemQ2.DOF(9);
    }

    // DOF 6, Nodes 1-2
    {
      rmpfile >> X[0] >> X[1] >> X[2];

      xfldElemLine.DOF(0) = xfldElemQ2.DOF(1);
      xfldElemLine.DOF(1) = xfldElemQ2.DOF(2);

      sRefLine = Line::centerRef;
      solverLine.solve(X, sRefLine);

      sRefTet[0] = 1-sRefLine[0];
      sRefTet[1] = 1-sRefTet[0];
      sRefTet[2] = 0;

      //BOOST_CHECK_EQUAL(sRefTet[0], sRefLg[6][0]);
      //BOOST_CHECK_EQUAL(sRefTet[1], sRefLg[6][1]);
      //BOOST_CHECK_EQUAL(sRefTet[2], sRefLg[6][2]);

      xfldElemQ2.evalBasis(sRefTet, phi.data(), phi.size());
      for (std::size_t i = 0; i < phi.size(); i++)
        V(6,i) = phi[i];

      rhs[6] = X; //xfldElemQ2.DOF(6);
    }

    // DOF 7, Nodes 0-2
    {
      rmpfile >> X[0] >> X[1] >> X[2];

      xfldElemLine.DOF(0) = xfldElemQ2.DOF(0);
      xfldElemLine.DOF(1) = xfldElemQ2.DOF(2);

      sRefLine = Line::centerRef;
      solverLine.solve(X, sRefLine);

      sRefTet[0] = 0;
      sRefTet[1] = sRefLine[0];
      sRefTet[2] = 0;

      //BOOST_CHECK_EQUAL(sRefTet[0], sRefLg[7][0]);
      //BOOST_CHECK_EQUAL(sRefTet[1], sRefLg[7][1]);
      //BOOST_CHECK_EQUAL(sRefTet[2], sRefLg[7][2]);

      xfldElemQ2.evalBasis(sRefTet, phi.data(), phi.size());
      for (std::size_t i = 0; i < phi.size(); i++)
        V(7,i) = phi[i];

      rhs[7] = X; //xfldElemQ2.DOF(7);
    }

    // DOF 8, Nodes 0-3
    {
      rmpfile >> X[0] >> X[1] >> X[2];

      xfldElemLine.DOF(0) = xfldElemQ2.DOF(0);
      xfldElemLine.DOF(1) = xfldElemQ2.DOF(3);

      sRefLine = Line::centerRef;
      solverLine.solve(X, sRefLine);

      sRefTet[0] = 0;
      sRefTet[1] = 0;
      sRefTet[2] = sRefLine[0];

      //BOOST_CHECK_EQUAL(sRefTet[0], sRefLg[8][0]);
      //BOOST_CHECK_EQUAL(sRefTet[1], sRefLg[8][1]);
      //BOOST_CHECK_EQUAL(sRefTet[2], sRefLg[8][2]);

      xfldElemQ2.evalBasis(sRefTet, phi.data(), phi.size());
      for (std::size_t i = 0; i < phi.size(); i++)
        V(8,i) = phi[i];

      rhs[8] = X; //xfldElemQ2.DOF(8);
    }

    // DOF 5, Nodes 1-3
    {
      rmpfile >> X[0] >> X[1] >> X[2];

      xfldElemLine.DOF(0) = xfldElemQ2.DOF(1);
      xfldElemLine.DOF(1) = xfldElemQ2.DOF(3);

      sRefLine = Line::centerRef;
      solverLine.solve(X, sRefLine);

      sRefTet[0] = 1-sRefLine[0];
      sRefTet[1] = 0;
      sRefTet[2] = 1-sRefTet[0];

      //BOOST_CHECK_EQUAL(sRefTet[0], sRefLg[5][0]);
      //BOOST_CHECK_EQUAL(sRefTet[1], sRefLg[5][1]);
      //BOOST_CHECK_EQUAL(sRefTet[2], sRefLg[5][2]);

      xfldElemQ2.evalBasis(sRefTet, phi.data(), phi.size());
      for (std::size_t i = 0; i < phi.size(); i++)
        V(5,i) = phi[i];

      rhs[5] = X; //xfldElemQ2.DOF(5);
    }

    // DOF 4, Nodes 3-2
    {
      rmpfile >> X[0] >> X[1] >> X[2];

      xfldElemLine.DOF(0) = xfldElemQ2.DOF(3);
      xfldElemLine.DOF(1) = xfldElemQ2.DOF(2);

      sRefLine = Line::centerRef;
      solverLine.solve(X, sRefLine);

      sRefTet[0] = 0;
      sRefTet[2] = 1-sRefLine[0];
      sRefTet[1] = 1-sRefTet[2];

      //BOOST_CHECK_EQUAL(sRefTet[0], sRefLg[4][0]);
      //BOOST_CHECK_EQUAL(sRefTet[1], sRefLg[4][1]);
      //BOOST_CHECK_EQUAL(sRefTet[2], sRefLg[4][2]);

      xfldElemQ2.evalBasis(sRefTet, phi.data(), phi.size());
      for (std::size_t i = 0; i < phi.size(); i++)
        V(4,i) = phi[i];

      rhs[4] = X; //xfldElemQ2.DOF(4);
    }

    xfldElemQ2.vectorViewDOF() = DLA::InverseLUP::Solve(V, rhs);
#endif
    xfldCellQ2.setElement(xfldElemQ2, itet);
  }

  //std::cout << V << std::endl;

  //xfldCellQ2.getElement(xfldElemQ2, 0);
  //xfldElemQ2.dumpTecplot();
  //xfldCellQ2.getElement(xfldElemQ2, 27);
  //xfldElemQ2.dumpTecplot();

  xfldQ2.checkGrid();

  //std::cout << "Writing grid" << std::endl;

  //WriteMesh_gmsh(xfldQ1, filebase+".q1.msh");
  //WriteMesh_gmsh(xfldQ2, filebase+".q2.msh");

  Field_DG_Cell<PhysD3, TopoD3,Real> qfldQ2(xfldQ2, 1, BasisFunctionCategory_Lagrange);

  qfldQ2 = 0;

  output_Tecplot(qfldQ2, filebase+".q2.dat");
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
