// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGAdvective_Triangle_Euler_Gaussian_btest
// testing of 2-D DG Advective for Euler on triangle Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#if 1

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsBoltzmannD2Q9.h"
#include "pde/NS/PDEBoltzmannD2Q9.h"
#include "pde/NS/BCBoltzmannD2Q9.h"
#include "pde/NS/OutputBoltzmannD2Q9.h"
#include "pde/NS/SolutionFunction_BoltzmannD2Q9.h"

#include "pde/NS/QD2Q9PrimitiveDistributionFunctions.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_BoxPeriodic_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGAdvective_Triangle_Euler_Gaussian_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGAdvective_Triangle_Gaussian )
{
  // Set up Newton Solver
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

//  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeEntropy QType;
  typedef QTypePrimitiveDistributionFunctions QType;
  typedef TraitsModelBoltzmannD2Q9<QType, GasModel> TraitsModelBoltzmannD2Q9Class;
  typedef PDEBoltzmannD2Q9<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCBoltzmannD2Q9Vector<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> AlgebraicEquationSet_DGClass;

  typedef AlgebraicEquationSet_DGClass::BCParams BCParams;

  typedef SolutionFunction_BoltzmannD2Q9_Gaussian<TraitsSizeBoltzmannD2Q9, TraitsModelBoltzmannD2Q9Class> SolutionClass;
  typedef SolnNDConvertSpace<PhysD2, SolutionClass> SolutionNDClass;

  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> RKClass;

  //typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef AlgebraicEquationSet_DGClass::SystemVector SystemVectorClass;

#if 0
 /* //Output classes
  typedef OutputBoltzmannD2Q9_SolutionVariable<NDPDEClass> OutputSolutionVariableClass;
  typedef OutputNDConvertSpace<PhysD2, OutputSolutionVariableClass> NDOutputSolutionVariableClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputSolutionVariableClass> IntegrandSolutionVariableClass;

  typedef OutputBoltzmannD2Q9_SolutionTotalErrorSquare<NDPDEClass> OutputSolutionTotalErrorSquareClass;
  typedef OutputNDConvertSpace<PhysD2, OutputSolutionTotalErrorSquareClass> NDOutputSolutionTotalErrorSquareClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputSolutionTotalErrorSquareClass> IntegrandSolutionTotalErrorSquareClass;

  typedef OutputBoltzmannD2Q9_ConservativeVariable<NDPDEClass> OutputConservativeVariableClass;
  typedef OutputNDConvertSpace<PhysD2, OutputConservativeVariableClass> NDOutputConservativeVariableClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputConservativeVariableClass> IntegrandConservativeVariableClass;

  typedef OutputBoltzmannD2Q9_ConservativeTotalErrorSquare<NDPDEClass> OutputConservativeTotalErrorSquareClass;
  typedef OutputNDConvertSpace<PhysD2, OutputConservativeTotalErrorSquareClass> NDOutputConservativeTotalErrorSquareClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputConservativeTotalErrorSquareClass> IntegrandConservativeTotalErrorSquareClass;

  typedef OutputBoltzmannD2Q9_EntropyVariable<NDPDEClass> OutputEntropyVariableClass;
  typedef OutputNDConvertSpace<PhysD2, OutputEntropyVariableClass> NDOutputEntropyVariableClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputEntropyVariableClass> IntegrandEntropyVariableClass;

  typedef OutputBoltzmannD2Q9_EntropyTotalErrorSquare<NDPDEClass> OutputEntropyTotalErrorSquareClass;
  typedef OutputNDConvertSpace<PhysD2, OutputEntropyTotalErrorSquareClass> NDOutputEntropyTotalErrorSquareClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputEntropyTotalErrorSquareClass> IntegrandEntropyTotalErrorSquareClass;*/
#endif

  // PDE
  GlobalTime time;
  Real Tperiod, dt;
  int nsteps;
  int output_count, output_freq;

  time = 0.;
  Tperiod = 1.0;
  dt = 0.1;
  output_freq = 1;

  nsteps = int (Tperiod/dt);


  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;

  Real viscosity = 1e-2;
  Real csq = 1/3.;
  Real tau = viscosity/csq;
  Real Uadv = 0.00;
  Real Vadv = 0.00;
  //Real Uadv = 0.01;
  NDPDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                 PDEClass::BGK, tau,
                 PDEClass::AdvectionDiffusion, Uadv, Vadv );
  int nSol = NDPDEClass::N;

  // BC
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {};

  //Set up IC
  Real max_density = 5.;
  Real x_spread = 2., y_spread = 2.;
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.A] = max_density;
  solnArgs[SolutionClass::ParamsType::params.sigma_x_sq] = x_spread;
  solnArgs[SolutionClass::ParamsType::params.sigma_y_sq] = y_spread;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionNDClass solnExact(time, solnArgs);
#if 0
/*  //Set up output computation
  NDOutputSolutionVariableClass outQ0( pde, 0 );
  NDOutputSolutionVariableClass outQ1( pde, 1 );
  NDOutputSolutionVariableClass outQ2( pde, 2 );
  NDOutputSolutionVariableClass outQ3( pde, 3 );

  IntegrandSolutionVariableClass fcnErrQ0( outQ0, {0} );
  IntegrandSolutionVariableClass fcnErrQ1( outQ1, {0} );
  IntegrandSolutionVariableClass fcnErrQ2( outQ2, {0} );
  IntegrandSolutionVariableClass fcnErrQ3( outQ3, {0} );

  NDOutputConservativeVariableClass outU0( pde, 0 );
  NDOutputConservativeVariableClass outU1( pde, 1 );
  NDOutputConservativeVariableClass outU2( pde, 2 );
  NDOutputConservativeVariableClass outU3( pde, 3 );

  IntegrandConservativeVariableClass fcnErrU0( outU0, {0} );
  IntegrandConservativeVariableClass fcnErrU1( outU1, {0} );
  IntegrandConservativeVariableClass fcnErrU2( outU2, {0} );
  IntegrandConservativeVariableClass fcnErrU3( outU3, {0} );

  NDOutputEntropyVariableClass outV0( pde, 0 );
  NDOutputEntropyVariableClass outV1( pde, 1 );
  NDOutputEntropyVariableClass outV2( pde, 2 );
  NDOutputEntropyVariableClass outV3( pde, 3 );

  IntegrandEntropyVariableClass fcnErrV0( outV0, {0} );
  IntegrandEntropyVariableClass fcnErrV1( outV1, {0} );
  IntegrandEntropyVariableClass fcnErrV2( outV2, {0} );
  IntegrandEntropyVariableClass fcnErrV3( outV3, {0} );

  ArrayQ qstar;
  qstar = 0.;
  NDOutputSolutionTotalErrorSquareClass outSolutionTotalError( pde, qstar );
  IntegrandSolutionTotalErrorSquareClass fcnErrQtotal( outSolutionTotalError, {0} );

  ArrayQ ustar;
  ustar = 0.;
  NDOutputConservativeTotalErrorSquareClass outConservativeTotalError( pde, ustar );
  IntegrandConservativeTotalErrorSquareClass fcnErrUtotal( outConservativeTotalError, {0} );

  ArrayQ vstar;
  vstar = 0.;
  NDOutputEntropyTotalErrorSquareClass outEntropyTotalError( pde, vstar );
  IntegrandEntropyTotalErrorSquareClass fcnErrVtotal( outEntropyTotalError, {0} );*/
#endif
  int RKtype;
  int RKmin, RKmax;

  RKtype = 0;

  RKmin = 2;
  RKmax = 2;

  for (int RKorder = RKmin; RKorder <= RKmax; RKorder++)  //temporal order
  {
    int RKstages = RKorder;

    int ordermin = 1;
    int ordermax = 1;

    for (int order = ordermin; order <= ordermax; order++) //spatial order
    {
      int ii, jj;
      int powermin = 4;
      int powermax = 4;

      for (int power = powermin; power <= powermax; power++) //grid resolution
      {
        ii = pow( 2, power );
        jj = ii;

        Real gscale;
        Real xmin, xmax, ymin, ymax;

        xmin = 0.;
        xmax = 1.;
        ymin = 0.;
        ymax = 1.;

        gscale = 2.*3.141592653589793;

        xmin = xmin*gscale;
        xmax = xmax*gscale;
        ymin = ymin*gscale;
        ymax = ymax*gscale;

        //Real domainMeasure;
        //domainMeasure = (xmax-xmin)*(ymax-ymin);

        //create grid: 2D periodic mesh
        XField2D_BoxPeriodic_Triangle_X1 xfld(ii, jj, xmin, xmax, ymin, ymax);
        //XField2D_BoxPeriodic_Triangle_X1 xfld(ii, jj);

        //integration
        QuadratureOrder quadratureOrder(xfld, -1);
        //QuadratureOrder quadratureOrder(xfld, 10);
        std::vector<Real> tol = {1e-9, 1e-9};

        //create solution
        Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

        // Lagrange multiplier:
        Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
          lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
        lgfld = 0;

        //spatial discretization
        AlgebraicEquationSet_DGClass AlgEqSetSpace(xfld, qfld, lgfld, pde, quadratureOrder,
                                                   ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups, time);

        //temporal discretiation
        RKClass RK(RKorder, RKstages, RKtype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace);

        // Set the initial condition
        for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );

        const int nDOFPDE = qfld.nDOF();

        SystemVectorClass rsd(AlgEqSetSpace.vectorEqSize());

        std::cout << std::setprecision(16);
#if 0
 /*       /////////////////////////////////////////////////////////////////////////////////////////
        //Integrate initial condition (Solution)
        /////////////////////////////////////////////////////////////////////////////////////////
        Real Q0 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrQ0, Q0 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        Q0 = Q0/domainMeasure;
        std::cout<< "Integrated initial Q0 " << Q0 << std::endl;

        Real Q1 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrQ1, Q1 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        Q1 = Q1/domainMeasure;
        std::cout<< "Integrated initial Q1 " << Q1 << std::endl;

        Real Q2 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrQ2, Q2 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        Q2 = Q2/domainMeasure;
        std::cout<< "Integrated initial Q2 " << Q2 << std::endl;

        Real Q3 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrQ3, Q3 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        Q3 = Q3/domainMeasure;
        std::cout<< "Integrated initial Q3 " << Q3 << std::endl;

        //Set domain averaged solution (Solution)
        qstar(0) = Q0;
        qstar(1) = Q1;
        qstar(2) = Q2;
        qstar(3) = Q3;

        //Compute initial difference (Solution)
        Real QtotalError = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrQtotal, QtotalError ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        std::cout << "Q total norm (initial) " << sqrt(QtotalError) << std::endl;


        /////////////////////////////////////////////////////////////////////////////////////////
        //Integrate initial condition (Conservative)
        /////////////////////////////////////////////////////////////////////////////////////////
        Real U0 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrU0, U0 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        U0 = U0/domainMeasure;
        std::cout<< "Integrated initial U0 " << U0 << std::endl;

        Real U1 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrU1, U1 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        U1 = U1/domainMeasure;
        std::cout<< "Integrated initial U1 " << U1 << std::endl;

        Real U2 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrU2, U2 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        U2 = U2/domainMeasure;
        std::cout<< "Integrated initial U2 " << U2 << std::endl;

        Real U3 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrU3, U3 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        U3 = U3/domainMeasure;
        std::cout<< "Integrated initial U3 " << U3 << std::endl;

        //Set domain averaged solution (Conservative)
        ustar(0) = U0;
        ustar(1) = U1;
        ustar(2) = U2;
        ustar(3) = U3;

        //Compute initial difference (Conservative)
        Real UtotalError = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrUtotal, UtotalError ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        std::cout << "U total norm (initial) " << sqrt(UtotalError) << std::endl;


        /////////////////////////////////////////////////////////////////////////////////////////
        //Integrate initial condition (Entropy)
        /////////////////////////////////////////////////////////////////////////////////////////
        Real V0 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrV0, V0 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        V0 = V0/domainMeasure;
        std::cout<< "Integrated initial V0 " << V0 << std::endl;

        Real V1 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrV1, V1 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        V1 = V1/domainMeasure;
        std::cout<< "Integrated initial V1 " << V1 << std::endl;

        Real V2 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrV2, V2 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        V2 = V2/domainMeasure;
        std::cout<< "Integrated initial V2 " << V2 << std::endl;

        Real V3 = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrV3, V3 ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        V3 = V3/domainMeasure;
        std::cout<< "Integrated initial V3 " << V3 << std::endl;

        //Set domain averaged solution (Entropy)
        vstar(0) = V0;
        vstar(1) = V1;
        vstar(2) = V2;
        vstar(3) = V3;

        //Compute initial difference (Entropy)
        Real VtotalError = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
           FunctionalCell_Galerkin( fcnErrVtotal, VtotalError ),
           xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        std::cout << "V total norm (initial) " << sqrt(VtotalError) << std::endl;*/
#endif
#if 1
        //See initial condition
        int fieldorder = 1;
        string filename = "tmp/solnDG_EulerTaylorGreen_P";
        filename += to_string(order);
        filename += "_RK";
        filename += to_string(RKorder);
        filename += "_Q";
        filename += to_string(fieldorder);
        filename += "_";
        filename += to_string(ii);
        filename += "x";
        filename += to_string(jj);
        filename += ".plt";
        output_Tecplot( qfld, filename );
#endif

        // Advance the solution
        output_count = 0;
        while (output_count< nsteps)
        {
          RK.march(output_freq);
          output_count += output_freq;
          //std::cout << "Counter " << output_count << std::endl;

          //prevent overshoot
          if ((nsteps-output_count) < output_freq)
          {
             output_freq = nsteps-output_count;
          }
#if 0
/*          //evaluate norm (Solution)
          Real Qnorm = 0.0;
          IntegrateCellGroups<TopoD2>::integrate(
             FunctionalCell_Galerkin( fcnErrQtotal, Qnorm ),
             xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

          //evaluate norm (Conservative)
          Real Unorm = 0.0;
          IntegrateCellGroups<TopoD2>::integrate(
             FunctionalCell_Galerkin( fcnErrUtotal, Unorm ),
             xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

          //evaluate norm (Entropy)
          Real Vnorm = 0.0;
          IntegrateCellGroups<TopoD2>::integrate(
             FunctionalCell_Galerkin( fcnErrVtotal, Vnorm ),
             xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

          std::cout << "Counter " << output_count << ",    Q total norm " << sqrt(Qnorm) << ",    U total norm ";
          std::cout << sqrt(Unorm) << ",    V total norm " << sqrt(Vnorm) << std::endl;*/
#endif
        }


        // Check spatial residual
        rsd = 0.;
        AlgEqSetSpace.residual(rsd);
        Real rsdPDEnrm = 0.;
        for (int n = 0; n < nDOFPDE; n++)
        {
          for (int j = 0; j < nSol; j++)
          {
            rsdPDEnrm += pow(rsd[0][n][j], 2);
          }
        }

        std::cout << "Spatial residual " << sqrt(rsdPDEnrm) << std::endl;

#if 1
        // See final condition
        int fieldorder_aft = 1;
        string filename_aft = "tmp/solnDG_EulerTaylorGreen_aft_P";
        filename_aft += to_string(order);
        filename_aft += "_RK";
        filename_aft += to_string(RKorder);
        filename_aft += "_Q";
        filename_aft += to_string(fieldorder_aft);
        filename_aft += "_";
        filename_aft += to_string(ii);
        filename_aft += "x";
        filename_aft += to_string(jj);
        filename_aft += ".plt";
        output_Tecplot( qfld, filename_aft );
#endif

        //reset time
        time = 0.;

      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
#endif
