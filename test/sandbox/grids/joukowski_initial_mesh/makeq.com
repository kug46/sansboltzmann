// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

! Script to convert a linear grid to curved

lua OUTQ3GRM_NOPROJ = "jouk_q4_noproj.grm"
lua OUTQ3GRM = "jouk_q4.grm"
lua CSFFILE = "jouk.csf"
lua UCRV0 = "Airfoil"
lua UCRV1 = "Farfield"

file open csf $CSFFILE$
file set working csf $CSFFILE$
om set file csf $CSFFILE$
file set units inches file csf $CSFFILE$

! Load geometry curves
om select clear
om select usurf object $UCRV0$ file csf $CSFFILE$
om select usurf object $UCRV1$ file csf $CSFFILE$
geom register curvedata

GGU ADAPT SET_EPIC_PARAM PROJECT_TO_GEOM CURVE

! Select input object
om select clear
om select usurf object 'jouk_q1' file csf $CSFFILE$
ggu higherorder qorder 4 projectdelta 'projectdeform.disp'
file save name 'INIT Q4 NOPROJ' overwrite file csf $CSFFILE$
om select $result
file export to file grm $OUTQ3GRM_NOPROJ$ overwrite optionalarg

!manip deform solver cg precond sgs geometry
manip deform solver local model quality geometry
file save name 'INIT Q4 PROJECTED' overwrite file csf $CSFFILE$
om select $result
file export to file grm $OUTQ3GRM$ overwrite optionalarg

