! MADCAP SCRIPT TO:
! IMPORT GRI
! ADAPT
! AND EXPORT RESULT TO GRI
lua ORIGNAME = "pxmadcap.csf"
lua INGRI = "pxmadcap.in.gri"
lua OUTQ1GRI = "pxmadcap.out.q1.gri"
file import gri $INGRI$ csf $ORIGNAME$ 'INPUTOBJ'
file open csf $ORIGNAME$
file set working csf $ORIGNAME$
om set file csf $ORIGNAME$
file set units inches file csf $ORIGNAME$

lua OUTQ2GRI_NOPROJ = "pxmadcap.out.q2.noproj.gri"
lua OUTQ2GRI = "pxmadcap.out.q2.gri"
lua CSFFILE = "../meshgen/naca0012.csf"
lua UCRV0 = "body2"
lua UCRV1 = "freestream"
lua UCRV2 = "outflow"

! Load geometry curves
file open csf $CSFFILE$
om select clear
om select usurf object $UCRV0$ file csf $CSFFILE$
om select usurf object $UCRV1$ file csf $CSFFILE$
om select usurf object $UCRV2$ file csf $CSFFILE$
geom register curvedata
GGU ADAPT SET_EPIC_PARAM PROJECT_TO_GEOM CURVE

! Select input object
om select clear
om select usurf object 'INPUTOBJ' file csf $ORIGNAME$

! Curvature-based refinement
ggu adapt set_metric_param GEOM_CURVELIMIT_NPTS 12 GEOM_CURVELIMIT_MIN  -1.00000000e+00 GEOM_CURVELIMIT_MAX  -1.00000000e+00
ggu adapt set_metric_param GEOM_CURVELIMIT_NORMSIZE  -1.00000000e+00
ggu adapt attach_geom_size
om select clear
om select $result

GGU ADAPT SET_ERROR_PARAM METHOD FILE
GGU ADAPT SET_ERROR_PARAM FILE 'pxmadcap.bnmetric'
GGU ADAPT SET_EPIC_PARAM COARSENREFINE_BITMAP 51
GGU ADAPT NCELL_REQUEST 0
GGU ADAPT SET_EPIC_QUALPARAM NORM 2
GGU ADAPT SET_EPIC_QUALPARAM QUALMODE Shape0
GGU ADAPT SET_EPIC_QUALPARAM QUALCOMBMODE Minimum
GGU ADAPT SET_EPIC_QUALPARAM CELLQUALITY_RATIO  -1.00000000e+00
GGU ADAPT SET_EPIC_QUALPARAM FACEQUALITY_RATIO  -1.00000000e+00
GGU ADAPT SET_METRIC_PARAM HMIN  -1.00000000e+00 HMAX  -1.00000000e+00
GGU ADAPT SET_METRIC_PARAM MAXANISOTROPY   1.00000000e+06
GGU ADAPT SET_METRIC_PARAM TRANSFER AVERAGE AVERAGE LOG
GGU ADAPT SET_METRIC_PARAM GROWTH_LIMIT   0.00000000e+00
GGU ADAPT SET_METRIC_PARAM GROWTH_LIMIT_INITIAL   0.00000000e+00
GGU ADAPT SET_METRIC_PARAM GROWTH_NLAYER 0
GGU ADAPT SET_METRIC_PARAM GROWTH_NITER 0
GGU ADAPT ATTACH_TARGET_SIZE
om select $result
GGU ADAPT SET_EPIC_PARAM PROJECT_MODE Elastic
GGU ADAPT SET_EPIC_QUALPARAM FOLDED_FACEANGLE   7.00000000e+01
GGU ADAPT SET_EPIC_QUALPARAM CORNER_EDGEANGLE   7.00000000e+01
GGU ADAPT SET_EPIC_QUALPARAM REMOVETRAPPED ON
GGU ADAPT SET_EPIC_PARAM NITER 25
GGU ADAPT SET_EPIC_PARAM MESSAGE_LEVEL 3
GGU ADAPT SET_EPIC_PARAM BACKGROUND_MESH INITIAL
GGU ADAPT SET_EPIC_BLAYERPARAM NLAYER1 0 NLAYER2 0
GGU ADAPT EXECUTE USING EPIC
file save name 'ADAPTED' overwrite file csf $ORIGNAME$

om select clear
om select usurface object 'ADAPTED' file csf $ORIGNAME$
manip deform solver cg precond sgs geometry
file save name 'ADAPTED PROJECTED' overwrite file csf $ORIGNAME$
om select $result
file export to file gri $OUTQ1GRI$ overwrite optionalarg
om select clear
om select usurface object 'ADAPTED' file csf $ORIGNAME$
ggu higherorder qorder 3 projectdelta 'projectdeform.disp'
file save name 'ADAPTED Q2 NOPROJ' overwrite file csf $ORIGNAME$
om select $result
file export to file gri $OUTQ2GRI_NOPROJ$ overwrite optionalarg
manip deform solver local model quality geometry
file save name 'ADAPTED Q2 PROJECTED' overwrite file csf $ORIGNAME$
om select $result
file export to file gri $OUTQ2GRI$ overwrite optionalarg

