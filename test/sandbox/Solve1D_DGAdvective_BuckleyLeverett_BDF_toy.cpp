// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_DGAdvective_BuckleyLeverett_btest
// testing of 1-D DG with Buckley-Leverett

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"

#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGAdvective_BuckleyLeverett_BDF_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGAdvective_BuckleyLeverett_BDF )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_None CapillaryModel;
  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QTypePrimitive_Sw, PDEClass> ExactSolutionClass;
  typedef SolnNDConvertSpace<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef BCBuckleyLeverett1DVector<PDEClass> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> BDFClass;

  GlobalTime time(0);

  // PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  CapillaryModel cap_model;

  NDPDEClass pde(time, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 1.0;
  const Real SwR = 0.1;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(time, pde, SwL, SwR, xinit);

  // BCs

  // Create a BC dictionary
  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params.qB] = SwL;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params.qB] = SwR;

//  PyDict BCDirichletL;
//  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin;
//  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.A] = 1.0;
//  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.B] = 0.0;
//  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.bcdata] = SwL;
//
//  PyDict BCDirichletR;
//  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin;
//  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.A] = 1.0;
//  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.B] = 0.0;
//  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params.bcdata] = SwR;

  PyDict PyBCList;
  PyBCList["DirichletL"] = BCDirichletL;
  PyBCList["DirichletR"] = BCDirichletR;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletL"] = {0}; //Left boundary
  BCBoundaryGroups["DirichletR"] = {1}; //Bottom boundary

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

//  IntegrandSquareErrorClass fcnErr( solnExact );

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  PyDict NonLinearSolverDict;
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  // norm data
//  Real hVec[10];
//  Real hDOFVec[10];   // 1/sqrt(DOF)
//  Real normVec[10];   // L2 error
//  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/Solve1D_DGAdvective_BuckleyLeverett.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/Solve/Solve1D_DGAdvective_BuckleyLeverett_FullTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  //pyrite_file_stream pyriteFile("IO/Solve/Solve1D_DGAdvective_BuckleyLeverett_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
//  pyriteFile << std::setprecision(16) << std::scientific;


  // set BDF and grid parameters for the run
  int BDForder = 2;
  int order = 0;

  int ii = 200;
  int Nsteps = 100;

  // domain:
  XField1D xfld( ii, 0, 50 );
  Real Tend = 25;

  Real dt = Tend / Nsteps;

  // Initial solution
  ArrayQ qinit;
  pde.setDOFFrom( qinit, SwR, "Sw" );


  // solution:
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = qinit;

  // Or, use the projection of the exact solution as an initial solution
  for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
  output_Tecplot( qfld, "tmp/qfld_init.plt" );
  const int nDOFPDE = qfld.nDOF();

  std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, active_BGroup_list);
  lgfld = 0.0;
  const int nDOFBC = lgfld.nDOF();

  std::cout<<"DOF: "<<(nDOFPDE + nDOFBC)<<std::endl;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = { 1e-11, 1e-11 };

  // Create AlgebraicEquationSets
  PrimalEquationSetClass AlgEqSetSpace(xfld, qfld, lgfld, pde, quadratureOrder,
                                       ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups, time);

  // The BDF class
  BDFClass BDF( BDForder, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {0}, AlgEqSetSpace );


  // Set IC
  time = 0.0;
  int stepstart = 0;
  for (int j = BDForder-1; j >= 0; j--)
  {
    for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
    time += dt;
    stepstart++;
    BDF.setqfldPast(j, qfld);
  }

  for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );

  for (int step = stepstart; step < Nsteps; step += 10)
  {
    // March the solution in time
    BDF.march(10);

#if 1
    // Tecplot dump grid
    string filename = "tmp/timeDG_BuckleyLeverett_P";
    filename += to_string(order);
    filename += "_";
    filename += to_string(ii);
    filename += "_";
    filename += to_string(step);
    filename += ".plt";
    output_Tecplot( qfld, filename );
#endif
  }

#if 1
    // Tecplot dump grid
    string filename = "tmp/timeDG_BuckleyLeverett_P";
    filename += to_string(order);
    filename += "_";
    filename += to_string(ii);
    filename += "_";
    filename += to_string(Nsteps);
    filename += ".plt";
    output_Tecplot( qfld, filename );
#endif

  std::cout << stepstart << ", " << time << std::endl;

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
