// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DG_Poisson_btest
// Testing of the MOESS framework on the advection-diffusion pde

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/OutputCell_Solution.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/ForcingFunction2D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "tools/linspace.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_DG_Poisson_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_Poisson_Triangle )
{
  // Is this the Manufactured Solution case?
  typedef ScalarFunction2D_SineSine  SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_Uniform,
      Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_Solution<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  // Grid
  std::vector<Real> xpts = linspace(0,1,5);
  std::vector<Real> ypts(xpts.begin(),xpts.end());

  Real a = 0.0;
  Real b = 0.0;
  Real nu = 1.0;

  // PDE
  AdvectiveFlux2D_Uniform adv(a,b);

  ViscousFlux2D_Uniform visc( nu, 0., 0., nu );

  Source2D_UniformGrad source(0.0, 0.0, 0.0);

  // BC
  // Create a solution dictionary
  Real a_0 = 0.75, b_0 = 3.25, C_0 = 0.5, A_0 = 1.25;

  // J(u) = \int u
  const Real trueOutput = C_0 + A_0*(cos(a_0*PI) - 1.0)*(cos(b_0*PI) - 1.0)/(a_0*b_0*pow(PI,2.0));

  PyDict solnArgs;
  solnArgs[SolutionExact::ParamsType::params.a] = a_0;
  solnArgs[SolutionExact::ParamsType::params.b] = b_0;
  solnArgs[SolutionExact::ParamsType::params.C] = C_0;
  solnArgs[SolutionExact::ParamsType::params.A] = A_0;

  NDSolutionExact solnExact( solnArgs );

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  solnArgs[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.SineSine;

  PyDict BC_Dirichlet_Soln;
  BC_Dirichlet_Soln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
                    BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;

  PyBCList["BC_Dirichlet_Soln"] = BC_Dirichlet_Soln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["BC_Dirichlet_Soln"] = {
                                            XField2D_Box_Triangle_Lagrange_X1::iBottom,
                                            XField2D_Box_Triangle_Lagrange_X1::iRight,
                                            XField2D_Box_Triangle_Lagrange_X1::iTop,
                                            XField2D_Box_Triangle_Lagrange_X1::iLeft
                                          };

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-8, 1e-8};

  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});


  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::vector<std::string> meshers = {"avro","Epic"};

  int orderL = 1, orderH = 3;
  int powerL = 0, powerH = 4;

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc == 4)
  {
    orderL = orderH = std::stoi(argv[1]);
    powerL = powerH = std::stoi(argv[2]);
    meshers = {std::string(argv[3])};
  }
  if (argc == 3)
  {
    orderL = orderH = std::stoi(argv[1]);
    // powerL = powerH = std::stoi(argv[1]);
    meshers = {std::string(argv[2])};
  }
  if (argc == 2)
  {
    meshers = {std::string(argv[1])};
  }

  std::cout << "Number of mesher = " << meshers.size() << std::endl;
  std::cout << "orderL = " << orderL << ", orderH = " << orderH << std::endl;
  std::cout << "powerL = " << powerL << ", powerH = " << powerH << std::endl;
#endif

  //--------ADAPTATION LOOP--------
  for (const std::string& mesher : meshers)
  {
    std::cout<< "Mesher = " << mesher << std::endl;

    std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;

    const int maxIter = 30;

    //--------ADAPTATION LOOP--------
    for (int order = orderL; order <= orderH; order++)
    {
      timer totalTime;

      for (int power = powerH; power >= powerL; power--)
      {
        const int nk = pow(2,power);
        const int targetCost = 500*nk;

        // to make sure folders have a consistent number of zero digits
        const int string_pad = 6;
        std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
        std::string filename_base = "tmp/Poisson/DG_" + int_pad + "_P" + std::to_string(order) + "/";

        std::string adapthist_filename = filename_base + "test.adapthist";

        boost::filesystem::create_directories(filename_base);

        fstream fadapthist( adapthist_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

        PyDict MOESSDict;
        MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
        MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
        // MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction] = 1.0;
        MOESSDict[MOESSParams::params.UniformRefinement] = false;

  #ifdef SANS_AVRO
        std::shared_ptr<avro::Context> context;
  #endif
        PyDict MesherDict;
        if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic)
        {
          MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
          // MesherDict[EpicParams::params.nThread] = 4; // DO NOT USE THIS ON HYPERSONIC

          // uniform initial
          // pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
          // Initial cross clustering
          pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, xpts, ypts );
        }
  #ifdef SANS_AVRO
        else if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro)
        {
          MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;

          MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
          MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files

          // create the context
          context = std::make_shared<avro::Context>();

          using avro::coord_t;
          using avro::index_t;

          coord_t number = 2;

          std::vector<Real> lens(number,1.);
          // std::vector<index_t> dims(number,ii);

          Real xc[3] = {.5,.5,0.};
          std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( context.get() , xc , lens[0] , lens[1] );
          std::shared_ptr<avro::Model> model = std::make_shared<avro::Model> ( context.get(), "model" );
          model->addBody(pbody,true);

          // Initial Cartesian
          // XField2D_Box_Triangle_Lagrange_X1 xfld0( world, ii , jj );
          // Initial cross clustering
          XField2D_Box_Triangle_Lagrange_X1 xfld0( world, xpts, ypts );

          // copy the mesh into the domain and attach the geometry
          pxfld = std::make_shared< XField_avro<PhysD2,TopoD2> >(xfld0, model);
        }
  #endif
        else if (mesher == "fefloa")
        {
          MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

          // uniform initial
          // pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
          // Initial cross clustering
          pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, xpts, ypts );
        }
        else
          SANS_DEVELOPER_EXCEPTION("Unknown mesher");


        PyDict AdaptDict;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = true;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = true;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.hasTrueOutput] = true;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TrueOutput] = trueOutput;

        MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

        MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

        std::vector<int> cellGroups = {0};
        std::vector<int> interiorTraceGroups;
        for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        //Solution data
        std::shared_ptr<SolutionClass> pGlobalSol;
        pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                     BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                     active_boundaries, disc);

        const int quadOrder = 2*(order + 1);

        //Create solver interface
        std::shared_ptr<SolverInterfaceClass> pInterface;
        pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                            cellGroups, interiorTraceGroups,
                                                            PyBCList, BCBoundaryGroups,
                                                            SolverContinuationDict, AdjLinearSolverDict,
                                                            outputIntegrand);

        pGlobalSol->setSolution(0.0);

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();

        std::string qfld_filename = filename_base + "qfld_a0.plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a0.plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

  #ifdef XFIELD_OUT
        std::string xfld_filename = filename_base + "xfld_a0.grm";
        output_grm( *pxfld, xfld_filename);
  #endif

        //Compute error estimates
        pInterface->computeErrorEstimates();

        for (int iter = 0; iter < maxIter+1; iter++)
        {
          if (world.rank() == 0)
            std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

          //Perform local sampling and adapt mesh
          std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
          pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

          interiorTraceGroups.clear();
          for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
            interiorTraceGroups.push_back(i);

          std::shared_ptr<SolutionClass> pGlobalSolNew;
          pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                          BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                          active_boundaries, disc);

          //Perform L2 projection from solution on previous mesh
          pGlobalSolNew->setSolution(*pGlobalSol);

          std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
          pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                                 cellGroups, interiorTraceGroups,
                                                                 PyBCList, BCBoundaryGroups,
                                                                 SolverContinuationDict, AdjLinearSolverDict,
                                                                 outputIntegrand);

          //Update pointers to the newest problem (this deletes the previous mesh and solutions)
          pxfld = pxfldNew;
          pGlobalSol = pGlobalSolNew;
          pInterface = pInterfaceNew;

          pInterface->solveGlobalPrimalProblem();
          pInterface->solveGlobalAdjointProblem();

          std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

          std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
          output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
  // #ifdef XFIELD_OUT
  //         std::string xfld_filename = filename_base + "xfld_a" + std::to_string(iter+1) + ".grm";
  //         output_grm( *pxfld, xfld_filename);
  // #endif

          //Compute error estimates
          pInterface->computeErrorEstimates();
        }

        fadapthist.close();
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
