// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/OutputEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Adaptation/MOESS/SolverInterface_AGLS.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Meshing/EPIC/XField_PX.h"

#include "unit/UnitGrids/XField3D_FlatPlate_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_DGBR2_NavierStokes_FlatPlate_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_DGBR2_NavierStokes_FlatPlate_test )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Sutherland ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

#ifndef BOUNDARYOUTPUT
  typedef ScalarFunction3D_Const WeightFcn; // Change this to change output weighting type
  typedef OutputCell_WeightedSolution<PDEClass, WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler3D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef SolutionData_Galerkin_Stabilized<PhysDim, TopoDim, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                   AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  // typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef SolverInterface_AGLS<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // Sutherland viscosity
  const Real tSuth = 198.6/540;     // R/R
  //const Real tRef  = 491.6/540;     // R/R

  // reference state (freestream)
  const Real Mach = 0.1;
  const Real Reynolds = 1e3;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 0;                            // angle of attack (radians)

#if 1
  const Real tRef = 1;          // temperature
  const Real pRef = rhoRef*R*tRef;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;
#endif

#if 0
  const Real qRef = 1;                              // velocity scale
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = 0;
  const Real wRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  //const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef + wRef*wRef);

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef, tRef, tSuth);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure3D<Real>(rhoRef, uRef, vRef, wRef, pRef) );

  // Galerkin Stabilization
  StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby);

  const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  const Real TtRef = tRef*Gamma;
  const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  typedef BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass>::ParamsType PtTtaParams;

  PyDict BCInFlow;
  BCInFlow[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInFlow[PtTtaParams::params.TtSpec] = TtRef;
  BCInFlow[PtTtaParams::params.PtSpec] = PtRef;
  BCInFlow[PtTtaParams::params.aSpec]  = 0;
  BCInFlow[PtTtaParams::params.bSpec]  = 0;

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflow[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;


  PyDict PyBCList;
  PyBCList["BCInflow"]   = BCInFlow;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCOutflow"]  = BCOutflow;
  PyBCList["BCWall"]     = BCWall;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCInflow"]   = {XField3D_FlatPlate_X1::iLeft};
  BCBoundaryGroups["BCOutflow"]  = {XField3D_FlatPlate_X1::iRight};
  BCBoundaryGroups["BCWall"]     = {XField3D_FlatPlate_X1::iPlate};
  BCBoundaryGroups["BCSymmetry"] = {XField3D_FlatPlate_X1::iSlipIn,
                                    XField3D_FlatPlate_X1::iSlipOut,
                                    XField3D_FlatPlate_X1::iTop,
                                    XField3D_FlatPlate_X1::iFront,
                                    XField3D_FlatPlate_X1::iBack};

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-7, 1e-7};

#ifndef BOUNDARYOUTPUT
  //Output functional
  Real weight = 1.0;
  WeightFcn weightFcn(weight);
  NDOutputClass fcnOutput(weightFcn);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  // Residual weighted boundary output
  Real dynpRef = 0.5*rhoRef*(uRef*uRef + vRef*vRef + wRef*wRef);
  NDOutputClass outputFcn(pde, cos(aoaRef)/dynpRef, 0., sin(aoaRef)/dynpRef);
  OutputIntegrandClass outputIntegrand( outputFcn, BCBoundaryGroups["BCWall"] );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-9;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  PyDict SolverContinuationDict0, NonlinearSolverDict0;

  // Newton for
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  //PTC for
  NonlinearSolverDict0[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict0[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict0[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict0[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict0[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict0[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict0[PseudoTimeParam::params.invCFL_max] = 1e5;

  SolverContinuationDict0[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict0;
  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict0);
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  const bool DGCONV = true; // use DG DOF counts. Converts to matching DG for elements


  //--------ADAPTATION LOOP--------

  int maxIter = 30;
  int targetCost = 16000;

  // initial Grid
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld = std::make_shared<XField3D_FlatPlate_X1>(world, -2 );

  int order = 1;

  std::string filename_base = "tmp/NS3D/FP_Re1000/";
  if ( stab.getStabType() == StabilizationType::Adjoint )
  {
    filename_base += "VMS_";
  }
  else if ( stab.getStabType() == StabilizationType::Unstabilized )
  {
    filename_base += "CG_";
  }
  else if ( stab.getStabType() == StabilizationType::GLS )
  {
    filename_base += "GLS_";
  }
  else if ( stab.getStabType() == StabilizationType::SUPG )
  {
    filename_base += "SUPG_";
  }

  filename_base += "Elem_";

  // to make sure folders have a consistent number of zero digits
  const int string_pad = 6;
  std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);

  filename_base += int_pad + "_P" + std::to_string(order) + "/";

  boost::filesystem::create_directories(filename_base);

  if (DGCONV)
  {
    Real nDOFperCell_DG = (order+1)*(order+2)*(order+3)/6;
    Real nDOFperCell_CG = nDOFperCell_DG;

    nDOFperCell_CG -= (4 - 1./5); // the node dofs are shared by 20
    nDOFperCell_CG -= (6 - 1)*std::max(0,(order-1)); // if there are edge dofs they are shared by 6
    nDOFperCell_CG -= (4 - 2)*std::max(0,(order-1)*(order-2)/2); // if there are face dofs they are shared by 2


    targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
  }


  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

  PyDict ParamDict;

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;

  MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Element;
  // MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
  MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;

  PyDict MesherDict;
#if defined(SANS_REFINE) && USE_REFINE
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.refine;
  MesherDict[refineParams::params.FilenameBase] = filename_base;
  MesherDict[refineParams::params.CADfilename] = filename_base + "hemisph-cyl.egads";
  MesherDict[refineParams::params.GASFilename] = filename_base + "hsc01.gas";
  MesherDict[refineParams::params.DumpRefineDebugFiles] = true;
#else
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
  // MesherDict[EpicParams::params.Version] = "madcap_devel-10.1"; //Mesh order
  //MesherDict[EpicParams::params.CurvedQOrder] = 1; //Mesh order
  //MesherDict[EpicParams::params.CSF] = "tmp/hemisphere-cylinder/EPIC/hemisphere_cylinder3_cf.csf"; //madcap surface file
  //MesherDict[EpicParams::params.Geometry] = "tmp/hemisphere-cylinder/EPIC/hemisphere_cylinder3_cf.igs"; //CAD file
  //MesherDict[EpicParams::params.Shell] = "SHELL";
  //MesherDict[EpicParams::params.BoundaryObject] = "BDIST";
  //MesherDict[EpicParams::params.minGeom] = 1e-6;
  //MesherDict[EpicParams::params.maxGeom] = 2000;
  //MesherDict[EpicParams::params.nPointGeom] = -1;
  // MesherDict[EpicParams::params.NodeMovement] = false;
  //MesherDict[EpicParams::params.SymmetricSurf] = {1, 2};
  //MesherDict[EpicParams::params.ViscSurf] = BCBoundaryGroups["BCWall"];
  //MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;
#endif

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

  MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                               active_boundaries, stab);

  //Set initial solution
  pGlobalSol->setSolution(q0);

  const int quadOrder = 2*(order + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict0, LinearSolverDict,
                                                      outputIntegrand);

  //Set initial solution
  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename = filename_base + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

  std::string adjfld_filename = filename_base + "adjfld_a0.plt";
  output_Tecplot( pInterface->getAdjField(), adjfld_filename );

  pInterface->computeErrorEstimates();
  std::string efld_filename = filename_base + "efld_a0.plt";

  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                                                    active_boundaries, stab);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict0, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1)
                                                   + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();

    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1)
                                              + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    pInterface->solveGlobalAdjointProblem();

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1)
                                                + ".plt";
    output_Tecplot( pInterface->getAdjField(), adjfld_filename );

    pInterface->computeErrorEstimates();
    std::string efld_filename = filename_base + "efld_a0.plt";
  }

  fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
