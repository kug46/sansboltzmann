// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_AD_ArtificialViscosity_ST_btest
// Testing of the MOESS framework on a 2D advection problem with artificial viscosity

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion_ArtificialViscosity2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion_ArtificialViscosity2D.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"
#include "pde/Sensor/Source2D_JumpSensor.h"
#include "pde/Sensor/PDESensorParameter2D.h"
#include "pde/Sensor/BCSensorParameter2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/SolutionData_DGBR2_Block2.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2_Block2x2.h"
#include "Adaptation/MeshAdapter.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_AD_ArtificialViscosity_test_suite )

#define SOLUTION_BC

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_AD_ArtificialViscosity_Triangle )
{
  typedef AdvectiveFlux2D_Uniform AdvectiveFluxType;
//  typedef AdvectiveFlux2D_PiecewiseUniform AdvectiveFluxType;
  typedef ViscousFlux2D_Uniform ViscousFluxType;
  typedef Source2D_UniformGrad SourceType;

  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFluxType, ViscousFluxType, SourceType> PDEClass_Primal;
  typedef PDENDConvertSpace<PhysD2, PDEClass_Primal> NDPDEClass_Primal;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;
  typedef Sensor_AdvectiveFlux2D_Uniform Sensor_Advection;
  typedef Sensor_ViscousFlux2D_GenHScale Sensor_Diffusion;

  typedef PDESensorParameter<PhysD2,
                             SensorParameterTraits<PhysD2>,
                             Sensor_Advection,
                             Sensor_Diffusion,
                             Source_JumpSensor > PDEClass_Sensor;
  typedef PDENDConvertSpace<PhysD2, PDEClass_Sensor> NDPDEClass_Sensor;

  typedef BCTypeLinearRobin_sansLG BCType;
  typedef BCAdvectionDiffusion_ArtificialViscosity2DVector<AdvectiveFluxType, ViscousFluxType> BCVectorPrimal;

  typedef BCParameters<BCVectorPrimal> BCParamsPrimal;

  typedef BCSensorParameter2DVector<Sensor_Advection, Sensor_Diffusion> BCVectorSensor;

  typedef BCParameters<BCVectorSensor> BCParamsSensor;

//  typedef OutputCell_Solution<PDEClass_Primal> OutputClass;
  typedef OutputCell_SolutionSquared<PDEClass_Primal> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_ArtificialViscosity ParamBuilderType;

  //Solution data
  typedef SolutionData_DGBR2_Block2<PhysD2, TopoD2, NDPDEClass_Primal, NDPDEClass_Sensor, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType0 ParamFieldType0;
  typedef typename SolutionClass::ParamFieldType1 ParamFieldType1;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Primal, BCNDConvertSpace, BCVectorPrimal,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType0> PrimalEquationSetClass_Primal;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Sensor, BCNDConvertSpace, BCVectorSensor,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType1> PrimalEquationSetClass_Sensor;

  typedef SolverInterface_DGBR2_Block2x2<PhysD2, TopoD2,
                                         NDPDEClass_Primal, BCNDConvertSpace, BCVectorPrimal,
                                         NDPDEClass_Sensor, BCNDConvertSpace, BCVectorSensor,
                                         ParamBuilderType, SolutionClass,
                                         PrimalEquationSetClass_Primal, PrimalEquationSetClass_Sensor,
                                         OutputIntegrandClass> SolverInterfaceClass;


  // Grid
  int ii = 10;
  int jj = 10;

//  ReadMesh<PhysD2, TopoD2, FeFloa> reader("tmp/fefloa_out_a19.mesh");
//  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField<PhysD2, TopoD2>( reader ) );

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, jj, 0, 1, 0, 1, true );
//  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_Box_UnionJack_Triangle_X1( ii, jj ) );
  int iB = XField2D_Box_Triangle_X1::iBottom;
  int iR = XField2D_Box_Triangle_X1::iRight;
  int iT = XField2D_Box_Triangle_X1::iTop;
  int iL = XField2D_Box_Triangle_X1::iLeft;

  int order_primal = 1;
  int order_sensor = 1;

  // Principal PDE
  Real a = 0.5;
  Real b = 1.0;
  Real nu = 0.0;
  AdvectiveFluxType adv( a, b );
//  AdvectiveFluxType adv( 1, -1, 0, 0,
//                         1,  1, 0, 0, 0.5, -0.1);
  ViscousFluxType visc( nu, 0, 0, nu );
  SourceType source(0.0, 0.0, 0.0);

  //Create artificial viscosity PDE
  NDPDEClass_Primal pde_primal(adv, visc, source, order_primal);


  //Sensor PDE
  Sensor sensor(pde_primal);
  Sensor_Advection sensor_adv(0.0, 0.0);
  Sensor_Diffusion sensor_visc(1.0);
  Source_JumpSensor sensor_source(order_primal, sensor);

  NDPDEClass_Sensor pde_sensor(sensor_adv, sensor_visc, sensor_source);

  Real uL = 1.50;
  Real uR = 0.50;

#ifdef SOLUTION_BC
  typedef ScalarFunction2D_PiecewiseConstBlock ExactSolutionClass;
//  typedef ScalarFunction2D_Linear ExactSolutionClass;
  typedef SolnNDConvertSpace<PhysD2, ExactSolutionClass> NDExactSolutionClass;

  // Create a Solution dictionary
  PyDict SolnDict;
  SolnDict[BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.Function.Name]
           = BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.Function.PiecewiseConstBlock;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a00] = uL;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a01] = uR;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a10] = 0.0;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a11] = 0.0;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.x0] = 0.25;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.y0] = -0.1;

//  SolnDict[BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.Function.Name]
//           = BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.Function.Linear;
//  SolnDict[ScalarFunction2D_Linear::ParamsType::params.a0] =  uL;
//  SolnDict[ScalarFunction2D_Linear::ParamsType::params.ax] =  uR - uL;
//  SolnDict[ScalarFunction2D_Linear::ParamsType::params.ay] =  0.0;

  NDExactSolutionClass solnExact(SolnDict);
#endif

  // Primal PDE BCs

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.None;

#ifdef SOLUTION_BC
  PyDict BCSolution;
  BCSolution[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.FunctionLinearRobin_sansLG;
  BCSolution[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.A] = 1.0;
  BCSolution[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0.0;
  BCSolution[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function] = SolnDict;
#endif

  PyDict BCDirichletR;
  BCDirichletR[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.LinearRobin_sansLG;
  BCDirichletR[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCDirichletR[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;
  BCDirichletR[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = uR;

  PyDict BCDirichletL;
  BCDirichletL[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.LinearRobin_sansLG;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = uL;

  PyDict PyBCList_Primal;
//  PyBCList_Primal["DirichletR"] = BCDirichletR;
  PyBCList_Primal["DirichletL"] = BCDirichletL;
  PyBCList_Primal["DirichletSol"] = BCSolution;
  PyBCList_Primal["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Primal;

  // Define the BoundaryGroups for each boundary condition
//  BCBoundaryGroups_Primal["DirichletR"] = {iR};
  BCBoundaryGroups_Primal["DirichletL"] = {iL};
  BCBoundaryGroups_Primal["DirichletSol"] = {iB};
  BCBoundaryGroups_Primal["None"] = {iR,iT};

  //Check the BC dictionary
  BCParamsPrimal::checkInputs(PyBCList_Primal);

  std::vector<int> active_boundaries_primal = BCParamsPrimal::getLGBoundaryGroups(PyBCList_Primal, BCBoundaryGroups_Primal);

  // Sensor BCs
  PyDict BCSensorNeumann;
  BCSensorNeumann[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorNeumann[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 0.0;
  BCSensorNeumann[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCSensorNeumann[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorDirichlet;
  BCSensorDirichlet[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorDirichlet[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCSensorDirichlet[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0.0;
  BCSensorDirichlet[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorNone;
  BCSensorNone[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.None;

  PyDict PyBCList_Sensor;
  PyBCList_Sensor["SensorNeumann"] = BCSensorNeumann;
  PyBCList_Sensor["SensorDirichlet"] = BCSensorDirichlet;
//  PyBCList_Sensor["SensorNone"] = BCSensorNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Sensor;
  BCBoundaryGroups_Sensor["SensorNeumann"] = {iR,iT};
  BCBoundaryGroups_Sensor["SensorDirichlet"] = {iL, iB, iR};
//  BCBoundaryGroups_Sensor["SensorNone"] = {iB, iR, iT, iL}; // Top

  //Check the BC dictionary
  BCParamsSensor::checkInputs(PyBCList_Sensor);

  std::vector<int> active_boundaries_sensor = BCParamsSensor::getLGBoundaryGroups(PyBCList_Sensor, BCBoundaryGroups_Sensor);


  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol0 = {1e-11, 1e-11};
  std::vector<Real> tol1 = {1e-7, 1e-7};

  //Output functional
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
//  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = true;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type] = SolverContinuationParams::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  //Adaptation parameters
  int maxIter = 20;
  Real targetCost = 10000;

  std::string adapthist_filename = "tmp/test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = true;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = true;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpError] = true;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2_Block2<PhysD2, TopoD2, NDPDEClass_Primal, NDPDEClass_Sensor, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde_primal, pde_sensor,
                                               order_primal, order_sensor, order_primal+1, order_sensor+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries_primal, active_boundaries_sensor, disc, disc);

  const int quadOrder = -1; //2*(order_primal + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, ResNormType,
                                                      tol0, tol1, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList_Primal, PyBCList_Sensor,
                                                      BCBoundaryGroups_Primal, BCBoundaryGroups_Sensor,
                                                      SolverContinuationDict, outputIntegrand);

  //Set initial solution
#ifdef SOLUTION_BC
  pGlobalSol->setSolution0(solnExact, cellGroups);
#endif
  pGlobalSol->primal0.qfld = 0.1;
  pGlobalSol->primal1.qfld = 1.0;

  //Output h-field
//  std::string hfld_filename = "tmp/hfld_a0.plt";
//  output_Tecplot( get<0>(pGlobalSol->paramfld0), hfld_filename );

  std::string qfld0_init_filename = "tmp/qfld0_init_a0.plt";
  std::string qfld1_init_filename = "tmp/qfld1_init_a0.plt";
  output_Tecplot( pGlobalSol->primal0.qfld, qfld0_init_filename );
  output_Tecplot( pGlobalSol->primal1.qfld, qfld1_init_filename );

  pInterface->solveGlobalPrimalProblem();
//  std::rename("tmp/tracedump.dat", "tmp/tracedump_primal_a0.dat");
  std::string qfld0_filename = "tmp/qfld0_a0.plt";
  std::string qfld1_filename = "tmp/qfld1_a0.plt";
  output_Tecplot( pGlobalSol->primal0.qfld, qfld0_filename );
  output_Tecplot( pGlobalSol->primal1.qfld, qfld1_filename );

  pInterface->solveGlobalAdjointProblem();
//  std::rename("tmp/tracedump.dat", "tmp/tracedump_adjoint_a0.dat");
  std::string adjfld0_filename = "tmp/adjfld0_a0.plt";
  std::string adjfld1_filename = "tmp/adjfld1_a0.plt";
  output_Tecplot( pGlobalSol->adjoint0.qfld, adjfld0_filename );
  output_Tecplot( pGlobalSol->adjoint1.qfld, adjfld1_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<std::endl<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde_primal, pde_sensor,
                                                    order_primal, order_sensor, order_primal+1, order_sensor+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries_primal, active_boundaries_sensor, disc, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, ResNormType,
                                                           tol0, tol1, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList_Primal, PyBCList_Sensor,
                                                           BCBoundaryGroups_Primal, BCBoundaryGroups_Sensor,
                                                           SolverContinuationDict, outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

//    hfld_filename = "tmp/hfld_a" + std::to_string(iter+1) + ".plt";
//    output_Tecplot( get<0>(pGlobalSol->paramfld0), hfld_filename );

    qfld0_init_filename = "tmp/qfld0_init_a" + std::to_string(iter+1) + ".plt";
    qfld1_init_filename = "tmp/qfld1_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal0.qfld, qfld0_init_filename );
    output_Tecplot( pGlobalSol->primal1.qfld, qfld1_init_filename );

//    std::remove("tmp/tracedump.dat");
    pInterface->solveGlobalPrimalProblem();
//    std::rename("tmp/tracedump.dat",std::string("tmp/tracedump_primal_a" + std::to_string(iter+1) + ".dat").c_str());

    pInterface->solveGlobalAdjointProblem();
//    std::rename("tmp/tracedump.dat",std::string("tmp/tracedump_adjoint_a" + std::to_string(iter+1) + ".dat").c_str());

    qfld0_filename = "tmp/qfld0_a" + std::to_string(iter+1) + ".plt";
    qfld1_filename = "tmp/qfld1_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal0.qfld, qfld0_filename );
    output_Tecplot( pGlobalSol->primal1.qfld, qfld1_filename );

    adjfld0_filename = "tmp/adjfld0_a" + std::to_string(iter+1) + ".plt";
    adjfld1_filename = "tmp/adjfld1_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint0.qfld, adjfld0_filename );
    output_Tecplot( pGlobalSol->adjoint1.qfld, adjfld1_filename );
  }

  fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
