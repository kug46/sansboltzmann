// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGBR2_Triangle_RANSSA_FlatPlate_toy
// testing of 2-D DGBR2 for RANS-SA for flat plate

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/DistanceFunction/DistanceFunction.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/EPIC/XField_PX.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

using namespace std;
using namespace SANS;

// DOF functions - These are buggy and not used
  int choose(int n, int k)
  {
      // Classic n choose k
      if (k == 0)
              return 1;
//      int result = 1;
//      for (int i = 1; i <= k; i++) {
//          result *= (n - (k - i)) / i;
//      }
//      return result;
      return (n * choose(n-1, k-1))/k;
  }

  int get_Npoly(int p, int nspatial=2)
  {
      // Degrees of freedom in a single element
      int result = 0;
      for (int m = 0; m <= p; m++)
      {
          result += choose(nspatial + m - 1, m);
      }
      return result;
  }

  int get_DOF(int power, int Nelem_order1, int Npoly)
  {
      // Degrees of Freedom
      int DOF = pow(4, power-1) * Nelem_order1 * Npoly;
      return DOF;
  }


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Triangle_RANSSA_Joukowsi_UROP_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Triangle_RANSSA_Joukowsi_UROP )
{
  typedef QTypePrimitiveRhoPressure QType;
  //typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType> PrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamFieldTupleType> AlgebraicEquationSet_PTCClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;

  mpi::communicator world;

  std::vector<Real> tol = {1e-8, 1e-8};



  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.5;
  const Real Reynolds = 1e6;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
//  const Real aoaRef = 0.0;                            // angle of attack (radians)
//  const Real aoaRef = 7.0/360.0*2*PI; //12 degree alpha
//
//  const Real pRef = 1.0;
//  const Real tRef = pRef/rhoRef/R;
//  const Real cRef = sqrt(gamma*R*tRef);
//  const Real qRef = Mach*cRef;
//
//  const Real uRef = qRef * cos(aoaRef);             // velocity
//  const Real vRef = qRef * sin(aoaRef);

  const Real aoaRef = 0.0/360.0*2*PI; //0 degree alpha
  const Real qRef = 1;

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure

  // stagnation temperature, pressure


  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // DRAG INTEGRAND
  typedef BCRANSSA2D<BCTypeWall_mitState, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  NDBCClass bc(pde);
  IntegrandBCClass fcnBC( pde, bc, {0}, disc );

  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> IntegrandOutputClass;

  NDOutputClass outputFcn(pde, cos(aoaRef), sin(aoaRef) );
  IntegrandOutputClass outputIntegrand( outputFcn, {0} );

  NDOutputClass outputFcnLift(pde,-sin(aoaRef), cos(aoaRef) );
  IntegrandOutputClass outputIntegrandLift( outputFcnLift, {0} );

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;


  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;
  StateVector["rhont"]  = rhoRef*ntRef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0};
  BCBoundaryGroups["BCIn"] = {1,2};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

//  const std::vector<int> BoundaryGroups = {0,1,2,3,4,5};

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0)
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 8;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-13;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 20000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 5000;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict), PreconditionerILUAdjoint(PreconditionerILU);

  PreconditionerILUAdjoint[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 1;
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILUAdjoint;

  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 10000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;
  PETScDictAdjoint[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  //NewtonSolverDict[NewtonSolverParam::params.MaxChangeFraction] = 0.1;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
  NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/residual_history.dat";
//  NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor] = 2;
#endif

  PseudoTimeParam::checkInputs(NonlinearSolverDict);

  //   norm data
  //  Real normVec[10];   // L2 error
  //  int indx = 0;

  //  fstream fout( "tmp/DGBR2_V2_Ec0p3Re1.txt", fstream::out );
  //
  int powermin = 0; // Min is 0
  int powermax = 3; // Max is 5
  //
  int ordermin = 0;
  int ordermax = 3;

  int order_pseq = 0; // Order of initial guess. Min is 0.
  if (world.rank() == 0)
  {
  cout << "P-sequencing order of " << order_pseq << endl;
  }

  int Nelem_power1 = 2048; // Calculated from Tecplot

  bool test_functions = false;
  if (test_functions)
  {
      //cout << "NOTE: All these functions are wrong right now." << endl;

      // Test Npoly
      for (int p=0; p<=3; p++)
      {
          cout << p+1 << " choose " << p << ": " << choose(p+1, p) << endl;
          cout << "Npoly for p = " << p << ": " << get_Npoly(p) << endl;
      }
      cout << endl;

      // Test DOF
      int power = 0;
      int Npoly = 1;
      cout << "DOF for power=" << power << " Npoly=" << Npoly << ": " << get_DOF(power, Nelem_power1, Npoly) << endl;
      cout << endl;

      cout << "Ending program early." << endl;
      return;
  }

  // loop over grid resolution: 2^power
  //    int power = 0;
  for (int power = powermin; power <= powermax; power++)
  {

    string filein = "/home/jzavala/Workspace/Joukowski_data/Joukowski_RANS_Challenge_quad_ref";
    filein += to_string(power);
    filein += "_Q4.grm";

//    string filein = "/home/arthuang/SANS/test/sandbox/tmp/jouk/RANS_Mach05_Re1e6_aoa7/32k_P2/xfld_a31.grm";

    XField_PX<PhysD2, TopoD2> xfld(world, filein);

    int dorder = 3; // Distance order
    Field_CG_Cell<PhysD2, TopoD2, Real> distfld(xfld, dorder, BasisFunctionCategory_Lagrange);
    DistanceFunction(distfld, BCBoundaryGroups.at("BCNoSlip"));

    std::shared_ptr< Field_DG_Cell<PhysD2, TopoD2, ArrayQ>>  qfldOldPtr;

    int order = ordermin;
    //bool zero_order_ran = false;
    bool pseq_done = false;
    bool pseq_started = false;

    while (order <= ordermax)
    {

      //if (! zero_order_ran) order = 0;
      if (! pseq_started)
      {
          order = 0;
      }

      //int Npoly = get_Npoly(order);
      //int DOF = get_DOF(power, Nelem_power1, Npoly);


      std::shared_ptr<Field_DG_Cell<PhysD2, TopoD2, ArrayQ>>
      qfld( new Field_DG_Cell<PhysD2, TopoD2, ArrayQ>(xfld, order, BasisFunctionCategory_Legendre) );
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

      //      const int nDOFPDE = qfld.nDOF();
      //      const int nDOFLIFT = rfld.nDOF();

      int DOF = qfld->nDOF();

      if (world.rank() == 0)
      {
              cout << "P = " << order << " power = " << power << std::endl;
              cout << "DOF = " << DOF << endl;
      }

      if (! pseq_started)
      {
        // Set the uniform initial condition
        ArrayQ q0;
        pde.setDOFFrom( q0, SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntRef}) );

        //      std::cout << rhoRef << " " << uRef << " " << vRef << " " << pRef << "\n";

        *qfld = q0;
        for (int i=0; i<rfld.nDOF(); i++) rfld.DOF(i) = 0.0;
        lgfld = 0;
        pseq_started = true;
      }
      else
      {
        qfldOldPtr->projectTo(*qfld);
        for (int i=0; i<rfld.nDOF(); i++) rfld.DOF(i) = 0.0;
        lgfld = 0;
      }

      //qfld.nDOF();

      QuadratureOrder quadratureOrder( xfld, 3*order+1 );//3*order + 1 );


      //////////////////////////
      //SOLVE SYSTEM
      //////////////////////////

      ParamFieldTupleType paramfld = (distfld, xfld);
      PrimalEquationSetClass PrimalEqSet(paramfld, *qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_L2, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

      AlgebraicEquationSet_PTCClass AlgEqSetPTC(paramfld, *qfld, pde, quadratureOrder, {0}, PrimalEqSet);
#if 1
      // Create the pseudo time continuation class
      PseudoTime<SystemMatrixClass> PTC(NonlinearSolverDict, AlgEqSetPTC);

      BOOST_CHECK( PTC.iterate(1000) );

#else
      typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
      typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0;

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      bool solved = Solver.solve(ini,sln).converged;
      BOOST_CHECK(solved);
      PrimalEqSet.setSolutionField(sln);
#endif

      qfldOldPtr = qfld;

#if 1
      // Tecplot dump grid
      string filename2 = "tmp/DGBR2_RANSJoukowski_quad_UROP_P";
      filename2 += to_string(order);
      filename2 += "_X";
      filename2 += to_string(power);
      filename2 += ".plt";
      output_Tecplot( *qfld, filename2 );
      if (world.rank() == 0)
        std::cout << "Wrote Tecplot File\n";
#endif

      // Monitor Drag Error
      Real Drag = 0;
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          FunctionalBoundaryTrace_DGBR2( outputIntegrand, fcnBC, Drag ),
          (distfld, xfld), (*qfld, rfld), quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size() );

      Real Lift = 0;
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          FunctionalBoundaryTrace_DGBR2( outputIntegrandLift, fcnBC, Lift ),
          (distfld, xfld), (*qfld, rfld), quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size() );

      // Calculate DOF
      //int DOF = qfld->nDOF(); //rfld.nDOF();
      //cout << "This core's DOF: " << DOF << endl;

#ifdef SANS_MPI
      Drag = boost::mpi::all_reduce( *xfld.comm(), Drag, std::plus<Real>() );
      //DOF = boost::mpi::all_reduce( *xfld.comm(), DOF, std::plus<int>() );
#endif


      // drag coefficient
      Real Cd = Drag / (0.5*rhoRef*qRef*qRef*lRef);
      Real Cl = Lift / (0.5*rhoRef*qRef*qRef*lRef);

      // h from DOF
      double h = pow(DOF, -0.5);

#define SANS_VERBOSE
#ifdef SANS_VERBOSE

      if (world.rank() == 0)
      {
        cout << "P = " << order << " power = " << power <<
            ": Cd = " << std::setprecision(12) << Cd
            << ": Cl = " << std::setprecision(12) << Cl <<
            ": Drag = " << std::setprecision(12) << Drag
            << ": Lift = " << std::setprecision(12) << Lift <<
            ": DOF = " << DOF;
        cout << endl;
        cout << endl << endl;

            // Dump to UROP data file
          string data_fname = "tmp/RAW_DGBR2_RANS_Joukowski_UROP_alpha0_quad";
    //    data_fname += to_string(order);
    //    data_fname += "_X";
    //    data_fname += to_string(power);
          data_fname += ".dat";

          fstream data_fs;
          data_fs.open (data_fname, fstream::in | fstream::out | fstream::app);

          // Dump: order, power, DOF, h, Cd (comma separated)
          data_fs << order << "," << power << "," << DOF << "," << h << "," << std::setprecision(12) << Cd << endl;
          data_fs.close();
      }
#endif

#if 0
      fstream fout( "tmp/flatplate_dgbr2.txt", fstream::out );
      fout << "P = " << order << " power = " << power <<
          ": Cd = " << std::setprecision(12) << Cd;
      fout << endl;
#endif


#if 0
      //Tecplot dump grid
      string filename = "tmp/DGBR2_Joukowski_alpha7_adapt.plt";
//      filename += std::to_string(power);
//      filename += "_P";
//      filename += std::to_string(order);
//      filename += ".plt";
      output_Tecplot( *qfld, filename );
      std::cout << "Wrote Tecplot File\n";
#endif

      if (! pseq_done && order == order_pseq)
      {
          order = ((ordermin == 0) ? 0 : ordermin-1);
          pseq_done = true;
      }

    // cout << "Order will now be " << order + 1 << endl;
    order++;
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
