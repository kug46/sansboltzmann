// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_DGBR2_Line_NS_Nozzle_toy
// testing of 2-D DG Advective for Euler on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/PDENavierStokes1D.h"
#include "pde/NS/BCNavierStokes1D.h"
#include "pde/NS/SolutionFunction_Euler1D.h"
#include "pde/NS/Fluids1D_Sensor.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"

#include "pde/BCParameters.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h" // HACK: This should go away BEFORE being commited

#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGBR2_Line_NS_Nozzle_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGBR2_Line_Euler_Nozzle )
{
//  typedef QTypeEntropy QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveRhoPressure QType;
  typedef QTypePrimitiveSurrogate QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes1D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes1DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD1, TopoD1>> PDEPrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD1, TopoD1>> AlgebraicEquationSet_PTCClass;
  typedef PDEPrimalEquationSetClass::BCParams BCParams;

  typedef PDEPrimalEquationSetClass::SystemMatrix SystemMatrix;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  Real gamma = 1.4;
  Real R = 1.0;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );
  Real muRef = 1.75e-2;
  Real Prandtl = 0.75;
  Real Cp = gamma/(gamma-1)*R;
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, PDEClass::Euler_ResidInterp_Raw, area);

  Real rho = 1.0;
  Real t =  1.0;
  Real M = 4.37;
  Real u = M * sqrt(gamma*R*t);
  Real p = gas.pressure(rho, t);
  Real pr = 4.07;

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict Inflow;
  Inflow[NSVariableType1DParams::params.StateVector.Variables] = NSVariableType1DParams::params.StateVector.PrimitivePressure;
  Inflow[DensityVelocityPressure1DParams::params.rho] = rho;
  Inflow[DensityVelocityPressure1DParams::params.u] = u;
  Inflow[DensityVelocityPressure1DParams::params.p] = p;

  PyDict BCInflowSupersonic;
  BCInflowSupersonic[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCInflowSupersonic[BCEuler1DFullStateParams<NSVariableType1DParams>::params.Characteristic] = true;
  BCInflowSupersonic[BCEuler1DFullStateParams<NSVariableType1DParams>::params.StateVector] = Inflow;

  PyDict PyPDEBCList;
  PyPDEBCList["BCInflowSupersonic"] = BCInflowSupersonic;
  std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;
  PDEBCBoundaryGroups["BCInflowSupersonic"] = {0}; //left

#define PRESSURE_EXIT
#ifdef PRESSURE_EXIT
  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = p * pr;
  std::cout << "Outflow pressure: " << p * pr << " ("  << p << ")" << std::endl;
  PyPDEBCList["BCOut"] = BCOut;
  PDEBCBoundaryGroups["BCOut"] = {1};
#else
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;
  PyPDEBCList["BCNone"] = BCNone;
  PDEBCBoundaryGroups["BCNone"] = {1};
#endif
  //No exceptions should be thrown
  BCParams::checkInputs(PyPDEBCList);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Initial condition
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef SolutionFunction_Euler1D_Riemann<TraitsSizeEuler, TraitsModelNavierStokesClass> SolutionClass;
  typedef SolnNDConvertSpace<PhysD1, SolutionClass> SolutionNDClass;
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.interface] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.rhoL] = rho;
  solnArgs[SolutionClass::ParamsType::params.uL] = u;
  solnArgs[SolutionClass::ParamsType::params.pL] = p;
  solnArgs[SolutionClass::ParamsType::params.rhoR] = 0.8525;
  solnArgs[SolutionClass::ParamsType::params.uR] = 0.2348;
  solnArgs[SolutionClass::ParamsType::params.pR] = p*pr;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionNDClass solnExact(solnArgs);

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "Running full test..." << endl;
#endif
  int ordermax = 5;
#else
  int ordermax = 1;//2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    // loop over grid resolution
    int factormin = 1;
#ifdef SANS_FULLTEST
    int factormax = 6;
#else
    int factormax = 1;//4;
#endif
    for (int factor = factormin; factor <= factormax; factor++)
    {
      int ii = 30*factor;
      XField1D xfld( ii, 0, 1 );

      ////////////////////////////////////////////////////////////////////////////////////////
      // Allocate our Euler PDE field
      ////////////////////////////////////////////////////////////////////////////////////////
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> pde_qfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> pde_rfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> pde_lgfld(xfld, order, BasisFunctionCategory_Legendre,
                                                               BCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups));
      for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );
      pde_rfld = 0;
      pde_lgfld = 0;

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our AES to solve our PDE mit AV
      ////////////////////////////////////////////////////////////////////////////////////////
      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-10, 1e-10};
      Real viscousEtaParameter = 2;
      DiscretizationDGBR2 disc(0, viscousEtaParameter);
      PDEPrimalEquationSetClass PDEPrimalEqSet(xfld, pde_qfld, pde_rfld, pde_lgfld, pde,
                                               disc, quadratureOrder, ResidualNorm_Default, tol,
                                               {0}, {0}, PyPDEBCList, PDEBCBoundaryGroups);


      ////////////////////////////////////////////////////////////////////////////////////////
      // Newton Solver
      ////////////////////////////////////////////////////////////////////////////////////////
      PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
      UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
      LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

      NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
      NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
      NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
      NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1000;
      NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
//#define NEWTON
#ifdef NEWTON
      NewtonSolverParam::checkInputs(NewtonSolverDict);
      NewtonSolver<SystemMatrixClass> SolverBlock( PDEPrimalEqSet, NewtonSolverDict );

      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial guess
      ////////////////////////////////////////////////////////////////////////////////////////
      SystemVectorClass q(PDEPrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PDEPrimalEqSet.vectorStateSize());
      PDEPrimalEqSet.fillSystemVector(q);
      sln = q;

      ////////////////////////////////////////////////////////////////////////////////////////
      // SOLVE
      ////////////////////////////////////////////////////////////////////////////////////////
      SolveStatus status = SolverBlock.solve(q, sln);
      BOOST_CHECK( status.converged );
#else
      NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
      NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
      PyDict NonLinearSolverDict;
      NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

      Real invCFL = 10;
      Real invCFL_min = 1.0;
      Real invCFL_max = 1000.0;
      Real CFLDecreaseFactor = 0.9;
      Real CFLIncreaseFactor = 1.1;
      Real CFLPartialStepDecreaseFactor = 0.9;
      // Create the pseudo time continuation class
      AlgebraicEquationSet_PTCClass AlgEqSetPTC(xfld, pde_qfld, pde, quadratureOrder, {0}, PDEPrimalEqSet);
      PseudoTime<SystemMatrix>
         PTC(invCFL, invCFL_max, invCFL_min, CFLDecreaseFactor, CFLIncreaseFactor, CFLPartialStepDecreaseFactor,
             NonLinearSolverDict, AlgEqSetPTC, true);

      BOOST_CHECK( PTC.iterate(100) );
#endif

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii;
      cout << endl;
#endif
#if 1
      // Gnuplot dump grid
      string gfilename = "tmp/slnDG_NSNozzle_P";
      gfilename += to_string(order);
      gfilename += "_";
      gfilename += to_string(ii);
      gfilename += ".gplt";
      output_gnuplot( pde_qfld, gfilename );
#endif
#if 0
      // gnuplot dump
      string gfilenameS = "tmp/sensorDG_P";
      gfilenameS += to_string(order);
      gfilenameS += "_";
      gfilenameS += to_string(ii);
      gfilenameS += ".gplt";
      output_gnuplot( sensorfld, gfilenameS );
#endif
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
