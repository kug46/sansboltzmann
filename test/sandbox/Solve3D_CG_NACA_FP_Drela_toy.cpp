// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve3D_CG_NACA_LIP_btest
// testing of 3-D CG Linarized Incompressible Potential on a NACA airfoil

#undef SANS_FULLTEST
#undef TIMING
//#undef SANS_AFLR

//#define LG_WAKE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"

#include "Python/InputException.h"

#include "Surreal/SurrealS.h"
#include "pde/FullPotential/PDEFullPotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"
#include "pde/FullPotential/BCFullPotential3D.h"
#include "pde/FullPotential/FullPotential3D_Trefftz.h"
#include "pde/FullPotential/IntegrandBoundary3D_FP_Force.h"
#include "pde/FullPotential/IntegrandFunctorBoundary3D_LIP_Vn2.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/LIP_HACK/IntegrandBoundaryTrace_LinearScalar_sansLG_FP_HACK.h"

#include "Discretization/Galerkin/FunctionalBoundaryTrace_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Galerkin.h"

#include "Discretization/Potential_Drela/ResidualCell_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/ResidualBoundaryTrace_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/ResidualBoundaryFrame_CG_Potential_Drela.h"

#include "Discretization/Potential_Drela/JacobianCell_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianBoundaryTrace_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianBoundaryFrame_CG_Potential_Drela.h"

#include "Discretization/Potential_Drela/IntegrandBoundaryFrame_CG_FP_Drela.h"

#include "Discretization/Potential_Drela/ResidualBoundaryTrace_WakeCut_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianBoundaryTrace_WakeCut_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/IntegrandBoundaryTrace_WakeCut_CG_FP_Drela.h"

#include "Discretization/Potential_Drela/AlgebraicEquationSet_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/ResidualParamSensitivity_CG_Potential_Drela.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"


#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryFrame.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/HField/HFieldVolume_DG.h"
#include "Field/Tuple/FieldTuple.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/Direct/MKL_PARDISO/MKL_PARDISOSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "Meshing/EGTess/makeWakedAirfoil.h"
#include "Meshing/EGADS/Airfoils/NACA4.h"
#include "Meshing/EGTess/WakedFarFieldBox.h"
#include "Meshing/TetGen/EGTetGen.h"

#include "Meshing/AFLR/AFLR3.h"

#include "unit/UnitGrids/XField3D_Box_Hex_X1_WakeCut.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1_WakeCut.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;
using namespace EGADS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_CG_NACA_FP_Drela_test_suite )

#if 1
//----------------------------------------------------------------------------//
// solution: Hierarchical P1
// wake included with other BC regions
BOOST_AUTO_TEST_CASE( Solve3D_NACA_Test_Working )
{
  typedef PDENDConvertSpace<PhysD3, PDEFullPotential3D<Real>> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef PDENDConvertSpace<PhysD3, PDEFullPotential3D<SurrealS<1>>> PDESensClass;

  typedef BCNDConvertSpace<PhysD3, BCFullPotential3D<BCTypeInflow, Real> > BCInflow;
  typedef BCNDConvertSpace<PhysD3, BCFullPotential3D<BCTypeOutflow, Real> > BCOutflow;
  typedef BCNDConvertSpace<PhysD3, BCFullPotential3D<BCTypeWall, Real> > BCWall;


  typedef FieldTuple< Field<PhysD3, TopoD3, Real>, XField3D_Wake, TupleClass<> > FieldParamType;

  typedef AlgebraicEquationSet_CG_Potential_Drela<PDEClass, BCNDConvertSpace, BCFullPotential3DVector<Real>,
                                              AlgEqSetTraits_Sparse, FieldParamType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef ResidualParamSensitivity_CG_Potential_Drela< PDESensClass, BCNDConvertSpace, BCFullPotential3DVector<SurrealS<1>>,
                                                   SurrealS<1>, FieldParamType > SensEquationSetClass;
  typedef SensEquationSetClass::SystemVectorClass SystemVectorSurrealClass;


  timer totaltime;

  timer meshtime;

  //int factor = 1;

//  #undef SANS_AFLR

#if 1
#if 0
  Real span = 4;

  //bool withWake = false;
  const int nChord = 5;
  const int nSpan  = span*nChord+1;
  const int nWake  = 5; //nChord;

  // Mark the wake or not as a wake
  bool withWake = true;

  EGContext context((CreateContext()));

  //EGApproximate<3> NACA_spline = NACA4<3>(context, 0.001, 0.3, 0);
  EGApproximate<3> NACA_spline = NACA4<3>(context, 0.12, 0.3, 0);

  // EGADS mesh parameters
  std::vector<double> EGADSParams = {50., 0.01, 12.0};
  std::vector<double> outflowParams = {2.5, 0.001, 15.0};

  //{32.,6.,320.} {1.5,0.75,2.}
  EGModel<3> model = makeWakedAirfoil(NACA_spline, TrefftzFrames, span, {32./4.,6./2.,320./4.}, nChord, nSpan, nWake, outflowParams, withWake);
  //model.save( "tmp/NACAWake3D.egads");

  // Simple NACA hershey bar
  Real Sref = span*1;

#elif 0

  EGContext context((CreateContext()));

  std::vector<double> EGADSParams = {0.005, 0.0005, 12.0};
  std::vector<double> boxsize = {-50, -50, -50, 100, 100, 100};

  EGModel<3> model(context);
  model.load("tmp/glider.egads");

  double size = model.getSize();

  // Scale the egads parameters relative to the model size
  EGADSParams[0] *= size;
  EGADSParams[1] *= size;

  model = EGModel<3>( WakedFarFieldBox( model.getBodies(), boxsize, TrefftzFrames ) );

  Real Sref = 100;

#else

  EGContext context((CreateContext()));

  std::vector<double> EGADSParams = {0.025, 0.001, 15.0};

  EGModel<3> model(context);
  //model.load("tmp/RAE.egads");
  model.load("tmp/GMGW.egads");

  Real Sref = 4240;

  double size = model.getSize();

  // Scale the egads parameters relative to the model size
  EGADSParams[0] *= size;
  EGADSParams[1] *= size;

#if 0
  //model.load("tmp/RAEwingsNoQuad1.egads");

  std::vector<double> EGADSParams = {0.5, 0.0005, 12.0};

  // The RAE model is scaled by 100
  Real Lref = (228.6 + 76.2)/2.*10;
  Real Sref = 457.2*2*Lref*10;

  std::vector<EGFace<3>> solid_faces = model.getBodies()[2].getFaces();

  solid_faces[18].delAttribute(".tParams");
  solid_faces[19].delAttribute(".tParams");

  solid_faces[18].addAttribute(".tParams", {317.139179/3., 12.685567, 15.000000} );
  solid_faces[19].addAttribute(".tParams", {317.139179/3., 12.685567, 15.000000} );

#if 0
  std::vector<EGEdge<3>> solid_edges = model.getBodies()[2].getEdges();

  solid_edges[39].addAttribute("Wake", "Wing_Wake");
  solid_edges[30].addAttribute("Wake", "Wing_Wake");
  solid_edges[22].addAttribute("Wake", "Wing_Wake");

  solid_edges[34].addAttribute("Wake", "Wing_Wake");
  solid_edges[26].addAttribute("Wake", "Wing_Wake");
  solid_edges[18].addAttribute("Wake", "Wing_Wake");
#endif
#endif
#endif

#if 0
  EGModel<3> model(context);
  model.load("tmp/glider.egads");

  Real size = model.getSize();
  std::cout << "Model size: " << size << std::endl;

  EGADSParams[0] = 0.25*size;
  EGADSParams[1] = 0.001*size;

#if 0 //RAE
  std::vector<EGEdge<3>> wake1_edges = model.getBodies()[0].getEdges();
  std::vector<EGEdge<3>> wake2_edges = model.getBodies()[1].getEdges();
  std::vector<EGEdge<3>> solid_edges = model.getBodies()[2].getEdges();

  std::vector<EGFace<3>> wake1_faces = model.getBodies()[0].getFaces();
  std::vector<EGFace<3>> wake2_faces = model.getBodies()[1].getFaces();

  wake1_faces[1-1].addAttribute(".tParams", {634.278358/2., 12.685567, 15.000000});
  wake2_faces[1-1].addAttribute(".tParams", {634.278358/2., 12.685567, 15.000000});

  std::vector<EGFace<3>> solid_faces = model.getBodies()[2].getFaces();

  solid_faces[8-1].delAttribute(".tParams");
  solid_faces[9-1].delAttribute(".tParams");
  solid_faces[10-1].delAttribute(".tParams");
  solid_faces[11-1].delAttribute(".tParams");

  std::vector<double> rSpan(nSpan-2);
  for ( int i = 1; i < nSpan-1; i++ )
    //rSpan[i-1] = i/Real(nSpan-1);
    rSpan[i-1] = 1-0.5*(1+cos(PI*i/Real(nSpan-1)));


  std::vector<double> rChord(nChord-2);
  for ( int i = 1; i < nChord-1; i++ )
    //rCos[i-1] = i/Real(nCos-1);
    rChord[i-1] = 1-0.5*(1+cos(PI*i/Real(nChord-1)));

  // LE
  solid_edges[66-1].addAttribute(".rPos", rSpan);
  solid_edges[60-1].addAttribute(".rPos", rSpan);

  // TE
  solid_edges[68-1].addAttribute(".rPos", rSpan);
  solid_edges[63-1].addAttribute(".rPos", rSpan);

  // Airfoils
  solid_edges[55-1].addAttribute(".rPos", rChord);
  solid_edges[53-1].addAttribute(".rPos", rChord);
  solid_edges[56-1].addAttribute(".rPos", rChord);
  solid_edges[52-1].addAttribute(".rPos", rChord);
  solid_edges[67-1].addAttribute(".rPos", rChord);
  solid_edges[64-1].addAttribute(".rPos", rChord);
  solid_edges[69-1].addAttribute(".rPos", rChord);
  solid_edges[62-1].addAttribute(".rPos", rChord);

  const int iiwake = MAX(nWake, 3);
#if 0
  // stagnation line: quadratic increments w/ dx1 and dx2 matching airfoil
  double x1 = rChord[rChord.size() - 1];
  double x2 = rChord[rChord.size() - 2];

  double xle = 0;
  double xte = 1;

  double dx1 = xte - x1;
  double dx2 = x1 - x2;

  double rffd = 2 - xte/2.;

  double a = (2*((rffd + (xle+xte)/2) - xte) + iiwake*((iiwake - 3)*dx1 - (iiwake - 1)*dx2)) / ((double) (2*iiwake*(2 - iiwake*(3 - iiwake))));
  double b = 0.5*(dx2 - dx1) - 3*a;
  double c = 0.5*(3*dx1 - dx2) + 2*a;

  int iim = iiwake-1;
  std::vector<double> rwk(iiwake-2);
  for (int i = 1; i < iiwake-1; i++)
    rwk[i-1] = i*(c + i*(b + i*a))/(iim*(c + iim*(b + iim*a)));
#endif

#if 1 //Wakes
  std::vector<double> rwk(iiwake-2);
  for (int i = 1; i < iiwake-1; i++)
    rwk[i-1] = (1-0.5*(1+cos(0.5*PI*i/Real(iiwake-1))))/0.5;


  std::vector<double> rwkinv(iiwake-2);
  for (int i = 0; i < iiwake-2; i++)
    rwkinv[i] = 1-rwk[iiwake-3-i];


  wake1_edges[1-1].addAttribute(".rPos", rSpan);
  //wake1_edges[5-1].addAttribute(".rPos", rSpan);
  wake1_edges[8-1].addAttribute(".rPos", rwkinv);
  wake1_edges[9-1].addAttribute(".rPos", rwk);
  wake2_edges[1-1].addAttribute(".rPos", rSpan);
  //wake2_edges[3-1].addAttribute(".rPos", rSpan);
  wake2_edges[8-1].addAttribute(".rPos", rwk);
  wake2_edges[9-1].addAttribute(".rPos", rwkinv);
#endif
#endif
#endif


  // Find the BC faces
  std::map<std::string, std::vector<int> > BCFaces;
  std::vector<int> WakeFaces;
  std::vector<int> TrefftzFrames;

  {
    // Find the Wake and BC faces
    std::vector< EGADS::EGBody<3> > bodies = model.getBodies();
    std::map<int,std::vector<int>> missingBC;
    {
      int BCFace = 0;
      for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ ) //Solid bodies
      {
        if (!bodies[ibody].isSolid()) continue;

        std::vector< EGADS::EGFace<3> > faces = bodies[ibody].getFaces();
        for (std::size_t i = 0; i < faces.size(); i++)
        {
          //faces[i].printAttributes(); std::cout << std::endl;
          if ( faces[i].hasAttribute("BCName") )
          {
            std::string BCName;
            faces[i].getAttribute("BCName", BCName);
            BCFaces[BCName].push_back(BCFace);
            BCFace++;
          }
          else if ( faces[i].hasAttribute("capsGroup") )
          {
            std::string BCName;
            faces[i].getAttribute("capsGroup", BCName);
            BCFaces[BCName].push_back(BCFace);
            BCFace++;
          }
          else
            missingBC[ibody+1].push_back(i+1);
        }
      }

      for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ ) //Sheet bodies
      {
        if (bodies[ibody].isSolid()) continue;
        std::vector< EGADS::EGFace<3> > faces = bodies[ibody].getFaces();
        for (std::size_t i = 0; i < faces.size(); i++)
        {
          if ( faces[i].hasAttribute("BCName") )
          {
            std::string WakeName;
            faces[i].getAttribute("BCName", WakeName);
            faces[i].addAttribute("Wake", WakeName);

            WakeFaces.push_back(BCFace);
            BCFace++;
          }
          else if ( faces[i].hasAttribute("capsGroup") )
          {
            std::string WakeName;
            faces[i].getAttribute("capsGroup", WakeName);
            faces[i].addAttribute("Wake", WakeName);
            faces[i].addAttribute("BCName", WakeName);

            WakeFaces.push_back(BCFace);
            BCFace++;
          }
          else
            missingBC[ibody+1].push_back(i+1);
        }
      }
    }

    if (!missingBC.empty())
    {
      std::cout << "Boundary conditions are missing on:" << std::endl;
      for (const std::pair<int,std::vector<int>>& faces : missingBC)
      {
        std::cout << "Body  : " << faces.first << std::endl;
        std::cout << "Faces : " << faces.second << std::endl;
        std::cout << std::endl;
      }
      SANS_ASSERT(false);
    }


    int nKuttaEdge = 0;

    for (std::size_t ibody = 0; ibody < bodies.size(); ibody++)
    {
      if (bodies[ibody].isSolid())
      {
        std::vector< EGADS::EGEdge<3> > solidEdges = bodies[ibody].getEdges();
        for ( const EGADS::EGEdge<3>& edge : solidEdges )
        {
          //edge.printAttributes(); std::cout << std::endl;
          if (edge.hasAttribute(XField3D_Wake::WAKESHEET))
            nKuttaEdge++;
        }
      }
    }

    int nTrefftzEdge = nKuttaEdge;

    for (std::size_t ibody = 0; ibody < bodies.size(); ibody++)
    {
      if (bodies[ibody].isSheet())
      {
        std::vector< EGADS::EGEdge<3> > sheetEdges = bodies[ibody].getEdges();
        for ( const EGADS::EGEdge<3>& edge : sheetEdges )
        {
          //edge.printAttributes(); std::cout << std::endl;
          if (edge.hasAttribute(XField3D_Wake::TREFFTZ))
          {
            TrefftzFrames.push_back(nTrefftzEdge);
            nTrefftzEdge++;
          }
        }
      }
    }

  }

  // BC
  PyDict BCWallDict;
  BCWallDict[BCParams::params.BC.BCType] = BCParams::params.BC.Wall;

  PyDict BCOutflowDict;
  BCOutflowDict[BCParams::params.BC.BCType] = BCParams::params.BC.Neumann;
  BCOutflowDict[BCOutflow::ParamsType::params.gradqn] = 0;

  PyDict BCInflowDict;
  BCInflowDict[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet;
  BCInflowDict[BCInflow::ParamsType::params.q] = 0;

  PyDict PyBCList;
  PyBCList["Wall"] = BCWallDict;
  PyBCList["Outflow"] = BCOutflowDict;
  PyBCList["Inflow"] = BCInflowDict;

  // Check that all BC types have a BC group, and vice versa
  checkBCInputs(PyBCList, BCFaces);

  std::cout << "Generating Surface Mesh" << std::endl;

  EGTessModel tessModel( model, EGADSParams );

  std::cout << "Generating Volume Mesh" << std::endl;

  // grid:
#ifdef SANS_AFLR
  AFLR3 xfld(tessModel);
#else
  // TetGen mesh parameters
  Real maxRadiusEdgeRatio = 1.1;//*2/1.1;
  Real minDihedralAngle = 25;//*0;

  EGTetGen xfld(tessModel, maxRadiusEdgeRatio, minDihedralAngle);
#endif

  std::cout << "Meshing Time: " << meshtime.elapsed() << std::endl;

  output_Tecplot( xfld, "tmp/GMGW_grid.dat" );
  //output_Tecplot( xfld, "tmp/NACA3D.plt" );

  //return;

  // quadrature rule
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 4);    // max
  std::vector<int> quadratureOrderMin(xfld.nBoundaryTraceGroups(), 0);     // min

#else

  XField3D_Box_Tet_X1_WakeCut xfld( nChord, nSpan, span, nWake );

  std::map<std::string, std::vector<int> > BCFaces;

  BCFaces["Wall"] = {6,7};
  BCFaces["Inflow"] = {0};
  BCFaces["Outflow"] = {1,2,3,4,5};
  //BCFaces["Wing_Wake"] = {8};
  std::vector<int> WakeFaces = {8};

  std::vector<int> TrefftzFrames = {1};

  // quadrature rule
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 2);    // max
  std::vector<int> quadratureOrderMin(xfld.nBoundaryTraceGroups(), 0);     // min

#endif

  // PDE
  Real Vinf = 1;

  // Shock with Minf = 0.75 and alpha = 2

  Real alphamin = 2; //0.01*180/PI;
  int nalpha = 1;
  Real alphamax = 2; //0.01*180/PI;

  Real gamma = 1.4;
  Real Minf = 0.0;
  Real rhoinf = 1.0;
  Real pinf = 1.0;
  if (Minf > 0 ) pinf = 1./(gamma*Minf*Minf);
  Real critVelFrac = 0.8;

  PDEClass pde( 1., 0., 0., gamma, Minf, rhoinf, pinf, critVelFrac );
  PDESensClass pdeSens( 1., 0., 0., gamma, Minf, rhoinf, pinf, critVelFrac );

  // BC needed for output functional

  BCWall bcwall(pde);

  std::cout << "Kutta point size: " << xfld.KuttaPoints_.size() << std::endl;

  int order = 1;

  // solution: Hierarchical, C0
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical),
                                        qfldnew(xfld, order, BasisFunctionCategory_Hierarchical);
  HField_DG<PhysD3, TopoD3> hfld(xfld);

//  Field_CG_Cell<PhysD3, TopoD3, DLA::VectorS<1,Real> > rsdfld(xfld, order, BasisFunctionCategory_Hierarchical);


  //output_Tecplot_FP( pde, qfld, "tmp/NACA_mesh.dat" );

  const int nDOFPDE = qfld.nDOF();

  std::cout << " nDOFPDE = " << nDOFPDE << std::endl;
  std::cout << " Duplicates after " << xfld.dupPointOffset_ << std::endl;

 //Dummy Lagrange multipliers for now...
#ifdef LG_WAKE
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order );
#else
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, {} );
#endif

  lgfld = 0;

  cout << "nDOFPDE = " << nDOFPDE << endl;
#if 0
  cout << "  InflowFaces = " << InflowFaces.size()
       << "  OutflowFaces = " << OutflowFaces.size()
       << "  WallFaces = " << WallFaces.size()
#endif
  cout << "  WakeFaces = " << WakeFaces << endl;
  cout << "  lgfld.nBoundaryTraceGroups() = " << lgfld.nBoundaryTraceGroups() << endl;

  FieldParamType fldParam(hfld, xfld);

  StabilizationNitsche stab(order);
  PrimalEquationSetClass
    PrimalEqSet(fldParam, qfld, lgfld, pde, stab, {0}, WakeFaces, PyBCList, BCFaces);

  SensEquationSetClass
    SensEqSet(fldParam, qfld, lgfld, pdeSens, stab, {0}, WakeFaces, PyBCList, BCFaces);

  // Solution and residual vector and jacobian matrix
  SystemVectorClass sln(PrimalEqSet.vectorStateSize()), q(PrimalEqSet.vectorStateSize()), qnew(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize()), rsdnew(PrimalEqSet.vectorEqSize());

  sln = 0;
  q = 0;

  qfld = ArrayQ({0,rhoinf});
  PrimalEqSet.fillSystemVector(q);

  // linear system setup
  {
    SystemNonZeroPattern nzPDE_q(PrimalEqSet.matrixSize());

    PrimalEqSet.jacobian(nzPDE_q);

    unsigned int maxRow = 0, maxRowSize = 0;
    for ( int i = 0; i < nzPDE_q(0,0).m(); i++ )
      if ( nzPDE_q(0,0).rowSize(i) > maxRowSize )
      {
        maxRow = i;
        maxRowSize = nzPDE_q(0,0).rowSize(i);
      }

    std::cout << "maxRow = " << maxRow << " maxRowSize = " << maxRowSize << std::endl;
  }

  //SystemMatrixClass jacPDE_q(nzPDE_q);


#if defined(INTEL_MKL)
  SLA::MKL_PARDISO<SystemMatrixClass> solver(PrimalEqSet);
  SLA::MKL_PARDISO<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);
#else
  SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
  SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);
#endif

  std::fstream filePolar("tmp/GMGW_FP_Polar.dat", std::ios::out);

  qfld = ArrayQ({0,rhoinf});

  PrimalEqSet.fillSystemVector(q);

  for ( int na = 0; na < nalpha; na++)
  {
    Real alpha = alphamin + na*(alphamax-alphamin)/std::max(1,nalpha-1);

    std::cout << "-----------------" << std::endl;
    std::cout << "Minf           = " << Minf << std::endl;
    std::cout << "alpha          = " << alpha << std::endl;
#if 0
    Real U = Vinf*cos(alpha*PI/180), V = Vinf*sin(alpha*PI/180), W = Vinf*0;
    DLA::VectorS<3, Real> Ddir = { U, V, W};
    DLA::VectorS<3, Real> Ldir = {-V, U, W};
#else
    Real U = Vinf*cos(alpha*PI/180), V = Vinf*0, W = Vinf*sin(alpha*PI/180);
    DLA::VectorS<3, Real> Ddir = {  U, V, W};
    DLA::VectorS<3, Real> Ldir = { -W, V, U};
#endif
    //Update the angle of attack
    pde.setFreestream(Ddir);

    Ddir /= Vinf;
    Ldir /= Vinf;

    for (int newton = 0; newton < 100; newton++)
    {
      rsd = 0;

      timer rsdtime;

      // residual

      PrimalEqSet.residual(q, rsd);

      std::cout << "Residual Time: " << rsdtime.elapsed() << std::endl;

      //WriteMatrixMarketFile(jacPDE_q, "tmp/FP.mtx");

      Real rsdPDEnrmPhi = 0;
      Real rsdPDEnrmRho = 0;
      for (int n = 0; n < nDOFPDE; n++)
      {
        rsdPDEnrmPhi += rsd(0)[n][0]*rsd(0)[n][0];
        rsdPDEnrmRho += rsd(0)[n][1]*rsd(0)[n][1];
      }
      rsdPDEnrmPhi = sqrt(rsdPDEnrmPhi);
      rsdPDEnrmRho = sqrt(rsdPDEnrmRho);

      std::cout << "Newton " << newton << " rsd Phi = " << rsdPDEnrmPhi << " Rho = " << rsdPDEnrmRho << std::endl;

      if (rsdPDEnrmPhi < 1e-10 && rsdPDEnrmRho < 0.001) break;

      timer solvetime;

      solver.solve(rsd, sln);

      std::cout << "Solve Time: " << solvetime.elapsed() << std::endl;

      // updated solution
      Real eps = 1;
      Real rsdPDEnrmPhinew = rsdPDEnrmPhi+1;
      Real rsdPDEnrmRhonew = rsdPDEnrmRho+1;
      while (rsdPDEnrmPhinew > rsdPDEnrmPhi || (rsdPDEnrmRhonew > rsdPDEnrmRho && rsdPDEnrmRho > 1e8) )
      {
        qnew = q - eps*sln;

        if ( !PrimalEqSet.isValidStateSystemVector(qnew) )
        {
          std::cout << "Invalid state: eps = " << eps << std::endl;
          eps /= 2;
          if (eps < 1e-6)
          {
            std::cout << "Could not find valid state..." << std::endl;
            break;
          }
          continue;
        }


        rsd = 0;
        PrimalEqSet.residual(qnew, rsd);

        rsdPDEnrmPhinew = 0;
        rsdPDEnrmRhonew = 0;
        for (int n = 0; n < nDOFPDE; n++)
        {
          rsdPDEnrmPhinew += rsd(0)[n][0]*rsd(0)[n][0];
          rsdPDEnrmRhonew += rsd(0)[n][1]*rsd(0)[n][1];
        }
        rsdPDEnrmPhinew = sqrt(rsdPDEnrmPhinew);
        rsdPDEnrmRhonew = sqrt(rsdPDEnrmRhonew);


        std::cout << "rsdPDEnrmPhinew = " << rsdPDEnrmPhinew << " rsdPDEnrmRhonew = " << rsdPDEnrmRhonew << " eps = " << eps << std::endl;
//        break;

        if (rsdPDEnrmPhinew > rsdPDEnrmPhi || (rsdPDEnrmRhonew > rsdPDEnrmRho && rsdPDEnrmRho > 1e8)) eps /= 2;
        if (eps < 1e-6)
        {
          std::cout << "Line search failed..." << std::endl;
          break;
        }

      }

      // Update the solution
      q = qnew;

      if (eps < 1e-6) break;
    }

    // check that the residual is zero
#if 1
    rsd = 0;
    PrimalEqSet.residual(q, rsd);

    Real rsdPDEnrm = 0;
    for (int n = 0; n < nDOFPDE; n++)
      rsdPDEnrm += dot(rsd(0)[n],rsd(0)[n]);

    BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-10 );
#endif
    Real Drag = 0;
    Real TrefftzLift = 0;
    DLA::VectorS<3,Real> PressureForce = 0;
    Real Vn2 = 0;
    quadratureOrder[0] = 2;
    quadratureOrder[1] = 2;

    IntegrandBoundaryFrame3D_FP_Trefftz<TrefftzInducedDrag, Real> fcnTrefftzDrag(pde, TrefftzFrames);
    IntegrandBoundaryFrame3D_FP_Trefftz<TrefftzPlaneForce, Real> fcnTrefftzLift(pde, Ldir, TrefftzFrames);

    FPFunctionalTrefftz::integrate(fcnTrefftzDrag, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), Drag);
    FPFunctionalTrefftz::integrate(fcnTrefftzLift, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), TrefftzLift);

    IntegrandBoundary3D_FP_Force<Real> fcnBodyForce(pde, BCFaces.at("Wall"));

    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( fcnBodyForce, PressureForce ), xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

    Real rho = 1;
    Real Vinf2 = U*U + V*V + W*W;

    Drag /= (0.5*rho*Vinf2*Sref);
    TrefftzLift /= (0.5*rho*Vinf2*Sref);

    Real CL = dot(PressureForce,Ldir)/(0.5*rho*Vinf2*Sref);
    Real CD = dot(PressureForce,Ddir)/(0.5*rho*Vinf2*Sref);

    std::cout << std::setprecision(16);
    std::cout << "Trefftz Drag   = " << Drag << std::endl;
    std::cout << "Trefftz Lift   = " << TrefftzLift << std::endl;
    std::cout << "Pressure Force = " << PressureForce << std::endl;
    std::cout << "Pressure Drag  = " << CD << std::endl;
    std::cout << "Pressure Lift  = " << CL << std::endl;
    std::cout << "L2 Vn  = " << sqrt(Vn2) << std::endl;

#if 0
    {
      Real alpha2(alpha+0.0001);

      U = Vinf*cos(alpha2*PI/180); V = Vinf*0; W = Vinf*sin(alpha2*PI/180);
      Ddir = {  U, V, W};
      Ldir = { -W, V, U};

      Ddir /= Vinf;
      Ldir /= Vinf;

      pde.setFreestream(Ddir);

      DLA::VectorS<3, Real> PressureForce2 = 0;

      IntegrateBoundaryTraceGroups<TopoD3>::integrate(
          FunctionalBoundaryTrace_sansLG( fcnBodyForce, PressureForce2 ), xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

      PressureForce2 /= (0.5*rho*Vinf2*Sref);

      std::cout << "Pressure Force2 = " << PressureForce2 << std::endl;

      Real CL2 = dot(PressureForce2,Ldir);
      std::cout << " DCL/Dalpha1 = " << (CL2-CL)/0.0001 << std::endl;

      // Remove the pertubation
      U = Vinf*cos(alpha*PI/180); V = Vinf*0; W = Vinf*sin(alpha*PI/180);
      Ddir = {  U, V, W};
      Ldir = { -W, V, U};

      Ddir /= Vinf;
      Ldir /= Vinf;

      pde.setFreestream(Ddir);
    }
#endif


#if 1
  // Tecplot dump
  string filename = "tmp/NACA0012_FP.dat";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot_FP( pde, qfld, filename );
#endif


    //SystemNonZeroPattern nzPDE_qT(Transpose(nzPDE_q));
    //SystemMatrixClass jacPDE_qT(nzPDE_qT);
    //jacPDE_qT = 0.0;
    //PrimalEqSet.jacobianTranspose(q, jacPDE_qT);

    SLA::SparseVector< IntegrandBoundary3D_FP_Force<Real>::MatrixJ<Real> > rhsVec(PrimalEqSet.vectorEqSize()[0]);
    SystemVectorClass g_CL(PrimalEqSet.vectorStateSize());
    SystemVectorClass g_CD(PrimalEqSet.vectorStateSize());
    SystemVectorClass g_CDT(PrimalEqSet.vectorStateSize());
    SystemVectorClass v_CL(PrimalEqSet.vectorStateSize());
    SystemVectorClass v_CD(PrimalEqSet.vectorStateSize());
    SystemVectorClass v_CDT(PrimalEqSet.vectorStateSize());

    rhsVec = 0;
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        JacobianFunctionalBoundaryTrace_Galerkin<SurrealS<2>>( fcnBodyForce, rhsVec ),
        xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );


    for (int i = 0; i < rhsVec.m(); i++)
    {
      g_CL[0][i] = 0;
      g_CD[0][i] = 0;
      for (int d = 0; d < PhysD3::D; d++)
      {
        for (int m = 0; m < ArrayQ::M; m++)
        {
          g_CL[0][i][m] += (rhsVec[i](m,d)*Ldir[d])/(0.5*rho*Vinf2*Sref);
          g_CD[0][i][m] += (rhsVec[i](m,d)*Ddir[d])/(0.5*rho*Vinf2*Sref);
        }
      }
    }

    g_CDT = 0;
    FPJacobianFunctionalTrefftz<SurrealS<2>>::integrate(fcnTrefftzDrag, xfld, qfld,
                                                        &quadratureOrder[0], xfld.nBoundaryFrameGroups(), g_CDT(0));
    g_CDT /= (0.5*rho*Vinf2*Sref);


    // Partial derivative of output with alpha
    Real CL_alpha = 0, CD_alpha = 0, CDT_alpha = 0;

    {
      SurrealS<1> alpha2(alpha);
      alpha2.deriv() = 1;

      SurrealS<1> U = Vinf*cos(alpha2*PI/180), V = Vinf*0, W = Vinf*sin(alpha2*PI/180);
      DLA::VectorS<3, SurrealS<1>> Ddir = {  U, V, W};
      DLA::VectorS<3, SurrealS<1>> Ldir = { -W, V, U};

      Ddir /= Vinf;
      Ldir /= Vinf;

#if 0
      for (int i = 0; i < qfld.nDOF(); i++)
      {
        for (int n = 0; n < ArrayQ::M; n++)
        {
          Real eps = 0.0000001;

          qfld.DOF(i)[n] += eps;

          DLA::VectorS<3, Real> PressureForce3 = 0;
          IntegrateBoundaryTraceGroups<TopoD3>::integrate(
              FunctionalBoundaryTrace_sansLG( fcnBodyForce, PressureForce3 ), xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

          qfld.DOF(i)[n] -= eps;
#if 1
          for (int d = 0; d < PhysD3::D; d++)
            if (fabs(rhsVec[i][d][n] - (PressureForce3[d]-PressureForce[d])/eps) > 1e-6 )
              std::cout << "rhsVec[" << i << "][" << d << "][" << n << "] = " << rhsVec[i][d][n]
                        << " DP/DU = " << (PressureForce3[d]-PressureForce[d])/eps
                        << " diff : " << fabs(rhsVec[i][d][n] - (PressureForce3[d]-PressureForce[d])/eps) << std::endl;
#endif

#if 1
          DLA::VectorS<3, Real> LdirValue = { Ldir[0].value(), Ldir[1].value(), Ldir[2].value()};

          Real CL2 = dot(PressureForce3, LdirValue)/(0.5*rho*Vinf2*Sref);

          if (fabs(g[0][i][n] - (CL2-CL)/eps) > 1e-6 )
          {
            std::cout << "g[" << i << "][" << n << "] = " << g[0][i][n]
                      << " DCL/DU = " << (CL2-CL)/eps
                      << " diff : " << fabs(g[0][i][n] - (CL2-CL)/eps) << std::endl;
          }
#endif
        }
      }
#endif
      solverAdj.factorize();
      solverAdj.backsolve(g_CL, v_CL);
      solverAdj.backsolve(g_CD, v_CD);
      solverAdj.backsolve(g_CDT, v_CDT);

      std::cout << "Ddir = " << Ddir << std::endl;
      std::cout << "Ldir = " << Ldir << std::endl;
      pdeSens.setFreestream(Ddir);

      IntegrandBoundary3D_FP_Force<SurrealS<1>> fcnBodyForce(pdeSens, BCFaces.at("Wall"));

      DLA::VectorS<3, SurrealS<1>> PressureForce2 = 0;
      IntegrateBoundaryTraceGroups<TopoD3>::integrate(
          FunctionalBoundaryTrace_Galerkin( fcnBodyForce, PressureForce2 ),
          xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

      PressureForce2 /= (0.5*rho*Vinf2*Sref);

      std::cout << "Pressure Force = " << PressureForce2 << std::endl;

      CL_alpha = dot(PressureForce2,Ldir).deriv();
      CD_alpha = dot(PressureForce2,Ddir).deriv();
      std::cout << " CL_alpha1 = " << CL_alpha << " CD_alpha1 = " << CD_alpha << std::endl;


      IntegrandBoundaryFrame3D_FP_Trefftz<TrefftzInducedDrag, SurrealS<1>> fcnTrefftzDragSens(pdeSens, TrefftzFrames);
      SurrealS<1> CDT = 0;
      FPFunctionalTrefftz::integrate(fcnTrefftzDragSens, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), CDT);
      CDT /= (0.5*rho*Vinf2*Sref);
      CDT_alpha = CDT.deriv();
      std::cout << " CDT_alpha1 = " << CDT_alpha << std::endl;


      SystemVectorSurrealClass rsd(PrimalEqSet.vectorEqSize());
      SystemVectorClass f(PrimalEqSet.vectorEqSize());
      //SystemVectorClass u(PrimalEqSet.vectorStateSize());

      // Compute f = -dN/dalpha
      rsd = 0;
      SensEqSet.residualSensitivity(rsd);

      // Extract the derivatives from the residual vector
      for (int j = 0; j < rsd.m(); j++)
        for (int k = 0; k < rsd[j].m(); k++)
          for (int m = 0; m < ArrayQ::M; m++)
            f[j][k][m] = -rsd[j][k][m].deriv();

//      u = solver.inverse(jacPDE_q)*f;
//      CL_alpha += dot(g,u);

      // compute DCL/Dalpha = g^T u + dCL/dalpha = v^T*f + dCL/dalpha
      CL_alpha += dot(v_CL, f);
      CD_alpha += dot(v_CD, f);
      CDT_alpha += dot(v_CDT, f);
    }

    std::cout << " DCL/Dalpha = " << CL/alpha << std::endl;
    std::cout << " CL_alpha = " << CL_alpha << std::endl;
    std::cout << " DCD/Dalpha = " << CD/alpha << std::endl;
    std::cout << " CD_alpha = " << CD_alpha << std::endl;
    std::cout << " DCDT/Dalpha = " << Drag/alpha << std::endl;
    std::cout << " CDT_alpha = " << CDT_alpha << std::endl;

    filePolar << alpha << " " << CL << " " << CL_alpha
                       << " " << CD << " " << CD_alpha
                       << " " << Drag << " " << CDT_alpha << std::endl;

#if 1
    PrimalEqSet.setSolutionField(v_CD);

    // Tecplot dump
    string filenameAdj = "tmp/NACA0012_FP_Adjoint_CD.dat";
    cout << "calling output_Tecplot: filename = " << filenameAdj << endl;
    output_Tecplot_FP( pde, qfld, filenameAdj );

    PrimalEqSet.setSolutionField(v_CDT);

    // Tecplot dump
    filenameAdj = "tmp/NACA0012_FP_Adjoint_CDT.dat";
    cout << "calling output_Tecplot: filename = " << filenameAdj << endl;
    output_Tecplot_FP( pde, qfld, filenameAdj );
#endif

  }

  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;

#if 0
  for (int n = 0; n < nDOFPDE; n++)
    rsdfld.DOF(n) = rsd[n];
  output_Tecplot( rsdfld, "tmp/NACA0_LIPrsd.dat" );
#endif

#if 0
  // Tecplot dump
  string filenameLG = "tmp/NACA0_LG_LIP.dat";
  cout << "calling output_Tecplot: filename = " << filenameLG << endl;
  output_Tecplot_LIP( pde, lgfld, filenameLG );
#endif

  cout << "1st finished:" << endl;
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
