// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_DGAdvective_Line_Boltzmann_Diffusion_toy

#include <fstream>
//#include "Field/Partition/XField_Lagrange.cpp"
#include <stdlib.h>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsBoltzmann2D.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveDistributionFunctions.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/BCBoltzmann2D.h"
//#include "pde/NS/OutputBoltzmann2D.h"
#include "pde/NS/SolutionFunction_Boltzmann2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
//#include "pde/AnalyticFunction/SSMEInletConditions.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"

#include "NonLinearSolver/NewtonSolver.h"
//#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
//#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"


#include "Field/XFieldArea.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/output_Tecplot.h"
//#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/output_Tecplot.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"
//#define FIELDAREA_DG_CELL_INSTANTIATE
//#include "Field/FieldArea_DG_Cell_impl.h"

#include "tools/output_std_vector.h"

using namespace std;
using namespace SANS;

namespace SANS
{
//template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
//template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveDistributionFunctions>      { static std::string str() { return "PrimitiveVariables";} };
//template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

//############################################################################//
BOOST_AUTO_TEST_SUITE(Solve2D_DGAdvective_Triangle_Boltzmann_LDCFlow_test_suite )

//------------------------------------------------------------------------------
BOOST_AUTO_TEST_CASE( Solve2D_DGAdvective_Triangle_Boltzmann_LDCFlow_Diffusion )
{

  //--------------------------------------------------------------------------------------
  // MPI comm world
  //--------------------------------------------------------------------------------------
  mpi::communicator world;
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Typedefs for Euler2D
  //--------------------------------------------------------------------------------------
  typedef QTypePrimitiveDistributionFunctions QType;
  typedef TraitsModelBoltzmann2D<QType, GasModel> TraitsModelBoltzmann2DClass;
  typedef PDEBoltzmann2D<TraitsSizeBoltzmann2D, TraitsModelBoltzmann2DClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  //--------------------------------------------------------------------------------------
  //RK:
  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> RKClass;
  //--------------------------------------------------------------------------------------
  // Typedefs for discretization
  //--------------------------------------------------------------------------------------
  typedef BCBoltzmann2DVector<TraitsSizeBoltzmann2D, TraitsModelBoltzmann2DClass> BCVector;
  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PDEPrimalEquationSetClass;
  // typedef AlgebraicEquationSet<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > AlgebraicEquationSet_Class;

  typedef PDEPrimalEquationSetClass::BCParams BCParams;

  //--------------------------------------------------------------------------------------

  int resolution = -1;
  int order_pde = -1;
  Real Kn = -1.;
  Real dt = -1.;
  Real tFinal = -1.;
  Real tole = -1.;
  int restart_stat = -1;
  int printFreqFactor = -1;
  Real velocity = -1.;

  // Read from file
  ifstream readInput;
  readInput.open("boltzmann.in");
  readInput >> velocity;
  readInput >> resolution;
  readInput >> order_pde;
  readInput >> Kn;
  readInput >> dt;
  readInput >> tFinal;
  readInput >> tole;
  readInput >> restart_stat;
  readInput >> printFreqFactor;

  if ( resolution == -1 || order_pde == -1 || Kn == -1. || dt == -1. || tole == -1. || restart_stat == -1 || tFinal == -1. )
    SANS_DEVELOPER_EXCEPTION("Unable to read all variables from boltzmann.in file. Review input file.");
  else if (world.rank() == 0)
  {
    std::cout << "Lid velocity" << velocity << std::endl;
    std::cout << "resolution: " << resolution << std::endl;
    std::cout << "order_pde : " << order_pde << std::endl;
    std::cout << "Kn        : " << Kn << std::endl;
    std::cout << "dt        : " << dt << std::endl;
    std::cout << "t_final   : " << tFinal << std::endl;
    std::cout << "tol       : " << tole << std::endl;
    std::cout << "restart status: " << restart_stat << std::endl;
    std::cout << "print factor  : " << printFreqFactor << std::endl;
  }
  //--------------------------------------------------------------------------------------
  // Solution order
  //--------------------------------------------------------------------------------------
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Create our Euler PDE
  //--------------------------------------------------------------------------------------
  int nSteps_fin = (tFinal) / dt;
  Real H = 1.0;
  Real L = 1.0;

  Real gamma = 1.4;
  Real R = 1.;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  std::cout<<"Setting Kn = " << Kn <<". and dt = "<<dt<<". Accept?";
  Real csq = 1.;
  // doi:10.1016/j.jcp.2008.06.012
  Real viscosity = sqrt(csq)*H*Kn*sqrt(1./3);
  Real tau = viscosity/csq;
  Real Uadv = 0.;
  Real Vadv = 0.;
  NDPDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                 PDEClass::BGK_exp, tau,
                 PDEClass::Hydrodynamics, Uadv, Vadv );

  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Define boundary conditions
  //--------------------------------------------------------------------------------------
  // BC

  PyDict BCLid;
  BCLid[BCParams::params.BC.BCType] = BCParams::params.BC.DiffuseKinetic;
  BCLid[BCBoltzmann2DParams<BCTypeDiffuseKinetic>::params.Rhow] = 1.0;
  BCLid[BCBoltzmann2DParams<BCTypeDiffuseKinetic>::params.Uw] = velocity;
  BCLid[BCBoltzmann2DParams<BCTypeDiffuseKinetic>::params.Vw] = 0.0;

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.DiffuseKinetic;
  BCWall[BCBoltzmann2DParams<BCTypeDiffuseKinetic>::params.Rhow] = 1.0;
  BCWall[BCBoltzmann2DParams<BCTypeDiffuseKinetic>::params.Uw] = -0.0;
  BCWall[BCBoltzmann2DParams<BCTypeDiffuseKinetic>::params.Vw] = 0.0;



  PyDict PyBCList;
  PyBCList["BCLid"] = BCLid;
  PyBCList["BCWall"] = BCWall;

  BCParams::checkInputs( PyBCList );

  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["BCLid"] = { XField2D_Box_Triangle_Lagrange_X1::iTop };
  BCBoundaryGroups["BCWall"] = { XField2D_Box_Triangle_Lagrange_X1::iBottom, XField2D_Box_Triangle_Lagrange_X1::iLeft,
                                      XField2D_Box_Triangle_Lagrange_X1::iRight};
  //PyDict PyBCList;
  //std::map<std::string, std::vector<int>> BCBoundaryGroups = {};
  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);


  GlobalTime time;
  time = 0.;

  //--------------------------------------------------------------------------------------
  // Set initial conditions
  //--------------------------------------------------------------------------------------
#if 0
  Real max_density = 500.;
  Real x_spread = 10000, y_spread = x_spread;
  typedef SolutionFunction_Boltzmann2D_Gaussian<TraitsSizeBoltzmann2D, TraitsModelBoltzmann2DClass> SolutionClass;
  typedef SolnNDConvertSpace<PhysD2, SolutionClass> SolutionNDClass;
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.A] = max_density;
  solnArgs[SolutionClass::ParamsType::params.sigma_x] = x_spread;
  solnArgs[SolutionClass::ParamsType::params.sigma_y] = y_spread;
  SolutionNDClass solnExact(solnArgs);

#elif 1
  Real fix_density = 1.000;
  typedef SolutionFunction_Boltzmann2D_WeightedDensity<TraitsSizeBoltzmann2D, TraitsModelBoltzmann2DClass> SolutionClass;
  typedef SolnNDConvertSpace<PhysD2, SolutionClass> SolutionNDClass;
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.Rho] = fix_density;
  SolutionNDClass solnExact(solnArgs);
#endif

for (int i=resolution; i<=resolution; i=i*2)
{
  //--------------------------------------------------------------------------------------
  // Create grid
  //--------------------------------------------------------------------------------------
  int ii = i;
  int jj = i;

  //for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );
  //pde_qfld = 100;
  // declare for sanity
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj, 0., L, 0., H, {{ false, false }} );


      ////////////////////////////////////////////////////////////////////////////////////////
      // Allocate our Euler PDE field
      ////////////////////////////////////////////////////////////////////////////////////////
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Lagrange);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> pde_rfld(xfld, order_pde, BasisFunctionCategory_Lagrange);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> pde_lgfld(xfld, order_pde, BasisFunctionCategory_Lagrange,
                                                           active_boundaries);// BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups));
  if (restart_stat == 1)
  {
    SANS_DEVELOPER_EXCEPTION("No restart enabled yet...");
    for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );
/*      string restart = "restart/Restart_Boltzmann_";
      restart += to_string( world.rank());
      restart += ".txt";
      int nDOF;
      ifstream fp;
      fp.open(restart);
      fp >> nDOF;
      BOOST_REQUIRE_EQUAL(nDOF, pde_qfld.nDOF());
      for ( int i = 0; i < pde_qfld.nDOF(); i++)
        for ( int n = 0; n < 16 ; n++)
          fp >> pde_qfld.DOF(i)[n];

      fp.close();*/
  }
  else
  {
    for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );
  }
    pde_lgfld = 0;

  //--------------------------------------------------------------------------------------
  // Output IC gnuplot file
  //--------------------------------------------------------------------------------------
   string filenameStem_IC = "tmp/solnDG_Boltzmann2D_step_0.plt";
   output_Tecplot( pde_qfld, filenameStem_IC );
  //--------------------------------------------------------------------------------------
  //--------------------------------------------------------------------------------------
  // AES Settings
  //--------------------------------------------------------------------------------------
  QuadratureOrder quadratureOrder( xfld, 2*order_pde+2 );
  std::vector<Real> tol = { tole, tole };

  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Create our AES to solve our PDE
  //--------------------------------------------------------------------------------------
  std::vector<int> interiorTraceGroups = linspace(0,xfld.nInteriorTraceGroups()-1);


  PDEPrimalEquationSetClass PrincipalEqSet(xfld, pde_qfld, pde_lgfld, pde, quadratureOrder, ResidualNorm_Default, tol, {0},
                                           interiorTraceGroups, PyBCList, BCBoundaryGroups, time);
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Construct Newton Solver
  //--------------------------------------------------------------------------------------

  // Set up Newton Solver
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict, LinSolverDict;
#ifdef SANS_PETSC
  // Take the Preconditioners from my Tandem Sphere case and just copy paste them here

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  LinSolverDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  LinSolverDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  LinSolverDict[SLA::PETScSolverParam::params.MaxIterations] = 100;
  LinSolverDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  LinSolverDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  LinSolverDict[SLA::PETScSolverParam::params.Verbose] = true;
  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  LinSolverDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  std::cout << "Linear solver: PETSc" << std::endl;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  NewtonSolverParam::checkInputs( NewtonSolverDict );
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#else

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  std::cout << "Linear solver: UMFPACK" << std::endl;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  //AdjSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  // Check inputs
  NewtonSolverParam::checkInputs( NewtonSolverDict );
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#endif
  //--------------------------------------------------------------------------------------

  int RKorder = 1;
  int RKtype = 0;
  int RKstages = RKorder;


  //temporal discretiation
  RKClass RK(RKorder, RKstages, RKtype, dt, time, xfld, pde_qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, PrincipalEqSet);

  //--------------------------------------------------------------------------------------

  //system("touch qfld_new");
  for (int step = 1; step <= nSteps_fin; step++ )//int(nSteps_fin/10); step++)
  {
    std::cout << "############################# Time Step: " << step << "\n time is " << time << std::endl;
    //----------------------------------------
    // check if code has to quit..
    //system("touch isSteady");
    //ifstream in_steady("isSteady");
    if (true) //in_steady.fail())
    {
      //in_steady.close();

    // Advance solution
      RK.march(int(1));
      float simTime = step*dt;
      if (step % printFreqFactor == 0 )
      {
        // Tecplot Output
        //system("mv qfld_new qfld_old");
        //int FieldOrder = order_pde;
        string filename = "tmp/solnDG_Boltzmann2D_Triangle_dt_"+to_string(dt)+"_Kn_";
        filename += to_string(Kn);
        filename += "_Order_";
        int f_order = order_pde;
        filename += to_string(f_order);
        filename += "_";
        filename += to_string(ii);
        filename += "x";
        filename += to_string(jj);
        filename += "_simTime_";
        filename += to_string(simTime);
        filename += ".plt";
        output_Tecplot( pde_qfld, filename );
        //system(("cp "+filename+" qfld_new").c_str());
        //system("diff qfld_new qfld_old > diffStat");
        //system(" if [ -s 'diffStat' ]; then echo Not Same; else echo Same > isSteady; fi;");
      }

#if 0
      string restart = "restart/Restart_Boltzmann_";
      restart += to_string( world.rank());
      restart += ".txt";

       ofstream fp;
       fp.open(restart);
       fp << std::scientific << std::setprecision(16);
       fp << pde_qfld.nDOF();
       fp << std::endl;
       for ( int i = 0; i < pde_qfld.nDOF(); i++)
        {
         for ( int n = 0; n < 16; n++)
            fp << pde_qfld.DOF(i)[n] << " ";
         fp << std::endl;
        }
#endif
    }
    else
    {
      //in_steady.close();
      std::cout<< " Breaking from loop at step " << step << std::endl;
      break;
    }
  }

}


}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
