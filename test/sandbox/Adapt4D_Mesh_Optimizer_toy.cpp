// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_L2_BoundaryLayer_btest
// Testing of the MOESS framework on the 2D boundary layer test-case found in Masa's thesis

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include <iostream>

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"

#include "Adaptation/MeshAdapter.h"
#include "Adaptation/callMesher.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "Meshing/AnalyticMetrics/MetricField.h"

#include "Field/XField.h"

using namespace std;

#include "Field/FieldSpacetime_CG_Cell.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"

#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

#define UNIFORM 0
#define LINEAR 1
#define BLAST 0

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt4D_Metric_Error_test_suite )

class Metric4d
{
public:
  Metric4d() {}

  DLA::MatrixSymS<4, Real> operator()( const DLA::VectorS<4, Real>& x ) const
  {
    #if UNIFORM
    DLA::MatrixSymS<4, Real> M(0);

    Real ii= 6;
    Real hx= 1./ii;
    Real hy= 1./ii;
    Real hz= 1./ii;
    Real ht= 1./ii;

    M(0,0) = 1./(hx*hx);
    M(0,1) = 0.;
    M(0,2) = 0.;
    M(0,3) = 0.;
    M(1,1) = 1./(hy*hy);
    M(1,2) = 0.;
    M(1,3) = 0.;
    M(2,2) = 1./(hz*hz);
    M(2,3) = 0.;
    M(3,3) = 1./(ht*ht);

    return M;

    #elif LINEAR
    DLA::MatrixSymS<4, Real> M(0);

    Real hx = 0.125;
    Real hy = 0.125;
    Real hz = 0.125;
    Real h0 = 1.25e-3;
    Real ht = h0 +2.*(hx -h0)*fabs( x[3] -0.5 );

    M(0,0) = 1./(hx*hx);
    M(0,1) = 0.;
    M(0,2) = 0.;
    M(0,3) = 0.;
    M(1,1) = 1./(hy*hy);
    M(1,2) = 0.;
    M(1,3) = 0.;
    M(2,2) = 1./(hz*hz);
    M(2,3) = 0.;
    M(3,3) = 1./(ht*ht);

    return M;

    #elif BLAST

    Real X = x[0];
    Real Y = x[1];
    Real Z = x[2];
    Real T = x[3];

    // initial and final blast radii
    Real r0 = 0.4;
    Real rf = 0.8;

    // hypercone angle
    Real alpha = atan2(1.0,(rf-r0));

    // spherical coordinates
    Real RHO = sqrt(X*X+Y*Y+Z*Z) +0.0001;
    Real THETA = acos(Z/RHO);
    Real PHI = atan2(Y,X);

    // eigenvectors are normal and tangents to the hypercone
    DLA::MatrixS<4,4,Real> Q(0);
    Q(0,0) =  sin(alpha)*cos(PHI)*sin(THETA);
    Q(0,1) =  cos(PHI)*cos(THETA);
    Q(0,2) = -sin(PHI);
    Q(0,3) =  cos(alpha)*cos(PHI)*sin(THETA);

    Q(1,0) =  sin(alpha)*sin(PHI)*sin(THETA);
    Q(1,1) =  cos(THETA)*sin(PHI);
    Q(1,2) =  cos(PHI);
    Q(1,3) =  cos(alpha)*sin(PHI)*sin(THETA);

    Q(2,0) =  sin(alpha)*cos(THETA);
    Q(2,1) = -sin(THETA);
    Q(2,2) = 0.0;
    Q(2,3) =  cos(alpha)*cos(THETA);

    Q(3,0) = -cos(alpha);
    Q(3,1) = 0.0;
    Q(3,2) = 0.0;
    Q(3,3) =  sin(alpha);

    Real rho0 = r0 +(rf-r0)*T; // blast speed is derivative wrt T
    Real h0 = 0.0025;
    Real hu = 0.125;
    Real hmin = 0.05;
    Real delta = 0.1;
    Real ht = 0.5;

    DLA::VectorS<4,Real> L(0);
    Real hrho = h0 +2*(hu-h0)*abs(RHO-rho0);
    Real htheta = hu;
    Real hphi = hu;

    if (fabs(RHO-rho0)>delta)
      htheta = hu;
    else htheta = (hu -hmin)*fabs(RHO-rho0)/delta +hmin;
    hphi = htheta;

    L(0) = 1./(hrho*hrho);
    L(1) = 1./(htheta*htheta);
    L(2) = 1./(hphi*hphi);
    L(3) = 1./(ht*ht);

    DLA::MatrixSymS<4,Real> M = Q*diag(L)*Transpose(Q);

    return M;
    #endif
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt4D_TargetMetric )
{
#ifdef SANS_AVRO
  mpi::communicator world;

  int maxIter = 20;

  using avro::coord_t;
  using avro::index_t;

  std::vector<int> cellGroups = {0};

  avro::Context context;

  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld0( world , {5,5,5,5} );

  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>( xfld0.context() , "tesseract" );
  model->addBody( xfld0.body_ptr() , false );

  // copy the mesh into the domain and attach the geometry
  std::shared_ptr< XField<PhysD4,TopoD4> > pxfld = std::make_shared<XField_avro<PhysD4,TopoD4>>(xfld0,model);

  std::string filename_base = "tmp/tesseract";
  #if LINEAR
  filename_base += "/linear/";
  #elif BLAST
  filename_base += "/wave/";
  #endif
  boost::filesystem::create_directories(filename_base);

  std::string boundary_subdirectory = "boundary";
  boost::filesystem::create_directories(filename_base+"/"+boundary_subdirectory);

  boost::filesystem::path base_dir(filename_base);
  if ( not boost::filesystem::exists(base_dir) )
    boost::filesystem::create_directory(base_dir);

  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD4, TopoD4>::params.Mesher.Name] = MeshAdapterParams<PhysD4, TopoD4>::params.Mesher.avro;
  MesherDict[avroParams::params.Curved] = false;
  MesherDict[avroParams::params.FilenameBase] = filename_base;
  MesherDict[avroParams::params.BoundarySubdirectory] = boundary_subdirectory;
  //MesherDict[avroParams::params.InsertionVolumeFactor] = -1;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.TargetCost] = 0; // not used
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD4, TopoD4>::params.FilenameBase] = filename_base;


  //--------ADAPTATION LOOP--------
  MeshAdapterParams<PhysD4, TopoD4>::checkInputs(AdaptDict);

//  MeshAdapter<PhysD4, TopoD4> mesh_adapter(AdaptDict, fadapthist);

  typedef DLA::MatrixSymS<PhysD4::D,Real> MatrixSym;


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    if (world.rank()==0)
      std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    // compute the target metric on the current mesh
    Metric4d metric;
    MetricField<PhysD4,TopoD4> targetMetric( *pxfld , cellGroups , metric );

    // create the dummy solver interface and the nodal view of the xfld
    SolverInterfaceDummy<PhysD4,TopoD4> problem;
    Field_NodalView xfld_nodalview(*pxfld, cellGroups);

    // create the metric request
    Field_CG_Cell<PhysD4,TopoD4,MatrixSym> metric_req(*pxfld, 1, BasisFunctionCategory_Lagrange, cellGroups);

    // limit the metric by limiting the step matrices from the implied one
    TargetMetric<PhysD4,TopoD4> target( *pxfld , cellGroups , targetMetric , MOESSDict );
    target.getNodalMetricRequestField(metric_req);

    target.dumpMetricConformity<Pentatope>( filename_base , "analytic"+to_string(iter) );

    // adapt mesh to metric
    MesherInterface<PhysD4, TopoD4, avroMesher> mesher(iter, MesherDict);
    std::shared_ptr<XField<PhysD4, TopoD4>> pxfldNew;
    pxfldNew = mesher.adapt(metric_req,*pxfld);

    // update mesh
    pxfld = pxfldNew;
  }

  fadapthist.close();

#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
