// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
//  Solve2D_HDG_Triangle_NavierStokes_Poiseuille_btest
// testing of 2-D HDG for NS on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/Stokes/TraitsStokes.h"
#include "pde/Stokes/PDEStokes2D.h"
#include "pde/Stokes/BCStokes2D.h"
#include "pde/Stokes/SolutionFunction_Stokes2D.h"
#include "pde/Stokes/OutputStokes2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/BCParameters.h"

#include "pde/OutputCell_SolutionErrorSquared.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/ProjectSoln_HDG.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"
#include "Discretization/HDG/JacobianFunctionalCell_HDG.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectSolnTrace_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Trace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_InteriorFieldTraceGroup.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_HDG_Triangle_NavierStokes_OjedaMMS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_HDG_Triangle_OjedaMMS )
{

  typedef PDEStokes2D<TraitsSizeStokes> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;
  typedef SolutionFunction_Stokes2D_OjedaMMS<TraitsSizeStokes> SolnType;
  typedef SolnNDConvertSpace<PhysD2,SolnType> NDSolnType;
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;

  typedef OutputStokes2D_TotalPressure<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_HDG_Output<NDOutputClass> IntegrandOutputClass;

  typedef OutputCell_SolutionErrorSquared< PDEClass, SolnType > SolutionErrorClass;
  typedef OutputNDConvertSpace<PhysD2, SolutionErrorClass> NDSolutionErrorClass;
  typedef IntegrandCell_HDG_Output<NDSolutionErrorClass> IntegrandSolutionErrorClass;

  typedef BCStokes2DVector<TraitsSizeStokes> BCVector;

  typedef AlgebraicEquationSet_HDG< NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  std::vector<Real> tol = {1e-10, 1e-10, 1e-10};

  // INFLOW CONDITIONS TO MATCH OJEDA'S MMS
  const Real nu = 1;

  const Real u0 = 0.0;
  const Real v0 = 0.0;
  const Real p0 = 0.0;

  PyDict dummy;
  NDSolnType soln(dummy);
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(soln) );

  NDPDEClass pde(nu, forcingptr );

  // HDG solution field at inlet
  ArrayQ q0;
  q0 = {p0, u0, v0};

  // L2 error
  NDSolutionErrorClass slnError(soln);
  IntegrandSolutionErrorClass fcnOut( slnError, {0} );

  // Enthalpy Output
  NDOutputClass out(pde);
  IntegrandOutputClass fcnOut2( out, {0} );

#if 1
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient);
#else
  Real refl = 1;
  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient, refl);
#endif



  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;

  BCIn[BCStokes2DFullStateParams::params.pSpec] = p0;
  BCIn[BCStokes2DFullStateParams::params.uSpec] = u0;
  BCIn[BCStokes2DFullStateParams::params.vSpec] = v0;
  BCIn[BCStokes2DFullStateParams::params.Characteristic] = false;
  BCIn[BCStokes2DFullStateParams::params.Viscous] = true;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;

  BCBoundaryGroups["BCIn"]  = {0,1,2,3};

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  // Set up Newton Solver
  PyDict NewtonSolverDict, LinSolverDict, NewtonLineUpdateDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
#else
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
#endif

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] =  true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  string filename_base = "tmp/StokesOjeda/EDGhQuad/EDGhQuad";

  string filename2 = filename_base + "_Errors.txt";
  fstream foutsol;


  int ii, jj;
//
  int ordermin = 1;
  int ordermax = 4;

  for (int order = ordermin; order <= ordermax; order++)
  {

    int powermin = 3;
    int powermax = 7;

    if (order==4)
      powermax = 6;

  // loop over grid resolution: 2^power
    for (int power = powermin; power <= powermax; power++)
    {
      jj = pow( 2, power ) + 1;
      ii = jj; //small grid!

      string filename = filename_base + "_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);

      foutsol.open( filename2, fstream::app );

      // grid
//      XField2D_Box_Triangle_X1 xfld(ii, jj);
      XField2D_Box_Quad_X1 xfld(ii,jj);

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);

//#define HDGSOLVE
#ifdef HDGSOLVE //HDG
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
#else //EDG??
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Lagrange);
#endif
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

      // Set the uniform initial condition
      qfld = q0;
      afld = 0.0;
      qIfld = q0;
      lgfld = 0;

      //////////////////////////
      //SOLVE HIGHER P SYSTEM
      //////////////////////////

//      QuadratureOrder quadratureOrder( xfld, 3*order+1 );
      QuadratureOrder quadratureOrder( xfld, 3*order+1 );

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                         {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      bool solved = Solver.solve(ini,sln).converged;
      BOOST_REQUIRE(solved);
      PrimalEqSet.setSolutionField(sln);

      // check that the residual is zero

      rsd = 0;
      PrimalEqSet.residual(sln, rsd);
      std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

//      bool converged = PrimalEqSet.convergedResidual(rsdNorm);

//      if (!converged)
//      {
//        std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);
//        PrimalEqSet.printDecreaseResidualFailure(rsdNorm, std::cout);
//      }
//      BOOST_CHECK(converged);

      // Monitor Velocity Error
      Real Hout = 0.0;
      ArrayQ L2err = 0.0;
//
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( fcnOut, L2err ), xfld, (qfld, afld),
          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( fcnOut2, Hout ), xfld, (qfld, afld),
          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real Jtrue = 0.002012582795004;
      Real Herr = fabs(Hout - Jtrue);

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii
          << ": Integrated Enthalpy = " << std::setprecision(12) <<  Hout << "\n";

      foutsol << order << " " << ii << std::setprecision(15);
      foutsol << " " << sqrt(L2err[0]);
      foutsol << " " << sqrt(L2err[1]);
      foutsol << " " << sqrt(L2err[2]);
      foutsol << " " << Herr << "\n";
#endif

      foutsol.close();

      string filenameprimal = filename + ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filenameprimal );

#if 0
      // adjoint (right hand side)
      typedef SurrealS<3> SurrealClass;
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, order, BasisFunctionCategory_Legendre);

#ifdef HDGSOLVE //HDG
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, order, BasisFunctionCategory_Legendre);
#else //EDG??
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, order, BasisFunctionCategory_Hierarchical);
#endif
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      mufld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

      SystemVectorClass rhs(PrimalEqSet.vectorEqSize());
      rhs = 0;

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_HDG<SurrealClass>( fcnOut2, rhs(PrimalEqSet.iPDE) ),
          xfld, (qfld, afld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // solve
      SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);
      SystemVectorClass adj(PrimalEqSet.vectorStateSize());
      solverAdj.solve(rhs, adj);

      bfld = 0;
      PrimalEqSet.setAdjointField(adj, wfld, bfld, wIfld, mufld);

      string filenameadjoint = filename + "_adj.plt";
      output_Tecplot( wfld, filenameadjoint );
#endif

    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
