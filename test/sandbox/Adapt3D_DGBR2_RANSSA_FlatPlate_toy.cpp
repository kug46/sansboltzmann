// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_DGBR2_RANS_FlatPlate_toy

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA3D.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCRANSSA3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/OutputEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field/FieldVolume_CG_Cell.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Meshing/EPIC/XField_PX.h"

#include "unit/UnitGrids/XField3D_FlatPlate_X1.h"
#include "unit/UnitGrids/XField3D_CornerFlatPlate_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_DGBR2_RANS_FlatPlate_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_DGBR2_RANS_FlatPlate_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCRANSSA3DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

#ifndef BOUNDARYOUTPUT
  typedef ScalarFunction3D_Const WeightFcn; // Change this to change output weighting type
  typedef OutputCell_WeightedSolution<PDEClass, WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler3D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputForce;
  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Moment<PDEClass>> NDOutputMoment;
  typedef NDVectorCategory<boost::mpl::vector2<NDOutputForce, NDOutputMoment>, NDOutputForce::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_Distance ParamBuilderType;

  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef SolutionData_DGBR2<PhysDim, TopoDim, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.189;
  const Real Reynolds = 2.4e6;
  const Real Prandtl = 0.72;
        Real lRef = 1;                         // length scale in mm
        Real MAC = 1;                          // mean aerodynamic chord for moments
        Real SRef = 1;                      // area scale

  const Real scale = lRef;

  Real CG[3] = {0/scale, 0/scale, 0/scale};

  lRef /= scale;
  MAC /= scale;
  SRef /= (scale*scale);

  const Real rhoRef = 1;                            // density scale
  const Real tRef = 1;                              // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef   = cRef*Mach;                    // velocity scale
  const Real aoaRef = 0.0*PI/180.;                  // angle of attack (radians)
  const Real uRef   = qRef * cos(aoaRef);           // velocity
  const Real vRef   = 0;
  const Real wRef   = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  const Real nuRef = 1;
//  const Real nuRef = muRef / rhoRef;
  const Real ntr = ntRef / nuRef;

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Entropy );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, SAnt3D<DensityVelocityPressure3D<Real>>({rhoRef, uRef, vRef, wRef, pRef, ntr}) );

  // DGBR2 discretization
  Real viscousEtaParameter = 2*Tet::NFace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  enum ResidualNormType ResNormType = ResidualNorm_L2_DOFWeighted;
  std::vector<Real> tol = {1e-12, 1e-12};

  const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  const Real TtRef = tRef*Gamma;
  const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  typedef BCRANSSA3D<BCTypeDirichlet_mitState, BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> >::ParamsType PtTtaParams;

  PyDict BCInFlow;
  BCInFlow[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInFlow[PtTtaParams::params.TtSpec] = TtRef;
  BCInFlow[PtTtaParams::params.PtSpec] = PtRef;
  BCInFlow[PtTtaParams::params.aSpec]  = 0;
  BCInFlow[PtTtaParams::params.bSpec]  = 0;
  BCInFlow[PtTtaParams::params.nt]     = ntr;

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflow[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;


  PyDict PyBCList;
  PyBCList["BCInflow"]   = BCInFlow;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCOutflow"]  = BCOutflow;
  PyBCList["BCWall"]     = BCWall;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCInflow"]   = {XField3D_FlatPlate_X1::iLeft};
  BCBoundaryGroups["BCOutflow"]  = {XField3D_FlatPlate_X1::iRight};
  BCBoundaryGroups["BCWall"]     = {XField3D_FlatPlate_X1::iPlate};
  BCBoundaryGroups["BCSymmetry"] = {XField3D_FlatPlate_X1::iSlipIn,
                                    XField3D_FlatPlate_X1::iSlipOut,
                                    XField3D_FlatPlate_X1::iTop,
                                    XField3D_FlatPlate_X1::iBack,
                                    XField3D_FlatPlate_X1::iFront};

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> lg_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);


  // Residual weighted boundary output
  Real dynpRef = 0.5*rhoRef*(uRef*uRef + vRef*vRef + wRef*wRef);

  NDOutputForce outputFcnDrag(pde, cos(aoaRef)/(SRef*dynpRef), 0., sin(aoaRef)/(SRef*dynpRef));
  OutputIntegrandClass outputIntegrandDrag( outputFcnDrag, BCBoundaryGroups.at("BCWall") );

  NDOutputForce outputFcnLift(pde, -sin(aoaRef)/(SRef*dynpRef), 0., cos(aoaRef)/(SRef*dynpRef));
  OutputIntegrandClass outputIntegrandLift( outputFcnLift, BCBoundaryGroups.at("BCWall") );

  NDOutputMoment outputFcnMoment(pde, CG[0], CG[1], CG[2], 0, 1, 0, MAC);
  OutputIntegrandClass outputIntegrandMoment( outputFcnMoment, BCBoundaryGroups.at("BCWall") );

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

//  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;
  
  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-8;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-14;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
//  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.DGMRES;
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PreconditionerAdjoint[SLA::PreconditionerILUParam::params.FillLevel] = 6;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-14;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-12;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 6000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 400;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;
//  PETScDictAdjoint[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif 0 //defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-6;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;


  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Optimizer_MaxEval] = 6000;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  //      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Detailed;
  MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Element;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;
  MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;
  //MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
  MesherDict[EpicParams::params.nThread] = 2;
  MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;


  int order = 1;
  int targetCost = 1000;

  int maxIter = 30;

  int restartIter = -1;
  std::string restartFileBase = "";

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    order = std::stoi(argv[1]);
  if (argc >= 3)
    targetCost = std::stoi(argv[2]);
  if (argc >= 4)
    maxIter = std::stoi(argv[3]);
  if (argc >= 5)
    restartIter = std::stoi(argv[4]);
  if (argc >= 6)
    restartFileBase = argv[5];

  if (world.rank() == 0)
  {
    std::cout << ", order = " << order;
    std::cout << ", targetCost = " << targetCost << ", maxIter: " << maxIter << std::endl;
  }
#endif

  //--------ADAPTATION LOOP--------
  // to make sure folders have a consistent number of zero digits
  const int string_pad = 5;
  std::string int_pad = std::string(string_pad - std::to_string((int)targetCost/1000).length(), '0') + std::to_string((int)targetCost/1000);

  std::string filename_base;
  filename_base = "tmp/flatplateDGBR2/EPIC/SA_P" + std::to_string(order) + "_X" + int_pad + "k/";

  boost::filesystem::create_directories(filename_base);

  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld;
  std::shared_ptr<Field_DG_Cell<PhysD3,TopoD3,ArrayQ>> prestartfld;
  int startIter = 0;
  bool isRestart = false;

  if (restartIter >= 0)
  {
    startIter = restartIter;
    std::string file_initial_xfld = restartFileBase + "restart.grm";
    std::string file_initial_qfld = restartFileBase + "restart.qrm";

    if (world.rank() == 0)
      std::cout<< "Starting from " << file_initial_xfld << " and " << file_initial_qfld << std::endl;

    pxfld = std::make_shared<XField_PX<PhysD3, TopoD3>>(world, file_initial_xfld);
    prestartfld = std::make_shared<Field_DG_Cell<PhysD3,TopoD3,ArrayQ>>(*pxfld, order, BasisFunctionCategory_Legendre);

    ReadSolution_PX(file_initial_qfld, *prestartfld);

    isRestart = true;
  }
  else //start from scratch
  {

    if (world.rank() == 0)
      std::cout<< "Starting from scratch" << std::endl;

    pxfld = std::make_shared<XField3D_FlatPlate_X1>(world, -1 );
  }

  // scale the grid
  for (int n = 0; n < pxfld->nDOF(); n++)
    pxfld->DOF(n) /= scale;

  std::string adapthist_filename = filename_base + "test.adapthist";

  fstream fadapthist, foutputhist;
  if (world.rank() == 0)
  {
    std::string adapthist_filename = filename_base + "test.adapthist";

    if (isRestart)
      fadapthist.open( adapthist_filename, fstream::app );
    else
      fadapthist.open( adapthist_filename, fstream::out );

    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

    std::string outputhist_filename = filename_base + "output.dat";

    if (isRestart)
      foutputhist.open( outputhist_filename, fstream::app );
    else
      foutputhist.open( outputhist_filename, fstream::out );

    BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + outputhist_filename);
  }


  // Easier PTC for subsequent solves
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor] = 1.5;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;
  NonlinearSolverDict[PseudoTimeParam::params.CFLPartialStepDecreaseFactor] = 0.9;
  NonlinearSolverDict[PseudoTimeParam::params.SteveLogic] = true;
  NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_residual.dat";
  NonlinearSolverDict[PseudoTimeParam::params.SteveLogic] = true;
  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.dumpStepMatrix] = false;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.dumpRateMatrix] = false;

  PyDict ParamDict;
  MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

  MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Compute distance field
  std::shared_ptr<Field_CG_Cell<PhysD3, TopoD3, Real>>
  pdistfld = std::make_shared<Field_CG_Cell<PhysD3, TopoD3, Real>>(*pxfld, 2, BasisFunctionCategory_Lagrange);
  DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCWall"));

  //Solution data
  //  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>((*pdistfld, *pxfld), pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               lg_boundaries, ParamDict, disc);


  const int quadOrder = 2*(order + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrandDrag);

  //Set initial solution
  if (isRestart)
    prestartfld->projectTo(pGlobalSol->primal.qfld);
  else
    pGlobalSol->setSolution(q0);

  if (!isRestart)
  {
    std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );
  }

  timer primalTimer0;
  pInterface->solveGlobalPrimalProblem();
  Real primalTime0 = primalTimer0.elapsed();

  std::string xfld_restart_filename = filename_base + "restart.grm";
  std::string qfld_restart_filename = filename_base + "restart.qrm";
  if (!isRestart)
  {
    std::string qfld_filename = filename_base + "qfld_a0.plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string delta_adjfld_filename = filename_base + "adjfld_a0.plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, delta_adjfld_filename );

    pInterface->writeRestart(xfld_restart_filename, qfld_restart_filename);

  }

  timer adjointTimer0;
  pInterface->solveGlobalAdjointProblem();
  Real adjointTime0 = adjointTimer0.elapsed();

  timer errEstTimer0;
  pInterface->computeErrorEstimates();
  Real errEstTime0 = errEstTimer0.elapsed();

  Real meshTime0 = 0;

  if (!isRestart)
  {
    std::string efld_filename = filename_base + "efld_a0.plt";
    pInterface->output_EField(efld_filename);
  }

  //----------//
  QuadratureOrder quadrule(*pxfld, quadOrder);

  Real outputLift = 0;

  Real outputMoment = 0;

#ifdef SANS_MPI
  int nDOFtotal = 0;
  boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );
#else
  int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
#endif

  Real globalEstimate=0,globalIndicator=0,globalOutput=0;
  globalEstimate  = pInterface->getGlobalErrorEstimate();
  globalIndicator = pInterface->getGlobalErrorIndicator();
  globalOutput    = pInterface->getOutput();

  if (world.rank() == 0 && startIter == 0)
  {
    // write the header to the output file
    foutputhist << "VARIABLES="
        << std::setw(5)  << "\"Iter\""
        << std::setw(10) << "\"DOF\""
        << std::setw(20) << "\"C<sub>L</sub>\""
        << std::setw(20) << "\"C<sub>D</sub>\""
        << std::setw(20) << "\"C<sub>M</sub>\""
        << std::setw(20) << "\"Estimate\""
        << std::setw(20) << "\"Indicator\""
        << std::setw(20) << "\"MeshTime\""
        << std::setw(20) << "\"PrimalTime\""
        << std::setw(20) << "\"AdjointTime\""
        << std::setw(20) << "\"ErrEstTime\""
        << std::endl;

#ifdef BOUNDARYOUTPUT
    foutputhist << "ZONE T=\"MOESS C<sub>D</sub> " << targetCost << "k\"" << std::endl;
#else
    foutputhist << "ZONE T=\"MOESS M " << targetCost << "k\"" << std::endl;
#endif

    foutputhist << std::setw(5) << 0
        << std::setw(10) << nDOFtotal
        << std::setw(20) << std::setprecision(10) << std::scientific << outputLift
        << std::setw(20) << std::setprecision(10) << std::scientific << globalOutput
        << std::setw(20) << std::setprecision(10) << std::scientific << outputMoment
        << std::setw(20) << std::setprecision(10) << std::scientific << globalEstimate
        << std::setw(20) << std::setprecision(10) << std::scientific << globalIndicator
        << std::setw(20) << std::setprecision(10) << std::scientific << meshTime0
        << std::setw(20) << std::setprecision(10) << std::scientific << primalTime0
        << std::setw(20) << std::setprecision(10) << std::scientific << adjointTime0
        << std::setw(20) << std::setprecision(10) << std::scientific << errEstTime0
        << std::endl;
  }

  for (int iter = startIter; iter < maxIter+1; iter++)
  {
    if (world.rank() == 0 )
      std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    // unscale the grid
    for (int n = 0; n < pxfld->nDOF(); n++)
      pxfld->DOF(n) *= scale;

    timer meshTimer;
    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);
    Real meshTime = meshTimer.elapsed();

    // scale the grid
    for (int n = 0; n < pxfldNew->nDOF(); n++)
      pxfldNew->DOF(n) /= scale;

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Compute distance field
    pdistfld = std::make_shared<Field_CG_Cell<PhysD3, TopoD3, Real>>(*pxfldNew, 2, BasisFunctionCategory_Lagrange);
    DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCWall"));

    //Solution data
    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>((*pdistfld, *pxfldNew), pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    lg_boundaries, ParamDict, disc);


#if 0
    pGlobalSolNew->setSolution(q0);
#else
    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol, true);
#endif

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrandDrag);


    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    //        std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".dat";
    //        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    timer primalTimer;
    pInterface->solveGlobalPrimalProblem();
    Real primalTime = primalTimer.elapsed();

    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    pInterface->writeRestart(xfld_restart_filename, qfld_restart_filename);

    timer adjointTimer;
    pInterface->solveGlobalAdjointProblem();
    Real adjointTime = adjointTimer.elapsed();

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

    //Compute error estimates
    timer estimateTimer;
    pInterface->computeErrorEstimates();
    Real estimateTime = estimateTimer.elapsed();

    std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".dat";
    pInterface->output_EField(efld_filename);


    Real outputLift = 0;
    Real outputMoment = 0;

#ifdef SANS_MPI
    int nDOFtotal = 0;
    boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );
#else
    int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
#endif

    Real globalEstimate=0,globalIndicator=0,globalOutput=0;
    globalEstimate  = pInterface->getGlobalErrorEstimate();
    globalIndicator = pInterface->getGlobalErrorIndicator();
    globalOutput    = pInterface->getOutput();

    if (world.rank() == 0)
    {
      foutputhist << std::setw(5) << iter+1
          << std::setw(10) << nDOFtotal
          << std::setw(20) << std::setprecision(10) << std::scientific << outputLift
          << std::setw(20) << std::setprecision(10) << std::scientific << globalOutput
          << std::setw(20) << std::setprecision(10) << std::scientific << outputMoment
          << std::setw(20) << std::setprecision(10) << std::scientific << globalEstimate
          << std::setw(20) << std::setprecision(10) << std::scientific << globalIndicator
          << std::setw(20) << std::setprecision(10) << std::scientific << meshTime
          << std::setw(20) << std::setprecision(10) << std::scientific << primalTime
          << std::setw(20) << std::setprecision(10) << std::scientific << adjointTime
          << std::setw(20) << std::setprecision(10) << std::scientific << estimateTime
          << std::endl;
    }
  }


  if (world.rank() == 0)
  {
    fadapthist.close();
    foutputhist.close();
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
