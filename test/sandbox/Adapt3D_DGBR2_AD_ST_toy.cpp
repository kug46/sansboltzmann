// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_DGBR2_AD_ST_toy
// testing of 2D+T adaptation on an advection diffusion problem
// learning to SANS case for Cory... don't @ me

#include <boost/test/unit_test.hpp>
#include <boost/filesystem/path.hpp>          // to make filesystems
#include <boost/filesystem/operations.hpp>    // to make filesystems
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"               // saw a comment that this gives us Reals

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

// these, I think, give us tools to convert the 2D physical problem to a
// 3D (2D+T) adaptation problem
#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime2D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime2D.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "pde/ForcingFunction2D_MMS.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

// not sure if this is sufficient include...
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#endif

using namespace std;

// explicitly instantiate the classes for correct coverage info
namespace SANS
{

}

using namespace SANS;

#ifdef SANS_AVRO
static inline int factorial(int n)
{
  int m;
  if (n <= 1)
    m= 1;
  else
    m= n*factorial(n - 1);

  return m;
}
#endif

BOOST_AUTO_TEST_SUITE(Adapt3D_DGBR2_AD_ST_toy_test_suite)

BOOST_AUTO_TEST_CASE(Adapt3D_DGBR2_AD_ST_toy_test_suite)
{
#ifdef SANS_AVRO
  // parse inputs

  int argc= boost::unit_test::framework::master_test_suite().argc;
  char ** argv= boost::unit_test::framework::master_test_suite().argv;

  int pSamples= -1;               // p to be used
  int dofReqSamples= -1;          // dof to be requested

  const int lastn= 5;             // adaptation iterations to print

  int maxIter= -1;                // adaptation iterations to do

  // specify dof and p and maxIter
  if (argc == 4)
  {
    dofReqSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    maxIter= std::stoi(argv[3]);
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // just specify dof and p
  else if (argc == 3)
  {
    dofReqSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    maxIter= 10;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // just specify dof
  else if (argc == 2)
  {
    dofReqSamples= std::stoi(argv[1]);
    pSamples= 1;
    maxIter= 10;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  // specify nothing
  else
  {
    dofReqSamples= 2000;
    pSamples= 1;
    maxIter= 10;
    std::cout << std::endl << "No input parsed! Defaults:" << std::endl;
  }

  SANS_ASSERT(dofReqSamples > 0);
  SANS_ASSERT(pSamples > 0);
  SANS_ASSERT(maxIter >= 0);
  SANS_ASSERT(maxIter >= lastn);

  Real ax= 0.5;
  Real ay= 0.5;
  Real nu= 1.0;

  Real lambda_exact= 5.0;         // for decay exact solution

  std::cout << "\tdofReqSamples: " << dofReqSamples << std::endl;
  std::cout << "\tpSamples: " << pSamples << std::endl;
  std::cout << "\tmaxIter: " << maxIter << std::endl << std::endl;

  std::cout << "Physical problem:" << std::endl;
  std::cout << "\ta: (" << ax << ", " << ay << ")" << std::endl;
  std::cout << "\tnu: " << nu << std::endl << std::endl;

  std::cout << "Running decaying sines MMS problem!" << std::endl;
  std::cout << "\tlambda: " << lambda_exact << std::endl << std::endl;

  std::string filename_base= "tmp/AD3D_ST_DG_adapconv/";
  std::string filename_case;

  typedef AdvectiveFlux2D_Uniform AdvectionModel;
  typedef ViscousFlux2D_Uniform DiffusionModel;
  typedef Source2D_UniformGrad SourceModel;

  // shortcut for the appropriate PDE object for our 2D physics specified
  typedef PDEAdvectionDiffusion<PhysD2, AdvectionModel, DiffusionModel, SourceModel> PDEClass;
  // shortcut for the 2D+T conversion
  typedef PDENDConvertSpaceTime<PhysD2, PDEClass> NDPDEClass;

  // the exact solution... decaying-in-time spatial sine-cosine product
  typedef ScalarFunction2D_SineCosineDecay SolutionExact;
  const Real trueOutput= 0.024998865001755937879;
  // shortcut to the conversion for the exact solution to spacetime
  typedef SolnNDConvertSpaceTime<PhysD2, SolutionExact> NDSolutionExact;

  typedef BCAdvectionDiffusion2DVector<AdvectionModel, DiffusionModel> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpaceTime<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  // the solution to the problem
  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass,
      ParamBuilderType> SolutionClass;
  // parameters to the solver
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  // algebraic equation set
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime,
      BCVector, AlgEqSetTraits_Sparse,
      DGBR2, ParamFieldType> PrimalEquationSetClass;
  // solver interface, everything it needs to run the solution
  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass,
      OutputIntegrandClass> SolverInterfaceClass;

  // mpi
  mpi::communicator world;

  // CREATE ADVECTION DIFFUSION PDE

  AdvectionModel advection(ax, ay);
  DiffusionModel diffusion(nu, 0, 0, nu);
  SourceModel source(0.0, 0.0, 0.0);

  //  // create the PDE
  //  NDPDEClass pde(advection, diffusion, source);

  // create exact solution
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function.Name]=
      BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function.SineCosineDecay;
  solnArgs[NDSolutionExact::ParamsType::params.lambda]= lambda_exact;

  NDSolutionExact solnExact(solnArgs);

  // create forcing function for MMS
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  // overwrite pde with forcing function
  NDPDEClass pde(advection, diffusion, source, forcingptr);

  // CREATE BOUNDARY CONDITIONS

  PyDict BCSoln_timeIC;
  BCSoln_timeIC[BCParams::params.BC.BCType]= BCParams::params.BC.TimeIC_Function;
  BCSoln_timeIC[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;

  PyDict BCSoln_timeOut;
  BCSoln_timeOut[BCParams::params.BC.BCType]= BCParams::params.BC.TimeOut;

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType]= BCParams::params.BC.Function_mitState;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.SolutionBCType]=
      BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Upwind]= false;

  //  PyDict BCSoln_Neumann;
  //  BCSoln_Neumann[BCParams::params.BC.BCType]= BCParams::params.BC.Function_mitState;
  //  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;
  //  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.SolutionBCType]=
  //      BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.SolutionBCType.Neumann;
  //  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Upwind]= false;

  //  PyDict BCNone;
  //  BCNone[BCParams::params.BC.BCType]= BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCSoln_timeIC"]= BCSoln_timeIC;
  PyBCList["BCSoln_timeOut"]= BCSoln_timeOut;
  PyBCList["BCSoln_Dirichlet"]= BCSoln_Dirichlet;
  //  PyBCList["BCSoln_Neumann"]= BCSoln_Neumann;
  //  PyBCList["BCNone"]= BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // CREATE DISCRETIZATION PARAMETERS

  Real viscousEtaParameter= 2*Tet::NTrace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType= ResidualNorm_L2;
  std::vector<Real> tol= {1.0e-10, 1.0e-10};

  // CREATE OUTPUT FUNCTIONAL TO ADAPT WITH

  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // CREATE INTEGRAND FOR EXACT SOLN ERROR

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // SETUP NONLINEAR SOLVER (PROBABLY UNNEEDED FOR LINEAR PROBLEM)

  // nonlinear solver dicts
  PyDict SolverContinuationDict;
  PyDict NonlinearSolverDict;
  PyDict NewtonSolverDict;
  PyDict AdjLinearSolverDict;
  PyDict LinearSolverDict;
  PyDict LineUpdateDict;
  // PyDict UMFPACKDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 )
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 3;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  //  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
  NewtonSolverDict[NewtonSolverParam::params.Verbose]= false;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= UMFPACKDict;
#endif

  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver]= SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  // LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method]= LineUpdateParam::params.LineUpdate.HalvingSearch;

  // NewtonSolverDict[NewtonSolverParam::params.LinearSolver]= UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate]= LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations]= 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations]= 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose]= true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]= NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation]= NonlinearSolverDict;

  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);

  // REFINEMENT LOOP
  // adaptively refine in ndof and in p

  Real targetCost;

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_base);

  // prep output files and data

  // norm data
  Real orderVec= 0;
  Real dofReqVec= 0;
  Real hDOFVec[lastn]= {0};
  int nElemVec[lastn]= {0};
  int nDOFVec[lastn]= {0};
  Real normVec[lastn]= {0};

  // output file
  std::string convhist_filename= filename_base + "test.convhist";
  fstream convhist;
  bool fileStarted= boost::filesystem::exists(convhist_filename);

  if (world.rank() == 0)
  {
    convhist.open(convhist_filename, fstream::app);
    BOOST_REQUIRE_MESSAGE(convhist.good(), "Error opening file: " + convhist_filename);

    if (!fileStarted)
    {
      convhist << "VARIABLES=";
      convhist << "\"p\"";
      convhist << ", \"DOFrequested\"";
      convhist << ", \"1/pow(DOF, 1.0/3.0)\"";
      convhist << ", \"Nelem\"";
      convhist << ", \"Ndof\"";
      convhist << ", \"L2 error\"";
      convhist << std::endl;
    }

    convhist << std::setprecision(16) << std::scientific;
  }

  // prep mesh adaption tools
  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction]= 1.0;
  MOESSDict[MOESSParams::params.CostModel]= MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity]= MOESSParams::VerbosityOptions::Progressbar;

  MOESSDict[MOESSParams::params.LocalSolve]= MOESSParams::params.LocalSolve.Element;

  MOESSDict[MOESSParams::params.MetricOptimization]= MOESSParams::params.MetricOptimization.SANS;
  MOESSDict[MOESSParams::params.ImpliedMetric]= MOESSParams::params.ImpliedMetric.Optimized;

  PyDict MesherDict;
#ifdef SANS_AVRO
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name]= MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.avro;
  MesherDict[avroParams::params.Curved]= false;
#else
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name]= MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
  //  std::vector<int> allBC;
  //  allBC.insert(allBC.end(), BCBoundaryGroups["BCSoln_timeIC"].begin(), BCBoundaryGroups["BCSoln_timeIC"].end());
  //  allBC.insert(allBC.end(), BCBoundaryGroups["BCSoln_timeOut"].begin(), BCBoundaryGroups["BCSoln_timeOut"].end());
  //  allBC.insert(allBC.end(), BCBoundaryGroups["BCSoln_Dirichlet"].begin(), BCBoundaryGroups["BCSoln_Dirichlet"].end());
  ////  allBC.insert(allBC.end(), BCBoundaryGroups["BCNone"].begin(), BCBoundaryGroups["BCNone"].end());
  //  MesherDict[EpicParams::params.SymmetricSurf]= allBC;
#endif

  std::vector<int> cellGroups= {0};

  // status update
  std::cout << std::endl << "OUTER LOOP: DOF request= " << dofReqSamples << ", order= " << pSamples << std::endl;

  // CREATE INITIAL GRID

  // 5x5x5
  const int ii= std::max(3, (int) floor(pow(dofReqSamples*factorial(pSamples)/factorial(pSamples + PhysD3::D), 1./PhysD3::D)));
  const int jj= ii;
  const int kk= jj;
  const Real xmin= 0.0;
  const Real xmax= 1.0;
  const Real ymin= 0.0;
  const Real ymax= 1.0;
  const Real tmin= 0.0;
  const Real tmax= 1.0;

#ifdef SANS_AVRO

  using avro::coord_t;
  using avro::index_t;

  avro::Context context;

  coord_t number= 3;
  Real x0[3]= {0., 0., 0.};
  std::vector<Real> lens(number, 1.);
  std::shared_ptr<avro::Body> pbody= std::make_shared<avro::library::EGADSBox>(&context, x0, lens.data());
  std::shared_ptr<avro::Model> model= std::make_shared<avro::Model>(&context, "cube");
  model->addBody(pbody, true);

  // initialize the mesh
  XField3D_Box_Tet_X1 xfld0(world, ii, jj, kk, xmin, xmax, ymin, ymax, tmin, tmax);
  // copy the mesh into the domain and attach the geometry
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld = std::make_shared<XField_avro<PhysD3, TopoD3>>(xfld0, model);

#else
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld= std::make_shared<XField3D_Box_Tet_X1>(world, ii, jj, kk,
                                                                                       xmin, xmax,
                                                                                       ymin, ymax,
                                                                                       tmin, tmax);
#endif

  const int order= pSamples;

  const int iXmin= XField3D_Box_Tet_X1::iXmin;
  const int iXmax= XField3D_Box_Tet_X1::iXmax;
  const int iYmin= XField3D_Box_Tet_X1::iYmin;
  const int iYmax= XField3D_Box_Tet_X1::iYmax;
  const int iTmin= XField3D_Box_Tet_X1::iZmin;
  const int iTmax= XField3D_Box_Tet_X1::iZmax;

  // define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSoln_timeIC"]= {iTmin};
  BCBoundaryGroups["BCSoln_timeOut"]= {iTmax};
  BCBoundaryGroups["BCSoln_Dirichlet"]= {iXmin, iYmin, iXmax, iYmax};
  //      BCBoundaryGroups["BCSoln_Dirichlet"]= {iXmin, iYmin};
  //      BCBoundaryGroups["BCSoln_Neumann"]= {iXmax, iYmax};
  //      BCBoundaryGroups["BCNone"]= {iXmax, iYmax};

  std::vector<int> active_boundaries= BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  std::vector<int> interiorTraceGroups;

  for (int iTG= 0; iTG < pxfld->nInteriorTraceGroups(); iTG++)
    interiorTraceGroups.push_back(iTG);

  // redeclare solution data for actually solving ST problem
  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol= std::make_shared<SolutionClass>(*pxfld, pde, order, order + 1,
                                              BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre, active_boundaries, disc);

  const int quadOrder= 2*(order + 1);    // somehow this means 2*(order + 1)?

  // create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface= std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                     cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, SolverContinuationDict,
                                                     AdjLinearSolverDict, outputIntegrand);

  // set target cost
  targetCost= dofReqSamples;

  // set initial solution
  //  pGlobalSol->setSolution(solnExact, cellGroups);
  pGlobalSol->setSolution(0.0);

  filename_case= filename_base + "dof" + stringify(targetCost) + "p" + stringify(order) + "/";

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_case);

  std::string adapthist_filename = filename_case + "test.adapthist";
  fstream fadapthist;

  if (world.rank() == 0)
  {
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.hasTrueOutput]= true;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TrueOutput]= trueOutput;

  MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

  MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

  std::string qfld_init_filename= filename_case + "qfld_init_a0.plt";
  output_Tecplot(pGlobalSol->primal.qfld, qfld_init_filename);

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename= filename_case + "qfld_a0.plt";
  output_Tecplot(pGlobalSol->primal.qfld, qfld_filename);

  std::string adjfld_filename= filename_case + "adjfld_a0.plt";
  output_Tecplot(pGlobalSol->adjoint.qfld, adjfld_filename);

  orderVec= order;
  dofReqVec= targetCost;

  // ADAPTATION LOOP

  int iprinted= 0;

  for (int iter= 0; iter < maxIter + 1; iter++)
  {

    if (world.rank() == 0)
      std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int iT = 0; iT < pxfldNew->nInteriorTraceGroups(); iT++)
      interiorTraceGroups.push_back(iT);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld= pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    std::string qfld_init_filename = filename_case + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    std::string qfld_filename = filename_case + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_case + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

    // compute exact error
    QuadratureOrder quadratureOrder(pGlobalSol->primal.qfld.getXField(), -1);

    Real squareError= 0;

    IntegrateCellGroups<TopoD3>::integrate(
        FunctionalCell_DGBR2(errorIntegrand, squareError),
        pGlobalSol->primal.qfld.getXField(), (pGlobalSol->primal.qfld, pGlobalSol->primal.rfld),
        quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size());

#ifdef SANS_MPI
    squareError= boost::mpi::all_reduce(*pxfld->comm(), squareError, std::plus<Real>());

    int nDOFtotal = 0;
    boost::mpi::all_reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>());

    // count the number of elements possessed by this processor
    int nElem = 0;
    for (int elem = 0; elem < pxfld->nElem(); elem++ )
      if (pxfld->getCellGroupGlobal<Tet>(0).associativity(elem).rank() == world.rank())
        nElem++;

    int nElemtotal = 0;
    boost::mpi::all_reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>());
#else
    int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
    int nElemtotal = pxfld->nElem();
#endif

    Real norm= squareError;

    if (iprinted < lastn)
    {
      //          if (world.rank() == 0)
      //            cout << "iprinted= " << iprinted << " < lastn= " << lastn << endl;
      hDOFVec[iprinted]= 1.0/pow(nDOFtotal, 1.0/((Real) PhysD3::D));
      nElemVec[iprinted]= nElemtotal;
      nDOFVec[iprinted]= nDOFtotal;
      normVec[iprinted]= sqrt(norm);
      iprinted++;
    }
    else
    {
      for (int k= 1; k < lastn; k++)
      {
        //            if (world.rank() == 0)
        //              cout << "moving k= " << k << "to (k - 1)= " << k - 1 << endl;
        hDOFVec[k - 1]= hDOFVec[k];
        nElemVec[k - 1]= nElemVec[k];
        nDOFVec[k - 1]= nDOFVec[k];
        normVec[k - 1]= normVec[k];
      }
      //          if (world.rank() == 0)
      //            cout << "crowning with k= " << lastn - 1 << endl;
      hDOFVec[lastn - 1]= 1.0/pow(nDOFtotal, 1.0/((Real) PhysD3::D));
      nElemVec[lastn - 1]= nElemtotal;
      nDOFVec[lastn - 1]= nDOFtotal;
      normVec[lastn - 1]= sqrt(norm);
    }

    std::cout << std::endl << "INNER LOOP EXIT with " << nDOFtotal << " DOFs and " <<
        nElemtotal << " elements." << std::endl << std::endl;

  }

  if (world.rank() == 0)
  {

    fadapthist.close();

    for (int k= 0; k < iprinted; k++)
    {
      // outputfile
      convhist << orderVec;
      convhist << ", " << (Real) dofReqVec;
      convhist << ", " << hDOFVec[k];
      convhist << ", " << (Real) nElemVec[k];
      convhist << ", " << (Real) nDOFVec[k];
      convhist << ", " << normVec[k];
      convhist << endl;
    }

  }

  if (world.rank() == 0)
    convhist.close();
#endif
}

BOOST_AUTO_TEST_SUITE_END()
