// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_CG_NACA_LIP_toy
// testing of 2-D CG for Linear Incompressible Potential on NACA/Joukowski grid

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
//#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

//#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/FullPotential/PDELinearizedIncompressiblePotential2D.h"
#include "pde/FullPotential/BCLinearizedIncompressiblePotential2D_sansLG.h"
#include "pde/FullPotential/SolutionFunction_Potential2D.h"
//#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

//#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
//#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
//#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegratePeriodicTraceGroups.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
//#include "Discretization/LIP_HACK/IntegrandBoundaryTrace_Galerkin_WakeCut_LIP_SRA.h"
#include "Discretization/LIP_HACK/IntegrandInteriorTrace_Galerkin_WakeCut_LIP_SRA.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualInteriorTrace_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Potential_Drela/ResidualBoundaryTrace_WakeCut_CG_Potential_Drela.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Potential_Drela/JacobianBoundaryTrace_WakeCut_CG_Potential_Drela.h"

//#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
//#include "Field/tools/for_each_CellGroup.h"
//#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

//#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
//#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_WakeSymAirfoil_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

//#define NACA

using namespace std;
using namespace SANS;

namespace
{           // local namespace

#ifdef NACA

//----------------------------------------------------------------------------//
// NACA 4-digit rescaled via Rumsey; gives sharp TE at x = 1
//
//  t = nominal thickness (e.g. 0.12 gives NACA0012)
class Naca
{
public:
  explicit Naca( double t ) : t_(t) {}

  void operator()( double s, double& x, double& y )
  {
    x = s;
    y = 4.955743175*t_*( 0.298222773*sqrt(x) - 0.127125232*x - 0.357907906*x*x + 0.291984971*x*x*x - 0.105174606*x*x*x*x );
  }

private:
  double t_;    // thickness
};

#else

//----------------------------------------------------------------------------//
// Joukowski airfoil definition (upper surface)
//
//  s in [0, 1];  LE: s = 0 --> (x,y) = (0,0)
//                TE: s = 1 --> (x,y) = (1,0)
//
//  a = 0.1 gives roughly 12% thickness

class Joukowski
{
public:
  explicit Joukowski( double a ) : a_(a) {}

  void operator()( double s, double& x, double& y )
  {
    double den, xnum, ynum;
    double a = a_;

    den  = 1 + 2*a*(1 + a)*(1 + cos(PI*s));
    xnum = (1 + a*(1 + 2*a)*(1 + cos(PI*s)))* sin(0.5*PI*s)*sin(0.5*PI*s);
    ynum = 0.5*a*(1 + 2*a)*(1 + cos(PI*s))*sin(PI*s);

    x = xnum/den;
    y = ynum/den;
  }

private:
  double a_;    // thickness parameter
};

#endif

//----------------------------------------------------------------------------//
// linear system

template <class SystemMatrix>
class MyLinearSystem : public AlgebraicEquationSetBase<SystemMatrix>
{
public:
  typedef AlgebraicEquationSetBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  explicit MyLinearSystem( const SystemNonZeroPattern& nz, const SystemMatrix& mtx, const SystemVector& rhs )
    : nz_(nz), mtx_(mtx), rhs_(rhs) {}
  virtual ~MyLinearSystem() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  virtual void residual(SystemVector& rsd) override { rsd = rhs_; }

  virtual void jacobian(SystemMatrix& mtx) override { mtx.copy_from(mtx_); }

  virtual void jacobian(SystemNonZeroPattern& nz) override { nz = nz_; }
  virtual void jacobianTranspose(SystemMatrix& mtx) override { SANS_DEVELOPER_EXCEPTION("jacobianTranspose not implemented"); }
  virtual void jacobianTranspose(SystemNonZeroPattern& nz) override { SANS_DEVELOPER_EXCEPTION("jacobianTranspose not implemented"); }

  virtual std::vector<std::vector<Real>> residualNorm(const SystemVector& rsd) const override { return {{0.}}; }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    return true;
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    return true;
  }

  // TODO:implement!
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    return true;
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override {}

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVector& q) override {}

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVector& q) const override {}

  // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorEqSize() const override { return VectorSizeClass(mtx_.m()); }

  // vector for state DOFs (columns in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override { return VectorSizeClass(mtx_.n()); }

  virtual MatrixSizeClass matrixSize() const override
  {
  //return {{ {mtx_.m(), mtx_.n()}, {mtx_.m(), 0} },
  //        { {0       , mtx_.n()}, {0       , 0} }};
    return MatrixSizeClass(mtx_.m(), mtx_.n());
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return 0; }
  virtual int indexQ() const override { return 0; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVector& q) override { return true; }

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override { return 1; }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override
  {
    mpi::communicator world;
    return std::make_shared<mpi::communicator>(world.split(world.rank()));
  }

  virtual void syncDOFs_MPI() override {}

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVector& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override { return true; };

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override {};


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVector& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return false;
  }

protected:
  const SystemNonZeroPattern& nz_;
  const SystemMatrix& mtx_;
  const SystemVector& rhs_;
};

void


//----------------------------------------------------------------------------//
// Tecplot output (assumes triangle)

output_Tecplot_LIP( const PDELinearizedIncompressiblePotential2D& pde,
                    const typename XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle>& xfld,
                    const typename Field<PhysD2, TopoD2, Real>::FieldCellGroupType<Triangle>& qfld,
                    const std::string& filename )
{
  typedef typename XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename Field<PhysD2, TopoD2, Real>::FieldCellGroupType<Triangle> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  typedef PDELinearizedIncompressiblePotential2D PDE;
  typedef PDE::template ArrayQ<Real> ArrayQ;

  std::cout << "output_Tecplot_LIP: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\"" );
  fprintf( fp, ", \"phi\", \"phix\", \"phiy\", \"u\", \"v\", \"|grad(phi)|\", \"phitot\"" );
  fprintf( fp, ", \"phiExact\", \"phixExact\", \"phiyExact\"" );
  fprintf( fp, ", \"phiDiff\", \"phixDiff\", \"phiyDiff\"" );
  fprintf( fp, "\n" );

  int nelem = xfld.nElem();
  int nnode = 3*nelem;
  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=TRIANGLE\n", nnode, nelem );

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );
  int nDOF = qfldElem.nDOF();
  SANS_ASSERT_MSG( nDOF <= 3, "output_Tecplot_LIP: hard-coded for triangles" );

  // grid coordinates, solution data

  typename ElementXFieldClass::RefCoordType stRef;
  typename ElementXFieldClass::VectorX X;
  ArrayQ q;
  DLA::VectorS<2, ArrayQ> gradq = 0;
  Real u, v;

  pde.freestream( u, v );

  // exact solution
  Real e = 0.1;
  Real aoa = 0;
  SolutionFunction_Potential2D_Joukowski slnExact(e, aoa);
  ArrayQ qE, qxE, qyE, qxxE, qxyE, qyyE;

  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );

    for (int i = 0; i < 3; i++)
    {
      if (i == 0) stRef = {0, 0};
      if (i == 1) stRef = {1, 0};
      if (i == 2) stRef = {0, 1};

      xfldElem.eval( stRef, X );
      qfldElem.eval( stRef, q );
      xfldElem.evalGradient( stRef, qfldElem, gradq );

      fprintf( fp, "%17.10e %17.10e", X[0], X[1] );
      fprintf( fp, " %14.7e %14.7e %14.7e", q, gradq[0], gradq[1] );
      fprintf( fp, " %14.7e %14.7e", u + gradq[0], v + gradq[1] );
      fprintf( fp, " %14.7e", sqrt( gradq[0]*gradq[0] + gradq[1]*gradq[1] ) );
      fprintf( fp, " %14.7e", q + (u*X[0] + v*X[1]) );

      slnExact( X[0], X[1], 0., qE, qxE, qyE, qxxE, qxyE, qyyE );
      fprintf( fp, " %14.7e %14.7e %14.7e", qE, qxE, qyE );
      fprintf( fp, " %14.7e %14.7e %14.7e", q - qE, gradq[0] - qxE, gradq[1] - qyE );
      fprintf( fp, "\n" );
    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    fprintf( fp, "%d %d %d\n", 3*elem+1, 3*elem+2, 3*elem+3 );
  }

  fclose( fp );
}

}           // local namespace


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_CG_NACA_LIP_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_CG_NACA_LIP )
{
  typedef PDELinearizedIncompressiblePotential2D PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;
  typedef PDEClass::MatrixQ<Real> MatrixQ;

  //typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputEntropyErrorSquaredClass;
  //typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquaredClass> NDOutputEntropyErrorSquaredClass;
  //typedef IntegrandCell_Galerkin_Output<NDOutputEntropyErrorSquaredClass> IntegrandSquareErrorClass;

#if 0
  typedef BCLinearizedIncompressiblePotential2DVector<Real> BCVector;

  typedef AlgebraicEquationSet_Galerkin< NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
#endif

  typedef BCNDConvertSpace<PhysD2, BCLinearizedIncompressiblePotential2D<BCTypeWall, Real> > NDBCWall;
  typedef BCNDConvertSpace<PhysD2, BCLinearizedIncompressiblePotential2D<BCTypeFarfieldVortex, Real> > NDBCFarfieldVortex;
  //typedef BCNDConvertSpace<PhysD2, BC_WakeCut_LIP_SRA > NDBCWakeCut;
  typedef BCNDConvertSpace<PhysD2, BCInt_WakeCut_LIP_SRA > NDBCIntWakeCut;

  typedef NDVectorCategory<boost::mpl::vector1<NDBCWall>, NDBCWall::Category> NDBCWallVectorCategory;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCFarfieldVortex>, NDBCFarfieldVortex::Category> NDBCFarfieldVortexVectorCategory;
  //typedef NDVectorCategory<boost::mpl::vector1<NDBCWakeCut>, NDBCWakeCut::Category> NDBCWakeCutVectorCategory;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCIntWakeCut>, NDBCIntWakeCut::Category> NDBCWakeCutVectorCategory;

  // integrand classes
  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandCellClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCWallVectorCategory, Galerkin> IntegrandBCWallClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCFarfieldVortexVectorCategory, Galerkin> IntegrandBCFarfieldClass;

  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCWakeCutVectorCategory, Galerkin> IntegrandBCWakeCutClass;

  // linear system classes
  typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
  typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
  typedef SparseMatrixClass               SystemMatrixClass;
  typedef SparseVectorClass               SystemVectorClass;

  // PDE
  const Real u = 1;
  const Real v = 0;
  NDPDEClass pde( u, v );

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BCs

  const Real x0 = 0.25;   // vortex centroid
  const Real y0 = 0;
  const Real circ = 1;    // vortex circulation (user specified)

  NDBCWall bcWall( u, v );
  NDBCFarfieldVortex bcFarfield( x0, y0, circ );
  //NDBCWakeCut bcWakeCut( circ );
  NDBCIntWakeCut bcIntWakeCut( circ );

#if 0   // not used; save for next version (that does use PyDict)
  // wall BC
  PyDict dictBCWall;
  dictBCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Wall;

  // wake-cut BC
  PyDict dictBCWakeCut;
  dictBCWakeCut[BCParams::params.BC.BCType] = BCParams::params.BC.WakeCut;

  // farfield BC: vortex
  const Real x0 = 0.25;   // vortex centroid
  const Real y0 = 0;
  const Real circ = 1;    // vortex circulation (user specified)

  typedef BCLinearizedIncompressiblePotential2D<BCFarfieldVortex, Real> BCFarfieldClass;
  PyDict dictBCFarfield;
  dictBCWall[BCParams::params.BC.BCType] = BCParams::params.BC.FarfieldVortex;
  dictBC[BCFarfieldClass::ParamsType::params.x0] = x0;
  dictBC[BCFarfieldClass::ParamsType::params.y0] = y0;
  dictBC[BCFarfieldClass::ParamsType::params.circ] = circ;

  PyDict dictBCList;
  dictBCList["BCWall"] = dictBCWall;
  dictBCList["BCWakeCut"] = dictBCWakeCut;
  dictBCList["BCFarfield"] = dictBCFarfield;

  // No exceptions should be thrown
  BCParams::checkInputs(dictBCList);

  std::map<std::string, std::vector<int>> mapBCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  mapBCBoundaryGroups["BCWall"] = {0};
  mapBCBoundaryGroups["BCWakeCut"] = {1};
  mapBCBoundaryGroups["BCFarfield"] = {2};
#endif

#if 0   // not used; direct residual/jacobian evaluation instead
  // Set up Newton Solver
  PyDict dictNewtonSolver, dictUMFPACK, dictNewtonLineUpdate;

  dictUMFPACK[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  dictNewtonLineUpdate[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  dictNewtonSolver[NewtonSolverParam::params.LinearSolver] = dictUMFPACK;
  dictNewtonSolver[NewtonSolverParam::params.LineUpdate] = dictNewtonLineUpdate;
  dictNewtonSolver[NewtonSolverParam::params.MinIterations] = 0;
  dictNewtonSolver[NewtonSolverParam::params.MaxIterations] = 2;

  NewtonSolverParam::checkInputs(dictNewtonSolver);

  //Real sExact = 0.0;
  //NDOutputEntropyErrorSquaredClass outEntropyError( pde, sExact );
  //IntegrandSquareErrorClass fcnErr( outEntropyError, {0} );

  //norm data
  //Real normVec[10];   // L2 error
#endif

#if 0
  // Tecplot output of grid convergence
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_2D_HDG.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/Solve/Solve2D_HDG_Triangle_AD_FullTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/Solve/Solve2D_HDG_Triangle_AD_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif

  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;
#endif

  // loop over solution orders
  int ordermin = 1;
  int ordermax = 1;
  for (int order = ordermin; order <= ordermax; order++)
  {
    //indx = 0;

    SANS_ASSERT_MSG( order == 1, "only P1 currently functioning; wake-cut breaks CG solution ctor for P>1" );

    // loop over grid resolution: 2^power
    int powermin = 6;
    int powermax = 6;
    for (int power = powermin; power <= powermax; power++)
    {
      const int iiairf = pow(2, power);
      const int iistag = iiairf/2;
      const int iiwake = iiairf/2;
      const int iiffd  = iiairf/2;

#ifdef NACA
      cout << "geometry: NACA" << endl;
      const Real tNACA = 0.12;
      Naca airfoilCoords( tNACA );
#else
      cout << "geometry: Joukowski" << endl;
      const Real aJouk = 0.1;
      Joukowski airfoilCoords( aJouk );
#endif

      // airfoil: cosine distribution
      std::vector< DLA::VectorS<2,Real> > airf(iiairf+1);
      Real s, x, y;
      Real sLE = 0, sTE = 1;
      Real xTE = 1;
      for ( int i = 0; i <= iiairf; i++)
      {
        s = sLE + (sTE - sLE)*(0.5 - 0.5*cos(PI*i/((Real) iiairf)));
        airfoilCoords( s, x, y );
        if (i == iiairf)    // needed for roundoff
        {
          x = xTE;
          y = 0;
        }

        airf[i][0] =  x;
        airf[i][1] =  y;
      }

      // grid: airfoil w/ wake (uses Triangle)
      XField2D_WakeSymAirfoil xfld( airf, iistag, iiwake, iiffd );
      //cout << "xfld boundary traces = " << xfld.nBoundaryTraceGroups() << endl;

      // solution: CG
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
      qfld = 0;

      const int nDOFPDE = qfld.nDOF();

      #if 0
      // Tecplot dump
      string filename = "tmp/slnCG_NACA_LIP_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(iiairf);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
      #endif

#if 0     // not needed for direct residual/jacobian evaluation
      // Lagrange multiplier: Hierarchical, C0 (also at corners)
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(dictBCList, mapBCBoundaryGroups) );
      lgfld = 0;
#endif

      // integrands

      IntegrandCellClass        fcnCell( pde, {0} );

      StabilizationNitsche stab(order);
      IntegrandBCWallClass      fcnBCWall( pde, bcWall, {0}, stab );
      IntegrandBCFarfieldClass  fcnBCFarfield( pde, bcFarfield, {2}, stab );
      //IntegrandBCWakeCutClass   fcnBCWakeCut( pde, bcWakeCut, {1} );

#define USE_BNDTRACE
#ifdef USE_BNDTRACE
      IntegrandBCWakeCutClass   fcnIntWakeCut( pde, bcIntWakeCut, {1} );
#else
      IntegrandInteriorWakeCutClass fcnIntWakeCut( pde, bcIntWakeCut, {1} );
#endif

      cout << "wake cut BC:  nPeriodicTraceGroups = " << fcnIntWakeCut.nPeriodicTraceGroups()
           << "  periodicTraceGroup(0) = " << fcnIntWakeCut.periodicTraceGroup(0) << endl;

      //IntegrandKutta fcnKutta(pde, xfld.dupPointOffset_, xfld.KuttaPoints_);

      //QuadratureOrder quadratureOrder( xfld, -1 );
      //QuadratureOrder quadratureOrderMin( xfld, 0 );
      //std::vector<Real> tolResidual = {1e-11, 1e-11};

      std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 1);
      std::vector<int> quadratureOrderMin(xfld.nBoundaryTraceGroups(), 0);

      // residual

      SystemVectorClass rsd(nDOFPDE);

      rsd = 0;

      IntegrateCellGroups<TopoD2>::integrate(
          ResidualCell_Galerkin( fcnCell, rsd ),
          xfld, qfld, quadratureOrder.data(), xfld.nCellGroups() );

      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          ResidualBoundaryTrace_Galerkin( fcnBCWall, rsd ),
          xfld, qfld, quadratureOrder.data(), xfld.nBoundaryTraceGroups() );
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          ResidualBoundaryTrace_Galerkin( fcnBCFarfield, rsd ),
          xfld, qfld, quadratureOrder.data(), xfld.nBoundaryTraceGroups() );

      IntegratePeriodicTraceGroups<TopoD2>::integrate(
          ResidualInteriorTrace_Galerkin( fcnIntWakeCut, rsd ),
          xfld, qfld, quadratureOrder.data(), xfld.nBoundaryTraceGroups() );

#if 0
      IntegratePeriodicTraceGroups<TopoD2>::integrate(
          ResidualInteriorTrace_Galerkin( fcnBCWakeCut, rsd ),
          xfld, qfld, quadratureOrder.data(), xfld.nBoundaryTraceGroups() );
#endif

      // initial residual norms

      Real norm;

      norm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        norm += rsd[n] * rsd[n];
      norm = sqrt( norm / static_cast<Real>(nDOFPDE) );

      cout << "initial residuals (RMS): " << norm << endl;

      // jacobian nonzero pattern

      typedef SurrealS<NDPDEClass::N> SurrealClass;
      typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

#if 0
      DLA::MatrixD<NonZeroPatternClass> nz = {{ {nDOFPDE, nDOFPDE} }};

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianCell_Galerkin( fcnCell, nz(0,0) ),
          xfld, qfld, quadratureOrderMin.data(), xfld.nCellGroups() );
#endif

      NonZeroPatternClass nz(nDOFPDE, nDOFPDE);

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianCell_Galerkin( fcnCell, nz ),
          xfld, qfld, quadratureOrderMin.data(), xfld.nCellGroups() );

      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          JacobianBoundaryTrace_Galerkin<SurrealClass>( fcnBCWall, nz ),
          xfld, qfld, quadratureOrderMin.data(), xfld.nBoundaryTraceGroups() );
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          JacobianBoundaryTrace_Galerkin<SurrealClass>( fcnBCFarfield, nz ),
          xfld, qfld, quadratureOrderMin.data(), xfld.nBoundaryTraceGroups() );

      IntegratePeriodicTraceGroups<TopoD2>::integrate(
          JacobianInteriorTrace_Galerkin( fcnIntWakeCut, nz ),
          xfld, qfld, quadratureOrderMin.data(), xfld.nBoundaryTraceGroups() );

#if 0
      //IntegratePeriodicTraceGroups<TopoD2>::integrate(
      //    JacobianInteriorTrace_Galerkin( fcnBCWakeCut, nz ),
      //    xfld, qfld, quadratureOrderMin.data(), xfld.nBoundaryTraceGroups() );
      JacobianInteriorTrace_Galerkin( fcnBCWakeCut, nz );
#endif
#if 0
      JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD2>::integrate(
          fcnBCWakeCut, qfld, quadratureOrderMin.data(), xfld.nBoundaryTraceGroups(), nz );
#endif

      // jacobian

      SystemMatrixClass jac(nz);

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianCell_Galerkin( fcnCell, jac ),
          xfld, qfld, quadratureOrder.data(), xfld.nCellGroups() );

      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          JacobianBoundaryTrace_Galerkin<SurrealClass>( fcnBCWall, jac ),
          xfld, qfld, quadratureOrder.data(), xfld.nBoundaryTraceGroups() );
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          JacobianBoundaryTrace_Galerkin<SurrealClass>( fcnBCFarfield, jac ),
          xfld, qfld, quadratureOrder.data(), xfld.nBoundaryTraceGroups() );

      IntegratePeriodicTraceGroups<TopoD2>::integrate(
          JacobianInteriorTrace_Galerkin( fcnIntWakeCut, jac ),
          xfld, qfld, quadratureOrder.data(), xfld.nBoundaryTraceGroups() );

      #if 1
      string foutName = "tmp/jacCG_NACA_LIP_P";
      foutName += to_string(order);
      foutName += "_";
      foutName += to_string(iiairf);
      foutName += ".mtx";
      fstream fout( foutName, fstream::out );
      cout << "btest: dumping jacobian to file " << foutName << endl;
      WriteMatrixMarketFile( jac, fout );
      #endif

      // linear solve

      SystemVectorClass sln(nDOFPDE);
      MyLinearSystem<SystemMatrixClass> sys(nz, jac, rsd);
      SLA::UMFPACK<SystemMatrixClass> solver(sys);

      solver.solve( rsd, sln );

      for (int k = 0; k < nDOFPDE; k++)
        qfld.DOF(k) = -sln[k];

      // final residual

      rsd = 0;

      IntegrateCellGroups<TopoD2>::integrate(
          ResidualCell_Galerkin( fcnCell, rsd ),
          xfld, qfld, quadratureOrder.data(), xfld.nCellGroups() );

      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          ResidualBoundaryTrace_Galerkin( fcnBCWall, rsd ),
          xfld, qfld, quadratureOrder.data(), xfld.nBoundaryTraceGroups() );
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          ResidualBoundaryTrace_Galerkin( fcnBCFarfield, rsd ),
          xfld, qfld, quadratureOrder.data(), xfld.nBoundaryTraceGroups() );

      IntegratePeriodicTraceGroups<TopoD2>::integrate(
          ResidualInteriorTrace_Galerkin( fcnIntWakeCut, rsd ),
          xfld, qfld, quadratureOrder.data(), xfld.nBoundaryTraceGroups() );

      norm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        norm += rsd[n] * rsd[n];
      norm = sqrt( norm / static_cast<Real>(nDOFPDE) );

      cout << "final residuals (RMS): " << norm << endl;

      #if 1
      // Tecplot dump
      string filename = "tmp/slnCG_NACA_LIP_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(iiairf);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot_LIP( pde, xfld.template getCellGroup<Triangle>(0), qfld.template getCellGroup<Triangle>(0), filename );
      #endif


#if 0   // temporary
      ////////////
      // SOLVE
      ////////////

      typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
      typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

      PrimalEquationSetClass primalEqSet(xfld, qfld, lgfld, pde, quadratureOrder, tolResidual, {0}, dictBCList, mapBCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> solver( primalEqSet, dictNewtonSolver );

      auto sizeSys = primalEqSet.vectorStateSize();
      SystemVectorClass ini(sizeSys);
      SystemVectorClass sln(sizeSys);
      SystemVectorClass slnchk(sizeSys);
      SystemVectorClass rsd(sizeSys);
      rsd = 0;

      primalEqSet.fillSystemVector(ini);
      sln = ini;

      BOOST_REQUIRE( solver.solve(ini,sln).converged );
      primalEqSet.setSolutionField(sln);

      #if 1
      string foutName = "tmp/jacCG_LIP_cubicbump_P";
      foutName += to_string(order);
      foutName += "_";
      foutName += to_string(ii);
      foutName += "x";
      foutName += to_string(jj);
      foutName += ".mtx";
      fstream fout( foutName, fstream::out );
      cout << "btest: dumping jacobian to file " << foutName << endl;

      PrimalEquationSetClass::SystemNonZeroPattern nz(primalEqSet.matrixSize());
      primalEqSet.jacobian( sln, nz );
      SystemMatrixClass jac(nz);
      primalEqSet.jacobian(sln, jac);
      //primalEqSet.jacobian( jac );
      WriteMatrixMarketFile( jac, fout );
      #endif

      rsd = 0;
      primalEqSet.residual(sln, rsd);

      // check that the residual is zero

      Real rsdPDEnorm = 0;

      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnorm += pow(rsd[0][n], 2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnorm), 1e-12 );

      #define SANS_VERBOSE
      #ifdef SANS_VERBOSE
      //cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( EntropySquareError );
      //if (indx > 1)
      //  cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;
      #endif

      #if 0
      // Tecplot dump
      string filename = "tmp/slnCG_NACA_LIP_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
      #endif
      #if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
      #endif
#endif    // temporary

    }

#if 0
    // Tecplot result file
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }
#endif

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
