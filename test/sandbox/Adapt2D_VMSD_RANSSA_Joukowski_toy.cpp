// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_NavierStokes_FlatPlate_btest
// Testing of the MOESS framework on a 2D Navier-Stokes problem


#define BOUNDARYOUTPUT
//#define WHOLEPATCH
//#define INNERPATCH

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include <iostream>
#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"

#include "Discretization/VMSD/IntegrandBoundaryTrace_Flux_mitState_VMSD.h"

#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_VMSD_Linear.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD_Linear.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_OutputWeightRsd_VMSD.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"
#include "Adaptation/MOESS/MOESSParams.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//#define LINEARVMSD

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_RANSSA_Joukowski_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_RANSSA_Joukowski_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, VMSD> OutputIntegrandClass;
#endif

  typedef ParamType_Distance ParamBuilderType;

  typedef SolutionData_VMSD<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
#ifdef LINEARVMSD
  typedef AlgebraicEquationSet_VMSD_Linear<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
#else
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
#endif


  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2_DOFWeighted;
  std::vector<Real> tol = {1.0e-13, 1.0e-13, 1.0e-13};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.5;
  const Real Reynolds = 1.e6;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1.;                            // density scale
  const Real tRef = 1;          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  const Real nuRef = 1;
  const Real ntr = ntRef / nuRef;

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Entropy, eVanLeer, nuRef );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntr}) );

  // Galerkin Stabilization
//  DiscretizationVMSD stab(false);
  DiscretizationVMSD stab;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;
#if 1
  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;
  StateVector["rhont"]  = rhoRef*ntr; //TODO: not sure about this, was previously just "nt" which gave a Python dictionary error

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

//  PyDict BCOut(BCIn);
#else

  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aRef = atan(uRef/vRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aRef;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
#endif

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
//  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0,1};
  BCBoundaryGroups["BCIn"] = {2,3};
//  BCBoundaryGroups["BCOut"] = {3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde, 1.0);
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, stab);
#else
  // Drag output
  NDOutputClass outputFcn(pde, 1., 0.);
  OutputIntegrandClass outputIntegrand( outputFcn, BCBoundaryGroups["BCNoSlip"] );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

#if 0
  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.MUMPS;
#else
#if 0
    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
#else
    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
    PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;
#endif
    PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;

    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
#endif

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-22;
  PETScDict[SLA::PETScSolverParam::params.DivergenceTolerance] = 1e6;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 50;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = true;
//  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
//  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.DGMRES;
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);

//  PreconditionerAdjoint[SLA::PreconditionerILUParam::params.FillLevel] = 8;
//  PreconditionerAdjoint[SLA::PreconditionerILUParam::params.NonzeroDiagonal] = true;

  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-22;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 200;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;
//  PETScDictAdjoint[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

 // Easier PTC for subsequent solves
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;
  NonlinearSolverDict[PseudoTimeParam::params.CFLPartialStepDecreaseFactor] = 0.9;
  NonlinearSolverDict[PseudoTimeParam::params.SteveLogic] = false;
  NonlinearSolverDict[PseudoTimeParam::params.RequireConvergedNewton] = false;

//  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
//                      = SolverContinuationParams<TemporalMarch>::params.Continuation.None;
//  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  int orderL = 1, orderH = 3;
  int powerL = 2, powerH = 6;

  int maxIter = 30;
  int restartIter = -1;
  std::string restartFileBase = "";

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    orderL = orderH = std::stoi(argv[1]);
  if (argc >= 3)
    powerL = powerH = std::stoi(argv[2]);
  if (argc >= 4)
    maxIter = std::stoi(argv[3]);
  if (argc >= 5)
    restartIter = std::stoi(argv[4]);
  if (argc >= 6)
    restartFileBase = argv[5];

  if (world.rank() == 0)
  {
    std::cout << ", orderL,H = " << orderL << ", " << orderH;
    std::cout << ", powerL,H = " << powerL << ", " << powerH << ", maxIter: " << maxIter << std::endl;
  }
#endif

 //--------ADAPTATION LOOP--------

  for ( int order = orderL; order<=orderH; order++)
  {
    stab.setNitscheOrder(order);

    for (int power=powerL; power<=powerH; power++)
    {

      // pcaplan TODO check these have lagrange basis functions...local patch only works with these
      Real targetCost = 500.0*pow(2.0,power);

      bool DGCONV = true;
      if (DGCONV)
      {
        Real nDOFperCell_DG = (order+1)*(order+2)/2;

        Real nDOFperCell_CG = nDOFperCell_DG;
        nDOFperCell_CG -= (3 - 1./2); // the node dofs are shared by 6
        nDOFperCell_CG -= (3 - 3./2)*std::max(0,(order-1)); // if there are edge dofs they are shared by 2

        targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
      }


      // to make sure folders have a consistent number of zero digits
      const int string_pad = 5;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);

      std::string filename_base;
      filename_base = "tmp/RANSJOUK/VMSDSIP/JOUK_" + int_pad + "_P" + std::to_string(order) + "/";

      boost::filesystem::create_directories(filename_base);

      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld_linear;

      std::shared_ptr<Field_CG_Cell<PhysD2,TopoD2,ArrayQ>> prestartfld;
      int startIter = 0;
      bool isRestart = false;

      if (restartIter >= 0)
      {
        startIter = restartIter;
        std::string file_initial_xfld = restartFileBase + "restart.grm";
        std::string file_initial_linear_mesh = restartFileBase + "restart_lin.grm";
        std::string file_initial_qfld = restartFileBase + "restart.qrm";

        if (world.rank() == 0)
          std::cout<< "Starting from " << file_initial_xfld << " and " << file_initial_qfld << std::endl;

        pxfld = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, file_initial_xfld);
        pxfld_linear = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, file_initial_linear_mesh);

        prestartfld = std::make_shared<Field_CG_Cell<PhysD2,TopoD2,ArrayQ>>(*pxfld, order, BasisFunctionCategory_Lagrange, EmbeddedCGField);

        ReadSolution_PX(file_initial_qfld, *prestartfld);

        isRestart = true;

      }
      else //start from scratch
      {

        if (world.rank() == 0)
          std::cout<< "Starting from scratch" << std::endl;

        std::string file_initial_mesh = "grids/joukowski_initial_mesh/jouk_q3.grm";
        std::string file_initial_linear_mesh = "grids/joukowski_initial_mesh/jouk_q1_out.grm";

        pxfld = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, file_initial_mesh);
        pxfld_linear = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, file_initial_linear_mesh);
      }

      std::string adapthist_filename = filename_base + "test.adapthist";

      fstream fadapthist;

      if (world.rank() == 0)
      {
        if (isRestart)
          fadapthist.open( adapthist_filename, fstream::app );
        else
          fadapthist.open( adapthist_filename, fstream::out );

        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
      }



      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Optimizer_MaxEval] = 6000;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
//      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Detailed;
      MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;//Dual;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;
      MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANSparallel;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
      MesherDict[EpicParams::params.CurvedQOrder] = 3; //Mesh order
      MesherDict[EpicParams::params.CSF] = "grids/joukowski_initial_mesh/jouk.csf"; //Geometry file
      MesherDict[EpicParams::params.GeometryList2D] = "Airfoil Farfield"; //Geometry list
      MesherDict[EpicParams::params.minGeom] = 1e-7;
      MesherDict[EpicParams::params.minH] = 1e-7;
      MesherDict[EpicParams::params.maxGeom] = 100.;
      MesherDict[EpicParams::params.maxH] = 100.;
      MesherDict[EpicParams::params.nPointGeom] = -1;
      MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;
      MesherDict[EpicParams::params.nThread] = 1;
      //MesherDict[EpicParams::params.Version] = "madcap_devel-10.2";

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Compute distance field
      PyDict paramDict;
      std::shared_ptr<Field_CG_Cell<PhysD2, TopoD2, Real>>
        pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfld, 4, BasisFunctionCategory_Lagrange);
      DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));



      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;

      const int porder = order;
      const int pporder = order+1;
      //Solution data
      pGlobalSol = std::make_shared<SolutionClass>((*pdistfld, *pxfld), pde, stab,
                                                   order, porder, order+1, pporder,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   active_boundaries, paramDict);


      const int quadOrder = 2*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      //Set initial solution
      if (isRestart)
      {
        prestartfld->projectTo(pGlobalSol->primal.qfld);
        pGlobalSol->primal.qpfld = 0;
      }
      else
      {
        pGlobalSol->setSolution(q0);
      }

      if (!isRestart)
      {
        std::string qfld_init_filename = filename_base + "qfld_init_a0_rank" + std::to_string(world.rank()) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );
      }

      pInterface->solveGlobalPrimalProblem();

      std::string xfld_restart_filename =  filename_base + "restart.grm";
      std::string xfld_linear_restart_filename =  filename_base + "restart_lin.grm";
      std::string qfld_restart_filename =  filename_base + "restart.qrm";
      pInterface->writeRestart(xfld_restart_filename, qfld_restart_filename);
      WriteMeshGrm<PhysD2, TopoD2>(*pxfld_linear, xfld_linear_restart_filename);

      pInterface->solveGlobalAdjointProblem();

      if (!isRestart)
      {
        std::string qfld_filename = filename_base + "qfld_a0_rank" + std::to_string(world.rank()) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string qpfld_filename = filename_base + "qpfld_a0_rank" + std::to_string(world.rank()) + ".plt";
        output_Tecplot( pGlobalSol->primal.qpfld, qpfld_filename );

        //      std::string delta_adjfld_filename = filename_base + "delta_adjfld_a0_rank" + std::to_string(world.rank()) + ".plt";
        //      output_Tecplot( pGlobalSol->adjoint.qfld, delta_adjfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a0_rank" + std::to_string(world.rank()) + ".plt";
        output_Tecplot( pInterface->getAdjField(), adjfld_filename );
      }

      pInterface->computeErrorEstimates();

      if (!isRestart)
      {
        std::string efld_filename = filename_base + "efld_a0_rank" + std::to_string(world.rank()) + ".plt";

        pInterface->output_EField(efld_filename);
      }

      for (int iter = startIter; iter < maxIter+1; iter++)
      {
        if (world.rank() == 0 )
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;
//        ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld_linear, cellGroups, *pInterface, iter);
        ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew_linear = ptr_pair.first;
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew = ptr_pair.second;

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        //Compute distance field
        pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfldNew, 4, BasisFunctionCategory_Lagrange);
        DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));

        //Solution data
        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>((*pdistfld, *pxfldNew), pde, stab,
                                                     order, porder, order+1, pporder,
                                                     BasisFunctionCategory_Lagrange,
                                                     BasisFunctionCategory_Lagrange,
                                                     BasisFunctionCategory_Lagrange,
                                                     active_boundaries, paramDict);

#if 0
        pGlobalSolNew->setSolution(q0);
#else
        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol, true);
#endif

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                            cellGroups, interiorTraceGroups,
                                                            PyBCList, BCBoundaryGroups,
                                                            SolverContinuationDict, LinearSolverDict,
                                                            outputIntegrand);


        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld = pxfldNew;
        pxfld_linear = pxfldNew_linear;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

        pInterface->solveGlobalPrimalProblem();

        pInterface->writeRestart(xfld_restart_filename, qfld_restart_filename);
        WriteMeshGrm<PhysD2, TopoD2>(*pxfldNew_linear, xfld_linear_restart_filename);

        pInterface->solveGlobalAdjointProblem();

        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string qpfld_filename = filename_base + "qpfld_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
        output_Tecplot( pGlobalSol->primal.qpfld, qpfld_filename );

        std::string delta_adjfld_filename = filename_base + "delta_adjfld_a"
        + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, delta_adjfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
        output_Tecplot( pInterface->getAdjField(), adjfld_filename );

        std::string padjfld_filename = filename_base + "adjfld_p_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
        output_Tecplot( pInterface->getPAdjField(), padjfld_filename );

        //Compute error estimates
        pInterface->computeErrorEstimates();

        std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
        pInterface->output_EField(efld_filename);

      }

      if (world.rank() == 0)
        fadapthist.close();
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
