// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve1D_DGBR2_Line_Euler_Nozzle_ArtificialViscosity_toy
// Inviscid quasi-1D nozzle with shock and artificial viscosity


//#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <chrono>
#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include "../../src/pde/NS/PDEEuler1D_Source.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler1D.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/BCEulermitAVSensor1D.h"
#include "pde/NS/Fluids1D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Output_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_Dispatch_DGBR2.h"

#include "Discretization/isValidState/SetValidStateCell.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/Function/FunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/output_Tecplot_PDE.h"
#include "Field/output_gnuplot.h"

#include "Meshing/XField1D/XField1D.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGBR2_Line_Euler_Nozzle_ArtificialViscosity_toy )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGBR2_Line_Euler_Nozzle_ArtificialViscosity_toy )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D_Source<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;
//  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD1, PDEBaseClass> Sensor;
  typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEulermitAVSensor1DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler1D_Entropy<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEulerQ1D_TotalPressure<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_CG ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD1, TopoD1, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Used to compute the volume of the domain
  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Const> NDScalarConst;

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1.0e-9, 1.0e-9};

  PDEBaseClass::EulerResidualInterpCategory interp = PDEBaseClass::Euler_ResidInterp_Raw;
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = true;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)

  // reference state (freestream)
  const Real pr = 1.4;
  const Real Pc = 1.0;
  const Real Tc = 1.0;
  const Real rhoc = 1;                   // density scale

  //Real Pte = 0.998615682125091553;

  // PDE
  GasModel gas(gamma, R);

  // DGBR2 discretization
  Real viscousEtaParameter = 2*Line::NEdge;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDScalarConst constFcn(1);

  typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDEClass> BCBase;
  typedef BCmitAVSensor1D<BCTypeFlux_mitState, BCBase> BCClass;

  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTt_mitState;
  BCInflow[BCClass::ParamsType::params.TtSpec] = Tc;
  BCInflow[BCClass::ParamsType::params.PtSpec] = Pc;

#define PRESSURE_EXIT 1
  PyDict BCOut;
#ifdef PRESSURE_EXIT
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_mitState;
  BCOut[BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = Pc / pr;
  std::cout << "Outflow pressure: " << Pc / pr << std::endl;
#else
  PyDict BCNone;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.None;
#endif

  // Define BC list
  PyDict PyBCList;
  PyBCList["BCInflow"] = BCInflow;
  PyBCList["BCOut"] = BCOut;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Define the BoundaryGroups for each boundary condition
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["BCInflow"] = {0}; //left
  BCBoundaryGroups["BCOut"] = {1}; // right


  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;
  PyDict ParamDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.0;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 10;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] =  false;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type] =
                  SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]    = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLDecreaseFactor]  = 0.1;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor]  = 2;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  //--------ADAPTATION LOOP--------
  for (int grid_index = 3; grid_index <= 3; grid_index++)
  {

    std::string filename_base = "tmp/NozzleNewSteadyQ0/";
    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directory(base_dir);

    NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_residual"
        + std::to_string(grid_index) + ".dat";

    // Grid
    int ii = 100*pow(2,grid_index);
    std::shared_ptr<XField<PhysD1, TopoD1>> pxfld =
            std::make_shared<XField1D>( ii, 0, 1 );

    std::vector<int> cellGroups;
    for ( int i = 0; i < pxfld->nCellGroups(); i++)
      cellGroups.push_back(i);

    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<GenHField_CG<PhysD1,TopoD1>> phfld =
        std::make_shared<GenHField_CG<PhysD1,TopoD1>>(*pxfld);

    //Check the BC dictionary
    BCParams::checkInputs(PyBCList);

    std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

    //Solution data
    typedef SolutionData_DGBR2<PhysD1, TopoD1, NDPDEClass, ParamBuilderType> SolutionClass;

    std::shared_ptr<SolutionClass> pGlobalSol;
    std::shared_ptr<SolutionClass> pGlobalSolOld;
    std::shared_ptr<SolverInterfaceClass> pInterface;


    for (int order = 0; order <= 3; order++)
    {
      std::shared_ptr<ScalarFunction1D_ArtNozzle> area( new ScalarFunction1D_ArtNozzle() );
      PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, area, true);
//      PDEBaseClass pdeEulerAV(order, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, area);
      // Sensor equation terms
      Sensor sensor(pdeEulerAV);
      SensorAdvectiveFlux sensor_adv(0.0);
      SensorViscousFlux sensor_visc(order);
      SensorSource sensor_source(order, sensor);
      // AV PDE with sensor equation
      NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order, hasSpaceTimeDiffusion,
                     EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, area);

#ifndef BOUNDARYOUTPUT
      // Enthalpy error output

      NDOutputClass fcnOutput(pde);
      OutputIntegrandClass outputIntegrand(fcnOutput, cellGroups);
#else
      // Pressure output
      NDOutputClass fcnOutput(pde, 1e-3, -0.5, 0.);
      IntegrandOutputPressure outputIntegrand( disc, fcnOutput, BCBoundaryGroups["Wall"] );
#endif

//      const int quadOrder = 3*(order + 2)-1; // 14 at order=3
      const int quadOrder = 3*order+1;

      pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pxfld), pde, order, order,
                                                   BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                   active_boundaries, ParamDict, disc);
      pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

      //Create solver interface
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      //Set initial solution
      if (order == 0)
      {
        // initial condition
        ArrayQ q0 = pde.setDOFFrom( AVVariable<DensityVelocityPressure1D, Real>({{rhoc, 0.1, Pc}, 0.0}) );

        pGlobalSol->setSolution(q0);
      }
      else
      {
        pGlobalSol->setSolution(*pGlobalSolOld);
      }
//      string filenameIC = "NozzleIC_"
//                     + Type2String<QType>::str()
//                     + "_P"
//                     + std::to_string(order)
//                     + "_G"
//                     + std::to_string(grid_index)
//                     + ".plt";
//      output_Tecplot( pGlobalSol->primal.qfld, filename_base + filenameIC );

      pGlobalSolOld = pGlobalSol;

      auto t1 = std::chrono::high_resolution_clock::now();
      pInterface->solveGlobalPrimalProblem();
      auto t2 = std::chrono::high_resolution_clock::now();
      std::cout << "P solve: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << "ms" << std::endl;
      string filename = "ArtNozzle_"
                     + Type2String<QType>::str()
                     + "_P"
                     + std::to_string(order)
                     + "_G"
                     + std::to_string(grid_index)
                     + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, filename_base + filename );

//      string sol_filename = "ArtNozzleDerived_"
//                     + Type2String<QType>::str()
//                     + "_P"
//                     + std::to_string(order)
//                     + "_G"
//                     + std::to_string(grid_index)
//                     + ".plt";
//      output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld,
//                     pGlobalSol->primal.rfld, filename_base + sol_filename);


    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
