// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_CG_AD_btest
// testing of 1-D CG with Advection-Diffusion

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "../../src/Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "../../src/Discretization/VMSD/FunctionalCell_VMSD.h"
#include "../../src/Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "../../src/Discretization/VMSD/JacobianFunctionalCell_VMSD.h"
#include "SANS_btest.h"

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_WeightedResidual_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_EG_Cell.h"
#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
#define TAU = 10;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_1D_CG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorOrder_1D_CG_AD )
{

  //ADVECTION_DIFFUSION CASE
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ScalarFunction1D_BL SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef ScalarFunction1D_BLAdj AdjSolutionExact;
  typedef SolnNDConvertSpace<PhysD1, AdjSolutionExact> NDAdjSolutionExact;

//  typedef BCTypeFunction_mitState<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> AlgebraicEquationSet_VMSDClass;
  typedef AlgebraicEquationSet_VMSDClass::BCParams BCParams;

  // PDE
  Real a = 1;
  AdvectiveFlux1D_Uniform adv( a );
  Real nu = 0.01;

  //ADVECTION DIFFUSION
  ViscousFlux1D_Uniform visc( nu );

  Real src = -2.1;
  Source1D_Uniform source(src);

  NDSolutionExact solnExact(a, nu, src);
  NDAdjSolutionExact adjSolnExact(a, nu, src);

  NDPDEClass pde(adv, visc, source);

  // DEFINE BCS
  // Create a BC dictionary
  PyDict BL;
  BL[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.BL;
  BL[SolutionExact::ParamsType::params.a] = a;
  BL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
    BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function] = BL;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType] = "Dirichlet";
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0,1};

  typedef AlgebraicEquationSet_VMSDClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_VMSDClass::SystemVector SystemVectorClass;

  // Primal Exact Soln
  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_VMSD_Output<NDErrorClass, NDPDEClass> ErrorIntegrandClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorGalerkinIntegrandClass;

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(pde, fcnError, {0}, true);
  ErrorGalerkinIntegrandClass errorGalerkinIntegrand(fcnError, {0});

  // Adjoint Exact Solution
  typedef OutputCell_SolutionErrorSquared<PDEClass, AdjSolutionExact> AdjErrorClass;
  typedef OutputNDConvertSpace<PhysD1, AdjErrorClass> NDAdjErrorClass;
  typedef IntegrandCell_VMSD_Output<NDAdjErrorClass, NDPDEClass> ErrorAdjIntegrandClass;
  typedef IntegrandCell_Galerkin_Output<NDAdjErrorClass> ErrorAdjIntegrandGalerkinClass;

  NDAdjErrorClass fcnAdjError(adjSolnExact);
  ErrorAdjIntegrandClass errorAdjIntegrand(pde, fcnAdjError, {0}, true);
  ErrorAdjIntegrandGalerkinClass errorAdjGalerkinIntegrand(fcnAdjError, {0});


#define VOLUME
#ifndef VOLUME
  // Output Functional
  typedef BCAdvectionDiffusion<PhysD1,BCType> BCClass;
  typedef BCNDConvertSpace<PhysD1,BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> IntegrandBCClass;

  NDBCClass bc(pde, BCSoln);
  IntegrandBCClass fcnBC(pde, bc, {1});

  typedef OutputAdvectionDiffusion1D_WeightedResidual OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> IntegrandOutputClass;

  NDOutputClass outputFcn(1.);
  IntegrandOutputClass outputIntegrand( outputFcn, {1} );
#else
  typedef OutputCell_Solution<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_VMSD_Output<NDOutputClass, NDPDEClass> IntegrandOutputClass;

  NDOutputClass outputFcn;
  IntegrandOutputClass outputIntegrand(pde, outputFcn, {0});
#endif

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  string filename_base = "tmp/1DAD/VMSDNEW/VMSDNEW";
  string filename2 = filename_base + "_Errors.txt";

  fstream foutsol( filename2, fstream::out );

  int ordermin = 1;
  int ordermax = 3;
  for (int order = ordermin; order <= ordermax; order++)
  {
    // loop over grid resolution: 2^power
    int powermin = 2;
    int powermax = 10;

    for (int power = powermin; power <= powermax; power++)
    {
      std::cout << "order: " << order << ", power:" << power << "\n";
      int ii = pow( 2, power )+1;

      // grid:

      XField1D xfld( ii );

      // solution: Hierarchical, C0
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Lagrange, EmbeddedCGField);
      Field_EG_Cell<PhysD1, TopoD1, ArrayQ> qpfld(qfld, 1, BasisFunctionCategory_Lagrange); //these should become EG in paralllal

      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> adjfld(xfld, order, BasisFunctionCategory_Lagrange, EmbeddedCGField);
      Field_EG_Cell<PhysD1, TopoD1, ArrayQ> adjpfld(adjfld, 1, BasisFunctionCategory_Lagrange); //these should become EG in paralllal

      qfld = 0; qpfld = 0; adjfld = 0; adjpfld = 0;

      const int nDOFPDE = qfld.nDOF();
      const int nDOFPDEp = qpfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 1
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Lagrange,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgflda( xfld, order, BasisFunctionCategory_Lagrange,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#elif 0
      Field2D_CG_BoundaryEdge_Independent<NDPDEClass> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                             BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
      Field2D_DG_BoundaryEdge<NDPDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                 BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#endif

      lgfld = 0; lgflda = 0;

      const int nDOFBC = lgfld.nDOF();

      QuadratureOrder quadratureOrder( xfld, 3*(order+1) );
      std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

      DiscretizationVMSD stab(VMSDp, order);
      AlgebraicEquationSet_VMSDClass AlgEqSet(xfld, qfld, qpfld, lgfld, pde, stab,
                                                  quadratureOrder, ResidualNorm_L2, tol, {0}, {0}, PyBCList, BCBoundaryGroups );

      // residual vectors

      SystemVectorClass q(AlgEqSet.vectorStateSize());
      SystemVectorClass rsd(AlgEqSet.vectorEqSize());

      AlgEqSet.fillSystemVector(q);

      rsd = 0;

      AlgEqSet.residual(q, rsd);

      // solve

      SLA::UMFPACK<SystemMatrixClass> solver(AlgEqSet);

      SystemVectorClass dq(q.size());
      dq = 0;

      solver.solve(rsd, dq);
      // updated solution

#if 0
      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile(
          solver.A(), fout );
#endif

      q -= dq;

      AlgEqSet.setSolutionField(q);

      // check that the residual is zero

      rsd = 0;
      AlgEqSet.residual(q, rsd);

      Real rsdPDEpnrm = 0;
      for (int n = 0; n < nDOFPDEp; n++)
        rsdPDEpnrm += pow(rsd[0][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEpnrm), tol[1] );

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), tol[0] );

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[2][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdBCnrm), tol[2] );

      // L2 solution error

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_VMSD( errorIntegrand, SquareError ),
          xfld, (qfld, qpfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real primalErr = sqrt(SquareError);

      ArrayQ SquareErrorGalerkin = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorGalerkinIntegrand, SquareErrorGalerkin ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real primalErrGalerkin = sqrt(SquareErrorGalerkin);

      ArrayQ output = 0;
#ifndef VOLUME

      IntegrateBoundaryTraceGroups<TopoD1>::integrate(
          FunctionalBoundaryTrace_WeightedResidual_Galerkin( outputIntegrand, fcnBC, output ),
          xfld, qfld, quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size() );
#else
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_VMSD( outputIntegrand, output ),
          xfld, (qfld, qpfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#endif
      // solve
//      Real outTrue = 3.431191405491972;
//      Real outTrue = 3.4311914055472848368; //Matlab
      Real outTrue =   3.4311914055472848381984761232566;

      Real outputErr = fabs(outTrue - output);


#if 1

      // adjoint (right hand side)

      SystemVectorClass rhs(q.size());
      rhs = 0;

#ifndef VOLUME

      IntegrateBoundaryTraceGroups<TopoD1>::integrate(
          JacobianFunctionalBoundaryTrace_WeightedResidual_Galerkin<SurrealClass>(outputIntegrand, fcnBC, rhs(0)), xfld, qfld,
          quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size()  );
#else
      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell_VMSD( outputIntegrand, rhs(0), rhs(1) ),
          xfld, (qfld, qpfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
#endif

      SLA::UMFPACK<SystemMatrixClass> solverAdj(AlgEqSet, SLA::TransposeSolve);

      SystemVectorClass adj(q.size());
      adj = 0;

      solverAdj.solve(rhs, adj);

      // updated solution

      for (int k = 0; k < nDOFPDEp; k++)
        adjpfld.DOF(k) = adj[0][k];

      for (int k = 0; k < nDOFPDE; k++)
        adjfld.DOF(k) = adj[1][k];

      for (int k = 0; k < nDOFBC; k++)
        lgflda.DOF(k) = adj[2][k];

      // L2 adjoint solution error

      Real AdjSquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_VMSD( errorAdjIntegrand, AdjSquareError ),
          xfld, (adjfld, adjpfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real adjError = sqrt(AdjSquareError);

      Real AdjSquareErrorGalerkin = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorAdjGalerkinIntegrand, AdjSquareErrorGalerkin ),
          xfld, adjfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real adjErrorGalerkin = sqrt(AdjSquareErrorGalerkin);
#endif

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      foutsol << order << " " << ii << " " << std::setprecision(15) << primalErr;
      foutsol << " " << primalErrGalerkin;
      foutsol << " " << adjError;
      foutsol << " " << adjErrorGalerkin;
      foutsol << " " << outputErr  << "\n";
#endif


#if 1
      {
        // Tecplot dump of solution
        string filenameQ = filename_base + "_P";
        filenameQ += stringify(order);
        filenameQ += "_";
        filenameQ += stringify(ii);
        filenameQ += "_qfld.plt";
        //      cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( qfld, filenameQ );
      }

      {
        // Tecplot dump of solution
        string filenameQ = filename_base + "_P";
        filenameQ += stringify(order);
        filenameQ += "_";
        filenameQ += stringify(ii);
        filenameQ += "_qpfld.plt";
        //      cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( qpfld, filenameQ );
      }

      {
        // Tecplot dump of solution
        string filenameQ = filename_base + "_P";
        filenameQ += stringify(order);
        filenameQ += "_";
        filenameQ += stringify(ii);
        filenameQ += "_adjfld.plt";
        //      cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( adjfld, filenameQ );
      }

      {
        // Tecplot dump of solution
        string filenameQ = filename_base + "_P";
        filenameQ += stringify(order);
        filenameQ += "_";
        filenameQ += stringify(ii);
        filenameQ += "_adjpfld.plt";
        //      cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( adjpfld, filenameQ );
      }


#endif

#if 0
      // Tecplot dump of solution
      string filenameAdj = filename_base + "_P";
      filenameAdj += stringify(order);
      filenameAdj += "_";
      filenameAdj += stringify(ii);
      filenameAdj += "_adj.plt";
//      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qflda, filenameAdj );

#endif

#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
#endif

    }

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
