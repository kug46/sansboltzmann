// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve3D_DGBR2_Tet_Navier_Stokes_Tandem_Sphere_toy.cpp

#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "NonLinearSolver/NonLinearSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DConservative.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/OutputEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
//#include "Discretization/DG/IntegrandBoundaryTrace_Dirichlet_mitState_DGBR2.h"
//#include "Discretization/DG/IntegrandBoundaryTrace_Robin_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "tools/linspace.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

#include "Meshing/EPIC/XField_PX.h"
#include "Meshing/gmsh/XField_gmsh.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"
#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h" // Added for Transient

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

//Instantiate
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"
using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE (Solve3D_DGBR2_Tet_NavierStokes_Cylinder_test_suite)

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGBR2_Tet_Cylinder )
{
/*
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector, AlgEqSetTraits_Sparse,
                                     DGBR2, XField<PhysD3, TopoD3>> PrimalEquationSetClass;
  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> RKClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  std::vector<Real> tol = { 1e-6, 1e-6 };

  // gas model
  const Real gamma = 1.4;
  const Real R = 1; // 2.964e-4;        // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R / (gamma - 1);
  const Real Cp = gamma * Cv;

  // reference state (freestream)
  const Real Mach = 0.1; //.5936599e-4;
  const Real Reynolds = 550;
  const Real Prandtl = 0.72;
  const Real lRef = 1; // 0.2;          // Need to look up in write up                    // length scale
  const Real rhoRef = 1; //1.177;                            // density scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real tRef = 1;                           //temperature X-> // pressure
  const Real pRef = rhoRef * R * tRef;          // pressure

  const Real cRef = sqrt( gamma * R * tRef );
  const Real qRef = cRef * Mach;                              // velocity scale

  const Real uRef = qRef * cos( aoaRef );             // velocity
  const Real vRef = qRef * sin( aoaRef );
  const Real wRef = 0;
  const Real ERef = Cv * tRef + 0.5 * (uRef * uRef + vRef * vRef);
  const Real muRef = rhoRef * qRef * lRef / Reynolds;
  //std::cout << "Viscosity = " << muRef << std::endl;

  // PDE
  GasModel gas( gamma, R );
  ViscosityModelType visc( muRef );
  ThermalConductivityModel tcond( Prandtl, Cp );
  NDPDEClass pde( gas, visc, tcond, Euler_ResidInterp_Raw );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc( 0, viscousEtaParameter );

  // BC Set up

  typedef BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass> BCClass;
  typedef BCNDConvertSpace<PhysD3, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  NDBCClass bc( pde );
  IntegrandBCClass fcnBC( pde, bc, { 0 }, disc );

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict StateVector;
  StateVector[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  StateVector[Conservative3DParams::params.rho] = rhoRef;
  StateVector[Conservative3DParams::params.rhou] = rhoRef * uRef;
  StateVector[Conservative3DParams::params.rhov] = rhoRef * vRef;
  StateVector[Conservative3DParams::params.rhow] = rhoRef * wRef;
  StateVector[Conservative3DParams::params.rhoE] = rhoRef * ERef;

  PyDict BCFlow;
  BCFlow[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFlow[BCEuler3DFullStateParams<NSVariableType3DParams>::params.StateVector] = StateVector;
  BCFlow[BCEuler3DFullStateParams<NSVariableType3DParams>::params.Characteristic] = true;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict PyBCList;
  PyBCList["BCWall"] = BCNoSlip;
  PyBCList["Flow"] = BCFlow;
  PyBCList["Symm"] = BCSymmetry;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //std::cout<<"BC gourps updated @ 10.27 1205 hrs"<<std::endl;
  BCBoundaryGroups["BCWall"] =
  { 4};             //{0};
  BCBoundaryGroups["Symm"] =
  { 2,3,5,6};             //{3,4,5,6};
  BCBoundaryGroups["Flow"] =
  { 0,1};             //{1,2};
  BCParams::checkInputs( PyBCList );

  GlobalTime time;
  Real Tperiod, dt;
  int nsteps;

  time = 0.;
  Tperiod = 10;
  dt = 5e-3;
  //d::cout << "!!!!!!!!==    TIME STEP: " << dt << "& BC GROUPS UPDATED ==!!!!!!!!" << std::endl;
  nsteps = int( Tperiod / dt );

  // Set up NEWTON solver:
  PyDict NonLinearSolverDict, NewtonSolverDict, NewtonLineUpdateDict, LinSolverDict, UMFPACKDict, LineUpdateDict;

  // Set up Newton Solver

  // Take the Preconditioners from my Tandem Sphere case and just copy paste them here

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  LinSolverDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  LinSolverDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  LinSolverDict[SLA::PETScSolverParam::params.MaxIterations] = 2500;
  LinSolverDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  LinSolverDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  LinSolverDict[SLA::PETScSolverParam::params.Verbose] = true;
  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  LinSolverDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //std::cout << "Linear solver: PETSc" << std::endl;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  NewtonSolverParam::checkInputs( NewtonSolverDict );
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  int order = 2;             //1;

  // Read in Mesh

  int RKorder = 4;

  std::string filein = "grids/Cylinder_r1_Q2.msh";
  std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
  std::cout << "READING FROM THIS FILE:: " << filein << std::endl;
  std::cout << "!!!!!!!\n!!!!!!!!!!!!!!\n!!!!!!!!!!!!!\n";

  std::cout << "~~~~~~~~~~ ORDER = " << order << " ~~~~~~~~~~~" << std::endl;
  XField_gmsh<PhysD3, TopoD3> xfld( world, filein );

  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld( xfld, order, BasisFunctionCategory_Hierarchical );
  FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld( xfld, order, BasisFunctionCategory_Hierarchical );
  Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                        BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups ) );
  lgfld = 0;
  // Set Initial Conditions

  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure3D<Real>( rhoRef, uRef, vRef, wRef, pRef ) );

  qfld = q0;
  for (int i = 0; i < rfld.nDOF(); i++)
    rfld.DOF( i ) = 0.0;
  lgfld = 0;

  // Solving System
  const int quadOrder = 8;             //3*order + 2;
  QuadratureOrder quadratureOrder( xfld, quadOrder );
  std::cout << "$$$$$$$$$ QuadORDER = " << quadOrder << " $$$$$$$$$$$$$$" << std::endl;

  PrimalEquationSetClass PrimalEqSet( xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder, tol, { 0 }, { 0 }, PyBCList, BCBoundaryGroups, time );

  // RK set up

  int RKtype = 0;
  int RKstages = RKorder;
  RKClass RK( RKorder, RKstages, RKtype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, { tol[0] }, { 0 }, PrimalEqSet );
  //LinSolverDict
  //NonLinearSolverDict
  // Tecplot Output
  //int FieldOrder = order;
  //string filename = "tmp/TandemSphere_TEST";
  //filename += to_string( order );
  //filename += "_Q";
  //filename += "_Time_";
  //int f_time = time * 10;
  //filename += to_string( 0 );
  //filename += "_time.plt";
  //output_Tecplot( qfld, filename );

  for (int step = 0; step < nsteps; step++)
  {
    std::cout << "Time Step: " << step + 1 << "\n value of time is " << time << std::endl;
    // Advance solution
    RK.march( 1 );
    if (step % 10 == 0)
// Tecplot Output
    {
      int FieldOrder = order;
      string filename = "tmp/quick_Re550_Cylinder_r1_";
      filename += to_string( order );
      filename += "_Q";
      filename += to_string( FieldOrder );
      filename += "_Time_";
      //int f_time = time * 10;
      filename += to_string( step + 1 );
      filename += "_rank" + to_string( world.rank() );
      filename += ".dat";
      output_Tecplot( qfld, filename );
    }
  }
*/
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
