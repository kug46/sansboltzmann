// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_HDG_Line_Euler_Sod_toy
// testing of 1-D HDG with Timestepping

#undef SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"

#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DEntropy.h"

#include "pde/NS/PDEEuler1D.h"
#include "pde/NS/BCEuler1D.h"
#include "pde/NS/SolutionFunction_Euler1D.h"
#include "pde/BCParameters.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction_MMS.h"

#include "pde/BCParameters.h" // Needed because the BCVector is extended here

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Field/Element/ElementProjection_L2.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/ProjectSoln/ProjectConstCell.h"


#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnTrace_Discontinuous.h"
#include "Field/tools/for_each_InteriorFieldTraceGroup.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_HDG_Line_Euler_Sod_test_suite )

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( Solve1D_HDG_Line_Euler_Sod )
{
  typedef QTypeEntropy QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> PDENDClass;
  typedef PDENDClass::ArrayQ<Real> ArrayQ;
  typedef PDENDClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_HDG< PDENDClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolutionFunction_Euler1D_Riemann<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolnNDConvertSpace<PhysD1, SolutionClass> SolutionNDClass;

  typedef AlgebraicEquationSet_TimeMarch<PDENDClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> AlgebraicEquationSet_BDFClass;

  typedef PDENDClass::template ArrayQ<Real> ArrayQ;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  GlobalTime time(0);

  // Gas Model
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);
  // PDE
  PDENDClass pde(time, gas, PDEClass::Euler_ResidInterp_Momentum);

  typedef IntegrandCell_EulerDensity<PhysD1, PDENDClass > IntegrandDensity;

  IntegrandDensity fcnDensity( pde, {0} );

  std::vector<Real> tol = {1e-12, 1e-12, 1e-12, 1e-12};

  // HDG discretization
  DiscretizationHDG<PDENDClass> disc( pde, Global, Gradient );

  // BC
  // Create a BC dictionary
  PyDict BCReflect;

  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect;

  PyDict PyBCList;
  PyBCList["BCReflect"] = BCReflect;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCReflect"] = {0,1}; //left and right

  // Set up IC with Sod's shock tube problem
  //  See: "Riemann Solvers and Numerical Methods for Fluid Dynamics" - Toro (Section 4.3.3 Table 4.1 in my copy)
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.interface] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.rhoL] = 1.0;
  solnArgs[SolutionClass::ParamsType::params.uL] = 0.0;
  solnArgs[SolutionClass::ParamsType::params.pL] = 1.0;
  solnArgs[SolutionClass::ParamsType::params.rhoR] = 0.125;
  solnArgs[SolutionClass::ParamsType::params.uR] = 0.0;
  solnArgs[SolutionClass::ParamsType::params.pR] = 0.1;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionNDClass solnExact(time, solnArgs);
  Real Tend = 0.25;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1};

  // set BDF and grid parameters for the run
  int BDFmin = 2;
  int BDFmax = 2;

  for (int BDFweights = BDFmin; BDFweights <= BDFmax; BDFweights++)
  {

    // grid and temporal weights - use P1 for BDF1, etc...
    std::vector<Real> weights;

    int order;

    if (BDFweights==2)
    {
      weights = {1, -1}; //BDF1
      order = 1;
    }
    else if (BDFweights==3)
    {
      weights = {1.5, -2, 0.5}; //BDF2
      order = 2;
    }
    else if (BDFweights==4)
    {
      weights = {11./6., -3., 9./6., -1./3.}; //BDF3
      order = 3;
    }
    else if (BDFweights==5)
    {
      weights = {25./12., -4., 3., -4./3., 1./4.}; //BDF3
      order = 4;
    }
    else
    {
      weights = {1, -1}; //BDF1
      order = 1;
      cout << "BDF not defined, using BDF1\n";
    }

    int indx = 0;

#if 0
    cout << "BDF" <<  (BDFweights-1) << ": \n";
#endif

    int ii = 200; //grid size - using timesteps = grid size

    XField1D xfld( ii );

    Real dt;
    int nsteps = 22;
    dt = Tend / (nsteps);

    // DG solution field
    // cell solution
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufld(xfld, order, BasisFunctionCategory_Legendre);
    ufld = 0;

    Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
    qfld = 0;

    Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> uIfld(xfld, order, BasisFunctionCategory_Legendre);
    uIfld = 0.0;

    // lagrange multipliers not actually used either
    Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ>
      lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
    lgfld = 0;

    // Previous solutions for BDF
    FieldSequence<PhysD1, TopoD1, ArrayQ> ufldspast(BDFweights-1, FieldConstructor<Field_DG_Cell>(), xfld, order, BasisFunctionCategory_Legendre);

    // Initial condition
    typedef typename XField<PhysD1, TopoD1>::FieldCellGroupType<TopoD1> XFieldCellGroupType;
    typedef typename Field< PhysD1, TopoD1, ArrayQ>::FieldCellGroupType<TopoD1> UFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename UFieldCellGroupType::template ElementType<> ElementUFieldClass;

    ElementXFieldClass xfldElem( xfld.getCellGroup<Line>(0).basis() );
    ElementUFieldClass ufldElem( qfld.getCellGroup<Line>(0).basis() );

    for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, ufld) );
#if 0
    // Tecplot dump grid
    string filename2 = "tmp/icHDG_EulerSod_P";
    filename2 += to_string(order);
    filename2 += "_";
    filename2 += to_string(ii);
    filename2 += ".plt";
    output_Tecplot( qfld, filename2 );
#endif

    // Set IC
//    time = -(BDFweights-2)*dt;
    time = 0.0;
    int stepstart = 0;
    for (int j = BDFweights-2; j >= 0; j--)
    {
      for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, ufldspast[j]) );
      time += dt;
      stepstart += 1;
    }

    // Set up Newton Solver
    PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
    NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
    NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

    NewtonSolverParam::checkInputs(NewtonSolverDict);

    QuadratureOrder quadratureOrder( xfld, -1 );

    // Create the spatial discretization
    PrimalEquationSetClass AlgEqSetSpace(xfld, ufld, qfld, uIfld, lgfld, pde, disc, quadratureOrder, tol,
                                         {0}, {0}, PyBCList, BCBoundaryGroups, time);
    AlgebraicEquationSet_BDFClass AlgEqSetBDF(xfld, ufld, ufldspast, pde, {0}, dt, weights, AlgEqSetSpace );

    // Non-linear solver
    NewtonSolver<SystemMatrixClass> Solver( AlgEqSetBDF, NewtonSolverDict );

    // residual vectors
    SystemVectorClass q(AlgEqSetBDF.vectorStateSize());
    SystemVectorClass rsd(AlgEqSet.vectorEqSize());
    SystemVectorClass dq(q.size());

    q = 0;
    rsd = 0;

    for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, ufld) );
    for_each_InteriorFieldTraceGroup<TopoD1>::apply( ProjectSolnTrace_Discontinuous(solnExact, {0}), (xfld, uIfld) );
    AlgEqSetBDF.fillSystemVector(q);

    // set up jacobian sparsity pattern once
    SystemNonZeroPattern nz(AlgEqSetBDF.matrixSize());
    AlgEqSetBDF.jacobian(q, nz);

    // jacobian
    SystemMatrixClass jac(nz);
    AlgEqSetBDF.jacobian(q, jac);

    //start clock
    timer solution_time;

#if 1
    // Tecplot dump grid
    string filename2 = "tmp/icHDG_EulerSod_P";
    filename2 += to_string(order);
    filename2 += "_";
    filename2 += to_string(ii);
    filename2 += ".plt";
    output_Tecplot( ufld, filename2 );

    // Gnuplot dump grid
    string gfilename2 = "tmp/icHDG_EulerSod_P";
    gfilename2 += to_string(order);
    gfilename2 += "_";
    gfilename2 += to_string(ii);
    gfilename2 += ".gplt";
    output_gnuplot( ufld, gfilename2 );
#endif

    /////////////////////////////
    // add on unsteady term (du/dt=0)
    /////////////////////////////
    bool solved = false;
    for (int step = stepstart; step < nsteps; step++)
    {
      // reset fld, residual, jac, etc...
      rsd = 0;
      jac  = 0;

      solved = Solver.solve(q,q).converged;
      BOOST_CHECK(solved);

      //rotate current field into t-1 field and t-1 field into t-2 field.
      ufldspast.rotate();
      for (int i = 0; i < ufld.nDOF(); i++)
        ufldspast[0].DOF(i) = ufld.DOF(i);
#if 0
    // Tecplot dump grid
    string filename3 = "tmp/timeHDG_EulerSod_P";
    filename3 += to_string(order);
    filename3 += "_";
    filename3 += to_string(ii);
    filename3 += "_";
    filename3 += to_string(step);
    filename3 += ".plt";
    output_Tecplot( qfld, filename3 );
#endif

      time += dt;

    }

#if 1
    const int nDOFPDE = qfld.nDOF();
    const int nDOFBC  = lgfld.nDOF();

    const int nDOFtot = nDOFPDE + nDOFBC;

    // Monitor Density
//    Real Mass = 0.0;
//    int quadratureOrder[4] = {-1, -1, -1, -1};    // max
//    IntegrateCellGroups<TopoD1>::integrate(
//        FunctionalCell( fcnDensity, Mass ), xfld, qfld, quadratureOrder, 1 );
//    // We should be conserving mass...
//    BOOST_CHECK_CLOSE( 0.5625, Mass, 1e-12 );

    cout << std::setprecision(16) << std::scientific;
    cout << "P = " << order << " ii = " << ii << ": DOF = " << nDOFtot << ": nsteps = " << nsteps;
//    cout << " Mass: " << Mass;
    cout << " CPUTime = " << solution_time.elapsed() << " s";
    cout << endl;
#endif

    indx++;
    BOOST_CHECK_CLOSE( time, Tend, 1e-12 );

#if 1
    // Tecplot dump grid
    string filename = "tmp/slnHDG_EulerSod_P";
    filename += to_string(order);
    filename += "_";
    filename += to_string(ii);
    filename += ".plt";
    output_Tecplot( ufld, filename );

    // gnuplot dump grid
    string gfilename = "tmp/slnHDG_EulerSod_P";
    gfilename += to_string(order);
    gfilename += "_";
    gfilename += to_string(ii);
    gfilename += ".gplt";
    output_gnuplot( ufld, gfilename );
#endif
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
