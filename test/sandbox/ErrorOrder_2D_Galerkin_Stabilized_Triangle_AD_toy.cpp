// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//#define SANS_FULLTEST
//#define BOUNDARYOUTPUT
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "pyrite_fstream.h"
#include "Field/output_Tecplot.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_Solution.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Output_Galerkin.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Adaptation/MOESS/SolverInterface_AGLS.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/Function/WeightedFunctionIntegral.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_CG_Triangle_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_CG_Triangle_AD )
{
//  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  //EXACT SOLN
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_DoubleBL> SolutionExact;


#ifndef BOUNDARYOUTPUT
  //ADJOINT TYPEDEFS
  typedef ScalarFunction2D_ForcingFunction<NDPDEClass> WeightFunctional;
  typedef SolnNDConvertSpace<PhysD2, WeightFunctional>       NDWeightFunctional;

  typedef OutputCell_WeightedSolution<PDEClass, WeightFunctional> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDWeightOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDWeightOutputClass,NDPDEClass> OutputIntegrandClass;

  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> AdjSolutionExact;
  typedef ForcingFunction2D_MMS<PDEClass> ForcingAdjType;
  typedef OutputCell_SolutionErrorSquared<PDEClass, AdjSolutionExact> AdjErrorClass;
  typedef OutputNDConvertSpace<PhysD2, AdjErrorClass> AdjNDErrorClass;
  typedef IntegrandCell_Galerkin_Output<AdjNDErrorClass> AdjErrorIntegrandClassL2;
#else
  typedef ScalarFunction2D_QuadraticExponential BoundaryWeight;
  typedef OutputAdvectionDiffusion2D_FunctionWeightedResidual<BoundaryWeight> OutputClass;
//  typedef OutputAdvectionDiffusion2D_FunctionWeightedFlux<PDEClass, BoundaryWeight> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;
#endif

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClassL2;

  typedef ParamType_None ParamBuilderType;
  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
//  typedef SolverInterface_AGLS<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  // PDE
  Real a = 0.6;
  Real b = 0.8;
  Real nu = 1./50.;

  AdvectiveFlux2D_Uniform adv( a, b );
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_None source;

  // Create a solution dictionary
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;
  DoubleBL[SolutionExact::ParamsType::params.offset] = 1;
  DoubleBL[SolutionExact::ParamsType::params.scale] = -1;

  SolutionExact solnExact( DoubleBL );

  NDPDEClass pde( adv, visc, source );


  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] = "Dirichlet";
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCSolution"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSolution"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-10, 1e-10};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;


  // integrands
  NDErrorClass fcnErrorL2(solnExact);
  ErrorIntegrandClassL2 errorIntegrandL2(fcnErrorL2, {0});


  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;
#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

//  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 1;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.PreconditionerSide] = SLA::PreconditionerASMParam::params.PreconditionerSide.Right;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
//  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-15;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 4000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 200;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;

  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  string filename_base = "tmp/AD2DSTRUC/GLS/";
  boost::filesystem::create_directories(filename_base);

  string filename2 = filename_base + "Errors.txt";
  fstream foutsol;

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;

  int ordermin = 1; int ordermax = 3;
  for (int order = ordermin; order <= ordermax; order++)
  {
//    int indx = 0;
    StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby, order, 1, order );

    // loop over grid resolution: 2^power
    int powermin = 2; int powermax = 7;
    for (int power = powermin; power <= powermax; power++)
    {


#ifndef BOUNDARYOUTPUT
  AdjSolutionExact adjExact(1.,1.,0.,1.);

  AdvectiveFlux2D_Uniform adv_adj( -a, -b );

  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact));
  NDPDEClass pdeadj( adv_adj, visc, source, forcingadjptr );

  NDWeightFunctional volumeFcn( pdeadj );
  NDWeightOutputClass weightOutput( volumeFcn );
  OutputIntegrandClass outputIntegrand(pde, weightOutput, {0}, stab );

  Real trueOutput = 0.08033955918095300;
//
//  //---------------------------------------------------------------------------//
//    // Compute the exact output on a fine grid and high-quadrature
//    Real trueOutput=0;
//    {
//      XField2D_Box_UnionJack_Triangle_X1 xfld( 512, 512 );
//
//      for_each_CellGroup<TopoD2>::apply( WeightedFunctionIntegral(volumeFcn, solnExact, trueOutput, {0}), xfld );
//      std::cout << "trueOutput = " << std::scientific << std::setprecision(16) << outputTrue << std::endl;
//    }
//
  AdjNDErrorClass adjfcnErrorL2(adjExact);
  AdjErrorIntegrandClassL2 adjerrorIntegrandL2(adjfcnErrorL2, {0});

//  NDOutputClass weightOutput( boundaryWeight );
//  OutputIntegrandClass outputIntegrand(pde, weightOutput, {0}, stab );
//
//  Real trueOutput = 8.033955918095300-02;

#else

  // Drag output
  PyDict boundaryWeight;
  boundaryWeight[BoundaryWeight::ParamsType::params.a0] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.ax] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.ay] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.axx] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.ayy] = 0;
  boundaryWeight[BoundaryWeight::ParamsType::params.axy] = 1;


//  NDOutputClass outputFcn(pde, boundaryWeight, false, true);
  NDOutputClass outputFcn(boundaryWeight);
  OutputIntegrandClass outputIntegrand( outputFcn, {0,1,2,3} );

//  const Real trueOutput = 0.04040277777771614;

  // From mathematica!
//  const Real trueOutput = -(pow(b,3)*(pow(a,2)*(-1 + exp(a/nu)) + exp(b/nu)*(pow(a,2)
//    - 2*exp(a/nu)*(a - nu)*nu - 2*pow(nu,2))) + pow(a,3)*(pow(b,2)*(-1 + exp(b/nu))
//    + exp(a/nu)*(pow(b,2) - 2*exp(b/nu)*(b - nu)*nu - 2*pow(nu,2))))
//    /(2.*pow(a,2)*pow(b,2)*(-1 + exp(a/nu))*(-1 + exp(b/nu)));

  const Real trueOutput = 0.068262275884618573592511779613638;
//  const Real trueOutput = -0.93733228395804475591189068028009;
#endif


      foutsol.open( filename2, fstream::app );

      int ii = pow( 2, power );
      int jj = ii;

      // grid:
      pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                   active_boundaries, stab);

      const int quadOrder = 2*(order+1);
      QuadratureOrder quadratureOrder( *pxfld, quadOrder );

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrand);

      pGlobalSol->setSolution(0.0);

      timer clock;
      pInterface->solveGlobalPrimalProblem();
      Real elapsed = clock.elapsed();
//      Real output1 = pInterface->getOutput();

      pInterface->solveGlobalAdjointProblem();
      Real output2 = pInterface->getOutput();

      pInterface->computeErrorEstimates();
      Real outputErrEstimate = pInterface->getGlobalErrorEstimate();

//      Real outputErr1 = (output1 - trueOutput);
      Real outputErr2 = (output2 - trueOutput);

//      Real errEstErr1 = outputErr1 - outputErrEstimate;
      Real errEstErr2 = outputErr2 - outputErrEstimate;


      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_Galerkin( errorIntegrandL2, SquareError ),
          *pxfld, pGlobalSol->primal.qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real L2err = sqrt(SquareError);

#ifndef BOUNDARYOUTPUT
      Real AdjSquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_Galerkin( adjerrorIntegrandL2, AdjSquareError ),
          *pxfld, pInterface->getAdjField(), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real adjError = sqrt(AdjSquareError);
#else
      Real adjError = 0;
#endif


      // Tecplot dump
      std::string filename_base2 = filename_base + "P";
      filename_base2 += to_string(order);
      filename_base2 += "_";
//      filename_base2 += int_pad;
      filename_base2 += "x";
      filename_base2 += to_string(jj);

      std::string filenameq = filename_base2 + "_qfld.plt";
      std::string filenameadj  = filename_base2 + "_adjfld.plt";

      output_Tecplot( pGlobalSol->primal.qfld, filenameq );
      output_Tecplot( pInterface->getAdjField(), filenameadj );

      int nDOF = pGlobalSol->primal.qfld.nDOF();

      foutsol << order << " " << ii << " " << nDOF << std::setprecision(15);
      foutsol << " " << L2err;
      foutsol << " " << adjError;
//      foutsol << " " << fabs(outputErr1);
      foutsol << " " << fabs(outputErr2);
//      foutsol << " " << fabs(errEstErr1);
      foutsol << " " << fabs(errEstErr2);
      foutsol << " " << elapsed << "\n";

      foutsol.close();
    }



  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
