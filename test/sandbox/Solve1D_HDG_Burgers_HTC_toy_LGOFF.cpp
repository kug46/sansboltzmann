// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_HDG_Burgers_HTC_btest
// testing of 1-D HDG with Burgers solved with HTC

//#define SANS_VERBOSE

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/output_Tecplot.h"

#include "pde/BCParameters.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/ForcingFunction_MMS.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

//#define BCPARAMETERS_INSTANTIATE
//#include "pde/BCParameters_impl.h" // HACK: I need to work out how to sort out homotopy based stuff

#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE // HACK: I need to work out how to sort out homotopy based stuff
#include "Discretization/HDG/AlgebraicEquationSet_HDG_impl.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_Dispatch_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_HDG.h"


#include "SolutionTrek/Continuation/Homotopy/Homotopy.h"

#include "Meshing/XField1D/XField1D.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_HDG_Burgers_HTC_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AD_Solve )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > PrimaryPDE;
  typedef PDENDConvertSpace<PhysD1, PrimaryPDE> NDPrimaryPDE;
  typedef PDEAdvectionDiffusion<PhysD1,
                                LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD1, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef ScalarFunction1D_Tanh SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCAdvectionDiffusion<PhysD1,BCType> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  typedef BCHomotopy<BCClass, BCClass> NDBC;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertHomotopy, boost::mpl::vector<NDBC>,
                                   AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> AlgebraicEquationSet_HDGClass;
  typedef AlgebraicEquationSet_HDGClass::BCParams BCParams;

  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  Real nu = 0.005;
  Real b =  0.2;

  SolutionExact soln(nu);
  NDSolutionExact solnExact(nu);

  // Generate PDEs
  LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers> adv;
  ViscousFlux1D_Uniform visc(nu);
  SolutionExact MMS_soln(nu);
//  ForcingFunction1D_MMS<SolutionExact> forcing(MMS_soln);
  Source1D_Uniform source(b);

  typedef ForcingFunction1D_MMS<PrimaryPDE, SolutionExact> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(MMS_soln) );

  typedef ForcingFunction1D_MMS<AuxiliaryPDE, SolutionExact> ForcingAuxType;
  std::shared_ptr<ForcingAuxType> forcingauxptr( new ForcingAuxType(MMS_soln) );

  Real largeNu = 1.0;
  ViscousFlux1D_Uniform auxVisc(largeNu);

  NDPrimaryPDE primarypde(adv, visc, source, forcingptr);
  NDAuxiliaryPDE auxiliarypde(adv, auxVisc, source, forcingauxptr);

  // Set up our Homotopy pde
  Real lambda = 0.0;
  NDPDEClass pde(lambda, primarypde, auxiliarypde);

  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(nu) );
  BCClass bc( solnExactPtr, visc );
  // Create a Solution dictionary
  PyDict SolnDict;
  SolnDict["nu"] = nu;
  // Create a BC dictionary
  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln["SolutionArgs"] = SolnDict;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  const std::vector<int> BoundaryGroups = {0,1};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef AlgebraicEquationSet_HDGClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  typedef IntegrandCell_HDG<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, BCClass, BCClass::Category, HDG> IntegrandBoundClass;
  typedef IntegrandCell_SquareError<NDPDEClass,NDSolutionExact> IntegrandSquareErrorClass;

  // Grid
  XField1D xfld( 9, -1, 1 );
  int order = 0;
  // HDG solution field
  // cell solution
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufld(xfld, order, BasisFunctionCategory_Legendre);
  // auxiliary variable
  Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  // interface solution
  Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> uIfld(xfld, order, BasisFunctionCategory_Legendre);
  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroups);

  lgfld = 0;

  const int nDOFPDE = ufld.nDOF();
  const int nDOFAu  = qfld.nDOF();
  const int nDOFInt = uIfld.nDOF();
  const int nDOFBC  = lgfld.nDOF();

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnInt( pde, disc, {0} );
  IntegrandBoundClass fcnBC( pde, bc, BoundaryGroups, disc );
  IntegrandSquareErrorClass fcnErr( solnExact, {0} );

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-12, 1e-12, 1e-12, 1e-12};

  // Create the spatial discretization
  AlgebraicEquationSet_HDGClass AlgEqSet(xfld, ufld, qfld, uIfld, lgfld, pde, disc, quadratureOrder, tol,
                                         {0}, {0}, PyBCList, BCBoundaryGroups, lambda);

  // residual vectors
  SystemVectorClass q0(AlgEqSet.vectorStateSize());
  SystemVectorClass sln(AlgEqSet.vectorStateSize());
  SystemVectorClass rsd(sln.size());

  // set initial condition
  q0 = 0.0;
  sln = 0.0;

  // Create the Homotopy continuation class
  Homotopy<PhysD1, NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>>
     HTC(xfld, ufld, NonLinearSolverDict, pde, {0}, AlgEqSet, lambda);

  // Iterate the homotopy to convergence
  bool converged;
  BOOST_CHECK( converged = HTC.iterate(300) );

  // Did we get to zero dissipation
  BOOST_CHECK_CLOSE( 1.0, lambda, 1e-12 );

  AlgEqSet.fillSystemVector(sln);

  // quadrature rule
  int quadratureOrder[2] = {-1, -1};    // max
  int quadratureOrderTrace[2] = {0, 0};     // quadrature order for a node

  // check that the residual is zero
  rsd = 0;
  IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsd(0), rsd(1)),
                                          xfld, (ufld, qfld), quadratureOrder, 1 );
  IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(0), rsd(1), rsd(2)),
                                                          xfld, (ufld, qfld), uIfld, quadratureOrderTrace, 1);
  IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsd(0), rsd(1), rsd(3)),
                                                      xfld, (ufld, qfld), lgfld, quadratureOrderTrace, 2 );

  Real rsdPDEnrm = 0;
  for (int n = 0; n < nDOFPDE; n++)
    rsdPDEnrm += pow(rsd[0][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1.1e-12 );

  Real rsdAunrm = 0;
  for (int n = 0; n < nDOFAu; n++)
    rsdAunrm += pow(rsd[1][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdAunrm), 1.1e-12 );

  Real rsdIntnrm = 0;
  for (int n = 0; n < nDOFInt; n++)
    rsdIntnrm += pow(rsd[2][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdIntnrm), 1.1e-12 );

  Real rsdBCnrm = 0;
  for (int n = 0; n < nDOFBC; n++)
    rsdBCnrm += pow(rsd[3][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1.1e-12 );

#ifdef SANS_VERBOSE
  // L2 solution error
  int quadratureOrder[2] = {-1, -1};    // max
  ArrayQ SquareError = 0;
  FunctionalCell<TopoD1>::integrate( fcnErr, ufld, quadratureOrder, 1, SquareError );
  Real norm = SquareError;

  if (converged)
  {
    std::cout << "Converged   ";
  }
  else
  {
    std::cout << "Unconverged ";
  }
  std::cout << "P = " << order << ": L2 solution error = " << sqrt( norm );
//  if (indx > 1)
//    std::cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
  std::cout << std::endl;
#endif

#ifdef SANS_VERBOSE
  // Tecplot dump
  std::string filename = "tmp/slnHDG.plt";
  output_Tecplot( ufld, filename );
#endif
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
