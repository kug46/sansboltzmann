// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_DGBR2_Lorenz_toy
// testing of 1-D DG with Lorenz

//#define SANS_FULLTEST
//#define SANS_VERBOSE
// #define USE_VMSD
// #define USE_VMSDBR2
// #define USE_DGBR2
#define USE_DGA

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include "SANS_btest.h"

#include <boost/mpl/vector_c.hpp>

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#ifdef USE_DGBR2
// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"
#elif defined(USE_DGA)
// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"
#endif
// #define BCPARAMETERS_INSTANTIATE
// #include "pde/BCParameters_impl.h" // HACK: I need to work out how to sort out homotopy based stuff

#include "pde/Lorenz/PDELorenz.h"
#include "pde/Lorenz/BCLorenz.h"
#include "pde/Lorenz/OutputLorenz.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/VectorFunction1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#if defined(USE_VMSD)
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"
#include "Adaptation/MOESS/SolverInterface_VMSD.h"
#elif defined(USE_VMSDBR2)
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "Discretization/VMSDBR2/SolutionData_VMSD_BR2.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_VMSD_BR2.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_Local_VMSD_BR2.h"
#include "Adaptation/MOESS/SolverInterface_VMSD_BR2.h"
#elif defined(USE_DGBR2)
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"
#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#elif defined(USE_DGA)
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/DG/SolutionData_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"
#include "Adaptation/MOESS/SolverInterface_DGAdvective.h"
#else
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#endif

#include "Discretization/JacobianParam.h"
#include "Discretization/Block/JacobianParam_Decoupled.h"
#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"

#include "Discretization/DiscretizationObject.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
// #include "Field/FieldLiftLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_EG_Cell.h"

#include "SolutionTrek/Continuation/Homotopy/Homotopy.h"

#include "Meshing/XField1D/XField1D.h"

#include "Field/output_gnuplot.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

using namespace std;

namespace SANS
{

}

using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGBR2_Lorenz_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGBR2_Lorenz )
{
  typedef PhysD1 PhysDim;
//  typedef TopoD1 TopoDim;

  typedef PDELorenz PDEPrimaryClass;
  typedef PDELorenz PDEAuxiliaryClass;

  typedef PDENDConvertSpace<PhysDim, PDEPrimaryClass> NDPDEPrimaryClass;
  typedef PDENDConvertSpace<PhysDim, PDEAuxiliaryClass> NDPDEAuxiliaryClass;
  typedef NDPDEPrimaryClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEPrimaryClass::VectorArrayQ<Real> VectorArrayQ;
  // typedef NDPDEAuxiliaryClass::ArrayQ<Real> ArrayQ_aux;
  // typedef NDPDEAuxiliaryClass::VectorArrayQ<Real> VectorArrayQ_aux;

//  typedef DLA::MatrixS<PDEPrimaryClass::N, 1, Real> MatrixParam;
//  typedef DLA::MatrixS<1, PDEPrimaryClass::N, Real> MatrixTransposeParam;

  typedef PDEHomotopy<NDPDEPrimaryClass, NDPDEAuxiliaryClass> NDHPDEClass;
//  typedef NDHPDEClass::ArrayQ<Real> ArrayHQ;
//  typedef NDHPDEClass::VectorArrayQ<Real> VectorArrayHQ;

  //typedef BCLorenzVector<PDEPrimaryClass> BCVector;

  //typedef BCParameters<BCVector> BCParams;

  /////////////////////////////////////////////////////////////////////////
  // Not sure yet how this factors into our source layout
  /////////////////////////////////////////////////////////////////////////
  typedef BCNone<PhysD1, LorenzTraits::N> BCClassRawNone;
  typedef BCNDConvertSpace<PhysD1, BCClassRawNone> BCClassNone;

  typedef BCLorenz<PhysD1,BCTypeDirichlet_mitState> BCClassRawDirichlet;
  typedef BCNDConvertSpace<PhysD1, BCClassRawDirichlet> BCClassDirichlet;

  typedef BCHomotopy<NDPDEPrimaryClass, NDPDEAuxiliaryClass, BCClassNone, BCClassNone> NDBCNone;
  typedef BCHomotopy<NDPDEPrimaryClass, NDPDEAuxiliaryClass, BCClassDirichlet, BCClassDirichlet> NDBCDirichlet;
  typedef boost::mpl::vector2<NDBCNone, NDBCDirichlet> BCVector;
  /////////////////////////////////////////////////////////////////////////

  // typedef BCLorenz<PhysD1, BCTypeDirichlet_mitState, PDEPrimaryClass> BCClassRaw0;
  // typedef BCNone<PhysD1, LorenzTraits::N> BCClassRaw1;
  // typedef BCNDConvertSpace<PhysD1, BCClassRaw0> BCClass0;
  // typedef BCNDConvertSpace<PhysD1, BCClassRaw1> BCClass1;
  //
  // typedef BCHomotopy<BCTypeDirichlet_mitState, BCClass0, BCClass0> NDHBC0;
  // typedef BCHomotopy<BCTypeDirichlet_mitState, BCClass1, BCClass1> NDHBC1;

//  typedef ScalarFunction1D_ZeroLorenz Solution1Exact;
//  typedef ScalarFunction1D_ZeroLorenz Solution2Exact;
//  typedef ScalarFunction1D_ZeroLorenz Solution3Exact;
//  typedef Vector3Function1D<Solution1Exact, Solution2Exact, Solution3Exact> SolutionExact;
//  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  // typedef AlgebraicEquationSet_DGAdvective<NDHPDEClass, BCNDConvertSpace,
  //     BCVector, AlgEqSetTraits_Sparse, DGAdv, XField<PhysD1, TopoD1>> AlgebraicEquationSetClass;
  typedef AlgebraicEquationSet_DGAdvective<NDHPDEClass, BCNDConvertHomotopy,
      BCVector, AlgEqSetTraits_Sparse, DGAdv, XField<PhysD1, TopoD1>> AlgebraicEquationSetClass;
  typedef AlgebraicEquationSetClass::BCParams BCParams;

  PyDict NonLinearSolverDict;
  PyDict NewtonSolverDict;
  PyDict UMFPACKDict;
  PyDict NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver]= SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method]= LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver]= NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver]= UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate]= NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations]= 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations]= 100;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]= NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  // SPECIFICATIONS

  // source term constants
  // const Real alpha0= 10.; // sigma
  // const Real alpha1= 0.5; // rho
  // const Real alpha2= 8./3.; // beta
  // const Real alpha0= 10.; // sigma
  // const Real alpha1= 28.; // rho
  // const Real alpha2= 8./3.; // beta
  const Real alpha0= 0.; // sigma
  const Real alpha1= 0.; // rho
  const Real alpha2= 0.; // beta

  // initial conditions (as influx BC)
  const Real q0B= 1.;
  const Real q1B= 1.;
  const Real q2B= 1.;

  // temporal domain
  const Real xL= 0;
  const Real xR= 100;

//  // discretization settings
//  const int maxNewtonIter= 50;
//
//  // orders to cycle through
//  const int ordermin= 1; // 0; // 1;
//  const int ordermax= 3; // 0; // 1;
//
//  // grid resolution factors to cycle through
//  const int factormin= 3;
//  const int factormax= 6;

  // GENERATE PHYSICS

  // create PDE class
  NDPDEPrimaryClass pdeP(alpha0, alpha1, alpha2);
  NDPDEAuxiliaryClass pdeA(0.5, 0.5, 0.5); // known not to be chaotic

  // set up homotopy PDE
  Real lambda= 0.0;
  NDHPDEClass pde(lambda, pdeP, pdeA);

  // boundary conditions
  const ArrayQ qB= {q0B, q1B, q2B};
  // BCClassDirichlet bc0(pdeP, qB);
  // BCClassNone bc1();

  // none BC
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType]= BCParams::params.BC.None;

  // dirichlet BC
  PyDict BCDirichlet_mitState;
  BCDirichlet_mitState[BCParams::params.BC.BCType]=
      BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet_mitState[BCLorenzParams<PhysDim,
      BCTypeDirichlet_mitStateParam>::params.q0B]= q0B;
  BCDirichlet_mitState[BCLorenzParams<PhysDim,
      BCTypeDirichlet_mitStateParam>::params.q1B]= q1B;
  BCDirichlet_mitState[BCLorenzParams<PhysDim,
      BCTypeDirichlet_mitStateParam>::params.q2B]= q2B;

  // create BC list
  PyDict BCList;
  BCList["BCNone"]= BCNone;
  BCList["BCDirichlet_mitState"]= BCDirichlet_mitState;

  // define the boundarygroups for each boundary condition
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["BCNone"]= {1}; // right/temporal outflow
  BCBoundaryGroups["BCDirichlet_mitState"]= {0}; // left/temporal inflow

  // check the BC dictionary
  BCParams::checkInputs(BCList);

  // grid
  const int ii= 10;
  XField1D xfld(ii, xL, xR);
  int order= 1;

  // boundaries and elements to include (all)
  std::vector<int> cellGroups= {0};

  std::vector<int> active_boundaries=
      BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups);

  std::vector<int> interiorTraceGroups;
  for (int i= 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

//  typedef AlgebraicEquationSetClass::SystemMatrix SystemMatrix00Class;

  QuadratureOrder quadOrder(xfld, -1);
  std::vector<Real> tol= {1.0e-12, 1.0e-12};

  // cell solution
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  // // auxiliary variable
  // Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> gfld(xfld, order, BasisFunctionCategory_Legendre);
  // // interface solution
  // Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> uIfld(xfld, order, BasisFunctionCategory_Legendre);
  // Lagrange multiplier
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, active_boundaries);

  // create the discretization
  AlgebraicEquationSetClass AlgEqSet(xfld, qfld, lgfld, pde, quadOrder,
        ResidualNorm_L2, tol, cellGroups, interiorTraceGroups, BCList,
        BCBoundaryGroups, lambda);

  std::cout << "\n\ngot here!!! bout to run homotopy\n" << std::endl;

  Homotopy<PhysD1, NDHPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>>
      HTC(xfld, qfld, NonLinearSolverDict, pde, cellGroups, AlgEqSet, lambda);

  // iterate the homotopy to convergence
  bool converged;
  BOOST_CHECK(converged= HTC.iterate(300));

  // // did we converge to the chaotic system?
  // BOOST_CHECK_CLOSE(lambda, 1.0, 1.0e-12);
  //
  //
  // // ...

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
