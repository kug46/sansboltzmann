// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

// #define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/SolutionFunction_NavierStokes3D.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/ForcingFunction3D_MMS.h"
#include "pde/BCParameters.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/BCParameters.h"

#include "Discretization/VMSD/FunctionalCell_VMSD.h"
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"

#include "Discretization/VMSD/SolutionData_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"
#include "Adaptation/MOESS/SolverInterface_VMSD.h"

#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_EG_Cell.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#endif

#include "tools/timer.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_VMSD_NS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve3D_VMSD_NS_TripleBoundaryLayer )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypeEntropy QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;

  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_NavierStokes3D_OjedaMMS<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolnType;
  typedef SolnNDConvertSpace<PhysD3,SolnType> NDSolnType;
  typedef ForcingFunction3D_MMS<PDEClass> ForcingType;

  typedef OutputEuler3D_TotalEnthalpy<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_VMSD_Output<NDOutputClass, NDPDEClass> IntegrandOutputClass;

  typedef BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_VMSD<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass, IntegrandOutputClass> SolverInterfaceClass;

  mpi::communicator world;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  GasModel gas(gamma, R);
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // INFLOW CONDITIONS TO MATCH OJEDA'S MMS
  Real rho0 = 1.0;
  Real u0 = 0.2;
  Real v0 = 0.1;
  Real w0 = 0.15;
  Real p0 = 1.0;
  Real t0 = p0/rho0/R;

  const Real ERef = Cv*t0 + 0.5*(u0*u0 + v0*v0 + w0*w0);

//  const Real Re = 1000;
  const Real muRef = 0.02; // Re_L = 10ish
  const Real Prandtl = 0.72;

  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDSolnType soln(gas);
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(soln) );

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, forcingptr );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure3D<Real>(rho0, u0, v0, w0, p0) );


  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-10, 1e-10, 1e-10};

  //  Total enthalpy integral
  NDOutputClass fcnOutput(pde);

  // Stabilization and outputs
  DiscretizationVMSD stab;

  IntegrandOutputClass fcnErr( pde, fcnOutput, {0} );
  Real trueOutput = 3.53812899678627234;

  typedef OutputCell_SolutionErrorSquared<NDPDEClass, NDSolnType> L2ErrorClass;
  typedef OutputNDConvertSpace<PhysD3, L2ErrorClass> NDL2ErrorClass;
  typedef IntegrandCell_VMSD_Output<NDL2ErrorClass, NDPDEClass> L2ErrorIntegrandClass;

  NDL2ErrorClass fcnL2Err(soln);
  L2ErrorIntegrandClass errorL2Integrand(pde, fcnL2Err, {0}, true);

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  PyDict StateVector;
  StateVector[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  StateVector[Conservative3DParams::params.rho] = rho0;
  StateVector[Conservative3DParams::params.rhou] = rho0*u0;
  StateVector[Conservative3DParams::params.rhov] = rho0*v0;
  StateVector[Conservative3DParams::params.rhow] = rho0*w0;
  StateVector[Conservative3DParams::params.rhoE] = rho0*ERef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler3DFullStateParams<NSVariableType3DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler3DFullStateParams<NSVariableType3DParams>::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;
  BCBoundaryGroups["BCIn"]  = {0,1,2,3,4,5};

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 )
      std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-16;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 300;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 30;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.DGMRES;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 500;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-12;
  PETScDict_adjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-16;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-12;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 10;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::string filename_base = "tmp/OJEDA3D/VMSD/";

  fstream foutsol;

  boost::filesystem::path base_dir(filename_base);
  if ( not boost::filesystem::exists(base_dir) )
    boost::filesystem::create_directories(base_dir);

  int orderL = 1, orderH = 3;
  int powerL = 2, powerH = 6;


#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    orderL = orderH = std::stoi(argv[1]);
  if (argc >= 3)
    powerL = powerH = std::stoi(argv[2]);

  if (world.rank() == 0 )
  {
    std::cout << ", orderL,H = " << orderL << ", " << orderH;
    std::cout << ", powerL,H = " << powerL << ", " << powerH << std::endl;
  }
#endif

  //--------ADAPTATION LOOP--------
  for (int order = orderL; order <= orderH; order++)
  {
    stab.setNitscheOrder(order);

    filename_base = "tmp/OJEDA3D/VMSD/VMSD_";
    filename_base += "P" + std::to_string(order) + "_";


    for (int power = powerL; power <= powerH; power++ )
    {

      std::string output_filename = filename_base + "output.dat";
      if ( world.rank() == 0 )
      {
        foutsol.open( output_filename, fstream::app );
        BOOST_REQUIRE_MESSAGE(foutsol.good(), "Error opening file: " + output_filename);
      }

      int nk = power;

      // Grid
      int ii = nk;
      int jj = nk;
      int kk = nk;

      std::shared_ptr<XField<PhysD3, TopoD3>> pxfld = std::make_shared<XField3D_Box_Tet_X1>( world, ii, jj, kk ); //pts, pts, pts );

      std::vector<int> cellGroups = {0};

      const int quadOrder = 2*(order + 1);

      //const Real iCost = pInterface->getCost();

      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, stab, order, order, order+1, order+1,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   active_boundaries);

      //Perform L2 projection from solution on previous mesh
      pGlobalSol->setSolution(q0);

      //       Set the initial condition
      for_each_CellGroup<TopoD3>::apply( InterpolateFunctionCell_Continuous(soln, {0}), (*pxfld, pGlobalSol->primal.qfld) );

//      output_Tecplot(pGlobalSol->primal.qfld, "tmp/init.plt");

      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          fcnErr);

      timer tt;
      pInterface->solveGlobalPrimalProblem();
      Real solveTime = tt.elapsed();


      std::string qfld_filename = filename_base + "X" + std::to_string(nk) + "_qfld.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

//      pInterface->solveGlobalAdjointProblem();
//
//      std::string adjfld_filename = filename_base + "X" + std::to_string(nk) + "_adjfld.plt";
//      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

#ifdef SANS_MPI
      int nDOFtotal = 0;
      boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );

      // count the number of elements possessed by this processor
      // int nElem = 0;
      // for (int elem = 0; elem < pxfld->nElem(); elem++ )
      //   if (pxfld->getCellGroupGlobal<Tet>(0).associativity(elem).rank() == world.rank())
      //     nElem++;

      // int nElemtotal = 0;
      // boost::mpi::reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>(), 0 );
#else
      int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
      // int nElemtotal = pxfld->nElem();
#endif

      //Compute error estimates
//      pInterface->computeErrorEstimates();

      Real u2 = pInterface->getOutput();
//      Real globalEstimate  = pInterface->getGlobalErrorEstimate();

//      Real errEstErr = abs(u2-trueOutput - globalEstimate);

      QuadratureOrder quadrule(*pxfld, quadOrder);

      ArrayQ L2error = 0;
      IntegrateCellGroups<TopoD3>::integrate( FunctionalCell_VMSD( errorL2Integrand, L2error ),
                                              *pxfld, (pGlobalSol->primal.qfld, pGlobalSol->primal.qpfld),
                                              quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#ifdef SANS_MPI
      L2error = boost::mpi::all_reduce( *pxfld->comm(), L2error, std::plus<ArrayQ>() );
#endif
      for (int i=0; i<pde.N; i++)
        L2error[i] = sqrt(L2error[i]);

      if (world.rank() == 0 )
      {
        foutsol << order << " " << to_string(nk) << " " << nDOFtotal << std::setprecision(15);
        foutsol << " " << L2error[0];
        foutsol << " " << L2error[1];
        foutsol << " " << L2error[2];
        foutsol << " " << L2error[3];
        foutsol << " " << L2error[4];
        foutsol << " " << fabs(u2-trueOutput);
//        foutsol << " " << globalEstimate;
        foutsol << " " << solveTime << "\n";

        foutsol.close();
      }

    }
    //timehist.close();
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
