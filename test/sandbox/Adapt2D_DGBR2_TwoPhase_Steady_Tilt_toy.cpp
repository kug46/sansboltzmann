// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_TwoPhase_Steady_QuarterFiveSpot_btest
// Testing of the MOESS framework on a space-time two-phase problem

#define USE_AV

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"

#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/SolutionFunction_TwoPhase1D.h"
#include "pde/PorousMedia/OutputTwoPhase.h"

#ifdef USE_AV
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity2D.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#else
#include "pde/PorousMedia/PDETwoPhase2D.h"
#include "pde/PorousMedia/BCTwoPhase2D.h"
#endif

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"

#include "Field/output_Tecplot_PDE.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_Lagrange_X1.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif


using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_TwoPhase_Steady_QuarterFiveSpot_test_suite )

/* IMPORTANT:
 * If running a single phase case, make sure to turn on the "additional viscosity" term in PDETwoPhase */

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_TwoPhase_Steady_QuarterFiveSpot_Triangle )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

#ifdef USE_AV
  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,TraitsModelClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

  typedef BCTwoPhaseArtificialViscosity2DVector<TraitsSizeTwoPhaseArtificialViscosity,
                                                TraitsModelAV> BCVector;

  typedef ParamType_GenH_DG ParamBuilderType;
  typedef GenHField_DG<PhysD2, TopoD2> GenHFieldType;

  typedef Q2D<QType, CapillaryModel, TraitsSizeTwoPhaseArtificialViscosity> QInterpreter;
  typedef SolutionFunction_TwoPhase2D_Reservoir<QInterpreter, TraitsSizeTwoPhaseArtificialViscosity> SolutionType;

#else
  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef BCTwoPhase2DVector<PDEClass> BCVector;

  typedef ParamType_None ParamBuilderType;

  typedef Q2D<QType, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;
  typedef SolutionFunction_TwoPhase2D_Reservoir<QInterpreter, TraitsSizeTwoPhase> SolutionType;
#endif

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCParameters<BCVector> BCParams;
  typedef OutputTwoPhase_SaturationPower<PDEClass> OutputClass;
//  typedef OutputTwoPhase_PressurePower<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
  

  typedef SolnNDConvertSpace<PhysD2,SolutionType> NDSolutionType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  timer clock;

  mpi::communicator world;

  // PARAMS
  bool grav_on = true;
  double pc_max = 0.0;
  double nu_global = 0.0;
  bool wells_on = true;
  Real is_comp = 1.0;
  bool PTC_on = true;
  bool read_resid = false;
  
  const Real T = 50.0; //days
  const Real Lx = 1000.0; //ft
  const Real Ly = Lx; //ft

  int ii = 15;
  int jj = 15;

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;
  PyDict MesherDict;

#ifdef SANS_AVRO
  std::shared_ptr<avro::Context> context;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;
  MesherDict[avroParams::params.Curved] = false;

  // create the context
  context = std::make_shared<avro::Context>();

  using avro::coord_t;
  using avro::index_t;

  Real xc[3] = {Lx/2, Ly/2, 0.};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( context.get() , xc , Lx , Ly );
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model> ( context.get(), "model" );
  model->addBody(pbody,true);

  XField2D_Box_Triangle_Lagrange_X1 xfld0( world, ii, jj, 0, Lx, 0, Ly );
  // copy the mesh into the domain and attach the geometry
  pxfld = std::make_shared< XField_avro<PhysD2,TopoD2> >(xfld0, model);
#else
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;
  pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj,  0, Lx, 0, Ly );
#endif

  const int iB = XField2D_Box_Triangle_Lagrange_X1::iBottom;
  const int iR = XField2D_Box_Triangle_Lagrange_X1::iRight;
  const int iT = XField2D_Box_Triangle_Lagrange_X1::iTop;
  const int iL = XField2D_Box_Triangle_Lagrange_X1::iLeft;

  int order = 1;

  if (read_resid)
  {
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> Vfld (*pxfld, order, BasisFunctionCategory_Legendre);

    std::fstream fVmin("tmp/rsd_plain_iter0.dat", std::fstream::in);

    for (int i = 0; i < Vfld.nDOF(); i++)
    {
      fVmin  >> Vfld.DOF(i)[0];
      fVmin  >> Vfld.DOF(i)[1];
    }
    fVmin .close();

    output_Tecplot( Vfld, "tmp/resid_fld.plt" );
    return;
  }

  // PDE
  const Real pref = 14.7;

  Real rhow_ref = 62.4;
  Real rhon_ref = 52.1;
  DensityModel rhow(rhow_ref, is_comp*5.0e-6, pref);
  DensityModel rhon(rhon_ref, is_comp*1.5e-5, pref);

  PorosityModel phi(0.3, is_comp*3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200;
  const Real Kxx = Kref;
  const Real Kyy = Kref;

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kxx, 0.0}, {0.0, Kyy}};
  RockPermModel K(Kref_mat);

  CapillaryModel pc(pc_max);

  // Set up source term PyDicts
  Real R_bore = 1.0/6.0; //ft
  Real R_well = 100.0; //ft
  Real pB = 2000.0; //psi
  int nParam = 6;

  if (world.rank() == 0)
  {
    std::cout << "rw: " << R_bore << " ft" << std::endl;
    std::cout << "R: " << R_well << " ft" << std::endl;
    std::cout << "pB: " << pB << " psi" << std::endl;
    std::cout << "nParam: " << nParam << std::endl;
  }

  // PyDict well_in;
  // well_in[SourceTwoPhase2DType_FixedInflowRate_Param::params.Q] = 0.1; // [1/day]
  // well_in[SourceTwoPhase2DType_FixedInflowRate_Param::params.pn] = 2500.0;
  // well_in[SourceTwoPhase2DType_FixedInflowRate_Param::params.Sw] = 1.0;
  // well_in[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedInflowRate;

  // PyDict well_out;
  // well_out[SourceTwoPhase2DType_FixedOutflowRate_Param::params.Q] = 0.1; // [1/day]
  // well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedOutflowRate;

  double p_well_depth = 2319; //psi
  double dp = 30.0; //psi

  PyDict well_in;
  well_in[SourceTwoPhase2DType_FixedPressureInflow_Param::params.pB] = p_well_depth + 0.5*dp;
  well_in[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Sw] = 1.0;
  well_in[SourceTwoPhase2DType_FixedPressureInflow_Param::params.Rwellbore] = R_bore;
  well_in[SourceTwoPhase2DType_FixedPressureInflow_Param::params.nParam] = nParam;
  well_in[SourceTwoPhase2DType_FixedPressureInflow_Param::params.WellModel] =
    SourceTwoPhase2DType_FixedPressureInflow_Param::params.WellModel.Polynomial;
  well_in[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureInflow;

  PyDict well_out;
  typedef SourceTwoPhase2DType_FixedPressureOutflow_Param SourceParamClass;
  well_out[SourceParamClass::params.pB] = p_well_depth - 0.5*dp;
  well_out[SourceParamClass::params.Rwellbore] = R_bore;
  well_out[SourceParamClass::params.nParam] = nParam;
  well_out[SourceParamClass::params.WellModel] = SourceParamClass::params.WellModel.Polynomial;
  well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

  PyDict source_injection;
  source_injection[SourceTwoPhase2DParam::params.Source] = well_in;
  source_injection[SourceTwoPhase2DParam::params.xcentroid] = 200.0;
  source_injection[SourceTwoPhase2DParam::params.ycentroid] = 200.0;
  source_injection[SourceTwoPhase2DParam::params.R] = R_well;
  source_injection[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_injection[SourceTwoPhase2DParam::params.Tmax] = T;
  source_injection[SourceTwoPhase2DParam::params.smoothLr] = 0.2*R_well;
  source_injection[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_injection[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_production;
  source_production[SourceTwoPhase2DParam::params.Source] = well_out;
  source_production[SourceTwoPhase2DParam::params.xcentroid] = Lx-200.0;
  source_production[SourceTwoPhase2DParam::params.ycentroid] = 200.0;
  source_production[SourceTwoPhase2DParam::params.R] = R_well;
  source_production[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_production[SourceTwoPhase2DParam::params.Tmax] = T;
  source_production[SourceTwoPhase2DParam::params.smoothLr] = 0.0;
  source_production[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_production[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_list;
  if (wells_on)
  {
    source_list["injection_well"] = source_injection;
    source_list["production_well"] = source_production;
  }

  SourceTwoPhase2DListParam::checkInputs(source_list);

#ifdef USE_AV
  // TwoPhase PDE with AV fluxes
  const bool hasSpaceTimeDiffusionTwoPhase = false;
  PDEBaseClass pdeTwoPhaseAV(order, hasSpaceTimeDiffusionTwoPhase, rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list, nu_global, grav_on);

  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  const bool hasSpaceTimeDiffusionAV = false;
  const Real diffusionConstantAV = 3.0;
  SensorViscousFlux sensor_visc(order, hasSpaceTimeDiffusionAV, 1.0, diffusionConstantAV);
  const Real Sk_min = -3;
  const Real Sk_max = -1;
  SensorSource sensor_source(pdeTwoPhaseAV, Sk_min, Sk_max);

  // Augmented PDE with nu-equation
  bool isSteadyAV = true;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteadyAV,
                 order, hasSpaceTimeDiffusionTwoPhase, rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list, nu_global, grav_on);

  typedef BCmitAVSensor2DParams<BCTypeFlux_mitState, BCTwoPhase2DParams<BCTypeNoFlux>> BCParamsNoFlux;

#else
  NDPDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list, nu_global, grav_on);
#endif

  // Initial solution
  PyDict initSoln;
  initSoln[SolutionType::ParamsType::params.Ly] = Ly;
  initSoln[SolutionType::ParamsType::params.y_owc] = 0.6*Ly;
  initSoln[SolutionType::ParamsType::params.rho_o] = rhon_ref;
  initSoln[SolutionType::ParamsType::params.rho_w] = rhow_ref;
  initSoln[SolutionType::ParamsType::params.p_offset] = 2000;
  initSoln[SolutionType::ParamsType::params.Sw_bottom] = 1.0;
  initSoln[SolutionType::ParamsType::params.Sw_top] = 0.0;
  NDSolutionType NDinitSolution(initSoln, pde.variableInterpreter());

  // BCs
  PyDict BCNoFlux;
  BCNoFlux[BCParams::params.BC.BCType] = BCParams::params.BC.NoFlux;
  BCNoFlux[BCParamsNoFlux::params.Cdiff] = diffusionConstantAV;

  PyDict PyBCList;
  PyBCList["BCNoFlux"] = BCNoFlux;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoFlux"] = {iB,iL,iT,iR};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {5e-7, 5e-7};

  //Output functional
  const int phase = 0, exponent = 2;
  NDOutputClass fcnOutput(pde, phase, exponent, (1.0/(Lx*Ly)));
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict, UMFPACKDict;

#if defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)/2; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)/2; //elemDOF for p=order+1
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

  if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
#else
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  if (world.rank() == 0) std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = (PTC_on) ? 1 : 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

// Choose PTC or steady
  if (PTC_on)
  {
    NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
    NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
    NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
    NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 200;
    NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 1;
//    NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 1.0e-2;
  }
  else
  {
    NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  }

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  //--------ADAPTATION LOOP--------

  int maxIter = 20;
  Real targetCost = 20000;

  std::string filename_base = "tmp/";
//  std::string filename_base = "tmp/twophase_steady/Q1000/L1000_Kx200_Ky100/polynomial_d2fnonzero/adapted/poly_m"
//                              + to_string(nParam) + "_R" + to_string((int)R_well) + "_P" + to_string(order) + "_"
//                              + to_string((int) targetCost/1000) + "k/";

  fstream fadapthist;
  if (world.rank() == 0)
  {
    std::string adapthist_filename = filename_base + "test.adapthist";
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = true;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.FrobNorm;
//  MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpMetric] = false;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  std::shared_ptr<SolutionClass> pGlobalSol;

#ifdef USE_AV
  //Compute generalized log H-tensor field
  std::shared_ptr<GenHFieldType> pHfld = std::make_shared<GenHFieldType>(*pxfld);

  pGlobalSol = std::make_shared<SolutionClass>((*pHfld, *pxfld), pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);
  pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);
#else
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);
#endif

  const int quadOrder = 2*(order + 2);

  //Set initial solution
  pGlobalSol->setSolution(NDinitSolution, cellGroups);

  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, {"pn", "Sw", "nu"} );

#if 0
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  QuadratureOrder quadrule(*pxfld, quadOrder);

  PrimalEquationSetClass AlgEqSet(pGlobalSol->paramfld, pGlobalSol->primal, pGlobalSol->pliftedQuantityfld, pde, quadrule,
                                  ResNormType, tol, cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups);

  SystemVectorClass q(AlgEqSet.vectorStateSize());
  AlgEqSet.fillSystemVector(q);

  // jacobian nonzero pattern
  SystemNonZeroPattern nz(AlgEqSet.matrixSize());
  AlgEqSet.jacobian(q, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  jac = 0;
  AlgEqSet.jacobian(q, jac);

  fstream fout( "tmp/jac.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

  for (int iter = 0; iter < maxIter + 1; iter++)
  {
    if (world.rank() == 0)
      std::cout << "\n" << std::string(25,'-') + "Adaptation Iteration " << iter << std::string(25,'-') << "\n\n";

    //Create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, AdjLinearSolverDict,
                                                        outputIntegrand);
    pInterface->setBaseFilePath(filename_base);

    pInterface->solveGlobalPrimalProblem();
    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter) + ".plt";
//    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename, {"pn", "Sw", "nu"} );
    output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld, qfld_filename, {"pn", "Sw", "nu"});

    if (world.rank() == 0)
      std::cout << "Output: " << std::scientific << std::setprecision(12) << pInterface->getOutput() << std::endl;

    pInterface->solveGlobalAdjointProblem();
    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename, {"psiw", "psin", "psinu"} );

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
#ifdef USE_AV
    //Compute generalized log H-tensor field on new mesh
    pHfld = std::make_shared<GenHFieldType>(*pxfldNew);

    pGlobalSolNew = std::make_shared<SolutionClass>((*pHfld, *pxfldNew), pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);
    pGlobalSolNew->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);
#else
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);
#endif

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;

    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, {"pn", "Sw", "nu"} );

    if (world.rank() == 0)
      std::cout<<"Time elapsed: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl << std::endl;
  }

  if (world.rank() == 0)
    fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
