// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_HDG_Triangle_AD_STtrick_btest
// testing of 2-D HDG with Advection-Diffusion on triangles

#undef SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_HDG.h"

#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_HDG_Triangle_AD_STtrick_test_suite )


//----------------------------------------------------------------------------//
// solution: Hierarchical P1 thru P3
// Lagrange multiplier: continuous Hierarchical P1 thru P3
// exact solution: double BL
BOOST_AUTO_TEST_CASE( Solve_HDG_Triangle_AD )
{
  typedef SurrealS<1> SurrealClass;
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCTypeSoln;

  typedef BCNDConvertSpace<PhysD2,  BCAdvectionDiffusion<PhysD2,BCTypeSoln> > BCClassSoln;
  typedef BCNDConvertSpace<PhysD2,  BCAdvectionDiffusion<PhysD2,BCTypeNone> > BCClassNone;

  typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion2D::template MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  // PDE

  AdvectiveFlux2D_Uniform adv( -1.1, 1 );

  Real nu = 0.02;
  ViscousFlux2D_Uniform visc( nu, 0, 0, 0 );

  Source2D_None source;

  NDSolutionExact solnExact;
  //Real a0 = 1;
  //NDSolutionExact solnExact(a0);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  PDEClass pde( adv, visc, source, forcingptr );

  // BC
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact );
  BCClassSoln bcSoln(solnExactPtr, visc);

  BCClassNone bcNone;

//  Real A = 1;
//  Real B = 0;
//  Real bcdata = 0;
//  BCClass bc( A, B, bcdata );

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, BCClassSoln, BCClassSoln::Category, HDG> IntegrandBoundClassSoln;
  typedef IntegrandBoundaryTrace<PDEClass, BCClassNone, BCClassNone::Category, HDG> IntegrandBoundClassNone;

  typedef IntegrandCell_SquareError<PDEClass, NDSolutionExact> IntegrandSquareErrorClass;

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Global, Gradient );

  const std::vector<int> BoundaryGroupsNone = {2,3};
  const std::vector<int> BoundaryGroupsSoln = {0,1};

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnInt( pde, disc, {0,1,2} ); //UnionJack has 3 interior trace groups
  IntegrandBoundClassNone fcnBCNone( pde, bcNone, BoundaryGroupsNone, disc );
  IntegrandBoundClassSoln fcnBCSoln( pde, bcSoln, BoundaryGroupsSoln, disc );
  IntegrandSquareErrorClass fcnErr( solnExact, {0} );

  // norm data
//  Real hVec[10];
//  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  int ordermin = 1;
  int ordermax = 1;
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
    int powermax = 1;
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );
      jj = ii;

      // grid:

//      XField2D_Box_Triangle_X1 xfld( ii, jj );
      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

      BOOST_CHECK_EQUAL(1,1);

      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> ufld(xfld, order, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> uIfld(xfld, order, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroupsSoln);

      const int nDOFPDE = ufld.nDOF();
      const int nDOFAu  = qfld.nDOF()*PhysD2::D;
      const int nDOFInt = uIfld.nDOF();
      const int nDOFBC  = lgfld.nDOF();

      // quadrature rule
      int quadratureOrder[4] = {-1, -1, -1, -1};    // max
      int quadratureOrderMin[4] = {0, 0, 0, 0};     // min

      // linear system setup

      typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
      typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
      typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
      typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;


      // residual

      SystemVectorClass rsd = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};
      rsd = 0;

      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_HDG(fcnCell, rsd(0), rsd(1)),
                                              xfld, (ufld, qfld), quadratureOrder, 1 );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(0), rsd(1), rsd(2)),
                                                              xfld, (ufld, qfld), uIfld, quadratureOrder, 3);

      IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBCNone, rsd(0), rsd(1)),
                                                          xfld, (ufld, qfld), quadratureOrder, 4 );

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBCSoln, rsd(0), rsd(1), rsd(3)),
                                                          xfld, (ufld, qfld), lgfld, quadratureOrder, 4 );

       // jacobian nonzero pattern

      typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

      // jacobian nonzero pattern
      //
      //        u   q  uI  lg
      //   PDE  X   X   X   X
      //   Au   X   X   X   X
      //   Int  X   X   X   0
      //   BC   X   X   0   0

      //fld.SystemMatrixSize();
      DLA::MatrixD<NonZeroPatternClass> nz =
          {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFAu}, {nDOFPDE, nDOFInt}, {nDOFPDE, nDOFBC} },
           { {nDOFAu , nDOFPDE}, {nDOFAu , nDOFAu}, {nDOFAu , nDOFInt}, {nDOFAu , nDOFBC} },
           { {nDOFInt, nDOFPDE}, {nDOFInt, nDOFAu}, {nDOFInt, nDOFInt}, {nDOFInt, nDOFBC} },
           { {nDOFBC , nDOFPDE}, {nDOFBC , nDOFAu}, {nDOFBC , nDOFInt}, {nDOFBC , nDOFBC} }};

      NonZeroPatternClass& nzPDE_u  = nz(0,0);
      NonZeroPatternClass& nzPDE_q  = nz(0,1);
      NonZeroPatternClass& nzPDE_uI = nz(0,2);
      NonZeroPatternClass& nzPDE_lg = nz(0,3);

      NonZeroPatternClass& nzAu_u  = nz(1,0);
      NonZeroPatternClass& nzAu_q  = nz(1,1);
      NonZeroPatternClass& nzAu_uI = nz(1,2);
      NonZeroPatternClass& nzAu_lg = nz(1,3);

      NonZeroPatternClass& nzInt_u  = nz(2,0);
      NonZeroPatternClass& nzInt_q  = nz(2,1);
      NonZeroPatternClass& nzInt_uI = nz(2,2);
      //NonZeroPatternClass& nzInt_lg = nz(2,3);

      NonZeroPatternClass& nzBC_u  = nz(3,0);
      NonZeroPatternClass& nzBC_q  = nz(3,1);
      //NonZeroPatternClass& nzBC_uI = nz(3,2);
      NonZeroPatternClass& nzBC_lg = nz(3,3);

      IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nzPDE_u, nzPDE_q,
                                                                                      nzAu_u, nzAu_q),
                                              xfld, (ufld, qfld), quadratureOrderMin, 1 );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, nzPDE_u, nzPDE_q, nzPDE_uI,
                                                          nzAu_u ,          nzAu_uI ,
                                                          nzInt_u, nzInt_q, nzInt_uI),
          xfld, (ufld, qfld), uIfld, quadratureOrderMin, 3);

      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          JacobianBoundaryTrace_HDG<SurrealClass>(fcnBCNone, nzPDE_u, nzPDE_q,
                                                                  nzAu_u , nzAu_q),
          xfld, (ufld, qfld), quadratureOrderMin, 4);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBCSoln, nzPDE_u, nzPDE_q, nzPDE_lg,
                                                                nzAu_u , nzAu_q , nzAu_lg ,
                                                                nzBC_u , nzBC_q , nzBC_lg ),
          xfld, (ufld, qfld), lgfld, quadratureOrderMin, 4);

      // jacobian

      SystemMatrixClass jac(nz);

      SparseMatrixClass& jacPDE_u  = jac(0,0);
      SparseMatrixClass& jacPDE_q  = jac(0,1);
      SparseMatrixClass& jacPDE_uI = jac(0,2);
      SparseMatrixClass& jacPDE_lg = jac(0,3);

      SparseMatrixClass& jacAu_u  = jac(1,0);
      SparseMatrixClass& jacAu_q  = jac(1,1);
      SparseMatrixClass& jacAu_uI = jac(1,2);
      SparseMatrixClass& jacAu_lg = jac(1,3);

      SparseMatrixClass& jacInt_u  = jac(2,0);
      SparseMatrixClass& jacInt_q  = jac(2,1);
      SparseMatrixClass& jacInt_uI = jac(2,2);
      //SparseMatrixClass& jacInt_lg = jac(2,3);

      SparseMatrixClass& jacBC_u  = jac(3,0);
      SparseMatrixClass& jacBC_q  = jac(3,1);
      //SparseMatrixClass& jacBC_uI = jac(3,2);
      SparseMatrixClass& jacBC_lg = jac(3,3);

      jac = 0;

      IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacPDE_u, jacPDE_q,
                                                                                      jacAu_u, jacAu_q),
                                              xfld, (ufld, qfld), quadratureOrder, 1 );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacPDE_u, jacPDE_q, jacPDE_uI,
                                                          jacAu_u ,           jacAu_uI ,
                                                          jacInt_u, jacInt_q, jacInt_uI),
          xfld, (ufld, qfld), uIfld, quadratureOrder, 3);

      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          JacobianBoundaryTrace_HDG<SurrealClass>(fcnBCNone, jacPDE_u, jacPDE_q,
                                                                  jacAu_u , jacAu_q),
          xfld, (ufld, qfld), quadratureOrder, 4);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBCSoln, jacPDE_u, jacPDE_q, jacPDE_lg,
                                                                jacAu_u , jacAu_q , jacAu_lg ,
                                                                jacBC_u , jacBC_q , jacBC_lg ),
          xfld, (ufld, qfld), lgfld, quadratureOrder, 4);

#if 0
      fstream fout( "tmp/jac_HDG_new_2Dtrick.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
      fstream fout2( "tmp/rsd_HDG_new_2Dtrick.mtx", fstream::out );
      for (int i = 0; i<4 ; i++)
        for (int k = 0; k<nDOFPDE; k++)
          fout2 << rsd[i][k][0] << "\n";
#endif

      // solve

      SLA::UMFPACK<SystemMatrixClass> solver;

      SystemVectorClass sln = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};

      sln = solver.inverse(jac)*rsd;

      // updated solution

      for (int k = 0; k < nDOFPDE; k++)
        ufld.DOF(k) -= sln[0][k];

      for (int k = 0; k < nDOFPDE; k++)
        for (int d = 0; d < PhysD2::D; d++)
          qfld.DOF(k)[d] -= sln[1][PhysD2::D*k+d];

      for (int k = 0; k < nDOFInt; k++)
        uIfld.DOF(k) -= sln[2][k];

      for (int k = 0; k < nDOFBC; k++)
        lgfld.DOF(k) -= sln[3][k];

      // check that the residual is zero

      rsd = 0;

      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_HDG(fcnCell, rsd(0), rsd(1)),
                                              xfld, (ufld, qfld), quadratureOrder, 1 );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(0), rsd(1), rsd(2)),
                                                              xfld, (ufld, qfld), uIfld, quadratureOrder, 3);

      IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBCNone, rsd(0), rsd(1)),
                                                            xfld, (ufld, qfld), quadratureOrder, 4 );

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBCSoln, rsd(0), rsd(1), rsd(3)),
                                                          xfld, (ufld, qfld), lgfld, quadratureOrder, 4 );


      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

      Real rsdAunrm = 0;
      for (int n = 0; n < nDOFAu; n++)
        rsdAunrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdAunrm), 1e-12 );

      Real rsdIntnrm = 0;
      for (int n = 0; n < nDOFInt; n++)
        rsdIntnrm += pow(rsd[2][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdIntnrm), 1e-12 );

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[3][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

      // L2 solution error

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell( fcnErr, SquareError ), xfld, ufld, quadratureOrder, 1 );
      Real norm = SquareError;

      //hVec[indx] = 1./ii;
      //hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;

//      //dump solution
//      for (int k = 0; k < nDOFPDE; k++)
//        cout << ufld.DOF(k) << "\n";
//      output_Tecplot(ufld, "tmp/ufldout_topoff.dat");

    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
