// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "Field/output_Tecplot.h"
#include "Field/output_grm.h"

#include "unit/UnitGrids/XField2D_Duct_Triangle_Lagrange_Xq.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

BOOST_AUTO_TEST_SUITE( Duct2D_Meshing_test_suite )

Real gaussian_bump(Real x)
{ return 0.0625*exp(-25*(x*x)); }

Real constant_height(Real x)
{ return 0.8; }

BOOST_AUTO_TEST_CASE( Duct2D_Meshing_Gaussian_Bump )
{


mpi::communicator comm;

int refinement = 0;
int order = 4;
int ii = 6*pow(2, refinement);
int jj = 2*pow(2, refinement);
Real xmin = -1.5e-0;
Real xmax = +1.5e-0;
string filename = "tmp/test_duct_grid";

XField2D_Duct_Triangle_Lagrange_Xq xfld(comm, ii, jj, order, xmin, xmax, gaussian_bump, constant_height);
output_Tecplot(xfld, filename + ".plt");
output_grm(xfld, filename + ".grm");

}
BOOST_AUTO_TEST_SUITE_END()
