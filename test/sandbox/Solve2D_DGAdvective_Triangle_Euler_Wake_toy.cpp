// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGAdvective_Triangle_Euler_CubicSource_btest
// testing of 2-D DG Advective for Euler on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/SolutionFunction_Euler2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h" // because BC has solution template

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE // This is a HACK...
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGAdvective_Triangle_Euler_Wake_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGAdvective_Triangle_Wake10 )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorX VectorX;

  typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputEntropyErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquaredClass> NDOutputEntropyErrorSquaredClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputEntropyErrorSquaredClass> IntegrandSquareErrorClass;

  // PDE
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;

  GasModel gas(gasModelDict);
  NDPDEClass pde(gas, Euler_ResidInterp_Momentum);
  int nSol = NDPDEClass::N;

  typedef SolutionFunction_Euler2D_Wake<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolnNDConvertSpace<PhysD2, SolutionClass> SolutionNDClass;

  typedef BCTypeInflowSubsonic_sHqt_Profile<SolutionNDClass> BCInflowSubProf;
  typedef BCEuler2D<BCInflowSubProf, PDEClass> BCSolnClass;

  //SolutionClass wakesol(gas, 0.1, 1.0, 1.0, 1.0);
  //BCSolnClass bc(pde, wakesol);

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;
  typedef boost::mpl::push_back< BCVector, BCSolnClass >::type BCVectorWithSolnClass;

  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVectorWithSolnClass,
                               AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  PyDict BCInflowSubsonicProfile;
  BCInflowSubsonicProfile[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_Profile;

  Real ptref = pow(1 + 0.2*0.01,3.5);
  Real ttref = 1+ 0.2*0.01;

  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.wakedepth] = 0.1;
  solnArgs[SolutionClass::ParamsType::params.Pt] = ptref;
  solnArgs[SolutionClass::ParamsType::params.Tt] = ttref;
  solnArgs[SolutionClass::ParamsType::params.p] = 1.0;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;

  BCInflowSubsonicProfile[BCEuler2DParams<BCInflowSubProf>::params.SolutionArgs] = solnArgs;
  SolutionNDClass solnExact( solnArgs );
  VectorX X = {1.0, 0.5};
  ArrayQ q0 = solnExact(X);

  // BC
  // Create a BC dictionary
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure>::params.pSpec] = 1.0;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCInflowSubsonicProfile;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2};
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {3};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  //setup output error

  Real sExact = 0.0;
  NDOutputEntropyErrorSquaredClass outEntropyError( pde, sExact );
  IntegrandSquareErrorClass fcnErr( outEntropyError, {0} );

  //IntegrandSquareErrorClass fcnErr( solnExact ); //exact solution error TODO:need to modify square error class for arrays!

  // norm data
//  Real normVec[10];   // L2 error
//  int indx = 0;

  // Tecplot output
#if 0
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_2D_HDG.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/Solve/Solve2D_HDG_Triangle_AD_FullTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/Solve/Solve2D_HDG_Triangle_AD_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif

  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

#endif

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 5;
#else
  int ordermax = 1;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    //indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
#else
    int powermax = 2;
#endif
    for (int power = powermin; power <= powermax; power++)
    {

      jj = pow( 2, power );
      ii = 3*jj; //small grid!
      Real tau = 0.0; //10% bump

      // grid: 2D Cubic Source Bump
      XField2D_CubicSourceBump_Xq xfld( ii, jj, tau );

      // DG solution field
      //freestream solution is (1, 0.1, 0, 1);
      ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(1, 0.1, 0, 1) );

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

      // Set the initial condition
      qfld = q0;

      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
        lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      lgfld = 0;

      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-11, 1e-11};

      ////////////
      //SOLVE
      ////////////

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0;

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      Real rsdPDEnrm[4] = {0,0,0,0};

#if 0
// VERBOSE LINESEARCH
      Real rsdPDEnrmOLD[4];
      //Initialize Residual
      PrimalEqSet.residual(ini,rsd);

      for (int n = 0; n < nDOFPDE; n++)
        for (int j = 0; j < nSol; j++)
          rsdPDEnrm[j] += pow(rsd[0][n][j],2);

      for (int j = 0; j < nSol; j++)
        rsdPDEnrmOLD[j] = rsdPDEnrm[j];

      // Initialize jacobian
      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());

      PrimalEqSet.jacobian(ini, nz);

      //TODO: We should not create a temporary matrix here. It should be moved into LinearSolver_
      SystemMatrixClass A(nz);
      A = 0;

      SLA::UMFPACK<SystemMatrixClass> solver;

      SystemVectorClass dsln(PrimalEqSet.vectorStateSize());

      int maxit = 5;
      int nit = 0;




      while ( sqrt(rsdPDEnrm[0]) > 1e-12 || sqrt(rsdPDEnrm[1]) > 1e-12 || sqrt(rsdPDEnrm[2]) > 1e-12 || sqrt(rsdPDEnrm[3]) > 1e-12  )
      {
        A = 0;
        PrimalEqSet.jacobian(sln,A);

        dsln = solver.inverse(A)*rsd;

        int linesearchit = 0;
        int maxlineit = 20;
        while ( linesearchit < maxlineit  )
        {
          slnchk = sln - dsln;

          rsd = 0;
          PrimalEqSet.residual(slnchk, rsd);

          for (int j = 0; j < nSol; j++)
            rsdPDEnrm[j] = 0;

          for (int n = 0; n < nDOFPDE; n++)
            for (int j = 0; j < nSol; j++)
              rsdPDEnrm[j] += pow(rsd[0][n][j],2);

          if ( rsdPDEnrm[0] < rsdPDEnrmOLD[0] && rsdPDEnrm[1] < rsdPDEnrmOLD[1] && rsdPDEnrm[2] < rsdPDEnrmOLD[2] && rsdPDEnrm[3] < rsdPDEnrmOLD[3])
          {
            std::cout << "  linesearchit: " << linesearchit;
            std::cout << " rsd0: " << sqrt(rsdPDEnrm[0]) <<  " rsd1: " << sqrt(rsdPDEnrm[1]) << " rsd2: "
                << sqrt(rsdPDEnrm[2]) << " rsd3: " << sqrt(rsdPDEnrm[3]) << "\n";
            sln = slnchk;
            break;
          }
          else
          {
            std::cout << "  linesearchit: " << linesearchit;
            std::cout << " rsd0: " << sqrt(rsdPDEnrm[0]) <<  " rsd1: " << sqrt(rsdPDEnrm[1]) << " rsd2: "
                << sqrt(rsdPDEnrm[2]) << " rsd3: " << sqrt(rsdPDEnrm[3]) << "\n";
            for (int k=0; k<dsln.m(); k++)
              dsln[k] = dsln[k]*0.5;
            linesearchit++;
          }


        }

        if (linesearchit >= maxlineit)
          std::cout << "linesearch failed" << "\n";

        // check whether the residual is zero

        for (int j = 0; j < nSol; j++)
          rsdPDEnrm[j] = 0;

        for (int n = 0; n < nDOFPDE; n++)
          for (int j = 0; j < nSol; j++)
            rsdPDEnrm[j] += pow(rsd[0][n][j],2);

        for (int j = 0; j < nSol; j++)
          rsdPDEnrmOLD[j] = rsdPDEnrm[j];

        std::cout << "nit: " << nit << " rsd0: " << sqrt(rsdPDEnrm[0]);
        std::cout << " rsd1: " << sqrt(rsdPDEnrm[1]) << " rsd2: " << sqrt(rsdPDEnrm[2]) << " rsd3: " << sqrt(rsdPDEnrm[3]);
        std::cout << " lineit: " << linesearchit << "\n";

//        string filename = "tmp/slnDG_EulerWake_NOTCONVERGED_P";
//        filename += to_string(order);
//        filename += "_";
//        filename += to_string(ii);
//        filename += "x";
//        filename += to_string(jj);
//        filename += "nit";
//        filename += to_string(nit);
//        filename += ".plt";
//        cout << "calling output_Tecplot: filename = " << filename << endl;
//        output_Tecplot( qfld, filename );

        if (nit > maxit)
          break;
        else
          nit++;

      }
#else
      bool solved;
      solved = Solver.solve(ini,sln).converged;
      BOOST_REQUIRE(solved);
      PrimalEqSet.setSolutionField(sln);

#endif

#if 0
      fstream fout( "tmp/jac_eulerDG_new.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( A, fout );
#endif


      rsd = 0;
      PrimalEqSet.residual(sln, rsd);

      // check that the residual is zero

      for (int j = 0; j < nSol; j++)
        rsdPDEnrm[j] = 0;

      for (int n = 0; n < nDOFPDE; n++)
        for (int j = 0; j < nSol; j++)
          rsdPDEnrm[j] += pow(rsd[0][n][j],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[0]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[1]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[2]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[3]), 1e-12 );

      // Monitor Entropy Error
      Real EntropySquareError = 0.0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_Galerkin( fcnErr, EntropySquareError ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( EntropySquareError );
//      if (indx > 1)
//        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;
#endif

#if 0
      // Tecplot dump grid
      string filename = "tmp/slnDG_EulerWake_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
#endif

    }

    // Tecplot result file
#if 0
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }
#endif

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
