// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Testing of the MOESS framework with L2-Projection

//#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <fstream>

#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/FunctionNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "Adaptation/MOESS/SolverInterface_L2Project.h"
#include "Adaptation/MeshAdapter.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field/output_Tecplot.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "tools/timer.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_L2Project_MOESS_test_suite )

//----------------------------------------------------------------------------//

template< class SolutionExact,  template<class,class,class> class FieldConstructor >
void
adapt( const PyDict& solnArgs,
       const int order, const int power, const int maxIter,
       const string mesher, const bool dumpMetrics, const string file_tag )
{

  string func;
  if (typeid(SolutionExact) == typeid(SolnNDConvertSpace<PhysD3,ScalarFunction3D_TripleBL>))
    func = "TripleBL";
  else if (typeid(SolutionExact) == typeid(SolnNDConvertSpace<PhysD3,ScalarFunction3D_CornerSingularity>))
    func = "Corner";
  else if (typeid(SolutionExact) == typeid(SolnNDConvertSpace<PhysD3,ScalarFunction3D_Tanh3>))
    func = "Corner";

  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;

  const bool use_CG = (typeid(FieldConstructor<PhysDim,TopoDim,Real>) == typeid(Field_CG_Cell<PhysDim,TopoDim,Real>));

  mpi::communicator world;

  typedef SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact, FieldConstructor> SolverInterfaceClass;

  // Create a solution dictionary
  SolutionExact solnExact(solnArgs);

  std::array<Real,1> tol = {{1e-11}};

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;
//
// #if defined(SANS_PETSC)
//   std::cout << "Linear solver: PETSc" << std::endl;
//
//   PyDict PreconditionerDict;
//   PyDict PreconditionerILU;
//   PyDict PETScDict;
//
//   PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
//   PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//   PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//   PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;
//
//   PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
//   PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
//
//   PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//   PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-9;
//   PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
//   PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
//   PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
//   PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
//   PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
//   PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
//   PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
//   //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;
//
//   PyDict PETScDictAdjoint(PETScDict);
//   PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
//   PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
//
//   LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
//
// #elif defined(INTEL_MKL)
//   std::cout << "Linear solver: MKL_PARDISO" << std::endl;
//   PyDict MKL_PARDISODict;
//   MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
//   LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
//   NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
// #else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
// #endif

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;

  MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;

  if (use_CG)
    MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;

  PyDict MesherDict;
  if (mesher == "refine")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.refine;
    MesherDict[refineParams::params.DumpRefineDebugFiles] = false;
#ifdef SANS_REFINE
    MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;
    //MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.system;
#endif
  }
  else if (mesher == "EPIC")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
    std::vector<int> allBC = {0,1,2,3,4,5};
    MesherDict[EpicParams::params.SymmetricSurf] = allBC;
  }
  else if (mesher == "fefloa")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.FeFloa;
  }
#ifdef SANS_AVRO
  else if (mesher == "avro")
  {
    MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.avro;
    MesherDict[avroParams::params.Curved] = false;
    MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
    MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files
  }
#endif
  else
    BOOST_REQUIRE_MESSAGE(false, "Unknown mesh generator.");


  //--------ADAPTATION LOOP--------

  timer totalTime;

  const Real nk = pow(2,power);
  int targetCost = 500*nk;

  // Real nDOFperCell_DG = (order+1)*(order+2)*(order+3)/6;
  // //equiVol = sqrt(2.0)/12;
  // //targetComp = targetCost*equiVol/nDOFperCell_DG;
  // //targetCost = targetComp*nDOFperCell_CG/equiVol;
  //
  // Real nDOFperCell_CG = nDOFperCell_DG;
  // nDOFperCell_CG -= (4 - 1./5); // the node dofs are shared by 20
  // nDOFperCell_CG -= (6 - 1)*std::max(0,(order-1)); // if there are edge dofs they are shared by 6
  // nDOFperCell_CG -= (4 - 2)*std::max(0,(order-1)*(order-2)/2); // if there are face dofs they are shared by 2
  //
  // targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;

  const int string_pad = 6;
  string int_pad = string(string_pad - std::to_string(targetCost).length(), '0') + std::to_string(targetCost);

  if ( use_CG ) // Modify the dof counts to match the DG element counts - Should be much faster to run and show the same
  {
    Real nDOFperCell_DG = (order+1)*(order+2)/2;
    Real nDOFperCell_CG = nDOFperCell_DG;
    nDOFperCell_CG -= (4 - 4.0/20.0); // the node dofs are shared by 20
    nDOFperCell_CG -= (6 - 6.0/(11./2.0))*std::max(0,(order-1)); // if there are edge dofs they are shared by 11/2
    nDOFperCell_CG -= (4 - 4.0/2.0)*std::max(0,(order-1)*(order-2)/2); // if there are face dofs they are shared by 2

    targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
  }

  string filename_base = "tmp/L2Project_3D/" + func + "/" + mesher + "_";

  BasisFunctionCategory basis_category;

  if ( use_CG &&  (typeid(FieldConstructor<PhysDim,TopoDim,Real>) == typeid(Field_CG_Cell<PhysDim,TopoDim,Real>)) )
  {
    filename_base += "CG_";
    basis_category = BasisFunctionCategory_Lagrange;
  }
  else if (typeid(FieldConstructor<PhysDim,TopoDim,Real>) == typeid(Field_DG_Cell<PhysDim,TopoDim,Real>))
  {
    filename_base += "DG_";
    basis_category = BasisFunctionCategory_Legendre;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Invalid choice of solution field!");

  if (file_tag.size() > 0)
    filename_base += file_tag + "_"; // the additional file name bits kept especially

  filename_base += int_pad + "_P" + std::to_string(order) + "/";

  boost::filesystem::path base_dir(filename_base);
  if ( not boost::filesystem::exists(base_dir) )
    boost::filesystem::create_directories(filename_base);

  string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist;
  if ( world.rank() == 0 )
  {
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  string output_filename = filename_base + "output.dat";
  fstream foutputhist;
  if ( world.rank() == 0 )
  {
    foutputhist.open( output_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + output_filename);
  }

  if (world.rank() == 0 )
  {
    // write the header to the output file
    foutputhist << "VARIABLES="
                << std::setw(5)  << "\"Iter\""
                << std::setw(10) << "\"DOF\""
                << std::setw(20) << "\"Elements\""
                << std::setw(20) << "\"L<sup>2</sup> Error\""
                << std::endl;

    foutputhist << "ZONE T=\"MOESS " << mesher << " " << nk << "k\"" << std::endl;
  }

  MesherDict[refineParams::params.FilenameBase] = filename_base;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpMetric] = dumpMetrics;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpOptimizationInfo] = false;

  MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

  MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld;

  const int ii = 4;
#if defined(SANS_AVRO)
  std::shared_ptr<avro::Context> context;
  if (mesher == "avro")
  {
    using avro::coord_t;
    using avro::index_t;

    context = std::make_shared<avro::Context>();

    coord_t number = 3;
    std::vector<avro::real> x0(number,0.);
    std::vector<avro::real> lens(number,1.);
    std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>( context.get(), x0.data(), lens.data() );
    std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(),"cube");
    model->addBody(pbody,true);

    // initialize the mesh
    XField3D_Box_Tet_X1 xfld0( world, ii , ii , ii );
    // copy the mesh into the domain and attach the geometry
    pxfld = std::make_shared< XField_avro<PhysD3,TopoD3> >(xfld0,model);
  }
  else
#endif
  {
    pxfld = std::make_shared<XField3D_Box_Tet_X1>( world, ii, ii, ii );
  }

  std::vector<int> cellGroups = {0};
  int iter = 0;
  while (true)
  {
    std::cout << "-----Adaptation Iteration " << iter << "-----" << std::endl;

    FieldConstructor<PhysDim,TopoDim,Real> qfld(*pxfld,order,basis_category);

    // Adjoint equation is integrated with 2 times adjoint order
    const int quadOrder = 2*(order+1);

    //Perform L2 projection from solution on previous mesh
    qfld = 0;

    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(qfld, quadOrder,
                                                        cellGroups, tol, solnExact,
                                                        LinearSolverDict,true); // the true is to local project DG

    pInterface->solveGlobalPrimalProblem();

    // std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter)
    //                                           + "_P" + std::to_string(order) +
    //                                           + ".plt";
    if ( iter == maxIter )
    {
      std::string qfld_filename = filename_base + "qfld_final.plt";
      output_Tecplot( qfld, qfld_filename );
    }

#ifdef SANS_MPI
    int nDOFtotal = 0;
    boost::mpi::reduce(*pxfld->comm(), qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );

    // count the number of elements possessed by this processor
    int nElem = 0;
    for (int elem = 0; elem < pxfld->nElem(); elem++ )
      if (pxfld->getCellGroupGlobal<Tet>(0).associativity(elem).rank() == world.rank())
        nElem++;

    int nElemtotal = 0;
    boost::mpi::reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>(), 0 );
#else
    int nDOFtotal = qfld.nDOFpossessed();
    int nElemtotal = pxfld->nElem();
#endif

    //Compute error estimates
    pInterface->computeErrorEstimates();

    Real L2error = pInterface->getOutput();

    if (world.rank() == 0 )
    {
      foutputhist << std::setw(5) << iter
                  << std::setw(10) << nDOFtotal
                  << std::setw(10) << nElemtotal
                  << std::setw(20) << std::setprecision(10) << std::scientific << L2error
                  << std::endl;
    }
    if ( iter == maxIter ) break;

    //Perform local sampling and adapt mesh
    pxfld = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    iter++;
  }
  if (world.rank() == 0)
    fadapthist << "\n\nTotal Time elapsed: " << totalTime.elapsed() << "s" << std::endl;

  fadapthist.close();
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_L2_test )
{
  // default parameters
  string func = "TripleBL";
  int orderL = 1, orderH = 3;
  int powerL = 0, powerH = 4;
  int maxIter = 100;
  string mesher = "avro";
  bool use_CG = false;
  bool dumpMetrics = false;
  string file_tag = "";

  // read command-line args if any
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc >= 2)
    func = string(argv[1]);
  if (argc >= 3)
    mesher = string(argv[2]);
  if (argc >= 4)
    file_tag = string(argv[3]);
  if (argc >= 5)
    use_CG = std::stoi(argv[4]);
  if (argc >= 6)
    dumpMetrics = std::stoi(argv[5]);
  if (argc >= 7)
    orderL = orderH = std::stoi(argv[6]);
  if (argc >= 8)
    powerL = powerH = std::stoi(argv[7]);

  std::cout << "func: " << func << ", mesher: " << mesher << ", file_tag: " << file_tag << ", disc: ";
  if (use_CG)
    std::cout << "CG, ";
  else
    std::cout << "DG, ";
  std::cout << "dumpMetrics: " << dumpMetrics << ", orderL,H: " << orderL << "," << orderH << ", powerL,H: " << powerL << "," << powerH << std::endl;

  for (int order = orderL; order <= orderH; order++)
    for (int power = powerL; power <= powerH; power++)
    {
      // set up the dictionary based on the params
      if (func=="TripleBL")
      {
        PyDict solnArgs;
        typedef SolnNDConvertSpace<PhysD3,ScalarFunction3D_TripleBL> SolutionExact;
        solnArgs[SolutionExact::ParamsType::params.a] = 8.0/17.0;
        solnArgs[SolutionExact::ParamsType::params.b] = 9.0/17.0;
        solnArgs[SolutionExact::ParamsType::params.c] = 12.0/17.0;
        solnArgs[SolutionExact::ParamsType::params.nu] = 1.0/30.0;
        solnArgs[SolutionExact::ParamsType::params.offset] = 1.0;
        solnArgs[SolutionExact::ParamsType::params.scale] = -1.0;

        if (use_CG)
        {
          adapt<SolutionExact,Field_CG_Cell>(solnArgs,order,power,maxIter,mesher,dumpMetrics,file_tag);
        }
        else
        {
          adapt<SolutionExact,Field_DG_Cell>(solnArgs,order,power,maxIter,mesher,dumpMetrics,file_tag);
        }
      }
      else if (func=="Corner")
      {
        PyDict solnArgs;
        typedef SolnNDConvertSpace<PhysD3,ScalarFunction3D_CornerSingularity> SolutionExact;
        solnArgs[SolutionExact::ParamsType::params.alpha] = 2.0/3.0;
        // solnArgs[SolutionExact::ParamsType::params.theta0] = 0.0;

        if (use_CG)
        {
          adapt<SolutionExact,Field_CG_Cell>(solnArgs,order,power,maxIter,mesher,dumpMetrics,file_tag);
        }
        else
        {
          adapt<SolutionExact,Field_DG_Cell>(solnArgs,order,power,maxIter,mesher,dumpMetrics,file_tag);
        }
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION("bad function");
      }
    }


}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
