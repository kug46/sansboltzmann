// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_HDG_Line_Euler_Sod_ST_toy
// testing of 2-D HDG with Timestepping

#undef SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DEntropy.h"

#include "pde/NS/PDEEuler1D.h"
#include "pde/NS/BCEuler1D.h"
#include "pde/NS/SolutionFunction_Euler1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "pde/BCParameters.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE // This is a HACK...
#include "Discretization/HDG/AlgebraicEquationSet_HDG_impl.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/XFieldArea.h"
#include "Field/FieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"

#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_HDG_Line_Euler_Sod_test_suite )

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( Solve1D_HDG_Line_Euler_Sod )
{
//  typedef QTypeEntropy QType;
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef SolutionFunction_Euler1D_Riemann<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, SolutionClass> SolutionNDClass;

  typedef BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_HDG< NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef BCEuler1D<BCTypeFunction_mitState, PDEClass> BCSolnClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // Gas Model
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);
  // PDE
  PDEClass::EulerResidualInterpCategory interp = PDEClass::Euler_ResidInterp_Momentum;
  NDPDEClass pde(gas, interp);

  // Set up IC with Sod's shock tube problem
  //  See: "Riemann Solvers and Numerical Methods for Fluid Dynamics" - Toro (Section 4.3.3 Table 4.1 in my copy)
  PyDict solnArgs;
  solnArgs[BCSolnClass::ParamsType::params.Function.Name] = BCSolnClass::ParamsType::params.Function.Riemann;
  solnArgs[SolutionClass::ParamsType::params.interface] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.rhoL] = 1.0;
  solnArgs[SolutionClass::ParamsType::params.uL] = 0.0;
  solnArgs[SolutionClass::ParamsType::params.pL] = 1.0;
  solnArgs[SolutionClass::ParamsType::params.rhoR] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.uR] = 0.0;
  solnArgs[SolutionClass::ParamsType::params.pR] = 0.5;
  solnArgs[SolutionClass::ParamsType::params.gasModel] = gasModelDict;
  SolutionNDClass solnExact(solnArgs);
  Real Tend = 0.25;

  // BC
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCReflect;
  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  PyDict BCTypeFunction_mitState;
  BCTypeFunction_mitState[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCTypeFunction_mitState[BCSolnClass::ParamsType::params.Function] = solnArgs;
  BCTypeFunction_mitState[BCSolnClass::ParamsType::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCNone"] = BCNone;
  PyBCList["BCReflect"] = BCReflect;
  PyBCList["BCIn"] = BCTypeFunction_mitState;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNone"] = {2}; //t-max
  BCBoundaryGroups["BCReflect"] = {1,3}; //x-min, x-max
  BCBoundaryGroups["BCIn"] = {0}; //t-min

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  int ii = 40; //grid size - using timesteps = grid size
  int tt = 20;
  int order = 1;

  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, tt, 0, 1, 0, Tend);

  // DG solution field
  ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure1D<Real>(1.1, 0, 1.1) );
  // cell solution
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = q0;
  //for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );

  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = q0;

  // lagrange multipliers not actually used either
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
    lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

#if 1
  // Tecplot dump grid
  string filename2 = "tmp/icHDG_ST_EulerSod_P";
  filename2 += to_string(order);
  filename2 += "_";
  filename2 += to_string(ii);
  filename2 += "_";
  filename2 += to_string(tt);
  filename2 += ".plt";
  output_Tecplot( qfld, filename2 );
#endif

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-12, 1e-12, 1e-12};
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                     {0}, {0,1,2}, PyBCList, BCBoundaryGroups);

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1.0e-12;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);
#if 1
  NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

  SystemVectorClass ini(PrimalEqSet.vectorStateSize());
  SystemVectorClass sln(PrimalEqSet.vectorStateSize());

  PrimalEqSet.fillSystemVector(ini);
  sln = ini;

  SolveStatus status = Solver.solve(ini, sln);
  BOOST_CHECK( status.converged );
  PrimalEqSet.setSolutionField(sln);
#else
  Real CFLi_max = 1;
  Real CFLi_min = 0;

  // Create the pseudo time continuation class
  PseudoTime<NDPDEClass, XField<PhysD2, TopoD2>>
    PTC(CFLi_max, CFLi_min, xfld, qfld, NewtonSolverDict, pde, {0}, PrimalEqSet);

  // iterate the pseudo time to convergence
  converged = PTC.iterate(100);
  BOOST_CHECK(converged);
#endif

#if 1
  cout << std::setprecision(16) << std::scientific;
  cout << "P = " << order << " ii = " << ii << " tt = " << tt;// << ": L2 solution error = " << sqrt( EntropySquareError );
  cout << endl;
#endif

#if 1
  // Tecplot dump grid
  string filename = "tmp/slnHDG_ST_EulerSod_P";
  filename += to_string(order);
  filename += "_";
  filename += to_string(ii);
  filename += "_";
  filename += to_string(tt);
  filename += ".plt";
  output_Tecplot( qfld, filename );
#endif
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
