// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve_DGBR2_Triangle_AD_btest
// testing of 2-D DG BR2 with Advection-Diffusion on triangles

//#define SANS_FULLTEST
//#define SANS_VERBOSE
//#define SANS_TIMING

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

//#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Triangle_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Triangle_AD )
{
  typedef ScalarFunction2D_Gaussian SolutionExact;
//  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeLinearRobin_sansLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE

  Real a = 0.3;
  Real b = 0.2;

  AdvectiveFlux2D_Uniform adv( a, b );

  //Non-symmetric diffusion matrix
  Real nu = 0.1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_None source;

  Real x0 = 0.5; Real y0 = 0.5;
  Real sx = 0.03; Real sy = 0.03;
  Real amp = 1.0;

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[SolutionExact::ParamsType::params.a] = amp;
  solnArgs[SolutionExact::ParamsType::params.x0] = x0;
  solnArgs[SolutionExact::ParamsType::params.y0] = y0;
  solnArgs[SolutionExact::ParamsType::params.sx] = sx;
  solnArgs[SolutionExact::ParamsType::params.sy] = sy;

  SolutionExact solnExact( solnArgs );
  //
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
//  PyDict DoubleBL;
//  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
//      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
//  DoubleBL[SolutionExact::ParamsType::params.a] = a;
//  DoubleBL[SolutionExact::ParamsType::params.b] = b;
//  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
#if 0
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2, BCType>::params.Upwind] = false;
  BCSoln[BCAdvectionDiffusionParams<PhysD2, BCType>::params.SolutionBCType] = "Dirichlet";
#elif 0
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
#else
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = 0.0;

#endif

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCBottom"] = BCSoln;
  PyBCList["BCRight"] = BCSoln;
  PyBCList["BCTop"] = BCSoln;
  PyBCList["BCLeft"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
//  BCBoundaryGroups["BCNameSolution"] = {0, 1, 2, 3};
  BCBoundaryGroups["BCBottom"] = {0};
  BCBoundaryGroups["BCRight"] = {1};
  BCBoundaryGroups["BCTop"] = {2};
  BCBoundaryGroups["BCLeft"] = {3};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // norm data
  Real hVec[10];
//  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "Running full test..." << endl;
#endif
  int ordermax = 5;
#else
  int ordermax = 3;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 2;
#ifdef SANS_FULLTEST
    int powermax = 5;
    if ( order > 4 ) powermax = 4;
#else
    int powermax = 8;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );
      jj = ii;

      // grid:
//      XField2D_Box_Triangle_X1 xfld( ii, jj );
      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

      // solution: Legendre, C0
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      qfld = 0;

      // lifting operators
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      rfld = 0;

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 0
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#elif 0
      std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order-1, BasisFunctionCategory_Legendre,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#endif

      lgfld = 0;

      QuadratureOrder quadratureOrder( xfld, 3*order+1 );
      std::vector<Real> tol = {1e-11, 1e-11};
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      // residual
      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

      PrimalEqSet.fillSystemVector(q);

      rsd = 0;
      PrimalEqSet.residual(q, rsd);

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);

      SystemVectorClass dq(q.size());
      solver.solve(rsd, dq);

      // update solution
      q -= dq;
      PrimalEqSet.setSolutionField(q);

      // check that the residual is zero
      rsd = 0;
      PrimalEqSet.residual(q, rsd);
      std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

      bool converged = PrimalEqSet.convergedResidual(rsdNorm);
      BOOST_CHECK( converged );
      if (!converged) PrimalEqSet.printDecreaseResidualFailure( PrimalEqSet.residualNorm(rsd) );

      // L2 solution error

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( errorIntegrand, SquareError ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size()  );
      Real norm = SquareError;

      hVec[indx] = 1./ii;
//      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
      {
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        Real slope = (log(normVec[indx-1]) - log(normVec[indx-2]))/(log(hVec[indx-1]) - log(hVec[indx-2]));
        cout << "  (slope = " << slope << ")";
      }
      cout << endl;

#if 1
      // Tecplot dump
      string filename = "tmp/slnDG_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
#endif

    } //grid refinement loop


  } //order loop
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
