// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_CG_Triangle_LIP_CubicSource_btest
// testing of 2-D CG Advective for Linear Incompressible Potential on triangle Cubic-Source Bump mesh

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/FullPotential/PDELinearizedIncompressiblePotential2D.h"
#include "pde/FullPotential/BCLinearizedIncompressiblePotential2D_sansLG.h"
//#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
//#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_CG_Triangle_LIP_CubicSource_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_CG_Triangle_Bump10 )
{
  typedef PDELinearizedIncompressiblePotential2D PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef PDEClass::ArrayQ<Real> ArrayQ;

  //typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputEntropyErrorSquaredClass;
  //typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquaredClass> NDOutputEntropyErrorSquaredClass;
  //typedef IntegrandCell_Galerkin_Output<NDOutputEntropyErrorSquaredClass> IntegrandSquareErrorClass;

  typedef BCLinearizedIncompressiblePotential2DVector<Real> BCVector;

  typedef AlgebraicEquationSet_Galerkin< NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  //const int nSol = NDPDEClass::N;

  // PDE
  Real u = 1;
  Real v = 0;
  NDPDEClass pde(u, v);

  BOOST_CHECK( pde.D == 2 );
  BOOST_CHECK( pde.N == 1 );

  // BC

  // exact solution
  //const Real tau = 0.1;   // 10% bump
  //SolutionFunction2D_CubicSourceBump slnExact(tau);

  // cubic-source bump solution function
  typedef BCLinearizedIncompressiblePotential2DParams<BCTypeFunction> BCFunctionClass;
  typedef SolutionFunction_Potential2D_CubicSourceBump SolutionClass;
  const Real tau = 0.1;   // 10% bump
  PyDict dictFunction;
  dictFunction[BCFunctionClass::params.Function.SolutionFunctionName] =
               BCFunctionClass::params.Function.CubicSourceBump;
  dictFunction[SolutionClass::ParamsType::params.tau] = tau;

  // wall BC
  PyDict dictBCWall;
  dictBCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Wall;

  // inflow/outflow BC: exact solution
  PyDict dictBCFunction;
  dictBCFunction[BCParams::params.BC.BCType] = BCParams::params.BC.Function;
  dictBCFunction[BCFunctionClass::params.Function] = dictFunction;

#if 0
  // outflow: exact solution
  typedef BCLinearizedIncompressiblePotential2DParams<BCTypeFunction> BCOutClass;
  PyDict dictBCOut;
  dictBCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure;
  dictBCOut[BCOutClass::ParamsType::params.Function] = dictFunction;
#endif

  PyDict dictBCList;
  dictBCList["BCWall"] = dictBCWall;
  dictBCList["BCIn"]   = dictBCFunction;
  dictBCList["BCOut"]  = dictBCFunction;

  // No exceptions should be thrown
  BCParams::checkInputs(dictBCList);

  std::map<std::string, std::vector<int>> mapBCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  mapBCBoundaryGroups["BCWall"] = {0,2};
  mapBCBoundaryGroups["BCOut"] = {1};
  mapBCBoundaryGroups["BCIn"] = {3};


  //const std::vector<int> vecBoundaryGroups = {0,1,2,3};

  // Set up Newton Solver
  PyDict dictNewtonSolver, dictUMFPACK, dictNewtonLineUpdate;

  dictUMFPACK[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  dictNewtonLineUpdate[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  dictNewtonSolver[NewtonSolverParam::params.LinearSolver] = dictUMFPACK;
  dictNewtonSolver[NewtonSolverParam::params.LineUpdate] = dictNewtonLineUpdate;
  dictNewtonSolver[NewtonSolverParam::params.MinIterations] = 0;
  dictNewtonSolver[NewtonSolverParam::params.MaxIterations] = 2;

  NewtonSolverParam::checkInputs(dictNewtonSolver);

  //Real sExact = 0.0;
  //NDOutputEntropyErrorSquaredClass outEntropyError( pde, sExact );
  //IntegrandSquareErrorClass fcnErr( outEntropyError, {0} );

  //norm data
  //Real normVec[10];   // L2 error

#if 0
  // Tecplot output of grid convergence
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_2D_HDG.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/Solve/Solve2D_HDG_Triangle_AD_FullTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/Solve/Solve2D_HDG_Triangle_AD_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif

  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;
#endif

  // loop over solution orders
  int ordermin = 1;
  int ordermax = 1;
  for (int order = ordermin; order <= ordermax; order++)
  {
    //indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
    int powermax = 1;
    for (int power = powermin; power <= powermax; power++)
    {
      jj = pow( 2, power );
      ii = 4*jj;

      // grid: 2D Cubic Source Bump
      XField2D_CubicSourceBump_Xq xfld( ii, jj, tau );

      // CG solution field
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

      // initial condition
      //ArrayQ q0 = pde.setDOFFrom( {0}, "", 1 );
      //qfld = q0;
      qfld = 0;

      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(dictBCList, mapBCBoundaryGroups) );
      lgfld = 0;

      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tolResidual = {1e-11, 1e-11};

      ////////////
      // SOLVE
      ////////////

      typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
      typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

      StabilizationNitsche stab(order);
      PrimalEquationSetClass primalEqSet(xfld, qfld, lgfld, pde, stab,
          quadratureOrder, ResidualNorm_L2, tolResidual, {0}, dictBCList, mapBCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> solver( primalEqSet, dictNewtonSolver );

      auto sizeSys = primalEqSet.vectorStateSize();
      SystemVectorClass ini(sizeSys);
      SystemVectorClass sln(sizeSys);
      SystemVectorClass slnchk(sizeSys);
      SystemVectorClass rsd(sizeSys);
      rsd = 0;

      primalEqSet.fillSystemVector(ini);
      sln = ini;

      BOOST_REQUIRE( solver.solve(ini,sln).converged );
      primalEqSet.setSolutionField(sln);

      #if 1
      string foutName = "tmp/jacCG_LIP_cubicbump_P";
      foutName += to_string(order);
      foutName += "_";
      foutName += to_string(ii);
      foutName += "x";
      foutName += to_string(jj);
      foutName += ".mtx";
      fstream fout( foutName, fstream::out );
      cout << "btest: dumping jacobian to file " << foutName << endl;

      PrimalEquationSetClass::SystemNonZeroPattern nz(primalEqSet.matrixSize());
      primalEqSet.jacobian( sln, nz );
      SystemMatrixClass jac(nz);
      primalEqSet.jacobian(sln, jac);
      //primalEqSet.jacobian( jac );
      WriteMatrixMarketFile( jac, fout );
      #endif

      rsd = 0;
      primalEqSet.residual(sln, rsd);

      // check that the residual is zero

      Real rsdPDEnorm = 0;

      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnorm += pow(rsd[0][n], 2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnorm), 1e-12 );

      #define SANS_VERBOSE
      #ifdef SANS_VERBOSE
      //cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( EntropySquareError );
      //if (indx > 1)
      //  cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;
      #endif

      #if 1
      // Tecplot dump
      string filename = "tmp/slnCG_LIP_cubicbump_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
      #endif
      #if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
      #endif

    }

#if 0
    // Tecplot result file
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }
#endif

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
