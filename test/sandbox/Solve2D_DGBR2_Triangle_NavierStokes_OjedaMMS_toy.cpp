// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
//  Solve2D_DGBR2_Triangle_NavierStokes_Poiseuille_btest
// testing of 2-D DGBR2 for NS on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>

#include "Field/ProjectSoln/InterpolateFunctionCell_Continuous.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/PDENavierStokes_Brenner2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/BCParameters.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Triangle_NavierStokes_OjedaMMS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGBR2_Triangle_OjedaMMS )
{

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;

//#define BRENNER
#ifndef BRENNER
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
#else
  typedef PDENavierStokes_Brenner2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
#endif

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;
  typedef SolutionFunction_NavierStokes2D_OjedaMMS<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolnType;
  typedef SolnNDConvertSpace<PhysD2,SolnType> NDSolnType;
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;

  typedef OutputEuler2D_TotalEnthalpyWindowed<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> IntegrandOutputClass;

#ifndef BRENNER
  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;
#else
  typedef BCNavierStokesBrenner2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;
#endif

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;


  std::vector<Real> tol = {1e-10, 1e-10};

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  GasModel gas(gamma, R);
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // INFLOW CONDITIONS TO MATCH OJEDA'S MMS

  Real rho0 = 1.0;
  Real u0 = 0.2;
  Real v0 = 0.1;
  Real p0 = 1.0;
  Real t0 = p0/rho0/R;

  const Real muRef = 0.02; // FOR RE = 100
  const Real Prandtl = 0.72;

  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDSolnType soln(gas);
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(soln) );

#ifndef BRENNER
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, forcingptr );
#else
  Real rhodiff = 4./3.;
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, rhodiff, forcingptr );
#endif


  //  Velocity Magnitude Error
  NDOutputClass fcnOutput(pde);
  IntegrandOutputClass fcnErr( fcnOutput, {0} );

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolnType> L2ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, L2ErrorClass> NDL2ErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDL2ErrorClass> L2ErrorIntegrandClass;

  NDL2ErrorClass fcnL2Err(soln);
  L2ErrorIntegrandClass errorL2Integrand(fcnL2Err, {0});

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  std::map<std::string, std::vector<int>> BCBoundaryGroups;
#if 1
  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rho0;
  StateVector[Conservative2DParams::params.rhou] = rho0*u0;
  StateVector[Conservative2DParams::params.rhov] = rho0*v0;

  const Real ERef = Cv*t0 + 0.5*(u0*u0+v0*v0);

  StateVector[Conservative2DParams::params.rhoE] = rho0*ERef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;

  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;

  BCBoundaryGroups["BCIn"]  = {0,1,2,3};

#else
#if 1

  // BC
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;

  typedef BCEuler2D<BCTypeFunction_mitState, PDEClass> BCClass2;

  PyDict Function;
  Function[BCClass2::ParamsType::params.Function.Name] = BCClass2::ParamsType::params.Function.OjedaMMS;
  Function[SolnType::ParamsType::params.gasModel] = gasModelDict;


  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCIn[BCEulerFunction_mitState2DParams<TraitsSizeNavierStokes>::params.Function] = Function;
  BCIn[BCEulerFunction_mitState2DParams<TraitsSizeNavierStokes>::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;

  BCBoundaryGroups["BCIn"] = {0,1,2,3};

#else

  Real s0 = log(  p0/ pow(rho0, gamma) );
  Real H0 =  gas.enthalpy(rho0,t0) + 0.5*(u0*u0+v0*v0);

  PyDict BCIn;

  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = s0;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = H0;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = atan(v0/u0);

  PyDict BCOut;

  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = p0;


  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;
  PyBCList["BCSym"] = BCSymmetry;

  BCBoundaryGroups["BCIn"] = {3};
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCSym"] = {0,2};
#endif
#endif

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};


  // Set up Newton Solver
  PyDict NewtonSolverDict, LinSolverDict, NewtonLineUpdateDict, NonLinearSolverDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
#else
  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
#endif

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  string filename_base = "/home2/PROJECTIONPAPER/NS/DG/DG";

  string filename2 = filename_base + "_Errors.txt";
  fstream foutsol;


  int ii, jj;

  int powermin = 3;
  int powermax = 7;
//
  int ordermin = 1;
  int ordermax = 3;

  for (int order = ordermin; order <= ordermax; order++)
  {

  // loop over grid resolution: 2^power
    for (int power = powermin; power <= powermax; power++)
    {

      jj = pow( 2, power )+1;
      ii = jj; //small grid!

      string filename = filename_base + "_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);

      foutsol.open( filename2, fstream::app );

      // grid:
      XField2D_Box_Triangle_X1 xfld(ii, jj);
      ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rho0, u0, v0, p0) );

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

//      const int nDOFPDE = qfld.nDOF();
//      const int nDOFLIFT = rfld.nDOF();

      // Set the initial condition
//      qfld = q0;

      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous<NDSolnType>(soln, {0}), (xfld, qfld) );
      for (int i=0; i<rfld.nDOF(); i++) rfld.DOF(i) = 0.0;
      lgfld = 0;

      //////////////////////////
      //SOLVE SYSTEM
      //////////////////////////

      QuadratureOrder quadratureOrder( xfld, 3*order+1 );

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_L2, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups);

      bool solved;
#if 1

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      solved = Solver.solve(ini,sln).converged;
      BOOST_REQUIRE(solved);
      PrimalEqSet.setSolutionField(sln);
#else
      typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD2, TopoD2>> AlgebraicEquationSet_PTCClass;

      Real invCFL = 100;
      Real invCFL_min = 0.0;
      Real invCFL_max = 1000.0;
      Real CFLDecreaseFactor = 0.1;
      Real CFLIncreaseFactor = 2.0;
      Real CFLPartialStepDecreaseFactor = 0.5;

      AlgebraicEquationSet_PTCClass AlgEqSetPTC(xfld, qfld, pde, quadratureOrder, {0}, PrimalEqSet);

      PseudoTime<SystemMatrixClass>
         PTC(invCFL, invCFL_max, invCFL_min, CFLDecreaseFactor, CFLIncreaseFactor, CFLPartialStepDecreaseFactor,
             NonLinearSolverDict, AlgEqSetPTC, true);


      solved = PTC.iterate(200);

#endif

      if (solved)
      {

      // Monitor Velocity Error
      Real PressureOutput = 0.0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( fcnErr, PressureOutput ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real HExact = 0.142091576773162;
      Real outputErr = fabs(HExact - PressureOutput);

      // Monitor Velocity Error
      ArrayQ L2err = 0.0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( errorL2Integrand, L2err ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );


#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      //        fstream fout( outputfile, fstream::out );
      cout << "P = " << order << " ii = " << ii
          << ": Pressure Output = " << std::setprecision(12) <<  PressureOutput << "\n";

      foutsol << order << " " << ii << std::setprecision(15);
      foutsol << " " << sqrt(L2err[0]);
      foutsol << " " << sqrt(L2err[1]);
      foutsol << " " << sqrt(L2err[2]);
      foutsol << " " << sqrt(L2err[3]);
      foutsol << " " << outputErr << "\n";

#endif

      foutsol.close();
      //ADJOINT SOLVE

//      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
//      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, order, BasisFunctionCategory_Legendre);
//      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
//      mufld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
//
//      SystemVectorClass rhs(PrimalEqSet.vectorEqSize());
//      rhs = 0;
//
//      typedef SurrealS<4> SurrealClass;
//      IntegrateCellGroups<TopoD2>::integrate( JacobianFunctionalCell_DGBR2<SurrealClass>( fcnErr, rhs(0) ),
//                                              xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
//
//      // adjoint solve
//      SLA::LinearSolver<SystemMatrixClass> solverAdj(NewtonSolverDict, PrimalEqSet, SLA::TransposeSolve);
//
//      SystemVectorClass adj(PrimalEqSet.vectorStateSize());
//      solverAdj.solve(rhs, adj);
//
//      PrimalEqSet.setAdjointField(adj, wfld, sfld, mufld);


#if 1
// Tecplot dump grid
      string filenameprimal = filename + ".plt";
      string filenameadjoint = filename + "_adj.plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filenameprimal );
//      output_Tecplot( wfld, filenameadjoint );
#endif
      }
      else
      {
#if 1
      // Tecplot dump grid
      string filename = "tmp/DGBR2_OjedaMMS_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += "_UNCONVERGED.plt";
      output_Tecplot( qfld, filename );
      std::cout << "Wrote Tecplot File\n";
#endif
      }

    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
