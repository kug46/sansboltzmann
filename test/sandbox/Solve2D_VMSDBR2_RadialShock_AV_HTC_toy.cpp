// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_VMSDBR2_RadialShock_AV_HTC_toy
// testing of 2-D VMSD-BR2 with Euler solved with HTC

#define SANS_VERBOSE

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <chrono>
#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_BoundaryTrace.h"
#include "Field/Function/FunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Field/output_Tecplot.h"

#include "pde/BCParameters.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/SolutionFunction_Euler2D.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/SourceOnly/PDESourceOnly2D.h"
#include "pde/SourceOnly/BCSourceOnly2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/BCParameters.h"
#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h" // HACK: I need to work out how to sort out homotopy based stuff

#include "Discretization/VMSDBR2/AlgebraicEquationSet_VMSD_BR2.h"
#include "Discretization/VMSDBR2/JacobianCell_VMSD_BR2.h"
#include "Discretization/VMSDBR2/JacobianInteriorTrace_VMSD_BR2.h"
#include "Discretization/VMSDBR2/IntegrandBoundaryTrace_OutputWeightRsd_VMSD_BR2.h"
#include "Discretization/VMSD/FunctionalCell_VMSD.h"
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "SolutionTrek/Continuation/Homotopy/Homotopy.h"
#include "SolutionTrek/Continuation/Homotopy/PDEHomotopy.h"

#include "Field/output_Tecplot_PDE.h"
#include "Field/output_Tecplot.h"

#include "unit/UnitGrids/XField2D_Annulus_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_VMSDBR2_HOW5_VI2_HTC_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HTC_Solve )
{
  /////////////////////////////////////////////////////////////////////////////////////////
  // PDE definitions
  /////////////////////////////////////////////////////////////////////////////////////////
//  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;
  typedef QTypeEntropy QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;

  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;

  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;

//  typedef AVSensor_Source2D_PressureGrad<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;
  typedef AVSensor_Source2D_VMSD_EntropyInequality<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PrimaryPDE;
  typedef PDENDConvertSpace<PhysD2, PrimaryPDE> NDPrimaryPDE;

  typedef PDESourceOnly2D<PrimaryPDE> AuxiliaryPDE;
  typedef PDENDConvertSpace<PhysD2, AuxiliaryPDE> NDAuxiliaryPDE;

  typedef PDEHomotopy<NDPrimaryPDE, NDAuxiliaryPDE> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;
  /////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////
  // Not sure yet how this factors into our source layout
  /////////////////////////////////////////////////////////////////////////////////////////
  typedef BCNone<PhysD2, PrimaryPDE::N> BCClassRawNone;
  typedef BCNDConvertSpace<PhysD2, BCClassRawNone> BCClassNone;

  typedef BCmitAVSensor2D
          <
            BCTypeFlux_mitState,
            BCEuler2D
            <
              BCTypeInflowSubsonic_PtTta_mitState,
              PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV>
            >
          > BCClassRawInflowSubsonic_PtTta_mitState;
  typedef BCNDConvertSpace<PhysD2, BCClassRawInflowSubsonic_PtTta_mitState> BCClassInflowSubsonic_PtTta_mitState;

  typedef BCmitAVSensor2D
          <
            BCTypeSymmetry_mitState,
            BCEuler2D
            <
              BCTypeSymmetry_mitState,
              PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV>
            >
          > BCClassRawSymmetry_mitState;
  typedef BCNDConvertSpace<PhysD2, BCClassRawSymmetry_mitState> BCClassSymmetry_mitState;

  typedef BCmitAVSensor2D
          <
            BCTypeFlux_mitState,
            BCEuler2D
            <
              BCTypeOutflowSubsonic_Pressure_mitState,
              PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV>
            >
          > BCClassRawOutflowSubsonic_Pressure_mitState;
  typedef BCNDConvertSpace<PhysD2, BCClassRawOutflowSubsonic_Pressure_mitState> BCClassOutflowSubsonic_Pressure_mitState;

  typedef BCmitAVSensor2D
          <
            BCTypeFlux_mitState,
            BCEuler2D
            <
              BCTypeFullStateRadialInflow_mitState,
              PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV>
            >
          > BCClassRawFullStateRadialInflow_mitState;
  typedef BCNDConvertSpace<PhysD2, BCClassRawFullStateRadialInflow_mitState> BCClassFullStateRadialInflow_mitState;

  typedef BCHomotopy<NDPrimaryPDE, NDAuxiliaryPDE, BCClassNone, BCClassNone> NDBCNone;
  typedef BCHomotopy<NDPrimaryPDE, NDAuxiliaryPDE, BCClassInflowSubsonic_PtTta_mitState, BCClassNone> NDBCInflowSubsonic_PtTta_mitState;
  typedef BCHomotopy<NDPrimaryPDE, NDAuxiliaryPDE, BCClassSymmetry_mitState, BCClassNone> NDBCClassSymmetry_mitState;
  typedef BCHomotopy<NDPrimaryPDE, NDAuxiliaryPDE, BCClassOutflowSubsonic_Pressure_mitState, BCClassNone> NDBCClassOutflowSubsonic_Pressure_mitState;
  typedef BCHomotopy<NDPrimaryPDE, NDAuxiliaryPDE, BCClassFullStateRadialInflow_mitState, BCClassNone> NDBCClassFullStateRadialInflow_mitState;
  typedef boost::mpl::vector5<NDBCNone,
                              NDBCInflowSubsonic_PtTta_mitState,
                              NDBCClassSymmetry_mitState,
                              NDBCClassOutflowSubsonic_Pressure_mitState,
                              NDBCClassFullStateRadialInflow_mitState> BCVector;
  /////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////
  // Algebraic equation set definition
  /////////////////////////////////////////////////////////////////////////////////////////
  typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef GenHField_CG<PhysD2, TopoD2> GenHFieldType;
  typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                         XField<PhysD2, TopoD2>>::type ParamFieldTupleType;
  typedef AlgebraicEquationSet_VMSD_BR2<NDPDEClass, BCNDConvertHomotopy, BCVector,
                                        AlgEqSetTraits_Sparse, ParamFieldTupleType> PDEPrimalEquationSetClass;
  typedef PDEPrimalEquationSetClass::BCParams BCParams;
  /////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up integrand L2 Error
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef SolutionFunction_Euler2D_RadialShock<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDExactSolutionClass;
  ////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////
  // MPI communicator
  /////////////////////////////////////////////////////////////////////////////////////////
  mpi::communicator world;
  /////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////
  // Set up non-linear solver params for corrector step
  /////////////////////////////////////////////////////////////////////////////////////////
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, LineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 10;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);
  /////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////
  // General parameters
  /////////////////////////////////////////////////////////////////////////////////////////
  enum ResidualNormType ResNormType = ResidualNorm_L2_DOFWeighted;
  std::vector<Real> tol = {2.0e-14, 2.0e-14, 2.0e-14};
  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  /////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  RoeEntropyFix entropyFix = eIsmailRoeHartenFix;
  EulerArtViscosity viscosityType = EulerArtViscosity::eLaplaceViscosityEnergy;
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = false;
  PyDict gasModelDict;
  Real gamma, R;
  gamma = 1.4;
  R = 1.0;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Define inflow data
  ////////////////////////////////////////////////////////////////////////////////////////
  // reference state (freestream)
  const Real Mach = 2.0;
  const Real rhoRef = 1;                            // density scale

  const Real pRef = 1.0;
  const Real tRef = pRef/rhoRef/R;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;

  const Real aoaRef = M_PI / 4.0;
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  // Lets calculate our inflow enthalpy since this should be constant
  const Real inflowEntropy = log( pRef / pow(rhoRef,gamma) );
  const Real TtSpec = tRef * (1 + (gamma-1)/2 * Mach*Mach);
  const Real PtSpec = pRef * pow(TtSpec/ tRef, gamma/(gamma-1));
  const Real rhotSpec = PtSpec / (R*TtSpec);

  std::cout << "rho:  " << rhoRef << "[kg/m3]" << std::endl
            << "V:    " << qRef   << "[m/s]"   << std::endl
            << "T:    " << tRef   << "[K]"     << std::endl
            << "Tt:   " << TtSpec << "[K]"     << std::endl
            << "Pt:   " << PtSpec << "[K]"     << std::endl
            << "rhot: " << rhotSpec << "kg/m3]" << std::endl
            << "sRef: " << inflowEntropy << "[-]"     << std::endl;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Define boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  // Define inflow BC
  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.FullStateRadialInflow_mitState;
  BCInflow[BCEuler2DParams<BCTypeFullStateRadialInflow_mitState>::params.rho] = rhoRef;
  BCInflow[BCEuler2DParams<BCTypeFullStateRadialInflow_mitState>::params.V] = qRef;
  BCInflow[BCEuler2DParams<BCTypeFullStateRadialInflow_mitState>::params.T] = tRef;
  BCInflow[BCEuler2DParams<BCTypeFullStateRadialInflow_mitState>::params.Characteristic] = false;

  // Define wall BC
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  // Define outflow BC
  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflow[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = 0.47 * PtSpec;

  // Define BC list
  PyDict PyBCList;
  PyBCList["Outflow"] = BCOutflow;
  PyBCList["Symmetry"] = BCSymmetry;
  PyBCList["Inflow"] = BCInflow;
  BCParams::checkInputs(PyBCList);

  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["Inflow"] = {XField2D_Annulus_Triangle_X1::iTop};
  BCBoundaryGroups["Outflow"] = {XField2D_Annulus_Triangle_X1::iBottom};
  BCBoundaryGroups["Symmetry"] = {XField2D_Annulus_Triangle_X1::iLeft, XField2D_Annulus_Triangle_X1::iRight};
  std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
  ////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////
  // Create tecplot file to put total enthalpy errors
  ////////////////////////////////////////////////////////////////////////////////////////
  std::string filename_base = "tmp/Radial_Shock/EntropyInequality_Positivity/";
  boost::filesystem::create_directories(filename_base);
  std::ofstream resultFile(filename_base + "Output.dat", std::ios::out);
  resultFile << "VARIABLES=";
  resultFile << "\"Order\"";
  resultFile << ", \"DOF\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error v1\"";
  resultFile << ", \"L2 error v2 \"";
  resultFile << ", \"L2 error v3\"";
  resultFile << ", \"L2 error v4\"";
  resultFile << ", \"Output error\"";
  resultFile << ", \"Solve Time /ms\"";
  resultFile << ", \"Converged\"";
  resultFile << std::endl;
  resultFile << std::setprecision(16) << std::scientific;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Grid loop
  ////////////////////////////////////////////////////////////////////////////////////////
  for (int grid_index = 0; grid_index <= 4; grid_index++)
  {
    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directory(base_dir);

    // Grid
    int ii = 4 * std::pow(2, grid_index);
    int jj = ii;
    Real R1 = 1.0;
    Real h = 1.0;
    Real thetamin = 0.0;
    Real thetamax = 90.0; // XField2D_Annulus_Triangle_X1 appears to take in degrees
    if (world.rank() == 0) std::cout << "Generating grid: " << ii << "x" << jj << std::endl;
    XField2D_Annulus_Triangle_X1 xfld(ii, jj, R1, h, thetamin,  thetamax );
    if (world.rank() == 0) std::cout << "Generated" << std::endl;

    std::vector<int> cellGroups;
    for ( int i = 0; i < xfld.nCellGroups(); i++)
      cellGroups.push_back(i);
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    GenHFieldType hfld(xfld);

    for (int order = 1; order <= 3; order++)
    {
      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our Euler PDE
      ////////////////////////////////////////////////////////////////////////////////////////
      PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, viscosityType,
                              gas, interp, entropyFix);
      // Sensor equation terms
      Sensor sensor(pdeEulerAV);
      SensorAdvectiveFlux sensor_adv(0.0, 0.0);
      SensorViscousFlux sensor_visc(order);
//      SensorViscousFlux sensor_visc;
      SensorSource sensor_source(order, sensor);
      // AV PDE with sensor equation
      NDPrimaryPDE primarypde(sensor_adv, sensor_visc, sensor_source, isSteady,
                              order, hasSpaceTimeDiffusion, viscosityType,
                              gas, interp, entropyFix);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our other PDE
      ////////////////////////////////////////////////////////////////////////////////////////
      ArrayQ q0 = primarypde.setDOFFrom( AVVariable<DensityVelocityTemperature2D, Real>({{rhoRef, uRef, vRef, tRef}, 1.0}) );
      NDAuxiliaryPDE auxiliarypde(q0, sensor_adv, sensor_visc, sensor_source, isSteady,
                                  order, hasSpaceTimeDiffusion, viscosityType,
                                  gas, interp, entropyFix);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our Homotopy PDE
      ////////////////////////////////////////////////////////////////////////////////////////
      Real lambda = 0.0;
      NDPDEClass pde(lambda, primarypde, auxiliarypde);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // VMSD-BR2 solution field
      ////////////////////////////////////////////////////////////////////////////////////////
      int porder = order;
      // cell solution
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Lagrange);
      qfld = q0;
      // perturbation
      Field_EG_Cell<PhysD2, TopoD2, ArrayQ> qpfld(qfld, porder, BasisFunctionCategory_Lagrange);
      qpfld = 0;
      // auxiliary variable
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, porder, BasisFunctionCategory_Lagrange);
      rfld = 0;
      // auxiliary variable on boundary
      FieldLift_DG_BoundaryTrace<PhysD2, TopoD2, VectorArrayQ> rbfld(xfld, order, BasisFunctionCategory_Lagrange);
      rbfld = 0;
      // Lagrange multiplier
      std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Lagrange, LG_BGroup_list);
      lgfld = 0;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // VMSD-BR2 discretization
      ////////////////////////////////////////////////////////////////////////////////////////
      bool isStaticCondensed = true;
      DiscretizationVMSD stab(VMSDp, isStaticCondensed);
      stab.setNitscheOrder(order);
      const int quadOrder = 3*(order + 2)-1; // 14 at order=3
      QuadratureOrder quadratureOrder( xfld, quadOrder );
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create the spatial discretization
      ////////////////////////////////////////////////////////////////////////////////////////
      ParamFieldTupleType hxfld = (hfld, xfld);
      PDEPrimalEquationSetClass AlgEqSet(hxfld, qfld, qpfld, rfld,  rbfld, lgfld,
                                         pde, stab, quadratureOrder, ResNormType, tol,
                                         cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, lambda);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create the Homotopy continuation class
      ////////////////////////////////////////////////////////////////////////////////////////
      Homotopy<PhysD2, NDPDEClass, AlgEqSetTraits_Sparse, ParamFieldTupleType>
         HTC(hxfld, qfld, NonLinearSolverDict, pde, cellGroups, AlgEqSet, lambda, 1000, true);
      ////////////////////////////////////////////////////////////////////////////////////////

//      ////////////////////////////////////////////////////////////////////////////////////////
//      // Output initial Jacobian
//      ////////////////////////////////////////////////////////////////////////////////////////
//      typename PDEPrimalEquationSetClass::SystemNonZeroPattern nnz(AlgEqSet.matrixSize());
//      AlgEqSet.jacobian(nnz);
//      typename PDEPrimalEquationSetClass::SystemMatrix jac(nnz);
//      AlgEqSet.jacobian(jac);
//      {
//        std::string filename = "tmp/jac_initial.mtx";
//        std::fstream fout(filename, std::fstream::out);
//        WriteMatrixMarketFile(jac, fout);
//      }
//      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Iterate the homotopy to convergence
      ////////////////////////////////////////////////////////////////////////////////////////
      bool converged = false;
      auto t1 = std::chrono::high_resolution_clock::now();
      std::cout << "Starting Homotopy" << std::endl;
      try
      {
        converged = HTC.solve();
      }
      catch (...)
      {
        converged = false;
      }
      auto t2 = std::chrono::high_resolution_clock::now();
      std::cout << "Grid: " << grid_index << ", P = " << order << ", converged: " << (converged ? "true" : "false")
                << ", time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << "ms" << std::endl;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // L2 solution error
      ////////////////////////////////////////////////////////////////////////////////////////
      typedef OutputCell_SolutionErrorSquared<PDEBaseClass, SolutionExact> ErrorClass;
      typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
      typedef IntegrandCell_VMSD_Output<NDErrorClass, NDPrimaryPDE> ErrorIntegrandClass;

      Real rstar = R1 + 0.5*h;
      Real Mstar = 2.456;
      NDExactSolutionClass solnExact(gas, rhotSpec, Mstar, TtSpec, rstar);
      NDErrorClass fcnError(solnExact);
      ErrorIntegrandClass errorIntegrand(primarypde, fcnError, cellGroups);
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_VMSD( errorIntegrand, SquareError ),
          xfld, (qfld, qpfld),
          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm0 = sqrt(SquareError(0));
      Real norm1 = sqrt(SquareError(1));
      Real norm2 = sqrt(SquareError(2));
      Real norm3 = sqrt(SquareError(3));
      std::cout << "  L2 solution error: " << norm0 << " " << norm1 << " " << norm2 << " " << norm3 << std::endl;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Output functional
      ////////////////////////////////////////////////////////////////////////////////////////
      typedef OutputEuler2D_Entropy<NDPrimaryPDE> OutputSolutionSquared;
      typedef OutputNDConvertSpace<PhysD2, OutputSolutionSquared> NDOutputSolutionSquared;
      typedef IntegrandCell_VMSD_Output<NDOutputSolutionSquared, NDPrimaryPDE> IntegrandOutputSolutionSquared;

      NDOutputSolutionSquared outputSolutionSquared(primarypde);
      IntegrandOutputSolutionSquared fcnOutputSolutionSquared(primarypde, outputSolutionSquared, cellGroups);

      Real SolutionSquared = 0.0;
      const Real SolutionSquaredExact =  0.25 * M_PI * (rstar*rstar - R1*R1) * 0.0
                                       + 0.25 * M_PI * ((R1+h)*(R1+h) - rstar*rstar)*2.64003863402411e-01;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_VMSD( fcnOutputSolutionSquared, SolutionSquared ),
          hxfld, (qfld, qpfld),
          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      std::cout << "  Output Error: " << abs(SolutionSquared - SolutionSquaredExact) << std::endl;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Output \bar{q}
      ////////////////////////////////////////////////////////////////////////////////////////
      {
        std::string filename = "Primal_"
                               + Type2String<QType>::str()
                               + "_P"
                               + std::to_string(order)
                               + "_G"
                               + std::to_string(grid_index)
                               + ".dat";
        output_Tecplot( primarypde, hxfld, qfld, filename_base + filename );
      }
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Output \bar{q} + \hat{q}
      ////////////////////////////////////////////////////////////////////////////////////////
      {
        int orderCombined = std::max(order,porder);
        Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldCombined(xfld, orderCombined, BasisFunctionCategory_Lagrange);
        Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qpfld_temp(xfld, orderCombined, BasisFunctionCategory_Lagrange);
        FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld_temp(xfld, orderCombined, BasisFunctionCategory_Lagrange);
        qfld.projectTo(qfldCombined);
        qpfld.projectTo(qpfld_temp);
        rfld.projectTo(rfld_temp);
        for (int i = 0; i < qfldCombined.nDOF(); i++)
        {
          qfldCombined.DOF(i) += qpfld_temp.DOF(i);
        }
        std::string filename = "Combined_"
                               + Type2String<QType>::str()
                               + "_P"
                               + std::to_string(order)
                               + "_G"
                               + std::to_string(grid_index)
                               + ".dat";
        output_Tecplot( primarypde, hxfld, qfldCombined, rfld_temp, filename_base + filename );
      }
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Output \hat{q}
      ////////////////////////////////////////////////////////////////////////////////////////
      {
        std::string filename = "Perturbation_"
                               + Type2String<QType>::str()
                               + "_P"
                               + std::to_string(order)
                               + "_G"
                               + std::to_string(grid_index)
                               + ".dat";
        output_Tecplot( qpfld, filename_base + filename );
      }
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Output Error to tecplot file
      ////////////////////////////////////////////////////////////////////////////////////////
      resultFile << " " << order;
      resultFile << " " << qfld.nDOF();
      resultFile << " " << 1.0/sqrt(qfld.nDOF());
      resultFile << " " << norm0;
      resultFile << " " << norm1;
      resultFile << " " << norm2;
      resultFile << " " << norm3;
      resultFile << " " << abs(SolutionSquared - SolutionSquaredExact);
      resultFile << " " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
      resultFile << " " << (converged ? "true" : "false");
      resultFile << std::endl;
      ////////////////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////////////////
    }
    ////////////////////////////////////////////////////////////////////////////////////////
  }
  ////////////////////////////////////////////////////////////////////////////////////////
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
