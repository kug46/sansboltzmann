// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// #define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <fstream>

#include "pde/AnalyticFunction/ScalarFunction4D.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Adaptation/MOESS/SolverInterface_L2Project.h"
#include "Adaptation/MeshAdapter.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Field/FieldSpacetime_CG_Cell.h"
#include "Field/FieldSpacetime_CG_BoundaryTrace.h"

#include "Field/FieldSpacetime_DG_Cell.h"
#include "Field/FieldSpacetime_DG_BoundaryTrace.h"

#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/NDConvert/OutputNDConvertSpace4D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime3D.h"
#include "pde/NDConvert/FunctionNDConvertSpace4D.h"


#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve4D_CG_AD_L2_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve4D_CG_AD_L2_test )
{

  typedef PhysD4 PhysDim;
  typedef TopoD4 TopoDim;

#define USE_CG 1

  mpi::communicator world;

  // specify order and grid power for sweep
  int orderL= 1;
  int orderH= 3;
  int N_L= 2;
  int N_H= 6;
  // specify mesher
  std::string mesher= "avro";

  // hand in CLI arguments
  int argc= boost::unit_test::framework::master_test_suite().argc;
  char **argv= boost::unit_test::framework::master_test_suite().argv;

  if (argc == 4)
  {
    orderL= orderH = std::stoi(argv[1]);
    N_L= N_H = std::stoi(argv[2]);
    mesher= std::string(argv[3]);
  }

  // checking tolerance
  std::array<Real,1> tol= {{1e-11}};

  // nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name]= SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide]= SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering]= SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel]= 2;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name]= SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner]= PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver]= SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance]= 1e-9;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance]= 1e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations]= 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart]= 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner]= PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose]= true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues]= false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile]= "";
  // PETSCDict[SLA::PETScSolverParam::params.FilenameBase]= filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance]= 1e-10;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance]= 1e-10;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= PETScDictAdjoint;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver]= SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver]= MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver]= SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= UMFPACKDict;
#endif

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel]= MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity]= MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement]= false;
  MOESSDict[MOESSParams::params.ImpliedMetric]= MOESSParams::params.ImpliedMetric.Optimized;
  MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction]= 1.0;
  MOESSDict[MOESSParams::params.MetricOptimization]= MOESSParams::params.MetricOptimization.SANS;
#if USE_CG
  //MOESSDict[MOESSParams::params.LocalSolve]= MOESSParams::params.LocalSolve.Element;
#endif

  PyDict MesherDict;
#ifdef SANS_AVRO
  if (mesher == "avro")
  {
    MesherDict[MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.Name]= MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.avro;
    MesherDict[avroParams::params.Curved]= false;
  }
  else
#endif
    BOOST_REQUIRE_MESSAGE(false, "Unknown mesh generator.");


  //--------SOLVE LOOP--------

  for (int order= orderL; order <= orderH; order++)
  {
#if 1
    typedef SolnNDConvertSpaceTime<PhysD3, ScalarFunction4D_Monomial> SolutionExact;
    std::vector<Real> coeffs= {0.1, 0.2, 0.3, 0.4};
    std::vector<int> E= {3, 1, 2, 4};
    SolutionExact solnExact(1., coeffs, E);
    std::string func= "monomial";
#elif 0
    typedef SolnNDConvertSpaceTime<PhysD3,ScalarFunction4D_BoundaryLayer> SolutionExact;
    Real epsilon= 0.01; // originally 0.01
    Real beta= pow(2.,order+1);
    SolutionExact solnExact(epsilon, beta, order);
    std::string func= "BL";
#elif 0
    typedef SolnNDConvertSpaceTime<PhysD3,ScalarFunction4D_QuadrupleBL> SolutionExact;
    Real nu= 0.01;
    SolutionExact solnExact(1, 1, 1, 1, nu);
    std::string func= "QuadBL";
#endif

#if USE_CG
    typedef SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact, Field_CG_Cell> SolverInterfaceClass;
#else
    typedef SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact, Field_DG_Cell> SolverInterfaceClass;
#endif

    for (int NOI= N_L; NOI <= N_H; NOI++)
    {
      int nk= NOI;

      //       Real targetCost= pow(NOI, 4.)*(order+1)*(order+2)*(order+3)*(order+4);
      //
      //       Real nDOFperCell_DG= (order+1)*(order+2)*(order+3)*(order+4)/24;
      // #if USE_CG
      //       // equiVol= sqrt(2.0)/12;
      //       // targetComp= targetCost*equiVol/nDOFperCell_DG;
      //       // targetCost= targetComp*nDOFperCell_CG/equiVol;
      //       // SANS_DEVELOPER_EXCEPTION("implement");
      //       Real nDOFperCell_CG= nDOFperCell_DG;
      //       // nDOFperCell_CG -= (4 - 1./5); // the node dofs are shared by 20
      //       nDOFperCell_CG -= (5 - 1./18); // the node dofs are shared by 90
      //       nDOFperCell_CG -= (10 - 1./2.)*std::max(0,(order-1)); // if there are edge dofs they are shared by 20
      //       nDOFperCell_CG -= (10 - 5./6.)*std::max(0,(order-1)); // if there are area dofs they are shared by 6
      //       nDOFperCell_CG -= (5 - 5./12.)*std::max(0,(order-1)*(order-2)/2); // if there are face dofs they are shared by 2
      //
      //       targetCost= targetCost*nDOFperCell_CG/nDOFperCell_DG;
      // #endif

      // put together filename
      const int string_pad= 6;
      std::string int_pad= std::string(string_pad - std::to_string(nk).length(), '0') + std::to_string(nk) + "k";

#if USE_CG
      std::string G= "CG";
#else
      std::string G= "DG";
#endif

      std::string filename_base= "tmp/L2Project4DST_" + G + "/";
      std::string filename_case= filename_base + "n" + stringify(nk) + "p"
        + stringify(order) + "_" + mesher + "_MOESS_" + func + "/";

      // create directory structure if it doesnt exist
      boost::filesystem::path base_dir(filename_base);
      if ( not boost::filesystem::exists(base_dir) )
        boost::filesystem::create_directories(filename_base);

      // create directory structure if it doesnt exist
      boost::filesystem::path case_dir(filename_case);
      if ( not boost::filesystem::exists(case_dir) )
        boost::filesystem::create_directories(filename_case);

      // address adapthist outfile
      std::string adapthist_filename= filename_case + "test.adapthist";
      fstream fadapthist;
      if ( world.rank() == 0 )
      {
        fadapthist.open( adapthist_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
      }

      // address outfile
      std::string convhist_filename= filename_base + "test.convhist";
      fstream convhist;

      bool fileStarted= boost::filesystem::exists(convhist_filename);

      if ( world.rank() == 0 )
      {
        convhist.open( convhist_filename, fstream::app );
        BOOST_REQUIRE_MESSAGE(convhist.good(), "Error opening file: " + convhist_filename);

        // begin to write outfile
        if (!fileStarted)
        {
          convhist << "VARIABLES=";
          convhist << "\"p\"";
          convhist << ", \"DOFrequested\"";
          convhist << ", \"1/pow(DOF, 1.0/4.0)\"";
          convhist << ", \"Nelem\"";
          convhist << ", \"Ndof\"";
          convhist << ", \"L2 error\"";
          convhist << std::endl;
        }

        convhist << std::setprecision(16) << std::scientific;
      }

      MesherDict[refineParams::params.FilenameBase]= filename_base;

      // create the initial mesh
      std::shared_ptr<XField<PhysD4, TopoD4>> pxfld;
      if (mesher == "avro")
      {
#ifdef SANS_AVRO
        avro::Context context;

        // ballpark N based on target request using right triangulation
        // nDOF= (N*p + 1)^4 for right triangulation ... N= (nDOF^(1/4) - 1)/p??
        int N= NOI;
        if (N < 2)
          N= 2;
        printf("\n\nN= %d\n\n", N);
        XField_KuhnFreudenthal<PhysD4, TopoD4> xfld0(world, {N, N, N, N});

        std::shared_ptr<avro::Model> model= std::make_shared<avro::Model>(xfld0.context(), "tesseract");
        model->addBody(xfld0.body_ptr() , false);

        // copy the mesh into the domain and attach the geometry
        pxfld= std::make_shared<XField_avro<PhysD4, TopoD4>>(xfld0, model);
#endif
      }
      else
        SANS_DEVELOPER_EXCEPTION("only avro provides 4d meshing so far...");

      std::vector<int> cellGroups= {0};
      // int total_iter= 0;

      // solve

#if USE_CG
      Field_CG_Cell<PhysDim,TopoDim,Real> qfld(*pxfld, order, BasisFunctionCategory_Lagrange);
#else
      Field_DG_Cell<PhysDim,TopoDim,Real> qfld(*pxfld, order, BasisFunctionCategory_Lagrange);
#endif // USE_CG

      // adjoint equation is integrated with 2 times adjoint order
      const int quadOrder= 2*(order+1);

      // perform L2 projection from solution on previous mesh
      qfld= 0;

      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(qfld, quadOrder,
                                                          cellGroups, tol, solnExact,
                                                          LinearSolverDict, false); // true to use elementwise_projection

      pInterface->solveGlobalPrimalProblem();

#ifdef SANS_MPI
      int nDOFtotal= 0;
      boost::mpi::reduce(*pxfld->comm(), qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0);

      // count the number of elements possessed by this processor
      int nElem= 0;
      for (int elem= 0; elem < pxfld->nElem(); elem++)
        if (pxfld->getCellGroupGlobal<Pentatope>(0).associativity(elem).rank() == world.rank())
          nElem++;

      // sum across all processes to get total number of elements
      int nElemtotal= 0;
      boost::mpi::reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>(), 0);
#else
      int nDOFtotal= qfld.nDOFpossessed();
      int nElemtotal= pxfld->nElem();
#endif // SANS_MPI

      // compute error estimates
      pInterface->computeErrorEstimates();

      Real L2error = pInterface->getOutput();

      if (world.rank() == 0 )
      {
        convhist << order;
        convhist << ", " << nDOFtotal;
        convhist << ", " << 1.0/pow(nDOFtotal, 1.0/((Real) PhysD4::D));
        convhist << ", " << nElemtotal;
        convhist << ", " << nDOFtotal;
        convhist << ", " << L2error;
        convhist << endl;

//        convhist << std::setw(5) << total_iter
//            << std::setw(10) << nDOFtotal
//            << std::setw(10) << nElemtotal
//            << std::setw(20) << std::setprecision(10) << std::scientific << L2error
//            << std::endl;
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
