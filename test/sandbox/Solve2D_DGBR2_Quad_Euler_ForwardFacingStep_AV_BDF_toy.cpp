// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_DGBR2_Quad_Euler_ForwardFacingStep_AV_BDF_toy
// Inviscid quasi-2D ForwardFacingStep with shock and artificial viscosity


//#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <chrono>
#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/BCEuler2D_Solution.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Output_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_Dispatch_DGBR2.h"

#include "Discretization/isValidState/SetValidStateCell.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/Function/FunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/output_Tecplot_PDE.h"
#include "Field/output_gnuplot.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "unit/UnitGrids/XField2D_ForwardStep_Quad_X1.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Quad_Euler_ForwardFacingStep_AV_BDF_toy )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Quad_Euler_ForwardFacingStep_AV_BDF_toy )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
//  typedef PDEEuler2D_Source<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> BCVector;

  typedef OutputEuler2D_Pressure<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_GenH_CG ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  //unsteady solver
  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, ParamFieldType> BDFClass;

  GlobalTime time(0);

  ////////////////////////////////////////////////////////////////////////////////////////
  // MPI Communicator
  ////////////////////////////////////////////////////////////////////////////////////////
  mpi::communicator world;
  ////////////////////////////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////////////////////////
  // Discretization parameters
  ////////////////////////////////////////////////////////////////////////////////////////
  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1.0e-13, 1.0e-13};
  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = true;
  Real viscousEtaParameter = 2*Hex::NEdge;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our gas model
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict gasModelDict;
  Real gamma = 1.4;
  Real R = 0.4;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  ////////////////////////////////////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////////////////////////////////////
  // Initial Condition
  ////////////////////////////////////////////////////////////////////////////////////////
  //typedef SolutionFunction_Euler2D_Const<TraitsSizeEulerArtificialViscosity, TraitsModelAV> ICSolnClass;
  //PyDict ICSoln;
  //ICSoln[BCEulerSolution2DParams<TraitsSizeEulerArtificialViscosity>::Function.Name] = "Const";
  //ICSoln[ICSolnClass::ParamsType::params.Function.Name] = ICSolnClass::ParamsType::params.Function.Const;
  //ICSoln[ICSolnClass::ParamsType::params.rho] = gamma;
  //ICSoln[ICSolnClass::ParamsType::params.u] = 3.0;
  //ICSoln[ICSolnClass::ParamsType::params.v] = 0.0;
  //ICSoln[ICSolnClass::ParamsType::params.p] = 1.0;
  //ICSoln[ICSolnClass::ParamsType::params.gasModel] = gasModelDict;
  //
  //SolnNDConvertSpace<PhysD2, ICSolnClass> solnExact(ICSoln);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict Inflow;
  Inflow[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitiveTemperature;
  Inflow[DensityVelocityTemperature2DParams::params.rho] = gamma;
  Inflow[DensityVelocityTemperature2DParams::params.u] = 3.0;
  Inflow[DensityVelocityTemperature2DParams::params.v] = 0.0;
  Inflow[DensityVelocityTemperature2DParams::params.t] = 1.0 / (gamma * R);

  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCBase;
  typedef BCmitAVSensor2D<BCTypeFullState_mitState, BCBase> BCClass;
  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCInflow[BCClass::ParamsType::params.Characteristic] = true;
  BCInflow[BCClass::ParamsType::params.StateVector] = Inflow;

  PyDict BCReflect;
  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Add BCs to BC list
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict PyBCList;
  PyBCList["BCReflect"] = BCReflect;
  PyBCList["BCInflow"] = BCInflow;
  PyBCList["None"] = BCNone;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check BCs!
  ////////////////////////////////////////////////////////////////////////////////////////
  BCParams::checkInputs(PyBCList);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Assign BCs to grid
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["BCReflect"] = {XField2D_ForwardStep_Quad_X1::iYmin, XField2D_ForwardStep_Quad_X1::iYmax};
  BCBoundaryGroups["BCInflow"] = {XField2D_ForwardStep_Quad_X1::iXmin};
  BCBoundaryGroups["None"] = {XField2D_ForwardStep_Quad_X1::iXmax};
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up Linear solver
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict LinearSolverDict;
#if defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict;

  if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up Line update
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict LineUpdateDict;
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 1.1;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up Newton solver
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict NewtonSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] =  true;
#if defined(SANS_PETSC)
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
#else
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up overall non-Linear solver
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict SolverContinuationDict, NonLinearSolverDict;
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonLinearSolverDict;
  ////////////////////////////////////////////////////////////////////////////////////////

  //--------Grid Density LOOP--------
  for (int grid_index = 3; grid_index <= 3; grid_index++)
  {

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set up grid here
    ////////////////////////////////////////////////////////////////////////////////////////
    int ii = 30*grid_index;
    int jj = 10*grid_index;
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfld =
            std::make_shared<XField2D_ForwardStep_Quad_X1>( world, ii, jj, 0, 3, 0, 1);

    std::vector<int> cellGroups;
    for ( int i = 0; i < pxfld->nCellGroups(); i++)
      cellGroups.push_back(i);

    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<GenHField_DG<PhysD2,TopoD2>> phfld =
        std::make_shared<GenHField_DG<PhysD2,TopoD2>>(*pxfld);
    ////////////////////////////////////////////////////////////////////////////////////////

    //Check the BC dictionary
    BCParams::checkInputs(PyBCList);

    std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);


    std::shared_ptr<SolutionClass> pGlobalSol;
    //std::shared_ptr<SolverInterfaceClass> pInterface;
    for (int order = 2; order <= 2; order++)
    {
      ////////////////////////////////////////////////////////////////////////////////////////
      // Where all our data gets saved to
      ////////////////////////////////////////////////////////////////////////////////////////
      std::string filename_base = "tmp/ForwardFacingStep";
      filename_base += std::to_string(order);
      filename_base += "_X";
      filename_base += std::to_string(ii);
      filename_base += "/";

      boost::filesystem::path base_dir(filename_base);
      if ( not boost::filesystem::exists(base_dir) )
        boost::filesystem::create_directory(base_dir);
      ////////////////////////////////////////////////////////////////////////////////////////

      int BDForder = order+1;

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our pde objects
      ////////////////////////////////////////////////////////////////////////////////////////
      PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp);
      // Sensor equation terms
      Sensor sensor(pdeEulerAV);
      SensorAdvectiveFlux sensor_adv(0.0, 0.0);
      SensorViscousFlux sensor_visc(order);
      SensorSource sensor_source(order, sensor);
      // AV PDE with sensor equation
      NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order,
                     hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Pressure output
      ////////////////////////////////////////////////////////////////////////////////////////
      NDOutputClass fcnOutput(pde);
      OutputIntegrandClass outputIntegrand(fcnOutput, cellGroups);
      ////////////////////////////////////////////////////////////////////////////////////////

      Real Tend = 4.;
      Real Nsteps = 500*pow(2,grid_index)*Tend;

      Real dt = Tend / Nsteps;

      ////////////////////////////////////////////////////////////////////////////////////////
      // Set up solution field data structures
      ////////////////////////////////////////////////////////////////////////////////////////
      QuadratureOrder quadratureOrder( *pxfld, 3*order+1 );
      //const int quadOrder = 3*order+1;

      std::vector<Real> tol = { 1e-11, 1e-11 };

      PyDict ParamDict;
      SolutionClass sol( (*phfld, *pxfld), pde, order, order,
                         BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                         active_boundaries, ParamDict, disc);
      sol.createLiftedQuantityField(0, BasisFunctionCategory_Legendre);
      ////////////////////////////////////////////////////////////////////////////////////////
      
      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial condition
      ////////////////////////////////////////////////////////////////////////////////////////
      ArrayQ q0 = pde.setDOFFrom( AVVariable<DensityVelocityPressure2D, Real>({{gamma, 3.0, 0.0, 1.0}, 0.0}) );
      sol.setSolution(q0);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      //Create solver interface
      ////////////////////////////////////////////////////////////////////////////////////////
      //pInterface = std::make_shared<SolverInterfaceClass>(sol, ResNormType, tol, quadOrder,
      //                                                    cellGroups, interiorTraceGroups,
      //                                                    PyBCList, BCBoundaryGroups,
      //                                                    SolverContinuationDict, LinearSolverDict,
      //                                                    outputIntegrand);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // SOLVE PRIMAL
      ////////////////////////////////////////////////////////////////////////////////////////
      //pInterface->solveGlobalPrimalProblem();
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Output primal
      ////////////////////////////////////////////////////////////////////////////////////////
      string filename = filename_base + "ForwardFacingStepInit_P";
      filename += to_string(order);
      filename += "_X";
      filename += to_string(ii);
      filename += ".plt";
      output_Tecplot( sol.primal.qfld, filename );
      ////////////////////////////////////////////////////////////////////////////////////////

      PyDict NonLinearSolverDict2;
      NonLinearSolverDict2[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

      // Create AlgebraicEquationSets
      PrimalEquationSetClass AlgEqSetSpace( sol.paramfld, sol.primal, sol.pliftedQuantityfld,
                                            pde, disc, quadratureOrder, ResNormType, tol,
                                            cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, time);

      // The BDF class
      BDFClass BDF( BDForder, dt, time, sol.paramfld, sol.primal.qfld, NonLinearSolverDict2, pde, quadratureOrder, {0}, AlgEqSetSpace );

      // Set IC
      time = -dt*Real(BDForder);
      int stepstart = 0;
      for (int j = BDForder-1; j >= 0; j--)
      {
        time += dt;
        BDF.setqfldPast(j, sol.primal.qfld);
      }

      auto time1 = std::chrono::high_resolution_clock::now();
      for (int step = stepstart; step < Nsteps; step++)
      {
        // March the solution in time
        BDF.march(1);

        if (step % 10 == 0)
        {
          // Tecplot dump grid
          string filename = filename_base + "ForwardFacingStepBDF_P";
          filename += to_string(order);
          filename += "_X";
          filename += to_string(ii);
          filename += "_t";
          filename += to_string(step);
          filename += ".plt";
          output_Tecplot( sol.primal.qfld, filename );
        }

      }

      auto time2 = std::chrono::high_resolution_clock::now();

      string filenameout2 = filename_base + "ForwardFacingStepBDF_time_P";
      filenameout2 += to_string(order);
      filenameout2 += "_X";
      filenameout2 += to_string(ii);
      filenameout2 += ".txt";
      fstream fout2( filenameout2, fstream::out );
      fout2 << "P solve: " << std::chrono::duration_cast<std::chrono::milliseconds>(time2-time1).count() << "ms" << std::endl;
      fout2.close();

    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
