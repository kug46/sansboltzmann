// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_TwoPhase_ST_btest
// Testing of the MOESS framework on a space-time two-phase problem

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/PDETwoPhase1D.h"
#include "pde/PorousMedia/BCTwoPhase1D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/OutputTwoPhase.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_TwoPhase_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_TwoPhase_ST_Triangle )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef QTypePrimitive_pnSw QType;
  typedef Q1D<QType, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, Real,
                              CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTwoPhase1DVector<PDEClass> BCVector;
  typedef BCTwoPhase1D<BCTypeFullState, PDEClass> BCClass;
  typedef BCTwoPhase1D<BCTypeTimeIC_Function, PDEClass> BCClassTimeIC_Function;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputTwoPhase_SaturationPower<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Grid
  int ii = 20;
  int jj = 10;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>(ii, jj, 0, 100, 0, 50, true);

  int order = 1;

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(0.1);
  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;

  NDPDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  Real pnL = 2500.0;
  Real SwL = 1.0;

  Real pnR = 2470.0;
  Real SwR = 0.1;

  // Initial solution
  ArrayQ qinit;
  PressureNonWet_SaturationWet<Real> qdata(0.5*(pnL + pnR), SwR);
  pde.setDOFFrom( qinit, qdata );

  // BCs
  PyDict LeftState;
  LeftState[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  LeftState[PressureNonWet_SaturationWet_Params::params.pn] = pnL;
  LeftState[PressureNonWet_SaturationWet_Params::params.Sw] = SwL;

  PyDict RightState;
  RightState[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  RightState[PressureNonWet_SaturationWet_Params::params.pn] = pnR;
  RightState[PressureNonWet_SaturationWet_Params::params.Sw] = SwR;

  PyDict BCLeft;
  BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCLeft[BCClass::ParamsType::params.StateVector] = LeftState;

  PyDict BCRight;
  BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCRight[BCClass::ParamsType::params.StateVector] = RightState;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

#if 0
  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  TimeIC[BCTwoPhase1DParams<BCTypeTimeIC>::params.StateVector] = RightState;
#else
  typedef SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, TraitsSizeTwoPhase> SolutionType;
  PyDict SolutionInit;
  SolutionInit[BCClassTimeIC_Function::ParamsType::params.Function.Name] = BCClassTimeIC_Function::ParamsType::params.Function.ThreeConstSaturation;
  SolutionInit[SolutionType::ParamsType::params.pn] = 0.5*(pnL + pnR);
  SolutionInit[SolutionType::ParamsType::params.Sw_left] = SwL;
  SolutionInit[SolutionType::ParamsType::params.Sw_mid] = SwR;
  SolutionInit[SolutionType::ParamsType::params.Sw_right] = SwR;
  SolutionInit[SolutionType::ParamsType::params.xLeftMid] = 20.0;
  SolutionInit[SolutionType::ParamsType::params.xMidRight] = 80.0;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC_Function;
  TimeIC[BCClassTimeIC_Function::ParamsType::params.Function] = SolutionInit;
#endif

  PyDict PyBCList;
  PyBCList["BCLeft"] = BCLeft;
  PyBCList["BCRight"] = BCRight;
  PyBCList["BCNone"] = BCNone;
  PyBCList["TimeIC"] = TimeIC;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TimeIC"] = {0};
  BCBoundaryGroups["BCRight"] = {1};
  BCBoundaryGroups["BCNone"] = {2};
  BCBoundaryGroups["BCLeft"] = {3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {5e-8, 5e-8};

  //Output functional
  NDOutputClass fcnOutput(pde, 0, 2);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;


  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);


  //--------ADAPTATION LOOP--------

  int maxIter = 15;
  Real targetCost = 10000;

  std::string filename_base = "tmp/";
  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  const int quadOrder = 2*(order + 2);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrand);

  //Set initial solution
  pGlobalSol->setSolution(qinit);

  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename = filename_base + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

  std::string adjfld_filename = filename_base + "adjfld_a0.plt";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
  }

  fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
