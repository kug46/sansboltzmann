// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve_DGBR2_Triangle_AD_btest
// testing of 2-D DG BR2 with Advection-Diffusion on triangles

//#define SANS_FULLTEST
//#define SANS_VERBOSE
//#define SANS_TIMING

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"

//#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "tools/linspace.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Triangle_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Triangle_AD )
{
  typedef ScalarFunction2D_Gaussian SolutionExact;
//  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_None > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  //typedef BCTypeLinearRobin_sansLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PDEPrimalEquationSetClass;
  typedef PDEPrimalEquationSetClass::BCParams BCParams;

  // PDE
  std::cout << "This has rectified data.....\n";
  Real a = 0.1;
  Real b = 0.1;

  AdvectiveFlux2D_Uniform adv( a, b );

  //Non-symmetric diffusion matrix
  Real nu = 1e-2;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_None source;

  Real x0 = 0; Real y0 = 0;
  Real sx = 0.75; Real sy = sx;
  Real amp = 500.0;

  // Create a solution dictionary
  PyDict solnArgs;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> SolutionNDClass;
  solnArgs[SolutionExact::ParamsType::params.a] = amp;
  solnArgs[SolutionExact::ParamsType::params.x0] = x0;
  solnArgs[SolutionExact::ParamsType::params.y0] = y0;
  solnArgs[SolutionExact::ParamsType::params.sx] = sx;
  solnArgs[SolutionExact::ParamsType::params.sy] = sy;

  SolutionNDClass solnExact( solnArgs );
  //
  NDPDEClass pde( adv, visc, source);

  // BC
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCParams::checkInputs( PyBCList );

  const std::vector<int> BoundaryGroups = { };


  GlobalTime time;
  time = 0.;
  // Gas Model
  Real dt = 0.001;
  Real tFinal = .5;
  int nSteps = tFinal / dt;

  //--------------------------------------------------------------------------------------
  mpi::communicator world;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  //--------------------------------------------------------------------------------------
  // Create grid
  //--------------------------------------------------------------------------------------
  int ii = 20;
  int jj = 20;

  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj, -2., 2., -2., 2., {{ true, true }} );

  int order_pde = 1;
  // Compiles till here ^^^^^^^^^

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate our Euler PDE field
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Lagrange);

  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> pde_rfld(xfld, order_pde, BasisFunctionCategory_Lagrange);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> pde_lgfld(xfld, order_pde, BasisFunctionCategory_Lagrange,
                                                           BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups));
  for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );
  //pde_rfld = 0;
  pde_lgfld = 0;

  //DGBR2 Discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc( 0, viscousEtaParameter );


    //--------------------------------------------------------------------------------------



  //--------------------------------------------------------------------------------------
  // Output IC gnuplot file
  //--------------------------------------------------------------------------------------
  string filenameStem_IC = "tmp/solnDG_Diffusion_step_0.plt";
   output_Tecplot( pde_qfld, filenameStem_IC );
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // AES Settings
  //--------------------------------------------------------------------------------------
  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = { 1e-6, 1e-6 };

  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Create our AES to solve our PDE
  //--------------------------------------------------------------------------------------

  std::vector<int> interiorTraceGroups = linspace(0,xfld.nInteriorTraceGroups()-1);


  PDEPrimalEquationSetClass PrincipalEqSet(xfld, pde_qfld, pde_rfld, pde_lgfld, pde, disc, quadratureOrder, ResidualNorm_Default, tol, {0},
                                           interiorTraceGroups, PyBCList, BCBoundaryGroups, time);

  //AlgebraicEquationSet_Class PrincipalEqSet(xfld, pde_qfld, pde, quadratureOrder, {0}, PrincipalEqSet);
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Construct Newton Solver
  //--------------------------------------------------------------------------------------
#if 0
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NewtonSolverParam::checkInputs(NewtonSolverDict);
  //NewtonSolver<SystemMatrixClass> Solver( PrincipalEqSet, NewtonSolverDict );
#endif

  // Set up Newton Solver
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict, LinSolverDict;
#ifdef SANS_PETSC
  // Take the Preconditioners from my Tandem Sphere case and just copy paste them here

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  LinSolverDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  LinSolverDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  LinSolverDict[SLA::PETScSolverParam::params.MaxIterations] = 2500;
  LinSolverDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  LinSolverDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  LinSolverDict[SLA::PETScSolverParam::params.Verbose] = true;
  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  LinSolverDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  std::cout << "Linear solver: PETSc" << std::endl;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  NewtonSolverParam::checkInputs( NewtonSolverDict );
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#else

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  std::cout << "Linear solver: UMFPACK" << std::endl;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  //AdjSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  // Check inputs
  NewtonSolverParam::checkInputs( NewtonSolverDict );
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#endif
  //--------------------------------------------------------------------------------------

  int RKorder = 4;
  int RKtype = 0;
  int RKstages = RKorder;

  //RK:
  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> RKClass;
  //temporal discretiation
  RKClass RK(RKorder, RKstages, RKtype, dt, time, xfld, pde_qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, PrincipalEqSet);
/*
  //--------------------------------------------------------------------------------------
  // Set initial guess from initial condition
  //--------------------------------------------------------------------------------------
  SystemVectorClass ini(PrincipalEqSet.vectorStateSize());
  SystemVectorClass sln(PrincipalEqSet.vectorStateSize());

  PrincipalEqSet.fillSystemVector(ini);
  sln = ini;
  */
  //--------------------------------------------------------------------------------------
#if 1
  for (int step = 1; step <= 300; step++)
  {
    std::cout << "############################# Time Step: " << step << "\n time is " << time << std::endl;
    // Advance solution
    RK.march(1);
    if (true)
    {
      // Tecplot Output
      //int FieldOrder = order_pde;
      string filename = "tmp/solnDG_Diffusion_step_";
      int f_time = step;
      filename += to_string(f_time);
      filename += ".plt";
       output_Tecplot( pde_qfld, filename );
    }
  }
#endif
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
