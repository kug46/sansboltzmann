// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGAdvective_Triangle_Euler_CubicSource_btest
// testing of 2-D DG Advective for Euler on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/EPIC/XField_PX.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "unit/UnitGrids/XField2D_Duct_Triangle_Lagrange_Xq.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

Real gaussian_bump(Real x)
{ return 0.0625*exp(-25*(x*x)); }

Real constant_height(Real x)
{ return 0.8; }

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGAdvective_Triangle_Euler_CubicSource_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGAdvective_Triangle_Bump10 )
{
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputEntropyErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquaredClass> NDOutputEntropyErrorSquaredClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputEntropyErrorSquaredClass> IntegrandSquareErrorClass;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD2, TopoD2>> AlgebraicEquationSet_PTCClass;

  mpi::communicator world;

  // PDE

  //int nSol = NDPDEClass::N;

  // const Real gamma = 1.4;
  // const Real R = 0.4;
  // GasModel gas(gamma, R);
  // NDPDEClass pde(gas, Euler_ResidInterp_Momentum);

  const Real gamma = 1.4;
  const Real R = 1.0;

  // reference state (freestream)
  const Real Mach = 0.3;

  // const Real lRef = 1;                              // length scale
  const Real rhoRef = 1.0;                            // density scale
//  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real pRef = 1.0;
  const Real cRef = sqrt(gamma*pRef/rhoRef);
  const Real qRef = Mach*cRef;
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
//  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature

  // stagnation temperature, pressure
  const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  const Real TtRef = tRef*Gamma;
  const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Momentum);

  // BC
  // Create a BC dictionary
  PyDict BCSymmetry;

#if 1
// PX STYLE BC:
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;
#else
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry;
#endif

  // PyDict BCIn;
  // BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt;
  //
  // BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.sSpec] = 0.0;
  // BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.HSpec] = 3.563;
  // BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.qtSpec] = 0;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aoaRef;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure>::params.pSpec] = pRef;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

//  const bool use_structured = false;
//  #define USE_STRUC

#ifdef USE_STRUC
  // Structured meshes
  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {2,3}; //bottom and top
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {0};
#else
  // Duct Mesher
  BCBoundaryGroups["BCSymmetry"] = {0,2}; //bottom and top
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {3};
#endif

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  //typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  //typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict, NonlinearSolverDict, SolverContinuationDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 500;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  //NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
  //                  = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 1e2;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;
#endif

  //SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  PseudoTimeParam::checkInputs(NonlinearSolverDict);

  Real sExact = 1.0;
  NDOutputEntropyErrorSquaredClass outEntropyError( pde, sExact );
  IntegrandSquareErrorClass fcnErr( outEntropyError, {0} );

  // norm data
//  Real normVec[10];   // L2 error
//  int indx = 0;

//  fstream fout( "tmp/cubicsource_results.txt", fstream::out );

  int ordermin = 1;
  int ordermax = 3;

  for (int order = ordermin; order <= ordermax; order++)
  {
    //indx = 0;

    // loop over grid resolution: 2^power
    int powermin = 0;
    int powermax = 4;

    for (int power = powermin; power <= powermax; power++)
    {
      cout << "P = " << order << " power = " << power << std::endl;

      int ii = 6*pow(2, power);
      int jj = 2*pow(2, power);

#ifdef USE_STRUC
      string filein = "grids/SmoothBump/SmoothBump_tri_ref";
      filein += to_string(power);
      filein += "_Q4.grm";

      XField_PX<PhysD2, TopoD2> xfld(world, filein);
#else
      int fieldorder = 4;
      Real xmin = -1.5e-0;
      Real xmax = +1.5e-0;
      XField2D_Duct_Triangle_Lagrange_Xq xfld(world, ii, jj, fieldorder, xmin, xmax, gaussian_bump, constant_height);
#endif


      // DG solution field
      //solution at inlet is (1, 0.1, 0, 1);
      ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

      QuadratureOrder quadratureOrder( xfld, 10 );
      std::vector<Real> tol = {1e-11, 1e-11};

#if 0
      //////////////////////
      // SET UP P0 solve
      ///////////////////////
      int order0 = 0;

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld0(xfld, order0, BasisFunctionCategory_Legendre);

      //const int nDOFPDE0 = qfld0.nDOF();

      // Set the initial condition
      qfld0 = q0;

      // Lagrange multiplier:
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
        lgfld0( xfld, order0, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      lgfld0 = 0;

      ////////////
      //SOLVE P0 SYSTEM
      ////////////

      PrimalEquationSetClass PrimalEqSet0(xfld, qfld0, lgfld0, pde, quadratureOrder,
                                          ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> Solver0( PrimalEqSet0, NewtonSolverDict );

      SystemVectorClass ini0(PrimalEqSet0.vectorStateSize());
      SystemVectorClass sln0(PrimalEqSet0.vectorStateSize());
      SystemVectorClass slnchk0(PrimalEqSet0.vectorStateSize());
      SystemVectorClass rsd0(PrimalEqSet0.vectorEqSize());
      rsd0 = 0;

      PrimalEqSet0.fillSystemVector(ini0);
      sln0 = ini0;

      Real rsdPDEnrm0[4] = {0,0,0,0};


      SolveStatus status = Solver0.solve(ini0,sln0);
      BOOST_CHECK( status.converged );
      PrimalEqSet0.setSolutionField(sln0);

      rsd0 = 0;
      PrimalEqSet0.residual(sln0, rsd0);

      // check that the residual is zero

      for (int j = 0; j < nSol; j++)
        rsdPDEnrm0[j] = 0;

      for (int n = 0; n < nDOFPDE0; n++)
        for (int j = 0; j < nSol; j++)
          rsdPDEnrm0[j] += pow(rsd0[0][n][j],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm0[0]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm0[1]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm0[2]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm0[3]), 1e-12 );
#if 0
      // Tecplot dump grid
      string filename0 = "tmp/pseqDG_EulerBump_P";
      filename0 += to_string(order);
      filename0 += "_Q";
      filename0 += to_string(fieldorder);
      filename0 += "_";
      filename0 += to_string(ii);
      filename0 += "x";
      filename0 += to_string(jj);
      filename0 += ".plt";
      output_Tecplot( qfld0, filename0 );
#endif
#endif
      ////////////////////////////////////
      // PROJECT P0 SOLUTION TO HIGHER P
      ///////////////////////////////////

      if (order !=-1)
      {

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

      const int nDOFPDE = qfld.nDOF();

      const int nDOFGlobal = nDOFPDE;
      const Real hGlobal = 1 / sqrt(nDOFGlobal);

      // Set the initial condition
      //qfld0.projectTo(qfld);
      qfld = q0;

      // Lagrange multiplier:
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
        lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      lgfld = 0;

      //////////////////////////
      //SOLVE HIGHER P SYSTEM
      //////////////////////////

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups );

      AlgebraicEquationSet_PTCClass AlgEqSetPTC(xfld, qfld, pde, quadratureOrder, {0}, PrimalEqSet);

      //NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );
      PseudoTime<SystemMatrixClass> Solver(NonlinearSolverDict, AlgEqSetPTC);

      BOOST_CHECK( Solver.iterate(1000) );

      // Monitor Entropy Error
      Real EntropySquareError = 0.0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_Galerkin( fcnErr, EntropySquareError ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      cout << "P = " << order << ": L2 solution error = " << sqrt( EntropySquareError );
//      if (indx > 1)
//        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;
#endif
#if 1
      // Tecplot dump grid
      string filename = "tmp/Euler_SmoothBump/slnDG_EulerBump_P";
      filename += to_string(order);
      filename += "_Q";
      filename += to_string(4); // fieldorder
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += "_";
#ifdef USE_STRUC
      filename += "structured";
#else
      filename += "meshgenerator";
#endif
      filename += ".plt";
      output_Tecplot( qfld, filename );
#endif
#if 1
      // Data dump
      string filename_out;
#ifdef USE_STRUC
      filename_out = "tmp/Euler_SmoothBump/structured_meshes_results.dat";
#else
      filename_out = "tmp/Euler_SmoothBump/duct_mesher_results.dat";
#endif
      fstream data_fs;
      data_fs.open(filename_out, fstream::in | fstream::out | fstream::app);

      // Dump: order, power, DOF, h, Entropy Error, Work Units, Time (s) (comma separated)
      data_fs << order << "," << power << "," << nDOFGlobal << "," << std::setprecision(12) << hGlobal << "," <<
              std::setprecision(12) << sqrt( EntropySquareError ) << "," << 0 << "," << 0 << endl;
      data_fs.close();
#endif

      }
#if 0
      else
      {
        // Monitor Entropy Error
        Real EntropySquareError = 0.0;
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( fcnErr, EntropySquareError ),
            xfld, qfld0, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      cout << "P = " << order << ": L2 solution error = " << sqrt( EntropySquareError );
//      if (indx > 1)
//        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;
#endif

      }
#endif
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
