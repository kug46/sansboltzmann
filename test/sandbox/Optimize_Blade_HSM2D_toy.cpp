// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Optimize_Blade_HSM2D_toy
// Optimizing a "blade" thickness using PDEHSM2D

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "pde/HSM/PDEHSM2D.h"
#include "pde/HSM/BCHSM2D.h"
#include "pde/HSM/ComplianceMatrix.h"
#include "pde/HSM/SolutionFunctionHSM2D.h"
#include "pde/HSM/OutputHSM2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_Solution.h"

#include "MPI/serialize_SurrealS.h"

#include "Discretization/Galerkin/FunctionalBoundaryTrace_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Galerkin.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Output_Galerkin.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/CG/ResidualParamSensitivity_CG.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_HSM2D.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_Galerkin_HSM2D.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#include "Field/Tuple/FieldTuple.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include <nlopt.hpp>

using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Optimize_Blade_HSM_test_suite )


const int iBCbottom = 0;
const int iBCright = 1;
const int iBCtop = 2;
const int iBCleft = 3;

class Thickness_Optimizer
{
public:

  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::ArrayQ<SurrealS<1>> ArrayQSurreal;

  typedef PDEClass::VectorX<Real> VectorX;
  typedef PDEClass::VectorE<Real> VectorE;
  typedef PDEClass::ComplianceMatrix<Real> ComplianceMatrix;
  typedef PDEClass::ComplianceMatrix<SurrealS<1>> ComplianceMatrixSurreal;

  typedef BCHSM2DVector<VarTypeLambda> BCVector;

  typedef MakeTuple<FieldTuple, Field<PhysD2, TopoD2, ComplianceMatrix>,
                                Field<PhysD2, TopoD2, Real>,
                                Field<PhysD2, TopoD2, VectorE>,
                                Field<PhysD2, TopoD2, VectorX>,
                                Field<PhysD2, TopoD2, VectorX>,
                                XField<PhysD2, TopoD2> >::type FieldParamPrimalType;


  typedef MakeTuple<FieldTuple, Field<PhysD2, TopoD2, ComplianceMatrixSurreal>,
                                Field<PhysD2, TopoD2, SurrealS<1> >,
                                Field<PhysD2, TopoD2, VectorE>,
                                Field<PhysD2, TopoD2, VectorX>,
                                Field<PhysD2, TopoD2, VectorX>,
                                XField<PhysD2, TopoD2> >::type FieldParamSensType;

  typedef AlgebraicEquationSet_Galerkin< NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Sparse, FieldParamPrimalType > PrimalEquationSetClass;
  typedef ResidualParamSensitivity_CG< NDPDEClass, BCNDConvertSpace, BCVector, SurrealS<1>, FieldParamSensType > SensEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef PrimalEquationSetClass::SystemVectorTemplate<ArrayQSurreal> SystemVectorSurrealClass;

  typedef SolnNDConvertSpace<PhysD2, SolutionFunctionHSM2D_HangingBarZeroPoisson> SolutionFunction;


  typedef OutputCell_SolutionErrorSquared<NDPDEClass, SolutionFunction> OutputSolutionErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD2, OutputSolutionErrorSquaredClass> NDOutputSolutionErrorSquaredClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputSolutionErrorSquaredClass> IntegrandSquareErrorClass;

  struct muTraits
  {
    typedef PhysD2 PhysDim;

    template<class T>
    using ArrayQ = T;

    template<class T>
    using MatrixQ = T;
  };
  typedef OutputCell_Solution<muTraits> OutputMuClass;
  typedef OutputNDConvertSpace<PhysD2, OutputMuClass> NDOutputMuClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputMuClass> IntegrandSolutionClass;

  typedef OutputHSM2D_Displacement<VarTypeLambda> OutputDisplacementClass;
  typedef OutputNDConvertSpace<PhysD2, OutputDisplacementClass> NDOutputDisplacementClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputDisplacementClass>, NDOutputDisplacementClass::Category> NDOutDispVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutDispVecCat, Galerkin> IntegrandDisplacementClass;


  const Real minThickness = 1e-6;

  const Real rho = 1;
  const Real E = 500000;
  const Real nu = 0;

  Thickness_Optimizer( const XField<PhysD2, TopoD2>& xfld, const int thickness_order, const Real targetDisplacement, const VectorX& gvec,
                       PyDict& NewtonSolverDict,
                       PyDict& PyBCList, const std::map< std::string, std::vector<int> >& BCBoundaryGroups )
    : xfld_(xfld),
      targetDisplacement_(targetDisplacement),
      thickness_(thickness_order, BasisFunctionCategory_Hierarchical),
      //thickness_(0, BasisFunctionCategory_Legendre),
      nvar_(thickness_.nDOF()-1),
      //nvar_(thickness_.nDOF()),
      opt_(nlopt::LD_MMA, nvar_),
      eval_count_(0),
      pde_(gvec, PDEClass::HSM_ResidInterp_Separate),
      fcnWeight_(outWeight_, {0}),
      outDispl_(pde_),
      fcnDispl_(outDispl_,{iBCtop}),
      AinvfldPrimal_(xfld_, 1, BasisFunctionCategory_Hierarchical), AinvfldSens_(xfld_, 1, BasisFunctionCategory_Hierarchical),
      mufldPrimal_(xfld_, 1, BasisFunctionCategory_Hierarchical), mufldSens_(xfld_, 1, BasisFunctionCategory_Hierarchical),
      qefld_(xfld_, 1, BasisFunctionCategory_Hierarchical),
      qxfld_(xfld_, 1, BasisFunctionCategory_Hierarchical),
      afld_(xfld_, 1, BasisFunctionCategory_Hierarchical),
      varfld_(xfld_, 1, BasisFunctionCategory_Hierarchical),
      lgfld_( xfld_, 1, BasisFunctionCategory_Hierarchical, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) ),
      fldParamPrimal_( (AinvfldPrimal_, mufldPrimal_, qefld_, qxfld_, afld_, xfld_) ),
      fldParamSens_( (AinvfldSens_, mufldSens_, qefld_, qxfld_, afld_, xfld_) ),
      stab_(1),
      PrimalEqSet_(fldParamPrimal_, varfld_, lgfld_, pde_, stab_,
                   QuadratureOrder( xfld, - 1 ), ResidualNorm_L2, {1e-11, 1e-11}, {0}, PyBCList, BCBoundaryGroups ),
      SensEqSet_(fldParamSens_, varfld_, lgfld_, pde_, stab_, {0}, PyBCList, BCBoundaryGroups ),
      NonLinearSolver_( PrimalEqSet_, NewtonSolverDict ),
      xmax_(0), ymax_(0)
  {
    // cppcheck-suppress useInitializationList
    qefld_ = 0;

    // cppcheck-suppress useInitializationList
    qxfld_ = 0;

    // cppcheck-suppress useInitializationList
    afld_ = 0;

#if 0 // Radial acceleration

    // Loop over nodes and make nonuniform acceleration field
    BOOST_REQUIRE_EQUAL(xfld.nDOF(), varfld.nDOF());

    Real omega = 2*PI*100; // Rotational freq. (rad/s)
    for (int i = 0; i < xfld.nDOF(); i++)
    {
      Real x0 = xfld.DOF(i)[0];
      Real y0 = xfld.DOF(i)[1];
      afld.DOF(i) = { 0, -y0*omega }; // Centrifugal acceleration
    }

#endif

    // Set to a constant thickness
    thickness_.DOF(0) = 0.01;
    thickness_.DOF(1) = 0.015;
    for (int i = 2; i < thickness_.nDOF(); i++)
      thickness_.DOF(i) = 0;

#if 0 // Constrained opt (tip displacement constraint)
    opt_.set_min_objective( NLOPT_WeightFunctional, reinterpret_cast<void*>(this) );
    opt_.add_inequality_constraint( NLOPT_DisplacementConstraint, reinterpret_cast<void*>(this) );
#else // Unconstrained opt (penalty function)
    opt_.set_min_objective( NLOPT_Unconstrained, reinterpret_cast<void*>(this) );
#endif

    std::vector<double> lower_bound(nvar_, -1000);
    lower_bound[0] = thickness_.DOF(0).value()*1.01;
    opt_.set_lower_bounds(lower_bound);

    //Termination conditions
    Real xtol = 1e-12;
    Real ftol = 1e-12;

    //stop when every parameter changes by less than the tolerance multiplied by the absolute value of the parameter.
    opt_.set_xtol_rel(xtol);

    //stop when the objective function value changes by less than the tolerance multiplied by the absolute value of the function value
    opt_.set_ftol_rel(ftol);

    //stop when the maximum number of function evaluations is reached
    opt_.set_maxeval( 30 );

    for (int i = 0; i < xfld_.nDOF(); i++)
    {
      xmax_ = max(xmax_, xfld_.DOF(i)[0]);
      ymax_ = max(ymax_, xfld_.DOF(i)[1]);
    }
  }

  //The function that NLOPT calls to evaluate the objective function
  static double NLOPT_WeightFunctional(unsigned n, const double* x, double* grad, void* f_data)
  {
    Thickness_Optimizer* functional = reinterpret_cast<Thickness_Optimizer*>(f_data);

    return (*functional).computeWeight(n, x, grad); //forward the call
  }

  //The function that NLOPT calls to evaluate the cost constraint
  static double NLOPT_DisplacementConstraint(unsigned n, const double* x, double* grad, void* f_data)
  {
    Thickness_Optimizer* functional = reinterpret_cast<Thickness_Optimizer*>(f_data);

    return (*functional).computeDisplacementConstraint(n, x, grad); //forward the call
  }

  //The function that NLOPT calls to evaluate the cost constraint
  static double NLOPT_Unconstrained(unsigned n, const double* x, double* grad, void* f_data)
  {
    Thickness_Optimizer* functional = reinterpret_cast<Thickness_Optimizer*>(f_data);

    return (*functional).unconstrained(n, x, grad); //forward the call
  }

  //Routine that computes the objective function and its derivative wrt optimization variables
  double computeWeight(const unsigned int n, const double* x, double* grad)
  {
    eval_count_++;

    setThickness(n, x);
    setMufldPrimal();

    Real Weight = 0;

    int quadratureOrder[1] = {9};
    IntegrateCellGroups<TopoD2>::integrate(
        FunctionalCell_Galerkin( fcnWeight_, Weight ), xfld_, mufldPrimal_, quadratureOrder, 1 );

    if ( grad != NULL )
    {
      for (unsigned int i = 0; i < n; i++ )
      {
        thickness_.DOF(i+1).deriv() = 1;
        setMufldSens();
        SurrealS<1> WeightSurreal = 0;
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( fcnWeight_, WeightSurreal ), xfld_, mufldSens_, quadratureOrder, 1 );
        grad[i] = WeightSurreal.deriv();
        thickness_.DOF(i+1).deriv() = 0;
      }
    }

#if 1
    std::cout << std::scientific << std::setprecision(16);
    Real t0 = thickness_.DOF(1).value();
    Real tL = thickness_.DOF(0).value();
    std::cout << "eval " << eval_count_ << " t0 = " << t0  << " tL = " << tL << " Weight = " << Weight;
    if ( grad != 0 )
    {
      for (unsigned int i = 0; i < n; i++)
        std::cout << " grad[" << i << "] = " << grad[i];
    }
    std::cout << std::endl;
#endif
    return Weight;
  }

  //Routine that computes the cost constraint and its derivative wrt optimization variables
  //Constraint form: computeDisplacementConstraint(x) <= 0
  double computeDisplacementConstraint(unsigned n, const double* x, double* grad)
  {
    setThickness(n, x);
    initVarfld();
    setParamfldPrimal();

    SystemVectorClass ini(PrimalEqSet_.vectorStateSize());
    SystemVectorClass sln(PrimalEqSet_.vectorStateSize());

    PrimalEqSet_.fillSystemVector(ini);

    SolveStatus status = NonLinearSolver_.solve(ini,sln);
    SANS_ASSERT( status.converged );

    PrimalEqSet_.setSolutionField(sln);

    std::vector<int> quadratureOrder(xfld_.nBoundaryTraceGroups(), 9);

    Real Displacement = 0;
    IntegrateBoundaryTraceGroups<TopoD2>::integrate(
        FunctionalBoundaryTrace_Galerkin( fcnDispl_, Displacement ),
        xfld_, varfld_, quadratureOrder.data(), quadratureOrder.size() );

    if ( grad != NULL )
    {
      DLA::VectorD< SLA::SparseVector< IntegrandDisplacementClass::MatrixJ<Real> > > g(PrimalEqSet_.vectorStateSize());
      g = 0;

      // compute g = dJ/dU
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          JacobianFunctionalBoundaryTrace_Galerkin<SurrealS<6>>( fcnDispl_, g(0) ),
          xfld_, varfld_, quadratureOrder.data(), quadratureOrder.size() );

      SystemNonZeroPattern nz(PrimalEqSet_.matrixSize());
      PrimalEqSet_.jacobian(sln, nz);
      SystemMatrixClass A(nz);

      SLA::UMFPACK<SystemMatrixClass> LinearSolver(PrimalEqSet_);

      LinearSolver.factorize();

      SystemVectorSurrealClass rsd(PrimalEqSet_.vectorEqSize());
      SystemVectorClass f(PrimalEqSet_.vectorStateSize());
      SystemVectorClass u(PrimalEqSet_.vectorStateSize());
      for (unsigned int i = 0; i < n; i++ )
      {
        thickness_.DOF(i+1).deriv() = 1;

        setParamfldSens();

        // Compute f = -dN/dalpha
        rsd = 0;
        SensEqSet_.residualSensitivity(rsd);

        // Extract the derivatives from the residual vector
        for (int j = 0; j < rsd.m(); j++)
          for (int k = 0; k < rsd[j].m(); k++)
            for (int m = 0; m < ArrayQ::M; m++)
              f[j][k][m] = -rsd[j][k][m].deriv();

        thickness_.DOF(i+1).deriv() = 0;

        // Solve Au = f
        LinearSolver.backsolve(f, u);

        // compute dJ/dalpha = g^T u
        grad[i] = dot(g,u);
      }
    }
#if 1
    std::cout << std::scientific << std::setprecision(16);
    Real t0 = thickness_.DOF(1).value();
    Real tL = thickness_.DOF(0).value();
    std::cout << "eval " << eval_count_ << " t0 = " << t0  << " tL = " << tL << " deltip = " << Displacement;
    if ( grad != 0 )
    {
      for (unsigned int i = 0; i < n; i++)
        std::cout << " grad[" << i << "] = " << grad[i];
    }
    std::cout << std::endl;
#endif
    return (Displacement - targetDisplacement_);
  }

  //Routine that computes the objective function and its derivative wrt optimization variables
  double unconstrained(const unsigned int n, const double* x, double* grad)
  {
    std::vector<double> weight_t(n);
    std::vector<double> deltip_t(n);

    Real Weight = computeWeight(n, x, weight_t.data());
    Real deltip = computeDisplacementConstraint(n, x, deltip_t.data()) + targetDisplacement_;

    Real K = 100;
    Real delspec = 0.008;

    Real t0 = thickness_.DOF(1).value();
    Real tL = thickness_.DOF(0).value();

    Real V = K*xmax_*ymax_*tL;

    Real penalty = V*pow(deltip/delspec - 1, 2);

    Real J = Weight/rho + penalty;

    if ( grad != 0 )
    {
      for (unsigned int i = 0; i < n; i++)
        grad[i] = weight_t[i]/rho + 2*V*(deltip/delspec - 1)*deltip_t[i]/delspec;
    }

    std::cout << std::scientific << std::setprecision(16);
    //std::cout << "eval " << eval_count_ << " t0 = " << t0  << " tL = " << tL
    //          << " Volume = " << Weight/rho << " deltip = " << deltip << " J = " << J;
    std::cout << "eval " << eval_count_ << " t0 = " << t0  << " tL = " << tL << " J = " << J;
    if ( grad != 0 )
    {
      for (unsigned int i = 0; i < n; i++)
        std::cout << " grad[" << i << "] = " << grad[i];
    }
    //std::cout << " volume = " << Weight/rho << " gradvol = " << weight_t[0]/rho
    //          << " penalty = " << penalty << " gradpenalty = "<< 2*V*(deltip/delspec - 1)*deltip_t[0]/delspec;

    std::cout << std::endl;

    return J;
  }


  void setThickness(unsigned n, const double* x)
  {
    // set the thickness (tip thickness is fixed)
    for (int i = 1; i < thickness_.nDOF(); i++)
      thickness_.DOF(i) = x[i-1];

    //for (int i = 0; i < thickness_.nDOF(); i++)
    //  thickness_.DOF(i) = x[i];
  }

  void initVarfld()
  {
    // NOTE: This assumes P1 CG field
    for (int i = 0; i < xfld_.nDOF(); i++)
    {
      PositionLambdaForces2D varData;
      varData.rx    = xfld_.DOF(i)[0];
      varData.ry    = xfld_.DOF(i)[1];
      varData.lam3  = 0;
      varData.f11   = 0;
      varData.f22   = 0;
      varData.f12   = 0;

      varfld_.DOF(i) = pde_.setDOFFrom( varData );
    }
  }

  void setMufldPrimal()
  {
    // NOTE: This assumes P1 CG field
    for (int i = 0; i < xfld_.nDOF(); i++)
    {
      Real t = max( minThickness, thickness_.eval( 1-xfld_.DOF(i)[1]/ymax_ ).value()); // Don't want negative thickenss
      mufldPrimal_.DOF(i) = rho*t;
      //std::cout << "{" << xfld_.DOF(i) << ": " << t << "} ";
    }
    //std::cout << std::endl;
  }

  void setMufldSens()
  {
    // NOTE: This assumes P1 CG field
    for (int i = 0; i < xfld_.nDOF(); i++)
    {
      SurrealS<1> t = max( minThickness, thickness_.eval( 1-xfld_.DOF(i)[1]/ymax_ ) ); // Don't want negative thickenss
      mufldSens_.DOF(i) = rho*t;
    }
  }

  void setParamfldPrimal()
  {
    // NOTE: This assumes P1 CG field
    for (int i = 0; i < xfld_.nDOF(); i++)
    {
      Real t = max( minThickness, thickness_.eval( 1-xfld_.DOF(i)[1]/ymax_ ).value()); // Don't want negative thickenss
      mufldPrimal_.DOF(i)   = rho*t;
      AinvfldPrimal_.DOF(i) = calcComplianceMatrix( E, nu, t );
    }
  }

  void setParamfldSens()
  {
    // NOTE: This assumes P1 CG field
    for (int i = 0; i < xfld_.nDOF(); i++)
    {
      const SurrealS<1> t = max( minThickness, thickness_.eval( 1-xfld_.DOF(i)[1]/ymax_ ) ); // Don't want negative thickenss
      mufldSens_.DOF(i)   = rho*t;
      AinvfldSens_.DOF(i) = calcComplianceMatrix( E, nu, t );
    }
  }

  void optimize()
  {
    eval_count_ = 0;

    //Initialize solution vector
    std::vector<double> x(nvar_);

    for (int i = 1; i < thickness_.nDOF(); i++)
      x[i-1] = thickness_.DOF(i).value();
    //for (int i = 0; i < thickness_.nDOF(); i++)
    //  x[i] = thickness_.DOF(i).value();

    double f_opt = 0.0;
    nlopt::result result;

    //try
    //{
      result = opt_.optimize(x, f_opt);
    //}
    //catch (const std::runtime_error& failure )
    //{
    //  SANS_DEVELOPER_EXCEPTION( "NLOPT - Runtime failure.");
    //}

    std::cout << "-------------------" << std::endl;
    std::cout << "   NLopt Summary   " << std::endl;
    std::cout << "-------------------" << std::endl;
    std::cout << "Variables  : " << x.size() << std::endl;
    std::cout << "Result code: " << result   << std::endl;
    std::cout << "Eval count : " << eval_count_ << std::endl;
    std::cout << "f_sol = " << f_opt << std::endl;

    std::cout << "x_sol = [" << x[0];
    for (std::size_t i = 1; i < x.size(); i++)
      std::cout << "," << x[i];
    std::cout << "]" << std::endl;
    std::cout << "-------------------------" << std::endl<<std::endl;

    // Set the solution to the thickness
    setThickness(x.size(), x.data());
  }

//protected:
  const XField<PhysD2, TopoD2>& xfld_;
  Real targetDisplacement_;
  Element<  SurrealS<1>, TopoD1, Line > thickness_;
  int nvar_;
  nlopt::opt opt_;
  int eval_count_; //No. of objective function evaluations

  NDPDEClass pde_;
  NDOutputMuClass outWeight_;
  IntegrandSolutionClass fcnWeight_;

  NDOutputDisplacementClass outDispl_;
  IntegrandDisplacementClass fcnDispl_;

  // Extensional compliance matrix CG field
  Field_CG_Cell<PhysD2, TopoD2, ComplianceMatrix> AinvfldPrimal_;
  Field_CG_Cell<PhysD2, TopoD2, ComplianceMatrixSurreal> AinvfldSens_;

  // Lumped shell mass CG field
  Field_CG_Cell<PhysD2, TopoD2, Real> mufldPrimal_;
  Field_CG_Cell<PhysD2, TopoD2, SurrealS<1>> mufldSens_;

  // Following force CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorE> qefld_;

  // Global forcing CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorX> qxfld_;

  // Accelerations CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorX> afld_;

  // solution variable CG field
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> varfld_;

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_;

  FieldParamPrimalType fldParamPrimal_;
  FieldParamSensType fldParamSens_;

  StabilizationNitsche stab_;
  PrimalEquationSetClass PrimalEqSet_;
  SensEquationSetClass SensEqSet_;

  NewtonSolver<SystemMatrixClass> NonLinearSolver_;

  Real xmax_, ymax_;
};



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_CG_HSM2D_HangingPlate_ZeroPoisson )
{

  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  //typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSM2D<VarTypeLambda>::VectorE<Real> VectorE;
  typedef PDEHSM2D<VarTypeLambda>::ComplianceMatrix<Real> ComplianceMatrix;

  //typedef SolnNDConvertSpace<PhysD2, SolutionFunctionHSM2D_HangingBarZeroPoisson> SolutionFunction;
  //typedef IntegrandCell_SquareError<SolutionFunction> IntegrandSquareErrorClass;

  typedef MakeTuple<FieldTuple, Field<PhysD2, TopoD2, ComplianceMatrix>,
                                Field<PhysD2, TopoD2, Real>,
                                Field<PhysD2, TopoD2, VectorE>,
                                Field<PhysD2, TopoD2, VectorX>,
                                Field<PhysD2, TopoD2, VectorX>,
                                XField<PhysD2, TopoD2> >::type FieldParamType;


  typedef BCHSM2DVector<VarTypeLambda> BCVector;
  typedef AlgebraicEquationSet_Galerkin< NDPDEClass, BCNDConvertSpace, BCVector, AlgEqSetTraits_Sparse, FieldParamType > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE
  Real g = 10.0*1000;
  VectorX gvec = { 0.0, g };
  NDPDEClass pde(gvec, PDEClass::HSM_ResidInterp_Separate);

  Real xmax = 1.;
  Real ymax = 1.;

  // BC
  // Create a BC dictionary
  PyDict BCDisp;
  BCDisp[BCParams::params.BC.BCType] = BCParams::params.BC.Displacements;
  BCDisp[BCHSM2DParams<BCTypeDisplacements>::params.rshiftx] = 0;
  BCDisp[BCHSM2DParams<BCTypeDisplacements>::params.rshifty] = 0;

  PyDict BCForceZero;
  BCForceZero[BCParams::params.BC.BCType] = BCParams::params.BC.Forces;
  BCForceZero[BCHSM2DParams<BCTypeForces>::params.f1BC] = 0; // f1BC
  BCForceZero[BCHSM2DParams<BCTypeForces>::params.f2BC] = 0; // f2BC

  PyDict BCForce;
  BCForce[BCParams::params.BC.BCType] = BCParams::params.BC.Forces;
  BCForce[BCHSM2DParams<BCTypeForces>::params.f1BC] = 100000; //f1BC
  BCForce[BCHSM2DParams<BCTypeForces>::params.f2BC] = 0;      //f2BC

  // String arguments in PyBCList and BCBoundaryGroups must always match

  PyDict PyBCList;
  PyBCList["BCDisp"] = BCDisp;
  // PyBCList["BCForce"] = BCForce;
  PyBCList["BCForceZero"] = BCForceZero;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;


  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDisp"] = {iBCbottom};
  // BCBoundaryGroups["BCForce"] = {iBCright};
  BCBoundaryGroups["BCForceZero"] = {iBCtop, iBCright, iBCleft};

  //typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  //typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  //typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // Analytic solution
  //Real mu = 10, E = 500000, nu = 0, t = 1./100.;
  //SolutionFunction solnExact( mu, g, E, ymax, t );
  //IntegrandSquareErrorClass fcnErr( solnExact );

  int ii = 2;
  int jj = 40;

  // grid: 2D Box mesh
  //XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, xmax, 0, ymax ); // This can do P2 elements
  XField2D_Box_Quad_X1 xfld( ii, jj, 0, xmax, 0, ymax ); // This can only do P1 elements

  Real targetDisplacement = 0.008;
  int thickness_order = 1;

  Thickness_Optimizer optimizer(xfld, thickness_order, targetDisplacement, gvec,
                       NewtonSolverDict, PyBCList, BCBoundaryGroups );


  optimizer.optimize();

#if 0
  Real eps = 0.002; //sqrt(std::numeric_limits<Real>::epsilon());

  for ( int i = 0; i < 11; i++)
  {
    double x = 0.018 + eps*i/10.;
    double grad = 0;
    //optimizer.computeDisplacementConstraint(1, &x, &grad); // + targetDisplacement;
    //optimizer.computeWeight(1, &x, &grad);
    optimizer.unconstrained(1, &x, &grad);
    //std::cout << "deltip = " << deltip << " grad = " << grad << std::endl;
  }
#endif

  // Tecplot dump grid
  std::string filename = "tmp/slnCG_HSM2D_Blade.dat";
  output_Tecplot( optimizer.varfld_, filename );

#if 0
  // Extensional compliance matrix CG field
  Field_CG_Cell<PhysD2, TopoD2, ComplianceMatrix> Ainvfld(xfld, order, BasisFunctionCategory_Hierarchical);
  Ainvfld = calcComplianceMatrix( E, nu, t );

  // Lumped shell mass CG field
  Field_CG_Cell<PhysD2, TopoD2, Real> mufld(xfld, order, BasisFunctionCategory_Hierarchical);
  mufld = mu;

  // Following force CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorE> qefld(xfld, order, BasisFunctionCategory_Hierarchical);
  qefld = 0;

  // Global forcing CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorX> qxfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qxfld = 0;

  // Accelerations CG field
  Field_CG_Cell<PhysD2, TopoD2, VectorX> afld(xfld, order, BasisFunctionCategory_Hierarchical);
  afld = 0;

  // P1 solution variable CG field
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> varfldP1(xfld, 1, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL(xfld.nDOF(), varfldP1.nDOF());
  for (int i = 0; i < xfld.nDOF(); i++)
  {
    PositionLambdaForces2D varData;
    varData.rx_    = xfld.DOF(i)[0];
    varData.ry_    = xfld.DOF(i)[1];
    varData.lam3_  = 0;
    varData.f11_   = 0;
    varData.f22_   = 0;
    varData.f12_   = 0;

    //afld.DOF(i) = accleration(xfld.DOF(i));

    varfldP1.DOF(i) = pde.setDOFFrom( varData );
  }

  // P solution variable CG field
  Field_CG_Cell<PhysD2, TopoD2, ArrayQ> varfld(xfld, order, BasisFunctionCategory_Hierarchical);
  varfldP1.projectTo(varfld);


  const int nDOFPDE = varfld.nDOF();

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
    lgfld( xfld, order, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  //////////////////////////
  //SYSTEM
  //////////////////////////

  FieldParamType fldParam = (Ainvfld, mufld, qefld, qxfld, afld, xfld);

  PrimalEquationSetClass PrimalEqSet(fldParam, varfld, lgfld, pde, {0}, PyBCList, BCBoundaryGroups );

  NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

  SystemVectorClass ini(PrimalEqSet.vectorStateSize());
  SystemVectorClass sln(PrimalEqSet.vectorStateSize());
  SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
  rsd = 0;

  PrimalEqSet.fillSystemVector(ini);
  sln = ini;

  BOOST_CHECK(Solver.solve(ini,sln).converged);
  PrimalEqSet.setSolutionField(sln);

  // check that the residual is zero

  PrimalEqSet.residual(sln,rsd);

  Real rsdPDEnrm[6] = {0,0,0,0,0,0};
  for (int n = 0; n < nDOFPDE; n++)
    for (int j = 0; j < nSol; j++)
      rsdPDEnrm[j] += pow(rsd[0][n][j],2);

  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[0]), 1e-11 );
  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[1]), 1e-11 );
  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[2]), 1e-11 );
  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[3]), 1e-11 );
  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[4]), 1e-11 );
  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[5]), 1e-11 );

  // Monitor Entropy Error
  ArrayQ SquareError = 0.0;
  // int quadratureOrder[4] = {-1, -1, -1, -1};    // max
  int quadratureOrder[4] = {9, 9, 9, 9};
  FunctionalCell<TopoD2>::integrate( fcnErr, varfld, quadratureOrder, 1, SquareError );
#endif
#if 0
  // Tecplot output
  resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
  for (int n = 0; n < indx; n++)
  {
    resultFile << hVec[n];
    resultFile << ", " << hDOFVec[n];
    for (int i = 0; i < 6; i++)
    {
      resultFile << ", " << normVec[i][n];
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[i][n])  - log(normVec[i][n-1])) /(log(hVec[n]) - log(hVec[n-1]));

      resultFile << ", " << slope;
    }
    resultFile << endl;
  }
#endif

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
