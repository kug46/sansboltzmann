// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGBR2_Triangle_RANSSA_FlatPlate_toy
// testing of 2-D DGBR2 for RANS-SA for flat plate

#define BOUNDARYOUTPUT
//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include <boost/filesystem.hpp> // to automagically make the directories

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"
#include "Meshing/EPIC/XField_PX.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/output_grm.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Triangle_RANSSA_Joukowski_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGBR2_Triangle_RANSSA_Joukowski )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_Entropy<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_Distance ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

   mpi::communicator world;

   enum ResidualNormType ResNormType = ResidualNorm_L2_DOFWeighted;
   std::vector<Real> tol = {1e-10, 1e-10};
  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;           // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.2;
  const Real Reynolds = 1e6;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real tRef = 1;          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                              // velocity scale
  const Real aoaRef = 16.0*PI/180.0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution
  const Real ntr = ntRef;

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, 1. );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntr}) );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);


  PyDict BCNoSlip;
#if 1
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;
#else
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipIsothermal_mitState;
  BCNoSlip[BCNavierStokes2DParams<BCTypeWallNoSlipIsothermal_mitState>::params.Twall] = tRef;
#endif

  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;
  StateVector["rhont"]  = rhoRef*ntr; //TODO: not sure about this, was previously just "nt" which gave a Python dictionary error

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0};
  BCBoundaryGroups["BCIn"] = {1,2};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  // Drag output
  NDOutputClass outputFcn(pde, cos(aoaRef), sin(aoaRef));
  OutputIntegrandClass outputIntegrand( outputFcn, {0} );

  // DRAG INTEGRAND
  typedef BCRANSSA2D<BCTypeWall_mitState, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  NDBCClass bc(pde);
  IntegrandBCClass fcnBC( pde, bc, {0}, disc );

  NDOutputClass outputFcn2(pde, -sin(aoaRef), cos(aoaRef));
  OutputIntegrandClass outputIntegrand2( outputFcn2, {0} );
#endif
//  const std::vector<int> BoundaryGroups = {0,1,2,3,4,5};

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;
  //
  //  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
    PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
    PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;
  //  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  //  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Left;

    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

    PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
    PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
    PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-20;
    PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1000;
    PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
    PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
    PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
    PETScDict[SLA::PETScSolverParam::params.Memory] = true;
    PETScDict[SLA::PETScSolverParam::params.Timing] = true;
    PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = true;
    PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;
    //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-30;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 40000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 4000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  // Easier PTC for subsequent solves
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;
  NonlinearSolverDict[PseudoTimeParam::params.SteveLogic] = true;
  NonlinearSolverDict[PseudoTimeParam::params.CFLPartialStepDecreaseFactor] = 0.9;

//  NonlinearSolverDict0[PseudoTimeParam::params.CFLPartialStepDecreaseFactor] = 0.8;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  PyDict ParamDict;
  //   norm data
  //  Real normVec[10];   // L2 error
  //  int indx = 0;

  //  fstream fout( "tmp/DGBR2_V2_Ec0p3Re1.txt", fstream::out );
  //
  int powermin = 1;
  int powermax = 1;
  //
  int ordermin = 3;
  int ordermax = 3;


  // loop over grid resolution: 2^power
  //    int power = 0;

  fstream fout;
  string filename_base = "tmp/RANSJOUK/DG_STRUC/";
  boost::filesystem::create_directories(filename_base);

  string filename2 = filename_base + "Drag.txt";

  std::shared_ptr<SolutionClass> pGlobalSol;
  std::shared_ptr<SolutionClass> pGlobalSolOld;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfldOld;

  for (int power = powermin; power <= powermax; power++)
  {

    string filein = "grids/Joukowski_RANS_Classic_tri_ref";
    filein += to_string(power);
    filein += "_Q4.grm";

    pxfld = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, filein);

    int dorder = 3; // Distance order
    Field_CG_Cell<PhysD2, TopoD2, Real> distfld(*pxfld, dorder, BasisFunctionCategory_Lagrange);
    DistanceFunction(distfld, BCBoundaryGroups.at("BCNoSlip"));

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    for (int order = ordermin; order <= ordermax; order++)
    {

      if (world.rank() == 0)
      {
        fout.open( filename2, fstream::app );
      }

      const int quadOrder = 2*(order + 1); // Is this high enough?

      //Solution data
      typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>((distfld, *pxfld), pde, order, order,
          BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
          active_boundaries, ParamDict, disc);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
          cellGroups, interiorTraceGroups,
          PyBCList, BCBoundaryGroups,
          SolverContinuationDict, LinearSolverDict,
          outputIntegrand);

      //Set initial solution
      if (order == ordermin && power == powermin)
        pGlobalSol->setSolution(q0);
      else
        pGlobalSol->setSolution(*pGlobalSolOld);

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

#if 0
      // Tecplot dump grid
      string filename2 = "tmp/DGBR2_SMOOTHR2_RANSJoukowski_P";
      filename2 += to_string(order);
      filename2 += "_X";
      filename2 += to_string(power);
      filename2 += ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, filename2 );
      if (world.rank() == 0)
        std::cout << "Wrote Tecplot File\n";


      // Tecplot dump grid
      string filename3 = "tmp/DGBR2_SMOOTHR2_RANSJoukowski_P";
      filename3 += to_string(order+1);
      filename3 += "_X";
      filename3 += to_string(power);
      filename3 += "_adjoint.plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, filename3 );
      if (world.rank() == 0)
        std::cout << "Wrote Tecplot File\n";
#endif

      // Monitor Drag Error
      Real Drag = pInterface->getOutput();
      // drag coefficient
      Real Cd = Drag / (0.5*rhoRef*qRef*qRef*lRef);
//      Real Cl = Lift / (0.5*rhoRef*qRef*qRef*lRef);
      Real Lift = 0;
      QuadratureOrder quadratureOrder( *pxfld, quadOrder );//3*order + 1 );
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          FunctionalBoundaryTrace_DGBR2( outputIntegrand2, fcnBC, Lift ),
          (distfld, *pxfld), (pGlobalSol->primal.qfld, pGlobalSol->primal.rfld),
          quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size() );

      Real Cl = Lift / (0.5*rhoRef*qRef*qRef*lRef);


      if (world.rank() == 0)
      {
        fout << "P = " << order << " power = " << power <<
            ": Cd = " << std::setprecision(12) << Cd <<
            ": Drag = " << std::setprecision(12) << Drag <<
        ": Cl = " << std::setprecision(12) << Cl <<
        ": Lift = " << std::setprecision(12) << Lift;
        fout << endl;
      }

      pGlobalSolOld = pGlobalSol;
      pxfldOld = pxfld;

    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
