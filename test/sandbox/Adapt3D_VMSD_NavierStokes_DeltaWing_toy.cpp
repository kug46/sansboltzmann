// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include <boost/filesystem.hpp> // to automagically make the directories
#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/OutputEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"

#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_OutputWeightRsd_VMSD.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"
#include "Adaptation/MOESS/MOESSParams.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_EG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Meshing/libMeshb/WriteSolution_libMeshb.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/EPIC/XField_PX.h"

#ifdef SANS_AVRO
#include "Meshing/avro/MesherInterface_avro.h"
#include "Meshing/avro/XField_avro.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

template <class PDE, class ArrayQ>
void computeMachField( const PDE& pde, //const std::vector<int> wallBTraceGroups,
                       Field_CG_Cell<PhysD3,TopoD3,ArrayQ>& Cfld,
                       const std::string& filename )
{
  const XField<PhysD3,TopoD3>& xfld = Cfld.getXField();

  SANS_ASSERT_MSG( Cfld.getCellGroupBase(0).basisCategory() == BasisFunctionCategory_Lagrange,
                   "Calculation of Mach Field assumes that the basis representation is interpolatory" );
  Field_CG_Cell<PhysD3,TopoD3,Real> Mfld( xfld, Cfld.getCellGroupBase(0).order(),
                                                BasisFunctionCategory_Lagrange );
  Mfld = 0;

  Real x=0,y=0,z=0,t=0;
  ArrayQ qx=0,qy=0,qz=0;

  for (int dof = 0; dof < Cfld.nDOF(); dof++)
  {
    pde.derivedQuantity(0,x,y,z,t,Cfld.DOF(dof),qx,qy,qz,Mfld.DOF(dof));
  }

  Mfld.syncDOFs_MPI_noCache();

  //output_Tecplot( Cfld, filename + "_cfld.plt" );
  WriteSolution_libMeshb( Mfld, filename + ".solb" );
  output_Tecplot( Mfld, filename + ".plt" );
}




//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_VMSD_NavierStokes_DeltaWing_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_DGBR2_NavierStokes_DeltaWing_test )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Mach<PDEClass>> NDOutputMach;
  typedef IntegrandCell_Galerkin_Output<NDOutputMach> OutputIntegrandMachClass;

  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Force<PDEClass>> NDOutputForce;
  typedef OutputNDConvertSpace<PhysD3, OutputEuler3D_Moment<PDEClass>> NDOutputMoment;
  typedef NDVectorCategory<boost::mpl::vector2<NDOutputForce, NDOutputMoment>, NDOutputForce::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, VMSD> OutputIntegrandBoundaryClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_VMSD<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

#ifdef BOUNDARYOUTPUT
  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass, OutputIntegrandBoundaryClass> SolverInterfaceClass;
#else
  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass, OutputIntegrandMachClass> SolverInterfaceClass;
#endif

  mpi::communicator world;

  std::string init_grid_path;
  if (world.size() <= 16)
    init_grid_path = "grids/DeltaWing/xfld_42Elem_initial.meshb";
  else if (world.size() <= 32)
    init_grid_path = "grids/DeltaWing/xfld_95Elem_initial.meshb";
  else
    init_grid_path = "grids/DeltaWing/xfld_172Elem_initial.meshb";

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // Sutherland viscosity
  //const Real tSuth = 198.6/540;     // R/R

  // reference state (freestream)
  const Real Mach = 0.3;
  const Real Reynolds = 4000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 12.5 * PI/180;                // angle of attack (radians)

  const Real SRef = 0.133974596;                    // area scale
#if 1
  const Real tRef = 1;          // temperature
  const Real pRef = rhoRef*R*tRef;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;
#endif

#if 0
  const Real qRef = 1;                              // velocity scale
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = 0;
  const Real wRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef + wRef*wRef);

  Real CG[3] = {lRef/0.25, 0, 0};

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure3D<Real>(rhoRef, uRef, vRef, wRef, pRef) );

  // Galerkin Stabilization -- for use in Nitsche params
  DiscretizationVMSD stab;

  std::string mesher = "EPIC";
  std::string file_tag = "";
  bool dumpField = false;
  int maxIter = 50;
  const bool DGCONV = true; // Run with similar element numbers to DG?
  // Should you start from the grid 'xfld_initial_FINE_PX.grm where X is the polynomial order
  // This is useful for starting big runs from an already pretty good grid
  bool readBigGrid = false;

  int orderL = 1, orderH = 3;
  int powerL = 2, powerH = 2;

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    mesher = std::string(argv[1]);
  if (argc >= 3)
    file_tag = std::string(argv[2]);
  if (argc >= 4)
    dumpField = std::stoi(argv[3]);
  if (argc >= 5)
    orderL = orderH = std::stoi(argv[4]);
  if (argc >= 6)
    powerL = powerH = std::stoi(argv[5]);
  if (argc >= 7)
    maxIter = std::stoi(argv[6]);
  if (argc >= 8)
    readBigGrid = std::stoi(argv[7]); // Should you read in the large fine starting grid?

  if (world.rank() == 0)
  {
    std::cout << "mesher: " << mesher << ", file_tag: " << file_tag << ", dumpField: " << dumpField;
    std::cout << ", orderL,H = " << orderL << ", " << orderH;
    std::cout << ", powerL,H = " << powerL << ", " << powerH << ", maxIter: " << maxIter << std::endl;
  }
#endif

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipIsothermal_mitState;
  BCWall[BCNavierStokes3DParams<BCTypeWallNoSlipIsothermal_mitState>::params.Twall] = tRef;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict StateVector;
  StateVector[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  StateVector[Conservative3DParams::params.rho] = rhoRef;
  StateVector[Conservative3DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative3DParams::params.rhov] = rhoRef*vRef;
  StateVector[Conservative3DParams::params.rhow] = rhoRef*wRef;
  StateVector[Conservative3DParams::params.rhoE] = rhoRef*ERef;

  PyDict BCFarField;
  BCFarField[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.StateVector] = StateVector;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.Characteristic] = true;

  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOutflow[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;

  PyDict PyBCList;
  PyBCList["BCFarField"] = BCFarField;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCOutflow"]  = BCOutflow;
  PyBCList["BCWall"]     = BCWall;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  /*
      Boundary conditons (delta01.mapbc):
      1. solid wall top
      2. solid wall bottom
      3. solid wall bevel
      4. solid wall back face
      5. freestream x min
      6. freestream x max
      7. symmetry y 0
      8. freestream y max
      9. freestream z min
      10. freestream z max

      starting from 0

      Boundary conditons (delta01.mapbc):
      0. solid wall top
      1. solid wall bottom
      2. solid wall bevel
      3. solid wall back face
      4. freestream x min
      5. freestream x max
      6. symmetry y 0
      7. freestream y max
      8. freestream z min
      9. freestream z max
   */
  // Define the BoundaryGroups for each boundary condition
  if (mesher == "avro")
  {
    BCBoundaryGroups["BCFarField"] = {0,2,3,4,5};
    BCBoundaryGroups["BCSymmetry"] = {1};
    BCBoundaryGroups["BCWall"]     = {6,7,8,9};
    BCBoundaryGroups["BCOutflow"]  = {};
  }
  else
  {
    BCBoundaryGroups["BCFarField"] = {4,5,7,8,9};
    BCBoundaryGroups["BCSymmetry"] = {6};
    BCBoundaryGroups["BCOutflow"]  = {};
    BCBoundaryGroups["BCWall"]     = {0,1,2,3};
  }

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-7, 1e-7, 1e-7};

  //Output functional
  NDOutputMach outputFcnMach(pde, Mach);
  OutputIntegrandMachClass outputIntegrandMach(outputFcnMach, {0});

  // Residual weighted boundary output
  Real dynpRef = 0.5*rhoRef*(uRef*uRef + vRef*vRef + wRef*wRef);

  NDOutputForce outputFcnDrag(pde,  cos(aoaRef)/(SRef*dynpRef), 0., sin(aoaRef)/(SRef*dynpRef));
  OutputIntegrandBoundaryClass outputIntegrandDrag( outputFcnDrag, BCBoundaryGroups["BCWall"] );

  NDOutputForce outputFcnLift(pde, -sin(aoaRef)/(SRef*dynpRef), 0., cos(aoaRef)/(SRef*dynpRef));
  OutputIntegrandBoundaryClass outputIntegrandLift( outputFcnLift, BCBoundaryGroups.at("BCWall") );

  NDOutputMoment outputFcnMoment(pde, CG[0], CG[1], CG[2], 0, 1, 0, SRef*dynpRef*lRef);
  OutputIntegrandBoundaryClass outputIntegrandMoment( outputFcnMoment, BCBoundaryGroups.at("BCWall") );



  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
if (world.rank() == 0 )
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  //PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 10;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;

  PreconditionerILU_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner.Name]
    = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.FillLevel] = 10;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  // PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.Overlap] = 1;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict_adjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-15;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif 0 && defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = true;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
  //NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 250;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  //--------ADAPTATION LOOP--------
  for (int order = orderL; order <= orderH; order++)
  {
    for (int power = powerL; power <= powerH; power++ )
    {
      int startIter = 0;
      stab.setNitscheOrder(order);

      Real nk = pow(2,power);
      Real targetCost = 1000*nk;

      std::shared_ptr<XField<PhysD3, TopoD3>> pxfld;

      // load in the really big starting grid for this polynomial order
      if (readBigGrid)
      {
        std::string xfld_filename;
        if (world.rank() == 0)
        {
          xfld_filename = "grids/DeltaWing/xfld_initial_FINE_P" + std::to_string(order) + ".meshb";

          // Check it exists, if it does, change the initial grid read
          if ( boost::filesystem::exists(xfld_filename) )
          {
            init_grid_path = xfld_filename;
            std::cout << "starting from grid: " << xfld_filename << std::endl;
          }
        }
#ifdef SANS_MPI
        boost::mpi::broadcast( world, init_grid_path, 0 ); // get the file to be read off of rank 0
#endif
      }


      // initial Grid
#ifdef SANS_AVRO
      std::shared_ptr<avro::Context> context;
      if (mesher == "avro")
      {
        using avro::coord_t;
        using avro::index_t;

        context = std::make_shared<avro::Context>();
        std::string input_base = "grids/DeltaWing/";

        std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(),"delta");
        model->load( input_base+"DeltaWing.egads" );

#if 0
        std::string mesh_file = input_base + "delta02_attached2DeltaWingCSM.mesh";
        avro::Gamma<avro::Simplex> mesh0(3,3);
        if (world.rank() == 0)
        {
          mesh0.readMesh( mesh_file );
          mesh0.readGeometry( mesh_file, *model );
        }

        // create the domain from the model
        std::shared_ptr< XField_avro<PhysD3,TopoD3> > pxfld_avro = std::make_shared< XField_avro<PhysD3,TopoD3> >(world,model);

        // read the avro mesh into the domain
        pxfld_avro->import( mesh0 );

        pxfld = pxfld_avro;
#else
        XField_libMeshb<PhysD3, TopoD3> xfld( world, init_grid_path );
        pxfld = std::make_shared< XField_avro<PhysD3,TopoD3> >(xfld,model);
#endif
      }
      else
#endif
        pxfld = std::make_shared<XField_libMeshb<PhysD3, TopoD3>>(world, init_grid_path );

      // to make sure folders have a consistent number of zero digits
      const int string_pad = 7;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);

#ifdef BOUNDARYOUTPUT
      std::string prefix = "Cd";
#else
      std::string prefix = "Mach";
#endif

      std::string filename_base = "tmp/DeltaWing/MOESS/" + mesher + "/VMSD_" + prefix + "_";

      if (file_tag.size() > 0 && file_tag != "_")
        filename_base += file_tag + "_"; // the additional file name bits kept especially

      filename_base += int_pad + "_P" + std::to_string(order) + "/";

      boost::filesystem::path base_dir(filename_base);
      if ( not boost::filesystem::exists(base_dir) )
        boost::filesystem::create_directories(filename_base);

      // Graceful restart capability

      const bool RESTART = true;// (order == 2 && power == 9);

      // read backwards from the end and look for the last file
      if (RESTART)
      {
        std::string xfld_filename;
        if (world.rank() == 0)
          for (int read = maxIter; read >=0; read--)
          {
            xfld_filename = filename_base + "mesh_a" + std::to_string(read) + "_outQ1.grm";
            if ( boost::filesystem::exists(xfld_filename) )
            {
              startIter = read+1;
              break; // found the file, exit the loop
            }
          }
#ifdef SANS_MPI
        boost::mpi::broadcast( world, xfld_filename, 0 ); // get the file to be read off of rank 0
        boost::mpi::broadcast( world, startIter, 0); // get the start iteration from rank 0
#endif

        // If the last grid is from the last adaptation then don't even run!
        if (startIter == maxIter+1)
        {
          if (world.rank() == 0)
            std::cout << "Not starting: Final mesh is greater than maxIter" << std::endl;
          return;
        }

        // read the old grid back in
        if (startIter > 0)
          pxfld = std::make_shared<XField_PX<PhysD3,TopoD3>>(world, xfld_filename);
      }

      if (world.rank() == 0)
      {
        if (startIter == 0)
          std::cout << "Starting from Initial mesh" << std::endl;
        else
          std::cout << "Starting mesh " << std::to_string(startIter-1) << std::endl;
      }

#ifdef SANS_PETSC
      PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;
#endif

      // CONVERT TO DG DOFS - This should give roughly equal number of elements compared to the DG solves
      if (DGCONV)
      {
        Real nDOFperCell_DG = (order+1)*(order+2)*(order+3)/6;

        Real nDOFperCell_CG = nDOFperCell_DG;
        nDOFperCell_CG -= (4 - 1./5); // the node dofs are shared by 20
        nDOFperCell_CG -= (6 - 1)*std::max(0,(order-1)); // if there are edge dofs they are shared by 6
        nDOFperCell_CG -= (4 - 2)*std::max(0,(order-1)*(order-2)/2); // if there are face dofs they are shared by 2

        targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
      }


      fstream fadapthist, foutputhist;
      if (world.rank() == 0)
      {
        std::string adapthist_filename = filename_base + "test.adapthist";
        fadapthist.open( adapthist_filename, startIter > 0 ? fstream::app : fstream::out );
        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

        std::string outputhist_filename = filename_base + "output.dat";
        foutputhist.open( outputhist_filename, startIter > 0 ? fstream::app : fstream::out );
        BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + outputhist_filename);
      }

      PyDict ParamDict;

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;
      MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
      MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;
      MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;

      PyDict MesherDict;
      if (mesher == "refine")
      {
        MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.refine;
        MesherDict[refineParams::params.FilenameBase] = filename_base;
        //MesherDict[refineParams::params.CADfilename] = ".egads";
#ifdef SANS_REFINE
        MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;
#endif
      }
#ifdef SANS_AVRO
      else if (mesher == "avro")
      {
        MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.avro;
        MesherDict[avroParams::params.FilenameBase] = filename_base;
        MesherDict[avroParams::params.Curved] = false; // is the grid curved?
        MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
        MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files
      }
#endif
      else if (mesher == "fefloa")
      {
        MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.FeFloa;
        MesherDict[FeFloaParams::params.FilenameBase] = filename_base;
      }
      else if (mesher == "EPIC")
      {
        MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
        //  MesherDict[EpicParams::params.Version] = "madcap_devel";
        //  MesherDict[EpicParams::params.CurvedQOrder] = 1; //Mesh order
        //  MesherDict[EpicParams::params.CSF] = "tmp/hemisphere-cylinder/EPIC/hemisphere_cylinder3_cf.csf"; //madcap surface file
        //  MesherDict[EpicParams::params.GeometryFile3D] = "tmp/hemisphere-cylinder/EPIC/hemisphere_cylinder3_cf.igs"; //CAD file
        //  MesherDict[EpicParams::params.Shell] = "SHELL";
        //  MesherDict[EpicParams::params.BoundaryObject] = "BDIST";
        //  MesherDict[EpicParams::params.minGeom] = 1e-6;
        //  MesherDict[EpicParams::params.maxGeom] = 2000;
        //  MesherDict[EpicParams::params.nPointGeom] = -1;
        std::vector<int> allBC;
        allBC.insert(allBC.end(), BCBoundaryGroups["BCFarField"].begin(), BCBoundaryGroups["BCFarField"].end());
        allBC.insert(allBC.end(), BCBoundaryGroups["BCSymmetry"].begin(), BCBoundaryGroups["BCSymmetry"].end());
        allBC.insert(allBC.end(), BCBoundaryGroups["BCOutflow"].begin(), BCBoundaryGroups["BCOutflow"].end());
        allBC.insert(allBC.end(), BCBoundaryGroups["BCWall"].begin(), BCBoundaryGroups["BCWall"].end());

        MesherDict[EpicParams::params.SymmetricSurf] = allBC;
        MesherDict[EpicParams::params.nThread] = 3;

        //  MesherDict[EpicParams::params.ViscSurf] = BCBoundaryGroups["BCWall"];
        //  MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;
        MesherDict[EpicParams::params.FilenameBase] = filename_base;
      }

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.dumpMetric] = false;

      MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

      MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      const int porder = order;
      const int pporder = order+1;

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, stab,
                                                   order, porder, order+1, pporder,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   active_boundaries, ParamDict);

      //Set initial solution
      pGlobalSol->setSolution(q0);

      const int quadOrder = 3*order + 1;

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
#ifdef BOUNDARYOUTPUT
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrandDrag);
#else
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrandMach);
#endif

      //Set initial solution
      //std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
      //output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

      pInterface->solveGlobalPrimalProblem();

      if (dumpField)
      {
        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(startIter) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
      }

      pInterface->solveGlobalAdjointProblem();

      if (dumpField)
      {
        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(startIter) + ".plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
      }

      //Compute error estimates
      pInterface->computeErrorEstimates();

      Real globalEstimate=0,globalIndicator=0,globalOutput=0;
      globalEstimate  = pInterface->getGlobalErrorEstimate();
      globalIndicator = pInterface->getGlobalErrorIndicator();
      globalOutput    = pInterface->getOutput();

      //std::string efld_filename = filename_base + "efld_a" + std::to_string(0) + ".plt";

      //pInterface->output_EField(efld_filename);

      //----------//
      QuadratureOrder quadrule(*pxfld, quadOrder);

      Real outputLift = 0;
      pInterface->AlgEqnSet().dispatchBC().dispatch_VMSD(
          FunctionalBoundaryTrace_Dispatch_VMSD(outputIntegrandLift, pGlobalSol->paramfld,
                                                    pGlobalSol->primal.qfld, pGlobalSol->primal.qpfld,
                                                    quadrule.boundaryTraceOrders.data(),
                                                    quadrule.boundaryTraceOrders.size(), outputLift ) );

#ifdef SANS_MPI
      outputLift = boost::mpi::all_reduce( *pxfld->comm(), outputLift, std::plus<Real>() );
#endif

      Real outputMoment = 0;
      pInterface->AlgEqnSet().dispatchBC().dispatch_VMSD(
          FunctionalBoundaryTrace_Dispatch_VMSD(outputIntegrandMoment, pGlobalSol->paramfld,
                                                    pGlobalSol->primal.qfld, pGlobalSol->primal.qpfld,
                                                    quadrule.boundaryTraceOrders.data(),
                                                    quadrule.boundaryTraceOrders.size(), outputMoment ) );

#ifdef SANS_MPI
      outputMoment = boost::mpi::all_reduce( *pxfld->comm(), outputMoment, std::plus<Real>() );
#endif

#ifdef SANS_MPI
      int nDOFtotal = 0;
      boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );
#else
      int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
#endif

      if (world.rank() == 0 )
      {
        if (startIter == 0)
        {
          // write the header to the output file
          foutputhist << "VARIABLES="
                      << std::setw(5)  << "\"Iter\""
                      << std::setw(10) << "\"DOF\""
                      << std::setw(20) << "\"C<sub>L</sub>\""
                      << std::setw(20) << "\"C<sub>D</sub>\""
                      << std::setw(20) << "\"C<sub>M</sub>\""
                      << std::setw(20) << "\"Estimate\""
                      << std::setw(20) << "\"Indicator\""
                      << std::endl;

#ifdef BOUNDARYOUTPUT
          foutputhist << "ZONE T=\"MOESS C<sub>D</sub> " << nk << "k\"" << std::endl;
#else
          foutputhist << "ZONE T=\"MOESS M " << nk << "k\"" << std::endl;
#endif
        }
        foutputhist << std::setw(5) << startIter
                    << std::setw(10) << nDOFtotal
                    << std::setw(20) << std::setprecision(10) << std::scientific << outputLift
                    << std::setw(20) << std::setprecision(10) << std::scientific << globalOutput
                    << std::setw(20) << std::setprecision(10) << std::scientific << outputMoment
                    << std::setw(20) << std::setprecision(10) << std::scientific << globalEstimate
                    << std::setw(20) << std::setprecision(10) << std::scientific << globalIndicator
                    << std::endl;
      }

      for (int iter = startIter; iter < maxIter+1; iter++)
      {
        if ( world.rank() == 0 ) std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        if (dumpField)
        {
          std::string machfld_filename = filename_base + "machfld_a"  + std::to_string(iter);
          computeMachField( pde, pGlobalSol->primal.qfld, machfld_filename ); // .solb and .dat files are written
        }


        // std::string xfld_filename = filename_base + "xfld_a"  + std::to_string(iter) + ".meshb";
        // WriteMesh_libMeshb( *pxfld, xfld_filename);

        //Perform local sampling and adapt mesh
        std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew;
        pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, stab,
                                                         order, porder, order+1, pporder,
                                                         BasisFunctionCategory_Lagrange,
                                                         BasisFunctionCategory_Lagrange,
                                                         BasisFunctionCategory_Lagrange,
                                                         active_boundaries, ParamDict);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
#ifdef BOUNDARYOUTPUT
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, AdjLinearSolverDict,
                                                               outputIntegrandDrag);
#else
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, AdjLinearSolverDict,
                                                               outputIntegrandMach);
#endif

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;
        pxfld = pxfldNew;

        //std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
        //output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

        pInterface->solveGlobalPrimalProblem();

        if (dumpField)
        {
          std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
        }
        else if ( iter == maxIter )
        {
          std::string qfld_filename = filename_base + "qfld_final.plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
        }

        pInterface->solveGlobalAdjointProblem();

        if (dumpField)
        {
          std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
          output_Tecplot( pInterface->getAdjField(), adjfld_filename );
        }
        else if ( iter == maxIter )
        {
          std::string adjfld_filename = filename_base + "adjfld_final.plt";
          output_Tecplot( pInterface->getAdjField(), adjfld_filename );
        }

        //Compute error estimates
        pInterface->computeErrorEstimates();

        Real globalEstimate=0,globalIndicator=0,globalOutput=0;
        globalEstimate  = pInterface->getGlobalErrorEstimate();
        globalIndicator = pInterface->getGlobalErrorIndicator();
        globalOutput    = pInterface->getOutput();

        //----------//
        QuadratureOrder quadrule(*pxfld, quadOrder);

        Real outputLift = 0;
        pInterface->AlgEqnSet().dispatchBC().dispatch_VMSD(
            FunctionalBoundaryTrace_Dispatch_VMSD(outputIntegrandLift, pGlobalSol->paramfld,
                                                      pGlobalSol->primal.qfld, pGlobalSol->primal.qpfld,
                                                      quadrule.boundaryTraceOrders.data(),
                                                      quadrule.boundaryTraceOrders.size(), outputLift ) );

  #ifdef SANS_MPI
        outputLift = boost::mpi::all_reduce( *pxfld->comm(), outputLift, std::plus<Real>() );
  #endif

        Real outputMoment = 0;
        pInterface->AlgEqnSet().dispatchBC().dispatch_VMSD(
            FunctionalBoundaryTrace_Dispatch_VMSD(outputIntegrandMoment, pGlobalSol->paramfld,
                                                      pGlobalSol->primal.qfld, pGlobalSol->primal.qpfld,
                                                      quadrule.boundaryTraceOrders.data(),
                                                      quadrule.boundaryTraceOrders.size(), outputMoment ) );

  #ifdef SANS_MPI
        outputMoment = boost::mpi::all_reduce( *pxfld->comm(), outputMoment, std::plus<Real>() );
  #endif

#ifdef SANS_MPI
        int nDOFtotal = 0;
        boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );
#else
        int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
#endif

        if (world.rank() == 0)
        {
          foutputhist << std::setw(5) << iter+1
                      << std::setw(10) << nDOFtotal
                      << std::setw(20) << std::setprecision(10) << std::scientific << outputLift
                      << std::setw(20) << std::setprecision(10) << std::scientific << globalOutput
                      << std::setw(20) << std::setprecision(10) << std::scientific << outputMoment
                      << std::setw(20) << std::setprecision(10) << std::scientific << globalEstimate
                      << std::setw(20) << std::setprecision(10) << std::scientific << globalIndicator
                      << std::endl;
        }
      }

      fadapthist.close();
      foutputhist.close();
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
