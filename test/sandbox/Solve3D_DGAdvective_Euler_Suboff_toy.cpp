// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve3D_DGAdvective_Euler_Suboff_toy
// testing of 3-D DG Advective for Euler for the Suboff geometry

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/stringify.h"
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler3D.h"
#include "pde/NS/BCEuler3D.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_DGAdvective_Euler_Suboff_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGAdvective_Triangle_Bump10 )
{
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD3, TopoD3> > PrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD3, TopoD3>> AlgebraicEquationSet_PTCClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  //typedef DLA::VectorS<PhysD3::D, ArrayQ> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;

  mpi::communicator world;

  // PDE

  const Real gamma = 1.4;
  const Real R = 0.4;

  // reference state (freestream)
  const Real Mach = 0.5;

  // const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real wRef = 0;
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure

  // stagnation temperature, pressure
  //const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  //const Real TtRef = tRef*Gamma;
  //const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // BC
  // Create a BC dictionary
  PyDict BCSymmetry;

// PX STYLE BC:
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict RefState;
  RefState[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.PrimitiveTemperature;
  RefState[DensityVelocityTemperature3DParams::params.rho] = rhoRef;
  RefState[DensityVelocityTemperature3DParams::params.u] = uRef;
  RefState[DensityVelocityTemperature3DParams::params.v] = vRef;
  RefState[DensityVelocityTemperature3DParams::params.w] = wRef;
  RefState[DensityVelocityTemperature3DParams::params.t] = tRef;

  PyDict BCFarField;
  BCFarField[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFarField[BCEuler3D<BCTypeFullState_mitState, PDEClass>::ParamsType::params.Characteristic] = true;
  BCFarField[BCEuler3D<BCTypeFullState_mitState, PDEClass>::ParamsType::params.StateVector] = RefState;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCFarField"] = BCFarField;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2}; //body and symmetry plane
  BCBoundaryGroups["BCFarField"] = {1};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Set up Newton Solver
  PyDict NewtonSolverDict, NewtonLineUpdateDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  //PTC
  PyDict SolverContinuationDict, NonlinearSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;

  // Check inputs
  PseudoTimeParam::checkInputs(NonlinearSolverDict);

  //int ordermin = 1;
  //int ordermax = 1;

  for (int grid = 4; grid > 3; grid--)
  {

    int order0 = 0;

    //int fieldorder = 3;

    // grid
    std::string filename = "tmp/suboff/hch_tetra." + stringify(grid) + ".meshb";
    XField_libMeshb<PhysD3, TopoD3> xfld( world, filename );

    // Initial state
    ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure3D<Real>(rhoRef, uRef, vRef, wRef, pRef) );

    //////////////////////
    // SET UP P0 solve
    ///////////////////////

    Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld0(xfld, order0, BasisFunctionCategory_Legendre);

    // Set the initial condition
    qfld0 = q0;

    // Lagrange multiplier:
    Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ>
    lgfld0( xfld, order0, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
    lgfld0 = 0;

    QuadratureOrder quadratureOrder( xfld, - 1 );
    std::vector<Real> tol = {5e-8, 5e-8};

    ////////////
    //SOLVE P0 SYSTEM
    ////////////

    PrimalEquationSetClass PrimalEqSet0(xfld, qfld0, lgfld0, pde, quadratureOrder,
                                        ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups );
    AlgebraicEquationSet_PTCClass AlgEqSetPTC(xfld, qfld0, pde, quadratureOrder, {0}, PrimalEqSet0);

    PseudoTime<SystemMatrixClass> PTC(NonlinearSolverDict, AlgEqSetPTC);

    PTC.iterate(100);

#if 1
    // Tecplot dump grid
    {
    string filename = "tmp/suboff/slnDG_Euler_P";
    filename += to_string(order0);
    filename += "_v";
    filename += to_string(grid);
    filename += ".plt";
    output_Tecplot( qfld0, filename );
    }
#endif

#if 0
    ////////////////////////////////////
    // PROJECT P0 SOLUTION TO HIGHER P
    ///////////////////////////////////

    if (order !=0)
    {

      Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

      const int nDOFPDE = qfld.nDOF();

      // Set the initial condition
      qfld0.projectTo(qfld);

      // Lagrange multiplier:
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ>
      lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      lgfld = 0;

      //////////////////////////
      //SOLVE HIGHER P SYSTEM
      //////////////////////////

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0;

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      Real rsdPDEnrm[4] = {0,0,0,0};

      SolveStatus status = Solver.solve(ini, sln);
      BOOST_CHECK( status.converged );

      // check that the residual is zero

      for (int j = 0; j < nSol; j++)
        rsdPDEnrm[j] = 0;

      for (int n = 0; n < nDOFPDE; n++)
        for (int j = 0; j < nSol; j++)
          rsdPDEnrm[j] += pow(rsd[0][n][j],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[0]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[1]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[2]), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[3]), 1e-12 );

#if 1
      // Tecplot dump grid
      string filename = "tmp/Suboff/slnDG_Euler_P";
      filename += to_string(order);
      filename += "_Q";
      filename += to_string(fieldorder);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      output_Tecplot( qfld, filename );
#endif

    }
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
