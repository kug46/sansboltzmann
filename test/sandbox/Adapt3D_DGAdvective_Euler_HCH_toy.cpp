// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler3D.h"
#include "pde/NS/BCEuler3D.h"
#include "pde/NS/OutputEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/SolutionData_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"

#include "Adaptation/MOESS/SolverInterface_DGAdvective.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field/FieldVolume_CG_Cell.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_DGAdvective_Euler_HemisphereCylinder_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_DGAdvective_Euler_HemisphereCylinder_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  //typedef QTypePrimitiveSurrogate QType;
//  typedef QTypeConservative QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

#ifndef BOUNDARYOUTPUT
  typedef ScalarFunction3D_Const WeightFcn; // Change this to change output weighting type
  typedef OutputCell_WeightedSolution<PDEClass, WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler3D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef SolutionData_DGAdvective<PhysDim, TopoDim, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGAdvective<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

#define USE_REFINE 0

#if defined(SANS_REFINE) && USE_REFINE
  std::string filename_base = "tmp/hemisphere-cylinder/refine/";

  // initial Grid
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld = std::make_shared<XField_libMeshb<PhysD3, TopoD3>>(world, filename_base + "hsc01.meshb" );
#else
  std::string filename_base = "tmp/suboff/EPIC/";

  // initial Grid
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld = std::make_shared<XField_PX<PhysD3, TopoD3>>(world, "tmp/suboff/hchf3.q2.grm" );
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld_linear
    = std::make_shared<XField_PX<PhysD3, TopoD3>>(*pxfld, XFieldBalance::CellPartitionCopy, "tmp/suboff/hchf3.q1.grm" );
  //std::shared_ptr<XField<PhysD3, TopoD3>> pxfld = std::make_shared<XField_PX<PhysD3, TopoD3>>(world, "tmp/suboff/EPIC_Re10k/mesh_a30_outQ1.grm" );
#endif


  int orderMax = 1;


  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);

  // reference state (freestream)
  const Real Mach = 0.6;

  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 0;                            // angle of attack (radians)

#if 1
  const Real tRef = 1;          // temperature
  const Real pRef = rhoRef*R*tRef;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;
#endif

#if 0
  const Real qRef = 1;                              // velocity scale
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = 0;
  const Real wRef = qRef * sin(aoaRef);

  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef + wRef*wRef);

  // PDE
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure3D<Real>(rhoRef, uRef, vRef, wRef, pRef) );

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict StateVector;
  StateVector[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  StateVector[Conservative3DParams::params.rho] = rhoRef;
  StateVector[Conservative3DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative3DParams::params.rhov] = rhoRef*vRef;
  StateVector[Conservative3DParams::params.rhow] = rhoRef*wRef;
  StateVector[Conservative3DParams::params.rhoE] = rhoRef*ERef;

  PyDict BCFarField;
  BCFarField[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.StateVector] = StateVector;
  BCFarField[BCEuler3DFullStateParams<NSVariableType3DParams>::params.Characteristic] = true;


  PyDict PyBCList;
  PyBCList["BCFarField"] = BCFarField;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCWall"]     = BCWall;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCFarField"] = {0};
  BCBoundaryGroups["BCSymmetry"] = {1};
  BCBoundaryGroups["BCWall"]     = {2,3,4};

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-7, 1e-7};

#ifndef BOUNDARYOUTPUT
  //Output functional
  Real weight = 1.0;
  WeightFcn weightFcn(weight);
  NDOutputClass fcnOutput(weightFcn);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  // Residual weighted boundary output
  Real dynpRef = 0.5*rhoRef*(uRef*uRef + vRef*vRef + wRef*wRef);
  NDOutputClass outputFcn(pde, cos(aoaRef)/dynpRef, 0., sin(aoaRef)/dynpRef);
  OutputIntegrandClass outputIntegrand( outputFcn, BCBoundaryGroups["BCWall"] );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-9;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
  NewtonSolverDict[NewtonSolverParam::params.MaxChangeFraction] = 0.1;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  //--------ADAPTATION LOOP--------

  int maxIter = 30;
  int targetCost = 1e5;

  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist;
  if ( world.rank() == 0 )
  {
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

  PyDict MesherDict;
#if defined(SANS_REFINE) && USE_REFINE
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.refine;
  MesherDict[refineParams::params.FilenameBase] = filename_base;
  MesherDict[refineParams::params.CADfilename] = filename_base + "hemisph-cyl.egads";
  MesherDict[refineParams::params.GASFilename] = filename_base + "hsc01.gas";
  MesherDict[refineParams::params.DumpRefineDebugFiles] = true;
#else
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
  MesherDict[EpicParams::params.Version] = "madcap_devel-10.1"; //Mesh order
  MesherDict[EpicParams::params.CurvedQOrder] = 2; //Mesh order
  MesherDict[EpicParams::params.CSF] = "tmp/suboff/hchf_cf.csf"; //madcap surface file
  MesherDict[EpicParams::params.GeometryFile3D] = "tmp/suboff/hchf_cf.igs"; //CAD file
  MesherDict[EpicParams::params.Shell] = "SHELL";
  MesherDict[EpicParams::params.BoundaryObject] = "BDIST";
  MesherDict[EpicParams::params.nAdaptIter] = 15;
  //MesherDict[EpicParams::params.minGeom] = 0.01;
  MesherDict[EpicParams::params.maxGeom] = 100;
  MesherDict[EpicParams::params.nPointGeom] = 16;
  MesherDict[EpicParams::params.NodeMovement] = false;
  MesherDict[EpicParams::params.SlipSurf] = BCBoundaryGroups["BCSymmetry"];
  MesherDict[EpicParams::params.ViscSurf] = BCBoundaryGroups["BCWall"];
  MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;
#endif

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

  MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGAdvective<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol, pGlobalSolNew;
  std::shared_ptr<SolverInterfaceClass> pInterface;

  for (int order = 0; order <= orderMax; order++)
  {
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries);

    //Set initial solution
    if (order == 0)
    {
      pGlobalSol = pGlobalSolNew;
      pGlobalSol->setSolution(q0);
    }
    else
    {
      pGlobalSolNew->setSolution(*pGlobalSol);
      pGlobalSol = pGlobalSolNew;
    }

    // Adjoint equation is integrated with 3 times adjoint order
    const int quadOrder = 3*(order+1);

    //Create solver interface
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, LinearSolverDict,
                                                        outputIntegrand);

    //Set initial solution
    std::string qfld_init_filename = filename_base + "qfld_init_a0_P" + std::to_string(order) + "_rank" + std::to_string(world.rank()) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();

    std::string qfld_filename = filename_base + "qfld_a0_P" + std::to_string(order) + "_rank" + std::to_string(world.rank()) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
  }

  pInterface->solveGlobalAdjointProblem();

  std::string adjfld_filename = filename_base + "adjfld_a0_P" + std::to_string(orderMax) + "_rank" + std::to_string(world.rank()) + ".plt";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
#if 1
    MeshAdapter<PhysD3, TopoD3>::MeshPtrPair ptr_pair;
    ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew_linear = ptr_pair.first;
    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew = ptr_pair.second;
#else
    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);
#endif

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    for (int order = 0; order <= orderMax; order++)
    {
      pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                      BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                      active_boundaries);

      // Adjoint equation is integrated with 3 times adjoint order
      const int quadOrder = 3*(order+1);

      //Perform L2 projection from solution on previous mesh
      pGlobalSolNew->setSolution(*pGlobalSol);
      pGlobalSol = pGlobalSolNew;

      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1)
                                                     + "_P" + std::to_string(order) +
                                                     + "_rank" + std::to_string(world.rank()) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

      pInterface->solveGlobalPrimalProblem();

      std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1)
                                                + "_P" + std::to_string(order) +
                                                + "_rank" + std::to_string(world.rank()) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
    }

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld_linear = pxfldNew_linear;
    pxfld = pxfldNew;

    pInterface->solveGlobalAdjointProblem();

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1)
                                                + "_P" + std::to_string(orderMax) +
                                                + "_rank" + std::to_string(world.rank()) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
  }

  fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
