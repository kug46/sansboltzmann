// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DG_AD_DoubleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionGradientErrorSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"
#include "Meshing/Embedding/MesherInterface_Embedding.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Meshing/XField1D/XField1D.h"


#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt1D_DG_AD_BoundaryLayer_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt1D_AD_BoundaryLayer_Line )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;

  //typedef ScalarFunction1D_Sine SolutionExact;
  // typedef ScalarFunction1D_Exp2 SolutionExact;
  typedef ScalarFunction1D_BL SolutionExact;
  //typedef ScalarFunction1D_Tanh SolutionExact;
  //
  // typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

//    typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
//  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputCell_SolutionGradientErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD1, TopoD1, NDPDEClass, ParamBuilderType> SolutionClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;



  // PDE
  Real a = 1.0, nu = 0.1;
  AdvectiveFlux1D_Uniform adv( a );

  ViscousFlux1D_Uniform visc( nu );

  Source1D_UniformGrad source(0.0,0.0);

  PyDict Soln;
  Soln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.Name] =
       BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.BL;
  Soln[SolutionExact::ParamsType::params.a] = a;
  Soln[SolutionExact::ParamsType::params.nu] = nu;
  Soln[SolutionExact::ParamsType::params.src] = 0.0;
  Soln[SolutionExact::ParamsType::params.scale] = -1;
  Soln[SolutionExact::ParamsType::params.offset] = 1;

  SolutionExact MMS_soln(Soln);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(MMS_soln));

  NDPDEClass pde(adv, visc, source, forcingptr);
  // NDPDEClass pde(adv,visc,source);

  // BC
  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function] = Soln;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
         BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // integrands
  NDOutputClass fcnOutput(MMS_soln);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-10, 1e-10};

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::vector<Real> targetCosts = {1000};//,50,100,200,500,1000};

  std::shared_ptr<XField<PhysD1, TopoD1>> pxfld;

  for (int order=1;order>0;order--)
  {

  for (auto it = targetCosts.begin(); it != targetCosts.end(); ++it)
  {
    Real targetCost = *it;

    // create the initial grid
    int nElements = 2;
    Real xL = 0;
    Real xR = 1.;
    pxfld = std::make_shared<XField1D>( nElements , xL , xR );

    const int maxIter = 100;

    // to make sure folders have a consistent number of zero digits
    const int string_pad = 5;
    std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
    std::string filename_base = "tmp/adapt/1d/" + int_pad + "_P" + std::to_string(order) + "/";

    boost::filesystem::create_directories(filename_base);

    std::string adapthist_filename = filename_base + "test.adapthist";
    fstream fadapthist( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

    PyDict MOESSDict;
    MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
    MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
    MOESSDict[MOESSParams::params.UniformRefinement] = true;
    MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction] = 1.0;
    MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

    PyDict MesherDict;
    MesherDict[MeshAdapterParams<PhysD1, TopoD1>::params.Mesher.Name] = MeshAdapterParams<PhysD1, TopoD1>::params.Mesher.embedding;

    PyDict AdaptDict;
    AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.TargetCost] = targetCost;
    AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.Algorithm] = MOESSDict;
    AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.Mesher] = MesherDict;
    AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.FilenameBase] = filename_base;
    AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.dumpStepMatrix] = true;
    AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.dumpRateMatrix] = true;
    AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.hasTrueOutput] = true;
    AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.TrueOutput] = 3./(25*exp(9)) + 2.*exp(1)/25;

    MeshAdapterParams<PhysD1, TopoD1>::checkInputs(AdaptDict);

    MeshAdapter<PhysD1, TopoD1> mesh_adapter(AdaptDict, fadapthist);

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    // solution data
    std::shared_ptr<SolutionClass> pGlobalSol;
    pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                 BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                 active_boundaries, disc);

    const int quadOrder = 2*(order + 1);

    // create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, LinearSolverDict,
                                                        outputIntegrand);

    // initialize the solution to 0
    pGlobalSol->setSolution(0.0);

    // solve the initial primal and dual
    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    std::string qfld_filename = filename_base + "qfld_a0.plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a0.plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

//    std::string efld_filename = filename_base + "efld_a0.plt";
//    pInterface->output_EField( efld_filename );

    for (int iter = 0; iter < maxIter+1; iter++)
    {
      std::cout << "-----Adaptation Iteration " << iter << "-----" << std::endl;

      // compute error estimates
      pInterface->computeErrorEstimates();

      // perform local sampling and adapt mesh
      std::shared_ptr<XField<PhysD1, TopoD1>> pxfldNew;
      pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

      interiorTraceGroups.clear();
      for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<SolutionClass> pGlobalSolNew;
      pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                      BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                      active_boundaries, disc);

      // perform L2 projection from solution on previous mesh
      pGlobalSolNew->setSolution(*pGlobalSol);

      std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
      pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                             cellGroups, interiorTraceGroups,
                                                             PyBCList, BCBoundaryGroups,
                                                             SolverContinuationDict, LinearSolverDict,
                                                             outputIntegrand);

      // update pointers to the newest problem (this deletes the previous mesh and solutions)
      pxfld = pxfldNew;
      pGlobalSol = pGlobalSolNew;
      pInterface = pInterfaceNew;

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

//      std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".plt";
//      pInterface->output_EField( efld_filename );


    } // adaptation iterations

    fadapthist.close();

  } // target costs

  } // solution order

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
