// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_TwoPhase_ArtificialViscosity_ST_SPE10_toy
// Testing of the MOESS framework on a space-time two-phase problem: SPE10

#define USE_PETSC_SOLVER

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/OutputTwoPhase.h"
#include "pde/PorousMedia/Sensor_TwoPhase.h"
//#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/VectorFunction2D.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime2D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "Field/output_Tecplot_PDE.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#endif

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_TwoPhase_ArtificialViscosity_ST_SPE10_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_TwoPhase_ArtificialViscosity_ST_SPE10_Triangle )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;

  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_CartTable PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_CartTable RockPermModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelTwoPhaseClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                            TraitsModelTwoPhaseClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTwoPhaseArtificialViscosity2DVector<TraitsSizeTwoPhaseArtificialViscosity,
                                                TraitsModelAV> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhaseArtificialViscosity,
                                                TraitsModelTwoPhaseClass> SourceOutflowClass;
  typedef SourceOutflowClass::ParamsType SourceOutflowParamClass;

  typedef OutputTwoPhase2D_Flowrate<SourceOutflowClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_GenH_DG ParamBuilderType;
  typedef GenHField_DG<PhysDim, TopoDim> GenHFieldType;

  typedef SolutionData_DGBR2<PhysDim, TopoDim, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  timer clock;

  const int order = 1;
  const int order_min = 1; //for p-sequencing

  int maxIter = 20;
  Real targetCost = 50000;
  std::string filename_base = "tmp/";

  const Real T = 100; //final time in days -- should be 100 yrs

//  XField3D_Box_Tet_X1 xfld_tmp( 10, 10, 10, 0, Lx, 0, Ly, 0, T );
//  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld_tmp(xfld_tmp, 1, BasisFunctionCategory_Legendre);
//  std::fstream fqfld("tmp/qfld_init_R100.txt", std::fstream::in);
//  for (int i = 0; i < qfld_tmp.nDOF(); i++)
//    for (int j = 0; j < ArrayQ::M; j++)
//      fqfld >> qfld_tmp.DOF(i)[j];
//  fqfld.close();

  // Grid
  std::shared_ptr<XField<PhysDim, TopoDim>> pxfld;
  PyDict dictMesher;

#ifdef SANS_AVRO
  dictMesher[MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.Name] = MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.avro;
  dictMesher[avroParams::params.Curved] = false; // is the grid curved?
  dictMesher[avroParams::params.WriteMesh] = false; // write all the .mesh files
  dictMesher[avroParams::params.WriteConformity] = false; // write all the .json files
  using avro::coord_t;
  using avro::index_t;

  // create the context
  std::shared_ptr<avro::Context> context = std::make_shared<avro::Context>();

  //Domain size
  const Real Lx = 1200; //ft
  const Real Ly = 2200; //ft

  std::vector<avro::real> x0 = {0., 0., 0.};
  std::vector<avro::real> xL = {Lx, Ly, T};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>(context.get(), x0.data(), xL.data());
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(), "cube");
  model->addBody(pbody,true);

  XField3D_Box_Tet_X1 xfld0( world, 10, 10, 10, 0, Lx, 0, Ly, 0, T );
  // copy the mesh into the domain and attach the geometry
  pxfld = std::make_shared< XField_avro<PhysDim,TopoDim> >(xfld0, model);
#else
  BOOST_FAIL( "avro is only current mesher option; to use, define SANS_AVRO" );
#endif

  const int iXmin = XField3D_Box_Tet_X1::iXmin;
  const int iXmax = XField3D_Box_Tet_X1::iXmax;
  const int iYmin = XField3D_Box_Tet_X1::iYmin;
  const int iYmax = XField3D_Box_Tet_X1::iYmax;
  const int iTmin = XField3D_Box_Tet_X1::iZmin;
  const int iTmax = XField3D_Box_Tet_X1::iZmax;

  // PDE
  const Real pref = 6000.0; //psi

  DensityModel rhow(64.0, 3.0e-6, pref);
  DensityModel rhon(53.0, 1.375e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(0.3);
  ViscModel mun(3.0);

  const Real pc_max = 0.0;
  CapillaryModel pc(pc_max);

  // porosity & permeability table lookup
  std::string lookupDir = "PorousMedia/";
  std::string lookupPoro = lookupDir + "spe10_layer62_poro.txt";
  std::string lookupPerm = lookupDir + "spe10_layer62_perm.txt";
//  std::string lookupPoro = lookupDir + "spe10_poro_homogeneous.txt";
//  std::string lookupPerm = lookupDir + "spe10_perm_homogeneous.txt";

  PorosityModel phi(lookupPoro, 3.0e-6, pref);

  // check porosity lookup
  {
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real pn = pref;
  Real x, y, time = 0;
  Real phiVal, phiValTrue;

  x = 1; y = 1; phiValTrue = 0.1;
  phiVal = phi.porosity(x, y, time, pn);
  SANS_CHECK_CLOSE( phiValTrue, phiVal, small_tol, close_tol);
  x = 1199; y = 1; phiValTrue = 0.1307;
  phiVal = phi.porosity(x, y, time, pn);
  SANS_CHECK_CLOSE( phiValTrue, phiVal, small_tol, close_tol);
  x = 1; y = 2199; phiValTrue = 0.27012;
  phiVal = phi.porosity(x, y, time, pn);
  SANS_CHECK_CLOSE( phiValTrue, phiVal, small_tol, close_tol);
  x = 1199; y = 2199; phiValTrue = 0.29862;
  phiVal = phi.porosity(x, y, time, pn);
  SANS_CHECK_CLOSE( phiValTrue, phiVal, small_tol, close_tol);
  }

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real m2_per_mD = 9.869233e-16;
  const Real scale_factor = conversion / m2_per_mD;

  PyDict dictPerm;
  dictPerm[PermeabilityModel2D_CartTable_Params::params.PermeabilityFile] = lookupPerm;
  dictPerm[PermeabilityModel2D_CartTable_Params::params.scale_factor] = scale_factor;
  PermeabilityModel2D_CartTable_Params::checkInputs(dictPerm);

  RockPermModel K(dictPerm);

  // check permeability lookup
  {
  const Real small_tol = 1e-18;
  const Real close_tol = 1e-18;

  Real x, y, time = 0;
  Real Kxx, Kxy, Kyx, Kyy;
  Real KxxTrue, KxyTrue, KyxTrue, KyyTrue;
  KxyTrue = KyxTrue = 0;

  x = 1; y = 1; KxxTrue = KyyTrue = scale_factor*9.582e-18;
  K.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);
  SANS_CHECK_CLOSE( KxxTrue, Kxx, small_tol, close_tol);
  SANS_CHECK_CLOSE( KxyTrue, Kxy, small_tol, close_tol);
  SANS_CHECK_CLOSE( KyxTrue, Kyx, small_tol, close_tol);
  SANS_CHECK_CLOSE( KyyTrue, Kyy, small_tol, close_tol);
  x = 1199; y = 1; KxxTrue = KyyTrue = scale_factor*3.722e-13;
  K.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);
  SANS_CHECK_CLOSE( KxxTrue, Kxx, small_tol, close_tol);
  SANS_CHECK_CLOSE( KxyTrue, Kxy, small_tol, close_tol);
  SANS_CHECK_CLOSE( KyxTrue, Kyx, small_tol, close_tol);
  SANS_CHECK_CLOSE( KyyTrue, Kyy, small_tol, close_tol);
  x = 1; y = 2199; KxxTrue = KyyTrue = scale_factor*4.3949e-13;
  K.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);
  SANS_CHECK_CLOSE( KxxTrue, Kxx, small_tol, close_tol);
  SANS_CHECK_CLOSE( KxyTrue, Kxy, small_tol, close_tol);
  SANS_CHECK_CLOSE( KyxTrue, Kyx, small_tol, close_tol);
  SANS_CHECK_CLOSE( KyyTrue, Kyy, small_tol, close_tol);
  x = 1199; y = 2199; KxxTrue = KyyTrue = scale_factor*1.5876e-13;
  K.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);
  SANS_CHECK_CLOSE( KxxTrue, Kxx, small_tol, close_tol);
  SANS_CHECK_CLOSE( KxyTrue, Kxy, small_tol, close_tol);
  SANS_CHECK_CLOSE( KyxTrue, Kyx, small_tol, close_tol);
  SANS_CHECK_CLOSE( KyyTrue, Kyy, small_tol, close_tol);
  }
  //printf( "made it past poro/perm checks\n" ); fflush(stdout);

  // Set up source term PyDicts

  Real R_bore = 1.0/6.0; //ft
  Real R_well = 100.0; //ft

  Real pnIn = 10000.0; //psi
  Real SwIn = 1.0;
  Real pnOut = 4000.0; //psi

  Real pwInit = 6000;
  Real SwInit = 0.1;
  Real sensorInit = 5.0;

  Real xcenInjection =  610.;
  Real ycenInjection = 1095.;

  std::vector<Real> xcenProduction = {90., 1110.,   90., 1110.};
  std::vector<Real> ycenProduction = {95.,   95., 2105., 2105.};

  const int nWellParam = 6;

  PyDict dictWellIn;
  typedef SourceTwoPhase2DType_FixedPressureInflow_Param SourceInflowParamClass;
  dictWellIn[SourceInflowParamClass::params.pB] = pnIn;
  dictWellIn[SourceInflowParamClass::params.Sw] = SwIn;
  dictWellIn[SourceInflowParamClass::params.Rwellbore] = R_bore;
  dictWellIn[SourceInflowParamClass::params.nParam] = nWellParam;
  dictWellIn[SourceInflowParamClass::params.WellModel] = SourceInflowParamClass::params.WellModel.Polynomial;
  dictWellIn[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureInflow;

  PyDict dictWellOut;
  dictWellOut[SourceOutflowParamClass::params.pB] = pnOut;
  dictWellOut[SourceOutflowParamClass::params.Rwellbore] = R_bore;
  dictWellOut[SourceOutflowParamClass::params.nParam] = nWellParam;
  dictWellOut[SourceOutflowParamClass::params.WellModel] = SourceOutflowParamClass::params.WellModel.Polynomial;
  dictWellOut[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

  PyDict dictSourceInjection;
  dictSourceInjection[SourceTwoPhase2DParam::params.Source] = dictWellIn;
  dictSourceInjection[SourceTwoPhase2DParam::params.xcentroid] = xcenInjection;
  dictSourceInjection[SourceTwoPhase2DParam::params.ycentroid] = ycenInjection;
  dictSourceInjection[SourceTwoPhase2DParam::params.R] = R_well;
  dictSourceInjection[SourceTwoPhase2DParam::params.Tmin] = 0;
  dictSourceInjection[SourceTwoPhase2DParam::params.Tmax] = T;
  dictSourceInjection[SourceTwoPhase2DParam::params.smoothLr] = 0.0*R_well;
  dictSourceInjection[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  dictSourceInjection[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  std::vector<PyDict> dictSourceProduction(4);
  for (int n = 0; n < 4; n++)
  {
    dictSourceProduction[n][SourceTwoPhase2DParam::params.Source] = dictWellOut;
    dictSourceProduction[n][SourceTwoPhase2DParam::params.xcentroid] = xcenProduction[n];
    dictSourceProduction[n][SourceTwoPhase2DParam::params.ycentroid] = ycenProduction[n];
    dictSourceProduction[n][SourceTwoPhase2DParam::params.R] = R_well;
    dictSourceProduction[n][SourceTwoPhase2DParam::params.Tmin] = 0;
    dictSourceProduction[n][SourceTwoPhase2DParam::params.Tmax] = T;
    dictSourceProduction[n][SourceTwoPhase2DParam::params.smoothLr] = 0.0*R_well;
    dictSourceProduction[n][SourceTwoPhase2DParam::params.smoothT] = 0.0;
    dictSourceProduction[n][SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;
  }

  PyDict dictSource;
  dictSource["dictSourceInjection"]   = dictSourceInjection;
  dictSource["dictSourceProduction0"] = dictSourceProduction[0];
  dictSource["dictSourceProduction1"] = dictSourceProduction[1];
  dictSource["dictSourceProduction2"] = dictSourceProduction[2];
  dictSource["dictSourceProduction3"] = dictSourceProduction[3];

  SourceTwoPhase2DListParam::checkInputs(dictSource);

  // TwoPhase PDE with AV
  bool hasSpaceTimeDiffusionTwoPhase = false;
  PDEBaseClass pdeTwoPhaseAV(order, hasSpaceTimeDiffusionTwoPhase, rhow, rhon, phi, krw, krn, muw, mun, K, pc, dictSource);

  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  bool hasSpaceTimeDiffusionAV = true;
  const Real diffusionConstantAV = 3.0;
  SensorViscousFlux sensor_visc(order, hasSpaceTimeDiffusionAV, 1.0, diffusionConstantAV);
  const Real Sk_min = -3;
  const Real Sk_max = -1;
  SensorSource sensor_source(pdeTwoPhaseAV, Sk_min, Sk_max);

  // AV PDE with sensor equation
  bool isSteadyAV = true;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteadyAV,
                 order, hasSpaceTimeDiffusionTwoPhase, rhow, rhon, phi, krw, krn, muw, mun, K, pc, dictSource);

  // Initial solution
  ArrayQ qInit;
  AVVariable<PressureNonWet_SaturationWet,Real> qdata({pwInit, SwInit}, sensorInit);
  pde.setDOFFrom( qInit, qdata );

  typedef BCmitAVSensor2DParams<BCTypeFlux_mitStateSpaceTime, BCTwoPhase2DParams<BCTypeTimeIC>> BCParamsTimeIC;
  typedef BCmitAVSensor2DParams<BCTypeFlux_mitStateSpaceTime, BCTwoPhase2DParams<BCTypeNoFluxSpaceTime>> BCParamsNoFluxST;

  PyDict dictBCStateInit;
  dictBCStateInit[TwoPhaseVariableTypeParams::params.StateVector.Variables] =
                  TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  dictBCStateInit[PressureNonWet_SaturationWet_Params::params.pn] = pwInit;
  dictBCStateInit[PressureNonWet_SaturationWet_Params::params.Sw] = SwInit;

  PyDict dictBCInit;
  dictBCInit[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  dictBCInit[BCParamsTimeIC::params.StateVector] = dictBCStateInit;
  dictBCInit[BCParamsTimeIC::params.Cdiff] = diffusionConstantAV;

  PyDict dictBCFinal;
  dictBCFinal[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict dictBCNoFlux;
  dictBCNoFlux[BCParams::params.BC.BCType] = BCParams::params.BC.NoFluxSpaceTime;
  dictBCNoFlux[BCParamsNoFluxST::params.Cdiff] = diffusionConstantAV;

  PyDict dictBCList;
  dictBCList["dictBCInit"]   = dictBCInit;
  dictBCList["dictBCFinal"]  = dictBCFinal;
  dictBCList["dictBCNoFlux"] = dictBCNoFlux;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["dictBCInit"] = {iTmin};
  BCBoundaryGroups["dictBCFinal"] = {iTmax};
  BCBoundaryGroups["dictBCNoFlux"] = {iXmin,iXmax,iYmin,iYmax};

  // check the BC dictionary
  BCParams::checkInputs(dictBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(dictBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 8;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-11, 1e-11};

  // output functional: sum over production wells
  //printf( "%s: before call to output functional\n", __func__ );  fflush(stdout);
  bool computeMassFlow = false;
  std::vector <SourceOutflowClass> sourceOutputList;
  for (int n = 0; n < 4; n++)
  {
    sourceOutputList.emplace_back(dictSourceProduction[n], rhow, rhon, krw, krn, muw, mun, K, pc, computeMassFlow);
  }
  int phase = 1;
  NDOutputClass fcnOutput(sourceOutputList, phase);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
  //printf( "%s: after call to output functional\n", __func__ );  fflush(stdout);

  int quadOrder = 2*(order + 2);

  // print case summary
  if (world.rank() == 0)
  {
    std::cout << "SPE10 case parameters" << std::endl;
    std::cout << "---------------" << std::endl;
    std::cout << "p-order: " << order << std::endl;
    std::cout << "DOF target: " << targetCost << std::endl;
    std::cout << "Processors: " << world.size() << std::endl;
    std::cout << "pc_max: " << pc_max << std::endl;
    std::cout << "nWellParam: " << nWellParam << std::endl;
    std::cout << "R_bore: " << R_bore << std::endl;
    std::cout << "R_well: " << R_well << std::endl;
    std::cout << "pnIn: " << pnIn << std::endl;
    std::cout << "pnOut: " << pnOut << std::endl;
    std::cout << "hasSpaceTimeDiffusionTwoPhase: " << hasSpaceTimeDiffusionTwoPhase << std::endl;
    std::cout << "hasSpaceTimeDiffusionAV: " << hasSpaceTimeDiffusionAV << std::endl;
    std::cout << "diffusionConstantAV: " << diffusionConstantAV << std::endl;
    std::cout << "Sk_min, Sk_max: " << Sk_min << ", " << Sk_max << std::endl;
    std::cout << "isSteadyAV: " << isSteadyAV << std::endl;
    std::cout << "viscousEta: " << viscousEtaParameter << std::endl;
    std::cout << "Residual tol: " << tol[0] << ", " << tol[1] << std::endl;
    std::cout << "Quadrature order: " << quadOrder << std::endl;
    std::cout << "---------------" << std::endl << std::endl;
  }

  // Nonlinear solver dicts - one for each p-order
  std::vector<PyDict> dictSolverContinuation(order+1);
  std::vector<PyDict> dictAdjLinearSolver(order+1);
  std::vector<Real> resGrowthFactor = {1.0, 1.0, 1.0};

  for (int porder = order_min; porder <= order; porder++)
  {
    PyDict dictNonlinearSolver, dictNewtonSolver, dictLineUpdate;

#if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
    PyDict dictPreconditioner;
    PyDict dictPreconditionerILU;
    PyDict dictPETSc;

    dictPreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
    dictPreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    dictPreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;
    dictPreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
    dictPreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (porder+1)*(porder+2)*(porder+3)/6; //elemDOF for p=order

    dictPreconditioner[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    dictPreconditioner[SLA::PreconditionerASMParam::params.SubPreconditioner] = dictPreconditionerILU;

    dictPETSc[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
    dictPETSc[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
    dictPETSc[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
    dictPETSc[SLA::PETScSolverParam::params.MaxIterations] = 1500;
    dictPETSc[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
    dictPETSc[SLA::PETScSolverParam::params.Preconditioner] = dictPreconditioner;
    dictPETSc[SLA::PETScSolverParam::params.Verbose] = true;
    dictPETSc[SLA::PETScSolverParam::params.computeSingularValues] = false;
    dictPETSc[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
    dictPETSc[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

    dictNewtonSolver[NewtonSolverParam::params.LinearSolver] = dictPETSc;

    // change parameters for adjoint solve
    PyDict dictPreconditionerILU_adjoint = dictPreconditionerILU;
    PyDict dictPreconditioner_adjoint = dictPreconditioner;
    PyDict dictPETSc_adjoint = dictPETSc;
    dictPreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (porder+2)*(porder+3)*(porder+4)/6; //elemDOF for p=order+1
    dictPreconditioner_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = dictPreconditionerILU_adjoint;
    dictPETSc_adjoint[SLA::PETScSolverParam::params.Preconditioner] = dictPreconditioner_adjoint;
    dictPETSc_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
    dictPETSc_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
    dictAdjLinearSolver[porder][SLA::LinearSolverParam::params.LinearSolver] = dictPETSc_adjoint;

    if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
  #else
    PyDict dictUMFPACK;
    dictUMFPACK[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    dictNewtonSolver[NewtonSolverParam::params.LinearSolver] = dictUMFPACK;
    dictAdjLinearSolver[porder][SLA::LinearSolverParam::params.LinearSolver] = dictUMFPACK;
    if (world.rank() == 0) std::cout << "Linear solver: UMFPACK" << std::endl;
  #endif

    dictLineUpdate[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
    dictLineUpdate[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
    dictLineUpdate[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = resGrowthFactor[porder];
    dictLineUpdate[HalvingSearchLineUpdateParam::params.verbose] = true;
    dictLineUpdate[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

    dictNewtonSolver[NewtonSolverParam::params.LineUpdate] = dictLineUpdate;
    dictNewtonSolver[NewtonSolverParam::params.MinIterations] = 0;
    dictNewtonSolver[NewtonSolverParam::params.MaxIterations] = 100;
    dictNewtonSolver[NewtonSolverParam::params.Verbose] = true;
    dictNewtonSolver[NewtonSolverParam::params.Timing] = false;

  #if 1
    dictNonlinearSolver[NonLinearSolverParam::params.NonLinearSolver] = dictNewtonSolver;
  #else
    //PTC
    dictNewtonSolver[NewtonSolverParam::params.MaxIterations] = 1;

    dictNonlinearSolver[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
    dictNonlinearSolver[PseudoTimeParam::params.NonLinearSolver] = dictNewtonSolver;
    dictNonlinearSolver[PseudoTimeParam::params.invCFL]     = 1e-4;
    dictNonlinearSolver[PseudoTimeParam::params.invCFL_max] = 1000;
    dictNonlinearSolver[PseudoTimeParam::params.invCFL_min] = 0;
    dictNonlinearSolver[PseudoTimeParam::params.Verbose] = true;

    if (world.rank() == 0)
    {
      std::cout << "PTC parameters:" << std::endl;
      std::cout << "---------------" << std::endl;
      std::cout << "invCFL init: " << dictNonlinearSolver.get(PseudoTimeParam::params.invCFL) << std::endl;
      std::cout << "CFLDecreaseFactor: " << dictNonlinearSolver.get(PseudoTimeParam::params.CFLDecreaseFactor) << std::endl;
      std::cout << "CFLIncreaseFactor: " << dictNonlinearSolver.get(PseudoTimeParam::params.CFLIncreaseFactor) << std::endl;
      std::cout << "CFLPartialStepDecreaseFactor: " << dictNonlinearSolver.get(PseudoTimeParam::params.CFLPartialStepDecreaseFactor) << std::endl;
      std::cout << "invCFL min: " << dictNonlinearSolver.get(PseudoTimeParam::params.invCFL_min) << std::endl;
      std::cout << "invCFL max: " << dictNonlinearSolver.get(PseudoTimeParam::params.invCFL_max) << std::endl;
      std::cout << "---------------" << std::endl << std::endl;
    }
  #endif

    dictSolverContinuation[porder][SolverContinuationParams<TemporalSpaceTime>::params.Continuation] = dictNonlinearSolver;

    // Check inputs
    SolverContinuationParams<TemporalSpaceTime>::checkInputs(dictSolverContinuation[porder]);

    //Print solver summary
    if (world.rank() == 0)
    {
      std::cout << "Solver parameters - order P" << porder  << std::endl;
      std::cout << "---------------" << std::endl;
      std::cout << "maxResGrowthFactor: " << dictLineUpdate.get(HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor) << std::endl;
  #if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
      std::cout << "ILU fill level: " << dictPreconditionerILU.get(SLA::PreconditionerILUParam::params.FillLevel) << std::endl;
      std::cout << "ILU ordering: " << dictPreconditionerILU.get(SLA::PreconditionerILUParam::params.Ordering) << std::endl;
      std::cout << "MDF block size: " << dictPreconditionerILU.get(SLA::PreconditionerILUParam::params.MDFOuterBlockSize) << std::endl;
      std::cout << "GMRES restart: " << dictPETSc.get(SLA::PETScSolverParam::params.GMRES_Restart) << std::endl;
      std::cout << "PETSc RelativeTolerance: " << dictPETSc.get(SLA::PETScSolverParam::params.RelativeTolerance) << std::endl;
      std::cout << "PETSc MaxIterations: " << dictPETSc.get(SLA::PETScSolverParam::params.MaxIterations) << std::endl;
  #endif
      std::cout << "---------------" << std::endl << std::endl;
    }
  }

  //--------ADAPTATION LOOP--------

  fstream fadapthist;
  if (world.rank() == 0)
  {
    std::string adapthist_filename = filename_base + "test.adapthist";
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict dictMOESS;
  dictMOESS[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  dictMOESS[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  dictMOESS[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;
  dictMOESS[MOESSParams::params.UniformRefinement] = false;

  PyDict dictAdapt;
  dictAdapt[MeshAdapterParams<PhysDim, TopoDim>::params.TargetCost] = targetCost;
  dictAdapt[MeshAdapterParams<PhysDim, TopoDim>::params.Algorithm] = dictMOESS;
  dictAdapt[MeshAdapterParams<PhysDim, TopoDim>::params.Mesher] = dictMesher;
  dictAdapt[MeshAdapterParams<PhysDim, TopoDim>::params.FilenameBase] = filename_base;
  dictAdapt[MeshAdapterParams<PhysDim, TopoDim>::params.dumpStepMatrix] = false;

  MeshAdapterParams<PhysDim, TopoDim>::checkInputs(dictAdapt);

  MeshAdapter<PhysDim, TopoDim> mesh_adapter(dictAdapt, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Compute generalized log H-tensor field
  std::shared_ptr<GenHFieldType> pHfld = std::make_shared<GenHFieldType>(*pxfld);

  //Solution data
  typedef SolutionData_DGBR2<PhysDim, TopoDim, NDPDEClass, ParamBuilderType> SolutionClass;
  BasisFunctionCategory basis_category = BasisFunctionCategory_Legendre;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>((*pHfld, *pxfld), pde, order, order+1,
                                               basis_category, basis_category,
                                               active_boundaries, disc);

  pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

  //Set initial solution
//  pGlobalSol->setSolution(solnInit, cellGroups);
  pGlobalSol->setSolution(qInit);

//  ProjectGlobalField(qfld_tmp, pGlobalSol->primal.qfld);

//  std::fstream fqfld("tmp/qfld_initial.txt", std::fstream::in);
//  for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
//    for (int j = 0; j < ArrayQ::M; j++)
//      fqfld >> pGlobalSol->primal.qfld.DOF(i)[j];
//  fqfld.close();

  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld,
                 qfld_init_filename, {"pn", "Sw", "nu"});
//  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, {"pn", "Sw", "nu"} );

  for (int iter = 0; iter < maxIter+1; iter++)
  {
    if (world.rank() == 0)
      std::cout << "\n" << std::string(25,'-') + "Adaptation Iteration " << iter << std::string(25,'-') << "\n\n";

    //Create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;

    for (int porder = order_min; porder <= order; porder++ )
    {
      if (world.rank() == 0)
        std::cout << std::string(5,'-') + "P-sequencing order " << porder << std::string(5,'-') << "\n\n";

      std::shared_ptr<SolutionClass> pGlobalSol_porder;
      pGlobalSol_porder = std::make_shared<SolutionClass>((*pHfld, *pxfld), pde, porder, porder+1,
                                                          basis_category, basis_category,
                                                          active_boundaries, disc);

      pGlobalSol_porder->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

      //Perform L2 projection from last solution
      pGlobalSol_porder->setSolution(*pGlobalSol);

      //Make current solution the projected solution in new p-order
      pGlobalSol = pGlobalSol_porder;

      quadOrder = 2*(porder + 2);

      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          dictBCList, BCBoundaryGroups,
                                                          dictSolverContinuation[porder], dictAdjLinearSolver[porder],
                                                          outputIntegrand);
      pInterface->setBaseFilePath(filename_base);

      pInterface->solveGlobalPrimalProblem();
      std::string qfld_filename = filename_base + "qfld_P" + std::to_string(porder) + "_a" + std::to_string(iter) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename, {"pn", "Sw", "nu"} );
  //    std::string sol_filename = filename_base + "sol_P" + std::to_string(porder) + "_a" + std::to_string(iter) + ".plt";
  //    output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld, sol_filename, {"pn", "Sw", "nu"});

      std::string liftedfld_filename = filename_base + "liftedfld_P" + std::to_string(porder) + "_a" + std::to_string(iter) + ".plt";
      output_Tecplot( *(pGlobalSol->pliftedQuantityfld), liftedfld_filename, {"s"} );

//      std::fstream fqfld("tmp/qfld_a" + std::to_string(iter) + ".txt", std::fstream::out);
//      fqfld << std::scientific << std::setprecision(15);
//      for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
//        for (int j = 0; j < ArrayQ::M; j++)
//          fqfld << pGlobalSol->primal.qfld.DOF(i)[j] << std::endl;
//      fqfld.close();

      if (world.rank() == 0)
        std::cout << "Output: " << std::scientific << std::setprecision(12) << pInterface->getOutput() << std::endl << std::endl;
    }

    pInterface->solveGlobalAdjointProblem();
    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename, {"psiw", "psin", "psinu"} );

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    timer adapt_clock;
    std::shared_ptr<XField<PhysDim, TopoDim>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    if (world.rank() == 0)
      std::cout<<"Mesh adapt time: " << std::fixed << std::setprecision(3) << adapt_clock.elapsed() << "s" << std::endl << std::endl;

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Compute generalized log H-tensor field on new mesh
    pHfld = std::make_shared<GenHFieldType>(*pxfldNew);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>((*pHfld,*pxfldNew), pde, order, order+1,
                                                    basis_category, basis_category,
                                                    active_boundaries, disc);

    pGlobalSolNew->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

    //Perform L2 projection from solution on previous mesh
//    pGlobalSolNew->setSolution(qInit);
    pGlobalSolNew->setSolution(*pGlobalSol);

#if 0
    //Hack: Initial solution projection for pressure
    const int nDOF_per_elem = (order + 1)*(order + 2)*(order + 3)/6;
    for (int i = 0; i < pGlobalSolNew->primal.qfld.nElem(); i++)
      for (int j = 0; j < nDOF_per_elem; j++)
      {
        if (j == 0)
          pGlobalSolNew->primal.qfld.DOF(nDOF_per_elem*i + j)[0] = qInit[0];
        else
          pGlobalSolNew->primal.qfld.DOF(nDOF_per_elem*i + j)[0] = 0.0;
      }
#endif

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;

    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, {"pn", "Sw", "nu"} );

    if (world.rank() == 0)
      std::cout<<"Time elapsed: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl << std::endl;
  }

  if (world.rank() == 0)
    fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
