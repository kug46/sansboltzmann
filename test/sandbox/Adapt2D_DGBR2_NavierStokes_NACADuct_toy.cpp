// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_NavierStokes_Joukowski_btest
// Testing of the MOESS framework on a 2D Navier-Stokes problem


#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/PDENavierStokes_Brenner2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/SolutionFunction_Euler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"
#include "Meshing/EPIC/XField_PX.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_NavierStokes_NACADuct_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_NavierStokes_NACADuct_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;

//  #define BRENNER
  #ifndef BRENNER
    typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  #else
    typedef PDENavierStokes_Brenner2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  #endif

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

#ifndef BRENNER
  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;
#else
  typedef BCNavierStokesBrenner2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;
#endif

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_Entropy<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-10, 1e-10};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.5;
  const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real pRef = 1.0;                            // pressure
  const Real tRef = pRef/(rhoRef*R);

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = cRef*Mach;                      // velocity scale

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

//  const Real Href = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);

  // stagnation temperature, pressure
  const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  const Real TtRef = tRef*Gamma;
  const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

#ifndef BRENNER
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer );
#else
  Real rhodiff = 4./3.;
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, rhodiff );
#endif

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;


  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;


#if 0
  typedef SolutionFunction_Euler2D_Wake<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass;

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Wake;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.Pt] = PtRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.Tt] = TtRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = pRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.wakedepth] = 0.3;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_mitState;
  BCIn[BCClass::ParamsType::params.Function] = Function;

#else
  typedef SolutionFunction_Euler2D_Wake<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass;

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Wake;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.Pt] = PtRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.Tt] = TtRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = pRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.wakedepth] = 0.3;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
  BCIn[BCClass::ParamsType::params.Function] = Function;
#endif

#if 0
  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;
#else
  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
#endif

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {3,5};
  BCBoundaryGroups["BCNoSlip"] = {0,1};
  BCBoundaryGroups["BCOut"] = {4};
  BCBoundaryGroups["BCIn"] = {2};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  // Drag output
  NDOutputClass outputFcn(pde, 1., 0.);
  OutputIntegrandClass outputIntegrand( outputFcn, {0,1} );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
//  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
//  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
//  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
#else
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
#endif


  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-6;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type] =
      SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor] = 1.5;
  NonlinearSolverDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.1;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  //--------ADAPTATION LOOP--------

  for (int order = 3; order <= 3; order++)
    for (int power = 2; power <= 4; power++ )
    {
      int maxIter = 25;

      int nk = pow(2,power);
      int targetCost = 1000*nk;

      // Grid
      std::string file_initial_mesh = "grids/shortduct2/shortductq3.gri";
      std::string file_initial_linear_mesh = "grids/shortduct2/shortductq1.gri";

//      std::string file_initial_mesh = "tmp/NACADUCT_Re10k/BN_P2_16k/mesh_a2_outQ3.grm";
//      std::string file_initial_linear_mesh = "tmp/NACADUCT_Re10k/BN_P2_16k/mesh_a2_outQ1.grm";

      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField_PX<PhysD2, TopoD2>(world, file_initial_mesh) );
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld_linear( new XField_PX<PhysD2, TopoD2>(world, file_initial_linear_mesh) );

      //IO
      std::string filename_base = "tmp/NACADUCT_Re1000_M0p5/NSBBN_P" + std::to_string(order) + "_" + std::to_string(nk) + "k/";

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
      MesherDict[EpicParams::params.CurvedQOrder] = 3; //Mesh order
      MesherDict[EpicParams::params.CSF] = "grids/shortduct2/naca0012_duct.csf"; //Geometry file
      MesherDict[EpicParams::params.GeometryList2D] = "duct0012_crv1_body duct0012_crv2_outer"; //Geometry list
//      MesherDict[EpicParams::params.minGeom] = 1e-6;
//      MesherDict[EpicParams::params.maxGeom] = 0.5;
//      MesherDict[EpicParams::params.nPointGeom] = -1;
      MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                   active_boundaries, disc);

      const int quadOrder = 3*order+2; //2*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      //Set initial solution
      pGlobalSol->setSolution(q0);

      std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      std::string qfld_filename = filename_base + "qfld_a0.plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_a0.plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


      for (int iter = 0; iter < maxIter+1; iter++)
      {
        std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        //Compute error estimates
        pInterface->computeErrorEstimates();

        //Perform local sampling and adapt mesh
        MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;
        ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew_linear = ptr_pair.first;
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew = ptr_pair.second;

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                        BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                        active_boundaries, disc);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);
//        pGlobalSolNew->setSolution(q0);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, LinearSolverDict,
                                                               outputIntegrand);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld_linear = pxfldNew_linear;
        pxfld = pxfldNew;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();

        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
      }

      fadapthist.close();
    }

}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
