// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_CG_AD_btest
// testing of 1-D CG with Advection-Diffusion

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_WeightedResidual_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
#define TAU = 10;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_1D_CG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorOrder_1D_CG_AD )
{
  //ADVECTION_DIFFUSION CASE
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ScalarFunction1D_BL SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef ScalarFunction1D_BLAdj AdjSolutionExact;
  typedef SolnNDConvertSpace<PhysD1, AdjSolutionExact> NDAdjSolutionExact;

//  typedef BCTypeFunction_mitState<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> AlgebraicEquationSet_GalerkinClass;
  typedef AlgebraicEquationSet_GalerkinClass::BCParams BCParams;

#if 1
  // PDE
  Real a = 1;
  AdvectiveFlux1D_Uniform adv( a );
  Real nu = 0.01;

  //ADVECTION DIFFUSION
  ViscousFlux1D_Uniform visc( nu );

  Real src = -2.1;
  Source1D_Uniform source(src);

  NDSolutionExact solnExact(a, nu, src);
  NDAdjSolutionExact adjSolnExact(a, nu, src);

//  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
//  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

//  NDPDEClass pde(adv, visc, source, forcingptr );
  NDPDEClass pde(adv, visc, source);

  // BC

  // Create a BC dictionary
  PyDict BL;
  BL[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.BL;
  BL[SolutionExact::ParamsType::params.a] = a;
  BL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
//  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
//  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.Function] = BL;
//  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.SolutionBCType] = "Dirichlet";

  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
    BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function] = BL;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType] = "Dirichlet";
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict BCSoln2;
  BCSoln2[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln2[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function] = BL;
  BCSoln2[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType] = "Dirichlet";
  BCSoln2[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;
  PyBCList["TheBCName2"] = BCSoln2;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {1};
  BCBoundaryGroups["TheBCName2"] = {0};
#else

  // PDE
  Real a = 1;
  AdvectiveFlux1D_Uniform adv( a );
  Real nu = 0.01;
  ViscousFlux1D_Uniform visc(0.0);

  Source1D_None source;

  NDSolutionExact solnExact(a, nu);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde(adv, visc, source, forcingptr );

  // BC

  // Create a BC dictionary
  PyDict BL;
  BL[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.BL;
  BL[SolutionExact::ParamsType::params.a] = a;
  BL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = BL;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;
  PyBCList["BCNone"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0};
  BCBoundaryGroups["BCNone"] = {1};
#endif


  typedef AlgebraicEquationSet_GalerkinClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_GalerkinClass::SystemVector SystemVectorClass;


  // Primal Exact Soln
  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // Adjoint Exact Solution
  typedef OutputCell_SolutionErrorSquared<PDEClass, AdjSolutionExact> AdjErrorClass;
  typedef OutputNDConvertSpace<PhysD1, AdjErrorClass> NDAdjErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDAdjErrorClass> ErrorAdjIntegrandClass;

  NDAdjErrorClass fcnAdjError(adjSolnExact);
  ErrorAdjIntegrandClass errorAdjIntegrand(fcnAdjError, {0});

  StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, 2.0);

#define VOLUME
#ifndef VOLUME
  // Output Functional
  typedef BCAdvectionDiffusion<PhysD1,BCType> BCClass;
  typedef BCNDConvertSpace<PhysD1,BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> IntegrandBCClass;

  NDBCClass bc(pde, BCSoln);
  IntegrandBCClass fcnBC(pde, bc, {1});

  typedef OutputAdvectionDiffusion1D_WeightedResidual OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> IntegrandOutputClass;

  NDOutputClass outputFcn(1.);
  IntegrandOutputClass outputIntegrand( outputFcn, {1} );
#else
  typedef OutputCell_Solution<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> IntegrandOutputClass;

  PyDict dummy;
  NDOutputClass outputFcn(dummy);
  IntegrandOutputClass outputIntegrand(pde, outputFcn, {0}, stab);
#endif

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  string filename_base = "tmp/1DAD/CGNEW/CG";
  string filename2 = filename_base + "_Errors.txt";

  fstream foutsol( filename2, fstream::out );

  int ordermin = 1;
  int ordermax = 3;
  for (int order = ordermin; order <= ordermax; order++)
  {
    stab.setStabOrder(order);
    stab.setNitscheOrder(order);
    // loop over grid resolution: 2^power
    int powermin = 2;
    int powermax = 10;

    for (int power = powermin; power <= powermax; power++)
    {
      std::cout << "order: " << order << ", power:" << power << "\n";
      int ii = pow( 2, power )+1;

      // grid:

      XField1D xfld( ii );

      // solution: Hierarchical, C0
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Lagrange);
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qflda(xfld, order, BasisFunctionCategory_Lagrange);
      qfld = 0; qflda = 0;

      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 1
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Lagrange,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgflda( xfld, order, BasisFunctionCategory_Lagrange,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#elif 0
      Field2D_CG_BoundaryEdge_Independent<NDPDEClass> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                             BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
      Field2D_DG_BoundaryEdge<NDPDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                 BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#endif

      lgfld = 0; lgflda = 0;

      const int nDOFBC = lgfld.nDOF();

      QuadratureOrder quadratureOrder( xfld, 3*(order+1) );
      std::vector<Real> tol = {1e-12, 1e-12};

      AlgebraicEquationSet_GalerkinClass AlgEqSet(xfld, qfld, lgfld, pde, stab,
                                                  quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );

      // residual vectors

      SystemVectorClass q(AlgEqSet.vectorStateSize());
      SystemVectorClass rsd(AlgEqSet.vectorEqSize());

      AlgEqSet.fillSystemVector(q);

      rsd = 0;

      AlgEqSet.residual(q, rsd);

#if 0
      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

      // solve

      SLA::UMFPACK<SystemMatrixClass> solver(AlgEqSet);

      SystemVectorClass dq(q.size());

      solver.solve(rsd, dq);
      // updated solution

      q -= dq;

      AlgEqSet.setSolutionField(q);

      // check that the residual is zero

      rsd = 0;

      AlgEqSet.residual(q, rsd);

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), tol[0] );

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdBCnrm), tol[1] );

      // L2 solution error

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorIntegrand, SquareError ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real primalErr = sqrt(SquareError);

      ArrayQ output = 0;
#ifndef VOLUME

      IntegrateBoundaryTraceGroups<TopoD1>::integrate(
          FunctionalBoundaryTrace_WeightedResidual_Galerkin( outputIntegrand, fcnBC, output ),
          xfld, qfld, quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size() );
#else
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( outputIntegrand, output ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#endif


#if 1

      // adjoint (right hand side)

      SystemVectorClass rhs(q.size());
      rhs = 0;

#ifndef VOLUME

      IntegrateBoundaryTraceGroups<TopoD1>::integrate(
          JacobianFunctionalBoundaryTrace_WeightedResidual_Galerkin<SurrealClass>(outputIntegrand, fcnBC, rhs(0)), xfld, qfld,
          quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size()  );
#else
      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell_Galerkin( outputIntegrand, rhs(0) ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#endif
      // solve
      Real outTrue =   3.4311914055472848381984761232566;

      Real outputErr = fabs(outTrue - output);

      SLA::UMFPACK<SystemMatrixClass> solverAdj(AlgEqSet, SLA::TransposeSolve);

      SystemVectorClass adj(q.size());

      solverAdj.solve(rhs, adj);

      // updated solution

      for (int k = 0; k < nDOFPDE; k++)
        qflda.DOF(k) = adj[0][k];

      for (int k = 0; k < nDOFBC; k++)
        lgflda.DOF(k) = adj[1][k];

      // L2 adjoint solution error

      Real AdjSquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorAdjIntegrand, AdjSquareError ),
          xfld, qflda, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real adjError = sqrt(AdjSquareError);
#endif

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      foutsol << order << " " << ii << " " << std::setprecision(15) << primalErr;
      foutsol << " " << adjError;
      foutsol << " " << outputErr  << "\n";

//      std::cout << std::setprecision(16) << output << "\n";
#endif


#if 1
      // Tecplot dump of solution
      string filenameQ = filename_base + "_P";
      filenameQ += stringify(order);
      filenameQ += "_";
      filenameQ += stringify(ii);
      filenameQ += ".plt";
//      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filenameQ );

#endif

#if 1
      // Tecplot dump of solution
      string filenameAdj = filename_base + "_P";
      filenameAdj += stringify(order);
      filenameAdj += "_";
      filenameAdj += stringify(ii);
      filenameAdj += "_adj.plt";
//      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qflda, filenameAdj );

#endif

#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
#endif

    }

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
