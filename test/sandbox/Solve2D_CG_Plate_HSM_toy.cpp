// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_CG_Plate_HSM_toy
// testing of 2-D CG HSM on a Plate

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "pde/HSM/PDEHSM2D.h"
#include "pde/HSM/BCHSM2D.h"
#include "pde/HSM/ComplianceMatrix.h"
#include "pde/HSM/SolutionFunctionHSM2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "pde/OutputCell_SolutionErrorSquared.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_HSM2D.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_Galerkin_HSM2D.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#include "Field/Tuple/FieldTuple.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_CG_Plate_HSM_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_CG_HSM2D_HangingPlate_ZeroPoisson )
{

  typedef PDEHSM2D<VarTypeLambda> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSM2D<VarTypeLambda>::VectorE<Real> VectorE;
  typedef PDEHSM2D<VarTypeLambda>::MatrixX<Real> MatrixX;
  typedef PDEHSM2D<VarTypeLambda>::ComplianceMatrix<Real> ComplianceMatrix;

  typedef SolnNDConvertSpace<PhysD2, SolutionFunctionHSM2D_HangingBarZeroPoisson> SolutionFunction;

  typedef OutputCell_SolutionErrorSquared<NDPDEClass, SolutionFunction> OutputSolutionErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD2, OutputSolutionErrorSquaredClass> NDOutputSolutionErrorSquaredClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputSolutionErrorSquaredClass> IntegrandSquareErrorClass;

  typedef MakeTuple<FieldTuple, Field<PhysD2, TopoD2, ComplianceMatrix>,
                                 Field<PhysD2, TopoD2, Real>,
                                 Field<PhysD2, TopoD2, VectorE>,
                                 Field<PhysD2, TopoD2, VectorX>,
                                 Field<PhysD2, TopoD2, VectorX>,
                                 XField<PhysD2, TopoD2> >::type FieldParamType;


  typedef BCHSM2DVector<VarTypeLambda> BCVector;
  typedef AlgebraicEquationSet_Galerkin< NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Sparse, FieldParamType > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  std::ofstream resultFile("tmp/L2_2D_CG_HSM.plt", std::ios::out);

  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"rx L2 error\"";
  resultFile << ", \"rx L2 error rate\"";
  resultFile << ", \"ry L2 error\"";
  resultFile << ", \"ry L2 error rate\"";
  resultFile << ", \"lam3 L2 error\"";
  resultFile << ", \"lam3 L2 error rate\"";
  resultFile << ", \"f11 L2 error\"";
  resultFile << ", \"f11 L2 error rate\"";
  resultFile << ", \"f22 L2 error\"";
  resultFile << ", \"f22 L2 error rate\"";
  resultFile << ", \"f12 L2 error\"";
  resultFile << ", \"f12 L2 error rate\"";
  resultFile << std::endl;

  // Rotation angle and matrix
  Real theta = PI / 8;
  MatrixX R = {{ cos(theta), sin(theta) },
               {-sin(theta), cos(theta) }};

  // PDE
  Real g = 10.0;
  VectorX gvec = { 0.0, g };
  VectorX gvecrot = R * gvec;

  NDPDEClass pde(gvecrot, PDEClass::HSM_ResidInterp_Separate);
  int nSol = NDPDEClass::N;

  Real xmax = 1.;
  Real ymax = 2.;

  // BC
  // Create a BC dictionary
  PyDict BCDisp;
  BCDisp[BCParams::params.BC.BCType] = BCParams::params.BC.Displacements;
  BCDisp[BCHSM2DParams<BCTypeDisplacements>::params.rshiftx] = 0;
  BCDisp[BCHSM2DParams<BCTypeDisplacements>::params.rshifty] = 0;

  PyDict BCForceZero;
  BCForceZero[BCParams::params.BC.BCType] = BCParams::params.BC.Forces;
  BCForceZero[BCHSM2DParams<BCTypeForces>::params.f1BC] = 0; // f1BC
  BCForceZero[BCHSM2DParams<BCTypeForces>::params.f2BC] = 0; // f2BC

  PyDict BCForce;
  BCForce[BCParams::params.BC.BCType] = BCParams::params.BC.Forces;
  BCForce[BCHSM2DParams<BCTypeForces>::params.f1BC] = 100000; //f1BC
  BCForce[BCHSM2DParams<BCTypeForces>::params.f2BC] = 0;      //f2BC

  // String arguments in PyBCList and BCBoundaryGroups must always match

  PyDict PyBCList;
  PyBCList["BCDisp"] = BCDisp;
  // PyBCList["BCForce"] = BCForce;
  PyBCList["BCForceZero"] = BCForceZero;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  const int iBCbottom = 0;
  const int iBCright = 1;
  const int iBCtop = 2;
  const int iBCleft = 3;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDisp"] = {iBCbottom};
  // BCBoundaryGroups["BCForce"] = {iBCright};
  BCBoundaryGroups["BCForceZero"] = {iBCtop, iBCright, iBCleft};

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // Analytic solution
  Real mu = 10, E = 500000, nu = 0, t = 1./100.;
  SolutionFunction solnExact( mu, g, E, ymax, t, theta );
  IntegrandSquareErrorClass fcnErr( solnExact, {0} );

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[6][10];   // L2 error
  int indx = 0;


//  fstream fout( "tmp/cubicsource_results.txt", fstream::out );

  int ordermin = 1;
  int ordermax = 1;

  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
    int powermax = 1;

    for (int power = powermin; power <= powermax; power++)
    {

      jj = pow( 2, power );
      ii = 2;

      // grid: 2D Box mesh
      //XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, xmax, 0, ymax ); // This can do P2 elements
      XField2D_Box_Quad_X1 xfld( ii, jj, 0, xmax, 0, ymax ); // This can only do P1 elements

      // Extensional compliance matrix CG field
      Field_CG_Cell<PhysD2, TopoD2, ComplianceMatrix> Ainvfld(xfld, order, BasisFunctionCategory_Hierarchical);
      Ainvfld = calcComplianceMatrix( E, nu, t );

      // Lumped shell mass CG field
      Field_CG_Cell<PhysD2, TopoD2, Real> mufld(xfld, order, BasisFunctionCategory_Hierarchical);
      mufld = mu;

      // Following force CG field
      Field_CG_Cell<PhysD2, TopoD2, VectorE> qefld(xfld, order, BasisFunctionCategory_Hierarchical);
      qefld = 0;

      // Global forcing CG field
      Field_CG_Cell<PhysD2, TopoD2, VectorX> qxfld(xfld, order, BasisFunctionCategory_Hierarchical);
      qxfld = 0;

      // Accelerations CG field
      Field_CG_Cell<PhysD2, TopoD2, VectorX> afld(xfld, order, BasisFunctionCategory_Hierarchical);
      afld = 0;

#if 0 // If using P > 1

      // P1 solution variable CG field
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> varfldP1(xfld, 1, BasisFunctionCategory_Hierarchical);

      // Loop over nodes
      BOOST_REQUIRE_EQUAL(xfld.nDOF(), varfldP1.nDOF());
      for (int i = 0; i < xfld.nDOF(); i++)
      {
        // Rotate grid
        VectorX rotatedVector = R * xfld.DOF(i);
        xfld.DOF(i) = rotatedVector;

        // Set state vector
        PositionLambdaForces2D varData;
        varData.rx_    = xfld.DOF(i)[0];
        varData.ry_    = xfld.DOF(i)[1];
        varData.lam3_  = 0;
        varData.f11_   = 0;
        varData.f22_   = 0;
        varData.f12_   = 0;

        //afld.DOF(i) = accleration(xfld.DOF(i));

        varfldP1.DOF(i) = pde.setDOFFrom( varData );
      }

      // P solution variable CG field
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> varfld(xfld, order, BasisFunctionCategory_Hierarchical);
      varfldP1.projectTo(varfld);

#else // P = 1

      // Solution variable CG field
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> varfld(xfld, 1, BasisFunctionCategory_Hierarchical);

      // Loop over nodes
      BOOST_REQUIRE_EQUAL(xfld.nDOF(), varfld.nDOF());
      for (int i = 0; i < xfld.nDOF(); i++)
      {
        // Rotate grid
        VectorX rotatedVector = R * xfld.DOF(i);
        xfld.DOF(i) = rotatedVector;

        // Set state vector
        PositionLambdaForces2D varData;
        varData.rx    = xfld.DOF(i)[0];
        varData.ry    = xfld.DOF(i)[1];
        varData.lam3  = 0;
        varData.f11   = 0;
        varData.f22   = 0;
        varData.f12   = 0;

        //afld.DOF(i) = accleration(xfld.DOF(i));

        varfld.DOF(i) = pde.setDOFFrom( varData );
      }

#endif


      const int nDOFPDE = varfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      lgfld = 0;

      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = { 1e-11, 1e-11 };

      //////////////////////////
      //SYSTEM
      //////////////////////////

      FieldParamType fldParam = (Ainvfld, mufld, qefld, qxfld, afld, xfld);

      StabilizationNitsche stab(order);
      PrimalEquationSetClass PrimalEqSet(fldParam, varfld, lgfld, pde, stab,
          quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0;

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

#if 0
      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
      PrimalEqSet.jacobian(ini,nz);

      SystemMatrixClass jac(nz);
      PrimalEqSet.jacobian(ini,jac);

      WriteMatrixMarketFile(jac,"tmp/HSM2D_Plate.mtx");
#endif


      BOOST_CHECK(Solver.solve(ini,sln).converged);
      PrimalEqSet.setSolutionField(sln);

      // check that the residual is zero

      PrimalEqSet.residual(sln,rsd);

      Real rsdPDEnrm[6] = {0,0,0,0,0,0};
      for (int n = 0; n < nDOFPDE; n++)
        for (int j = 0; j < nSol; j++)
          rsdPDEnrm[j] += pow(rsd[0][n][j],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[0]), 1e-11 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[1]), 1e-11 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[2]), 1e-11 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[3]), 1e-11 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[4]), 1e-11 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[5]), 1e-11 );

      // Monitor Entropy Error
      ArrayQ SquareError = 0.0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_Galerkin( fcnErr, SquareError ),
          xfld, varfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      // normVec[0][indx] = sqrt( SquareError[0] + SquareError[1] ); // Distance error
      // normVec[1][indx] = sqrt( SquareError[2] ); // Angle error
      // normVec[2][indx] = sqrt( SquareError[3] + SquareError[4] + SquareError[5] ); // Stress error

      normVec[0][indx] = sqrt( SquareError[0] ); // rx error
      normVec[1][indx] = sqrt( SquareError[1] ); // ry error
      normVec[2][indx] = sqrt( SquareError[2] ); // lam3 error
      normVec[3][indx] = sqrt( SquareError[3] ); // f11 error
      normVec[4][indx] = sqrt( SquareError[4] ); // f22 error
      normVec[5][indx] = sqrt( SquareError[5] ); // f12 error
      indx++;

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error =";
      for (int i = 0; i < 6; i++)
      {
        cout << " " << normVec[i][indx-1];
        if (indx > 1)
            cout << "  (ratio = " << normVec[i][indx-1]/normVec[i][indx-2] << ")";
      }
      cout << endl;
#endif

#if 1
      // Tecplot dump grid
      string filename = "tmp/slnCG_HSM2D_Plate_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      output_Tecplot( varfld, filename );
#endif

    }

    // Tecplot output
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      for (int i = 0; i < 6; i++)
      {
        resultFile << ", " << normVec[i][n];
        Real slope = 0;
        if (n > 0)
          slope = (log(normVec[i][n])  - log(normVec[i][n-1])) /(log(hVec[n]) - log(hVec[n-1]));

        resultFile << ", " << slope;
      }
      resultFile << endl;
    }

  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
