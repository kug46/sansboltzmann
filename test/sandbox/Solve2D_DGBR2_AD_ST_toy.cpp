// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_DGBR2_AD_ST_toy
// Testing of the solve framework on a space-time Advection Diffusion problem

#include <boost/test/unit_test.hpp>
#include <boost/filesystem/path.hpp>            // to make filesystems
#include <boost/filesystem/operations.hpp>      // to make filesystems
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_Burgers_AdvectionDiffusion_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_Burgers_AdvectionDiffusion_Triangle )
{
  typedef AdvectiveFlux1D_Uniform AdvectionModel;
  typedef ViscousFlux1D_Uniform DiffusionModel;
  typedef Source1D_UniformGrad SourceModel;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectionModel,
                                DiffusionModel,
                                SourceModel > PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_sansLG BCType;

  typedef ScalarFunction1D_Tanh SolutionExact;
  // typedef ScalarFunction1D_Gaussian SolutionExact;
  typedef SolnNDConvertSpaceTime<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCAdvectionDiffusion1DVector<AdvectionModel, DiffusionModel>::type BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  int ii = 2;
  int jj = ii;
  Real xmin = -0.5, xmax = 1, Tstart = 0, Tend = 1;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, jj, xmin, xmax, Tstart, Tend, false );

  int order = 1;

  int iB = XField2D_Box_Triangle_X1::iBottom;
  int iR = XField2D_Box_Triangle_X1::iRight;
  int iT = XField2D_Box_Triangle_X1::iTop;
  int iL = XField2D_Box_Triangle_X1::iLeft;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create Advection Diffusion PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  Real a = 0.5, nu = 0.0;

  AdvectionModel advection(a);
  DiffusionModel diffusion(nu);
  SourceModel source(0.0,0.0);

  NDPDEClass pde( advection, diffusion, source );

  // // Gaussian parameters
  // Real A = 1, mu = 0, sigma = 0.01;
  // NDSolutionExact solnExact( A, mu, sigma );

  // Tanh parameters
  Real nuTanh =  0.001, A = 2.0, B = 1.0;
  NDSolutionExact solnExact( nuTanh, A, B );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////

  // Create a BC dictionary

  // Initial Condition
  PyDict InitialCondition;

  // // Gaussian
  // InitialCondition[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
  //     BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Gaussian;
  // InitialCondition[SolutionExact::ParamsType::params.A] = A;
  // InitialCondition[SolutionExact::ParamsType::params.mu] = mu;
  // InitialCondition[SolutionExact::ParamsType::params.sigma] = sigma;

  // Tanh
  InitialCondition[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Tanh;
  InitialCondition[SolutionExact::ParamsType::params.A] = A;
  InitialCondition[SolutionExact::ParamsType::params.B] = B;
  InitialCondition[SolutionExact::ParamsType::params.nu] = nuTanh;

  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCInflow[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = InitialCondition;
  BCInflow[BCAdvectionDiffusionParams<PhysD1,BCType>::params.A] = 1.0;
  BCInflow[BCAdvectionDiffusionParams<PhysD1,BCType>::params.B] = 0.0;

  // Outflow
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

#if 1 // Right Running Wave
    Real tDum = 0.0;
    Real stateL = solnExact(xmin,tDum);

    PyDict BCLeft;
    BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
    BCLeft[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1;
    BCLeft[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 0;
    BCLeft[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = stateL;

    PyDict BCRight = BCNone;
#else // Left Running Wave
    Real tDum = 0.0;
    Real stateR = solnExact(xmax,tDum);

    PyDict BCLeft = BCNone;

    PyDict BCRight;
    BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
    BCRight[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1;
    BCRight[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 0;
    BCRight[BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = stateR;
#endif

  PyDict PyBCList;
  PyBCList["Right"]   = BCRight;
  PyBCList["Left"]    = BCLeft;
  PyBCList["Outflow"] = BCNone;
  PyBCList["Inflow"]  = BCInflow;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["Right"]   = {iR};  // Right Boundary
  BCBoundaryGroups["Left"]    = {iL};  // Left boundary
  BCBoundaryGroups["Outflow"] = {iT};  // Top boundary
  BCBoundaryGroups["Inflow"]  = {iB};  // Bottom boundary

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create discretization parameters
  ////////////////////////////////////////////////////////////////////////////////////////
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-12, 1e-12};

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create output functional to adapt with
  ////////////////////////////////////////////////////////////////////////////////////////
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  ////////////////////////////////////////////////////////////////////////////////////////
  // Setup Nonlinear Solver (maybe unneeded for linear problems)
  ////////////////////////////////////////////////////////////////////////////////////////

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);

//  //--------ADAPTATION LOOP--------
//
//  int maxIter = 20;
//  Real targetCost = 500;

  std::string filename_base = "tmp/AD_ST/";

  // if (world.rank() == 0)
  // {
  boost::filesystem::create_directories(filename_base);
  // }

//  std::string adapthist_filename = filename_base + "test.adapthist";
//  fstream fadapthist( adapthist_filename, fstream::out );
//  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

//  PyDict MOESSDict;
//  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
//  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
//
//  PyDict MesherDict;
//  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
//
//  PyDict AdaptDict;
//  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
//  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
//  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
//  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
//
//  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);
//
//  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  const int quadOrder = -1; //2*(order + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrand);

  //Set initial solution
  pGlobalSol->setSolution(solnExact, cellGroups);
//  pGlobalSol->setSolution(0.0);

  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

  pInterface->solveGlobalPrimalProblem();
//  pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename = filename_base + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

//  std::string adjfld_filename = filename_base + "adjfld_a0.plt";
//  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


//  for (int iter = 0; iter < maxIter+1; iter++)
//  {
//    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;
//
//    //Compute error estimates
//    pInterface->computeErrorEstimates();
//
//    //Perform local sampling and adapt mesh
//    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
//    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);
//
//    interiorTraceGroups.clear();
//    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
//      interiorTraceGroups.push_back(i);
//
//    std::shared_ptr<SolutionClass> pGlobalSolNew;
//    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
//                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
//                                                    active_boundaries, disc);
//
//    //Perform L2 projection from solution on previous mesh
//    pGlobalSolNew->setSolution(*pGlobalSol);
//
//    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
//    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
//                                                           cellGroups, interiorTraceGroups,
//                                                           PyBCList, BCBoundaryGroups,
//                                                           SolverContinuationDict, LinearSolverDict,
//                                                           outputIntegrand);
//
//    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
//    pxfld = pxfldNew;
//    pGlobalSol = pGlobalSolNew;
//    pInterface = pInterfaceNew;
//
//    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
//    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );
//
//    pInterface->solveGlobalPrimalProblem();
//    pInterface->solveGlobalAdjointProblem();
//
//    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
//    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
//
//    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
//    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
//  }
//
//  fadapthist.close();

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
