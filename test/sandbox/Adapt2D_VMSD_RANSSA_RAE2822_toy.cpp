// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_NavierStokes_FlatPlate_btest
// Testing of the MOESS framework on a 2D Navier-Stokes problem

#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include <iostream>
#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"


#include "../../src/Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "../../src/Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"
#include "../../src/Discretization/VMSD/SolutionData_VMSD.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_OutputWeightRsd_VMSD.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"
#include "Adaptation/MOESS/MOESSParams.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_RANSSA_RAE2822_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_RANSSA_RAE2822 )
{
  typedef QTypePrimitiveRhoPressure QType;
  // typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, VMSD> OutputIntegrandClass;
#endif

  typedef ParamType_Distance ParamBuilderType;

  typedef SolutionData_VMSD<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  enum ResidualNormType ResNormType= ResidualNorm_L2;
  std::vector<Real> tol= {1.0e-9, 1.0e-9, 1.0e-9};

  // gas model
  const Real gamma= 1.4;
  const Real R= 1.;
  const Real Cv= R/(gamma - 1.);
  const Real Cp= gamma*Cv;

  // reference state (freestream)
  const Real Mach= 0.5;
  const Real Reynolds= 1.e6;
  const Real Prandtl= 0.72;

  const Real lRef= 1.;                        // length scale
  const Real rhoRef= 1.;                      // density scale
  const Real tRef= 1.;                        // temperature scale
  const Real pRef= R*rhoRef*tRef;             // pressure scale
  const Real cRef= sqrt(gamma*R*tRef);        // speed of sound

  const Real qRef= cRef*Mach;
  const Real aoaRef= 0.;
  const Real uRef= qRef*cos(aoaRef);
  const Real vRef= qRef*sin(aoaRef);

  const Real muRef= rhoRef*qRef*lRef/Reynolds;

  const Real chiRef= 3.;
  const Real ntRef= chiRef*muRef/rhoRef;

  const Real nuRef= 1.;
  const Real ntr= ntRef/nuRef;

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

#ifndef BRENNER
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Entropy, eVanLeer, nuRef);
#else
  Real rhodiff= 4./3.;
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Entropy, eVanLeer, rhodiff);
#endif

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom(q0, SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntr}));

  // VMSD stabilization
  DiscretizationVMSD stab;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType]= BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables]= NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho]= rhoRef;
  StateVector[Conservative2DParams::params.rhou]= rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov]= rhoRef*vRef;
  const Real ERef= Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE]= rhoRef*ERef;
  StateVector["rhont"]= rhoRef*ntr;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType]= BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector]= StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic]= true;

  PyDict PyBCList;
  PyBCList["BCNoSlip"]= BCNoSlip;
  PyBCList["BCIn"]= BCIn;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // define the boundary groups for each boundary condition
  BCBoundaryGroups["BCNoSlip"]= {0, 1};
  BCBoundaryGroups["BCIn"]= {2, 3, 4, 5};

  // check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries= BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // entropy output
  NDOutputClass fcnOutput(pde, 1.0);
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, stab);
#else
  // drag output
  NDOutputClass outputFcn(pde, 1.0, 0.0);
  OutputIntegrandClass outputIntegrand(outputFcn, BCBoundaryGroups["BCNoSlip"]);
#endif

  // nonlinear solver dicts
  PyDict SolverContinuationDict;
  PyDict NonlinearSolverDict;
  PyDict NewtonSolverDict;
  PyDict LinearSolverDict;
  PyDict LineUpdateDict;

#if defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  //  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 1;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-20;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = true;
  //  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  //  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-20;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 10000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 500;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;
  //  PETScDictAdjoint[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-10;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 7;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

  // Easier PTC for subsequent solves
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
  = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;
  NonlinearSolverDict[PseudoTimeParam::params.CFLPartialStepDecreaseFactor] = 0.9;

  //  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
  //                      = SolverContinuationParams<TemporalMarch>::params.Continuation.None;
  //  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  //// ADAPTATION LOOP

  int maxIter= 20;

  // could loop here over orders
  // could loop here over number of DOFs to request

  int order= 3;

  stab.setNitscheOrder(order);

  Real targetCost= 10000.0;

  bool DGCONV= false;

  if (DGCONV)
  {
    Real nDOFperCell_DG= (order + 1)*(order + 2)/2;

    Real nDOFperCell_CG= nDOFperCell_DG;
    nDOFperCell_CG -= (3 - 1./2); // the node dofs are shared by 6
    nDOFperCell_CG -= (3 - 3./2)*std::max(0, (order - 1)); // if there are edge dofs they are shared by 2

    targetCost= targetCost*nDOFperCell_CG/nDOFperCell_DG;
  }

  // to make sure folders have a consistent number of zero digits
  const int string_pad= 5;
  std::string int_pad= std::string(string_pad - std::to_string((int) targetCost).length(), '0') + std::to_string((int) targetCost);

  std::string filename_base;
  filename_base= "tmp/RANS_RAE2822/VMSD_PAR/RAE2822_dofreq" + int_pad + "_p" + std::to_string(order) + "/";

  boost::filesystem::create_directories(filename_base);

  const int startIter= 0;

  std::string file_initial_mesh= "grids/rae2822_outQ3.grm";
  std::string file_initial_linear_mesh = "grids/rae2822_outQ1.grm";

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld(new XField_PX<PhysD2, TopoD2>(world, file_initial_mesh));
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld_linear(new XField_PX<PhysD2, TopoD2>(world, file_initial_linear_mesh));

  // printf("xfld linear order: %d\n\n", pxfld_linear->order()); return;

  std::string adapthist_filename= filename_base + "test.adapthist";

  fstream fadapthist;

  if (world.rank() == 0)
  {
    fadapthist.open(adapthist_filename, fstream::out);

    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel]= MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Optimizer_MaxEval]= 6000;
  MOESSDict[MOESSParams::params.Verbosity]= MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.LocalSolve]= MOESSParams::params.LocalSolve.Edge;
  MOESSDict[MOESSParams::params.UniformRefinement]= false;
  MOESSDict[MOESSParams::params.MetricOptimization]= MOESSParams::params.MetricOptimization.SANS;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name]= MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
  MesherDict[EpicParams::params.CurvedQOrder]= 3; // mesh order
  MesherDict[EpicParams::params.CSF]= "grids/rae2822.2019.csf"; // geometry file
  MesherDict[EpicParams::params.GeometryList2D]= "rae2822_mixL1"; // geometry list
  MesherDict[EpicParams::params.minGeom]= 1e-6;
  //      MesherDict[EpicParams::params.maxGeom] = 0.5;
  MesherDict[EpicParams::params.nPointGeom]= -1;
  MesherDict[EpicParams::params.ProjectionMethod]= EpicParams::params.ProjectionMethod.CavityRegrid;
  MesherDict[EpicParams::params.nThread]= 4;
  // MesherDict[EpicParams::params.Version] = "madcap_devel-10.2";

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost]= targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm]= MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher]= MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase]= filename_base;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix]= false;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix]= false;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups= {0};
  std::vector<int> interiorTraceGroups;
  for (int i= 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  // compute distance field
  PyDict paramDict;
  std::shared_ptr<Field_CG_Cell<PhysD2, TopoD2, Real>> pdistfld=
      std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfld, 3, BasisFunctionCategory_Lagrange);
  DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));

  // solution data
  std::shared_ptr<SolutionClass> pGlobalSol;

  const int porder= order;
  const int pporder= order + 1;

  // solution data
  pGlobalSol= std::make_shared<SolutionClass>((*pdistfld, *pxfld), pde, stab,
                                              order, porder, order + 1, pporder,
                                              BasisFunctionCategory_Lagrange,
                                              BasisFunctionCategory_Lagrange,
                                              BasisFunctionCategory_Lagrange,
                                              active_boundaries, paramDict);

  const int quadOrder= 2*(order + 1);

  // create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface= std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                     cellGroups, interiorTraceGroups,
                                                     PyBCList, BCBoundaryGroups,
                                                     SolverContinuationDict, LinearSolverDict,
                                                     outputIntegrand);
  // set initial solution
  pGlobalSol->setSolution(q0);

  std::string qfld_init_filename= filename_base + "qfld_init_a0_rank" + std::to_string(world.rank()) + ".plt";
  output_Tecplot(pGlobalSol->primal.qfld, qfld_init_filename);

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename= filename_base + "qfld_a0_rank" + std::to_string(world.rank()) + ".plt";
  output_Tecplot(pGlobalSol->primal.qfld, qfld_filename);

  std::string qpfld_filename= filename_base + "qpfld_a0_rank" + std::to_string(world.rank()) + ".plt";
  output_Tecplot(pGlobalSol->primal.qfld, qpfld_filename);

  std::string adjfld_filename= filename_base + "adjfld_a0_rank" + std::to_string(world.rank()) + ".plt";
  output_Tecplot(pInterface->getAdjField(), adjfld_filename);

  pInterface->computeErrorEstimates();

  std::string efld_filename= filename_base + "efld_a0_rank" + std::to_string(world.rank()) + ".plt";
  pInterface->output_EField(efld_filename);

  for (int iter= startIter; iter < maxIter + 1; iter++)
  {
    if (world.rank() == 0)
      std::cout << "----- Adaptation Iteration " << iter << " -----" << std::endl;

    MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;

    ptr_pair= mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew_linear= ptr_pair.first;
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew= ptr_pair.second;

    interiorTraceGroups.clear();
    for (int i= 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    // compute distance field (new one)
    pdistfld= std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfldNew, 3, BasisFunctionCategory_Lagrange);
    DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));

    // solution data
    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew= std::make_shared<SolutionClass>((*pdistfld, *pxfldNew), pde, stab,
                                                   order, porder, order + 1, pporder,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   active_boundaries, paramDict);

    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew= std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

    // update pointers to the newest problem (deleting previous mesh and solutions)
    pxfld= pxfldNew;
    pxfld_linear= pxfldNew_linear;
    pGlobalSol= pGlobalSolNew;
    pInterface= pInterfaceNew;

    std::string qfld_init_filename= filename_base + "qfld_init_a" + std::to_string(iter + 1) + "_rank" + std::to_string(world.rank()) + ".plt";
    output_Tecplot(pGlobalSol->primal.qfld, qfld_init_filename);

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    std::string qfld_filename= filename_base + "qfld_a" + std::to_string(iter + 1) + "_rank" + std::to_string(world.rank()) + ".plt";
    output_Tecplot(pGlobalSol->primal.qfld, qfld_filename);

    std::string qpfld_filename= filename_base + "qpfld_a" + std::to_string(iter + 1) + "_rank" + std::to_string(world.rank()) + ".plt";
    output_Tecplot(pGlobalSol->primal.qfld, qpfld_filename);

    std::string adjfld_filename= filename_base + "adjfld_a" + std::to_string(iter + 1) + "_rank" + std::to_string(world.rank()) + ".plt";
    output_Tecplot(pInterface->getAdjField(), adjfld_filename);

    pInterface->computeErrorEstimates();

    std::string efld_filename= filename_base + "efld_a" + std::to_string(iter + 1) + "_rank" + std::to_string(world.rank()) + ".plt";
    pInterface->output_EField(efld_filename);

  }


  if (world.rank() == 0)
    fadapthist.close();

}

BOOST_AUTO_TEST_SUITE_END() // Adapt2D_RANSSA_RAE2822_test_suite
