// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_HDG_Triangle_NavierStokes_FlatPlate_toy
// testing of 2-D HDG for NS for flat plate

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/HDG/FunctionalBoundaryTrace_Dispatch_HDG.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/ProjectSoln_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_OutputWeightRsd_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectSolnTrace_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_CG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_InteriorFieldTraceGroup.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

//#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_FlatPlate_X1.h"
#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_HDG_Triangle_NavierStokes_FlatPlate_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_HDG_Triangle_FlatPlate )
{

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  // Drag integrand
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, HDG> IntegrandOutputClass;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_HDG< NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  std::vector<Real> tol = {1e-11, 1e-11, 1e-11};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.3;
  const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real tRef = 1;          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // HDG discretization
#if 1
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient);
#else
  Real refl = 0.1;
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient, refl);
#endif

  // Drag integrand
  NDOutputClass outputFcn(pde, 1., 0.);
  IntegrandOutputClass fcnOut( outputFcn, {1} );

  //BCS

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aSpec = atan(vRef/uRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2,4};
  BCBoundaryGroups["BCNoSlip"] = {1};
  BCBoundaryGroups["BCOut"] = {3};
  BCBoundaryGroups["BCIn"] = {5};

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1.0e-48;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] =  true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);
//
  int powermin = 0;
  int powermax = 3;
//
  int ordermin = 1;
  int ordermax = 2;

  std::string filename_base = "tmp/NS_FP_Re1000/EDGstruc/";

//  fstream fout( "tmp/flatplate_hdgh.txt", fstream::out );
  for (int order = ordermin; order <= ordermax; order++)
  {

  // loop over grid resolution: 2^power
//    int power = 0;
    for (int power = powermin; power <= powermax; power++)
    {

#if 1
      XField2D_FlatPlate_X1 xfld(world, power);

#else

      // Read PX Output grid
      string filein = "grids/fp64k.grm";
//      filein += to_string(power);
//      filein += "_SANS.grm";

      XField_PX<PhysD2, TopoD2> xfld(filein);

//      std::vector<int> interiorTraceGroups = {0};
#endif


      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
//      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Hierarchical);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      lgfld(xfld, order-1, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

//      const int nDOFPDE = qfld.nDOF();
//      const int nDOFAUX = afld.nDOF();
//      const int nDOFINT = qIfld.nDOF();

      // Set the uniform initial condition
      ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

      qfld = q0;
      afld = 0.0;
      qIfld = q0;
      lgfld = 0;

      //////////////////////////
      //SOLVE HIGHER P SYSTEM
      //////////////////////////
      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      QuadratureOrder quadratureOrder( xfld, (3*order+1) );

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                         cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0;

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      Solver.solve(ini,sln);

      PrimalEqSet.setSolutionField(sln);

      // Monitor drag
      Real Drag = 0.0;

      PrimalEqSet.dispatchBC().dispatch_HDG(
        FunctionalBoundaryTrace_Dispatch_HDG(fcnOut, xfld, qfld, afld, qIfld,
                                             quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size(), Drag ) );

      // drag coefficient
//      Real Cd = Drag / (0.5*rhoRef*qRef*qRef*lRef);
      Real Cd = Drag;
      int nDOF = qfld.nDOF();

#define SANS_VERBOSE
#ifdef SANS_VERBOSE
      cout << "nDOF = " << nDOF <<
          ": Drag = " << std::setprecision(12) << Cd;
      cout << endl;
#endif


#if 1
      // Tecplot dump grid
      string filename = filename_base + "HDG_FlatPlate_P";
      filename += to_string(order);
      filename += "_X";
      filename += to_string(power);
      filename += ".plt";
      output_Tecplot( qfld, filename );
      std::cout << "Wrote Tecplot File\n";
#endif

    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
