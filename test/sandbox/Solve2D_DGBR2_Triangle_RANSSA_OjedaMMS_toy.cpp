// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
//  Solve2D_DGBR2_Triangle_NavierStokes_Poiseuille_btest
// testing of 2-D DGBR2 for NS on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>

#include "Field/ProjectSoln/InterpolateFunctionCell_Continuous.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/PDENavierStokes_Brenner2D.h"
#include "pde/NS/SolutionFunction_RANSSA2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/BCParameters.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Triangle_NavierStokes_OjedaMMS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_DGBR2_Triangle_OjedaMMS )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef SolutionFunction_RANSSA2D_OjedaMMS<TraitsSizeRANSSA, TraitsModelRANSSAClass>  SolnType;
  typedef SolnNDConvertSpace<PhysD2,SolnType> NDSolnType;

  typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef OutputEuler2D_TotalEnthalpy<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_Distance ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  mpi::communicator world;

  std::vector<Real> tol = {1e-10, 1e-10};

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;

  GasModel gas(gasModelDict);
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // INFLOW CONDITIONS TO MATCH OJEDA'S MMS

  const Real mu0 = 0.001; // FOR RE = 100

  Real rho0 = 1.0;
  Real u0 = 0.2;
  Real v0 = 0.1;
  Real p0 = 1.0;
//  Real t0 = p0/rho0/R;
  Real nu0 = mu0/rho0;
//  Real nu0 = 1;
  Real ntRef = nu0;
  Real nt0 = mu0/rho0/ntRef;

  const Real Prandtl = 0.72;

  ViscosityModelType visc(mu0); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  typedef SolnType::ParamsType SlnParams;
  PyDict slnArgs;
  slnArgs[SlnParams::params.gasModel] = gasModelDict;
  slnArgs[SlnParams::params.ntRef] = ntRef;
  std::shared_ptr<NDSolnType> solnptr( new NDSolnType(slnArgs) );

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, ntRef, solnptr );

  //  Velocity Magnitude Error
  NDOutputClass fcnOutput(pde);
  OutputIntegrandClass outputIntegrand( fcnOutput, {0} );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // BC
  slnArgs[BCEuler2D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Function.Name] =
      BCEuler2D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Function.OjedaMMS;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCEuler2D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Function] = slnArgs;
  BCSoln[BCEuler2D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Characteristic] = true;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  PyDict PyBCList;
  PyBCList["BCSoln"] = BCSoln;

  BCBoundaryGroups["BCSoln"] = {0,1,2,3};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  int ii = 4;
  int jj = 4;
  int order = 1;
#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    order = std::stoi(argv[1]);
  if (argc >= 3)
    ii = jj = std::stoi(argv[2]);

  if (world.rank() == 0 )
  {
    std::cout << ", orderL,H = " << order;
    std::cout << ", powerL,H = " << ii << std::endl;
  }
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

//  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.PreconditionerSide] = SLA::PreconditionerASMParam::params.PreconditionerSide.Right;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-16;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 200;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = true;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
//  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-30;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 40000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 4000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  if (world.rank() == 0 )
  {
    std::cout << "Linear solver: PETSc" << std::endl;
  }
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-5;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 7;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = true;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  // Easier PTC for subsequent solves
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  string filename_base = "tmp/OJEDARANS2D/DGBR2/";

  string filename2 = filename_base + "_Errors.txt";
  fstream foutsol;

  string filename = filename_base + "_P";
  filename += to_string(order);
  filename += "_";
  filename += to_string(ii);
  filename += "x";
  filename += to_string(jj);

  foutsol.open( filename2, fstream::app );

  // grid:
  XField2D_Box_Triangle_Lagrange_X1 pxfld( world, ii, jj );
  ArrayQ q0 = 0;
  pde.setDOFFrom( q0, SAnt2D<DensityVelocityPressure2D<Real>>({rho0, u0, v0, p0, nt0}) );

  Field_CG_Cell<PhysD2, TopoD2, Real> distfld(pxfld, 1, BasisFunctionCategory_Lagrange);
  for (int n = 0; n < distfld.nDOF(); n++)
    distfld.DOF(n) = 1000.;

  const int quadOrder = 2*(order + 1); // Is this high enough?

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  PyDict ParamDict;
  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>((distfld, pxfld), pde, order, order,
      BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
      active_boundaries, ParamDict, disc);

  pGlobalSol->setSolution(q0);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
      cellGroups, interiorTraceGroups,
      PyBCList, BCBoundaryGroups,
      SolverContinuationDict, LinearSolverDict,
      outputIntegrand);

  // Sortof L2 projection of exact solution as initial guess
  //for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(*solnptr, {0}),
  //    (pxfld, pGlobalSol->primal.qfld) );

  pInterface->solveGlobalPrimalProblem();

  Real HExact = 3.5287871306206915036;
  Real H = pInterface->getOutput();

  if (world.rank() == 0 )
  {
  std::cout << "Enthalpy Error: " << std::setprecision(15) << fabs(H - HExact) << "\n";
  }

  std::string filename_q = filename_base + "qfld_P" + std::to_string(order) +"_X" + std::to_string(ii) + ".plt";

  output_Tecplot(pGlobalSol->primal.qfld, filename_q);

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
