// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_DGBR2_Triangle_Euler_RAE2822_ArtificialViscosity_toy
// testing of 2-D DGBR2 for Transonic Euler


#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/JacobianParam.h"
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_Block2x2_PTC.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Field/HField/GenHFieldArea_CG.h"
#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "Meshing/EPIC/XField_PX.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "tools/output_std_vector.h"
#include "tools/linspace.h"

using namespace std;
using namespace SANS;

namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_Triangle_Euler_RAE2822_ArtificialViscosity_toy )

////////////////////////////////////////////////////////////////////////////////////////
// Notes:
//  - Using QTypePrimitiveRhoPressure
//  - Needed to PSeq and use PTC to get a subsonic (M=0.5) result
//  - Needed to use PTC on P0 at M=0.85
//  - PSeq and PTC work for P1 at M=0.85 with vanilla artificial viscosity
// No longer running Joukowski - Running RAE2822 instead
//  - Moved to M=0.73 aoa=3.2deg following a BR paper
//  - PTC P0 -> PTC P1 worked on 2494 elem mesh but generated lots of viscosity
//  - PTC P0 -> PTC P1 ??? on 5568
// Default PETSc abstol is 1e-50, 1e-10 is much too big
////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_Triangle_Euler_RAE2822_ArtificialViscosity_toy )
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // MPI comm world
  ////////////////////////////////////////////////////////////////////////////////////////
  mpi::communicator world;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for Euler2D sans AV
  ////////////////////////////////////////////////////////////////////////////////////////
//  typedef QTypeEntropy QType;
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for discretization sans AV
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;
  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PDEPrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD2, TopoD2> > AlgebraicEquationSet_PTCClass;

  typedef PDEPrimalEquationSetClass::BCParams BCParams;
  typedef PDEPrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PDEPrimalEquationSetClass::SystemVector SystemVectorClass;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for Euler2D mit AV
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef PDENDConvertSpace<PhysD2, AVPDEClass> AVNDPDEClass;
  typedef AVNDPDEClass::ArrayQ<Real> AVArrayQ;
  typedef AVNDPDEClass::template VectorArrayQ<Real> AVVectorArrayQ;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for discretization mit AV
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;

  typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                         XField<PhysD2, TopoD2> >::type ParamField;

  typedef BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVBCVector;
  typedef AlgebraicEquationSet_DGBR2< AVNDPDEClass, BCNDConvertSpace, AVBCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, ParamField > AVPDEPrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<AVNDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamField > AVAlgebraicEquationSet_PTCClass;

  typedef AVPDEPrimalEquationSetClass::BCParams AVBCParams;
  typedef AVPDEPrimalEquationSetClass::SystemMatrix AVSystemMatrixClass;
  typedef AVPDEPrimalEquationSetClass::SystemVector AVSystemVectorClass;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Solution order
  ////////////////////////////////////////////////////////////////////////////////////////
  int order_pde = 1;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler PDE sans AV
  ////////////////////////////////////////////////////////////////////////////////////////
  // Gas Model
  Real gamma, R;
  gamma = 1.4;
  R = 1;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  // PDE
  EulerResidualInterpCategory interp = Euler_ResidInterp_Momentum;
  NDPDEClass pde(gas, interp);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler PDE mit AV
  ////////////////////////////////////////////////////////////////////////////////////////
  bool hasSpaceTimeDiffusion = false;
  PDEBaseClass pdeEulerAV(order_pde, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp);
  // Sensor equation terms
  Sensor sensor(pdeEulerAV);
  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  SensorViscousFlux sensor_visc(order_pde);
  SensorSource sensor_source(order_pde, sensor);
  // AV PDE with sensor equation
  bool isSteady = false;
  AVNDPDEClass avpde(sensor_adv, sensor_visc, sensor_source, isSteady, order_pde,
                     hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Define boundary conditions - P0
  ////////////////////////////////////////////////////////////////////////////////////////
  // reference state (freestream)
  const Real Mach = 0.73;
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 3.2 * PI / 180.0;                          // angle of attack (radians)

  const Real pRef = 1.0;
  const Real tRef = pRef/rhoRef/R;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  PyDict Inflow;
  Inflow[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitiveTemperature;
  Inflow[DensityVelocityTemperature2DParams::params.rho] = rhoRef;
  Inflow[DensityVelocityTemperature2DParams::params.u] = uRef;
  Inflow[DensityVelocityTemperature2DParams::params.v] = vRef;
  Inflow[DensityVelocityTemperature2DParams::params.t] = tRef;

  std::cout << "rho: " << rhoRef << "[kg/m3]" << std::endl
            << "u:   " << uRef   << "[m/s]"   << std::endl
            << "v:   " << vRef   << "[m/s]"   << std::endl
            << "T:   " << tRef   << "[K]"     << std::endl;

  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCClass;

  // Define inflow BC
  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCInflow[BCClass::ParamsType::params.Characteristic] = true;
  BCInflow[BCClass::ParamsType::params.StateVector] = Inflow;

  // Define wall BC
  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  // Define BC list
  PyDict PyPDEBCList;
  PyPDEBCList["Wall"] = BCWall;
  PyPDEBCList["Inflow"] = BCInflow;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Define boundary conditions - Higher P
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef BCEuler2D<BCTypeFullState_mitState, AVPDEClass> AVBCBase;
  typedef BCmitAVSensor2D<BCTypeFullState_mitState, AVBCBase> AVBCClass;

  // Define inflow BC
  PyDict BCAVInflow;
  BCAVInflow[AVBCParams::params.BC.BCType] = AVBCParams::params.BC.FullState;
  BCAVInflow[AVBCClass::ParamsType::params.Characteristic] = true;
  BCAVInflow[AVBCClass::ParamsType::params.StateVector] = Inflow;

  // Define wall BC
  PyDict BCAVWall;
  BCAVWall[AVBCParams::params.BC.BCType] = AVBCParams::params.BC.Symmetry_mitState;

  // Define BC list
  PyDict PyAVPDEBCList;
  PyAVPDEBCList["Wall"] = BCAVWall;
  PyAVPDEBCList["Inflow"] = BCAVInflow;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  string meshName = "grids/RAE2822_5568.gri";
  XField_PX<PhysD2, TopoD2> xfld(world, meshName);
  GenHField_CG<PhysD2,TopoD2> hfld(xfld);
  std::cout << "nCellGroups: " << xfld.nCellGroups() << " nInteriorTraceGroups: " << xfld.nInteriorTraceGroups() << std::endl;
  std::vector<int> cellGroups, interiorTraceGroups;
  linspace( 0, xfld.nCellGroups()-1, cellGroups );
  linspace( 0, xfld.nInteriorTraceGroups()-1, interiorTraceGroups );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Get boundary group information
  ////////////////////////////////////////////////////////////////////////////////////////
  std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;
  PDEBCBoundaryGroups["Wall"] = {4};
  PDEBCBoundaryGroups["Inflow"] = {0,1,2,3};
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check BCs
  ////////////////////////////////////////////////////////////////////////////////////////
  BCParams::checkInputs(PyPDEBCList);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate our Euler PDE field - P0
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> pde_qfld0(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, AVArrayQ> pde_qqfld0(xfld, 0, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> pde_lgfld0( xfld, 0, BasisFunctionCategory_Legendre,
                                                            BCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups) );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate our Euler PDE field - higher P
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysD2, TopoD2, AVArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Legendre);
  FieldLift_DG_Cell<PhysD2, TopoD2, AVVectorArrayQ> pde_rfld(xfld, order_pde, BasisFunctionCategory_Legendre);
  std::shared_ptr<Field_DG_Cell<PhysD2, TopoD2, Real>> pde_sfld (new Field_DG_Cell<PhysD2, TopoD2, Real>(xfld, 0, BasisFunctionCategory_Legendre) );
  Field_DG_BoundaryTrace<PhysD2, TopoD2, AVArrayQ> pde_lgfld( xfld, order_pde, BasisFunctionCategory_Legendre,
                                                            BCParams::getLGBoundaryGroups(PyAVPDEBCList, PDEBCBoundaryGroups) );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Initial condition for what ever I am doing
  ////////////////////////////////////////////////////////////////////////////////////////
  ArrayQ q0 = pde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoRef, uRef, vRef, tRef) );
  AVVariable<DensityVelocityTemperature2D, Real> qdata = {{rhoRef, uRef, vRef, tRef}, 0.0};
  ArrayQ q1 = pde.setDOFFrom(qdata);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial conditions - P0
  ////////////////////////////////////////////////////////////////////////////////////////
  pde_qfld0 = q0;
  pde_lgfld0 = 0;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // AES Settings
  ////////////////////////////////////////////////////////////////////////////////////////
  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {2e-8, 2e-8};
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our AES to solve our PDE sans AV
  ////////////////////////////////////////////////////////////////////////////////////////
  ParamField paramfld = (hfld, xfld);
  PDEPrimalEquationSetClass PrincipalEqSet0(xfld, pde_qfld0, pde_lgfld0, pde, quadratureOrder, ResidualNorm_Default, tol, cellGroups,
                                            interiorTraceGroups, PyPDEBCList, PDEBCBoundaryGroups);
  AVPDEPrimalEquationSetClass PrincipalEqSet(paramfld, pde_qfld, pde_rfld, pde_lgfld, pde_sfld, avpde, disc, quadratureOrder, ResidualNorm_Default,
                                             tol, cellGroups, interiorTraceGroups, PyAVPDEBCList, PDEBCBoundaryGroups);

  AlgebraicEquationSet_PTCClass PrincipalEqSetPTC0(xfld, pde_qfld0, pde, quadratureOrder, cellGroups, PrincipalEqSet0);
  AVAlgebraicEquationSet_PTCClass PrincipalEqSetPTC(paramfld, pde_qfld, avpde, quadratureOrder, cellGroups, PrincipalEqSet);
  ////////////////////////////////////////////////////////////////////////////////////////

#define P_SEQUENCE 1
#if P_SEQUENCE
  ////////////////////////////////////////////////////////////////////////////////////////
  // Construct Newton Solver - P0
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict NewtonSolverDict0, LineUpdateDict0;
#if defined(SANS_PETSC)
  std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict0;
  PyDict PreconditionerILU0;
  PyDict PETScDict0;

  PreconditionerILU0[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU0[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU0[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU0[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict0[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict0[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU0;

  PETScDict0[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict0[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict0[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0e-10;
  PETScDict0[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict0[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict0[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict0;
  PETScDict0[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict0[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict0[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict0[NewtonSolverParam::params.LinearSolver] = PETScDict0;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict0;
  MKL_PARDISODict0[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  NewtonSolverDict0[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict0;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict0;
  UMFPACKDict0[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict0[NewtonSolverParam::params.LinearSolver] = UMFPACKDict0;
#endif

  LineUpdateDict0[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict0[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;
  LineUpdateDict0[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict0[NewtonSolverParam::params.LineUpdate] = LineUpdateDict0;
  NewtonSolverDict0[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict0[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict0[NewtonSolverParam::params.Verbose] = true;

  NewtonSolverParam::checkInputs(NewtonSolverDict0);
  NewtonSolver<SystemMatrixClass> Solver0( PrincipalEqSet0, NewtonSolverDict0 );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial guess from initial condition - P0
  ////////////////////////////////////////////////////////////////////////////////////////
  SystemVectorClass ini0(PrincipalEqSet0.vectorStateSize());
  SystemVectorClass sln0(PrincipalEqSet0.vectorStateSize());

  PrincipalEqSet0.fillSystemVector(ini0);
  sln0 = ini0;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // SOLVE - P0
  ////////////////////////////////////////////////////////////////////////////////////////
#define PSEQ_NEWTON_SOLVE 0
#if PSEQ_NEWTON_SOLVE
  SolveStatus status = Solver.solve(ini, sln);
  BOOST_CHECK( status.converged );
#else
  NewtonSolverDict0[NewtonSolverParam::params.MaxIterations] = 20;
  PyDict PseudoTimeDict0;
  PseudoTimeDict0[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict0;
  PseudoTimeDict0[PseudoTimeParam::params.invCFL] = 100;
  PseudoTimeDict0[PseudoTimeParam::params.invCFL_max] = 1000;
  PseudoTimeDict0[PseudoTimeParam::params.invCFL_min] = 0.0;

  PseudoTimeDict0[PseudoTimeParam::params.CFLDecreaseFactor] = 0.1;
  PseudoTimeDict0[PseudoTimeParam::params.CFLIncreaseFactor] = 2.0;
  PseudoTimeDict0[PseudoTimeParam::params.Verbose] = true;

#if defined(SANS_PETSC)
  PseudoTimeDict0[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/PTC0_History_PETSc.dat";
#elif defined(INTEL_MKL)
  PseudoTimeDict0[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/PTC0_History_MKL.dat";
#else
  PseudoTimeDict0[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/PTC0_History_UMFPACK.dat";
#endif

  PseudoTime<SystemMatrixClass> PTC0(PseudoTimeDict0, PrincipalEqSetPTC0);
  PTC0.iterate(100);
#endif
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial conditions - Higher P
  ////////////////////////////////////////////////////////////////////////////////////////
//  pde_qfld0.projectTo(pde_qfld);
  // This here is a HACK
  for (int i = 0; i < pde_qqfld0.nDOF(); i++)
  {
    pde_qqfld0.DOF(i)[0] = pde_qfld0.DOF(i)[0];
    pde_qqfld0.DOF(i)[1] = pde_qfld0.DOF(i)[1];
    pde_qqfld0.DOF(i)[2] = pde_qfld0.DOF(i)[2];
    pde_qqfld0.DOF(i)[3] = pde_qfld0.DOF(i)[3];
    pde_qqfld0.DOF(i)[4] = 0.0;
  }
  pde_qqfld0.projectTo(pde_qfld);
  pde_rfld = 0;
  pde_lgfld = 0;
  ////////////////////////////////////////////////////////////////////////////////////////

#if 1
  string filename0;
  filename0 = "tmp/RAE2822_5568_"
            + Type2String<QType>::str()
            + "_P"
            + std::to_string(order_pde)
            + "_PSeq."
            + std::to_string(world.rank())
            + ".plt";
  output_Tecplot( pde_qfld, filename0 );
#endif

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial guess from initial condition - Higher P
  ////////////////////////////////////////////////////////////////////////////////////////
  AVSystemVectorClass ini(PrincipalEqSetPTC.vectorStateSize());
  AVSystemVectorClass sln(PrincipalEqSetPTC.vectorStateSize());

  PrincipalEqSetPTC.fillSystemVector(ini);
  sln = ini;
  ////////////////////////////////////////////////////////////////////////////////////////
#else
  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  pde_qfld = q1;
  pde_rfld = 0;
  pde_lgfld = 0;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial guess from initial condition - P0
  ////////////////////////////////////////////////////////////////////////////////////////
  BlockVectorClass ini(BlockAES.vectorStateSize());
  BlockVectorClass sln(BlockAES.vectorStateSize());

  BlockAES.fillSystemVector(ini);
  sln = ini;
  ////////////////////////////////////////////////////////////////////////////////////////
#endif

  ////////////////////////////////////////////////////////////////////////////////////////
  // Construct Newton Solver
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict NewtonSolverDict, LineUpdateDict;
#if defined(SANS_PETSC)
  std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NewtonSolverParam::checkInputs(NewtonSolverDict);
  NewtonSolver<AVSystemMatrixClass> Solver( PrincipalEqSetPTC, NewtonSolverDict );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Solve
  ////////////////////////////////////////////////////////////////////////////////////////
#define NEWTON_SOLVE 0
#if NEWTON_SOLVE
  SolveStatus status = Solver.solve(ini, sln);
  BOOST_CHECK( status.converged );
#else
  bool converged = false;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;
  PyDict PseudoTimeDict;
  PseudoTimeDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  PseudoTimeDict[PseudoTimeParam::params.invCFL] = 100;
  PseudoTimeDict[PseudoTimeParam::params.invCFL_max] = 1000;
  PseudoTimeDict[PseudoTimeParam::params.invCFL_min] = 0.0;

  PseudoTimeDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.1;
  PseudoTimeDict[PseudoTimeParam::params.CFLIncreaseFactor] = 2.0;
  PseudoTimeDict[PseudoTimeParam::params.Verbose] = true;

#if defined(SANS_PETSC)
  PseudoTimeDict[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/PTC_History_PETSc.dat";
#elif defined(INTEL_MKL)
  PseudoTimeDict[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/PTC_History_MKL.dat";
#else
  PseudoTimeDict[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/PTC_History_UMFPACK.dat";
#endif

  PseudoTime<AVSystemMatrixClass> PTC(PseudoTimeDict, PrincipalEqSetPTC);
  converged = PTC.iterate(120);
#endif
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output Tecplot file
  ////////////////////////////////////////////////////////////////////////////////////////
  string filename;
  filename = "tmp/RAE2822_5568_"
            + Type2String<QType>::str()
            + "_P"
            + std::to_string(order_pde)
            + "."
            + std::to_string(world.rank())
            + ".plt";
  output_Tecplot( pde_qfld, filename );
  ////////////////////////////////////////////////////////////////////////////////////////
  BOOST_CHECK(converged);
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
