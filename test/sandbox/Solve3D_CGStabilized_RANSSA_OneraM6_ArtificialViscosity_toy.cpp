// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve3D_CGStabilized_RANSSA_OneraM6_ArtificialViscosity_toy

//#define SANS_FULLTEST
//#define SANS_VERBOSE
#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>

#include <boost/filesystem.hpp> // to automagically make the directories

#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"

#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/TraitsRANSSAArtificialViscosity.h"
#include "pde/NS/PDERANSSA3D.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/PDEEulermitAVDiffusion3D.h"
#include "pde/NS/BCRANSSA3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/BCEulermitAVSensor3D.h"
#include "pde/NS/Fluids3D_Sensor.h"
#include "pde/NS/OutputEuler3D.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/ugrid/XField_ugrid.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


using namespace std;

namespace SANS
{

template <class T>
using SAnt3D_rhovP = SAnt3D<DensityVelocityPressure3D<T>>;

}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_CGStabilized_RANSSA_OneraM6_ArtificialViscosity_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve3D_CGStabilized_RANSSA_OneraM6_ArtificialViscosity )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;

  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
//  typedef PDERANSSA3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass> PDEBaseClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD3, PDEBaseClass> Sensor;
  typedef AVSensor_Source3D_PressureGrad<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCRANSSAmitAVDiffusion3DVector<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputRANSSA3D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
#if 0
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#endif
#else
  typedef OutputEuler3D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_DG_Distance ParamBuilderType;
  typedef GenHField_DG<PhysD3, TopoD3> GenHFieldType;

  typedef SolutionData_Galerkin_Stabilized<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {5e-7, 5e-7};

  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  bool isSteady = true;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const Real tRef = 1.0;          // temperature
  const Real pRef = 1.0;          // pressure
  const Real lRef = 1;            // length scale

  const Real Mach = 0.8395;
  const Real Reynolds = 11.72e6;
  const Real Prandtl = 0.72;
  const Real chiRef = 3;

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;

//  const Real lRef = 1;                            // length scale
  const Real rhoRef = 1;                            // density scale
//  const Real qRef = 0.1;                          // velocity scale
  const Real aoaRef = 3.06 * M_PI/180.0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = 0.0;
  const Real wRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict StateVector;
  StateVector[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.Conservative;
  StateVector[Conservative3DParams::params.rho] = rhoRef;
  StateVector[Conservative3DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative3DParams::params.rhov] = rhoRef*vRef;
  StateVector[Conservative3DParams::params.rhow] = rhoRef*wRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef + wRef*wRef);
  StateVector[Conservative3DParams::params.rhoE] = rhoRef*ERef;
  StateVector["rhont"]  = rhoRef*ntRef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler3DFullStateParams<NSVariableType3DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler3DFullStateParams<NSVariableType3DParams>::params.Characteristic] = true;

//  PyDict BCOut;
//  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
//  const Real p_ratio = 1.0;
//  BCOut[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = p_ratio*pRef;

  // Create a BC dictionary
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;
//  PyBCList["BCOut"] = BCOut;
  PyBCList["BCWall"] = BCWall;
  PyBCList["BCSymmetry"] = BCSymmetry;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCWall"] = {0};
//  BCBoundaryGroups["BCOut"] = {2};
  BCBoundaryGroups["BCIn"] = {2};
  BCBoundaryGroups["BCSymmetry"] = {1};

  std::vector<int> walls;
  walls.insert( walls.end(), BCBoundaryGroups.at("BCWall").begin(), BCBoundaryGroups.at("BCWall").end() );

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);


  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
if (world.rank() == 0 )
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
  PreconditionerDict[SLA::PreconditionerASMParam::params.Overlap] = 2;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 500;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  NewtonSolverParam::checkInputs(NewtonSolverDict);
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);
  //
  int ordermin = 1;
  int ordermax = 2;

  std::string filename_base = "tmp/RANSOneraM6/";

  if (world.rank() == 0)
  {
    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directories(filename_base);
  }

  fstream foutputhist;
  std::ofstream resultFile;
  if (world.rank() == 0)
  {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Create tecplot file to put total enthalpy errors
    ////////////////////////////////////////////////////////////////////////////////////////
    resultFile.open( filename_base + "DragCGRANSSA.dat", fstream::out );
    resultFile << "VARIABLES=";
    resultFile << "\"DOF\"";
    resultFile << ", \"1/sqrt(DOF)\"";
    resultFile << ", \"L2 error\"";
    resultFile << std::endl;
    resultFile << std::setprecision(16) << std::scientific;
    ////////////////////////////////////////////////////////////////////////////////////////
  }

  world.barrier();

  for (int order = ordermin; order <= ordermax; order++)
  {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Tecplot zone for this order
    ////////////////////////////////////////////////////////////////////////////////////////
    if (world.rank() == 0)
    {
      resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    }
    ////////////////////////////////////////////////////////////////////////////////////////

    PDEBaseClass pdeRANSSAAV(order, gas, visc, tcond, interp );
    // Sensor equation terms
    Sensor sensor(pdeRANSSAAV);
    SensorAdvectiveFlux sensor_adv(0.0, 0.0, 0.0);
    SensorViscousFlux sensor_visc(order);
    SensorSource sensor_source(order, sensor);
    // AV PDE with sensor equation
    NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady,
                   order, gas, visc, tcond, interp);

    // initial condition
    AVVariable<SAnt3D_rhovP,Real> qdata({rhoRef, uRef, vRef, wRef, pRef, ntRef}, 0.0);
    ArrayQ q0;
    pde.setDOFFrom( q0, qdata );

    StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby);

#ifndef BOUNDARYOUTPUT
    // Entropy output
    NDOutputClass fcnOutput(pde, 1.0);
#if 0
    OutputIntegrandClass outputIntegrand(fcnOutput, walls);
#else
    OutputIntegrandClass outputIntegrand(pde, fcnOutput, walls, stab);
#endif
#else
    // Drag output
    NDOutputClass outputFcn(pde, 1., 0., 1.);
    OutputIntegrandClass outputIntegrand( outputFcn, walls );
#endif

    stab.setStabOrder(order);
    stab.setNitscheOrder(order);

    for (int grid_index = 5; grid_index >= 3; grid_index--)
    {
      std::string meshName = "grids/OneraM6/wing_tetra." + std::to_string(grid_index) + ".lb8.ugrid";

      std::shared_ptr<XField<PhysD3, TopoD3>> pxfld( new XField_ugrid<PhysD3, TopoD3>( world, meshName ) );
      std::shared_ptr<GenHFieldType> phfld = std::make_shared<GenHFieldType>(*pxfld);
      const int orderDist = 3;
      std::shared_ptr<Field_CG_Cell<PhysD3, TopoD3, Real>>
        pdistfld = std::make_shared<Field_CG_Cell<PhysD3, TopoD3, Real>>(*pxfld, orderDist, BasisFunctionCategory_Lagrange);
      DistanceFunction(*pdistfld, walls);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_P" + to_string(order)
                                                                                       + "_G" + to_string(grid_index)
                                                                                       + "_residual.dat";

      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pdistfld, *pxfld), pde, order, order+1,
                                                   BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                   active_boundaries, stab);

      const int quadOrder = 3*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrand);
                                                          
      output_Tecplot( get<1>(pGlobalSol->paramfld), filename_base + "distfld.dat" );

      pGlobalSol->setSolution(q0);

      pInterface->solveGlobalPrimalProblem();

      // Monitor Drag Error
      Real drag = pInterface->getOutput();


#ifdef SANS_MPI
      int nDOFtotal = 0;
      boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );
#else
      int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
#endif
      if (world.rank() == 0)
        std::cout << "P = " << order << " nDOF = " << nDOFtotal
                  << " : Drag = " << std::setprecision(16) << drag << endl;

      if (world.rank() == 0)
      {
        ////////////////////////////////////////////////////////////////////////////////////////
        // Output Error to tecplot file
        ////////////////////////////////////////////////////////////////////////////////////////
        resultFile << " " << nDOFtotal;
        resultFile << " " << 1.0/sqrt(nDOFtotal);
        resultFile << " " << drag;
        resultFile << std::endl;
        ////////////////////////////////////////////////////////////////////////////////////////
      }

#if 1
      // Tecplot dump grid
      // Tecplot dump grid
      std::string qfld_filename = filename_base + "slnCG_RANSSAONERAM6_P";
      qfld_filename += to_string(order);
      qfld_filename += "_G";
      qfld_filename += to_string(grid_index);
      qfld_filename += ".dat";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
#endif
    }

  }

}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
