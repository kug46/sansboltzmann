// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Testing of the MOESS framework with L2-Projection

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <fstream>

#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/FunctionNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "Adaptation/MOESS/SolverInterface_L2Project.h"
#include "Adaptation/MeshAdapter.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_CG_Cell.h"

#include "Field/output_Tecplot.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt1D_L2Project_MOESS_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt1D_L2Project_MOESS_test )
{

#define MAXCUBIC

#ifdef SINGLE_BL
  typedef SolnNDConvertSpace<PhysD1,ScalarFunction1D_BL> SolutionExact;
  std::string func = "SingleBL";
#elif defined(CORNER)
  typedef SolnNDConvertSpace<PhysD1,ScalarFunction1D_Monomial> SolutionExact;
  std::string func = "Corner1D";
#elif defined(TANH)
  typedef SolnNDConvertSpace<PhysD1,ScalarFunction1D_Tanh> SolutionExact;
  std::string func = "Tanh1D";
#elif defined(EXP)
  typedef SolnNDConvertSpace<PhysD1,ScalarFunction1D_Exp> SolutionExact;
  std::string func = "Exp1D";
#elif defined(MAXCUBIC)
  typedef SolnNDConvertSpace<PhysD1,ScalarFunction1D_MaxCubic> SolutionExact;
  std::string func = "MaxCubic1D";
#endif

  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;

#define USE_CG 0

#if USE_CG
  typedef SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact, Field_CG_Cell> SolverInterfaceClass;
#else
  typedef SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact, Field_DG_Cell> SolverInterfaceClass;
#endif

  mpi::communicator world;

  int orderL = 1, orderH = 1;
  int powerL = 3, powerH = 3;

  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc == 3)
  {
    orderL = orderH = std::stoi(argv[1]);
    powerL = powerH = std::stoi(argv[2]);
  }

  // Create a solution dictionary
  PyDict solnArgs;
#ifdef SINGLE_BL
  Real xmin = 0.0;
  Real xmax = 1.0;

  solnArgs[SolutionExact::ParamsType::params.a] = 1.0;
  solnArgs[SolutionExact::ParamsType::params.nu] = 1./10.;
  solnArgs[SolutionExact::ParamsType::params.offset] = 1;
  solnArgs[SolutionExact::ParamsType::params.scale] = -1;
#elif defined(CORNER)
  Real xmin = 0.0;
  Real xmax = 1.0;
  solnArgs[SolutionExact::ParamsType::params.i] = 2.0/3.0;
#elif defined(TANH)
  Real xmin = -1.0;
  Real xmax =  1.0;
  solnArgs[SolutionExact::ParamsType::params.nu] = 1./10;
#elif defined(EXP)
  Real xmin = 0.0;
  Real xmax = 1.0;
  solnArgs[SolutionExact::ParamsType::params.a] = 1.0;
#elif defined(MAXCUBIC)
  Real xmin = -1.0;
  Real xmax = 1.0;
  solnArgs[SolutionExact::ParamsType::params.a] = 1.0;
#endif

  SolutionExact solnExact(solnArgs);

  std::array<Real,1> tol = {{1e-20}};

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-9;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;
  MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
  //MOESSDict[MOESSParams::params.FrobNormSqSum_GlobalFraction] = 1.0;
  //MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
#if USE_CG
  MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Element;
#endif

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD1, TopoD1>::params.Mesher.Name] = MeshAdapterParams<PhysD1, TopoD1>::params.Mesher.embedding;

  //--------ADAPTATION LOOP--------

  const int maxIter = 30;

  for (int order = orderL; order <= orderH; order++)
  {
    for (int power = powerL; power <= powerH; power++ )
    {
      int nk = pow(2,power);
      int targetCost = 10*nk;

#if USE_CG
      Real nDOFperCell_DG = (order+1)*(order+2)/2;
      //equiVol = sqrt(2.0)/12;
      //targetComp = targetCost*equiVol/nDOFperCell_DG;
      //targetCost = targetComp*nDOFperCell_CG/equiVol;

      Real nDOFperCell_CG = nDOFperCell_DG;
      nDOFperCell_CG -= (3 - 1./2); // the node dofs are shared by 6
      nDOFperCell_CG -= (3 - 3./2)* MAX(0,(order - 1 )); // if there are edge dofs they are shared

      targetCost = targetCost*nDOFperCell_CG/nDOFperCell_DG;
#endif

      const int string_pad = 6;
      std::string int_pad = std::string(string_pad - std::to_string(targetCost).length(), '0') + std::to_string(targetCost);

      std::string filename_base = "tmp/L2Project/" + func + "/";
#if USE_CG
      std::string G = "CG";
#else
      std::string G = "DG";
#endif
      filename_base += G + "_" + int_pad + "_P" + std::to_string(order) + "_MOESS/";

      boost::filesystem::path base_dir(filename_base);
      if ( not boost::filesystem::exists(base_dir) )
        boost::filesystem::create_directories(filename_base);

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist;
      if ( world.rank() == 0 )
      {
        fadapthist.open( adapthist_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
      }

      std::string output_filename = filename_base + "output.dat";
      fstream foutputhist;
      if ( world.rank() == 0 )
      {
        foutputhist.open( output_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + output_filename);
      }

      if (world.rank() == 0 )
      {
        // write the header to the output file
        foutputhist << "VARIABLES="
                    << std::setw(5)  << "\"Iter\""
                    << std::setw(10) << "\"DOF\""
                    << std::setw(20) << "\"Elements\""
                    << std::setw(20) << "\"L<sup>2</sup> Error\""
                    << std::endl;

        foutputhist << "ZONE T=\"MOESS " << nk << "k\"" << std::endl;
      }

      MesherDict[refineParams::params.FilenameBase] = filename_base;

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.FilenameBase] = filename_base;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.dumpStepMatrix] = true;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.dumpRateMatrix] = true;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.dumpMetric] = true;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.dumpError] = true;
      AdaptDict[MeshAdapterParams<PhysD1, TopoD1>::params.dumpOptimizationInfo] = true;

      MeshAdapterParams<PhysD1, TopoD1>::checkInputs(AdaptDict);

      MeshAdapter<PhysDim, TopoDim> mesh_adapter(AdaptDict, fadapthist);

      std::shared_ptr<XField<PhysDim, TopoDim>> pxfld;

      pxfld = std::make_shared<XField1D>( 5, xmin, xmax );

      std::vector<int> cellGroups = {0};
      int iter = 0;
      while (true)
      {
        std::cout << "-----Adaptation Iteration " << iter << "-----" << std::endl;

#if USE_CG
        Field_CG_Cell<PhysDim,TopoDim,Real> qfld(*pxfld, order, BasisFunctionCategory_Lagrange);
#else
        Field_DG_Cell<PhysDim,TopoDim,Real> qfld(*pxfld, order, BasisFunctionCategory_Legendre);
#endif
        // Adjoint equation is integrated with 2 times adjoint order
        const int quadOrder = 2*(order+1);

        //Perform L2 projection from solution on previous mesh
        qfld = 0;

        std::shared_ptr<SolverInterfaceClass> pInterface;
        pInterface = std::make_shared<SolverInterfaceClass>(qfld, quadOrder,
                                                            cellGroups, tol, solnExact,
                                                            LinearSolverDict);

        pInterface->solveGlobalPrimalProblem();

        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter)
                                                  + "_P" + std::to_string(order) +
                                                  + ".plt";
        //if ( iter == maxIter )
          output_Tecplot( qfld, qfld_filename );

#ifdef SANS_MPI
        int nDOFtotal = 0;
        boost::mpi::reduce(*pxfld->comm(), qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );

        // count the number of elements possessed by this processor
        int nElem = 0;
        for (int elem = 0; elem < pxfld->nElem(); elem++ )
          if (pxfld->getCellGroupGlobal<Line>(0).associativity(elem).rank() == world.rank())
            nElem++;

        int nElemtotal = 0;
        boost::mpi::reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>(), 0 );
#else
        int nDOFtotal = qfld.nDOFpossessed();
        int nElemtotal = pxfld->nElem();
#endif

        //Compute error estimates
        pInterface->computeErrorEstimates();

        Real L2error = pInterface->getOutput();

        if (world.rank() == 0 )
        {
          foutputhist << std::setw(5) << iter
                      << std::setw(10) << nDOFtotal
                      << std::setw(10) << nElemtotal
                      << std::setw(20) << std::setprecision(10) << std::scientific << L2error
                      << std::endl;
        }
        if ( iter == maxIter ) break;

        //Perform local sampling and adapt mesh
        pxfld = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

        iter++;
      }

      fadapthist.close();
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
