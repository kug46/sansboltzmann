// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_Euler_ArtificialViscosity_ST_toy
// Testing of the MOESS framework on a space-time Buckley-Leverett problem with artificial viscosity

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/PDEEuler1D.h"
#include "pde/NS/BCEuler1D.h"
#include "pde/NS/SolutionFunction_Euler1D.h"
#include "pde/NS/Fluids1D_Sensor.h"
#include "pde/NS/OutputEuler1D.h"

#include "pde/Sensor/Source2D_JumpSensor.h"
#include "pde/Sensor/PDESensorParameter2D.h"
#include "pde/Sensor/BCSensorParameter2D.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2_Block2x2.h"
#include "Discretization/DG/SolutionData_DGBR2_Block2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//#define UNSTEADY_FORMULATION

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_Euler_ArtificialViscosity_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_Euler_ArtificialViscosity_ST_Triangle )
{
//  typedef QTypeEntropy QType;
  typedef QTypeConservative QType;
//  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass_Principal;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass_Principal> NDPDEClass_Principal;
  typedef NDPDEClass_Principal::ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_Euler1D_Riemann<TraitsSizeEuler, TraitsModelEulerClass> EulerSolution;

  typedef BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef Fluids_Sensor<PhysD1, PDEClass> Sensor;
  typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;
  typedef PDESensorParameter<PhysD2,
                             TraitsSizeEuler<PhysD1>,
                             Sensor_AdvectiveFlux2D_Uniform,
                             Sensor_ViscousFlux2D_GenHScale,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpace<PhysD2, SensorPDEClass> NDPDEClass_Sensor;
  typedef NDPDEClass_Sensor::template ArrayQ<Real> SensorArrayQ;
  typedef BCSensorParameter1DVector<Sensor_AdvectiveFlux2D_Uniform, Sensor_ViscousFlux2D_GenHScale> BCVectorSensor;

  typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;

  typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                         Field<PhysD2, TopoD2, SensorArrayQ>,
                                         XField<PhysD2, TopoD2> >::type AVParamField;

  typedef AlgebraicEquationSet_DGBR2< NDPDEClass_Principal, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, AVParamField > PrimalEquationSetClass;
  typedef BCEuler1D<BCTypeFunction_mitState, PDEClass> BCSolnClass;

  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef BCParameters<BCVectorSensor> BCParamsSensor;

  typedef BCSensorParameter2DVector<Sensor_AdvectiveFlux2D_None, Sensor_ViscousFlux2D_Uniform> SensorBCVector;

//  typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
//                                         Field<PhysD2, TopoD2, ArrayQ>,
//                                         XField<PhysD2, TopoD2>>::type ParamField;
//  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Sensor, BCNDConvertSpace, SensorBCVector,
//                                     AlgEqSetTraits_Sparse, DGBR2, ParamField> SensorPrimalEquationSetClass;
//  typedef SensorPrimalEquationSetClass::BCParams SensorBCParams;

  typedef OutputEuler1D_Entropy<PDEClass_Principal> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_ArtificialViscosity ParamBuilderType;

  typedef SolutionData_DGBR2_Block2<PhysD2, TopoD2, NDPDEClass_Principal, NDPDEClass_Sensor, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType0 ParamFieldType0;
  typedef typename SolutionClass::ParamFieldType1 ParamFieldType1;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Principal, BCNDConvertSpaceTime, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType0> PrimalEquationSetClass_Primal;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Sensor, BCNDConvertSpace, SensorBCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType1> PrimalEquationSetClass_Sensor;

  typedef SolverInterface_DGBR2_Block2x2<PhysD2, TopoD2,
                                         NDPDEClass_Principal, BCNDConvertSpaceTime, BCVector,
                                         NDPDEClass_Sensor, BCNDConvertSpace, SensorBCVector,
                                         ParamBuilderType, SolutionClass,
                                         PrimalEquationSetClass_Primal, PrimalEquationSetClass_Sensor,
                                         OutputIntegrandClass> SolverInterfaceClass;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  Real Tend = 0.25;
  int ii = 20; //grid size - using timesteps = grid size
  int tt = 20;
  int order_principal = 1;
  int order_sensor = 1;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, tt, 0, 1, 0, Tend, true );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler pde_principal
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);
  // pde_principal
  PDEClass::EulerResidualInterpCategory interp = PDEClass::Euler_ResidInterp_Momentum;
  bool hasSpaceTimeDiffusion = true;
  NDPDEClass_Principal pde_principal(order_principal, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  //  See: "Riemann Solvers and Numerical Methods for Fluid Dynamics" - Toro (Section 4.3.3 Table 4.1 in my copy)
  PyDict solnArgs;
  solnArgs[BCSolnClass::ParamsType::params.Function.Name] = BCSolnClass::ParamsType::params.Function.Riemann;
  solnArgs[EulerSolution::ParamsType::params.interface] = 0.5;
  solnArgs[EulerSolution::ParamsType::params.rhoL] = 1.0;
  solnArgs[EulerSolution::ParamsType::params.uL] = 0.0;
  solnArgs[EulerSolution::ParamsType::params.pL] = 1.0;
  solnArgs[EulerSolution::ParamsType::params.rhoR] = 0.5;
  solnArgs[EulerSolution::ParamsType::params.uR] = 0.0;
  solnArgs[EulerSolution::ParamsType::params.pR] = 0.5;
  solnArgs[EulerSolution::ParamsType::params.gasModel] = gasModelDict;

  // BC
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCReflect;
  BCReflect[BCParams::params.BC.BCType] = BCParams::params.BC.Reflect_mitState;

  PyDict BCTypeSpaceTimeInitialCondition;
  BCTypeSpaceTimeInitialCondition[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCTypeSpaceTimeInitialCondition[BCSolnClass::ParamsType::params.Function] = solnArgs;
  BCTypeSpaceTimeInitialCondition[BCSolnClass::ParamsType::params.Characteristic] = true;

  PyDict PyBCList_Principal;
  PyBCList_Principal["BCNone"] = BCNone;
  PyBCList_Principal["BCReflect"] = BCReflect;
  PyBCList_Principal["BCIn"] = BCTypeSpaceTimeInitialCondition;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Principal;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_Principal["BCNone"] = {2}; //t-max
  BCBoundaryGroups_Principal["BCReflect"] = {1,3}; //x-min, x-max
  BCBoundaryGroups_Principal["BCIn"] = {0}; //t-min

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList_Principal);

  std::vector<int> active_boundaries_principal = BCParams::getLGBoundaryGroups(PyBCList_Principal, BCBoundaryGroups_Principal);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our sensor parameter PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  Sensor sensor(pde_principal);
  Sensor_AdvectiveFlux2D_Uniform sensor_adv(0.0, 0.0);
  Sensor_ViscousFlux2D_GenHScale sensor_visc(1.0);
  Source_JumpSensor sensor_source(order_principal, sensor);

  NDPDEClass_Sensor pde_sensor(sensor_adv, sensor_visc, sensor_source);

  ////////////////////////////////////////////////////////////////////////////////////////
  // And boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict BCSensorRobin;
  BCSensorRobin[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorNeumann;
  BCSensorNeumann[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorNeumann[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 0.0;
  BCSensorNeumann[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCSensorNeumann[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorNone;
  BCSensorNone[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.None;

  PyDict PyBCList_Sensor;
//  PyBCList_Sensor["SensorRobin"] = BCSensorRobin;
  PyBCList_Sensor["SensorNeumann"] = BCSensorNeumann;
//  PyBCList_Sensor["SensorNone"] = BCSensorNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Sensor;
//  BCBoundaryGroups_Sensor["SensorRobin"] = {0,1,2,3}; // Bottom, right, left
  BCBoundaryGroups_Sensor["SensorNeumann"] = {2}; // Bottom, right, left
//  BCBoundaryGroups_Sensor["SensorNone"] = {2}; // Top

  //Check the BC dictionary
  BCParamsSensor::checkInputs(PyBCList_Sensor);

  std::vector<int> active_boundaries_sensor = BCParamsSensor::getLGBoundaryGroups(PyBCList_Sensor, BCBoundaryGroups_Sensor);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create parameters
  ////////////////////////////////////////////////////////////////////////////////////////
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol0 = {1e-12, 1e-12};
  std::vector<Real> tol1 = {1e-7, 1e-7};

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output Functional
  ////////////////////////////////////////////////////////////////////////////////////////
  NDOutputClass fcnOutput(pde_principal);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  ////////////////////////////////////////////////////////////////////////////////////////
  // Newton Solver
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-8;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type] =
                  SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver]    = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL]             = 1;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max]         = 1e4;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min]         = 0;
  NonlinearSolverDict[PseudoTimeParam::params.CFLDecreaseFactor]  = 0.2;
  NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor]  = 2;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up pyrite file
  ////////////////////////////////////////////////////////////////////////////////////////
  pyrite_file_stream pyriteFile("IO/Adaptation/Adaptation2D_Euler_ArtificialViscosity_ST.txt",
                                1e-10, 1e-10, pyrite_file_stream::check);
  pyriteFile << std::setprecision(16) << std::scientific;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Adaptation parameters
  ////////////////////////////////////////////////////////////////////////////////////////
  int maxIter = 20;
  Real targetCost = 10000;

  std::string adapthist_filename = "tmp/test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);


  ////////////////////////////////////////////////////////////////////////////////////////
  // Create ProblemStatement
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef SolutionData_DGBR2_Block2<PhysD2, TopoD2, NDPDEClass_Principal, NDPDEClass_Sensor, ParamBuilderType> SolutionClass;
  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde_principal, pde_sensor,
                                               order_principal, order_sensor, order_principal+1, order_sensor+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries_principal, active_boundaries_sensor, disc, disc);

  const int quadOrder = 2*(order_principal + 1);

  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, ResNormType, tol0, tol1, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList_Principal, PyBCList_Sensor,
                                                      BCBoundaryGroups_Principal, BCBoundaryGroups_Sensor,
                                                      SolverContinuationDict, outputIntegrand);


  ////////////////////////////////////////////////////////////////////////////////////////
  // Set Initial condition
  ////////////////////////////////////////////////////////////////////////////////////////
//  pGlobalSol->setSolution0(solnExact, cellGroups);
  ArrayQ q0 = pde_principal.setDOFFrom( DensityVelocityPressure1D<Real>(0.75, 0, 0.75) );
  pGlobalSol->primal0.qfld = q0;
  pGlobalSol->primal1.qfld = 0.0;

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output Initial Condition
  ////////////////////////////////////////////////////////////////////////////////////////
  //Solve the sensor equation independently
  pInterface->solveGlobalPrimalProblemIsolated(0);

  std::string qfld0_init_filename = "tmp/qfld0_init_a0.plt";
  std::string qfld1_init_filename = "tmp/qfld1_init_a0.plt";
  output_Tecplot( pGlobalSol->primal0.qfld, qfld0_init_filename );
  output_Tecplot( pGlobalSol->primal1.qfld, qfld1_init_filename );


  ////////////////////////////////////////////////////////////////////////////////////////
  // Solve Primal and output
  ////////////////////////////////////////////////////////////////////////////////////////
  pInterface->solveGlobalPrimalProblem();
  std::string qfld0_filename = "tmp/qfld0_a0.plt";
  std::string qfld1_filename = "tmp/qfld1_a0.plt";
  output_Tecplot( pGlobalSol->primal0.qfld, qfld0_filename );
  output_Tecplot( pGlobalSol->primal1.qfld, qfld1_filename );


  ////////////////////////////////////////////////////////////////////////////////////////
  // Solve Dual and output
  ////////////////////////////////////////////////////////////////////////////////////////
  pInterface->solveGlobalAdjointProblem();
  std::string adjfld0_filename = "tmp/adjfld0_a0.plt";
  std::string adjfld1_filename = "tmp/adjfld1_a0.plt";
  output_Tecplot( pGlobalSol->adjoint0.qfld, adjfld0_filename );
  output_Tecplot( pGlobalSol->adjoint1.qfld, adjfld1_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<std::endl<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde_principal, pde_sensor,
                                                    order_principal, order_sensor, order_principal+1, order_sensor+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries_principal, active_boundaries_sensor, disc, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, ResNormType,
                                                           tol0, tol1, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList_Principal, PyBCList_Sensor,
                                                           BCBoundaryGroups_Principal, BCBoundaryGroups_Sensor,
                                                           SolverContinuationDict, outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    qfld0_init_filename = "tmp/qfld0_init_a" + std::to_string(iter+1) + ".plt";
    qfld1_init_filename = "tmp/qfld1_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal0.qfld, qfld0_init_filename );
    output_Tecplot( pGlobalSol->primal1.qfld, qfld1_init_filename );

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    qfld0_filename = "tmp/qfld0_a" + std::to_string(iter+1) + ".plt";
    qfld1_filename = "tmp/qfld1_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal0.qfld, qfld0_filename );
    output_Tecplot( pGlobalSol->primal1.qfld, qfld1_filename );

    adjfld0_filename = "tmp/adjfld0_a" + std::to_string(iter+1) + ".plt";
    adjfld1_filename = "tmp/adjfld1_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint0.qfld, adjfld0_filename );
    output_Tecplot( pGlobalSol->adjoint1.qfld, adjfld1_filename );
  }

  fadapthist.close();
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
