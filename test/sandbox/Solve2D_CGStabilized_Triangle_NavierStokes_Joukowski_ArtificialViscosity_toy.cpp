// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_CGStabilized_Triangle_NavierStokes_Joukowski_ArtificialViscosity_toy

//#define SANS_FULLTEST
//#define SANS_VERBOSE
#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>

#include <boost/filesystem.hpp> // to automagically make the directories

#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsNavierStokesArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"
#include "pde/NS/EulerArtificialViscosityType.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/gmsh/XField_gmsh.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_CGStabilized_Triangle_NavierStokes_Joukowski_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DCGStabilized_Triangle_NavierStokes_Joukowski )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;

  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeNavierStokesArtificialViscosity, TraitsModelNavierStokesClass, PDENavierStokes2D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeNavierStokesArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeNavierStokesArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_PressureGrad<TraitsSizeNavierStokesArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeNavierStokesArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCNavierStokesmitAVDiffusion2DVector<TraitsSizeNavierStokesArtificialViscosity, TraitsModelAV> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputNavierStokes2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
#if 0
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#endif
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_DG ParamBuilderType;
  typedef GenHField_DG<PhysD2, TopoD2> GenHFieldType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {5e-7, 5e-7};

  RoeEntropyFix entropyFix = eVanLeer;
  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = false;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const Real tRef = 1.0;          // temperature
  const Real pRef = 1.0;          // pressure
  const Real lRef = 1;            // length scale

  const Real Mach = 0.75;
  const Real Reynolds = 1e5;
  const Real Prandtl = 0.72;

  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;

//  const Real lRef = 1;                            // length scale
  const Real rhoRef = 1;                            // density scale
//  const Real qRef = 0.1;                          // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  // Entropy, Stagnation Enthalpy, Tangential velocity
  const Real sSpec = log( pRef / pow(rhoRef,gamma) );
  const Real HSpec = gas.enthalpy(rhoRef, tRef) + 0.5*(uRef*uRef+vRef*vRef);
  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_mitState;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.sSpec] = sSpec;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.HSpec] = HSpec;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VxSpec] = uRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VySpec] = vRef;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  const Real p_ratio = 1.0;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = p_ratio*pRef;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;
  PyBCList["BCWall"] = BCWall;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCWall"] = {0};
  BCBoundaryGroups["BCOut"] = {2};
  BCBoundaryGroups["BCIn"] = {1};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);


  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
if (world.rank() == 0 )
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  NewtonSolverParam::checkInputs(NewtonSolverDict);
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);
  //
  int ordermin = 1;
  int ordermax = 3;

  std::string filename_base = "tmp/LaminarJoukowski/";

  if (world.rank() == 0)
  {
    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directories(filename_base);
  }

  fstream foutputhist;
  std::ofstream resultFile;
  if (world.rank() == 0)
  {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Create tecplot file to put total enthalpy errors
    ////////////////////////////////////////////////////////////////////////////////////////
    resultFile.open( filename_base + "DragCGNavierStokes.plt", fstream::out );
    resultFile << "VARIABLES=";
    resultFile << "\"DOF\"";
    resultFile << ", \"1/sqrt(DOF)\"";
    resultFile << ", \"L2 error\"";
    resultFile << std::endl;
    resultFile << std::setprecision(16) << std::scientific;
    ////////////////////////////////////////////////////////////////////////////////////////
  }

  world.barrier();

  for (int order = ordermin; order <= ordermax; order++)
  {
    ////////////////////////////////////////////////////////////////////////////////////////
    // Tecplot zone for this order
    ////////////////////////////////////////////////////////////////////////////////////////
    if (world.rank() == 0)
    {
      resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    }
    ////////////////////////////////////////////////////////////////////////////////////////

    PDEBaseClass pdeNavierStokesAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                                   gas, visc, tcond, interp, entropyFix);
    // Sensor equation terms
    Sensor sensor(pdeNavierStokesAV);
    SensorAdvectiveFlux sensor_adv(0.0, 0.0);
    SensorViscousFlux sensor_visc(order);
    SensorSource sensor_source(order, sensor);
    // AV PDE with sensor equation
    NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order,
                   hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, interp, entropyFix);

    // initial condition
    ArrayQ q0 = pde.setDOFFrom( AVVariable<DensityVelocityPressure2D, Real>({{rhoRef, uRef, vRef, pRef}, 0.0}) );

    StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby);

#ifndef BOUNDARYOUTPUT
    // Entropy output
    NDOutputClass fcnOutput(pde, 1.0);
#if 0
    OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
    OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, stab);
#endif
#else
    // Drag output
    NDOutputClass outputFcn(pde, 1., 0.);
    OutputIntegrandClass outputIntegrand( outputFcn, {0} );
#endif

    stab.setStabOrder(order);
    stab.setNitscheOrder(order);

    for (int grid_index = 0; grid_index <= 2; grid_index++)
    {
      std::string meshName = "grids/LaminarJoukowski/Joukowski_Laminar_Challenge_tri_ref"
                            + std::to_string(grid_index)
                            + "_Q2.msh";

      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField_gmsh<PhysD2, TopoD2>( world, meshName ) );
      std::shared_ptr<GenHFieldType> phfld = std::make_shared<GenHFieldType>(*pxfld);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pxfld), pde, order, order+1,
                                                   BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                   active_boundaries, stab);

      const int quadOrder = 3*(order + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrand);

      pGlobalSol->setSolution(q0);

      pInterface->solveGlobalPrimalProblem();

      // Monitor Drag Error
      Real drag = pInterface->getOutput();


#ifdef SANS_MPI
      int nDOFtotal = 0;
      boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );
#else
      int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
#endif
      if (world.rank() == 0)
        std::cout << "P = " << order << " nDOF = " << nDOFtotal
                  << " : Drag = " << std::setprecision(16) << drag << endl;

      if (world.rank() == 0)
      {
        ////////////////////////////////////////////////////////////////////////////////////////
        // Output Error to tecplot file
        ////////////////////////////////////////////////////////////////////////////////////////
        resultFile << " " << nDOFtotal;
        resultFile << " " << 1.0/sqrt(nDOFtotal);
        resultFile << " " << drag;
        resultFile << std::endl;
        ////////////////////////////////////////////////////////////////////////////////////////
      }

#if 1
      // Tecplot dump grid
      std::string qfld_filename = filename_base + "slnCG_NavierStokesBump_P";
      qfld_filename += to_string(order);
      qfld_filename += "_G";
      qfld_filename += to_string(grid_index);
      qfld_filename += ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
#endif
    }

  }

}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
