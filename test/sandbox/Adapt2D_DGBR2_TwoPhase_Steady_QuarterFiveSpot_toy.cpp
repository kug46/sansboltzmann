// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_TwoPhase_Steady_QuarterFiveSpot_btest
// Testing of the MOESS framework on a space-time two-phase problem

#define SINGLE_RUN 1
#define FIXED_BHP 0

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/Q2DPrimitive_LogpnSw.h"
#include "pde/PorousMedia/PDETwoPhase2D.h"
#include "pde/PorousMedia/BCTwoPhase2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/SolutionFunction_TwoPhase1D.h"
#include "pde/PorousMedia/OutputTwoPhase.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_Lagrange_X1.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_TwoPhase_Steady_QuarterFiveSpot_test_suite )

/* IMPORTANT:
 * If running a single phase case, make sure to turn on the "additional viscosity" term in PDETwoPhase */

#if !SINGLE_RUN
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_TwoPhase_Steady_QuarterFiveSpot_Triangle )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;
  typedef Q2D<QType, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTwoPhase2DVector<PDEClass> BCVector;
  typedef BCTwoPhase2D<BCTypeFullState, PDEClass> BCClassFullState;
  typedef BCTwoPhase2D<BCTypeFunction_mitState, PDEClass> BCClassSolution;

  typedef BCParameters<BCVector> BCParams;

#if FIXED_BHP
  typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;
  typedef SourceTwoPhase2DType_FixedPressureOutflow_Param SourceParamClass;
#else
  typedef SourceTwoPhase2D_FixedOutflowRate<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;
  typedef SourceTwoPhase2DType_FixedOutflowRate_Param SourceParamClass;
#endif

//  typedef OutputTwoPhase2D_Flowrate<SourceClass> OutputClass;
  typedef OutputTwoPhase2D_BottomHolePressure<SourceClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  timer clock;

  mpi::communicator world;

  const Real T = 50.0; //days
  const Real Lx = 1000.0; //ft
  const Real Ly = Lx; //ft

  int ii = 11;
  int jj = 11;
//  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_UnionJack_Triangle_X1>( ii, jj, -Lx, Lx, -Ly, Ly );
//  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, jj, -Lx, Lx, -Ly, Ly, true );
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj, -Lx, Lx, -Ly, Ly );
//  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField_libMeshb<PhysD2, TopoD2>>( world, "tmp/poly_m6_R100_P1_10k_a20.mesh" );

  const int iB = XField2D_Box_Triangle_Lagrange_X1::iBottom;
  const int iR = XField2D_Box_Triangle_Lagrange_X1::iRight;
  const int iT = XField2D_Box_Triangle_Lagrange_X1::iTop;
  const int iL = XField2D_Box_Triangle_Lagrange_X1::iLeft;

  int order = 1;

  // PDE
  const Real pref = 14.7;

  const bool is_comp = 0;
  DensityModel rhow(62.4, is_comp*5.0e-6, pref);
  DensityModel rhon(52.1, is_comp*1.5e-5, pref);

  PorosityModel phi(0.3, is_comp*3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200;
  const Real Kxx = Kref;
  const Real Kyy = 0.5*Kref;

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kxx, 0.0}, {0.0, Kyy}};
  RockPermModel K(Kref_mat);

  CapillaryModel pc(0.0);

  Real scalar_visc = 1.0; //need extra viscosity to stabilize two-phase equations for single-phase flow

  const Real Q_outflow = 1000; //[ft^2/day]
  const Real R_bore = 1.0/6.0; //ft
  const Real R_well = 100.0; //ft
  const Real pB = 2000.0; //psi
  const int nParam = 6;

  if (world.rank() == 0)
  {
    std::cout << "rw: " << R_bore << " ft" << std::endl;
    std::cout << "R: " << R_well << " ft" << std::endl;
    std::cout << "pB: " << pB << " psi" << std::endl;
    std::cout << "nParam: " << nParam << std::endl;
  }

  PyDict well_out;
#if FIXED_BHP
  well_out[SourceParamClass::params.pB] = pB;
  well_out[SourceParamClass::params.Rwellbore] = R_bore;
  well_out[SourceParamClass::params.nParam] = nParam;
  well_out[SourceParamClass::params.WellModel] = SourceParamClass::params.WellModel.Polynomial;
  well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;
#else
  well_out[SourceParamClass::params.Q] = Q_outflow;
  well_out[SourceParamClass::params.nParam] = nParam;
  well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedOutflowRate;
#endif

  PyDict source_production;
  source_production[SourceTwoPhase2DParam::params.Source] = well_out;
  source_production[SourceTwoPhase2DParam::params.xcentroid] = 0.0;
  source_production[SourceTwoPhase2DParam::params.ycentroid] = 0.0;
  source_production[SourceTwoPhase2DParam::params.R] = R_well;
  source_production[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_production[SourceTwoPhase2DParam::params.Tmax] = T;
  source_production[SourceTwoPhase2DParam::params.smoothLr] = 0.0;
  source_production[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_production[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_list;
  source_list["production_well"] = source_production;

  SourceTwoPhase2DListParam::checkInputs(source_list);

  NDPDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list, scalar_visc);

  // Initial solution
  Real Sw_init = 1.0;
  ArrayQ qinit;
  PressureNonWet_SaturationWet<Real> qdata(2500.0, Sw_init);
  pde.setDOFFrom( qinit, qdata );

  // BCs
  PyDict BCState1;
  BCState1[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  BCState1[PressureNonWet_SaturationWet_Params::params.pn] = 3000.0;
  BCState1[PressureNonWet_SaturationWet_Params::params.Sw] = Sw_init;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClassFullState::ParamsType::params.StateVector] = BCState1;

  typedef SolutionFunction_TwoPhase2D_SingleWell_Peaceman<QInterpreter, TraitsSizeTwoPhase> SolutionType;
  PyDict SolutionPeacemanWell;
  SolutionPeacemanWell[BCClassSolution::ParamsType::params.Function.Name] = BCClassSolution::ParamsType::params.Function.SingleWell_Peaceman;
  SolutionPeacemanWell[SolutionType::ParamsType::params.pB] = pB;
  SolutionPeacemanWell[SolutionType::ParamsType::params.Rwellbore] = R_bore;
  SolutionPeacemanWell[SolutionType::ParamsType::params.Sw] = Sw_init;
  SolutionPeacemanWell[SolutionType::ParamsType::params.mu] = 1.0;
  SolutionPeacemanWell[SolutionType::ParamsType::params.Q] = -Q_outflow;
  SolutionPeacemanWell[SolutionType::ParamsType::params.xcentroid] = 0.0;
  SolutionPeacemanWell[SolutionType::ParamsType::params.ycentroid] = 0.0;
  SolutionPeacemanWell[SolutionType::ParamsType::params.Kxx] = Kxx;
  SolutionPeacemanWell[SolutionType::ParamsType::params.Kyy] = Kyy;

  PyDict BCSolution;
  BCSolution[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSolution[BCClassSolution::ParamsType::params.Function] = SolutionPeacemanWell;

  PyDict PyBCList;
  PyBCList["BCSolution"] = BCSolution;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSolution"] = {iB,iR,iT,iL};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-8, 1e-8};

  //Output functional
//  NDOutputClass fcnOutput(pde);
  bool computeMassFlow = false;
  SourceClass source(source_production, rhow, rhon, krw, krn, muw, mun, K, pc, computeMassFlow);
//  int phase = 0;
//  std::vector<SourceClass> sourceList = { source };
//  NDOutputClass fcnOutput(sourceList, phase);
  NDOutputClass fcnOutput(source, R_bore);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict, UMFPACKDict;

#if defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)/2; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)/2; //elemDOF for p=order+1
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

  if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
#else
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  if (world.rank() == 0) std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  //--------ADAPTATION LOOP--------

  int maxIter = 20;
  Real targetCost = 10000;

  std::string filename_base = "tmp/";
//  std::string filename_base = "tmp/twophase_steady/Q1000/L1000_Kx200_Ky100/polynomial_d2fnonzero/adapted/poly_m"
//                              + to_string(nParam) + "_R" + to_string((int)R_well) + "_P" + to_string(order) + "_"
//                              + to_string((int) targetCost/1000) + "k/";

  fstream fadapthist;
  if (world.rank() == 0)
  {
    std::string adapthist_filename = filename_base + "test.adapthist";
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = true;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  const int quadOrder = 2*(order + 2);

  //Set initial solution
//  pGlobalSol->setSolution(NDinitSolution, cellGroups);
  pGlobalSol->setSolution(qinit);

  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

#if 0
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  QuadratureOrder quadrule(*pxfld, quadOrder);

  PrimalEquationSetClass AlgEqSet(pGlobalSol->paramfld, pGlobalSol->primal, pGlobalSol->pliftedQuantityfld, pde, quadrule,
                                  ResNormType, tol, cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups);

  SystemVectorClass q(AlgEqSet.vectorStateSize());
  AlgEqSet.fillSystemVector(q);

  // jacobian nonzero pattern
  SystemNonZeroPattern nz(AlgEqSet.matrixSize());
  AlgEqSet.jacobian(q, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  jac = 0;
  AlgEqSet.jacobian(q, jac);

  fstream fout( "tmp/jac.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

  for (int iter = 0; iter < maxIter + 1; iter++)
  {
    if (world.rank() == 0)
      std::cout << "\n" << std::string(25,'-') + "Adaptation Iteration " << iter << std::string(25,'-') << "\n\n";

    //Create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, AdjLinearSolverDict,
                                                        outputIntegrand);
    pInterface->setBaseFilePath(filename_base);

    pInterface->solveGlobalPrimalProblem();
    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    if (world.rank() == 0)
      std::cout << "Output: " << std::scientific << std::setprecision(12) << pInterface->getOutput() << std::endl;

    pInterface->solveGlobalAdjointProblem();
    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;

    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    if (world.rank() == 0)
      std::cout<<"Time elapsed: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl << std::endl;
  }

  if (world.rank() == 0)
    fadapthist.close();
}

#else

/* IMPORTANT:
 * If running a single phase case, make sure to turn on the "additional viscosity" term in PDETwoPhase */

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_TwoPhase_Steady_QuarterFiveSpot_Triangle )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;
  typedef Q2D<QType, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTwoPhase2DVector<PDEClass> BCVector;
  typedef BCTwoPhase2D<BCTypeFullState, PDEClass> BCClassFullState;
  typedef BCTwoPhase2D<BCTypeFunction_mitState, PDEClass> BCClassSolution;
  typedef BCParameters<BCVector> BCParams;

#if FIXED_BHP
  typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;
#else
  typedef SourceTwoPhase2D_FixedOutflowRate<TraitsSizeTwoPhase, TraitsModelClass> SourceClass;
#endif

  typedef SourceClass::ParamsType SourceParamClass;

  typedef OutputTwoPhase2D_Flowrate<SourceClass> OutputClass;
//  typedef OutputTwoPhase2D_BottomHolePressure<SourceClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  const Real T = 50.0; //days

  const Real Lx = 1000.0; //ft
  const Real Ly = Lx; //ft

  const int iB = XField2D_Box_Quad_Lagrange_X1::iBottom;
  const int iR = XField2D_Box_Quad_Lagrange_X1::iRight;
  const int iT = XField2D_Box_Quad_Lagrange_X1::iTop;
  const int iL = XField2D_Box_Quad_Lagrange_X1::iLeft;

  int order = 1;

  // PDE
  const Real pref = 14.7;

  const bool is_comp = 0;
  DensityModel rhow(62.4, is_comp*5.0e-6, pref);
  DensityModel rhon(52.1, is_comp*1.5e-5, pref);

  PorosityModel phi(0.3, is_comp*3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200;
  const Real Kxx = Kref;
  const Real Kyy = 0.5*Kref;

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kxx, 0.0}, {0.0, Kyy}};
  RockPermModel K(Kref_mat);

  CapillaryModel pc(0.0);

  Real scalar_visc = 1.0; //need extra viscosity to stabilize two-phase equations for single-phase flow

#if 1
  const Real Q_outflow = 1000; //[ft^2/day]

  // Set up source term PyDicts
  const Real R_bore = 1.0/6.0; //ft
  const Real R_well = 100.0; //ft
  const Real pB = 2000.0; //psi
  const int nParam = 6;

  if (world.rank() == 0)
  {
    std::cout << "rw: " << R_bore << " ft" << std::endl;
    std::cout << "R: " << R_well << " ft" << std::endl;
    std::cout << "pB: " << pB << " psi" << std::endl;
    std::cout << "nParam: " << nParam << std::endl;
  }

  PyDict well_out;
#if FIXED_BHP
  well_out[SourceParamClass::params.pB] = pB;
  well_out[SourceParamClass::params.Rwellbore] = R_bore;
  well_out[SourceParamClass::params.nParam] = nParam;
  well_out[SourceParamClass::params.WellModel] = SourceParamClass::params.WellModel.Polynomial;
  well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;
#else
  well_out[SourceParamClass::params.Q] = Q_outflow;
  well_out[SourceParamClass::params.nParam] = nParam;
  well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedOutflowRate;
#endif

  PyDict source_production;
  source_production[SourceTwoPhase2DParam::params.Source] = well_out;
  source_production[SourceTwoPhase2DParam::params.xcentroid] = 0.0;
  source_production[SourceTwoPhase2DParam::params.ycentroid] = 0.0;
  source_production[SourceTwoPhase2DParam::params.R] = R_well;
  source_production[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_production[SourceTwoPhase2DParam::params.Tmax] = T;
  source_production[SourceTwoPhase2DParam::params.smoothLr] = 0.0;
  source_production[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_production[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_list;
  source_list["production_well"] = source_production;

  SourceTwoPhase2DListParam::checkInputs(source_list);
#endif

  NDPDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list, scalar_visc);

  // Initial solution
  Real Sw_init = 1.0;
  ArrayQ qinit;
  PressureNonWet_SaturationWet<Real> qdata(2500.0, Sw_init);
  pde.setDOFFrom( qinit, qdata );

  // BCs
  PyDict BCState1;
  BCState1[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  BCState1[PressureNonWet_SaturationWet_Params::params.pn] = 3000.0;
  BCState1[PressureNonWet_SaturationWet_Params::params.Sw] = Sw_init;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClassFullState::ParamsType::params.StateVector] = BCState1;

  typedef SolutionFunction_TwoPhase2D_SingleWell_Peaceman<QInterpreter, TraitsSizeTwoPhase> SolutionType;
  PyDict SolutionPeacemanWell;
  SolutionPeacemanWell[BCClassSolution::ParamsType::params.Function.Name] = BCClassSolution::ParamsType::params.Function.SingleWell_Peaceman;
  SolutionPeacemanWell[SolutionType::ParamsType::params.pB] = pB;
  SolutionPeacemanWell[SolutionType::ParamsType::params.Rwellbore] = R_bore;
  SolutionPeacemanWell[SolutionType::ParamsType::params.Sw] = Sw_init;
  SolutionPeacemanWell[SolutionType::ParamsType::params.mu] = 1.0;
  SolutionPeacemanWell[SolutionType::ParamsType::params.Q] = -Q_outflow;
  SolutionPeacemanWell[SolutionType::ParamsType::params.xcentroid] = 0.0;
  SolutionPeacemanWell[SolutionType::ParamsType::params.ycentroid] = 0.0;
  SolutionPeacemanWell[SolutionType::ParamsType::params.Kxx] = Kxx;
  SolutionPeacemanWell[SolutionType::ParamsType::params.Kyy] = Kyy;

  PyDict BCSolution;
  BCSolution[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSolution[BCClassSolution::ParamsType::params.Function] = SolutionPeacemanWell;

  PyDict PyBCList;
  PyBCList["BCSolution"] = BCSolution;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSolution"] = {iB,iR,iT,iL};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {5e-10, 5e-10};

  //Output functional
//  NDOutputClass fcnOutput(pde);
  bool computeMassFlow = false;
  SourceClass source(source_production, rhow, rhon, krw, krn, muw, mun, K, pc, computeMassFlow);
  std::vector<SourceClass> sourceList = { source };
  int phase = 0;
  NDOutputClass fcnOutput(sourceList, phase);
//  NDOutputClass fcnOutput(source, R_bore);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict, UMFPACKDict;

#if defined(SANS_PETSC)
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PETScDict_adjoint = PETScDict;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

  if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
#else
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  if (world.rank() == 0) std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

#if 1
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                    = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  //--------SOLVE LOOP--------

  std::string filename_base = "tmp/";
//  std::string filename_base = "tmp/twophase_steady/Q1000/L1000_Kx200_Ky100/polynomial_d2fnonzero/structured/poly_m" + to_string(nParam) + "_R"
//                              + to_string((int)R_well) + "_P" + to_string(order) + "/";

  for (int m = 0; m < 5; m++)
  {
    timer clock;

    int ii = 20 * pow(2,m) + 1;
    int jj = ii;
  //  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_Box_UnionJack_Triangle_X1( ii, jj, -Lx, Lx, -Ly, Ly ) );
  //  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_Box_Triangle_X1( ii, jj, -Lx, Lx, -Ly, Ly, true ) );
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_Box_Quad_Lagrange_X1( world, ii, jj, -Lx, Lx, -Ly, Ly ) );

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Solution data
    typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

    std::shared_ptr<SolutionClass> pGlobalSol;
    pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order,
                                                 BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                 active_boundaries, disc);

    const int quadOrder = 2*(order + 2);

    //Create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict, AdjLinearSolverDict,
                                                        outputIntegrand);

    //Set initial solution
    pGlobalSol->setSolution(qinit);

    std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();
    std::string qfld_filename = filename_base + "qfld_P" + to_string(order) + "_" + to_string(ii) + "x" + to_string(jj) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

#if FIXED_BHP
    double pB_output = pB;
#else
    typedef OutputTwoPhase2D_BottomHolePressure<SourceClass> OutputClassBHP;
    typedef OutputNDConvertSpace<PhysD2, OutputClassBHP> NDOutputClassBHP;
    typedef IntegrandCell_DGBR2_Output<NDOutputClassBHP> OutputIntegrandClassBHP;

    NDOutputClassBHP fcnOutputBHP(source, R_bore);
    OutputIntegrandClassBHP bhpIntegrand(fcnOutputBHP, {0});

    // evaluate BHP output function
    OutputClassBHP::ArrayJ<Real> bhp_integrals = 0.0;
    QuadratureOrder quadrule(*pxfld, quadOrder);
    IntegrateCellGroups<TopoD2>::integrate( FunctionalCell_DGBR2( bhpIntegrand, bhp_integrals ),
                                            pGlobalSol->paramfld, (pGlobalSol->primal.qfld, pGlobalSol->primal.rfld),
                                            quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#ifdef SANS_MPI
    bhp_integrals = boost::mpi::all_reduce( *get<-1>(*pxfld).comm(), bhp_integrals, std::plus<OutputClassBHP::ArrayJ<Real>>() );
#endif

    double pB_output = (-Q_outflow + bhp_integrals[0]) / bhp_integrals[1];
#endif

    if (world.rank() == 0)
    {
      std::cout << std::scientific << std::setprecision(12);
      std::cout << ii << ", " << jj << ", " << pInterface->getOutput() << ", " << pB_output << ", " << clock.elapsed() << std::endl;
    }
  }

}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
