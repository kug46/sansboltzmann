// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// SolveD2Q16_DGAdvective_Line_Boltzmann_Diffusion_toy


#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsBoltzmannD2Q16.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/QD2Q16PrimitiveDistributionFunctions.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/BCBoltzmannD2Q16.h"
//#include "pde/NS/OutputBoltzmannD2Q16.h"
#include "pde/NS/SolutionFunction_BoltzmannD2Q16.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
//#include "pde/AnalyticFunction/SSMEInletConditions.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"

#include "NonLinearSolver/NewtonSolver.h"
//#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
//#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "Field/XFieldArea.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/output_Tecplot.h"
//#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/output_Tecplot.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"
//#define FIELDAREA_DG_CELL_INSTANTIATE
//#include "Field/FieldArea_DG_Cell_impl.h"

#include "tools/output_std_vector.h"

using namespace std;
using namespace SANS;

namespace SANS
{
//template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
//template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveDistributionFunctions>      { static std::string str() { return "PrimitiveVariables";} };
//template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

//############################################################################//
BOOST_AUTO_TEST_SUITE(SolveD2Q16_DGAdvective_Triangle_Boltzmann_CouetteFlow_test_suite )

//------------------------------------------------------------------------------
BOOST_AUTO_TEST_CASE( SolveD2Q16_DGAdvective_Triangle_Boltzmann_CouetteFlow_Diffusion )
{

  //--------------------------------------------------------------------------------------
  // MPI comm world
  //--------------------------------------------------------------------------------------
  mpi::communicator world;
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Typedefs for Euler2D
  //--------------------------------------------------------------------------------------
  typedef QTypePrimitiveDistributionFunctions QType;
  typedef TraitsModelBoltzmannD2Q16<QType, GasModel> TraitsModelBoltzmannD2Q16Class;
  typedef PDEBoltzmannD2Q16<TraitsSizeBoltzmannD2Q16, TraitsModelBoltzmannD2Q16Class> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  //--------------------------------------------------------------------------------------
  //RK:
  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> RKClass;
  //--------------------------------------------------------------------------------------
  // Typedefs for discretization
  //--------------------------------------------------------------------------------------
  typedef BCBoltzmannD2Q16Vector<TraitsSizeBoltzmannD2Q16, TraitsModelBoltzmannD2Q16Class> BCVector;
  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PDEPrimalEquationSetClass;
  // typedef AlgebraicEquationSet<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > AlgebraicEquationSet_Class;

  typedef PDEPrimalEquationSetClass::BCParams BCParams;

  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Solution order
  //--------------------------------------------------------------------------------------
  int order_pde = 1;
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Create our Euler PDE
  //--------------------------------------------------------------------------------------
  // Gas Model
  Real dt = 0.005;
  Real dt_fin = dt;
  Real tFinal = 50;
//  int nSteps = 100;
  int nSteps_fin = (tFinal) / dt_fin;
  Real H = 1.0;
  Real L = 1.0;

  Real gamma = 1.4;
  Real R = 1.;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  Real Kn = 1.0;
  std::cout<<"Setting Kn = " << Kn <<". and dt = "<<dt_fin<<". Accept?";
  char dummy;
  std::cin >> dummy;
  Real csq = 1.;
  Real viscosity = sqrt(csq)*H*Kn*sqrt(2/PI);
  Real tau = viscosity/csq;
  Real Uadv = 0.;
  Real Vadv = 0.;
  NDPDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                 PDEClass::BGK, tau,
                 PDEClass::Hydrodynamics, Uadv, Vadv );

  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Define boundary conditions
  //--------------------------------------------------------------------------------------
  // BC

  PyDict BCTop;
  BCTop[BCParams::params.BC.BCType] = BCParams::params.BC.DiffuseKinetic;
  BCTop[BCBoltzmannD2Q16Params<BCTypeDiffuseKinetic>::params.Rhow] = -1;
  BCTop[BCBoltzmannD2Q16Params<BCTypeDiffuseKinetic>::params.Uw] = 0.1;
  BCTop[BCBoltzmannD2Q16Params<BCTypeDiffuseKinetic>::params.Vw] = 0.0;

  PyDict BCBottom;
  BCBottom[BCParams::params.BC.BCType] = BCParams::params.BC.DiffuseKinetic;
  BCBottom[BCBoltzmannD2Q16Params<BCTypeDiffuseKinetic>::params.Rhow] = -1;
  BCBottom[BCBoltzmannD2Q16Params<BCTypeDiffuseKinetic>::params.Uw] = -0.0;
  BCBottom[BCBoltzmannD2Q16Params<BCTypeDiffuseKinetic>::params.Vw] = 0.0;


  PyDict PyBCList;
  PyBCList["BCTop"] = BCTop;
  PyBCList["BCBottom"] = BCBottom;

  BCParams::checkInputs( PyBCList );

  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["BCTop"] = { XField2D_Box_Triangle_Lagrange_X1::iTop };
  BCBoundaryGroups["BCBottom"] = { XField2D_Box_Triangle_Lagrange_X1::iBottom };
  //PyDict PyBCList;
  //std::map<std::string, std::vector<int>> BCBoundaryGroups = {};
  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);


  GlobalTime time;
  time = 0.;

  //--------------------------------------------------------------------------------------
  // Set initial conditions
  //--------------------------------------------------------------------------------------
#if 0
  Real max_density = 500.;
  Real x_spread = 10000, y_spread = x_spread;
  typedef SolutionFunction_BoltzmannD2Q16_Gaussian<TraitsSizeBoltzmannD2Q16, TraitsModelBoltzmannD2Q16Class> SolutionClass;
  typedef SolnNDConvertSpace<PhysD2, SolutionClass> SolutionNDClass;
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.A] = max_density;
  solnArgs[SolutionClass::ParamsType::params.sigma_x] = x_spread;
  solnArgs[SolutionClass::ParamsType::params.sigma_y] = y_spread;
  SolutionNDClass solnExact(solnArgs);

#elif 1
  Real fix_density = 1.000;
  typedef SolutionFunction_BoltzmannD2Q16_WeightedDensity<TraitsSizeBoltzmannD2Q16, TraitsModelBoltzmannD2Q16Class> SolutionClass;
  typedef SolnNDConvertSpace<PhysD2, SolutionClass> SolutionNDClass;
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.Rho] = fix_density;
  SolutionNDClass solnExact(solnArgs);
#endif

for (int i=20; i<=20; i=i*2)
{
  //--------------------------------------------------------------------------------------
  // Create grid
  //--------------------------------------------------------------------------------------
  int ii = i;
  int jj = i;

  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj, 0., L, 0., H, {{ true, false }} );

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate our Euler PDE field
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Lagrange);
  FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> pde_rfld(xfld, order_pde, BasisFunctionCategory_Lagrange);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> pde_lgfld(xfld, order_pde, BasisFunctionCategory_Lagrange,
                                                           active_boundaries);// BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups));
  for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );
  //pde_qfld = 100;
  pde_lgfld = 0;

  //--------------------------------------------------------------------------------------
  // Output IC gnuplot file
  //--------------------------------------------------------------------------------------
   string filenameStem_IC = "tmp/solnDG_BoltzmannD2Q16_step_0.plt";
   output_Tecplot( pde_qfld, filenameStem_IC );
  //--------------------------------------------------------------------------------------
  //--------------------------------------------------------------------------------------
  // AES Settings
  //--------------------------------------------------------------------------------------
  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = { 1e-7, 1e-7 };

  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Create our AES to solve our PDE
  //--------------------------------------------------------------------------------------
  std::vector<int> interiorTraceGroups = linspace(0,xfld.nInteriorTraceGroups()-1);


  PDEPrimalEquationSetClass PrincipalEqSet(xfld, pde_qfld, pde_lgfld, pde, quadratureOrder, ResidualNorm_Default, tol, {0},
                                           interiorTraceGroups, PyBCList, BCBoundaryGroups, time);
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Construct Newton Solver
  //--------------------------------------------------------------------------------------

  // Set up Newton Solver
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict, LinSolverDict;
#ifdef SANS_PETSC
  // Take the Preconditioners from my Tandem Sphere case and just copy paste them here

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  LinSolverDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
  LinSolverDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  LinSolverDict[SLA::PETScSolverParam::params.MaxIterations] = 100;
  LinSolverDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  LinSolverDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  LinSolverDict[SLA::PETScSolverParam::params.Verbose] = true;
  LinSolverDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  LinSolverDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  std::cout << "Linear solver: PETSc" << std::endl;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  NewtonSolverParam::checkInputs( NewtonSolverDict );
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#else

  LinSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  std::cout << "Linear solver: UMFPACK" << std::endl;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-11;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  //AdjSolverDict[NewtonSolverParam::params.LinearSolver] = LinSolverDict;

  // Check inputs
  NewtonSolverParam::checkInputs( NewtonSolverDict );
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

#endif
  //--------------------------------------------------------------------------------------

  int RKorder = 4;
  int RKtype = 0;
  int RKstages = RKorder;


  //temporal discretiation
  RKClass RK(RKorder, RKstages, RKtype, dt, time, xfld, pde_qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, PrincipalEqSet);

  //--------------------------------------------------------------------------------------

  for (int step = 1; step <= nSteps_fin; step++ )//int(nSteps_fin/10); step++)
  {
    std::cout << "############################# Time Step: " << step << "\n time is " << time << std::endl;
    // Advance solution
    RK.march(int(1));
    if (step%100 == 0)
    {
      // Tecplot Output
      //int FieldOrder = order_pde;
      string filename = "tmp/solnDG_BoltzmannD2Q16_Kn_";
      filename += to_string(Kn);
      filename += "_Order_";
      int f_order = order_pde;
      filename += to_string(f_order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += "step_";
      filename += to_string(step);
      filename += ".plt";
       output_Tecplot( pde_qfld, filename );
    }
  }

}


}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
