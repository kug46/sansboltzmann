// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_CGStabilized_Triangle_NavierStokes_OjedaMMS_btest
// testing of 2-D CG Stabilized for Navier Stokes

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/FunctionalCell_VMSD.h"
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"
#include "Field/ProjectSoln/InterpolateFunctionCell_Continuous.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes_Brenner2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/BCParameters.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "Adaptation/MOESS/SolverInterface_VMSD.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "tools/timer.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_VMSD_Triangle_NavierStokes_OjedaMMS_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_VMSD_Triangle_Ojeda )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypeEntropy QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;

  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef SolutionFunction_NavierStokes2D_Hartmann<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolnType;
  typedef SolutionFunction_NavierStokes2D_OjedaMMS<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolnType;
  typedef SolnNDConvertSpace<PhysD2,SolnType> NDSolnType;
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;

  typedef OutputEuler2D_TotalEnthalpy<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_VMSD_Output<NDOutputClass, NDPDEClass> IntegrandOutputClass;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_VMSD< NDPDEClass, BCNDConvertSpace, BCVector, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef ParamType_None ParamBuilderType;
  typedef SolutionData_VMSD<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass, IntegrandOutputClass> SolverInterfaceClass;

  mpi::communicator world;

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  GasModel gas(gamma, R);
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // INFLOW CONDITIONS TO MATCH OJEDA'S MMS
  Real rho0 = 1.0;
  Real u0 = 0.2;
  Real v0 = 0.1;
  Real p0 = 1.0;
  Real t0 = p0/rho0/R;

  const Real ERef = Cv*t0 + 0.5*(u0*u0+v0*v0);

//  const Real Re = 1000;
  const Real muRef = 0.02; // Re_L = 10ish
  const Real Prandtl = 0.72;

  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDSolnType soln(gas);
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(soln) );


  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum, eVanLeer, forcingptr );

  //  Total enthalpy integral
  NDOutputClass fcnOutput(pde);

  // Stabilization and outputs
  DiscretizationVMSD stab;
  enum ResidualNormType ResNormType = ResidualNorm_L2;

  IntegrandOutputClass fcnErr(pde, fcnOutput, {0} );

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolnType> L2ErrorClass;


  typedef OutputNDConvertSpace<PhysD2, L2ErrorClass> NDL2ErrorClass;
  typedef IntegrandCell_VMSD_Output<NDL2ErrorClass, NDPDEClass> L2ErrorIntegrandClass;

  NDL2ErrorClass fcnL2Err(soln);
  L2ErrorIntegrandClass errorL2Integrand(pde, fcnL2Err, {0}, true);

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rho0;
  StateVector[Conservative2DParams::params.rhou] = rho0*u0;
  StateVector[Conservative2DParams::params.rhov] = rho0*v0;

  StateVector[Conservative2DParams::params.rhoE] = rho0*ERef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;
  BCBoundaryGroups["BCIn"]  = {0,1,2,3};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};
  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

#if (defined(SANS_PETSC) && 1)
  if (world.rank() == 0 ) std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

//  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name]
//                    = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name]
                    = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide]
                    = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 3;
//  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order
//
  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
//
  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-20;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 500;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;
//
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
//
//  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
//  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
////  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 12000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 500;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
//
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;
//
//#elif defined(INTEL_MKL)
//
//  std::cout << "Using MKL\n";
//  PyDict MKL_PARDISODict;
//  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
//  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
//  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
 PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 10;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  string filename_base = "tmp/NS2D/VMSD/";
  fstream foutsol;

  int ordermin = 1;
  int ordermax = 2;

  boost::filesystem::path base_dir(filename_base);
  if ( not boost::filesystem::exists(base_dir) )
    boost::filesystem::create_directories(base_dir);

  for (int order = ordermin; order <= ordermax; order++)
  {
    stab.setNitscheOrder(order);

    filename_base = "tmp/NS2D/VMSD/VMSD_";
    filename_base += "P" + std::to_string(order) + "_";

    // loop over grid resolution7: 2^power
    int ii, jj;

    int powermin = 2;
    int powermax = 7;
    for (int power = powermin; power <= powermax; power++)
    {
      std::string output_filename = filename_base + "output.dat";
      if ( world.rank() == 0 )
      {
        foutsol.open( output_filename, fstream::app );
        BOOST_REQUIRE_MESSAGE(foutsol.good(), "Error opening file: " + output_filename);
      }

      jj = pow( 2, power )+1;
      ii = jj; //small grid!

      string filename = filename_base + "_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);

      // grid:
      XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );
      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<SolutionClass> pGlobalSol;

      pGlobalSol = std::make_shared<SolutionClass>(xfld, pde, stab, order, order, order+1, order+1,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   BasisFunctionCategory_Lagrange,
                                                   active_boundaries);


      std::vector<Real> tol = {1e-10, 1e-10, 1e-10};
      const int quadOrder = 2*(order + 1);
      QuadratureOrder quadratureOrder( xfld, quadOrder );

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          fcnErr);

//       Set the initial condition
      for_each_CellGroup<TopoD2>::apply( InterpolateFunctionCell_Continuous(soln, {0}), (xfld, pGlobalSol->primal.qfld) );
      pGlobalSol->primal.qpfld = 0;

      timer clock;
      pInterface->solveGlobalPrimalProblem();
      Real solveTime = clock.elapsed();
      pInterface->solveGlobalAdjointProblem();
      Real output = pInterface->getOutput();
//
      pInterface->computeErrorEstimates();
      Real outputErrEstimate = pInterface->getGlobalErrorEstimate();
//
//      //compute output error
      Real HExact = 3.5287871306206915036;
      Real outputErr = fabs(HExact - output);
//
//      Real errEsterr = fabs(outputErrEstimate - outputErr);
//
//      // Monitor L2 Error
      ArrayQ L2err = 0.0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_VMSD( errorL2Integrand, L2err ),
          xfld, (pGlobalSol->primal.qfld, pGlobalSol->primal.qpfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

#ifdef SANS_MPI
      L2err = boost::mpi::all_reduce( *xfld.comm(), L2err, std::plus<ArrayQ>() );
#endif

      if (world.rank() == 0)
      {
        //dump L2, output errors to file
        foutsol << order << " " << ii << std::setprecision(15);
        foutsol << " " << sqrt(L2err[0]);
        foutsol << " " << sqrt(L2err[1]);
        foutsol << " " << sqrt(L2err[2]);
        foutsol << " " << sqrt(L2err[3]);
        foutsol << " " << outputErr;
        foutsol << " " << outputErrEstimate;
        foutsol << " " << solveTime << "\n";
      }

      foutsol.close();
//
#if 1
      // Tecplot dump grid
      string filenameprimal = filename + ".plt";
//      string filenameadjoint = filename + "_adj.plt";
      output_Tecplot( pGlobalSol->primal.qfld, filenameprimal );
//      output_Tecplot( pGlobalSol->adjoint.qfld, filenameadjoint );
#endif

    }

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
