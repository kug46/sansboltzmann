// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// SolveD1Q3_DGAdvective_Line_Boltzmann_Diffusion_toy


#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsBoltzmannD1Q3.h"
//#include "pde/NS/Q1DConservative.h"
#include "pde/NS/QD1Q3PrimitiveDistributionFunctions.h"
//#include "pde/NS/Q1DEntropy.h"
//#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/BCBoltzmannD1Q3.h"
#include "pde/NS/OutputBoltzmannD1Q3.h"
#include "pde/NS/SolutionFunction_BoltzmannD1Q3.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
//#include "pde/AnalyticFunction/SSMEInletConditions.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"


#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "Meshing/XField1D/XField1D.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "tools/output_std_vector.h"

using namespace std;
using namespace SANS;

namespace SANS
{
//template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
//template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveDistributionFunctions>      { static std::string str() { return "PrimitiveVariables";    } };
//template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

//############################################################################//
BOOST_AUTO_TEST_SUITE(SolveD1Q3_DGAdvective_Line_Boltzmann_test_suite )

//------------------------------------------------------------------------------
BOOST_AUTO_TEST_CASE( SolveD1Q3_DGAdvective_Line_Boltzmann_Diffusion )
{
  //--------------------------------------------------------------------------------------
  // MPI comm world
  //--------------------------------------------------------------------------------------
  mpi::communicator world;
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Typedefs for Euler1D
  //--------------------------------------------------------------------------------------
  typedef QTypePrimitiveDistributionFunctions QType;
  typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
  typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  //--------------------------------------------------------------------------------------
  //RK:
  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> RKClass;
  //--------------------------------------------------------------------------------------
  // Typedefs for discretization
  //--------------------------------------------------------------------------------------
  typedef BCBoltzmannD1Q3Vector<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> BCVector;
  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD1, TopoD1> > PDEPrimalEquationSetClass;
  //typedef AlgebraicEquationSet<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1> > AlgebraicEquationSet_Class;

  typedef PDEPrimalEquationSetClass::BCParams BCParams;
//  typedef PDEPrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PDEPrimalEquationSetClass::SystemVector SystemVectorClass;
  //typedef ScalarFunction1D_Sine SolutionClass;
  //typedef SolnNDConvertSpace<PhysD1, SolutionClass> SolutionNDClass;
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Solution order
  //--------------------------------------------------------------------------------------
  int order_pde = 1;
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Area function for the nozzle contour
  //--------------------------------------------------------------------------------------
  //std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Create our Euler PDE
  //--------------------------------------------------------------------------------------
  // Gas Model
  Real dt = 0.01;
  Real tFinal = 10;
  int nSteps = tFinal / dt;

  Real gamma = 1.4;
  Real R = 1./3;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);
  Real viscosity = 1e-2;
  Real csq = 1/3.;
  Real tau = viscosity/csq;
  Real Uadv = 0.01;
  //Real Uadv = 0.01;
  NDPDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw,
                 PDEClass::BGK, tau,
                 PDEClass::AdvectionDiffusion, Uadv );

  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Define boundary conditions
  //--------------------------------------------------------------------------------------
  Real LeftVal = 2.;
  Real RightVal = 0.;
//  bool upwind = true;

/*Real rho = 1.0;
  Real t =  1.0;
  Real M = 1.0;
  Real a = sqrt(gamma*R*t);
  Real u = M * a;
  Real p = gas.pressure(rho, t);
*/
  PyDict StateRight;
  StateRight[BoltzmannVariableTypeD1Q3Params::params.StateVector.Variables] =
      BoltzmannVariableTypeD1Q3Params::params.StateVector.PrimitiveDistributionFunctions3;
  StateRight[PrimitiveDistributionFunctionsD1Q3Params::params.pdf0] = RightVal;
  StateRight[PrimitiveDistributionFunctionsD1Q3Params::params.pdf1] = 0;
  StateRight[PrimitiveDistributionFunctionsD1Q3Params::params.pdf2] = 0;

  PyDict StateLeft;
  StateLeft[BoltzmannVariableTypeD1Q3Params::params.StateVector.Variables] =
      BoltzmannVariableTypeD1Q3Params::params.StateVector.PrimitiveDistributionFunctions3;
  StateLeft[PrimitiveDistributionFunctionsD1Q3Params::params.pdf0] = 0;
  StateLeft[PrimitiveDistributionFunctionsD1Q3Params::params.pdf1] = 0;
  StateLeft[PrimitiveDistributionFunctionsD1Q3Params::params.pdf2] = LeftVal;

  // Create a BC dictionary
  PyDict BCLeft;
  typedef BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf0, PDEClass> BCClass;
  typedef BCParameters< BCBoltzmannD1Q3Vector<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> > BCParams;
  BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.BounceBackDirichletPDF2;
  BCLeft[BCClass::ParamsType::params.Characteristic] = true;
  BCLeft[BCClass::ParamsType::params.StateVector] = StateLeft;

  // Create a BC dictionary
  PyDict BCRight;
  typedef BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf2, PDEClass> BCClass2;
  typedef BCParameters< BCBoltzmannD1Q3Vector<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> > BCParams;
  BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.BounceBackDirichletPDF0;
  BCRight[BCClass2::ParamsType::params.Characteristic] = true;
  BCRight[BCClass2::ParamsType::params.StateVector] = StateRight;

  // Define BC list
  PyDict PyPDEBCList;
  PyPDEBCList["Left"] = BCLeft;
  PyPDEBCList["Right"] = BCRight;


  GlobalTime time;
  time = 0.;


  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Create grid
  //--------------------------------------------------------------------------------------
  int ii = 50;
  XField1D xfld( ii, 0., 1. );
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Get boundary group information
  //--------------------------------------------------------------------------------------
  std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;
  PDEBCBoundaryGroups["Left"] = {0}; // Left
  PDEBCBoundaryGroups["Right"] = {1}; // Right
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Check BCs
  //--------------------------------------------------------------------------------------
  BCParams::checkInputs(PyPDEBCList);
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Set initial conditions
  //--------------------------------------------------------------------------------------
  Real rhoInit = 1./3;
  typedef SolutionFunction_BoltzmannD1Q3_SinX<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> SolutionClass;
  typedef SolnNDConvertSpace<PhysD1, SolutionClass> SolutionNDClass;
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.pdf0] = rhoInit;
  solnArgs[SolutionClass::ParamsType::params.pdf1] = rhoInit;
  solnArgs[SolutionClass::ParamsType::params.pdf2] = rhoInit;
  SolutionNDClass solnExact(solnArgs);


  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate our Euler PDE field
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Lagrange);
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> pde_rfld(xfld, order_pde, BasisFunctionCategory_Lagrange);
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> pde_lgfld(xfld, order_pde, BasisFunctionCategory_Lagrange,
                                                           BCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups));
  for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, pde_qfld) );
  pde_rfld = 0;
  pde_lgfld = 0;




    //--------------------------------------------------------------------------------------



  //--------------------------------------------------------------------------------------
  // Output IC gnuplot file
  //--------------------------------------------------------------------------------------
  string filenameStem_IC = "tmp/solnDG_BoltzmannD1Q3_step_0";
  output_gnuplot( pde_qfld, filenameStem_IC );
  //--------------------------------------------------------------------------------------
#if 1
  //--------------------------------------------------------------------------------------
  // AES Settings
  //--------------------------------------------------------------------------------------
  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = { 1e-6, 1e-6 };

  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Create our AES to solve our PDE
  //--------------------------------------------------------------------------------------
  std::vector<int> interiorTraceGroups = linspace(0,xfld.nInteriorTraceGroups()-1);


  PDEPrimalEquationSetClass PrincipalEqSet(xfld, pde_qfld, pde_lgfld, pde, quadratureOrder, ResidualNorm_Default, tol, {0},
                                           interiorTraceGroups, PyPDEBCList, PDEBCBoundaryGroups, time);

  //AlgebraicEquationSet_Class PrincipalEqSet(xfld, pde_qfld, pde, quadratureOrder, {0}, PrincipalEqSet);
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Construct Newton Solver
  //--------------------------------------------------------------------------------------
  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NewtonSolverParam::checkInputs(NewtonSolverDict);
  //NewtonSolver<SystemMatrixClass> Solver( PrincipalEqSet, NewtonSolverDict );
  //--------------------------------------------------------------------------------------

  int RKorder = 4;
  int RKtype = 0;
  int RKstages = RKorder;


  //temporal discretiation
  RKClass RK(RKorder, RKstages, RKtype, dt, time, xfld, pde_qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, PrincipalEqSet);
/*
  //--------------------------------------------------------------------------------------
  // Set initial guess from initial condition
  //--------------------------------------------------------------------------------------
  SystemVectorClass ini(PrincipalEqSet.vectorStateSize());
  SystemVectorClass sln(PrincipalEqSet.vectorStateSize());

  PrincipalEqSet.fillSystemVector(ini);
  sln = ini;
  */
  //--------------------------------------------------------------------------------------
#if 1
  for (int step = 1; step <= nSteps; step++)
  {
    std::cout << "############################# Time Step: " << step << "\n time is " << time << std::endl;
    // Advance solution
    RK.march(1);
    if (false)
    {
      // Tecplot Output
      //int FieldOrder = order_pde;
      string filename = "tmp/solnDG_BoltzmannD1Q3_step_";
      int f_time = step;
      filename += to_string(f_time);
      output_gnuplot( pde_qfld, filename );
    }
  }
#endif

      string filename = "tmp/solnDG_BoltzmannD1Q3_time_1_dt";
      filename += to_string(dt);
      output_gnuplot( pde_qfld, filename );
#endif
#if 1
  //--------------------------------------------------------------------------------------
  // SOLVE
  //--------------------------------------------------------------------------------------
#if 0
#define USE_NEWTON_SOLVE 1
#if USE_NEWTON_SOLVE
  SolveStatus status = Solver.solve(ini, sln);
  BOOST_CHECK( status.converged );
#else
  bool converged = false;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  PyDict PseudoTimeDict;
  PseudoTimeDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  PseudoTimeDict[PseudoTimeParam::params.invCFL] = 1;
  PseudoTimeDict[PseudoTimeParam::params.invCFL_max] = 100;
  PseudoTimeDict[PseudoTimeParam::params.invCFL_min] = 0.0;

  PseudoTimeDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.5;//0.5;
  PseudoTimeDict[PseudoTimeParam::params.CFLIncreaseFactor] = 10;
  PseudoTimeDict[PseudoTimeParam::params.Verbose] = true;

  PseudoTime<SystemMatrixClass> PTC(PseudoTimeDict, PrincipalEqSetPTC);
  converged = PTC.iterate(100);
  BOOST_CHECK(converged);
#endif
#endif


  //--------------------------------------------------------------------------------------
  // Output Tecplot file
  //--------------------------------------------------------------------------------------
  string filenameStem = "tmp/EBoltzmann_" + Type2String<QType>::str() + "_P" + std::to_string(order_pde) + "." + std::to_string(world.rank());
  output_Tecplot( pde_qfld, filenameStem + ".plt" );
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Output gnuplot file
  //--------------------------------------------------------------------------------------
  output_gnuplot( pde_qfld, filenameStem + ".gplt" );
  output_gnuplot( pde_qfld, filenameStem + ".txt" );
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Get residual after 0 iterations
  //--------------------------------------------------------------------------------------
  SystemVectorClass q(PrincipalEqSet.vectorStateSize());
  PrincipalEqSet.fillSystemVector(q);
  SystemVectorClass rsd(PrincipalEqSet.vectorEqSize());
  rsd = 0.0;
  PrincipalEqSet.residual(q, rsd);
  PrincipalEqSet.setSolutionField(rsd);
  //--------------------------------------------------------------------------------------
  // Output file
  //--------------------------------------------------------------------------------------
  filenameStem = "tmp/EulerNozzle_" + Type2String<QType>::str() + "_P" + std::to_string(order_pde) + "." + std::to_string(world.rank()) + "_residual";
  //--------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------
  // Output gnuplot file
  //--------------------------------------------------------------------------------------
  output_gnuplot( pde_qfld, filenameStem + ".gplt" );
  output_gnuplot( pde_qfld, filenameStem + ".txt" );
  //--------------------------------------------------------------------------------------
#endif


}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
