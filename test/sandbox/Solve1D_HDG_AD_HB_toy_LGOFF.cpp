// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_HDG_HB_AD_TimeStep_btest
// testing of 1-D HDG with Unsteady Advection-Diffusion Timestepping

#undef SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction_MMS.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandFunctor_HDG_HB.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualCell_HDG_HB.h"
#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianCell_HDG_HB.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_HDG_AD_HB_test_suite )

BOOST_AUTO_TEST_CASE( Solve_HDG_ODE_HB )
{

  // solves du/dt + u = f
  // for u = cos(2*pi*t)

  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::template MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_ConstCosUnsteady> NDSolutionExact;

  // PDE
  Real nu = 0.0;
  Real c = 0.0;
//  Real nu = 0.0;
//  Real c = 0.0;
  Real S = 1.0;

  GlobalTime time(0);

  AdvectiveFlux1D_Uniform adv( c );
  ViscousFlux1D_Uniform visc( nu );
  Source1D_Uniform source( S );

  ScalarFunction1D_ConstCosUnsteady MMS_soln;
  typedef ForcingFunction1D_MMS<PDEAdvectionDiffusion1D, ScalarFunction1D_ConstCosUnsteady> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(MMS_soln) );

  // Exact Soln + BC
  NDSolutionExact solnExact(time);

  PDEClass pde(time, adv, visc, source, forcingptr );
  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Global, Gradient );

  //set up integrands
  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandCell_HDG_HB<PDEClass> IntegrandCellHBClass;

  typedef IntegrandCell_SquareError<PDEClass,NDSolutionExact> IntegrandSquareErrorClass;


  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandSquareErrorClass fcnErr( solnExact, {0} );

  // set HB and grid parameters for the run
  int ModesMin = 1;
  int ModesMax = 1;
  Real period = 1.;
  Real t0 = 0.;

  // quadrature rule
  int quadratureOrder = -1;    // max
  int quadratureOrderMin = 0;     // min

  for (int modes = ModesMin; modes <= ModesMax; modes++)
  {
    // compute time slices
    cout << "HB modes: " <<  modes << ": \n";
    int ntimes = 2*modes + 1;
    int order = 2;
    Real tvec[ntimes];
    if (ntimes!= 1)
    {
      for (int i=0; i<ntimes; i++)
        tvec[i] = t0 + i*period/(ntimes);
    }
    else
      tvec[0] = t0;

    // L2 norm vector
    Real normVec[10];
    int indx = 0;

    //initialize HB integrand functor with period and number of times
    IntegrandCellHBClass fcnHB( pde, disc, period, ntimes );

    // time run for each grid
    std::clock_t start;
    double duration;

    start = std::clock();

    //set up xfield
    int ii = pow(2,1); //grid size
    XField1D xfld( ii );
    //XField1D_1Line_X1_1Group xfld;

    ////////////////////////////////////////////
    //set up solution field vectors
    ////////////////////////////////////////////
    typedef std::unique_ptr<Field<PhysD1, TopoD1, ArrayQ>> UFieldPtr;
    typedef std::unique_ptr<Field<PhysD1, TopoD1, VectorArrayQ>> QFieldPtr;

    typedef Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufldclass;
    typedef Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfldclass;

    std::vector< UFieldPtr > uflds;
    std::vector< QFieldPtr > qflds;

    for (int j=0; j<ntimes; j++)
      uflds.push_back( UFieldPtr(new ufldclass(xfld, order, BasisFunctionCategory_Legendre)) );
    for (int j=0; j<ntimes; j++)
      qflds.push_back( QFieldPtr(new qfldclass(xfld, order, BasisFunctionCategory_Legendre)) );

    const int nDOFPDE = uflds[0]->nDOF();
    const int nDOFAu  = qflds[0]->nDOF();

    const int nDOFtot = ntimes*(nDOFPDE + nDOFAu );

    SANS_ASSERT(uflds[0]->nDOF() == ii*(order+1) );

    ////////////////////////
    //RESIDUAL EVALUATION
    ////////////////////////

    // linear system setup

    typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
    typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
    typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
    typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;

    // Declare and size residual vector
    SystemVectorClass rsd(ntimes);
    BOOST_CHECK_EQUAL(rsd.m(),ntimes);

    for (int i=0; i<ntimes; i++)
    {
      //probably not the best way to do this but works for now
      rsd(i).resize( nDOFPDE ); //set size of individual residual vectors in rsdHB.
    }

    // zero out residual
    rsd = 0;
    SystemVectorClass rsddummy(rsd);

    // Evaluate residual vector
    for (int i=0; i<ntimes; i++)
    {
      time = tvec[i]; //increment time for time-dependent terms
      BOOST_CHECK_EQUAL(time, tvec[i]); //cppcheck warning

      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsd(i), rsddummy(i)),
                                              xfld, (*uflds[i], *qflds[i]), &quadratureOrder, 1 );

      ResidualCell_HDG_HB<TopoD1>::integrate( fcnHB, i, uflds,
          &quadratureOrder, 1, rsd(i) );
    }

    ///////////////////////////////
    // START SETTING UP JACOBIAN
    ///////////////////////////////
    //       set up jacobian sparsity pattern...
    typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;
    typedef SLA::SparseMatrixSize SparseMatrixSizeClass;

    //declare matrix sizes
    DLA::MatrixD<SparseMatrixSizeClass> sz(ntimes,ntimes); // N x N (just PDE)

    //resize sparsematrixsizes inside the individual matrices
    for (int i=0; i<ntimes; i++)
      for (int j=0; j<ntimes; j++)
        sz(i,j).resize(nDOFPDE,nDOFPDE);

    DLA::MatrixD<NonZeroPatternClass> nz(sz); // initialize NZ pattern from size matrix
    DLA::MatrixD<NonZeroPatternClass> nzdummy(sz); // initialize NZ pattern from size matrix

    // fill out non-zero patterns with spatial non-zeros
    time = tvec[0];
    BOOST_CHECK(time == tvec[0]); // for cppcheck

    IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nz(0,0), nzdummy(0,0),
                                                                                    nzdummy(0,0),  nzdummy(0,0)),
                                            xfld, (*uflds[0], *qflds[0]), &quadratureOrderMin, 1 );

    // copy spatial non-zero pattern from first blocks to other diagonal blocks
    for (int i=1; i<ntimes; i++)
      nz(i,i) = nz(0,0);


    // add temporal non-zero pattern
    for (int i=0; i<ntimes; i++)
      for (int j=0; j<ntimes; j++)
      {
        if (i != j)
          JacobianCell_HDG_HB<SurrealClass,TopoD1>::integrate( fcnHB, i, j,
              uflds, &quadratureOrderMin, 1, nz(i,j));
      }

    //initialize the jacobian from non-zero patterns
    SystemMatrixClass jac(nz);
    SystemMatrixClass jacdummy(nz);
    jac = 0;

    //fill spatial jacobian
    for (int i=0; i<ntimes; i++)
    {
      time = tvec[i]; // increment time for time-dependent terms
      BOOST_CHECK_EQUAL(time, tvec[i]); //cppcheck warning

      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jac(i,i), jacdummy(i,i),
                                                                                      jacdummy(i,i),  jacdummy(i,i)),
                                              xfld, (*uflds[i], *qflds[i]), &quadratureOrder, 1 );
    }

    //fill temporal jacobian
    for (int i=0; i<ntimes; i++)
      for (int j=0; j<ntimes; j++)
      {
        if (i != j)
          JacobianCell_HDG_HB<SurrealClass,TopoD1>::integrate( fcnHB, i, j,
              uflds, &quadratureOrder, 1, jac(i,j));
      }

#if 0
    fstream fout( "tmp/jacHBunsteady.mtx", fstream::out );
    cout << "btest: writing global jac" << endl;
    WriteMatrixMarketFile( jac, fout );
#endif

    ////////////////////////
    //CREATE SOLUTION VECTOR
    ///////////////////////
    SystemVectorClass sol(ntimes);

    for (int i=0; i<ntimes; i++)
      sol(i).resize( nDOFPDE ); //set size of individual residual vectors in rsdHB.

    ////////////////////////
    //DIRECT SOLVE FOR NOW
    ////////////////////////

    SLA::UMFPACK<SystemMatrixClass> solver;

    sol = solver.inverse(jac)*rsd;

    // update solution fields
    for (int i=0; i < ntimes; i++)
      for (int k = 0; k < nDOFPDE; k++)
        uflds[i]->DOF(k) -= sol[i][k];

    /////////////////////////////////////
    // Re-evaluate residual, check equal to zero
    /////////////////////////////////////
    rsd = 0;
    for (int i=0; i < ntimes; i++)
      for (int k=0; k < nDOFPDE; k++)
        BOOST_CHECK_SMALL(rsd[i][k],1e-12);

    // Evaluate residual vector
    for (int i=0; i<ntimes; i++)
    {
      time = tvec[i]; //increment time for time-dependent terms
      BOOST_CHECK_EQUAL(time, tvec[i]); //cppcheck warning

      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsd(i), rsddummy(i)),
                                              xfld, (*uflds[i], *qflds[i]), &quadratureOrder, 1 );

      ResidualCell_HDG_HB<TopoD1>::integrate( fcnHB, i, uflds,
          &quadratureOrder, 1, rsd(i) );
    }


    Real rsdPDEnrm = 0;

    for (int i=0; i<ntimes; i++)
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[i][n],2);

    BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

    /////////////////////////////////////
    // Compute L2 error
    /////////////////////////////////////

    ArrayQ Error2D = 0;

    for (int i=0; i<ntimes; i++)
    {
      time = tvec[i];
      BOOST_CHECK_EQUAL(time, tvec[i]); //cppcheck warning
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell( fcnErr, SquareError ), xfld, *uflds[i], &quadratureOrder, 1 );
      Error2D += SquareError*(period / (ntimes-1));
    }

    Real norm = Error2D;

    normVec[indx] = sqrt( norm );
    indx++;

    duration = ( std::clock() - start) / (double) CLOCKS_PER_SEC;

#if 1
cout << "P = " << order << ": modes = " << modes << ": nDOF = " << nDOFtot;
cout << " CPUTime = " << duration << " s";
cout << " : L2 error = " << sqrt( norm );
if (indx > 1)
  cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
cout << endl;
#endif



  }
  cout << endl;
}


//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( Solve_HDG_AD_HB )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None> PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::template MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  typedef ScalarFunction1D_SineSineUnsteady SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCAdvectionDiffusion<PhysD1,BCType> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  // PDE
  Real nu = 0.02;
  Real c = -1.1;
//  Real nu = 0.0;
//  Real c = 0.0;


  GlobalTime time(0);

  AdvectiveFlux1D_Uniform adv( c );
  ViscousFlux1D_Uniform visc( nu );
  Source1D_None source;

  // Exact Soln + BC
  NDSolutionExact solnExact(time);

  PDEClass pde(time, adv, visc, source );

  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact );
  BCClass bc(time, solnExactPtr, visc );
  const std::vector<int> BoundaryGroups = {0,1};

  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Global, Gradient );

  //set up integrands
  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandCell_HDG_HB<PDEClass> IntegrandCellHBClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, BCClass, BCClass::Category, HDG> IntegrandBoundClass;

  typedef IntegrandCell_SquareError<PDEClass,NDSolutionExact> IntegrandSquareErrorClass;


  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnInt( pde, disc, {0} );
  IntegrandBoundClass fcnBC( pde, bc, BoundaryGroups, disc );
  IntegrandSquareErrorClass fcnErr( solnExact, {0} );

  // set HB and grid parameters for the run
  int ModesMin = 1;
  int ModesMax = 1;
  int iipowmin = 1;
  int iipowmax = 1;
  int ordermin = 1;
  int ordermax = 1;
  Real period = 1.;
  Real t0 = 0.;

  // quadrature rule
  int quadratureOrder[2] = {-1, -1};    // max
  int quadratureOrderMin[2] = {0, 0};     // min

  for (int order = ordermin; order <= ordermax; order++)
    for (int modes = ModesMin; modes <= ModesMax; modes++)
    {
      // compute time slices
      cout << "HB modes: " <<  modes << ": \n";
      const int ntimes = 2*modes + 1;

      Real tvec[ntimes];
      if (ntimes!= 1)
      {
        for (int i=0; i<ntimes; i++)
          tvec[i] = t0 + i*period/ntimes;
      }
      else
        tvec[0] = t0;

      // L2 norm vector
      Real normVec[10];
      int indx = 0;

      //initialize HB integrand functor with period and number of times
      IntegrandCellHBClass fcnHB( pde, disc, period, ntimes );

      for (int iipow = iipowmin; iipow <= iipowmax; iipow++)
      {
        // time run for each grid
        std::clock_t start;
        double duration;

        start = std::clock();

        //set up xfield
        int ii = pow(2,iipow); //grid size - using ntimes = grid size?
        XField1D xfld( ii );

        ////////////////////////////////////////////
        //set up solution field vectors
        ////////////////////////////////////////////
        typedef std::unique_ptr<Field<PhysD1, TopoD1, ArrayQ>> UFieldPtr;
        typedef std::unique_ptr<Field<PhysD1, TopoD1, VectorArrayQ>> QFieldPtr;
        typedef std::unique_ptr<Field<PhysD1, TopoD1, ArrayQ>> UIFieldPtr;
        typedef std::unique_ptr<Field<PhysD1, TopoD1, ArrayQ>> LGFieldPtr;

        typedef Field_DG_Cell<PhysD1, TopoD1, ArrayQ> ufldclass;
        typedef Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> qfldclass;
        typedef Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> uIfldclass;
        typedef Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldclass;

        std::vector< UFieldPtr > uflds;
        std::vector< QFieldPtr > qflds;
        std::vector< UIFieldPtr > uIflds;
        std::vector< LGFieldPtr > lgflds;

        for (int j=0; j<ntimes; j++)
          uflds.push_back( UFieldPtr(new ufldclass(xfld, order, BasisFunctionCategory_Hierarchical)) );
        for (int j=0; j<ntimes; j++)
          qflds.push_back( QFieldPtr(new qfldclass(xfld, order, BasisFunctionCategory_Hierarchical)) );
        for (int j=0; j<ntimes; j++)
          uIflds.push_back( UIFieldPtr(new uIfldclass(xfld, order, BasisFunctionCategory_Hierarchical)) );
        for (int j=0; j<ntimes; j++)
          lgflds.push_back( LGFieldPtr(new lgfldclass(xfld, order, BasisFunctionCategory_Hierarchical, BoundaryGroups)) );

        const int nDOFPDE = uflds[0]->nDOF();
        const int nDOFAu  = qflds[0]->nDOF();
        const int nDOFInt = uIflds[0]->nDOF();
        const int nDOFBC  = lgflds[0]->nDOF();

        const int nDOFtot = ntimes*(nDOFPDE + nDOFAu + nDOFInt + nDOFBC);

        SANS_ASSERT(uflds[0]->nDOF() == (ii*(order+1)));

        ////////////////////////
        //RESIDUAL EVALUATION
        ////////////////////////

        // linear system setup

        typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
        typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
        typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
        typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;

        // Declare and size residual vector
        SystemVectorClass rsd(4*ntimes);
        BOOST_CHECK_EQUAL(rsd.m(),4*ntimes);

        for (int i=0; i<ntimes; i++)
        {
          //different indices for different vectors
          int iPDE = i;
          int iAu = ntimes + i;
          int iInt = 2*ntimes + i;
          int iBC = 3*ntimes + i;

          //probably not the best way to do this but works for now
          rsd(iPDE).resize( nDOFPDE ); //set size of individual residual vectors in rsdHB.
          rsd(iAu).resize( nDOFAu ); //set size of individual residual vectors in rsdHB
          rsd(iInt).resize( nDOFInt ); //set size of individual residual vectors in rsdHB
          rsd(iBC).resize( nDOFBC ); //set size of individual residual vectors in rsdHB
        }

        // zero out residual
        rsd = 0;

        // Evaluate residual vector
        for (int i=0; i<ntimes; i++)
        {
          //different indices for different vectors
          int iPDE = i;
          int iAu = ntimes + i;
          int iInt = 2*ntimes + i;
          int iBC = 3*ntimes + i;

          time = tvec[i]; //increment time for time-dependent terms
          BOOST_CHECK_EQUAL(time, tvec[i]); //cppcheck warning

          IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsd(iPDE), rsd(iAu)),
                                                  xfld, (*uflds[i], *qflds[i]), quadratureOrder, 1 );
          ResidualCell_HDG_HB<TopoD1>::integrate( fcnHB, i, uflds,
              quadratureOrder, 1, rsd(iPDE) );
          IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(iPDE), rsd(iAu), rsd(iInt)),
                                                                  xfld, (*uflds[i], *qflds[i]), *uIflds[i], quadratureOrder, 1);
          IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsd(iPDE), rsd(iAu), rsd(iBC)),
                                                              xfld, (*uflds[i], *qflds[i]), *lgflds[i], quadratureOrder, 2 );
        }

        ///////////////////////////////
        // START SETTING UP JACOBIAN
        ///////////////////////////////
        //       set up jacobian sparsity pattern...
        //
        typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;
        typedef SLA::SparseMatrixSize SparseMatrixSizeClass;

        //declare matrix sizes
        DLA::MatrixD<SparseMatrixSizeClass> sz(4*ntimes,4*ntimes); // 4N x 4N

        //resize sparsematrixsizes inside the individual matrices
        for (int i=0; i<ntimes; i++)
          for (int j=0; j<ntimes; j++)
          {
            //index offsets for different matrix blocks
            int iPDE = i;
            int iAu = ntimes + i;
            int iInt = 2*ntimes + i;
            int iBC = 3*ntimes + i;

            int jPDE = j;
            int jAu = ntimes + j;
            int jInt = 2*ntimes + j;
            int jBC = 3*ntimes + j;

            sz(iPDE,jPDE).resize(nDOFPDE,nDOFPDE);
            sz(iPDE,jAu).resize(nDOFPDE,nDOFAu);
            sz(iPDE,jInt).resize(nDOFPDE,nDOFInt);
            sz(iPDE,jBC).resize(nDOFPDE,nDOFBC);

            //finish up later
            sz(iAu,jPDE).resize(nDOFAu,nDOFPDE);
            sz(iAu,jAu).resize(nDOFAu,nDOFAu);
            sz(iAu,jInt).resize(nDOFAu,nDOFInt);
            sz(iAu,jBC).resize(nDOFAu,nDOFBC);

            sz(iInt,jPDE).resize(nDOFInt,nDOFPDE);
            sz(iInt,jAu).resize(nDOFInt,nDOFAu);
            sz(iInt,jInt).resize(nDOFInt,nDOFInt);
            sz(iInt,jBC).resize(nDOFInt,nDOFBC);

            sz(iBC,jPDE).resize(nDOFBC,nDOFPDE);
            sz(iBC,jAu).resize(nDOFBC,nDOFAu);
            sz(iBC,jInt).resize(nDOFBC,nDOFInt);
            sz(iBC,jBC).resize(nDOFBC,nDOFBC);
          }

        DLA::MatrixD<NonZeroPatternClass> nz(sz); // initialize NZ pattern from size matrix

        // set up zero index for each block
        int iPDE0 = 0;
        int iAu0 = ntimes + 0;
        int iInt0 = 2*ntimes + 0;
        int iBC0 = 3*ntimes + 0;

        // some checks
#if 0
        BOOST_CHECK(nz(iPDE0,iPDE0).m() == nDOFPDE);
        BOOST_CHECK(nz(iPDE0,iPDE0).n() == nDOFPDE);
        BOOST_CHECK(nz(iAu0,iPDE0).m() == nDOFAu);
        BOOST_CHECK(nz(iAu0,iPDE0).n() == nDOFPDE);
        BOOST_CHECK(nz(iInt0,iPDE0).m() == nDOFInt);
        BOOST_CHECK(nz(iInt0,iPDE0).n() == nDOFPDE);
        BOOST_CHECK(nz(iBC0,iPDE0).m() == nDOFBC);
        BOOST_CHECK(nz(iBC0,iPDE0).n() == nDOFPDE);
        BOOST_CHECK(nz(iPDE0,iAu0).m() == nDOFPDE);
        BOOST_CHECK(nz(iPDE0,iAu0).n() == nDOFAu);
        BOOST_CHECK(nz(iAu0,iAu0).m() == nDOFAu);
        BOOST_CHECK(nz(iAu0,iAu0).n() == nDOFAu);
        BOOST_CHECK(nz(iInt0,iAu0).m() == nDOFInt);
        BOOST_CHECK(nz(iInt0,iAu0).n() == nDOFAu);
        BOOST_CHECK(nz(iBC0,iAu0).m() == nDOFBC);
        BOOST_CHECK(nz(iBC0,iAu0).n() == nDOFAu);
        BOOST_CHECK(nz(iBC0,iBC0).m() == nDOFBC);
        BOOST_CHECK(nz(iBC0,iBC0).n() == nDOFBC);
#endif

        // fill out non-zero patterns with spatial non-zeros
        time = t0;
        BOOST_CHECK(time == t0);

        IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nz(iPDE0,iPDE0), nz(iPDE0,iAu0),
                                                                                        nz(iAu0,iPDE0),  nz(iAu0,iAu0) ),
                                                xfld, (*uflds[0], *qflds[0]), quadratureOrderMin, 1 );

        IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
            JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, nz(iPDE0,iPDE0), nz(iPDE0,iAu0), nz(iPDE0,iInt0),
                                                            nz(iAu0,iPDE0) ,                 nz(iAu0,iInt0) ,
                                                            nz(iInt0,iPDE0), nz(iInt0,iAu0), nz(iInt0,iInt0) ),
            xfld, (*uflds[0], *qflds[0]), *uIflds[0], quadratureOrderMin, 1);

        IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
            JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, nz(iPDE0,iPDE0), nz(iPDE0,iAu0), nz(iPDE0,iBC0),
                                                              nz(iAu0,iPDE0) , nz(iAu0,iAu0) , nz(iAu0,iBC0) ,
                                                              nz(iBC0,iPDE0) , nz(iBC0,iAu0) , nz(iBC0,iBC0)),
            xfld, (*uflds[0], *qflds[0]), *lgflds[0], quadratureOrderMin, 2);

        // copy spatial non-zero pattern from first blocks to other diagonal blocks
        for (int i=1; i<ntimes; i++)
        {
          int iPDE = i;
          int iAu = ntimes + i;
          int iInt = 2*ntimes + i;
          int iBC = 3*ntimes + i;

          nz(iPDE,iPDE) = nz(iPDE0,iPDE0);
          nz(iPDE,iAu) = nz(iPDE0,iAu0);
          nz(iPDE,iInt) = nz(iPDE0,iInt0);
          nz(iPDE,iBC) = nz(iPDE0,iBC0);

          nz(iAu,iPDE) = nz(iAu0,iPDE0);
          nz(iAu,iAu) = nz(iAu0,iAu0);
          nz(iAu,iInt) = nz(iAu0,iInt0);
          nz(iAu,iBC) = nz(iAu0,iBC0);

          nz(iInt,iPDE) = nz(iInt0,iPDE0);
          nz(iInt,iAu) = nz(iInt0,iAu0);
          nz(iInt,iInt) = nz(iInt0,iInt0);
          nz(iInt,iBC) = nz(iInt0,iBC0);

          nz(iBC,iPDE) = nz(iBC0,iPDE0);
          nz(iBC,iAu) = nz(iBC0,iAu0);
          nz(iBC,iInt) = nz(iBC0,iInt0);
          nz(iBC,iBC) = nz(iBC0,iBC0);
        }

        // add temporal non-zero pattern
        for (int i=0; i<ntimes; i++)
          for (int j=0; j<ntimes; j++)
          {
            int iPDE = i;
            int jPDE = j;

            if (i != j)
              JacobianCell_HDG_HB<SurrealClass,TopoD1>::integrate( fcnHB, i, j,
                  uflds, quadratureOrderMin, 1, nz(iPDE,jPDE));
          }

        //initialize the jacobian from non-zero patterns
        SystemMatrixClass jac(nz);
        jac = 0;

        //fill spatial jacobian
        for (int i=0; i<ntimes; i++)
        {
          int iPDE = i;
          int iAu = ntimes + i;
          int iInt = 2*ntimes + i;
          int iBC = 3*ntimes + i;

          time = tvec[i]; // increment time for time-dependent terms
          BOOST_CHECK_EQUAL(time, tvec[i]); //cppcheck warning

          IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jac(iPDE,iPDE), jac(iPDE,iAu),
                                                                                          jac(iAu,iPDE),  jac(iAu,iAu) ),
                                                  xfld, (*uflds[i], *qflds[i]), quadratureOrder, 1 );

          IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
              JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jac(iPDE,iPDE), jac(iPDE,iAu), jac(iPDE,iInt),
                                                              jac(iAu,iPDE) ,                jac(iAu,iInt) ,
                                                              jac(iInt,iPDE), jac(iInt,iAu), jac(iInt,iInt) ),
              xfld, (*uflds[i], *qflds[i]), *uIflds[i], quadratureOrder, 1);

          IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
              JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jac(iPDE,iPDE), jac(iPDE,iAu), jac(iPDE,iBC),
                                                                jac(iAu,iPDE) , jac(iAu,iAu) , jac(iAu,iBC) ,
                                                                jac(iBC,iPDE) , jac(iBC,iAu) , jac(iBC,iBC)),
              xfld, (*uflds[i], *qflds[i]), *lgflds[i], quadratureOrder, 2);
        }

        //fill temporal jacobian
        for (int i=0; i<ntimes; i++)
          for (int j=0; j<ntimes; j++)
          {
            int iPDE = i;
            int jPDE = j;

            if (i != j)
              JacobianCell_HDG_HB<SurrealClass,TopoD1>::integrate( fcnHB, i, j,
                  uflds, quadratureOrder, 1, jac(iPDE,jPDE));
          }

#if 0
        fstream fout( "tmp/jacHBunsteady.mtx", fstream::out );
        //cout << "btest: global jac" << endl;
        WriteMatrixMarketFile( jac, fout );
#endif

        ////////////////////////
        //CREATE SOLUTION VECTOR
        ///////////////////////
        SystemVectorClass sol(4*ntimes);

        for (int i=0; i<ntimes; i++)
        {
          //different indices for different vectors
          int iPDE = i;
          int iAu = ntimes + i;
          int iInt = 2*ntimes + i;
          int iBC = 3*ntimes + i;

          //probably not the best way to do this but works for now
          sol(iPDE).resize( nDOFPDE ); //set size of individual residual vectors in rsdHB.
          sol(iAu).resize( nDOFAu ); //set size of individual residual vectors in rsdHB
          sol(iInt).resize( nDOFInt ); //set size of individual residual vectors in rsdHB
          sol(iBC).resize( nDOFBC ); //set size of individual residual vectors in rsdHB
        }

        ////////////////////////
        //DIRECT SOLVE FOR NOW
        ////////////////////////

        SLA::UMFPACK<SystemMatrixClass> solver;

        sol = solver.inverse(jac)*rsd;

        // update solution fields
        for (int i=0; i < ntimes; i++)
        {
          int iPDE = i;
          int iAu = ntimes + i;
          int iInt = 2*ntimes + i;
          int iBC = 3*ntimes + i;

          for (int k = 0; k < nDOFPDE; k++)
            uflds[i]->DOF(k) -= sol[iPDE][k];

          for (int k = 0; k < nDOFPDE; k++)
            for (int d = 0; d < PhysD1::D; d++)
              qflds[i]->DOF(k)[d] -= sol[iAu][PhysD1::D*k+d];

          for (int k = 0; k < nDOFInt; k++)
            uIflds[i]->DOF(k) -= sol[iInt][k];

          for (int k = 0; k < nDOFBC; k++)
            lgflds[i]->DOF(k) -= sol[iBC][k];
        }

        /////////////////////////////////////
        // Re-evaluate residual, check equal to zero
        /////////////////////////////////////
        rsd = 0;

        // Evaluate residual vector
        for (int i=0; i<ntimes; i++)
        {
          //different indices for different vectors
          int iPDE = i;
          int iAu = ntimes + i;
          int iInt = 2*ntimes + i;
          int iBC = 3*ntimes + i;

          time = tvec[i]; //increment time for time-dependent terms
          BOOST_CHECK_EQUAL(time, tvec[i]); //cppcheck warning

          IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsd(iPDE), rsd(iAu)),
                                                  xfld, (*uflds[i], *qflds[i]), quadratureOrder, 1 );
          ResidualCell_HDG_HB<TopoD1>::integrate( fcnHB, i, uflds,
              quadratureOrder, 1, rsd(iPDE) );
          IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(iPDE), rsd(iAu), rsd(iInt)),
                                                                  xfld, (*uflds[i], *qflds[i]), *uIflds[i], quadratureOrder, 1);
          IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsd(iPDE), rsd(iAu), rsd(iBC)),
                                                              xfld, (*uflds[i], *qflds[i]), *lgflds[i], quadratureOrder, 2 );

        }

#if 0
        //dump rsd
        for (int i=0; i<ntimes; i++)
        {
          //different indices for different vectors
          //int iPDE = i;
          for (int k=0; k<nDOFPDE; k++)
            std::cout << rsd[i][k][0] << "\n";
        }
#endif

        Real rsdPDEnrm = 0;
        Real rsdAunrm = 0;
        Real rsdIntnrm = 0;
        Real rsdBCnrm = 0;

        for (int i=0; i<ntimes; i++)
        {
          int iPDE = i;
          int iAu = ntimes + i;
          int iInt = 2*ntimes + i;
          int iBC = 3*ntimes + i;

          for (int n = 0; n < nDOFPDE; n++)
            rsdPDEnrm += pow(rsd[iPDE][n],2);

          for (int n = 0; n < nDOFAu; n++)
            rsdAunrm += pow(rsd[iAu][n],2);

          for (int n = 0; n < nDOFInt; n++)
            rsdIntnrm += pow(rsd[iInt][n],2);

          for (int n = 0; n < nDOFBC; n++)
            rsdBCnrm += pow(rsd[iBC][n],2);
        }

        BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );
        BOOST_CHECK_SMALL( sqrt(rsdAunrm), 1e-12 );
        BOOST_CHECK_SMALL( sqrt(rsdIntnrm), 1e-12 );
        BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

        /////////////////////////////////////
        // Compute L2 error
        /////////////////////////////////////

        ArrayQ Error2D = 0;

        for (int i=0; i<ntimes; i++)
        {
          time = tvec[i];
          BOOST_CHECK_EQUAL(time, tvec[i]); //cppcheck warning
          ArrayQ SquareError = 0;
          IntegrateCellGroups<TopoD1>::integrate(
              FunctionalCell( fcnErr, SquareError ), xfld, *uflds[i], quadratureOrder, 1 );
          //        Error2D += SquareError*(period / (ntimes-1));
          Error2D += SquareError;
        }


        Real norm = Error2D;

        normVec[indx] = sqrt( norm );
        indx++;

        duration = ( std::clock() - start) / (double) CLOCKS_PER_SEC;
        //
#if 1
        cout << "P = " << order << " ii = " << ii << ": modes = " << modes << ": nDOF = " << nDOFtot;
        cout << " CPUTime = " << duration << " s";
        cout << " : L2 error = " << sqrt( norm );
        if (indx > 1)
          cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << endl;
#endif
        //
        //      BOOST_CHECK_CLOSE( time, Tend, 1e-12 );


      }
    }

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
