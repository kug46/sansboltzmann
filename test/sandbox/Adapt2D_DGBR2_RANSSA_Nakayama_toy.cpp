// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_RANSSA_Joukowski_btest
// Testing of the MOESS framework on a 2D RANS-SA problem


#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"
#include "Meshing/EPIC/XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_RANSSA_NACADuct_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_RANSSA_NACADuct_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_Entropy<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_Distance ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-9, 1e-9};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.088;
  const Real Reynolds = 1.2e6;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real aoaRef = 0;                            // angle of attack (radians)

#if 1
  const Real tRef = 1;          // temperature
  const Real pRef = rhoRef*R*tRef;
  const Real cRef = sqrt(gamma*R*tRef);
  const Real qRef = Mach*cRef;
#endif

#if 0
  const Real qRef = 1;                              // velocity scale
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntRef}) );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

#if 0
  typedef SolutionFunction_Euler2D_Wake<TraitsSizeRANSSA, TraitsModelRANSSAClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Wake;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = rhoRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = uRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = pRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.wakedepth] = 0.6;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_mitState;
  BCIn[BCClass::ParamsType::params.Function] = Function;
  BCIn["nt"]  = ntRef; //TODO: not sure about this, was previously just "nt" which gave a Python dictionary error

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;

#elif 0
  typedef SolutionFunction_Euler2D_Wake<TraitsSizeRANSSA, TraitsModelRANSSAClass> SolutionFunction_EulerClass;
  typedef BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDEClass> BCClass;

  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 1.0;

  PyDict Function;
  Function[BCClass::ParamsType::params.Function.Name] = BCClass::ParamsType::params.Function.Wake;
  Function[SolutionFunction_EulerClass::ParamsType::params.gasModel] = gasModelDict;
  Function[SolutionFunction_EulerClass::ParamsType::params.rho] = rhoRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.u] = uRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.p] = pRef;
  Function[SolutionFunction_EulerClass::ParamsType::params.wakedepth] = 0.6;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.SolutionInflow_sHqt_BN;
  BCIn[BCClass::ParamsType::params.Function] = Function;
  BCIn["nt"]  = ntRef; //TODO: not sure about this, was previously just "nt" which gave a Python dictionary error

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;

#else

  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;
  StateVector["rhont"]  = rhoRef*ntRef;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

#endif

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  // PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0,1};
  // BCBoundaryGroups["BCOut"] = {2};
  BCBoundaryGroups["BCIn"] = {2,3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  // Drag output
  NDOutputClass outputFcn(pde, 1./(0.5*rhoRef*(uRef*uRef + vRef*vRef)), 0.);
  OutputIntegrandClass outputIntegrand( outputFcn, {0,1} );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);



  //--------ADAPTATION LOOP--------

  for (int order = 1; order <= 3; order++)
    for (int power = 2; power <= 6; power++ )
    {
      int maxIter = 30;
      int initIter = 0;

      PyDict ParamDict;

      int nk = pow(2,power);
      int targetCost = 1000*nk;

      // Grid
      //std::string file_initial_mesh = "grids/nearwake/nearwake_q3.gri";
      //std::string file_initial_linear_mesh = "grids/nearwake/nearwake_q1.gri";
      std::string file_initial_mesh = "tmp/NEARWAKE/P" + std::to_string(order) + "/" + std::to_string(nk) + "k/mesh_a30_outQ3.grm";
      std::string file_initial_linear_mesh = "tmp/NEARWAKE/P" + std::to_string(order) + "/" + std::to_string(nk) + "k/mesh_a30_outQ1.grm";

      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField_PX<PhysD2, TopoD2>(world, file_initial_mesh) );
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld_linear( new XField_PX<PhysD2, TopoD2>(world, file_initial_linear_mesh) );

      //IO
      std::string filename_base = "tmp/NEARWAKE/P" + std::to_string(order) + "/" + std::to_string(nk) + "k/";
      boost::filesystem::path base_dir(filename_base);
      if ( not boost::filesystem::exists(base_dir) )
        boost::filesystem::create_directory(base_dir);

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
//      MOESSDict[MOESSParams::params.UniformRefinement] = false;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
      MesherDict[EpicParams::params.CurvedQOrder] = 3; //Mesh order
      MesherDict[EpicParams::params.CSF] = "grids/nearwake/nearwake.csf"; //Geometry file
      MesherDict[EpicParams::params.GeometryList2D] = "airfoil farfield"; //Geometry list
      MesherDict[EpicParams::params.minGeom] = 1e-6;
      MesherDict[EpicParams::params.maxGeom] = 0.5;
      MesherDict[EpicParams::params.nPointGeom] = -1;
      MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Compute distance field
      std::shared_ptr<Field_CG_Cell<PhysD2, TopoD2, Real>>
        pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfld, 4, BasisFunctionCategory_Lagrange);
      DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));

      //Solution data
      typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>((*pdistfld, *pxfld), pde, order, order+1,
                                                   BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                   active_boundaries, ParamDict, disc);

      output_Tecplot( get<0>(pGlobalSol->paramfld), filename_base + "distfld_a" + std::to_string(initIter) + ".plt" );

      const int quadOrder = 3*(order + 2) - 1; //14 at order = 3

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      //Set initial solution
      pGlobalSol->setSolution(q0);

#if 0
      {
        std::cout << "Reading " << filename_base + "qDOF_init_p" + std::to_string(order) + "_" + std::to_string(nk) + "k_a31.plt" <<std::endl;
        ifstream DOFfile(filename_base + "qDOF_init_p" + std::to_string(order) + "_" + std::to_string(nk) + "k_a31.plt");
        for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
        {
          for (int n = 0; n < ArrayQ::M; n++)
            DOFfile >> pGlobalSol->primal.qfld.DOF(i)[n];
        }
      }
#endif

      std::string qfld_init_filename = filename_base + "qfld_init_p" + std::to_string(order) +
                                                                 "_" + std::to_string(nk) +
                                                               "k_a" + std::to_string(initIter) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      std::string qfld_filename = filename_base + "qfld_p" + std::to_string(order) +
                                                       "_" + std::to_string(nk) +
                                                     "k_a" + std::to_string(initIter) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_p" + std::to_string(order) +
                                                           "_" + std::to_string(nk) +
                                                         "k_a" + std::to_string(initIter) + ".plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


      for (int iter = initIter; iter < maxIter+1; iter++)
      {
        std::cout << "-----Adaptation Iteration "<< iter << "-----" << std::endl;

        //Compute error estimates
        pInterface->computeErrorEstimates();

        //Perform local sampling and adapt mesh
        MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;
        ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew_linear = ptr_pair.first;
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew = ptr_pair.second;

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        //Compute distance field
        pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfldNew, 4, BasisFunctionCategory_Lagrange);
        DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>((*pdistfld, *pxfldNew), pde, order, order+1,
                                                        BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                        active_boundaries, ParamDict, disc);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, LinearSolverDict,
                                                               outputIntegrand);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld_linear = pxfldNew_linear;
        pxfld = pxfldNew;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        std::string tag = "_p"  + std::to_string(order)
                        + "_"   + std::to_string(nk)
                        + "k_a" + std::to_string(iter+1) + ".plt";

        std::string qfld_init_filename = filename_base + "qfld_init" + tag;

        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

        std::cout << "Dumping " << filename_base + "qDOF_init" + tag <<std::endl;
        ofstream DOFfile(filename_base + "qDOF_init" + tag);
        DOFfile << std::setprecision(16);
        for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
        {
          for (int n = 0; n < ArrayQ::M; n++)
            DOFfile << pGlobalSol->primal.qfld.DOF(i)[n] << " ";
          DOFfile << std::endl;
        }

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();

        std::string qfld_filename = filename_base + "qfld" + tag;
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string adjfld_filename = filename_base + "adjfld" + tag;
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
      }

      fadapthist.close();
    }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
