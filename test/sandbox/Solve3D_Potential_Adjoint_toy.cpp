// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve3D_CG_NACA_LIP_btest
// testing of 3-D CG Linarized Incompressible Potential on a NACA airfoil

#undef SANS_FULLTEST
#undef TIMING

//#define LG_WAKE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Python/InputException.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "Surreal/SurrealS.h"
#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/SolutionFunction3D_LIP.h"
#include "pde/FullPotential/BCLinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/LinearizedIncompressiblePotential3D_Trefftz.h"
#include "pde/FullPotential/IntegrandBoundary3D_LIP_Force.h"
#include "pde/FullPotential/IntegrandFunctorBoundary3D_LIP_Vn2.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "Discretization/Galerkin/FunctionalBoundaryTrace_Galerkin.h"

#include "Discretization/Potential_Adjoint/AlgebraicEquationSet_CG_Potential_Adjoint.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryFrame.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/Direct/MKL_PARDISO/MKL_PARDISOSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/EGTess/makeWakedAirfoil.h"
#include "Meshing/EGADS/Airfoils/NACA4.h"
#include "Meshing/TetGen/EGTetGen.h"

#include "Meshing/AFLR/AFLR3.h"

#include "unit/UnitGrids/XField3D_Box_Hex_X1_WakeCut.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1_WakeCut.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;
using namespace EGADS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_Potential_Adjoint_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve3D__Test_Working )
{
  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D<Real>> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef AlgebraicEquationSet_CG_Potential_Adjoint<PDEClass, BCNDConvertSpace, BCLinearizedIncompressiblePotential3DVector<Real>,
      AlgEqSetTraits_Sparse, XField3D_Wake> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef BCLinearizedIncompressiblePotential3DParams<BCTypeNeumann> BCNeumannParamsType;
  typedef BCLinearizedIncompressiblePotential3DParams<BCTypeDirichlet> BCDirichletParamsType;

  timer totaltime;

  timer meshtime;

  Real span = 4;

  int factor = 1;

  // Mark the wake or not as a wake
  bool withWake = true;
  //bool withWake = false;
  const int nChord = 20*factor + 1;
  const int nSpan  = 60*factor + 1; //span*nChord;
  const int nWake  = 10*factor + 1; //nChord;

#if 0
  EGContext context((CreateContext()));

  EGApproximate<3> NACA_spline = NACA4<3>(context, 0.12, 0.3, 0);

  // EGADS mesh parameters
  std::vector<double> EGADSParams = {30.0, 0.1, 15.0};
  std::vector<double> outflowParams = {2.5, 0.001, 15.0};
  // TetGen mesh parameters
  Real maxRadiusEdgeRatio = 1.1;
  Real minDihedralAngle = 25;

  std::vector<int> TrefftzFrames;

  //{32.,6.,320.}
  EGModel<3> model = makeWakedAirfoil(NACA_spline, TrefftzFrames, span,
                                      {-4, -2*span,-2*span,     // x, y, z box corner
                                       10,  3*span, 3*span}, // dx, dy, dz box size
                                      nChord, nSpan, nWake, outflowParams, withWake);
  //model.save( "tmp/NACAWake3D.egads");

  //std::vector<int> KuttaFrames = {0,1};
  //std::vector<int> TrefftzFrames = {2,3};

  //std::vector<int> KuttaFrames = {0,1,2,3};
  //std::vector<int> TrefftzFrames = {4,5,6,7};

  std::vector<int> KuttaFrames = {0};

  EGTessModel tessModel( model, EGADSParams);

//#if SANS_AFLR
//  AFLR3 xfld(tessModel);
//#else
  EGTetGen xfld(tessModel, maxRadiusEdgeRatio, minDihedralAngle, true);
//#endif
  // grid:
  //EGTetGen xfld(model, EGADSParams, maxRadiusEdgeRatio, minDihedralAngle);

  std::cout << "Meshing Time: " << meshtime.elapsed() << std::endl;

  output_Tecplot( xfld, "tmp/NACA3D.dat" );

  //return;

  // Find the BC faces
  std::map<std::string, std::vector<int> > BCFaces;
  std::vector<int> WakeFaces;

  {
    int BCFace = 0;
    std::vector< EGBody<3> > bodies = model.getBodies();
    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
    {
      if (not bodies[ibody].isSolid()) continue;
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        if ( faces[i].hasAttribute("BCName") )
        {
          std::string BCName;
          faces[i].getAttribute("BCName", BCName);
          if ( BCName == "Lateral") BCName = "Outflow";
          //if ( BCName == "Lateral") BCName = "Inflow";
          BCFaces[BCName].push_back(BCFace);
          BCFace++;
        }
      }
    }

    for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
    {
      if (bodies[ibody].isSolid()) continue;
      std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        SANS_ASSERT ( faces[i].hasAttribute("BCName") );

        std::string BCName;
        faces[i].getAttribute("BCName", BCName);
        WakeFaces.push_back(BCFace);
        BCFace++;
      }
    }
  }

  // quadrature rule
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 2);    // max
  std::vector<int> quadratureOrderMin(xfld.nBoundaryTraceGroups(), 0);     // min

#else

  XField3D_Box_Hex_X1_WakeCut xfld( nChord, nSpan, span, nWake, true );

  std::map<std::string, std::vector<int> > BCFaces;

  BCFaces["Wall"] = {6,7};
  BCFaces["Inflow"] = {0};
  BCFaces["Outflow"] = {1,2,3,4,5};

  std::vector<int> WakeFaces = {8};

  std::vector<int> KuttaFrames = {0};
  std::vector<int> TrefftzFrames = {1};

  // quadrature rule
  std::vector<int> quadratureOrder(xfld.nBoundaryTraceGroups(), 2);    // max
  std::vector<int> quadratureOrderMin(xfld.nBoundaryTraceGroups(), 0);     // min


  output_Tecplot( xfld, "tmp/BoxWake.dat" );

#endif

  // BC
  PyDict BCWallDict;
  BCWallDict[BCParams::params.BC.BCType] = BCParams::params.BC.Wall;

  PyDict BCOutflowDict;
  BCOutflowDict[BCParams::params.BC.BCType] = BCParams::params.BC.Neumann;
  BCOutflowDict[BCNeumannParamsType::params.gradqn] = 0;

  PyDict BCInflowDict;
  BCInflowDict[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet;
  BCInflowDict[BCDirichletParamsType::params.q] = 0;

  PyDict PyBCList;
  PyBCList["Wall"] = BCWallDict;
  PyBCList["Inflow"] = BCInflowDict;
  PyBCList["Outflow"] = BCOutflowDict;

  checkBCInputs(PyBCList, BCFaces);

  // PDE
  Real Vinf = 1;

  Real alphamin = 10; //0.01*180/PI;
  int nalpha = 1;
  Real alphamax = 10; //0.01*180/PI;

  PDEClass pde( 1, 0, 0 );

  int order = 1;

  // solution: Hierarchical, C0
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  qfld = 0;

  const int nDOFPDE = qfld.nDOF();

  std::cout << " nDOFPDE = " << nDOFPDE << std::endl;
  std::cout << " Duplicates after " << xfld.dupPointOffset_ << std::endl;

  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> gamfld( xfld, order, BasisFunctionCategory_Hierarchical, WakeFaces );

  gamfld = 0;

  const int nDOFGAM = gamfld.nDOF();

#if 0
  cout << "nDOFPDE = " << nDOFPDE << endl;
  cout << "  InflowFaces = " << InflowFaces.size()
       << "  OutflowFaces = " << OutflowFaces.size()
       << "  WallFaces = " << WallFaces.size()
       << "  WakeFaces = " << WakeFaces.size() << endl;
  cout << "  lgfld.nBoundaryTraceGroups() = " << lgfld.nBoundaryTraceGroups() << endl;
#endif


  StabilizationNitsche stab(order);
  PrimalEquationSetClass PrimalEqSet(xfld, qfld, gamfld, pde, stab,
                                     {0}, WakeFaces, KuttaFrames, PyBCList, BCFaces);

  // linear system setup

  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize()), dq(q.size());
  q = 0;
  rsd = 0;
  dq = 0;

#ifdef INTEL_MKL
  SLA::MKL_PARDISO<SystemMatrixClass> solver(PrimalEqSet, SLA::RegularSolve, true);
#else
  SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet, SLA::RegularSolve, true);
#endif

  for ( int na = 0; na < nalpha; na++)
  {
    Real alpha = alphamin + na*(alphamax-alphamin)/std::max(1,nalpha-1);

    std::cout << "-----------------" << std::endl;
    std::cout << "alpha          = " << alpha << std::endl;
#if 0
    Real U = Vinf*cos(alpha*PI/180), V = Vinf*sin(alpha*PI/180), W = Vinf*0;
    DLA::VectorS<3, Real> Ddir = { U, V, W};
    DLA::VectorS<3, Real> Ldir = {-V, U, W};
#else
    Real U = Vinf*cos(alpha*PI/180), V = Vinf*0, W = Vinf*sin(alpha*PI/180);
    DLA::VectorS<3, Real> Ddir = { U, V, W};
    DLA::VectorS<3, Real> Ldir = {-W, V, U};
#endif
    //Update the angle of attack
    pde.setFreestream(Ddir);

    Ddir /= Vinf;
    Ldir /= Vinf;

    qfld = 1;
    for (int newton = 0; newton < 1; newton++)
    {
      rsd = 0;

      timer rsdtime;

      // residual

      PrimalEqSet.residual(q, rsd);

      std::cout << "Residual Time: " << rsdtime.elapsed() << std::endl;

      timer solvetime;

      solver.solve(rsd, dq);

      std::cout << "Solve Time: " << solvetime.elapsed() << std::endl;

      // update the solution
      q -= dq;

      // set the field
      PrimalEqSet.setSolutionField(q);

#if 0
      std::cout << std::setprecision(16) << "rsd = {";
      for (int n = 0; n < nDOFPDE; n++)
      {
        Real z = rsd[n];
        int exponent = z == 0 ? 0 : floor( log10( std::abs(z) ) );
        Real base     = z / pow(10., exponent);
        std::cout << base << " 10^" << exponent;
        if (n < nDOFPDE-1)
          std::cout << ", ";
      }
      std::cout << "};" << endl;
#endif

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      for (int n = 0; n < nDOFGAM; n++)
        rsdPDEnrm += pow(rsd[1][n],2);

      std::cout << "Newton " << newton << " rsd = " << sqrt(rsdPDEnrm) << std::endl;

      if (rsdPDEnrm < 1e-10) break;

      break;
    }
    // check that the residual is zero
#if 1
    rsd = 0;

    timer rsdtime;

    // residual

    PrimalEqSet.residual(q, rsd);

    Real rsdPDEnrm = 0;
    for (int n = 0; n < nDOFPDE; n++)
      rsdPDEnrm += pow(rsd[0][n],2);

    for (int n = 0; n < nDOFGAM; n++)
      rsdPDEnrm += pow(rsd[1][n],2);

    BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );
#endif
    Real Drag = 0;
    Real TrefftzLift = 0;
    DLA::VectorS<3,Real> PressureForce = 0;
    Real Vn2 = 0;
    quadratureOrder[0] = 2;
    quadratureOrder[1] = 2;

    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzInducedDrag, Real> fcnTrefftzDrag(pde, TrefftzFrames);
    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Real> fcnTrefftzLift(pde, Ldir, TrefftzFrames);
    IntegrandBoundary3D_LIP_Force<Real> fcnBodyForce(pde, BCFaces.at("Wall"));
    IntegrandBoundary3D_LIP_Vn2<Real> fcnVn(pde, BCFaces.at("Wall"));

    if ( withWake )
    {
      FunctionalTrefftz::integrate(fcnTrefftzDrag, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), Drag);
      FunctionalTrefftz::integrate(fcnTrefftzLift, xfld, qfld, &quadratureOrder[0], xfld.nBoundaryFrameGroups(), TrefftzLift);
    }
    //FunctionalBoundaryTrace_sansLG<TopoD3>::integrate(fcnBodyForce, qfld, &quadratureOrder[0], quadratureOrder.size(), PressureForce);
    //FunctionalBoundaryTrace_sansLG<TopoD3>::integrate(fcnVn, qfld, &quadratureOrder[0], quadratureOrder.size(), Vn2);
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( fcnBodyForce, PressureForce ), xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( fcnVn, Vn2 ), xfld, qfld, &quadratureOrder[0], quadratureOrder.size() );

    Real rho = 1;
    Real Vinf2 = U*U + V*V + W*W;

    // Simple NACA hershey bar
    Real Sref = span*1;

    // The RAE model is scaled by 100
    //Real Lref = (228.6 + 76.2)/2.*100;
    //Real Sref = 457.2*2*Lref*100;

    // Swept wing
    //Real Lref = (7.77964496612548828 - 4);
    //Real Sref = 13.2287569046020508*2*Lref;

    Drag /= (0.5*rho*Vinf2*Sref);
    TrefftzLift /= (0.5*rho*Vinf2*Sref);
    PressureForce /= (0.5*rho*Vinf2*Sref);

    std::cout << std::setprecision(16);
    std::cout << "Trefftz Drag   = " << Drag << std::endl;
    std::cout << "Trefftz Lift   = " << TrefftzLift << std::endl;
    std::cout << "Pressure Force = " << PressureForce << std::endl;
    std::cout << "Pressure Drag  = " << dot(PressureForce,Ddir) << std::endl;
    std::cout << "Pressure Lift  = " << dot(PressureForce,Ldir) << std::endl;
    std::cout << "L2 Vn  = " << sqrt(Vn2) << std::endl;

  }

  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;

#if 1
  // Tecplot dump
  string filename = "tmp/LIP_Adjoint.dat";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot_LIP( pde, qfld, filename );
  output_Tecplot( gamfld, "tmp/LIP_circAdjoint.dat", {"gamma"} );
#endif

#if 0
  for (int n = 0; n < nDOFPDE; n++)
    rsdfld.DOF(n) = rsd[n];
  output_Tecplot( rsdfld, "tmp/NACA0_LIPrsd.dat" );
#endif

#if 0
  // Tecplot dump
  string filenameLG = "tmp/NACA0_LG_LIP.dat";
  cout << "calling output_Tecplot: filename = " << filenameLG << endl;
  output_Tecplot_LIP( pde, lgfld, filenameLG );
#endif

  cout << "1st finished:" << endl;
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
