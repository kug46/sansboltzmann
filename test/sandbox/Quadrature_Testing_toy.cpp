// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php


#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to make filesystems
#include <boost/format.hpp> // to format
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <vector>
#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Quadrature/QuadratureSpacetime.h"
#include "Topology/ElementTopology.h"

#include "Field/Element/ElementXFieldSpacetime.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"

#include "unit/UnitGrids/XField4D_Box_Ptope_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

using namespace SANS;

#if 0

// local definitions
namespace
{

  Real function_case0(const Real &x,
                      const Real &y,
                      const Real &z,
                      const Real &t)
  {
    Real f= sin(PI*(x*x + y*y + z*z + t*t));
    return f;
  }

  Real IFunction_case0()
  {
    return -0.086862527278014279358;
  }

  Real function_case1(const Real &x,
                      const Real &y,
                      const Real &z,
                      const Real &t)
  {
    Real f= exp((x*x + y*y + z*z + t*t));
    return f;
  }

  Real IFunction_case1()
  {
    return 4.5768189778980675680;
  }

  Real function_case2(const Real &x,
                      const Real &y,
                      const Real &z,
                      const Real &t)
  {
    Real f= pow(x, 9) + pow(y, 9) + pow(z, 9) + pow(t, 9);
    return f;
  }

  Real IFunction_case2()
  {
    return 2.0/5.0;
  }

  Real function_case3(const Real &x,
                      const Real &y,
                      const Real &z,
                      const Real &t)
  {
    Real f= pow(x, 4) + pow(y, 4) + pow(z, 4) + pow(t, 4);
    return f;
  }

  Real IFunction_case3()
  {
    return 4.0/5.0;
  }

  Real function_case4(const Real &x,
                      const Real &y,
                      const Real &z,
                      const Real &t)
  {
    Real f= pow(x, 2) + pow(y, 2) + pow(z, 2) + pow(t, 2);
    return f;
  }

  Real IFunction_case4()
  {
    return 4.0/3.0;
  }

  void testCase(const int caseNo, const int ii,
                Real (*function_case)(const Real &, const Real &, const Real &, const Real &),
                Real (*IFunction_case)(),
                const int xMin= 0.0, const int xMax= 0.0,
                const std::vector<int> &degrees_to_test= {1, 2, 3, 5, 6, 8})
  {
    if (caseNo < 0 || caseNo > 4)
      SANS_DEVELOPER_EXCEPTION("that case doesn't exist...");

    mpi::communicator world;

    std::string casetext= "case" + to_string(caseNo);

    std::string filename_base= "tmp/Quadrature_Testing/";

    if (world.rank() == 0)
      boost::filesystem::create_directories(filename_base + casetext);

    std::string quadfile_filename= filename_base + casetext + "/results.txt";
    fstream quadfile;

    bool fileStarted= boost::filesystem::exists(quadfile_filename);

    if (world.rank() == 0)
    {
      quadfile.open(quadfile_filename, fstream::app);
      BOOST_REQUIRE_MESSAGE(quadfile.good(), "Error opening file: " + quadfile_filename);

      if (!fileStarted)
      {
        quadfile << boost::format("%8s%8s%8s%24s%24s%24s") % "CASE" % "DEGREE"
            % "II" % "IEXACT" % "IQUAD" % "ERRORI" << std::endl;
      }
    }

    // generate an ii x ii x ii x ii box
    XField4D_Box_Ptope_X1 xfld(world, ii, ii, ii, ii);

    // XField element
    int order= 1; // needs an order, not used for anything here
    ElementXField<PhysD4, TopoD4, Pentatope> xElem(order, BasisFunctionCategory_Lagrange);
    typename ElementXField<PhysD4, TopoD4, Pentatope>::VectorX X;

    for (const int &degree : degrees_to_test)
    {

      // get the quadrature
      QuadratureSpacetime<Pentatope> quad(degree);

      Real I= 0.0;
      const Real Iexact= IFunction_case();

      // loop over cellgroups
      for (int iCG= 0; iCG < xfld.nCellGroups(); iCG++)
      {
        // loop over elements
        for (int iElem= 0; iElem < xfld.getCellGroupBase(iCG).nElem(); iElem++)
        {
          xfld.getCellGroup<Pentatope>(iCG).getElement(xElem, iElem);

          // loop over quadrature points
          for (int iQuad= 0; iQuad < quad.nQuadrature(); iQuad++)
          {
            Real sRef;
            Real tRef;
            Real uRef;
            Real vRef;
            Real wgt;

            Real x;
            Real y;
            Real z;
            Real t;

            quad.coordinates(iQuad, sRef, tRef, uRef, vRef);
            quad.weight(iQuad, wgt);

            Real vol= xElem.volume();

            xElem.coordinates(sRef, tRef, uRef, vRef, X);

            x= X[0];
            y= X[1];
            z= X[2];
            t= X[3];

            Real f= function_case(x, y, z, t);

            // printf("f(%f, %f, %f, %f)= %f!\n", x, y, z, t, f);
            // printf("wgt= %f, vol= %f\n", wgt, vol);

            I += wgt*f*vol;

          } // iQuad
        } // iElem
      } // iCG

      const Real errorI= I - Iexact;

      // if (degree >= 2)
      //   BOOST_REQUIRE_CLOSE(I, Iexact, 1e-10);

      if (world.rank() == 0)
      {
        quadfile << boost::format("%8s%8d%8d%24.20f%24.20f%24.20f") % casetext
            % degree % ii % Iexact % I % errorI << std::endl;
      }

    } // degree

    quadfile.close();

  } // testCase

}

// -------------------------------------------------------------------------- //
BOOST_AUTO_TEST_SUITE(Quadrature_Testing_test_suite)

// -------------------------------------------------------------------------- //
BOOST_AUTO_TEST_CASE(Quadrature_Testing_case3_test)
{
  const int iiMin= 2;
  const int iiMax= 8;
  int caseNo;

  for (int ii= iiMin; ii < iiMax; ii++)
  {
    caseNo= 0;
    testCase(caseNo, ii, function_case0, IFunction_case0);

    caseNo= 1;
    testCase(caseNo, ii, function_case1, IFunction_case1);

    caseNo= 2;
    testCase(caseNo, ii, function_case2, IFunction_case2);

    caseNo= 3;
    testCase(caseNo, ii, function_case3, IFunction_case3);

    caseNo= 4;
    testCase(caseNo, ii, function_case4, IFunction_case4);
  }
}

BOOST_AUTO_TEST_SUITE_END()

#endif
