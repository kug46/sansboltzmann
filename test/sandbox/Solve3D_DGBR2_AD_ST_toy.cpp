// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve3D_DGBR2_AD_ST_toy
// testing of 2D+T convergence on an advection diffusion problem
// learning to SANS case for Cory... don't @ me

#include <boost/test/unit_test.hpp>
#include <boost/filesystem/path.hpp>          // to make filesystems
#include <boost/filesystem/operations.hpp>    // to make filesystems
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"               // saw a comment that this gives us Reals

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

// these give us tools to convert the 2D physical problem to a
// 3D (2D+T) adaptation problem
#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime2D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime2D.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "pde/ForcingFunction2D_MMS.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"
#include "unit/UnitGrids/XField3D_Box_Hex_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif

// #define HEX_GRIDS
// #define REFINE_UNSTRUCTURED
#undef SANS_AVRO

using namespace std;

// explicitly instantiate the classes for correct coverage info
namespace SANS
{
}

using namespace SANS;

static int factorial(int n)
{
  int m;
  if (n <= 1)
    m= 1;
  else
    m= n*factorial(n - 1);

  return m;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE(Solve3D_DGBR2_AD_ST_toy_test_suite)

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Solve3D_DGBR2_AD_ST_toy_test_suite)
{

  // parse inputs

  int argc= boost::unit_test::framework::master_test_suite().argc;
  char ** argv= boost::unit_test::framework::master_test_suite().argv;

  int iiSamples= -1;
  int pSamples= -1;

#ifndef REFINE_UNSTRUCTURED
  // specify m (m x m x m x m grid) and p
  if (argc == 3)
  {
    iiSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  else if (argc == 2)
  {
    iiSamples= std::stoi(argv[1]);
    pSamples= 1;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  else
  {
    iiSamples= 2;
    pSamples= 1;
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
#else
  std::string filename_mesh= "";

  // specify m (m x m x m x m grid) and p
  if (argc == 4)
  {
    iiSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    filename_mesh= argv[3];
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  else if (argc == 3)
  {
    iiSamples= std::stoi(argv[1]);
    pSamples= std::stoi(argv[2]);
    filename_mesh= "tmp/unif_meshes/n" + to_string(iiSamples) + ".json";
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  else if (argc == 2)
  {
    iiSamples= std::stoi(argv[1]);
    pSamples= 1;
    filename_mesh= "tmp/unif_meshes/n" + to_string(iiSamples) + ".json";
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
  else
  {
    iiSamples= 4;
    pSamples= 1;
    filename_mesh= "tmp/unif_meshes/n" + to_string(iiSamples) + ".json";
    std::cout << std::endl << "Input parsed:" << std::endl;
  }
#endif

  SANS_ASSERT(iiSamples > 0);
  SANS_ASSERT(pSamples > 0);

  // specify physical problem

  Real ax= 0.5;
  Real ay= 0.5;
  Real nu= 0.0;

  Real lambda_exact= 5.0;

  std::cout << "\tiiSamples: " << iiSamples << std::endl;
  std::cout << "\tpSamples: " << pSamples << std::endl << std::endl;

  std::cout << "Physical problem:" << std::endl;
  std::cout << "\ta: (" << ax << ", " << ay << ")" << std::endl;
  std::cout << "\tnu: " << nu << std::endl << std::endl;

#ifndef BOUNDARY_LAYER
  std::cout << "Running decaying sines MMS problem!" << std::endl;
  std::cout << "\tlambda: " << lambda_exact << std::endl << std::endl;
#else
  std::cout << "Running boundary layer MMS problem!" << std::endl;
  std::cout << "\tepsilon: " << epsilon_exact << std::endl;
  std::cout << "\tbeta: " << beta_exact << std::endl << std::endl;
#endif

#ifndef REFINE_UNSTRUCTURED
  std::string filename_base= "tmp/AD3D_ST_unifconv/";
#else
  std::string filename_base= "tmp/AD3D_ST_unifconv_unstructured/";
#endif
  std::string filename_case;

  typedef AdvectiveFlux2D_Uniform AdvectionModel;
  typedef ViscousFlux2D_Uniform DiffusionModel;
  typedef Source2D_UniformGrad SourceModel;

  // shortcut for the appropriate PDE object for our 2D physics specified
  typedef PDEAdvectionDiffusion<PhysD2, AdvectionModel, DiffusionModel, SourceModel> PDEClass;
  // shortcut for the 2D+T conversion
  typedef PDENDConvertSpaceTime<PhysD2, PDEClass> NDPDEClass;

  // the exact solution... decaying-in-time spatial sine-cosine product
  typedef ScalarFunction2D_Gaussian SolutionExact;
  // typedef ScalarFunction2D_SineCosineDecay SolutionExact;
  // shortcut to the conversion for the exact solution to spacetime
  typedef SolnNDConvertSpaceTime<PhysD2, SolutionExact> NDSolutionExact;

  typedef BCAdvectionDiffusion2DVector<AdvectionModel, DiffusionModel> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpaceTime<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  // the solution to the problem
  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass,
      ParamBuilderType> SolutionClass;
  // parameters to the solver
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  // algebraic equation set
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime,
      BCVector, AlgEqSetTraits_Sparse,
      DGBR2, ParamFieldType> PrimalEquationSetClass;
  // solver interface, everything it needs to run the solution
  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass,
      OutputIntegrandClass> SolverInterfaceClass;

  // mpi
  mpi::communicator world;

  // CREATE ADVECTION DIFFUSION PDE

  AdvectionModel advection(ax, ay);
  DiffusionModel diffusion(nu, 0, 0, nu);
  SourceModel source(0.0, 0.0, 0.0);

  //  // create the PDE
  //  NDPDEClass pde(advection, diffusion, source);

  // create exact solution
  PyDict solnArgs;
  // solnArgs[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function.Name]=
  //     BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function.SineCosineDecay;
  // solnArgs[NDSolutionExact::ParamsType::params.lambda]= lambda_exact;
  solnArgs[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function.Name]=
      BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function.Gaussian;
  solnArgs[NDSolutionExact::ParamsType::params.a]= 1.0;
  solnArgs[NDSolutionExact::ParamsType::params.x0]= 0.5;
  solnArgs[NDSolutionExact::ParamsType::params.y0]= 0.5;
  solnArgs[NDSolutionExact::ParamsType::params.sx]= 1.0;
  solnArgs[NDSolutionExact::ParamsType::params.sy]= 1.0;

  NDSolutionExact solnExact(solnArgs);

  // create forcing function for MMS
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  // overwrite pde with forcing function
  NDPDEClass pde(advection, diffusion, source, forcingptr);

  // CREATE BOUNDARY CONDITIONS

  // awaiting implementation
  //  PyDict BCSoln_timeIC;
  //  BCSoln_timeIC[BCParams::params.BC.BCType]= BCParams::params.BC.Function_mitState;

  PyDict BCSoln_timeIC;
  BCSoln_timeIC[BCParams::params.BC.BCType]= BCParams::params.BC.TimeIC_Function;
  BCSoln_timeIC[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;

  PyDict BCSoln_timeOut;
  BCSoln_timeOut[BCParams::params.BC.BCType]= BCParams::params.BC.TimeOut;

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType]= BCParams::params.BC.Function_mitState;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Function]= solnArgs;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.SolutionBCType]=
      BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params.Upwind]= false;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType]= BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCSoln_timeIC"]= BCSoln_timeIC;
  PyBCList["BCSoln_timeOut"]= BCSoln_timeOut;
  PyBCList["BCSoln_Dirichlet"]= BCSoln_Dirichlet;
  PyBCList["BCNone"]= BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // CREATE DISCRETIZATION PARAMETERS

  Real viscousEtaParameter= 2*Tet::NTrace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType= ResidualNorm_L2;
  std::vector<Real> tol= {1.0e-10, 1.0e-10};

  // CREATE OUTPUT FUNCTIONAL TO ADAPT WITH

  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // CREATE INTEGRAND FOR EXACT SOLN ERROR

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // SETUP NONLINEAR SOLVER (PROBABLY UNNEEDED FOR LINEAR PROBLEM)

  // nonlinear solver dicts
  PyDict SolverContinuationDict;
  PyDict NonlinearSolverDict;
  PyDict NewtonSolverDict;
  PyDict AdjLinearSolverDict;
  PyDict LinearSolverDict;
  PyDict LineUpdateDict;
  // PyDict UMFPACKDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 )
    std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 3;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
  //  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
  NewtonSolverDict[NewtonSolverParam::params.Verbose]= false;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver]= SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  // LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method]= LineUpdateParam::params.LineUpdate.HalvingSearch;

  // NewtonSolverDict[NewtonSolverParam::params.LinearSolver]= UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate]= LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations]= 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations]= 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose]= true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]= NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation]= NonlinearSolverDict;

  SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict);

  // REFINEMENT LOOP
  // uniformly refine in h and in p

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_base);

  // prep output files and data

  // norm data
  Real orderVec;
  Real hDOFVec;
  int nElemVec;
  int nDOFVec;
  Real normVec;

  // output file
  std::string convhist_filename= filename_base + "test.convhist";
  fstream convhist;

  bool fileStarted= boost::filesystem::exists(convhist_filename);

  if (world.rank() == 0)
  {
    convhist.open(convhist_filename, fstream::app);
    BOOST_REQUIRE_MESSAGE(convhist.good(), "Error opening file: " + convhist_filename);

    if (!fileStarted)
    {
      convhist << "VARIABLES=";
      convhist << "\"p\"";
      convhist << ", \"DOFrequested\"";
      convhist << ", \"1/pow(DOF, 1.0/3.0)\"";
      convhist << ", \"Nelem\"";
      convhist << ", \"Ndof\"";
      convhist << ", \"L2 error\"";
      convhist << std::endl;
    }

    convhist << std::setprecision(16) << std::scientific;
  }

  std::vector<int> cellGroups= {0};


  // CREATE INITIAL GRID

  // 5x5x5
  const int ii= iiSamples;
  const int jj= ii;
  const int kk= jj;

#ifndef REFINE_UNSTRUCTURED
  const Real xmin= 0.0;
  const Real xmax= 1.0;
  const Real ymin= 0.0;
  const Real ymax= 1.0;
  const Real tmin= 0.0;
  const Real tmax= 1.0;

#ifdef HEX_GRIDS
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld= std::make_shared<XField3D_Box_Hex_X1>(world, ii, jj, kk, xmin, xmax, ymin, ymax, tmin, tmax);
#else
  std::shared_ptr<XField<PhysD3, TopoD3>> pxfld= std::make_shared<XField3D_Box_Tet_X1>(world, ii, jj, kk, xmin, xmax, ymin, ymax, tmin, tmax);
#endif
#else
  using avro::coord_t;
  using avro::index_t;

  avro::Context context;

  coord_t number= 3;                        // topological dimension
  Real x0[3]= {0., 0., 0.};
  std::vector<Real> lens(number, 1.);
  std::shared_ptr<avro::Body> pbody= std::make_shared<avro::library::EGADSBox>(&context, x0, lens.data());
  std::shared_ptr<avro::Model> model= std::make_shared<avro::Model>(&context, "box");
  model->addBody(pbody,false);

  avro::Mesh<avro::Simplex> mesh(number, number);

  std::shared_ptr<XField_avro<PhysD3, TopoD3>> pxfld_avro= std::make_shared<XField_avro<PhysD3, TopoD3>>(world, model);
  std::shared_ptr<XField_avro<PhysD3, TopoD3>> pxfld= pxfld_avro;

  avro::io::readMesh(filename_mesh, mesh);

  mesh.vertices().findGeometry(*pbody);

  pxfld_avro->import(mesh);

 //pxfld.attachToGeometry();

#endif

  const int order= pSamples;

#ifdef HEX_GRIDS
  const int iXmin= XField3D_Box_Hex_X1::iXmin;
  const int iXmax= XField3D_Box_Hex_X1::iXmax;
  const int iYmin= XField3D_Box_Hex_X1::iYmin;
  const int iYmax= XField3D_Box_Hex_X1::iYmax;
  const int iTmin= XField3D_Box_Hex_X1::iZmin;
  const int iTmax= XField3D_Box_Hex_X1::iZmax;
#else
  const int iXmin= XField3D_Box_Tet_X1::iXmin;
  const int iXmax= XField3D_Box_Tet_X1::iXmax;
  const int iYmin= XField3D_Box_Tet_X1::iYmin;
  const int iYmax= XField3D_Box_Tet_X1::iYmax;
  const int iTmin= XField3D_Box_Tet_X1::iZmin;
  const int iTmax= XField3D_Box_Tet_X1::iZmax;
#endif

  // define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSoln_timeIC"]= {iTmin};
  BCBoundaryGroups["BCSoln_timeOut"]= {iTmax};
  // BCBoundaryGroups["BCSoln_Dirichlet"]= {iXmin, iYmin, iXmax, iYmax};
  BCBoundaryGroups["BCSoln_Dirichlet"]= {iXmin, iYmin};
  BCBoundaryGroups["BCNone"]= {iXmax, iYmax};

  std::vector<int> active_boundaries= BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  std::vector<int> interiorTraceGroups;

  for (int iT= 0; iT < pxfld->nInteriorTraceGroups(); iT++)
    interiorTraceGroups.push_back(iT);

  // redeclare solution data for actually solving ST problem
  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol= std::make_shared<SolutionClass>(*pxfld, pde, order, order + 1,
                                              BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre, active_boundaries, disc);

  const int quadOrder= 2*(order + 1);    // somehow this means 2*(order + 1)?

  // create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface= std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                     cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups, SolverContinuationDict,
                                                     AdjLinearSolverDict, outputIntegrand);

  // set initial solution
  //  pGlobalSol->setSolution(solnExact, cellGroups);
  pGlobalSol->setSolution(0.0);

  filename_case= filename_base + "n" + stringify(ii) + "p" + stringify(order) + "/";

  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_case);

  std::string qfld_init_filename= filename_case + "qfld_init_a0.plt";
  output_Tecplot(pGlobalSol->primal.qfld, qfld_init_filename);

  pInterface->solveGlobalPrimalProblem();
  //      pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename= filename_case + "qfld_a0.plt";
  output_Tecplot(pGlobalSol->primal.qfld, qfld_filename);

  //      std::string adjfld_filename= filename_case + "adjfld_a0.plt";
  //      output_Tecplot(pGlobalSol->adjoint.qfld, adjfld_filename);

  // compute exact error
  QuadratureOrder quadratureOrder(pGlobalSol->primal.qfld.getXField(), quadOrder);

  Real squareError= 0;

  IntegrateCellGroups<TopoD3>::integrate(
      FunctionalCell_DGBR2(errorIntegrand, squareError),
      pGlobalSol->primal.qfld.getXField(), (pGlobalSol->primal.qfld, pGlobalSol->primal.rfld),
      quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size());

#ifdef SANS_MPI
  squareError= boost::mpi::all_reduce(*pxfld->comm(), squareError, std::plus<Real>());

  int nDOFtotal = 0;
  boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );

  // count the number of elements possessed by this processor
  int nElem = 0;
  for (int elem = 0; elem < pxfld->nElem(); elem++ )
#ifdef HEX_GRIDS
    if (pxfld->getCellGroupGlobal<Hex>(0).associativity(elem).rank() == world.rank())
#else
    if (pxfld->getCellGroupGlobal<Tet>(0).associativity(elem).rank() == world.rank())
#endif
      nElem++;

  int nElemtotal = 0;
  boost::mpi::reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>(), 0);
#else
  int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
  int nElemtotal = pxfld->nElem();
#endif

  Real norm= squareError;

  orderVec= order;
  nElemVec= nElemtotal;
  nDOFVec= nDOFtotal;
  hDOFVec= 1.0/pow(nDOFtotal, 1.0/((Real) PhysD3::D));
  normVec= sqrt(norm);

  if (world.rank() == 0)
  {
    // outputfile
    convhist << orderVec;
    convhist << ", " << ii*jj*kk*factorial(order + PhysD3::D)/factorial(order);
    convhist << ", " << hDOFVec;
    convhist << ", " << nElemVec;
    convhist << ", " << nDOFVec;
    convhist << ", " << normVec;
    convhist << endl;
  }

  if (world.rank() == 0)
  {
    convhist.close();
    std::cout << "Output file written to " << convhist_filename << "." << std::endl;
  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
