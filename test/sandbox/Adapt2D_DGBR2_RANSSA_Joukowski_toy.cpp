// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_RANSSA_Joukowski_btest
// Testing of the MOESS framework on a 2D RANS-SA problem


#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include <boost/filesystem.hpp> // to automagically make the directories

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"
#include "Meshing/EPIC/XField_PX.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/output_grm.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_RANSSA_Joukowski_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_RANSSA_Joukowski_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass> BCVector;

  typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_Entropy<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_Distance ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

   mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2_DOFWeighted;
  std::vector<Real> tol = {1e-10, 1e-10};


  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;           // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.2;
  const Real Reynolds = 1e6;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real tRef = 1;          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                              // velocity scale
  const Real aoaRef = 16.0*PI/180.0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Entropy );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, SAnt2D<DensityVelocityPressure2D<Real>>({rhoRef, uRef, vRef, pRef, ntRef}) );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);


  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;
//  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipIsothermal_mitState;
//  BCNoSlip[BCNavierStokes2DParams<BCTypeWallNoSlipIsothermal_mitState>::params.Twall] = tRef;

  PyDict StateVector;
  StateVector[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.Conservative;
  StateVector[Conservative2DParams::params.rho] = rhoRef;
  StateVector[Conservative2DParams::params.rhou] = rhoRef*uRef;
  StateVector[Conservative2DParams::params.rhov] = rhoRef*vRef;
  const Real ERef = Cv*tRef + 0.5*(uRef*uRef + vRef*vRef);
  StateVector[Conservative2DParams::params.rhoE] = rhoRef*ERef;
  StateVector["rhont"]  = rhoRef*ntRef; //TODO: not sure about this, was previously just "nt" which gave a Python dictionary error

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = StateVector;
  BCIn[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {0,1};
  BCBoundaryGroups["BCIn"] = {2,3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  // Drag output
  NDOutputClass outputFcn(pde, cos(aoaRef), sin(aoaRef));
  OutputIntegrandClass outputIntegrand( outputFcn, {0,1} );

  // DRAG INTEGRAND
  typedef BCRANSSA2D<BCTypeWall_mitState, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCClass;

  NDBCClass bc(pde);
  IntegrandBCClass fcnBC( pde, bc, {0,1}, disc );

//  NDOutputClass outputFcn2(pde, -sin(aoaRef), cos(aoaRef));
  NDOutputClass outputFcn2(pde, -sin(aoaRef), cos(aoaRef));
  OutputIntegrandClass outputIntegrand2( outputFcn2, {0,1} );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;
//
//  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.ReverseCuthillMcKee;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;
//  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
//  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Left;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-20;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 100;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.Memory] = true;
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = true;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
//  PETScDict[SLA::PETScSolverParam::params.KSPSolver] = SLA::PETScSolverParam::params.KSPSolver.BICGStab;
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict), PreconditionerAdjoint(PreconditionerDict);
//  PreconditionerAdjoint[SLA::PreconditionerASMParam::params.Overlap] = 2;
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-15;
  PETScDictAdjoint[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-20;
  PETScDictAdjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDictAdjoint[SLA::PETScSolverParam::params.GMRES_Restart] = 200;
  PETScDictAdjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerAdjoint;
//  PETScDictAdjoint[SLA::PETScSolverParam::params.ResidualHistoryFile] = filename_base + "PETSc_adjoint_residual.dat";

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
    std::cout << "Linear solver: MKL_PARDISO" << std::endl;
    PyDict MKL_PARDISODict;
    MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;

#else
    std::cout << "Linear solver: UMFPACK" << std::endl;
    PyDict UMFPACKDict;
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

#if 0
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#else
  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
//  NewtonSolverDict[NewtonSolverParam::params.MaxChangeFraction] = 0.1;


  PyDict ParamDict;

  int orderL = 1, orderH = 2;
  int powerL = 2, powerH = 2;

  int maxIter = 30;
  int restartIter = -1;
  std::string restartFileBase = "";

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    orderL = orderH = std::stoi(argv[1]);
  if (argc >= 3)
    powerL = powerH = std::stoi(argv[2]);
  if (argc >= 4)
    maxIter = std::stoi(argv[3]);
  if (argc >= 5)
    restartIter = std::stoi(argv[4]);
  if (argc >= 6)
    restartFileBase = argv[5];

  if (world.rank() == 0)
  {
    std::cout << ", orderL,H = " << orderL << ", " << orderH;
    std::cout << ", powerL,H = " << powerL << ", " << powerH << ", maxIter: " << maxIter << std::endl;
  }
#endif

  std::string filename_base = "";
  for ( int order = orderL; order<=orderH; order++)
  {

    for (int power=powerL; power<=powerH; power++)
    {
      Real targetCost = 500.0*pow(2.0,power);

      // Grid

      //#define RESTART

      // to make sure folders have a consistent number of zero digits
      const int string_pad = 5;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);


//      restartFileBase = filename_base;
      filename_base = "tmp/RANSJOUK_ALPHA/DG/DG_" + int_pad + "_P" + std::to_string(order) + "/";

      boost::filesystem::create_directories(filename_base);

      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfld_linear;

      std::shared_ptr<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>> prestartfld;
      int startIter = 0;
      bool isRestart = false;

      if (restartIter >= 0)
      {
        startIter = restartIter;
        std::string file_initial_xfld = restartFileBase + "restart.grm";
        std::string file_initial_linear_mesh = restartFileBase + "restart_lin.grm";
        std::string file_initial_qfld = restartFileBase + "restart.qrm";

        if (world.rank() == 0)
          std::cout<< "Starting from " << file_initial_xfld << " and " << file_initial_qfld << std::endl;

        pxfld = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, file_initial_xfld);
        pxfld_linear = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, file_initial_linear_mesh);

        prestartfld = std::make_shared<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>>(*pxfld, order, BasisFunctionCategory_Legendre);

        ReadSolution_PX(file_initial_qfld, *prestartfld);

        isRestart = true;

      }
      else //start from scratch
      {

        if (world.rank() == 0)
          std::cout<< "Starting from scratch" << std::endl;

        std::string file_initial_mesh = "grids/joukowski_initial_mesh/jouk_q3.grm";
        std::string file_initial_linear_mesh = "grids/joukowski_initial_mesh/jouk_q1_out.grm";

        pxfld = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, file_initial_mesh);
        pxfld_linear = std::make_shared<XField_PX<PhysD2, TopoD2>>(world, file_initial_linear_mesh);
      }
      //--------ADAPTATION LOOP--------

      std::string adapthist_filename = filename_base + "test.adapthist";

      fstream fadapthist;

      if (world.rank() == 0)
      {
        if (isRestart)
          fadapthist.open( adapthist_filename, fstream::app );
        else
          fadapthist.open( adapthist_filename, fstream::out );

        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
      }


#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 1e3;
  NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor] = 1.5;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;
  NonlinearSolverDict[PseudoTimeParam::params.SteveLogic] = true;
  NonlinearSolverDict[PseudoTimeParam::params.RequireConvergedNewton] = false;
  NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "residhist.dat";
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;
      MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANSparallel;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
      MesherDict[EpicParams::params.CurvedQOrder] = 3; //Mesh order
      MesherDict[EpicParams::params.CSF] = "grids/joukowski_initial_mesh/jouk.csf"; //Geometry file
      MesherDict[EpicParams::params.GeometryList2D] = "Airfoil Farfield"; //Geometry list
      MesherDict[EpicParams::params.minGeom] = 1e-7;
      MesherDict[EpicParams::params.minH] = 1e-7;
      MesherDict[EpicParams::params.maxGeom] = 100.;
      MesherDict[EpicParams::params.maxH] = 100.;
      MesherDict[EpicParams::params.nPointGeom] = -1;
      MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;
      MesherDict[EpicParams::params.nThread] = 2;
      MesherDict[EpicParams::params.Version] = "madcap_devel-10.2";

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Compute distance field
      std::shared_ptr<Field_CG_Cell<PhysD2, TopoD2, Real>>
      pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfld, 4, BasisFunctionCategory_Lagrange);
      DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));

      //Solution data
      typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>((*pdistfld, *pxfld), pde, order, order+1,
          BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
          active_boundaries, ParamDict, disc);

      if (!isRestart)
        output_Tecplot( get<0>(pGlobalSol->paramfld), filename_base + "distfld_a0.plt" );

      const int quadOrder = 2*(order + 1); // Is this high enough?

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand2);

      //Set initial solution
      pGlobalSol->setSolution(q0);

      if (!isRestart)
      {
        std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );
      }

      pInterface->solveGlobalPrimalProblem();

      std::string xfld_restart_filename =  filename_base + "restart.grm";
      std::string xfld_linear_restart_filename =  filename_base + "restart_lin.grm";
      std::string qfld_restart_filename =  filename_base + "restart.qrm";
      pInterface->writeRestart(xfld_restart_filename, qfld_restart_filename);
      WriteMeshGrm<PhysD2, TopoD2>(*pxfld_linear, xfld_linear_restart_filename);

      pInterface->solveGlobalAdjointProblem();
      //Compute error estimates
      pInterface->computeErrorEstimates();

      if (!isRestart)
      {
        std::string qfld_filename = filename_base + "qfld_a0.plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a0.plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

        std::string rfld_filename = filename_base + "rfld_a0.plt";
        std::string sfld_filename = filename_base + "sfld_a0.plt";
        std::string efld_filename = filename_base + "efld_a0.plt";

        if (world.rank() == 0)
        {
          std::cout<< "output e and rflds" <<std::endl;
        }
        pInterface->output_EField(efld_filename);
      }

      for (int iter = startIter; iter < maxIter+1; iter++)
      {
        if (world.rank() == 0)
        {
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;
        }


        //Perform local sampling and adapt mesh
        MeshAdapter<PhysD2, TopoD2>::MeshPtrPair ptr_pair;
        ptr_pair = mesh_adapter.adapt(*pxfld_linear, *pxfld, cellGroups, *pInterface, iter);

        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew_linear = ptr_pair.first;
        std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew = ptr_pair.second;

        interiorTraceGroups.clear();
        for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        //Compute distance field
        pdistfld = std::make_shared<Field_CG_Cell<PhysD2, TopoD2, Real>>(*pxfldNew, 4, BasisFunctionCategory_Lagrange);
        DistanceFunction(*pdistfld, BCBoundaryGroups.at("BCNoSlip"));

        std::shared_ptr<SolutionClass> pGlobalSolNew;
        pGlobalSolNew = std::make_shared<SolutionClass>((*pdistfld, *pxfldNew), pde, order, order+1,
                                                        BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                        active_boundaries, ParamDict, disc);

        //Perform L2 projection from solution on previous mesh
        pGlobalSolNew->setSolution(*pGlobalSol, true);

        std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
        pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                               cellGroups, interiorTraceGroups,
                                                               PyBCList, BCBoundaryGroups,
                                                               SolverContinuationDict, LinearSolverDict,
                                                               outputIntegrand2);

        //Update pointers to the newest problem (this deletes the previous mesh and solutions)
        pxfld_linear = pxfldNew_linear;
        pxfld = pxfldNew;
        pGlobalSol = pGlobalSolNew;
        pInterface = pInterfaceNew;

        std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

        pInterface->solveGlobalPrimalProblem();

        pInterface->writeRestart(xfld_restart_filename, qfld_restart_filename);
        WriteMeshGrm<PhysD2, TopoD2>(*pxfldNew_linear, xfld_linear_restart_filename);

        pInterface->solveGlobalAdjointProblem();
        //Compute error estimates
        pInterface->computeErrorEstimates();

         std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

        std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + ".plt";
        std::string rfld_filename = filename_base + "rfld_a" + std::to_string(iter+1) + ".plt";
        std::string sfld_filename = filename_base + "sfld_a" + std::to_string(iter+1) + ".plt";

        if (world.rank() == 0)
        {
          std::cout<< "output e and rflds" <<std::endl;
        }
        pInterface->output_EField(efld_filename);

        std::string xfld_linear_filename = filename_base + "xfld_linear_a" + std::to_string(iter+1) + ".grm";
        std::string xfld_filename = filename_base + "xfld_a" + std::to_string(iter+1) + ".grm";

        output_grm( *pxfld_linear, xfld_linear_filename);
        output_grm( *pxfld, xfld_filename);
      }
      restartIter = 0;
      maxIter = 15;
      fadapthist.close();
    }
  }
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
