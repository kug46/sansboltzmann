// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_TwoPhase_ST_QuarterFiveSpot_btest
// Testing of the MOESS framework on a space-time two-phase problem

#define USE_PETSC_SOLVER

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity2D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/OutputTwoPhase.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/VectorFunction2D.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime2D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "Field/output_Tecplot_PDE.h"

#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#endif

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_TwoPhase_ArtificialViscosity_ST_QuarterFiveSpot_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_TwoPhase_ArtificialViscosity_ST_QuarterFiveSpot_Triangle )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef PermeabilityModel2D_QuadBlock RockPermModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel,
                              CapillaryModel> TraitsModelTwoPhaseClass;

  typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                            TraitsModelTwoPhaseClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_TwoPhase<PDEBaseClass> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTwoPhaseArtificialViscosity2DVector<TraitsSizeTwoPhaseArtificialViscosity,
                                                TraitsModelAV> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef SourceTwoPhase2D_FixedPressureOutflow<TraitsSizeTwoPhaseArtificialViscosity,
                                                TraitsModelTwoPhaseClass> SourceOutflowClass;
  typedef SourceOutflowClass::ParamsType SourceOutflowParamClass;

  typedef OutputTwoPhase2D_Flowrate<SourceOutflowClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_GenH_DG ParamBuilderType;
  typedef GenHField_DG<PhysD3, TopoD3> GenHFieldType;

  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef SolutionData_DGBR2<PhysDim, TopoDim, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  timer clock;

  const int order = 1;
  const int order_min = 1; //for p-sequencing

  int maxIter = 20;
  Real targetCost = 250000;
  std::string filename_base = "tmp/";

  const Real Lx = 3000.0; //ft
  const Real Ly = Lx; //ft
  const Real T = 4000.0; //days

//  XField3D_Box_Tet_X1 xfld_tmp( 10, 10, 10, 0, Lx, 0, Ly, 0, T );
//  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld_tmp(xfld_tmp, 1, BasisFunctionCategory_Legendre);

//  std::fstream fqfld("tmp/qfld_init_R100.txt", std::fstream::in);
//  for (int i = 0; i < qfld_tmp.nDOF(); i++)
//    for (int j = 0; j < ArrayQ::M; j++)
//      fqfld >> qfld_tmp.DOF(i)[j];
//  fqfld.close();

  // Grid
  std::shared_ptr<XField<PhysDim, TopoDim>> pxfld;
  PyDict MesherDict;

#ifdef SANS_AVRO
  MesherDict[MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.Name] = MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.avro;
  MesherDict[avroParams::params.Curved] = false; // is the grid curved?
  MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
  MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files
  using avro::coord_t;
  using avro::index_t;

  // create the context
  std::shared_ptr<avro::Context> context = std::make_shared<avro::Context>();

  std::vector<avro::real> x0 = {0., 0., 0.};
  std::vector<avro::real> xL = {Lx, Ly, T};
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>(context.get(), x0.data(), xL.data());
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(), "cube");
  model->addBody(pbody,true);

  XField3D_Box_Tet_X1 xfld0( world, 10, 10, 10, 0, Lx, 0, Ly, 0, T );
  // copy the mesh into the domain and attach the geometry
  pxfld = std::make_shared< XField_avro<PhysDim,TopoDim> >(xfld0, model);
#else
  pxfld = std::make_shared<XField3D_Box_Tet_X1>( world, 10, 10, 10, 0, Lx, 0, Ly, 0, T );

  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.FeFloa;
//  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;
//  MesherDict[EpicParams::params.Version] = "madcap_devel-10.1";
#endif

  const int iXmin = XField3D_Box_Tet_X1::iXmin;
  const int iXmax = XField3D_Box_Tet_X1::iXmax;
  const int iYmin = XField3D_Box_Tet_X1::iYmin;
  const int iYmax = XField3D_Box_Tet_X1::iYmax;
  const int iTmin = XField3D_Box_Tet_X1::iZmin;
  const int iTmax = XField3D_Box_Tet_X1::iZmax;

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  const Real ft3_per_bbl = 5.61458;
  const Real conversion = (1.127e-3)*ft3_per_bbl; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real Kref = conversion*200; //mD

  DLA::MatrixS<2,2,Real> Kref_mat = {{Kref, 0.0},
                                     {0.0, Kref}};

#if 1 //Heterogeneous block
//  RockPermModel::QuadBlock quadblock0 = {{RockPermModel::Coord({{ 800.0, 1800.0}}),
//                                          RockPermModel::Coord({{1800.0,  800.0}}),
//                                          RockPermModel::Coord({{2200.0, 1200.0}}),
//                                          RockPermModel::Coord({{1200.0, 2200.0}})}};

  RockPermModel::QuadBlock quadblock0 = {{RockPermModel::Coord({{2000.0, 1000.0}}),
                                          RockPermModel::Coord({{2300.0, 1300.0}}),
                                          RockPermModel::Coord({{1100.0, 2200.0}}),
                                          RockPermModel::Coord({{ 800.0, 1400.0}})}};

  RockPermModel K(Kref_mat, {quadblock0}, {Kref_mat/100.0});
#elif 0 //Cosine-wave
  const Real Kref_cos = conversion*100; //mD
  const Real Kratio = 100.0;
  const Real amp = 0.05;
  const Real freq = 2.0;
  const Real width = 0.05;
  const Real yoffset = 0.5;
  RockPermModel K(Kref_cos, Kratio, Lx, Ly, amp, freq, width, yoffset);

  if (world.rank() == 0)
  {
    std::cout << "Kref_cos: " << Kref_cos / conversion << " mD" << std::endl;
    std::cout << "Kratio: " << Kratio << std::endl;
    std::cout << "Amplitude: " << amp << std::endl;
    std::cout << "Frequency: " << freq << std::endl;
    std::cout << "Width: " << width << std::endl;
    std::cout << "y-offset: " << yoffset << std::endl << std::endl;
  }
#else
  RockPermModel K(Kref_mat);
#endif

  const Real pc_max = 0.0;
  CapillaryModel pc(pc_max);

  // Set up source term PyDicts

  Real R_bore = 1.0/6.0; //ft
  Real R_well = 100.0; //ft
  Real L_offset = 500.0; //ft

  Real pnIn = 4000.0; //psi
  Real SwIn = 1.0;
  Real pnOut = 2000.0; //psi

  Real pwInit = 3000;
  Real SwInit = 0.1;
  Real sensorInit = 5.0;

  const int nWellParam = 6;

  PyDict well_in;
  typedef SourceTwoPhase2DType_FixedPressureInflow_Param SourceInflowParamClass;
  well_in[SourceInflowParamClass::params.pB] = pnIn;
  well_in[SourceInflowParamClass::params.Sw] = SwIn;
  well_in[SourceInflowParamClass::params.Rwellbore] = R_bore;
  well_in[SourceInflowParamClass::params.nParam] = nWellParam;
  well_in[SourceInflowParamClass::params.WellModel] = SourceInflowParamClass::params.WellModel.Polynomial;
  well_in[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureInflow;

  PyDict well_out;
  well_out[SourceOutflowParamClass::params.pB] = pnOut;
  well_out[SourceOutflowParamClass::params.Rwellbore] = R_bore;
  well_out[SourceOutflowParamClass::params.nParam] = nWellParam;
  well_out[SourceOutflowParamClass::params.WellModel] = SourceOutflowParamClass::params.WellModel.Polynomial;
  well_out[SourceTwoPhase2DParam::params.Source.SourceType] = SourceTwoPhase2DParam::params.Source.FixedPressureOutflow;

  PyDict source_injection;
  source_injection[SourceTwoPhase2DParam::params.Source] = well_in;
  source_injection[SourceTwoPhase2DParam::params.xcentroid] = L_offset;
  source_injection[SourceTwoPhase2DParam::params.ycentroid] = L_offset;
  source_injection[SourceTwoPhase2DParam::params.R] = R_well;
  source_injection[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_injection[SourceTwoPhase2DParam::params.Tmax] = T;
  source_injection[SourceTwoPhase2DParam::params.smoothLr] = 0.0*R_well;
  source_injection[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_injection[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_production;
  source_production[SourceTwoPhase2DParam::params.Source] = well_out;
  source_production[SourceTwoPhase2DParam::params.xcentroid] = Lx - L_offset;
  source_production[SourceTwoPhase2DParam::params.ycentroid] = Ly - L_offset;
  source_production[SourceTwoPhase2DParam::params.R] = R_well;
  source_production[SourceTwoPhase2DParam::params.Tmin] = 0;
  source_production[SourceTwoPhase2DParam::params.Tmax] = T;
  source_production[SourceTwoPhase2DParam::params.smoothLr] = 0.0*R_well;
  source_production[SourceTwoPhase2DParam::params.smoothT] = 0.0;
  source_production[SourceTwoPhase2DParam::params.visibleAngle] = 2.0*PI;

  PyDict source_list;
  source_list["injection_well"] = source_injection;
  source_list["production_well"] = source_production;

  SourceTwoPhase2DListParam::checkInputs(source_list);

  // TwoPhase PDE with AV
  bool hasSpaceTimeDiffusionTwoPhase = false;
  PDEBaseClass pdeTwoPhaseAV(order, hasSpaceTimeDiffusionTwoPhase, rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list);

  SensorAdvectiveFlux sensor_adv(0.0, 0.0);
  bool hasSpaceTimeDiffusionAV = true;
  const Real diffusionConstantAV = 3.0;
  SensorViscousFlux sensor_visc(order, hasSpaceTimeDiffusionAV, 1.0, diffusionConstantAV);
  const Real Sk_min = -3;
  const Real Sk_max = -1;
  SensorSource sensor_source(pdeTwoPhaseAV, Sk_min, Sk_max);

  // AV PDE with sensor equation
  bool isSteadyAV = true;
  NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteadyAV,
                 order, hasSpaceTimeDiffusionTwoPhase, rhow, rhon, phi, krw, krn, muw, mun, K, pc, source_list);

  // Initial solution
  ArrayQ qInit;
  AVVariable<PressureNonWet_SaturationWet,Real> qdata({pwInit, SwInit}, sensorInit);
  pde.setDOFFrom( qInit, qdata );

  typedef BCmitAVSensor2DParams<BCTypeFlux_mitStateSpaceTime, BCTwoPhase2DParams<BCTypeTimeIC>> BCParamsTimeIC;
  typedef BCmitAVSensor2DParams<BCTypeFlux_mitStateSpaceTime, BCTwoPhase2DParams<BCTypeNoFluxSpaceTime>> BCParamsNoFluxST;

  PyDict BCStateInit;
  BCStateInit[TwoPhaseVariableTypeParams::params.StateVector.Variables] =
              TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  BCStateInit[PressureNonWet_SaturationWet_Params::params.pn] = pwInit;
  BCStateInit[PressureNonWet_SaturationWet_Params::params.Sw] = SwInit;

  PyDict BCInit;
  BCInit[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  BCInit[BCParamsTimeIC::params.StateVector] = BCStateInit;
  BCInit[BCParamsTimeIC::params.Cdiff] = diffusionConstantAV;

  PyDict BCNoFlux;
  BCNoFlux[BCParams::params.BC.BCType] = BCParams::params.BC.NoFluxSpaceTime;
  BCNoFlux[BCParamsNoFluxST::params.Cdiff] = diffusionConstantAV;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCNoFlux"] = BCNoFlux;
  PyBCList["BCInit"] = BCInit;
  PyBCList["BCNone"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoFlux"] = {iXmin,iXmax,iYmin,iYmax};
  BCBoundaryGroups["BCInit"] = {iTmin};
  BCBoundaryGroups["BCNone"] = {iTmax};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 8;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-11, 1e-11};

  //Output functional
  bool computeMassFlow = false;
  SourceOutflowClass source(source_production, rhow, rhon, krw, krn, muw, mun, K, pc, computeMassFlow);
  std::vector<SourceOutflowClass> sourceList = { source };
  int phase = 1;
  NDOutputClass fcnOutput(sourceList, phase);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  int quadOrder = 2*(order + 2);

  //Print case summary
  if (world.rank() == 0)
  {
    std::cout << "Case parameters" << std::endl;
    std::cout << "---------------" << std::endl;
    std::cout << "p-order: " << order << std::endl;
    std::cout << "DOF target: " << targetCost << std::endl;
    std::cout << "Processors: " << world.size() << std::endl;
    std::cout << "pc_max: " << pc_max << std::endl;
    std::cout << "nWellParam: " << nWellParam << std::endl;
    std::cout << "R_bore: " << R_bore << std::endl;
    std::cout << "R_well: " << R_well << std::endl;
    std::cout << "pnIn: " << pnIn << std::endl;
    std::cout << "pnOut: " << pnOut << std::endl;
    std::cout << "hasSpaceTimeDiffusionTwoPhase: " << hasSpaceTimeDiffusionTwoPhase << std::endl;
    std::cout << "hasSpaceTimeDiffusionAV: " << hasSpaceTimeDiffusionAV << std::endl;
    std::cout << "diffusionConstantAV: " << diffusionConstantAV << std::endl;
    std::cout << "Sk_min, Sk_max: " << Sk_min << ", " << Sk_max << std::endl;
    std::cout << "isSteadyAV: " << isSteadyAV << std::endl;
    std::cout << "viscousEta: " << viscousEtaParameter << std::endl;
    std::cout << "Residual tol: " << tol[0] << ", " << tol[1] << std::endl;
    std::cout << "Quadrature order: " << quadOrder << std::endl;
    std::cout << "---------------" << std::endl << std::endl;
  }

  // Nonlinear solver dicts - one for each p-order
  std::vector<PyDict> SolverContinuationDict(order+1);
  std::vector<PyDict> AdjLinearSolverDict(order+1);
  std::vector<Real> resGrowthFactor = {1.0, 1.0, 1.0};

  for (int porder = order_min; porder <= order; porder++)
  {
    PyDict NonlinearSolverDict, NewtonSolverDict, LineUpdateDict;

#if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
    PyDict PreconditionerDict;
    PyDict PreconditionerILU;
    PyDict PETScDict;

    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
    PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
    PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 2;
    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
    PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (porder+1)*(porder+2)*(porder+3)/6; //elemDOF for p=order

    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

    PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
    PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-6;
    PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
    PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
    PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
    PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
    PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
    PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
    PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
    PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

    //Change parameters for adjoint solve
    PyDict PreconditionerILU_adjoint = PreconditionerILU;
    PyDict PreconditionerDict_adjoint = PreconditionerDict;
    PyDict PETScDict_adjoint = PETScDict;
    PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (porder+2)*(porder+3)*(porder+4)/6; //elemDOF for p=order+1
    PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
    PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
    PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
    PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
    AdjLinearSolverDict[porder][SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

    if (world.rank() == 0) std::cout << "Linear solver: PETSc" << std::endl;
  #else
    PyDict UMFPACKDict;
    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
    AdjLinearSolverDict[porder][SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
    if (world.rank() == 0) std::cout << "Linear solver: UMFPACK" << std::endl;
  #endif

    LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
    LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
    LineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = resGrowthFactor[porder];
    LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
    LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = false;

    NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
    NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
    NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
    NewtonSolverDict[NewtonSolverParam::params.Timing] = false;

  #if 1
    NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  #else
    //PTC
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;

    NonlinearSolverDict[SolverContinuationParams<TemporalSpaceTime>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalSpaceTime>::params.Continuation.PseudoTime;
    NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
    NonlinearSolverDict[PseudoTimeParam::params.invCFL]     = 1e-4;
    NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1000;
    NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
    NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;

    if (world.rank() == 0)
    {
      std::cout << "PTC parameters:" << std::endl;
      std::cout << "---------------" << std::endl;
      std::cout << "invCFL init: " << NonlinearSolverDict.get(PseudoTimeParam::params.invCFL) << std::endl;
      std::cout << "CFLDecreaseFactor: " << NonlinearSolverDict.get(PseudoTimeParam::params.CFLDecreaseFactor) << std::endl;
      std::cout << "CFLIncreaseFactor: " << NonlinearSolverDict.get(PseudoTimeParam::params.CFLIncreaseFactor) << std::endl;
      std::cout << "CFLPartialStepDecreaseFactor: " << NonlinearSolverDict.get(PseudoTimeParam::params.CFLPartialStepDecreaseFactor) << std::endl;
      std::cout << "invCFL min: " << NonlinearSolverDict.get(PseudoTimeParam::params.invCFL_min) << std::endl;
      std::cout << "invCFL max: " << NonlinearSolverDict.get(PseudoTimeParam::params.invCFL_max) << std::endl;
      std::cout << "---------------" << std::endl << std::endl;
    }
  #endif

    SolverContinuationDict[porder][SolverContinuationParams<TemporalSpaceTime>::params.Continuation] = NonlinearSolverDict;

    // Check inputs
    SolverContinuationParams<TemporalSpaceTime>::checkInputs(SolverContinuationDict[porder]);

    //Print solver summary
    if (world.rank() == 0)
    {
      std::cout << "Solver parameters - order P" << porder  << std::endl;
      std::cout << "---------------" << std::endl;
      std::cout << "maxResGrowthFactor: " << LineUpdateDict.get(HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor) << std::endl;
  #if defined(USE_PETSC_SOLVER) && defined(SANS_PETSC)
      std::cout << "ILU fill level: " << PreconditionerILU.get(SLA::PreconditionerILUParam::params.FillLevel) << std::endl;
      std::cout << "ILU ordering: " << PreconditionerILU.get(SLA::PreconditionerILUParam::params.Ordering) << std::endl;
      std::cout << "MDF block size: " << PreconditionerILU.get(SLA::PreconditionerILUParam::params.MDFOuterBlockSize) << std::endl;
      std::cout << "GMRES restart: " << PETScDict.get(SLA::PETScSolverParam::params.GMRES_Restart) << std::endl;
      std::cout << "PETSc RelativeTolerance: " << PETScDict.get(SLA::PETScSolverParam::params.RelativeTolerance) << std::endl;
      std::cout << "PETSc MaxIterations: " << PETScDict.get(SLA::PETScSolverParam::params.MaxIterations) << std::endl;
  #endif
      std::cout << "---------------" << std::endl << std::endl;
    }
  }

  //--------ADAPTATION LOOP--------

  fstream fadapthist;
  if (world.rank() == 0)
  {
    std::string adapthist_filename = filename_base + "test.adapthist";
    fadapthist.open( adapthist_filename, fstream::out );
    BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
  }

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
//  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.dumpStepMatrix] = false;

  MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

  MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Compute generalized log H-tensor field
  std::shared_ptr<GenHFieldType> pHfld = std::make_shared<GenHFieldType>(*pxfld);

  //Solution data
  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  BasisFunctionCategory basis_category = BasisFunctionCategory_Legendre;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>((*pHfld, *pxfld), pde, order, order+1,
                                               basis_category, basis_category,
                                               active_boundaries, disc);

  pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

  //Set initial solution
//  pGlobalSol->setSolution(solnInit, cellGroups);
  pGlobalSol->setSolution(qInit);

//  ProjectGlobalField(qfld_tmp, pGlobalSol->primal.qfld);

//  std::fstream fqfld("tmp/qfld_initial.txt", std::fstream::in);
//  for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
//    for (int j = 0; j < ArrayQ::M; j++)
//      fqfld >> pGlobalSol->primal.qfld.DOF(i)[j];
//  fqfld.close();

  std::string qfld_init_filename = filename_base + "qfld_init_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, {"pn", "Sw", "nu"} );

  for (int iter = 0; iter < maxIter+1; iter++)
  {
    if (world.rank() == 0)
      std::cout << "\n" << std::string(25,'-') + "Adaptation Iteration " << iter << std::string(25,'-') << "\n\n";

    //Create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;

    for (int porder = order_min; porder <= order; porder++ )
    {
      if (world.rank() == 0)
        std::cout << std::string(5,'-') + "P-sequencing order " << porder << std::string(5,'-') << "\n\n";

      std::shared_ptr<SolutionClass> pGlobalSol_porder;
      pGlobalSol_porder = std::make_shared<SolutionClass>((*pHfld, *pxfld), pde, porder, porder+1,
                                                          basis_category, basis_category,
                                                          active_boundaries, disc);

      pGlobalSol_porder->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

      //Perform L2 projection from last solution
      pGlobalSol_porder->setSolution(*pGlobalSol);

      //Make current solution the projected solution in new p-order
      pGlobalSol = pGlobalSol_porder;

      quadOrder = 2*(porder + 2);

      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict[porder], AdjLinearSolverDict[porder],
                                                          outputIntegrand);
      pInterface->setBaseFilePath(filename_base);

      pInterface->solveGlobalPrimalProblem();
      std::string qfld_filename = filename_base + "qfld_P" + std::to_string(porder) + "_a" + std::to_string(iter) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename, {"pn", "Sw", "nu"} );
  //    std::string sol_filename = filename_base + "sol_P" + std::to_string(porder) + "_a" + std::to_string(iter) + ".plt";
  //    output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld, sol_filename, {"pn", "Sw", "nu"});

      std::string liftedfld_filename = filename_base + "liftedfld_P" + std::to_string(porder) + "_a" + std::to_string(iter) + ".plt";
      output_Tecplot( *(pGlobalSol->pliftedQuantityfld), liftedfld_filename, {"s"} );

//      std::fstream fqfld("tmp/qfld_a" + std::to_string(iter) + ".txt", std::fstream::out);
//      fqfld << std::scientific << std::setprecision(15);
//      for (int i = 0; i < pGlobalSol->primal.qfld.nDOF(); i++)
//        for (int j = 0; j < ArrayQ::M; j++)
//          fqfld << pGlobalSol->primal.qfld.DOF(i)[j] << std::endl;
//      fqfld.close();

      if (world.rank() == 0)
        std::cout << "Output: " << std::scientific << std::setprecision(12) << pInterface->getOutput() << std::endl << std::endl;
    }

    pInterface->solveGlobalAdjointProblem();
    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename, {"psiw", "psin", "psinu"} );

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    timer adapt_clock;
    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    if (world.rank() == 0)
      std::cout<<"Mesh adapt time: " << std::fixed << std::setprecision(3) << adapt_clock.elapsed() << "s" << std::endl << std::endl;

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Compute generalized log H-tensor field on new mesh
    pHfld = std::make_shared<GenHFieldType>(*pxfldNew);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>((*pHfld,*pxfldNew), pde, order, order+1,
                                                    basis_category, basis_category,
                                                    active_boundaries, disc);

    pGlobalSolNew->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

    //Perform L2 projection from solution on previous mesh
//    pGlobalSolNew->setSolution(qInit);
    pGlobalSolNew->setSolution(*pGlobalSol);

#if 0
    //Hack: Initial solution projection for pressure
    const int nDOF_per_elem = (order + 1)*(order + 2)*(order + 3)/6;
    for (int i = 0; i < pGlobalSolNew->primal.qfld.nElem(); i++)
      for (int j = 0; j < nDOF_per_elem; j++)
      {
        if (j == 0)
          pGlobalSolNew->primal.qfld.DOF(nDOF_per_elem*i + j)[0] = qInit[0];
        else
          pGlobalSolNew->primal.qfld.DOF(nDOF_per_elem*i + j)[0] = 0.0;
      }
#endif

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;

    std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename, {"pn", "Sw", "nu"} );

    if (world.rank() == 0)
      std::cout<<"Time elapsed: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl << std::endl;
  }

  if (world.rank() == 0)
    fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
