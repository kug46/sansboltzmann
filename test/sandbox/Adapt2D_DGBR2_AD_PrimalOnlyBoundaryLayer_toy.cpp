// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_AD_PrimalOnlyBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion "primal-only" boundary layer test-case found in Masa's thesis

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_AD_PrimalOnlyBoundaryLayer_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_AD_PrimalOnlyBoundaryLayer_Triangle )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef ScalarFunction2D_Gaussian WeightFcn; // Change this to change output weighting type
  typedef OutputCell_WeightedSolution<PDEClass, WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Grid
  int ii = 4;
  int jj = 4;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_UnionJack_Triangle_X1>(ii, jj, -1.5, 1.5, 0, 1);

  int order = 1;

  // PDE
  AdvectiveFlux2D_Uniform adv( 1, 0 );

  Real nu = 0.001;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0,0,0);

  NDPDEClass pde( adv, visc, source );

  Real u_bc_bottom = 1.0;

  PyDict BC_Bottom;
  BC_Bottom[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BC_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1;
  BC_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0;
  BC_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = u_bc_bottom;

  PyDict BC_Right;
  BC_Right[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BC_Right[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 0;
  BC_Right[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 1;
  BC_Right[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = 0;

//  PyDict BC_Right;
//  BC_Right[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BC_Top;
  BC_Top[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BC_Top[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1;
  BC_Top[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0;
  BC_Top[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = 0;

  PyDict BC_Left;
  BC_Left[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BC_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1;
  BC_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 1;
  BC_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = 0;

  PyDict PyBCList;
  PyBCList["BC_Bottom"] = BC_Bottom;
  PyBCList["BC_Right"] = BC_Right;
  PyBCList["BC_Top"] = BC_Top;
  PyBCList["BC_Left"] = BC_Left;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BC_Bottom"] = {0};
  BCBoundaryGroups["BC_Right"] = {1};
  BCBoundaryGroups["BC_Top"] = {2};
  BCBoundaryGroups["BC_Left"] = {3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_Default;
  std::vector<Real> tol = {1e-12, 1e-12};

  //2D Gaussian weighting function for output
  Real a = 1.0/(2.0*PI*0.0012);
  Real x0 = 0.0;
  Real y0 = 0.25;
  Real sx = 0.02;
  Real sy = 0.06;
  WeightFcn weightFcn(a, x0, y0, sx, sy);
  NDOutputClass fcnOutput(weightFcn);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  //--------ADAPTATION LOOP--------

  int maxIter = 20;
  Real targetCost = 1000;

  std::string filename_base = "tmp/";
  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries, disc);

  const int quadOrder = -1; //2*(order_primal + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList, BCBoundaryGroups,
                                                      SolverContinuationDict, LinearSolverDict,
                                                      outputIntegrand);

  pGlobalSol->setSolution(0.0);

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  std::string qfld_filename = filename_base + "qfld_a0.plt";
  output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

  std::string adjfld_filename = filename_base + "adjfld_a0.plt";
  output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, disc);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution(*pGlobalSol);

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
  }

  fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
