// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_CG_AD_btest
// testing of 1-D CG with Advection-Diffusion

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_Solution.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_WeightedResidual_Galerkin.h"
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Adaptation/MOESS/SolverInterface_AGLS.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/Function/WeightedFunctionIntegral.h"
#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
#define TAU = 10;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_1D_CG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorOrder_1D_CG_AD )
{
  //ADVECTION_DIFFUSION CASE
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ScalarFunction1D_BL SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;


  //ADJOINT TYPEDEFS
//  typedef ScalarFunction1D_Linear WeightFunctional;
  typedef OutputCell_WeightedSolution<PDEClass, SolutionExact> WeightOutputClass;
  typedef OutputNDConvertSpace<PhysD1, WeightOutputClass>           NDWeightOutputClass;
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDWeightOutputClass, NDPDEClass> OutputIntegrandClass;

  typedef ScalarFunction1D_BLAdj AdjSolutionExact;
  typedef SolnNDConvertSpace<PhysD1, AdjSolutionExact> NDAdjSolutionExact;

//  typedef BCTypeFunction_mitState<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef ParamType_None ParamBuilderType;
  typedef SolutionData_Galerkin_Stabilized<PhysD1, TopoD1, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

//    typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
    typedef SolverInterface_AGLS<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;


  // PDE
  Real a = 1;
  AdvectiveFlux1D_Uniform adv( a );
  Real nu = 0.01;

  //ADVECTION DIFFUSION
  ViscousFlux1D_Uniform visc( nu );

  Real src = 0;
  Source1D_Uniform source(src);

  NDSolutionExact solnExact(a, nu, src);
  NDAdjSolutionExact adjSolnExact(a, nu, src);
  Real outputExact = 0.005;

//  NDPDEClass pde(adv, visc, source, forcingptr );
  NDPDEClass pde(adv, visc, source);

  // BC

  // Create a BC dictionary
  PyDict BL;
  BL[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function.BL;
  BL[SolutionExact::ParamsType::params.a] = a;
  BL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
//  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
//  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.Function] = BL;
//  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_sansLG>::params.SolutionBCType] = "Dirichlet";

  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
    BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Function] = BL;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.SolutionBCType] = "Dirichlet";
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0,1};
  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);
  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Primal Exact Soln
  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // Adjoint Exact Solution
  typedef OutputCell_SolutionErrorSquared<PDEClass, AdjSolutionExact> AdjErrorClass;
  typedef OutputNDConvertSpace<PhysD1, AdjErrorClass> NDAdjErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDAdjErrorClass> ErrorAdjIntegrandClass;

  NDAdjErrorClass fcnAdjError(adjSolnExact);
  ErrorAdjIntegrandClass errorAdjIntegrand(fcnAdjError, {0});

  StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby, 1.0);

  //output functional
//  WeightFunctional volumeFcn(0,1);

  SolutionExact volumeFcn(a, nu, src, 1, -1);
  NDWeightOutputClass weightOutput( volumeFcn );
  OutputIntegrandClass outputIntegrand(pde, weightOutput, {0}, stab );

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

#if defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  string filename_base = "tmp/1DADNEW/AGLS/AGLS";
  string filename2 = filename_base + "_Errors.txt";

  fstream foutsol;
  foutsol.open( filename2, fstream::trunc );
  foutsol.close();

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-10, 1e-10};

  int ordermin = 1;
  int ordermax = 3;
  for (int order = ordermin; order <= ordermax; order++)
  {
    stab.setStabOrder(order);
    stab.setNitscheOrder(order);
    // loop over grid resolution: 2^power
    int powermin = 2;
    int powermax = 10;

    for (int power = powermin; power <= powermax; power++)
    {
      std::cout << "order: " << order << ", power:" << power << "\n";
      int ii = pow( 2, power )+1;
//      int ii = power;

      // grid:

      std::shared_ptr<XField<PhysD1, TopoD1>> pxfld( new XField1D(ii) );

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                   BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                   active_boundaries, stab);

      const int quadOrder = 2*(order + 1);
      QuadratureOrder quadratureOrder( *pxfld, 2*(order+1) );

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, AdjLinearSolverDict,
                                                          outputIntegrand);

      pGlobalSol->setSolution(0.0);

      pInterface->solveGlobalPrimalProblem();
      Real output1 = pInterface->getOutput();

      pInterface->solveGlobalAdjointProblem();
      Real output2 = pInterface->getOutput();

      pInterface->computeErrorEstimates();
      Real outputErrEstimate = pInterface->getGlobalErrorEstimate();

      Real outputErr1 = (output1 - outputExact);
      Real outputErr2 = (output2 - outputExact);

      Real errEstErr1 = outputErr1 - outputErrEstimate;
      Real errEstErr2 = outputErr2 - outputErrEstimate;

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorIntegrand, SquareError ),
          *pxfld, pGlobalSol->primal.qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real L2err = sqrt(SquareError);

      Real AdjSquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorAdjIntegrand, AdjSquareError ),
          *pxfld, pInterface->getAdjField(), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real adjError = sqrt(AdjSquareError);

      // Tecplot dump
      std::string filename_base2 = filename_base + "_P";
      filename_base2 += stringify(order);
      filename_base2 += "_";
      filename_base2 += stringify(ii);
//      filename_base2 += "x";
//      filename_base2 += to_string(jj);

      std::string filenameq = filename_base2 + "_qfld.plt";
      std::string filenameadj  = filename_base2 + "_adjfld.plt";

      output_Tecplot( pGlobalSol->primal.qfld, filenameq );
      output_Tecplot( pInterface->getAdjField(), filenameadj );

      foutsol.open( filename2, fstream::app );
      foutsol << order << " " << ii << std::setprecision(15);
      foutsol << " " << L2err;
      foutsol << " " << adjError;
      foutsol << " " << fabs(outputErr1);
      foutsol << " " << fabs(outputErr2);
      foutsol << " " << fabs(errEstErr1);
      foutsol << " " << fabs(errEstErr2) << "\n";

      foutsol.close();

    }

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
