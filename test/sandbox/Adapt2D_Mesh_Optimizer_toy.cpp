// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_L2_BoundaryLayer_btest
// Testing of the MOESS framework on the 2D boundary layer test-case found in Masa's thesis

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <fstream>

#include "Field/FieldArea_CG_Cell.h"

#include "Adaptation/MeshAdapter.h"

#include "Meshing/AnalyticMetrics/MetricField.h"
#include "Meshing/AnalyticMetrics/CornerSingularity.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

//#define TEST_MODE

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_Mesh_Optimizer_test_suite )

//#define CASE_BOUNDARYLAYER
#define CASE_CORNERSINGULARITY

class SinShock
{
public:
  SinShock() {}

#ifdef CASE_BOUNDARYLAYER
  const Real length = 1.0;
  DLA::MatrixSymS<2, Real> operator()( const DLA::VectorS<2, Real>& X ) const
  {
    DLA::MatrixSymS<2, Real> M(0);
    M(0,0) = 1;
    Real hy = 1e-4 +0.1*X[1];
    M(1,1) = 1./(hy*hy);// 100000*(1. -X[1]*X[1]) +1e-4;
    return M;
  }
#else
  DLA::MatrixSymS<2, Real> operator()( const DLA::VectorS<2, Real>& X )
  {
    Real H_ = 5.;
    Real f_ = 10.;
    Real A_ = 2.;
    Real P_ = 5.;
    Real y0_ = 5.;
    Real w_ = 1.*M_PI/P_;

    std::vector<Real> x(2);
    x[0] = X[0];
    x[1] = X[1];

    Real phi = -f_*( x[1] -A_*cos(w_*x[0]) -y0_ );

    Real dzdx = -A_*H_*f_*w_*sin(w_*x[0])*( pow(tanh(phi),2.) -1. );
    Real dzdy = -H_*f_*( pow(tanh(phi),2.) -1. );

    DLA::MatrixSymS<2, Real> M(0);
    M(0,0) = 1. +dzdx*dzdx;
    M(0,1) = dzdx*dzdy;
    M(1,1) = 1. +dzdy*dzdy;

    return M;
  }
#endif
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_Mesh_Optimizer_test )
{
  const int maxIter = 25;

  int powerL = 0, powerH = 7;

  for (int p = 1; p <= 3; p++)
  {
    for (int power = powerH; power >= powerL; power--)
    {
      const int nk = pow(2,power);
      int targetCost = 125*nk;

  #ifdef CASE_CORNERSINGULARITY
    const Real length = 1.0;
    const Real offset = 0.0;
      Real alpha = 2./3.;
      int nElem = 2*targetCost/((p+1)*(p+2)/2);
      Real C = nElem/7.245391218226922;
      Metric::CornerSingularity<2> metric_func( alpha, p, C );
      std::string func = "corner";

  #elif defined(CASE_BOUNDARYLAYER)
    const Real length = 1.0;
      std::string func = "bl";
      SinShock metric_func;
  #else
    const Real length 10.0
      std::string func = "sshock";
      SinShock metric_func;
  #endif

      mpi::communicator world;

      std::vector<int> cellGroups = {0};

      // std::vector<std::string> meshers = {"avro","EPIC"};//,"fefloa"};
      // std::vector<std::string> meshers = {"EPIC"};
      std::vector<std::string> meshers = {"avro"};

      Real lens[2] = {length,length};

      for (const std::string& mesher : meshers)
      {
        std::fprintf(stdout,"Mesher = %s\n",mesher.c_str());

        const int string_pad = 6;
        std::cout << "int_pad " << string_pad - std::to_string(targetCost).length() << std::endl;

        std::string int_pad = std::string(string_pad - std::to_string(targetCost).length(), '0') + std::to_string(targetCost);

        std::string filename_base = "tmp/metric2d/" + func + "/" + mesher + "_P" + std::to_string(p) + "_" + int_pad + "/";

        boost::filesystem::create_directories(filename_base);

        //--------ADAPTATION LOOP--------

        std::string adapthist_filename = filename_base + "test.adapthist";
        std::fstream fadapthist( adapthist_filename, std::fstream::out );
        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

        PyDict MOESSDict;
        MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
        MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
        MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;

        PyDict MesherDict;
        if (mesher == "refine")
        {
          MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.refine;
          MesherDict[refineParams::params.DumpRefineDebugFiles] = false;
    #ifdef SANS_REFINE
          MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.API;
          //MesherDict[refineParams::params.CallMethod] = refineParams::params.CallMethod.system;
    #endif
        }
        else if (mesher == "EPIC")
        {
          MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
          std::vector<int> allBC = {0,1,2,3};
          MesherDict[EpicParams::params.SymmetricSurf] = allBC;
          MesherDict[EpicParams::params.nThread] = 4;
        }
        else if (mesher == "fefloa")
        {
          MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;
        }
    #ifdef SANS_AVRO
        else if (mesher == "avro")
        {
          MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;
          MesherDict[avroParams::params.Curved] = false;
          MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
          MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files
        }
    #endif
        else
          BOOST_REQUIRE_MESSAGE(false, "Unknown mesh generator.");


        PyDict AdaptDict;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpMetric] = true;

        MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

        MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

        std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;

    #if defined(SANS_AVRO)
        std::shared_ptr<avro::Context> context;
        if (mesher == "avro")
        {
          using avro::coord_t;
          using avro::index_t;

          context = std::make_shared<avro::Context>();

          Real xc[2] = {length*.5+offset,length*.5+offset};
          coord_t number = 2;
          std::vector<avro::real> lens(number,1.);
          std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( context.get() , xc , lens[0] , lens[1] );
          std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(),"cube");
          model->addBody(pbody,true);

          // initialize the mesh
          XField2D_Box_Triangle_Lagrange_X1 xfld0( world,2,2,offset,lens[0]+offset,offset,lens[1]+offset );
          // copy the mesh into the domain and attach the geometry
          pxfld = std::make_shared< XField_avro<PhysD2,TopoD2> >(xfld0,model);
        }
        else
    #endif
        {
          pxfld = std::make_shared< XField2D_Box_Triangle_Lagrange_X1>(world, 5,5,
                                                                      offset,lens[0]+offset,
                                                                      offset,lens[1]+offset);
        }

        for (int iter = 0; iter < maxIter+1; iter++)
        {
          std::cout << "-----Adaptation Iteration " << iter << "-----" << std::endl;

          WriteMesh_libMeshb( *pxfld , filename_base + "/mesh_" + std::to_string(iter) + ".mesh" );

          // // adapt to the implied metric!
          pxfld = mesh_adapter.adapt_implied_metric(*pxfld, cellGroups, iter);

          // evaluate the target metric
          MetricField<PhysD2,TopoD2> targetMetric( *pxfld , cellGroups , metric_func );

          // adapt mesh to metric
          pxfld = mesh_adapter.adapt_metric(*pxfld, cellGroups, targetMetric, iter);
        }

        std::string xfld_filename = filename_base + "xfld_final.mesh";
        WriteMesh_libMeshb( *pxfld, xfld_filename);

        fadapthist.close();
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
