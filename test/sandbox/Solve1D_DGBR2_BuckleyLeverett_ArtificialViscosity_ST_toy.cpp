// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_DGBR2_BuckleyLeverett_ArtificialViscosity_btest
// testing of 1-D DG of Buckley-Leverett with artificial viscosity

//#define SANS_FULLTEST
#define SANS_VERBOSE

#define UNSTEADY_FORMULATION

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/mpl/vector_c.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"
#include "pde/PorousMedia/Sensor_BuckleyLeverett.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/JacobianParam.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/HField/GenHFieldArea_CG.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGBR2_BuckleyLeverett_ArtificialViscosity_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGBR2_BuckleyLeverett_ArtificialViscosity_ST )
{
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;

  typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEmitAVSensor;
  typedef PDENDConvertSpaceTime<PhysD1, PDEmitAVSensor> NDPDEmitAVSensor;
  typedef NDPDEmitAVSensor::template ArrayQ<Real> ArrayQ;
  typedef NDPDEmitAVSensor::template VectorArrayQ<Real> VectorArrayQ;
  typedef NDPDEmitAVSensor::template MatrixParam<Real> PDEMatrixParam;

  typedef Sensor_BuckleyLeverett<PDEClass> Sensor;
  typedef Sensor_AdvectiveFlux1D_Uniform Sensor_Advection;
  typedef Sensor_ViscousFlux1D_GenHScale Sensor_Diffusion;
  typedef Source1D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor;

  typedef PDESensorParameter<PhysD1,
                             SensorParameterTraits<PhysD1>,
                             Sensor_Advection,
                             Sensor_Diffusion,
                             Source_JumpSensor > SensorPDEClass;
  typedef PDENDConvertSpaceTime<PhysD1, SensorPDEClass> SensorNDPDEClass;

  typedef SensorNDPDEClass::template ArrayQ<Real> SensorArrayQ;
  typedef SensorNDPDEClass::template VectorArrayQ<Real> SensorVectorArrayQ;
  typedef SensorNDPDEClass::template MatrixParam<Real> SensorMatrixParam;

  typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;

  typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                         Field<PhysD2, TopoD2, SensorArrayQ>,
                                         XField<PhysD2, TopoD2>>::type AVParamField;

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QTypePrimitive_Sw, PDEClass> ExactSolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef BCBuckleyLeverett_ArtificialViscosity1DVector<TraitsSizeBuckleyLeverett, TraitsModelClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEmitAVSensor, BCNDConvertSpaceTime, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, AVParamField> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;

  typedef BCSensorParameter1DVector<Sensor_Advection, Sensor_Diffusion> SensorBCVector;

  typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                         Field<PhysD2, TopoD2, ArrayQ>,
                                         XField<PhysD2, TopoD2>>::type ParamField;
  typedef AlgebraicEquationSet_DGBR2<SensorNDPDEClass, BCNDConvertSpaceTime, SensorBCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamField> SensorPrimalEquationSetClass;
  typedef SensorPrimalEquationSetClass::BCParams SensorBCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, ExactSolutionClass> ErrorClass;
  typedef OutputNDConvertSpaceTime<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;

  // Create Buckley-Leverett PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 0.0;
  CapillaryModel cap_model(pc_max);

  NDPDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 1.0;
  const Real SwR = 0.1;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(pde, SwL, SwR, xinit);

  Real Cdiff = 3.0;

  // PDE BCs
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitState,
                                BCBuckleyLeverett1DParams<BCTypeFunction_mitState>> BCParamsAV_Sol;
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitState,
                                BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>> BCParamsAV_Dirichlet_mitState;
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitStateSpaceTime, BCBuckleyLeverett1DParams<BCTypeTimeIC>> BCParamsAV_IC;

  // Create a Solution dictionary
  PyDict SolnDict;
  SolnDict[BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.Name]
           = BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.PiecewiseConst;
  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.a0] = SwL;
  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.a1] = SwR;
  SolnDict[ScalarFunction1D_PiecewiseConst::ParamsType::params.x0] = xinit;

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCDirichletB;
  BCDirichletB[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCDirichletB[BCParamsAV_Sol::params.Function] = SolnDict;
  BCDirichletB[BCParamsAV_Sol::params.Cdiff] = Cdiff;

  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletL[BCParamsAV_Dirichlet_mitState::params.qB] = SwL;
  BCDirichletL[BCParamsAV_Dirichlet_mitState::params.Cdiff] = Cdiff;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletR[BCParamsAV_Dirichlet_mitState::params.qB] = SwR;
  BCDirichletR[BCParamsAV_Dirichlet_mitState::params.Cdiff] = Cdiff;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  TimeIC[BCParamsAV_IC::params.qB] = SwR;
  TimeIC[BCParamsAV_IC::params.Cdiff] = Cdiff;

  PyDict PyBCList;
  PyBCList["TimeIC"] = TimeIC;
  PyBCList["DirichletR"] = BCDirichletR;
  PyBCList["DirichletL"] = BCDirichletL;
  PyBCList["None"] = BCNone;

  // Define the BoundaryGroups for each boundary condition
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["TimeIC"] = {0}; //Bottom boundary
  BCBoundaryGroups["DirichletR"] = {1}; //Right boundary
  BCBoundaryGroups["DirichletL"] = {3}; //Left boundary
  BCBoundaryGroups["None"] = {2}; //Top boundary

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Sensor BCs
  PyDict BCSensorRobin;
  BCSensorRobin[SensorBCParams::params.BC.BCType] = SensorBCParams::params.BC.LinearRobin_sansLG;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorNone;
  BCSensorNone[SensorBCParams::params.BC.BCType] = SensorBCParams::params.BC.None;

  PyDict PySensorBCList;
  PySensorBCList["SensorRobin"] = BCSensorRobin;
//  PySensorBCList["SensorNone"] = BCSensorNone;

  std::map<std::string, std::vector<int>> SensorBCBoundaryGroups;
  SensorBCBoundaryGroups["SensorRobin"] = {0, 1, 2, 3}; // Bottom, right, left
//  SensorBCBoundaryGroups["SensorNone"] = {2}; // Top

  //Check the BC dictionary
  SensorBCParams::checkInputs(PySensorBCList);

  std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
  std::vector<int> active_sensor_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, SensorBCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // Initial solution
  ArrayQ qinit;
  pde.setDOFFrom( qinit, SwR, "Sw" );

  std::vector<Real> tol = { 1e-11, 1e-11 };

  // norm data
  Real hVec[10];
//  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/Solve1D_DGBR2_BuckleyLeverett_AV_ST.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/Solve/Solve1D_DGBR2_BuckleyLeverett_AV_ST_FullTest.txt",
                                1e-10, 1e-10, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
//  pyrite_file_stream pyriteFile("IO/Solve/Solve1D_DGBR2_BuckleyLeverett_AV_ST_MinTest.txt",
//                                1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
//  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 0;
#else
  int ordermax = 1;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    //Create artificial viscosity PDE
    const bool hasSpaceTimeDiffusion = false;
    NDPDEmitAVSensor avpde(order, hasSpaceTimeDiffusion, phi, uT, kr_model, mu_w, mu_n, K, cap_model);

    // Create the sensor parameter PDE
    Sensor sensor(avpde);

    Sensor_Advection sensor_adv(0.0);
    Sensor_Diffusion sensor_visc(Cdiff);

    Source_JumpSensor sensor_source(order, sensor);

    SensorNDPDEClass sensorpde(sensor_adv, sensor_visc, sensor_source);

    // loop over grid resolution: 30*factor
    int ii, jj;
    int factormin = 1;
#ifdef SANS_FULLTEST
    int factormax = 1;
#else
    int factormax = 1;
#endif
    for (int factor = factormin; factor <= factormax; factor++)
    {
      ii = 20*factor;
      jj = 20*factor;

      // grid:
      XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 50, 0, 25, true );
//      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, 50, 0, 25);

      GenHField_CG<PhysD2,TopoD2> hfld(xfld);

      // PDE solution fields
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, active_BGroup_list);

      //Initialize
      qfld = qinit;
      rfld = 0.0;
      lgfld = 0.0;

      // Or, use the projection of the exact solution as an initial solution
      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
      output_Tecplot( qfld, "tmp/qfld_init.plt" );

      //Sensor solution fields
      int sensor_order = 1;
      Field_DG_Cell<PhysD2, TopoD2, SensorArrayQ> sensor_qfld(xfld, sensor_order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, SensorVectorArrayQ> sensor_rfld(xfld, sensor_order, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, SensorArrayQ> sensor_lgfld(xfld, sensor_order, BasisFunctionCategory_Legendre,
                                                                        active_sensor_BGroup_list);
      sensor_qfld = 0;
      sensor_rfld = 0;
      sensor_lgfld = 0;

      const int nDOFPDE = qfld.nDOF();
      const int nDOFBC = lgfld.nDOF();
      std::cout<<"DOF: "<<(nDOFPDE + nDOFBC)<<std::endl;

#if 0
      //Temporary: for visualizing smallest left/right eigenvectors
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> Vfld_q(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> VTfld_q(xfld, order, BasisFunctionCategory_Legendre);

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> Vfld_sensor(xfld, sensor_order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> VTfld_sensor(xfld, sensor_order, BasisFunctionCategory_Legendre);

      std::fstream fVmin("tmp/Vmin.txt", std::fstream::in);
      std::fstream fVTmin("tmp/VTmin.txt", std::fstream::in);

      for (int i = 0; i < Vfld_q.nDOF(); i++)
      {
        fVmin >> Vfld_q.DOF(i);
        fVTmin >> VTfld_q.DOF(i);
      }
      for (int i = 0; i < Vfld_sensor.nDOF(); i++)
      {
        fVmin >> Vfld_sensor.DOF(i);
        fVTmin >> VTfld_sensor.DOF(i);
      }
      fVmin.close();
      fVTmin.close();

      output_Tecplot( Vfld_q, "tmp/Vfld_q.plt" );
      output_Tecplot( VTfld_q, "tmp/VTfld_q.plt" );
      output_Tecplot( Vfld_sensor, "tmp/Vfld_sensor.plt" );
      output_Tecplot( VTfld_sensor, "tmp/VTfld_sensor.plt" );
#endif

      QuadratureOrder quadratureOrder( xfld, -1 );

      //Create AlgEqnSet for artificial viscosity PDE
      AVParamField avparamfld = (hfld, sensor_qfld, xfld);
      PrimalEquationSetClass PrimalEqSet(avparamfld, qfld, rfld, lgfld, avpde,
                                         disc, quadratureOrder, ResidualNorm_Default, tol,
                                         {0}, {0,1,2}, PyBCList, BCBoundaryGroups);

      //Create AlgEqnSet for sensor PDE
      ParamField paramfield = (hfld, qfld, xfld);
      SensorPrimalEquationSetClass SensorPrimalEqSet(paramfield, sensor_qfld, sensor_rfld, sensor_lgfld, sensorpde,
                                                     disc, quadratureOrder, ResidualNorm_Default, tol,
                                                     {0}, {0,1,2}, PySensorBCList, SensorBCBoundaryGroups);

      //Initialize the sensor field to be consistent with the initial qfld
      typedef SensorPrimalEquationSetClass::SystemMatrix SensorSystemMatrixClass;
      typedef SensorPrimalEquationSetClass::SystemVector SensorSystemVectorClass;

      NewtonSolver<SensorSystemMatrixClass> SensorSolver( SensorPrimalEqSet, NewtonSolverDict );

      // set initial condition from current solution in solution fields
      SensorSystemVectorClass sensor_sln0(SensorPrimalEqSet.vectorStateSize());
      SensorPrimalEqSet.fillSystemVector(sensor_sln0);

      // solve for the sensor field for the initial qfld solution
      SensorSystemVectorClass sensor_sln(SensorPrimalEqSet.vectorStateSize());
      SolveStatus status = SensorSolver.solve(sensor_sln0, sensor_sln);
      BOOST_CHECK( status.converged );

#if 1
      string filename_init = "tmp/sensorDGBR2_BuckleyLeverett_AV_ST_P";
      filename_init += to_string(order);
      filename_init += "_";
      filename_init += to_string(ii);
      filename_init += "x";
      filename_init += to_string(jj);
      filename_init += "_init.plt";
      cout << "calling output_Tecplot: filename = " << filename_init << endl;
      output_Tecplot( sensor_qfld, filename_init );
#endif


      // Generate off diagonal AlgEqnSets
      typedef PDEMatrixParam MatrixQ01;
      typedef SensorMatrixParam MatrixQ10;

      typedef SLA::SparseMatrix_CRS<MatrixQ01>   MatrixClass01;
      typedef SLA::SparseMatrix_CRS<MatrixQ10>   MatrixClass10;

      typedef DLA::MatrixD<MatrixClass01> SystemMatrix01;
      typedef DLA::MatrixD<MatrixClass10> SystemMatrix10;

      const int iPDE_SensorParam = 1; // Sensor parameter in PDE index
      const int iSensor_PDEParam = 1; // PDE solution parameter in sensor index

      typedef JacobianParam<boost::mpl::vector1_c<int,iPDE_SensorParam>,PrimalEquationSetClass> JacobianParam_PDE;
      typedef JacobianParam<boost::mpl::vector1_c<int,iSensor_PDEParam>,SensorPrimalEquationSetClass> JacobianParam_Sensor;

      std::map<int,int> PDEMap;
      PDEMap[iPDE_SensorParam] = SensorPrimalEqSet.iq; //The parameter in the PDE

      std::map<int,int> SensorMap;
      SensorMap[iSensor_PDEParam] = PrimalEqSet.iq;

      JacobianParam_PDE JP01(PrimalEqSet, PDEMap);
      JacobianParam_Sensor JP10(SensorPrimalEqSet, SensorMap);

      // Generate 2x2 Block system
      typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass, SystemMatrix01,
                                            SystemMatrix10, SensorSystemMatrixClass> AlgEqSetType_2x2;

      AlgEqSetType_2x2 BlockAES(PrimalEqSet, JP01,
                                JP10, SensorPrimalEqSet);

      typedef AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
      typedef AlgEqSetType_2x2::SystemVector BlockVectorClass;

      //Solve

      // Create the solver object
//      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );
      NewtonSolver<BlockMatrixClass> Solver( BlockAES, NewtonSolverDict );

      // set initial condition from current solution in solution fields
      BlockVectorClass sln0(BlockAES.vectorStateSize());
      BlockAES.fillSystemVector(sln0);

#if 0
      // jacobian nonzero pattern
      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
      PrimalEqSet.jacobian(sln0, nz);

      // jacobian
      SystemMatrixClass jac(nz);
      jac = 0.0;
      PrimalEqSet.jacobian(sln0, jac);

      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
//      fstream fout00( "tmp/jac00.mtx", fstream::out );
//      cout << "btest: global jac00" << endl;  WriteMatrixMarketFile( jac(0,0), fout00 );
//      fstream fout10( "tmp/jac10.mtx", fstream::out );
//      cout << "btest: global jac10" << endl;  WriteMatrixMarketFile( jac(1,0), fout10 );
//      fstream fout01( "tmp/jac01.mtx", fstream::out );
//      cout << "btest: global jac01" << endl;  WriteMatrixMarketFile( jac(0,1), fout01 );
#endif

      // nonlinear solve
      BlockVectorClass sln(BlockAES.vectorStateSize());
      status = Solver.solve(sln0, sln);
      BOOST_CHECK( status.converged );

#if 0 //Print residual vector
      cout << "Lifting operator:" <<endl;
      for (int k = 0; k < rfld.nDOF(); k++) cout << k << "\t" << rsd[0][2*k + 0] <<"\t" << rsd[0][2*k + 1] << endl;

      cout <<endl << "Solution:" <<endl;
      for (int k = 0; k < nDOFPDE; k++) cout << k << "\t" << rsd[1][k] <<endl;

      cout <<endl << "BC:" <<endl;
      for (int k = 0; k < nDOFBC; k++) cout << k << "\t" << rsd[2][k] << endl;
#endif

      // L2 solution error
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( errorIntegrand, SquareError ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError;

      hVec[indx] = 1./ii;
//      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
      {
        cout << "  (L2 err ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        Real slope = (log(normVec[indx-1]) - log(normVec[indx-2]))/(log(hVec[indx-1]) - log(hVec[indx-2]));
        cout << "  (L2 slope = " << slope << ")";
      }
      cout << endl;
#endif

#if 1
      // Tecplot dump
      string filename = "tmp/slnDGBR2_BuckleyLeverett_AV_ST_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );

      filename = "tmp/sensorDGBR2_BuckleyLeverett_AV_ST_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( sensor_qfld, filename );
#endif

    } //grid refinement loop

#if 0
    // Tecplot output
    resultFile << "ZONE T=\"DG BR2 P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }
#endif

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
//    std::cout<<resultFile.str()<<std::endl;
  } //order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
