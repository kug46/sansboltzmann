// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_BuckleyLeverett_ST_btest
// Testing of the MOESS framework on a space-time Buckley-Leverett problem with HDG

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/IntegrandCell_HDG_Output.h"

#include "Adaptation/MOESS/ProblemStatement_HDG_ErrorEstimate.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_HDG_BuckleyLeverett_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_HDG_BuckleyLeverett_ST_Triangle )
{
  typedef QTypePrimitive_Sw QType;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef TraitsModelBuckleyLeverett<QType, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QType, PDEClass> ExactSolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef BCBuckleyLeverett1DVector<PDEClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_HDG_Output<NDOutputClass> OutputIntegrandClass;

  typedef ProblemStatement_HDG_ErrorEstimate<NDPDEClass, BCNDConvertSpaceTime, BCVector, HDG,
                                             XField<PhysD2, TopoD2>, OutputIntegrandClass> ProblemStatementClass;

  // Grid
  int ii = 20;
  int jj = ii;

  Real L = 50.0;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld = std::make_shared<XField2D_Box_Triangle_X1>( ii, jj, 0, L, 0, 25, true );
//  XField2D_Box_Triangle_X1 xfld(ii, jj, 0, 50, 0, 25, true);
//  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, 50, 0, 25 );

  int order = 1;

  // PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 1;
  CapillaryModel cap_model(pc_max);

  NDPDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 1.0;
  const Real SwR = 0.1;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(pde, SwL, SwR, xinit);

  // BCs

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCInit;
  BCInit[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  BCInit[BCBuckleyLeverett1DParams<BCTypeTimeIC>::params.qB] = SwR;

  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params.qB] = SwL;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params.qB] = SwR;

  PyDict PyBCList;
  PyBCList["DirichletL"] = BCDirichletL;
  PyBCList["DirichletR"] = BCDirichletR;
  PyBCList["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletL"] = {3}; //Left boundary
  BCBoundaryGroups["DirichletR"] = {1}; //Right boundary
  BCBoundaryGroups["BCInit"] = {0}; //Bottom boundary
  BCBoundaryGroups["None"] = {2}; //Top boundary

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // HDG discretization
//  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient, L );
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

  const int quadOrder = -1;

  //Output functional
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Newton solver
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);


  //--------ADAPTATION LOOP--------

  int maxIter = 20;
  Real targetCost = 8000;

  std::string adapthist_filename = "tmp/test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Create solver interface
  std::shared_ptr<ProblemStatementClass> pProblem;
  pProblem = std::make_shared<ProblemStatementClass>(*pxfld, pde, disc, tol, quadOrder, order,
                                                     BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                     cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups,
                                                     NewtonSolverDict, outputIntegrand);

  //Set initial solution
  pProblem->setInitialSolution(solnExact, cellGroups);
//  pProblem->setInitialSolution(0.5);

  std::string qfld_init_filename = "tmp/qfld_init_a0.plt";
  pProblem->exportPrimalSolutionTo(qfld_init_filename);

  pProblem->solveGlobalPrimalProblem();
  pProblem->solveGlobalAdjointProblem();

  std::string qfld_filename = "tmp/qfld_a0.plt";
  pProblem->exportPrimalSolutionTo(qfld_filename);

  std::string adjfld_filename = "tmp/adjfld_a0.plt";
  pProblem->exportAdjointSolutionTo(adjfld_filename);


  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout<<std::endl<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //Compute error estimates
    pProblem->computeErrorEstimates();

    //Perform local sampling and adapt mesh
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
    pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pProblem, iter);

    interiorTraceGroups.clear();
    for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<ProblemStatementClass> pProblemNew;
    pProblemNew = std::make_shared<ProblemStatementClass>(*pxfldNew, pde, disc, tol, quadOrder, order,
                                                          BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                          cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups,
                                                          NewtonSolverDict, outputIntegrand);

    //Perform L2 projection from solution on previous mesh
    pProblemNew->setInitialSolution(pProblem->getPrimalSolutionField());

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pProblem = pProblemNew;

    qfld_init_filename = "tmp/qfld_init_a" + std::to_string(iter+1) + ".plt";
    pProblem->exportPrimalSolutionTo(qfld_init_filename);

    pProblem->solveGlobalPrimalProblem();
    pProblem->solveGlobalAdjointProblem();

    qfld_filename = "tmp/qfld_a" + std::to_string(iter+1) + ".plt";
    pProblem->exportPrimalSolutionTo(qfld_filename);

    adjfld_filename = "tmp/adjfld_a" + std::to_string(iter+1) + ".plt";
    pProblem->exportAdjointSolutionTo(adjfld_filename);
  }

  fadapthist.close();
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
