// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve1D_DGAdvective_Line_Euler_Duct_toy

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/BCEuler1D.h"
#include "pde/NS/SolutionFunction_Euler1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/output_gnuplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Surreal/SurrealS.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "tools/output_std_vector.h"

using namespace std;
using namespace SANS;

namespace SANS
{
template<> struct Type2String<QTypeEntropy>
{
  static std::string str()
  {
    return "EntropyVariables";
  }
};
template<> struct Type2String<QTypeConservative>
{
  static std::string str()
  {
    return "ConservativeVariables";
  }
};
template<> struct Type2String<QTypePrimitiveRhoPressure>
{
  static std::string str()
  {
    return "PrimitiveVariables";
  }
};
template<> struct Type2String<QTypePrimitiveSurrogate>
{
  static std::string str()
  {
    return "SurrogateVariables";
  }
};

}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGAdvective_Line_Euler_SSME_toy )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGAdvective_Line_Euler_SSME_toy )
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // MPI comm world
  ////////////////////////////////////////////////////////////////////////////////////////
  mpi::communicator world;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for Euler1D
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for discretization
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;
  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD1,
                                           TopoD1> > PDEPrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, XField<PhysD1,
                                   TopoD1> > AlgebraicEquationSet_PTCClass;

  typedef PDEPrimalEquationSetClass::BCParams BCParams;
  typedef PDEPrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PDEPrimalEquationSetClass::SystemVector SystemVectorClass;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Solution order
  ////////////////////////////////////////////////////////////////////////////////////////
  int order_pde = 0;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Area function for the nozzle contour
  ////////////////////////////////////////////////////////////////////////////////////////

  std::shared_ptr<ScalarFunction1D_SSME> area( new ScalarFunction1D_SSME() );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our Euler PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  // Gas Model
  Real gamma, R;
  gamma = 1.1875;//1.147;
  R = 942;
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas( gasModelDict );
  NDPDEClass pde( gas, PDEClass::Euler_ResidInterp_Momentum, area);
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create grid
  ////////////////////////////////////////////////////////////////////////////////////////
  int nElements = 1000;
  Real xL = -0.22669;
  Real xR = 3.0734;
  XField1D xfld( nElements, xL, xR );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Define boundary conditions
  ////////////////////////////////////////////////////////////////////////////////////////
#define EXACT_IC 0
  const Real Tc = 3639;
  const Real Pc = 2935.7 * 6897.8;
  const Real rhoc = Pc / (R * Tc);

  // At exit, M = 4.3344, Pe = 23275.2 Pa
  Real Pa = 23275.2;

  typedef BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDEClass> BCClass;
  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTt_mitState;
  BCInflow[BCClass::ParamsType::params.TtSpec] = Tc;
  BCInflow[BCClass::ParamsType::params.PtSpec] = Pc;

  // Define BC list
  PyDict PyPDEBCList;
  PyPDEBCList["Inflow"] = BCInflow;
  std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;
  PDEBCBoundaryGroups["Inflow"] = { 0}; //left

  PyDict BCOutflow;

#if EXACT_IC
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_mitState;
  BCOutflow[BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_mitState>::params.pSpec] = Pa;
  std::cout << "Outflow pressure: " << Pa << " (" << Pa << ")" << std::endl;
  //BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.None;
  PyPDEBCList["Outflow"] = BCOutflow;
  PDEBCBoundaryGroups["Outflow"] = { 1};
  BCParams::checkInputs( PyPDEBCList );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Check BCs
  ////////////////////////////////////////////////////////////////////////////////////////
  BCParams::checkInputs( PyPDEBCList );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Allocate our Euler PDE field
  ////////////////////////////////////////////////////////////////////////////////////////
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> pde_qfld( xfld, order_pde, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> pde_lgfld( xfld, order_pde, BasisFunctionCategory_Legendre,
                                                            BCParams::getLGBoundaryGroups( PyPDEBCList, PDEBCBoundaryGroups ) );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Calculate exact solution and project it as IC
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef SolutionFunction_Euler1D_SSME<TraitsSizeEuler, TraitsModelEulerClass> SolutionClass;
  typedef SolnNDConvertSpace<PhysD1, SolutionClass> NDExactSolutionClass;

  NDExactSolutionClass solnExact( gas, gamma, R, rhoc, Tc );

  for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous( solnExact, { 0 } ), (xfld, pde_qfld) );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output IC gnuplot file
  ////////////////////////////////////////////////////////////////////////////////////////
  string filenameStem_IC = "tmp/SSME_" + Type2String<QType>::str() + "_P" + std::to_string( order_pde ) + "_IC." + std::to_string( world.rank() );
  output_gnuplot( pde_qfld, filenameStem_IC + ".gplt" );
  output_gnuplot( pde_qfld, filenameStem_IC + ".txt" );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output gnuplot file
  ////////////////////////////////////////////////////////////////////////////////////////
//  string filenameStem_res = "tmp/SSME_" + Type2String<QType>::str() + "_P" + std::to_string(order_pde) + "_res." + std::to_string(world.rank());
//  output_gnuplot( res, filenameStem_res + ".gplt" );
  ////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////
  // AES Settings
  ////////////////////////////////////////////////////////////////////////////////////////
  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = { 8e-5, 1e-12 };
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Create our AES to solve our PDE
  ////////////////////////////////////////////////////////////////////////////////////////
  PDEPrimalEquationSetClass PrincipalEqSet( xfld, pde_qfld, pde_lgfld, pde, quadratureOrder, ResidualNorm_L2, tol, { 0 }, { 0 }, PyPDEBCList,
                                            PDEBCBoundaryGroups );

  AlgebraicEquationSet_PTCClass PrincipalEqSetPTC( xfld, pde_qfld, pde, quadratureOrder, { 0 }, PrincipalEqSet );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Construct Newton Solver
  ////////////////////////////////////////////////////////////////////////////////////////
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.ResidualHistoryFile] = "tmp/residual.txt";

  NewtonSolverParam::checkInputs( NewtonSolverDict );

  NewtonSolver<SystemMatrixClass> Solver( PrincipalEqSet, NewtonSolverDict );

  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set initial guess from initial condition
  ////////////////////////////////////////////////////////////////////////////////////////
  SystemVectorClass ini( PrincipalEqSet.vectorStateSize() );
  SystemVectorClass sln( PrincipalEqSet.vectorStateSize() );

  PrincipalEqSet.fillSystemVector( ini );
  sln = ini;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // SOLVE
  ////////////////////////////////////////////////////////////////////////////////////////
#define USE_NEWTON_SOLVE 1
#if USE_NEWTON_SOLVE
  SolveStatus status = Solver.solve( ini, sln );
  BOOST_CHECK( status.converged );
#else
  bool converged = false;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  PyDict PseudoTimeDict;
  PseudoTimeDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  PseudoTimeDict[PseudoTimeParam::params.invCFL] = 1;
  PseudoTimeDict[PseudoTimeParam::params.invCFL_max] = 100;
  PseudoTimeDict[PseudoTimeParam::params.invCFL_min] = 0.0;

  PseudoTimeDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.5;  //0.5;
  PseudoTimeDict[PseudoTimeParam::params.CFLIncreaseFactor] = 10;
  PseudoTimeDict[PseudoTimeParam::params.Verbose] = true;

  PseudoTime<SystemMatrixClass> PTC(PseudoTimeDict, PrincipalEqSetPTC);
  converged = PTC.iterate(100);
  BOOST_CHECK(converged);
#endif

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output Tecplot file
  ////////////////////////////////////////////////////////////////////////////////////////
  string filenameStem = "tmp/SSME_" + Type2String<QType>::str() + "_P" + std::to_string( order_pde ) + "." + std::to_string( world.rank() );
  output_Tecplot( pde_qfld, filenameStem + ".plt" );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Output gnuplot file
  ////////////////////////////////////////////////////////////////////////////////////////
  output_gnuplot( pde_qfld, filenameStem + ".gplt" );
  output_gnuplot( pde_qfld, filenameStem + ".txt" );
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Get residual
  ////////////////////////////////////////////////////////////////////////////////////////
  SystemVectorClass q( PrincipalEqSet.vectorStateSize() );
  PrincipalEqSet.fillSystemVector( q );
  SystemVectorClass rsd( PrincipalEqSet.vectorEqSize() );
  rsd = 0.0;
  PrincipalEqSet.residual( q, rsd );
  PrincipalEqSet.setSolutionField( rsd );
  //std::cout << "Rsd " << rsd << std::endl;
  ////////////////////////////////////////////////////////////////////////////////////////
  // Output file
  ////////////////////////////////////////////////////////////////////////////////////////
  filenameStem = "tmp/SSME_" + Type2String<QType>::str() + "_P" + std::to_string( order_pde ) + "." + std::to_string( world.rank() ) + "_residual";
  ////////////////////////////////////////////////////////////////////////////////////////
  // Output gnuplot file
  ////////////////////////////////////////////////////////////////////////////////////////
  output_gnuplot( pde_qfld, filenameStem + ".gplt" );
  output_gnuplot( pde_qfld, filenameStem + ".txt" );
  ////////////////////////////////////////////////////////////////////////////////////////
#else
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> pde_qfld_old( xfld, order_pde, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> pde_lgfld_old( xfld, order_pde, BasisFunctionCategory_Legendre,
                                                            BCParams::getLGBoundaryGroups( PyPDEBCList, PDEBCBoundaryGroups ) );

  for (int i=0; i<1; i++)
  {

    Pa = 0.99999*Pc;//0.999965248370329*Pc;//Pc*pow(0.9, i);

    BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSupersonic_Pressure_mitState;
    BCOutflow[BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_mitState>::params.pSpec] = Pa;

    std::cout << std::setprecision( 16 );
    std::cout << "Inlet pressure: " << Pc<< std::endl;
    std::cout << "Outflow pressure ratio: " << Pa/Pc << " (" << Pa << ")" << std::endl;
    std::cout << "i: " << i << std::endl;

    PyPDEBCList["Outflow"] = BCOutflow;
    PDEBCBoundaryGroups["Outflow"] = { 1};
    BCParams::checkInputs( PyPDEBCList );

    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Check BCs
    ////////////////////////////////////////////////////////////////////////////////////////
    BCParams::checkInputs( PyPDEBCList );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Allocate our Euler PDE field
    ////////////////////////////////////////////////////////////////////////////////////////
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> pde_qfld( xfld, order_pde, BasisFunctionCategory_Legendre );
    Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> pde_lgfld( xfld, order_pde, BasisFunctionCategory_Legendre,
                                                              BCParams::getLGBoundaryGroups( PyPDEBCList, PDEBCBoundaryGroups ) );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set IC
    ////////////////////////////////////////////////////////////////////////////////////////
    if (i == 0)
    {
      ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure1D<Real>( rhoc, 0.0, Pc ) );
      pde_qfld = q0;
      pde_lgfld = 0;
    }
    else
    {
      for (int j = 0; j < pde_qfld_old.nDOF(); j++)
      {
        pde_qfld.DOF( j ) = pde_qfld_old.DOF( j );
      }
      for (int j = 0; j < pde_lgfld_old.nDOF(); j++)
      {
        pde_lgfld.DOF( j ) = pde_lgfld_old.DOF( j );
      }
    }
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Output IC gnuplot file
    ////////////////////////////////////////////////////////////////////////////////////////
    string filenameStem_IC = "tmp/SSME_" + Type2String<QType>::str() + "_P" + std::to_string( order_pde ) + "_IC." + std::to_string( world.rank() );
    output_gnuplot( pde_qfld, filenameStem_IC + ".gplt" );
    output_gnuplot( pde_qfld, filenameStem_IC + ".txt" );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // AES Settings
    ////////////////////////////////////////////////////////////////////////////////////////
    QuadratureOrder quadratureOrder( xfld, -1 );
    std::vector<Real> tol = { 9e-3, 1e-12 };
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Create our AES to solve our PDE
    ////////////////////////////////////////////////////////////////////////////////////////
    PDEPrimalEquationSetClass PrincipalEqSet( xfld, pde_qfld, pde_lgfld, pde, quadratureOrder, ResidualNorm_L2, tol, { 0 }, { 0 }, PyPDEBCList,
                                              PDEBCBoundaryGroups );

    AlgebraicEquationSet_PTCClass PrincipalEqSetPTC( xfld, pde_qfld, pde, quadratureOrder, { 0 }, PrincipalEqSet );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Construct Newton Solver
    ////////////////////////////////////////////////////////////////////////////////////////
    PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;

    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

    LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
    NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
    NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
    NewtonSolverParam::checkInputs( NewtonSolverDict );

    NewtonSolver<SystemMatrixClass> Solver( PrincipalEqSet, NewtonSolverDict );

    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set initial guess from initial condition
    ////////////////////////////////////////////////////////////////////////////////////////
    SystemVectorClass ini( PrincipalEqSet.vectorStateSize() );
    SystemVectorClass sln( PrincipalEqSet.vectorStateSize() );

    PrincipalEqSet.fillSystemVector( ini );
    sln = ini;
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // SOLVE
    ////////////////////////////////////////////////////////////////////////////////////////
#define USE_NEWTON_SOLVE 0
#if USE_NEWTON_SOLVE
      SolveStatus status = Solver.solve( ini, sln );
      BOOST_CHECK( status.converged );
      std::cout << "here" <<std::endl;
#else
    bool converged = false;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 200;
    PyDict PseudoTimeDict;
    PseudoTimeDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
    PseudoTimeDict[PseudoTimeParam::params.invCFL] = 1;
    PseudoTimeDict[PseudoTimeParam::params.invCFL_max] = 100;
    PseudoTimeDict[PseudoTimeParam::params.invCFL_min] = 0.0;

//    PseudoTimeDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.5;//0.1;
//    PseudoTimeDict[PseudoTimeParam::params.CFLIncreaseFactor] = 10;//2.0;
    PseudoTimeDict[PseudoTimeParam::params.Verbose] = true;
    //PseudoTimeDict[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/residual_" + std::to_string( i ) + ".txt";
    PseudoTime<SystemMatrixClass> PTC( PseudoTimeDict, PrincipalEqSetPTC );
    converged = PTC.iterate( 10000 );
    BOOST_CHECK( converged );
#endif
    for (int j = 0; j < pde_qfld.nDOF(); j++)
    {
      pde_qfld_old.DOF( j ) = pde_qfld.DOF( j );
    }
    for (int j = 0; j < pde_lgfld.nDOF(); j++)
    {
      pde_lgfld_old.DOF( j ) = pde_lgfld.DOF( j );
    }

    ////////////////////////////////////////////////////////////////////////////////////////

    // Output Tecplot file
    ////////////////////////////////////////////////////////////////////////////////////////
    string filenameStem = "tmp/SSME_" + Type2String<QType>::str() + "_P" + std::to_string( order_pde ) + "." + std::to_string( world.rank() );
    output_Tecplot( pde_qfld, filenameStem + "_" + std::to_string(i) + ".plt" );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Output gnuplot file
    ////////////////////////////////////////////////////////////////////////////////////////
    output_gnuplot( pde_qfld, filenameStem + ".gplt" );
    output_gnuplot( pde_qfld, filenameStem + "_" + std::to_string(i) +".txt" );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    // Get residual
    ////////////////////////////////////////////////////////////////////////////////////////
    SystemVectorClass q( PrincipalEqSet.vectorStateSize() );
    PrincipalEqSet.fillSystemVector( q );
    SystemVectorClass rsd( PrincipalEqSet.vectorEqSize() );
    rsd = 0.0;
    PrincipalEqSet.residual( q, rsd );
    PrincipalEqSet.setSolutionField( rsd );
    ////////////////////////////////////////////////////////////////////////////////////////
    // Output file
    ////////////////////////////////////////////////////////////////////////////////////////
    filenameStem = "tmp/SSME_" + Type2String<QType>::str() + "_P" + std::to_string( order_pde ) + "." + std::to_string( world.rank() ) + "_residual";
    ////////////////////////////////////////////////////////////////////////////////////////
    // Output gnuplot file
    ////////////////////////////////////////////////////////////////////////////////////////
    output_gnuplot( pde_qfld, filenameStem + ".gplt" );
    output_gnuplot( pde_qfld, filenameStem + "_" + std::to_string(i) + ".txt" );
    ////////////////////////////////////////////////////////////////////////////////////////

  }

#endif
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
