// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "Field/output_Tecplot.h"
#include "Field/output_grm.h"

#include "unit/UnitGrids/XField2D_Annulus_Triangle_Lagrange_Xq.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

BOOST_AUTO_TEST_SUITE( Annulus2D_Meshing_test_suite )

Real Rlower(Real theta)
{ return 1; }

Real Rupper(Real theta)
{ return 2; }

BOOST_AUTO_TEST_CASE( Annulus2D_Meshing_Constant_Radii )
{


mpi::communicator comm;

int refinement = 2;
int order = 4;
int ii = 6*pow(2, refinement);
int jj = 2*pow(2, refinement);
Real thetamin = PI/2;
Real thetamax = 3*PI/2;
string filename = "tmp/test_annulus_grid";

XField2D_Annulus_Triangle_Lagrange_Xq xfld(comm, ii, jj, order, thetamin, thetamax, Rlower, Rupper);
output_Tecplot(xfld, filename + ".plt");
output_grm(xfld, filename + ".grm");

}
BOOST_AUTO_TEST_SUITE_END()
