// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt3D_AD_TripleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

//#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/OutputCell_Solution.h"
// #include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field/FieldVolume_CG_Cell.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#endif

#include "tools/linspace.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_DGBR2_AD_TripleBoundaryLayer_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_DGBR2_AD_TripleBoundaryLayer )
{
  typedef ScalarFunction3D_TripleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputNDConvertSpace<PhysD3, OutputCell_SolutionErrorSquared<PDEClass, SolutionExact>> L2ErrorClass;
  typedef IntegrandCell_DGBR2_Output<L2ErrorClass> L2ErrorIntegrandClass;

#ifndef BOUNDARYOUTPUT
  typedef OutputCell_Solution<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputAdvectionDiffusion3D_WeightedResidual OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // global communicator
  mpi::communicator world;

  // Grid
  std::vector<Real> xpts = linspace(0,1,3);
  std::vector<Real> ypts(xpts.begin(),xpts.end());
  std::vector<Real> zpts(xpts.begin(),xpts.end());

  // 8^2 + 9^2 + 12^2 = 17^2
  // Pe = 1/25
  Real a = 8.0/17.0;
  Real b = 9.0/17.0;
  Real c = 12.0/17.0;
  Real nu = 1./50.;

  // PDE
  AdvectiveFlux3D_Uniform adv( a, b, c );

  ViscousFlux3D_Uniform visc( nu, 0, 0, 0, nu, 0, 0, 0, nu );

  Source3D_UniformGrad source(0,0,0,0);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function.Name] =
           BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function.TripleBL;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.c] = c;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;
  solnArgs[NDSolutionExact::ParamsType::params.offset] = 1;
  solnArgs[NDSolutionExact::ParamsType::params.scale] = -1;

  NDSolutionExact solnExact( solnArgs );

  NDPDEClass pde( adv, visc, source );

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Upwind] = true;

  PyDict BCSoln_Robin;
  BCSoln_Robin[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln_Robin[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  BCSoln_Robin[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType.Robin;
  BCSoln_Robin[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Upwind] = true;

  PyDict PyBCList;
  PyBCList["BCSoln_Dirichlet"] = BCSoln_Dirichlet;
  PyBCList["BCSoln_Robin"] = BCSoln_Robin;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSoln_Dirichlet"] = {XField3D_Box_Tet_X1::iXmax,
                                          XField3D_Box_Tet_X1::iYmax,
                                          XField3D_Box_Tet_X1::iZmax,
                                          XField3D_Box_Tet_X1::iXmin,
                                          XField3D_Box_Tet_X1::iYmin,
                                          XField3D_Box_Tet_X1::iZmin};

  BCBoundaryGroups["BCSoln_Robin"] = {};

    //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 2*Tet::NTrace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-12, 1e-12};

#ifndef BOUNDARYOUTPUT
  //Output functional
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
  // From Mathematica!
  const Real trueOutput = 1 - ((exp(a/nu)*(a - nu) + nu)*(exp(b/nu)*(b - nu) + nu)*(exp(c/nu)*(c - nu) + nu))
                              /(a*b*c*(-1 + exp(a/nu))*(-1 + exp(b/nu))*(-1 + exp(c/nu)));
#else
  // Residual weighted boundary output
  NDOutputClass fcnOutput(1.);
  OutputIntegrandClass outputIntegrand( fcnOutput, {XField3D_Box_Tet_X1::iXmax} );
  // From Mathematica!
  const Real trueOutput = a - (a*exp(a/nu)*(exp(b/nu)*(b - nu) + nu)*(exp(c/nu)*(c - nu) + nu))
                              /(b*c*(-1 + exp(a/nu))*(-1 + exp(b/nu))*(-1 + exp(c/nu)));
#endif

  // L2 Error output functional
  L2ErrorClass L2ErrorOutput(solnExact);
  L2ErrorIntegrandClass L2ErrorIntegrand(L2ErrorOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, AdjLinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)
  if (world.rank() == 0 )
      std::cout << "Linear solver: PETSc" << std::endl;

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 3;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-14;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
//  PETScDict[SLA::PETScSolverParam::params.printMatrixInfo] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  // PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  // PETScDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  //Change parameters for adjoint solve
  PyDict PreconditionerILU_adjoint = PreconditionerILU;
  PyDict PreconditionerDict_adjoint = PreconditionerDict;
  PyDict PETScDict_adjoint = PETScDict;
  // PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+2)*(order+3)*(order+4)/6; //elemDOF for p=order+1
  PreconditionerILU_adjoint[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict_adjoint[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict_adjoint;
  PETScDict_adjoint[SLA::PETScSolverParam::params.MaxIterations] = 5000;
  PETScDict_adjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  // PyDict UMFPACKDict;
  // UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict_adjoint;

#elif defined(INTEL_MKL)

  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  AdjLinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::string mesher = "Epic";
  std::string file_tag = "";
  bool dumpField = false;
  int maxIter = 50;

  int orderL = 1, orderH = 3;
  int powerL = 0, powerH = 7;

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  // -- mesher file_tag dumpField order power maxIter
  if (argc >= 2)
    mesher = std::string(argv[1]);
  if (argc >= 3)
    file_tag = std::string(argv[2]);
  if (argc >= 4)
    dumpField = std::stoi(argv[3]);
  if (argc >= 5)
    orderL = orderH = std::stoi(argv[4]);
  if (argc >= 6)
    powerL = powerH = std::stoi(argv[5]);
  if (argc >= 7)
    maxIter = std::stoi(argv[6]);

  std::cout << "mesher: " << mesher << ", file_tag: " << file_tag << ", dumpField: " << dumpField;
  std::cout << ", orderL,H = " << orderL << ", " << orderH << ", powerL,H = " << powerL << ", " << powerH << "maxIter: " << maxIter << std::endl;
#endif

  std::shared_ptr<XField<PhysD3,TopoD3>> pxfld;

  #define CHAIN_ADAPT 0  // Whether to use the lower DOF mesh as the initial condition for the next dof target

  //--------ADAPTATION LOOP--------
  for (int order = orderL; order <= orderH; order++)
  {
#if CHAIN_ADAPT
#ifdef SANS_AVRO
    std::shared_ptr<avro::Context> context;
#endif
    PyDict MesherDict;
    if (mesher == MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic)
    {
      MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;

      pxfld = std::make_shared<XField3D_Box_Tet_X1>( world, xpts, ypts );
    }
#ifdef SANS_AVRO
    else if (mesher == MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.avro)
    {
      MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.avro;

      MesherDict[avroParams::params.Curved] = false; // is the grid curved?
      MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
      MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files

      using avro::coord_t;
      using avro::index_t;

      context = std::make_shared<avro::Context>();

      coord_t number = 3;
      Real x0[3] = {0.,0.,0.};
      std::vector<Real> lens(number,1.);
      std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>( context.get() , x0 , lens.data() );
      std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(),"cube");
      model->addBody(pbody,true);

      // initialize the mesh
      XField3D_Box_Tet_X1 xfld0( world, xpts, ypts, zpts );
      // copy the mesh into the domain and attach the geometry
      pxfld = std::make_shared< XField_avro<PhysD3,TopoD3> >(xfld0,model);
    }
#endif
    else if (mesher == "fefloa")
    {
      MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.FeFloa;

      // uniform initial
      // pxfld = std::make_shared<XField3D_Box_Tet_X1>( world, ii, jj );
      // Initial cross clustering
      pxfld = std::make_shared<XField3D_Box_Tet_X1>( world, xpts, ypts );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown mesher");
#endif

#if CHAIN_ADAPT
    for (int power = powerL; power <= powerH; power++)
#else
    for (int power = powerH; power >= powerL; power--)
#endif
    {
      timer totalTime;
      int nk = pow(2,power);
      int targetCost = 1000*nk;

      // to make sure folders have a consistent number of zero digits
      const int string_pad = 6;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);

      std::string filename_base = "tmp/AD_3D/TripleBoundaryLayer/" + mesher + "_";

      filename_base += "DG_";

      if (file_tag.size() > 0)
        filename_base += file_tag + "_"; // the additional file name bits kept especially

      filename_base += int_pad + "_P" + std::to_string(order) + "/";

      std::cout<< filename_base << std::endl;

      boost::filesystem::create_directories(filename_base);

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;
      MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.AffineInvariant;
      MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;

#if not(CHAIN_ADAPT)
#ifdef SANS_AVRO
      std::shared_ptr<avro::Context> context;
#endif
      PyDict MesherDict;
      if (mesher == MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic)
      {
        MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Epic;

        // MesherDict[EpicParams::params.nThread] = 4; // DO NOT USE THIS ON HYPERSONIC

        // initialize the mesh
        pxfld = std::make_shared<XField3D_Box_Tet_X1>( world, xpts, ypts, zpts );
      }
#ifdef SANS_AVRO
      else if (mesher == MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.avro)
      {
        MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.avro;

        MesherDict[avroParams::params.Curved] = false; // is the grid curved?
        MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
        MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files

        using avro::coord_t;
        using avro::index_t;

        context = std::make_shared<avro::Context>();

        coord_t number = 3;
        Real x0[3] = {0.,0.,0.};
        std::vector<Real> lens(number,1.);
        std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>( context.get() , x0 , lens.data() );
        std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(context.get(),"cube");
        model->addBody(pbody,true);

        // initialize the mesh
        XField3D_Box_Tet_X1 xfld0( world, xpts, ypts, zpts );
        // copy the mesh into the domain and attach the geometry
        pxfld = std::make_shared< XField_avro<PhysD3,TopoD3> >(xfld0, model);
      }
#endif
      else if (mesher == "fefloa")
      {
        MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.FeFloa;

        // initialize the mesh
        pxfld = std::make_shared<XField3D_Box_Tet_X1>( world, xpts, ypts, zpts );
      }
      else
        SANS_DEVELOPER_EXCEPTION("Unknown mesher");
#endif

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.dumpStepMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.dumpRateMatrix] = false;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.hasTrueOutput] = true;
      AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TrueOutput] = trueOutput;

      MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

      std::string output_filename = filename_base + "output.dat";
      fstream foutputhist;
      if ( world.rank() == 0 )
      {
        foutputhist.open( output_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(foutputhist.good(), "Error opening file: " + output_filename);
      }

      if (world.rank() == 0 )
      {
        // write the header to the output file
        foutputhist << "VARIABLES="
                    << std::setw(5)  << "\"Iter\""
                    << std::setw(10) << "\"DOF\""
                    << std::setw(20) << "\"Elements\""
                    // << std::setw(20) << "\"u<sup>2</sup>\""
                    << std::setw(20) << "\"J<sub>h</sub>\""
                    << std::setw(20) << "\"|J - J<sub>h</sub>|\""
                    << std::setw(20) << "\"Estimate\""
                    << std::setw(20) << "\"L<sup>2</sup> Error\""
                    << std::endl;

        foutputhist << "ZONE T=\"MOESS " << mesher << " " << nk << "k\"" << std::endl;
      }

      MesherDict[refineParams::params.FilenameBase] = filename_base;
      MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

      std::vector<int> cellGroups = {0};
      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      const int quadOrder = 2*(order + 1);

      int iter = 0;
      while (true)
      {
        if (world.rank() == 0)
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;


        std::shared_ptr<SolutionClass> pGlobalSol;
        pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                     BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                     active_boundaries, disc);

        pGlobalSol->setSolution(0.0);

        std::shared_ptr<SolverInterfaceClass> pInterface;
        pInterface= std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                            cellGroups, interiorTraceGroups,
                                                            PyBCList, BCBoundaryGroups,
                                                            SolverContinuationDict, AdjLinearSolverDict,
                                                            outputIntegrand);

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();


        if (dumpField)
        {
          std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter)
                                                    + "_P" + std::to_string(order) +
                                                    + ".plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

          std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter)
                                                      + "_P" + std::to_string(order) +
                                                      + ".plt";
          output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
        }
        else if ( iter == maxIter )
        {
          std::string qfld_filename = filename_base + "qfld_final.plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );
          std::string adjfld_filename = filename_base + "adjfld_final.plt";
          output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );
        }

#ifdef SANS_MPI
        int nDOFtotal = 0;
        boost::mpi::reduce(*pxfld->comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>(), 0 );

        // count the number of elements possessed by this processor
        int nElem = 0;
        for (int elem = 0; elem < pxfld->nElem(); elem++ )
          if (pxfld->getCellGroupGlobal<Tet>(0).associativity(elem).rank() == world.rank())
            nElem++;

        int nElemtotal = 0;
        boost::mpi::reduce(*pxfld->comm(), nElem, nElemtotal, std::plus<int>(), 0 );
#else
        int nDOFtotal = pGlobalSol->primal.qfld.nDOFpossessed();
        int nElemtotal = pxfld->nElem();
#endif

        //Compute error estimates
        pInterface->computeErrorEstimates();

        Real J = pInterface->getOutput();
        Real globalEstimate  = pInterface->getGlobalErrorEstimate();

        QuadratureOrder quadrule(*pxfld, quadOrder);

        Real L2error = 0;
        IntegrateCellGroups<TopoD3>::integrate( FunctionalCell_DGBR2( L2ErrorIntegrand, L2error ),
                                                *pxfld, (pGlobalSol->primal.qfld, pGlobalSol->primal.rfld),
                                                quadrule.cellOrders.data(), quadrule.cellOrders.size() );

  #ifdef SANS_MPI
        L2error = boost::mpi::all_reduce( *pxfld->comm(), L2error, std::plus<Real>() );
  #endif
        L2error = sqrt(L2error);

        if (world.rank() == 0 )
        {
          foutputhist << std::setw(5) << iter
                      << std::setw(10) << nDOFtotal
                      << std::setw(10) << nElemtotal
                      << std::setw(20) << std::setprecision(10) << std::scientific << J
                      << std::setw(20) << std::setprecision(10) << std::scientific << fabs(J-trueOutput)
                      << std::setw(20) << std::setprecision(10) << std::scientific << globalEstimate
                      << std::setw(20) << std::setprecision(10) << std::scientific << L2error
                      << std::endl;
        }
        if ( iter == maxIter ) break;

        //Perform local sampling and adapt mesh
        pxfld = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);
        iter++;
      }
      if (world.rank() == 0)
        fadapthist << "\n\nTotal Time elapsed: " << totalTime.elapsed() << "s" << std::endl;
      fadapthist.close();
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
