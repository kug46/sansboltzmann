// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGADS/EGPlane.h"
#include "Meshing/EGADS/extrude.h"

#include <vector>
#include <iostream>
#include <cstdio>

using namespace SANS;
using namespace SANS::EGADS;

namespace SANS
{
namespace EGADS
{
//Explicitly instantiate the class to generate all the functions so that coverage information is correct

}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( EGADS )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EGModel_ctor )
{
  // cppcheck-suppress unreadVariable
  static const int Dim = 3;

  EGContext context((CreateContext()));

  // outer frame
  EGNode<Dim> n0(context, {-50.0, -50.0, 0.0});
  EGNode<Dim> n1(context, { 50.0, -50.0, 0.0});
  EGNode<Dim> n2(context, { 50.0,  50.0, 0.0});
  EGNode<Dim> n3(context, {-50.0,  50.0, 0.0});

  EGEdge<Dim> edge0(n0, n1);
  EGEdge<Dim> edge1(n1, n2);
  EGEdge<Dim> edge2(n2, n3);
  EGEdge<Dim> edge3(n3, n0);

  // EGEdge<Dim> circle( EGCircle<Dim>(context, {0.5,0.5}, 0.25) );

  EGNode<Dim> n4(context, {-0.5,  0.0, 0.0});
  EGNode<Dim> n5(context, { 0.5,  0.5, 0.0});
  EGNode<Dim> n6(context, { 0.5, -0.5, 0.0});

  EGEdge<Dim> edge4(n4, n5);
  EGEdge<Dim> edge5(n5, n6);
  EGEdge<Dim> edge6(n6, n4);

  EGLoop<Dim> loop0(context, {(edge0, 1), (edge1, 1), (edge2, 1), (edge3, 1)}, CLOSED);
  EGLoop<Dim> loop1(context, {(edge4, 1), (edge5, 1), (edge6, 1)}, CLOSED);

  EGPlane<Dim> plane(context, {0.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0});
  EGFace<Dim> face(plane, {(loop0, 1), (loop1, -1)});

  DLA::VectorS<3, double> normal= {0., 0., 1.};

  EGBody<Dim> volbody= extrude(face, 100.0, normal);

  EGModel<Dim> model(volbody);

  std::string filename= "/home/cfrontin/Desktop/wedge.egads";

  model.save( filename );

  // EGModel<Dim> model2(context);
  //
  // model2.load( filename );
  //
  // //Remove the test file from the file system
  // std::remove(filename.c_str());
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
