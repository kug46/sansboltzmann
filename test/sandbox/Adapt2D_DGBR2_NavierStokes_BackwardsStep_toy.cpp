// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DGBR2_NavierStokes_BackwardsStep_btest
// Testing of the MOESS framework on a 2D Navier-Stokes problem


#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_BackwardsStep_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_NavierStokes_BackwardsStep_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_NavierStokes_BackwardsStep_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_EntropyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;

  // Grid
  const int initGridPower = 1;
  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_BackwardsStep_X1(world, initGridPower,4,30,4) );

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1.0e-12, 1.0e-12};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.1;
  const Real Reynolds = 400;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real tRef = 1;          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum );

  // initial condition
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  // DGBR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

#if 0

  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_mitState;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.vtSpec] = vRef;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;

#else

  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aSpec = atan(vRef/uRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
#endif

  PyDict PyBCList;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNoSlip"] = {XField2D_BackwardsStep_X1::iBottomInlet,
                                  XField2D_BackwardsStep_X1::iStep,
                                  XField2D_BackwardsStep_X1::iBottomOutlet,
                                  XField2D_BackwardsStep_X1::iTop};
  BCBoundaryGroups["BCOut"] = {XField2D_BackwardsStep_X1::iOutlet};
  BCBoundaryGroups["BCIn"] = {XField2D_BackwardsStep_X1::iInlet};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

#ifndef BOUNDARYOUTPUT
  // Entropy output
  NDOutputClass fcnOutput(pde, 1.0);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  // Drag output
  NDOutputClass outputFcn(pde, 1., 0.);
  OutputIntegrandClass outputIntegrand( outputFcn, {XField2D_BackwardsStep_X1::iBottomInlet,
                                                    XField2D_BackwardsStep_X1::iStep,
                                                    XField2D_BackwardsStep_X1::iBottomOutlet} );
#endif

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

#if defined(SANS_PETSC)

  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  // PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
  //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

  PyDict PETScDictAdjoint(PETScDict);
  PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;

  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

  std::cout << "Linear solver: PETSc" << std::endl;
#elif defined(INTEL_MKL)
  std::cout << "Linear solver: MKL_PARDISO" << std::endl;
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  std::cout << "Linear solver: UMFPACK" << std::endl;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-32;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  PyDict SolverContinuationDict0, NonlinearSolverDict0, NewtonSolverDict0(NewtonSolverDict);
  NewtonSolverDict0[NewtonSolverParam::params.MaxIterations] = 1;

  // Newton for
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  //PTC for
  NonlinearSolverDict0[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                      = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict0[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict0;
  NonlinearSolverDict0[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict0[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict0[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict0[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict0[PseudoTimeParam::params.invCFL_max] = 1e3;

  SolverContinuationDict0[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict0;
  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict0);
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  //--------ADAPTATION LOOP--------

  int maxIter = 30;

  for (int order = 3; order <=3; order++)
{
  for (int power=5; power<=5; power++)
  {

    Real targetCost = 500.0*pow(2.0,power);

    // to make sure folders have a consistent number of zero digits
    const int string_pad = 5;
    std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
    std::string filename_base = "tmp/NS_BS_Re400/DG_" + int_pad + "_P" + std::to_string(order) + "/";

    boost::filesystem::create_directories(filename_base);

    std::string adapthist_filename = filename_base + "test.adapthist";
    fstream fadapthist;
    if (world.rank() == 0)
    {
      fadapthist.open( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);
    }

    PyDict MOESSDict;
    MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
    MOESSDict[MOESSParams::params.Optimizer_MaxEval] = 6000;
    MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
    MOESSDict[MOESSParams::params.UniformRefinement] = false;

    PyDict MesherDict;
    MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;
  //  MesherDict[EpicParams::params.minH] = 1e-11; //Minimum Mesh Size
  //  MesherDict[EpicParams::params.MaxAnisotropy] = 1e4;
  //  MesherDict[EpicParams::params.CSF] = "grids/joukowski_initial_mesh/jouk.csf"; //Geometry file
  //  MesherDict[EpicParams::params.Geometry] = "Airfoil Farfield"; //Geometry list
  //  MesherDict[EpicParams::params.minGeom] = 1e-6;
  //  MesherDict[EpicParams::params.maxGeom] = 0.5;
  //  MesherDict[EpicParams::params.nPointGeom] = -1;
  //  MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;

    PyDict AdaptDict;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
    AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

    MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

    MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    //Solution data
    typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

    std::shared_ptr<SolutionClass> pGlobalSol;
    pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                 BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                 active_boundaries, disc);

    const int quadOrder = 3*(order+1); //2*(order + 1);

    //Create solver interface
    std::shared_ptr<SolverInterfaceClass> pInterface;
    pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                        cellGroups, interiorTraceGroups,
                                                        PyBCList, BCBoundaryGroups,
                                                        SolverContinuationDict0, LinearSolverDict,
                                                        outputIntegrand);

    //Set initial solution
    pGlobalSol->setSolution(q0);

    std::string qfld_init_filename = filename_base + "qfld_init_a0_rank" + std::to_string(world.rank()) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();

    std::string qfld_filename = filename_base + "qfld_a0_rank" + std::to_string(world.rank()) + ".plt";
    output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

    std::string adjfld_filename = filename_base + "adjfld_a0_rank" + std::to_string(world.rank()) + ".plt";
    output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

    //Compute error estimates
    pInterface->computeErrorEstimates();

    std::string rfld_filename = filename_base + "rfld_a"  + std::to_string(0) + "_rank" + std::to_string(world.rank()) + ".plt";
    std::string sfld_filename = filename_base + "sfld_a" + std::to_string(0) + "_rank" + std::to_string(world.rank()) + ".plt";
    std::string efld_filename = filename_base + "efld_a" + std::to_string(0) + "_rank" + std::to_string(world.rank()) + ".plt";

    pInterface->output_EField(efld_filename);
    output_Tecplot_LO( pGlobalSol->primal.rfld, rfld_filename );
    output_Tecplot_LO( pGlobalSol->adjoint.rfld, sfld_filename );

    for (int iter = 0; iter < maxIter+1; iter++)
    {
      std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;


      //Perform local sampling and adapt mesh
      std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;
      pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);

      interiorTraceGroups.clear();
      for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      std::shared_ptr<SolutionClass> pGlobalSolNew;
      pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                      BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                      active_boundaries, disc);

      //Perform L2 projection from solution on previous mesh
      pGlobalSolNew->setSolution(*pGlobalSol);

      std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
      pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                             cellGroups, interiorTraceGroups,
                                                             PyBCList, BCBoundaryGroups,
                                                             SolverContinuationDict0, LinearSolverDict,
                                                             outputIntegrand);

      //Update pointers to the newest problem (this deletes the previous mesh and solutions)
      pxfld = pxfldNew;
      pGlobalSol = pGlobalSolNew;
      pInterface = pInterfaceNew;

      std::string qfld_init_filename = filename_base + "qfld_init_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_init_filename );

      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
      output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

      std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
      output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

      //Compute error estimates
      pInterface->computeErrorEstimates();

      std::string rfld_filename = filename_base + "rfld_a"  + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
      std::string sfld_filename = filename_base + "sfld_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";
      std::string efld_filename = filename_base + "efld_a" + std::to_string(iter+1) + "_rank" + std::to_string(world.rank()) + ".plt";

      pInterface->output_EField(efld_filename);
      output_Tecplot_LO( pGlobalSol->primal.rfld, rfld_filename );
      output_Tecplot_LO( pGlobalSol->adjoint.rfld, sfld_filename );

    }

    if (world.rank() == 0)
      fadapthist.close();
  }
}
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
