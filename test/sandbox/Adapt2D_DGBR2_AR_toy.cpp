// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_DG_AD_DoubleBoundaryLayer_btest
// Testing of the MOESS framework on the advection-diffusion pde

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_Solution.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"
#include "Meshing/EPIC/XField_PX.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "Field/output_grm.h"

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_DGBR2_AR_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_AR_Triangle )
{
  // typedef ScalarFunction2D_ASExp SolutionExact;
  // typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef OutputCell_Solution<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  mpi::communicator world;
  // Grid
  int ii = 6;
  int jj = ii;

  Real a = 2.0;
  Real b = 1.0;
  Real nu = 0.00;

  // PDE
  AdvectiveFlux2D_Uniform adv( a, b );

  ViscousFlux2D_Uniform visc( nu, 0., 0., nu );

  Real alpha = 5.0;
  Source2D_UniformGrad source(alpha, 0.0, 0.0);

  // // Create a solution dictionary
  // PyDict solnArgs;
  // solnArgs[SolutionExact::ParamsType::params.a] = a;
  // solnArgs[SolutionExact::ParamsType::params.b] = b;
  // solnArgs[SolutionExact::ParamsType::params.alpha] = alpha;
  //
  // NDSolutionExact solnExact( solnArgs );
  //
  // typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  // std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source );

  // BC
  PyDict BCSoln_Bottom, BCSoln_Right, BCSoln_Top, BCSoln_Left;

  BCSoln_Bottom[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCTypeDirichlet_mitStateParam>::params.qB] = 1;

  BCSoln_Left[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCTypeDirichlet_mitStateParam>::params.qB] = 1;

  BCSoln_Top[BCParams::params.BC.BCType] = BCParams::params.BC.None;
  BCSoln_Right[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BC_Bottom"] = BCSoln_Bottom;
  PyBCList["BC_Right"] = BCSoln_Right;
  PyBCList["BC_Top"] = BCSoln_Top;
  PyBCList["BC_Left"] = BCSoln_Left;


  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BC_Bottom"] = {0};
  BCBoundaryGroups["BC_Right"] = {1};
  BCBoundaryGroups["BC_Top"] = {2};
  BCBoundaryGroups["BC_Left"] = {3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-10, 1e-10};

  //Output functional
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

// #if defined(SANS_PETSC)
//   std::cout << "Linear solver: PETSc" << std::endl;
//
//   PyDict PreconditionerDict;
//   PyDict PreconditionerILU;
//   PyDict PETScDict;
//
//   PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
//   PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//
//   PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
//   PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
//
//   PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//   PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
//   PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-9;
//   PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
//   PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
//   PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
//   PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
//   PETScDict[SLA::PETScSolverParam::params.Timing] = true;
//   PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
//   PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
//   //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;
//
//   PyDict PETScDictAdjoint(PETScDict);
//   PETScDictAdjoint[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-10;
//
//   LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDictAdjoint;
//   NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
//
// #elif defined(INTEL_MKL)
//   std::cout << "Linear solver: MKL_PARDISO" << std::endl;
//   PyDict MKL_PARDISODict;
//   MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
//   LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
//   NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
// #else
  std::cout << "Linear solver: UMFPACK" << std::endl;
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
// #endif

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::vector<std::string> meshers = {"Epic","avro"};
  int orderL = 1, orderH = 3;
  int powerL = 0, powerH = 4;

#if 1
  int argc = boost::unit_test::framework::master_test_suite().argc;
  char **argv = boost::unit_test::framework::master_test_suite().argv;

  if (argc == 4)
  {
    orderL = orderH = std::stoi(argv[1]);
    powerL = powerH = std::stoi(argv[2]);
    meshers = {std::string(argv[3])};
  }
  if (argc == 3)
  {
    orderL = orderH = std::stoi(argv[1]);
    meshers = {std::string(argv[3])};
  }
  if (argc == 2)
  {
    meshers = {std::string(argv[1])};
  }

  std::cout << "Number of mesher = " << meshers.size() << std::endl;
  std::cout << "orderL = " << orderL << ", orderH = " << orderH << std::endl;
  std::cout << "powerL = " << powerL << ", powerH = " << powerH << std::endl;
#endif


  //--------ADAPTATION LOOP--------
  for (const std::string& mesher : meshers)
  {
    std::cout<< "Mesher = " << mesher << std::endl;

    std::shared_ptr<XField<PhysD2, TopoD2>> pxfld;

    const int maxIter = 30;

    for (int order = orderL; order <= orderH; order++)
    {
      timer totalTime;

      for (int power = powerH; power >= powerL; power--)
      {
        int nk = pow(2,power);
        int targetCost = 500*nk;

        // to make sure folders have a consistent number of zero digits
        const int string_pad = 6;
        std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
        std::string filename_base = "tmp/AR/DG_" + mesher + "_" + int_pad + "_P" + std::to_string(order) + "/";

        boost::filesystem::create_directories(filename_base);

        std::string adapthist_filename = filename_base + "test.adapthist";
        fstream fadapthist( adapthist_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

        PyDict MOESSDict;
        MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
        MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
        MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Element;
        MOESSDict[MOESSParams::params.UniformRefinement] = false;
        MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;
        MOESSDict[MOESSParams::params.MetricOptimization] = MOESSParams::params.MetricOptimization.SANS;

  #ifdef SANS_AVRO
        std::shared_ptr<avro::Context> context;
  #endif
        PyDict MesherDict;
        if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic)
        {
          MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;

          MesherDict[EpicParams::params.nThread] = 1; // DO NOT USE THIS ON HYPERSONIC

          pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
        }
  #ifdef SANS_AVRO
        else if (mesher == MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro)
        {
          MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.avro;

          MesherDict[avroParams::params.WriteMesh] = false; // write all the .mesh files
          MesherDict[avroParams::params.WriteConformity] = false; // write all the .json files
          // create the context
          context = std::make_shared<avro::Context>();

          using avro::coord_t;
          using avro::index_t;

          coord_t number = 2;

          std::vector<Real> lens(number,1.);
          std::vector<index_t> dims(number,ii);

          Real xc[3] = {.5,.5,0.};
          std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSSquare>( context.get() , xc , lens[0] , lens[1] );
          std::shared_ptr<avro::Model> model = std::make_shared<avro::Model> ( context.get(), "model" );
          model->addBody(pbody,true);

          XField2D_Box_Triangle_Lagrange_X1 xfld0( world, ii , jj );
          // copy the mesh into the domain and attach the geometry
          pxfld = std::make_shared< XField_avro<PhysD2,TopoD2> >(xfld0, model);
        }
  #endif
        else if (mesher == "fefloa")
        {
          MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

          pxfld = std::make_shared<XField2D_Box_Triangle_Lagrange_X1>( world, ii, jj );
        }
        else
          SANS_DEVELOPER_EXCEPTION("Unknown mesher");

        PyDict AdaptDict;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.hasTrueOutput] = true;
  //      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TrueOutput] = (13- 3./exp(5))/50; // J(u) = u^2
        AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TrueOutput] = (11- 1./exp(5./2))/25; // J(u) = u

        MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

        MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

        std::vector<int> cellGroups = {0};
        std::vector<int> interiorTraceGroups;
        for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        //Solution data
        std::shared_ptr<SolutionClass> pGlobalSol;
        pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, order, order+1,
                                                     BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                     active_boundaries, disc);

        const int quadOrder = 2*(order + 1);

        //Create solver interface
        std::shared_ptr<SolverInterfaceClass> pInterface;
        pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                            cellGroups, interiorTraceGroups,
                                                            PyBCList, BCBoundaryGroups,
                                                            SolverContinuationDict, LinearSolverDict,
                                                            outputIntegrand);

        pGlobalSol->setSolution(0.0);

        pInterface->solveGlobalPrimalProblem();
        pInterface->solveGlobalAdjointProblem();

        std::string qfld_filename = filename_base + "qfld_a0.plt";
        output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

        std::string adjfld_filename = filename_base + "adjfld_a0.plt";
        output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

  #ifdef XFIELD_OUT
        std::string xfld_filename = filename_base + "xfld_a0.grm";
        output_grm( *pxfld, xfld_filename);
  #endif

        for (int iter = 0; iter < maxIter+1; iter++)
        {
          std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

          //Compute error estimates
          pInterface->computeErrorEstimates();

          //Perform local sampling and adapt mesh
          std::shared_ptr<XField<PhysD2, TopoD2>> pxfldNew;

  #ifdef XFIELD_IN
          pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter); // so that adapthist fills

          xfld_filename = "tmp/AD/CG_P" + std::to_string(order) + "/xfld_a" + std::to_string(iter+1) + ".grm";
          pxfldNew = std::make_shared<XField_PX<PhysD2,TopoD2>>(world,xfld_filename);
  #else
          pxfldNew = mesh_adapter.adapt(*pxfld, cellGroups, *pInterface, iter);
  #endif

          interiorTraceGroups.clear();
          for ( int i = 0; i < pxfldNew->nInteriorTraceGroups(); i++)
            interiorTraceGroups.push_back(i);

          std::shared_ptr<SolutionClass> pGlobalSolNew;
          pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, order, order+1,
                                                          BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                          active_boundaries, disc);

          //Perform L2 projection from solution on previous mesh
          pGlobalSolNew->setSolution(*pGlobalSol);

          std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
          pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                                 cellGroups, interiorTraceGroups,
                                                                 PyBCList, BCBoundaryGroups,
                                                                 SolverContinuationDict, LinearSolverDict,
                                                                 outputIntegrand);

          //Update pointers to the newest problem (this deletes the previous mesh and solutions)
          pxfld = pxfldNew;
          pGlobalSol = pGlobalSolNew;
          pInterface = pInterfaceNew;

          pInterface->solveGlobalPrimalProblem();
          pInterface->solveGlobalAdjointProblem();

          std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter+1) + ".plt";
          output_Tecplot( pGlobalSol->primal.qfld, qfld_filename );

          std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter+1) + ".plt";
          output_Tecplot( pGlobalSol->adjoint.qfld, adjfld_filename );

  #ifdef XFIELD_OUT
          std::string xfld_filename = filename_base + "xfld_a" + std::to_string(iter+1) + ".grm";
          output_grm( *pxfld, xfld_filename);
  #endif

        }

        fadapthist.close();
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
