// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_L2_BoundaryLayer_btest
// Testing of the MOESS framework on the 2D boundary layer test-case found in Masa's thesis

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include <iostream>

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"

#include "Meshing/libMeshb/XField_libMeshb.h"
#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "Meshing/AnalyticMetrics/MetricField.h"

using namespace std;

//#define TEST_MODE

#ifdef SANS_AVRO
#include "Meshing/avro/XField_avro.h"
#endif

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

#define CUBE 0
#define CUBE_CYL 1

#define UNIFORM 0
#define LINEAR 1
#define POLAR1 0
#define POLAR2 0
#define BLAST 0
#define BLAST_SPACETIME 0

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt3D_Metric_Error_test_suite )

class Metric3d
{
public:
  Metric3d() {}

  DLA::MatrixSymS<3, Real> operator()( const DLA::VectorS<3, Real>& x ) const
  {
    #if UNIFORM

    DLA::MatrixSymS<3, Real> M(0);

    Real ii= 15.;
    Real hx= 1./ii;
    Real hy= 1./ii;
    Real hz= 1./ii;

    M(0,0) = 1./(hx*hx);
    M(0,1) = 0.;
    M(0,2) = 0.;
    M(1,1) = 1./(hy*hy);
    M(1,2) = 0.;
    M(2,2) = 1./(hz*hz);

    return M;

    #elif LINEAR

    DLA::MatrixSymS<3, Real> M(0);

    Real hx = 1.0e-1;
    Real hy = 1.0e-1;
    Real h0 = 1e-3;
    Real hz = h0 +2.*(0.1 -h0)*fabs( x[2] -0.5 );

    M(0,0) = 1./(hx*hx);
    M(0,1) = 0.;
    M(0,2) = 0.;
    M(1,1) = 1./(hy*hy);
    M(1,2) = 0.;
    M(2,2) = 1./(hz*hz);

    return M;

    #elif POLAR1

    Real r = sqrt( x[0]*x[0] +x[1]*x[1] );
    Real t = atan2( x[1] , x[0] );

    Real hz = 0.1;
    Real ht = 0.1;
    Real h0 = 0.001;
    Real hr = h0 +2.0*(0.1 -h0)*fabs( r -0.5 );

    DLA::MatrixS<3,3,Real> Q(0);
    Q(0,0) = cos(t);
    Q(0,1) = -sin(t);
    Q(0,2) = 0.0;

    Q(1,0) = sin(t);
    Q(1,1) = cos(t);
    Q(1,2) = 0.0;

    Q(2,0) = 0.0;
    Q(2,1) = 0.0;
    Q(2,2) = 1.0;

    DLA::VectorS<3,Real> L(0);
    L(0) = 1./(hr*hr);
    L(1) = 1./(ht*ht);
    L(2) = 1./(hz*hz);

    DLA::MatrixSymS<3,Real> M = Q*diag(L)*Transpose(Q);
    return M;

    #elif POLAR2

    Real r = sqrt( x[0]*x[0] +x[1]*x[1] );
    Real t = atan2( x[1] , x[0] );

    Real d = 10.0*( 0.6 -r );

    Real hz = 0.1;
    Real ht;
    Real h0 = 0.001;
    Real hr = h0 +2.0*(0.1 -h0)*fabs( r -0.5 );
    if (d<0.) ht = 0.1;
    else ht = d/40. +0.1*(1. -d);

    DLA::MatrixS<3,3,Real> Q(0);
    Q(0,0) = cos(t);
    Q(0,1) = -sin(t);
    Q(0,2) = 0.0;

    Q(1,0) = sin(t);
    Q(1,1) = cos(t);
    Q(1,2) = 0.0;

    Q(2,0) = 0.0;
    Q(2,1) = 0.0;
    Q(2,2) = 1.0;

    DLA::VectorS<3,Real> L(0);
    L(0) = 1./(hr*hr);
    L(1) = 1./(ht*ht);
    L(2) = 1./(hz*hz);

    DLA::MatrixSymS<3,Real> M = Q*diag(L)*Transpose(Q);

    return M;

    #elif BLAST

    Real X = x[0];
    Real Y = x[1];
    Real Z = x[2];

    Real RHO = sqrt(X*X+Y*Y+Z*Z) +0.0001;
    Real THETA = acos(Z/RHO);
    Real PHI = atan2(Y,X);

    DLA::MatrixS<3,3,Real> Q(0);
    Q(0,0) = (cos(PHI)*sin(THETA))/((pow(cos(PHI),2.0)+pow(sin(PHI),2.0))*(pow(cos(THETA),2.0)+pow(sin(THETA),2.0)));
    Q(0,1) = (cos(PHI)*cos(THETA))/((pow(cos(PHI),2.0)+pow(sin(PHI),2.0))*(pow(cos(THETA),2.0)+pow(sin(THETA),2.0)));
    Q(0,2) = -sin(PHI)/(pow(cos(PHI),2.0)+pow(sin(PHI),2.0));
    Q(1,0) = (sin(PHI)*sin(THETA))/((pow(cos(PHI),2.0)+pow(sin(PHI),2.0))*(pow(cos(THETA),2.0)+pow(sin(THETA),2.0)));
    Q(1,1) = (cos(THETA)*sin(PHI))/((pow(cos(PHI),2.0)+pow(sin(PHI),2.0))*(pow(cos(THETA),2.0)+pow(sin(THETA),2.0)));
    Q(1,2) = cos(PHI)/(pow(cos(PHI),2.0)+pow(sin(PHI),2.0));
    Q(2,0) = cos(THETA)/(pow(cos(THETA),2.0)+pow(sin(THETA),2.0));
    Q(2,1) = -sin(THETA)/(pow(cos(THETA),2.0)+pow(sin(THETA),2.0));

    Real rho0 = 0.5;
    Real h0 = 0.0025;
    Real hu = 0.125;
    Real hmin = 0.05;
    Real delta = 0.1;

    DLA::VectorS<3,Real> L(0);
    Real hrho = h0 +2*(hu-h0)*abs(RHO-rho0);
    Real htheta = hu;
    Real hphi = hu;

    if (fabs(RHO-rho0)>delta)
      htheta = hu;
    else htheta = (hu -hmin)*fabs(RHO-rho0)/delta +hmin;
    hphi = htheta;

    L(0) = 1./(hrho*hrho);
    L(1) = 1./(htheta*htheta);
    L(2) = 1./(hphi*hphi);

    DLA::MatrixSymS<3,Real> M = Q*diag(L)*Transpose(Q);

    return M;

    #elif BLAST_SPACETIME
    Real r = sqrt( x[0]*x[0] +x[1]*x[1] ) +0.0001; // meh
    Real t = atan2( x[1] , x[0] );

    // initial and final blast radii
    Real r0 = 0.4;
    Real rf = 0.8;

    // cone angle
    Real alpha = atan2(1.0,(rf-r0));

    // eigenvectors are normal and tangents to the cone
    DLA::MatrixS<3,3,Real> Q(0);
    Q(0,0) =  sin(alpha)*cos(t);
    Q(0,1) = -sin(t);
    Q(0,2) =  cos(alpha)*cos(t);

    Q(1,0) =  sin(alpha)*sin(t);
    Q(1,1) =  cos(t);
    Q(1,2) =  cos(alpha)*sin(t);

    Q(2,0) = -cos(alpha);
    Q(2,1) =  0.0;
    Q(2,2) =  sin(alpha);

    Real hz = 0.5; // controls anisotropy in temporal direction
    Real hu = 0.1; // uniform spacing
    Real h0 = 0.001;
    Real hmin = 0.01; // minimum thickness in gradation layer
    Real delta = 0.1; // thickness of gradation layer

    Real T = x[2];
    Real rho0 = r0 +(rf-r0)*T; // blast speed is derivative wrt T

    // spacing in radial direction
    Real hr = h0 +2*(hu-h0)*abs(r-rho0);

    // spacing in tangential direction, nicely graded
    Real ht = hu;
    if (fabs(r-rho0)<=delta)
      ht = (hu -hmin)*fabs(r-rho0)/delta +hmin;

    // set the eigenvalues
    DLA::VectorS<3,Real> L(0);
    L(0) = 1./(hr*hr);
    L(1) = 1./(ht*ht);
    L(2) = 1./(hz*hz);

    DLA::MatrixSymS<3,Real> M = Q*diag(L)*Transpose(Q);

    return M;

    #endif
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt3D_Metric_Error )
{

#ifdef SANS_AVRO

  mpi::communicator world;

  int maxIter = 20;

  using avro::coord_t;
  using avro::index_t;


  std::vector<int> cellGroups = {0};

  avro::Context context;

#if CUBE
  coord_t number = 3;
  Real x0[3] = {0.,0.,0.};
  std::vector<Real> lens(number,1.);
  std::shared_ptr<avro::Body> pbody = std::make_shared<avro::library::EGADSBox>( &context , x0 , lens.data() );
  avro::Body& body = *pbody;
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(&context,"cube");
  model->addBody(pbody,true);

#elif CUBE_CYL
  // load the egads model into an avro model
  std::string base = "/home/philip/Dropbox (MIT)/acdl/data/ugawg/adapt-benchmarks/cube-cylinder/";
  std::shared_ptr<avro::Model> model = std::make_shared<avro::Model>(&context,"cube-cylinder");
  model->load( base+"cube-cylinder.egads" );
#endif

  // initialize the mesh
  #if CUBE
  //XField3D_Box_Tet_X1 xfld0( world, 5 , 5 , 5);
  XField_KuhnFreudenthal<PhysD3,TopoD3> xfld0(world,{10,10,10});
  #elif CUBE_CYL
  XField_libMeshb<PhysD3,TopoD3> xfld0( world , base+"cube-cylinder.mesh" );
  #endif

  // copy the mesh into the domain and attach the geometry
  std::shared_ptr< XField<PhysD3,TopoD3> > pxfld = std::make_shared< XField_avro<PhysD3,TopoD3> >(xfld0,model);

  printf("created initial mesh\n");

  std::string filename_base = "tmp/cube";
  #if CUBE_CYL
  filename_base += "-cyl";
  #endif
  #if LINEAR
  filename_base += "/linear/";
  #elif POLAR1
  filename_base += "/polar1/";
  #elif POLAR2
  filename_base += "/polar2/";
  #elif BLAST
  filename_base += "/blast/";
  #elif BLAST_SPACETIME
  filename_base += "/blast_spacetime/";
  #endif
  boost::filesystem::create_directories(filename_base);

  boost::filesystem::path base_dir(filename_base);
  if ( not boost::filesystem::exists(base_dir) )
    boost::filesystem::create_directory(base_dir);

  std::string adapthist_filename = filename_base + "test.adapthist";
  fstream fadapthist( adapthist_filename, fstream::out );
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  //MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
  MOESSDict[MOESSParams::params.UniformRefinement] = false;

  PyDict MesherDict;
#if USE_FEFLOA
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.FeFloa;
#else
  MesherDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.Name] = MeshAdapterParams<PhysD3, TopoD3>::params.Mesher.avro;
  MesherDict[avroParams::params.InsertionVolumeFactor] = -1;
  #if CUBE
  MesherDict[avroParams::params.Curved] = false;
  #endif
  MesherDict[avroParams::params.FilenameBase] = filename_base;
#endif

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.TargetCost] = 0; // not used
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD3, TopoD3>::params.FilenameBase] = filename_base;


  //--------ADAPTATION LOOP--------
  MeshAdapterParams<PhysD3, TopoD3>::checkInputs(AdaptDict);

  MeshAdapter<PhysD3, TopoD3> mesh_adapter(AdaptDict, fadapthist);

  for (int iter = 0; iter < maxIter+1; iter++)
  {
    if (world.rank()==0)
    std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

    //if (pxfld->comm()->rank()==0)
    //  WriteMesh_libMeshb( *pxfld , filename_base+"/mesh_"+std::to_string(iter)+".mesh" );

    // evaluate the target metric
    Metric3d metric;
    MetricField<PhysD3,TopoD3> targetMetric( *pxfld , cellGroups , metric );

    // adapt mesh to metric
    std::shared_ptr<XField<PhysD3, TopoD3>> pxfldNew;
    pxfldNew = mesh_adapter.adapt_metric(*pxfld,cellGroups, targetMetric, iter);

    // update mesh
    pxfld = pxfldNew;
  }

  fadapthist.close();

#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
