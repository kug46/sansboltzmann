// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_DGBR2_HOW5_VI2_toy
// Trying to build the HOW5 VI2 case


#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <boost/mpl/vector_c.hpp>

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/JacobianParam.h"
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "NonLinearSolver/NewtonSolver.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_Block2x2_PTC.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Field/HField/GenHFieldArea_CG.h"
#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "Field/Function/FunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Meshing/EPIC/XField_PX.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "tools/output_std_vector.h"
#include "tools/linspace.h"

using namespace std;
using namespace SANS;

namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_HOW5_VI2_toy )

////////////////////////////////////////////////////////////////////////////////////////
// Notes:
//  -Built of RAE2282 case
// Default PETSc abstol is 1e-50, 1e-10 is much too big
////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_HOW5_VI2_toy )
{
  ////////////////////////////////////////////////////////////////////////////////////////
  // MPI comm world
  ////////////////////////////////////////////////////////////////////////////////////////
  mpi::communicator world;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for Euler2D sans AV
  ////////////////////////////////////////////////////////////////////////////////////////
//  typedef QTypeEntropy QType;
//  typedef QTypeConservative QType;
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypePrimitiveSurrogate QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for discretization sans AV
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;
  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PDEPrimalEquationSetClass;

  typedef PDEPrimalEquationSetClass::BCParams BCParams;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for Euler2D mit AV
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef PDENDConvertSpace<PhysD2, AVPDEClass> AVNDPDEClass;
  typedef AVNDPDEClass::ArrayQ<Real> AVArrayQ;
  typedef AVNDPDEClass::template VectorArrayQ<Real> AVVectorArrayQ;


  typedef OutputEuler2D_EntropyErrorSquare<AVNDPDEClass> OutputEntropyErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquaredClass> NDOutputEntropyErrorSquaredClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputEntropyErrorSquaredClass> IntegrandSquareErrorClass;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Typedefs for discretization mit AV
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;

  typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                         XField<PhysD2, TopoD2> >::type ParamField;

  typedef BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVBCVector;
  typedef AlgebraicEquationSet_DGBR2< AVNDPDEClass, BCNDConvertSpace, AVBCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, ParamField > AVPDEPrimalEquationSetClass;
  typedef AlgebraicEquationSet_PTC<AVNDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamField > AVAlgebraicEquationSet_PTCClass;

  typedef AVPDEPrimalEquationSetClass::BCParams AVBCParams;
  typedef AVPDEPrimalEquationSetClass::SystemMatrix AVSystemMatrixClass;
  typedef AVPDEPrimalEquationSetClass::SystemVector AVSystemVectorClass;
  ////////////////////////////////////////////////////////////////////////////////////////



  ////////////////////////////////////////////////////////////////////////////////////////
  // Create tecplot file to put total enthalpy errors
  ////////////////////////////////////////////////////////////////////////////////////////
  std::ofstream resultFile("tmp/HOW5_VI2/Entropy.plt", std::ios::out);
  resultFile << "VARIABLES=";
  resultFile << "\"DOF\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << std::endl;
  resultFile << std::setprecision(16) << std::scientific;
  ////////////////////////////////////////////////////////////////////////////////////////

  for (int order_pde = 1; order_pde <= 4; order_pde++)
  {

    ////////////////////////////////////////////////////////////////////////////////////////
    // Tecplot zone for this order
    ////////////////////////////////////////////////////////////////////////////////////////
    resultFile << "ZONE T=\"DG P=" << order_pde << "\"" << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////////

    for (int grid_index = 0; grid_index <= 5; grid_index++)
    {

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our Euler PDE sans AV
      ////////////////////////////////////////////////////////////////////////////////////////
      // Gas Model
      Real gamma, R;
      gamma = 1.4;
      R = 1;
      PyDict gasModelDict;
      gasModelDict[GasModelParams::params.gamma] = gamma;
      gasModelDict[GasModelParams::params.R] = R;
      GasModel gas(gasModelDict);
      // PDE
      EulerResidualInterpCategory interp = Euler_ResidInterp_Momentum;
      //NDPDEClass pde(gas, interp, eHarten);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our Euler PDE mit AV
      ////////////////////////////////////////////////////////////////////////////////////////
      int order_pde0 = 0;
      bool hasSpaceTimeDiffusion = false;
      PDEBaseClass pdeEulerAV0(order_pde0, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
      // Sensor equation terms
      Sensor sensor0(pdeEulerAV0);
      SensorAdvectiveFlux sensor_adv0(0.0, 0.0);
      SensorViscousFlux sensor_visc0(order_pde0);
      SensorSource sensor_source0(order_pde0, sensor0);
      // AV PDE with sensor equation
      bool isSteady = true;
      AVNDPDEClass avpde0(sensor_adv0, sensor_visc0, sensor_source0, isSteady, order_pde0,
                          hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our Euler PDE mit AV
      ////////////////////////////////////////////////////////////////////////////////////////
      PDEBaseClass pdeEulerAV(order_pde, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
      // Sensor equation terms
      Sensor sensor(pdeEulerAV);
      SensorAdvectiveFlux sensor_adv(0.0, 0.0);
      SensorViscousFlux sensor_visc(order_pde);
      SensorSource sensor_source(order_pde, sensor);
      // AV PDE with sensor equation
      AVNDPDEClass avpde(sensor_adv, sensor_visc, sensor_source, isSteady, order_pde,
                         hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Define boundary conditions - P0
      ////////////////////////////////////////////////////////////////////////////////////////
      // reference state (freestream)
      const Real Mach = 0.5;
      const Real rhoRef = 1;                            // density scale
      const Real aoaRef = 0.0 * PI / 180.0;                          // angle of attack (radians)

      const Real pRef = 1.0;
      const Real tRef = pRef/rhoRef/R;
      const Real cRef = sqrt(gamma*R*tRef);
      const Real qRef = Mach*cRef;

      const Real uRef = qRef * cos(aoaRef);             // velocity
      const Real vRef = qRef * sin(aoaRef);

      // Lets calculate our inflow enthalpy since this should be constant
      const Real inflowEntropy = log( pRef / pow(rhoRef,gamma) );
      const Real TtSpec = tRef * (1 + (gamma-1)/2 * Mach*Mach);
      const Real PtSpec = pRef * pow(TtSpec/ tRef, gamma/(gamma-1));

      std::cout << "rho:  " << rhoRef << "[kg/m3]" << std::endl
                << "u:    " << uRef   << "[m/s]"   << std::endl
                << "v:    " << vRef   << "[m/s]"   << std::endl
                << "T:    " << tRef   << "[K]"     << std::endl
                << "Tt:   " << TtSpec << "[K]"     << std::endl
                << "Pt:   " << PtSpec << "[K]"     << std::endl
                << "sRef: " << inflowEntropy << "[-]"     << std::endl;

      // Define inflow BC
      PyDict BCInflow;
      BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
      BCInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
      BCInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
      BCInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aoaRef;

      // Define wall BC
      PyDict BCWall;
      BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

      // Define outflow BC
      PyDict BCOutflow;
      BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
      BCOutflow[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;

      // Define BC list
      PyDict PyPDEBCList;
      PyPDEBCList["FarfieldOutflow"] = BCOutflow;
      PyPDEBCList["Wall"] = BCWall;
      PyPDEBCList["FarfieldInflow"] = BCInflow;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Define boundary conditions - Higher P
      ////////////////////////////////////////////////////////////////////////////////////////
      // Define inflow BC
      PyDict BCAVInflow;
      BCAVInflow[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
      BCAVInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
      BCAVInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
      BCAVInflow[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aoaRef;

      // Define wall BC
      PyDict BCAVWall;
      BCAVWall[AVBCParams::params.BC.BCType] = AVBCParams::params.BC.Symmetry_mitState;

      // Define outflow BC
      PyDict BCAVOutflow;
      BCAVOutflow[AVBCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
      BCAVOutflow[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;

      // Define BC list
      PyDict PyAVPDEBCList;
      PyAVPDEBCList["FarfieldOutflow"] = BCAVOutflow;
      PyAVPDEBCList["Wall"] = BCAVWall;
      PyAVPDEBCList["FarfieldInflow"] = BCAVInflow;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create grid
      ////////////////////////////////////////////////////////////////////////////////////////
      std::string meshName = "grids/HOW5_VI2/SmoothBump_tri_ref"
                            + std::to_string(grid_index)
                            + "_Q4.grm";
      XField_PX<PhysD2, TopoD2> xfld( world, meshName );
      GenHField_CG<PhysD2,TopoD2> hfld(xfld);
      std::cout << "nCellGroups: " << xfld.nCellGroups() << " nInteriorTraceGroups: " << xfld.nInteriorTraceGroups() << std::endl;
      std::vector<int> cellGroups, interiorTraceGroups;
      linspace( 0, xfld.nCellGroups()-1, cellGroups );
      linspace( 0, xfld.nInteriorTraceGroups()-1, interiorTraceGroups );
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Get boundary group information
      ////////////////////////////////////////////////////////////////////////////////////////
      std::map<std::string, std::vector<int>> PDEBCBoundaryGroups;
      PDEBCBoundaryGroups["FarfieldInflow"] = {0};
      PDEBCBoundaryGroups["FarfieldOutflow"] = {1};
      PDEBCBoundaryGroups["Wall"] = {2, 3};
      for (auto it = PDEBCBoundaryGroups.begin(); it != PDEBCBoundaryGroups.end(); ++it)
      {
        std::string sectionName = it->first;
        std::vector<int> group = it->second;
        std::cout << "PDEBCBoundaryGroups[\"" << sectionName << "\"] = { " << group << "}" << std::endl;
      }
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Check BCs
      ////////////////////////////////////////////////////////////////////////////////////////
      BCParams::checkInputs(PyPDEBCList);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Allocate our Euler PDE field - P0
      ////////////////////////////////////////////////////////////////////////////////////////
      Field_DG_Cell<PhysD2, TopoD2, AVArrayQ> pde_qfld0(xfld, order_pde0, BasisFunctionCategory_Legendre);
      std::shared_ptr<Field_DG_Cell<PhysD2, TopoD2, Real>> pde_sfld0(new Field_DG_Cell<PhysD2, TopoD2, Real>(xfld, 0,
                                                                                                             BasisFunctionCategory_Legendre) );
      FieldLift_DG_Cell<PhysD2, TopoD2, AVVectorArrayQ> pde_rfld0(xfld, order_pde0, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, AVArrayQ> pde_lgfld0( xfld, order_pde0, BasisFunctionCategory_Legendre,
                                                                BCParams::getLGBoundaryGroups(PyPDEBCList, PDEBCBoundaryGroups) );
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Allocate our Euler PDE field - higher P
      ////////////////////////////////////////////////////////////////////////////////////////
      Field_DG_Cell<PhysD2, TopoD2, AVArrayQ> pde_qfld(xfld, order_pde, BasisFunctionCategory_Legendre);
      std::shared_ptr<Field_DG_Cell<PhysD2, TopoD2, Real>> pde_sfld (new Field_DG_Cell<PhysD2, TopoD2, Real>(xfld, 0,
                                                                                                             BasisFunctionCategory_Legendre) );
      FieldLift_DG_Cell<PhysD2, TopoD2, AVVectorArrayQ> pde_rfld(xfld, order_pde, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, AVArrayQ> pde_lgfld( xfld, order_pde, BasisFunctionCategory_Legendre,
                                                                BCParams::getLGBoundaryGroups(PyAVPDEBCList, PDEBCBoundaryGroups) );
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Initial condition for what ever I am doing
      ////////////////////////////////////////////////////////////////////////////////////////
      AVVariable<DensityVelocityTemperature2D, Real> qdata = {{rhoRef, uRef, vRef, tRef}, 0.0};
      AVArrayQ q1 = avpde.setDOFFrom(qdata);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial conditions - P0
      ////////////////////////////////////////////////////////////////////////////////////////
      pde_qfld0 = q1;
      pde_lgfld0 = 0;
      pde_rfld0 = 0;
      *pde_sfld0 = 0;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // AES Settings
      ////////////////////////////////////////////////////////////////////////////////////////
      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {2e-8, 2e-8};
      Real viscousEtaParameter = 2*Triangle::NEdge;
      DiscretizationDGBR2 disc(0, viscousEtaParameter);
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Create our AES to solve our PDE sans AV
      ////////////////////////////////////////////////////////////////////////////////////////
      ParamField paramfld = (hfld, xfld);
      AVPDEPrimalEquationSetClass PrincipalEqSet0(paramfld, pde_qfld0, pde_rfld0, pde_lgfld0, pde_sfld0, avpde0, disc, quadratureOrder,
                                                  ResidualNorm_L2, tol, cellGroups, interiorTraceGroups, PyAVPDEBCList, PDEBCBoundaryGroups);
      AVPDEPrimalEquationSetClass PrincipalEqSet (paramfld, pde_qfld,  pde_rfld,  pde_lgfld,  pde_sfld,  avpde,  disc, quadratureOrder,
                                                  ResidualNorm_L2, tol, cellGroups, interiorTraceGroups, PyAVPDEBCList, PDEBCBoundaryGroups);

      AVAlgebraicEquationSet_PTCClass PrincipalEqSetPTC0(paramfld, pde_qfld0, avpde0, quadratureOrder, cellGroups, PrincipalEqSet0);
      AVAlgebraicEquationSet_PTCClass PrincipalEqSetPTC (paramfld, pde_qfld,  avpde,  quadratureOrder, cellGroups, PrincipalEqSet);
      ////////////////////////////////////////////////////////////////////////////////////////

#define P_SEQUENCE 1
#if P_SEQUENCE
      ////////////////////////////////////////////////////////////////////////////////////////
      // Construct Newton Solver - P0
      ////////////////////////////////////////////////////////////////////////////////////////
      PyDict NewtonSolverDict0, LineUpdateDict0;
#if defined(SANS_PETSC)
      std::cout << "Linear solver: PETSc" << std::endl;

      PyDict PreconditionerDict0;
      PyDict PreconditionerILU0;
      PyDict PETScDict0;

      PreconditionerILU0[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
      PreconditionerILU0[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
      PreconditionerILU0[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
      PreconditionerILU0[SLA::PreconditionerILUParam::params.FillLevel] = 4;

      PreconditionerDict0[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
      PreconditionerDict0[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU0;

      PETScDict0[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
      PETScDict0[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
      PETScDict0[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0e-10;
      PETScDict0[SLA::PETScSolverParam::params.MaxIterations] = 2000;
      PETScDict0[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
      PETScDict0[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict0;
      PETScDict0[SLA::PETScSolverParam::params.Verbose] = true;
      PETScDict0[SLA::PETScSolverParam::params.computeSingularValues] = false;
      PETScDict0[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
      //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

      NewtonSolverDict0[NewtonSolverParam::params.LinearSolver] = PETScDict0;

#elif defined(INTEL_MKL)
      std::cout << "Linear solver: MKL_PARDISO" << std::endl;
      PyDict MKL_PARDISODict0;
      MKL_PARDISODict0[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
      NewtonSolverDict0[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict0;
#else
      std::cout << "Linear solver: UMFPACK" << std::endl;
      PyDict UMFPACKDict0;
      UMFPACKDict0[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
      NewtonSolverDict0[NewtonSolverParam::params.LinearSolver] = UMFPACKDict0;
#endif

      LineUpdateDict0[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//      LineUpdateDict0[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;
      LineUpdateDict0[HalvingSearchLineUpdateParam::params.verbose] = true;
      LineUpdateDict0[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-4;

      NewtonSolverDict0[NewtonSolverParam::params.LineUpdate] = LineUpdateDict0;
      NewtonSolverDict0[NewtonSolverParam::params.MinIterations] = 0;
      NewtonSolverDict0[NewtonSolverParam::params.MaxIterations] = 1;
      NewtonSolverDict0[NewtonSolverParam::params.Verbose] = true;

      NewtonSolverParam::checkInputs(NewtonSolverDict0);
      NewtonSolver<AVSystemMatrixClass> Solver0( PrincipalEqSet0, NewtonSolverDict0 );
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // SOLVE - P0
      ////////////////////////////////////////////////////////////////////////////////////////
#define PSEQ_NEWTON_SOLVE 0
#if PSEQ_NEWTON_SOLVE
      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial guess from initial condition - P0
      ////////////////////////////////////////////////////////////////////////////////////////
      AVSystemVectorClass ini0(PrincipalEqSetPTC0.vectorStateSize());
      AVSystemVectorClass sln0(PrincipalEqSetPTC0.vectorStateSize());

      PrincipalEqSet0.fillSystemVector(ini0);
      sln0 = ini0;
      ////////////////////////////////////////////////////////////////////////////////////////

      SolveStatus status = Solver0.solve(ini0, sln0);
      BOOST_CHECK( status.converged );
#else
//      NewtonSolverDict0[NewtonSolverParam::params.MaxIterations] = 20;
      PyDict PseudoTimeDict0;
      PseudoTimeDict0[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict0;
      PseudoTimeDict0[PseudoTimeParam::params.invCFL] = 100;
      PseudoTimeDict0[PseudoTimeParam::params.invCFL_max] = 1000;
      PseudoTimeDict0[PseudoTimeParam::params.invCFL_min] = 0.0;

      //PseudoTimeDict0[PseudoTimeParam::params.CFLDecreaseFactor] = 0.5;
      //PseudoTimeDict0[PseudoTimeParam::params.CFLIncreaseFactor] = 10.0;
      PseudoTimeDict0[PseudoTimeParam::params.Verbose] = true;
      PseudoTimeDict0[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/HOW5_VI2/PTC_History_P"
                                                                     + std::to_string(order_pde)
                                                                     + "_G"
                                                                     + std::to_string(grid_index)
                                                                     + "_PSeq.dat";

      PseudoTime<AVSystemMatrixClass> PTC0(PseudoTimeDict0, PrincipalEqSetPTC0);
      PTC0.iterate(500);
#endif
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial conditions - Higher P
      ////////////////////////////////////////////////////////////////////////////////////////
      pde_qfld0.projectTo(pde_qfld);
      pde_sfld0->projectTo(*pde_sfld);
      pde_rfld = 0;
      pde_lgfld = 0;
      ////////////////////////////////////////////////////////////////////////////////////////

      string filename0;
      filename0 = "tmp/HOW5_VI2/HOW5_VI2_"
                + Type2String<QType>::str()
                + "_P"
                + std::to_string(order_pde)
                + "_G"
                + std::to_string(grid_index)
                + "_PSeq.plt";
      output_Tecplot( pde_qfld, filename0 );

      filename0 = "tmp/HOW5_VI2/HOW5_VI2_Sensor_"
                + Type2String<QType>::str()
                + "_P"
                + std::to_string(order_pde)
                + "_G"
                + std::to_string(grid_index)
                + "_PSeq.plt";
      output_Tecplot( *pde_sfld, filename0 );

      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial guess from initial condition - Higher P
      ////////////////////////////////////////////////////////////////////////////////////////
      AVSystemVectorClass ini(PrincipalEqSetPTC.vectorStateSize());
      AVSystemVectorClass sln(PrincipalEqSetPTC.vectorStateSize());

      PrincipalEqSetPTC.fillSystemVector(ini);
      PrincipalEqSetPTC.setSolutionField(ini); // compute lifting operators
      sln = ini;
      ////////////////////////////////////////////////////////////////////////////////////////
#else
      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial conditions
      ////////////////////////////////////////////////////////////////////////////////////////
      pde_qfld = q1;
      pde_rfld = 0;
      pde_lgfld = 0;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Set initial guess from initial condition - P0
      ////////////////////////////////////////////////////////////////////////////////////////
      BlockVectorClass ini(BlockAES.vectorStateSize());
      BlockVectorClass sln(BlockAES.vectorStateSize());

      BlockAES.fillSystemVector(ini);
      sln = ini;
      ////////////////////////////////////////////////////////////////////////////////////////
#endif

      ////////////////////////////////////////////////////////////////////////////////////////
      // Construct Newton Solver
      ////////////////////////////////////////////////////////////////////////////////////////
      PyDict NewtonSolverDict, LineUpdateDict;
#if defined(SANS_PETSC)
      std::cout << "Linear solver: PETSc" << std::endl;

      PyDict PreconditionerDict;
      PyDict PreconditionerILU;
      PyDict PETScDict;

      PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
      PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
      PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
      PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 4;

      PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
      PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

      PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
      PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-5;
      PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 0e-10;
      PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 2000;
      PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
      PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
      PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
      PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
      PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";
      //PETSCDict[SLA::PETScSolverParam::params.FilenameBase] = filename_base;

      NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;

#elif defined(INTEL_MKL)
      std::cout << "Linear solver: MKL_PARDISO" << std::endl;
      PyDict MKL_PARDISODict;
      MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
      NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
      std::cout << "Linear solver: UMFPACK" << std::endl;
      PyDict UMFPACKDict;
      UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
      NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

      LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//      LineUpdateDict[HalvingSearchLineUpdateParam::params.dumpLineSearchField] = true;
      LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
      LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-5;

      NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
      NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
      NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
      NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

      NewtonSolverParam::checkInputs(NewtonSolverDict);
      NewtonSolver<AVSystemMatrixClass> Solver( PrincipalEqSetPTC, NewtonSolverDict );
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Solve
      ////////////////////////////////////////////////////////////////////////////////////////
#define NEWTON_SOLVE 0
#if NEWTON_SOLVE
      SolveStatus status = Solver.solve(ini, sln);
      BOOST_CHECK( status.converged );
#else
      bool converged = false;
      NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
      PyDict PseudoTimeDict;
      PseudoTimeDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
      PseudoTimeDict[PseudoTimeParam::params.invCFL] = 100;
      PseudoTimeDict[PseudoTimeParam::params.invCFL_max] = 1000;
      PseudoTimeDict[PseudoTimeParam::params.invCFL_min] = 0.0;

      PseudoTimeDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.1;
      PseudoTimeDict[PseudoTimeParam::params.CFLIncreaseFactor] = 2.0;
      PseudoTimeDict[PseudoTimeParam::params.Verbose] = true;

      PseudoTimeDict[PseudoTimeParam::params.ResidualHistoryFile] = "tmp/HOW5_VI2/PTC_History_P"
                                                                    + std::to_string(order_pde)
                                                                    + "_G"
                                                                    + std::to_string(grid_index)
                                                                    + ".dat";

      PseudoTime<AVSystemMatrixClass> PTC(PseudoTimeDict, PrincipalEqSetPTC);
      converged = PTC.iterate(250);
#endif
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Calculate the volume of the domain
      ////////////////////////////////////////////////////////////////////////////////////////
      SolnNDConvertSpace<PhysD2, ScalarFunction2D_Const> constFcn(1);

      Real localDomainArea = 0;
      Real domainArea = 0;
      for_each_CellGroup<TopoD2>::apply( FunctionIntegral(constFcn, localDomainArea, world.rank(), cellGroups), xfld );
#ifdef SANS_MPI
      boost::mpi::reduce(world, localDomainArea, domainArea, std::plus<Real>(), 0);
#else
      domainArea = localDomainArea;
#endif
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Calculate Enthalpy error
      ////////////////////////////////////////////////////////////////////////////////////////
      NDOutputEntropyErrorSquaredClass outEnthalpyError( avpde, 1.0 );
      IntegrandSquareErrorClass fcnErr( outEnthalpyError, cellGroups );

      Real EntropySquareError = 0.0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_Galerkin( fcnErr, EntropySquareError ),
          xfld, pde_qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      std::cout << "RMS Entropy Error: " << sqrt(EntropySquareError/domainArea) << std::endl;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Output Error to tecplot file
      ////////////////////////////////////////////////////////////////////////////////////////
      resultFile << " " << pde_qfld.nDOF();
      resultFile << " " << 1.0/sqrt(pde_qfld.nDOF());
      resultFile << " " << sqrt(EntropySquareError/domainArea) / (cRef*cRef);
      resultFile << std::endl;
      ////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////////////////////
      // Output Tecplot file
      ////////////////////////////////////////////////////////////////////////////////////////
      string filename;
      filename = "tmp/HOW5_VI2/HOW5_VI2_"
                + Type2String<QType>::str()
                + "_P"
                + std::to_string(order_pde)
                + "_G"
                + std::to_string(grid_index)
                + ".plt";
      output_Tecplot( pde_qfld, filename );


      filename = "tmp/HOW5_VI2/HOW5_VI2_Sensor_"
               + Type2String<QType>::str()
                + "_P"
               + std::to_string(order_pde)
               + "_G"
               + std::to_string(grid_index)
               + ".plt";
      output_Tecplot( *pde_sfld, filename );
      ////////////////////////////////////////////////////////////////////////////////////////
      BOOST_CHECK(converged);

    }
  }
  resultFile.close();
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
