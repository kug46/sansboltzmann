// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_HDG_AD_ST_btest
// testing of 1-D+time HDG Spacetime Solve with Unsteady Advection-Diffusion

#undef SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/ForcingFunction1D.h"
#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/ForcingFunction_MMS.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_HDG.h"

#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_HDG_AD_ST_test_suite )

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( Solve_HDG_AD_ST )
{
  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpaceTime<PhysD1, PDEAdvectionDiffusion1D> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;
  typedef PDEClass::template MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  typedef ScalarFunction1D_SineSineUnsteady SolutionExact;
  typedef SolnNDConvertSpaceTime<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCTypeSoln;
  typedef BCNDConvertSpaceTime<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeSoln> > BCClassSoln;
  typedef BCNDConvertSpaceTime<PhysD1, BCAdvectionDiffusion<PhysD1,BCTypeNone> > BCClassNone;

  // PDE

  Real nu = 0.02;
  Real c = -1.1;

  // Temporal discretization
  AdvectiveFlux1D_Uniform adv( c );
  ViscousFlux1D_Uniform visc( nu );

  ScalarFunction1D_SineSineUnsteady MMS_soln;
  typedef ForcingFunction1D_MMS<PDEAdvectionDiffusion1D,ScalarFunction1D_SineSineUnsteady> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(MMS_soln));
  Source1D_None source;

  // BC

  NDSolutionExact solnExact;
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact );

  BCClassSoln bcSoln(solnExactPtr, visc);

  BCClassNone bcNone;

  PDEClass pde(adv, visc, source, forcingptr );

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, BCClassSoln, BCClassSoln::Category, HDG> IntegrandBoundClassSoln;
  typedef IntegrandBoundaryTrace<PDEClass, BCClassNone, BCClassNone::Category, HDG> IntegrandBoundClassNone;

  typedef IntegrandCell_SquareError<PDEClass,NDSolutionExact> IntegrandSquareErrorClass;


  // HDG discretization
  DiscretizationHDG<PDEClass> disc( pde, Global, Gradient );

  const std::vector<int> BoundaryGroupsNone = {2};
  const std::vector<int> BoundaryGroupsSoln = {0,1,3};

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnInt( pde, disc, {0} );
  IntegrandBoundClassNone fcnBCNone( pde, bcNone, BoundaryGroupsNone, disc );
  IntegrandBoundClassSoln fcnBCSoln( pde, bcSoln, BoundaryGroupsSoln, disc );
  IntegrandSquareErrorClass fcnErr( solnExact, {0} );

  // norm data
  //    Real hVec[10];
  //    Real hDOFVec[10];   // 1/sqrt(DOF)


  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_1D_HDG.plt", std::ios::out);
#else
  std::stringstream resultFile;
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  // set grid parameters and order for the run
  int ordermin = 1;
  int ordermax = 1;
  //   loop over temporal resolution
  int powermin = 1;
#ifdef SANS_FULLTEST
  int powermax = 2;
  cout << "...running full test" << endl;
#else
  int powermax = 2;
#endif


  for (int order = ordermin; order <= ordermax; order++)
  {

  Real normVec[10];   // Keep track of L2 error
  int indx = 0;

  for (int power = powermin; power <= powermax; power++)
  {

    //start clock
    std::clock_t start;
    double duration;

    start = std::clock();

    int ii = pow(2, power); //grid size
    int jj = pow(2, power);
    XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

    // HDG solution field
    // cell solution
    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
    // auxiliary variable
    Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
    // interface solution
    Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> uIfld(xfld, order, BasisFunctionCategory_Legendre);
    // Lagrange multiplier
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroupsSoln);


    const int nDOFPDE = qfld.nDOF();
    const int nDOFAu  = afld.nDOF()*PhysD2::D;
    const int nDOFInt = uIfld.nDOF();
    const int nDOFBC  = lgfld.nDOF();

    int nDOFtot = nDOFPDE + nDOFAu + nDOFInt + nDOFBC;

    // quadrature rule
    int quadratureOrder[4] = {-1, -1, -1, -1};    // max
    int quadratureOrderMin[4] = {0, 0, 0, 0};     // min

    // linear system setup

    typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
    typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
    typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
    typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;

    // residual

    SystemVectorClass rsd = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};
    rsd = 0;

    IntegrateCellGroups<TopoD2>::integrate( ResidualCell_HDG(fcnCell, rsd(0), rsd(1)),
                                            xfld, (qfld, afld), quadratureOrder, 1 );

    IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(0), rsd(1), rsd(2)),
                                                            xfld, (qfld, afld), uIfld, quadratureOrder, 3);

    IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBCNone, rsd(0), rsd(1)),
                                                        xfld, (qfld, afld), quadratureOrder, 4 );

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBCSoln, rsd(0), rsd(1), rsd(3)),
                                                        xfld, (qfld, afld), lgfld, quadratureOrder, 4 );

    // jacobian nonzero pattern

    typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

    // jacobian nonzero pattern
    //
    //        q   a  qI  lg
    //   PDE  X   X   X   X
    //   Au   X   X   X   X
    //   Int  X   X   X   0
    //   BC   X   X   0   0

    //fld.SystemMatrixSize();
    DLA::MatrixD<NonZeroPatternClass> nz =
        {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFAu}, {nDOFPDE, nDOFInt}, {nDOFPDE, nDOFBC} },
         { {nDOFAu , nDOFPDE}, {nDOFAu , nDOFAu}, {nDOFAu , nDOFInt}, {nDOFAu , nDOFBC} },
         { {nDOFInt, nDOFPDE}, {nDOFInt, nDOFAu}, {nDOFInt, nDOFInt}, {nDOFInt, nDOFBC} },
         { {nDOFBC , nDOFPDE}, {nDOFBC , nDOFAu}, {nDOFBC , nDOFInt}, {nDOFBC , nDOFBC} }};

    NonZeroPatternClass& nzPDE_q  = nz(0,0);
    NonZeroPatternClass& nzPDE_a  = nz(0,1);
    NonZeroPatternClass& nzPDE_qI = nz(0,2);
    NonZeroPatternClass& nzPDE_lg = nz(0,3);

    NonZeroPatternClass& nzAu_q  = nz(1,0);
    NonZeroPatternClass& nzAu_a  = nz(1,1);
    NonZeroPatternClass& nzAu_qI = nz(1,2);
    NonZeroPatternClass& nzAu_lg = nz(1,3);

    NonZeroPatternClass& nzInt_q  = nz(2,0);
    NonZeroPatternClass& nzInt_a  = nz(2,1);
    NonZeroPatternClass& nzInt_qI = nz(2,2);
    //NonZeroPatternClass& nzInt_lg = nz(2,3);

    NonZeroPatternClass& nzBC_q  = nz(3,0);
    NonZeroPatternClass& nzBC_a  = nz(3,1);
    //NonZeroPatternClass& nzBC_qI = nz(3,2);
    NonZeroPatternClass& nzBC_lg = nz(3,3);

    IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nzPDE_q, nzPDE_a,
                                                                                    nzAu_q, nzAu_a),
                                            xfld, (qfld, afld), quadratureOrderMin, 1 );

    IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, nzPDE_q, nzPDE_a, nzPDE_qI,
                                                        nzAu_q ,          nzAu_qI ,
                                                        nzInt_q, nzInt_a, nzInt_qI),
        xfld, (qfld, afld), uIfld, quadratureOrderMin, 3);

    IntegrateBoundaryTraceGroups<TopoD2>::integrate(
        JacobianBoundaryTrace_HDG<SurrealClass>(fcnBCNone, nzPDE_q, nzPDE_a,
                                                                nzAu_q , nzAu_a),
        xfld, (qfld, afld), quadratureOrderMin, 4);

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBCSoln, nzPDE_q, nzPDE_a, nzPDE_lg,
                                                              nzAu_q , nzAu_a , nzAu_lg ,
                                                              nzBC_q , nzBC_a , nzBC_lg ),
        xfld, (qfld, afld), lgfld, quadratureOrderMin, 4);

    // jacobian

    SystemMatrixClass jac(nz);

    SparseMatrixClass& jacPDE_q  = jac(0,0);
    SparseMatrixClass& jacPDE_a  = jac(0,1);
    SparseMatrixClass& jacPDE_qI = jac(0,2);
    SparseMatrixClass& jacPDE_lg = jac(0,3);

    SparseMatrixClass& jacAu_q  = jac(1,0);
    SparseMatrixClass& jacAu_a  = jac(1,1);
    SparseMatrixClass& jacAu_qI = jac(1,2);
    SparseMatrixClass& jacAu_lg = jac(1,3);

    SparseMatrixClass& jacInt_q  = jac(2,0);
    SparseMatrixClass& jacInt_a  = jac(2,1);
    SparseMatrixClass& jacInt_qI = jac(2,2);
    //SparseMatrixClass& jacInt_lg = jac(2,3);

    SparseMatrixClass& jacBC_q  = jac(3,0);
    SparseMatrixClass& jacBC_a  = jac(3,1);
    //SparseMatrixClass& jacBC_qI = jac(3,2);
    SparseMatrixClass& jacBC_lg = jac(3,3);

    jac = 0;

    IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacPDE_q, jacPDE_a,
                                                                                    jacAu_q, jacAu_a),
                                            xfld, (qfld, afld), quadratureOrder, 1 );

    IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacPDE_q, jacPDE_a, jacPDE_qI,
                                                        jacAu_q ,           jacAu_qI ,
                                                        jacInt_q, jacInt_a, jacInt_qI),
        xfld, (qfld, afld), uIfld, quadratureOrder, 3);

    IntegrateBoundaryTraceGroups<TopoD2>::integrate(
        JacobianBoundaryTrace_HDG<SurrealClass>(fcnBCNone, jacPDE_q, jacPDE_a,
                                                                jacAu_q , jacAu_a),
        xfld, (qfld, afld), quadratureOrder, 4);

    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBCSoln, jacPDE_q, jacPDE_a, jacPDE_lg,
                                                              jacAu_q , jacAu_a , jacAu_lg ,
                                                              jacBC_q , jacBC_a , jacBC_lg ),
        xfld, (qfld, afld), lgfld, quadratureOrder, 4);

#if 0
    fstream fout( "tmp/jac_HDG_1D.mtx", fstream::out );
    cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
    fstream fout2( "tmp/rsd_HDG_1D.mtx", fstream::out );
    for (int i = 0; i<4 ; i++)
      for (int k = 0; k<nDOFPDE; k++)
        fout2 << rsd[i][k][0] << "\n";
#endif

    // solve

    SLA::UMFPACK<SystemMatrixClass> solver;

     SystemVectorClass sln = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};

     sln = solver.inverse(jac)*rsd;

     // updated solution

     for (int k = 0; k < nDOFPDE; k++)
       qfld.DOF(k) -= sln[0][k];

     for (int k = 0; k < nDOFPDE; k++)
       for (int d = 0; d < PhysD2::D; d++)
         afld.DOF(k)[d] -= sln[1][PhysD2::D*k+d];

     for (int k = 0; k < nDOFInt; k++)
       uIfld.DOF(k) -= sln[2][k];

     for (int k = 0; k < nDOFBC; k++)
       lgfld.DOF(k) -= sln[3][k];

     // check that the residual is zero

     rsd = 0;

     IntegrateCellGroups<TopoD2>::integrate( ResidualCell_HDG(fcnCell, rsd(0), rsd(1)),
                                             xfld, (qfld, afld), quadratureOrder, 1 );

     IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(0), rsd(1), rsd(2)),
                                                             xfld, (qfld, afld), uIfld, quadratureOrder, 3);

     IntegrateBoundaryTraceGroups<TopoD2>::integrate( ResidualBoundaryTrace_HDG(fcnBCNone, rsd(0), rsd(1)),
                                                           xfld, (qfld, afld), quadratureOrder, 4 );

     IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBCSoln, rsd(0), rsd(1), rsd(3)),
                                                         xfld, (qfld, afld), lgfld, quadratureOrder, 4 );


     Real rsdPDEnrm = 0;
     for (int n = 0; n < nDOFPDE; n++)
       rsdPDEnrm += pow(rsd[0][n],2);

     BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

     Real rsdAunrm = 0;
     for (int n = 0; n < nDOFAu; n++)
       rsdAunrm += pow(rsd[1][n],2);

     BOOST_CHECK_SMALL( sqrt(rsdAunrm), 1e-12 );

     Real rsdIntnrm = 0;
     for (int n = 0; n < nDOFInt; n++)
       rsdIntnrm += pow(rsd[2][n],2);

     BOOST_CHECK_SMALL( sqrt(rsdIntnrm), 1e-12 );

     Real rsdBCnrm = 0;
     for (int n = 0; n < nDOFBC; n++)
       rsdBCnrm += pow(rsd[3][n],2);

     BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

     // L2 solution error

     ArrayQ SquareError = 0;
     IntegrateCellGroups<TopoD2>::integrate(
         FunctionalCell( fcnErr, SquareError ), xfld, qfld, quadratureOrder, 1 );
     Real norm = SquareError;

     //hVec[indx] = 1./ii;
     //hDOFVec[indx] = 1./sqrt(nDOFPDE);
     normVec[indx] = sqrt( norm );
     indx++;


     duration = ( std::clock() - start) / (double) CLOCKS_PER_SEC;

#if 1
     cout << "P = " << order << " ii = " << ii << ": DOF = " << nDOFtot;
     cout << " CPUTime = " << duration << " s";
     cout << " : L2 error = " << sqrt( norm );
     if (indx > 1)
       cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
     cout << endl;
#endif


//      //dump solution
//      for (int k = 0; k < nDOFPDE; k++)
//        cout << qfld.DOF(k) << "\n";
//      output_Tecplot(qfld, "tmp/ufldout_topoff.dat");


  }
  cout << "\n";

  }

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
