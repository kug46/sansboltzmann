// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Adapt2D_HDG_NavierStokes_FlatPlate_btest
// Testing of the MOESS framework on the advection-diffusion pde
// Uses error estimates to drive MOESS


#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/IntegrandCell_HDG_Output.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_OutputWeightRsd_HDG.h"

#include "Adaptation/MOESS/ProblemStatement_HDG_ErrorEstimate.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_FlatPlate_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_HDG_NavierStokes_FlatPlate_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_HDG_NavierStokes_FlatPlate_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  // typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef BCParameters<BCVector> BCParams;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_Entropy<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_HDG_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_Force<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, HDG> IntegrandOutputClass;
#endif

  typedef ProblemStatement_HDG_ErrorEstimate<NDPDEClass, BCNDConvertSpace, BCVector, HDG,
                                             XField<PhysD2, TopoD2>, IntegrandOutputClass> ProblemStatementClass;

  mpi::communicator world;

  // Grid
  int power = 0;

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_FlatPlate_X1(world, power) );

  std::vector<Real> tol = {1e-10, 1e-10, 1e-10};

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.3;
  const Real Reynolds = 1000;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real tRef = 1;          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum );

  // initial conditions
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  // BC
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aSpec = atan(vRef/uRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;
  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2,4};
  BCBoundaryGroups["BCNoSlip"] = {1};
  BCBoundaryGroups["BCOut"] = {3};
  BCBoundaryGroups["BCIn"] = {5};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);


  #ifndef BOUNDARYOUTPUT
    // Entropy output
    NDOutputClass fcnOutput(pde);
    OutputIntegrandClass outputIntegrand(fcnOutput, {0});
  #else
    // Drag output
    NDOutputClass outputFcn(pde, 1., 0.);
    IntegrandOutputClass outputIntegrand( outputFcn, {1} );
  #endif

  // Newton solver
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-32;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);


  //--------ADAPTATION LOOP--------

  int maxIter = 25;

  for (int order = 1; order <= 2; order++)
    for (int power = 0; power<=2; power++)
    {
      Real targetCost = 500.0*pow(2.0,power);

      const int quadOrder = 3*order + 1;

      const int string_pad = 5;
      std::string int_pad = std::string(string_pad - std::to_string((int)targetCost).length(), '0') + std::to_string((int)targetCost);
      std::string filename_base = "tmp/NS_FP_Re1000/HDG_" + int_pad + "_P" + std::to_string(order) + "/";

      std::string adapthist_filename = filename_base + "test.adapthist";
      fstream fadapthist( adapthist_filename, fstream::out );
      BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " +  adapthist_filename);

      PyDict MOESSDict;
      MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
      MOESSDict[MOESSParams::params.Optimizer_MaxEval] = 6000;
      MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
      MOESSDict[MOESSParams::params.UniformRefinement] = false;

      PyDict MesherDict;
      MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Epic;
      //  MesherDict[EpicParams::params.minH] = 1e-11; //Minimum Mesh Size
      //  MesherDict[EpicParams::params.MaxAnisotropy] = 1e4;
      //  MesherDict[EpicParams::params.CSF] = "grids/joukowski_initial_mesh/jouk.csf"; //Geometry file
      //  MesherDict[EpicParams::params.Geometry] = "Airfoil Farfield"; //Geometry list
      //  MesherDict[EpicParams::params.minGeom] = 1e-6;
      //  MesherDict[EpicParams::params.maxGeom] = 0.5;
      //  MesherDict[EpicParams::params.nPointGeom] = -1;
      //  MesherDict[EpicParams::params.ProjectionMethod] = EpicParams::params.ProjectionMethod.CavityRegrid;

      PyDict AdaptDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = targetCost;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
      AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename_base;

      MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

      MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

      for (int iter = 0; iter < maxIter+1; iter++)
      {
        std::cout<<"-----Adaptation Iteration "<<iter<<"-----"<<std::endl;

        std::vector<int> cellGroups = {0};
        std::vector<int> interiorTraceGroups;
        for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
          interiorTraceGroups.push_back(i);

        ProblemStatementClass problem(*pxfld, pde, disc, tol, quadOrder, order,
                                      BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                      cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups,
                                      NewtonSolverDict, outputIntegrand);

        problem.setInitialSolution(q0);

        problem.solveGlobalPrimalProblem();
        std::string qfld_filename = filename_base + "qfld_a" + std::to_string(iter) + ".plt";
        problem.exportPrimalSolutionTo(qfld_filename);

        problem.solveGlobalAdjointProblem();
        std::string adjfld_filename = filename_base + "adjfld_a" + std::to_string(iter) + ".plt";
        problem.exportAdjointSolutionTo(adjfld_filename);

        problem.computeErrorEstimates();
        pxfld = mesh_adapter.adapt(*pxfld, cellGroups, problem, iter);
      }

      fadapthist.close();
    }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
