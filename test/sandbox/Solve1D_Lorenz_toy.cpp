// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_DGBR2_Lorenz_toy
// testing of 1-D DG with Lorenz

//#define SANS_FULLTEST
//#define SANS_VERBOSE
// #define USE_VMSD
// #define USE_VMSDBR2
// #define USE_DGBR2
#define USE_DGA

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include "SANS_btest.h"

#include <boost/mpl/vector_c.hpp>

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

// #ifdef USE_DGBR2
// // This is a HACK while I build up the case...
// #define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
// #include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"
// #endif

#include "pde/Lorenz/PDELorenz.h"
#include "pde/Lorenz/BCLorenz.h"
#include "pde/Lorenz/OutputLorenz.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/VectorFunction1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#if defined(USE_VMSD)
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "Discretization/VMSD/SolutionData_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"
#include "Adaptation/MOESS/SolverInterface_VMSD.h"
#elif defined(USE_VMSDBR2)
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"
#include "Discretization/VMSDBR2/SolutionData_VMSD_BR2.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_VMSD_BR2.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_Local_VMSD_BR2.h"
#include "Adaptation/MOESS/SolverInterface_VMSD_BR2.h"
#elif defined(USE_DGBR2)
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"
#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#elif defined(USE_DGA)
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/DG/SolutionData_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"
#include "Adaptation/MOESS/SolverInterface_DGAdvective.h"
#else
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output_Stabilized.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#endif

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
// #include "Field/FieldLiftLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_EG_Cell.h"

#include "Meshing/XField1D/XField1D.h"

#include "Field/output_gnuplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

namespace SANS
{

}

using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_DGBR2_Lorenz_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve1D_DGBR2_Lorenz )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;

  typedef PDELorenz PDEClass;

  typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;

  typedef BCLorenzVector BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef ScalarFunction1D_ZeroLorenz Solution1Exact;
  typedef ScalarFunction1D_ZeroLorenz Solution2Exact;
  typedef ScalarFunction1D_ZeroLorenz Solution3Exact;
  typedef Vector3Function1D<Solution1Exact, Solution2Exact, Solution3Exact> SolutionExact;
  const Real trueOutput= 103.5;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef OutputLorenz_SolutionSquared OutputClass;
  // typedef OutputCell_SolutionSquared<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysDim, OutputClass> NDOutputClass;
#if defined(USE_VMSD) || defined(USE_VMSDBR2)
  typedef IntegrandCell_VMSD_Output<NDOutputClass, NDPDEClass> OutputIntegrandClass;
#elif defined(USE_DGBR2)
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#elif defined(USE_DGA)
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDOutputClass,
      NDPDEClass> OutputIntegrandClass;
#endif

  typedef OutputLorenz_SolutionErrorSquared ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
#if defined(USE_VMSD) || defined(USE_VMSDBR2)
  typedef IntegrandCell_VMSD_Output<NDErrorClass, NDPDEClass> ErrorIntegrandClass;
#elif defined(USE_DGBR2)
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;
#elif defined(USE_DGA)
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;
#else
  typedef IntegrandCell_Galerkin_Output_Stabilized<NDErrorClass,
      NDPDEClass> ErrorIntegrandClass;
#endif

  typedef ParamType_None ParamBuilderType;

#if defined(USE_VMSD)
  typedef SolutionData_VMSD<PhysDim, TopoDim, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_VMSD<SolutionClass, PrimalEquationSetClass,
      OutputIntegrandClass> SolverInterfaceClass;
#elif defined(USE_VMSDBR2)
  typedef SolutionData_VMSD_BR2<PhysDim, TopoDim, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_VMSD_BR2<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_VMSD_BR2<SolutionClass, PrimalEquationSetClass,
        OutputIntegrandClass> SolverInterfaceClass;
#elif defined(USE_DGBR2)
  typedef SolutionData_DGBR2<PhysDim, TopoDim, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass,
      OutputIntegrandClass> SolverInterfaceClass;
#elif defined(USE_DGA)
  typedef SolutionData_DGAdvective<PhysDim, TopoDim, NDPDEClass,
      ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, DGAdv, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGAdvective<SolutionClass, PrimalEquationSetClass,
      OutputIntegrandClass> SolverInterfaceClass;
#else
  typedef SolutionData_Galerkin_Stabilized<PhysDim, TopoDim, NDPDEClass,
      ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace,
      BCVector, AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass,
      OutputIntegrandClass> SolverInterfaceClass;
#endif

  // mpi
  mpi::communicator world;

  // SPECIFICATIONS

  // source term constants
  // const Real alpha0= 10.; // sigma
  // const Real alpha1= 0.5; // rho
  // const Real alpha2= 8./3.; // beta
  const Real alpha0= 10.; // sigma
  const Real alpha1= 28.; // rho
  const Real alpha2= 8./3.; // beta
  // const Real alpha0= 0.; // sigma
  // const Real alpha1= 0.; // rho
  // const Real alpha2= 0.; // beta

  // initial conditions (as influx BC)
  const Real q0B= 1.;
  const Real q1B= 1.;
  const Real q2B= 1.;

  // temporal domain
  const Real xL= 0;
  const Real xR= 100;

  // discretization settings
  const int maxNewtonIter= 50;

  // orders to cycle through
  const int ordermin= 0; // 0; // 1;
  const int ordermax= 0; // 0; // 1;

  // grid resolution factors to cycle through
  const int factormin= 5;
  const int factormax= 5;

  // GENERATE PHYSICS

  // create PDE class
  NDPDEClass pde(alpha0, alpha1, alpha2);

  // exact solution
  PyDict solnArgs;
  solnArgs[Solution1Exact::ParamsType::params.eqno]= 0;
  solnArgs[Solution1Exact::ParamsType::params.uB0]= q0B;
  solnArgs[Solution1Exact::ParamsType::params.uB1]= q1B;
  solnArgs[Solution1Exact::ParamsType::params.uB2]= q2B;
  Solution1Exact sol1(solnArgs);

  solnArgs[Solution2Exact::ParamsType::params.eqno]= 1;
  solnArgs[Solution2Exact::ParamsType::params.uB0]= q0B;
  solnArgs[Solution2Exact::ParamsType::params.uB1]= q1B;
  solnArgs[Solution2Exact::ParamsType::params.uB2]= q2B;
  Solution2Exact sol2(solnArgs);

  solnArgs[Solution3Exact::ParamsType::params.eqno]= 2;
  solnArgs[Solution3Exact::ParamsType::params.uB0]= q0B;
  solnArgs[Solution3Exact::ParamsType::params.uB1]= q1B;
  solnArgs[Solution3Exact::ParamsType::params.uB2]= q2B;
  Solution3Exact sol3(solnArgs);

  NDSolutionExact solutionExact(sol1, sol2, sol3);

  // none BC
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType]= BCParams::params.BC.None;

  // dirichlet BC
  PyDict BCDirichlet_mitState;
  BCDirichlet_mitState[BCParams::params.BC.BCType]=
      BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet_mitState[BCLorenzParams<PhysDim,
      BCTypeDirichlet_mitStateParam>::params.q0B]= q0B;
  BCDirichlet_mitState[BCLorenzParams<PhysDim,
      BCTypeDirichlet_mitStateParam>::params.q1B]= q1B;
  BCDirichlet_mitState[BCLorenzParams<PhysDim,
      BCTypeDirichlet_mitStateParam>::params.q2B]= q2B;

  // create BC list
  PyDict BCList;
  BCList["BCNone"]= BCNone;
  BCList["BCDirichlet_mitState"]= BCDirichlet_mitState;

  // define the boundarygroups for each boundary condition
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["BCNone"]= {1}; // right/temporal outflow
  BCBoundaryGroups["BCDirichlet_mitState"]= {0}; // left/temporal inflow

  // check the BC dictionary
  BCParams::checkInputs(BCList);

  // SET UP DISCRETIZATION

#if defined(USE_VMSD) || defined(USE_VMSDBR2)
  DiscretizationVMSD stab;

  enum ResidualNormType ResNormType= ResidualNorm_L2_DOFWeighted;
  std::vector<Real> tol= {1.e-10, 1.e-10, 1.e-10};
#elif defined(USE_DGBR2)
  Real viscousEtaParameter= 2*Line::NTrace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  enum ResidualNormType ResNormType= ResidualNorm_L2;
  std::vector<Real> tol= {1.e-10, 1.e-10};
#elif defined(USE_DGA)
  enum ResidualNormType ResNormType= ResidualNorm_L2;
  std::vector<Real> tol= {1.e-10, 1.e-10};
#else
  // create discretization parameters
  StabilizationMatrix stab(StabilizationType::Unstabilized);

  enum ResidualNormType ResNormType= ResidualNorm_L2;
  std::vector<Real> tol= {1.e-10, 1.e-10};
#endif

  // create output functional to adapt with (dummy)
  NDOutputClass fcnOutput;
#if defined(USE_VMSD) || defined(USE_VMSDBR2)
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, true);
#elif defined(USE_DGBR2) || defined(USE_DGA)
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});
#else
  OutputIntegrandClass outputIntegrand(pde, fcnOutput, {0}, stab);
#endif

  // create integrand for exact solution error
  // NDErrorClass fcnError(solnArgs);
  NDErrorClass fcnError(sol1, sol2, sol3);
#if defined(USE_VMSD) || defined(USE_VMSDBR2)
  ErrorIntegrandClass errorIntegrand(fcnError, {0}, true);
#elif defined(USE_DGBR2) || defined(USE_DGA)
  ErrorIntegrandClass errorIntegrand(fcnError, {0});
#else
  ErrorIntegrandClass errorIntegrand(fcnError, {0}, stab);
#endif

  // nonlinear solver dicts
  PyDict SolverContinuationDict;
  PyDict NonlinearSolverDict;
  PyDict NewtonSolverDict;
  PyDict LinearSolverDict;
  PyDict LineUpdateDict;
  PyDict UMFPACKDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver]=
      SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver]= UMFPACKDict;

  // LineUpdateDict[LineUpdateParam::params.LineUpdate.Method]
  //     = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver]= UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate]= LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations]= 1; // 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations]= maxNewtonIter;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver]= NewtonSolverDict;

  SolverContinuationDict[
      SolverContinuationParams<TemporalMarch>::params.Continuation]= NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  // PREP OUTPUT

  const int string_pad= 9;

#if defined(USE_VMSD)
  std::string filename_base= "tmp/LZ1D_VMSD_unifconv/";
#elif defined(USE_VMSDBR2)
  std::string filename_base= "tmp/LZ1D_VMSDBR2_unifconv/";
#elif defined(USE_DGBR2)
  std::string filename_base= "tmp/LZ1D_DGBR2_unifconv/";
#elif defined(USE_DGA)
  std::string filename_base= "tmp/LZ1D_DGAdv_unifconv/";
#else
  std::string filename_base= "tmp/LZ1D_CG_unifconv/";
#endif

  // create filesystems
  if (world.rank() == 0)
    boost::filesystem::create_directories(filename_base);

  // output file
  std::string convhist_filename= filename_base + "test.convhist";
  fstream convhist;

  if (world.rank() == 0)
  {
    bool fileStarted= boost::filesystem::exists(convhist_filename);

    convhist.open(convhist_filename, fstream::app);
    BOOST_REQUIRE_MESSAGE(convhist.good(), "Error opening file: " + convhist_filename);

    if (!fileStarted)
    {
      convhist << "VARIABLES=";
      convhist << "\"p\"";
      convhist << ", \"DOFrequested\"";
      convhist << ", \"1/DOF\"";
      convhist << ", \"Nelem\"";
      convhist << ", \"Ndof\"";
      convhist << ", \"L2 error\"";
      convhist << std::endl;
    }
    convhist << std::setprecision(16) << std::scientific;
  }

  // DO THE DAMN THING!

  // loop over orders
  for (int order= ordermin; order <= ordermax; order++)
  {
    // dial in stabilization
#if !defined(USE_VMSD) && !defined(USE_VMSDBR2) && !defined(USE_DGBR2) && !defined(USE_DGA)
    stab.setStabOrder(order);
    stab.setNitscheOrder(order);
#elif defined(USE_VMSD) || defined(USE_VMSDBR2)
    stab.setNitscheOrder(order);
#endif

    // loop over grid resolution factors
    for (int factor= factormin; factor <= factormax; factor++)
    {
      // number of elements
      int ii= pow(10, factor);
      int iiDof= (order + 1)*ii;

      // to make sure folders have a consistent number of zero digits
      std::string int_pad= std::string(string_pad
          - std::to_string((int) iiDof).length(), '0')
          + std::to_string((int) iiDof);

      // case filename
      std::string filename_case= filename_base + "dof" + int_pad
          + "p" + std::to_string(order) + "/";

      // create filesystems
      if (world.rank() == 0)
        boost::filesystem::create_directories(filename_case);

      std::string adapthist_filename = filename_case + "test.adapthist";
      fstream fadapthist;

      if (world.rank() == 0)
      {
        fadapthist.open( adapthist_filename, fstream::out );
        BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + adapthist_filename);

        fadapthist << std::setw(4)  << "Iter"
            << std::setw(12) << "DOFTrue"
            << std::setw(16) << "DOFEstimate"
            << std::setw(16) << "DOFPrediction"
            << std::setw(20) << "ErrorIndicator"
            << std::setw(20) << "ErrorEstimate"
            << std::setw(20) << "ErrorTrue"
            << std::setw(22) << "Output"
            << std::setw(11) << "EvalCount"
            << std::setw(15) << "ObjReduction"
            << std::endl;
      }

      // xfield
      XField1D xfld(ii, xL, xR);

      // boundaries and elements to include (all)
      std::vector<int> cellGroups= {0};

      std::vector<int> active_boundaries=
          BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups);

      std::vector<int> interiorTraceGroups;
      for (int i= 0; i < xfld.nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      // solution data
      std::shared_ptr<SolutionClass> pGlobalSol;
#if defined(USE_VMSD) || defined(USE_VMSDBR2)
      pGlobalSol= std::make_shared<SolutionClass>(xfld, pde, stab, order, order,
          order + 1, order + 1,
          BasisFunctionCategory_Lagrange,
          BasisFunctionCategory_Lagrange,
          BasisFunctionCategory_Lagrange,
          active_boundaries);
#elif defined(USE_DGBR2)
      pGlobalSol= std::make_shared<SolutionClass>(xfld, pde, order, order + 1,
          BasisFunctionCategory_Lagrange,
          BasisFunctionCategory_Lagrange,
          active_boundaries, disc);
#elif defined(USE_DGA)
      pGlobalSol= std::make_shared<SolutionClass>(xfld, pde, order, order + 1,
          BasisFunctionCategory_Legendre,
          BasisFunctionCategory_Legendre,
          active_boundaries);
#else
      pGlobalSol= std::make_shared<SolutionClass>(xfld, pde, order, order + 1,
          BasisFunctionCategory_Lagrange,
          BasisFunctionCategory_Lagrange,
          active_boundaries, stab);
#endif
      const int quadOrder= 2*(order + 1);

      // create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface= std::make_shared<SolverInterfaceClass>(*pGlobalSol,
          ResNormType, tol, quadOrder, cellGroups, interiorTraceGroups,
          BCList, BCBoundaryGroups, SolverContinuationDict, LinearSolverDict,
          outputIntegrand);

#if 1
      ifstream infile("/Users/coryfrontin/Desktop/data.txt");
      std::cout << "loading...";

      int iDOF= 0;
      Real DOF0= q0B;
      Real DOF1= q1B;
      Real DOF2= q2B;

      pGlobalSol->primal.qfld.DOF(iDOF)[0]= DOF0;
      pGlobalSol->primal.qfld.DOF(iDOF)[1]= DOF1;
      pGlobalSol->primal.qfld.DOF(iDOF)[2]= DOF2;

      while (infile >> iDOF >> DOF0 >> DOF1 >> DOF2)
      {
        if (iDOF == 0)
          continue;
        else if (iDOF < 0)
          SANS_DEVELOPER_EXCEPTION("??? nick young meme ???");
        else if (iDOF > pGlobalSol->primal.qfld.nDOF())
          SANS_DEVELOPER_EXCEPTION("size the damn problem right to recycle data.");

        std::cout << "iDOF: " << iDOF - 1 << "\t"
            << "(" << DOF0 << ", " << DOF1 << ", " << DOF2 << ")" << std::endl;
        pGlobalSol->primal.qfld.DOF(iDOF - 1)[0]= DOF0;
        pGlobalSol->primal.qfld.DOF(iDOF - 1)[1]= DOF1;
        pGlobalSol->primal.qfld.DOF(iDOF - 1)[2]= DOF2;
      }
      infile.close();

      std::cout << " done!" << std::endl;
#else
      pGlobalSol->setSolution(0.0);
#endif

      pInterface->solveGlobalPrimalProblem();
      // pInterface->solveGlobalAdjointProblem();

      // compute exact error
      QuadratureOrder quadratureOrder(pGlobalSol->primal.qfld.getXField(),  2*(order + 1));

      Real squareError= 0;

      IntegrateCellGroups<TopoDim>::integrate(FunctionalCell_Galerkin(errorIntegrand, squareError),
          pGlobalSol->primal.qfld.getXField(),
          pGlobalSol->primal.qfld, quadratureOrder.cellOrders.data(),
          quadratureOrder.cellOrders.size());

#ifdef SANS_MPI
      squareError= boost::mpi::all_reduce(*xfld.comm(), squareError, std::plus<Real>());

      int nDOFtotal= 0;
      boost::mpi::all_reduce(*xfld.comm(), pGlobalSol->primal.qfld.nDOFpossessed(), nDOFtotal, std::plus<int>());

      // count the number of elements possessed by this processor
      int nElem= 0;
      for (int elem= 0; elem < xfld.nElem(); elem++ )
        if (xfld.getCellGroupGlobal<Line>(0).associativity(elem).rank() == world.rank())
          nElem++;

      int nElemtotal= 0;
      boost::mpi::all_reduce(*xfld.comm(), nElem, nElemtotal, std::plus<int>());
#else
      int nDOFtotal= pGlobalSol->primal.qfld.nDOFpossessed();
      int nElemtotal= xfld.nElem();
#endif

      Real norm= sqrt(squareError);

      Real output= pInterface->getOutput();

      std::string qfld_tec_filename= filename_case + "qfld_a0.plt";
      std::string qfld_gnu_filename= filename_case + "qfld_a0.gplt";
      output_Tecplot(pGlobalSol->primal.qfld, qfld_tec_filename);
      output_gnuplot(pGlobalSol->primal.qfld, qfld_gnu_filename);

      cout << "P = " << order << " ii = " << ii << endl;
      cout << "output= " << boost::format("%16.10f") % output << endl;
      cout << "norm= " << boost::format("%16.10e") % norm << endl;

      // file output
      if (world.rank() == 0)
      {
        // outputfile
        convhist << order;
        convhist << ", " << nDOFtotal;
        convhist << ", " << 1.0/nDOFtotal;
        convhist << ", " << nElemtotal;
        convhist << ", " << nDOFtotal;
        convhist << ", " << norm;
        convhist << endl;

        fadapthist << std::setw(4)  << 0;
        fadapthist << std::setw(12) << (int) 0;
        fadapthist << std::setw(16) << std::setprecision(3) << std::fixed << 0.0;
        fadapthist << std::setw(16) << std::setprecision(3) << std::fixed << 0.0;
        fadapthist << std::setw(20) << std::setprecision(10) << std::scientific << (pInterface->getOutput() - trueOutput);
        fadapthist << std::setw(20) << std::setprecision(10) << std::scientific << (pInterface->getOutput() - trueOutput);
        fadapthist << std::setw(20) << std::setprecision(10) << std::scientific << (pInterface->getOutput() - trueOutput);
        fadapthist << std::setw(22) << std::setprecision(12) << std::scientific << pInterface->getOutput();
        fadapthist << std::setw(11) << 0;
        fadapthist << std::setw(15) << std::setprecision(4) << std::scientific << 0.0;
        fadapthist << std::endl;
      }

#if 0

    for (int iDOF= 0; iDOF < pGlobalSol->primal.qfld.nDOF(); iDOF++)
    {
      std::cout << "iDOF= " << iDOF << "\tDOF: ("
          << pGlobalSol->primal.qfld.DOF(iDOF)[0] << ", "
          << pGlobalSol->primal.qfld.DOF(iDOF)[1] << ", "
          << pGlobalSol->primal.qfld.DOF(iDOF)[2] << ")" << std::endl;
    }

#endif

    }
  }

  if (world.rank() == 0)
    convhist.close();

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
