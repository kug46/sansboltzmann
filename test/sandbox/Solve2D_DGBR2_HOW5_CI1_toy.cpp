// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Solve2D_DGBR2_HOW5_CI1_toy
// Trying to build the HOW5 CI1 case


//#define BOUNDARYOUTPUT

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <chrono>
#include <iostream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/IntegrandBoundaryTrace_OutputWeightRsd_DGBR2.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"

#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Output_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_Dispatch_DGBR2.h"

#include "Discretization/isValidState/SetValidStateCell.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/Function/FunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/output_Tecplot_PDE.h"

#include "Meshing/gmsh/XField_gmsh.h"

// This is a HACK while I build up the case...
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{
template<> struct Type2String<QTypeEntropy>                   { static std::string str() { return "EntropyVariables";      } };
template<> struct Type2String<QTypeConservative>              { static std::string str() { return "ConservativeVariables"; } };
template<> struct Type2String<QTypePrimitiveRhoPressure>      { static std::string str() { return "PrimitiveVariables";    } };
template<> struct Type2String<QTypePrimitiveSurrogate>        { static std::string str() { return "SurrogateVariables";    } };
}

using namespace SANS;
//############################################################################//
BOOST_AUTO_TEST_SUITE( Adapt2D_NavierStokes_FlatPlate_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_NavierStokes_FlatPlate_Triangle )
{
  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
//  typedef QTypePrimitiveSurrogate QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_PressureGrad<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> BCVector;

#ifndef BOUNDARYOUTPUT
  typedef OutputEuler2D_totalEnthalpyErrorSquare<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;
#else
  typedef OutputEuler2D_TotalPressure<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDOutputClass>, NDOutputClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> OutputIntegrandClass;
#endif

  typedef ParamType_GenH_CG ParamBuilderType;
  typedef GenHField_CG<PhysD2, TopoD2> GenHFieldType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2,ParamFieldType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Used to compute the volume of the domain
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Const> NDScalarConst;


  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up integrand for stagnation pressure
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeSymmetry_mitState, PDEClass>> BCSymmetry;
  typedef BCNDConvertSpace<PhysD2, BCSymmetry> NDBCSymmetry;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCSymmetry>, NDBCSymmetry::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, DGBR2> IntegrandBCSymmetry;

  typedef OutputNDConvertSpace<PhysD2, OutputEuler2D_TotalPressure<PDEClass>> GaussianPressureClass;
  typedef NDVectorCategory<boost::mpl::vector1<GaussianPressureClass>, GaussianPressureClass::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDOutVecCat, DGBR2> IntegrandOutputPressure;
  ////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////////////////
  // Set up integrand for stagnation pressure
  ////////////////////////////////////////////////////////////////////////////////////////
  typedef OutputEuler2D_totalEnthalpyErrorSquare<NDPDEClass> OutputEntropyErrorSquared;
  typedef OutputNDConvertSpace<PhysD2, OutputEntropyErrorSquared> NDOutputTotalEnthalpyErrorSquared;
  typedef IntegrandCell_Galerkin_Output<NDOutputTotalEnthalpyErrorSquared> IntegrandTotalEnthalpySquareError;
  ////////////////////////////////////////////////////////////////////////////////////////


  mpi::communicator world;

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1.0e-10, 1.0e-10};

  EulerResidualInterpCategory interp = Euler_ResidInterp_Raw;
  bool isSteady = true;
  bool hasSpaceTimeDiffusion = false;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)

  // reference state (freestream)
  const Real Mach = 4.0;

  const Real rhoRef = 1;                            // density scale

#if 1
  const Real tRef = 1;                              // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                      // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
#else
  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
#endif

  // PDE
  GasModel gas(gamma, R);

  const Real inflowTotalEnthalpy = (gas.enthalpy(rhoRef, tRef) + 0.5*qRef*qRef);
  const Real StagPRef = pRef*(1-gamma+2*gamma*Mach*Mach) / (gamma + 1)
                      * pow(((gamma+1)*(gamma+1)*Mach*Mach)/(4*gamma*Mach*Mach - 2*(gamma-1)), gamma/(gamma-1));

  // DGBR2 discretization
  Real viscousEtaParameter = 2*Triangle::NEdge;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDScalarConst constFcn(1);

  PyDict Inflow;
  Inflow[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitiveTemperature;
  Inflow[DensityVelocityTemperature2DParams::params.rho] = rhoRef;
  Inflow[DensityVelocityTemperature2DParams::params.u] = uRef;
  Inflow[DensityVelocityTemperature2DParams::params.v] = vRef;
  Inflow[DensityVelocityTemperature2DParams::params.t] = tRef;

  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCBase;
  typedef BCmitAVSensor2D<BCTypeFullState_mitState, BCBase> BCClass;

  // Define inflow BC
  PyDict BCInflow;
  BCInflow[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCInflow[BCClass::ParamsType::params.Characteristic] = false;
  BCInflow[BCClass::ParamsType::params.StateVector] = Inflow;

  // Define wall BC
  PyDict BCWall;
  BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  // Define outflow BC
  PyDict BCOutflow;
  BCOutflow[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  // Define BC list
  PyDict PyBCList;
  PyBCList["Outflow"] = BCOutflow;
  PyBCList["Wall"] = BCWall;
  PyBCList["Inflow"] = BCInflow;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["Outflow"] = {1,3};
  BCBoundaryGroups["Wall"] = {0};
  BCBoundaryGroups["Inflow"] = {2};

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;
  PyDict ParamDict;

#ifdef INTEL_MKL
  std::cout << "Using MKL\n";
  PyDict MKL_PARDISODict;
  MKL_PARDISODict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = MKL_PARDISODict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = MKL_PARDISODict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-6;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 1;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;
  NewtonSolverDict[NewtonSolverParam::params.Timing] =  true;

#if 0
  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
#else
  //PTC
  NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type] =
                  SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
  NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver]    = NewtonSolverDict;
  NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 1000;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
  NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e6;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLDecreaseFactor]  = 0.1;
  //NonlinearSolverDict[PseudoTimeParam::params.CFLIncreaseFactor]  = 2;
  NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
  NonlinearSolverDict[PseudoTimeParam::params.SteveLogic] = true;
#endif

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;


  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  //--------ADAPTATION LOOP--------
  for (int grid_index = 2; grid_index <= 4; grid_index++)
  {

    std::string filename_base = "tmp/HOW5_CI1/DGBR2/";
    boost::filesystem::path base_dir(filename_base);
    if ( not boost::filesystem::exists(base_dir) )
      boost::filesystem::create_directory(base_dir);

    NonlinearSolverDict[PseudoTimeParam::params.ResidualHistoryFile] = filename_base + "PTC_residual"
        + std::to_string(grid_index) + ".dat";

    // Grid
    std::string meshName = "grids/HOW5_CI1/N5/grid" + std::to_string(grid_index) + ".msh";
    if (world.rank() == 0) std::cout << "Opening: " << meshName << std::endl;
    std::shared_ptr<XField<PhysD2, TopoD2>> pxfld =
        std::make_shared<XField_gmsh<PhysD2,TopoD2>>(world, meshName);
    if (world.rank() == 0) std::cout << "Grid opened" << std::endl;

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    std::shared_ptr<GenHFieldType> phfld = std::make_shared<GenHFieldType>(*pxfld);

    //Check the BC dictionary
    BCParams::checkInputs(PyBCList);

    std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

    //Solution data
    typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;

    std::shared_ptr<SolutionClass> pGlobalSol;
    std::shared_ptr<SolutionClass> pGlobalSolOld;
    std::shared_ptr<SolverInterfaceClass> pInterface;

    std::ofstream resultFile(filename_base+"output.plt", std::ios::out);
    resultFile << "VARIABLES=";
    resultFile << "\"Order\"";
    resultFile << "\"DOF\"";
    resultFile << ", \"1/sqrt(DOF)\"";
    resultFile << ", \"Total Enthalpy L<sup>2</sup> Error\"";
    resultFile << ", \"Total Pressure Error\"";
    resultFile << ", \"Solve Time /ms\"";
    resultFile << std::endl;
    resultFile << std::setprecision(16) << std::scientific;


    for (int order = 1; order <= 3; order++)
    {
      PDEBaseClass pdeEulerAV(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);
      // Sensor equation terms
      Sensor sensor(pdeEulerAV);
      SensorAdvectiveFlux sensor_adv(0.0, 0.0);
      SensorViscousFlux sensor_visc(order);
      SensorSource sensor_source(order, sensor);
      // AV PDE with sensor equation
      NDPDEClass pde(sensor_adv, sensor_visc, sensor_source, isSteady, order,
                     hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, interp, eHarten);

#ifndef BOUNDARYOUTPUT
      // Enthalpy error output

      NDOutputClass fcnOutput(pde, inflowTotalEnthalpy);
      OutputIntegrandClass outputIntegrand(fcnOutput, cellGroups);
#else
      // Pressure output
      NDOutputClass fcnOutput(pde, 1e-3, -0.5, 0.);
      IntegrandOutputPressure outputIntegrand( disc, fcnOutput, BCBoundaryGroups["Wall"] );
#endif

      const int quadOrder = 3*(order + 2)-1; // 14 at order=3

      pGlobalSol = std::make_shared<SolutionClass>((*phfld, *pxfld), pde, order, order,
                                                   BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                   active_boundaries, ParamDict, disc);
//      pGlobalSol->createLiftedQuantityField(0, BasisFunctionCategory_Legendre);

      //Create solver interface
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);

      //Set initial solution
      if (order == 1)
      {
        // initial condition
        ArrayQ q0 = pde.setDOFFrom( AVVariable<DensityVelocityTemperature2D, Real>({{rhoRef, uRef, vRef, tRef}, 0.0}) );

        pGlobalSol->setSolution(q0);
      }
      else
        pGlobalSol->setSolution(*pGlobalSolOld);

      pGlobalSolOld = pGlobalSol;

//        output_Tecplot( pGlobalSol->primal.qfld, filename_base + "qfld_init_a0_P"+std::to_string(order)+".plt" );

      auto t1 = std::chrono::high_resolution_clock::now();
      pInterface->solveGlobalPrimalProblem();
      auto t2 = std::chrono::high_resolution_clock::now();
      std::cout << "P solve: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << "ms" << std::endl;
      string filename = "HOW5_CI1_"
                     + Type2String<QType>::str()
                     + "_P"
                     + std::to_string(order)
                     + "_G"
                     + std::to_string(grid_index)
                     + ".dat";
      output_Tecplot( pde, (*phfld, *pxfld), pGlobalSol->primal.qfld, filename_base + filename );

      std::string sol_filename = filename_base + "sol_a0.plt";
      output_Tecplot(pde, pGlobalSol->paramfld, pGlobalSol->primal.qfld, pGlobalSol->primal.rfld, sol_filename);

      Real domainArea = 0;
      for_each_CellGroup<TopoD2>::apply( FunctionIntegral(constFcn, domainArea, world.rank(), cellGroups), *pxfld );

      QuadratureOrder quadratureOrder( *pxfld, -1 );

      NDBCSymmetry bcSym(pde);
      IntegrandBCSymmetry fcnBC( pde, bcSym, BCBoundaryGroups["Wall"], disc );

      GaussianPressureClass outputFcn(pde, 0.024, -0.5, 0., StagPRef);
      IntegrandOutputPressure outputPressureIntegrand( disc, outputFcn, BCBoundaryGroups["Wall"] );

      Real StagPError = 0.0;
      IntegrateBoundaryTraceGroups<TopoD2>::integrate(
          FunctionalBoundaryTrace_DGBR2( outputPressureIntegrand, fcnBC, StagPError ),
          *pxfld, (pGlobalSol->primal.qfld, pGlobalSol->primal.rfld),
          quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size() );

      NDOutputTotalEnthalpyErrorSquared outEnthalpyError( pde, inflowTotalEnthalpy );
      IntegrandTotalEnthalpySquareError fcnErr( outEnthalpyError, cellGroups );

      Real TotalEnthalpySquareError = 0.0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_Galerkin( fcnErr, TotalEnthalpySquareError ),
          *pxfld, pGlobalSol->primal.qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      resultFile << " " << order;
      resultFile << " " << pGlobalSol->primal.qfld.nDOF();
      resultFile << " " << 1.0/sqrt(pGlobalSol->primal.qfld.nDOF());
      resultFile << " " << sqrt(TotalEnthalpySquareError/domainArea) / (cRef*cRef);
      resultFile << " " << abs(StagPError) / (rhoRef*cRef*cRef);
      resultFile << " " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
      resultFile << std::endl;
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
