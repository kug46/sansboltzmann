// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include <boost/version.hpp>

#include <petscsys.h>

//---------------------------------------------------------------------------//
//
// A fixture to initialize and terminate PETSc in unit tests
//
//---------------------------------------------------------------------------//

struct PETSc_unit_fixture
{

  PETSc_unit_fixture()
  {
#ifndef SANS_MPI
    int narg = 0;
    char** argv = NULL;

    PetscInitialize(&narg, &argv, (char *)0, (char *)0);
#endif
  }

  ~PETSc_unit_fixture()
  {
#ifndef SANS_MPI
    PetscFinalize();
#endif
  }

};


BOOST_GLOBAL_FIXTURE( PETSc_unit_fixture )
// Boost 1.59 removed the ';' in the above macro, but simply adding it for all versions gives compiler warnings for ';;'
#if BOOST_VERSION >= 105900
;
#endif
