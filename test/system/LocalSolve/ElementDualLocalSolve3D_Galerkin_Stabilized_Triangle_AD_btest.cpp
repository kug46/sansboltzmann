// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementLocalSolve_DGBR2_Triangle_Legendre_AD_btest
// testing of 2-D DG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/ForcingFunction3D_MMS.h"
#include "pde/OutputCell_SolutionGradientErrorSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "tools/minmax.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Prints out a lot of useful information
// #define DEBUG_FLAG


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementDualLocalSolve3D_Dual_Galerkin_Triangle_AD_test_suite )

/*
 * Computes a re-solve on a given local mesh, then checks that the solution matches exactly the original
 * As a consequence, this implies that the re-solve was on an unsplit mesh. It is basically lifted from solveLocalProblem
 *
 */
//  // A helper function to construct a Solver interface and test a single element on a grid
//  void localExtractAndTest(const XField<PhysD3, TopoD3>& xfld, const int order, const BasisFunctionCategory basis_category,
//                           const Real small_tol, const Real close_tol, const bool isSplit, const int group, const int elem )
//  {
//    typedef ScalarFunction3D_ASExp SolutionExact;
//    typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;
//
//    typedef PDEAdvectionDiffusion<PhysD3,
//                                 AdvectiveFlux3D_Uniform,
//                                 ViscousFlux3D_Uniform,
//                                 Source3D_UniformGrad > PDEAdvectionDiffusion3D;
//    typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> NDPDEClass;
//
//    typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;
//
//    // Manufactured Solution Adjoints
//
//    typedef OutputCell_SolutionGradientErrorSquared<PDEAdvectionDiffusion3D,NDSolutionExact> OutputClass;
//    typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
//    typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;
//
//    typedef BCParameters<BCVector> BCParams;
//    typedef ParamType_None ParamBuilderType;
//
//    typedef SolutionData_Galerkin_Stabilized<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
//    typedef typename SolutionClass::ParamFieldType ParamFieldType;
//    typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
//                                        AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;
//
//    typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;
//
//    // Pieces for the debugging
//    typedef typename SolverInterfaceClass::LocalProblem LocalProblem;
//
//    typedef typename SolverInterfaceClass::ArrayQ ArrayQ;
//    typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QFieldType;
//    typedef Field_CG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > LGFieldType;
//
//    // A debugging class for exposing protected members
//    struct debugLocalProblem : public LocalProblem
//    {
//      typedef LocalProblem BaseType;
//
//      debugLocalProblem( const SolverInterfaceClass& owner, XField_Local_Base<PhysD3,TopoD3>& xfld_local,
//                         LocalSolveStatus& status) : BaseType(owner,xfld_local,status) {}
//
//      using BaseType::interface_;
//      using BaseType::xfld_local_;
//      using BaseType::status_;
//      using BaseType::quadrule_;
//      using BaseType::maxIter;
//      using BaseType::BCList_local_;
//      using BaseType::BCBoundaryGroups_local_;
//      using BaseType::local_cellGroups_;
//      using BaseType::local_interiorTraceGroups_;
//      using BaseType::active_local_boundaries_;
//      using BaseType::primal_local_;
//      using BaseType::adjoint_local_;
//      using BaseType::parambuilder_;
//      using BaseType::stol;
//      using BaseType::local_error_;
//      using BaseType::solveLocalPrimalProblem;
//    };
//
//
//    ///////////////////////
//    // PRIMAL
//    ///////////////////////
//
//    // PDE
//    Real u = 2.0;
//    Real v = 1.0;
//    AdvectiveFlux3D_Uniform adv(u, v);
//
//    Real nu = 0;
//    Real kxx = nu;
//    Real kxy = 0;
//    Real kyx = 0;
//    Real kyy = nu;
//    ViscousFlux3D_Uniform visc(kxx, kxy, kyx, kyy);
//
//    Real alpha = 0.5;
//    Source3D_UniformGrad source(alpha, 0.5*0, 0.7*0);
//
//    // Create a solution dictionary
//    // Real i = 1;
//    PyDict solnArgs;
//    solnArgs[SolutionExact::ParamsType::params.a] = u;
//    solnArgs[SolutionExact::ParamsType::params.b] = v;
//    solnArgs[SolutionExact::ParamsType::params.alpha] = v;
//
//    NDSolutionExact solnExact( solnArgs );
//
//    typedef ForcingFunction3D_MMS<PDEAdvectionDiffusion3D> ForcingType;
//    // Real a = 1.0;
//    std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));
//
//    NDPDEClass pde( adv, visc, source, forcingptr );
//
//    ///////////////////////
//    // OUTPUT
//    ///////////////////////
//
//    NDOutputClass fcnOutput(solnExact);
//    OutputIntegrandClass errIntegrandGlobal(fcnOutput, {0}); // Assumes there's only 0 cellGroup
//    OutputIntegrandClass errIntegrandLocal(fcnOutput, {0});
//
//    ///////////////////////
//    // BC
//    ///////////////////////
//
//    solnArgs[BCAdvectionDiffusionParams<PhysD3,BCTypeFunctionLinearRobin_sansLG>::params.Function.Name] =
//        BCAdvectionDiffusionParams<PhysD3,BCTypeFunctionLinearRobin_sansLG>::params.Function.ASExp;
//
//    PyDict BCSoln_Dirichlet;
//    BCSoln_Dirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
//    BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
//    BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
//                     BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
//    BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Upwind] = false;
//
//    PyDict PyBCList;
//    PyBCList["BCDirichlet"] = BCSoln_Dirichlet;
//
//    std::map<std::string, std::vector<int>> BCBoundaryGroups;
//
//    // Construct group vectors for the XField
//    std::vector<int> dirichlet_btracegroup_list = {};
//    for (int i = 0; i < xfld.nBoundaryTraceGroups(); i++)
//      dirichlet_btracegroup_list.push_back(i);
//
//    std::vector<int> cellGroups;
//    for (int i = 0; i < xfld.nCellGroups(); i++)
//      cellGroups.push_back(i);
//
//    std::vector<int> interiorTraceGroups;
//    for (int i = 0; i < xfld.nInteriorTraceGroups(); i++)
//      interiorTraceGroups.push_back(i);
//
//    // Define the BoundaryGroups for each boundary condition
//    BCBoundaryGroups["BCDirichlet"] = dirichlet_btracegroup_list;
//
//    std::vector<int> mitLG_boundaries = BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups );
//
//    //Check the BC dictionary
//    BCParams::checkInputs(PyBCList);
//
//    // Stabilization
//    const StabilizationMatrix stab(StabilizationType::SUPG, TauType::Glasby, order+1);
//
//
//    ///////////////////////
//    // SOLVER DICTIONARIES
//    ///////////////////////
//
//
//    PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;
//
//    LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//    LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
//
//    NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
//    NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
//    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
//    NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
//
//  #ifdef SANS_MPI
//  #ifndef SANS_PETSC
//  #error "SANS must be configured with USE_PETSC=ON if USE_MPI=ON"
//  #endif
//    PyDict PreconditionerDict;
//    PyDict PreconditionerILU;
//    PyDict PETScDict;
//
//    PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
//    PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
//    PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
//    PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
//    //PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
//    //PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order
//
//    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
//    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;
//
//    PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
//    PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-14;
//    PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-12;
//    PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
//    PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
//    PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
//    PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
//
//    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
//    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict;
//  #else
//    PyDict UMFPACKDict;
//    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
//
//    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
//    LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
//  #endif
//
//    NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
//
//    SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;
//
//    // Check inputs
//    SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);
//
//
//    ///////////////////////
//    // SOLUTION CLASS
//    ///////////////////////
//
//    SolutionClass globalSol( xfld, pde, order, order+1,
//                             basis_category, basis_category,
//                             mitLG_boundaries, stab );
//
//    ///////////////////////
//    // SOLVER INTERFACE
//    ///////////////////////
//
//    const int quadOrder = 2*(order+1);
//    std::vector<Real> tol = {1e-11, 1e-11};
//    const ResidualNormType residNormType = ResidualNorm_L2;
//
//    SolverInterfaceClass solverInt( globalSol, residNormType, tol, quadOrder, cellGroups, interiorTraceGroups,
//                                    PyBCList, BCBoundaryGroups,
//                                    SolverContinuationDict, LinearSolverDict,
//                                    errIntegrandGlobal);
//
//    ///////////////////////
//    // GLOBAL COMPUTATIONS
//    ///////////////////////
//
//    globalSol.setSolution(0.0);
//    solverInt.solveGlobalPrimalProblem();
//
//    typedef typename std::set<std::array<int,3>> ElemList;
//
//    const int nTrace = Triangle::NTrace; // Number of different trace
//
//    QuadratureOrder quadratureOrder( xfld, 2*order + 1 );
//
//    XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);
//
//    ///////////////////////
//    // LOOP OVER CELL GROUPS -> ELEMENTS -> TRACES
//    ///////////////////////
//
//    // split the communicator for local solves
//    int comm_rank = xfld.comm()->rank();
//    mpi::communicator comm_local = xfld.comm()->split(comm_rank);
//
//    // Count the number of local solves that decrease and the number that increase
//    // Most should decrease (fingers crossed)...
//    int nIncreaseError = 0;
//    int nDecreaseError = 0;
//
//    const XField_CellToTrace<PhysD3,TopoD3> xfld_connectivity(xfld);
//
//
//    ElemList failed_elem_list;
//    const Field_NodalView nodalView(xfld, {group});
//
//
//    /////////////////////////////////////////////
//    // ELEMENT LOCAL MESH CONSTRUCT
//    /////////////////////////////////////////////
//
// #ifdef DEBUG_FLAG
//    std::cout<< "==================" << std::endl;
//    std::cout<< "group, elem = (" << group << "," << elem <<")" <<std::endl;
//    std::cout<< "==================" << std::endl;
// #endif
//
//    // skip any ghosted cells
//    // Local solve does not work with ghosted cells (ill posed)
//    // if ( xfld.getCellGroup<Triangle>(group).associativity(elem).rank() != comm_rank ) return;
//
//
//    XField_ElementLocal<PhysD3,TopoD3> xfld_local( comm_local, connectivity, nodalView, group, elem, Neighborhood::Dual );
//
//    ArrayQ SqErrLocal = 0, SqErrLocal_resolve = 0;
//    if (!isSplit)
//    {
//      /////////////////////////////////////////////
//      // ELEMENT LOCAL UNSPLIT CHECK
//      /////////////////////////////////////////////
//
//
//      LocalSolveStatus status(false,10);
//      // A debugging version of the Local Problem class
//      // Exposes protected members
//      debugLocalProblem localProblem( solverInt, xfld_local, status);
//
//      // make a copy of the qfld
//      QFieldType  qfld_local_pre(  localProblem.primal_local_.qfld,  FieldCopy() );
//      LGFieldType lgfld_local_pre( localProblem.primal_local_.lgfld, FieldCopy() );
//
//      QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );
//
//      // Compute square error on split mesh before local solve
//      IntegrateCellGroups<TopoD3>::integrate(
//          FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal ),
//          xfld_local, qfld_local_pre, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );
//
//      // Re-solve the local problem
//      localProblem.solveLocalPrimalProblem( localProblem.xfld_local_, localProblem.primal_local_ );
//
//      BOOST_CHECK( status.converged );
//
//      // Check that the re-solve DOFS have been unchanged.
//      // This is because the mesh hasn't changed, so nothing should happen
//      BOOST_CHECK_EQUAL( qfld_local_pre.nDOF(), localProblem.primal_local_.qfld.nDOF() );
//      BOOST_CHECK_EQUAL( lgfld_local_pre.nDOF(), localProblem.primal_local_.lgfld.nDOF() );
//      if ( !( basis_category == BasisFunctionCategory_Hierarchical && order >= 3) )
//      {
//        for ( int i = 0; i < qfld_local_pre.nDOF(); i++)
//          SANS_CHECK_CLOSE( qfld_local_pre.DOF(i), localProblem.primal_local_.qfld.DOF(i), small_tol, close_tol );
//
//        for (int i = 0; i < lgfld_local_pre.nDOF(); i++)
//          SANS_CHECK_CLOSE( lgfld_local_pre.DOF(i), localProblem.primal_local_.lgfld.DOF(i), small_tol, close_tol );
//      }
//
//
//      // Compute square error on split mesh before local solve
//      IntegrateCellGroups<TopoD3>::integrate(
//          FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal_resolve ),
//          xfld_local, localProblem.primal_local_.qfld,
//          quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );
//
//      // Nothing should change, but there is projection so might not be exact
//      SANS_CHECK_CLOSE( SqErrLocal, SqErrLocal_resolve, small_tol, close_tol );
//      if ( abs(SqErrLocal - SqErrLocal_resolve)>close_tol )
//        failed_elem_list.insert( std::array<int,3>{{group,elem,-1}} );
//
//    }
//    else if (isSplit)
//    {
//
//      for ( int trace = 0; trace < nTrace; trace++)
//      {
//
//        /////////////////////////////////////////////
//        // ELEMENT LOCAL SPLIT CHECK
//        /////////////////////////////////////////////
//
//        XField_ElementLocal<PhysD3,TopoD3> xfld_local( xfld_local, trace );
//
//        LocalSolveStatus status_split(false,10);
//        // A debugging version of the Local Problem class
//        // Exposes protected members
//        debugLocalProblem localProblem_split( solverInt, xfld_local, status_split);
//
//        QFieldType qfld_local_split_pre( localProblem_split.primal_local_.qfld, FieldCopy() );
//
//        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );
//
//        // Compute square error on split mesh before local solve
//        IntegrateCellGroups<TopoD3>::integrate(
//            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal ),
//            xfld_local, qfld_local_split_pre,
//            quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );
//
//        localProblem_split.solveLocalPrimalProblem( localProblem_split.xfld_local_, localProblem_split.primal_local_ );
//
//        BOOST_CHECK( status_split.converged );
//
//        IntegrateCellGroups<TopoD3>::integrate(
//            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal_resolve ),
//            xfld_local, localProblem_split.primal_local_.qfld,
//            quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );
//
//        //BOOST_CHECK_GE( SqErrLocal, SqErrLocal_resolve );
//        if (SqErrLocal + close_tol >= SqErrLocal_resolve)
//          nDecreaseError++;
//        else
//          nIncreaseError++;
//
//        if ( SqErrLocal + close_tol < SqErrLocal_resolve )
//        {
// #ifdef DEBUG_FLAG
//          std::cout<< "SqErrLocal - SqErrLocal_resolve = " << SqErrLocal -  SqErrLocal_resolve << std::endl;
// #endif
//          failed_elem_list.insert( std::array<int,3>{{group,elem,trace}} );
//        }
//      }
//
//    }
//
//   #ifdef DEBUG_FLAG
//      std::cout<< "failed_elems_traces = ";
//      for (auto it = failed_elem_list.begin(); it != failed_elem_list.end(); ++it)
//        std::cout<< "(" << (*it)[0] << "," << (*it)[1] << "," << (*it)[2] << "), ";
//      std::cout << std::endl;
//  #endif
//
//    // make sure more decrease than increase
//    BOOST_CHECK_GE( nDecreaseError, nIncreaseError );
//  }


// A helper function to construct a Solver interface and test all the edges on a grid
void localExtractAndTest(const XField<PhysD3, TopoD3>& xfld, const int order, const BasisFunctionCategory basis_category,
                         const Real small_tol, const Real close_tol, const bool isSplit )
{
  typedef ScalarFunction3D_SineSineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD3,
                               AdvectiveFlux3D_Uniform,
                               ViscousFlux3D_Uniform,
                               Source3D_UniformGrad > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> NDPDEClass;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

  // Manufactured Solution Adjoints

  typedef OutputCell_SolutionErrorSquared<PDEAdvectionDiffusion3D,NDSolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef BCParameters<BCVector> BCParams;
  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Pieces for the debugging
  typedef typename SolverInterfaceClass::LocalProblem LocalProblem;

  typedef typename SolverInterfaceClass::ArrayQ ArrayQ;
  typedef Field_CG_Cell< PhysD3, TopoD3, ArrayQ > QFieldType;
  typedef Field_CG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > LGFieldType;

  // A debugging class for exposing protected members
  struct debugLocalProblem : public LocalProblem
  {
    typedef LocalProblem BaseType;

    debugLocalProblem( const SolverInterfaceClass& owner, XField_Local_Base<PhysD3,TopoD3>& xfld_local,
                       LocalSolveStatus& status) : BaseType(owner,xfld_local,status) {}

    using BaseType::interface_;
    using BaseType::xfld_local_;
    using BaseType::status_;
    using BaseType::quadrule_;
    using BaseType::maxIter;
    using BaseType::BCList_local_;
    using BaseType::BCBoundaryGroups_local_;
    using BaseType::local_cellGroups_;
    using BaseType::local_interiorTraceGroups_;
    using BaseType::active_local_boundaries_;
    using BaseType::primal_local_;
    using BaseType::adjoint_local_;
    using BaseType::parambuilder_;
    using BaseType::stol;
    using BaseType::local_error_;
    using BaseType::solveLocalPrimalProblem;
  };


  ///////////////////////
  // PRIMAL
  ///////////////////////

  // PDE
  Real u = 0.0;
  Real v = 0.0;
  Real w = 0.0;
  AdvectiveFlux3D_Uniform adv(u, v, w);

  Real nu = 1;
  Real kxx = nu, kxy = 0,  kxz = 0;
  Real kyx = 0,  kyy = nu, kyz = 0;
  Real kzx = 0,  kzy = 0,  kzz = nu;
  ViscousFlux3D_Uniform visc( kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );

  Source3D_UniformGrad source(0.3*0, 0.5*0, 0.7*0, 0.9*0);

  // Create a solution dictionary
  Real a_0 = 1.0, b_0 = 1.0, c_0 = 1.0, C_0 = 1, A_0 = 2;
  PyDict solnArgs;
  solnArgs[SolutionExact::ParamsType::params.a] = a_0;
  solnArgs[SolutionExact::ParamsType::params.b] = b_0;
  solnArgs[SolutionExact::ParamsType::params.c] = c_0;
  solnArgs[SolutionExact::ParamsType::params.C] = C_0;
  solnArgs[SolutionExact::ParamsType::params.A] = A_0;

  NDSolutionExact solnExact( solnArgs );

  typedef ForcingFunction3D_Const<PDEAdvectionDiffusion3D> ForcingType;
  Real a = 1.0;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(a));

  NDPDEClass pde( adv, visc, source, forcingptr );

  ///////////////////////
  // OUTPUT
  ///////////////////////

  NDOutputClass fcnOutput(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnOutput, {0}); // Assumes there's only 0 cellGroup
  OutputIntegrandClass errIntegrandLocal(fcnOutput, {0});

  ///////////////////////
  // BC
  ///////////////////////

  solnArgs[BCAdvectionDiffusionParams<PhysD3,BCTypeFunctionLinearRobin_sansLG>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD3,BCTypeFunctionLinearRobin_sansLG>::params.Function.SineSineSine;

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
                   BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCDirichlet"] = BCSoln_Dirichlet;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Construct group vectors for the XField
  std::vector<int> dirichlet_btracegroup_list = {};
  for (int i = 0; i < xfld.nBoundaryTraceGroups(); i++)
    dirichlet_btracegroup_list.push_back(i);

  std::vector<int> cellGroups;
  for (int i = 0; i < xfld.nCellGroups(); i++)
    cellGroups.push_back(i);

  std::vector<int> interiorTraceGroups;
  for (int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDirichlet"] = dirichlet_btracegroup_list;

  std::vector<int> mitLG_boundaries = BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups );

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Stabilization
  const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, order+1);


  ///////////////////////
  // SOLVER DICTIONARIES
  ///////////////////////


  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

#ifdef SANS_MPI
#ifndef SANS_PETSC
#error "SANS must be configured with USE_PETSC=ON if USE_MPI=ON"
#endif
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  //PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  //PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-14;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  ///////////////////////
  // SOLUTION CLASS
  ///////////////////////

  SolutionClass globalSol( xfld, pde, order, order+1,
                           basis_category, basis_category,
                           mitLG_boundaries, stab );

  ///////////////////////
  // SOLVER INTERFACE
  ///////////////////////

  const int quadOrder = 2*(order+1);
  std::vector<Real> tol = {1e-11, 1e-11};
  const ResidualNormType residNormType = ResidualNorm_L2;

  SolverInterfaceClass solverInt( globalSol, residNormType, tol, quadOrder, cellGroups, interiorTraceGroups,
                                  PyBCList, BCBoundaryGroups,
                                  SolverContinuationDict, LinearSolverDict,
                                  errIntegrandGlobal);

  ///////////////////////
  // GLOBAL COMPUTATIONS
  ///////////////////////

  globalSol.setSolution(0.0);
  solverInt.solveGlobalPrimalProblem();

  typedef typename std::set<std::array<int,3>> ElemList;

  const int nEdge = Tet::NEdge; // Number of different edges

  QuadratureOrder quadratureOrder( xfld, 2*order + 1 );

  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  ///////////////////////
  // LOOP OVER CELL GROUPS -> ELEMENTS -> TRACES
  ///////////////////////

  // split the communicator for local solves
  int comm_rank = xfld.comm()->rank();
  mpi::communicator comm_local = xfld.comm()->split(comm_rank);

  // Count the number of local solves that decrease and the number that increase
  // Most should decrease (fingers crossed)...
  int nIncreaseError = 0;
  int nDecreaseError = 0;

  const XField_CellToTrace<PhysD3,TopoD3> xfld_connectivity(xfld);

  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    ElemList failed_elem_list;
    const Field_NodalView nodalView(xfld, {group});

    for ( int elem = 0; elem < xfld.getCellGroupBase(group).nElem(); elem++)
    {
      /////////////////////////////////////////////
      // ELEMENT LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

#ifdef DEBUG_FLAG
      std::cout<< "==================" << std::endl;
      std::cout<< "group, elem = (" << group << "," << elem <<")" <<std::endl;
      std::cout<< "==================" << std::endl;
#endif

      // skip any ghosted cells
      // Local solve does not work with ghosted cells (ill posed)
      if ( xfld.getCellGroup<Tet>(group).associativity(elem).rank() != comm_rank ) continue;

      ArrayQ SqErrLocal = 0, SqErrLocal_resolve = 0;
      if (!isSplit)
      {
        /////////////////////////////////////////////
        // ELEMENT LOCAL UNSPLIT CHECK
        /////////////////////////////////////////////

        XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalView);
        XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);

        LocalSolveStatus status(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem( solverInt, xfld_local, status);

        // make a copy of the qfld
        QFieldType  qfld_local_pre(  localProblem.primal_local_.qfld,  FieldCopy() );
        LGFieldType lgfld_local_pre( localProblem.primal_local_.lgfld, FieldCopy() );

        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD3>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal ),
            xfld_local, qfld_local_pre, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        // Re-solve the local problem
        localProblem.solveLocalPrimalProblem( localProblem.xfld_local_, localProblem.primal_local_ );

        BOOST_CHECK( status.converged );

        // Check that the re-solve DOFS have been unchanged.
        // This is because the mesh hasn't changed, so nothing should happen
        BOOST_CHECK_EQUAL( qfld_local_pre.nDOF(), localProblem.primal_local_.qfld.nDOF() );
        BOOST_CHECK_EQUAL( lgfld_local_pre.nDOF(), localProblem.primal_local_.lgfld.nDOF() );
        if ( !( basis_category == BasisFunctionCategory_Hierarchical && order >= 3) )
        {
          for ( int i = 0; i < qfld_local_pre.nDOF(); i++)
            SANS_CHECK_CLOSE( qfld_local_pre.DOF(i), localProblem.primal_local_.qfld.DOF(i), small_tol, close_tol );

          for (int i = 0; i < lgfld_local_pre.nDOF(); i++)
            SANS_CHECK_CLOSE( lgfld_local_pre.DOF(i), localProblem.primal_local_.lgfld.DOF(i), small_tol, close_tol );
        }


        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD3>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal_resolve ),
            xfld_local, localProblem.primal_local_.qfld,
            quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        // Nothing should change, but there is projection so might not be exact
        SANS_CHECK_CLOSE( SqErrLocal, SqErrLocal_resolve, small_tol, close_tol );
        if ( abs(SqErrLocal - SqErrLocal_resolve) > close_tol )
          failed_elem_list.insert( std::array<int,3>{{group,elem,-1}} );

      }
      else if (isSplit)
      {

        XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalView);

        for ( int edge = 0; edge < nEdge; edge++)
        {

          /////////////////////////////////////////////
          // ELEMENT LOCAL SPLIT CHECK
          /////////////////////////////////////////////

          XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

          LocalSolveStatus status_split(false,10);
          // A debugging version of the Local Problem class
          // Exposes protected members
          debugLocalProblem localProblem_split( solverInt, xfld_local, status_split);

          QFieldType qfld_local_split_pre( localProblem_split.primal_local_.qfld, FieldCopy() );

          QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

          // Compute square error on split mesh before local solve
          IntegrateCellGroups<TopoD3>::integrate(
              FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal ),
              xfld_local, qfld_local_split_pre,
              quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

          localProblem_split.solveLocalPrimalProblem( localProblem_split.xfld_local_, localProblem_split.primal_local_ );

          BOOST_CHECK( status_split.converged );

          IntegrateCellGroups<TopoD3>::integrate(
              FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal_resolve ),
              xfld_local, localProblem_split.primal_local_.qfld,
              quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

          //BOOST_CHECK_GE( SqErrLocal, SqErrLocal_resolve );
          if (SqErrLocal + close_tol >= SqErrLocal_resolve)
            nDecreaseError++;
          else
            nIncreaseError++;

          if ( SqErrLocal + close_tol < SqErrLocal_resolve )
          {
#ifdef DEBUG_FLAG
            std::cout<< "SqErrLocal - SqErrLocal_resolve = " << SqErrLocal -  SqErrLocal_resolve << std::endl;
#endif
            failed_elem_list.insert( std::array<int,3>{{group,elem,edge}} );
          }
        }

      }
    }
#ifdef DEBUG_FLAG
    std::cout<< "failed_elems_traces = ";
    for (auto it = failed_elem_list.begin(); it != failed_elem_list.end(); ++it)
      std::cout<< "(" << (*it)[0] << "," << (*it)[1] << "," << (*it)[2] << "), ";
    std::cout << std::endl;
#endif
  }

  // make sure more decrease than increase
  BOOST_CHECK_GE( nDecreaseError, nIncreaseError );
}

// #define DEBUG_FLAG

// Minimal Tests
#if 0

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_SmallBox_Lagrange_Unsplit_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 1, jj = ii, kk = ii, order = 1;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_SmallBox_Lagrange_Split_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 1, jj = ii, kk = ii, order = 1;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_SmallBox_Lagrange_Unsplit_P2 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 1, jj = ii, kk = ii, order = 2;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_SmallBox_Lagrange_Split_P2 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 1, jj = ii, kk = ii, order = 2;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_SmallBox_Lagrange_Unsplit_P3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 1, jj = ii, kk = ii, order = 3;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_SmallBox_Lagrange_Split_P3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 1, jj = ii, kk = ii, order = 3;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif


#endif



// Full Tests

#if 1


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_BigBox_Lagrange_Unsplit_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 2, jj = ii, kk = ii, order = 1;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_BigBox_Lagrange_Split_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 2, jj = ii, kk = ii, order = 1;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_BigBox_Lagrange_Unsplit_P2 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 2, jj = ii, kk = ii, order = 2;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_BigBox_Lagrange_Split_P2 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 2, jj = ii, kk = ii, order = 2;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_BigBox_Lagrange_Unsplit_P3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 2, jj = ii, kk = ii, order = 3;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve3D_Galerkin_AD_BigBox_Lagrange_Split_P3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  const int ii = 2, jj = ii, kk = ii, order = 3;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
