// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementLocalSolve_DGAdvective_Tet_AD_btest
// testing of 3-D DG with Advection on tetrahedron

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/ForcingFunction3D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/Local/XField_LocalPatch.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/DG/SolutionData_DGAdvective.h"

#include "Adaptation/MOESS/SolverInterface_DGAdvective.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "tools/minmax.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Prints out a lot of useful information
//#define DEBUG_FLAG


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementLocalSolve3D_Primal_DGAdvective_Tet_Legendre_AD_test_suite )

/*
 * Computes a re-solve on a given local mesh, then checks that the solution matches exactly the original
 * As a consequence, this implies that the re-solve was on an unsplit mesh. It is basically lifted from solveLocalProblem
 *
 */

// A helper function to construct a Solver interface and test all the edges on a grid
void localExtractAndTest(const XField<PhysD3, TopoD3>& xfld, const int order, const BasisFunctionCategory basis_category,
                         const Real small_tol, const Real close_tol, const bool isSplit )
{
  typedef ScalarFunction3D_ASExp SolutionExact;
  typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_None,
                                Source3D_Uniform > PDEAdvectionDiffusion3D;
  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> NDPDEClass;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_None> BCVector;

  // Manufactured Solution Adjoints

  typedef OutputCell_SolutionErrorSquared<PDEAdvectionDiffusion3D,NDSolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD3, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef BCParameters<BCVector> BCParams;
  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGAdvective<PhysD3, TopoD3, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, DGAdv, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGAdvective<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Pieces for the debugging
  typedef typename SolverInterfaceClass::LocalProblem LocalProblem;

  typedef typename SolverInterfaceClass::ArrayQ ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QFieldType;
  typedef Field_DG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > LGFieldType;

  // A debugging class for exposing protected members
  struct debugLocalProblem : public LocalProblem
  {
    typedef LocalProblem BaseType;

    debugLocalProblem( const SolverInterfaceClass& owner, XField_Local_Base<PhysD3,TopoD3>& xfld_local,
                       LocalSolveStatus& status) : BaseType(owner,xfld_local,status) {}

    using BaseType::interface_;
    using BaseType::xfld_local_;
    using BaseType::status_;
    using BaseType::quadrule_;
    using BaseType::maxIter;
    using BaseType::BCList_local_;
    using BaseType::BCBoundaryGroups_local_;
    using BaseType::local_cellGroups_;
    using BaseType::local_interiorTraceGroups_;
    using BaseType::active_local_boundaries_;
    using BaseType::primal_local_;
    using BaseType::adjoint_local_;
    using BaseType::parambuilder_;
    using BaseType::stol;
    using BaseType::local_error_;
    using BaseType::solveLocalPrimalProblem;
  };


  ///////////////////////
  // PRIMAL
  ///////////////////////

  // PDE
  Real a = 0.5;
  Real b = 0.25;
  Real c = 0.75;
  AdvectiveFlux3D_Uniform adv(a, b, c);

  ViscousFlux3D_None visc;

  Real alpha = 1;
  Source3D_Uniform source(alpha);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.c] = c;
  solnArgs[NDSolutionExact::ParamsType::params.alpha] = alpha;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction3D_MMS<PDEAdvectionDiffusion3D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  ///////////////////////
  // OUTPUT
  ///////////////////////

  NDOutputClass fcnOutput(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnOutput, {0}); // Assumes there's only 0 cellGroup
  OutputIntegrandClass errIntegrandLocal(fcnOutput, {0});

  ///////////////////////
  // BC
  ///////////////////////
  typedef BCTypeFunctionLinearRobin_sansLG BCType;
  solnArgs[BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function.Exp;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function] = solnArgs;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["Dirichlet"] = BCSoln;
  PyBCList["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Construct group vectors for the XField
  std::vector<int> dirichlet_btracegroup_list = {XField3D_Box_Tet_X1::iXmin,
                                                 XField3D_Box_Tet_X1::iYmin,
                                                 XField3D_Box_Tet_X1::iZmin};

  std::vector<int> none_btracegroup_list= {XField3D_Box_Tet_X1::iXmax,
                                           XField3D_Box_Tet_X1::iYmax,
                                           XField3D_Box_Tet_X1::iZmax};

  std::vector<int> cellGroups;
  for (int i = 0; i < xfld.nCellGroups(); i++)
    cellGroups.push_back(i);

  std::vector<int> interiorTraceGroups;
  for (int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["Dirichlet"] = dirichlet_btracegroup_list;
  BCBoundaryGroups["None"]   = none_btracegroup_list;

  std::vector<int> mitLG_boundaries = BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups );

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  ///////////////////////
  // SOLVER DICTIONARIES
  ///////////////////////

  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

#ifdef SANS_MPI
#ifndef SANS_PETSC
#error "SANS must be configured with USE_PETSC=ON if USE_MPI=ON"
#endif
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  //PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  //PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-14;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  ///////////////////////
  // SOLUTION CLASS
  ///////////////////////

  SolutionClass globalSol( xfld, pde, order, order+1,
                           basis_category, basis_category,
                           mitLG_boundaries );

  ///////////////////////
  // SOLVER INTERFACE
  ///////////////////////

  const int quadOrder = 2*(order+1);
  std::vector<Real> tol = {1e-11, 1e-11};
  const ResidualNormType residNormType = ResidualNorm_L2;

  SolverInterfaceClass solverInt( globalSol, residNormType, tol, quadOrder, cellGroups, interiorTraceGroups,
                                  PyBCList, BCBoundaryGroups,
                                  SolverContinuationDict, LinearSolverDict,
                                  errIntegrandGlobal);

  ///////////////////////
  // GLOBAL COMPUTATIONS
  ///////////////////////

  globalSol.setSolution(0.0);
  solverInt.solveGlobalPrimalProblem();

  typedef typename std::set<std::array<int,3>> ElemList;

  const int nTrace = Tet::NTrace; // Number of different trace

  QuadratureOrder quadratureOrder( xfld, quadOrder );

  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  ///////////////////////
  // LOOP OVER CELL GROUPS -> ELEMENTS -> TRACES
  ///////////////////////

  // split the communicator for local solves
  int comm_rank = xfld.comm()->rank();
  mpi::communicator comm_local = xfld.comm()->split(comm_rank);

  // Count the number of local solves that decrease and the number that increase
  // Most should decrease (fingers crossed)...
  int nIncreaseError = 0;
  int nDecreaseError = 0;

  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    ElemList failed_elem_list;

    for ( int elem = 0; elem < xfld.getCellGroupBase(group).nElem(); elem++)
    {
      /////////////////////////////////////////////
      // ELEMENT LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

#ifdef DEBUG_FLAG
      //      std::cout<< "==================" << std::endl;
      //      std::cout<< "group, elem = (" << group << "," << elem <<")" <<std::endl;
      //      std::cout<< "==================" << std::endl;
#endif

      // skip any ghosted cells
      // Local solve does not work with ghosted cells (ill posed)
      if ( xfld.getCellGroup<Tet>(group).associativity(elem).rank() != comm_rank ) continue;

      ArrayQ SqErrLocal = 0, SqErrLocal_resolve = 0;
      if (!isSplit)
      {
        /////////////////////////////////////////////
        // ELEMENT LOCAL UNSPLIT CHECK
        /////////////////////////////////////////////

        //Extract the local mesh
        XField_LocalPatchConstructor<PhysD3,Tet> xfld_local(comm_local, connectivity, group, elem, SpaceType::Discontinuous);

        // create an unsplit patch
        XField_LocalPatch<PhysD3,Tet> xfld_unsplit( xfld_local );

        LocalSolveStatus status(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem( solverInt, xfld_local, status);

        // make a copy of the qfld
        QFieldType  qfld_local_pre(  localProblem.primal_local_.qfld,  FieldCopy() );
        LGFieldType lgfld_local_pre( localProblem.primal_local_.lgfld, FieldCopy() );

        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD3>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal ),
            xfld_local, qfld_local_pre, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        // Re-solve the local problem
        localProblem.solveLocalPrimalProblem( localProblem.xfld_local_, localProblem.primal_local_ );

        BOOST_CHECK( status.converged );

        // Check that the re-solve DOFS have been unchanged.
        // This is because the mesh hasn't changed, so nothing should happen
        BOOST_CHECK_EQUAL( qfld_local_pre.nDOF(), localProblem.primal_local_.qfld.nDOF() );
        BOOST_CHECK_EQUAL( lgfld_local_pre.nDOF(), localProblem.primal_local_.lgfld.nDOF() );
        if ( !( basis_category == BasisFunctionCategory_Hierarchical && order >= 3) )
        {
          for ( int i = 0; i < qfld_local_pre.nDOF(); i++)
            SANS_CHECK_CLOSE( qfld_local_pre.DOF(i), localProblem.primal_local_.qfld.DOF(i), 1e-7, 1e-7 );

          for (int i = 0; i < lgfld_local_pre.nDOF(); i++)
            SANS_CHECK_CLOSE( lgfld_local_pre.DOF(i), localProblem.primal_local_.lgfld.DOF(i), 5e-8, 5e-8 );
        }

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD3>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal_resolve ),
            xfld_local, localProblem.primal_local_.qfld,
            quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        // Nothing should change, but there is projection so might not be exact
        SANS_CHECK_CLOSE( SqErrLocal, SqErrLocal_resolve, small_tol, close_tol );
        if ( abs(SqErrLocal - SqErrLocal_resolve) > close_tol )
          failed_elem_list.insert( std::array<int,3>{{group,elem,-1}} );

      }
      else if (isSplit)
      {
        XField_LocalPatchConstructor<PhysD3,Tet> xfld_unsplit_local(comm_local, connectivity, group, elem, SpaceType::Discontinuous);

        for ( int trace = 0; trace < nTrace; trace++)
        {

          /////////////////////////////////////////////
          // ELEMENT LOCAL SPLIT CHECK
          /////////////////////////////////////////////

          XField_LocalPatch<PhysD3,Tet> xfld_local_split( xfld_unsplit_local, ElementSplitType::Edge, trace );

          LocalSolveStatus status_split(false,10);
          // A debugging version of the Local Problem class
          // Exposes protected members
          debugLocalProblem localProblem_split( solverInt, xfld_local_split, status_split);

          QFieldType qfld_local_split_pre( localProblem_split.primal_local_.qfld, FieldCopy() );

          QuadratureOrder quadratureOrder_local( xfld_local_split, 2*order + 1 );

          // Compute square error on split mesh before local solve
          IntegrateCellGroups<TopoD3>::integrate(
              FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal ),
              xfld_local_split, qfld_local_split_pre,
              quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

          localProblem_split.solveLocalPrimalProblem( localProblem_split.xfld_local_, localProblem_split.primal_local_ );

          BOOST_CHECK( status_split.converged );

          IntegrateCellGroups<TopoD3>::integrate(
              FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal_resolve ),
              xfld_local_split, localProblem_split.primal_local_.qfld,
              quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

          //BOOST_CHECK_GE( SqErrLocal, SqErrLocal_resolve );
          if (SqErrLocal + 1e-6 > SqErrLocal_resolve)
            nDecreaseError++;
          else
            nIncreaseError++;

          if ( SqErrLocal + 1e-6 < SqErrLocal_resolve )
          {
#ifdef DEBUG_FLAG
            std::cout << "diff = " <<  SqErrLocal - SqErrLocal_resolve << " % diff = " <<  (SqErrLocal - SqErrLocal_resolve)/SqErrLocal << std::endl;
#endif
            failed_elem_list.insert( std::array<int,3>{{group,elem,trace}} );
          }
        }

      }
    }

#ifdef DEBUG_FLAG
    std::cout<< "failed_elems_traces = ";
    for (auto it = failed_elem_list.begin(); it != failed_elem_list.end(); ++it)
      std::cout<< "(" << (*it)[0] << "," << (*it)[1] << "," << (*it)[2] << "), ";
    std::cout << std::endl;
#endif
  }

  // make sure more decrease than increase
  BOOST_CHECK_GE( nDecreaseError, nIncreaseError );
}


//=============================================================================//
// UNSPLIT TESTS - Nothing should change
//=============================================================================//

// -----------------------------------------------------
//  Legendre
// -----------------------------------------------------

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Legendre_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world;
  int order = 1;
  int ii = 2, jj = 2, kk = 2;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Legendre_P2 )
{
  mpi::communicator world;
  mpi::communicator comm = world;

  int order = 2;
  int ii = 2, jj = 2, kk = 2;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Legendre_P3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split( world.rank() < 2 );

  if ( world.rank() >= 2 ) return;

  int order = 3;
  int ii = 1, jj = 1, kk = 1;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

//=============================================================================//
// SPLIT TESTS - Check that energy norm error decreases
//=============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Legendre_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world;

  int order = 1;
  int ii = 2, jj = 2, kk = 2;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Legendre_P2 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split( world.rank() < 2 );

  if ( world.rank() >= 2 ) return;

  int order = 2;
  int ii = 1, jj = 1, kk = 1;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Legendre_P3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split( world.rank() < 2 );

  if ( world.rank() >= 2 ) return;

  int order = 3;
  int ii = 1, jj = 1, kk = 1;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
