// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LocalSolve_HDG_Triangle_AD_btest
// testing of 2-D HDG HDG local solves with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/AlgebraicEquationSet_Local_HDG.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/Local/Field_Local.h"
#include "Field/Local/XField_LocalPatch.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( LocalSolve2D_HDG_Triangle_AD_test_suite )

void localsolve(const XField<PhysD2, TopoD2>& xfld, const int order, const int main_group, const int main_cell,
                const std::vector<int>& global_itracegroup_list, const std::vector<int>& global_btracegroup_list,
                const std::vector<int>& neighbor_elem_list,  const std::vector<int>& neighbor_trace_list,
                const std::vector<int>& mainBtrace_grouplist, const std::vector<int>& mainBtrace_elemlist,
                const std::vector<int>& mainTrace_global_orient)
{
  const int D = PhysD2::D;

  typedef ScalarFunction2D_Vortex SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_DG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_DG_Cell;
  typedef Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> AField2D_DG_Cell;
  typedef Field_DG_Trace<PhysD2, TopoD2, ArrayQ> QIField2D_DG_Trace;
  typedef Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_DG_Trace;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetClass_Sparse;
  typedef AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Dense, XField<PhysD2, TopoD2>> PrimalEquationSetClass_Dense;

  typedef PrimalEquationSetClass_Sparse::BCParams BCParams;

  typedef PrimalEquationSetClass_Sparse::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass_Sparse::SystemVector SystemVectorClass;

  typedef PrimalEquationSetClass_Dense::SystemMatrix SystemMatrixClassD;
  typedef PrimalEquationSetClass_Dense::SystemVector SystemVectorClassD;
  typedef PrimalEquationSetClass_Dense::SystemNonZeroPattern SystemNonZeroPatternD;

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 2.123;
  Real kxy = 0.553;
  Real kyy = 1.007;
  ViscousFlux2D_Uniform visc(kxx, kxy, kxy, kyy);

  Source2D_UniformGrad source(0., 0., 0.);

  Real xvort = -1./sqrt(3);
  Real yvort = 1./sqrt(7);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.x0] = xvort;
  solnArgs[NDSolutionExact::ParamsType::params.y0] = yvort;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict Vortex;
  Vortex[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Vortex;
  Vortex[SolutionExact::ParamsType::params.x0] = xvort;
  Vortex[SolutionExact::ParamsType::params.y0] = yvort;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = Vortex;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
         BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Robin;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  // solution
  QField2D_DG_Cell qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // auxiliary operator
  AField2D_DG_Cell afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  // interface
  QIField2D_DG_Trace qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = 0;

  //Lagrange multiplier
  std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
  LGField2D_DG_Trace lgfld( xfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list );
  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> eqnsettol = {1e-11, 1e-11, 1e-11};

  PrimalEquationSetClass_Sparse PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, eqnsettol,
                                            {0}, global_itracegroup_list, PyBCList, BCBoundaryGroups);

  // residual
  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
  PrimalEqSet.fillSystemVector(q);

  rsd = 0;
  PrimalEqSet.residual(q, rsd);

#if 0 //Print residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < rsd[0].m(); k++) cout << k << "\t" << rsd[0][k] <<endl;

  cout << "Aux residual:" <<endl;
  for (int k = 0; k < rsd[0].m(); k++) cout << k << "\t" << rsd[1][D*k + 0] <<"\t" << rsd[1][D*k + 1] << endl;

  cout <<endl << "Int residual:" <<endl;
  for (int k = 0; k < rsd[2].m(); k++) cout << k << "\t" << rsd[2][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd[3].m(); k++) cout << k << "\t" << rsd[3][k] << endl;
#endif

  // solve
  SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);

  SystemVectorClass dq(PrimalEqSet.vectorStateSize());
  solver.solve(rsd, dq);

  // update solution
  q -= dq;
  PrimalEqSet.setSolutionField(q);

  rsd = 0;
  PrimalEqSet.residual(rsd);
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

  //Check if residual is zero for global solution
  BOOST_REQUIRE( PrimalEqSet.convergedResidual(rsdNorm) );

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  //Extract the local mesh for the given element
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_constructor(comm_local, connectivity, main_group, main_cell, SpaceType::Discontinuous);

  // create an unsplit patch
  XField_LocalPatch<PhysD2,Triangle> xfld_local( xfld_constructor );

  std::vector<int> active_BGroup_list_local = {}; //Using mitState - so lgfld should be empty

  int nLocalInteriorTraceGroups = std::min(xfld_local.nInteriorTraceGroups(), 2);
  std::vector<int> interiorTraceGroups(nLocalInteriorTraceGroups);
  for (int i = 0; i < nLocalInteriorTraceGroups; i++)
    interiorTraceGroups[i] = i;

  int nLocalBoundaryTraceGroups = xfld_local.nBoundaryTraceGroups();
  std::vector<int> boundaryTraceGroups(nLocalBoundaryTraceGroups);
  for (int i = 0; i < nLocalBoundaryTraceGroups; i++)
    boundaryTraceGroups[i] = i;

  Field_Local<QField2D_DG_Cell> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);
  Field_Local<AField2D_DG_Cell> afld_local(xfld_local, afld, order, BasisFunctionCategory_Legendre);
  Field_Local<QIField2D_DG_Trace> qIfld_local(xfld_local, qIfld, order, BasisFunctionCategory_Legendre, interiorTraceGroups, boundaryTraceGroups);
  Field_Local<LGField2D_DG_Trace> lgfld_local(xfld_local, lgfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list_local);

  const int nDOF_per_cell = qfld_local.nDOF()/qfld_local.nElem();
  const int nDOF_per_itrace = qIfld_local.nDOF()/qIfld_local.nElem();
  const int nDOF_per_btrace = nDOF_per_itrace;

  BOOST_REQUIRE(nDOF_per_cell > 0);
  BOOST_REQUIRE(nDOF_per_itrace > 0);
  BOOST_REQUIRE(nDOF_per_btrace > 0);

  std::vector<int> MainBTraceGroup_list;
  int nMainBTraces = 0;

  for (int i = 0; i < qIfld_local.nBoundaryTraceGroups(); i++)
  {
    MainBTraceGroup_list.push_back(i);
    nMainBTraces += qIfld_local.getBoundaryTraceGroupBase(i).nElem();
  }

  int nMainITraces = 0;
  for (int i = 0; i < qIfld_local.nInteriorTraceGroups(); i++)
    nMainITraces += qIfld_local.getInteriorTraceGroupBase(i).nElem();

  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_local["BCName"] = MainBTraceGroup_list;

#if 0
   xfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   xfld_local.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
#endif

#if 0
//   qfld.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;
//   qfld_local.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;

//   afld.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;
//   afld_local.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;

   qIfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   qIfld_local.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;

//   lgfld.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;
//   lgfld_local.dump(3,std::cout);
#endif

   QuadratureOrder quadratureOrder_local( xfld_local, -1 );

   PrimalEquationSetClass_Dense PrimalEqSet_Local(xfld_local, qfld_local, afld_local, qIfld_local, lgfld_local,
                                                  pde, disc, quadratureOrder_local, eqnsettol, {0,1}, {0}, PyBCList, BCBoundaryGroups_local);

   // Local residual
   SystemVectorClassD rsd_local(PrimalEqSet_Local.vectorEqSize());
   rsd_local = 0;
   PrimalEqSet_Local.residual(rsd_local);
   rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout << "Aux residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[1][D*k + 0] <<"\t" << rsd_local[1][D*k + 1] << endl;

  cout <<endl << "Int residual:" <<endl;
  for (int k = 0; k < rsd_local[2].m(); k++) cout << k << "\t" << rsd_local[2][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local[3].m(); k++) cout << k << "\t" << rsd_local[3][k] << endl;
#endif

  //Check if residual is zero for DOFs on main cell
  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNorm) );

  //Zero out qfld and afld solution in main cell, so that residual becomes non-zero
  for (int i = 0; i < nDOF_per_cell; i++)
  {
    qfld_local.DOF(i) = 0.0;
    afld_local.DOF(i) = 0.0;
  }

  //Zero out qIfld solution in main interior traces and main boundary traces, so that the residual becomes non-zero
  int nDOFModify = nMainITraces*nDOF_per_itrace + nMainBTraces*nDOF_per_btrace;
  for (int i = 0; i < nDOFModify; i++)
    qIfld_local.DOF(i) = 0.0;

  //Re-compute residual vector
  SystemVectorClassD q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);

  //Jacobian
  SystemNonZeroPatternD nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClassD jac_local(nz_local);
  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  //Local solve - with extracted sub-system
  SystemVectorClassD dq_local(PrimalEqSet_Local.vectorStateSize());
  dq_local = DLA::InverseLU::Solve(jac_local,rsd_local);

  //Update local solution fields
  q_local -= dq_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNorm) );

// Can't seem to suppress clang analyzer with size checks...
#ifndef __clang_analyzer__

  //Check if local solutions are equal to the global solutions
  std::vector<int> Cell_DOF_map_local(nDOF_per_cell), Cell_DOF_map_global(nDOF_per_cell);
  std::vector<int> Itrace_DOF_map_local(nDOF_per_itrace), Itrace_DOF_map_global(nDOF_per_itrace);
//  int Btrace_DOF_map_local[nDOF_per_btrace], Btrace_DOF_map_global[nDOF_per_btrace];

  Real tol = 1e-8;

  //Check qfld DOFs on main cell
  qfld_local.getCellGroup<Triangle>(0).associativity(0).getCellGlobalMapping(Cell_DOF_map_local.data(), Cell_DOF_map_local.size());
  qfld.getCellGroup<Triangle>(main_group).associativity(main_cell).getCellGlobalMapping(Cell_DOF_map_global.data(), Cell_DOF_map_global.size());

  for (int k = 0; k < nDOF_per_cell; k++)
    BOOST_CHECK_CLOSE( qfld_local.DOF(Cell_DOF_map_local[k]), qfld.DOF(Cell_DOF_map_global[k]), tol );

  //Check afld DOFs on main cell
  afld_local.getCellGroup<Triangle>(0).associativity(0).getCellGlobalMapping(Cell_DOF_map_local.data(), Cell_DOF_map_local.size());
  afld.getCellGroup<Triangle>(main_group).associativity(main_cell).getCellGlobalMapping(Cell_DOF_map_global.data(), Cell_DOF_map_global.size());

  for (int k = 0; k < nDOF_per_cell; k++)
    for (int d = 0; d < D; d++)
      BOOST_CHECK_CLOSE( afld_local.DOF(Cell_DOF_map_local[k])[d], afld.DOF(Cell_DOF_map_global[k])[d], tol );

  // Check qIfld DOFs on the main interior traces
  for (int trace = 0; trace < Triangle::NTrace; trace++)
  {
    // extract trace in global mesh
    const TraceInfo& traceinfo = connectivity.getTrace(main_group, main_cell, trace);

    if (traceinfo.type == TraceInfo::Interior)
    {
      qIfld.getInteriorTraceGroup<Line>(traceinfo.group).associativity(traceinfo.elem).
                                                         getEdgeGlobalMapping(Itrace_DOF_map_global.data(), Itrace_DOF_map_global.size());
      qIfld_local.getInteriorTraceGroup<Line>(0).associativity(trace).getEdgeGlobalMapping(Itrace_DOF_map_local.data(), Itrace_DOF_map_local.size());

      for (int k = 0; k < nDOF_per_itrace; k++)
      {
        if (k == 1 && mainTrace_global_orient[trace] == -1)
        {
          //If the global interior trace is reversed relative to local interior trace,
          //then the DOF for the linear mode should be negated for Legendre basis
          BOOST_CHECK_CLOSE( qIfld_local.DOF(Itrace_DOF_map_local[k]), -qIfld.DOF(Itrace_DOF_map_global[k]), tol );
        }
        else
          BOOST_CHECK_CLOSE( qIfld_local.DOF(Itrace_DOF_map_local[k]), qIfld.DOF(Itrace_DOF_map_global[k]), tol );
//        std::cout<<qIfld_local.DOF(Itrace_DOF_map_local[k])<<","<<qIfld.DOF(Itrace_DOF_map_global[k])<<std::endl;
      }
    }
  }
#endif
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_HDG_AD_Box_Triangle_CornerCell_Target_P2 )
{
  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;

  int main_group = 0;
  int main_cell = 0;

  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};

  std::vector<int> neighbor_elem_list = {1};
  std::vector<int> neighbor_trace_list = {0};

  std::vector<int> mainBtrace_grouplist = {3,0};
  std::vector<int> mainBtrace_elemlist = {1,0};

  //Orientation of the traces around the main-cell in the local mesh, relative to the main-cell in global mesh
  //(i.e. how were the interior traces flipped/rotated in the construction of the local mesh?)
  // 1 = same orientation, -1 = reverse orientation, 0 = doesn't matter (for boundary traces)
  std::vector<int> mainTrace_global_orient = {1, 1, 1};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist,
             mainTrace_global_orient);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_HDG_AD_Box_Triangle_InteriorCell_Target_P2 )
{
  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;

  int main_group = 0;
  int main_cell = 1;

  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};

  std::vector<int> neighbor_elem_list = {0,2,4};
  std::vector<int> neighbor_trace_list = {0,1,2};

  std::vector<int> mainBtrace_grouplist = {};
  std::vector<int> mainBtrace_elemlist = {};

  //Orientation of the traces around the main-cell in the local mesh, relative to the main-cell in global mesh
  //(i.e. how were the interior traces flipped/rotated in the construction of the local mesh?)
  // 1 = same orientation, -1 = reverse orientation, 0 = doesn't matter (for boundary traces)
  std::vector<int> mainTrace_global_orient = {-1, 1,-1};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist,
             mainTrace_global_orient);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_HDG_AD_Box_Triangle_BoundaryCell_Target_P2 )
{
  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;

  int main_group = 0;
  int main_cell = 2;

  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};

  std::vector<int> neighbor_elem_list = {3,1};
  std::vector<int> neighbor_trace_list = {0,1};

  std::vector<int> mainBtrace_grouplist = {0};
  std::vector<int> mainBtrace_elemlist = {1};

  //Orientation of the traces around the main-cell in the local mesh, relative to the main-cell in global mesh
  //(i.e. how were the interior traces flipped/rotated in the construction of the local mesh?)
  // 1 = same orientation, -1 = reverse orientation, 0 = doesn't matter (for boundary traces)
  std::vector<int> mainTrace_global_orient = {1,-1, 1};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist,
             mainTrace_global_orient);
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_HDG_AD_2Triangle_LeftCell_Target_P1 )
{
  XField2D_2Triangle_X1_1Group xfld;

  int order = 1;

  int main_group = 0;
  int main_cell = 0;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0};

  std::vector<int> neighbor_elem_list = {1};
  std::vector<int> neighbor_trace_list = {0};

  std::vector<int> mainBtrace_grouplist = {0,0};
  std::vector<int> mainBtrace_elemlist = {3,0};

  //Orientation of the traces around the main-cell in the local mesh, relative to the main-cell in global mesh
  //(i.e. how were the interior traces flipped/rotated in the construction of the local mesh?)
  // 1 = same orientation, -1 = reverse orientation, 0 = doesn't matter (for boundary traces)
  std::vector<int> mainTrace_global_orient = {1, 0, 0};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist,
             mainTrace_global_orient);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_HDG_AD_2Triangle_RightCell_Target_P2 )
{
  XField2D_2Triangle_X1_1Group xfld;

  int order = 2;

  int main_group = 0;
  int main_cell = 1;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0};

  std::vector<int> neighbor_elem_list = {0};
  std::vector<int> neighbor_trace_list = {0};

  std::vector<int> mainBtrace_grouplist = {0,0};
  std::vector<int> mainBtrace_elemlist = {1,2};

  //Orientation of the traces around the main-cell in the local mesh, relative to the main-cell in global mesh
  //(i.e. how were the interior traces flipped/rotated in the construction of the local mesh?)
  // 1 = same orientation, -1 = reverse orientation, 0 = doesn't matter (for boundary traces)
  std::vector<int> mainTrace_global_orient = {-1, 0, 0};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist,
             mainTrace_global_orient);
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
