// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ElementLocalSolve_DGBR2_Triangle_Legendre_AD_btest
// testing of 2-D DG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionGradientErrorSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/Local/XField_LocalPatch.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MOESS/SolverInterface_DGBR2.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "tools/minmax.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Prints out a lot of useful information
//#define DEBUG_FLAG


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ElementLocalSolve2D_Primal_DGBR2_Triangle_Legendre_AD_test_suite )

/*
 * Computes a re-solve on a given local mesh, then checks that the solution matches exactly the original
 * As a consequence, this implies that the re-solve was on an unsplit mesh. It is basically lifted from solveLocalProblem
 *
 */

// A helper function to construct a Solver interface and test all the edges on a grid
void localExtractAndTest(const XField<PhysD2, TopoD2>& xfld, const int order, const BasisFunctionCategory basis_category,
                         const Real small_tol, const Real close_tol, const bool isSplit )
{
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD2,
                               AdvectiveFlux2D_Uniform,
                               ViscousFlux2D_Uniform,
                               Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  // Manufactured Solution Adjoints

  typedef OutputCell_SolutionGradientErrorSquared<PDEAdvectionDiffusion2D,NDSolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef BCParameters<BCVector> BCParams;
  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Pieces for the debugging
  typedef typename SolverInterfaceClass::LocalProblem LocalProblem;

  typedef typename SolverInterfaceClass::ArrayQ ArrayQ;
  typedef typename SolverInterfaceClass::VectorArrayQ VectorArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QFieldType;
  typedef FieldLift_DG_Cell< PhysD2, TopoD2, VectorArrayQ > RFieldType;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > LGFieldType;

  // A debugging class for exposing protected members
  struct debugLocalProblem : public LocalProblem
  {
    typedef LocalProblem BaseType;

    debugLocalProblem( const SolverInterfaceClass& owner, XField_Local_Base<PhysD2,TopoD2>& xfld_local,
                       LocalSolveStatus& status) : BaseType(owner,xfld_local,status) {}

    using BaseType::interface_;
    using BaseType::xfld_local_;
    using BaseType::status_;
    using BaseType::quadrule_;
    using BaseType::maxIter;
    using BaseType::BCList_local_;
    using BaseType::BCBoundaryGroups_local_;
    using BaseType::local_cellGroups_;
    using BaseType::local_interiorTraceGroups_;
    using BaseType::active_local_boundaries_;
    using BaseType::primal_local_;
    using BaseType::adjoint_local_;
    using BaseType::parambuilder_;
    using BaseType::stol;
    using BaseType::local_error_;
    using BaseType::solveLocalPrimalProblem;
  };


  ///////////////////////
  // PRIMAL
  ///////////////////////

  // PDE
  Real u = 0.3; // Dirichlet bc only work with no advection
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 1, kxy = 0;
  Real kyx = 0, kyy = 1;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Source2D_UniformGrad source(0.3*0, 0.5*0, 0.7*0);

  // Create a solution dictionary

  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function.SineSine;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  ///////////////////////
  // OUTPUT
  ///////////////////////

  NDOutputClass fcnOutput(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnOutput, {0}); // Assumes there's only 0 cellGroup
  OutputIntegrandClass errIntegrandLocal(fcnOutput, {0});

  ///////////////////////
  // BC
  ///////////////////////
  PyDict BCDirichlet, BCNeumann;

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function] = solnArgs;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.A] = 1;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0;

  PyDict BCSoln_Neumann;
  BCSoln_Neumann[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function] = solnArgs;
  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.A] = 0;
  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.B] = 1;

  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0.0;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  BCNeumann[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 0.0;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict PyBCList;
  PyBCList["BCDirichlet"] = BCSoln_Dirichlet;
  //  PyBCList["BCDirichlet0"] = BCDirichlet;
  //  PyBCList["BCDirichlet1"] = BCDirichlet;
  //  PyBCList["BCDirichlet2"] = BCDirichlet;
  //  PyBCList["BCDirichlet3"] = BCDirichlet;
  //PyBCList["BCNeumann"] = BCSoln_Neumann;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Construct group vectors for the XField
  std::vector<int> dirichlet_btracegroup_list = {};
  for (int i = 0; i < xfld.nBoundaryTraceGroups(); i++)
    dirichlet_btracegroup_list.push_back(i);

  //std::vector<int> neumann_btracegroup_list= {};

  std::vector<int> cellGroups;
  for (int i = 0; i < xfld.nCellGroups(); i++)
    cellGroups.push_back(i);

  std::vector<int> interiorTraceGroups;
  for (int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDirichlet"] = dirichlet_btracegroup_list;
  //  BCBoundaryGroups["BCDirichlet0"] = {0};
  //  BCBoundaryGroups["BCDirichlet1"] = {1};
  //  BCBoundaryGroups["BCDirichlet2"] = {2};
  //  BCBoundaryGroups["BCDirichlet3"] = {3};
  //BCBoundaryGroups["BCNeumann"]   = neumann_btracegroup_list;

  std::vector<int> mitlg_boundaries = BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups );

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Stabilization
  Real viscousEtaParameter = 2*Triangle::NTrace;
  DiscretizationDGBR2 disc( 0, viscousEtaParameter );


  ///////////////////////
  // SOLVER DICTIONARIES
  ///////////////////////


  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

#ifdef SANS_MPI
#ifndef SANS_PETSC
#error "SANS must be configured with USE_PETSC=ON if USE_MPI=ON"
#endif
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  //PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  //PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-14;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-12;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  ///////////////////////
  // SOLUTION CLASS
  ///////////////////////

  SolutionClass globalSol( xfld, pde, order, order+1,
                           basis_category, basis_category,
                           mitlg_boundaries, disc );

  ///////////////////////
  // SOLVER INTERFACE
  ///////////////////////

  const int quadOrder = -1;
  std::vector<Real> tol = {1e-11, 1e-11};
  const ResidualNormType residNormType = ResidualNorm_L2;

  SolverInterfaceClass solverInt( globalSol, residNormType, tol, quadOrder, cellGroups, interiorTraceGroups,
                                  PyBCList, BCBoundaryGroups,
                                  SolverContinuationDict, LinearSolverDict,
                                  errIntegrandGlobal);

  ///////////////////////
  // GLOBAL COMPUTATIONS
  ///////////////////////

  globalSol.setSolution(0.0);
  solverInt.solveGlobalPrimalProblem();

  typedef typename std::set<std::array<int,3>> ElemList;

  const int nTrace = Triangle::NTrace; // Number of different trace

  QuadratureOrder quadratureOrder( xfld, 2*order + 1 );

  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  ///////////////////////
  // LOOP OVER CELL GROUPS -> ELEMENTS -> TRACES
  ///////////////////////

  // split the communicator for local solves
  int comm_rank = xfld.comm()->rank();
  mpi::communicator comm_local = xfld.comm()->split(comm_rank);

  // Count the number of local solves that decrease and the number that increase
  // Most should decrease (fingers crossed)...
  int nIncreaseError = 0;
  int nDecreaseError = 0;

  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    ElemList failed_elem_list;

    for ( int elem = 0; elem < xfld.getCellGroupBase(group).nElem(); elem++)
    {
      /////////////////////////////////////////////
      // ELEMENT LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

#ifdef DEBUG_FLAG
      //      std::cout<< "==================" << std::endl;
      //      std::cout<< "group, elem = (" << group << "," << elem <<")" <<std::endl;
      //      std::cout<< "==================" << std::endl;
#endif

      // skip any ghosted cells
      // Local solve does not work with ghosted cells (ill posed)
      if ( xfld.getCellGroup<Triangle>(group).associativity(elem).rank() != comm_rank ) continue;

      ArrayQ SqErrLocal = 0, SqErrLocal_resolve = 0;
      if (!isSplit)
      {
        /////////////////////////////////////////////
        // ELEMENT LOCAL UNSPLIT CHECK
        /////////////////////////////////////////////

        //Extract the local mesh for the central tet
        XField_LocalPatchConstructor<PhysD2,Triangle> xfld_local(comm_local, connectivity, group, elem, SpaceType::Discontinuous);

        // create an unsplit patch
        XField_LocalPatch<PhysD2,Triangle> xfld_unsplit( xfld_local );

        LocalSolveStatus status(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem( solverInt, xfld_local, status);

        // make a copy of the qfld
        QFieldType  qfld_local_pre(  localProblem.primal_local_.qfld,  FieldCopy() );
        RFieldType  rfld_local_pre(  localProblem.primal_local_.rfld,  FieldCopy() );
        LGFieldType lgfld_local_pre( localProblem.primal_local_.lgfld, FieldCopy() );

        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_DGBR2( errIntegrandLocal, SqErrLocal ),
            xfld_local, (qfld_local_pre, rfld_local_pre) , quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        // Re-solve the local problem
        localProblem.solveLocalPrimalProblem( localProblem.xfld_local_, localProblem.primal_local_ );

        BOOST_CHECK( status.converged );

        // Check that the re-solve DOFS have been unchanged.
        // This is because the mesh hasn't changed, so nothing should happen
        BOOST_CHECK_EQUAL( qfld_local_pre.nDOF(), localProblem.primal_local_.qfld.nDOF() );
        BOOST_CHECK_EQUAL( rfld_local_pre.nDOF(), localProblem.primal_local_.rfld.nDOF() );
        BOOST_CHECK_EQUAL( lgfld_local_pre.nDOF(), localProblem.primal_local_.lgfld.nDOF() );
        if ( !( basis_category == BasisFunctionCategory_Hierarchical && order >= 3) )
        {
          for ( int i = 0; i < qfld_local_pre.nDOF(); i++)
            SANS_CHECK_CLOSE( qfld_local_pre.DOF(i), localProblem.primal_local_.qfld.DOF(i), 1e-7, 1e-7 );

          for ( int i = 0; i < rfld_local_pre.nDOF(); i++)
            for (int j = 0; j < PhysD2::D; j++)
              SANS_CHECK_CLOSE( rfld_local_pre.DOF(i)[j], localProblem.primal_local_.rfld.DOF(i)[j], 5e-6, 5e-6 );

          for (int i = 0; i < lgfld_local_pre.nDOF(); i++)
            SANS_CHECK_CLOSE( lgfld_local_pre.DOF(i), localProblem.primal_local_.lgfld.DOF(i), 5e-8, 5e-8 );
        }


        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_DGBR2( errIntegrandLocal, SqErrLocal_resolve ),
            xfld_local, ( localProblem.primal_local_.qfld, localProblem.primal_local_.rfld ),
            quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        // Nothing should change, but there is projection so might not be exact
        SANS_CHECK_CLOSE( SqErrLocal, SqErrLocal_resolve, small_tol, close_tol );
        if ( abs(SqErrLocal - SqErrLocal_resolve)>close_tol )
          failed_elem_list.insert( std::array<int,3>{{group,elem,-1}} );

      }
      else if (isSplit)
      {
        XField_LocalPatchConstructor<PhysD2,Triangle> xfld_unsplit_local(comm_local, connectivity, group, elem, SpaceType::Discontinuous);

        for ( int trace = 0; trace < nTrace; trace++)
        {

          /////////////////////////////////////////////
          // ELEMENT LOCAL SPLIT CHECK
          /////////////////////////////////////////////

          XField_LocalPatch<PhysD2,Triangle> xfld_local_split( xfld_unsplit_local, ElementSplitType::Edge, trace );

          LocalSolveStatus status_split(false,10);
          // A debugging version of the Local Problem class
          // Exposes protected members
          debugLocalProblem localProblem_split( solverInt, xfld_local_split, status_split);

          QFieldType qfld_local_split_pre( localProblem_split.primal_local_.qfld, FieldCopy() );
          RFieldType rfld_local_split_pre( localProblem_split.primal_local_.rfld, FieldCopy() );

          QuadratureOrder quadratureOrder_local( xfld_local_split, 2*order + 1 );

          // Compute square error on split mesh before local solve
          IntegrateCellGroups<TopoD2>::integrate(
              FunctionalCell_DGBR2( errIntegrandLocal, SqErrLocal ),
              xfld_local_split, ( qfld_local_split_pre, rfld_local_split_pre ),
              quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

          localProblem_split.solveLocalPrimalProblem( localProblem_split.xfld_local_, localProblem_split.primal_local_ );

          BOOST_CHECK( status_split.converged );

          IntegrateCellGroups<TopoD2>::integrate(
              FunctionalCell_DGBR2( errIntegrandLocal, SqErrLocal_resolve ),
              xfld_local_split, ( localProblem_split.primal_local_.qfld, localProblem_split.primal_local_.rfld ),
              quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

          //BOOST_CHECK_GE( SqErrLocal, SqErrLocal_resolve );
          if (SqErrLocal > SqErrLocal_resolve)
            nDecreaseError++;
          else
            nIncreaseError++;

          if ( SqErrLocal < SqErrLocal_resolve )
          {
#ifdef DEBUG_FLAG
            std::cout<< "SqErrLocal - SqErrLocal_resolve = " << SqErrLocal -  SqErrLocal_resolve << std::endl;
#endif
            failed_elem_list.insert( std::array<int,3>{{group,elem,trace}} );
          }
        }

      }

    }
#ifdef DEBUG_FLAG
    std::cout<< "failed_elems_traces = ";
    for (auto it = failed_elem_list.begin(); it != failed_elem_list.end(); ++it)
      std::cout<< "(" << (*it)[0] << "," << (*it)[1] << "," << (*it)[2] << "), ";
    std::cout << std::endl;
#endif
  }

  // make sure more decrease than increase
  BOOST_CHECK_GE( nDecreaseError, nIncreaseError );
}

// Full Tests

// UNSPLIT TESTS - Nothing should change
#if 1

// -----------------------------------------------------
//  Legendre
// -----------------------------------------------------

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Legendre_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world;

  int ii = 3, jj = ii, order = 1;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  //  std::cout<< "==========" << std::endl;
  //  std::cout<< "Union Jack" << std::endl;
  //  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Legendre_P2 )
{
  mpi::communicator world;
  mpi::communicator comm = world;

  int ii = 3, jj = ii, order = 2;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Legendre_P3 )
{
  mpi::communicator world;
  mpi::communicator comm = world;

  int ii = 3, jj = ii, order = 3;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#endif

// =====================================================
// SPLIT TESTS - Check that energy norm error (for Poisson) decreases
// =====================================================

/*
 * These should be uncommented when a local solve that always reduces the energy norm is devised -- Hugh
 */
#if 1

// -----------------------------------------------------
//  Legendre
// -----------------------------------------------------

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Legendre_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world;

  int ii = 3, jj = ii, order = 1;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Legendre_P2 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split( world.rank() < 2 );

  if ( world.rank() >= 2 ) return;

  int ii = 2, jj = ii, order = 2;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Legendre_P3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split( world.rank() < 2 );

  if ( world.rank() >= 2 ) return;

  int ii = 2, jj = ii, order = 3;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
