// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// EdgeLocalSolve_Galerkin_Triangle_Hierarchical_AD_btest
// testing of 2-D DG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/GasModel.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"

#include "tools/minmax.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Prints out a lot of useful information
//#define DEBUG_FLAG


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( EdgeLocalSolve2D_Dual_Galerkin_Triangle_Hierarchical_Euler_test_suite )

/*
* Computes a re-solve on a given local mesh, then checks that the solution matches exactly the original
* As a consequence, this implies that the re-solve was on an unsplit mesh. It is basically lifted from solveLocalProblem
*
*/

// A helper function to construct a Solver interface and test all the edges on a grid
void localExtractAndTest(const XField<PhysD2, TopoD2>& xfld, const int order, const BasisFunctionCategory basis_category,
                         const Real small_tol, const Real close_tol, const bool isSplit )
{
  typedef QTypePrimitiveRhoPressure QType;

  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;
  typedef BCParameters<BCVector> BCParams;

  // Manufactured Solution Adjoints

  typedef OutputEuler2D_EntropyErrorSquare<NDPDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef BCParameters<BCVector> BCParams;
  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Pieces for the debugging
  typedef typename SolverInterfaceClass::LocalProblem LocalProblem;

  typedef typename SolverInterfaceClass::ArrayQ ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QFieldType;
  typedef Field_CG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > LGFieldType;

  // A debugging class for exposing protected members
  struct debugLocalProblem : public LocalProblem
  {
    typedef LocalProblem BaseType;

    debugLocalProblem( const SolverInterfaceClass& owner, XField_Local_Base<PhysD2,TopoD2>& xfld_local,
                       LocalSolveStatus& status) : BaseType(owner,xfld_local,status)
    {
//      std::cout<< "in Local Problem active_boundaries_ = ";
//      for (auto it = active_local_boundaries_.begin(); it != active_local_boundaries_.end(); ++it)
//        std::cout<< *it << ", ";
//      std::cout << std::endl;
    }

    using BaseType::interface_;
    using BaseType::xfld_local_;
    using BaseType::status_;
    using BaseType::quadrule_;
    using BaseType::maxIter;
    using BaseType::BCList_local_;
    using BaseType::BCBoundaryGroups_local_;
    using BaseType::local_cellGroups_;
    using BaseType::local_interiorTraceGroups_;
    using BaseType::active_local_boundaries_;
    using BaseType::primal_local_;
    using BaseType::adjoint_local_;
    using BaseType::parambuilder_;
    using BaseType::stol;
    using BaseType::local_error_;
    using BaseType::solveLocalPrimalProblem;
  };

  ///////////////////////
  // PRIMAL
  ///////////////////////

  // PDE
  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);

  // Initial Condition
  //solution at inlet is (1, 0.1, 0, 1);
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(1, 0.1, 0, 1) );

  // BC
  // Create a BC dictionary
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;

  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = 0.0;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = 3.505;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = 0;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = 1.0;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2};
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {3};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // std::cout<< "active_boundaries = ";
  // for (auto it = active_boundaries.begin(); it != active_boundaries.end(); ++it)
  //   std::cout<< *it << ", ";
  // std::cout << std::endl;

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Output functional
  Real sExact = 0.0;
  NDOutputClass outEntropyError( pde, sExact );
  OutputIntegrandClass outputIntegrand( outEntropyError, {0} );

  // Stabilization
  const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, order+1);

  ///////////////////////
  // SOLVER DICTIONARIES
  ///////////////////////

  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 10;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  ///////////////////////
  // SOLUTION CLASS
  ///////////////////////

  SolutionClass globalSol( xfld, pde, order, order+1,
                           basis_category, basis_category,
                           active_boundaries, stab );

  ///////////////////////
  // SOLVER INTERFACE
  ///////////////////////

  const int quadOrder = 3*(order+1);
  std::vector<Real> tol = {1e-11, 1e-11};
  const ResidualNormType residNormType = ResidualNorm_L2;

  SolverInterfaceClass solverInt( globalSol, residNormType, tol, quadOrder, cellGroups, interiorTraceGroups,
                                  PyBCList, BCBoundaryGroups,
                                  SolverContinuationDict, LinearSolverDict,
                                  outputIntegrand);

  ///////////////////////
  // GLOBAL COMPUTATIONS
  ///////////////////////

  globalSol.setSolution(q0);
  solverInt.solveGlobalPrimalProblem();

  const QFieldType& qfld = globalSol.primal.qfld;

  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename QFieldType::template FieldCellGroupType<Triangle> QFieldCellGroupType;

  const int nEdge = Triangle::NEdge; // Number of different edges

  typedef typename std::set<std::array<int,Line::NNode>> EdgeList;

  QuadratureOrder quadratureOrder( xfld, 2*order + 1 );

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  ///////////////////////
  // LOOP OVER CELL GROUPS ->
  ///////////////////////
  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Triangle>::EdgeNodes;
  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    EdgeList edge_list, failed_edge_list;

    // edge_list is cleared with each loop over the groups
    const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Triangle>(group);
    const QFieldCellGroupType& qfldCellGroup = qfld.template getCellGroup<Triangle>(group);

    const int nElem = xfldCellGroup.nElem();

    ///////////////////////
    // EDGE LIST EXTRACTION
    ///////////////////////

    for ( int elem = 0; elem < nElem; elem++ )
    {
      std::array<int,Triangle::NNode> xnodeMap;
      std::array<int,Triangle::NNode> qnodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping( xnodeMap.data(), xnodeMap.size() );
      qfldCellGroup.associativity(elem).getNodeGlobalMapping( qnodeMap.data(), qnodeMap.size() );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,2> xEdge;
        xEdge[0] = xnodeMap[ EdgeNodes[edge][0] ];
        xEdge[1] = xnodeMap[ EdgeNodes[edge][1] ];

        std::array<int,2> qEdge;
        qEdge[0] = qnodeMap[ EdgeNodes[edge][0] ];
        qEdge[1] = qnodeMap[ EdgeNodes[edge][1] ];

        if (xfld.local2nativeDOFmap(xEdge[0]) > xfld.local2nativeDOFmap(xEdge[1]))
        {
          std::swap(xEdge[0], xEdge[1]);
          std::swap(qEdge[0], qEdge[1]);
        }

        // only consider edges where the primary node is possessed
        if (qEdge[0] >= qfld.nDOFpossessed()) continue;

        edge_list.insert(xEdge);
      }
    }

    /////////////////////////////////////////////
    // EDGE LOCAL MESH CONSTRUCT
    /////////////////////////////////////////////

    const XField_CellToTrace<PhysD2,TopoD2> xfld_connectivity(xfld);
    const Field_NodalView nodalView(xfld, {group});

    typedef typename Field_NodalView::IndexVector IndexVector;

    for ( const std::array<int,Line::NNode>& edge : edge_list )
    {

      /////////////////////////////////////////////
      // EDGE LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

      IndexVector nodeGroup0, nodeGroup1;
      nodeGroup0 = nodalView.getCellList( edge[0] );
      nodeGroup1 = nodalView.getCellList( edge[1] );
//      std::cout << "===============" <<std::endl;

      // find the interesction of the two sets
      std::set<GroupElemIndex> attachedCells;
      typename std::set<GroupElemIndex>::iterator attachedCells_it;
      std::set_intersection( nodeGroup0.begin(), nodeGroup0.end(), nodeGroup1.begin(), nodeGroup1.end(),
                             std::inserter( attachedCells, attachedCells.begin() ) );

#ifdef DEBUG_FLAG
      std::cout<< "==================" << std::endl;
      std::cout<< "nodes = (" << nodes.first << "," << nodes.second <<")" <<std::endl;
      std::cout<< "==================" << std::endl;
#endif

      Real outLocal = 0, outLocal_resolve = 0;
      if (!isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL UNSPLIT CHECK
        /////////////////////////////////////////////

        XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,xfld_connectivity,edge,SpaceType::Continuous,&nodalView);
        XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

        LocalSolveStatus status(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem( solverInt, xfld_local, status);

        // make a copy of the qfld
        QFieldType  qfld_local_pre(  localProblem.primal_local_.qfld,  FieldCopy() );
        LGFieldType lgfld_local_pre( localProblem.primal_local_.lgfld, FieldCopy() );

        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( outputIntegrand, outLocal ),
            xfld_local, qfld_local_pre, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

//        std::cout<< "unsplit" <<std::endl;

        // Re-solve the local problem
        localProblem.solveLocalPrimalProblem( localProblem.xfld_local_, localProblem.primal_local_ );

        BOOST_CHECK( status.converged );

//        output_Tecplot( localProblem.primal_local_.qfld, "tmp/qfld_local_post.plt");

//        std::cout << "qfld DOF post = " ;
//        for (int i = 0; i < localProblem.primal_local_.qfld.nDOF(); i++)
//          std::cout << localProblem.primal_local_.qfld.DOF(i) << ", ";
//        std::cout << std::endl;

        // Check that the re-solve DOFS have been unchanged.
        // This is because the mesh hasn't changed, so nothing should happen
        BOOST_CHECK_EQUAL( qfld_local_pre.nDOF(), localProblem.primal_local_.qfld.nDOF() );
        BOOST_CHECK_EQUAL( lgfld_local_pre.nDOF(), localProblem.primal_local_.lgfld.nDOF() );
        if ( !( basis_category == BasisFunctionCategory_Hierarchical && order >= 3) )
        {
          for ( int i = 0; i < qfld_local_pre.nDOF(); i++)
            for (int d = 0; d < 4; d++)
            SANS_CHECK_CLOSE( qfld_local_pre.DOF(i)[d], localProblem.primal_local_.qfld.DOF(i)[d], small_tol, close_tol );

          for (int i = 0; i < lgfld_local_pre.nDOF(); i++)
            for (int d = 0; d < 4; d++)
            SANS_CHECK_CLOSE( lgfld_local_pre.DOF(i)[d], localProblem.primal_local_.lgfld.DOF(i)[d], small_tol, close_tol );
        }


        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( outputIntegrand, outLocal_resolve ),
            xfld_local, localProblem.primal_local_.qfld, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        // Nothing should change, but there is projection so might not be exact
        SANS_CHECK_CLOSE( outLocal, outLocal_resolve, small_tol, close_tol );
        if ( abs(outLocal - outLocal_resolve)>close_tol )
          failed_edge_list.insert( edge );

//        localProblem.primal_local_.qfld.dump();
//        localProblem.primal_local_.lgfld.dump();
      }
      else if (isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL SPLIT CHECK
        /////////////////////////////////////////////

        XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,xfld_connectivity,edge,SpaceType::Continuous,&nodalView);

        LocalSolveStatus status_split(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem_split( solverInt, xfld_local, status_split);

        QFieldType qfld_local_split_pre( localProblem_split.primal_local_.qfld, FieldCopy() );

        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( outputIntegrand, outLocal ),
            xfld_local, qfld_local_split_pre, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        localProblem_split.solveLocalPrimalProblem( localProblem_split.xfld_local_, localProblem_split.primal_local_ );

        BOOST_CHECK( status_split.converged );

        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( outputIntegrand, outLocal_resolve ),
            xfld_local, localProblem_split.primal_local_.qfld, quadratureOrder_local.cellOrders.data(),
                                                                     quadratureOrder_local.cellOrders.size() );

        BOOST_CHECK( outLocal - outLocal_resolve + small_tol >= 0 );
        if ( (outLocal - outLocal_resolve  + small_tol) < 0 )
        {
          std::cout<< "SqErrLocal - SqErrLocal_resolve = " << outLocal -  outLocal_resolve << std::endl;
          failed_edge_list.insert( edge );
        }
//        std::cout<< "lgfld.nDOF() = " << localProblem_split.primal_local_.lgfld.nDOF() << std::endl;
      }

//      /////////////////////////////////////////////
//      // EDGE LOCAL ESTIMATION CHECK
//      /////////////////////////////////////////////
//
//      // Check that the computed error matches the original. The solution should not have changed
//      for (attachedCells_it = attachedCells.begin(); attachedCells_it != attachedCells.end(); ++attachedCells_it)
//      {
//        // only handle elements in current group, others will get counted on the loop with that group
//        if (attachedCells_it->group == group ) // if the cell is in the current group
//        {
//         // Extract the
//
//          /////////////////////////////////////////////
//          // EDGE LOCAL ERROR ESTIMATE CHECK
//          /////////////////////////////////////////////
//        }
//      }
      std::cout << "-----" << std::endl;
      return;
    }
#ifdef DEBUG_FLAG
    std::cout<< "failed_edges = ";
    for (auto it = failed_edge_list.begin(); it != failed_edge_list.end(); ++it)
      std::cout<< "(" << (*it)[0] << "," << (*it)[1] << "), ";
    std::cout << std::endl;
#endif
  }


}

// Basic Tests

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource2_Hierarchical_P1 )
{
  int jj = pow(2,1), ii = 3*jj, order = 1;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 5e-9, close_tol = 5e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 0

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource4_Hierarchical_P1 )
{
  int jj = pow(2,2), ii = 3*jj, order = 1;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 5e-9, close_tol = 5e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource8_Hierarchical_P1 )
{
  int jj = pow(2,3), ii = 3*jj, order = 1;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 2e-8, close_tol = 2e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif



#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource2_Hierarchical_P2 )
{
  int jj = pow(2,1), ii = 3*jj, order = 2;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 2e-6, close_tol = 2e-6;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource4_Hierarchical_P2 )
{
  int jj = pow(2,2), ii = 3*jj, order = 2;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 5e-5, close_tol = 5e-5;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource2_Hierarchical_P1_Split )
{
  int jj = pow(2,1), ii = 3*jj, order = 1;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 5e-6, close_tol = 5e-6;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource4_Hierarchical_P1_Split )
{
  int jj = pow(2,2), ii = 3*jj, order = 1;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 5e-7, close_tol = 5e-7;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource8_Hierarchical_P1_Split )
{
  int jj = pow(2,3), ii = 3*jj, order = 1;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 5e-7, close_tol = 5e-7;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif



#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource2_Hierarchical_P2_Split )
{
  int jj = pow(2,1), ii = 3*jj, order = 2;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 3e-6, close_tol = 3e-6;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource4_Hierarchical_P2_Split )
{
  int jj = pow(2,2), ii = 3*jj, order = 2;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 5e-5, close_tol = 5e-5;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif




#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_CubicSource8_Hierarchical_P2 )
{
  int jj = pow(2,3), ii = 3*jj, order = 2;
  Real tau = 0.1;
  XField2D_CubicSourceBump_Xq xfld(ii,jj, tau, 1);
  const Real small_tol = 2e-6, close_tol = 2e-6;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif



#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_Euler_1Box_Triangle_Hierarchical_Split_P1 )
{
  int ii = 2, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

// Full Tests
#if 0

// UNSPLIT TESTS - Nothing should change
#if 1

// -----------------------------------------------------
//  Hierarchical
// -----------------------------------------------------

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_P1 )
{
  int ii = 2, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_P2 )
{
  int ii = 2, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_P3 )
{
  int ii = 2, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Triangle_Hierarchical_P1 )
{
  int ii = 4, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Triangle_Hierarchical_P2 )
{
  int ii = 4, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Triangle_Hierarchical_P3 )
{
  int ii = 4, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 5e-9, close_tol = 5e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Triangle_Hierarchical_P1 )
{
  int ii = 6, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Triangle_Hierarchical_P2 )
{
  int ii = 6, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Triangle_Hierarchical_P3 )
{
  int ii = 6, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 5e-9, close_tol = 5e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Triangle_Hierarchical_P1 )
{
  int ii = 8, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Triangle_Hierarchical_P2 )
{
  int ii = 8, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Triangle_Hierarchical_P3 )
{
  int ii = 8, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif


// -----------------------------------------------------
//  Lagrange
// -----------------------------------------------------

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Triangle_Lagrange_P1 )
{
  int ii = 2, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Triangle_Lagrange_P2 )
{
  int ii = 2, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Triangle_Lagrange_P3 )
{
  int ii = 2, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-8, close_tol = 1e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Triangle_Lagrange_P1 )
{
  int ii = 4, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Triangle_Lagrange_P2 )
{
  int ii = 4, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Triangle_Lagrange_P3 )
{
  int ii = 4, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 2e-9, close_tol = 2e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Triangle_Lagrange_P1 )
{
  int ii = 6, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Triangle_Lagrange_P2 )
{
  int ii = 6, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Triangle_Lagrange_P3 )
{
  int ii = 6, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 5e-9, close_tol = 5e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Triangle_Lagrange_P1 )
{
  int ii = 8, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Triangle_Lagrange_P2 )
{
  int ii = 8, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Triangle_Lagrange_P3 )
{
  int ii = 8, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif



#endif

// =====================================================
// SPLIT TESTS - Check that energy norm error (for Poisson) decreases
// =====================================================

#if 0

// -----------------------------------------------------
//  Hierarchical
// -----------------------------------------------------

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Split_Triangle_Hierarchical_P1 )
{
  int ii = 2, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Split_Triangle_Hierarchical_P2 )
{
  int ii = 2, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Split_Triangle_Hierarchical_P3 )
{
  int ii = 2, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Split_Triangle_Hierarchical_P1 )
{
  int ii = 4, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Split_Triangle_Hierarchical_P2 )
{
  int ii = 4, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Split_Triangle_Hierarchical_P3 )
{
  int ii = 4, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Split_Triangle_Hierarchical_P1 )
{
  int ii = 6, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Split_Triangle_Hierarchical_P2 )
{
  int ii = 6, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Split_Triangle_Hierarchical_P3 )
{
  int ii = 6, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Split_Triangle_Hierarchical_P1 )
{
  int ii = 8, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Split_Triangle_Hierarchical_P2 )
{
  int ii = 8, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Split_Triangle_Hierarchical_P3 )
{
  int ii = 8, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif


// -----------------------------------------------------
//  Lagrange
// -----------------------------------------------------

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Split_Triangle_Lagrange_P1 )
{
  int ii = 2, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Split_Triangle_Lagrange_P2 )
{
  int ii = 2, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Split_Triangle_Lagrange_P3 )
{
  int ii = 2, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Split_Triangle_Lagrange_P1 )
{
  int ii = 4, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Split_Triangle_Lagrange_P2 )
{
  int ii = 4, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_4Box_Split_Triangle_Lagrange_P3 )
{
  int ii = 4, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Split_Triangle_Lagrange_P1 )
{
  int ii = 6, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Split_Triangle_Lagrange_P2 )
{
  int ii = 6, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_9Box_Split_Triangle_Lagrange_P3 )
{
  int ii = 6, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Split_Triangle_Lagrange_P1 )
{
  int ii = 8, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Split_Triangle_Lagrange_P2 )
{
  int ii = 8, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_16Box_Split_Triangle_Lagrange_P3 )
{
  int ii = 8, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif



#endif
// Other Tests


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_Split_P1 )
{
  std::cout << "///////" << std::endl;
  std::cout << "P1 test" << std::endl;
  std::cout << "///////" << std::endl;
  int ii = 1;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-9, close_tol = 1e-9;

  bool split_test = true;
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_Split_P2 )
{
  std::cout << "///////" << std::endl;
  std::cout << "P1 test" << std::endl;
  std::cout << "///////" << std::endl;
  int ii = 1;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 1e-9, close_tol = 1e-9;

  bool split_test = true;
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_Split_P3 )
{
  std::cout << "///////" << std::endl;
  std::cout << "P1 test" << std::endl;
  std::cout << "///////" << std::endl;
  int ii = 1;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-9, close_tol = 1e-9;

  bool split_test = true;
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#endif

#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
