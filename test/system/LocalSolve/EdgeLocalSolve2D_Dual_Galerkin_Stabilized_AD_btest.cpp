// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// EdgeLocalSolve_Galerkin_Triangle_Hierarchical_AD_btest
// testing of 2-D DG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionGradientErrorSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"


#include "tools/minmax.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Prints out a lot of useful information
// #define DEBUG_FLAG


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( EdgeLocalSolve2D_Dual_Galerkin_Stabilized_AD_test_suite )

/*
* Computes a re-solve on a given local mesh, then checks that the solution matches exactly the original
* As a consequence, this implies that the re-solve was on an unsplit mesh. It is basically lifted from solveLocalProblem
*
*/

// A helper function to construct a Solver interface and test all the edges on a grid
void localExtractAndTest(const XField<PhysD2, TopoD2>& xfld, const int order, const BasisFunctionCategory basis_category,
                         const Real small_tol, const Real close_tol, const bool isSplit )
{
  typedef ScalarFunction2D_LaplacianUnitForcing SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  // Manufactured Solution Adjoints

  typedef OutputCell_SolutionErrorSquared<PDEAdvectionDiffusion2D,NDSolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef BCParameters<BCVector> BCParams;
  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Pieces for the debugging
  typedef typename SolverInterfaceClass::LocalProblem LocalProblem;

  typedef typename SolverInterfaceClass::ArrayQ ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QFieldType;
  typedef Field_CG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > LGFieldType;

  // A debugging class for exposing protected members
  struct debugLocalProblem : public LocalProblem
  {
    typedef LocalProblem BaseType;

    debugLocalProblem( const SolverInterfaceClass& owner, XField_Local_Base<PhysD2,TopoD2>& xfld_local,
                       LocalSolveStatus& status) : BaseType(owner,xfld_local,status) {}

    using BaseType::interface_;
    using BaseType::xfld_local_;
    using BaseType::status_;
    using BaseType::quadrule_;
    using BaseType::maxIter;
    using BaseType::BCList_local_;
    using BaseType::BCBoundaryGroups_local_;
    using BaseType::local_cellGroups_;
    using BaseType::local_interiorTraceGroups_;
    using BaseType::active_local_boundaries_;
    using BaseType::primal_local_;
    using BaseType::adjoint_local_;
    using BaseType::parambuilder_;
    using BaseType::stol;
    using BaseType::local_error_;
    using BaseType::solveLocalPrimalProblem;
  };


  ///////////////////////
  // PRIMAL
  ///////////////////////

  // PDE
  Real u = 1.1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real nu = 0.1;
  Real kxx = nu;
  Real kxy = 0;
  Real kyx = 0;
  Real kyy = nu;
  ViscousFlux2D_Uniform visc(kxx, kxy, kyx, kyy);

  Source2D_UniformGrad source(0.3*0, 0.5*0, 0.7*0);

  // Create a solution dictionary
  PyDict solnArgs;
  // solnArgs[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
  //     BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.DoubleBL;
  // solnArgs[SolutionExact::ParamsType::params.a] = u;
  // solnArgs[SolutionExact::ParamsType::params.b] = v;
  // solnArgs[SolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_Const<PDEAdvectionDiffusion2D> ForcingType;
  Real a = 1.0;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(a));

  NDPDEClass pde( adv, visc, source, forcingptr );

  ///////////////////////
  // OUTPUT
  ///////////////////////

  NDOutputClass fcnOutput(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnOutput, {0}); // Assumes there's only 0 cellGroup
  OutputIntegrandClass errIntegrandLocal(fcnOutput, {0});

  ///////////////////////
  // BC
  ///////////////////////

  PyDict BCDirichlet;
  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeDirichlet_mitStateParam>::params.qB] = 0;

  PyDict PyBCList;
  PyBCList["BCDirichlet"] = BCDirichlet;
//  PyBCList["BCDirichlet0"] = BCDirichlet;
//  PyBCList["BCDirichlet1"] = BCDirichlet;
//  PyBCList["BCDirichlet2"] = BCDirichlet;
//  PyBCList["BCDirichlet3"] = BCDirichlet;
//  PyBCList["BCNeumann"] = BCSoln_Neumann;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Construct group vectors for the XField
  std::vector<int> dirichlet_btracegroup_list = {};
  for (int i = 0; i < xfld.nBoundaryTraceGroups(); i++)
    dirichlet_btracegroup_list.push_back(i);

  std::vector<int> neumann_btracegroup_list= {};

  std::vector<int> cellGroups;
  for (int i = 0; i < xfld.nCellGroups(); i++)
    cellGroups.push_back(i);

  std::vector<int> interiorTraceGroups;
  for (int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDirichlet"] = dirichlet_btracegroup_list;

  std::vector<int> mitLG_boundaries = BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups );

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Stabilization
  const StabilizationMatrix stab(StabilizationType::GLS, TauType::Glasby, order+1);

  ///////////////////////
  // SOLVER DICTIONARIES
  ///////////////////////


  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

#ifdef SANS_MPI
#ifndef SANS_PETSC
#error "SANS must be configured with USE_PETSC=ON if USE_MPI=ON"
#endif
  PyDict PreconditionerDict;
  PyDict PreconditionerILU;
  PyDict PETScDict;

  PreconditionerILU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.ILU;
  PreconditionerILU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;
  PreconditionerILU[SLA::PreconditionerILUParam::params.FillLevel] = 0;
  PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  //PreconditionerILU[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.MDF;
  //PreconditionerILU[SLA::PreconditionerILUParam::params.MDFOuterBlockSize] = (order+1)*(order+2)*(order+3)/6; //elemDOF for p=order

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
  PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerILU;

  PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-14;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-13;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 1500;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = PETScDict;
#else
  PyDict UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;
#endif

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  ///////////////////////
  // SOLUTION CLASS
  ///////////////////////

  SolutionClass globalSol( xfld, pde, order, order+1,
                           basis_category, basis_category,
                           mitLG_boundaries, stab );

  ///////////////////////
  // SOLVER INTERFACE
  ///////////////////////

  const int quadOrder = 2*(order+1);
  std::vector<Real> tol = {1e-11, 1e-11};
  const ResidualNormType residNormType = ResidualNorm_L2;

  SolverInterfaceClass solverInt( globalSol, residNormType, tol, quadOrder, cellGroups, interiorTraceGroups,
                                  PyBCList, BCBoundaryGroups,
                                  SolverContinuationDict, LinearSolverDict,
                                  errIntegrandGlobal);

  ///////////////////////
  // GLOBAL COMPUTATIONS
  ///////////////////////

  globalSol.setSolution(0.0);
  solverInt.solveGlobalPrimalProblem();

  const QFieldType& qfld = globalSol.primal.qfld;

  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename QFieldType::template FieldCellGroupType<Triangle> QFieldCellGroupType;

  const int nEdge = Triangle::NEdge; // Number of different edges

  typedef typename std::set<std::array<int,Line::NNode>> EdgeList;

  QuadratureOrder quadratureOrder( xfld, 2*order + 1 );

  // Count the number of local solves that decrease and the number that increase
  // Most should decrease (fingers crossed)...
  int nIncreaseError = 0;
  int nDecreaseError = 0;

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  ///////////////////////
  // LOOP OVER CELL GROUPS ->
  ///////////////////////
  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Triangle>::EdgeNodes;
  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    EdgeList edge_list, failed_edge_list;

    // edge_list is cleared with each loop over the groups
    const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Triangle>(group);
    const QFieldCellGroupType& qfldCellGroup = qfld.template getCellGroup<Triangle>(group);

    const int nElem = xfldCellGroup.nElem();

    ///////////////////////
    // EDGE LIST EXTRACTION
    ///////////////////////

    for ( int elem = 0; elem < nElem; elem++ )
    {
      std::array<int,Triangle::NNode> xnodeMap;
      std::array<int,Triangle::NNode> qnodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping( xnodeMap.data(), xnodeMap.size() );
      qfldCellGroup.associativity(elem).getNodeGlobalMapping( qnodeMap.data(), qnodeMap.size() );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,2> xEdge;
        xEdge[0] = xnodeMap[ EdgeNodes[edge][0] ];
        xEdge[1] = xnodeMap[ EdgeNodes[edge][1] ];

        std::array<int,2> qEdge;
        qEdge[0] = qnodeMap[ EdgeNodes[edge][0] ];
        qEdge[1] = qnodeMap[ EdgeNodes[edge][1] ];

        if (xfld.local2nativeDOFmap(xEdge[0]) > xfld.local2nativeDOFmap(xEdge[1]))
        {
          std::swap(xEdge[0], xEdge[1]);
          std::swap(qEdge[0], qEdge[1]);
        }

        // only consider edges where the primary node is possessed
        if (qEdge[0] >= qfld.nDOFpossessed()) continue;

        edge_list.insert(xEdge);
      }
    }

    /////////////////////////////////////////////
    // EDGE LOCAL MESH CONSTRUCT
    /////////////////////////////////////////////

    const XField_CellToTrace<PhysD2,TopoD2> xfld_connectivity(xfld);
    const Field_NodalView nodalView(xfld, {group});

    typedef typename Field_NodalView::IndexVector IndexVector;

    for ( const std::array<int,Line::NNode>& edge : edge_list )
    {

      /////////////////////////////////////////////
      // EDGE LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

      IndexVector nodeGroup0, nodeGroup1;
      nodeGroup0 = nodalView.getCellList( edge[0] );
      nodeGroup1 = nodalView.getCellList( edge[1] );
//      std::cout << "===============" <<std::endl;

      // find the intersection of the two sets
      std::set<GroupElemIndex> attachedCells;
      std::set_intersection( nodeGroup0.begin(), nodeGroup0.end(), nodeGroup1.begin(), nodeGroup1.end(),
                             std::inserter( attachedCells, attachedCells.begin() ) );

#ifdef DEBUG_FLAG
      std::cout<< "==================" << std::endl;
      std::cout<< "nodes = (" << nodes.first << "," << nodes.second <<")" <<std::endl;
      std::cout<< "==================" << std::endl;
#endif

      ArrayQ SqErrLocal = 0, SqErrLocal_resolve = 0;
      if (!isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL UNSPLIT CHECK
        /////////////////////////////////////////////

        XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,xfld_connectivity,edge,SpaceType::Continuous,&nodalView);
        XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

        LocalSolveStatus status(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem( solverInt, xfld_local, status);

//        output_Tecplot( localProblem.primal_local_.qfld, "tmp/qfld_local.plt");

//        std::cout << "qfld DOF pre = " ;
//        for (int i = 0; i < localProblem.primal_local_.qfld.nDOF(); i++)
//          std::cout << localProblem.primal_local_.qfld.DOF(i) << ", ";
//        std::cout << std::endl;

        // make a copy of the qfld
        QFieldType  qfld_local_pre(  localProblem.primal_local_.qfld,  FieldCopy() );
        LGFieldType lgfld_local_pre( localProblem.primal_local_.lgfld, FieldCopy() );

        QuadratureOrder quadratureOrder_local( xfld_local, 3*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal ),
            xfld_local, qfld_local_pre, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

//        std::cout<< "unsplit" <<std::endl;

        // Re-solve the local problem
        localProblem.solveLocalPrimalProblem( localProblem.xfld_local_, localProblem.primal_local_ );

        BOOST_CHECK( status.converged );

//        output_Tecplot( localProblem.primal_local_.qfld, "tmp/qfld_local_post.plt");

//        std::cout << "qfld DOF post = " ;
//        for (int i = 0; i < localProblem.primal_local_.qfld.nDOF(); i++)
//          std::cout << localProblem.primal_local_.qfld.DOF(i) << ", ";
//        std::cout << std::endl;

        // Check that the re-solve DOFS have been unchanged.
        // This is because the mesh hasn't changed, so nothing should happen
        BOOST_CHECK_EQUAL( qfld_local_pre.nDOF(), localProblem.primal_local_.qfld.nDOF() );
        BOOST_CHECK_EQUAL( lgfld_local_pre.nDOF(), localProblem.primal_local_.lgfld.nDOF() );
        if ( !( basis_category == BasisFunctionCategory_Hierarchical && order >= 3) )
        {
          for ( int i = 0; i < qfld_local_pre.nDOF(); i++)
            SANS_CHECK_CLOSE( qfld_local_pre.DOF(i), localProblem.primal_local_.qfld.DOF(i), small_tol, close_tol );

          for (int i = 0; i < lgfld_local_pre.nDOF(); i++)
            SANS_CHECK_CLOSE( lgfld_local_pre.DOF(i), localProblem.primal_local_.lgfld.DOF(i), small_tol, close_tol );
        }


        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal_resolve ),
            xfld_local, localProblem.primal_local_.qfld, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        // Nothing should change, but there is projection so might not be exact
        if ( !( basis_category == BasisFunctionCategory_Hierarchical && order >= 3) )
        {
          SANS_CHECK_CLOSE( SqErrLocal, SqErrLocal_resolve, small_tol, close_tol );
        }

        if ( abs(SqErrLocal - SqErrLocal_resolve)>close_tol )
          failed_edge_list.insert( edge );

//        localProblem.primal_local_.qfld.dump();
//        localProblem.primal_local_.lgfld.dump();
      }
      else if (isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL SPLIT CHECK
        /////////////////////////////////////////////

        XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,xfld_connectivity,edge,SpaceType::Continuous,&nodalView);

        LocalSolveStatus status_split(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem_split( solverInt, xfld_local, status_split);

        QFieldType qfld_local_split_pre( localProblem_split.primal_local_.qfld, FieldCopy() );
        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal ),
            xfld_local, qfld_local_split_pre, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

//        std::cout<< ", split" <<std::endl;
        localProblem_split.solveLocalPrimalProblem( localProblem_split.xfld_local_, localProblem_split.primal_local_ );

        BOOST_CHECK( status_split.converged );

//        output_Tecplot( localProblem_split.primal_local_.qfld, "tmp/qfld_local_post.plt");

//        std::cout << "qfld DOF post = " ;
//        for (int i = 0; i < localProblem_split.primal_local.qfld.nDOF(); i++)
//          std::cout << localProblem_split.primal_local.qfld.DOF(i) << ", ";
//        std::cout << std::endl;

        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal_resolve ),
            xfld_local, localProblem_split.primal_local_.qfld, quadratureOrder_local.cellOrders.data(),
                                                                     quadratureOrder_local.cellOrders.size() );

        Real growth = 1.1;
        //BOOST_CHECK_GE( growth*SqErrLocal, SqErrLocal_resolve );
        if (growth*SqErrLocal > SqErrLocal_resolve && SqErrLocal > small_tol)
          nDecreaseError++;
        else
          nIncreaseError++;

        if ( growth*SqErrLocal < SqErrLocal_resolve && SqErrLocal > small_tol )
        {
#ifdef DEBUG_FLAG
          std::cout << "SqErrLocal = " << SqErrLocal
                    << " diff = " <<  SqErrLocal - SqErrLocal_resolve
                    << " % diff = " <<  (SqErrLocal - SqErrLocal_resolve)/SqErrLocal << std::endl;
#endif
          failed_edge_list.insert( edge );
        }
//        std::cout<< "lgfld.nDOF() = " << localProblem_split.primal_local_.lgfld.nDOF() << std::endl;
      }

//      /////////////////////////////////////////////
//      // EDGE LOCAL ESTIMATION CHECK
//      /////////////////////////////////////////////
//
//      // Check that the computed error matches the original. The solution should not have changed
//      for (attachedCells_it = attachedCells.begin(); attachedCells_it != attachedCells.end(); ++attachedCells_it)
//      {
//        // only handle elements in current group, others will get counted on the loop with that group
//        if (attachedCells_it->group == group ) // if the cell is in the current group
//        {
//         // Extract the
//
//          /////////////////////////////////////////////
//          // EDGE LOCAL ERROR ESTIMATE CHECK
//          /////////////////////////////////////////////
//        }
//      }
//      std::cout << "-----" << std::endl;
//     return;
    }
#ifdef DEBUG_FLAG
    std::cout<< "failed_edges = ";
    for (auto it = failed_edge_list.begin(); it != failed_edge_list.end(); ++it)
      std::cout<< "(" << (*it)[0] << "," << (*it)[1] << "), ";
    std::cout << std::endl;
#endif
  }


  // make sure more decrease than increase
  BOOST_CHECK_GE( nDecreaseError, nIncreaseError );

}

// Minimal Tests
#if 1
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hierarchical_SmallBox_Unsplit_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int ii = 1, jj = ii, order = 1;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hierarchical_SmallBox_Split_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int ii = 1, jj = ii, order = 1;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hierarchical_SmallBox_Unsplit_P1_Q2 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int ii = 1, jj = ii, order = 1, qorder = 2;
  XField2D_Box_Triangle_Lagrange_X1 xfld_linear(comm, ii,jj);
  XField<PhysD2,TopoD2> xfld(xfld_linear, qorder, BasisFunctionCategory_Lagrange );
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hierarchical_SmallBox_Split_P1_Q2 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int ii = 1, jj = ii, order = 1, qorder = 2;
  XField2D_Box_Triangle_Lagrange_X1 xfld_linear(comm, ii,jj);
  XField<PhysD2,TopoD2> xfld(xfld_linear, qorder, BasisFunctionCategory_Lagrange );
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hierarchical_SmallBox_Unsplit_P1_Q3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int ii = 1, jj = ii, order = 1, qorder = 3;
  XField2D_Box_Triangle_Lagrange_X1 xfld_linear(comm, ii,jj);
  XField<PhysD2,TopoD2> xfld(xfld_linear, qorder, BasisFunctionCategory_Lagrange );
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hierarchical_SmallBox_Split_P1_Q3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int ii = 1, jj = ii, order = 1, qorder = 3;
  XField2D_Box_Triangle_Lagrange_X1 xfld_linear(comm, ii,jj);
  XField<PhysD2,TopoD2> xfld(xfld_linear, qorder, BasisFunctionCategory_Lagrange );
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#endif

// UNSPLIT TESTS - Nothing should change

// -----------------------------------------------------
//  Hierarchical
// -----------------------------------------------------

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world;//.split(world.rank());
  int ii = 4, jj = ii, order = 1;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-10, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_P2 )
{
  mpi::communicator world;
  int size = 4*4*world.size();
  int ii = sqrt(size), jj = ii, order = 2;
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii,jj);
  const Real small_tol = 1e-10, close_tol = 1e-7;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_P3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

  int ii = 4, jj = ii, order = 3;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-7;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

// -----------------------------------------------------
//  Lagrange
// -----------------------------------------------------

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  int ii = 4, jj = ii, order = 1;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-10, close_tol = 5e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_P2 )
{
  mpi::communicator world;
  int size = 4*4*world.size();
  int ii = sqrt(size), jj = ii, order = 2;
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii,jj);
  const Real small_tol = 1e-10, close_tol = 5e-8;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_P3 )
{
  mpi::communicator world;
  int size = 4*4*world.size();
  int ii = sqrt(size), jj = ii, order = 3;
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii,jj);
  const Real small_tol = 5e-7, close_tol = 5e-7;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

// =====================================================
// SPLIT TESTS - Check that energy norm error (for Poisson) decreases
// =====================================================

// -----------------------------------------------------
//  Hierarchical
// -----------------------------------------------------

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  int ii = 4, jj = ii, order = 1;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-10, close_tol = 1e-10;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_P2 )
{
  mpi::communicator world;
  int size = 4*4*world.size();
  int ii = sqrt(size), jj = ii, order = 2;
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii,jj);
  const Real small_tol = 1e-10, close_tol = 1e-10;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_P3 )
{
  mpi::communicator world;
  int size = 4*4*world.size();
  int ii = sqrt(size), jj = ii, order = 3;
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii,jj);
  const Real small_tol = 1e-10, close_tol = 1e-10;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );
}
#endif


// -----------------------------------------------------
//  Lagrange
// -----------------------------------------------------

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_P1 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  int ii = 2, jj = ii, order = 1;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-10, close_tol = 1e-10;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_P2 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  int ii = 2, jj = ii, order = 2;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-10, close_tol = 1e-10;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_P3 )
{
  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());
  int ii = 4, jj = ii, order = 3;
  XField2D_Box_Triangle_Lagrange_X1 xfld(comm, ii,jj);
  const Real small_tol = 1e-10, close_tol = 1e-10;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
