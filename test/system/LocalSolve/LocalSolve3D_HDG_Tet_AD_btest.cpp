// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LocalSolve_HDG_Tet_AD_btest
// testing of 3-D HDG local solves with Advection-Diffusion on tets

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/AlgebraicEquationSet_Local_HDG.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_Trace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/Local/Field_Local.h"
#include "Field/Local/XField_LocalPatch.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( LocalSolve3D_HDG_Tet_AD_test_suite )

void localsolve(const XField<PhysD3, TopoD3>& xfld, const int order, const int main_group, const int main_cell,
                const std::vector<int>& global_itracegroup_list, const std::vector<int>& global_btracegroup_list,
                const std::vector<int>& neighbor_elem_list,  const std::vector<int>& neighbor_trace_list,
                const std::vector<int>& mainBtrace_grouplist, const std::vector<int>& mainBtrace_elemlist)
{
  const int D = PhysD3::D;

  typedef ScalarFunction3D_TripleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEAdvectionDiffusion3D;

  typedef PDENDConvertSpace<PhysD3, PDEAdvectionDiffusion3D> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_sansLG BCType;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_DG_Cell<PhysD3, TopoD3, ArrayQ> QField3D_DG_Cell;
  typedef Field_DG_Cell<PhysD3, TopoD3, VectorArrayQ> AField3D_DG_Cell;
  typedef Field_DG_Trace<PhysD3, TopoD3, ArrayQ> QIField3D_DG_Trace;
  typedef Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> LGField3D_DG_Trace;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> PrimalEquationSetClass_Sparse;
  typedef AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Dense, XField<PhysD3, TopoD3>> PrimalEquationSetClass_Dense;

  typedef PrimalEquationSetClass_Sparse::BCParams BCParams;

  typedef PrimalEquationSetClass_Sparse::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass_Sparse::SystemVector SystemVectorClass;

  typedef PrimalEquationSetClass_Dense::SystemMatrix SystemMatrixClassD;
  typedef PrimalEquationSetClass_Dense::SystemVector SystemVectorClassD;
  typedef PrimalEquationSetClass_Dense::SystemNonZeroPattern SystemNonZeroPatternD;

  Real a = 0.5;
  Real b = 0.5;
  Real c = 0.5;
  AdvectiveFlux3D_Uniform adv( a, b, c );

  Real nu = 1;
  ViscousFlux3D_Uniform visc( nu, 0, 0, 0, nu, 0, 0, 0, nu );

  Source3D_UniformGrad source(0,0,0,0);

  NDPDEClass pde( adv, visc, source );

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.c] = c;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  // BC
  PyDict TripleBL;
  TripleBL[BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function.TripleBL;
  TripleBL[SolutionExact::ParamsType::params.a] = a;
  TripleBL[SolutionExact::ParamsType::params.b] = b;
  TripleBL[SolutionExact::ParamsType::params.c] = c;
  TripleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function] = TripleBL;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  // solution
  QField3D_DG_Cell qfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld = 0;

  // auxiliary operator
  AField3D_DG_Cell afld(xfld, order, BasisFunctionCategory_Hierarchical);
  afld = 0;

  // interface
  QIField3D_DG_Trace qIfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qIfld = 0;

  //Lagrange multiplier
  std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
  LGField3D_DG_Trace lgfld( xfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list );
  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> eqnsettol = {1e-12, 1e-12, 1e-12};

  PrimalEquationSetClass_Sparse PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, eqnsettol,
                                            {0}, global_itracegroup_list, PyBCList, BCBoundaryGroups );

  // residual
  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
  PrimalEqSet.fillSystemVector(q);

  rsd = 0;
  PrimalEqSet.residual(q, rsd);

#if 0 //Print residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < qfld.nDOF(); k++) cout << k << "\t" << rsd[0][k] <<endl;

  cout << "Aux residual:" <<endl;
  for (int k = 0; k < afld.nDOF(); k++) cout << k << "\t" << rsd[1][D*k + 0] <<"\t" << rsd[1][D*k + 1] <<"\t" << rsd[1][D*k + 2] << endl;

  cout <<endl << "Int residual:" <<endl;
  for (int k = 0; k < qIfld.nDOF(); k++) cout << k << "\t" << rsd[2][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < lgfld.nDOF(); k++) cout << k << "\t" << rsd[3][k] << endl;
#endif

  // solve
  SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);

  SystemVectorClass dq(PrimalEqSet.vectorStateSize());
  solver.solve(rsd, dq);

  // update solution
  q -= dq;
  PrimalEqSet.setSolutionField(q);


  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  //Extract the local mesh for the given element
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_constructor(comm_local, connectivity, main_group, main_cell, SpaceType::Discontinuous);

  // create an unsplit patch
  XField_LocalPatch<PhysD3,Tet> xfld_local( xfld_constructor );

  std::vector<int> active_BGroup_list_local = {}; //Using sansLG - so lgfld should be empty

  int nLocalInteriorTraceGroups = std::min(xfld_local.nInteriorTraceGroups(), 2);
  std::vector<int> interiorTraceGroups(nLocalInteriorTraceGroups);
  for (int i = 0; i < nLocalInteriorTraceGroups; i++)
    interiorTraceGroups[i] = i;

  int nLocalBoundaryTraceGroups = xfld_local.nBoundaryTraceGroups();
  std::vector<int> boundaryTraceGroups(nLocalBoundaryTraceGroups);
  for (int i = 0; i < nLocalBoundaryTraceGroups; i++)
    boundaryTraceGroups[i] = i;

  Field_Local<QField3D_DG_Cell> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Hierarchical);
  Field_Local<AField3D_DG_Cell> afld_local(xfld_local, afld, order, BasisFunctionCategory_Hierarchical);
  Field_Local<QIField3D_DG_Trace> qIfld_local(xfld_local, qIfld, order, BasisFunctionCategory_Hierarchical,
                                              interiorTraceGroups, boundaryTraceGroups);
  Field_Local<LGField3D_DG_Trace> lgfld_local(xfld_local, lgfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list_local);

  const int nDOF_per_cell = qfld_local.nDOF()/qfld_local.nElem();
  const int nDOF_per_itrace = qIfld_local.nDOF()/qIfld_local.nElem();
  const int nDOF_per_btrace = nDOF_per_itrace;

  BOOST_REQUIRE(nDOF_per_cell > 0);
  BOOST_REQUIRE(nDOF_per_itrace > 0);
  BOOST_REQUIRE(nDOF_per_btrace > 0);

  std::vector<int> MainBTraceGroup_list;
  int nMainBTraces = 0;

  for (int i = 0; i < qIfld_local.nBoundaryTraceGroups(); i++)
  {
    MainBTraceGroup_list.push_back(i);
    nMainBTraces += qIfld_local.getBoundaryTraceGroupBase(i).nElem();
  }

  int nMainITraces = 0;
  for (int i = 0; i < qIfld_local.nInteriorTraceGroups(); i++)
    nMainITraces += qIfld_local.getInteriorTraceGroupBase(i).nElem();

//  std::cout<<"nMainBTraces:"<<nMainBTraces<<std::endl;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_local["BCName"] = MainBTraceGroup_list;

#if 0
   xfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   xfld_local.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
#endif

#if 0
   qfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   qfld_local.dump(3,std::cout);

//   afld.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;
//   afld_local.dump(3,std::cout);
//
//   lgfld.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;
//   lgfld_local.dump(3,std::cout);
#endif

   QuadratureOrder quadratureOrder_local( xfld_local, -1 );

   PrimalEquationSetClass_Dense PrimalEqSet_Local(xfld_local, qfld_local, afld_local, qIfld_local, lgfld_local,
                                                  pde, disc, quadratureOrder_local, eqnsettol, {0,1}, {0}, PyBCList, BCBoundaryGroups_local);

   // Local residual
   SystemVectorClassD rsd_local(PrimalEqSet_Local.vectorEqSize());
   rsd_local = 0;
   PrimalEqSet_Local.residual(rsd_local);
   std::vector<std::vector<Real>> rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < qfld_local.nDOF(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout << "Aux residual:" <<endl;
  for (int k = 0; k < afld_local.nDOF(); k++) cout << k << "\t" << rsd_local[1][D*k + 0]
                                                        << "\t" << rsd_local[1][D*k + 1]
                                                        << "\t" << rsd_local[1][D*k + 2] << endl;

  cout <<endl << "Int residual:" <<endl;
  for (int k = 0; k < qIfld_local.nDOF(); k++) cout << k << "\t" << rsd_local[2][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < lgfld_local.nDOF(); k++) cout << k << "\t" << rsd_local[3][k] << endl;
#endif

  //Check if residual is zero for DOFs on main cell
  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNorm) );

  //Zero out qfld and afld solution in main cell, so that residual becomes non-zero
  for (int i = 0; i < nDOF_per_cell; i++)
  {
    qfld_local.DOF(i) = 0.0;
    afld_local.DOF(i) = 0.0;
  }

  //Zero out qIfld solution in main interior traces and main boundary traces, so that the residual becomes non-zero
  int nDOFModify = nMainITraces*nDOF_per_itrace + nMainBTraces*nDOF_per_btrace;
  for (int i = 0; i < nDOFModify; i++)
    qIfld_local.DOF(i) = 0.0;

  //Re-compute residual vector
  SystemVectorClassD q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);

  //Jacobian
  SystemNonZeroPatternD nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClassD jac_local(nz_local);
  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  //Local solve - with extracted sub-system
  SystemVectorClassD dq_local(PrimalEqSet_Local.vectorStateSize());
  dq_local = DLA::InverseLU::Solve(jac_local,rsd_local);

  //Update local solution fields
  q_local -= dq_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNorm) );

 // Can't seem to suppress clang analyzer with size checks...
#ifndef __clang_analyzer__

  //Check if local solutions are equal to the global solutions
  std::vector<int> Cell_DOF_map_local(nDOF_per_cell), Cell_DOF_map_global(nDOF_per_cell);

  Real tol = 5e-9;

  //Check qfld DOFs on main cell
  qfld_local.getCellGroup<Tet>(0).associativity(0).getNodeGlobalMapping(Cell_DOF_map_local.data(), nDOF_per_cell);
  qfld.getCellGroup<Tet>(main_group).associativity(main_cell).getNodeGlobalMapping(Cell_DOF_map_global.data(), nDOF_per_cell);

  for (int k = 0; k < nDOF_per_cell; k++)
    BOOST_CHECK_CLOSE( qfld_local.DOF(Cell_DOF_map_local[k]), qfld.DOF(Cell_DOF_map_global[k]), tol );

  //Check afld DOFs on main cell
  afld_local.getCellGroup<Tet>(0).associativity(0).getNodeGlobalMapping(Cell_DOF_map_local.data(), nDOF_per_cell);
  afld.getCellGroup<Tet>(main_group).associativity(main_cell).getNodeGlobalMapping(Cell_DOF_map_global.data(), nDOF_per_cell);

  for (int k = 0; k < nDOF_per_cell; k++)
    for (int d = 0; d < D; d++)
      BOOST_CHECK_CLOSE( afld_local.DOF(Cell_DOF_map_local[k])[d], afld.DOF(Cell_DOF_map_global[k])[d], tol );

#if 0 //Using sansLG, so need to check lgfld
  int Btrace_DOF_map_local[nDOF_per_btrace], Btrace_DOF_map_global[nDOF_per_btrace];

  //Check lgfld DOFs for main-boundary traces
  for (int i=0; i < (int)mainBtrace_grouplist.size(); i++)
  {
    int btrace_group = mainBtrace_grouplist[i];
    int btrace_elem = mainBtrace_elemlist[i];

    lgfld.getBoundaryTraceGroup<Triangle>(btrace_group).associativity(btrace_elem).getCellGlobalMapping(Btrace_DOF_map_global, nDOF_per_btrace);
    lgfld_local.getBoundaryTraceGroup<Triangle>(i).associativity(0).getCellGlobalMapping(Btrace_DOF_map_local, nDOF_per_btrace);

    for (int k = 0; k < nDOF_per_btrace; k++)
      BOOST_CHECK_CLOSE( lgfld_local.DOF(Btrace_DOF_map_local[k]), lgfld.DOF(Btrace_DOF_map_global[k]), tol );
  }
#endif
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_HDG_AD_Box_Tet_Cell0_Target_P1 )
{
  //Targeting a cell with 3 main boundary traces

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 0;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> neighbor_elem_list = {1};
  std::vector<int> neighbor_trace_list = {2};

  std::vector<int> mainBtrace_grouplist = {0,2,4}; //in-order: xmin, ymin, zmin
  std::vector<int> mainBtrace_elemlist = {0,0,0};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_HDG_AD_Box_Tet_Cell2_Target_P1 )
{
  //Targeting a cell with 2 main boundary traces

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 2;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> neighbor_elem_list = {1,4};
  std::vector<int> neighbor_trace_list = {0,2};

  std::vector<int> mainBtrace_grouplist = {0,3}; //in-order: xmin, ymax
  std::vector<int> mainBtrace_elemlist = {1,0};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_HDG_AD_Box_Tet_Cell3_Target_P1 )
{
  //Targeting a cell with 2 main boundary traces

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 3;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> neighbor_elem_list = {4,1};
  std::vector<int> neighbor_trace_list = {1,3};

  std::vector<int> mainBtrace_grouplist = {1,2}; //in order: xmax, ymin
  std::vector<int> mainBtrace_elemlist = {0,1};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_HDG_AD_Box_Tet_Cell5_Target_P1 )
{
  //Targeting a cell with 3 main boundary traces

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 5;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> neighbor_elem_list = {4};
  std::vector<int> neighbor_trace_list = {0};

  std::vector<int> mainBtrace_grouplist = {1,5,3}; //in order: xmax, zmax, ymax
  std::vector<int> mainBtrace_elemlist = {1,1,1};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_HDG_AD_Box_Tet_Cell_OneBTrace_Target_P1 )
{
  //Targeting a cell with 1 main boundary trace

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 2;
  int jj = ii;
  int kk = 1;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 18;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> neighbor_elem_list = {19,15,8};
  std::vector<int> neighbor_trace_list = {2,1,3};

  std::vector<int> mainBtrace_grouplist = {4}; //in order: zmin
  std::vector<int> mainBtrace_elemlist = {6};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_HDG_AD_Box_Tet_InteriorCell_Target_P1 )
{
  //Targeting a cell with no main boundary traces (fully interior cell)

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 2;
  int jj = ii;
  int kk = 1;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 8;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> neighbor_elem_list = {7,10,5,18};
  std::vector<int> neighbor_trace_list = {0,2,1,2};

  std::vector<int> mainBtrace_grouplist = {};
  std::vector<int> mainBtrace_elemlist = {};

  localsolve(xfld, order, main_group, main_cell, global_itracegroup_list, global_btracegroup_list,
             neighbor_elem_list, neighbor_trace_list, mainBtrace_grouplist, mainBtrace_elemlist);
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
