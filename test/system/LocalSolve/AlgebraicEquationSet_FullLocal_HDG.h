// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "tools/SANSnumerics.h"     // Real

namespace SANS
{
namespace testspace
{

//A temporary AlgEqnSet class that's derived from the main HDG AlgEqnSet, so that we can use its protected constructor

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_FullLocal_HDG : public AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>
{
public:

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType> BaseType;

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename BaseType::VectorSizeClass VectorSizeClass;
  typedef typename BaseType::MatrixSizeClass MatrixSizeClass;

  typedef typename BaseType::SystemMatrix SystemMatrix;
  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::BCParams BCParams;

  static const int main_cellgroup = 0;

  template< class... BCArgs >
  AlgebraicEquationSet_FullLocal_HDG( const XFieldType& xfld,
                                      Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                      Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& afld,
                                      Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                      Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                      const NDPDEClass& pde,
                                      DiscretizationHDG<NDPDEClass>& disc,
                                      const QuadratureOrder& quadratureOrder,
                                      std::vector<Real>& tol,
                                      const std::vector<int>& CellGroups,
                                      const std::vector<int>& InteriorTraceGroups,
                                      PyDict& BCList,
                                      const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args )
   : BaseType(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
              CellGroups, {main_cellgroup}, InteriorTraceGroups, BCList, BCBoundaryGroups, args... ) {}
};

inline void
solveFullLocalSystem(const DLA::VectorD< DLA::VectorD<Real> >& rsd, const DLA::MatrixD< DLA::MatrixD<Real> >& jac,
                     const int sub_nDOFPDE, const int sub_nDOFInt, const int sub_nDOFBC,
                     DLA::VectorD< DLA::VectorD<Real> >& sol)
{
  DLA::MatrixD< DLA::MatrixD<Real> > jac_sub(jac.size());
  DLA::VectorD< DLA::VectorD<Real> > rsd_sub(rsd.size());

  const int iq = 0;
  const int iqI = 1;
  const int ilg = 2;

  jac_sub = DLA::Identity();
  rsd_sub = 0.0;

  for (int i=0; i<sub_nDOFPDE; i++)
  {
    for (int j=0; j<sub_nDOFPDE; j++)
      jac_sub(iq,iq)(i,j) = jac(iq,iq)(i,j);

    for (int j=0; j<sub_nDOFInt; j++)
      jac_sub(iq,iqI)(i,j) = jac(iq,iqI)(i,j);

    for (int j=0; j<sub_nDOFBC; j++)
      jac_sub(iq,ilg)(i,j) = jac(iq,ilg)(i,j);

    rsd_sub[iq][i] = rsd[iq][i];
  }

  for (int i=0; i<sub_nDOFInt; i++)
  {
    for (int j=0; j<sub_nDOFPDE; j++)
      jac_sub(iqI,iq)(i,j) = jac(iqI,iq)(i,j);

    for (int j=0; j<sub_nDOFInt; j++)
      jac_sub(iqI,iqI)(i,j) = jac(iqI,iqI)(i,j);

    for (int j=0; j<sub_nDOFBC; j++)
      jac_sub(iqI,ilg)(i,j) = jac(iqI,ilg)(i,j);

    rsd_sub[iqI][i] = rsd[iqI][i];
  }

  for (int i=0; i<sub_nDOFBC; i++)
  {
    for (int j=0; j<sub_nDOFPDE; j++)
      jac_sub(ilg,iq)(i,j) = jac(ilg,iq)(i,j);

    for (int j=0; j<sub_nDOFInt; j++)
      jac_sub(ilg,iqI)(i,j) = jac(ilg,iqI)(i,j);

    for (int j=0; j<sub_nDOFBC; j++)
      jac_sub(ilg,ilg)(i,j) = jac(ilg,ilg)(i,j);

    rsd_sub[ilg][i] = rsd[ilg][i];
  }

//  std::cout << std::endl;
//  std::cout << jac_sub(0,0) << std::endl << std::endl;
//  std::cout << jac_sub(0,1) << std::endl << std::endl;
//  std::cout << jac_sub(1,0) << std::endl << std::endl;
//  std::cout << jac_sub(1,1) << std::endl << std::endl;

  sol = DLA::InverseLU::Solve(jac_sub,rsd_sub);
}

} //test namespace
}
