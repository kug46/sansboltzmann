// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LocalSolve_Split_DGBR2_Triangle_AD_AV_btest
// testing of 2-D DG Advection-Diffusion with artificial viscosity on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion_ArtificialViscosity2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion_ArtificialViscosity2D.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"
#include "pde/Sensor/Source2D_JumpSensor.h"
#include "pde/Sensor/PDESensorParameter2D.h"
#include "pde/Sensor/BCSensorParameter2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_Solution.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/SolutionData_DGBR2_Block2.h"

#include "pde/BCParameters.h"

#include "Adaptation/MOESS/SolverInterface_DGBR2_Block2x2.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/Local/XField_LocalPatch.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"


#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( LocalSolve2D_Split_DGBR2_Triangle_AD_AV_test_suite )

template <class SolverInterfaceClass>
void
localsolve(SolverInterfaceClass& interface, const XField<PhysD2, TopoD2>& xfld, const int main_group, const int main_cell,
           const ElementSplitType split_type, const int split_edge_index, const bool verbose);

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adapt2D_AD_ArtificialViscosity_Triangle )
{
  typedef AdvectiveFlux2D_Uniform AdvectiveFluxType;
  typedef ViscousFlux2D_Uniform ViscousFluxType;
  typedef Source2D_UniformGrad SourceType;

  typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFluxType, ViscousFluxType, SourceType> PDEClass_Primal;
  typedef PDENDConvertSpace<PhysD2, PDEClass_Primal> NDPDEClass_Primal;

  typedef AdvectionDiffusion_Sensor Sensor;
  typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;

  typedef Sensor_AdvectiveFlux2D_Uniform Sensor_Advection;
  typedef Sensor_ViscousFlux2D_GenHScale Sensor_Diffusion;

  typedef PDESensorParameter<PhysD2,
                             SensorParameterTraits<PhysD2>,
                             Sensor_Advection,
                             Sensor_Diffusion,
                             Source_JumpSensor > PDEClass_Sensor;
  typedef PDENDConvertSpace<PhysD2, PDEClass_Sensor> NDPDEClass_Sensor;

  typedef BCTypeLinearRobin_sansLG BCType;
  typedef BCAdvectionDiffusion_ArtificialViscosity2DVector<AdvectiveFluxType, ViscousFluxType> BCVectorPrimal;

  typedef BCParameters<BCVectorPrimal> BCParamsPrimal;

  typedef BCSensorParameter2DVector<Sensor_Advection, Sensor_Diffusion> BCVectorSensor;

  typedef BCParameters<BCVectorSensor> BCParamsSensor;

  typedef OutputCell_Solution<PDEClass_Primal> OutputClass;
//  typedef OutputCell_SolutionSquared<PDEClass_Primal> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_ArtificialViscosity ParamBuilderType;

  //Solution data
  typedef SolutionData_DGBR2_Block2<PhysD2, TopoD2, NDPDEClass_Primal, NDPDEClass_Sensor, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType0 ParamFieldType0;
  typedef typename SolutionClass::ParamFieldType1 ParamFieldType1;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Primal, BCNDConvertSpace, BCVectorPrimal,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType0> PrimalEquationSetClass_Primal;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass_Sensor, BCNDConvertSpace, BCVectorSensor,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldType1> PrimalEquationSetClass_Sensor;

  typedef SolverInterface_DGBR2_Block2x2<PhysD2, TopoD2,
                                         NDPDEClass_Primal, BCNDConvertSpace, BCVectorPrimal,
                                         NDPDEClass_Sensor, BCNDConvertSpace, BCVectorSensor,
                                         ParamBuilderType, SolutionClass,
                                         PrimalEquationSetClass_Primal, PrimalEquationSetClass_Sensor,
                                         OutputIntegrandClass> SolverInterfaceClass;

  const bool verbose = false;

  // Grid
  int ii = 3;
  int jj = 3;

  std::shared_ptr<XField<PhysD2, TopoD2>> pxfld( new XField2D_Box_Triangle_X1( ii, jj, 0, 1, 0, 1, true ) );

  int order_primal = 1;
  int order_sensor = 1;

  // Primal PDE
  Real a = 0.75;
  Real b = 1.0;
  Real nu = 0.0;
  AdvectiveFluxType adv( a, b );
//  AdvectiveFluxType adv( 0, a, 0, a,
//                         b, b, b, 0, 0.0, 0.0);
  ViscousFluxType visc( nu, 0, 0, nu );
  SourceType source(0.0, 0.0, 0.0);

  //Create artificial viscosity PDE
  NDPDEClass_Primal pde_primal(adv, visc, source, order_primal);


  //Sensor PDE
  Sensor sensor(pde_primal);
  Sensor_Advection sensor_adv(0.0, 0.0);

//  Real mu_s = 1.0;
//  Sensor_Diffusion sensor_visc(mu_s, 0.0, 0.0, mu_s); //constant
//  Sensor_Diffusion sensor_visc(0.1); //h-dependent
  Sensor_Diffusion sensor_visc(1.0);

  Source_JumpSensor sensor_source(order_primal, sensor);
//  Source_JumpSensor sensor_source;

  NDPDEClass_Sensor pde_sensor(sensor_adv, sensor_visc, sensor_source);

  Real uL = 1.25;
  Real uB = 0.75;

#ifdef SOLUTION_BC
  typedef ScalarFunction2D_PiecewiseConstBlock ExactSolutionClass;
  typedef SolnNDConvertSpace<PhysD2, ExactSolutionClass> NDExactSolutionClass;

  // Create a Solution dictionary
  PyDict SolnDict;
  SolnDict[BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.Function.Name]
           = BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params.Function.PiecewiseConstBlock;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a00] = uL;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a01] = 0.0;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a10] = 0.5;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.a11] = 0.1;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.x0] = 0.0;
  SolnDict[ScalarFunction2D_PiecewiseConstBlock::ParamsType::params.y0] = 0.0;

  NDExactSolutionClass solnExact(SolnDict);
#endif


  // Primal PDE BCs

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.None;

#ifdef SOLUTION_BC
  PyDict BCSolution;
  BCSolution[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.FunctionLinearRobin_sansLG;
  BCSolution[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.A] = 1.0;
  BCSolution[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0.0;
  BCSolution[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function] = SolnDict;
#endif

  PyDict BCDirichletB;
  BCDirichletB[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.LinearRobin_sansLG;
  BCDirichletB[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCDirichletB[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;
  BCDirichletB[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = uB;

  PyDict BCDirichletL;
  BCDirichletL[BCParamsPrimal::params.BC.BCType] = BCParamsPrimal::params.BC.LinearRobin_sansLG;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.bcdata] = uL;

  PyDict PyBCList_Primal;
  PyBCList_Primal["DirichletB"] = BCDirichletB;
  PyBCList_Primal["DirichletL"] = BCDirichletL;
//  PyBCList_Primal["DirichletSol"] = BCSolution;
  PyBCList_Primal["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Primal;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_Primal["DirichletB"] = {0,1}; //Bottom boundary
  BCBoundaryGroups_Primal["DirichletL"] = {3}; //Left boundary
//  BCBoundaryGroups_Primal["DirichletSol"] = {0,3}; //Left boundary
  BCBoundaryGroups_Primal["None"] = {2}; //Top boundary

  //Check the BC dictionary
  BCParamsPrimal::checkInputs(PyBCList_Primal);

  std::vector<int> active_boundaries_primal = BCParamsPrimal::getLGBoundaryGroups(PyBCList_Primal, BCBoundaryGroups_Primal);

  // Sensor BCs
  PyDict BCSensorRobin;
  BCSensorRobin[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorRobin[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 0.0;
  BCSensorRobin[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCSensorRobin[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorDirichlet;
  BCSensorDirichlet[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.LinearRobin_sansLG;
  BCSensorDirichlet[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCSensorDirichlet[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0.0;
  BCSensorDirichlet[BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict BCSensorNone;
  BCSensorNone[BCParamsSensor::params.BC.BCType] = BCParamsSensor::params.BC.None;

  PyDict PyBCList_Sensor;
  PyBCList_Sensor["SensorRobin"] = BCSensorRobin;
  PyBCList_Sensor["SensorDirichlet"] = BCSensorDirichlet;
//  PyBCList_Sensor["SensorNone"] = BCSensorNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_Sensor;
  BCBoundaryGroups_Sensor["SensorRobin"] = {1,2}; // Bottom, right, left
  BCBoundaryGroups_Sensor["SensorDirichlet"] = {0,3}; // Bottom, right, left
//  BCBoundaryGroups_Sensor["SensorNone"] = {0,1,2,3}; // Top

  //Check the BC dictionary
  BCParamsSensor::checkInputs(PyBCList_Sensor);

  std::vector<int> active_boundaries_sensor = BCParamsSensor::getLGBoundaryGroups(PyBCList_Sensor, BCBoundaryGroups_Sensor);


  // BR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  std::vector<Real> tol0 = {1e-11, 1e-11};
  std::vector<Real> tol1 = {1e-7, 1e-7};

  //Output functional
  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
//  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-13;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = verbose;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = verbose;
//  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde_primal, pde_sensor,
                                               order_primal, order_sensor, order_primal+1, order_sensor+1,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                               active_boundaries_primal, active_boundaries_sensor, disc, disc);

  const int quadOrder = -1;

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol,
                                                      ResidualNorm_Default, ResidualNorm_Default,
                                                      tol0, tol1, quadOrder,
                                                      cellGroups, interiorTraceGroups,
                                                      PyBCList_Primal, PyBCList_Sensor,
                                                      BCBoundaryGroups_Primal, BCBoundaryGroups_Sensor,
                                                      SolverContinuationDict, outputIntegrand);

  //Set initial solution
#ifdef SOLUTION_BC
  pGlobalSol->setSolution0(solnExact, cellGroups);
#endif
  pGlobalSol->primal0.qfld = 0.1;
  pGlobalSol->primal1.qfld = 0.0;

  if (verbose)
  {
    //Output h-field
//    std::string hfld_filename = "tmp/hfld_a0.plt";
//  output_Tecplot( get<0>(pGlobalSol->paramfld0), hfld_filename );

    std::string qfld0_init_filename = "tmp/qfld0_init_a0.plt";
    std::string qfld1_init_filename = "tmp/qfld1_init_a0.plt";
    output_Tecplot( pGlobalSol->primal0.qfld, qfld0_init_filename );
    output_Tecplot( pGlobalSol->primal1.qfld, qfld1_init_filename );
  }

  pInterface->solveGlobalPrimalProblem();
  pInterface->solveGlobalAdjointProblem();

  if (verbose)
  {
    std::string qfld0_filename = "tmp/qfld0_a0.plt";
    std::string qfld1_filename = "tmp/qfld1_a0.plt";
    output_Tecplot( pGlobalSol->primal0.qfld, qfld0_filename );
    output_Tecplot( pGlobalSol->primal1.qfld, qfld1_filename );

    std::string adjfld0_filename = "tmp/adjfld0_a0.plt";
    std::string adjfld1_filename = "tmp/adjfld1_a0.plt";
    output_Tecplot( pGlobalSol->adjoint0.qfld, adjfld0_filename );
    output_Tecplot( pGlobalSol->adjoint1.qfld, adjfld1_filename );
  }

  //Compute error estimates
  pInterface->computeErrorEstimates();

  std::cout << std::scientific << std::setprecision(3);

  //Check errors for different local solves on center elements
  localsolve(*pInterface, *pxfld, 0, 7, ElementSplitType::Edge, 0, verbose);
  localsolve(*pInterface, *pxfld, 0, 7, ElementSplitType::Edge, 1, verbose);
  localsolve(*pInterface, *pxfld, 0, 7, ElementSplitType::Edge, 2, verbose);
  //localsolve(*pInterface, *pxfld, 0, 7, ElementSplitType::Isotropic, -1, verbose);

  localsolve(*pInterface, *pxfld, 0, 8, ElementSplitType::Edge, 0, verbose);
  localsolve(*pInterface, *pxfld, 0, 8, ElementSplitType::Edge, 1, verbose);
  localsolve(*pInterface, *pxfld, 0, 8, ElementSplitType::Edge, 2, verbose);
  //localsolve(*pInterface, *pxfld, 0, 8, ElementSplitType::Isotropic, -1, verbose);
}

template <class SolverInterfaceClass>
void
localsolve(SolverInterfaceClass& interface, const XField<PhysD2, TopoD2>& xfld, const int main_group, const int main_cell,
           const ElementSplitType split_type, const int split_edge_index, const bool verbose)
{
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the given element
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_constructor(comm_local, connectivity, main_group, main_cell, SpaceType::Discontinuous);

  // create an split patch
  XField_LocalPatch<PhysD2,Triangle> xfld_local( xfld_constructor, split_type, split_edge_index);

  std::vector<Real> local_error(0,0);
  interface.solveLocalProblem(xfld_local, local_error);
  //Check that the error has reduced after the solve
  BOOST_CHECK_LT( local_error[0], interface.getElementalErrorEstimate(main_group, main_cell) );

  //Check the sum of individual errors from the two PDEs
  std::array<Real,2> global_elemental_errors = interface.getElementalErrorEstimates(main_group, main_cell);
  Real global_elemental_error = global_elemental_errors[0] + global_elemental_errors[1];
  SANS_CHECK_CLOSE( interface.getElementalErrorEstimate(main_group, main_cell),
                    global_elemental_error, small_tol, close_tol );

  if (verbose)
  {
    std::cout << "Local error (total): " << local_error[0] << std::endl;
    std::cout << "Global elemental error: PDE0 = " << global_elemental_errors[0] << ", PDE1 = " << global_elemental_errors[1]
              << ", Sum = " << interface.getElementalErrorEstimate(main_group, main_cell)
              << ", elem = " << main_cell << ", split = " << split_edge_index
              << std::endl;
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
