// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// EdgeLocalSolve_Galerkin_Triangle_Hierarchical_AD_btest
// testing of 2-D DG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionGradientErrorSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#include "tools/minmax.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Prints out a lot of useful information
//#define DEBUG_FLAG
//

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( EdgeLocalSolve1D_Dual_Galerkin_Triangle_Hierarchical_AD_test_suite )

/*
* Computes a re-solve on a given local mesh, then checks that the solution matches exactly the original
* As a consequence, this implies that the re-solve was on an unsplit mesh. It is basically lifted from solveLocalProblem
*
*/

// A helper function to construct a Solver interface and test all the edges on a grid
void localExtractAndTest(const XField<PhysD1, TopoD1>& xfld, const int order, const BasisFunctionCategory basis_category,
                         const Real small_tol, const Real close_tol, const bool isSplit )
{

  typedef ScalarFunction1D_Quad SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> NDPDEClass;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  // Manufactured Solution Adjoints

  typedef OutputCell_SolutionGradientErrorSquared<PDEAdvectionDiffusion1D,NDSolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef BCParameters<BCVector> BCParams;
  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD1, TopoD1, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Pieces for the debugging
  typedef typename SolverInterfaceClass::LocalProblem LocalProblem;

  typedef typename SolverInterfaceClass::ArrayQ ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QFieldType;
  typedef Field_CG_BoundaryTrace< PhysD1, TopoD1, ArrayQ > LGFieldType;

  // A debugging class for exposing protected members
  struct debugLocalProblem : public LocalProblem
  {
    typedef LocalProblem BaseType;

    debugLocalProblem( const SolverInterfaceClass& owner, XField_Local_Base<PhysD1,TopoD1>& xfld_local,
                       LocalSolveStatus& status) : BaseType(owner,xfld_local,status) {}

    using BaseType::interface_;
    using BaseType::xfld_local_;
    using BaseType::status_;
    using BaseType::quadrule_;
    using BaseType::maxIter;
    using BaseType::BCList_local_;
    using BaseType::BCBoundaryGroups_local_;
    using BaseType::local_cellGroups_;
    using BaseType::local_interiorTraceGroups_;
    using BaseType::active_local_boundaries_;
    using BaseType::primal_local_;
    using BaseType::adjoint_local_;
    using BaseType::parambuilder_;
    using BaseType::stol;
    using BaseType::local_error_;
    using BaseType::solveLocalPrimalProblem;
  };


  ///////////////////////
  // PRIMAL
  ///////////////////////

  // PDE
  Real u = 1.1*0; // Dirichlet bc only work with no advection
  AdvectiveFlux1D_Uniform adv(u);
  AdvectiveFlux1D_Uniform adj_adv(-u);

  Real kxx = 1;
  ViscousFlux1D_Uniform visc(kxx);

  Source1D_UniformGrad source(0.3*0, 0.7*0);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCTypeFunctionLinearRobin_mitLG>::params.Function.Quad;
//  solnArgs[SolutionExact::ParamsType::params.a] = 1;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction1D_MMS<PDEAdvectionDiffusion1D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  ///////////////////////
  // OUTPUT
  ///////////////////////

  NDOutputClass fcnOutput(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnOutput, {0}); // Assumes there's only 0 cellGroup
  OutputIntegrandClass errIntegrandLocal(fcnOutput, {0});

  ///////////////////////
  // BC
  ///////////////////////
  PyDict BCDirichlet;

  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD1,BCTypeDirichlet_mitStateParam>::params.qB] = 0;

  PyDict PyBCList;
  PyBCList["BCDirichlet"] = BCDirichlet;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Construct group vectors for the XField
  std::vector<int> dirichlet_btracegroup_list = {};
  for (int i = 0; i < xfld.nBoundaryTraceGroups(); i++)
    dirichlet_btracegroup_list.push_back(i);

  std::vector<int> cellGroups;
  for (int i = 0; i < xfld.nCellGroups(); i++)
    cellGroups.push_back(i);

  std::vector<int> interiorTraceGroups;
  for (int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDirichlet"] = dirichlet_btracegroup_list;

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups );

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Stabilization
  const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby,order+1);

  ///////////////////////
  // SOLVER DICTIONARIES
  ///////////////////////

  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  ///////////////////////
  // SOLUTION CLASS
  ///////////////////////

  SolutionClass globalSol( xfld, pde, order, order+1,
                           basis_category, basis_category,
                           active_boundaries, stab );

  ///////////////////////
  // SOLVER INTERFACE
  ///////////////////////

  const int quadOrder = 2*(order+1);
  std::vector<Real> tol = {1e-11, 1e-11};
  const ResidualNormType residNormType = ResidualNorm_L2;

  SolverInterfaceClass solverInt( globalSol, residNormType, tol, quadOrder, cellGroups, interiorTraceGroups,
                                  PyBCList, BCBoundaryGroups,
                                  SolverContinuationDict, LinearSolverDict,
                                  errIntegrandGlobal);

  ///////////////////////
  // GLOBAL COMPUTATIONS
  ///////////////////////

  globalSol.setSolution(0.0);
  solverInt.solveGlobalPrimalProblem();

//  output_Tecplot( globalSol.primal.qfld, "tmp/qfld_global.plt");

  typedef typename XField<PhysD1, TopoD1>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  const int nEdge = Line::NEdge; // Number of different edges

  typedef typename std::set<std::array<int,Line::NNode>> EdgeList;

  QuadratureOrder quadratureOrder( xfld, 2*order + 1 );


  ///////////////////////
  // LOOP OVER CELL GROUPS ->
  ///////////////////////
  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Line>::EdgeNodes;
  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    EdgeList edge_list, failed_edge_list;

    // edge_list is cleared with each loop over the groups
    XFieldCellGroupType xfldCellGroup = xfld.template getCellGroup<Line>(group);
    const int nElem = xfldCellGroup.nElem();

    ElementXFieldClass xfldElem(xfldCellGroup.basis() );

    ///////////////////////
    // EDGE LIST EXTRACTION
    ///////////////////////

    for ( int elem = 0; elem < nElem; elem++ )
    {
      xfldCellGroup.getElement(xfldElem,elem);
      std::array<int,Line::NNode> nodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,2> newEdge;
        newEdge[0] = MIN(nodeMap[(EdgeNodes[edge][0])],nodeMap[(EdgeNodes[edge][1])]);
        newEdge[1] = MAX(nodeMap[(EdgeNodes[edge][0])],nodeMap[(EdgeNodes[edge][1])]);

        edge_list.insert(newEdge);
      }
    }

    /////////////////////////////////////////////
    // EDGE LOCAL MESH CONSTRUCT
    /////////////////////////////////////////////

    const XField_CellToTrace<PhysD1,TopoD1> xfld_connectivity(xfld);
    const Field_NodalView nodalView(xfld, {group});

    typedef typename Field_NodalView::IndexVector IndexVector;

    for ( const std::array<int,Line::NNode>& edge : edge_list )
    {

      /////////////////////////////////////////////
      // EDGE LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

      IndexVector nodeGroup0, nodeGroup1;
      nodeGroup0 = nodalView.getCellList( edge[0] );
      nodeGroup1 = nodalView.getCellList( edge[1] );
//      std::cout << "===============" <<std::endl;

      // find the interesction of the two sets
      std::set<GroupElemIndex> attachedCells;
      typename std::set<GroupElemIndex>::iterator attachedCells_it;
      std::set_intersection( nodeGroup0.begin(), nodeGroup0.end(), nodeGroup1.begin(), nodeGroup1.end(),
                             std::inserter( attachedCells, attachedCells.begin() ) );
//      nodes = std::make_pair(1,2);

#ifdef DEBUG_FLAG
      std::cout<< "==================" << std::endl;
      std::cout<< "nodes = (" << nodes.first << "," << nodes.second <<")" <<std::endl;
      std::cout<< "==================" << std::endl;
#endif

      mpi::communicator world;
      mpi::communicator comm_local = world.split(world.rank());

      ArrayQ SqErrLocal = 0, SqErrLocal_resolve = 0;
      if (!isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL UNSPLIT CHECK
        /////////////////////////////////////////////

        XField_LocalPatchConstructor<PhysD1,Line> xfld_construct(comm_local,xfld_connectivity,edge,SpaceType::Continuous,&nodalView);
        XField_LocalPatch<PhysD1,Line> xfld_local(xfld_construct);

//        std::vector<int> tmp;
//        xfld_local.getReSolveCellGroups(tmp);
//        std::cout<< "cellGroups = ";
//        for (auto it = tmp.begin(); it != tmp.end(); ++it)
//          std::cout<< *it;
//        std::cout << std::endl;
//        xfld_local.getReSolveInteriorTraceGroups(tmp);
//        std::cout<< "ITGroups = ";
//        for (auto it = tmp.begin(); it != tmp.end(); ++it)
//          std::cout<< *it;
//        std::cout << std::endl;
//        xfld_local.getReSolveBoundaryTraceGroups(tmp);
//        std::cout<< "BTGroups = ";
//        for (auto it = tmp.begin(); it != tmp.end(); ++it)
//          std::cout<< *it;
//        std::cout << std::endl;

        LocalSolveStatus status(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem( solverInt, xfld_local, status);

//        output_Tecplot( localProblem.primal_local_.qfld, "tmp/qfld_local.plt");

//        std::cout << "qfld DOF pre = " ;
//        for (int i = 0; i < localProblem.primal_local_.qfld.nDOF(); i++)
//          std::cout << localProblem.primal_local_.qfld.DOF(i) << ", ";
//        std::cout << std::endl;

        // make a copy of the qfld
        QFieldType  qfld_local_pre(  localProblem.primal_local_.qfld,  FieldCopy() );
        LGFieldType lgfld_local_pre( localProblem.primal_local_.lgfld, FieldCopy() );

        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal ),
            xfld_local, qfld_local_pre, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

//        std::cout<< "unsplit" <<std::endl;

        // Re-solve the local problem
        localProblem.solveLocalPrimalProblem( localProblem.xfld_local_, localProblem.primal_local_ );

        BOOST_CHECK( status.converged );

//        output_Tecplot( localProblem.primal_local_.qfld, "tmp/qfld_local_post.plt");

//        std::cout << "qfld DOF post = " ;
//        for (int i = 0; i < localProblem.primal_local_.qfld.nDOF(); i++)
//          std::cout << localProblem.primal_local_.qfld.DOF(i) << ", ";
//        std::cout << std::endl;

        // Check that the re-solve DOFS have been unchanged.
        // This is because the mesh hasn't changed, so nothing should happen
        BOOST_CHECK_EQUAL( qfld_local_pre.nDOF(), localProblem.primal_local_.qfld.nDOF() );
        BOOST_CHECK_EQUAL( lgfld_local_pre.nDOF(), localProblem.primal_local_.lgfld.nDOF() );

        for ( int i = 0; i < qfld_local_pre.nDOF(); i++)
          SANS_CHECK_CLOSE( qfld_local_pre.DOF(i), localProblem.primal_local_.qfld.DOF(i), small_tol, close_tol );

        for (int i = 0; i < lgfld_local_pre.nDOF(); i++)
          SANS_CHECK_CLOSE( lgfld_local_pre.DOF(i), localProblem.primal_local_.lgfld.DOF(i), small_tol, close_tol );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal_resolve ),
            xfld_local, localProblem.primal_local_.qfld, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        // Nothing should change, but there is projection so might not be exact
        SANS_CHECK_CLOSE( SqErrLocal, SqErrLocal_resolve, small_tol, close_tol );
        if ( abs(SqErrLocal - SqErrLocal_resolve)>close_tol )
          failed_edge_list.insert( edge );

//        localProblem.primal_local_.qfld.dump();
//        localProblem.primal_local_.lgfld.dump();
      }
      else if (isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL SPLIT CHECK
        /////////////////////////////////////////////

        XField_LocalPatch<PhysD1,Line> xfld_local(comm_local,xfld_connectivity,edge,SpaceType::Continuous,&nodalView);
//        xfld_local.dump();

        LocalSolveStatus status_split(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem_split( solverInt, xfld_local, status_split);

        QFieldType qfld_local_split_pre( localProblem_split.primal_local_.qfld, FieldCopy() );

//        qfld_local_split_pre.dump();

//        output_Tecplot( localProblem_split.primal_local_.qfld, "tmp/qfld_local.plt");

//        std::cout << "qfld DOF pre = " ;
//        for (int i = 0; i < localProblem_split.primal_local_.qfld.nDOF(); i++)
//          std::cout << localProblem_split.primal_local_.qfld.DOF(i) << ", ";
//        std::cout << std::endl;

        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal ),
            xfld_local, qfld_local_split_pre, quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

//        std::cout<< ", split" <<std::endl;
        localProblem_split.solveLocalPrimalProblem( localProblem_split.xfld_local_, localProblem_split.primal_local_ );

        BOOST_CHECK( status_split.converged );

//        output_Tecplot( localProblem_split.primal_local_.qfld, "tmp/qfld_local_post.plt");

//        std::cout << "qfld DOF post = " ;
//        for (int i = 0; i < localProblem_split.primal_local.qfld.nDOF(); i++)
//          std::cout << localProblem_split.primal_local.qfld.DOF(i) << ", ";
//        std::cout << std::endl;

        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_Galerkin( errIntegrandLocal, SqErrLocal_resolve ),
            xfld_local, localProblem_split.primal_local_.qfld, quadratureOrder_local.cellOrders.data(),
                                                                     quadratureOrder_local.cellOrders.size() );

//        BOOST_CHECK( SqErrLocal - SqErrLocal_resolve + small_tol >= 0 );
//        if ( (SqErrLocal - SqErrLocal_resolve  + small_tol) < 0 )
//        {
//          std::cout<< "SqErrLocal - SqErrLocal_resolve = " << SqErrLocal -  SqErrLocal_resolve << std::endl;
//          std::cout<< "SqErrLocal = " << SqErrLocal << std::endl;
//          std::cout<< "SqErrLocal_resolve = " << SqErrLocal_resolve << std::endl;
//          failed_edge_list.insert( edge );
//        }
//        std::cout<< "lgfld.nDOF() = " << localProblem_split.primal_local_.lgfld.nDOF() << std::endl;
      }

//      /////////////////////////////////////////////
//      // EDGE LOCAL ESTIMATION CHECK
//      /////////////////////////////////////////////
//
//      // Check that the computed error matches the original. The solution should not have changed
//      for (attachedCells_it = attachedCells.begin(); attachedCells_it != attachedCells.end(); ++attachedCells_it)
//      {
//        // only handle elements in current group, others will get counted on the loop with that group
//        if (attachedCells_it->group == group ) // if the cell is in the current group
//        {
//         // Extract the
//
//          /////////////////////////////////////////////
//          // EDGE LOCAL ERROR ESTIMATE CHECK
//          /////////////////////////////////////////////
//        }
//      }
//      std::cout << "-----" << std::endl;

    }
#ifdef DEBUG_FLAG
    std::cout<< "failed_edges = ";
    for (auto it = failed_edge_list.begin(); it != failed_edge_list.end(); ++it)
      std::cout<< "(" << (*it)[0] << "," << (*it)[1] << "), ";
    std::cout << std::endl;
#endif
  }


}

// Basic Tests
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Lines_Hierarchical_Unsplit_P1 )
{
  int order = 1;
  XField1D xfld(5,0,5); //Linear mesh with 2 lines from 0 to 2
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "==========" << std::endl;
  std::cout<< "2 Triangle" << std::endl;
  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif


// Full Tests
#if 1

// UNSPLIT TESTS - Nothing should change
#if 1

// -----------------------------------------------------
//  Hierarchical
// -----------------------------------------------------

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Hierarchical_P1 )
{
  int order = 1;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Hierarchical_P2 )
{
  int order = 2;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Hierarchical_P3 )
{
  int order = 3;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Lagrange_P1 )
{
  int order = 1;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Lagrange_P2 )
{
  int order = 2;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Lagrange_P3 )
{
  int order = 3;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#endif

// SPLIT TESTS - Things should get better
#if 1

// -----------------------------------------------------
//  Hierarchical
// -----------------------------------------------------

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Hierarchical_Split_P1 )
{
  int order = 1;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Hierarchical_Split_P2 )
{
  int order = 2;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Hierarchical_Split_P3 )
{
  int order = 3;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Hierarchical, small_tol, close_tol, split_test );

}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Lagrange_Split_P1 )
{
  int order = 1;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Lagrange_Split_P2 )
{
  int order = 2;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve1D_Galerkin_AD_5Line_Lagrange_Split_P3 )
{
  int order = 3;
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Lagrange, small_tol, close_tol, split_test );

}
#endif

#endif


#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
