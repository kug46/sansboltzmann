// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LocalSolve_Split_HDG_Triangle_AD_btest
// testing of 2-D HDG local solves with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/AlgebraicEquationSet_Local_HDG.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/Local/Field_Local.h"
#include "Field/Local/XField_LocalPatch.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"


#include "AlgebraicEquationSet_FullLocal_HDG.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( LocalSolve2D_Split_HDG_Triangle_AD_test_suite )

std::vector<Real>
localsolve(const XField<PhysD2, TopoD2>& xfld, const int order, const int main_group, const int main_cell,
           const ElementSplitType split_type, const int split_edge_index,
           const std::vector<int>& global_itracegroup_list, const std::vector<int>& global_btracegroup_list,
           const std::vector<int>& local_itracegroup_list)
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_DG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_DG_Cell;
  typedef Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> AField2D_DG_Cell;
  typedef Field_DG_Trace<PhysD2, TopoD2, ArrayQ> QIField2D_DG_Trace;
  typedef Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_DG_Trace;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> AlgEqnSet_Global_Sparse;
  typedef testspace::AlgebraicEquationSet_FullLocal_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                                        AlgEqSetTraits_Dense, XField<PhysD2, TopoD2>> AlgEqnSet_FullLocal;
  typedef AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Dense, XField<PhysD2, TopoD2>> AlgEqnSet_Local;
  typedef AlgEqnSet_Global_Sparse::BCParams BCParams;

  typedef AlgEqnSet_Global_Sparse::SystemMatrix SystemMatrixClass;
  typedef AlgEqnSet_Global_Sparse::SystemVector SystemVectorClass;

  typedef AlgEqnSet_FullLocal::SystemMatrix SystemMatrixClassD;
  typedef AlgEqnSet_FullLocal::SystemVector SystemVectorClassD;
  typedef AlgEqnSet_FullLocal::SystemNonZeroPattern SystemNonZeroPatternD;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  // PDE
  Real a = 1.0;
  Real b = 1.0;
  AdvectiveFlux2D_Uniform adv( a, b );

  Real nu = 0.273;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0., 0., 0.);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  NDPDEClass pde( adv, visc, source );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;

#if 0
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.A] = 0.7;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0.4;
#else
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
         BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Robin;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;
#endif

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputSolutionErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD2, OutputSolutionErrorSquaredClass> NDOutputSolutionErrorSquaredClass;
  typedef IntegrandCell_HDG_Output<NDOutputSolutionErrorSquaredClass> IntegrandSquareErrorClass;

  NDOutputSolutionErrorSquaredClass outErr( solnExact );
  IntegrandSquareErrorClass fcnSqErr( outErr, {0} );
  IntegrandSquareErrorClass fcnSqErr_local( outErr, {0,1} );

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  // solution
  QField2D_DG_Cell qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // auxiliary operator
  AField2D_DG_Cell afld(xfld, order, BasisFunctionCategory_Legendre);
  afld = 0;

  // interface
  QIField2D_DG_Trace qIfld(xfld, order, BasisFunctionCategory_Legendre);
  qIfld = 0;

  //Lagrange multiplier
  std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
  LGField2D_DG_Trace lgfld( xfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list );
  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> eqnsettol = {1e-12, 1e-12, 1e-12};

  AlgEqnSet_Global_Sparse PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, eqnsettol,
                                      {0}, global_itracegroup_list, PyBCList, BCBoundaryGroups);

  // residual
  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
  PrimalEqSet.fillSystemVector(q);

  rsd = 0;
  PrimalEqSet.residual(q, rsd);

#if 0 //Print residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < rsd[0].m(); k++) cout << k << "\t" << rsd[0][k] <<endl;

  cout <<endl << "INT residual:" <<endl;
  for (int k = 0; k < rsd[1].m(); k++) cout << k << "\t" << rsd[1][k] <<endl;

  cout <<endl << "BC residual:" <<endl;
  for (int k = 0; k < rsd[2].m(); k++) cout << k << "\t" << rsd[2][k] << endl;
#endif

  // solve
  SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);

  SystemVectorClass dq(PrimalEqSet.vectorStateSize());
  solver.solve(rsd, dq);

  // update solution
  q -= dq;
  PrimalEqSet.setSolutionField(q);

  rsd = 0;
  PrimalEqSet.residual(rsd);
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

  //Check if residual is zero for global solution
  BOOST_REQUIRE( PrimalEqSet.convergedResidual(rsdNorm) );

  //Compute square error on global mesh
  ArrayQ SqErrGlobal = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_HDG( fcnSqErr, SqErrGlobal ), xfld, (qfld, afld),
      quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );


  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  //Extract the local mesh for the given element
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_constructor(comm_local, connectivity, main_group, main_cell, SpaceType::Discontinuous);

  // create an unsplit patch
  XField_LocalPatch<PhysD2,Triangle> xfld_local( xfld_constructor, split_type, split_edge_index);

  std::vector<int> active_BGroup_list_local = {}; //Using mitState - so lgfld should be empty

  int nLocalInteriorTraceGroups = std::min(xfld_local.nInteriorTraceGroups(), 2);
  std::vector<int> interiorTraceGroups(nLocalInteriorTraceGroups);
  for (int i = 0; i < nLocalInteriorTraceGroups; i++)
    interiorTraceGroups[i] = i;

  int nLocalBoundaryTraceGroups = xfld_local.nBoundaryTraceGroups();
  std::vector<int> boundaryTraceGroups(nLocalBoundaryTraceGroups);
  for (int i = 0; i < nLocalBoundaryTraceGroups; i++)
    boundaryTraceGroups[i] = i;

  Field_Local<QField2D_DG_Cell> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);
  Field_Local<AField2D_DG_Cell> afld_local(xfld_local, afld, order, BasisFunctionCategory_Legendre);
  Field_Local<QIField2D_DG_Trace> qIfld_local(xfld_local, qIfld, order, BasisFunctionCategory_Legendre, interiorTraceGroups, boundaryTraceGroups);
  Field_Local<LGField2D_DG_Trace> lgfld_local(xfld_local, lgfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list_local);

  const int nMainCells = qfld_local.getCellGroupBase(main_group).nElem();
  const int nDOF_per_cell = qfld_local.nDOF()/qfld_local.nElem();
  const int nDOF_per_itrace = qIfld_local.nDOF()/qIfld_local.nElem();
  const int nDOF_per_btrace = nDOF_per_itrace;

  int nMainBTraces = 0;
  std::vector<int> MainBTraceGroup_list;

  for (int i=0; i < qIfld_local.nBoundaryTraceGroups(); i++)
  {
    MainBTraceGroup_list.push_back(i);
    nMainBTraces += qIfld_local.getBoundaryTraceGroupBase(i).nElem();
  }

  int nMainITraces = 0;
  for (int i = 0; i < qIfld_local.nInteriorTraceGroups(); i++)
    nMainITraces += qIfld_local.getInteriorTraceGroupBase(i).nElem();

//  std::cout<<"nMainCells:"<<nMainCells<<", nDOF_per_cell:"<< nDOF_per_cell<<std::endl;
//  std::cout<<"nMainITraces:"<<nMainITraces<<", nDOF_per_trace:"<< nDOF_per_itrace<<std::endl;
//  std::cout<<"nMainBTraces:"<<nMainBTraces<<", nDOF_per_trace:"<< nDOF_per_btrace<<std::endl;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_local["BCName"] = MainBTraceGroup_list;

  QuadratureOrder quadratureOrder_local( xfld_local, -1 );

  // Compute square error on split mesh before local solve
  ArrayQ SqErrLocal = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_HDG( fcnSqErr_local, SqErrLocal ), xfld_local, (qfld_local, afld_local),
      quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

#if 0
   xfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   xfld_local.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
#endif

#if 0
//   qfld.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;
//   qfld_local.dump(3,std::cout);
//
//   afld.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;
//   afld_local.dump(3,std::cout);

   qIfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   qIfld_local.dump(3,std::cout);

//   lgfld.dump(3,std::cout);
//   std::cout<<std::endl<<std::endl;
//   lgfld_local.dump(3,std::cout);
#endif

   AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, afld_local, qIfld_local, lgfld_local,
                                     pde, disc, quadratureOrder_local, eqnsettol,
                                     {0,1}, local_itracegroup_list, PyBCList, BCBoundaryGroups_local);

   AlgEqnSet_FullLocal PrimalEqSet_Full(xfld_local, qfld_local, afld_local, qIfld_local, lgfld_local,
                                        pde, disc, quadratureOrder_local, eqnsettol,
                                        {0,1}, local_itracegroup_list, PyBCList, BCBoundaryGroups_local);

   // Local residual
   SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
   SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
   PrimalEqSet_Local.fillSystemVector(q_local);

   rsd_local = 0;
   PrimalEqSet_Local.residual(q_local, rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "INT residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;

  cout <<endl << "BC residual:" <<endl;
  for (int k = 0; k < rsd_local[2].m(); k++) cout << k << "\t" << rsd_local[2][k] << endl;
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClass_Local jac_local(nz_local);
  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

#if 0
  for (int i = 0; i < jac_local.m(); i++)
    for (int j = 0; j < jac_local.n(); j++)
    {
      std::cout <<"("<< i << "," << j << ")" <<std::endl;
      //std::string filename = "tmp/jac" + std::to_string(i) + std::to_string(j) + ".mtx";
      //fstream fout(filename.c_str(), fstream::out);
      jac_local(i,j).dump(0);
      //for (int ii = 0; ii < jac_local(i,j).m(); ii++)
      //  for (int jj = 0; jj < jac_local(i,j).n(); jj++)
      //    if ( jac_local(i,j)(ii,jj) != 0 )
      //      std::cout << ii << " " << jj << std::endl;
    }
#endif

  //Local solve - with extracted sub-system
  SystemVectorClass_Local dq_local(PrimalEqSet_Local.vectorStateSize());
  dq_local = DLA::InverseLUP::Solve(jac_local,rsd_local);

  //------------------------------------------------------------------------------------------

  //Set up linear subsystem
  int sub_qfld_nDOF = nMainCells*nDOF_per_cell; //Re-solving for only main-cell DOFs in local mesh
  int sub_qIfld_nDOF = nMainITraces*nDOF_per_itrace + nMainBTraces*nDOF_per_btrace; //Re-solving for all main Itrace/Btrace DOFs in local mesh
  int sub_lgfld_nDOF = 0; //Using mitState - so lgfld should be empty

  BOOST_CHECK_EQUAL( q_local[0].m(), sub_qfld_nDOF );
  BOOST_CHECK_EQUAL( q_local[1].m(), sub_qIfld_nDOF );
  BOOST_CHECK_EQUAL( q_local[2].m(), sub_lgfld_nDOF );

  SystemVectorClassD q_full_local(PrimalEqSet_Full.vectorStateSize());
  SystemVectorClassD rsd_full_local(PrimalEqSet_Full.vectorEqSize());
  PrimalEqSet_Full.fillSystemVector(q_full_local);

  rsd_full_local = 0.0;
  PrimalEqSet_Full.residual(q_full_local, rsd_full_local);

  SystemNonZeroPatternD nz_full_local(PrimalEqSet_Full.matrixSize());
  PrimalEqSet_Full.jacobian(nz_full_local);

  SystemMatrixClassD jac_full_local(nz_full_local);
  jac_full_local = 0.0;
  PrimalEqSet_Full.jacobian(jac_full_local);

#if 0
  for (int i = 0; i < jac_sublocal.m(); i++)
    for (int j = 0; j < jac_sublocal.n(); j++)
    {
      std::cout<<jac_sublocal(i,j).m()<<","<<jac_sublocal(i,j).n()<<std::endl;
      std::string filename = "tmp/jac" + std::to_string(i) + std::to_string(j) + ".mtx";
      fstream fout(filename.c_str(), fstream::out);
      jac_sublocal(i,j).dump(0,fout);
    }
#endif

  //Local solve - full local system (with identity for unwanted DOFs) for comparison
  SystemVectorClassD dq_full_local(PrimalEqSet_Full.vectorStateSize());
  dq_full_local = 0.0;
  testspace::solveFullLocalSystem(rsd_full_local, jac_full_local,
                                  sub_qfld_nDOF, sub_qIfld_nDOF, sub_lgfld_nDOF,
                                  dq_full_local);

//  dq_local.dump(3,std::cout);
//  dq_full_local.dump(3,std::cout);

  //------------------------------------------------------------------------------------------

  Real small_tol = 1e-9;
  Real close_tol = 1e-7;

  const int iq  = PrimalEqSet_Full.iq;
  const int iqI = PrimalEqSet_Full.iqI;
  const int ilg = PrimalEqSet_Full.ilg;

  //Check if the two local solutions are equal
  for (int k = 0; k < sub_qfld_nDOF; k++)
    SANS_CHECK_CLOSE( dq_full_local[iq][k], dq_local[iq][k], small_tol, close_tol );

  for (int k = 0; k < sub_qIfld_nDOF; k++)
    SANS_CHECK_CLOSE( dq_full_local[iqI][k], dq_local[iqI][k], small_tol, close_tol );

  for (int k = 0; k < sub_lgfld_nDOF; k++)
    SANS_CHECK_CLOSE( dq_full_local[ilg][k], dq_local[ilg][k], small_tol, close_tol );


  //Update local solution fields
  q_local -= dq_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  //Re-compute residual and check if it's zero
  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);

//  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);
//  PrimalEqSet_Local.printDecreaseResidualFailure(rsdNorm, std::cout);

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNorm) );

  //Compute square error on split mesh after local solve
  ArrayQ SqErrLocalSplit = 0;
  IntegrateCellGroups<TopoD2>::integrate(
      FunctionalCell_HDG( fcnSqErr_local, SqErrLocalSplit ), xfld_local, (qfld_local, afld_local),
      quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

  return {SqErrGlobal, SqErrLocal, SqErrLocalSplit};
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_Split_HDG_AD_Box_Triangle_CornerCell_Target_SplitEdge0_P2 )
{
  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;

  int main_group = 0;
  int main_cell = 0;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 0;

  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_Split_HDG_AD_Box_Triangle_CornerCell_Target_SplitIsotropic_P2 )
{
  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;

  int main_group = 0;
  int main_cell = 0;
  ElementSplitType split_type = ElementSplitType::Isotropic;
  int split_edge_index = -1;

  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_Split_HDG_AD_Box_Triangle_InteriorCell_Target_SplitEdge1_P1 )
{
  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;

  int main_group = 0;
  int main_cell = 1;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 1;

  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_Split_HDG_AD_Box_Triangle_InteriorCell_Target_SplitIsotropic_P1 )
{
  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;

  int main_group = 0;
  int main_cell = 1;
  ElementSplitType split_type = ElementSplitType::Isotropic;
  int split_edge_index = -1;

  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_Split_HDG_AD_Box_Triangle_BoundaryCell_Target_SplitEdge2_P2 )
{
  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;

  int main_group = 0;
  int main_cell = 2;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 2;

  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_Split_HDG_AD_Box_Triangle_BoundaryCell_Target_SplitIsotropic_P2 )
{
  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;

  int main_group = 0;
  int main_cell = 2;
  ElementSplitType split_type = ElementSplitType::Isotropic;
  int split_edge_index = -1;

  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_Split_HDG_AD_2Triangle_LeftCell_Target_SplitEdge0_P1 )
{
  XField2D_2Triangle_X1_1Group xfld;

  int order = 1;

  int main_group = 0;
  int main_cell = 0;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 0;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  //Global and local errors (before local solve) should be equal since the local mesh is equal to the global mesh.
  BOOST_CHECK_CLOSE( errors[0], errors[1], 1e-10 );

  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_Split_HDG_AD_2Triangle_LeftCell_Target_SplitIsotropic_P1 )
{
  XField2D_2Triangle_X1_1Group xfld;

  int order = 1;

  int main_group = 0;
  int main_cell = 0;
  ElementSplitType split_type = ElementSplitType::Isotropic;
  int split_edge_index = -1;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  //Global and local errors (before local solve) should be equal since the local mesh is equal to the global mesh.
  BOOST_CHECK_CLOSE( errors[0], errors[1], 1e-10 );

  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_Split_HDG_AD_2Triangle_RightCell_Target_SplitEdge1_P2 )
{
  XField2D_2Triangle_X1_1Group xfld;

  int order = 2;

  int main_group = 0;
  int main_cell = 1;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 1;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  //Global and local errors (before local solve) should be equal since the local mesh is equal to the global mesh.
  BOOST_CHECK_CLOSE( errors[0], errors[1], 1e-10 );

  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve2D_Split_HDG_AD_2Triangle_RightCell_Target_SplitIsotropic_P2 )
{
  XField2D_2Triangle_X1_1Group xfld;

  int order = 2;

  int main_group = 0;
  int main_cell = 1;
  ElementSplitType split_type = ElementSplitType::Isotropic;
  int split_edge_index = -1;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  //Global and local errors (before local solve) should be equal since the local mesh is equal to the global mesh.
  BOOST_CHECK_CLOSE( errors[0], errors[1], 1e-10 );

  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}
#endif
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
