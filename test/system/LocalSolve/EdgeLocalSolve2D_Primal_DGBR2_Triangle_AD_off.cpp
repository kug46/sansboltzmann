// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// EdgeLocalSolve_DGBR2_Triangle_Legendre_AD_btest
// testing of 2-D DG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <cmath>
#include <ostream>
#include <fstream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionGradientErrorSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/XField_EdgeLocal.h"

#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/SolutionData_DGBR2.h"
#include "Adaptation/MOESS/SolverInterface_DGBR2.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "tools/minmax.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Prints out a lot of useful information
//#define DEBUG_FLAG


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( EdgeLocalSolve2D_Primal_DGBR2_Triangle_Legendre_AD_test_suite )

// A helper function to construct a Solver interface and test all the edges on a grid
void localExtractAndTest(const XField<PhysD2, TopoD2>& xfld, const int order, const BasisFunctionCategory basis_category,
                         const Real small_tol, const Real close_tol, const bool isSplit )
{
  typedef ScalarFunction2D_SineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  // Manufactured Solution Adjoints

  typedef OutputCell_SolutionErrorSquared<PDEAdvectionDiffusion2D,NDSolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef BCParameters<BCVector> BCParams;
  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGBR2<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGBR2<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Pieces for the debugging
  typedef typename SolverInterfaceClass::LocalProblem LocalProblem;

  typedef typename SolverInterfaceClass::ArrayQ ArrayQ;
  typedef typename SolverInterfaceClass::VectorArrayQ VectorArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QFieldType;
  typedef FieldLift_DG_Cell< PhysD2, TopoD2, VectorArrayQ > RFieldType;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > LGFieldType;

  // A debugging class for exposing protected members
  struct debugLocalProblem : public LocalProblem
  {
    typedef LocalProblem BaseType;

    debugLocalProblem( const SolverInterfaceClass& owner, XField_Local_Base<PhysD2,TopoD2>& xfld_local,
                       LocalSolveStatus& status) : BaseType(owner,xfld_local,status) {}

    using BaseType::interface_;
    using BaseType::xfld_local_;
    using BaseType::status_;
    using BaseType::quadrule_;
    using BaseType::maxIter;
    using BaseType::BCList_local_;
    using BaseType::BCBoundaryGroups_local_;
    using BaseType::local_cellGroups_;
    using BaseType::local_interiorTraceGroups_;
    using BaseType::active_local_boundaries_;
    using BaseType::primal_local_;
    using BaseType::adjoint_local_;
    using BaseType::parambuilder_;
    using BaseType::stol;
    using BaseType::local_error_;
    using BaseType::solveLocalPrimalProblem;
  };


  ///////////////////////
  // PRIMAL
  ///////////////////////

  // PDE
  Real u = 0.3;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv(u, v);

  Real kxx = 1.2, kxy = 0.0;
  Real kyx = 0.0, kyy = 1.3;
  ViscousFlux2D_Uniform visc(kxx, kxy,
                             kyx, kyy);

  Source2D_UniformGrad source(0.3*0, 0.5*0, 0.7*0);

  // Create a solution dictionary

  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function.SineSine;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  ///////////////////////
  // OUTPUT
  ///////////////////////

  NDOutputClass fcnOutput(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnOutput, {0}); // Assumes there's only 0 cellGroup
  OutputIntegrandClass errIntegrandLocal(fcnOutput, {0});

  ///////////////////////
  // BC
  ///////////////////////
  PyDict BCDirichlet, BCNeumann;

  PyDict BCSoln_Dirichlet;
  BCSoln_Dirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function] = solnArgs;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.A] = 1;
  BCSoln_Dirichlet[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0;

  PyDict BCSoln_Neumann;
  BCSoln_Neumann[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function] = solnArgs;
  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.A] = 0;
  BCSoln_Neumann[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.B] = 1;

  BCDirichlet[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0.0;
  BCDirichlet[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  BCNeumann[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 0.0;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 1.0;
  BCNeumann[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.0;

  PyDict PyBCList;
  PyBCList["BCDirichlet"] = BCDirichlet;
//  PyBCList["BCDirichlet0"] = BCDirichlet;
//  PyBCList["BCDirichlet1"] = BCDirichlet;
//  PyBCList["BCDirichlet2"] = BCDirichlet;
//  PyBCList["BCDirichlet3"] = BCDirichlet;
  PyBCList["BCNeumann"] = BCSoln_Neumann;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Construct group vectors for the XField
  std::vector<int> dirichlet_btracegroup_list = {};
  for (int i = 0; i < xfld.nBoundaryTraceGroups(); i++)
    dirichlet_btracegroup_list.push_back(i);

  std::vector<int> neumann_btracegroup_list= {};

  std::vector<int> cellGroups;
  for (int i = 0; i < xfld.nCellGroups(); i++)
    cellGroups.push_back(i);

  std::vector<int> interiorTraceGroups;
  for (int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDirichlet"] = dirichlet_btracegroup_list;
//  BCBoundaryGroups["BCDirichlet0"] = {0};
//  BCBoundaryGroups["BCDirichlet1"] = {1};
//  BCBoundaryGroups["BCDirichlet2"] = {2};
//  BCBoundaryGroups["BCDirichlet3"] = {3};
  BCBoundaryGroups["BCNeumann"]   = neumann_btracegroup_list;

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups( PyBCList, BCBoundaryGroups );

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Stabilization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc( 0, viscousEtaParameter );


  ///////////////////////
  // SOLVER DICTIONARIES
  ///////////////////////

  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  ///////////////////////
  // SOLUTION CLASS
  ///////////////////////

  SolutionClass globalSol( xfld, pde, order, order+1,
                           basis_category, basis_category,
                           active_boundaries, disc );

  ///////////////////////
  // SOLVER INTERFACE
  ///////////////////////

  const int quadOrder = -1;
  std::vector<Real> tol = {1e-11, 1e-11};
  const ResidualNormType residNormType = ResidualNorm_L2;

  SolverInterfaceClass solverInt( globalSol, residNormType, tol, quadOrder, cellGroups, interiorTraceGroups,
                                  PyBCList, BCBoundaryGroups,
                                  SolverContinuationDict, LinearSolverDict,
                                  errIntegrandGlobal);

  ///////////////////////
  // GLOBAL COMPUTATIONS
  ///////////////////////

  globalSol.setSolution(0.0);
  solverInt.solveGlobalPrimalProblem();

  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  const int nEdge = Triangle::NTrace; // Number of different edges

  typedef typename std::set<std::array<int,Line::NNode>> EdgeList;

  QuadratureOrder quadratureOrder( xfld, 2*order + 1 );

  int comm_rank = xfld.comm()->rank();
  mpi::communicator comm_local = xfld.comm()->split(comm_rank);

  ///////////////////////
  // LOOP OVER CELL GROUPS ->
  ///////////////////////
  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Triangle>::EdgeNodes;
  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    EdgeList edge_list, failed_edge_list;
    typename EdgeList::iterator edge_list_it;

    // edge_list is cleared with each loop over the groups
    XFieldCellGroupType xfldCellGroup = xfld.template getCellGroup<Triangle>(group);
    const int nElem = xfldCellGroup.nElem();

    ElementXFieldClass xfldElem(xfldCellGroup.basis() );

    ///////////////////////
    // EDGE LIST EXTRACTION
    ///////////////////////

    for ( int elem = 0; elem < nElem; elem++ )
    {
      xfldCellGroup.getElement(xfldElem,elem);
      std::array<int,Triangle::NNode> nodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,2> newEdge;
        newEdge[0] = MIN(nodeMap[(EdgeNodes[edge][0])],nodeMap[(EdgeNodes[edge][1])]);
        newEdge[1] = MAX(nodeMap[(EdgeNodes[edge][0])],nodeMap[(EdgeNodes[edge][1])]);

        edge_list.insert(newEdge);
      }
    }

    /////////////////////////////////////////////
    // EDGE LOCAL MESH CONSTRUCT
    /////////////////////////////////////////////

    const XField_CellToTrace<PhysD2,TopoD2> xfld_connectivity(xfld);
    const Field_NodalView nodalView(xfld, {group});

    typedef typename Field_NodalView::IndexVector IndexVector;

    for ( edge_list_it = edge_list.begin(); edge_list_it != edge_list.end(); ++edge_list_it)
    {

      /////////////////////////////////////////////
      // EDGE LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

      IndexVector nodeGroup0, nodeGroup1;
      nodeGroup0 = nodalView.getCellList( (*edge_list_it)[0] );
      nodeGroup1 = nodalView.getCellList( (*edge_list_it)[1] );
//      std::cout << "===============" <<std::endl;

      // find the interesction of the two sets
      std::set<GroupElemIndex> attachedCells;
      typename std::set<GroupElemIndex>::iterator attachedCells_it;
      std::set_intersection( nodeGroup0.begin(), nodeGroup0.end(), nodeGroup1.begin(), nodeGroup1.end(),
                             std::inserter( attachedCells, attachedCells.begin() ) );

      std::pair<int,int> nodes;
      nodes = std::make_pair((*edge_list_it)[0],(*edge_list_it)[1]);
//      nodes = std::make_pair(1,2);

#ifdef DEBUG_FLAG
//      std::cout<< "==================" << std::endl;
//      std::cout<< "nodes = (" << nodes.first << "," << nodes.second <<")" <<std::endl;
//      std::cout<< "==================" << std::endl;
#endif

      XField_EdgeLocal<PhysD2,TopoD2> xfld_local(comm_local, xfld_connectivity, nodalView, nodes, Neighborhood::Primal, isSplit );

      ArrayQ SqErrLocal = 0, SqErrLocal_resolve = 0;
      if (!isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL UNSPLIT CHECK
        /////////////////////////////////////////////

        LocalSolveStatus status(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem( solverInt, xfld_local, status);

        // make a copy of the qfld
        QFieldType  qfld_local_pre(  localProblem.primal_local_.qfld,  FieldCopy() );
        RFieldType  rfld_local_pre(  localProblem.primal_local_.rfld,  FieldCopy() );
        LGFieldType lgfld_local_pre( localProblem.primal_local_.lgfld, FieldCopy() );

        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_DGBR2( errIntegrandLocal, SqErrLocal ),
            xfld_local, (qfld_local_pre, rfld_local_pre) , quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

//        std::cout<< "unsplit" <<std::endl;

        // Re-solve the local problem
        localProblem.solveLocalPrimalProblem( localProblem.xfld_local_, localProblem.primal_local_ );

        BOOST_CHECK( status.converged );

        // Check that the re-solve DOFS have been unchanged.
        // This is because the mesh hasn't changed, so nothing should happen
        BOOST_CHECK_EQUAL( qfld_local_pre.nDOF(), localProblem.primal_local_.qfld.nDOF() );
        BOOST_CHECK_EQUAL( rfld_local_pre.nDOF(), localProblem.primal_local_.rfld.nDOF() );
        BOOST_CHECK_EQUAL( lgfld_local_pre.nDOF(), localProblem.primal_local_.lgfld.nDOF() );
        if ( !( basis_category == BasisFunctionCategory_Hierarchical && order >= 3) )
        {
          for ( int i = 0; i < qfld_local_pre.nDOF(); i++)
            SANS_CHECK_CLOSE( qfld_local_pre.DOF(i), localProblem.primal_local_.qfld.DOF(i), 1e-7, 1e-7 );

          for ( int i = 0; i < rfld_local_pre.nDOF(); i++)
            for (int j = 0; j < PhysD2::D; j++)
              SANS_CHECK_CLOSE( rfld_local_pre.DOF(i)[j], localProblem.primal_local_.rfld.DOF(i)[j], 5e-6, 5e-6 );

          for (int i = 0; i < lgfld_local_pre.nDOF(); i++)
            SANS_CHECK_CLOSE( lgfld_local_pre.DOF(i), localProblem.primal_local_.lgfld.DOF(i), 5e-8, 5e-8 );
        }


        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_DGBR2( errIntegrandLocal, SqErrLocal_resolve ),
            xfld_local, ( localProblem.primal_local_.qfld, localProblem.primal_local_.rfld ),
            quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        // Nothing should change, but there is projection so might not be exact
        SANS_CHECK_CLOSE( SqErrLocal, SqErrLocal_resolve, small_tol, close_tol );
        if ( abs(SqErrLocal - SqErrLocal_resolve)>close_tol )
          failed_edge_list.insert( *edge_list_it );

//        localProblem.primal_local_.qfld.dump();
//        localProblem.primal_local_.lgfld.dump();
      }
      else if (isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL SPLIT CHECK
        /////////////////////////////////////////////

        LocalSolveStatus status_split(false,10);
        // A debugging version of the Local Problem class
        // Exposes protected members
        debugLocalProblem localProblem_split( solverInt, xfld_local, status_split);

        QFieldType qfld_local_split_pre( localProblem_split.primal_local_.qfld, FieldCopy() );
        RFieldType rfld_local_split_pre( localProblem_split.primal_local_.rfld, FieldCopy() );

//        output_Tecplot( localProblem_split.primal_local_.qfld, "tmp/qfld_local.plt");

//        std::cout << "qfld DOF pre = " ;
//        for (int i = 0; i < localProblem_split.primal_local_.qfld.nDOF(); i++)
//          std::cout << localProblem_split.primal_local_.qfld.DOF(i) << ", ";
//        std::cout << std::endl;

        QuadratureOrder quadratureOrder_local( xfld_local, 2*order + 1 );

        // Compute square error on split mesh before local solve
        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_DGBR2( errIntegrandLocal, SqErrLocal ),
            xfld_local, ( qfld_local_split_pre, rfld_local_split_pre ),
            quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

//        std::cout<< ", split" <<std::endl;
        localProblem_split.solveLocalPrimalProblem( localProblem_split.xfld_local_, localProblem_split.primal_local_ );

        BOOST_CHECK( status_split.converged );

//        output_Tecplot( localProblem_split.primal_local_.qfld, "tmp/qfld_local_post.plt");

//        std::cout << "qfld DOF post = " ;
//        for (int i = 0; i < localProblem_split.primal_local.qfld.nDOF(); i++)
//          std::cout << localProblem_split.primal_local.qfld.DOF(i) << ", ";
//        std::cout << std::endl;

        IntegrateCellGroups<TopoD2>::integrate(
            FunctionalCell_DGBR2( errIntegrandLocal, SqErrLocal_resolve ),
            xfld_local, ( localProblem_split.primal_local_.qfld, localProblem_split.primal_local_.rfld ),
            quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

        BOOST_CHECK_GE( SqErrLocal, SqErrLocal_resolve );
        if ( SqErrLocal < SqErrLocal_resolve )
        {
#ifdef DEBUG_FLAG
          std::cout<< "SqErrLocal - SqErrLocal_resolve = " << SqErrLocal -  SqErrLocal_resolve << std::endl;
#endif
          failed_edge_list.insert( *edge_list_it );
        }
//        std::cout<< "lgfld.nDOF() = " << localProblem_split.primal_local_.lgfld.nDOF() << std::endl;
      }

//      /////////////////////////////////////////////
//      // EDGE LOCAL ESTIMATION CHECK
//      /////////////////////////////////////////////
//
//      // Check that the computed error matches the original. The solution should not have changed
//      for (attachedCells_it = attachedCells.begin(); attachedCells_it != attachedCells.end(); ++attachedCells_it)
//      {
//        // only handle elements in current group, others will get counted on the loop with that group
//        if (attachedCells_it->group == group ) // if the cell is in the current group
//        {
//         // Extract the
//
//          /////////////////////////////////////////////
//          // EDGE LOCAL ERROR ESTIMATE CHECK
//          /////////////////////////////////////////////
//        }
//      }
//      std::cout << "-----" << std::endl;
//      return;
    }
#ifdef DEBUG_FLAG
    std::cout<< "failed_edges = ";
    for (auto it = failed_edge_list.begin(); it != failed_edge_list.end(); ++it)
      std::cout<< "(" << (*it)[0] << "," << (*it)[1] << "), ";
    std::cout << std::endl;
#endif
  }


}

#if 1 // These tests are all turned off, because right now, the Primal BCs are just wrong


// Basic Tests
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_2Triangle_Legendre_Unsplit_P1 )
{
  int order = 1;
  XField2D_2Triangle_X1_1Group xfld;
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "==========" << std::endl;
  std::cout<< "2 Triangle" << std::endl;
  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );

}
#endif


#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_4Triangle_Legendre_Unsplit_P1 )
{
  int order = 1;
  XField2D_4Triangle_X1_1Group xfld;
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "==========" << std::endl;
  std::cout<< "4 Triangle" << std::endl;
  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_4Triangle_Legendre_Split_P1 )
{
  int order = 1;
  XField2D_4Triangle_X1_1Group xfld;
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "==========" << std::endl;
  std::cout<< "4 Triangle" << std::endl;
  std::cout<< "==========" << std::endl;
  std::cout<< "order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );

}
#endif

// Full Tests

// UNSPLIT TESTS - Nothing should change
#if 1

// -----------------------------------------------------
//  Legendre
// -----------------------------------------------------

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_1Box_Triangle_Legendre_P1 )
{
  int ii = 2, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
//  std::cout<< "==========" << std::endl;
//  std::cout<< "Union Jack" << std::endl;
//  std::cout<< "==========" << std::endl;
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_1Box_Triangle_Legendre_P2 )
{
  int ii = 2, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_1Box_Triangle_Legendre_P3 )
{
  int ii = 2, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_4Box_Triangle_Legendre_P1 )
{
  int ii = 4, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_4Box_Triangle_Legendre_P2 )
{
  int ii = 4, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_4Box_Triangle_Legendre_P3 )
{
  int ii = 4, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_9Box_Triangle_Legendre_P1 )
{
  int ii = 6, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_9Box_Triangle_Legendre_P2 )
{
  int ii = 6, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_9Box_Triangle_Legendre_P3 )
{
  int ii = 6, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 5e-9, close_tol = 5e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_16Box_Triangle_Legendre_P1 )
{
  int ii = 8, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_16Box_Triangle_Legendre_P2 )
{
  int ii = 8, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_16Box_Triangle_Legendre_P3 )
{
  int ii = 8, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = false;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#endif

// =====================================================
// SPLIT TESTS - Check that energy norm error (for Poisson) decreases
// =====================================================

/*
 * These should be uncommented when a local solve that always reduces the energy norm is devised -- Hugh
 */
#if 0

// -----------------------------------------------------
//  Legendre
// -----------------------------------------------------

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_1Box_Split_Triangle_Legendre_P1 )
{
  int ii = 2, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_1Box_Split_Triangle_Legendre_P2 )
{
  int ii = 2, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_1Box_Split_Triangle_Legendre_P3 )
{
  int ii = 2, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_4Box_Split_Triangle_Legendre_P1 )
{
  int ii = 4, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_4Box_Split_Triangle_Legendre_P2 )
{
  int ii = 4, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_4Box_Split_Triangle_Legendre_P3 )
{
  int ii = 4, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_9Box_Split_Triangle_Legendre_P1 )
{
  int ii = 6, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_9Box_Split_Triangle_Legendre_P2 )
{
  int ii = 6, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_9Box_Split_Triangle_Legendre_P3 )
{
  int ii = 6, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_16Box_Split_Triangle_Legendre_P1 )
{
  int ii = 8, jj = ii, order = 1;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_16Box_Split_Triangle_Legendre_P2 )
{
  int ii = 8, jj = ii, order = 2;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_DGBR2_AD_16Box_Split_Triangle_Legendre_P3 )
{
  int ii = 8, jj = ii, order = 3;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);
  const Real small_tol = 1e-9, close_tol = 1e-9;
  bool split_test = true;

#ifdef DEBUG_FLAG
  std::cout<< "ii = " << ii << ", order = " << order << ", split = " << split_test <<std::endl;
#endif
  localExtractAndTest( xfld, order, BasisFunctionCategory_Legendre, small_tol, close_tol, split_test );
}
#endif


#endif

#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
