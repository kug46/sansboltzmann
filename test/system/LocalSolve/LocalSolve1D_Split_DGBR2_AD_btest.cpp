// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LocalSolve_Split_DGBR2_AD_btest
// testing of 1-D DG with Advection-Diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"

#include "Field/Local/Field_Local.h"
#include "Field/Local/FieldLift_Local.h"
#include "Field/Local/XField_LocalPatch.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( LocalSolve1D_Split_DGBR2_AD_test_suite )

void
solveFullLocalSystem(const DLA::VectorD< DLA::VectorD<Real> >& rsd, const DLA::MatrixD< DLA::MatrixD<Real> >& jac,
                     const int sub_qfld_nDOF, const int sub_lgfld_nDOF,
                     DLA::VectorD< DLA::VectorD<Real> >& sol)
{
  DLA::MatrixD< DLA::MatrixD<Real> > jac_local(jac.size());
  DLA::VectorD< DLA::VectorD<Real> > rsd_local(rsd.size());

  const int iq = 0;
  const int ilg = 1;
  //  std::cout<<jac_local(0,0).m()<<","<<jac_local(1,0).m()<<","<<jac_local(2,0).m()<<std::endl;

  jac_local = DLA::Identity();
  rsd_local = 0.0;

  for (int i=0; i<sub_qfld_nDOF; i++)
  {
    for (int j=0; j<sub_qfld_nDOF; j++)
      jac_local(iq,iq)(i,j) = jac(iq,iq)(i,j);

    for (int j=0; j<sub_lgfld_nDOF; j++)
      jac_local(iq,ilg)(i,j) = jac(iq,ilg)(i,j);

    rsd_local[iq][i] = rsd[iq][i];
  }

  for (int i=0; i<sub_lgfld_nDOF; i++)
  {
    for (int j=0; j<sub_qfld_nDOF; j++)
      jac_local(ilg,iq)(i,j) = jac(ilg,iq)(i,j);

    for (int j=0; j<sub_lgfld_nDOF; j++)
      jac_local(ilg,ilg)(i,j) = jac(ilg,ilg)(i,j);

    rsd_local[ilg][i] = rsd[ilg][i];
  }

  sol = DLA::InverseLU::Solve(jac_local,rsd_local);
}

std::vector<Real>
localsolve(const XField<PhysD1, TopoD1>& xfld, const int order, const int main_group, const int main_cell)
{
  typedef ScalarFunction1D_BL SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_DG_Cell<PhysD1, TopoD1, ArrayQ> QField1D_DG_Cell;
  typedef FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> RField1D_DG_Cell;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD1, TopoD1>> AlgEqnSet_Global_Sparse;
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Dense, DGBR2, XField<PhysD1, TopoD1>> AlgEqnSet_Global_Dense;
  typedef AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvertSpace, BCVector,
                                        DGBR2, XField<PhysD1, TopoD1>, AlgebraicEquationSet_DGBR2> AlgEqnSet_Local;
  typedef AlgEqnSet_Global_Sparse::BCParams BCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef AlgEqnSet_Global_Sparse::SystemMatrix SystemMatrixClass;
  typedef AlgEqnSet_Global_Sparse::SystemVector SystemVectorClass;

  typedef AlgEqnSet_Global_Dense::SystemMatrix SystemMatrixClassD;
  typedef AlgEqnSet_Global_Dense::SystemVector SystemVectorClassD;
  typedef AlgEqnSet_Global_Dense::SystemNonZeroPattern SystemNonZeroPatternD;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  // PDE
  Real a = 1.1;
  AdvectiveFlux1D_Uniform adv( a );

  Real nu = 0.5;
  ViscousFlux1D_Uniform visc( nu );

  Source1D_UniformGrad source(0.3, 0.5);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  //Exact solution
  SolutionExact solnExact(solnArgs);
  NDSolutionExact NDsolnExact(solnArgs);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde(adv, visc, source, forcingptr);

  // BC
  // Create a BC dictionary
  PyDict BL;
  BL[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.BL;
  BL[SolutionExact::ParamsType::params.a] = a;
  BL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = BL;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = {0, 1};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDOutputClass fcnSqError(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnSqError, {0});
  OutputIntegrandClass errIntegrandLocal(fcnSqError, {0,1});

  // solution: Legendre
  QField1D_DG_Cell qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0;

  // lifting operators
  RField1D_DG_Cell rfld(xfld, order, BasisFunctionCategory_Legendre);
  rfld = 0;

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 0
  Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order );
#elif 0
  std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
  Field_CG_BoundaryTrace_Independent<PhysD1, TopoD1, ArrayQ> lgfld( boundaryGroupSets, xfld, order );
#else
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order-1, BasisFunctionCategory_Legendre );
#endif

  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> eqnsettol = {1e-12, 1e-12};
  AlgEqnSet_Global_Sparse PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                      ResidualNorm_Default, eqnsettol, {0}, {0}, PyBCList, BCBoundaryGroups);

  // residual

  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
  rsd = 0;
  PrimalEqSet.residual(rsd);

//  fstream fout( "jac_localsolves.mtx", fstream::out );
//  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );

#if 0 //Print residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd[0].m(); k++) cout << k << "\t" << rsd[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd[1].m(); k++) cout << k << "\t" << rsd[1][k] << endl;
#endif

  // solve
  SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);

  SystemVectorClass sln(PrimalEqSet.vectorStateSize());
  solver.solve(rsd, sln);

  // update solution
  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  PrimalEqSet.fillSystemVector(q);
  q -= sln;
  PrimalEqSet.setSolutionField(q);

  //Compute square error on global mesh
  ArrayQ SqErrGlobal = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_DGBR2( errIntegrandGlobal, SqErrGlobal ),
      xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );


  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  //Extract the local mesh for the given element
  XField_LocalPatchConstructor<PhysD1,Line> xfld_constructor(comm_local, connectivity, main_group, main_cell, SpaceType::Discontinuous);

  // create an unsplit patch
  XField_LocalPatch<PhysD1,Line> xfld_local( xfld_constructor, ElementSplitType::Edge, 0 );

  Field_Local<QField1D_DG_Cell> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);
  FieldLift_Local<RField1D_DG_Cell> rfld_local(xfld_local, rfld, order, BasisFunctionCategory_Legendre);
  Field_Local<Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ>> lgfld_local(xfld_local, lgfld, order-1, BasisFunctionCategory_Legendre);

  const int nMainCells = qfld_local.getCellGroupBase(main_group).nElem();
  const int nDOF_per_cell = qfld_local.nDOF()/qfld_local.nElem();
  int nDOF_per_trace = 0;

  if ( lgfld_local.nElem() > 0 ) nDOF_per_trace = lgfld_local.nDOF()/lgfld_local.nElem();

  int nMainBTraces = 0;
  std::vector<int> MainBTraceGroup_list;

  for (int i = 0; i < lgfld_local.nBoundaryTraceGroups(); i++)
  {
    MainBTraceGroup_list.push_back(i);
    nMainBTraces += lgfld_local.getBoundaryTraceGroupBase(i).nElem();
  }

  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_local["BCName"] = MainBTraceGroup_list;

  QuadratureOrder quadratureOrder_local( xfld_local, - 1 );

  //Compute square error on split mesh before local solve
  ArrayQ SqErrLocal = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_DGBR2( errIntegrandLocal, SqErrLocal ),
      xfld_local, (qfld_local, rfld_local),
      quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  xfld_local.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
#endif

#if 0
//  qfld.dump(3,std::cout);
//  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;

//  rfld.dump(3,std::cout);
//  std::cout<<std::endl<<std::endl;
  rfld_local.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;

//  lgfld.dump(3,std::cout);
//  std::cout<<std::endl<<std::endl;
  lgfld_local.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
#endif

  std::vector<int> localCellGroups = {0};

  AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, rfld_local, lgfld_local,
                                    pde, disc, quadratureOrder_local, ResidualNorm_Default, eqnsettol,
                                    localCellGroups, {0,1}, PyBCList, BCBoundaryGroups_local);

  AlgEqnSet_Global_Dense PrimalEqSet_Full(xfld_local, qfld_local, rfld_local, lgfld_local,
                                          pde, disc, quadratureOrder_local, ResidualNorm_Default, eqnsettol,
                                          {0,1}, {0,1}, PyBCList, BCBoundaryGroups_local);

  //------------------------------------------------------------------------------------------

  // Local residual
  SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClass_Local jac_local(nz_local);
  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  //Local solve - with extracted sub-system
  SystemVectorClass_Local sln_local(PrimalEqSet_Local.vectorStateSize());
  sln_local = DLA::InverseLU::Solve(jac_local,rsd_local);

  //------------------------------------------------------------------------------------------

  //Set up linear subsystem
  int sub_qfld_nDOF = nMainCells*nDOF_per_cell; //Re-solving for only main-cell DOFs in local mesh
  int sub_lgfld_nDOF = nMainBTraces*nDOF_per_trace; //Re-solving for only btrace DOFs of main-cells in the local mesh;

  SystemVectorClassD rsd_full_local(PrimalEqSet_Full.vectorEqSize());
  rsd_full_local = 0.0;
  PrimalEqSet_Full.residual(rsd_full_local);

  SystemNonZeroPatternD nz_full_local(PrimalEqSet_Full.matrixSize());
  SystemMatrixClassD jac_full_local(nz_full_local);
  jac_full_local = 0.0;
  PrimalEqSet_Full.jacobian(jac_full_local);

  //Local solve - full local system (with identity for unwanted DOFs) for comparison
  SystemVectorClassD sln_full_local(PrimalEqSet_Full.vectorStateSize());
  sln_full_local = 0.0;
  solveFullLocalSystem(rsd_full_local, jac_full_local, sub_qfld_nDOF, sub_lgfld_nDOF, sln_full_local);

//  sln_local.dump(3,std::cout);
//  sln_full_local.dump(3,std::cout);

  //------------------------------------------------------------------------------------------

  Real small_tol = 1e-12;
  Real close_tol = 1e-10;

  //Check if the two local solutions are equal
  for (int k = 0; k < sub_qfld_nDOF; k++)
    SANS_CHECK_CLOSE( sln_full_local[0][k], sln_local[0][k], small_tol, close_tol );

  for (int k = 0; k < sub_lgfld_nDOF; k++)
    SANS_CHECK_CLOSE( sln_full_local[1][k], sln_local[1][k], small_tol, close_tol );


  //Update local solution fields
  q_local -= sln_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  //Re-compute residual and check if it's zero
  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNorm) );

  //Compute square error on split mesh after local solve
  ArrayQ SqErrLocalSplit = 0;
  IntegrateCellGroups<TopoD1>::integrate(
      FunctionalCell_DGBR2( errIntegrandLocal, SqErrLocalSplit ),
      xfld_local, (qfld_local, rfld_local), quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

  return {SqErrGlobal, SqErrLocal, SqErrLocalSplit};
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve1D_Split_DGBR2_AD_InteriorCell_Target_P1 )
{
  int ii = 3;
  XField1D xfld( ii );

  int order = 1;
  std::vector<Real> errors = localsolve(xfld, order, 0, 1);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  //Global and local errors (before local solve) should be equal since the local mesh is equal to the global mesh.
  BOOST_CHECK_CLOSE( errors[0], errors[1], 1e-10 );
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve1D_Split_DGBR2_AD_LeftBoundaryCell_Target_P2 )
{
  int ii = 3;
  XField1D xfld( ii );

  int order = 2;
  std::vector<Real> errors = localsolve(xfld, order, 0, 0);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve1D_Split_DGBR2_AD_RightBoundaryCell_Target_P2 )
{
  int ii = 3;
  XField1D xfld( ii );

  int order = 2;
  std::vector<Real> errors = localsolve(xfld, order, 0, 2);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
