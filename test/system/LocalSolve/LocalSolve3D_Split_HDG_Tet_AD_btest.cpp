// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LocalSolve_Split_HDG_Tet_AD_btest
// testing of 3-D HDG local solves with Advection-Diffusion on tets

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"

#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/AlgebraicEquationSet_Local_HDG.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_Trace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/Local/Field_Local.h"
#include "Field/Local/XField_LocalPatch.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"


#include "AlgebraicEquationSet_FullLocal_HDG.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( LocalSolve3D_Split_HDG_Tet_AD_test_suite )

std::vector<Real>
localsolve(const XField<PhysD3, TopoD3>& xfld, const int order, const int main_group, const int main_cell,
           const ElementSplitType split_type, const int split_edge_index,
           const std::vector<int>& global_itracegroup_list, const std::vector<int>& global_btracegroup_list,
           const std::vector<int>& local_itracegroup_list)
{
  typedef ScalarFunction3D_TripleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEClass;

  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_sansLG BCType;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_DG_Cell<PhysD3, TopoD3, ArrayQ> QField3D_DG_Cell;
  typedef Field_DG_Cell<PhysD3, TopoD3, VectorArrayQ> AField3D_DG_Cell;
  typedef Field_DG_Trace<PhysD3, TopoD3, ArrayQ> QIField3D_DG_Trace;
  typedef Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> LGField3D_DG_Trace;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> AlgEqnSet_Global_Sparse;
  typedef testspace::AlgebraicEquationSet_FullLocal_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                                        AlgEqSetTraits_Dense, XField<PhysD3, TopoD3>> AlgEqnSet_FullLocal;
  typedef AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Dense, XField<PhysD3, TopoD3>> AlgEqnSet_Local;

  typedef AlgEqnSet_Global_Sparse::BCParams BCParams;

  typedef AlgEqnSet_Global_Sparse::SystemMatrix SystemMatrixClass;
  typedef AlgEqnSet_Global_Sparse::SystemVector SystemVectorClass;

  typedef AlgEqnSet_FullLocal::SystemMatrix SystemMatrixClassD;
  typedef AlgEqnSet_FullLocal::SystemVector SystemVectorClassD;
  typedef AlgEqnSet_FullLocal::SystemNonZeroPattern SystemNonZeroPatternD;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  //PDE
  Real a = 1;
  Real b = 1;
  Real c = 1;
  AdvectiveFlux3D_Uniform adv( a, b, c );

  Real nu = 0.5;
  ViscousFlux3D_Uniform visc( nu, 0, 0, 0, nu, 0, 0, 0, nu );

  Source3D_UniformGrad source(0,0,0,0);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.c] = c;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  NDPDEClass pde( adv, visc, source );

  // BC
  PyDict TripleBL;
  TripleBL[BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function.TripleBL;
  TripleBL[SolutionExact::ParamsType::params.a] = a;
  TripleBL[SolutionExact::ParamsType::params.b] = b;
  TripleBL[SolutionExact::ParamsType::params.c] = c;
  TripleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function] = TripleBL;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputSolutionErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD3, OutputSolutionErrorSquaredClass> NDOutputSolutionErrorSquaredClass;
  typedef IntegrandCell_HDG_Output<NDOutputSolutionErrorSquaredClass> IntegrandSquareErrorClass;

  NDOutputSolutionErrorSquaredClass outErr( solnExact );
  IntegrandSquareErrorClass fcnSqErr( outErr, {0} );
  IntegrandSquareErrorClass fcnSqErr_local( outErr, {0,1} );

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  // solution
  QField3D_DG_Cell qfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld = 0;

  // auxiliary operator
  AField3D_DG_Cell afld(xfld, order, BasisFunctionCategory_Hierarchical);
  afld = 0;

  // interface
  QIField3D_DG_Trace qIfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qIfld = 0;

  //Lagrange multiplier
  std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
  LGField3D_DG_Trace lgfld( xfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list );
  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> eqnsettol = {1e-12, 1e-12, 1e-12};

  AlgEqnSet_Global_Sparse PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, eqnsettol,
                                      {0}, global_itracegroup_list, PyBCList, BCBoundaryGroups );

  // residual
  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
  PrimalEqSet.fillSystemVector(q);

  rsd = 0;
  PrimalEqSet.residual(q, rsd);

#if 0 //Print residual vector
  cout << "Lifting operator residual:" <<endl;
  for (int k = 0; k < rfld.nDOF(); k++) cout << k << "\t" << rsd[0][2*k + 0] <<"\t" << rsd[0][2*k + 1] << endl;

  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < qfld.nDOF(); k++) cout << k << "\t" << rsd[1][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < lgfld.nDOF(); k++) cout << k << "\t" << rsd[2][k] << endl;
#endif

  // solve
  SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);

  SystemVectorClass dq(PrimalEqSet.vectorStateSize());
  solver.solve(rsd, dq);

  // update solution
  q -= dq;
  PrimalEqSet.setSolutionField(q);

  rsd = 0;
  PrimalEqSet.residual(rsd);
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

  //Check if residual is zero for global solution
  BOOST_REQUIRE( PrimalEqSet.convergedResidual(rsdNorm) );

  //Compute square error on global mesh
  ArrayQ SqErrGlobal = 0;
  IntegrateCellGroups<TopoD3>::integrate(
      FunctionalCell_HDG( fcnSqErr, SqErrGlobal ), xfld, (qfld, afld),
      quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );


  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  //Extract the local mesh for the given element
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_constructor(comm_local, connectivity, main_group, main_cell, SpaceType::Discontinuous);

  // create an unsplit patch
  XField_LocalPatch<PhysD3,Tet> xfld_local( xfld_constructor, split_type, split_edge_index );

  std::vector<int> active_BGroup_list_local = {}; //Using sansLG - so lgfld should be empty

#if 0
   xfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   xfld_local.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
#endif

  std::vector<int> local_btracegroup_list; //Indices of all boundary trace groups in local mesh
  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    local_btracegroup_list.push_back(i);

  int nLocalInteriorTraceGroups = std::min(xfld_local.nInteriorTraceGroups(), 2);
  std::vector<int> interiorTraceGroups(nLocalInteriorTraceGroups);
  for (int i = 0; i < nLocalInteriorTraceGroups; i++)
    interiorTraceGroups[i] = i;

  int nLocalBoundaryTraceGroups = xfld_local.nBoundaryTraceGroups();
  std::vector<int> boundaryTraceGroups(nLocalBoundaryTraceGroups);
  for (int i = 0; i < nLocalBoundaryTraceGroups; i++)
    boundaryTraceGroups[i] = i;

  Field_Local<QField3D_DG_Cell> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Hierarchical);
  Field_Local<AField3D_DG_Cell> afld_local(xfld_local, afld, order, BasisFunctionCategory_Hierarchical);
  Field_Local<QIField3D_DG_Trace> qIfld_local(xfld_local, qIfld, order, BasisFunctionCategory_Hierarchical, interiorTraceGroups, boundaryTraceGroups);
  Field_Local<LGField3D_DG_Trace> lgfld_local(xfld_local, lgfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list_local);

  const int nMainCells = qfld_local.getCellGroupBase(main_group).nElem();
  const int nDOF_per_cell = qfld_local.nDOF()/qfld_local.nElem();
  const int nDOF_per_itrace = qIfld_local.nDOF()/qIfld_local.nElem();
  const int nDOF_per_btrace = nDOF_per_itrace;

  int nMainBTraces = 0;
  std::vector<int> MainBTraceGroup_list;

  for (int i = 0; i < qIfld_local.nBoundaryTraceGroups(); i++)
  {
    MainBTraceGroup_list.push_back(i);
    nMainBTraces += qIfld_local.getBoundaryTraceGroupBase(i).nElem();
  }

  int nMainITraces = 0;
  for (int i = 0; i < qIfld_local.nInteriorTraceGroups(); i++)
    nMainITraces += qIfld_local.getInteriorTraceGroupBase(i).nElem();

//  std::cout<<"nMainCells:"<<nMainCells<<", nDOF_per_cell:"<< nDOF_per_cell<<std::endl;
//  std::cout<<"nMainITraces:"<<nMainITraces<<", nDOF_per_trace:"<< nDOF_per_itrace<<std::endl;
//  std::cout<<"nMainBTraces:"<<nMainBTraces<<", nDOF_per_trace:"<< nDOF_per_btrace<<std::endl;

  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_local["BCName"] = MainBTraceGroup_list;

  QuadratureOrder quadratureOrder_local( xfld_local, -1 );

  //Compute square error on split mesh before local solve
  ArrayQ SqErrLocal = 0;
  IntegrateCellGroups<TopoD3>::integrate(
      FunctionalCell_HDG( fcnSqErr_local, SqErrLocal ), xfld_local, (qfld_local, afld_local),
      quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

#if 0
   xfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   xfld_local.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
#endif

#if 0
   qfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   qfld_local.dump(3,std::cout);

   rfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   rfld_local.dump(3,std::cout);

   lgfld.dump(3,std::cout);
   std::cout<<std::endl<<std::endl;
   lgfld_local.dump(3,std::cout);
#endif

   AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, afld_local, qIfld_local, lgfld_local,
                                     pde, disc, quadratureOrder_local, eqnsettol,
                                     {0,1}, local_itracegroup_list, PyBCList, BCBoundaryGroups_local);

   AlgEqnSet_FullLocal PrimalEqSet_Full(xfld_local, qfld_local, afld_local, qIfld_local, lgfld_local,
                                        pde, disc, quadratureOrder_local, eqnsettol,
                                        {0,1}, local_itracegroup_list, PyBCList, BCBoundaryGroups_local);

   // Local residual
   SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
   SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
   PrimalEqSet_Local.fillSystemVector(q_local);

   rsd_local = 0;
   PrimalEqSet_Local.residual(q_local, rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "PDE residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "INT residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;

  cout <<endl << "BC residual:" <<endl;
  for (int k = 0; k < rsd_local[2].m(); k++) cout << k << "\t" << rsd_local[2][k] << endl;
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClass_Local jac_local(nz_local);
  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

//  std::cout << std::endl;
//  std::cout << jac_local(0,0) << std::endl << std::endl;
//  std::cout << jac_local(0,1) << std::endl << std::endl;
//  std::cout << jac_local(1,0) << std::endl << std::endl;
//  std::cout << jac_local(1,1) << std::endl << std::endl;

  //Local solve - with extracted sub-system
  SystemVectorClass_Local dq_local(PrimalEqSet_Local.vectorStateSize());
  dq_local = DLA::InverseLU::Solve(jac_local,rsd_local);

  //------------------------------------------------------------------------------------------

  //Set up linear subsystem
  int sub_qfld_nDOF = nMainCells*nDOF_per_cell; //Re-solving for only main-cell DOFs in local mesh
  int sub_qIfld_nDOF = nMainITraces*nDOF_per_itrace + nMainBTraces*nDOF_per_btrace; //Re-solving for all main Itrace/Btrace DOFs in local mesh
  int sub_lgfld_nDOF = 0; //Using mitState/sansLG - so lgfld should be empty

  BOOST_CHECK_EQUAL( q_local[0].m(), sub_qfld_nDOF );
  BOOST_CHECK_EQUAL( q_local[1].m(), sub_qIfld_nDOF );
  BOOST_CHECK_EQUAL( q_local[2].m(), sub_lgfld_nDOF );

  SystemVectorClassD q_full_local(PrimalEqSet_Full.vectorStateSize());
  SystemVectorClassD rsd_full_local(PrimalEqSet_Full.vectorEqSize());
  PrimalEqSet_Full.fillSystemVector(q_full_local);

  rsd_full_local = 0.0;
  PrimalEqSet_Full.residual(q_full_local, rsd_full_local);

  SystemNonZeroPatternD nz_full_local(PrimalEqSet_Full.matrixSize());
  SystemMatrixClassD jac_full_local(nz_full_local);
  jac_full_local = 0.0;
  PrimalEqSet_Full.jacobian(jac_full_local);

//  std::cout << std::endl;
//  std::cout << jac_full_local(0,0) << std::endl << std::endl;
//  std::cout << jac_full_local(0,1) << std::endl << std::endl;
//  std::cout << jac_full_local(1,0) << std::endl << std::endl;
//  std::cout << jac_full_local(1,1) << std::endl << std::endl;

  //Local solve - full local system (with identity for unwanted DOFs) for comparison
  SystemVectorClassD dq_full_local(PrimalEqSet_Full.vectorStateSize());
  dq_full_local = 0.0;
  testspace::solveFullLocalSystem(rsd_full_local, jac_full_local,
                                  sub_qfld_nDOF, sub_qIfld_nDOF, sub_lgfld_nDOF,
                                  dq_full_local);

//  dq_local.dump(3,std::cout);
//  dq_full_local.dump(3,std::cout);

  //------------------------------------------------------------------------------------------

  Real small_tol = 1e-10;
  Real close_tol = 5e-8;

  //Check if the two local solutions are equal
  for (int k = 0; k < sub_qfld_nDOF; k++)
    SANS_CHECK_CLOSE( dq_full_local[0][k], dq_local[0][k], small_tol, close_tol );

  for (int k = 0; k < sub_qIfld_nDOF; k++)
    SANS_CHECK_CLOSE( dq_full_local[1][k], dq_local[1][k], small_tol, close_tol );

  for (int k = 0; k < sub_lgfld_nDOF; k++)
    SANS_CHECK_CLOSE( dq_full_local[2][k], dq_local[2][k], small_tol, close_tol );


  //Update local solution fields
  q_local -= dq_local;
  PrimalEqSet_Local.setSolutionField(q_local);

//  afld_local.dump(3,std::cout);

  //Re-compute residual and check if it's zero
  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);

//  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet_Local.residualNorm(rsd_local);
//  PrimalEqSet_Local.printDecreaseResidualFailure(rsdNorm, std::cout);
//
//  std::cout << rsd_local[0] << std::endl << std::endl;
//  std::cout << rsd_local[1] << std::endl << std::endl;

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNorm) );

  //Compute square error on split mesh after local solve
  ArrayQ SqErrLocalSplit = 0;
  IntegrateCellGroups<TopoD3>::integrate(
      FunctionalCell_HDG( fcnSqErr_local, SqErrLocalSplit ), xfld_local, (qfld_local, afld_local),
      quadratureOrder_local.cellOrders.data(), quadratureOrder_local.cellOrders.size() );

  return {SqErrGlobal, SqErrLocal, SqErrLocalSplit};
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_Split_HDG_AD_Box_Tet_Cell0_Target_SplitEdge0_P1 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 0;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 0;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_Split_HDG_AD_Box_Tet_Cell1_Target_SplitEdge1_P1 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 1;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 1;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_Split_HDG_AD_Box_Tet_Cell2_Target_SplitEdge2_P1 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 2;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 2;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_Split_HDG_AD_Box_Tet_Cell3_Target_SplitEdge3_P1 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 3;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 3;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_Split_HDG_AD_Box_Tet_Cell4_Target_SplitEdge4_P1 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 4;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 4;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_Split_HDG_AD_Box_Tet_Cell5_Target_SplitEdge5_P1 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 5;
  ElementSplitType split_type = ElementSplitType::Edge;
  int split_edge_index = 5;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_Split_HDG_AD_Box_Tet_Cell2_Target_SplitIsotropic_P1 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 2;
  ElementSplitType split_type = ElementSplitType::Isotropic;
  int split_edge_index = -1;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LocalSolve3D_Split_HDG_AD_Box_Tet_Cell4_Target_SplitIsotropic_P1 )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  int ii = 1;
  int jj = ii;
  int kk = ii;
  XField3D_Box_Tet_X1 xfld(comm, ii,jj,kk);

  int order = 1;

  int main_group = 0;
  int main_cell = 4;
  ElementSplitType split_type = ElementSplitType::Isotropic;
  int split_edge_index = -1;

  std::vector<int> global_itracegroup_list = {0};
  std::vector<int> global_btracegroup_list = {0,1,2,3,4,5};

  std::vector<int> local_itracegroup_list = {0,1};

  std::vector<Real> errors = localsolve(xfld, order, main_group, main_cell, split_type, split_edge_index,
                                        global_itracegroup_list, global_btracegroup_list, local_itracegroup_list);

//  std::cout<<"Errors: Global = "<<errors[0]<<", Pre-local solve: "<<errors[1]
//           <<", Post-local solve: "<<errors[2]<<std::endl;

  BOOST_REQUIRE( errors[1] < errors[0]); //Error on local mesh before local solve should be smaller than on global mesh.
  BOOST_REQUIRE( errors[2] < errors[1]); //The split mesh error after the local solve should be smaller
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
