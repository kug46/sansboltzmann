// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// LocalSolve_Split_Galerkin_Triangle_AD_btest
// testing of 2-D DG with Advection-Diffusion on triangles

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real


#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/VMSD/IntegrandCell_VMSD_Output.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/Field_NodalView.h"
#include "Field/Local/Field_Local.h"
#include "Field/Local/XField_LocalPatch.h"
#include "Field/tools/findGroups.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_Local_VMSD_AD_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_Local_VMSD_BoundaryEdge_P1_AD_test )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunction_mitStateParam BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_CG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_CG_Cell;
  typedef Field_EG_Cell<PhysD2, TopoD2, ArrayQ> QPField2D_EG_Cell;
  typedef Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_CG_Trace;


  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Dense, XField<PhysD2, TopoD2>> AlgEqnSet_Global;

  typedef AlgEqnSet_Global::SystemMatrix GlobalSystemMatrixClass;
  typedef AlgEqnSet_Global::SystemVector GlobalSystemVectorClass;

  typedef AlgebraicEquationSet_Local_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                                         XField<PhysD2, TopoD2>> AlgEqnSet_Local;
  typedef AlgEqnSet_Local::BCParams BCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_VMSD_Output<NDOutputClass, NDPDEClass> OutputIntegrandClass;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;

//  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};
  std::vector<int> local_itracegroup_list = {0,1,2};

  // PDE
  Real a = 1;
  Real b = 1;
  AdvectiveFlux2D_Uniform adv( a, b );

  Real nu = 1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0.3, 0.5, 0.1);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Output
  NDOutputClass fcnSqError(solnExact);
  OutputIntegrandClass errIntegrandGlobal(pde, fcnSqError, {0});
  OutputIntegrandClass errIntegrandLocal(pde, fcnSqError, {0,1});

  typedef FieldBundle_VMSD<PhysD2,TopoD2,ArrayQ> FieldBundle;
  FieldBundle flds( xfld, order, order,
                    BasisFunctionCategory_Lagrange,
                    BasisFunctionCategory_Lagrange, BasisFunctionCategory_Lagrange,
                    {} );
  //Solution
  flds.qfld = 0;
  flds.qpfld = 0;
  flds.lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> eqnsettol = {1e-12, 1e-12, 1e-12};
  const DiscretizationVMSD stab;
  std::vector<int> interiorTraceGroups;
  for (int i=0; i<xfld.nInteriorTraceGroups(); i++) { interiorTraceGroups.push_back(i); }

  AlgEqnSet_Global PrimalEqSet_global(xfld, flds.qfld, flds.qpfld, flds.lgfld, pde, stab, quadratureOrder,
                                      ResidualNorm_L2, eqnsettol, {0}, interiorTraceGroups, PyBCList, BCBoundaryGroups);


  GlobalSystemVectorClass q_global(PrimalEqSet_global.vectorStateSize());
  GlobalSystemVectorClass rsd_global(PrimalEqSet_global.vectorEqSize());
  q_global = 0;
  rsd_global = 0;

  PrimalEqSet_global.fillSystemVector(q_global);
  PrimalEqSet_global.residual(q_global, rsd_global);

  GlobalSystemMatrixClass jac_global(PrimalEqSet_global.matrixSize());
  jac_global = 0;

  PrimalEqSet_global.jacobian(rsd_global, jac_global, false);

  //Local solve - with extracted sub-system

  GlobalSystemVectorClass dq_global(q_global.size());
  dq_global = 0;

  GlobalSystemVectorClass dq_global_condensed = dq_global.sub(1,2);
  GlobalSystemVectorClass rsd_global_condensed = rsd_global.sub(1,2);

  std::cout << "219\n";
  dq_global_condensed = DLA::InverseLUP::Solve(jac_global, rsd_global_condensed);
  std::cout << "220\n";

  PrimalEqSet_global.completeUpdate(rsd_global, dq_global_condensed, dq_global);

  //Update local solution fields
  q_global -= dq_global;
  PrimalEqSet_global.setSolutionField(q_global);

  //Re-compute residual and check if it's zero
  rsd_global = 0;
  PrimalEqSet_global.residual(rsd_global);

  std::vector<std::vector<Real>> rsdNrm_Global( PrimalEqSet_global.residualNorm(rsd_global) );

  BOOST_REQUIRE( PrimalEqSet_global.convergedResidual(rsdNrm_Global) );

  flds.up_resfld
    = SANS::make_unique<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>>(xfld, order, BasisFunctionCategory_Lagrange);

  //transfer solution to DG field so that residual fcn can be re-used
  Field_DG_Cell<PhysD2,TopoD2,ArrayQ> qfld_dg(xfld, order, BasisFunctionCategory_Lagrange, {0} );

  flds.qfld.projectTo(qfld_dg);

  PrimalEqSet_global.fillResidualField( qfld_dg, flds.up_resfld );

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

//  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // Left most of bottom edge
  std::array<int,2> edge{{0,1}};

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  //Find the mapping between the boundary conditions of local problem and the global problem
  std::vector<int> boundaryTraceGroups = xfld_local.getReSolveBoundaryTraceGroups();

  PyDict BCList_local;
  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  for (std::size_t local_group = 0; local_group < boundaryTraceGroups.size(); local_group++)
  {
    //Get the global boundary-trace group index for the current local boundary-trace group
    int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[local_group],0}).first;

    BOOST_REQUIRE(globalBTraceGroup >= 0); //Check for non-negative index

    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it)
    {
      const std::vector<int>& grouplist = it->second;
      bool found = false;

      for (int j = 0; j < (int) grouplist.size(); j++)
      {
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the BC associated with the globalBTraceGroup we want, so add it to the local BTraceGroup map
          BCBoundaryGroups_local[it->first].push_back(local_group);
          BCList_local[it->first] = PyBCList[it->first];
          found = true;
          break;
        }
      }

      if (found) break;
    }
  } //loop over local btracegroups

  std::vector<int> active_local_boundaries = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(BCList_local, BCBoundaryGroups_local);

  // active_local_boundaries, collected into sets that share dofs (if the original global fields are continuous)
  std::vector<std::vector<int>> local_boundary_bundle( BCBoundaryGroups_local.size() ); // There are this many unique global bcs

  // Bundle local groups based on whether they came from the same global group. Groups can only be broken when the global group was too
  // Loop over active_local_boundaries, then check each of boundaryTraceGroups. If they're really that global group, place in the bundle
  for (auto it = active_local_boundaries.begin(); it != active_local_boundaries.end(); ++it)
  {
    // The globalBTraceGroup from whence it came
    const int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[*it],0}).first;

    // search over list of global group bundles
    for (auto itt = BCBoundaryGroups_local.begin(); itt != BCBoundaryGroups_local.end(); ++itt)
    {
      const std::vector<int>& grouplist = itt->second;

      bool found = false;

      for (std::size_t j = 0; j < grouplist.size(); j++)
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the global bundle associated with active_local_boundary
          // Place in the corresponding bundle

          // Can't use [i] and std::size_t above because need to use iterators to move over std::map
          local_boundary_bundle[std::distance(BCBoundaryGroups_local.begin(), itt)].push_back(*it);
          found = true;
          break;
        }

      if (found) break;
    }
  }

  // remove-erase idiom | remove those bundles that are empty
  local_boundary_bundle.erase( std::remove_if( local_boundary_bundle.begin(), local_boundary_bundle.end(),
                                               [](const std::vector<int>& i){ return i.empty(); } ), // dummy function
                               local_boundary_bundle.end() );

  Field_Local<QField2D_CG_Cell> qfld_local(SANS::split(xfld_local.getReSolveCellGroups()), xfld_local,
                                           flds.qfld, order, BasisFunctionCategory_Lagrange);

  Field_Local<QPField2D_EG_Cell> qpfld_local( qfld_local, flds.qpfld, order, BasisFunctionCategory_Lagrange);

  Field_Local<LGField2D_CG_Trace> lgfld_local(local_boundary_bundle, xfld_local, flds.lgfld, order, BasisFunctionCategory_Lagrange );

  std::unique_ptr<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>> up_resfld_local
    = SANS::make_unique<Field_Local<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>>>(xfld_local, *(flds.up_resfld), order,BasisFunctionCategory_Lagrange);

  // zero out the field here. We're not going to actually use it, so we're going to say evaluate on all cell groups
//   *up_resfld_local = 0;
   QuadratureOrder quadratureOrder_local( xfld_local, -1 );


  std::vector<int> localCellGroups = {0}; // evaluating on cellgroup zero
  std::vector<int> localInteriorTraceGroups = findInteriorTraceGroup(xfld_local,0,1);

  {

    AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, qpfld_local, lgfld_local, up_resfld_local,
                                      pde, stab, quadratureOrder_local, ResidualNorm_L2, eqnsettol,
                                      localCellGroups, localInteriorTraceGroups, PyBCList, BCBoundaryGroups_local);

    //------------------------------------------------------------------------------------------
    // Local residual
    SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
    PrimalEqSet_Local.fillSystemVector(q_local);

    BOOST_CHECK_EQUAL( q_local.m(), 3 );
    BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::iq].m(), 4 );
    BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::ilg].m(), 0 );

    SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
    rsd_local = 0;
    PrimalEqSet_Local.residual(q_local, rsd_local);

  #if 0 //Print local residual vector
    cout <<endl << "Solution residual:" <<endl;
    for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

    cout <<endl << "Lagrange multiplier residual:" <<endl;
    for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
  #endif

  #ifdef __clang_analyzer__
    return; // clang analyzer complains about the matrix size below
  #endif

    //Jacobian
    SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
    SystemMatrixClass_Local jac_local(nz_local);

    BOOST_CHECK_EQUAL( jac_local.m(), 2 );
    BOOST_CHECK_EQUAL( jac_local.n(), 2 );
    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDESC, AlgEqnSet_Local::iqSC).m(), 4 );
    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDESC, AlgEqnSet_Local::iqSC).n(), 4 );

    jac_local = 0;
    PrimalEqSet_Local.jacobian(rsd_local, jac_local, false);

    BOOST_CHECK_EQUAL( xfld.nDOF(), 9 );
    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 7 );

    const std::array<std::vector<int>,3>& freeDOFs = PrimalEqSet_Local.getFreeDOFs();
    // Check that the right dofs are frozen
    std::vector<int> true_frozeDOFs = {4,5,6,7,8};
    std::vector<int> true_freeDOFs  = {0,1,2,3};

    BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[1].size());

    for ( auto it = true_frozeDOFs.begin(); it != true_frozeDOFs.end(); ++it)
      BOOST_CHECK( std::find(freeDOFs[1].begin(),freeDOFs[1].end(),*it) == (freeDOFs[1]).end() );

    for ( auto it = true_freeDOFs.begin(); it != true_freeDOFs.end(); ++it)
      BOOST_CHECK( std::find(freeDOFs[1].begin(),freeDOFs[1].end(),*it) != (freeDOFs[1]).end() );

//    jac_local.dump();
//    jac_local(AlgEqnSet_Local::iPDE,AlgEqnSet_Local::iPDE).dump();
//    jac_local(AlgEqnSet_Local::iPDE,AlgEqnSet_Local::iBC).dump();
//    jac_local(AlgEqnSet_Local::iBC,AlgEqnSet_Local::iPDE).dump();
//    jac_local(AlgEqnSet_Local::iBC,AlgEqnSet_Local::iBC).dump();

    //Local solve - with extracted sub-system
    SystemVectorClass_Local sln_local(rsd_local.size());

    sln_local = 0;

    SystemVectorClass_Local dq_condensed = sln_local.sub(1,2);
    SystemVectorClass_Local rsd_condensed = rsd_local.sub(1,2);

    dq_condensed = DLA::InverseLUP::Solve(jac_local, rsd_condensed);

    PrimalEqSet_Local.completeUpdate(rsd_local, dq_condensed, sln_local);

    //Update local solution fields
    q_local -= sln_local;
    PrimalEqSet_Local.setSolutionField(q_local);

    //Re-compute residual and check if it's zero
    rsd_local = 0;
    PrimalEqSet_Local.residual(rsd_local);

    std::vector<std::vector<Real>> rsdNrm( PrimalEqSet_Local.residualNorm(rsd_local) );


    BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNrm) );
  }
  //---------------------------------------------//
  // Testing Alternative FieldBundle Constructor //
  //---------------------------------------------//
  if ( true )
  {
    typedef typename AlgEqnSet_Local::FieldBundle_Local FieldBundle_Local;

    std::vector<int> active_BCGroup_list_local = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups_local);

    std::vector<int> dummy = { active_BCGroup_list_local };

    FieldBundle_Local flds_local( xfld_local, flds, order, dummy );

    AlgEqnSet_Local PrimalEqSet_Local( xfld_local, flds_local, {}, pde, stab, quadratureOrder_local, ResidualNorm_L2, eqnsettol,
                                      localCellGroups, localInteriorTraceGroups, PyBCList, BCBoundaryGroups_local);


    //------------------------------------------------------------------------------------------

    // Local residual
    SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
    PrimalEqSet_Local.fillSystemVector(q_local);

    BOOST_CHECK_EQUAL( q_local.m(), 3 );
    BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::iq].m(), 4 );
    BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::ilg].m(), 0 );

    SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
    rsd_local = 0;
    PrimalEqSet_Local.residual(q_local, rsd_local);

  #if 0 //Print local residual vector
    cout <<endl << "Solution residual:" <<endl;
    for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

    cout <<endl << "Lagrange multiplier residual:" <<endl;
    for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
  #endif

  #ifdef __clang_analyzer__
    return; // clang analyzer complains about the matrix size below
  #endif

    //Jacobian
    SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
    SystemMatrixClass_Local jac_local(nz_local);

    BOOST_CHECK_EQUAL( jac_local.m(), 2 );
    BOOST_CHECK_EQUAL( jac_local.n(), 2 );
    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDESC, AlgEqnSet_Local::iqSC).m(), 4 );
    BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDESC, AlgEqnSet_Local::iqSC).n(), 4 );

    jac_local = 0;
    PrimalEqSet_Local.jacobian(rsd_local, jac_local, false);

    BOOST_CHECK_EQUAL( xfld.nDOF(), 9 );
    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 7 );

    const std::array<std::vector<int>,3>& freeDOFs = PrimalEqSet_Local.getFreeDOFs();

    // Check that the right dofs are frozen
    std::vector<int> true_frozeDOFs = {4,5,6,7,8};
    std::vector<int> true_freeDOFs  = {0,1,2,3};

    BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[1].size());

    for ( auto it = true_frozeDOFs.begin(); it != true_frozeDOFs.end(); ++it)
      BOOST_CHECK( std::find(freeDOFs[1].begin(),freeDOFs[1].end(),*it) == (freeDOFs[1]).end() );

    for ( auto it = true_freeDOFs.begin(); it != true_freeDOFs.end(); ++it)
      BOOST_CHECK( std::find(freeDOFs[1].begin(),freeDOFs[1].end(),*it) != (freeDOFs[1]).end() );


    //Local solve - with extracted sub-system
    SystemVectorClass_Local sln_local(rsd_local.size());
    sln_local = 0;

    SystemVectorClass_Local dq_condensed = sln_local.sub(1,2);
    SystemVectorClass_Local rsd_condensed = rsd_local.sub(1,2);

    dq_condensed = DLA::InverseLUP::Solve(jac_local, rsd_condensed);

    PrimalEqSet_Local.completeUpdate(rsd_local, dq_condensed, sln_local);

    //Update local solution fields
    q_local -= sln_local;
    PrimalEqSet_Local.setSolutionField(q_local);

    //Re-compute residual and check if it's zero
    rsd_local = 0;
    PrimalEqSet_Local.residual(rsd_local);

    std::vector<std::vector<Real>> rsdNrm( PrimalEqSet_Local.residualNorm(rsd_local) );

    BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNrm) );

  }
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_Local_Galerkin_Stabilized_InteriorEdge_P1_AD_test )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunction_mitStateParam BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;


  typedef Field_CG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_CG_Cell;
  typedef Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_CG_Trace;

  typedef AlgebraicEquationSet_Local_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                         XField<PhysD2, TopoD2>> AlgEqnSet_Local;
  typedef AlgEqnSet_Local::BCParams BCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;

  std::vector<int> global_btracegroup_list = {0,1,2,3};
  std::vector<int> local_itracegroup_list = {0,1,2};

  // PDE
  Real a = 1.0;
  Real b = 1.0;
  AdvectiveFlux2D_Uniform adv( a, b );

  Real nu = 0.273;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0.3, 0.5, 0.1);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Output
  NDOutputClass fcnSqError(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnSqError, {0});
  OutputIntegrandClass errIntegrandLocal(fcnSqError, {0,1});

  //Solution
  QField2D_CG_Cell qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  for (int i = 0; i < qfld.nDOF(); i++)
    qfld.DOF(i) = i+1;

  //Lagrange multipliers
  LGField2D_CG_Trace lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

  for (int i = 0; i < lgfld.nDOF(); i++)
    lgfld.DOF(i) = i+1;

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

//  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // First interior vertical edge
  std::array<int,2> edge{{1,4}};

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  //Find the mapping between the boundary conditions of local problem and the global problem
  std::vector<int> boundaryTraceGroups = xfld_local.getReSolveBoundaryTraceGroups();

  PyDict BCList_local;
  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  for (std::size_t local_group = 0; local_group < boundaryTraceGroups.size(); local_group++)
  {
    //Get the global boundary-trace group index for the current local boundary-trace group
    int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[local_group],0}).first;

    BOOST_REQUIRE(globalBTraceGroup >= 0); //Check for non-negative index

    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it)
    {
      const std::vector<int>& grouplist = it->second;
      bool found = false;

      for (int j = 0; j < (int) grouplist.size(); j++)
      {
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the BC associated with the globalBTraceGroup we want, so add it to the local BTraceGroup map
          BCBoundaryGroups_local[it->first].push_back(local_group);
          BCList_local[it->first] = PyBCList[it->first];
          found = true;
          break;
        }
      }

      if (found) break;
    }
  } //loop over local btracegroups

  std::vector<int> active_local_boundaries = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(BCList_local, BCBoundaryGroups_local);

  // active_local_boundaries, collected into sets that share dofs (if the original global fields are continuous)
  std::vector<std::vector<int>> local_boundary_bundle( BCBoundaryGroups_local.size() ); // There are this many unique global bcs

  // Bundle local groups based on whether they came from the same global group. Groups can only be broken when the global group was too
  // Loop over active_local_boundaries, then check each of boundaryTraceGroups. If they're really that global group, place in the bundle
  for (auto it = active_local_boundaries.begin(); it != active_local_boundaries.end(); ++it)
  {
    // The globalBTraceGroup from whence it came
    const int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[*it],0}).first;

    // search over list of global group bundles
    for (auto itt = BCBoundaryGroups_local.begin(); itt != BCBoundaryGroups_local.end(); ++itt)
    {
      const std::vector<int>& grouplist = itt->second;

      bool found = false;

      for (std::size_t j = 0; j < grouplist.size(); j++)
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the global bundle associated with active_local_boundary
          // Place in the corresponding bundle

          // Can't use [i] and std::size_t above because need to use iterators to move over std::map
          local_boundary_bundle[std::distance(BCBoundaryGroups_local.begin(), itt)].push_back(*it);
          found = true;
          break;
        }

      if (found) break;
    }
  }

  // remove-erase idiom | remove those bundles that are empty
  local_boundary_bundle.erase( std::remove_if( local_boundary_bundle.begin(), local_boundary_bundle.end(),
                                               [](const std::vector<int>& i){ return i.empty(); } ), // dummy function
                               local_boundary_bundle.end() );

  Field_Local<QField2D_CG_Cell> qfld_local(SANS::split(xfld_local.getReSolveCellGroups()),
                                           xfld_local, qfld, order, BasisFunctionCategory_Hierarchical);
  Field_Local<LGField2D_CG_Trace> lgfld_local(local_boundary_bundle, xfld_local, lgfld, order, BasisFunctionCategory_Hierarchical );

  std::unique_ptr<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>> up_resfld_local =
    SANS::make_unique<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>>(xfld_local,order,BasisFunctionCategory_Hierarchical);


  // zero out the field here. We're not going to actually use it, so we're going to say evaluate on all cell groups
  *up_resfld_local = 0;
  // output_Tecplot(xfld_local, "tmp/xfld_local.plt");

  QuadratureOrder quadratureOrder_local( xfld_local, -1 );
  std::vector<Real> eqnsettol = {1e-12, 1e-12};

  std::vector<int> localCellGroups = {0,1}; // evaluating on all because we're cheats
  std::vector<int> localInteriorTraceGroups = findInteriorTraceGroup(xfld_local,0,1);

  const StabilizationMatrix stab(StabilizationType::Adjoint, TauType::Glasby, order);

  AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, lgfld_local, up_resfld_local,
                                    pde, stab, quadratureOrder_local, ResidualNorm_L2, eqnsettol,
                                    localCellGroups, localInteriorTraceGroups, PyBCList, BCBoundaryGroups_local);


  //------------------------------------------------------------------------------------------

  // Local residual
  SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  BOOST_CHECK_EQUAL( q_local.m(), 2 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::iq].m(), 5 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::ilg].m(), 0 );

  SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
#endif

#ifdef __clang_analyzer__
  return; // clang analyzer complains about the matrix size below
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClass_Local jac_local(nz_local);

  BOOST_CHECK_EQUAL( jac_local.m(), 2 );
  BOOST_CHECK_EQUAL( jac_local.n(), 2 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).m(), 5 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).n(), 5 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).m(), 5 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).n(), 0 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).m(), 0 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).n(), 5 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).m(), 0 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).n(), 0 );

  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 9 );
  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 9 );
  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 13 );

  const std::array<std::vector<int>,2>& freeDOFs = PrimalEqSet_Local.getFreeDOFs();

  // Check that the right dofs are frozen
  std::vector<int> true_frozenDOFs = {5,6,7,8};
  std::vector<int> true_freeDOFs   = {0,1,2,3,4};

  BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[0].size());

  for ( int true_frozenDOF : true_frozenDOFs)
    BOOST_CHECK( std::find(freeDOFs[0].begin(),freeDOFs[0].end(), true_frozenDOF) == (freeDOFs[0]).end() );

  for ( int true_freeDOF : true_freeDOFs)
    BOOST_CHECK( std::find(freeDOFs[0].begin(),freeDOFs[0].end(), true_freeDOF)   != (freeDOFs[0]).end() );


  BOOST_CHECK_EQUAL( lgfld_local.nDOF(), 0 );

  true_freeDOFs = {};

  //Local solve - with extracted sub-system
  SystemVectorClass_Local sln_local(rsd_local.size());

  //------------------------------------------------//
  //                  FLAT SOLVE                    //
  //------------------------------------------------//
  if (jac_local(1,1).m() > 0 ) // Flat Solve
  {
    // Copy, flatten and pivot inverse
    int m0 = jac_local(0,0).m(), m1 = jac_local(1,0).m();
    int n0 = jac_local(0,0).n(), n1 = jac_local(0,1).n();
    int qL = DLA::VectorSize<ArrayQ>::M;

    BOOST_REQUIRE( m0 + m1 == n0 + n1 );
    DLA::MatrixD<Real> jac_local_flat(qL*(m0+m1),qL*(n0+n1));
    DLA::VectorD<Real> rsd_local_flat(qL*(n0+n1)), dsln_local_flat(qL*(n0+n1));

    // 0 Row
    for ( int j = 0; j < n0; j++)
      for (int qJ = 0; qJ < DLA::VectorSize<ArrayQ>::M; qJ++)
      {
        rsd_local_flat(j + qJ*qL) = DLA::index((rsd_local(0))(j),qJ);
        for (int qI = 0; qI < DLA::VectorSize<ArrayQ>::M; qI++)
        {
          // (0,0) block
          for (int i = 0; i < m0; i++)
            jac_local_flat(i + qI*qL,j + qJ*qL) = DLA::index((jac_local(0,0))(i,j),qI,qJ);
          // (0,1) block
          for (int i = m0; i < m0 + m1; i++)
            jac_local_flat(i + qI*qL,j + qJ*qL) = DLA::index((jac_local(1,0))(i-m0,j),qI,qJ);
        }
      }


    // 1 Row
    for ( int j = n0; j < n0+n1; j++)
      for (int qJ = 0; qJ < DLA::VectorSize<ArrayQ>::M; qJ++)
      {
        rsd_local_flat(j + qJ*qL) = DLA::index((rsd_local(1))(j-n0),qJ);
        for (int qI = 0; qI < DLA::VectorSize<ArrayQ>::M; qI++)
        {
          // (1,0) block
          for (int i = 0; i < m0; i++)
            jac_local_flat(i + qI*qL,j + qJ*qL) = DLA::index((jac_local(0,1))(i,j-n0),qI,qJ);
          // (1,1) block
          for (int i = m0; i < m0 + m1; i++)
            jac_local_flat(i + qI*qL,j + qJ*qL) = DLA::index((jac_local(1,1))(i-m0,j-n0),qI,qJ);
        }
      }


    dsln_local_flat = DLA::InverseLUP::Solve(jac_local_flat, rsd_local_flat);

    // fold back up into the solution vector
    for (int j = 0; j < n0; j++)
      for (int qJ = 0; qJ < DLA::VectorSize<ArrayQ>::M; qJ++)
        DLA::index(sln_local(0)(j),qJ) = dsln_local_flat(j + qJ*qL);

    for (int j = n0; j < n0+n1; j++)
      for (int qJ = 0; qJ < DLA::VectorSize<ArrayQ>::M; qJ++)
        DLA::index(sln_local(1)(j-n0),qJ) = dsln_local_flat(j + qJ*qL);

  }
  else
    sln_local = DLA::InverseLUP::Solve(jac_local, rsd_local);

  //------------------------------------------------//
  //                END FLAT SOLVE                  //
  //------------------------------------------------//

  //Update local solution fields
  q_local -= sln_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  //Re-compute residual and check if it's zero
  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  std::vector<std::vector<Real>> rsdNrm( PrimalEqSet_Local.residualNorm(rsd_local) );

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNrm) );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_Local_Galerkin_Stabilized_BoundaryEdge_P2_AD_test )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunction_mitStateParam BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_CG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_CG_Cell;
  typedef Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_CG_Trace;

  typedef AlgebraicEquationSet_Local_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                         XField<PhysD2, TopoD2>> AlgEqnSet_Local;
  typedef AlgEqnSet_Local::BCParams BCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

//  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};
  std::vector<int> local_itracegroup_list = {0,1,2};

  // PDE
  Real a = 1.0;
  Real b = 1.0;
  AdvectiveFlux2D_Uniform adv( a, b );

  Real nu = 0.273;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0.3, 0.5, 0.1);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Output
  NDOutputClass fcnSqError(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnSqError, {0});
  OutputIntegrandClass errIntegrandLocal(fcnSqError, {0,1});

  const int solnOrder = 2;

  //Solution
  QField2D_CG_Cell qfld(xfld, solnOrder, BasisFunctionCategory_Hierarchical);

  for (int i = 0; i < qfld.nDOF(); i++)
    qfld.DOF(i) = i;

  //Lagrange multipliers
  LGField2D_CG_Trace lgfld( xfld, solnOrder, BasisFunctionCategory_Hierarchical,
                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

  for (int i = 0; i < lgfld.nDOF(); i++)
    lgfld.DOF(i) = i;


  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

//  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // Left most of bottom edge
  std::array<int,2> edge{{0,1}};

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

//  xfld_local.dump();

  //Find the mapping between the boundary conditions of local problem and the global problem
  std::vector<int> boundaryTraceGroups = xfld_local.getReSolveBoundaryTraceGroups();

  PyDict BCList_local;
  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  for (std::size_t local_group = 0; local_group < boundaryTraceGroups.size(); local_group++)
  {
    //Get the global boundary-trace group index for the current local boundary-trace group
    int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[local_group],0}).first;

    BOOST_REQUIRE(globalBTraceGroup >= 0); //Check for non-negative index

    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it)
    {
      const std::vector<int>& grouplist = it->second;
      bool found = false;

      for (int j = 0; j < (int) grouplist.size(); j++)
      {
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the BC associated with the globalBTraceGroup we want, so add it to the local BTraceGroup map
          BCBoundaryGroups_local[it->first].push_back(local_group);
          BCList_local[it->first] = PyBCList[it->first];
          found = true;
          break;
        }
      }

      if (found) break;
    }
  } //loop over local btracegroups

  std::vector<int> active_local_boundaries = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(BCList_local, BCBoundaryGroups_local);

  // active_local_boundaries, collected into sets that share dofs (if the original global fields are continuous)
  std::vector<std::vector<int>> local_boundary_bundle( BCBoundaryGroups_local.size() ); // There are this many unique global bcs

  // Bundle local groups based on whether they came from the same global group. Groups can only be broken when the global group was too
  // Loop over active_local_boundaries, then check each of boundaryTraceGroups. If they're really that global group, place in the bundle
  for (auto it = active_local_boundaries.begin(); it != active_local_boundaries.end(); ++it)
  {
    // The globalBTraceGroup from whence it came
    const int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[*it],0}).first;

    // search over list of global group bundles
    for (auto itt = BCBoundaryGroups_local.begin(); itt != BCBoundaryGroups_local.end(); ++itt)
    {
      const std::vector<int>& grouplist = itt->second;

      bool found = false;

      for (std::size_t j = 0; j < grouplist.size(); j++)
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the global bundle associated with active_local_boundary
          // Place in the corresponding bundle

          // Can't use [i] and std::size_t above because need to use iterators to move over std::map
          local_boundary_bundle[std::distance(BCBoundaryGroups_local.begin(), itt)].push_back(*it);
          found = true;
          break;
        }

      if (found) break;
    }
  }

  // remove-erase idiom | remove those bundles that are empty
  local_boundary_bundle.erase( std::remove_if( local_boundary_bundle.begin(), local_boundary_bundle.end(),
                                               [](const std::vector<int>& i){ return i.empty(); } ), // dummy function
                               local_boundary_bundle.end() );

  Field_Local<QField2D_CG_Cell> qfld_local(SANS::split(xfld_local.getReSolveCellGroups()),
                                           xfld_local, qfld, solnOrder, BasisFunctionCategory_Hierarchical);
  Field_Local<LGField2D_CG_Trace> lgfld_local( local_boundary_bundle, xfld_local, lgfld, solnOrder, BasisFunctionCategory_Hierarchical );

  std::unique_ptr<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>> up_resfld_local =
    SANS::make_unique<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>>(xfld_local,solnOrder,BasisFunctionCategory_Hierarchical);

  // zero out the field here. We're not going to actually use it, so we're going to say evaluate on all cell groups
  *up_resfld_local = 0;
//  output_Tecplot(qfld_local, "tmp/qfld_local.plt");

  QuadratureOrder quadratureOrder_local( xfld_local, -1 );
  std::vector<Real> eqnsettol = {1e-10, 1e-10};

  std::vector<int> localCellGroups = {0,1}; // evaluating on all because we're cheats
  std::vector<int> localInteriorTraceGroups = findInteriorTraceGroup(xfld_local,0,1);

  const StabilizationMatrix stab(StabilizationType::Adjoint, TauType::Glasby, solnOrder);

  AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, lgfld_local, up_resfld_local,
                                    pde, stab, quadratureOrder_local, ResidualNorm_L2, eqnsettol,
                                    localCellGroups, localInteriorTraceGroups, PyBCList, BCBoundaryGroups_local);

  //------------------------------------------------------------------------------------------

  // Local residual
  SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  BOOST_CHECK_EQUAL( q_local.m(), 2 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::iq].m(), 9 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::ilg].m(), 0 );

  SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
#endif

#ifdef __clang_analyzer__
  return; // clang analyzer complains about the matrix size below
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClass_Local jac_local(nz_local);

  BOOST_CHECK_EQUAL( jac_local.m(), 2 );
  BOOST_CHECK_EQUAL( jac_local.n(), 2 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).m(), 9 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).n(), 9 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).m(), 9 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).n(), 0  );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).m(), 0  );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).n(), 9 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).m(), 0 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).n(), 0 );

  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 9 );
  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 7 );

  const std::array<std::vector<int>,2>& freeDOFs = PrimalEqSet_Local.getFreeDOFs();

  // Check that the right dofs are frozen, format is {edge,edge,edge, node,node}
  std::vector<int> true_frozeDOFs = {5,6,7,8,9,10,11};
  std::vector<int> true_freeDOFs  = {0,1,2,3,4,12,13,14,15};

  BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[0].size());

  for ( auto it = true_frozeDOFs.begin(); it != true_frozeDOFs.end(); ++it)
    BOOST_CHECK( std::find(freeDOFs[0].begin(),freeDOFs[0].end(),*it) == (freeDOFs[0]).end() );

  for ( auto it = true_freeDOFs.begin(); it != true_freeDOFs.end(); ++it)
    BOOST_CHECK( std::find(freeDOFs[0].begin(),freeDOFs[0].end(),*it) != (freeDOFs[0]).end() );

  // Check that the right dofs are frozen, format is {edge,edge,edge, node,node}
  // NB no frozen edge dofs...
  true_freeDOFs = {};

  BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[1].size());

  //Local solve - with extracted sub-system
  SystemVectorClass_Local sln_local(rsd_local.size());

  //------------------------------------------------//
  //                  FLAT SOLVE                    //
  //------------------------------------------------//
  if (jac_local(1,1).m() > 0 ) // Flat Solve
  {
    // Copy, flatten and pivot inverse
    int m0 = jac_local(0,0).m(), m1 = jac_local(1,0).m();
    int n0 = jac_local(0,0).n(), n1 = jac_local(0,1).n();
    int qL = DLA::VectorSize<ArrayQ>::M;

    BOOST_REQUIRE( m0 + m1 == n0 + n1 );
    DLA::MatrixD<Real> jac_local_flat(qL*(m0+m1),qL*(n0+n1));
    DLA::VectorD<Real> rsd_local_flat(qL*(n0+n1)), dsln_local_flat(qL*(n0+n1));

    // 0 Row
    for ( int j = 0; j < n0; j++)
      for (int qJ = 0; qJ < DLA::VectorSize<ArrayQ>::M; qJ++)
      {
        rsd_local_flat(j + qJ*qL) = DLA::index((rsd_local(0))(j),qJ);
        for (int qI = 0; qI < DLA::VectorSize<ArrayQ>::M; qI++)
        {
          // (0,0) block
          for (int i = 0; i < m0; i++)
            jac_local_flat(i + qI*qL,j + qJ*qL) = DLA::index((jac_local(0,0))(i,j),qI,qJ);
          // (0,1) block
          for (int i = m0; i < m0 + m1; i++)
            jac_local_flat(i + qI*qL,j + qJ*qL) = DLA::index((jac_local(1,0))(i-m0,j),qI,qJ);
        }
      }


    // 1 Row
    for ( int j = n0; j < n0+n1; j++)
      for (int qJ = 0; qJ < DLA::VectorSize<ArrayQ>::M; qJ++)
      {
        rsd_local_flat(j + qJ*qL) = DLA::index((rsd_local(1))(j-n0),qJ);
        for (int qI = 0; qI < DLA::VectorSize<ArrayQ>::M; qI++)
        {
          // (1,0) block
          for (int i = 0; i < m0; i++)
            jac_local_flat(i + qI*qL,j + qJ*qL) = DLA::index((jac_local(0,1))(i,j-n0),qI,qJ);
          // (1,1) block
          for (int i = m0; i < m0 + m1; i++)
            jac_local_flat(i + qI*qL,j + qJ*qL) = DLA::index((jac_local(1,1))(i-m0,j-n0),qI,qJ);
        }
      }


    dsln_local_flat = DLA::InverseLUP::Solve(jac_local_flat, rsd_local_flat);

    // fold back up into the solution vector
    for (int j = 0; j < n0; j++)
      for (int qJ = 0; qJ < DLA::VectorSize<ArrayQ>::M; qJ++)
        DLA::index(sln_local(0)(j),qJ) = dsln_local_flat(j + qJ*qL);

    for (int j = n0; j < n0+n1; j++)
      for (int qJ = 0; qJ < DLA::VectorSize<ArrayQ>::M; qJ++)
        DLA::index(sln_local(1)(j-n0),qJ) = dsln_local_flat(j + qJ*qL);

  }
  else
    sln_local = DLA::InverseLUP::Solve(jac_local, rsd_local);

  //------------------------------------------------//
  //                END FLAT SOLVE                  //
  //------------------------------------------------//


  //Update local solution fields
  q_local -= sln_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  //Re-compute residual and check if it's zero
  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);

  std::vector<std::vector<Real>> rsdNrm( PrimalEqSet_Local.residualNorm(rsd_local) );
  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNrm) );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_Local_Galerkin_Stabilized_InteriorEdge_P2_AD_test )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunction_mitStateParam BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_CG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_CG_Cell;
  typedef Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_CG_Trace;

  typedef AlgebraicEquationSet_Local_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                         XField<PhysD2, TopoD2>> AlgEqnSet_Local;
  typedef AlgEqnSet_Local::BCParams BCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

//  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};
  std::vector<int> local_itracegroup_list = {0,1,2};

  // PDE
  Real a = 1.0;
  Real b = 1.0;
  AdvectiveFlux2D_Uniform adv( a, b );

  Real nu = 0.273;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0.3, 0.5, 0.1);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Output
  NDOutputClass fcnSqError(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnSqError, {0});
  OutputIntegrandClass errIntegrandLocal(fcnSqError, {0,1});

  const int solnOrder = 2;

  //Solution
  QField2D_CG_Cell qfld(xfld, solnOrder, BasisFunctionCategory_Hierarchical);
  qfld = 0;

  //Lagrange multipliers
  LGField2D_CG_Trace lgfld( xfld, solnOrder, BasisFunctionCategory_Hierarchical,
                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

//  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // First interior vertical edge
  std::array<int,2> edge{{1,4}};

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 9);

  //Find the mapping between the boundary conditions of local problem and the global problem
  std::vector<int> boundaryTraceGroups = xfld_local.getReSolveBoundaryTraceGroups();

  PyDict BCList_local;
  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  for (std::size_t local_group = 0; local_group < boundaryTraceGroups.size(); local_group++)
  {
    //Get the global boundary-trace group index for the current local boundary-trace group
    int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[local_group],0}).first;

    BOOST_REQUIRE(globalBTraceGroup >= 0); //Check for non-negative index

    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it)
    {
      const std::vector<int>& grouplist = it->second;
      bool found = false;

      for (int j = 0; j < (int) grouplist.size(); j++)
      {
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the BC associated with the globalBTraceGroup we want, so add it to the local BTraceGroup map
          BCBoundaryGroups_local[it->first].push_back(local_group);
          BCList_local[it->first] = PyBCList[it->first];
          found = true;
          break;
        }
      }

      if (found) break;
    }
  } //loop over local btracegroups

  std::vector<int> active_local_boundaries = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(BCList_local, BCBoundaryGroups_local);

  // active_local_boundaries, collected into sets that share dofs (if the original global fields are continuous)
  std::vector<std::vector<int>> local_boundary_bundle( BCBoundaryGroups_local.size() ); // There are this many unique global bcs

  // Bundle local groups based on whether they came from the same global group. Groups can only be broken when the global group was too
  // Loop over active_local_boundaries, then check each of boundaryTraceGroups. If they're really that global group, place in the bundle
  for (auto it = active_local_boundaries.begin(); it != active_local_boundaries.end(); ++it)
  {
    // The globalBTraceGroup from whence it came
    const int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[*it],0}).first;

    // search over list of global group bundles
    for (auto itt = BCBoundaryGroups_local.begin(); itt != BCBoundaryGroups_local.end(); ++itt)
    {
      const std::vector<int>& grouplist = itt->second;

      bool found = false;

      for (std::size_t j = 0; j < grouplist.size(); j++)
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the global bundle associated with active_local_boundary
          // Place in the corresponding bundle

          // Can't use [i] and std::size_t above because need to use iterators to move over std::map
          local_boundary_bundle[std::distance(BCBoundaryGroups_local.begin(), itt)].push_back(*it);
          found = true;
          break;
        }

      if (found) break;
    }
  }

  // remove-erase idiom | remove those bundles that are empty
  local_boundary_bundle.erase( std::remove_if( local_boundary_bundle.begin(), local_boundary_bundle.end(),
                                               [](const std::vector<int>& i){ return i.empty(); } ), // dummy function
                               local_boundary_bundle.end() );

  Field_Local<QField2D_CG_Cell> qfld_local(SANS::split(xfld_local.getReSolveCellGroups()),
                                           xfld_local, qfld, solnOrder, BasisFunctionCategory_Hierarchical);
  Field_Local<LGField2D_CG_Trace> lgfld_local( local_boundary_bundle, xfld_local, lgfld, solnOrder, BasisFunctionCategory_Hierarchical );

  std::unique_ptr<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>> up_resfld_local =
    SANS::make_unique<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>>(xfld_local,solnOrder,BasisFunctionCategory_Hierarchical);

  // zero out the field here. We're not going to actually use it, so we're going to say evaluate on all cell groups
  *up_resfld_local = 0;

  QuadratureOrder quadratureOrder_local( xfld_local, -1 );
  std::vector<Real> eqnsettol = {1e-12, 1e-12};

  std::vector<int> localCellGroups = {0,1}; // evaluating on all because we're cheats
  std::vector<int> localInteriorTraceGroups = findInteriorTraceGroup(xfld_local,0,1);

  const StabilizationMatrix stab(StabilizationType::Adjoint, TauType::Glasby, solnOrder);

  AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, lgfld_local, up_resfld_local,
                                    pde, stab, quadratureOrder_local, ResidualNorm_L2, eqnsettol,
                                    localCellGroups, localInteriorTraceGroups, PyBCList, BCBoundaryGroups_local);

  //------------------------------------------------------------------------------------------

  // Local residual
  SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  BOOST_CHECK_EQUAL( q_local.m(), 2 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::iq].m(), 13 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::ilg].m(), 0 );

  SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
#endif

#ifdef __clang_analyzer__
  return; // clang analyzer complains about the matrix size below
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClass_Local jac_local(nz_local);

  BOOST_CHECK_EQUAL( jac_local.m(), 2 );
  BOOST_CHECK_EQUAL( jac_local.n(), 2 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).m(), 13 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).n(), 13 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).m(), 13 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).n(), 0  );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).m(), 0  );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).n(), 13 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).m(), 0 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).n(), 0 );

  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  const std::array<std::vector<int>,2>& freeDOFs = PrimalEqSet_Local.getFreeDOFs();

  // Check that the right dofs are frozen, format is {edge,edge,edge, node,node}
  std::vector<int> true_frozeDOFs = {8,9,10,11,12,13,14,15,16,17,18,19};
  std::vector<int> true_freeDOFs = {0, 1, 2, 3, 4, 5, 6, 7, 20, 21, 22, 23, 24};

  BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[0].size());

  for ( auto it = true_frozeDOFs.begin(); it != true_frozeDOFs.end(); ++it)
    BOOST_CHECK( std::find(freeDOFs[0].begin(),freeDOFs[0].end(),*it) == (freeDOFs[0]).end() );

  for ( auto it = true_freeDOFs.begin(); it != true_freeDOFs.end(); ++it)
    BOOST_CHECK( std::find(freeDOFs[0].begin(),freeDOFs[0].end(),*it) != (freeDOFs[0]).end() );

  // Check that the right dofs are frozen, format is {edge,edge,edge, node,node}
  true_freeDOFs = {};

  BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[1].size());

  //Local solve - with extracted sub-system
  SystemVectorClass_Local sln_local(rsd_local.size());
  sln_local = DLA::InverseLUP::Solve(jac_local,rsd_local);

  //Update local solution fields
  q_local -= sln_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  //Re-compute residual and check if it's zero
  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  std::vector<std::vector<Real>> rsdNrm( PrimalEqSet_Local.residualNorm(rsd_local) );

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNrm) );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_Local_Galerkin_Stabilized_BoundaryEdge_P3_AD_test )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunction_mitStateParam BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_CG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_CG_Cell;
  typedef Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_CG_Trace;

  typedef AlgebraicEquationSet_Local_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                         XField<PhysD2, TopoD2>> AlgEqnSet_Local;
  typedef AlgEqnSet_Local::BCParams BCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

//  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};
  std::vector<int> local_itracegroup_list = {0,1,2};

  // PDE
  Real a = 1.0;
  Real b = 1.0;
  AdvectiveFlux2D_Uniform adv( a, b );

  Real nu = 0.273;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0.3, 0.5, 0.1);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Output
  NDOutputClass fcnSqError(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnSqError, {0});
  OutputIntegrandClass errIntegrandLocal(fcnSqError, {0,1});

  const int solnOrder = 3;

  //Solution
  QField2D_CG_Cell qfld(xfld, solnOrder, BasisFunctionCategory_Hierarchical);
  qfld = 0;

  //Lagrange multipliers
  LGField2D_CG_Trace lgfld( xfld, solnOrder, BasisFunctionCategory_Hierarchical,
                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

//  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // Left most of bottom edge
  std::array<int,2> edge{{0,1}};

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  //Find the mapping between the boundary conditions of local problem and the global problem
  std::vector<int> boundaryTraceGroups = xfld_local.getReSolveBoundaryTraceGroups();

  PyDict BCList_local;
  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  for (std::size_t local_group = 0; local_group < boundaryTraceGroups.size(); local_group++)
  {
    //Get the global boundary-trace group index for the current local boundary-trace group
    int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[local_group],0}).first;

    BOOST_REQUIRE(globalBTraceGroup >= 0); //Check for non-negative index

    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it)
    {
      const std::vector<int>& grouplist = it->second;
      bool found = false;

      for (int j = 0; j < (int) grouplist.size(); j++)
      {
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the BC associated with the globalBTraceGroup we want, so add it to the local BTraceGroup map
          BCBoundaryGroups_local[it->first].push_back(local_group);
          BCList_local[it->first] = PyBCList[it->first];
          found = true;
          break;
        }
      }

      if (found) break;
    }
  } //loop over local btracegroups

  std::vector<int> active_local_boundaries = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(BCList_local, BCBoundaryGroups_local);

  // active_local_boundaries, collected into sets that share dofs (if the original global fields are continuous)
  std::vector<std::vector<int>> local_boundary_bundle( BCBoundaryGroups_local.size() ); // There are this many unique global bcs

  // Bundle local groups based on whether they came from the same global group. Groups can only be broken when the global group was too
  // Loop over active_local_boundaries, then check each of boundaryTraceGroups. If they're really that global group, place in the bundle
  for (auto it = active_local_boundaries.begin(); it != active_local_boundaries.end(); ++it)
  {
    // The globalBTraceGroup from whence it came
    const int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[*it],0}).first;

    // search over list of global group bundles
    for (auto itt = BCBoundaryGroups_local.begin(); itt != BCBoundaryGroups_local.end(); ++itt)
    {
      const std::vector<int>& grouplist = itt->second;

      bool found = false;

      for (std::size_t j = 0; j < grouplist.size(); j++)
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the global bundle associated with active_local_boundary
          // Place in the corresponding bundle

          // Can't use [i] and std::size_t above because need to use iterators to move over std::map
          local_boundary_bundle[std::distance(BCBoundaryGroups_local.begin(), itt)].push_back(*it);
          found = true;
          break;
        }

      if (found) break;
    }
  }

  // remove-erase idiom | remove those bundles that are empty
  local_boundary_bundle.erase( std::remove_if( local_boundary_bundle.begin(), local_boundary_bundle.end(),
                                               [](const std::vector<int>& i){ return i.empty(); } ), // dummy function
                               local_boundary_bundle.end() );

  Field_Local<QField2D_CG_Cell> qfld_local(SANS::split(xfld_local.getReSolveCellGroups()),
                                           xfld_local, qfld, solnOrder, BasisFunctionCategory_Hierarchical);
  Field_Local<LGField2D_CG_Trace> lgfld_local(local_boundary_bundle, xfld_local, lgfld, solnOrder, BasisFunctionCategory_Hierarchical );

  std::unique_ptr<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>> up_resfld_local =
    SANS::make_unique<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>>(xfld_local,solnOrder,BasisFunctionCategory_Hierarchical);

  // zero out the field here. We're not going to actually use it, so we're going to say evaluate on all cell groups
  *up_resfld_local = 0;

  QuadratureOrder quadratureOrder_local( xfld_local, -1 );
  std::vector<Real> eqnsettol = {1e-10, 1e-10};

  std::vector<int> localCellGroups = {0,1}; // evaluating on all because we're cheats
  std::vector<int> localInteriorTraceGroups = findInteriorTraceGroup(xfld_local,0,1);

  const StabilizationMatrix stab(StabilizationType::Adjoint, TauType::Glasby, solnOrder);

  AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, lgfld_local, up_resfld_local,
                                    pde, stab, quadratureOrder_local, ResidualNorm_L2, eqnsettol,
                                    localCellGroups, localInteriorTraceGroups, PyBCList, BCBoundaryGroups_local);

  //------------------------------------------------------------------------------------------

  // Local residual
  SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  BOOST_CHECK_EQUAL( q_local.m(), 2 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::iq].m(), 16 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::ilg].m(), 0 );

  SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
#endif

#ifdef __clang_analyzer__
  return; // clang analyzer complains about the matrix size below
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClass_Local jac_local(nz_local);

  BOOST_CHECK_EQUAL( jac_local.m(), 2 );
  BOOST_CHECK_EQUAL( jac_local.n(), 2 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).m(), 16 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).n(), 16 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).m(), 16 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).n(), 0 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).m(), 0 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).n(), 16 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).m(), 0 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).n(), 0 );

  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  const std::array<std::vector<int>,2>& freeDOFs = PrimalEqSet_Local.getFreeDOFs();

  // Check that the right dofs are frozen, format is {cell,cell,  edge,edge,edge, node,node}
  std::vector<int> true_freeDOFs = {0, 1, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 29, 30, 31, 32};

  BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[0].size());

  for ( auto it = true_freeDOFs.begin(); it != true_freeDOFs.end(); ++it)
    BOOST_CHECK( std::find(freeDOFs[0].begin(),freeDOFs[0].end(),*it) != (freeDOFs[0]).end() );

  // Check that the right dofs are frozen, format is {edge,edge,edge, node,node}
  // NB no frozen edge dofs...
  true_freeDOFs = {};

  BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[1].size());

  //Local solve - with extracted sub-system
  SystemVectorClass_Local sln_local(rsd_local.size());
  sln_local = DLA::InverseLUP::Solve(jac_local,rsd_local);

  //Update local solution fields
  q_local -= sln_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  //Re-compute residual and check if it's zero
  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  std::vector<std::vector<Real>> rsdNrm( PrimalEqSet_Local.residualNorm(rsd_local) );

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNrm) );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AlgebraicEquationSet_Local_Galerkin_Stabilized_InteriorEdge_P3_AD_test )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunction_mitStateParam BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
//  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_CG_Cell<PhysD2, TopoD2, ArrayQ> QField2D_CG_Cell;
  typedef Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> LGField2D_CG_Trace;

  typedef AlgebraicEquationSet_Local_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                                         XField<PhysD2, TopoD2>> AlgEqnSet_Local;
  typedef AlgEqnSet_Local::BCParams BCParams;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef AlgEqnSet_Local::SystemMatrix SystemMatrixClass_Local;
  typedef AlgEqnSet_Local::SystemVector SystemVectorClass_Local;
  typedef AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern_Local;

  int ii = 2;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

//  std::vector<int> global_itracegroup_list = {0,1,2};
  std::vector<int> global_btracegroup_list = {0,1,2,3};
  std::vector<int> local_itracegroup_list = {0,1,2};

  // PDE
  Real a = 1.0;
  Real b = 1.0;
  AdvectiveFlux2D_Uniform adv( a, b );

  Real nu = 0.273;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0.3, 0.5, 0.1);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
              BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = global_btracegroup_list;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Output
  NDOutputClass fcnSqError(solnExact);
  OutputIntegrandClass errIntegrandGlobal(fcnSqError, {0});
  OutputIntegrandClass errIntegrandLocal(fcnSqError, {0,1});

  const int solnOrder = 3;

  //Solution
  QField2D_CG_Cell qfld(xfld, solnOrder, BasisFunctionCategory_Hierarchical);
  qfld = 0;

  //Lagrange multipliers
  LGField2D_CG_Trace lgfld( xfld, solnOrder, BasisFunctionCategory_Hierarchical,
                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

//  mpi::communicator comm_local = xfld.comm()->split(xfld.comm()->rank());

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // First interior vertical edge
  std::array<int,2> edge{{4,1}};

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  //Find the mapping between the boundary conditions of local problem and the global problem
  std::vector<int> boundaryTraceGroups = xfld_local.getReSolveBoundaryTraceGroups();

  PyDict BCList_local;
  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  for (std::size_t local_group = 0; local_group < boundaryTraceGroups.size(); local_group++)
  {
    //Get the global boundary-trace group index for the current local boundary-trace group
    int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[local_group],0}).first;

    BOOST_REQUIRE(globalBTraceGroup >= 0); //Check for non-negative index

    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it)
    {
      const std::vector<int>& grouplist = it->second;
      bool found = false;

      for (int j = 0; j < (int) grouplist.size(); j++)
      {
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the BC associated with the globalBTraceGroup we want, so add it to the local BTraceGroup map
          BCBoundaryGroups_local[it->first].push_back(local_group);
          BCList_local[it->first] = PyBCList[it->first];
          found = true;
          break;
        }
      }

      if (found) break;
    }
  } //loop over local btracegroups

  std::vector<int> active_local_boundaries = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(BCList_local, BCBoundaryGroups_local);

  // active_local_boundaries, collected into sets that share dofs (if the original global fields are continuous)
  std::vector<std::vector<int>> local_boundary_bundle( BCBoundaryGroups_local.size() ); // There are this many unique global bcs

  // Bundle local groups based on whether they came from the same global group. Groups can only be broken when the global group was too
  // Loop over active_local_boundaries, then check each of boundaryTraceGroups. If they're really that global group, place in the bundle
  for (auto it = active_local_boundaries.begin(); it != active_local_boundaries.end(); ++it)
  {
    // The globalBTraceGroup from whence it came
    const int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({boundaryTraceGroups[*it],0}).first;

    // search over list of global group bundles
    for (auto itt = BCBoundaryGroups_local.begin(); itt != BCBoundaryGroups_local.end(); ++itt)
    {
      const std::vector<int>& grouplist = itt->second;

      bool found = false;

      for (std::size_t j = 0; j < grouplist.size(); j++)
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the global bundle associated with active_local_boundary
          // Place in the corresponding bundle

          // Can't use [i] and std::size_t above because need to use iterators to move over std::map
          local_boundary_bundle[std::distance(BCBoundaryGroups_local.begin(), itt)].push_back(*it);
          found = true;
          break;
        }

      if (found) break;
    }
  }

  // remove-erase idiom | remove those bundles that are empty
  local_boundary_bundle.erase( std::remove_if( local_boundary_bundle.begin(), local_boundary_bundle.end(),
                                               [](const std::vector<int>& i){ return i.empty(); } ), // dummy function
                               local_boundary_bundle.end() );

  Field_Local<QField2D_CG_Cell> qfld_local(SANS::split(xfld_local.getReSolveCellGroups()),
                                           xfld_local, qfld, solnOrder, BasisFunctionCategory_Hierarchical);
  Field_Local<LGField2D_CG_Trace> lgfld_local(local_boundary_bundle, xfld_local, lgfld, solnOrder, BasisFunctionCategory_Hierarchical );

  std::unique_ptr<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>> up_resfld_local =
    SANS::make_unique<Field_DG_Cell<PhysD2,TopoD2,ArrayQ>>(xfld_local,solnOrder,BasisFunctionCategory_Hierarchical);

  // zero out the field here. We're not going to actually use it, so we're going to say evaluate on all cell groups
  *up_resfld_local = 0;

  QuadratureOrder quadratureOrder_local( xfld_local, -1 );
  std::vector<Real> eqnsettol = {1e-10, 1e-10};

  std::vector<int> localCellGroups = {0,1}; // evaluating on all because we're cheats
  std::vector<int> localInteriorTraceGroups = findInteriorTraceGroup(xfld_local,0,1);

  const StabilizationMatrix stab(StabilizationType::Adjoint, TauType::Glasby, solnOrder);

  AlgEqnSet_Local PrimalEqSet_Local(xfld_local, qfld_local, lgfld_local, up_resfld_local,
                                    pde, stab, quadratureOrder_local, ResidualNorm_L2, eqnsettol,
                                    localCellGroups, localInteriorTraceGroups, PyBCList, BCBoundaryGroups_local);

  //------------------------------------------------------------------------------------------

  // Local residual
  SystemVectorClass_Local q_local(PrimalEqSet_Local.vectorStateSize());
  PrimalEqSet_Local.fillSystemVector(q_local);

  BOOST_CHECK_EQUAL( q_local.m(), 2 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::iq].m(), 25 );
  BOOST_CHECK_EQUAL( q_local[AlgEqnSet_Local::ilg].m(), 0 );

  SystemVectorClass_Local rsd_local(PrimalEqSet_Local.vectorEqSize());
  rsd_local = 0;
  PrimalEqSet_Local.residual(q_local, rsd_local);

#if 0 //Print local residual vector
  cout <<endl << "Solution residual:" <<endl;
  for (int k = 0; k < rsd_local[0].m(); k++) cout << k << "\t" << rsd_local[0][k] <<endl;

  cout <<endl << "Lagrange multiplier residual:" <<endl;
  for (int k = 0; k < rsd_local[1].m(); k++) cout << k << "\t" << rsd_local[1][k] << endl;
#endif

#ifdef __clang_analyzer__
  return; // clang analyzer complains about the matrix size below
#endif

  //Jacobian
  SystemNonZeroPattern_Local nz_local(PrimalEqSet_Local.matrixSize());
  SystemMatrixClass_Local jac_local(nz_local);

  BOOST_CHECK_EQUAL( jac_local.m(), 2 );
  BOOST_CHECK_EQUAL( jac_local.n(), 2 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).m(), 25 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::iq).n(), 25 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).m(), 25 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iPDE, AlgEqnSet_Local::ilg).n(), 0 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).m(), 0 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::iq).n(), 25 );

  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).m(), 0 );
  BOOST_CHECK_EQUAL( jac_local(AlgEqnSet_Local::iBC, AlgEqnSet_Local::ilg).n(), 0 );

  jac_local = 0;
  PrimalEqSet_Local.jacobian(jac_local);

  const std::array<std::vector<int>,2>& freeDOFs = PrimalEqSet_Local.getFreeDOFs();

  // Check that the right dofs are frozen, format is {cell,cell,  edge,edge,edge, node,node}
  std::vector<int> true_freeDOFs = {0, 1, 2, 3, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                                    20, 21, 22, 23, 24, 49, 50, 51, 52, 53};
  // for ( auto it = freeDOFs[0].begin(); it != freeDOFs[0].end(); ++it)
  //   std::cout << "dof = " << *it << std::endl;

  BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[0].size() );

  for ( auto it = true_freeDOFs.begin(); it != true_freeDOFs.end(); ++it)
    BOOST_CHECK( std::find(freeDOFs[0].begin(),freeDOFs[0].end(),*it) != (freeDOFs[0]).end() );

  // Check that the right dofs are frozen, format is {edge,edge,edge, node,node}
  true_freeDOFs = {};

  // for ( auto it = freeDOFs[1].begin(); it != freeDOFs[1].end(); ++it)
  //   std::cout << "dof = " << *it << std::endl;

  BOOST_CHECK_EQUAL( true_freeDOFs.size(), freeDOFs[1].size() );

  //Local solve - with extracted sub-system
  SystemVectorClass_Local sln_local(rsd_local.size());
  sln_local = DLA::InverseLUP::Solve(jac_local,rsd_local);

  //Update local solution fields
  q_local -= sln_local;
  PrimalEqSet_Local.setSolutionField(q_local);

  //Re-compute residual and check if it's zero
  rsd_local = 0;
  PrimalEqSet_Local.residual(rsd_local);
  std::vector<std::vector<Real>> rsdNrm( PrimalEqSet_Local.residualNorm(rsd_local) );

  BOOST_REQUIRE( PrimalEqSet_Local.convergedResidual(rsdNrm) );

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
