// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Block_1D_Decoupled_Burgers_btest
// testing AlgebraicEquationSet_Block2x2 solves

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"
#include "Discretization/Block/JacobianParam_Decoupled.h"
#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Block_1D_Decoupled_Burgers_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Block_1D_DGBR2_Burgers_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>,
                                ViscousFlux1D_Uniform,
                                Source1D_Uniform > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef ScalarFunction1D_Tanh SolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion1DVector<LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD1, TopoD1>> PrimalEquationSetClass;

  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::TraitsType AlgEqTraitsType;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef JacobianParam_Decoupled< SystemMatrixClass,
                                   AlgEqTraitsType > JacobianParamClass;

  typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass, SystemMatrixClass,
                                        SystemMatrixClass, SystemMatrixClass> AlgEqSetType_2x2;

  typedef AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
  typedef AlgEqSetType_2x2::SystemVector BlockVectorClass;

  const Real small_tol = 1e-6;
  const Real close_tol = 1e-6;

  // PDE
  LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers> adv;

  Real nu1 = 0.1;
  Real nu2 = 0.05;
  Real A = 2.0;
  Real B = 0.0;
  ViscousFlux1D_Uniform visc1( nu1 );
  ViscousFlux1D_Uniform visc2( nu2 );

  ScalarFunction1D_Tanh MMS_soln1( nu1, A, B );
  ScalarFunction1D_Tanh MMS_soln2( nu2, A, B );

  typedef ForcingFunction1D_MMS<PDEAdvectionDiffusion1D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr1( new ForcingType(MMS_soln1) );
  std::shared_ptr<ForcingType> forcingptr2( new ForcingType(MMS_soln2) );

  Real b =  0.2;
  Source1D_Uniform source(b);

  //Exact solution
  NDPDEClass pde1(adv, visc1, source, forcingptr1);
  NDPDEClass pde2(adv, visc2, source, forcingptr2);

  // BC
  // Create a BC dictionary
  PyDict Tanh;
  Tanh[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Tanh;
  Tanh[SolutionExact::ParamsType::params.nu] = nu1;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = Tanh;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // BR2 discretization
  Real viscousEtaParameter = 2;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  SolveStatus status;
  // grid: HierarchicalP1 (aka X1)
  int ii = 60;
  // grid:
  XField1D xfld( ii );

  int order = 1;

  // solution: Hierarchical, C0
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld1(xfld, order, BasisFunctionCategory_Legendre);
  qfld1 = 0;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld2(xfld, order, BasisFunctionCategory_Legendre);
  qfld2 = 0;

  // lifting operators
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld1(xfld, order, BasisFunctionCategory_Legendre);
  for (int i=0; i<rfld1.nDOF(); i++) rfld1.DOF(i) = 0;
  FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld2(xfld, order, BasisFunctionCategory_Legendre);
  for (int i=0; i<rfld2.nDOF(); i++) rfld2.DOF(i) = 0;


  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld1(xfld, order-1, BasisFunctionCategory_Legendre);
  lgfld1 = 0;
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld2(xfld, order-1, BasisFunctionCategory_Legendre);
  lgfld2 = 0;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-12, 1e-12};

  PrimalEquationSetClass AES00(xfld, qfld1, rfld1, lgfld1, pde1, disc, quadratureOrder,
                               ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);
  PrimalEquationSetClass AES11(xfld, qfld2, rfld2, lgfld2, pde2, disc, quadratureOrder,
                               ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups);

  JacobianParamClass JP01(AES00);
  JacobianParamClass JP10(AES11);

  AlgEqSetType_2x2 BlockAES(AES00, JP01,
                            JP10, AES11);

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, ScalarGoldenSearchLineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  ScalarGoldenSearchLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] =
      LineUpdateParam::params.LineUpdate.ScalarGoldenSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = ScalarGoldenSearchLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);
  // Create the solver object
  NewtonSolver<SystemMatrixClass> Solver00( AES00, NewtonSolverDict );
  NewtonSolver<SystemMatrixClass> Solver11( AES11, NewtonSolverDict );

  // Solve AES00 directly
  // residual vectors
  SystemVectorClass q0(AES11.vectorStateSize());
  SystemVectorClass sln0(AES11.vectorStateSize());

  // set initial condition
  q0 = 0.0;
  sln0 = 0.0;
  // Solve
  status = Solver00.solve(q0, sln0);
  BOOST_CHECK( status.converged );

  // Solve AES11 directly
  // residual vectors
  SystemVectorClass q1(AES11.vectorStateSize());
  SystemVectorClass sln1(AES11.vectorStateSize());

  // set initial condition
  q1 = 0.0;
  sln1 = 0.0;
  // Solve
  status = Solver11.solve(q1, sln1);
  BOOST_CHECK( status.converged );

  // Create the solver object
  NewtonSolver<BlockMatrixClass> SolverBlock( BlockAES, NewtonSolverDict );

  // Solve BlockAES directly
  // residual vectors
  BlockVectorClass q(BlockAES.vectorStateSize());
  BlockVectorClass sln(BlockAES.vectorStateSize());

  // set initial condition
  q = 0.0;
  sln = 0.0;

  // Solve
  status = SolverBlock.solve(q, sln);
  BOOST_CHECK( status.converged );

  // Compare results for AES00
  BOOST_REQUIRE_EQUAL( q.v0.m(), q0.m() );
  BOOST_REQUIRE_EQUAL( sln.v0.m(), sln0.m() );

  for ( int ii = 0; ii < sln.v0.m(); ii++ )
    for ( int i = 0; i < sln.v0[ii].m(); i++ )
      SANS_CHECK_CLOSE( sln.v0[ii][i], sln0[ii][i], small_tol, close_tol );

  // Compare results for AES11
  BOOST_REQUIRE_EQUAL( q.v1.m(), q1.m() );
  BOOST_REQUIRE_EQUAL( sln.v1.m(), sln1.m() );

  for ( int ii = 0; ii < sln.v1.m(); ii++ )
    for ( int i = 0; i < sln.v1[ii].m(); i++ )
      SANS_CHECK_CLOSE( sln.v1[ii][i], sln1[ii][i], small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
