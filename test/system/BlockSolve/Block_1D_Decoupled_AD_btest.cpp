// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Block_1D_Decoupled_AD_btest
// testing AlgebraicEquationSet_Block2x2 solves

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"
#include "Discretization/Block/JacobianParam_Decoupled.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_CG_Cell.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Block_1D_DecoupledGeneral_SamePDE_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Block_1D_CG_AD_test )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ScalarFunction1D_Sine SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Sine> NDSolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> AlgebraicEquationSet_CGClass;
  typedef AlgebraicEquationSet_CGClass::BCParams BCParams;
  typedef AlgebraicEquationSet_CGClass::TraitsType AlgEqTraitsType;
  typedef AlgebraicEquationSet_CGClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_CGClass::SystemVector SystemVectorClass;

  typedef JacobianParam_Decoupled< SystemMatrixClass,
                                   AlgEqTraitsType > JacobianParamClass;

  typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass, SystemMatrixClass,
                                        SystemMatrixClass, SystemMatrixClass> AlgEqSetType_2x2;

  typedef AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
  typedef AlgEqSetType_2x2::SystemVector BlockVectorClass;

  const Real small_tol = 1e-6;
  const Real close_tol = 1e-6;

  // PDE
  AdvectiveFlux1D_Uniform adv1( 0 );
  AdvectiveFlux1D_Uniform adv2( 0.1 );

  Real nu = 1;
  ViscousFlux1D_Uniform visc( nu );

  Source1D_None source;

  SolutionExact MMS_soln;
  NDSolutionExact solnExact;

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(MMS_soln));

  NDPDEClass pde1(adv1, visc, source, forcingptr );
  NDPDEClass pde2(adv2, visc, source, forcingptr );

  // BC

  // Create a BC dictionary
  PyDict Sine;
  Sine[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Sine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = Sine;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // grid: HierarchicalP1 (aka X1)
  int ii = 3;
  // grid:
  XField1D xfld( ii );

  int order = 1;
  // solution: Hierarchical, C0
  Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld1(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld1 = 0;
  Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld2(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld2 = 0;

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld1( xfld, order, BasisFunctionCategory_Hierarchical,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld1 = 0;
  Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld2( xfld, order, BasisFunctionCategory_Hierarchical,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld2 = 0;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  StabilizationNitsche stab(order);
  AlgebraicEquationSet_CGClass AES00(xfld, qfld1, lgfld1, pde1, stab,
                                     quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );
  AlgebraicEquationSet_CGClass AES11(xfld, qfld2, lgfld2, pde2, stab,
                                     quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );

  JacobianParamClass JP01(AES00);
  JacobianParamClass JP10(AES11);

  AlgEqSetType_2x2 BlockAES(AES00, JP01,
                            JP10, AES11);

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, ScalarGoldenSearchLineUpdateDict, NewtonLineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  ScalarGoldenSearchLineUpdateDict[LineUpdateParam::params.LineUpdate.Method]
      = LineUpdateParam::params.LineUpdate.ScalarGoldenSearch;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method]
      = LineUpdateParam::params.LineUpdate.HalvingSearch;


  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);
  // Create the solver object
  NewtonSolver<SystemMatrixClass> Solver00( AES00, NewtonSolverDict );
  NewtonSolver<SystemMatrixClass> Solver11( AES11, NewtonSolverDict );

  // Solve AES00 directly
  // residual vectors
  SystemVectorClass q0(AES11.vectorStateSize());
  SystemVectorClass sln0(AES11.vectorStateSize());

  // set initial condition
  q0 = 0.0;
  sln0 = 0.0;
  // Solve
  SolveStatus status = Solver00.solve(q0, sln0);
  BOOST_CHECK( status.converged );

  // Solve AES11 directly
  // residual vectors
  SystemVectorClass q1(AES11.vectorStateSize());
  SystemVectorClass sln1(AES11.vectorStateSize());

  // set initial condition
  q1 = 0.0;
  sln1 = 0.0;
  // Solve
  SolveStatus status11 = Solver11.solve(q1, sln1);
  BOOST_CHECK( status11.converged );

  // Create the solver object
  NewtonSolver<BlockMatrixClass> SolverBlock( BlockAES, NewtonSolverDict );

  // Solve BlockAES directly
  // residual vectors
  BlockVectorClass q(BlockAES.vectorStateSize());
  BlockVectorClass sln(BlockAES.vectorStateSize());

  // set initial condition
  q = 0.0;
  sln = 0.0;

  // Solve
  SolveStatus statusblock = SolverBlock.solve(q, sln);
  BOOST_CHECK( statusblock.converged );

  // Compare results for AES00
  BOOST_REQUIRE_EQUAL( q.v0.m(), q0.m() );
  BOOST_REQUIRE_EQUAL( sln.v0.m(), sln0.m() );

  for ( int ii = 0; ii < sln.v0.m(); ii++ )
    for ( int i = 0; i < sln.v0[ii].m(); i++ )
      SANS_CHECK_CLOSE( sln.v0[ii][i], sln0[ii][i], small_tol, close_tol );

  // Compare results for AES11
  BOOST_REQUIRE_EQUAL( q.v1.m(), q1.m() );
  BOOST_REQUIRE_EQUAL( sln.v1.m(), sln1.m() );

  for ( int ii = 0; ii < sln.v1.m(); ii++ )
    for ( int i = 0; i < sln.v1[ii].m(); i++ )
      SANS_CHECK_CLOSE( sln.v1[ii][i], sln1[ii][i], small_tol, close_tol );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
