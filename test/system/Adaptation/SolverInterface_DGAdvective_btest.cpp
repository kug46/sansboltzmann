// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// SolverInterface_DGAdvective_test_suite
// Testing of the MOESS framework on the advection-diffusion pde

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_Solution.h"
#include "pde/AdvectionDiffusion/OutputAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/SolutionFunction1D.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Output_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_OutputWeightRsd_Galerkin.h"
#include "Discretization/DG/SolutionData_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"

#include "Adaptation/MOESS/SolverInterface_DGAdvective.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Meshing/XField1D/XField1D.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#define ERRORESTIMATE_DGADVECTIVE_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGAdvective.h"


#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolverInterface_DGAdvective_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SolverInterface_DGAdvective_2D_AD_DoubleBoundaryLayer_Triangle )
{
  /*
   * Really just Checking that it can compile and call the methods, without disguising the error
   * messages with all the shared_ptr material used in the sandbox. Not a unit test given don't
   * want to generate coverage, but do want to check it'll run.
   */
  typedef ScalarFunction2D_ASExp SolutionExact;
//  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
//  typedef BCTypeFunctionLinearRobin_sansLG BCType;
  typedef BCTypeFunction_mitStateParam BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef BCParameters<BCVector> BCParams;

  //typedef ScalarFunction2D_VarExp3 WeightFcn; // Change this to change output weighting type
  typedef ScalarFunction2D_DoubleBL WeightFcn; // Change this to change output weighting type
  typedef OutputCell_WeightedSolution<PDEClass, WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGAdvective<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGAdvective<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Grid
  int ii = 4;
  int jj = ii;

  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

  int order = 1;

  Real a = 1.0;
  Real b = 1.0;
  Real nu = 0.01;

  Real alpha = 1;

  // PDE
  AdvectiveFlux2D_Uniform adv( a, b );

  ViscousFlux2D_None visc;

  Source2D_None source;

//  // Create a solution dictionary
//  PyDict solnArgs;
//  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
//  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
//  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;
//  solnArgs[NDSolutionExact::ParamsType::params.c] = 1.0;
//  solnArgs[NDSolutionExact::ParamsType::params.s] = -1.0;
//
//  NDSolutionExact solnExact( solnArgs );
//
//  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
//  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source );

  PyDict ParamDict;

  // BC
  PyDict ASExp;
  ASExp[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.ASExp;
  ASExp[SolutionExact::ParamsType::params.a] = a;
  ASExp[SolutionExact::ParamsType::params.b] = b;
  ASExp[SolutionExact::ParamsType::params.alpha] = alpha;

  PyDict BCSoln_Bottom;
  BCSoln_Bottom[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = ASExp;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.SolutionBCType] = "Dirichlet";


  PyDict BCSoln_Right;
  BCSoln_Right[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCSoln_Top;
  BCSoln_Top[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCSoln_Left;
  BCSoln_Left[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = ASExp;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.SolutionBCType] = "Dirichlet";

  PyDict PyBCList;
  PyBCList["BC_Bottom"] = BCSoln_Bottom;
  PyBCList["BC_Right"] = BCSoln_Right;
  PyBCList["BC_Top"] = BCSoln_Top;
  PyBCList["BC_Left"] = BCSoln_Left;


  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BC_Bottom"] = {0};
  BCBoundaryGroups["BC_Right"] = {1};
  BCBoundaryGroups["BC_Top"] = {2};
  BCBoundaryGroups["BC_Left"] = {3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  std::vector<Real> tol = {1e-11, 1e-11};

  //Output functional
//  Real aa = 3.0;
  WeightFcn weightFcn(a, b, nu, 100.0, -100.0);
  NDOutputClass fcnOutput(weightFcn);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  // Dictionaries

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
//  MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
//  MOESSDict[MOESSParams::params.UniformRefinement] = false;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  const std::string filename = "tmp/dummy_test.adapthist";
  fstream fadapthist( filename, fstream::out);
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + filename);
  if ( std::remove( filename.c_str() ) != 0 )
    perror( "Error deleting file" );

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = 2000;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  SolutionClass globalSol( xfld, pde, order, order+1,
                           BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                           active_boundaries );

  const int quadOrder = -1; //2*(order + 1);

  //Create solver interface
  SolverInterfaceClass solInterface ( globalSol, ResidualNorm_Default, tol, quadOrder,
                                      cellGroups, interiorTraceGroups,
                                      PyBCList, BCBoundaryGroups,
                                      SolverContinuationDict, LinearSolverDict,
                                      outputIntegrand);

  globalSol.setSolution(0.0);

  solInterface.solveGlobalPrimalProblem();
  solInterface.solveGlobalAdjointProblem();

  solInterface.computeErrorEstimates();

}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SolverInterface_DGAdv_1D_AD_CheckErrorEstimates )
{
  typedef ScalarFunction1D_Exp3 SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_None,
                                Source1D_Uniform > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;

  typedef BCTypeFunction_mitStateParam BCType;
  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_None> BCVector;

  typedef BCParameters<BCVector> BCParams;

  typedef ScalarFunction1D_Sine AdjointExact;
  typedef SolnNDConvertSpace<PhysD1, AdjointExact> NDAdjointExact;
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_None,
                                Source1D_Uniform > PDEAdjoint1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdjoint1D> PDEAdjointClass;
  typedef SolutionFunction1D_ForcingFunction<PDEAdjointClass>      WeightFunctional;
  typedef SolnNDConvertSpace<PhysD1, WeightFunctional>           NDWeightFunctional;
  typedef OutputCell_WeightedSolution<PDEClass, WeightFunctional> WeightOutputClass;
  typedef OutputNDConvertSpace<PhysD1, WeightOutputClass>        NDWeightOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDWeightOutputClass>      OutputIntegrandClass;
  typedef ScalarFunction1D_Sine AdjointExact;
  typedef SolnNDConvertSpace<PhysD1, AdjointExact> NDAdjointExact;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_None,
                                Source1D_Uniform> PDEAdjoint1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdjoint1D> PDEAdjointClass;
  typedef SolutionFunction1D_ForcingFunction<PDEAdjointClass>      WeightFunctional;
  typedef SolnNDConvertSpace<PhysD1, WeightFunctional>           NDWeightFunctional;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_DGAdvective<PhysD1, TopoD1, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGAdv,ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_DGAdvective<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  // Grid
  const int nEl = 10;
  std::shared_ptr<XField<PhysD1, TopoD1>> pxfld = std::make_shared<XField1D>(nEl);

  Real a = 0.1;

   //PDE
  AdvectiveFlux1D_Uniform adv( a );

  ViscousFlux1D_None visc;

  Real b = 5.0;
  Source1D_Uniform source(b);

  Real aExp = 5;

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = aExp;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  PyDict ParamDict;

  // BC
  PyDict SolnBC;
  SolnBC[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Exp3;
  SolnBC[SolutionExact::ParamsType::params.a] = aExp;

  PyDict BCLeft;
  BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCLeft[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = SolnBC;
  BCLeft[BCAdvectionDiffusionParams<PhysD1,BCType>::params.SolutionBCType] = "Dirichlet";

  PyDict BCRight;
  BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCRight"] = BCRight;
  PyBCList["BCLeft"] = BCLeft;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCLeft"] = {0};
  BCBoundaryGroups["BCRight"] = {1};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-10, 1e-10};

  AdvectiveFlux1D_Uniform adv_adj( -a );

  a=1;
  NDAdjointExact adjExact(a);

  typedef ForcingFunction1D_MMS<PDEAdjoint1D> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr(new ForcingAdjType(adjExact));

  PDEAdjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  NDWeightFunctional volumeFcn( pdeadj );
  NDWeightOutputClass weightOutput( volumeFcn );
  OutputIntegrandClass outputIntegrand( weightOutput, {0} );
//  NDOutputClass fcnOutput;
//  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  std::vector<int> cellGroups;
  cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < pxfld->nInteriorTraceGroups(); i++)
  interiorTraceGroups.push_back(i);

  int orderPrimal = 1;
  int orderAdjoint = orderPrimal + 1;

  //Solution data
  std::shared_ptr<SolutionClass> pGlobalSol;
  pGlobalSol = std::make_shared<SolutionClass>(*pxfld, pde, orderPrimal, orderAdjoint,
                                                BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                active_boundaries, ParamDict);

  const int quadOrder = -1;//2*(orderPrimal + 1);

  //Create solver interface
  std::shared_ptr<SolverInterfaceClass> pInterface;
  pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                       cellGroups, interiorTraceGroups,
                                                       PyBCList, BCBoundaryGroups,
                                                       SolverContinuationDict, LinearSolverDict,
                                                       outputIntegrand);

  pGlobalSol->setSolution(0.0);

  pInterface->solveGlobalPrimalProblem();

  ////////////////////////////////////////////////////////////////////////////////////////
  // Solve Dual and Adaptation
  ////////////////////////////////////////////////////////////////////////////////////////
  pInterface->solveGlobalAdjointProblem();

  int maxIter = 5;

  Real globalEstimateInitial, globalEstimateFinal;
  Real globalIndicatorInitial, globalIndicatorFinal;
  Real outputInitial, outputFinal;

  std::vector<ArrayQ> estimateInitial (nEl), estimateFinal (nEl);

  int nDOFs = pGlobalSol->primal.qfld.nDOF();
  std::vector<ArrayQ> solutionInitial (nDOFs), solutionFinal (nDOFs);
  std::vector<ArrayQ> adjointInitial (nDOFs), adjointFinal (nDOFs);

  for (int iter = 0; iter < maxIter+1; iter++)
  {
    std::cout << std::endl << "-----Adaptation Iteration " << iter << "-----" << std::endl;

    //Compute error estimates
    pInterface->computeErrorEstimates();

    if (iter == 0)
    {
      outputInitial = pInterface->getOutput();
      globalEstimateInitial = pInterface->getGlobalErrorEstimate();
      globalIndicatorInitial = pInterface->getGlobalErrorIndicator();

      for (int i = 0; i < nEl; i++)
      {
        estimateInitial[i] = pInterface->getElementalErrorEstimate(0, i);
      }

      for (int i = 0; i<nDOFs; i++)
      {
        solutionInitial[i] = pGlobalSol->primal.qfld.DOF(i);
        adjointInitial[i] = pGlobalSol->adjoint.qfld.DOF(i);
      }
    }
    else if (iter == maxIter)
    {
      // these aren't actually the final values but that's fine for this test
      outputFinal = pInterface->getOutput();
      globalEstimateFinal = pInterface->getGlobalErrorEstimate();
      globalIndicatorFinal = pInterface->getGlobalErrorIndicator();

      for (int i = 0; i < nEl; i++)
      {
        estimateFinal[i] = pInterface->getElementalErrorEstimate(0, i);
      }

      for (int i = 0; i<nDOFs; i++)
      {
        solutionFinal[i] = pGlobalSol->primal.qfld.DOF(i);
        adjointFinal[i] = pGlobalSol->adjoint.qfld.DOF(i);
      }
    }

    std::shared_ptr<XField<PhysD1, TopoD1>> pxfldNew;
    pxfldNew = std::make_shared<XField1D>(nEl);

    interiorTraceGroups.clear();
    for (int j = 0; j < pxfldNew->nInteriorTraceGroups(); j++)
      interiorTraceGroups.push_back( j );

    std::shared_ptr<SolutionClass> pGlobalSolNew;
    pGlobalSolNew = std::make_shared<SolutionClass>(*pxfldNew, pde, orderPrimal, orderAdjoint,
                                                    BasisFunctionCategory_Legendre, BasisFunctionCategory_Legendre,
                                                    active_boundaries, ParamDict);

    //Perform L2 projection from solution on previous mesh
    pGlobalSolNew->setSolution( *pGlobalSol );

    std::shared_ptr<SolverInterfaceClass> pInterfaceNew;
    pInterfaceNew = std::make_shared<SolverInterfaceClass>(*pGlobalSolNew, ResNormType, tol, quadOrder,
                                                           cellGroups, interiorTraceGroups,
                                                           PyBCList, BCBoundaryGroups,
                                                           SolverContinuationDict, LinearSolverDict,
                                                           outputIntegrand);

    //Update pointers to the newest problem (this deletes the previous mesh and solutions)
    pxfld = pxfldNew;
    pGlobalSol = pGlobalSolNew;
    pInterface = pInterfaceNew;

    pInterface->solveGlobalPrimalProblem();
    pInterface->solveGlobalAdjointProblem();
  }
  const Real small_tol = 1e-7;
  const Real close_tol = 5e-7;

  SANS_CHECK_CLOSE(outputInitial, outputFinal, small_tol, close_tol);
  SANS_CHECK_CLOSE(globalEstimateInitial, globalEstimateFinal, small_tol, close_tol);
  SANS_CHECK_CLOSE(globalIndicatorInitial, globalIndicatorFinal, small_tol, close_tol);

  for (int i = 0; i < nEl; i++)
  {
    SANS_CHECK_CLOSE(estimateInitial[i], estimateFinal[i], small_tol, close_tol);
  }

  for (int i = 0; i < nDOFs; i++)
  {
    SANS_CHECK_CLOSE(solutionInitial[i], solutionFinal[i], small_tol, close_tol);
    SANS_CHECK_CLOSE(adjointInitial[i], adjointFinal[i], small_tol, close_tol);
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
