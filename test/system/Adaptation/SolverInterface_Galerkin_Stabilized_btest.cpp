// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// SolverInterface_Galerkin_Stabilized_test_suite
// Testing of the MOESS framework on the advection-diffusion pde

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>

#include "pyrite_fstream.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "Adaptation/MOESS/SolverInterface_Galerkin.h"
#include "Adaptation/MeshAdapter.h"

#include "Field/Local/XField_LocalPatch.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;

//Explicitly instantiate the classes so that coverage information is correct
namespace SANS
{

}

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( SolverInterface_Galerkin_test_suite )

/*
 * Really just Checking that it can compile and call the methods, without disguising the error
 * messages with all the shared_ptr material used in the sandbox. Not a unit test given don't
 * want to generate coverage, but do want to check it'll run.
 */

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SolverInterface_Galerkin_Stabilized_2D_AD_DoubleBoundaryLayer_Triangle )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
//  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_sansLG BCType;
//  typedef BCTypeFunction_mitStateParam BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

//  typedef ScalarFunction2D_VarExp3 WeightFcn; // Change this to change output weighting type
  typedef ScalarFunction2D_DoubleBL WeightFcn; // Change this to change output weighting type
  typedef OutputCell_WeightedSolution<PDEClass, WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Grid
  int ii = 4;
  int jj = ii;

  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

  int order = 1;

  Real a = 1.0;
  Real b = 1.0;
  Real nu = 0.01;

  // PDE
  AdvectiveFlux2D_Uniform adv( a, b );

  ViscousFlux2D_Uniform visc( nu, 0., 0., nu );

  Source2D_UniformGrad source(0.0, 0.0, 0.0);

  // Create a solution dictionary
//  PyDict solnArgs;
//  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
//  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
//  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;
//  solnArgs[NDSolutionExact::ParamsType::params.c] = 1.0;
//  solnArgs[NDSolutionExact::ParamsType::params.s] = -1.0;
//
//  NDSolutionExact solnExact( solnArgs );
//
//  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
//  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln_Bottom;
  BCSoln_Bottom[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;

  PyDict BCSoln_Right;
  BCSoln_Right[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;

  PyDict BCSoln_Top;
  BCSoln_Top[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;

  PyDict BCSoln_Left;
  BCSoln_Left[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;

  PyDict PyBCList;
  PyBCList["BC_Bottom"] = BCSoln_Bottom;
  PyBCList["BC_Right"] = BCSoln_Right;
  PyBCList["BC_Top"] = BCSoln_Top;
  PyBCList["BC_Left"] = BCSoln_Left;


  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BC_Bottom"] = {0};
  BCBoundaryGroups["BC_Right"] = {1};
  BCBoundaryGroups["BC_Top"] = {2};
  BCBoundaryGroups["BC_Left"] = {3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Galerkin Stabilization
  const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, order+1);

  std::vector<Real> tol = {1e-11, 1e-11};

  //Output functional
//  Real aa = 3.0;
  WeightFcn weightFcn(a, b, nu, 100.0, -100.0);
  NDOutputClass fcnOutput(weightFcn);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  // Nonlinear solver dicts
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);

  // Dictionaries

  PyDict MOESSDict;
  MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
  MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
//  MOESSDict[MOESSParams::params.LocalSolve] = MOESSParams::params.LocalSolve.Edge;
//  MOESSDict[MOESSParams::params.UniformRefinement] = false;

  PyDict MesherDict;
  MesherDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.Name] = MeshAdapterParams<PhysD2, TopoD2>::params.Mesher.FeFloa;

  std::string filename = "tmp/dummy_test.adapthist";
  fstream fadapthist( filename, fstream::out);
  BOOST_REQUIRE_MESSAGE(fadapthist.good(), "Error opening file: " + filename);
  if ( std::remove( filename.c_str() ) != 0 )
    perror( "Error deleting file" );

  PyDict AdaptDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.TargetCost] = 2000;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Algorithm] = MOESSDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.Mesher] = MesherDict;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.FilenameBase] = filename;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpStepMatrix] = false;
  AdaptDict[MeshAdapterParams<PhysD2, TopoD2>::params.dumpRateMatrix] = false;

  MeshAdapterParams<PhysD2, TopoD2>::checkInputs(AdaptDict);

  MeshAdapter<PhysD2, TopoD2> mesh_adapter(AdaptDict, fadapthist);

  std::vector<int> cellGroups = {0};
  std::vector<int> interiorTraceGroups;
  for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
    interiorTraceGroups.push_back(i);

  //Solution data
  SolutionClass globalSol( xfld, pde, order, order+1,
                           BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                           active_boundaries, stab);

  const int quadOrder = -1; //2*(order + 1);

  //Create solver interface
  SolverInterfaceClass solInterface ( globalSol, ResidualNorm_L2, tol, quadOrder,
                                      cellGroups, interiorTraceGroups,
                                      PyBCList, BCBoundaryGroups,
                                      SolverContinuationDict, LinearSolverDict,
                                      outputIntegrand);

  globalSol.setSolution(0.0);

  solInterface.solveGlobalPrimalProblem();
  solInterface.solveGlobalAdjointProblem();

  solInterface.computeErrorEstimates();

  std::vector<Real> local_error;

  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);
  Field_NodalView nodalView(xfld,{0});
  std::array<int,2> edge{{6,7}};

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalView);

  LocalSolveStatus status = solInterface.solveLocalProblem(xfld_local,local_error);

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
