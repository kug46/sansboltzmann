// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_DGBR2_BuckleyLeverett_ST_btest
// testing of 1-D DG with Buckley-Leverett

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionSquared.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_1D_DGBR2_BuckleyLeverett_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorOrder_1D_DGBR2_BuckleyLeverett_ST )
{

  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;

  typedef ScalarFunction1D_SineSineUnsteady ExactSolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

  typedef ForcingFunction1D_MMS<PDEClass> ForcingFunction;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCBuckleyLeverett1DVector<PDEClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, ExactSolutionClass> ErrorClass;
  typedef OutputNDConvertSpaceTime<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;

  // PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.2;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 4.0;
  CapillaryModel cap_model(pc_max);

  // Create a Solution dictionary
  PyDict SolnDict;
  SolnDict[BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.Name]
           = BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function.SineSineUnsteady;
  SolnDict[ExactSolutionClass::ParamsType::params.a] = 1.0;
  SolnDict[ExactSolutionClass::ParamsType::params.b] = 1.0;
  SolnDict[ExactSolutionClass::ParamsType::params.c] = 0.1;
  SolnDict[ExactSolutionClass::ParamsType::params.A] = 0.8;

  ExactSolutionClass solnExact(SolnDict);
  NDExactSolutionClass solnExactND(SolnDict);

  std::shared_ptr<ForcingFunction> forceptr( new ForcingFunction(solnExact) );

  NDPDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model, forceptr);

  // BCs
  // Create a BC dictionary
  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params.Function] = SolnDict;
//  BCSoln[BCBuckleyLeverett1DParams<BCTypeFunction_mitStateParam>::params.SolutionBCType] =
//         BCBuckleyLeverett1DParams<BCTypeFunction_mitStateParam>::params.SolutionBCType.Robin;
//  BCSoln[BCBuckleyLeverett1DParams<BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC_Function;
  TimeIC[BCBuckleyLeverett1DParams<BCTypeTimeIC_Function>::params.Function] = SolnDict;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["ExactSolBC"] = BCSoln;
  PyBCList["TimeIC"] = TimeIC;
  PyBCList["BCNone"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["ExactSolBC"] = {XField2D_Box_UnionJack_Triangle_X1::iLeft, XField2D_Box_UnionJack_Triangle_X1::iRight};
  BCBoundaryGroups["BCNone"] = {XField2D_Box_UnionJack_Triangle_X1::iTop};
  BCBoundaryGroups["TimeIC"] = {XField2D_Box_UnionJack_Triangle_X1::iBottom};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // BR2 discretization
  Real viscousEtaParameter = Triangle::NTrace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  //Volume integral of Sw^2 for the exact solution
  Real output_exact = 0.01*(17.0 + 64.0/(PI*PI));

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 10;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  Real outputerrVec[10]; // output error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/ErrorOrder_1D_DGBR2_BuckleyLeverett_ST.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_DGBR2_BuckleyLeverett_ST_FullTest.txt", 1e-10, 5e-7, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_DGBR2_BuckleyLeverett_ST_MinTest.txt", 1e-10, 5e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << ", \"Output error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "Running full test..." << endl;
#endif
  int ordermax = 3;
#else
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution:
    int ii, jj;
    int factormin = 1;
#ifdef SANS_FULLTEST
    int factormax = 5;
#else
    int factormax = 2;
#endif
    for (int factor = factormin; factor <= factormax; factor++)
    {
      ii = 4*factor;
      jj = ii;

      // grid:
      XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 1, 0, 1, true );
//      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, 50, 0, 25);

      // Initial solution
      ArrayQ qinit;
      pde.setDOFFrom( qinit, 0.5, "Sw" );

      // solution: Hierarchical, C0
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      qfld = qinit;

      // Or, use the projection of the exact solution as an initial solution
      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExactND, {0}), (xfld, qfld) );
//      output_Tecplot( qfld, "tmp/qfld_init.plt" );
      const int nDOFPDE = qfld.nDOF();

      // lifting operators
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      rfld = 0.0;

      // Lagrange multipliers
      std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, active_BGroup_list);
      lgfld = 0.0;

      //--------PRIMAL SOLVE------------

      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-11, 1e-11};
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups);

      // Create the solver object
      NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );

      // set initial condition from current solution in solution fields
      SystemVectorClass sln0(PrimalEqSet.vectorStateSize());
      PrimalEqSet.fillSystemVector(sln0);

#ifdef SANS_VERBOSE
      std::cout<<"DOF: "<<sln0[0].m() + sln0[1].m()<<std::endl;
#endif

      // nonlinear solve
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SolveStatus status = nonlinear_solver.solve(sln0, sln);
      BOOST_CHECK( status.converged );

#if 0
      // jacobian nonzero pattern
      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
      PrimalEqSet.jacobian(sln0, nz);

      // jacobian
      SystemMatrixClass jac(nz);
      PrimalEqSet.jacobian(sln0, jac);

      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
      fstream fout00( "tmp/jac00.mtx", fstream::out );
      cout << "btest: global jac00" << endl;  WriteMatrixMarketFile( jac(0,0), fout00 );
      fstream fout10( "tmp/jac10.mtx", fstream::out );
      cout << "btest: global jac10" << endl;  WriteMatrixMarketFile( jac(1,0), fout10 );
      fstream fout01( "tmp/jac01.mtx", fstream::out );
      cout << "btest: global jac01" << endl;  WriteMatrixMarketFile( jac(0,1), fout01 );
#endif

      // L2 solution error
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( errorIntegrand, SquareError ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError;

      ArrayQ output = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( outputIntegrand, output ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      ArrayQ output_error = fabs(output - output_exact);

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      outputerrVec[indx] = output_error;
      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm ) << ", Output error = " << output_error;
      if (indx > 1)
      {
        cout << "  (L2 err ratio = " << normVec[indx-1]/normVec[indx-2]
             << ", Output err ratio = " << outputerrVec[indx-1]/outputerrVec[indx-2] << ")";
        Real slope = (log(normVec[indx-1]) - log(normVec[indx-2]))/(log(hVec[indx-1]) - log(hVec[indx-2]));
        Real output_slope = (log(outputerrVec[indx-1]) - log(outputerrVec[indx-2]))/(log(hVec[indx-1]) - log(hVec[indx-2]));
        cout << "  (L2 slope = " << slope << ") (Output slope = " << output_slope <<")";
      }
      cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/slnDGBR2_BuckleySpaceTime_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
#endif

    } //grid refinement loop

#if 1
    // Tecplot output
    resultFile << "ZONE T=\"DG BR2 P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real L2_slope = 0, output_slope = 0;
      if (n > 0)
      {
        L2_slope = (log(normVec[n]) - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
        output_slope = (log(outputerrVec[n]) - log(outputerrVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      }
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << L2_slope;
      resultFile << ", " << output_slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << L2_slope;
      pyriteFile.close(1e-3) << output_slope << std::endl;
    }
#endif

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  } //order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
