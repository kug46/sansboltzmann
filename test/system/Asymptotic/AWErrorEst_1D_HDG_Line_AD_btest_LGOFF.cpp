// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEst_1D_HDG_Line_AD_btest
// testing error estimation of 1-D HDG with Advection-Diffusion on triangles
//
// case: double BL

#undef SANS_FULLTEST
//#define SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "tools/timer.h"
#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/SolutionFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction_MMS.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/HDG/AWIntegrandFunctor_HDG.h"
#include "Discretization/HDG/AWIntegrandBoundaryFunctor_HDG.h"
#include "Discretization/HDG/AWIntegrandBCFunctor_HDG.h"

#include "Discretization/HDG/AWResidualCell_HDG.h"
#include "Discretization/HDG/AWResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/AWResidualBoundaryTrace_HDG.h"
#include "Discretization/HDG/AWResidualBCTrace_HDG.h"

#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementIntegral.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( AWErrorEst_NEW_HDG_Line_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWErrorEst_1D_HDG_Line_AD )
{
  timer totaltime;

  typedef SurrealS<1> SurrealClass;


#if 1  // Laplacian w/ Exponential functions
  // Exacts
  typedef ScalarFunction1D_Exp3 SolutionExact;
  typedef ScalarFunction1D_Sine AdjointExact;

  // ND Exacts
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD1, AdjointExact> NDAdjointExact;

  // Primal PDE
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEAdvectionDiffusion1D::template ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::template MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCAdvectionDiffusion<PhysD1,BCType> BCClassRaw;
  typedef BCNDConvertSpace<PhysD1, BCClassRaw> BCClass;

  // Adjoint PDE
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEAdjoint1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdjoint1D> PDEAjointClass;

  typedef SolnNDConvertSpace<PhysD1,  SolutionFunction1D_ForcingFunction<PDEAjointClass> > VolumeFcn;
  typedef IntegrandCell_WeightedSolution<PDEClass,VolumeFcn>                                IntegrandClass;
  typedef IntegrandClass::Functor<ArrayQ,TopoD1,Line>                                       FunctorClass;
  typedef IntegrandCell_WeightedExactSolution<PDEClass,VolumeFcn,NDSolutionExact>           ExactIntegrandClass;
  typedef ExactIntegrandClass::Functor<TopoD1,Line>                                         ExactFunctorClass;


  // PDE
  Real u=0.;
  AdvectiveFlux1D_Uniform adv( u );

  Real nu = 1;
  ViscousFlux1D_Uniform visc( nu );

  Source1D_None source;

  Real a=5;

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction1D_MMS<PDEAdvectionDiffusion1D, NDSolutionExact> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnArgs) );

  PDEClass pde( adv, visc, source, forcingptr );

  // BC
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(a) );

  BCClass bc( solnExactPtr, visc );

  // Adjoint PDE

  AdvectiveFlux1D_Uniform adv_adj( -u );

  a=1;
  NDAdjointExact adjExact(a);

  typedef ForcingFunction1D_MMS<PDEAdjoint1D, NDAdjointExact> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact) );

  PDEAjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  // Weighting function for the volume output based on the forcing function of the adjoint PDE
  VolumeFcn  volumeFcn( pdeadj );

  cout << "case: Laplacian w/ Exponentials" << endl;

#endif

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundClass;

  typedef AWIntegrandCell_HDG<PDEClass,NDAdjointExact> AWIntegrandCellClass;
  typedef AWIntegrandTrace_HDG<PDEClass,NDAdjointExact> AWIntegrandTraceClass;
  typedef AWIntegrandBoundary_HDG<PDEClass,NDAdjointExact> AWIntegrandBoundClass;
  typedef AWIntegrandBC_HDG<PDEClass,NDAdjointExact, BCClass> AWIntegrandBCClass;

  string lRef;
  // HDG discretization
  // lRef = 1/10 for pyrite, half min distance to peak
  DiscretizationHDG<PDEClass> disc( pde, Global , AugGradient, 1./10 ); cout << "...using HDG-L" << endl; lRef = "L";
  //DiscretizationHDG<PDEClass> disc( pde, Local); cout << "...using HDG-h" << endl; lRef = "h";

  const std::vector<int> BoundaryGroups = {0,1};
  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnInt( pde, disc, {0} );
  IntegrandBoundClass fcnBC( pde, bc, BoundaryGroups, disc );

  // AW integrands
  AWIntegrandCellClass AWfcnCell( pde, adjExact,  disc );
  AWIntegrandTraceClass AWfcnInt( pde, adjExact, disc );
  AWIntegrandBoundClass AWfcnBound( pde, adjExact, disc, BoundaryGroups );
  AWIntegrandBCClass AWfcnBC( pde, adjExact, disc, bc, BoundaryGroups );

  // norm data
  const int vsize = 25;
  Real hVec[vsize];
  Real globalErrorVec[vsize], globalEstVec[vsize], globalEstErrVec[vsize], globalEffVec[vsize];
  Real localErrorVec[vsize], localEstVec[vsize], localEstErrVec[vsize], localEffVec[vsize];
  Real globalErrorRate[vsize], globalEstRate[vsize], globalEstErrRate[vsize], globalEffRate[vsize];
  Real localErrorRate[vsize], localEstRate[vsize], localEstErrRate[vsize], localEffRate[vsize];
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::string filename = "tmp/ErrorEst1D_HDG_" + lRef;
  std::ofstream resultFile(filename +".plt", std::ios::out);
  std::ofstream datFile(filename + ".dat", std::ios::out);
  std::string pyritefilename = "IO/ErrorEstimates/AWErrorEst_1D_HDG_" + lRef + "_AD_FullTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-10, 5e-10, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  std::stringstream datFile;
  std::string pyritefilename = "IO/ErrorEstimates/AWErrorEst_1D_HDG_" + lRef + "_AD_MinTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-10, 5e-10, pyrite_file_stream::check);
#endif

  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"Global Error\"";
  resultFile << ", \"Global Error Estimate\"";
  resultFile << ", \"Global Error Estimate Error\"";
  resultFile << ", \"Global 1-Effectivity\"";
  resultFile << ", \"Global Error Rate\"";
  resultFile << ", \"Global Error Estimate Rate\"";
  resultFile << ", \"Global Error Estimate Error Rate\"";
  resultFile << ", \"Global 1-Effectivity Rate\"";
  resultFile << ", \"Local Error\"";
  resultFile << ", \"Local Error Estimate\"";
  resultFile << ", \"Local Error Estimate Error\"";
  resultFile << ", \"Local 1-Effectivity\"";
  resultFile << ", \"Local Error Rate\"";
  resultFile << ", \"Local Error Estimate Rate\"";
  resultFile << ", \"Local Error Estimate Error Rate\"";
  resultFile << ", \"Local 1-Effectivity Rate\"";
  resultFile << endl;

  // For Latex Output
  datFile << "order,"
      "h,"
      "globalError,"
      "globalErrorEstimate,"
      "globalErrorEstimateError,"
      "globalEffectivity,"
      "globalErrorRate,"
      "globalErrorEstimateRate,"
      "globalErrorEstimateErrorRate,"
      "globalEffectivityRate,"
      "localError,"
      "localErrorEstimate,"
      "localErrorEstimateError,"
      "localEffectivity,"
      "localErrorRate,"
      "localErrorEstimateRate,"
      "localErrorEstimateErrorRate,"
      "localEffectivityRate";
  datFile << endl;


  int quadorder=-1;
  ElementIntegral <TopoD1, Line, ArrayQ> ElemIntegral(quadorder);

//---------------------------------------------------------------------------//
  // Computer the exact output on a fine grid and high-quadrature
  Real globalExt=0;

  {
    XField1D xfld( 50 );

    // Field Cell Types
    typedef typename XField<PhysD1, TopoD1>::FieldCellGroupType<Line> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;

    ExactIntegrandClass exFcnint( volumeFcn, solnExact, {0} );

    ArrayQ localExt=0;

    const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Line>(0);

    ElementXFieldClass  xfldElem( xfldCell.basis() );

    // Loop over Volume Integrals
    for (int elem = 0; elem < xfldCell.nElem(); elem++)
    {
      xfldCell.getElement( xfldElem, elem );

      ExactFunctorClass exFcn = exFcnint.integrand(xfldElem);

      localExt = 0;
      ElemIntegral(exFcn, xfldElem, localExt);

      globalExt += localExt;
    }
    //std::cout << "globalExt = " << std::scientific << std::setprecision(16) << globalExt << std::endl;
  }
//---------------------------------------------------------------------------//

  // orderinc =2 is used for the pyrite
  const int orderinc = 2;

  int ordermin = 1;
#ifndef SANS_FULLTEST
  // ordermax = 1 is used for the pyrite
  int ordermax = 1;
#else
  // ordermax = 2 is used for the pyrite
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
    cout << "...running full test" << endl;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
#ifndef SANS_FULLTEST
      //ii = 2 is used for the pyrite
      ii = 2;
#else
      //ii = 4*power is used for the pyrite
      ii = 4*power;
#endif

      cout << std::scientific << std::setprecision(7);
      // grid: Hierarchical P1 ( aka X1)

      XField1D xfld( ii );

      int nInteriorTraceGroups = xfld.nInteriorTraceGroups();
      int nBoundaryTraceGroups = xfld.nBoundaryTraceGroups();
      int nCellGroups = xfld.nCellGroups();

      BOOST_CHECK_EQUAL( 1, nCellGroups );
      BOOST_CHECK_EQUAL( 1, nInteriorTraceGroups );
      BOOST_CHECK_EQUAL( 2, nBoundaryTraceGroups );

      BOOST_REQUIRE_EQUAL( 1, nCellGroups ); //unit test hard coded for single area group

      // HDG solution field
      // solution in p: Legendre
      // cell solution
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroups);

      int nDOFPDE = qfld.nDOF();
      int nDOFAu  = afld.nDOF();
      int nDOFInt = qIfld.nDOF();
      int nDOFBC  = lgfld.nDOF();

      // quadrature rule
      int quadratureOrder[2] = {-1, -1};    // max
      int quadratureOrderMin[2] = {0, 0};     // min

      // linear system setup

      typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
      typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
      typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
      typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;

      // residual

      SystemVectorClass rsd = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};
      rsd = 0;

      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsd(0), rsd(1)),
                                              xfld, (qfld, afld), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(0), rsd(1), rsd(2)),
                                                              xfld, (qfld, afld), qIfld, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsd(0), rsd(1), rsd(3)),
                                                          xfld, (qfld, afld), lgfld, quadratureOrder, nBoundaryTraceGroups );

      // jacobian nonzero pattern
      //
      //        u   q  uI  lg
      //   PDE  X   X   X   X
      //   Au   X   X   X   X
      //   Int  X   X   X   0
      //   BC   X   X   0   0

      typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

      //fld.SystemMatrixSize();
      DLA::MatrixD<NonZeroPatternClass> nz =
          {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFAu}, {nDOFPDE, nDOFInt}, {nDOFPDE, nDOFBC} },
           { {nDOFAu , nDOFPDE}, {nDOFAu , nDOFAu}, {nDOFAu , nDOFInt}, {nDOFAu , nDOFBC} },
           { {nDOFInt, nDOFPDE}, {nDOFInt, nDOFAu}, {nDOFInt, nDOFInt}, {nDOFInt, nDOFBC} },
           { {nDOFBC , nDOFPDE}, {nDOFBC , nDOFAu}, {nDOFBC , nDOFInt}, {nDOFBC , nDOFBC} }};

      NonZeroPatternClass& nzPDE_q  = nz(0,0);
      NonZeroPatternClass& nzPDE_a  = nz(0,1);
      NonZeroPatternClass& nzPDE_qI = nz(0,2);
      NonZeroPatternClass& nzPDE_lg = nz(0,3);

      NonZeroPatternClass& nzAu_q  = nz(1,0);
      NonZeroPatternClass& nzAu_a  = nz(1,1);
      NonZeroPatternClass& nzAu_qI = nz(1,2);
      NonZeroPatternClass& nzAu_lg = nz(1,3);

      NonZeroPatternClass& nzInt_q  = nz(2,0);
      NonZeroPatternClass& nzInt_a  = nz(2,1);
      NonZeroPatternClass& nzInt_qI = nz(2,2);
      //NonZeroPatternClass& nzInt_lg = nz(2,3);

      NonZeroPatternClass& nzBC_q  = nz(3,0);
      NonZeroPatternClass& nzBC_a  = nz(3,1);
      //NonZeroPatternClass& nzBC_qI = nz(3,2);
      NonZeroPatternClass& nzBC_lg = nz(3,3);

      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nzPDE_q, nzPDE_a,
                                                                                      nzAu_q, nzAu_a),
                                              xfld, (qfld, afld), quadratureOrderMin, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, nzPDE_q, nzPDE_a, nzPDE_qI,
                                                          nzAu_q ,          nzAu_qI ,
                                                          nzInt_q, nzInt_a, nzInt_qI),
          xfld, (qfld, afld), qIfld, quadratureOrderMin, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, nzPDE_q, nzPDE_a, nzPDE_lg,
                                                               nzAu_q , nzAu_a , nzAu_lg ,
                                                               nzBC_q , nzBC_a , nzBC_lg ),
          xfld, (qfld, afld), lgfld, quadratureOrderMin, nBoundaryTraceGroups);

      // jacobian
      SystemMatrixClass jac(nz);

      SparseMatrixClass& jacPDE_q  = jac(0,0);
      SparseMatrixClass& jacPDE_a  = jac(0,1);
      SparseMatrixClass& jacPDE_qI = jac(0,2);
      SparseMatrixClass& jacPDE_lg = jac(0,3);

      SparseMatrixClass& jacAu_q  = jac(1,0);
      SparseMatrixClass& jacAu_a  = jac(1,1);
      SparseMatrixClass& jacAu_qI = jac(1,2);
      SparseMatrixClass& jacAu_lg = jac(1,3);

      SparseMatrixClass& jacInt_q  = jac(2,0);
      SparseMatrixClass& jacInt_a  = jac(2,1);
      SparseMatrixClass& jacInt_qI = jac(2,2);
      //SparseMatrixClass& jacInt_lg = jac(2,3);

      SparseMatrixClass& jacBC_q  = jac(3,0);
      SparseMatrixClass& jacBC_a  = jac(3,1);
      //SparseMatrixClass& jacBC_qI = jac(3,2);
      SparseMatrixClass& jacBC_lg = jac(3,3);

      jac = 0;

      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacPDE_q, jacPDE_a,
                                                                                      jacAu_q,  jacAu_a),
                                              xfld, (qfld, afld), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacPDE_q, jacPDE_a, jacPDE_qI,
                                                          jacAu_q ,           jacAu_qI ,
                                                          jacInt_q, jacInt_a, jacInt_qI),
          xfld, (qfld, afld), qIfld, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jacPDE_q, jacPDE_a, jacPDE_lg,
                                                               jacAu_q , jacAu_a , jacAu_lg ,
                                                               jacBC_q , jacBC_a , jacBC_lg ),
          xfld, (qfld, afld), lgfld, quadratureOrder, nBoundaryTraceGroups);

#if 0
      fstream fout( "tmp/jac.dat", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver;
      SystemVectorClass sln = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};
      sln = solver.inverse(jac)*rsd;

      // update solution
      for (int k = 0; k < nDOFPDE; k++)
        qfld.DOF(k) -= sln[0][k];
      for (int k = 0; k < nDOFPDE; k++)
        for (int d = 0; d < PhysD1::D; d++)
          afld.DOF(k)[d] -= sln[1][PhysD1::D*k+d];
      for (int k = 0; k < nDOFInt; k++)
        qIfld.DOF(k) -= sln[2][k];
      for (int k = 0; k < nDOFBC; k++)
        lgfld.DOF(k) -= sln[3][k];

      // check that the residual is zero

#if 0
      // Tecplot dump
      string filename = "tmp/slnHDG_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, qfldArea, rxfldArea, ryfldArea, &solnExact, filename, order-1 );
#endif

      // Adjoint in p

      // HDG adjoint solution field

      // cell solution
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qflda(xfld, order, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> aflda(xfld, order, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIflda(xfld, order, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgflda(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroups);

      nDOFPDE = qflda.nDOF();
      nDOFAu  = aflda.nDOF();
      nDOFInt = qIflda.nDOF();
      nDOFBC  = lgflda.nDOF();

      // jacobian transpose
      // jacobian nonzero pattern
      //
      //        u   q  uI  lg
      //   PDE  X   X   X   X
      //   Au   X   X   X   X
      //   Int  X   X   X   0
      //   BC   X   X   0   0

      //Create a transposed non-zero pattern
      DLA::MatrixD<NonZeroPatternClass> nzT(Transpose(nz));

      SystemMatrixClass jacT(nzT);

      // ND version
      auto jacTPDE_q  = Transpose(jacT)(0,0);
      auto jacTPDE_a  = Transpose(jacT)(0,1);
      auto jacTPDE_qI = Transpose(jacT)(0,2);
      auto jacTPDE_lg = Transpose(jacT)(0,3);

      auto jacTAu_q  = Transpose(jacT)(1,0);
      auto jacTAu_a  = Transpose(jacT)(1,1);
      auto jacTAu_qI = Transpose(jacT)(1,2);
      auto jacTAu_lg = Transpose(jacT)(1,3);

      auto jacTInt_q  = Transpose(jacT)(2,0);
      auto jacTInt_a  = Transpose(jacT)(2,1);
      auto jacTInt_qI = Transpose(jacT)(2,2);
      //auto jacInt_lg  = Transpose(jacT)(2,3);

      auto jacTBC_q  = Transpose(jacT)(3,0);
      auto jacTBC_a  = Transpose(jacT)(3,1);
      //auto jacTBC_qI = Transpose(jacT)(3,2);
      auto jacTBC_lg = Transpose(jacT)(3,3);

      jac = 0;

      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacTPDE_q, jacTPDE_a,
                                                                                      jacTAu_q,  jacTAu_a),
                                              xfld, (qfld, afld), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacTPDE_q, jacTPDE_a, jacTPDE_qI,
                                                          jacTAu_q ,            jacTAu_qI ,
                                                          jacTInt_q, jacTInt_a, jacTInt_qI),
          xfld, (qflda, aflda), qIflda, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jacTPDE_q, jacTPDE_a, jacTPDE_lg,
                                                               jacTAu_q , jacTAu_a , jacTAu_lg ,
                                                               jacTBC_q , jacTBC_a , jacTBC_lg ),
          xfld, (qflda, aflda), lgflda, quadratureOrder, nBoundaryTraceGroups);

      // solve
      SystemVectorClass adj = {{nDOFPDE}, {nDOFAu}, {nDOFInt}, {nDOFBC}};

      rsd = 0; // reset residual
      IntegrandClass outputFcn( volumeFcn, {0} ); // declared back with the pde
      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell<SurrealClass>( outputFcn, rsd(0) ), xfld, qflda, quadratureOrder, xfld.nCellGroups() );

      // compute the adjoint
      adj = solver.inverse(jacT)*rsd;

      // update solution
      for (int k = 0; k < nDOFPDE; k++)
        qflda.DOF(k) = adj[0][k];
      for (int k = 0; k < nDOFPDE; k++)
        for (int d = 0; d < PhysD1::D; d++)
          aflda.DOF(k)[d] = adj[1][PhysD1::D*k+d];
      for (int k = 0; k < nDOFInt; k++)
        qIflda.DOF(k) = adj[2][k];
      for (int k = 0; k < nDOFBC; k++)
        lgflda.DOF(k) = adj[3][k];

      // ---------------------
      // Adjoint error estimate
      // ---------------------
      // prolongate P+1 primal solution
      // HDG primal solution field
      // cell solution
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre, BoundaryGroups);

      qfld.projectTo(qfldPp1);
      afld.projectTo(afldPp1);
      qIfld.projectTo(qIfldPp1);
      lgfld.projectTo(lgfldPp1);

      const int nDOFPDEPp1 = qfldPp1.nDOF();
      const int nDOFAuPp1  = afldPp1.nDOF();
      const int nDOFIntPp1 = qIfldPp1.nDOF();
      const int nDOFBCPp1  = lgfldPp1.nDOF();

      // residual
      SystemVectorClass rsdPp1 = {{nDOFPDEPp1},{nDOFAuPp1},{nDOFIntPp1},{nDOFBCPp1}};
      rsdPp1 = 0;

      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre, BoundaryGroups);


      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsdPp1(0), rsdPp1(1)),
                                              xfld, (qfldPp1, afldPp1), quadratureOrder, nCellGroups );
      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsdPp1(0), rsdPp1(1), rsdPp1(2)),
                                                              xfld, (qfldPp1, afldPp1), qIfldPp1, quadratureOrder, nInteriorTraceGroups);
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsdPp1(0), rsdPp1(1), rsdPp1(3)),
                                                             xfld, (qfldPp1, afldPp1), lgfldPp1, quadratureOrder, nBoundaryTraceGroups );

      // Map residual to field
     for (int k = 0; k < nDOFPDEPp1; k++)
       qfldrPp1.DOF(k) = rsdPp1[0][k];
     for (int k = 0; k < nDOFPDEPp1; k++)
       for (int d = 0; d < PhysD1::D; d++)
         afldrPp1.DOF(k)[d] = rsdPp1[1][PhysD1::D*k+d];
     for (int k = 0; k < nDOFIntPp1; k++)
       qIfldrPp1.DOF(k) = rsdPp1[2][k];
     for (int k = 0; k < nDOFBCPp1; k++)
       lgfldrPp1.DOF(k) = rsdPp1[3][k];

      // jacobian nonzero pattern
      //
      //        u   q  uI  lg
      //   PDE  X   X   X   X
      //   Au   X   X   X   X
      //   Int  X   X   X   0
      //   BC   X   X   0   0

      //typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

      //fld.SystemMatrixSize();
      DLA::MatrixD<NonZeroPatternClass> nzPp1 =
          {{ {nDOFPDEPp1, nDOFPDEPp1}, {nDOFPDEPp1, nDOFAuPp1}, {nDOFPDEPp1, nDOFIntPp1}, {nDOFPDEPp1, nDOFBCPp1} },
           { {nDOFAuPp1 , nDOFPDEPp1}, {nDOFAuPp1 , nDOFAuPp1}, {nDOFAuPp1 , nDOFIntPp1}, {nDOFAuPp1 , nDOFBCPp1} },
           { {nDOFIntPp1, nDOFPDEPp1}, {nDOFIntPp1, nDOFAuPp1}, {nDOFIntPp1, nDOFIntPp1}, {nDOFIntPp1, nDOFBCPp1} },
           { {nDOFBCPp1 , nDOFPDEPp1}, {nDOFBCPp1 , nDOFAuPp1}, {nDOFBCPp1 , nDOFIntPp1}, {nDOFBCPp1 , nDOFBCPp1} }};

      NonZeroPatternClass& nzPDEPp1_q  = nzPp1(0,0);
      NonZeroPatternClass& nzPDEPp1_a  = nzPp1(0,1);
      NonZeroPatternClass& nzPDEPp1_qI = nzPp1(0,2);
      NonZeroPatternClass& nzPDEPp1_lg = nzPp1(0,3);

      NonZeroPatternClass& nzAuPp1_q  = nzPp1(1,0);
      NonZeroPatternClass& nzAuPp1_a  = nzPp1(1,1);
      NonZeroPatternClass& nzAuPp1_qI = nzPp1(1,2);
      NonZeroPatternClass& nzAuPp1_lg = nzPp1(1,3);

      NonZeroPatternClass& nzIntPp1_q  = nzPp1(2,0);
      NonZeroPatternClass& nzIntPp1_a  = nzPp1(2,1);
      NonZeroPatternClass& nzIntPp1_qI = nzPp1(2,2);
      //NonZeroPatternClass& nzIntPp1_lg = nzPp1(2,3);

      NonZeroPatternClass& nzBCPp1_q  = nzPp1(3,0);
      NonZeroPatternClass& nzBCPp1_a  = nzPp1(3,1);
      //NonZeroPatternClass& nzBCPp1_qI = nzPp1(3,2);
      NonZeroPatternClass& nzBCPp1_lg = nzPp1(3,3);


      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nzPDEPp1_q, nzPDEPp1_a,
                                                                                      nzAuPp1_q, nzAuPp1_a),
                                              xfld, (qfldPp1, afldPp1), quadratureOrderMin, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, nzPDEPp1_q, nzPDEPp1_a, nzPDEPp1_qI,
                                                          nzAuPp1_q ,             nzAuPp1_qI ,
                                                          nzIntPp1_q, nzIntPp1_a, nzIntPp1_qI),
          xfld, (qfldPp1, afldPp1), qIfldPp1, quadratureOrderMin, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, nzPDEPp1_q, nzPDEPp1_a, nzPDEPp1_lg,
                                                               nzAuPp1_q , nzAuPp1_a , nzAuPp1_lg ,
                                                               nzBCPp1_q , nzBCPp1_a , nzBCPp1_lg ),
          xfld, (qfldPp1, afldPp1), lgfldPp1, quadratureOrderMin, nBoundaryTraceGroups);

      // HDG adjoint solution field
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre, BoundaryGroups);

      // jacobian transpose
      //Create a transposed non-zero pattern
      DLA::MatrixD<NonZeroPatternClass> nzTPp1(Transpose(nzPp1));

      SystemMatrixClass jacTPp1(nzTPp1);
      // ND version
      auto jacTPDEPp1_q  = Transpose(jacTPp1)(0,0);
      auto jacTPDEPp1_a  = Transpose(jacTPp1)(0,1);
      auto jacTPDEPp1_qI = Transpose(jacTPp1)(0,2);
      auto jacTPDEPp1_lg = Transpose(jacTPp1)(0,3);

      auto jacTAuPp1_q  = Transpose(jacTPp1)(1,0);
      auto jacTAuPp1_a  = Transpose(jacTPp1)(1,1);
      auto jacTAuPp1_qI = Transpose(jacTPp1)(1,2);
      auto jacTAuPp1_lg = Transpose(jacTPp1)(1,3);

      auto jacTIntPp1_q  = Transpose(jacTPp1)(2,0);
      auto jacTIntPp1_a  = Transpose(jacTPp1)(2,1);
      auto jacTIntPp1_qI = Transpose(jacTPp1)(2,2);
      //auto jacIntPp1_lg  = Transpose(jacTPp1)(2,3);

      auto jacTBCPp1_q  = Transpose(jacTPp1)(3,0);
      auto jacTBCPp1_a  = Transpose(jacTPp1)(3,1);
      //auto jacTBCPp1_qI = Transpose(jacTPp1)(3,2);
      auto jacTBCPp1_lg = Transpose(jacTPp1)(3,3);

      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacTPDEPp1_q, jacTPDEPp1_a,
                                                                                      jacTAuPp1_q, jacTAuPp1_a),
                                              xfld, (qfldaPp1, afldaPp1), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacTPDEPp1_q, jacTPDEPp1_a, jacTPDEPp1_qI,
                                                          jacTAuPp1_q ,               jacTAuPp1_qI ,
                                                          jacTIntPp1_q, jacTIntPp1_a, jacTIntPp1_qI),
          xfld, (qfldaPp1, afldaPp1), qIfldaPp1, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jacTPDEPp1_q, jacTPDEPp1_a, jacTPDEPp1_lg,
                                                               jacTAuPp1_q , jacTAuPp1_a , jacTAuPp1_lg ,
                                                               jacTBCPp1_q , jacTBCPp1_a , jacTBCPp1_lg ),
          xfld, (qfldaPp1, afldaPp1), lgfldaPp1, quadratureOrder, nBoundaryTraceGroups);

      // Functional integral
      SystemVectorClass rhsPp1 = {{nDOFPDEPp1},  {nDOFAuPp1}, {nDOFIntPp1}, {nDOFBCPp1}};
      SystemVectorClass adjPp1 = {{nDOFPDEPp1},  {nDOFAuPp1}, {nDOFIntPp1}, {nDOFBCPp1}};

      rhsPp1 = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell<SurrealClass>( outputFcn, rhsPp1(0) ), xfld, qfldaPp1, quadratureOrder, xfld.nCellGroups() );

      // compute the adjoint
      adjPp1 = solver.inverse(jacTPp1)*rhsPp1;

      // update solution
      for (int k = 0; k < nDOFPDEPp1; k++)
        qfldaPp1.DOF(k) = adjPp1[0][k];
      for (int k = 0; k < nDOFPDEPp1; k++)
        for (int d = 0; d < PhysD1::D; d++)
          afldaPp1.DOF(k)[d] = adjPp1[1][PhysD1::D*k+d];
      for (int k = 0; k < nDOFIntPp1; k++)
        qIfldaPp1.DOF(k) = adjPp1[2][k];
      for (int k = 0; k < nDOFBCPp1; k++)
        lgfldaPp1.DOF(k) = adjPp1[3][k];

      // ---------------------
      // Adjoint error exact
      // ---------------------

      // HDG primal solution field
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> eqfld(xfld, 0, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> eafld(xfld, 0, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> eqIfld(xfld, 0, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> elgfld(xfld, 0, BasisFunctionCategory_Legendre, BoundaryGroups);
      eqfld = 0;eafld = 0;eqIfld = 0;elgfld = 0;

      SystemVectorClass ersd = {{ eqfld.nDOF() },{ eafld.nDOF() },{ eqIfld.nDOF() },{ elgfld.nDOF()}};
      ersd = 0;

      AWResidualCell_HDG<TopoD1>::integrate( AWfcnCell, qfld, afld, quadratureOrder, nCellGroups, ersd(0), ersd(1) );
      AWResidualInteriorTrace_HDG<TopoD1>::integrate( AWfcnInt, qfld, afld, qIfld,
                                          quadratureOrder, nInteriorTraceGroups, ersd(0), ersd(1), ersd(2) );
      AWResidualBoundaryTrace_HDG<TopoD1>::integrate( AWfcnBound, qfld, afld,
                                          quadratureOrder, nBoundaryTraceGroups, ersd(0), ersd(1) );
      AWResidualBCTrace_HDG<TopoD1>::integrate( AWfcnBC, qfld, afld, lgfld,
                                          quadratureOrder, nBoundaryTraceGroups, ersd(0), ersd(1), ersd(3) );

      // storage field
      // populate the field
      for (int k = 0; k < eqfld.nDOF(); k++)
        eqfld.DOF(k) += ersd[0][k];
      for (int k = 0; k < eafld.nDOF(); k++)
        for (int d = 0; d < PhysD1::D; d++)
          eafld.DOF(k)[d] += ersd[1][PhysD1::D*k+d];
      for (int k = 0; k < eqIfld.nDOF(); k++)
        eqIfld.DOF(k) += ersd[2][k];
      for (int k = 0; k < elgfld.nDOF(); k++)
        elgfld.DOF(k) += ersd[3][k];

      // exact global and local error
      // Field Cell Types
      typedef typename XField<PhysD1, TopoD1            >::FieldCellGroupType<Line> XFieldCellGroupType;
      typedef typename Field< PhysD1, TopoD1, ArrayQ    >::FieldCellGroupType<Line> UFieldCellGroupType;
      typedef typename Field< PhysD1, TopoD1, VectorArrayQ>::FieldCellGroupType<Line> QFieldCellGroupType;

      typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;
      typedef typename UFieldCellGroupType::ElementType<> ElementUFieldClass;
      typedef typename QFieldCellGroupType::ElementType<> ElementQFieldClass;

      const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Line>(0);

      // solution - p
      UFieldCellGroupType&  qfldCell = qfld.getCellGroup<Line>(0);
      QFieldCellGroupType&  afldCell = afld.getCellGroup<Line>(0);

      // adjoint - p
      UFieldCellGroupType&  adjqfldCell = qflda.getCellGroup<Line>(0);
      QFieldCellGroupType&  adjafldCell = aflda.getCellGroup<Line>(0);

      // residual - p+1
      UFieldCellGroupType&  rsdqfldCellPp1 = qfldrPp1.getCellGroup<Line>(0);
      QFieldCellGroupType&  rsdafldCellPp1 = afldrPp1.getCellGroup<Line>(0);

      // adjoint - p+1
      UFieldCellGroupType&  adjqfldCellPp1 = qfldaPp1.getCellGroup<Line>(0);
      QFieldCellGroupType&  adjafldCellPp1 = afldaPp1.getCellGroup<Line>(0);

      // exact error
      UFieldCellGroupType&  eqfldCell = eqfld.getCellGroup<Line>(0);
      QFieldCellGroupType&  eafldCell = eafld.getCellGroup<Line>(0);

      // fields for element storage of local error estimates
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> localErrEstfld( xfld, 0, BasisFunctionCategory_Legendre );
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> localErrExtfld( xfld, 0, BasisFunctionCategory_Legendre );
      UFieldCellGroupType& localErrEstfldCell = localErrEstfld.getCellGroup<Line>(0);
      UFieldCellGroupType& localErrExtfldCell = localErrExtfld.getCellGroup<Line>(0);
      localErrEstfld = 0;
      localErrExtfld = 0;

      ElementUFieldClass localErrEstfldElem( localErrEstfldCell.basis() );
      ElementUFieldClass localErrExtfldElem( localErrExtfldCell.basis() );

      ElementXFieldClass  xfldElem( xfldCell.basis() );
      ElementUFieldClass  qfldElem( qfldCell.basis() );
      ElementQFieldClass  afldElem( afldCell.basis() );

      ElementUFieldClass  adjqfldElem( adjqfldCell.basis() );
      ElementQFieldClass  adjafldElem( adjafldCell.basis() );

      ElementUFieldClass  rsdqfldElemPp1( rsdqfldCellPp1.basis() );
      ElementQFieldClass  rsdafldElemPp1( rsdafldCellPp1.basis() );

      ElementUFieldClass  adjqfldElemPp1( adjqfldCellPp1.basis() );
      ElementQFieldClass  adjafldElemPp1( adjafldCellPp1.basis() );

      ElementUFieldClass  eqfldElem( eqfldCell.basis() );
      ElementQFieldClass  eafldElem( eafldCell.basis() );

      ArrayQ localSln=0;
      Real globalSln=0;
      Real globalErrEst=0;

      IntegrandClass fcnint( volumeFcn, {0} );

      for (int elem = 0; elem < xfldCell.nElem(); elem++)
      {
        xfldCell.getElement( xfldElem, elem );
        qfldCell.getElement( qfldElem, elem );
        afldCell.getElement( afldElem, elem );

        FunctorClass fcn = fcnint.integrand(xfldElem,qfldElem);

        localSln = 0;
        ElemIntegral(fcn, xfldElem, localSln);
        globalSln += localSln;

        // local error estimate
        rsdqfldCellPp1.getElement( rsdqfldElemPp1, elem );
        rsdafldCellPp1.getElement( rsdafldElemPp1, elem );

        adjqfldCellPp1.getElement( adjqfldElemPp1, elem );
        adjafldCellPp1.getElement( adjafldElemPp1, elem );

        Real localErrEst = 0;
        for ( int n = 0; n < adjqfldElemPp1.nDOF(); n++ )
        {
          localErrEst += dot( rsdqfldElemPp1.DOF(n), adjqfldElemPp1.DOF(n) );
          localErrEst += dot( rsdafldElemPp1.DOF(n), adjafldElemPp1.DOF(n) );
        }

        // error in local error estimate
        // extract exact
        eqfldCell.getElement( eqfldElem, elem );
        eafldCell.getElement( eafldElem, elem );

        Real localErrExt = 0;
        localErrExt += eqfldElem.DOF(0);
        for (int d =0; d<PhysD1::D; d++)
        {
          localErrExt += eafldElem.DOF(0)[d];
        }

        globalErrEst += localErrEst;

        localErrEstfldElem.DOF(0) = localErrEst;
        localErrEstfldCell.setElement( localErrEstfldElem, elem );

        localErrExtfldElem.DOF(0) = localErrExt;
        localErrExtfldCell.setElement( localErrExtfldElem, elem );
      }

      // Check if the Global Error Estimate from the Volume terms is equal to the sum of the Local Estimates of the Volume terms
      Real globalErrEst1 = 0;
      for (int k = 0; k < nDOFPDEPp1; k++) // loop over u
      {
        globalErrEst1 += dot( adjPp1[0][k], rsdPp1[0][k] ); // dot the u rsd with adjoint weighting
      }
      for (int k =0; k < nDOFAuPp1; k++) // loop over q
      {
        globalErrEst1 += dot( adjPp1[1][k], rsdPp1[1][k] ); // dot the q rsd with adjoint weighting
      }
      const Real small_tol = 5e-7;
      const Real close_tol = 5e-7;
      SANS_CHECK_CLOSE( globalErrEst1, globalErrEst, small_tol, close_tol );

      typedef typename Field<PhysD1, TopoD1, ArrayQ>::FieldTraceGroupType<Node> UFieldTraceGroupType;
      typedef typename XField<PhysD1, TopoD1>::FieldTraceGroupType<Node> XFieldTraceGroupType;
      typedef typename UFieldTraceGroupType::ElementType<> ElementUFieldTraceClass;
      typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;

      // loop over interior face groups
      for ( int group = 0; group <nInteriorTraceGroups; group++)
      {
        const XFieldTraceGroupType xfldInteriorTrace = xfld.getInteriorTraceGroup<Node>(group);

        UFieldTraceGroupType adjqIfldInteriorTrace = qIflda.getInteriorTraceGroup<Node>(group);

        UFieldTraceGroupType rsdqIfldInteriorTracePp1 = qIfldrPp1.getInteriorTraceGroup<Node>(group);
        UFieldTraceGroupType adjqIfldInteriorTracePp1 = qIfldaPp1.getInteriorTraceGroup<Node>(group);

        UFieldTraceGroupType eqIfldInteriorTrace = eqIfld.getInteriorTraceGroup<Node>(group);

        ElementXFieldTraceClass xfldElem( xfldInteriorTrace.basis() );

        // adjoint in p
        ElementUFieldTraceClass adjqIfldElem( adjqIfldInteriorTrace.basis() );

        // rsd projected from p to p+1, adjoint solved in p+1
        ElementUFieldTraceClass rsdqIfldElemPp1( rsdqIfldInteriorTracePp1.basis() );
        ElementUFieldTraceClass adjqIfldElemPp1( adjqIfldInteriorTracePp1.basis() );

        // ext field
        ElementUFieldTraceClass eqIfldElem( eqIfldInteriorTrace.basis() );

        for (int elem = 0; elem< xfldInteriorTrace.nElem(); elem++)
        {
          xfldInteriorTrace.getElement( xfldElem, elem );

          rsdqIfldInteriorTracePp1.getElement( rsdqIfldElemPp1, elem );
          adjqIfldInteriorTracePp1.getElement( adjqIfldElemPp1, elem );

          Real localErrEst = 0;
          for (int n=0; n< adjqIfldElemPp1.nDOF(); n++)
          {
            localErrEst += dot( rsdqIfldElemPp1.DOF(n), adjqIfldElemPp1.DOF(n) );
          }
          globalErrEst += localErrEst;

          // extract exact
          eqIfldInteriorTrace.getElement( eqIfldElem, elem );

          Real localErrExt = 0;
          localErrExt += eqIfldElem.DOF(0);

          // Distribute the local error estimates to adjacent elements
          int elemL = xfldInteriorTrace.getElementLeft( elem );
          int elemR = xfldInteriorTrace.getElementRight( elem );

          localErrEstfldCell.getElement( localErrEstfldElem, elemL );
          localErrEstfldElem.DOF(0) += 0.5* localErrEst;
          localErrEstfldCell.setElement( localErrEstfldElem, elemL );

          localErrEstfldCell.getElement( localErrEstfldElem, elemR );
          localErrEstfldElem.DOF(0) += 0.5* localErrEst;
          localErrEstfldCell.setElement( localErrEstfldElem, elemR );

          localErrExtfldCell.getElement( localErrExtfldElem, elemL );
          localErrExtfldElem.DOF(0) += 0.5* localErrExt;
          localErrExtfldCell.setElement( localErrExtfldElem, elemL );

          localErrExtfldCell.getElement( localErrExtfldElem, elemR );
          localErrExtfldElem.DOF(0) += 0.5* localErrExt;
          localErrExtfldCell.setElement( localErrExtfldElem, elemR );
        } // loop over elements of trace group
      } // loop over interior trace groups

      // Check if the Global Error Estimate from the Volume and interior terms
      // is equal to the sum of the Local Estimates thus far
      Real globalErrEst2 = 0;
      for (int k = 0; k < nDOFPDEPp1; k++) // loop over u
      {
        globalErrEst2 += dot( adjPp1[0][k], rsdPp1[0][k] ); // dot the u rsd with adjoint weighting
      }
      for (int k =0; k < nDOFAuPp1; k++) // loop over q
      {
        globalErrEst2 += dot( adjPp1[1][k], rsdPp1[1][k] ); // dot the q rsd with adjoint weighting
      }
      for (int k =0; k< nDOFInt; k++)
      {
        globalErrEst2 += dot( adjPp1[2][k], rsdPp1[2][k] ); // dot the u^ rsd with adjoint weighting
      }

      SANS_CHECK_CLOSE( globalErrEst2, globalErrEst, small_tol, close_tol );

      //boundary contributions
      for (int group = 0; group < nBoundaryTraceGroups; group++)
      {
        const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<Node>(group);

        UFieldTraceGroupType adjlgfldBoundaryTrace = lgflda.getBoundaryTraceGroup<Node>(group);

        UFieldTraceGroupType rsdlgfldBoundaryTracePp1 = lgfldrPp1.getBoundaryTraceGroup<Node>(group);
        UFieldTraceGroupType adjlgfldBoundaryTracePp1 = lgfldaPp1.getBoundaryTraceGroup<Node>(group);

        UFieldTraceGroupType elgfldBoundaryTrace = elgfld.getBoundaryTraceGroup<Node>(group);

        ElementXFieldTraceClass xfldElem( xfldBoundaryTrace.basis() );

        ElementUFieldTraceClass adjlgfldElem( adjlgfldBoundaryTrace.basis() );

        ElementUFieldTraceClass rsdlgfldElemPp1( rsdlgfldBoundaryTracePp1.basis() );
        ElementUFieldTraceClass adjlgfldElemPp1( adjlgfldBoundaryTracePp1.basis() );

        ElementUFieldTraceClass elgfldElem( elgfldBoundaryTrace.basis() );

        for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
        {
          xfldBoundaryTrace.getElement( xfldElem, elem );

          rsdlgfldBoundaryTracePp1.getElement( rsdlgfldElemPp1, elem );
          adjlgfldBoundaryTracePp1.getElement( adjlgfldElemPp1, elem );

          Real localErrEst = 0;
          for (int n=0; n< adjlgfldElemPp1.nDOF(); n++)
          {
            localErrEst += dot( rsdlgfldElemPp1.DOF(n), adjlgfldElemPp1.DOF(n) );
          }
          globalErrEst += localErrEst;

          // extract exact
          elgfldBoundaryTrace.getElement( elgfldElem, elem );

          Real localErrExt = 0;
          localErrExt += elgfldElem.DOF(0);

          // Distribute the local error estimates to adjacent elements
          int elemL = xfldBoundaryTrace.getElementLeft( elem );

          localErrEstfldCell.getElement( localErrEstfldElem, elemL );
          localErrEstfldElem.DOF(0) += localErrEst;
          localErrEstfldCell.setElement( localErrEstfldElem, elemL );

          localErrExtfldCell.getElement( localErrExtfldElem, elemL );
          localErrExtfldElem.DOF(0) += localErrExt;
          localErrExtfldCell.setElement( localErrExtfldElem, elemL );
        } // loop over elements of trace group
      } // loop over boundary trace groups

      Real avgLocalErrExt=0,avgLocalErrEst=0;
      Real avglocalErrEstError=0, avglocalErrEstEff=0;

#if 1 // actual average local estimate and effectivity

      // Kahan Summation working variables
      Real smaExt=0,smaEst=0,smaErrEst=0;
      Real bigExt=0,bigEst=0,bigErrEst=0;
      Real midExt=0,midEst=0,midErrEst=0;
      for (int elem = 0; elem < xfldCell.nElem(); elem++)
      {
        // Kahan Summation of the Estimate
        localErrEstfldCell.getElement( localErrEstfldElem, elem );
        bigEst = fabs(localErrEstfldElem.DOF(0)) - smaEst;
        midEst = avgLocalErrEst + bigEst;
        smaEst = (midEst - avgLocalErrEst) - bigEst;
        avgLocalErrEst = midEst;

        // Kahan Summation of the Exact
        localErrExtfldCell.getElement( localErrExtfldElem, elem );
        bigExt = fabs(localErrExtfldElem.DOF(0)) - smaExt;
        midExt = avgLocalErrExt + bigExt;
        smaExt = (midExt - avgLocalErrExt) - bigExt;
        avgLocalErrExt = midExt;

        // Kahan Summation of the Error in the Estimate
        bigErrEst = fabs(localErrExtfldElem.DOF(0)- localErrEstfldElem.DOF(0)) - smaErrEst;
        midErrEst = avglocalErrEstError + bigErrEst;
        smaErrEst = (midErrEst - avglocalErrEstError) - bigErrEst;
        avglocalErrEstError = midErrEst;
      }
      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExt;
      avgLocalErrExt      /= xfldCell.nElem();
      avgLocalErrEst      /= xfldCell.nElem();
      avglocalErrEstError /= xfldCell.nElem();

#else// specific element - not really average at all

      int elem = 0;

      localErrEstfldCell.getElement( localErrEstfldElem, elem );
      Real localErrEst = localErrEstfldElem.DOF(0);

      localErrExtfldCell.getElement( localErrExtfldElem, elem );
      Real localErrExt = localErrExtfldElem.DOF(0);

      avgLocalErrExact    = fabs(localErrExt);
      avgLocalErrEst      = fabs(localErrEst);
      avglocalErrEstError = fabs(localErrExt - localErrEst);

      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExact;
#endif

      Real globalError = globalSln - globalExt;
#if 0
      Real sumLocExt=0;
      for (int elem = 0; elem < xfldCell.nElem(); elem++)
      {
        localErrExtfldCell.getElement( localErrExtfldElem, elem );
        sumLocExt += localErrExtfldElem.DOF(0);
      }
      SANS_CHECK_CLOSE( globalError, sumLocExt, small_tol, close_tol );
#endif
      // Checks that the global Estimate evaluated all at once is the same as the sum of the locals
      Real globalErrEst3 = dot( adjPp1, rsdPp1 );
      SANS_CHECK_CLOSE( globalErrEst3, globalErrEst, small_tol, close_tol );


      hVec[indx] = 1./ii;

      globalErrorVec[indx] = abs(globalError);
      globalEstVec[indx] = abs(globalErrEst);
      globalEstErrVec[indx] = abs( globalError - globalErrEst );
      globalEffVec[indx] = abs( 1 - globalErrEst/globalError );

      localErrorVec[indx] = abs(avgLocalErrExt);
      localEstVec[indx] = abs(avgLocalErrEst);
      localEstErrVec[indx] = avglocalErrEstError;
      localEffVec[indx] = avglocalErrEstEff;

      if (indx > 0)
      {
        globalErrorRate[indx]  = (log(globalErrorVec[indx])  - log(globalErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstRate[indx]    = (log(globalEstVec[indx])    - log(globalEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstErrRate[indx] = (log(globalEstErrVec[indx]) - log(globalEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        globalEffRate[indx]    = (log(globalEffVec[indx])    - log(globalEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));

        localErrorRate[indx]  = (log(localErrorVec[indx])  - log(localErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstRate[indx]    = (log(localEstVec[indx])    - log(localEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstErrRate[indx] = (log(localEstErrVec[indx]) - log(localEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        localEffRate[indx]    = (log(localEffVec[indx])    - log(localEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
      }
      else
      {
        globalErrorRate[indx]  = 0;
        globalEstRate[indx]    = 0;
        globalEstErrRate[indx] = 0;
        globalEffRate[indx]    = 0;

        localErrorRate[indx]  = 0;
        localEstRate[indx]    = 0;
        localEstErrRate[indx] = 0;
        localEffRate[indx]    = 0;
      }

      if ( power == powermin )
      {
        cout << "P = " << order << endl;
        cout << "  \t";
        cout << " global               global               global               global              ";
        cout << " local                local                local                local" << endl;
        cout << "ii\t";
        cout << " exact                estimate             err in estimate      effectivity         ";
        cout << " exact                estimate             err in estimate      effectivity" << endl;

        resultFile << "ZONE T=\"HDG(" <<lRef<<") P=" << order << "\"" << endl;
        pyriteFile << order << endl;
      }

      resultFile << std::setprecision(16) << std::scientific;
      resultFile << hVec[indx];
      resultFile << ", " << globalErrorVec[indx];
      resultFile << ", " << globalEstVec[indx];
      resultFile << ", " << globalEstErrVec[indx];
      resultFile << ", " << globalEffVec[indx];
      resultFile << ", " << globalErrorRate[indx];
      resultFile << ", " << globalEstRate[indx];
      resultFile << ", " << globalEstErrRate[indx];
      resultFile << ", " << globalEffRate[indx];

      resultFile << ", " << localErrorVec[indx];
      resultFile << ", " << localEstVec[indx];
      resultFile << ", " << localEstErrVec[indx];
      resultFile << ", " << localEffVec[indx];
      resultFile << ", " << localErrorRate[indx];
      resultFile << ", " << localEstRate[indx];
      resultFile << ", " << localEstErrRate[indx];
      resultFile << ", " << localEffRate[indx];
      resultFile << endl;

      // For Latex
      datFile << std::setprecision(16) << std::scientific;
      if (indx==0)
      {
        datFile << order;
      }
      else
      {
        datFile << " ";
      }
      datFile << "," << hVec[indx];
      datFile << "," << globalErrorVec[indx];
      datFile << "," << globalEstVec[indx];
      datFile << "," << globalEstErrVec[indx];
      datFile << "," << globalEffVec[indx];
      datFile << "," << globalErrorRate[indx];
      datFile << "," << globalEstRate[indx];
      datFile << "," << globalEstErrRate[indx];
      datFile << "," << globalEffRate[indx];
      datFile << "," << localErrorVec[indx];
      datFile << "," << localEstVec[indx];
      datFile << "," << localEstErrVec[indx];
      datFile << "," << localEffVec[indx];
      datFile << "," << localErrorRate[indx];
      datFile << "," << localEstRate[indx];
      datFile << "," << localEstErrRate[indx];
      datFile << "," << localEffRate[indx];
      datFile << endl;

      pyriteFile << hVec[indx]; // grid
      pyriteFile << globalErrorVec[indx] << globalEstVec[indx]<< globalEstErrVec[indx]; // values
      pyriteFile << globalErrorRate[indx]<< globalEstRate[indx]<< globalEstErrRate[indx]; // rates
      pyriteFile << globalEffVec[indx]<< globalEffRate[indx]; //effectivities
      pyriteFile << endl;

      cout << ii << "\t";
      cout << std::scientific;
      cout << std::scientific << std::setprecision(5) << globalErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEffRate[indx] << ")  ";

      cout << std::scientific << std::setprecision(5) << localErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEffRate[indx] << ")  ";
      cout << endl;

      indx++;

#if 0
      // Tecplot dump
      string filename = "tmp/adjDG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, adjfldAreaPp1, filename );

      filename = "tmp/qDG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, afldArea, filename );
#endif
#if 0
      string filename = "tmp/rsdPp1DG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, rsdfldAreaPp1, filename );
#endif
#if 0
      string filename = "tmp/locErrHDG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, localErrEstfldArea, filename );
#endif
//#endif // TEMP DEBUGGING
    }

    cout << endl;

  }
  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;
}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
