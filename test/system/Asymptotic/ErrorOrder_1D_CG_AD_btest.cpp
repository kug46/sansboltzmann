// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_CG_AD_btest
// testing of 1-D CG with Advection-Diffusion

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/output_Tecplot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_1D_CG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorOrder_1D_CG_AD )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_None > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ScalarFunction1D_Sine SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Sine> NDSolutionExact;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> AlgebraicEquationSet_GalerkinClass;
  typedef AlgebraicEquationSet_GalerkinClass::BCParams BCParams;

  // PDE

  AdvectiveFlux1D_Uniform adv( 0 );

  Real nu = 1;
  ViscousFlux1D_Uniform visc( nu );

  Source1D_None source;

  NDSolutionExact solnExact;

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde(adv, visc, source, forcingptr );

  // BC

  // Create a BC dictionary
  PyDict Sine;
  Sine[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Sine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = Sine;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};


  typedef AlgebraicEquationSet_GalerkinClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_GalerkinClass::SystemVector SystemVectorClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  // integrands
  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_1D_CG.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_CG_AD_FullTest.txt", 1e-9, 1e-4, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_CG_AD_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 7;
#else
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
    if (order == 7) powermax = 4;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );

      // grid:

      XField1D xfld( ii );

      // solution: Hierarchical, C0
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
      qfld = 0;

      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 1
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#elif 0
      Field2D_CG_BoundaryEdge_Independent<NDPDEClass> lgfld( xfld, order,
                                                             BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
      Field2D_DG_BoundaryEdge<NDPDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre,
                                                 BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#endif

      lgfld = 0;

      const int nDOFBC = lgfld.nDOF();

      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-11, 1e-11};

      StabilizationNitsche stab(order);
      AlgebraicEquationSet_GalerkinClass AlgEqSet(xfld, qfld, lgfld, pde, stab, quadratureOrder, ResidualNorm_L2,
                                                  tol, {0}, PyBCList, BCBoundaryGroups );

      // residual vectors

      SystemVectorClass q(AlgEqSet.vectorStateSize());
      SystemVectorClass rsd(AlgEqSet.vectorEqSize());

      AlgEqSet.fillSystemVector(q);

      rsd = 0;

      AlgEqSet.residual(q, rsd);

      // solve

      SLA::UMFPACK<SystemMatrixClass> solver(AlgEqSet);

      SystemVectorClass dq(q.size());

      solver.solve(rsd, dq);

      // updated solution

      q -= dq;

      AlgEqSet.setSolutionField(q);

      // check that the residual is zero

      rsd = 0;
      AlgEqSet.residual(q, rsd);

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

      // L2 solution error

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorIntegrand, SquareError ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
      {
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2];
        Real slope = (log(normVec[indx-1])  - log(normVec[indx-2])) /(log(hVec[indx-1]) - log(hVec[indx-2]));
        cout << "  slope = " << slope << ")";
      }
      cout << endl;
#endif

#if 0
      // Tecplot dump of solution
      string filename = "tmp/slnCG_P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( AlgEqSet.qfld(), filename );
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
#endif

    }

    // Tecplot output
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n];
      pyriteFile.close(4) << slope << std::endl;
    }

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
