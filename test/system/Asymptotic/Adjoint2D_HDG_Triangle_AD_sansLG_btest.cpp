// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adjoint2D_HDG_Triangle_AD_btest
// testing of 2-D HDG adjoint with Advection-Diffusion on triangles

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "pde/OutputCell_Solution.h"
#include "pde/OutputCell_WeightedSolution.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/IntegrandCell_HDG_Output.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/JacobianFunctionalCell_HDG.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_HDG_Triangle_AD_sansLG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adjoint2D_HDG_Triangle_AD_sansLG )
{
  typedef SurrealS<1> SurrealClass;

  // Laplacian w/ vortex
  typedef ScalarFunction2D_Vortex SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_sansLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE
  Real u = 1.0;
  Real v = 0.5;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real nu = 1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0.1, 0.0, 0.0);

  Real xvort = -1./sqrt(3);
  Real yvort = 1./sqrt(7);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.x0] = xvort;
  solnArgs[NDSolutionExact::ParamsType::params.y0] = yvort;

  NDSolutionExact solnExact( solnArgs );

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict Vortex;
  Vortex[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Vortex;
  Vortex[SolutionExact::ParamsType::params.x0] = xvort;
  Vortex[SolutionExact::ParamsType::params.y0] = yvort;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = Vortex;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.A] = 1.0;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0.0;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // Adjoint PDE
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> AdjointExact;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdjoint2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdjoint2D> PDEAdjointClass;

  typedef ScalarFunction2D_ForcingFunction<PDEAdjointClass> WeightFcn; // Change this to change output weighting type
  typedef OutputCell_WeightedSolution<PDEClass, WeightFcn> OutputClass;
//  typedef OutputCell_Solution<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_HDG_Output<NDOutputClass> OutputIntegrandClass;

  typedef OutputCell_SolutionErrorSquared<PDEAdjointClass, AdjointExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_HDG_Output<NDErrorClass> ErrorIntegrandClass;

  // Adjoint PDE
  AdvectiveFlux2D_Uniform adv_adj( -u, -v );

  AdjointExact adjExact;

  typedef ForcingFunction2D_MMS<PDEAdjoint2D> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact) );

  PDEAdjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  WeightFcn volumeFcn( pdeadj ); // This is 'g' in when integrating the solution as the output


  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global );

  // integrands
  NDOutputClass fcnOutput(volumeFcn);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  NDErrorClass fcnError(adjExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/adjL2_2D_HDG.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/ErrorOrder/Adjoint2D_HDG_Triangle_AD_sansLG_FullTest.txt", 1e-10, 1e-6, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/Adjoint2D_HDG_Triangle_AD_sansLG_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 5;
#else
  int ordermax = 1;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 2;
#ifdef SANS_FULLTEST
    int powermax = 5;
    if ( order == 5 ) powermax = 4;
#else
    int powermax = 3;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );
      jj = ii;

      // grid:
//      XField2D_Box_Triangle_X1 xfld( ii, jj );
      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

      // HDG solution field
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
      qfld = 0;

      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, order, BasisFunctionCategory_Legendre);
      afld = 0;

      // interface solution
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, order, BasisFunctionCategory_Legendre);
      qIfld = 0;

      // Lagrange multiplier
      std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, LG_BGroup_list);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld(xfld, order, BasisFunctionCategory_Legendre, LG_BGroup_list);
      lgfld = 0;

      const int nDOFPDE = qfld.nDOF();


      // quadrature rule
      QuadratureOrder quadratureOrder( xfld, -1 );

      std::vector<Real> tol = {1e-11, 1e-11, 1e-11};

      const std::vector<int> InteriorTraceGroups = {0,1,2}; //UnionJack has 3 interior trace groups

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                         {0}, InteriorTraceGroups, PyBCList, BCBoundaryGroups );

      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      PrimalEqSet.fillSystemVector(q);

      // residual
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0.0;
      PrimalEqSet.residual(q, rsd);

      // primal solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(PrimalEqSet.vectorStateSize());

      solver.solve(rsd, dq);

      // update primal solution
      q -= dq;
      PrimalEqSet.setSolutionField(q);

      // check that the residual is zero
      rsd = 0;
      PrimalEqSet.residual(q, rsd);
      std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

      bool converged = PrimalEqSet.convergedResidual(rsdNorm);
      BOOST_CHECK( converged );
      if (!converged) PrimalEqSet.printDecreaseResidualFailure( PrimalEqSet.residualNorm(rsd) );

      // adjoint (right hand side)
      SystemVectorClass rhs(q.size());
      rhs = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_HDG<SurrealClass>( outputIntegrand, rhs(PrimalEqSet.iPDE) ),
          xfld, (qfld, afld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // solve
//      SLA::UMFPACK<SystemMatrixClass> solver;

      SystemVectorClass adj(q.size());
      SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);
      solverAdj.solve(rhs, adj);

      bfld = 0;
      PrimalEqSet.setAdjointField(adj, wfld, bfld, wIfld, mufld);


      // L2 adjoint error
      Real SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( errorIntegrand, SquareError ),
          xfld, (wfld, bfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 adjoint error = " << sqrt( norm );
      if (indx > 1)
      {
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << "  (rate = " << (log(normVec[indx-1]) - log(normVec[indx-2])) /(log(hVec[indx-1]) - log(hVec[indx-2])) << ")";
      }
      cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/solHDG_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );

      filename = "tmp/adjHDG_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( wfld, filename );
#endif

    }

    // Tecplot output
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
