// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_2D_EDG_Triangle_AD_btest
// testing of 2-D EDG with Advection-Diffusion on triangles

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_2D_EDG_Triangle_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_EDG_Triangle_AD )
{

  // Laplacian w/ vortex
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Vortex> SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact>  NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdvectionDiffusion2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;

  typedef BCTypeFunction_mitStateParam BCType;
//  typedef BCTypeFunctionLinearRobin_sansLG BCType;
  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> AlgebraicEquationSet_HDGClass;
  typedef AlgebraicEquationSet_HDGClass::BCParams BCParams;

  // PDE
  AdvectiveFlux2D_Uniform adv( 0, 0 );

  Real nu = 1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0,0,0);

  Real xvort = -1./sqrt(3);
  Real yvort = 1./sqrt(7);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[SolutionExact::ParamsType::params.x0] = xvort;
  solnArgs[SolutionExact::ParamsType::params.y0] = yvort;

  SolutionExact solnExact( solnArgs );
  NDSolutionExact NDsolnExact(solnArgs); // needed for exact output calculations

  NDPDEClass pde( adv, visc, source );

  // BC
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(xvort,yvort));

  PyDict Vortex;
  Vortex[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Vortex;
  Vortex[SolutionExact::ParamsType::params.x0] = xvort;
  Vortex[SolutionExact::ParamsType::params.y0] = yvort;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = Vortex;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
         BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0,1,2,3};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

  typedef AlgebraicEquationSet_HDGClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_HDGClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  typedef OutputCell_SolutionErrorSquared<NDPDEClass, SolutionExact> OutputSolutionErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD2, OutputSolutionErrorSquaredClass> NDOutputSolutionErrorSquaredClass;
  typedef IntegrandCell_HDG_Output<NDOutputSolutionErrorSquaredClass> IntegrandSquareErrorClass;

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  const std::vector<int> BoundaryGroups = {0,1,2,3};
  const std::vector<int> InteriorTraceGroups = {0,1,2}; //UnionJack has 3 interior trace groups

  // output integrands
  NDOutputSolutionErrorSquaredClass outErr( NDsolnExact );
  IntegrandSquareErrorClass fcnErr( outErr, {0} );

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  //std::ofstream resultFile("tmp/L2_2D_HDG.plt", std::ios::out);
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_EDG_Triangle_AD_FullTest.txt", 1e-10, 5e-4, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_EDG_Triangle_AD_MinTest.txt", 1e-10, 5e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 5;
#else
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
    if ( order == 5 ) powermax = 4;
#else
    int powermax = 2;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );
      jj = ii;

      // grid:
      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

      // HDG solution field
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
      // interface solution
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Hierarchical);
      // Lagrange multiplier
      std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, LG_BGroup_list);

      qfld = 0;
      afld = 0;
      qIfld = 0;
      lgfld = 0;

      const int nDOFPDE = qfld.nDOF();

      // quadrature rule
      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-11, 1e-11, 1e-11};

      AlgebraicEquationSet_HDGClass AlgEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                             {0}, InteriorTraceGroups, PyBCList, BCBoundaryGroups );

      // residual vectors

      SystemVectorClass q(AlgEqSet.vectorStateSize());
      SystemVectorClass rsd(AlgEqSet.vectorEqSize());

      AlgEqSet.fillSystemVector(q);

      rsd = 0;
      AlgEqSet.residual(q, rsd);

#if 0
      fstream fout( "tmp/jac_HDG_new.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(AlgEqSet);

      SystemVectorClass dq(q.size());

      solver.solve(rsd, dq);

      // updated solution
      q -= dq;

      AlgEqSet.setSolutionField(q);

      // check that the residual is zero
      rsd = 0;
      AlgEqSet.residual(q, rsd);
      std::vector<std::vector<Real>> rsdNorm = AlgEqSet.residualNorm(rsd);

      BOOST_REQUIRE( AlgEqSet.convergedResidual(rsdNorm) );

      // L2 solution error

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( fcnErr, SquareError ), xfld, (qfld, afld),
          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real errorNorm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( errorNorm );
      indx++;

#ifdef SANS_VERBOSE
      std::cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( errorNorm );
      if (indx > 1)
      {
        std::cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        std::cout << "  (rate = " << (log(normVec[indx-1]) - log(normVec[indx-2])) /(log(hVec[indx-1]) - log(hVec[indx-2])) << ")";
      }
      std::cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/slnEDG_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += ".plt";
      output_Tecplot( qfld, filename );
#endif

    }

    // Tecplot output
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
