// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateOrder_2D_CG_Triangle_AD_btest
// testing error estimation of 2-D CG with Advection-Diffusion on triangles

// #define SANS_FULLTEST // Line 290 for Pyrites

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "tools/KahanSum.h"
#include "pyrite_fstream.h"

#include "Surreal/SurrealS.h"

//PDE
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

//Solve includes
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/Function/WeightedFunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

// Solver includes
#include "Adaptation/MOESS/SolverInterface_Galerkin.h"

// Estimate includes
#include "ErrorEstimate/Galerkin/ErrorEstimateNodal_Galerkin.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateOrder_2D_CG_Nodal_Triangle_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateOrder_2D_CG_Nodal_Triangle_AD )
{
  timer totaltime;
#if 1  // Laplacian w/ Exponential functions
  // Case used in Hugh's Masters Thesis - ALSO PYRITE SETTINGS
  // Case 1 - primal [a=1;b=1;c=1;d=1;]       - adjoint [a=1;b=-2;c=-2;d=1;]
  typedef ScalarFunction2D_VarExp3 SolutionExact;
  typedef ScalarFunction2D_SineSine AdjointExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD2, AdjointExact>  NDAdjointExact;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef BCParameters<BCVector> BCParams;

  // For doing the MMS with the adjoint
  typedef ScalarFunction2D_ForcingFunction<NDPDEClass> WeightFunctional;
  typedef SolnNDConvertSpace<PhysD2, WeightFunctional> NDWeightFunctional;

  typedef OutputCell_WeightedSolution<PDEClass, WeightFunctional> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  typedef ParamType_None ParamBuilderType;

  typedef SolutionData_Galerkin_Stabilized<PhysD2, TopoD2, NDPDEClass, ParamBuilderType> SolutionClass;
  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, ParamFieldType> PrimalEquationSetClass;

  typedef SolverInterface_Galerkin<SolutionClass, PrimalEquationSetClass, OutputIntegrandClass> SolverInterfaceClass;

  // Estimation done separately to the Solver Interface Class
#define NODAL
#ifndef NODAL
  // typedef ErrorEstimate_Galerkin_Stabilized<NDPDEClass, BCNDConvertSpace, BCVector, XField<PhysD2,TopoD2>> ErrorEstimateClass;
  typedef ErrorEstimate_StrongForm_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector, XField<PhysD2,TopoD2>> ErrorEstimateClass;
#else
  typedef ErrorEstimateNodal_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector, XField<PhysD2,TopoD2>> ErrorEstimateClass;
#endif

  // PDE
  Real u=0.;
  Real v=0.;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real nu = 1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0,0,0);

  // BC
  // Create a solution dictionary
  Real a = 3;
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
    BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.VarExp3;
  solnArgs[SolutionExact::ParamsType::params.a] = a;

  NDSolutionExact solnExact( solnArgs );

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict BC_Dirichlet_Soln;

  // BC_Dirichlet_Soln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  // BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.Function] = solnArgs;
  // BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.A] = 1;
  // BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_sansLG>::params.B] = 0;

  // BC_Dirichlet_Soln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  // BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.Function] = solnArgs;
  // BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.A] = 1;
  // BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.B] = 0;

  BC_Dirichlet_Soln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
                    BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCDirichlet_Soln"] = BC_Dirichlet_Soln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDirichlet_Soln"] = {0,1,2,3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);
  std::vector<int> active_boundaries = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Adjoint PDE -- doing MMS for the adjoint to get a different volume functional
  AdvectiveFlux2D_Uniform adv_adj( -u, -v );

  a = 1;
  NDAdjointExact adjExact(a,a);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact));
  NDPDEClass pdeadj( adv_adj, visc, source, forcingadjptr );

  NDWeightFunctional volumeFcn( pdeadj );

  // volumeFcn is g in (g,u) g = - Del psi
  NDOutputClass weightOutput( volumeFcn );
  OutputIntegrandClass outputIntegrand( weightOutput, {0} );

  cout << "case: Laplacian w/ Exponentials" << endl;

#endif

  // discretization pieces
  StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, 1);
  enum ResidualNormType ResNormType = ResidualNorm_L2;
  std::vector<Real> tol = {1e-11, 1e-11};

  // Solver dictionaries
  PyDict SolverContinuationDict, NonlinearSolverDict, NewtonSolverDict, LinearSolverDict, LineUpdateDict, UMFPACKDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver] = UMFPACKDict;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  NonlinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

  SolverContinuationDict[SolverContinuationParams<TemporalMarch>::params.Continuation] = NonlinearSolverDict;

  // Check inputs
  SolverContinuationParams<TemporalMarch>::checkInputs(SolverContinuationDict);


  // norm data
  const int vsize = 10;
  Real hVec[vsize];
  Real globalErrorVec[vsize], globalEstVec[vsize], globalEstErrVec[vsize], globalEffVec[vsize];
  Real localErrorVec[vsize], localEstVec[vsize], localEstErrVec[vsize], localEffVec[vsize];
  Real globalErrorRate[vsize], globalEstRate[vsize], globalEstErrRate[vsize], globalEffRate[vsize];
  Real localErrorRate[vsize], localEstRate[vsize], localEstErrRate[vsize], localEffRate[vsize];
  int indx;

#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/ErrorEst2D_CG.plt", std::ios::out);
  std::ofstream datFile("tmp/ErrorEst2D_CG.dat", std::ios::out);
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_2D_CG_Nodal_AD_FullTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-9, 9e-2, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  std::stringstream datFile;
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_2D_CG_Nodal_AD_MinTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-9, 5e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"Global Error\"";
  resultFile << ", \"Global Error Estimate\"";
  resultFile << ", \"Global Error Estimate Error\"";
  resultFile << ", \"Global 1-Effectivity\"";
  resultFile << ", \"Global Error Rate\"";
  resultFile << ", \"Global Error Estimate Rate\"";
  resultFile << ", \"Global Error Estimate Error Rate\"";
  resultFile << ", \"Global 1-Effectivity Rate\"";
  resultFile << ", \"Local Error\"";
  resultFile << ", \"Local Error Estimate\"";
  resultFile << ", \"Local Error Estimate Error\"";
  resultFile << ", \"Local 1-Effectivity\"";
  resultFile << ", \"Local Error Rate\"";
  resultFile << ", \"Local Error Estimate Rate\"";
  resultFile << ", \"Local Error Estimate Error Rate\"";
  resultFile << ", \"Local 1-Effectivity Rate\"";
  resultFile << endl;

  // For Latex Output
  datFile << "order,"
      "h,"
      "globalError,"
      "globalErrorEstimate,"
      "globalErrorEstimateError,"
      "globalEffectivity,"
      "globalErrorRate,"
      "globalErrorEstimateRate,"
      "globalErrorEstimateErrorRate,"
      "globalEffectivityRate,"
      "localError,"
      "localErrorEstimate,"
      "localErrorEstimateError,"
      "localEffectivity,"
      "localErrorRate,"
      "localErrorEstimateRate,"
      "localErrorEstimateErrorRate,"
      "localEffectivityRate";
  datFile << endl;

  //int quadorder=-1;
  //ElementIntegral <TopoD2, Triangle, ArrayQ> ElemIntegral(quadorder);

//---------------------------------------------------------------------------//
  // Compute the exact output on a fine grid and high-quadrature
  Real globalExt=0;

  {
    XField2D_Box_UnionJack_Triangle_X1 xfld( 100, 100 );

    for_each_CellGroup<TopoD2>::apply( WeightedFunctionIntegral(volumeFcn, solnExact, globalExt, {0}), xfld );
    std::cout << "globalExt = " << std::scientific << std::setprecision(16) << globalExt << std::endl;
  }
//---------------------------------------------------------------------------//

  const int orderinc = 2;

#define CG_TRACE

  int ordermin = 1;
#ifndef SANS_FULLTEST
  // ordermax = 3 is used for the pyrite
  int ordermax = 2;
#else
  // ordermax = 3 is used for the pyrite
  int ordermax = 2;
#endif

  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
    cout << "...running full test" << endl;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      //ii = pow( 2, power );
#ifndef SANS_FULLTEST
      //ii = 2 is used for the pyrite
      ii = 4;
#else
      // ii = 4*power + 4 is used for the pyrite
      ii = 4*power;
#endif
      //ii = 2;
      jj = ii;

      cout << std::scientific << std::setprecision(7);
      // grid: Hierarchical P1 ( aka X1)

      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

      std::vector<int> cellGroups = {0}, interiorTraceGroups = {};
      for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      //Solution data
      // Compute the primal in p, the adjoint in p+pinc and the p adjoint (done internally)
      std::shared_ptr<SolutionClass> pGlobalSol;
      pGlobalSol = std::make_shared<SolutionClass>(xfld, pde, order, order+orderinc,
                                                   BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                   active_boundaries, stab);

      const int quadOrder = 2*(order + orderinc);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterface;
      pInterface = std::make_shared<SolverInterfaceClass>(*pGlobalSol, ResNormType, tol, quadOrder,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);
      // Initialize
      pGlobalSol->setSolution(0.0);

      // Compute primal and adjoint
      pInterface->solveGlobalPrimalProblem();
      pInterface->solveGlobalAdjointProblem();

      Real globalSln = pInterface->getOutput(); // Compute the output functional

#if 0
      std::string adjfld_filename = "tmp/adjfld_i" + std::to_string(ii) + "_p" + std::to_string(order+orderinc) + ".plt";
      output_Tecplot( pInterface->getAdjField(), adjfld_filename );
#endif

      // Call the Error Estimate class independently and Explicitly
      QuadratureOrder quadratureOrder( xfld, quadOrder );
      ErrorEstimateClass ErrorEstimate( xfld,
                                        pGlobalSol->primal.qfld, pGlobalSol->primal.lgfld,
                                        pGlobalSol->adjoint.qfld, pGlobalSol->adjoint.lgfld, // This is the delta adjoint
                                        pde, stab, quadratureOrder, {0}, {0},
                                        PyBCList, BCBoundaryGroups );

      const Field<PhysD2,TopoD2,Real>& ifldEstimate = ErrorEstimate.getIField();

      // --------------------------------------------------------//
      // Compute `Exact' solution of Pmax = 7
      // --------------------------------------------------------//
// #ifdef SANS_FULLTEST
//       int pmax = 7; BOOST_REQUIRE( 2*order+orderinc <= 7); //BasisFunctionArea_Triangle_HierarchicalPMax;;
// #else
//       int pmax = 7; BOOST_REQUIRE( 2*order+1 <= 7);
// #endif

      const int pmax = 7;


      //Solution data
      // Compute the primal in p, the adjoint in p+pinc and the p adjoint (done internally)
      std::shared_ptr<SolutionClass> pGlobalSolPMax;
      pGlobalSolPMax = std::make_shared<SolutionClass>(xfld, pde, order, pmax,
                                                   BasisFunctionCategory_Hierarchical, BasisFunctionCategory_Hierarchical,
                                                   active_boundaries, stab);

      const int quadOrderPMax = 2*(pmax + 1);

      //Create solver interface
      std::shared_ptr<SolverInterfaceClass> pInterfacePMax;
      pInterfacePMax = std::make_shared<SolverInterfaceClass>(*pGlobalSolPMax, ResNormType, tol, quadOrderPMax,
                                                          cellGroups, interiorTraceGroups,
                                                          PyBCList, BCBoundaryGroups,
                                                          SolverContinuationDict, LinearSolverDict,
                                                          outputIntegrand);
      // Initialize
      pGlobalSolPMax->setSolution(0.0);

      // Compute primal and adjoint
      pInterfacePMax->solveGlobalPrimalProblem();
      pInterfacePMax->solveGlobalAdjointProblem();

      // Call the Error Estimate class independently and Explicitly
      QuadratureOrder quadratureOrderPMax( xfld, quadOrderPMax );
      ErrorEstimateClass ErrorExact( xfld,
                                     pGlobalSolPMax->primal.qfld, pGlobalSolPMax->primal.lgfld,
                                     pGlobalSolPMax->adjoint.qfld, pGlobalSolPMax->adjoint.lgfld, // This is the delta adjoint
                                     pde, stab, quadratureOrder, {0}, {0},
                                     PyBCList, BCBoundaryGroups );

      const Field<PhysD2,TopoD2,Real>& ifldExact = ErrorExact.getIField();

      // --------------------------------------------------------//
      // Manipulate the local error estimates to compute the global statistics required
      // --------------------------------------------------------//

      Real avglocalErrEstEff=0,globalErrEst=0;
      Real avgLocalErrExt=0, avgLocalErrEst=0,avglocalErrEstError=0;

      KahanSum<Real> sumErrEst=0;
      // Estimates
      ErrorEstimate.getErrorEstimate(globalErrEst);
      ErrorEstimate.getErrorIndicator(avgLocalErrEst);

      // 'Exact'
      ErrorExact.getErrorIndicator(avgLocalErrExt);

      // Error in local Estimate
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldExact.nDOF() );
      for (int i = 0; i < ifldExact.nDOF(); i++ )
      {
        sumErrEst += fabs( ifldExact.DOF(i)-ifldEstimate.DOF(i) );
        //std::cout << "ifldEstimate.DOF(i) = " << ifldEstimate.DOF(i) << std::endl;
        //std::cout << "ifldExact.DOF(i) = " << ifldExact.DOF(i) << std::endl;
      }

      avglocalErrEstError = (Real)sumErrEst;
      //std::cout<< "avglocalErrEstError = " << avglocalErrEstError <<std::endl;
      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExt;
      avgLocalErrExt      /= ifldEstimate.nDOF();
      avgLocalErrEst      /= ifldEstimate.nDOF();
      avglocalErrEstError /= ifldEstimate.nDOF();

      hVec[indx] = 1./ii; //sqrt(nDOFPDE);

      // Compute the true global error using the `Exact' output functional
      Real globalError = globalSln - globalExt;

      globalErrorVec[indx] = abs(globalError);
      globalEstVec[indx] = abs(globalErrEst);
      globalEstErrVec[indx] = abs( globalError - globalErrEst );
      globalEffVec[indx] = globalEstErrVec[indx]/globalEstVec[indx];

      localErrorVec[indx] = abs(avgLocalErrExt);
      localEstVec[indx] = abs(avgLocalErrEst);
      localEstErrVec[indx] = avglocalErrEstError;
      localEffVec[indx] = avglocalErrEstEff;

      if (indx > 0)
      {
        globalErrorRate[indx]  = (log(globalErrorVec[indx])  - log(globalErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstRate[indx]    = (log(globalEstVec[indx])    - log(globalEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstErrRate[indx] = (log(globalEstErrVec[indx]) - log(globalEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        globalEffRate[indx]    = (log(globalEffVec[indx])    - log(globalEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));

        localErrorRate[indx]  = (log(localErrorVec[indx])  - log(localErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstRate[indx]    = (log(localEstVec[indx])    - log(localEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstErrRate[indx] = (log(localEstErrVec[indx]) - log(localEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        localEffRate[indx]    = (log(localEffVec[indx])    - log(localEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
      }
      else
      {
        globalErrorRate[indx]  = 0;
        globalEstRate[indx]    = 0;
        globalEstErrRate[indx] = 0;
        globalEffRate[indx]    = 0;

        localErrorRate[indx]  = 0;
        localEstRate[indx]    = 0;
        localEstErrRate[indx] = 0;
        localEffRate[indx]    = 0;
      }

      if ( power == powermin )
      {
        cout << "P = " << order << ", P^ = " << order + orderinc << ", P_ = " <<  orderinc << endl;
        cout << "  \t";
        cout << " global     (2P)      global     (2P)      global     (2P^)     global     (2P_)    ";
        cout << " local      (2P+2)    local      (2P+2)    local      (P+P^+2)  local      (P_) " << endl;
        cout << "ii\t";
        cout << " exact                estimate             err in estimate      effectivity         ";
        cout << " exact                estimate             err in estimate      effectivity" << endl;

        resultFile << "ZONE T=\"CG P=" << order << "\"" << endl;
      }

      resultFile << std::setprecision(16) << std::scientific;
      resultFile << hVec[indx];
      resultFile << ", " << globalErrorVec[indx];
      resultFile << ", " << globalEstVec[indx];
      resultFile << ", " << globalEstErrVec[indx];
      resultFile << ", " << globalEffVec[indx];
      resultFile << ", " << globalErrorRate[indx];
      resultFile << ", " << globalEstRate[indx];
      resultFile << ", " << globalEstErrRate[indx];
      resultFile << ", " << globalEffRate[indx];

      resultFile << ", " << localErrorVec[indx];
      resultFile << ", " << localEstVec[indx];
      resultFile << ", " << localEstErrVec[indx];
      resultFile << ", " << localEffVec[indx];
      resultFile << ", " << localErrorRate[indx];
      resultFile << ", " << localEstRate[indx];
      resultFile << ", " << localEstErrRate[indx];
      resultFile << ", " << localEffRate[indx];
      resultFile << endl;

      // For Latex
      datFile << std::setprecision(16) << std::scientific;
      if (indx==0)
      {
        datFile << order;
      }
      else
      {
        datFile << " ";
      }
      datFile << "," << hVec[indx];
      datFile << "," << globalErrorVec[indx];
      datFile << "," << globalEstVec[indx];
      datFile << "," << globalEstErrVec[indx];
      datFile << "," << globalEffVec[indx];
      datFile << "," << globalErrorRate[indx];
      datFile << "," << globalEstRate[indx];
      datFile << "," << globalEstErrRate[indx];
      datFile << "," << globalEffRate[indx];
      datFile << "," << localErrorVec[indx];
      datFile << "," << localEstVec[indx];
      datFile << "," << localEstErrVec[indx];
      datFile << "," << localEffVec[indx];
      datFile << "," << localErrorRate[indx];
      datFile << "," << localEstRate[indx];
      datFile << "," << localEstErrRate[indx];
      datFile << "," << localEffRate[indx];
      datFile << endl;

      pyriteFile << hVec[indx]; // grid
      pyriteFile << globalErrorVec[indx] << globalEstVec[indx]<< globalEstErrVec[indx]; // values
      pyriteFile.close(0.15) << globalErrorRate[indx]<< globalEstRate[indx]<< globalEstErrRate[indx]; // rates
      pyriteFile << globalEffVec[indx];
      pyriteFile.close(0.15) << globalEffRate[indx]; //effectivities
      pyriteFile << endl;

      cout << ii << "\t";
      cout << std::scientific;
      cout << std::scientific << std::setprecision(5) << globalErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEffRate[indx] << ")  ";

      cout << std::scientific << std::setprecision(5) << localErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEffRate[indx] << ")  ";
      cout << endl;

      indx++;

    }

    cout << endl;
  }
  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
