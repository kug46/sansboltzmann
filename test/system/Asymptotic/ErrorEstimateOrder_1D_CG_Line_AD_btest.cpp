// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateOrder_1D_CG_Line_AD_btest
// testing error estimation of 2-D CG with Advection-Diffusion on triangles

//#define SANS_FULLTEST

#include <boost/test/unit_test.hpp>

#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_StrongForm.h"
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "tools/KahanSum.h"
#include "pyrite_fstream.h"

#include "Surreal/SurrealS.h"

//PDE
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/SolutionFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h" // Needed because the BCVector is extended here

//Solve includes
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Field/Function/WeightedFunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

// Estimate includes
#include "Discretization/Galerkin/IntegrandCell_Galerkin_StrongForm.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"

#include "ErrorEstimate/Galerkin/ErrorEstimate_StrongForm_Galerkin.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/Tuple/FieldTuple.h"

//Linear algebra
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateOrder_1D_CG_Line_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateOrder_1D_CG_Line_AD )
{
  timer totaltime;

#if 1  // Laplacian w/ Exponential functions
  // This is the worst case scenario with p_inc = 2 -- See Hugh's Masters Thesis
  typedef ScalarFunction1D_Exp3 SolutionExact;
  typedef ScalarFunction1D_Sine AdjointExact;

  // ND Exacts
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD1, AdjointExact> NDAdjointExact;

  // Primal PDE
  typedef PDEAdvectionDiffusion<PhysD1,
                                 AdvectiveFlux1D_Uniform,
                                 ViscousFlux1D_Uniform,
                                 Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCNDConvertSpace<PhysD1,  BCAdvectionDiffusion<PhysD1,BCType> > NDBCClass;
  typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // Adjoint PDE
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEAdjoint1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdjoint1D> PDEAjointClass;

  // Integrand Functors
  // call this class with something of Adjoint Exact Class
  typedef SolutionFunction1D_ForcingFunction<PDEAjointClass>      WeightFunctional;
  typedef SolnNDConvertSpace<PhysD1, WeightFunctional>           NDWeightFunctional;

  typedef OutputCell_WeightedSolution<PDEClass, WeightFunctional> WeightOutputClass;
  typedef OutputNDConvertSpace<PhysD1, WeightOutputClass>        NDWeightOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDWeightOutputClass>      IntegrandClass;

  // Estimation
  typedef ErrorEstimate_StrongForm_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector, XField<PhysD1,TopoD1>> ErrorEstimateClass;

  // PDE
  Real u=0.;
  AdvectiveFlux1D_Uniform adv( u );

  Real nu = 1.1;
  ViscousFlux1D_Uniform visc( nu );

  Source1D_UniformGrad source(0,0);

  Real a=5;

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict Exp3;
  Exp3[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Exp3;
  Exp3[SolutionExact::ParamsType::params.a] = a;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = Exp3;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = {0, 1};

  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(a) );
  NDBCClass bc( solnExactPtr, visc );

  // Adjoint PDE

  AdvectiveFlux1D_Uniform adv_adj( -u );

  a=1;
  NDAdjointExact adjExact(a);

  typedef ForcingFunction1D_MMS<PDEAdjoint1D> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr(new ForcingAdjType(adjExact));

  PDEAjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  NDWeightFunctional volumeFcn( pdeadj );

  // volumeFcn is g in (g,u) g = - Del psi
  NDWeightOutputClass weightOutput( volumeFcn );
  IntegrandClass outputFcn( weightOutput, {0} );

  cout << "case: Laplacian w/ Exponentials" << endl;

#endif

  // linear system setup
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // Solve integrand needed for strong form at the moment
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> IntegrandBCClass;

  // Estimate integrands
  typedef IntegrandCell_Galerkin_StrongForm<NDPDEClass> EstIntegrandCellClass;
  typedef IntegrandInteriorTrace_Galerkin_StrongForm<NDPDEClass> EstIntegrandInteriorTraceClass;
  typedef IntegrandBoundaryTrace<NDPDEClass, NDBCVecCat, Galerkin> EstIntegrandBCClass;

  // solve integrand needed for strong form
  IntegrandBCClass fcnBC( pde, bc, BCBoundaryGroups["BCName"] );

  // Stabilization
  const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, 1);

  //estimate integrands
  EstIntegrandCellClass estFcnCell( pde, {0}, stab );
  EstIntegrandInteriorTraceClass estFcnIntTrace( pde, {0} );
  EstIntegrandBCClass estFcnBC( pde, bc, BCBoundaryGroups["BCName"] );

  // norm data
  const int vsize = 10;
  Real hVec[vsize];
  Real globalErrorVec[vsize], globalEstVec[vsize], globalEstErrVec[vsize], globalEffVec[vsize];
  Real localErrorVec[vsize], localEstVec[vsize], localEstErrVec[vsize], localEffVec[vsize];
  Real globalErrorRate[vsize], globalEstRate[vsize], globalEstErrRate[vsize], globalEffRate[vsize];
  Real localErrorRate[vsize], localEstRate[vsize], localEstErrRate[vsize], localEffRate[vsize];
  int indx;

#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/ErrorEst1D_CG.plt", std::ios::out);
  std::ofstream datFile("tmp/ErrorEst1D_CG.dat", std::ios::out);
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_1D_CG_AD_FullTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-9, 1, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  std::stringstream datFile;
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_1D_CG_AD_MinTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-9, 5e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"Global Error\"";
  resultFile << ", \"Global Error Estimate\"";
  resultFile << ", \"Global Error Estimate Error\"";
  resultFile << ", \"Global 1-Effectivity\"";
  resultFile << ", \"Global Error Rate\"";
  resultFile << ", \"Global Error Estimate Rate\"";
  resultFile << ", \"Global Error Estimate Error Rate\"";
  resultFile << ", \"Global 1-Effectivity Rate\"";
  resultFile << ", \"Local Error\"";
  resultFile << ", \"Local Error Estimate\"";
  resultFile << ", \"Local Error Estimate Error\"";
  resultFile << ", \"Local 1-Effectivity\"";
  resultFile << ", \"Local Error Rate\"";
  resultFile << ", \"Local Error Estimate Rate\"";
  resultFile << ", \"Local Error Estimate Error Rate\"";
  resultFile << ", \"Local 1-Effectivity Rate\"";
  resultFile << endl;

  // For Latex Output
  datFile << "order,"
      "h,"
      "globalError,"
      "globalErrorEstimate,"
      "globalErrorEstimateError,"
      "globalEffectivity,"
      "globalErrorRate,"
      "globalErrorEstimateRate,"
      "globalErrorEstimateErrorRate,"
      "globalEffectivityRate,"
      "localError,"
      "localErrorEstimate,"
      "localErrorEstimateError,"
      "localEffectivity,"
      "localErrorRate,"
      "localErrorEstimateRate,"
      "localErrorEstimateErrorRate,"
      "localEffectivityRate";
  datFile << endl;

  int quadorder=-1;
  ElementIntegral <TopoD1, Line, ArrayQ> ElemIntegral(quadorder);

  //---------------------------------------------------------------------------//
    // Compute the exact output on a fine grid and high-quadrature
    Real globalExt=0;

    {
      XField1D xfld( 50 );

      for_each_CellGroup<TopoD1>::apply( WeightedFunctionIntegral(volumeFcn, solnExact, globalExt, {0}), xfld );

      //std::cout << "globalExt = " << std::scientific << std::setprecision(16) << globalExt << std::endl;
    }
//---------------------------------------------------------------------------//

  // orderinc = 2; used for pyrite
  const int orderinc = 2;

  int ordermin = 1;
#ifndef SANS_FULLTEST
  // ordermax = 2 is used for the pyrite
  int ordermax = 2;
#else
  // ordermax = 2 is used for the pyrite
  int ordermax = 2;
#endif

  for (int order = ordermin; order <= ordermax; order++)
  {

    indx = 0;

    // loop over grid resolution: 2^power
    int ii;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
    cout << "...running full test" << endl;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      //ii = pow( 2, power );
#ifndef SANS_FULLTEST
      //ii = 2 is used for the pyrite
      ii = 2;
#else
      // 4*power used for pyrite
      ii = 4*power;
#endif

      cout << std::scientific << std::setprecision(7);
      // grid: Hierarchical P1 ( aka X1)

      XField1D xfld( ii );

      // Primal Solution in p
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
      std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical, LG_BGroup_list );
      qfld = 0; lgfld = 0;

      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-11, 1e-11};

      // create the primal equations

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, stab,
                                         quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );

      // residual

      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

      PrimalEqSet.fillSystemVector(q);

      rsd = 0;
      PrimalEqSet.residual(q, rsd);

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());
      solver.solve(rsd, dq);

      // update solution
      q -= dq;

      PrimalEqSet.setSolutionField(q);

      // Adjoint in p
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qflda(xfld, order, BasisFunctionCategory_Hierarchical);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgflda( xfld, order, BasisFunctionCategory_Hierarchical, LG_BGroup_list );
      int nDOFPDE = qflda.nDOF(), nDOFBC = lgflda.nDOF();

      // adjoint solve
      SystemVectorClass adj(q.size());
      rsd = 0;

      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell_Galerkin( outputFcn, rsd(0) ),
          xfld, qflda, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint in p
      SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);
      solverAdj.solve(rsd, adj);

      // update the adjoint solution
      for (int k = 0; k < nDOFPDE; k++)
        qflda.DOF(k) = adj[0][k];
      for (int k = 0; k < nDOFBC; k++)
        lgflda.DOF(k) = adj[1][k];

      // ---------------------
      // Adjoint error estimate
      // Adjoint in p+1
      // ---------------------

      // GLOBAL ESTIMATE
      // prolongated primal to p+1
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfldPp1(xfld, order+orderinc, BasisFunctionCategory_Hierarchical);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldPp1( xfld, order+orderinc, BasisFunctionCategory_Hierarchical, LG_BGroup_list );
      const int nDOFPDEPp1 = qfldPp1.nDOF(), nDOFBCPp1 = lgfldPp1.nDOF();

      // prolongate the solution
      qfld.projectTo(qfldPp1);
      lgfld.projectTo(lgfldPp1);

      PrimalEquationSetClass PrimalEqSetPp1(xfld, qfldPp1, lgfldPp1, pde, stab,
                                            quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );

      // residual field
      SystemVectorClass qPp1(PrimalEqSetPp1.vectorStateSize());
      PrimalEqSetPp1.fillSystemVector(qPp1);


      // LOCAL ESTIMATE

      // Adjoint in p+1 solution field
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfldaPp1(xfld, order+orderinc, BasisFunctionCategory_Hierarchical);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldaPp1( xfld, order+orderinc, BasisFunctionCategory_Hierarchical, LG_BGroup_list );
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfldaProlongateP(xfld, order+orderinc, BasisFunctionCategory_Hierarchical); // for the prolongated p
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldaProlongateP( xfld, order+orderinc, BasisFunctionCategory_Hierarchical, LG_BGroup_list );

      qflda.projectTo(qfldaProlongateP); // prolongate the p adjoint to subtract from the p+1
      lgflda.projectTo(lgfldaProlongateP);

      // Functional integral
      SystemVectorClass rhsPp1(qPp1.size());
      SystemVectorClass adjPp1(qPp1.size());
      rhsPp1 = 0;

      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell_Galerkin( outputFcn, rhsPp1(0) ),
          xfld, qfldaPp1, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint
      SLA::UMFPACK<SystemMatrixClass> solverAdjPp1(PrimalEqSetPp1, SLA::TransposeSolve);
      solverAdjPp1.solve(rhsPp1, adjPp1);

      // update the solution  - subtract the p solution at the same time
      for (int k = 0; k < nDOFPDEPp1; k++)
        qfldaPp1.DOF(k) = adjPp1[0][k] - qfldaProlongateP.DOF(k);
      for (int k = 0; k < nDOFBCPp1; k++)
        lgfldaPp1.DOF(k) = adjPp1[1][k] - lgfldaProlongateP.DOF(k);

      // Calling the Error Estimate functions
      ErrorEstimateClass ErrorEstimate( xfld,
                                        qfld, lgfld, qfldaPp1, lgfldaPp1,
                                        pde, stab, quadratureOrder, {0}, {0},
                                        PyBCList,
                                        BCBoundaryGroups);
      const Field_DG_Cell<PhysD1,TopoD1,Real>& ifldEstimate = ErrorEstimate.getIField();

      // ---------------------
      // Adjoint 'exact' error
      // ---------------------
      // prolongate the primal to pmax and then evaluate strong residual

#ifdef SANS_FULLTEST
      int pmax = 7; BOOST_REQUIRE( 2*order+orderinc <= 7); //BasisFunctionArea_Line_HierarchicalPMax;;
#else
      int pmax = 2*order + 1; BOOST_REQUIRE( 2*order+1 <= 7);
#endif

      // GLOBAL ESTIMATE
      // prolongate primal to pmax
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfldPmax(xfld, pmax, BasisFunctionCategory_Hierarchical);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldPmax( xfld, pmax, BasisFunctionCategory_Hierarchical, LG_BGroup_list );
      const int nDOFPDEPmax = qfldPmax.nDOF(), nDOFBCPmax = lgfldPmax.nDOF();

      // prolongate the solution
      qfld.projectTo(qfldPmax);
      lgfld.projectTo(lgfldPmax);

      PrimalEquationSetClass PrimalEqSetPmax(xfld, qfldPmax, lgfldPmax, pde, stab,
                                             quadratureOrder,  ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );

      // residual field
      SystemVectorClass qPmax(PrimalEqSetPmax.vectorStateSize());

      PrimalEqSetPmax.fillSystemVector(qPmax);

      // Have to use a p7 solve because projection is a global operation

      // Adjoint in pmax solution field
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfldaPmax(xfld, pmax, BasisFunctionCategory_Hierarchical);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldaPmax( xfld, pmax, BasisFunctionCategory_Hierarchical, LG_BGroup_list );
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfldaProlongatePmax(xfld, pmax, BasisFunctionCategory_Hierarchical);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldaProlongatePmax( xfld, pmax, BasisFunctionCategory_Hierarchical, LG_BGroup_list );

      qflda.projectTo(qfldaProlongatePmax); // prolongate the p adjoint to subtract from the pmax
      lgflda.projectTo(lgfldaProlongatePmax);

      // Functional integral
      SystemVectorClass rhsPmax(qPmax.size());
      SystemVectorClass adjPmax(qPmax.size());
      rhsPmax = 0;

      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell_Galerkin( outputFcn, rhsPmax(0) ),
          xfld, qfldaPmax, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint
      SLA::UMFPACK<SystemMatrixClass> solverAdjPmax(PrimalEqSetPmax, SLA::TransposeSolve);
      solverAdjPmax.solve(rhsPmax, adjPmax);

      // update the solution - subtract the p solution at the same time
      for (int k = 0; k < nDOFPDEPmax; k++)
      {
        qfldaPmax.DOF(k) = adjPmax[0][k]- qfldaProlongatePmax.DOF(k);
//        std::cout<< "adjPmax.DOF(" << k << ") = " << adjPmax[0][k] <<
//            ", qfldaProlongatePmax.DOF(" << k << ") = " << qfldaProlongatePmax.DOF(k) <<std::endl;
      }
      for (int k = 0; k < nDOFBCPmax; k++)
      {
        lgfldaPmax.DOF(k) = adjPmax[1][k]- lgfldaProlongatePmax.DOF(k);
      }

      // Calling the Error Estimate functions
      ErrorEstimateClass ErrorExact( xfld,
                                     qfld, lgfld, qfldaPmax, lgfldaPmax,
                                     pde, stab, quadratureOrder, {0}, {0},
                                     PyBCList,
                                     BCBoundaryGroups);
      const Field_DG_Cell<PhysD1,TopoD1,Real>& ifldExact = ErrorExact.getIField();

      typedef typename XField<PhysD1, TopoD1>::FieldCellGroupType<Line> XFieldCellGroupType;
      const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Line>(0);

      // global and local error
      // solution - p - For directly evaluating the functional, and redistributing eLfld

      Real globalSln=0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( outputFcn, globalSln ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real avglocalErrEstEff=0,globalErrEst=0;
      Real avgLocalErrExt=0, avgLocalErrEst=0,avglocalErrEstError=0;

      KahanSum<Real> sumErrEst=0;
      // Estimates
      ErrorEstimate.getErrorEstimate(globalErrEst);
      ErrorEstimate.getErrorIndicator(avgLocalErrEst);

      // 'Exact'
      ErrorExact.getErrorIndicator(avgLocalErrExt);

      // Error in local Estimate
      Field_DG_Cell<PhysD1, TopoD1, Real> ifldDiff(xfld, 0, BasisFunctionCategory_Legendre);
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldExact.nDOF() );
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldDiff.nDOF() );
      for (int i = 0; i < ifldExact.nDOF(); i++ )
      {
        ifldDiff.DOF(i) = ifldExact.DOF(i)-ifldEstimate.DOF(i);
//        std::cout<< "ifldExact.DOF(" << i << ") = " << ifldExact.DOF(i) <<", ifldEstimate.DOF(" << i << ") = " << ifldEstimate.DOF(i) <<std::endl;
//        std::cout<< "ifldDiff.DOF(" << i << ") = " << ifldDiff.DOF(i) <<std::endl;
        sumErrEst += fabs( ifldDiff.DOF(i) );
      }
      avglocalErrEstError = (Real)sumErrEst;

      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExt;
      avgLocalErrExt      /= xfldCell.nElem();
      avgLocalErrEst      /= xfldCell.nElem();
      avglocalErrEstError /= xfldCell.nElem();

      const Real close_tol = 5e-7;
      const Real small_tol = 5e-7;
      if (power==1)
      {
        // Computing the estimate using the global weak form

        // residual field
        SystemVectorClass rsdPp1(PrimalEqSetPp1.vectorEqSize());
        rsdPp1 = 0;

        // Recompute lifting perators
        SystemVectorClass qPp1(PrimalEqSetPp1.vectorStateSize());
        PrimalEqSetPp1.fillSystemVector(qPp1);

        PrimalEqSetPp1.residual(qPp1, rsdPp1);

        Real globalErrEst2 = dot(rsdPp1, adjPp1);
        SANS_CHECK_CLOSE( globalErrEst2, globalErrEst, small_tol, close_tol );
      }

      Real globalError = globalSln - globalExt;

      hVec[indx] = 1./ii; //sqrt(nDOFPDE);

      globalErrorVec[indx] = abs(globalError);
      globalEstVec[indx] = abs(globalErrEst);
      globalEstErrVec[indx] = abs( globalError - globalErrEst );
      globalEffVec[indx] = globalEstErrVec[indx]/globalEstVec[indx];

      localErrorVec[indx] = abs(avgLocalErrExt);
      localEstVec[indx] = abs(avgLocalErrEst);
      localEstErrVec[indx] = avglocalErrEstError;
      localEffVec[indx] = avglocalErrEstEff;

      if (indx > 0)
      {
        globalErrorRate[indx]  = (log(globalErrorVec[indx])  - log(globalErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstRate[indx]    = (log(globalEstVec[indx])    - log(globalEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstErrRate[indx] = (log(globalEstErrVec[indx]) - log(globalEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        globalEffRate[indx]    = (log(globalEffVec[indx])    - log(globalEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));

        localErrorRate[indx]  = (log(localErrorVec[indx])  - log(localErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstRate[indx]    = (log(localEstVec[indx])    - log(localEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstErrRate[indx] = (log(localEstErrVec[indx]) - log(localEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        localEffRate[indx]    = (log(localEffVec[indx])    - log(localEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
      }
      else
      {
        globalErrorRate[indx]  = 0;
        globalEstRate[indx]    = 0;
        globalEstErrRate[indx] = 0;
        globalEffRate[indx]    = 0;

        localErrorRate[indx]  = 0;
        localEstRate[indx]    = 0;
        localEstErrRate[indx] = 0;
        localEffRate[indx]    = 0;
      }

      if ( power == powermin )
      {
        cout << "P = " << order << ", P^ = " << order + orderinc << ", P_ = " <<  orderinc << endl;
        cout << "  \t";
        cout << " global     (2P)      global     (2P)      global     (2P^)     global     (2P_)    ";
        cout << " local      (2P+1)    local      (2P+1)    local      (2P^+1)   local      (2P_) " << endl;
        cout << "ii\t";
        cout << " exact                estimate             err in estimate      effectivity         ";
        cout << " exact                estimate             err in estimate      effectivity" << endl;

        resultFile << "ZONE T=\"CG P=" << order << "\"" << endl;
      }

      resultFile << std::setprecision(16) << std::scientific;
      resultFile << hVec[indx];
      resultFile << ", " << globalErrorVec[indx];
      resultFile << ", " << globalEstVec[indx];
      resultFile << ", " << globalEstErrVec[indx];
      resultFile << ", " << globalEffVec[indx];
      resultFile << ", " << globalErrorRate[indx];
      resultFile << ", " << globalEstRate[indx];
      resultFile << ", " << globalEstErrRate[indx];
      resultFile << ", " << globalEffRate[indx];

      resultFile << ", " << localErrorVec[indx];
      resultFile << ", " << localEstVec[indx];
      resultFile << ", " << localEstErrVec[indx];
      resultFile << ", " << localEffVec[indx];
      resultFile << ", " << localErrorRate[indx];
      resultFile << ", " << localEstRate[indx];
      resultFile << ", " << localEstErrRate[indx];
      resultFile << ", " << localEffRate[indx];
      resultFile << endl;

      // For Latex
      datFile << std::setprecision(16) << std::scientific;
      if (indx==0)
      {
        datFile << order;
      }
      else
      {
        datFile << " ";
      }
      datFile << "," << hVec[indx];
      datFile << "," << globalErrorVec[indx];
      datFile << "," << globalEstVec[indx];
      datFile << "," << globalEstErrVec[indx];
      datFile << "," << globalEffVec[indx];
      datFile << "," << globalErrorRate[indx];
      datFile << "," << globalEstRate[indx];
      datFile << "," << globalEstErrRate[indx];
      datFile << "," << globalEffRate[indx];
      datFile << "," << localErrorVec[indx];
      datFile << "," << localEstVec[indx];
      datFile << "," << localEstErrVec[indx];
      datFile << "," << localEffVec[indx];
      datFile << "," << localErrorRate[indx];
      datFile << "," << localEstRate[indx];
      datFile << "," << localEstErrRate[indx];
      datFile << "," << localEffRate[indx];
      datFile << endl;

      pyriteFile << hVec[indx]; // grid
      pyriteFile << globalErrorVec[indx] << globalEstVec[indx] << globalEstErrVec[indx]; // values
      pyriteFile.close(1.5) << globalErrorRate[indx]<< globalEstRate[indx] << globalEstErrRate[indx]; // rates
      pyriteFile << globalEffVec[indx];
      pyriteFile.close(1.5) << globalEffRate[indx]; //effectivities
      pyriteFile << endl;


      cout << ii << "\t";
      cout << std::scientific;
      cout << std::scientific << std::setprecision(5) << globalErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEffRate[indx] << ")  ";

      cout << std::scientific << std::setprecision(5) << localErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEffRate[indx] << ")  ";
      cout << endl;

      indx++;

#if 0
      string filename = "tmp/locErrCG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, localErrEstfldArea, filename );
#endif
    }

    cout << endl;
  }
  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
