// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adjoint1D_DGBR2_BuckleyLeverett_btest
// testing of 1-D DG with Buckley-Leverett

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"
#include "pde/PorousMedia/SolutionFunction_BuckleyLeverett1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Adjoint1D_DGBR2_BuckleyLeverett_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adjoint1D_DGBR2_BuckleyLeverett_ST )
{
  typedef SurrealS<1> SurrealClass;

  typedef RelPermModel_PowerLaw RelPermModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel, CapillaryModel> TraitsModelClass;
  typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef SolutionFunction_BuckleyLeverett1D_Shock<QTypePrimitive_Sw, PDEClass> ExactSolutionClass;
  typedef SolnNDConvertSpaceTime<PhysD1, ExactSolutionClass> NDExactSolutionClass;

  const int D = PhysD2::D;

  typedef BCBuckleyLeverett1DVector<PDEClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  // PDE
  RelPermModel kr_model(2);
  const Real phi = 0.3;
  const Real uT = 0.3;
  const Real mu_w = 1;
  const Real mu_n = 2;

  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;
  const Real pc_max = 0.5;
  CapillaryModel cap_model(pc_max);

  NDPDEClass pde(phi, uT, kr_model, mu_w, mu_n, K, cap_model);

  const Real SwL = 1.0;
  const Real SwR = 0.1;
  const Real xinit = 0.0;
  NDExactSolutionClass solnExact(pde, SwL, SwR, xinit);


  int iB = XField2D_Box_Triangle_X1::iBottom;
  int iR = XField2D_Box_Triangle_X1::iRight;
  int iT = XField2D_Box_Triangle_X1::iTop;
  int iL = XField2D_Box_Triangle_X1::iLeft;


  // BCs

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  TimeIC[BCBuckleyLeverett1DParams<BCTypeTimeIC>::params.qB] = SwR;

  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletL[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params.qB] = SwL;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet_mitState;
  BCDirichletR[BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params.qB] = SwR;

  PyDict PyBCList;
  PyBCList["DirichletL"] = BCDirichletL;
  PyBCList["DirichletR"] = BCDirichletR;
  PyBCList["None"] = BCNone;
  PyBCList["TimeIC"] = TimeIC;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletR"] = {iR};
  BCBoundaryGroups["DirichletL"] = {iL};
  BCBoundaryGroups["None"] = {iT};
  BCBoundaryGroups["TimeIC"] = {iB};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // BR2 discretization
  Real viscousEtaParameter = Triangle::NTrace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  NDOutputClass fcnOutput;
  OutputIntegrandClass errIntegrand(fcnOutput, {0});

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 20;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);

  pyrite_file_stream pyriteFile("IO/ErrorOrder/Adjoint1D_DGBR2_BuckleyLeverett_SolnTest.txt", 1e-8, 5e-6, pyrite_file_stream::check);
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 1;
#else
  int ordermax = 1;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    // loop over grid resolution: 30*factor
    int ii, jj;
    int factormin = 1;
#ifdef SANS_FULLTEST
    int factormax = 1;
#else
    int factormax = 1;
#endif
    for (int factor = factormin; factor <= factormax; factor++)
    {
      ii = 10*factor;
      jj = 5*factor;

      // grid:
      XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 50, 0, 25, true );
//      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, 50, 0, 25);

      // Initial solution
      ArrayQ qinit;
      pde.setDOFFrom( qinit, 0.5, "Sw" );

      // solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_adj(xfld, order, BasisFunctionCategory_Hierarchical);
      qfld = qinit;
      qfld_adj = 0.0;

      // Or, use the projection of the exact solution as an initial solution
      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
//      output_Tecplot( qfld, "tmp/qfld_init.plt" );
      const int nDOFPDE = qfld.nDOF();

      // lifting operators
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Hierarchical);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld_adj(xfld, order, BasisFunctionCategory_Hierarchical);
      rfld = 0.0;
      rfld_adj = 0.0;

      // Lagrange multipliers
      std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Hierarchical, active_BGroup_list);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_adj(xfld, order, BasisFunctionCategory_Hierarchical, active_BGroup_list);
      lgfld = 0.0;
      lgfld_adj = 0.0;
      const int nDOFBC = lgfld.nDOF();

      std::cout<<"DOF: "<<(nDOFPDE + nDOFBC)<<std::endl;

      //--------PRIMAL SOLVE------------

      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-12, 1e-12};
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder, ResidualNorm_Default, tol,
                                         {0}, {0,1,2}, PyBCList, BCBoundaryGroups);

      // Create the solver object
      NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );

      // set initial condition from current solution in solution fields
      SystemVectorClass sln0(PrimalEqSet.vectorStateSize());
      PrimalEqSet.fillSystemVector(sln0);

      // nonlinear solve
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SolveStatus status = nonlinear_solver.solve(sln0, sln);
      BOOST_CHECK( status.converged );

      PrimalEqSet.setSolutionField(sln);

      // Compare primal solution with results saved in pyrite file (verified against PX)
      for (int k = 0; k < rfld.nDOF(); k++ )
        for (int d = 0; d < D; d++)
          pyriteFile << rfld.DOF(k)[d] << std::endl; //rfld

      for (int k = 0; k < qfld.nDOF(); k++ )
        pyriteFile << qfld.DOF(k) << std::endl; //qfld

      pyriteFile << std::endl;

#if 0
      // jacobian nonzero pattern
      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
      PrimalEqSet.jacobian(sln, nz);

      // jacobian
      SystemMatrixClass jac(nz);
      PrimalEqSet.jacobian(sln, jac);

      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );

//      fstream fout00( "tmp/jac00.mtx", fstream::out );
//      cout << "btest: global jac00" << endl;  WriteMatrixMarketFile( jac(0,0), fout00 );
//      fstream fout10( "tmp/jac10.mtx", fstream::out );
//      cout << "btest: global jac10" << endl;  WriteMatrixMarketFile( jac(1,0), fout10 );
//      fstream fout01( "tmp/jac01.mtx", fstream::out );
//      cout << "btest: global jac01" << endl;  WriteMatrixMarketFile( jac(0,1), fout01 );
#endif

      // check that the residual is zero
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0.0;
      PrimalEqSet.residual(sln, rsd);

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[PrimalEqSet.iPDE][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[PrimalEqSet.iBC][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );


      //--------ADJOINT SOLVE------------

      // adjoint (right hand side)
      SystemVectorClass rhs(PrimalEqSet.vectorEqSize());
      rhs = 0.0;

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_DGBR2<SurrealClass>( errIntegrand, rhs(PrimalEqSet.iPDE) ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

//      rhs /= phi; //The Buckley-Leverett equation is divided by phi

      // adjoint solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet, SLA::TransposeSolve);
      SystemVectorClass adj_sln(PrimalEqSet.vectorStateSize());
      solver.solve(rhs, adj_sln);

      PrimalEqSet.setAdjointField(adj_sln, qfld_adj, rfld_adj, lgfld_adj);

      for (int k = 0; k < rfld_adj.nDOF(); k++ )
        for (int d = 0; d < D; d++)
          pyriteFile << rfld_adj.DOF(k)[d] << std::endl; //rfld_adj

      // Compare adjoint solution with results saved in pyrite file (verified against PX)
      for (int k = 0; k < qfld_adj.nDOF(); k++ )
        pyriteFile << qfld_adj.DOF(k) << std::endl; //qfld_adj

      pyriteFile << std::endl;

#if 0
      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jacT, fout );
      fstream fout00( "tmp/jac00.mtx", fstream::out );
      cout << "btest: global jac00" << endl;  WriteMatrixMarketFile( jacT(0,0), fout00 );
//      fstream fout10( "tmp/jac10.mtx", fstream::out );
//      cout << "btest: global jac10" << endl;  WriteMatrixMarketFile( jacT(1,0), fout10 );
//      fstream fout01( "tmp/jac01.mtx", fstream::out );
//      cout << "btest: global jac01" << endl;  WriteMatrixMarketFile( jacT(0,1), fout01 );
#endif

//      cout << sqrt(rsdPDEnrm) << "\t" << sqrt(rsdLOnrm) << "\t" << sqrt(rsdBCnrm) <<endl;

#if 0
      // Tecplot dump
      string filename = "tmp/adjoint_DGBR2_BuckleySpaceTime_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld_adj, filename );
      output_Tecplot( qfld, "tmp/primal.plt" );
#endif

    } //grid refinement loop

  } //order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
