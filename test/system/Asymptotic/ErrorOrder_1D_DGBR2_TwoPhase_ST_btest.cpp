// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_DGBR2_TwoPhase_ST_btest
// testing of 1-D DG with TwoPhase

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/PDETwoPhase1D.h"
#include "pde/PorousMedia/BCTwoPhase1D.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/OutputTwoPhase.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"
#include "pde/NDConvert/OutputNDConvertSpaceTime1D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime1D.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_1D_DGBR2_TwoPhase_ST_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorOrder_1D_DGBR2_TwoPhase_ST )
{
  typedef DensityModel_Comp DensityModel;
  typedef PorosityModel_Comp PorosityModel;
  typedef RelPermModel_PowerLaw RelPermModel;
  typedef ViscosityModel_Constant ViscModel;
  typedef CapillaryModel_Linear CapillaryModel;
  typedef QTypePrimitive_pnSw QType;

  typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                              RelPermModel, RelPermModel, ViscModel, ViscModel, Real,
                              CapillaryModel> TraitsModelClass;

  typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

  typedef PDENDConvertSpaceTime<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

//  typedef BCTypeFullState BCType;
  typedef BCTwoPhase1DVector<PDEClass> BCVector;
  typedef BCTwoPhase1D<BCTypeFullState, PDEClass> BCClass;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpaceTime, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef OutputTwoPhase_SaturationPower<PDEClass> OutputClass;
  typedef OutputNDConvertSpaceTime<PhysD1, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  // PDE
  const Real pref = 14.7;

  DensityModel rhow(62.4, 5.0e-6, pref);
  DensityModel rhon(52.1, 1.5e-5, pref);

  PorosityModel phi(0.3, 3.0e-6, pref);

  RelPermModel krw(2);
  RelPermModel krn(2);

  ViscModel muw(1);
  ViscModel mun(2);

  CapillaryModel pc(2.5);
  const Real conversion = (1.127e-3)*5.615; //Units : [(bbl cP)/(day mD ft psi)] * [ft^3/bbl]
  const Real K = conversion*200;

  NDPDEClass pde(rhow, rhon, phi, krw, krn, muw, mun, K, pc);

  // BCs
  PyDict LeftState;
  LeftState[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  LeftState[PressureNonWet_SaturationWet_Params::params.pn] = 2500.0;
  LeftState[PressureNonWet_SaturationWet_Params::params.Sw] = 1.0;

  PyDict RightState;
  RightState[TwoPhaseVariableTypeParams::params.StateVector.Variables] = TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet;
  RightState[PressureNonWet_SaturationWet_Params::params.pn] = 2498.0;
  RightState[PressureNonWet_SaturationWet_Params::params.Sw] = 0.5;

  PyDict BCLeft;
  BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.FullStateSpaceTime;
  BCLeft[BCClass::ParamsType::params.StateVector] = LeftState;

  PyDict BCRight;
  BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.FullStateSpaceTime;
  BCRight[BCClass::ParamsType::params.StateVector] = RightState;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict TimeIC;
  TimeIC[BCParams::params.BC.BCType] = BCParams::params.BC.TimeIC;
  TimeIC[BCTwoPhase1DParams<BCTypeTimeIC>::params.StateVector] = RightState;

  PyDict PyBCList;
  PyBCList["BCLeft"] = BCLeft;
  PyBCList["BCRight"] = BCRight;
  PyBCList["BCNone"] = BCNone;
  PyBCList["TimeIC"] = TimeIC;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCRight"] = {1};
  BCBoundaryGroups["BCNone"] = {2};
  BCBoundaryGroups["BCLeft"] = {3};
  BCBoundaryGroups["TimeIC"] = {0};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  //Output functional
  NDOutputClass fcnOutput(pde, 0, 2);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  //Volume integral of Sw^2 - estimated using a P3 solution on a 50x50x2 triangle grid (350,000 DOF inc. rfld)
  //no-upwinding + max quad:
  //Real output_exact = 1.181011283729e+02; //pcmax = 2 //1.1809768719e+02; //(pc1) 1.1530083138e+02; //3.8246696863e+02;

  //Volume integral of Sw^2 - estimated using a P2 solution on a 100x100x2 triangle grid
  Real output_exact = 1.195487468925e+02; //1.195487467579e+02; //pcmax = 2.5 with upwinding + quadrature 2*(order + 1)

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-7;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 60;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real outputerrVec[10]; // output error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/ErrorOrder_1D_DGBR2_TwoPhase_ST.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_DGBR2_TwoPhase_ST_FullTest.txt", 5e-7, 1e-6, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_DGBR2_TwoPhase_ST_MinTest.txt", 1e-7, 1e-7, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"Output error\"";
  resultFile << ", \"Output error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  //****************************************************************************************
  //Note that the convergence rates are going to be smaller than the expected 2P order, because the grids tested on are too coarse.
  //Can't run on finer grids because of the Jenkins time-limit.
  //However, running on finer grids eventually does give the right order of convergence.
  //****************************************************************************************

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "Running full test..." << endl;
#endif
  int ordermax = 2;
#else
  int ordermax = 1;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 3^power
    int ii, jj;
    int powermin = 3;
#ifdef SANS_FULLTEST
    int powermax = 5;
#else
    int powermax = 4;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );
      jj = pow( 2, power );

      // grid:
      XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 20, 0, 20, true );
//      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, 50, 0, 25);

      // Initial solution
      ArrayQ qinit;
      PressureNonWet_SaturationWet<Real> qdata(2500.0, 0.75);
      pde.setDOFFrom( qinit, qdata );

      // solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      qfld = qinit;
//      output_Tecplot( qfld, "tmp/qfld_init.plt" );
      const int nDOFPDE = qfld.nDOF();

      // lifting operators
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      rfld = 0.0;

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 0
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#elif 0
      std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
      std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, active_BGroup_list);
#endif

      lgfld = 0.0;

      //--------PRIMAL SOLVE------------

      QuadratureOrder quadratureOrder( xfld, 2*(order + 1) );
      std::vector<Real> tol = {5e-8, 5e-8};
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups);

      // Create the solver object
      NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );

      // set initial condition from current solution in solution fields
      SystemVectorClass sln0(PrimalEqSet.vectorStateSize());
      PrimalEqSet.fillSystemVector(sln0);

      // nonlinear solve
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SolveStatus status = nonlinear_solver.solve(sln0, sln);
      BOOST_CHECK( status.converged );

#if 0
      // jacobian nonzero pattern
      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
      PrimalEqSet.jacobian(sln0, nz);

      // jacobian
      SystemMatrixClass jac(nz);
      PrimalEqSet.jacobian(sln0, jac);

      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
      fstream fout00( "tmp/jac00.mtx", fstream::out );
      cout << "btest: global jac00" << endl;  WriteMatrixMarketFile( jac(0,0), fout00 );
      fstream fout10( "tmp/jac10.mtx", fstream::out );
      cout << "btest: global jac10" << endl;  WriteMatrixMarketFile( jac(1,0), fout10 );
      fstream fout01( "tmp/jac01.mtx", fstream::out );
      cout << "btest: global jac01" << endl;  WriteMatrixMarketFile( jac(0,1), fout01 );
#endif

      // Output error
      Real output = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( outputIntegrand, output ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
//      std::cout << "Sw^2 integral: " << std::setprecision(12) << std::scientific << output << std::endl;
      Real output_error = fabs(output - output_exact);

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      outputerrVec[indx] = output_error;
      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ", Output error = " << output_error;
      if (indx > 1)
      {
        cout << ", Output err ratio = " << outputerrVec[indx-1]/outputerrVec[indx-2] << ")";
        Real output_slope = (log(outputerrVec[indx-1]) - log(outputerrVec[indx-2]))/(log(hVec[indx-1]) - log(hVec[indx-2]));
        cout << " (Output slope = " << output_slope <<")";
      }
      cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/slnDGBR2_TwoPhaseSpaceTime_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
#endif

    } //grid refinement loop

#if 1
    // Tecplot output
    resultFile << "ZONE T=\"DG BR2 P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real output_slope = 0;
      if (n > 0)
      {
        output_slope = (log(outputerrVec[n]) - log(outputerrVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      }
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << outputerrVec[n];
      resultFile << ", " << output_slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << outputerrVec[n] << output_slope << std::endl;
    }
#endif

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
//    std::cout<<resultFile.str()<<std::endl;
  } //order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
