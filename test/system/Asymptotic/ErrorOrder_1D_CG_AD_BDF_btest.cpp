// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_CG_AD_BDF_btest
// testing of 1-D CG with Unsteady Advection-Diffusion Timestepping

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/FunctionNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Project.h"
#include "Discretization/Galerkin/IntegrandCell_ProjectFunction.h"

#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"

#include "Field/Element/ElementProjection_L2.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_1D_CG_AD_BDF_test_suite )

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( Solve_CG_AD_BDF )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ScalarFunction1D_SineSineUnsteady SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;
  typedef FunctionNDConvertSpace<PhysD1, Real> Function1DND;

  typedef IntegrandCell_ProjectFunction<Function1DND,IntegrandCell_ProjFcn_detail::FcnX> IntegrandCellClass;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> AlgebraicEquationSet_GalerkinClass;
  typedef AlgebraicEquationSet_GalerkinClass::BCParams BCParams;

  typedef AlgebraicEquationSet_Project< XField<PhysD1, TopoD1>, IntegrandCellClass, TopoD1, AlgEqSetTraits_Sparse > ProjectionEquationSetClass;

  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> BDFClass;

  PyDict SineSineUnsteady;
  SineSineUnsteady[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.SineSineUnsteady;
  //SineSineUnsteady[ScalarFunction1D_SineSineUnsteady::ParamsType::params.c] = 1;

  // PDE

  Real c = 0.1;
  Real nu = 1.0;
  GlobalTime time(0);

  AdvectiveFlux1D_Uniform adv( c );
  ViscousFlux1D_Uniform visc( nu );
  Source1D_UniformGrad source(0.1,0.2);

  NDSolutionExact solnExact(time, SineSineUnsteady);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  NDPDEClass pde(time, adv, visc, source, forcingptr );

  // BC

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = SineSineUnsteady;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  // integrands
  NDErrorClass fcnError(time, solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});


  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  PyDict NonLinearSolverDict;
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);


  // Tecplot output
//#ifdef SANS_FULLTEST
//  std::ofstream resultFile("tmp/L2_1D_CG.plt", std::ios::out);
//#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_CG_AD_BDF_MinTest.txt", 1e-9, 1e-7, pyrite_file_stream::check);
//#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  // set BDF and grid parameters for the run
  int BDFmin = 1;
  int BDFmax = 4;
  int iipowmin = 3;
  int iipowmax = 6;

  for (int BDForder = BDFmin; BDForder <= BDFmax; BDForder++)
  {
    int order = BDForder;

    Real hVec[10];
    Real normVec[10];   // L2 error
    int indx = 0;

    for (int iipow = iipowmin; iipow <= iipowmax; iipow++)
    {

      int ii = pow(2,iipow); //grid size - using timesteps = grid size

      XField1D xfld( ii );

      Real Tend = 1.;
      int nsteps = ii;
      Real dt = Tend / (nsteps);

      // CG solution field
      // cell solution
      Field_CG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
      // Lagrange multiplier field
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

      qfld = 0;
      lgfld = 0;

      // quadrature rule
      QuadratureOrder quadratureOrder( xfld, -1 );

      std::vector<Real> tol = {1e-11, 1e-11};


      StabilizationNitsche stab(order);
      AlgebraicEquationSet_GalerkinClass AlgEqSetSpace(xfld, qfld, lgfld, pde, stab,
                                                       quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups, time);

      // The BDF class
      BDFClass BDF( BDForder, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {0}, AlgEqSetSpace );

      // Initial condition

      int stepstart = 0;
      {
        Function1DND fcnND(time, solnExact);

        IntegrandCellClass fcnCell( fcnND, {0} );
        const std::array<Real,1> tol_projection = {{1e-15}};
        ProjectionEquationSetClass ProjEqSet(xfld, qfld, fcnCell, quadratureOrder, tol_projection );

        typedef ProjectionEquationSetClass::SystemMatrix SystemMatrixClass;
        typedef ProjectionEquationSetClass::SystemVector SystemVectorClass;

        SystemVectorClass rhs(ProjEqSet.vectorEqSize());
        SystemVectorClass dq(ProjEqSet.vectorStateSize());
        SystemVectorClass q(ProjEqSet.vectorStateSize());

        // copy the field data to the vector
        ProjEqSet.fillSystemVector(q);

        // solver factorized

        SLA::UMFPACK<SystemMatrixClass> solver(ProjEqSet);
        solver.factorize();

        // Set previous solutions
        time = -dt;
        for (int j = BDForder-1; j >= 0; j--)
        {
          time += dt;
          stepstart += 1;

          rhs = 0;
          ProjEqSet.residual(rhs);

          solver.backsolve(rhs, dq);
          q -= dq;

          ProjEqSet.setSolutionField(q);
#if 0
          ArrayQ SquareError = 0;
          IntegrateCellGroups<TopoD1>::integrate(
              FunctionalCell_Galerkin( errorIntegrand, SquareError ),
              xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
          //BOOST_CHECK_SMALL(SquareError, 1e-20 );
          std::cout << "===== SquareError = " << SquareError << std::endl;
#endif
          BDF.setqfldPast(j, qfld);
        }
      }

      //start clock
      timer solution_time;

      ArrayQ Error2D = 0;

      for (int step = stepstart; step <= nsteps; step++)
      {
        // Advance one time setp at a time
        BDF.march(1);

        ArrayQ SquareError = 0;
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_Galerkin( errorIntegrand, SquareError ),
            xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        Error2D += SquareError*dt;
      }

      Real norm = Error2D;

      const int nDOFPDE = qfld.nDOF();
      const int nDOFBC  = lgfld.nDOF();

      const int nDOFtot = nDOFPDE + nDOFBC;

      hVec[indx] = sqrt(1./(nsteps*ii));
      normVec[indx] = sqrt( norm );

      Real slope = 0;
      if (indx > 0)
        slope = (log(normVec[indx])  - log(normVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));

#if 1
      cout << "P = " << order << " ii = " << ii << ": DOF = " << nDOFtot << ": nsteps = " << nsteps;
      //cout << " CPUTime = " << solution_time.elapsed() << " s";
      cout << " : L2 error = " << normVec[indx];
      if (indx > 0)
      {
        cout << "  (ratio = " << normVec[indx]/normVec[indx-1] << ")";
        cout << "  (slope = " << slope << ")";
      }
      cout << endl;
#endif

      pyriteFile << hVec[indx] << normVec[indx] << slope << std::endl;

      indx++;
      BOOST_CHECK_CLOSE( Tend, (Real)time, 1e-12 );

#if 0
      std::string filename = "tmp/slnCG_BDF_P";
      filename += to_string(order);
      filename += "_n";
      filename += to_string(ii);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
#endif
    }
  }

}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
