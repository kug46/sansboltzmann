// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_HDG_AD_BDF_btest
// testing of 1-D HDG with Unsteady Advection-Diffusion Timestepping

#undef SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <ctime>

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"

#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_1D_HDG_AD_BDF_test_suite )

//----------------------------------------------------------------------------//

BOOST_AUTO_TEST_CASE( Solve_HDG_AD_BDF )
{
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  typedef ScalarFunction1D_SineSineUnsteady SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCTypeFunctionLinearRobin_sansLG BCType;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef BDF<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> BDFClass;
  GlobalTime time(0);

  // PDE
  Real nu = 1;
  Real c = 0.1;

  AdvectiveFlux1D_Uniform adv( c );
  ViscousFlux1D_Uniform visc( nu );
  Source1D_UniformGrad source(0.1, 0.2);

  PyDict SineSineUnsteady;
  SineSineUnsteady[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.SineSineUnsteady;

  NDSolutionExact solnExact(time, SineSineUnsteady);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde(time, adv, visc, source, forcingptr );

  // BC
  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = SineSineUnsteady;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD1, ErrorClass> NDErrorClass;
  typedef IntegrandCell_HDG_Output<NDErrorClass> ErrorIntegrandClass;

  // integrands
  NDErrorClass fcnError(time, solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  PyDict NonLinearSolverDict;
  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);


  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_1D_HDG.plt", std::ios::out);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_HDG_AD_BDF_MinTest.txt", 1e-10, 5e-8, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  // set BDF and grid parameters for the run
  int BDFmin = 1;
  int BDFmax = 4;
  int iipowmin = 2;
  int iipowmax = 5;

  for (int BDForder = BDFmin; BDForder <= BDFmax; BDForder++)
  {
    int order = BDForder-1;

    Real hVec[10];
    Real hDOFVec[10];   // 1/sqrt(DOF)
    Real normVec[10];   // L2 error
    int indx = 0;

    for (int iipow = iipowmin; iipow <= iipowmax; iipow++)
    {
      int ii = pow(2,iipow); //grid size - using timesteps = grid size

      XField1D xfld( ii );

      Real Tend = 1.;
      int nsteps = ii;
      Real dt = Tend / (nsteps);

      // HDG solution field
      // cell solution
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      qfld = 0;

      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
      afld = 0;

      // interface solution
      Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
      qIfld = 0;

      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre,
                                                           BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups));
      lgfld = 0;


      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-10, 1e-10, 1e-10};
      PrimalEquationSetClass AlgEqSetSpace(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                           {0}, {0}, PyBCList, BCBoundaryGroups, time);

      // The BDF class
      BDFClass BDF( BDForder, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {0}, AlgEqSetSpace );

      // Initial condition

      // Set past solutions
      time = -dt;
      int stepstart = 0;
      for (int j = BDForder-1; j >= 0; j--)
      {
        time += dt;
        stepstart += 1;
        for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
#if 0
        ArrayQ SquareError = 0;
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_HDG( errorIntegrand, SquareError ),
            xfld, (qfld, afld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
        //BOOST_CHECK_SMALL(SquareError, 1e-12 );
        std::cout << "===== SquareError = " << SquareError << std::endl;
#endif
        // FOR DEBUGGING
        //std::cout << "qfld.DOF(0) " << qfld.DOF(0) << " time " << time << std::endl;
        BDF.setqfldPast(j, qfld);
      }

#if 0
      {
        std::string filename = "tmp/sln0HDG_BDF_P";
        filename += to_string(order);
        filename += "_n";
        filename += to_string(ii);
        filename += ".plt";
        cout << "calling output_Tecplot: filename = " << filename << endl;
        output_Tecplot( qfld, filename );
      }
#endif

      ArrayQ Error2D = 0;

      for (int step = stepstart; step <= nsteps; step++)
      {
        // Advance one time setp at a time
        BDF.march(1);

        // FOR DEBUGGING
        //std::cout << " dt " << dt << " qfld.DOF(0) " << qfld.DOF(0) << " time " << time << std::endl;

        ArrayQ SquareError = 0;
        IntegrateCellGroups<TopoD1>::integrate(
            FunctionalCell_HDG( errorIntegrand, SquareError ),
            xfld, (qfld, afld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

        Error2D += SquareError*dt;
      }

      Real norm = Error2D;

      const int nDOFPDE = qfld.nDOF();
      const int nDOFBC  = lgfld.nDOF();

      const int nDOFtot = nDOFPDE + nDOFBC;

      hVec[indx] = sqrt(1./(nsteps*ii));
      hDOFVec[indx] = sqrt(1./nDOFtot);
      normVec[indx] = sqrt( norm );

      Real slope = 0;
      if (indx > 0)
        slope = (log(normVec[indx])  - log(normVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));

#if 1
      cout << "P = " << order << " ii = " << ii << ": DOF = " << nDOFtot << ": nsteps = " << nsteps;
      cout << " : L2 error = " << normVec[indx];
      if (indx > 0)
      {
        cout << "  (ratio = " << normVec[indx]/normVec[indx-1] << ")";
        cout << "  (slope = " << slope << ")";
      }
      cout << endl;
#endif

      pyriteFile << hVec[indx] << normVec[indx] << std::endl;
      pyriteFile.close(1) << slope << std::endl;

      indx++;
      BOOST_CHECK_CLOSE( Tend, (Real)time, 1e-12 );
    } //grid refinement loop

    // Tecplot output
    resultFile << "ZONE T=\"DG BR2 P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;
    }
  } //order loop
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
