// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing of 1-D HDGAdvective with pure advection

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDGAdvective.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

namespace SANS
{
typedef PhysD1 PhysDim;
typedef TopoD1 TopoDim;

typedef AdvectiveFlux1D_Uniform AdvectiveFluxClass;
typedef ViscousFlux1D_None ViscousFluxClass;
typedef Source1D_Uniform SourceClass;
typedef PDEAdvectionDiffusion<PhysDim,AdvectiveFluxClass,ViscousFluxClass,SourceClass> PDEClass;
typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;

typedef ForcingFunction1D_MMS<PDEClass> ForcingClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

typedef ScalarFunction1D_Sine SolutionExactClass;
typedef SolnNDConvertSpace<PhysDim, SolutionExactClass> NDSolutionExactClass;

typedef BCTypeFunction_mitStateParam BCParamType_mitState;
typedef BCAdvectionDiffusionParams<PhysDim,BCParamType_mitState> BCParamsType_mitState;

typedef BCAdvectionDiffusion<PhysDim,BCTypeFunction_mitState<AdvectiveFluxClass,ViscousFluxClass>> BCClass;
typedef BCNDConvertSpace<PhysDim, BCClass> NDBCClass;
typedef NDVectorCategory<boost::mpl::vector1<NDBCClass>, NDBCClass::Category> NDBCVecCat;

typedef BCAdvectionDiffusion1DVector<AdvectiveFluxClass,ViscousFluxClass> BCVector;

typedef AlgebraicEquationSet_HDGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
          AlgEqSetTraits_Sparse, HDGAdv, XField<PhysDim, TopoDim>> AlgebraicEquationSetClass;
typedef typename AlgebraicEquationSetClass::SystemMatrix SystemMatrixClass;
typedef typename AlgebraicEquationSetClass::SystemNonZeroPattern SystemNonZeroPatternClass;
typedef typename AlgebraicEquationSetClass::SystemVector SystemVectorClass;
typedef typename AlgebraicEquationSetClass::BCParams BCParams;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_HDGAdvective_Advection_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_HDGAdvective_Advection_test )
{
  // PDE
  AdvectiveFluxClass adv( 1.1 );
  ViscousFluxClass visc;
  SourceClass source(1.0);

  PyDict scalarFunctionDict;
  //TODO: have to manually make sure that the Function.Name is consistent with SolutionExactClass
  scalarFunctionDict[BCParamsType_mitState::params.Function.Name] =
      BCParamsType_mitState::params.Function.Sine;
  scalarFunctionDict[SolutionExactClass::ParamsType::params.a] = 3.0;

  PyDict MMS_solnDict;
  MMS_solnDict[BCParamsType_mitState::params.Function] = scalarFunctionDict;

  const auto MMS_soln = ScalarFunction1DParams::getFunction(MMS_solnDict);

  std::shared_ptr<ForcingClass> forcingptr( new ForcingClass(*MMS_soln));

  NDPDEClass pde( adv, visc, source, forcingptr ); // solution as argument means a forcing function is generated

  // BC
  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCParamsType_mitState::params.Function] = scalarFunctionDict;
  BCSoln[BCParamsType_mitState::params.SolutionBCType] =
         BCParamsType_mitState::params.SolutionBCType.Dirichlet;
  BCSoln[BCParamsType_mitState::params.Upwind] = false;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCSoln;
  PyBCList["BCOut"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCIn"] = {0};
  BCBoundaryGroups["BCOut"] = {1};

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> cellGroups = {0};
  const std::vector<int> interiorTraceGroups = {0};
  const std::vector<int> boundaryTraceGroups = {0,1};

  // integrands for evaluating errors
  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExactClass> ErrorClass;
  typedef OutputNDConvertSpace<PhysDim, ErrorClass> NDErrorClass;
  typedef IntegrandCell_HDG_Output<NDErrorClass> ErrorIntegrandClass;

  NDSolutionExactClass solnExact(scalarFunctionDict);
  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass fcnErr( fcnError, cellGroups );

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_1D_HDGAdvective.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_HDGAdvective_Advection_FullTest.txt",
                                1e-10, 1e-7, pyrite_file_stream::check);
#else

  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_HDGAdvective_Advection_MinTest.txt",
                                1e-10, 1e-7, pyrite_file_stream::check);

#endif

  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  //----------------------------------------------------------------------------//
  // grid convergence study

  // loop over solution polynomial degrees
  const int polyDegMin = 1;
#ifdef SANS_FULLTEST

#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  const int polyDegMax = 5;

#else

  const int polyDegMax = 2;

#endif

  for (int polyDeg = polyDegMin; polyDeg <= polyDegMax; polyDeg++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    const int powermin = 2;
#ifdef SANS_FULLTEST
    const int powermax = ( polyDeg >= 6 ) ? 5 : 4;
#else
    const int powermax = 3;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      const int ii = pow(2, power);

      // grid:
      XField1D xfld( ii );

      // HDG solution fields
      Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld(xfld, polyDeg, BasisFunctionCategory_Legendre); // cell solution
      Field_DG_Trace<PhysDim, TopoDim, ArrayQ> qIfld(xfld, polyDeg, BasisFunctionCategory_Legendre); // interior trace solution
      qfld = 0.0;
      qIfld = 0.0;

      // Lagrange multiplier
      const std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> lgfld(xfld, polyDeg, BasisFunctionCategory_Legendre, LG_BGroup_list );

      // quadrature rule
      QuadratureOrder quadratureOrder(xfld, -1);

      const std::vector<Real> tol(AlgebraicEquationSetClass::nEqnSet, 1e-10);

      AlgebraicEquationSetClass AlgEqSet(xfld, qfld, qIfld, lgfld, pde, quadratureOrder, ResidualNorm_L2, tol,
                                         cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups );

      // residual vectors

      SystemVectorClass q(AlgEqSet.vectorStateSize());
      SystemVectorClass rsd(AlgEqSet.vectorEqSize());

      AlgEqSet.fillSystemVector(q);

#if defined(SANS_VERBOSE)
      {
        // dump jacobian
        SystemNonZeroPatternClass nz(AlgEqSet.matrixSize());
        AlgEqSet.jacobian(q,nz);
        SystemMatrixClass jac(nz);
        AlgEqSet.jacobian(q, jac);
        cout << "writing initial jacobian tmp/jac_initial.mtx" << endl;
        WriteMatrixMarketFile( jac, "tmp/jac_initial.mtx" );
      }
#endif

      rsd = 0.0;
      AlgEqSet.residual(q, rsd);

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(AlgEqSet);

      SystemVectorClass dq(q.size());

      solver.solve(rsd, dq);

      // updated solution
      q -= dq;

      AlgEqSet.setSolutionField(q);

      // check that the residual is zero
      rsd = 0.0;
      AlgEqSet.residual(q, rsd);
      std::vector<std::vector<Real>> rsdNorm = AlgEqSet.residualNorm(rsd);

      BOOST_REQUIRE( AlgEqSet.convergedResidual(rsdNorm) );

#if defined(SANS_VERBOSE)
      {
        // dump jacobian
        SystemNonZeroPatternClass nz(AlgEqSet.matrixSize());
        AlgEqSet.jacobian(q,nz);
        SystemMatrixClass jac(nz);
        AlgEqSet.jacobian(q, jac);
        cout << "writing initial jacobian tmp/jac_final.mtx" << endl;
        WriteMatrixMarketFile( jac, "tmp/jac_final.mtx" );
      }
#endif

      // L2 solution error

      ArrayQ SquareError = 0.0;
      typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;
      Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> afld(xfld, polyDeg, BasisFunctionCategory_Legendre); //TODO: dummy
      IntegrateCellGroups<TopoDim>::integrate(
          FunctionalCell_HDG( fcnErr, SquareError ), xfld, (qfld, afld),
          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError; // ArrayQ here is really just a scalar

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(qfld.nDOF());
      normVec[indx] = sqrt( norm );
      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << polyDeg << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
      {
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << "  (rate = " << (log(normVec[indx-1])  - log(normVec[indx-2])) /(log(hVec[indx-1]) - log(hVec[indx-2])) << ")";
      }
      cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/slnHGD_P";
      filename += to_string(polyDeg);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, afldArea, &solnExact, filename, polyDeg-1 );
#endif

    }

    // Tecplot output
    resultFile << "ZONE T=\"HDG P=" << polyDeg << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n];
      pyriteFile << slope << std::endl;
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
