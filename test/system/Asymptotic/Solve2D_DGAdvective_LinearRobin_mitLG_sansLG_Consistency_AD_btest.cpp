// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGAdvective_LinearRobin_mitLG_sansLG_Consistency_AD_btest
// testing of 2-D DG with Advection-Diffusion

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
//#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGAdvective_LinearRobin_mitLG_sansLG_Consistency_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGAdvective_LinearRobin_mitLG_sansLG_Consistency_AD )
{
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEAdvectionDiffusion2D;

  typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetClass;

  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // PDE
  AdvectiveFlux2D_Uniform adv(0.5, 0.5);

  ViscousFlux2D_None visc;

  Source2D_None source;
  typedef ForcingFunction2D_Const<PDEAdvectionDiffusion2D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr(new ForcingType(1));

  NDPDEClass pde( adv, visc, source, forcingptr );

  const Real uB = 0.1;

  // BCs

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCDirichlet_mitLG;
  BCDirichlet_mitLG[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_mitLG;
  BCDirichlet_mitLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.A] = 1;
  BCDirichlet_mitLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.B] = 0;
  BCDirichlet_mitLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params.bcdata] = uB;

  PyDict BCDirichlet_sansLG;
  BCDirichlet_sansLG[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichlet_sansLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCDirichlet_sansLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCDirichlet_sansLG[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = uB;


  PyDict PyBCList_mitLG;
  PyDict PyBCList_sansLG;
  PyBCList_mitLG["Dirichlet"] = BCDirichlet_mitLG;
  PyBCList_mitLG["None"] = BCNone;

  PyBCList_sansLG["Dirichlet"] = BCDirichlet_sansLG;
  PyBCList_sansLG["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["Dirichlet"] = {0,3}; //Bottom and left boundaries
  BCBoundaryGroups["None"] = {1,2}; //Right and top boundaries

  //Check the BC dictionaries
  BCParams::checkInputs(PyBCList_mitLG);
  BCParams::checkInputs(PyBCList_sansLG);

  int order = 2;

  // grid:
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld( ii, jj, 0, 1, 0, 1, true );
//  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj, 0, 1, 0, 1);

  // solution: Hierarchical, C0
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_mitLG(xfld, order, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_sansLG(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld_mitLG = 0.0;
  qfld_sansLG = 0.0;
  const int nDOFPDE = qfld_mitLG.nDOF();

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 0
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_mitLG( xfld, order, BasisFunctionCategory_Hierarchical,
                                                              BCParams::getLGBoundaryGroups(PyBCList_mitLG, BCBoundaryGroups) );
#elif 0
  std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_mitLG( xfld, order, BasisFunctionCategory_Hierarchical,
                                                                          BCParams::getLGBoundaryGroups(PyBCList_mitLG, BCBoundaryGroups) );
#else
  std::vector<int> active_BGroup_list_mitLG = BCParams::getLGBoundaryGroups(PyBCList_mitLG, BCBoundaryGroups);
  std::vector<int> active_BGroup_list_sansLG = BCParams::getLGBoundaryGroups(PyBCList_sansLG, BCBoundaryGroups);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_mitLG(xfld, order, BasisFunctionCategory_Legendre, active_BGroup_list_mitLG);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_sansLG(xfld, order, BasisFunctionCategory_Legendre, active_BGroup_list_sansLG);
#endif

  lgfld_mitLG = 0.0;
  lgfld_sansLG = 0.0;
  const int nDOFBC = lgfld_mitLG.nDOF();

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  //--------PRIMAL SOLVE------------

  PrimalEquationSetClass PrimalEqSet_mitLG(xfld, qfld_mitLG, lgfld_mitLG, pde, quadratureOrder,
                                           ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList_mitLG, BCBoundaryGroups);
  PrimalEquationSetClass PrimalEqSet_sansLG(xfld, qfld_sansLG, lgfld_sansLG, pde, quadratureOrder,
                                            ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList_sansLG, BCBoundaryGroups);

  // residual
  SystemVectorClass sln_mitLG(PrimalEqSet_mitLG.vectorStateSize());
  SystemVectorClass sln_sansLG(PrimalEqSet_sansLG.vectorStateSize());

  SystemVectorClass rsd_mitLG(PrimalEqSet_mitLG.vectorEqSize());
  SystemVectorClass rsd_sansLG(PrimalEqSet_sansLG.vectorEqSize());

  PrimalEqSet_mitLG.fillSystemVector(sln_mitLG);
  PrimalEqSet_sansLG.fillSystemVector(sln_sansLG);

  rsd_mitLG = 0;
  rsd_sansLG = 0;
  PrimalEqSet_mitLG.residual(sln_mitLG, rsd_mitLG);
  PrimalEqSet_sansLG.residual(sln_sansLG, rsd_sansLG);

#if 0
      fstream fout( "tmp/jac.mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac_sansLG, fout );
//      fstream fout00( "tmp/jac00.mtx", fstream::out );
//      cout << "btest: global jac00" << endl;  WriteMatrixMarketFile( jac(0,0), fout00 );
//      fstream fout10( "tmp/jac10.mtx", fstream::out );
//      cout << "btest: global jac10" << endl;  WriteMatrixMarketFile( jac(1,0), fout10 );
//      fstream fout01( "tmp/jac01.mtx", fstream::out );
//      cout << "btest: global jac01" << endl;  WriteMatrixMarketFile( jac(0,1), fout01 );
#endif

  // solve
  SLA::UMFPACK<SystemMatrixClass> solver_mitLG(PrimalEqSet_mitLG);
  SLA::UMFPACK<SystemMatrixClass> solver_sansLG(PrimalEqSet_sansLG);

  SystemVectorClass dsln_mitLG(sln_mitLG.size());
  SystemVectorClass dsln_sansLG(sln_sansLG.size());
  solver_mitLG.solve(rsd_mitLG, dsln_mitLG);
  solver_sansLG.solve(rsd_sansLG, dsln_sansLG);

  // update solutions
  sln_mitLG -= dsln_mitLG;
  sln_sansLG -= dsln_sansLG;
  PrimalEqSet_mitLG.setSolutionField(sln_mitLG);
  PrimalEqSet_sansLG.setSolutionField(sln_sansLG);

  Real check_tol = 5e-10;

  //Check if both solutions are equal
  for (int k = 0; k < nDOFPDE; k++)
    SANS_CHECK_CLOSE( sln_mitLG[0][k], sln_sansLG[0][k], check_tol, check_tol );

  // check that the residuals is zero
  rsd_mitLG = 0;
  rsd_sansLG = 0;
  PrimalEqSet_mitLG.residual(sln_mitLG, rsd_mitLG);
  PrimalEqSet_sansLG.residual(sln_sansLG, rsd_sansLG);

  Real rsdPDEnrm_mitLG = 0;
  Real rsdPDEnrm_sansLG = 0;
  for (int n = 0; n < nDOFPDE; n++)
  {
    rsdPDEnrm_mitLG  += pow(rsd_mitLG[0][n],2);
    rsdPDEnrm_sansLG += pow(rsd_sansLG[0][n],2);
  }

  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm_mitLG) , 1e-12 );
  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm_sansLG), 1e-12 );

  Real rsdBCnrm_mitLG = 0;
  for (int n = 0; n < nDOFBC; n++)
    rsdBCnrm_mitLG += pow(rsd_mitLG[1][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdBCnrm_mitLG), 1e-12 );

#if 0
  // Tecplot dump
  string filename = "tmp/slnDGAdv_AD_P";
  filename += to_string(order);
  filename += "_";
  filename += to_string(ii);
  filename += "x";
  filename += to_string(jj);
  filename += ".plt";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot( qfld_mitLG, filename );
#endif

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
