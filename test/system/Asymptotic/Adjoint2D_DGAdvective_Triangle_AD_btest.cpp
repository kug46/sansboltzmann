// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adjoint2D_DGAdvective_AD_btest
// testing of 2-D DG adjoint with advection-diffusion

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/OutputCell_SolutionSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Adjoint2D_DGAdvective_Triangle_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Adjoint2D_DGAdvective_Triangle_AD )
{

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_None,
                                Source2D_None > PDEClass;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef OutputCell_SolutionSquared<PDEClass> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDOutputClass> OutputIntegrandClass;

  // PDE
  AdvectiveFlux2D_Uniform adv( 1.0, 1.0 );

  ViscousFlux2D_None visc;

  Source2D_None source;

  NDPDEClass pde(adv, visc, source);

  // BCs

  // Create a BC dictionary
  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict BCDirichletL;
  BCDirichletL[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0.0;
  BCDirichletL[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 1.0;

  PyDict BCDirichletR;
  BCDirichletR[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCDirichletR[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.A] = 1.0;
  BCDirichletR[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.B] = 0.0;
  BCDirichletR[BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params.bcdata] = 0.5;

  PyDict PyBCList;
  PyBCList["DirichletL"] = BCDirichletL;
  PyBCList["DirichletR"] = BCDirichletR;
  PyBCList["None"] = BCNone;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["DirichletL"] = {3}; //Left boundary
  BCBoundaryGroups["DirichletR"] = {0}; //Bottom and right boundary
  BCBoundaryGroups["None"] = {1,2}; //Top boundary

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  NDOutputClass fcnOutput;
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  pyrite_file_stream pyriteFile("IO/ErrorOrder/Adjoint2D_DGAdvective_Triangle_AD_SolnTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);

  int order = 1;

  int ii = 4;
  int jj = ii;

  // grid:
  XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj);

  // solution
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_adj(xfld, order, BasisFunctionCategory_Legendre);
  qfld = 0.0;
  qfld_adj = 0.0;
//  output_Tecplot( qfld, "tmp/qfld_init.plt" );
  const int nDOFPDE = qfld.nDOF();

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 0
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                        BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups)  );
#elif 0
  std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical,
                                                                    BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#else
  std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre,active_BGroup_list);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_adj(xfld, order, BasisFunctionCategory_Legendre,active_BGroup_list);
//  std::cout<<active_BGroup_list[0]<<","<<active_BGroup_list[1]<<","<<active_BGroup_list[2]<<std::endl;
#endif

  lgfld = 0.0;
  lgfld_adj = 0.0;
  const int nDOFBC = lgfld_adj.nDOF();

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  //--------PRIMAL SOLVE------------

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                     {0}, {0,1,2}, PyBCList, BCBoundaryGroups);

  // primal solution vector
  SystemVectorClass q(PrimalEqSet.vectorStateSize());
  PrimalEqSet.fillSystemVector(q);

  // residual
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
  rsd = 0.0;
  PrimalEqSet.residual(q, rsd);

  // primal solve
  SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
  SystemVectorClass dq(PrimalEqSet.vectorStateSize());

  solver.solve(rsd, dq);

  // update primal solution
  q -= dq;
  PrimalEqSet.setSolutionField(q);


  // check that the residual is zero
  rsd = 0;
  PrimalEqSet.residual(q, rsd);

  Real rsdPDEnrm = 0;
  for (int n = 0; n < nDOFPDE; n++)
    rsdPDEnrm += pow(rsd[0][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

  Real rsdBCnrm = 0;
  for (int n = 0; n < nDOFBC; n++)
    rsdBCnrm += pow(rsd[1][n],2);

  BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

//      cout << sqrt(rsdPDEnrm) << "\t" << sqrt(rsdBCnrm) <<endl;


  //--------ADJOINT SOLVE------------

  PrimalEquationSetClass AdjointEqSet(xfld, qfld_adj, lgfld_adj, pde, quadratureOrder, ResidualNorm_Default, tol,
                                      {0}, {0,1,2}, PyBCList, BCBoundaryGroups);
  AdjointEqSet.setSolutionField(q);

  // adjoint (right hand side)
  SystemVectorClass rhs(AdjointEqSet.vectorEqSize());
  rhs = 0.0;

  IntegrateCellGroups<TopoD2>::integrate(
      JacobianFunctionalCell_Galerkin( outputIntegrand, rhs(0) ),
      xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  // adjoint solve
  SystemVectorClass adj_sln(PrimalEqSet.vectorStateSize());
  SLA::UMFPACK<SystemMatrixClass> solverAdj(AdjointEqSet, SLA::TransposeSolve);
  solverAdj.solve(rhs, adj_sln);

  AdjointEqSet.setSolutionField(adj_sln);

  for (int i = 0; i < adj_sln[0].m(); i++)
    pyriteFile << adj_sln[0][i] << std::endl;

#if 0
  // Tecplot dump
  string filename = "tmp/adjoint2D_DGAdv_AD_P";
  filename += to_string(order);
  filename += "_";
  filename += to_string(ii);
  filename += "x";
  filename += to_string(jj);
  filename += ".plt";
  cout << "calling output_Tecplot: filename = " << filename << endl;
  output_Tecplot( qfld_adj, filename );
  output_Tecplot( qfld, "tmp/primal.plt" );
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
