// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_2D_DGAdvective_Circle_Line_btest
// testing of 2-D DG Advection on a circle (or part of circle) of lines

//#define SANS_FULLTEST
//#define SANS_VERBOSE
//#define RESIDUAL_TEST  // test residual convergence for projections of the exact solution

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_Xq_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

namespace SANS
{
// ---------- Define type/class names ---------- //
// Exact solution
//  typedef ScalarFunction2D_SineTheta SolutionExact;
typedef ScalarFunction2D_ThetaGeometricSeries SolutionExact;

typedef SolnNDConvertSpace<PhysD2, SolutionExact> SolutionNDClass;
// Fluxes
typedef AdvectiveFlux2D_ConstRotation AdvectiveFluxClass;
typedef ViscousFlux2D_None ViscousFluxClass;
// Source
typedef Source2D_None SourceClass;
// PDE class
typedef PDEAdvectionDiffusion<PhysD2,
    AdvectiveFluxClass,
    ViscousFluxClass,
    SourceClass > PDEClass;
typedef PDENDConvertSpace< PhysD2, PDEClass > NDPDEClass;
typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
// BC
typedef BCAdvectionDiffusion2DVector<AdvectiveFluxClass, ViscousFluxClass> BCVector;
// Primal equation set
typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
    AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD1>> PrimalEquationSetClass;
typedef PrimalEquationSetClass::BCParams BCParams;
typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

// Solution square error
typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

// Forcing function
//  typedef ForcingFunction2D_SineTheta ForcingFcnClass;
typedef ForcingFunction2D_ThetaGeometricSeries<PDEClass> ForcingFcnClass;
}

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGAdvective_Circle_Line_test_suite )

BOOST_AUTO_TEST_CASE( Solve2D_DGAdvective_Circle_Line_test )
{
  // ---------- Set problem parameters ---------- //
  int gridOrder = 6;

  // PDE
  const Real digitDhDR1 = 0;
  const Real dhdR1 = pow( 10, digitDhDR1);  // h/R_1
  const Real digitR1 = 0;                   // R_1 = 10^digitR1
  const Real R1 = pow( 10, digitR1), R2 = R1 * (1 + dhdR1);
  const Real V = 1.0;  // uniform advection speed

  const int solnMonomialP = 10;    // manufactured solution geometric series in theta up to order p

  AdvectiveFluxClass adv( V );

  ViscousFluxClass visc;

  SourceClass source;

//  ForcingFcnClass forcing( V*(R2-R1)/R1);
  std::shared_ptr<ForcingFcnClass> forcingptr( new ForcingFcnClass( V*(R2-R1)/R1, solnMonomialP ) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  Real thetamin = 0;   // unit: degree angle
  Real thetamax = 45;  // unit: degree angle

  // ---------- Create parameter dictionaries ---------- //
  // Exact solution
  PyDict solnArgs;
  solnArgs[SolutionExact::ParamsType::params.c] = R2 - R1;        // exact solution: (R_2 - R_1)*theta^p
  solnArgs[SolutionExact::ParamsType::params.p] = solnMonomialP;  // exact solution: (R_2 - R_1)*theta^solnMonomialP
  SolutionNDClass solnExact( solnArgs );

  // BC
  PyDict ThetaGeometricSeries;
  ThetaGeometricSeries[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.Function.ThetaGeometricSeries;
  ThetaGeometricSeries[SolutionExact::ParamsType::params.c] = R2 - R1;
  ThetaGeometricSeries[SolutionExact::ParamsType::params.p] = solnMonomialP;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.Function] = ThetaGeometricSeries;

  PyDict PyBCList;
  PyBCList["BCSoln"] = BCSoln;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Define the BoundaryGroups for each boundary condition
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["BCSoln"] = {0,1};

  // ---------- Miscellaneous setup before runs ---------- //
  // Solution square error class
  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // norm data
  const int nrmDatSize = 10;  // size of norm data containers
  Real hVec[nrmDatSize];     // 1/nElem
  Real hDOFVec[nrmDatSize];  // 1/sqrt(DOF)
  Real normVec[nrmDatSize];  // L2 error
  int indx;                  // index of norm data entries

#ifdef RESIDUAL_TEST
  Real normVecRsd[nrmDatSize];  // PDE residual
#endif

  // Tecplot & pyrite output
#ifdef SANS_FULLTEST
#ifdef RESIDUAL_TEST
  // Tecplot
  string resultFilename = "tmp/L2_2D_DGAdvective_Circle_Line";
  resultFilename += "_Q";
  resultFilename += stringify(gridOrder);
  resultFilename += ".plt";
  cout << "calling resultFilename = " << resultFilename << endl;
  std::ofstream resultFile(resultFilename, std::ios::out);
#else
  std::stringstream resultFile;
#endif
  // Pyrite
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_DGAdvective_Circle_Line_AD_FullTest.txt",
                                1e-10, 1e-4, pyrite_file_stream::check);
  // Q = 6, exactSolnOrder = 10, tested up to p = 4
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_DGAdvective_Circle_Line_AD_MinTest.txt",
                                1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
#ifdef RESIDUAL_TEST
  resultFile << ", \"PDE residual\"";
  resultFile << ", \"PDE residual rate\"";
#endif
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  // Perform grid convergence tests for various solution orders
  int ordermin = 0;  // minimum solution/polynomial order
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 4;  // maximum solution/polynomial order of a full test
#else
  int ordermax = ordermin;
#endif
  // ---------- Run across solution orders & Grid refinements ---------- //
  // loop over solution order: p = order
  for ( int order = ordermin; order <= ordermax; order++ )
  {
    indx = 0;  // (re)initialized for each solution order loop

    int ii;  // number of elements
    int powermin = 2;
#ifdef SANS_FULLTEST
    int powermax = 6;
#else
    int powermax = powermin;
#endif
    // loop over grid refinement: 2^power
    for ( int power = powermin; power <= powermax; power++ )
    {
      ii = pow( 2, power );

      // grid:
      // Q1 linear grid
      std::vector<DLA::VectorS<2,Real>> coordinates_X1(ii+1);
      Real theta;
      for (int i = 0; i < ii+1; i++)
      {
        theta = ( (thetamax - thetamin) * PI/180.0 ) * ( static_cast<Real>(i) / static_cast<Real>(ii) );
        coordinates_X1[i] = { R1*cos(theta), R1*sin(theta) };
      }
      XField2D_Line_X1_1Group xfld_X1( coordinates_X1 );

#if 0
      gridOrder = 1;
      XField2D_Line_X1_1Group xfld( coordinates_X1 );
#else
      // higher-order Q grid
      // Compute coordinates
      const int nDOF = ii*gridOrder+1;
      std::vector<DLA::VectorS<2,Real>> coordinates(nDOF);
      for (int iDOF = 0; iDOF < nDOF; iDOF++ )
      {
        theta = ( (thetamax - thetamin) * PI/180.0 ) * ( static_cast<Real>(iDOF) / static_cast<Real>(nDOF-1) );
        coordinates[iDOF] = { R1*cos(theta), R1*sin(theta) };
      }
      XField2D_Line_Xq_1Group xfld( xfld_X1, gridOrder, coordinates);
#endif

#if 0
      output_Tecplot(xfld, "tmp/CircleLine_grid.dat");  // Tecplot dump grid
#endif

      // solution: Legendre
      Field_DG_Cell<PhysD2, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
//      qfld = 0;  // initial solution
      // use the project of exact solution as initial solution
      for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Legendre
      Field_DG_BoundaryTrace<PhysD2, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre );
      lgfld = 0;
      const int nDOFBC = lgfld.nDOF();

      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-12, 1e-12};

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups );

      // residual
      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      PrimalEqSet.fillSystemVector(q);
      rsd = 0;

      PrimalEqSet.residual(q, rsd);
#ifdef RESIDUAL_TEST
      Real rsdPDEnrmIS = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrmIS += pow(rsd[0][n],2);

      normVecRsd[indx] = sqrt(rsdPDEnrmIS);
#endif

      // Print residual vectors
#if 0
      std::cout <<endl << "Solution:" <<endl;
      for (int k = 0; k < nDOFPDE; k++) std::cout << k << "\t" << rsd[0][k] << std::endl;

      std::cout <<endl << "BC:" <<endl;
      for (int k = 0; k < nDOFBC; k++) std::cout << k << "\t" << rsd[1][k] << std::endl;
#endif

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());

      solver.solve(rsd, dq);

      // update solution
      q -= dq;
      PrimalEqSet.setSolutionField(q);

      // check that the residual is zero
      rsd = 0;
      PrimalEqSet.residual(q, rsd);

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdBCnrm),  1e-12 );

//      cout << sqrt(rsdPDEnrm) << "\t" << sqrt(rsdBCnrm) <<endl;

      // L2 solution error
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorIntegrand, SquareError ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

      // print L2 error & convergence rate
#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
        cout << "  (convergence rate = " << (log(normVec[indx-1])  - log(normVec[indx-2])) /(log(hVec[indx-1]) - log(hVec[indx-2]))
             << ")";
      cout << endl;
#endif

      // Tecplot dump
#if 0
      string filename = "tmp/slnDG_CircleLine_P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
#endif
    } // end: grid refinement loop

    // Tecplot output
    resultFile << "ZONE T=\"DG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) / (log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
#ifdef RESIDUAL_TEST
      Real slopeRsd = 0;
      if (n > 0)
        slopeRsd = (log(normVecRsd[n])  - log(normVecRsd[n-1])) / (log(hVec[n]) - log(hVec[n-1]));
      resultFile << ", " << normVecRsd[n];
      resultFile << ", " << slopeRsd;
#endif
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n];
      pyriteFile.close(1e-2) << slope << std::endl;
    }

    // Mathematica dump
#if 0
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
    // Mathematica dump (cut & paste)
#if 0
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  } // end: solution order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
