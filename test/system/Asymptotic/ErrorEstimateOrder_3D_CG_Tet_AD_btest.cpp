// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateOrder_3D_CG_Tet_AD_btest
// testing error estimation of 3-D CG with Advection-Diffusion on triangles

//#define SANS_FULLTEST // Line 290 for Pyrites

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "tools/KahanSum.h"
#include "pyrite_fstream.h"

#include "Surreal/SurrealS.h"

//PDE
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/ForcingFunction3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/ForcingFunction3D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

//Solve includes
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/Function/WeightedFunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"


// Estimate includes
#include "Discretization/Galerkin/IntegrandCell_Galerkin_StrongForm.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_StrongForm.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"

#include "ErrorEstimate/Galerkin/ErrorEstimate_StrongForm_Galerkin.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/Tuple/FieldTuple.h"

//Linear algebra
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateOrder_3D_CG_Tet_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateOrder_3D_CG_Tet_AD )
{
#if 0
  mpi::communicator world;

  timer totaltime;

  typedef ScalarFunction3D_Monomial SolutionExact;
  typedef ScalarFunction3D_Monomial AdjointExact;

  // ND Exacts
  typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD3, AdjointExact>  NDAdjointExact;

  // Primal PDE
  typedef PDEAdvectionDiffusion<PhysD3,
                                 AdvectiveFlux3D_Uniform,
                                 ViscousFlux3D_Uniform,
                                 Source3D_UniformGrad > PDEClass;
   typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;

   typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

   // typedef BCTypeFunction_mitStateParam BCType;
   // typedef BCNDConvertSpace<PhysD3,  BCAdvectionDiffusion<PhysD2,BCTypeFunction_mitStateParam> > NDBCClass;

   typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

   typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> PrimalEquationSetClass;
   typedef PrimalEquationSetClass::BCParams BCParams;

   // Adjoint PDE
   typedef PDEAdvectionDiffusion<PhysD3,
                                 AdvectiveFlux3D_Uniform,
                                 ViscousFlux3D_Uniform,
                                 Source3D_UniformGrad > PDEAdjoint3D;
   typedef PDENDConvertSpace<PhysD3, PDEAdjoint3D> PDEAdjointClass;

  // Integrand Functors
  // call this class with something of Adjoint Exact Class
  typedef ScalarFunction3D_ForcingFunction<PDEAdjointClass> WeightFunctional;
  typedef SolnNDConvertSpace<PhysD3, WeightFunctional>    NDWeightFunctional;

  typedef OutputCell_WeightedSolution<PDEClass, WeightFunctional> WeightOutputClass;
  typedef OutputNDConvertSpace<PhysD3, WeightOutputClass>        NDWeightOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDWeightOutputClass>      IntegrandClass;

  // Estimation
  typedef ErrorEstimate_StrongForm_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector, XField<PhysD3,TopoD3>> ErrorEstimateClass;

  // PDE
  Real u= 0.0;
  Real v= 0.0;
  Real w= 0.0;
  AdvectiveFlux3D_Uniform adv( u, v, w );

  Real nu = 1;
  ViscousFlux3D_Uniform visc( nu,  0.0, 0.0,
                              0.0, nu,  0.0,
                              0.0, 0.0, nu );

  Source3D_UniformGrad source( 0.0, 0.0, 0.0, 0.0 );

  Real a[3]= {1, 2, 3};

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD3,BCTypeFunctionLinearRobin_mitLG>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD3,BCTypeFunctionLinearRobin_mitLG>::params.Function.Monomial;
  solnArgs[SolutionExact::ParamsType::params.i] = a[0];
  solnArgs[SolutionExact::ParamsType::params.j] = a[1];
  solnArgs[SolutionExact::ParamsType::params.k] = a[2];

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction3D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );
  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict BC_Dirichlet_Soln;

  BC_Dirichlet_Soln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD3,BCTypeFunctionLinearRobin_mitLG>::params.Function] = solnArgs;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD3,BCTypeFunctionLinearRobin_mitLG>::params.A] = 1;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD3,BCTypeFunctionLinearRobin_mitLG>::params.B] = 0;

  // BC_Dirichlet_Soln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  // BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  // BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
  //                   BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  // BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD3,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCDirichlet_Soln"] = BC_Dirichlet_Soln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDirichlet_Soln"] = {0,1,2,3};

  // check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

   std::cout<< "LG_BGroup_list = ";
   for (auto it = LG_BGroup_list.begin(); it != LG_BGroup_list.end(); ++it)
     std::cout<< *it;
   std::cout<< std::endl;

  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(solnArgs) );
  // NDBCClass bc( solnExactPtr, visc );

  // Adjoint PDE

  AdvectiveFlux3D_Uniform adv_adj( -u, -v, -w );

  Real a_adj[3]= {4, 5, 6};
  NDAdjointExact adjExact(a_adj[0], a_adj[1], a_adj[2]);

  typedef ForcingFunction3D_MMS<PDEAdjoint3D> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact));
  PDEAdjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  NDWeightFunctional volumeFcn( pdeadj );

  // volumeFcn is g in (g,u) g = - Del psi
  NDWeightOutputClass weightOutput( volumeFcn );
  IntegrandClass outputFcn( weightOutput, {0} );

  // linear system setup
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // norm data
  const int vsize = 10;
  Real hVec[vsize];
  Real globalErrorVec[vsize], globalEstVec[vsize], globalEstErrVec[vsize], globalEffVec[vsize];
  Real localErrorVec[vsize], localEstVec[vsize], localEstErrVec[vsize], localEffVec[vsize];
  Real globalErrorRate[vsize], globalEstRate[vsize], globalEstErrRate[vsize], globalEffRate[vsize];
  Real localErrorRate[vsize], localEstRate[vsize], localEstErrRate[vsize], localEffRate[vsize];
  int indx;

#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/ErrorEst3D_CG.plt", std::ios::out);
  std::ofstream datFile("tmp/ErrorEst3D_CG.dat", std::ios::out);
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_3D_CG_AD_FullTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-9, 8e-2, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  std::stringstream datFile;
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_3D_CG_AD_MinTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-9, 5e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"Global Error\"";
  resultFile << ", \"Global Error Estimate\"";
  resultFile << ", \"Global Error Estimate Error\"";
  resultFile << ", \"Global 1-Effectivity\"";
  resultFile << ", \"Global Error Rate\"";
  resultFile << ", \"Global Error Estimate Rate\"";
  resultFile << ", \"Global Error Estimate Error Rate\"";
  resultFile << ", \"Global 1-Effectivity Rate\"";
  resultFile << ", \"Local Error\"";
  resultFile << ", \"Local Error Estimate\"";
  resultFile << ", \"Local Error Estimate Error\"";
  resultFile << ", \"Local 1-Effectivity\"";
  resultFile << ", \"Local Error Rate\"";
  resultFile << ", \"Local Error Estimate Rate\"";
  resultFile << ", \"Local Error Estimate Error Rate\"";
  resultFile << ", \"Local 1-Effectivity Rate\"";
  resultFile << endl;

  // For Latex Output
  datFile << "order,"
      "h,"
      "globalError,"
      "globalErrorEstimate,"
      "globalErrorEstimateError,"
      "globalEffectivity,"
      "globalErrorRate,"
      "globalErrorEstimateRate,"
      "globalErrorEstimateErrorRate,"
      "globalEffectivityRate,"
      "localError,"
      "localErrorEstimate,"
      "localErrorEstimateError,"
      "localEffectivity,"
      "localErrorRate,"
      "localErrorEstimateRate,"
      "localErrorEstimateErrorRate,"
      "localEffectivityRate";
  datFile << endl;

  //int quadorder=-1;
  //ElementIntegral <TopoD2, Triangle, ArrayQ> ElemIntegral(quadorder);

//---------------------------------------------------------------------------//
  // Computer the exact output on a fine grid and high-quadrature
  Real globalExt=0;

  {
    // XField3D_Box_Tet_X1 xfld(world, 5, 5, 5);
    XField3D_Box_Tet_X1 xfld(world, 50, 50, 50);

    for_each_CellGroup<TopoD3>::apply( WeightedFunctionIntegral(volumeFcn, solnExact, globalExt, {0}), xfld );
    std::cout << "globalExt = " << std::scientific << std::setprecision(16) << globalExt << std::endl;
  }
//---------------------------------------------------------------------------//

  const int orderinc = 1;

#define CG_TRACE

  int ordermin = 1;
#ifndef SANS_FULLTEST
  // ordermax = 3 is used for the pyrite
  int ordermax = 2;
#else
  // ordermax = 3 is used for the pyrite
  int ordermax = 2;
#endif

  for (int order = ordermin; order <= ordermax; order++)
  {
    // Stabilization
    const StabilizationMatrix stab(StabilizationType::Unstabilized, TauType::Glasby, order + 1);

    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj, kk;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 4;
    cout << "...running full test" << endl;
#else
    int powermax = 3;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      //ii = pow( 2, power );
#ifndef SANS_FULLTEST
      //ii = 2 is used for the pyrite
      ii = 2;
#else
      // ii = 4*power + 4 is used for the pyrite
      ii = 4*power+4;
#endif
      //ii = 2;
      jj = ii;
      kk = jj;

      cout << std::scientific << std::setprecision(7);
      // grid: Hierarchical P1 ( aka X1)

      XField3D_Box_Tet_X1 xfld(world, ii, jj, kk);

      // Primal Solution in p
      Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Lagrange);
#ifdef CG_TRACE // see line 292
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( {LG_BGroup_list}, xfld, order, BasisFunctionCategory_Lagrange );
#else
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
#endif
      qfld = 0; lgfld = 0;

      // quadrature rule
      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-11, 1e-11};

      // create the primal equations
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, stab, quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );

      // residual

      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

      PrimalEqSet.fillSystemVector(q);

      if (world.rank() == 0)
      {
        printf("\n");
        printf("order: %d, power: %d.\n", order, power);
        printf("\n");
      }

      rsd = 0;
      PrimalEqSet.residual(q, rsd);

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());
      solver.solve(rsd, dq);

      // update solution
      q -= dq;

      PrimalEqSet.setSolutionField(q);

//      output_Tecplot( qfld, "tmp/qfld_tmp.plt");

      // Adjoint in p
      Field_CG_Cell<PhysD3, TopoD3, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Lagrange);
#ifdef CG_TRACE // see line 292
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> mufld( {LG_BGroup_list}, xfld, order, BasisFunctionCategory_Lagrange );
#else
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> mufld( xfld, order-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
#endif
      int nDOFPDE = wfld.nDOF(), nDOFBC = mufld.nDOF();

      // adjoint solve
      SystemVectorClass adj(q.size());
      rsd = 0;

      IntegrateCellGroups<TopoD3>::integrate(
          JacobianFunctionalCell_Galerkin( outputFcn, rsd(0) ),
          xfld, wfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint in p
      SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);
      solverAdj.solve(rsd, adj);

      // update the adjoint solution
      for (int k = 0; k < nDOFPDE; k++)
        wfld.DOF(k) = adj[0][k];
      for (int k = 0; k < nDOFBC; k++)
        mufld.DOF(k) = adj[1][k];

      // ---------------------
      // Adjoint error estimate
      // Adjoint in p+1
      // ---------------------

      // GLOBAL ESTIMATE
      // This is currently the old hack, just to perform the Global Estimate for verification of the local sum
      // prolongated primal to p+1
      Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfldPp1(xfld, order+orderinc, BasisFunctionCategory_Lagrange);
#ifdef CG_TRACE // see line 292
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfldPp1( {LG_BGroup_list}, xfld, order+orderinc, BasisFunctionCategory_Lagrange );
#else
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfldPp1( xfld, order+orderinc-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
#endif
      const int nDOFPDEPp1 = qfldPp1.nDOF(), nDOFBCPp1 = lgfldPp1.nDOF();

      // prolongate the solution
      qfld.projectTo(qfldPp1);
      lgfld.projectTo(lgfldPp1);

      PrimalEquationSetClass PrimalEqSetPp1(xfld, qfldPp1, lgfldPp1, pde, stab, quadratureOrder, ResidualNorm_L2,
                                            tol, {0}, PyBCList, BCBoundaryGroups );

      // residual field
      SystemVectorClass qPp1(PrimalEqSetPp1.vectorStateSize());
      SystemVectorClass rsdWeakPp1(PrimalEqSetPp1.vectorEqSize());
      rsdWeakPp1 =0;
      PrimalEqSetPp1.fillSystemVector(qPp1);

      // evaluate the weak form residual in p+1 - For validation of the sum of the local
      PrimalEqSetPp1.residual(qPp1, rsdWeakPp1);

      // LOCAL ESTIMATE

      // Adjoint in p+1 solution field
      Field_CG_Cell<PhysD3, TopoD3, ArrayQ> wfldPp1(xfld, order+orderinc, BasisFunctionCategory_Lagrange);

      Field_CG_Cell<PhysD3, TopoD3, ArrayQ> wfldProlongateP(xfld, order+orderinc, BasisFunctionCategory_Lagrange); // for the prolongated p
#ifdef CG_TRACE // see line 292
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> mufldPp1( {LG_BGroup_list}, xfld, order+orderinc, BasisFunctionCategory_Lagrange );
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> mufldProlongateP( {LG_BGroup_list}, xfld,
                             order+orderinc, BasisFunctionCategory_Lagrange );
#else
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> mufldPp1( xfld, order+orderinc-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> mufldProlongateP( xfld, order+orderinc-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
#endif

      wfld.projectTo(wfldProlongateP); // prolongate the p adjoint to subtract from the p+1
      mufld.projectTo(mufldProlongateP);

      // Functional integral
      SystemVectorClass rhsPp1(qPp1.size());
      SystemVectorClass adjPp1(qPp1.size());
      rhsPp1 = 0;

      IntegrateCellGroups<TopoD3>::integrate(
          JacobianFunctionalCell_Galerkin( outputFcn, rhsPp1(0) ),
          xfld, wfldPp1, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint
      SLA::UMFPACK<SystemMatrixClass> solverAdjPp1(PrimalEqSetPp1, SLA::TransposeSolve);
      solverAdjPp1.solve(rhsPp1, adjPp1);

      // update the solution  - subtract the p solution at the same time
      for (int k = 0; k < nDOFPDEPp1; k++)
        wfldPp1.DOF(k) = adjPp1[0][k] - wfldProlongateP.DOF(k);
      for (int k = 0; k < nDOFBCPp1; k++)
        mufldPp1.DOF(k) = adjPp1[1][k];// - mufldProlongateP.DOF(k);

      // Calling the Error Estimate functions
      ErrorEstimateClass ErrorEstimate( xfld,
                                        qfld, lgfld, wfldPp1, mufldPp1,
                                        pde, stab, quadratureOrder, {0}, {0},
                                        PyBCList,
                                        BCBoundaryGroups);
      const Field_DG_Cell<PhysD3,TopoD3,Real>& ifldEstimate = ErrorEstimate.getIField();

//      ErrorEstimate.outputFields( "tmp/efld.plt" );
      // ---------------------
      // Adjoint 'exact' error
      // ---------------------
      // prolongate the primal to pmax and then evaluate strong residual

#ifdef SANS_FULLTEST
      int pmax = BasisFunctionVolume_Tet_LagrangePMax; BOOST_REQUIRE( 2*order+orderinc <= 7); //BasisFunctionArea_Triangle_HierarchicalPMax;;
#else
      int pmax = BasisFunctionVolume_Tet_LagrangePMax; BOOST_REQUIRE( 2*order+1 <= 7);
#endif

      // GLOBAL ESTIMATE
      // This is currently the old hack, just to perform the Global Estimate for verification of the local sum
      // prolongated primal to p+1
      Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfldPmax(xfld, pmax, BasisFunctionCategory_Lagrange);
#ifdef CG_TRACE // see line 292
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfldPmax( {LG_BGroup_list}, xfld, pmax, BasisFunctionCategory_Lagrange );
#else
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfldPmax( xfld, pmax-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
#endif

      const int nDOFPDEPmax = qfldPmax.nDOF(), nDOFBCPmax = lgfldPmax.nDOF();

      // prolongate the solution
      qfld.projectTo(qfldPmax);
      lgfld.projectTo(lgfldPmax);

      PrimalEquationSetClass PrimalEqSetPmax(xfld, qfldPmax, lgfldPmax, pde, stab, quadratureOrder, ResidualNorm_L2,
                                             tol, {0}, PyBCList, BCBoundaryGroups );

      // residual field
      SystemVectorClass qPmax(PrimalEqSetPmax.vectorStateSize());

      PrimalEqSetPmax.fillSystemVector(qPmax);

      // Have to use the p7 solve hack because CG L2 project is a global operation

      // Adjoint in pmax solution field
      Field_CG_Cell<PhysD3, TopoD3, ArrayQ> wfldPmax(xfld, pmax, BasisFunctionCategory_Lagrange);
      Field_CG_Cell<PhysD3, TopoD3, ArrayQ> wfldProlongatePmax(xfld, pmax, BasisFunctionCategory_Lagrange);

#ifdef CG_TRACE // see line 292
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> mufldPmax( {LG_BGroup_list}, xfld, pmax, BasisFunctionCategory_Lagrange );
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> mufldProlongatePmax( {LG_BGroup_list}, xfld, pmax, BasisFunctionCategory_Lagrange );
#else
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> mufldPmax( xfld, pmax-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> mufldProlongatePmax( xfld, pmax-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
#endif

      wfld.projectTo(wfldProlongatePmax); // prolongate the p adjoint to subtract from the pmax
      mufld.projectTo(mufldProlongatePmax);

      // Functional integral
      SystemVectorClass rhsPmax(qPmax.size());
      SystemVectorClass adjPmax(qPmax.size());
      rhsPmax = 0;

      IntegrateCellGroups<TopoD3>::integrate(
          JacobianFunctionalCell_Galerkin( outputFcn, rhsPmax(0) ),
          xfld, wfldPmax, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint
      SLA::UMFPACK<SystemMatrixClass> solverAdjPmax(PrimalEqSetPmax, SLA::TransposeSolve);
      solverAdjPmax.solve(rhsPmax, adjPmax);

      // update the solution - subtract the p solution at the same time
      for (int k = 0; k < nDOFPDEPmax; k++)
        wfldPmax.DOF(k) = adjPmax[0][k]- wfldProlongatePmax.DOF(k);
      for (int k = 0; k < nDOFBCPmax; k++)
        mufldPmax.DOF(k) = adjPmax[1][k];// - mufldProlongatePmax.DOF(k);

      // Calling the Error Estimate functions
      ErrorEstimateClass ErrorExact(xfld,
                                    qfld, lgfld, wfldPmax, mufldPmax,
                                    pde, stab, quadratureOrder, {0}, {0},
                                    PyBCList,
                                    BCBoundaryGroups);
      const Field_DG_Cell<PhysD3,TopoD3,Real>& ifldExact = ErrorExact.getIField();

      // Field Cell Types
      typedef typename XField<PhysD3, TopoD3>::FieldCellGroupType<Tet> XFieldCellGroupType;

      const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Tet>(0);

      Real globalSln=0;
      IntegrateCellGroups<TopoD3>::integrate(
          FunctionalCell_Galerkin( outputFcn, globalSln ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real avglocalErrEstEff=0,globalErrEst=0;
      Real avgLocalErrExt=0, avgLocalErrEst=0,avglocalErrEstError=0;

      KahanSum<Real> sumErrEst=0;
      // Estimates
      ErrorEstimate.getErrorEstimate(globalErrEst);
      ErrorEstimate.getErrorIndicator(avgLocalErrEst);

      // 'Exact'
      ErrorExact.getErrorIndicator(avgLocalErrExt);

      // Error in local Estimate
      Field_DG_Cell<PhysD3, TopoD3, Real> ifldDiff(xfld, 0, BasisFunctionCategory_Legendre);
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldExact.nDOF() );
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldDiff.nDOF() );
      for (int i = 0; i < ifldExact.nDOF(); i++ )
      {
        ifldDiff.DOF(i) = ifldExact.DOF(i)-ifldEstimate.DOF(i);
        sumErrEst += fabs( ifldDiff.DOF(i) );
      }
      avglocalErrEstError = (Real) sumErrEst;

      printf("avglocalErrorEstError= %f\n", avglocalErrEstError);

//       avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExt;
//       avgLocalErrExt      /= xfldCell.nElem();
//       avgLocalErrEst      /= xfldCell.nElem();
//       avglocalErrEstError /= xfldCell.nElem();
//
//       // Computing the estimate using the global weak form
//
//       // residual field
//       SystemVectorClass rsdPp1(PrimalEqSetPp1.vectorEqSize());
//       rsdPp1 = 0;
//
//       // Recompute lifting perators
//       SystemVectorClass qPp1_post(PrimalEqSetPp1.vectorStateSize());
//       PrimalEqSetPp1.fillSystemVector(qPp1_post);
//
//       PrimalEqSetPp1.residual(qPp1_post, rsdPp1);
//
//       Real globalErrEst2 = dot(rsdPp1, adjPp1);
//
//       const Real close_tol = 5e-7;
//       const Real small_tol = 5e-7;
//       SANS_CHECK_CLOSE( globalErrEst2, globalErrEst, small_tol, close_tol );
//
//
//       Real globalError = globalSln - globalExt;
//
//       hVec[indx] = 1./ii; //sqrt(nDOFPDE);
//
//       globalErrorVec[indx] = abs(globalError);
//       globalEstVec[indx] = abs(globalErrEst);
//       globalEstErrVec[indx] = abs( globalError - globalErrEst );
//       globalEffVec[indx] = globalEstErrVec[indx]/globalEstVec[indx];
//
//       localErrorVec[indx] = abs(avgLocalErrExt);
//       localEstVec[indx] = abs(avgLocalErrEst);
//       localEstErrVec[indx] = avglocalErrEstError;
//       localEffVec[indx] = avglocalErrEstEff;
//
//       if (indx > 0)
//       {
//         globalErrorRate[indx]  = (log(globalErrorVec[indx])  - log(globalErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
//         globalEstRate[indx]    = (log(globalEstVec[indx])    - log(globalEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
//         globalEstErrRate[indx] = (log(globalEstErrVec[indx]) - log(globalEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
//         globalEffRate[indx]    = (log(globalEffVec[indx])    - log(globalEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
//
//         localErrorRate[indx]  = (log(localErrorVec[indx])  - log(localErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
//         localEstRate[indx]    = (log(localEstVec[indx])    - log(localEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
//         localEstErrRate[indx] = (log(localEstErrVec[indx]) - log(localEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
//         localEffRate[indx]    = (log(localEffVec[indx])    - log(localEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
//       }
//       else
//       {
//         globalErrorRate[indx]  = 0;
//         globalEstRate[indx]    = 0;
//         globalEstErrRate[indx] = 0;
//         globalEffRate[indx]    = 0;
//
//         localErrorRate[indx]  = 0;
//         localEstRate[indx]    = 0;
//         localEstErrRate[indx] = 0;
//         localEffRate[indx]    = 0;
//       }
//
//       if ( power == powermin )
//       {
//         cout << "P = " << order << ", P^ = " << order + orderinc << ", P_ = " <<  orderinc << endl;
//         cout << "  \t";
//         cout << " global     (2P)      global     (2P)      global     (2P^)     global     (2P_)    ";
//         cout << " local      (2P+2)    local      (2P+2)    local      (P+P^+2)  local      (P_) " << endl;
//         cout << "ii\t";
//         cout << " exact                estimate             err in estimate      effectivity         ";
//         cout << " exact                estimate             err in estimate      effectivity" << endl;
//
//         resultFile << "ZONE T=\"CG P=" << order << "\"" << endl;
//       }
//
//       resultFile << std::setprecision(16) << std::scientific;
//       resultFile << hVec[indx];
//       resultFile << ", " << globalErrorVec[indx];
//       resultFile << ", " << globalEstVec[indx];
//       resultFile << ", " << globalEstErrVec[indx];
//       resultFile << ", " << globalEffVec[indx];
//       resultFile << ", " << globalErrorRate[indx];
//       resultFile << ", " << globalEstRate[indx];
//       resultFile << ", " << globalEstErrRate[indx];
//       resultFile << ", " << globalEffRate[indx];
//
//       resultFile << ", " << localErrorVec[indx];
//       resultFile << ", " << localEstVec[indx];
//       resultFile << ", " << localEstErrVec[indx];
//       resultFile << ", " << localEffVec[indx];
//       resultFile << ", " << localErrorRate[indx];
//       resultFile << ", " << localEstRate[indx];
//       resultFile << ", " << localEstErrRate[indx];
//       resultFile << ", " << localEffRate[indx];
//       resultFile << endl;
//
//       // For Latex
//       datFile << std::setprecision(16) << std::scientific;
//       if (indx==0)
//       {
//         datFile << order;
//       }
//       else
//       {
//         datFile << " ";
//       }
//       datFile << "," << hVec[indx];
//       datFile << "," << globalErrorVec[indx];
//       datFile << "," << globalEstVec[indx];
//       datFile << "," << globalEstErrVec[indx];
//       datFile << "," << globalEffVec[indx];
//       datFile << "," << globalErrorRate[indx];
//       datFile << "," << globalEstRate[indx];
//       datFile << "," << globalEstErrRate[indx];
//       datFile << "," << globalEffRate[indx];
//       datFile << "," << localErrorVec[indx];
//       datFile << "," << localEstVec[indx];
//       datFile << "," << localEstErrVec[indx];
//       datFile << "," << localEffVec[indx];
//       datFile << "," << localErrorRate[indx];
//       datFile << "," << localEstRate[indx];
//       datFile << "," << localEstErrRate[indx];
//       datFile << "," << localEffRate[indx];
//       datFile << endl;
//
//       pyriteFile << hVec[indx]; // grid
//       pyriteFile << globalErrorVec[indx] << globalEstVec[indx]<< globalEstErrVec[indx]; // values
//       pyriteFile.close(0.1) << globalErrorRate[indx]<< globalEstRate[indx]<< globalEstErrRate[indx]; // rates
//       pyriteFile << globalEffVec[indx];
//       pyriteFile.close(0.1) << globalEffRate[indx]; //effectivities
//       pyriteFile << endl;
//
//       cout << ii << "\t";
//       cout << std::scientific;
//       cout << std::scientific << std::setprecision(5) << globalErrorVec[indx];
//       cout << std::fixed      << std::setprecision(3) << " (" << globalErrorRate[indx] << ")  ";
//       cout << std::scientific << std::setprecision(5) << globalEstVec[indx];
//       cout << std::fixed      << std::setprecision(3) << " (" << globalEstRate[indx] << ")  ";
//       cout << std::scientific << std::setprecision(5) << globalEstErrVec[indx];
//       cout << std::fixed      << std::setprecision(3) << " (" << globalEstErrRate[indx] << ")  ";
//       cout << std::scientific << std::setprecision(5) << globalEffVec[indx];
//       cout << std::fixed      << std::setprecision(3) << " (" << globalEffRate[indx] << ")  ";
//
//       cout << std::scientific << std::setprecision(5) << localErrorVec[indx];
//       cout << std::fixed      << std::setprecision(3) << " (" << localErrorRate[indx] << ")  ";
//       cout << std::scientific << std::setprecision(5) << localEstVec[indx];
//       cout << std::fixed      << std::setprecision(3) << " (" << localEstRate[indx] << ")  ";
//       cout << std::scientific << std::setprecision(5) << localEstErrVec[indx];
//       cout << std::fixed      << std::setprecision(3) << " (" << localEstErrRate[indx] << ")  ";
//       cout << std::scientific << std::setprecision(5) << localEffVec[indx];
//       cout << std::fixed      << std::setprecision(3) << " (" << localEffRate[indx] << ")  ";
//       cout << endl;
//
//       indx++;
//
// #if 0
//       string filename = "tmp/locErrCG_";
//       filename += "P";
//       filename += stringify(order);
//       filename += "_";
//       filename += stringify(ii);
//       filename += "x";
//       filename += stringify(jj);
//       filename += ".plt";
//       cout << "calling output_Tecplot: filename = " << filename << endl;
//       output_Tecplot( xfldArea, localErrEstfldArea, filename );
// #endif
    }

    cout << endl;
  }
  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;
#endif
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
