// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateOrder_1D_DGBR2_Line_AD_btest
// testing error estimation of 2-D DG with Advection-Diffusion on triangles

//#define SANS_FULLTEST // Line 322 for pyrite

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "tools/KahanSum.h"
#include "pyrite_fstream.h"

#include "Surreal/SurrealS.h"

//PDE
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/SolutionFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/ForcingFunction1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h" // Needed because the BCVector is extended here

//Solve includes
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementProjection_Specialization_L2.h"

#include "Field/Function/WeightedFunctionIntegral.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"

#include "Discretization/IntegrateCellGroups.h"

//Linear algebra
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Meshing/XField1D/XField1D.h"

#include "Field/output_grm.h"
#include "tools/stringify.h"

#include "Field/output_Tecplot.h"

// Estimation
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateOrder_1D_DGBR2_Line_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateOrder_1D_DGBR2_Line_AD )
{
  timer totaltime;
  typedef SurrealS<1> SurrealClass;

  // This is the worst case scenario with p_inc = 2 -- See Hugh's Masters Thesis
  typedef ScalarFunction1D_Exp3 SolutionExact;
  typedef ScalarFunction1D_Sine AdjointExact;

  // ND Exacts
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD1, AdjointExact> NDAdjointExact;

  // Primal PDE
  typedef PDEAdvectionDiffusion< PhysD1,
                                 AdvectiveFlux1D_Uniform,
                                 ViscousFlux1D_Uniform,
                                 Source1D_UniformGrad > PDEClass;
   typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;

   typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
   typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

   typedef BCTypeFunctionLinearRobin_mitLG BCType;
   typedef BCAdvectionDiffusion<PhysD1,BCType> BCClass;
   typedef BCNDConvertSpace<PhysD1, BCClass> NDBCClass;

   typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

   typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                      AlgEqSetTraits_Sparse, DGBR2, XField<PhysD1, TopoD1>> PrimalEquationSetClass;
   typedef PrimalEquationSetClass::BCParams BCParams;
   typedef ErrorEstimate_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,XField<PhysD1,TopoD1>> ErrorEstimateClass;

   // Adjoint PDE
   typedef PDEAdvectionDiffusion< PhysD1,
                                  AdvectiveFlux1D_Uniform,
                                  ViscousFlux1D_Uniform,
                                  Source1D_UniformGrad > PDEAdjoint1D;
   typedef PDENDConvertSpace<PhysD1, PDEAdjoint1D> PDEAjointClass;

  // Integrand Functors
  // call this class with something of Adjoint Exact Class
  typedef SolutionFunction1D_ForcingFunction<PDEAjointClass>      WeightFunctional;
  typedef SolnNDConvertSpace<PhysD1, WeightFunctional>            NDWeightFunctional;

  typedef OutputCell_WeightedSolution<PDEClass, WeightFunctional> WeightOutputClass;
  typedef OutputNDConvertSpace<PhysD1, WeightOutputClass>         NDWeightOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDWeightOutputClass>         IntegrandClass;

  // PDE
  Real u=0.;
  AdvectiveFlux1D_Uniform adv( u );

  Real nu = 1.1;
  ViscousFlux1D_Uniform visc( nu );

  Source1D_UniformGrad source(0,0);

  Real a = 5;

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction1D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  // Primal PDE
  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict Exp3;
  Exp3[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function.Exp3;
  Exp3[SolutionExact::ParamsType::params.a]  = a;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD1,BCType>::params.Function] = Exp3;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = {0, 1};

  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(a) );
  NDBCClass bc( solnExactPtr, visc );

  // Adjoint PDE

  AdvectiveFlux1D_Uniform adv_adj( -u );

  a = 1;
  NDAdjointExact adjExact(a);

  typedef ForcingFunction1D_MMS<PDEAdjoint1D> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact));

  PDEAjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  NDWeightFunctional volumeFcn( pdeadj );

  // volumeFcn is g in (g,u) g = - Del psi
  NDWeightOutputClass weightOutput( volumeFcn );
  IntegrandClass outputFcn( weightOutput, {0} );

  // linear system setup
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // norm data
  const int vsize = 20;
  Real hVec[vsize];
  Real globalErrorVec[vsize], globalEstVec[vsize], globalEstErrVec[vsize], globalEffVec[vsize];
  Real localErrorVec[vsize], localEstVec[vsize], localEstErrVec[vsize], localEffVec[vsize];
  Real globalErrorRate[vsize], globalEstRate[vsize], globalEstErrRate[vsize], globalEffRate[vsize];
  Real localErrorRate[vsize], localEstRate[vsize], localEstErrRate[vsize], localEffRate[vsize];
  int indx;

#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/ErrorEst1D_DG.plt", std::ios::out);
  std::ofstream datFile("tmp/ErrorEst1D_DG.dat", std::ios::out);
#ifdef INTEL_MKL
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_1D_DG_AD_FullTest_Intel.txt";
#else
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_1D_DG_AD_FullTest.txt";
#endif
  pyrite_file_stream pyriteFile(pyritefilename, 5e-9, 0.5, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  std::stringstream datFile;
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_1D_DG_AD_MinTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-9, 5e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"Global Error\"";
  resultFile << ", \"Global Error Estimate\"";
  resultFile << ", \"Global Error Estimate Error\"";
  resultFile << ", \"Global 1-Effectivity\"";
  resultFile << ", \"Global Error Rate\"";
  resultFile << ", \"Global Error Estimate Rate\"";
  resultFile << ", \"Global Error Estimate Error Rate\"";
  resultFile << ", \"Global 1-Effectivity Rate\"";
  resultFile << ", \"Local Error\"";
  resultFile << ", \"Local Error Estimate\"";
  resultFile << ", \"Local Error Estimate Error\"";
  resultFile << ", \"Local 1-Effectivity\"";
  resultFile << ", \"Local Error Rate\"";
  resultFile << ", \"Local Error Estimate Rate\"";
  resultFile << ", \"Local Error Estimate Error Rate\"";
  resultFile << ", \"Local 1-Effectivity Rate\"";
  resultFile << endl;

  // For Latex Output
  datFile << "order,"
      "h,"
      "globalError,"
      "globalErrorEstimate,"
      "globalErrorEstimateError,"
      "globalEffectivity,"
      "globalErrorRate,"
      "globalErrorEstimateRate,"
      "globalErrorEstimateErrorRate,"
      "globalEffectivityRate,"
      "localError,"
      "localErrorEstimate,"
      "localErrorEstimateError,"
      "localEffectivity,"
      "localErrorRate,"
      "localErrorEstimateRate,"
      "localErrorEstimateErrorRate,"
      "localEffectivityRate";
  datFile << endl;

//---------------------------------------------------------------------------//
  // Compute the exact output on a fine grid and high-quadrature
  Real globalExt=0;

  {
    XField1D xfld( 50 );

    for_each_CellGroup<TopoD1>::apply( WeightedFunctionIntegral(volumeFcn, solnExact, globalExt, {0}), xfld );
  }
//---------------------------------------------------------------------------//

  const int orderinc = 2;

//#define MASA_1 // R_{h,p}(u_{h,p},\psi_{h,p^\prime})
//#define MASA_2 // R_{h,p^\prime}(u_{h,p},\psi_{h,p^\prime})

#if defined(MASA_1) || defined(MASA_2)
  std::cout<< "Lifting Operator error not included in estimate" << std::endl;
#else
  std::cout<< "Lifting Operator error included in estimate" << std::endl;
#endif

  int ordermin = 1;
#ifndef SANS_FULLTEST
  // ordermax = 2 is used for the pyrite
  int ordermax = 2;
#else
  // ordermax = 3 is used for the pyrite
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
    cout << "...running full test" << endl;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
#ifndef SANS_FULLTEST
      //ii = 2 is used for the pyrite
      ii = 2;
#else
      // ii = 4*power used for the pyrite
      ii = 4*power;
#endif

      cout << std::scientific << std::setprecision(7);
      // grid: Hierarchical P1 ( aka X1)

      XField1D xfld( ii );
      //int nInteriorTraceGroups = xfld.nInteriorTraceGroups();
      int nBoundaryTraceGroups = xfld.nBoundaryTraceGroups();

      // Primal Solution in p
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
      qfld = 0; lgfld = 0; rfld = 0;

      //output_grm(xfld, "tmp/hugh_" + stringify(ii) + ".grm");

      // quadrature rule
      QuadratureOrder quadratureOrder( xfld, - 1 );

      std::vector<Real> tol = {1e-12, 1e-12};
      // create the primal equations
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups );

      // residual

      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

      PrimalEqSet.fillSystemVector(q);

      rsd = 0;
      PrimalEqSet.residual(q, rsd);

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());
      solver.solve(rsd, dq);

      // update solution
      q -= dq;

      PrimalEqSet.setSolutionField(q); // This is putting q into qfld and rfld

      // Adjoint in p
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> mufld( xfld, order, BasisFunctionCategory_Hierarchical );
      wfld = 0; mufld = 0; sfld = 0;

      // adjoint solve
      SystemVectorClass adj(q.size());
      rsd = 0;

      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell_DGBR2<SurrealClass>( outputFcn, rsd(PrimalEqSet.iPDE) ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );


      // compute the adjoint in p
      SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);
      solver.solve(rsd, adj);

      // set adjoint fields
      PrimalEqSet.setAdjointField( adj, wfld, sfld, mufld );

      // ---------------------
      // Adjoint error estimate
      // Adjoint in p+p_inc
      // ---------------------

      // prolongated primal -- Needed so that Masa's estimates can be compared
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> rfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldPp1( xfld, order+orderinc, BasisFunctionCategory_Hierarchical );
      qfldPp1 = 0; lgfldPp1 = 0; rfldPp1 = 0;

      // prolongate the solution
      qfld.projectTo(qfldPp1);
      rfld.projectTo(rfldPp1);
      lgfld.projectTo(lgfldPp1);

      PrimalEquationSetClass PrimalEqSetPp1(xfld, qfldPp1, rfldPp1, lgfldPp1, pde, disc, quadratureOrder,
                                            ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups );

#ifdef MASA_2 // evaluate the lifting operators in p+p_inc
      // Estimate 2 from Masa's Thesis, Estimate 2 from Hugh's thesis -- R_{h,p^\prime}(u_{h,p}, \psi_{h,p^\prime})
      SystemVectorClass qPp1(PrimalEqSetPp1.vectorStateSize());
      PrimalEqSetPp1.fillSystemVector(qPp1);
      PrimalEqSetPp1.setSolutionField(qPp1);
#endif

      // Adjoint in p+1 solution field
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> mufldPp1( xfld, order+orderinc, BasisFunctionCategory_Hierarchical );
      wfldPp1 = 0; mufldPp1 = 0; sfldPp1 = 0;

      // Functional integral
      SystemVectorClass rhsPp1(PrimalEqSetPp1.vectorEqSize());
      SystemVectorClass adjPp1(PrimalEqSetPp1.vectorStateSize());
      rhsPp1 = 0;

      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell_DGBR2<SurrealClass>( outputFcn, rhsPp1(PrimalEqSetPp1.iPDE) ),
          xfld, (qfldPp1, rfldPp1), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint
      SLA::UMFPACK<SystemMatrixClass> solverAdjPp1(PrimalEqSetPp1, SLA::TransposeSolve);
      solverAdjPp1.solve(rhsPp1, adjPp1);

      // update solution
      PrimalEqSetPp1.setAdjointField(adjPp1, wfldPp1, sfldPp1, mufldPp1);

#if defined(MASA_1) || defined(MASA_2)
      // Estimate 3 from Masa's Thesis, Estimate 1 from Hugh's thesis -- R_{h,p}(u_{h,p}, \psi_{h,p^\prime})
      // Do not account for errors in the lifting operator
      sfldPp1 = 0; // zero out Sigma
#endif

#ifdef MASA_2
      ErrorEstimateClass ErrorEstimate(xfld,
                                       qfldPp1, rfldPp1, lgfldPp1, wfldPp1, sfldPp1, mufldPp1,
                                       pde, disc, quadratureOrder, {0}, {0},
                                       PyBCList,
                                       BCBoundaryGroups);
#else
      ErrorEstimateClass ErrorEstimate(xfld,
                                       qfld, rfld, lgfld, wfldPp1, sfldPp1, mufldPp1,
                                       pde, disc, quadratureOrder, {0}, {0},
                                       PyBCList,
                                       BCBoundaryGroups);
#endif
      const Field<PhysD1,TopoD1,Real>& ifldEstimate = ErrorEstimate.getIField();

      // ---------------------
      // Adjoint error exact
      // ---------------------
      // prolongate the primal to pmax and then evaluate residual

#ifdef SANS_FULLTEST
      int pmax = 7; BOOST_REQUIRE( 2*order+orderinc <= 7); //BasisFunctionArea_Line_HierarchicalPMax;;
#else
      int pmax = 2*order + orderinc;
#endif

      // Adjoint in pmax solution field
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> wfldPmax(xfld, pmax, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD1, TopoD1, VectorArrayQ> sfldPmax(xfld, pmax, BasisFunctionCategory_Legendre);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> mufldPmax( xfld, pmax, BasisFunctionCategory_Hierarchical );
      wfldPmax = 0; mufldPmax = 0; sfldPmax = 0;

      // Loop over elems and L2 Project the Exact solution onto the basis
      typedef typename XField<PhysD1, TopoD1                  >::FieldCellGroupType<Line> XFieldCellGroupType;
      typedef typename Field< PhysD1, TopoD1, ArrayQ          >::FieldCellGroupType<Line> QFieldCellGroupType;
      typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;
      typedef typename QFieldCellGroupType::ElementType<> ElementQFieldClass;

      const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Line>(0);
      QFieldCellGroupType&  adjqfldCellPmax = wfldPmax.getCellGroup<Line>(0);

      ElementXFieldClass  xfldElem( xfldCell.basis() );
      ElementQFieldClass  adjqfldElemExact( adjqfldCellPmax.basis() );

      // class for computing the projection
      ElementProjectionSolution_L2<TopoD1, Line> projector(adjqfldElemExact.basis());

      // Cell terms - hard coded to get the diffusive flux, little bit dirty
      for (int elem = 0; elem < xfldCell.nElem(); elem++)
      {
        xfldCell.getElement( xfldElem, elem );

        // L2 Project the exact adjoint onto the pmax basis
        projector.project( xfldElem, adjqfldElemExact, adjExact );
        adjqfldCellPmax.setElement(adjqfldElemExact,elem);

        // Lifting operator adjoint field is zero for little-R formulation without source with gradient
      }

      // Boundary terms
      typedef typename Field<PhysD1, TopoD1, ArrayQ>::FieldTraceGroupType<Node> QFieldTraceGroupType;
      typedef typename XField<PhysD1, TopoD1>::FieldTraceGroupType<Node> XFieldTraceGroupType;
      typedef typename QFieldTraceGroupType::ElementType<> ElementQFieldTraceClass;
      typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;

      for (int group = 0; group < nBoundaryTraceGroups; group++)
      {
        const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<Node>(group);
        QFieldTraceGroupType adjlgfldBoundaryTracePmax = mufldPmax.getBoundaryTraceGroup<Node>(group);

        ElementXFieldTraceClass xfldElem( xfldBoundaryTrace.basis() );
        ElementQFieldTraceClass adjlgfldElemExact( adjlgfldBoundaryTracePmax.basis() );

        for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
        {
          xfldBoundaryTrace.getElement( xfldElem, elem );

          // L2 Project for the lagrange multipliers
          ElementProjection_LinearScalarLG_L2(xfldElem, adjlgfldElemExact, pde, bc, adjExact );

          adjlgfldBoundaryTracePmax.setElement(adjlgfldElemExact, elem);
        }
      }

      // Reuse error estimation class
      ErrorEstimateClass ErrorExact(xfld,
                                    qfld, rfld, lgfld, wfldPmax, sfldPmax, mufldPmax,
                                    pde, disc, quadratureOrder, {0}, {0},
                                    PyBCList,
                                    BCBoundaryGroups);
      const Field<PhysD1,TopoD1,Real>& ifldExact = ErrorExact.getIField();

      // global and local error
      // solution - p - For directly evaluating the functional, and redistributing eLfld

      Real globalSln=0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_DGBR2( outputFcn, globalSln ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );


      Real avglocalErrEstEff=0,globalErrEst=0;
      Real avgLocalErrExt=0, avgLocalErrEst=0,avglocalErrEstError=0;

      KahanSum<Real> sumErrEst=0;
      // Estimates
      ErrorEstimate.getErrorEstimate(globalErrEst);
      ErrorEstimate.getErrorIndicator(avgLocalErrEst);

      // 'Exact'
      ErrorExact.getErrorIndicator(avgLocalErrExt);

      // Error in local Estimate
      Field_DG_Cell<PhysD1, TopoD1, Real> ifldDiff(xfld, 0, BasisFunctionCategory_Legendre);
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldExact.nDOF() );
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldDiff.nDOF() );
      for (int i = 0; i < ifldExact.nDOF(); i++ )
      {
        ifldDiff.DOF(i) = ifldExact.DOF(i)-ifldEstimate.DOF(i);
        sumErrEst += fabs( ifldDiff.DOF(i) );
      }
      avglocalErrEstError = (Real)sumErrEst;

      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExt;
      avgLocalErrExt      /= xfldCell.nElem();
      avgLocalErrEst      /= xfldCell.nElem();
      avglocalErrEstError /= xfldCell.nElem();

      const Real close_tol = 5e-7;
      const Real small_tol = 5e-7;
      if (power==1)
      {
        // residual field
        SystemVectorClass rsdPp1(PrimalEqSetPp1.vectorEqSize());
        rsdPp1 = 0;

        // Recompute lifting perators
        SystemVectorClass qPp1(PrimalEqSetPp1.vectorStateSize());
        PrimalEqSetPp1.fillSystemVector(qPp1);

        // recompute lifting operators and evaluate the residual in p+1
        // equivalent to Masa's Estimate 2 for the global, but gives bad local estimates
        PrimalEqSetPp1.residual(qPp1, rsdPp1);

        // This check is only valid if the lifting operator is recomputed in p+1
        // However, the local estimate is worse if the lifting operator is recomputed
        Real globalErrEst2 = dot(rsdPp1, adjPp1);
        SANS_CHECK_CLOSE( globalErrEst2, globalErrEst, small_tol, close_tol );
      }
      Real globalError = globalSln - globalExt;

      hVec[indx] = 1./ii; //sqrt(nDOFPDE);

      globalErrorVec[indx] = abs(globalError);
      globalEstVec[indx] = abs(globalErrEst);
      globalEstErrVec[indx] = abs( globalError - globalErrEst );
      globalEffVec[indx] = globalEstErrVec[indx]/globalEstVec[indx];

      localErrorVec[indx] = abs(avgLocalErrExt);
      localEstVec[indx] = abs(avgLocalErrEst);
      localEstErrVec[indx] = avglocalErrEstError;
      localEffVec[indx] = avglocalErrEstEff;

      //std::cout << "Local Error Exact: " << std::scientific << std::setprecision(16) << abs(avgLocalErrExt) << std::endl;

      if (indx > 0)
      {
        globalErrorRate[indx]  = (log(globalErrorVec[indx])  - log(globalErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstRate[indx]    = (log(globalEstVec[indx])    - log(globalEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstErrRate[indx] = (log(globalEstErrVec[indx]) - log(globalEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        globalEffRate[indx]    = (log(globalEffVec[indx])    - log(globalEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));

        localErrorRate[indx]  = (log(localErrorVec[indx])  - log(localErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstRate[indx]    = (log(localEstVec[indx])    - log(localEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstErrRate[indx] = (log(localEstErrVec[indx]) - log(localEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        localEffRate[indx]    = (log(localEffVec[indx])    - log(localEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
      }
      else
      {
        globalErrorRate[indx]  = 0;
        globalEstRate[indx]    = 0;
        globalEstErrRate[indx] = 0;
        globalEffRate[indx]    = 0;

        localErrorRate[indx]  = 0;
        localEstRate[indx]    = 0;
        localEstErrRate[indx] = 0;
        localEffRate[indx]    = 0;
      }

      if ( power == powermin )
      {
        cout << "P = " << order << ", P^ = " << order + orderinc << ", P_ = " <<  orderinc << endl;
        cout << "  \t";
        cout << " global     (2P)      global     (2P)      global     (2P^)     global     (2P_)    ";
        cout << " local      (2P+1)    local      (2P+1)    local      (2P^+1)   local      (2P_) " << endl;
        cout << "ii\t";
        cout << " exact                estimate             err in estimate      effectivity         ";
        cout << " exact                estimate             err in estimate      effectivity" << endl;

        resultFile << "ZONE T=\"DG P=" << order << "\"" << endl;
      }

      resultFile << std::setprecision(16) << std::scientific;
      resultFile << hVec[indx];
      resultFile << ", " << globalErrorVec[indx];
      resultFile << ", " << globalEstVec[indx];
      resultFile << ", " << globalEstErrVec[indx];
      resultFile << ", " << globalEffVec[indx];
      resultFile << ", " << globalErrorRate[indx];
      resultFile << ", " << globalEstRate[indx];
      resultFile << ", " << globalEstErrRate[indx];
      resultFile << ", " << globalEffRate[indx];

      resultFile << ", " << localErrorVec[indx];
      resultFile << ", " << localEstVec[indx];
      resultFile << ", " << localEstErrVec[indx];
      resultFile << ", " << localEffVec[indx];
      resultFile << ", " << localErrorRate[indx];
      resultFile << ", " << localEstRate[indx];
      resultFile << ", " << localEstErrRate[indx];
      resultFile << ", " << localEffRate[indx];
      resultFile << endl;

      // For Latex
      datFile << std::setprecision(16) << std::scientific;
      if (indx==0)
      {
        datFile << order;
      }
      else
      {
        datFile << " ";
      }
      datFile << "," << hVec[indx];
      datFile << "," << globalErrorVec[indx];
      datFile << "," << globalEstVec[indx];
      datFile << "," << globalEstErrVec[indx];
      datFile << "," << globalEffVec[indx];
      datFile << "," << globalErrorRate[indx];
      datFile << "," << globalEstRate[indx];
      datFile << "," << globalEstErrRate[indx];
      datFile << "," << globalEffRate[indx];
      datFile << "," << localErrorVec[indx];
      datFile << "," << localEstVec[indx];
      datFile << "," << localEstErrVec[indx];
      datFile << "," << localEffVec[indx];
      datFile << "," << localErrorRate[indx];
      datFile << "," << localEstRate[indx];
      datFile << "," << localEstErrRate[indx];
      datFile << "," << localEffRate[indx];
      datFile << endl;

      cout << ii << "\t";
      cout << std::scientific;
      cout << std::scientific << std::setprecision(5) << globalErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEffRate[indx] << ")  ";

      cout << std::scientific << std::setprecision(5) << localErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEffRate[indx] << ")  ";
      cout << endl;

      pyriteFile          << globalErrorVec[indx];
      pyriteFile.close(1) << globalErrorRate[indx];
      pyriteFile          << globalEstVec[indx];
      pyriteFile.close(1) << globalEstRate[indx];
      pyriteFile          << globalEstErrVec[indx];
      pyriteFile.close(1) << globalEstErrRate[indx];
      pyriteFile          << globalEffVec[indx];
      pyriteFile.close(1) << globalEffRate[indx];

      pyriteFile          << localErrorVec[indx];
      pyriteFile.close(1) << localErrorRate[indx];
      pyriteFile          << localEstVec[indx];
      pyriteFile.close(1) << localEstRate[indx];
      pyriteFile          << localEstErrVec[indx];
      pyriteFile.close(1) << localEstErrRate[indx];
      pyriteFile          << localEffVec[indx];
      pyriteFile.close(1) << localEffRate[indx];
      pyriteFile << std::endl;

      indx++;

#if 0
      string filename = "tmp/locErrCG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( wfld, filename );
#endif
    }

    cout << endl;
  }
  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
