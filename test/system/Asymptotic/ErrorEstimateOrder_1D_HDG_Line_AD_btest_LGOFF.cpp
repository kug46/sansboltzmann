// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEst_1D_HDG_Line_AD_btest
// testing error estimation of 1-D HDG with Advection-Diffusion on Lines
//
// case: double BL

#undef SANS_FULLTEST
//#define SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
//#include "pyrite_fstream.h"
#include "tools/timer.h"
#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/SolutionFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Dirichlet_mitState_HDG.h"
// #include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementIntegral.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEst_NEW_HDG_Line_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEst_1D_HDG_Line_AD )
{
  timer totaltime;

  typedef SurrealS<1> SurrealClass;

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEAdvectionDiffusion1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdvectionDiffusion1D> PDEClass;

  typedef PDEAdvectionDiffusion1D::ArrayQ<Real> ArrayQ;
  typedef PDEAdvectionDiffusion1D::MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  // Laplacian w/ Exponential functions
  typedef ScalarFunction1D_Exp1 SolutionExact;
  typedef ScalarFunction1D_Hex  AdjointExact;

  // ND Exacts
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD1, AdjointExact>  NDAdjointExact;

  // Integrand Functors
  // call this class with something of Adjoint Exact Class
  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEAdjoint1D;
  typedef PDENDConvertSpace<PhysD1, PDEAdjoint1D> PDEAjointClass;

  typedef SolnNDConvertSpace<PhysD1,  SolutionFunction1D_ForcingFunction<PDEAjointClass> > VolumeFcn;
  typedef IntegrandCell_WeightedSolution<PDEClass,VolumeFcn>                                IntegrandClass;
  typedef IntegrandClass::Functor<Real,TopoD1,Line>                                         FunctorClass;
  typedef IntegrandCell_WeightedExactSolution<PDEClass,VolumeFcn,NDSolutionExact>           ExactIntegrandClass;
  typedef ExactIntegrandClass::Functor<TopoD1,Line>                                         ExactFunctorClass;


  typedef BCTypeFunctionLinearRobin_mitLG BCType;
  typedef BCAdvectionDiffusion<PhysD1, BCType> BCAdvectionDiffusion1D;
  typedef BCNDConvertSpace<PhysD1, BCAdvectionDiffusion1D> BCClass;

  // PDE
  Real u=0.;
  AdvectiveFlux1D_Uniform adv( u );

  Real nu = 1;
  ViscousFlux1D_Uniform visc( nu );

  NDSolutionExact solnExact;

  Source1D_UniformGrad source(0,0);

  typedef ForcingFunction1D_MMS<PDEAdvectionDiffusion1D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));

  // uses the auto construction of forcing functions
  PDEClass pde( adv, visc , source, forcingptr );

  // BC
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact );
  BCClass bc( solnExactPtr, visc );

  // Adjoint PDE

  AdvectiveFlux1D_Uniform adv_adj( -u );

  NDAdjointExact          adjExact;

  typedef ForcingFunction1D_MMS<PDEAdjoint1D> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact));

  PDEAjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  // Weighting function for the volume output based on the forcing function of the adjoint PDE
  VolumeFcn  volumeFcn( pdeadj );

  cout << "case: Laplacian w/ Exponentials" << endl;

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;
  typedef NDVectorCategory<boost::mpl::vector1<BCClass>, BCClass::Category> NDBCVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCVecCat, HDG> IntegrandBoundClass;

  string lRef;
  // HDG discretization
  //DiscretizationHDG<PDEClass> disc( pde, 1 , AugGradient , 1); cout << "...using HDG-1" << endl; lRef = "1";
  DiscretizationHDG<PDEClass> disc( pde, Local); cout << "...using HDG-h" << endl; lRef = "h";

  const std::vector<int> BoundaryGroups = {0,1};
  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnInt( pde, disc, {0} );
  IntegrandBoundClass fcnBC( pde, bc, BoundaryGroups, disc );

  // norm data
  const int vsize = 50;
  Real hVec[vsize];
  Real globalErrorVec[vsize], globalEstVec[vsize], globalEstErrVec[vsize], globalEffVec[vsize];
  Real localErrorVec[vsize], localEstVec[vsize], localEstErrVec[vsize], localEffVec[vsize];
  Real globalErrorRate[vsize], globalEstRate[vsize], globalEstErrRate[vsize], globalEffRate[vsize];
  Real localErrorRate[vsize], localEstRate[vsize], localEstErrRate[vsize], localEffRate[vsize];
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::string filename = "tmp/ErrorEst1D_HDG_" + lRef + ".plt";
  std::ofstream resultFile(filename, std::ios::out);
  //pyrite_file_stream pyriteFile("IO/ErrorEstimates/ErrorEst_1D_HDG_AD_FullTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  //pyrite_file_stream pyriteFile("IO/ErrorEstimates/ErrorEst_1D_HDG_AD_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif

  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"Global Error\"";
  resultFile << ", \"Global Error Estimate\"";
  resultFile << ", \"Global Error Estimate Error\"";
  resultFile << ", \"Global 1-Effectivity\"";
  resultFile << ", \"Global Error Rate\"";
  resultFile << ", \"Global Error Estimate Rate\"";
  resultFile << ", \"Global Error Estimate Error Rate\"";
  resultFile << ", \"Global 1-Effectivity Rate\"";
  resultFile << ", \"Local Error\"";
  resultFile << ", \"Local Error Estimate\"";
  resultFile << ", \"Local Error Estimate Error\"";
  resultFile << ", \"Local 1-Effectivity\"";
  resultFile << ", \"Local Error Rate\"";
  resultFile << ", \"Local Error Estimate Rate\"";
  resultFile << ", \"Local Error Estimate Error Rate\"";
  resultFile << ", \"Local 1-Effectivity Rate\"";
  resultFile << endl;

  int quadorder=-1;
  ElementIntegral <TopoD1, Line, ArrayQ> ElemIntegral(quadorder);

//---------------------------------------------------------------------------//
  // Computer the exact output on a fine grid and high-quadrature
  Real globalExt=0;

  {
    XField1D xfld( 50 );

    // Field Cell Types
    typedef typename XField<PhysD1, TopoD1>::FieldCellGroupType<Line> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;

    ExactIntegrandClass exFcnint( volumeFcn, solnExact, {0} );

    ArrayQ localExt=0;

    const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Line>(0);

    ElementXFieldClass  xfldElem( xfldCell.basis() );

    // Loop over Volume Integrals
    for (int elem = 0; elem < xfldCell.nElem(); elem++)
    {
      xfldCell.getElement( xfldElem, elem );

      ExactFunctorClass exFcn = exFcnint.integrand(xfldElem);

      localExt = 0;
      ElemIntegral(exFcn, xfldElem, localExt);

      globalExt += localExt;
    }
    //std::cout << "globalExt = " << std::scientific << std::setprecision(16) << globalExt << std::endl;
  }
//---------------------------------------------------------------------------//


  const int orderinc = 1;

  int ordermin = 1;
  int ordermax = 3;
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 10;
    cout << "...running full test" << endl;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
#ifndef SANS_FULLTEST
      ii = 2;
#else
      ii = 4*power+4;
#endif

      cout << std::scientific << std::setprecision(7);
      // grid: Hierarchical P1 ( aka X1)

      XField1D xfld( ii );

      int nInteriorTraceGroups = xfld.nInteriorTraceGroups();
      int nBoundaryTraceGroups = xfld.nBoundaryTraceGroups();
      int nCellGroups = xfld.nCellGroups();

      BOOST_REQUIRE_EQUAL( 1, nCellGroups ); //unit test hard coded for single area group
      BOOST_REQUIRE_EQUAL( 1, nInteriorTraceGroups );
      BOOST_REQUIRE_EQUAL( 2, nBoundaryTraceGroups );

      // HDG solution field
      // solution in p: Legendre
      // cell solution
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroups);


      int nDOFPDE = qfld.nDOF();
      int nDOFAu  = afld.nDOF();
      int nDOFInt = qIfld.nDOF();
      int nDOFBC  = lgfld.nDOF();

      // quadrature rule
      int quadratureOrder[2] = {-1, -1};    // max
      int quadratureOrderMin[2] = {0, 0};     // min

      // linear system setup

      typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
      typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
      typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
      typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;

      // residual

      SystemVectorClass rsd = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};
      rsd = 0;

      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsd(0), rsd(1)),
                                              xfld, (qfld, afld), quadratureOrder, nCellGroups );
      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(0), rsd(1), rsd(2)),
                                                              xfld, (qfld, afld), qIfld, quadratureOrder, nInteriorTraceGroups);
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsd(0), rsd(1), rsd(3)),
                                                          xfld, (qfld, afld), lgfld, quadratureOrderMin, nBoundaryTraceGroups );

      // jacobian nonzero pattern
      //
      //        u   q  uI  lg
      //   PDE  X   X   X   X
      //   Au   X   X   X   X
      //   Int  X   X   X   0
      //   BC   X   X   0   0

      typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

      //fld.SystemMatrixSize();
      DLA::MatrixD<NonZeroPatternClass> nz =
          {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFAu}, {nDOFPDE, nDOFInt}, {nDOFPDE, nDOFBC} },
           { {nDOFAu , nDOFPDE}, {nDOFAu , nDOFAu}, {nDOFAu , nDOFInt}, {nDOFAu , nDOFBC} },
           { {nDOFInt, nDOFPDE}, {nDOFInt, nDOFAu}, {nDOFInt, nDOFInt}, {nDOFInt, nDOFBC} },
           { {nDOFBC , nDOFPDE}, {nDOFBC , nDOFAu}, {nDOFBC , nDOFInt}, {nDOFBC , nDOFBC} }};

      NonZeroPatternClass& nzPDE_q  = nz(0,0);
      NonZeroPatternClass& nzPDE_a  = nz(0,1);
      NonZeroPatternClass& nzPDE_qI = nz(0,2);
      NonZeroPatternClass& nzPDE_lg = nz(0,3);

      NonZeroPatternClass& nzAu_q  = nz(1,0);
      NonZeroPatternClass& nzAu_a  = nz(1,1);
      NonZeroPatternClass& nzAu_qI = nz(1,2);
      NonZeroPatternClass& nzAu_lg = nz(1,3);

      NonZeroPatternClass& nzInt_q  = nz(2,0);
      NonZeroPatternClass& nzInt_a  = nz(2,1);
      NonZeroPatternClass& nzInt_qI = nz(2,2);
      //NonZeroPatternClass& nzInt_lg = nz(2,3);

      NonZeroPatternClass& nzBC_q  = nz(3,0);
      NonZeroPatternClass& nzBC_a  = nz(3,1);
      //NonZeroPatternClass& nzBC_qI = nz(3,2);
      NonZeroPatternClass& nzBC_lg = nz(3,3);

      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nzPDE_q, nzPDE_a,
                                                                                      nzAu_q, nzAu_a),
                                              xfld, (qfld, afld), quadratureOrderMin, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, nzPDE_q, nzPDE_a, nzPDE_qI,
                                                          nzAu_q ,          nzAu_qI ,
                                                          nzInt_q, nzInt_a, nzInt_qI),
          xfld, (qfld, afld), qIfld, quadratureOrderMin, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, nzPDE_q, nzPDE_a, nzPDE_lg,
                                                            nzAu_q , nzAu_a , nzAu_lg ,
                                                            nzBC_q , nzBC_a , nzBC_lg ),
          xfld, (qfld, afld), lgfld, quadratureOrderMin, nBoundaryTraceGroups);

      // jacobian
      SystemMatrixClass jac(nz);

      SparseMatrixClass& jacPDE_q  = jac(0,0);
      SparseMatrixClass& jacPDE_a  = jac(0,1);
      SparseMatrixClass& jacPDE_qI = jac(0,2);
      SparseMatrixClass& jacPDE_lg = jac(0,3);

      SparseMatrixClass& jacAu_q  = jac(1,0);
      SparseMatrixClass& jacAu_a  = jac(1,1);
      SparseMatrixClass& jacAu_qI = jac(1,2);
      SparseMatrixClass& jacAu_lg = jac(1,3);

      SparseMatrixClass& jacInt_q  = jac(2,0);
      SparseMatrixClass& jacInt_a  = jac(2,1);
      SparseMatrixClass& jacInt_qI = jac(2,2);
      //SparseMatrixClass& jacInt_lg = jac(2,3);

      SparseMatrixClass& jacBC_q  = jac(3,0);
      SparseMatrixClass& jacBC_a  = jac(3,1);
      //SparseMatrixClass& jacBC_qI = jac(3,2);
      SparseMatrixClass& jacBC_lg = jac(3,3);

      jac = 0;

      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacPDE_q, jacPDE_a,
                                                                                      jacAu_q, jacAu_a),
                                              xfld, (qfld, afld), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacPDE_q, jacPDE_a, jacPDE_qI,
                                                          jacAu_q ,           jacAu_qI ,
                                                          jacInt_q, jacInt_a, jacInt_qI),
          xfld, (qfld, afld), qIfld, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jacPDE_q, jacPDE_a, jacPDE_lg,
                                                            jacAu_q , jacAu_a , jacAu_lg ,
                                                            jacBC_q , jacBC_a , jacBC_lg ),
          xfld, (qfld, afld), lgfld, quadratureOrder, nBoundaryTraceGroups);

#if 0
      fstream fout( "tmp/jac.dat", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

      // solve

      SLA::UMFPACK<SystemMatrixClass> solver;

      SystemVectorClass sln = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};

      sln = solver.inverse(jac)*rsd;

      // updated solution

      for (int k = 0; k < nDOFPDE; k++)
        qfld.DOF(k) -= sln[0][k];

      for (int k = 0; k < nDOFPDE; k++)
        for (int d = 0; d < PhysD1::D; d++)
          afld.DOF(k)[d] -= sln[1][PhysD1::D*k+d];

      for (int k = 0; k < nDOFInt; k++)
        qIfld.DOF(k) -= sln[2][k];

      for (int k = 0; k < nDOFBC; k++)
        lgfld.DOF(k) -= sln[3][k];

      // check that the residual is zero

      rsd = 0;

      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsd(0), rsd(1)),
                                              xfld, (qfld, afld), quadratureOrder, nCellGroups );
      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(0), rsd(1), rsd(2)),
                                                              xfld, (qfld, afld), qIfld, quadratureOrder, nInteriorTraceGroups);
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsd(0), rsd(1), rsd(3)),
                                                          xfld, (qfld, afld), lgfld, quadratureOrderMin, nBoundaryTraceGroups );

#if 0
      // Tecplot dump
      string filename = "tmp/slnHDG_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, qfldArea, rxfldArea, ryfldArea, &solnExact, filename, order-1 );
#endif

      // Adjoint in p

      // HDG adjoint solution field

      // cell solution
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qflda(xfld, order, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> aflda(xfld, order, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIflda(xfld, order, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgflda(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroups);


      nDOFPDE = qflda.nDOF();
      nDOFAu  = aflda.nDOF();
      nDOFInt = qIflda.nDOF();
      nDOFBC  = lgflda.nDOF();

      // jacobian transpose

      // jacobian nonzero pattern
      //
      //        u   q  uI  lg
      //   PDE  X   X   X   X
      //   Au   X   X   X   X
      //   Int  X   X   X   0
      //   BC   X   X   0   0

      //Create a transposed non-zero pattern
      DLA::MatrixD<NonZeroPatternClass> nzT(Transpose(nz));

      SystemMatrixClass jacT(nzT);

      // ND version
      auto jacTPDE_q  = Transpose(jacT)(0,0);
      auto jacTPDE_a  = Transpose(jacT)(0,1);
      auto jacTPDE_qI = Transpose(jacT)(0,2);
      auto jacTPDE_lg = Transpose(jacT)(0,3);

      auto jacTAu_q  = Transpose(jacT)(1,0);
      auto jacTAu_a  = Transpose(jacT)(1,1);
      auto jacTAu_qI = Transpose(jacT)(1,2);
      auto jacTAu_lg = Transpose(jacT)(1,3);

      auto jacTInt_q  = Transpose(jacT)(2,0);
      auto jacTInt_a  = Transpose(jacT)(2,1);
      auto jacTInt_qI = Transpose(jacT)(2,2);
      //auto jacInt_lg  = Transpose(jacT)(2,3);

      auto jacTBC_q  = Transpose(jacT)(3,0);
      auto jacTBC_a  = Transpose(jacT)(3,1);
      //auto jacTBC_qI = Transpose(jacT)(3,2);
      auto jacTBC_lg = Transpose(jacT)(3,3);

      jac = 0;

      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacTPDE_q, jacTPDE_a,
                                                                                      jacTAu_q, jacTAu_a),
                                              xfld, (qfld, afld), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacTPDE_q, jacTPDE_a, jacTPDE_qI,
                                                          jacTAu_q ,            jacTAu_qI ,
                                                          jacTInt_q, jacTInt_a, jacTInt_qI),
          xfld, (qflda, aflda), qIflda, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jacTPDE_q, jacTPDE_a, jacTPDE_lg,
                                                            jacTAu_q , jacTAu_a , jacTAu_lg ,
                                                            jacTBC_q , jacTBC_a , jacTBC_lg ),
          xfld, (qflda, aflda), lgflda, quadratureOrder, nBoundaryTraceGroups);

      // solve

      SystemVectorClass adj = {{nDOFPDE}, {nDOFAu}, {nDOFInt}, {nDOFBC}};

      rsd = 0; // reset residual

      IntegrandClass outputFcn( volumeFcn, {0} ); // declared back with the pde

      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell<SurrealClass>( outputFcn, rsd(0) ), xfld, qflda, quadratureOrder, nCellGroups );

      // compute the adjoint

      adj = solver.inverse(jacT)*rsd;

      // updated solution

      for (int k = 0; k < nDOFPDE; k++)
        qflda.DOF(k) = adj[0][k];
      for (int k = 0; k < nDOFPDE; k++)
        for (int d = 0; d < PhysD1::D; d++)
          aflda.DOF(k)[d] = adj[1][PhysD1::D*k+d];
      for (int k = 0; k < nDOFInt; k++)
        qIflda.DOF(k) = adj[2][k];
      for (int k = 0; k < nDOFBC; k++)
        lgflda.DOF(k) = adj[3][k];

      // ---------------------
      // Adjoint error estimate
      // ---------------------
      // prolongate P+1 primal solution

      // HDG primal solution field

      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre, BoundaryGroups);

      qfld.projectTo(qfldPp1);
      afld.projectTo(afldPp1);
      qIfld.projectTo(qIfldPp1);
      lgfld.projectTo(lgfldPp1);

      const int nDOFPDEPp1 = qfldPp1.nDOF();
      const int nDOFAuPp1  = afldPp1.nDOF();
      const int nDOFIntPp1 = qIfldPp1.nDOF();
      const int nDOFBCPp1  = lgfldPp1.nDOF();


      // residual

      SystemVectorClass rsdPp1 = {{nDOFPDEPp1},{nDOFAuPp1},{nDOFIntPp1},{nDOFBCPp1}};
      rsdPp1 = 0;

      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre, BoundaryGroups);


      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsdPp1(0), rsdPp1(1)),
                                              xfld, (qfldPp1, afldPp1), quadratureOrder, nCellGroups );
      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsdPp1(0), rsdPp1(1), rsdPp1(2)),
                                                              xfld, (qfldPp1, afldPp1), qIfldPp1, quadratureOrder, nInteriorTraceGroups);
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsdPp1(0), rsdPp1(1), rsdPp1(3)),
                                                          xfld, (qfldPp1, afldPp1), lgfldPp1, quadratureOrderMin, nBoundaryTraceGroups );


      // Map residual to field

     for (int k = 0; k < nDOFPDEPp1; k++)
       qfldrPp1.DOF(k) = rsdPp1[0][k];

     for (int k = 0; k < nDOFPDEPp1; k++)
       for (int d = 0; d < PhysD1::D; d++)
         afldrPp1.DOF(k)[d] = rsdPp1[1][PhysD1::D*k+d];

     for (int k = 0; k < nDOFIntPp1; k++)
       qIfldrPp1.DOF(k) = rsdPp1[2][k];

     for (int k = 0; k < nDOFBCPp1; k++)
       lgfldrPp1.DOF(k) = rsdPp1[3][k];


      // jacobian nonzero pattern
      //
      //        u   q  uI  lg
      //   PDE  X   X   X   X
      //   Au   X   X   X   X
      //   Int  X   X   X   0
      //   BC   X   X   0   0

      //typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

      //fld.SystemMatrixSize();
      DLA::MatrixD<NonZeroPatternClass> nzPp1 =
          {{ {nDOFPDEPp1, nDOFPDEPp1}, {nDOFPDEPp1, nDOFAuPp1}, {nDOFPDEPp1, nDOFIntPp1}, {nDOFPDEPp1, nDOFBCPp1} },
           { {nDOFAuPp1 , nDOFPDEPp1}, {nDOFAuPp1 , nDOFAuPp1}, {nDOFAuPp1 , nDOFIntPp1}, {nDOFAuPp1 , nDOFBCPp1} },
           { {nDOFIntPp1, nDOFPDEPp1}, {nDOFIntPp1, nDOFAuPp1}, {nDOFIntPp1, nDOFIntPp1}, {nDOFIntPp1, nDOFBCPp1} },
           { {nDOFBCPp1 , nDOFPDEPp1}, {nDOFBCPp1 , nDOFAuPp1}, {nDOFBCPp1 , nDOFIntPp1}, {nDOFBCPp1 , nDOFBCPp1} }};

      NonZeroPatternClass& nzPDEPp1_q  = nzPp1(0,0);
      NonZeroPatternClass& nzPDEPp1_a  = nzPp1(0,1);
      NonZeroPatternClass& nzPDEPp1_qI = nzPp1(0,2);
      NonZeroPatternClass& nzPDEPp1_lg = nzPp1(0,3);

      NonZeroPatternClass& nzAuPp1_q  = nzPp1(1,0);
      NonZeroPatternClass& nzAuPp1_a  = nzPp1(1,1);
      NonZeroPatternClass& nzAuPp1_qI = nzPp1(1,2);
      NonZeroPatternClass& nzAuPp1_lg = nzPp1(1,3);

      NonZeroPatternClass& nzIntPp1_q  = nzPp1(2,0);
      NonZeroPatternClass& nzIntPp1_a  = nzPp1(2,1);
      NonZeroPatternClass& nzIntPp1_qI = nzPp1(2,2);
      //NonZeroPatternClass& nzIntPp1_lg = nzPp1(2,3);

      NonZeroPatternClass& nzBCPp1_q  = nzPp1(3,0);
      NonZeroPatternClass& nzBCPp1_a  = nzPp1(3,1);
      //NonZeroPatternClass& nzBCPp1_qI = nzPp1(3,2);
      NonZeroPatternClass& nzBCPp1_lg = nzPp1(3,3);


      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nzPDEPp1_q, nzPDEPp1_a,
                                                                                      nzAuPp1_q, nzAuPp1_a),
                                              xfld, (qfldPp1, afldPp1), quadratureOrderMin, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, nzPDEPp1_q, nzPDEPp1_a, nzPDEPp1_qI,
                                                          nzAuPp1_q ,             nzAuPp1_qI ,
                                                          nzIntPp1_q, nzIntPp1_a, nzIntPp1_qI),
          xfld, (qfldPp1, afldPp1), qIfldPp1, quadratureOrderMin, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, nzPDEPp1_q, nzPDEPp1_a, nzPDEPp1_lg,
                                                            nzAuPp1_q , nzAuPp1_a , nzAuPp1_lg ,
                                                            nzBCPp1_q , nzBCPp1_a , nzBCPp1_lg ),
          xfld, (qfldPp1, afldPp1), lgfldPp1, quadratureOrderMin, nBoundaryTraceGroups);

      // HDG adjoint solution field
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre, BoundaryGroups);

      // jacobian transpose
      //Create a transposed non-zero pattern
      DLA::MatrixD<NonZeroPatternClass> nzTPp1(Transpose(nzPp1));

      SystemMatrixClass jacTPp1(nzTPp1);
      // ND version
      auto jacTPDEPp1_q  = Transpose(jacTPp1)(0,0);
      auto jacTPDEPp1_a  = Transpose(jacTPp1)(0,1);
      auto jacTPDEPp1_qI = Transpose(jacTPp1)(0,2);
      auto jacTPDEPp1_lg = Transpose(jacTPp1)(0,3);

      auto jacTAuPp1_q  = Transpose(jacTPp1)(1,0);
      auto jacTAuPp1_a  = Transpose(jacTPp1)(1,1);
      auto jacTAuPp1_qI = Transpose(jacTPp1)(1,2);
      auto jacTAuPp1_lg = Transpose(jacTPp1)(1,3);

      auto jacTIntPp1_q  = Transpose(jacTPp1)(2,0);
      auto jacTIntPp1_a  = Transpose(jacTPp1)(2,1);
      auto jacTIntPp1_qI = Transpose(jacTPp1)(2,2);
      //auto jacIntPp1_lg  = Transpose(jacTPp1)(2,3);

      auto jacTBCPp1_q  = Transpose(jacTPp1)(3,0);
      auto jacTBCPp1_a  = Transpose(jacTPp1)(3,1);
      //auto jacTBCPp1_qI = Transpose(jacTPp1)(3,2);
      auto jacTBCPp1_lg = Transpose(jacTPp1)(3,3);

      //jac = 0;

      IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacTPDEPp1_q, jacTPDEPp1_a,
                                                                                      jacTAuPp1_q, jacTAuPp1_a),
                                              xfld, (qfldaPp1, afldaPp1), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacTPDEPp1_q, jacTPDEPp1_a, jacTPDEPp1_qI,
                                                          jacTAuPp1_q ,               jacTAuPp1_qI ,
                                                          jacTIntPp1_q, jacTIntPp1_a, jacTIntPp1_qI),
          xfld, (qfldaPp1, afldaPp1), qIfldaPp1, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jacTPDEPp1_q, jacTPDEPp1_a, jacTPDEPp1_lg,
                                                            jacTAuPp1_q , jacTAuPp1_a , jacTAuPp1_lg ,
                                                            jacTBCPp1_q , jacTBCPp1_a , jacTBCPp1_lg ),
          xfld, (qfldaPp1, afldaPp1), lgfldaPp1, quadratureOrderMin, nBoundaryTraceGroups);

      // Functional integral
      SystemVectorClass rhsPp1 = {{nDOFPDEPp1},  {nDOFAuPp1}, {nDOFIntPp1}, {nDOFBCPp1}};
      SystemVectorClass adjPp1 = {{nDOFPDEPp1},  {nDOFAuPp1}, {nDOFIntPp1}, {nDOFBCPp1}};

      rhsPp1 = 0;

      IntegrateCellGroups<TopoD1>::integrate(
          JacobianFunctionalCell<SurrealClass>( outputFcn, rhsPp1(0) ), xfld, qfldaPp1, quadratureOrder, nCellGroups );

      // compute the adjoint

      adjPp1 = solver.inverse(jacTPp1)*rhsPp1;

      // updated solution
      for (int k = 0; k < nDOFPDEPp1; k++)
        qfldaPp1.DOF(k) = adjPp1[0][k];
      for (int k = 0; k < nDOFPDEPp1; k++)
        for (int d = 0; d < PhysD1::D; d++)
          afldaPp1.DOF(k)[d] = adjPp1[1][PhysD1::D*k+d];
      for (int k = 0; k < nDOFIntPp1; k++)
        qIfldaPp1.DOF(k) = adjPp1[2][k];
      for (int k = 0; k < nDOFBCPp1; k++)
        lgfldaPp1.DOF(k) = adjPp1[3][k];

      // ---------------------
      // Adjoint exact Error
      // ---------------------
      int pmax = 7; BOOST_REQUIRE( pmax <= 7); //BasisFunctionArea_Triangle_HierarchicalPMax;;
      // HDG primal solution field
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld2Pp1(xfld, pmax, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld2Pp1(xfld, pmax, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfld2Pp1(xfld, pmax, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld2Pp1(xfld, pmax, BasisFunctionCategory_Legendre, BoundaryGroups);

      qfld.projectTo(qfld2Pp1);
      afld.projectTo(afld2Pp1);
      qIfld.projectTo(qIfld2Pp1);
      lgfld.projectTo(lgfld2Pp1);

      const int nDOFPDE2Pp1 = qfld2Pp1.nDOF();
      const int nDOFAu2Pp1  = afld2Pp1.nDOF();
      const int nDOFInt2Pp1 = qIfld2Pp1.nDOF();
      const int nDOFBC2Pp1  = lgfld2Pp1.nDOF();

      // residual

      SystemVectorClass rsd2Pp1 = {{nDOFPDE2Pp1},{nDOFAu2Pp1},{nDOFInt2Pp1},{nDOFBC2Pp1}};
      rsd2Pp1 = 0;
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldr2Pp1(xfld, pmax, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afldr2Pp1(xfld, pmax, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIfldr2Pp1(xfld, pmax, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfldr2Pp1(xfld, pmax, BasisFunctionCategory_Legendre, BoundaryGroups);


      IntegrateCellGroups<TopoD1>::integrate( ResidualCell_HDG(fcnCell, rsd2Pp1(0), rsd2Pp1(1)),
                                              xfld, (qfld2Pp1, afld2Pp1), quadratureOrder, nCellGroups );
      IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd2Pp1(0), rsd2Pp1(1), rsd2Pp1(2)),
                                                              xfld, (qfld2Pp1, afld2Pp1), qIfld2Pp1, quadratureOrder, nInteriorTraceGroups);
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsd2Pp1(0), rsd2Pp1(1), rsd2Pp1(3)),
                                                          xfld, (qfld2Pp1, afld2Pp1), lgfld2Pp1, quadratureOrderMin, nBoundaryTraceGroups );

      // Map residual to field

     for (int k = 0; k < nDOFPDE2Pp1; k++)
       qfldr2Pp1.DOF(k) = rsd2Pp1[0][k];

     for (int k = 0; k < nDOFPDE2Pp1; k++)
       for (int d = 0; d < PhysD1::D; d++)
         afldr2Pp1.DOF(k)[d] = rsd2Pp1[1][PhysD1::D*k+d];

     for (int k = 0; k < nDOFInt2Pp1; k++)
       qIfldr2Pp1.DOF(k) = rsd2Pp1[2][k];

     for (int k = 0; k < nDOFBC2Pp1; k++)
       lgfldr2Pp1.DOF(k) = rsd2Pp1[3][k];


     // Adjoint Solve in 2Pp1 - Ultimately should be removed in favour
     // of analytically evaluated, but for Cockburn do this

     // ******************************** REMOVE ME LATER  \/ \/ \/ \/ \/
     // FROM HERE DOWN

     // jacobian nonzero pattern
     //
     //        u   q  uI  lg
     //   PDE  X   X   X   X
     //   Au   X   X   X   X
     //   Int  X   X   X   0
     //   BC   X   X   0   0

     //typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

     //fld.SystemMatrixSize();
     DLA::MatrixD<NonZeroPatternClass> nz2Pp1 =
         {{ {nDOFPDE2Pp1, nDOFPDE2Pp1}, {nDOFPDE2Pp1, nDOFAu2Pp1}, {nDOFPDE2Pp1, nDOFInt2Pp1}, {nDOFPDE2Pp1, nDOFBC2Pp1} },
          { {nDOFAu2Pp1 , nDOFPDE2Pp1}, {nDOFAu2Pp1 , nDOFAu2Pp1}, {nDOFAu2Pp1 , nDOFInt2Pp1}, {nDOFAu2Pp1 , nDOFBC2Pp1} },
          { {nDOFInt2Pp1, nDOFPDE2Pp1}, {nDOFInt2Pp1, nDOFAu2Pp1}, {nDOFInt2Pp1, nDOFInt2Pp1}, {nDOFInt2Pp1, nDOFBC2Pp1} },
          { {nDOFBC2Pp1 , nDOFPDE2Pp1}, {nDOFBC2Pp1 , nDOFAu2Pp1}, {nDOFBC2Pp1 , nDOFInt2Pp1}, {nDOFBC2Pp1 , nDOFBC2Pp1} }};

     NonZeroPatternClass& nzPDE2Pp1_q  = nz2Pp1(0,0);
     NonZeroPatternClass& nzPDE2Pp1_a  = nz2Pp1(0,1);
     NonZeroPatternClass& nzPDE2Pp1_qI = nz2Pp1(0,2);
     NonZeroPatternClass& nzPDE2Pp1_lg = nz2Pp1(0,3);

     NonZeroPatternClass& nzAu2Pp1_q  = nz2Pp1(1,0);
     NonZeroPatternClass& nzAu2Pp1_a  = nz2Pp1(1,1);
     NonZeroPatternClass& nzAu2Pp1_qI = nz2Pp1(1,2);
     NonZeroPatternClass& nzAu2Pp1_lg = nz2Pp1(1,3);

     NonZeroPatternClass& nzInt2Pp1_q  = nz2Pp1(2,0);
     NonZeroPatternClass& nzInt2Pp1_a  = nz2Pp1(2,1);
     NonZeroPatternClass& nzInt2Pp1_qI = nz2Pp1(2,2);
     //NonZeroPatternClass& nzInt2Pp1_lg = nz2Pp1(2,3);

     NonZeroPatternClass& nzBC2Pp1_q  = nz2Pp1(3,0);
     NonZeroPatternClass& nzBC2Pp1_a  = nz2Pp1(3,1);
     //NonZeroPatternClass& nzBC2Pp1_qI = nz2Pp1(3,2);
     NonZeroPatternClass& nzBC2Pp1_lg = nz2Pp1(3,3);

     //timer jacPat2Pp1//timer;
     IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nzPDE2Pp1_q, nzPDE2Pp1_a,
                                                                                     nzAu2Pp1_q, nzAu2Pp1_a),
                                             xfld, (qfld2Pp1, afld2Pp1), quadratureOrderMin, nCellGroups );

     IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
         JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, nzPDE2Pp1_q, nzPDE2Pp1_a, nzPDE2Pp1_qI,
                                                         nzAu2Pp1_q ,              nzAu2Pp1_qI ,
                                                         nzInt2Pp1_q, nzInt2Pp1_a, nzInt2Pp1_qI),
         xfld, (qfld2Pp1, afld2Pp1), qIfld2Pp1, quadratureOrderMin, nInteriorTraceGroups);

     IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
         JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, nzPDE2Pp1_q, nzPDE2Pp1_a, nzPDE2Pp1_lg,
                                                           nzAu2Pp1_q , nzAu2Pp1_a , nzAu2Pp1_lg ,
                                                           nzBC2Pp1_q , nzBC2Pp1_a , nzBC2Pp1_lg ),
         xfld, (qfld2Pp1, afld2Pp1), lgfld2Pp1, quadratureOrderMin, nBoundaryTraceGroups);

     //std::cout << "jacPat2Pp1 Time: " << jacPat2Pp1//timer.elapsed() << std::endl;
     // HDG adjoint solution field
     Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qflda2Pp1(xfld, pmax, BasisFunctionCategory_Legendre);
     // auxiliary variable
     Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> aflda2Pp1(xfld, pmax, BasisFunctionCategory_Legendre);
     // interface solution
     Field_DG_InteriorTrace<PhysD1, TopoD1, ArrayQ> qIflda2Pp1(xfld, pmax, BasisFunctionCategory_Legendre);
     // Lagrange multiplier
     Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgflda2Pp1(xfld, pmax, BasisFunctionCategory_Legendre, BoundaryGroups);

     // jacobian transpose

     //Create a transposed non-zero pattern
     DLA::MatrixD<NonZeroPatternClass> nzT2Pp1(Transpose(nz2Pp1));

     SystemMatrixClass jacT2Pp1(nzT2Pp1);
     // ND version
     auto jacTPDE2Pp1_q  = Transpose(jacT2Pp1)(0,0);
     auto jacTPDE2Pp1_a  = Transpose(jacT2Pp1)(0,1);
     auto jacTPDE2Pp1_qI = Transpose(jacT2Pp1)(0,2);
     auto jacTPDE2Pp1_lg = Transpose(jacT2Pp1)(0,3);

     auto jacTAu2Pp1_q  = Transpose(jacT2Pp1)(1,0);
     auto jacTAu2Pp1_a  = Transpose(jacT2Pp1)(1,1);
     auto jacTAu2Pp1_qI = Transpose(jacT2Pp1)(1,2);
     auto jacTAu2Pp1_lg = Transpose(jacT2Pp1)(1,3);

     auto jacTInt2Pp1_q  = Transpose(jacT2Pp1)(2,0);
     auto jacTInt2Pp1_a  = Transpose(jacT2Pp1)(2,1);
     auto jacTInt2Pp1_qI = Transpose(jacT2Pp1)(2,2);
     //auto jacInt2Pp1_lg  = Transpose(jacT2Pp1)(2,3);

     auto jacTBC2Pp1_q  = Transpose(jacT2Pp1)(3,0);
     auto jacTBC2Pp1_a  = Transpose(jacT2Pp1)(3,1);
     //auto jacTBC2Pp1_qI = Transpose(jacT2Pp1)(3,2);
     auto jacTBC2Pp1_lg = Transpose(jacT2Pp1)(3,3);

     //jac = 0;
     //timer jac2Pp1//timer;
     IntegrateCellGroups<TopoD1>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacTPDE2Pp1_q, jacTPDE2Pp1_a,
                                                                                     jacTAu2Pp1_q, jacTAu2Pp1_a),
                                             xfld, (qfld2Pp1, afld2Pp1), quadratureOrder, nCellGroups );

     IntegrateInteriorTraceGroups_FieldTrace<TopoD1>::integrate(
         JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacTPDE2Pp1_q, jacTPDE2Pp1_a, jacTPDE2Pp1_qI,
                                                         jacTAu2Pp1_q ,                jacTAu2Pp1_qI ,
                                                         jacTInt2Pp1_q, jacTInt2Pp1_a, jacTInt2Pp1_qI),
         xfld, (qflda2Pp1, aflda2Pp1), qIflda2Pp1, quadratureOrder, nInteriorTraceGroups);

     IntegrateBoundaryTraceGroups_FieldTrace<TopoD1>::integrate(
         JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jacTPDE2Pp1_q, jacTPDE2Pp1_a, jacTPDE2Pp1_lg,
                                                           jacTAu2Pp1_q , jacTAu2Pp1_a , jacTAu2Pp1_lg ,
                                                           jacTBC2Pp1_q , jacTBC2Pp1_a , jacTBC2Pp1_lg ),
         xfld, (qflda2Pp1, aflda2Pp1), lgflda2Pp1, quadratureOrderMin, nBoundaryTraceGroups);

     //std::cout << "jac2Pp1 Time: " << jac2Pp1//timer.elapsed() << std::endl;

     // Functional integral

     SystemVectorClass rhs2Pp1 = {{nDOFPDE2Pp1},  {nDOFAu2Pp1}, {nDOFInt2Pp1}, {nDOFBC2Pp1}};
     SystemVectorClass adj2Pp1 = {{nDOFPDE2Pp1},  {nDOFAu2Pp1}, {nDOFInt2Pp1}, {nDOFBC2Pp1}};

     rhs2Pp1 = 0;

     IntegrateCellGroups<TopoD1>::integrate(
         JacobianFunctionalCell<SurrealClass>( outputFcn, rhs2Pp1(0) ), xfld, qflda2Pp1, quadratureOrder, nCellGroups );

     // compute the adjoint

     adj2Pp1 = solver.inverse(jacT2Pp1)*rhs2Pp1;

     // updated solution
     for (int k = 0; k < nDOFPDE2Pp1; k++)
       qflda2Pp1.DOF(k) = adj2Pp1[0][k];
     for (int k = 0; k < nDOFPDE2Pp1; k++)
       for (int d = 0; d < PhysD1::D; d++)
         aflda2Pp1.DOF(k)[d] = adj2Pp1[1][PhysD1::D*k+d];
     for (int k = 0; k < nDOFInt2Pp1; k++)
       qIflda2Pp1.DOF(k) = adj2Pp1[2][k];
     for (int k = 0; k < nDOFBC2Pp1; k++)
       lgflda2Pp1.DOF(k) = adj2Pp1[3][k];

     // ******************************** REMOVE ME LATER  ^^^^^

      // exact global and local error

      // Field Cell Types
      typedef typename XField<PhysD1, TopoD1            >::FieldCellGroupType<Line> XFieldCellGroupType;
      typedef typename Field< PhysD1, TopoD1, ArrayQ    >::FieldCellGroupType<Line> UFieldCellGroupType;
      typedef typename Field< PhysD1, TopoD1, VectorArrayQ>::FieldCellGroupType<Line> QFieldCellGroupType;

      typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;
      typedef typename UFieldCellGroupType::ElementType<> ElementUFieldClass;
      typedef typename QFieldCellGroupType::ElementType<> ElementQFieldClass;

      const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Line>(0);

      // solution - p
      UFieldCellGroupType&  qfldCell = qfld.getCellGroup<Line>(0);

      // adjoint - p
      UFieldCellGroupType&  adjqfldCell = qflda.getCellGroup<Line>(0);
      QFieldCellGroupType&  adjafldCell = aflda.getCellGroup<Line>(0);

      // residual - p+1
      UFieldCellGroupType&  rsdqfldCellPp1 = qfldrPp1.getCellGroup<Line>(0);
      QFieldCellGroupType&  rsdafldCellPp1 = afldrPp1.getCellGroup<Line>(0);

      // adjoint - p+1
      UFieldCellGroupType&  adjqfldCellPp1 = qfldaPp1.getCellGroup<Line>(0);
      QFieldCellGroupType&  adjafldCellPp1 = afldaPp1.getCellGroup<Line>(0);

      // residual - 2p+1
      UFieldCellGroupType&  rsdqfldCell2Pp1 = qfldr2Pp1.getCellGroup<Line>(0);
      QFieldCellGroupType&  rsdafldCell2Pp1 = afldr2Pp1.getCellGroup<Line>(0);

      // adjoint - 2p+1
      UFieldCellGroupType&  adjqfldCell2Pp1 = qflda2Pp1.getCellGroup<Line>(0);
      QFieldCellGroupType&  adjafldCell2Pp1 = aflda2Pp1.getCellGroup<Line>(0);

      // fields for element storage of local error estimates
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> localErrEstfld( xfld, 0, BasisFunctionCategory_Legendre );
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> localErrExtfld( xfld, 0, BasisFunctionCategory_Legendre );
      UFieldCellGroupType& localErrEstfldCell = localErrEstfld.getCellGroup<Line>(0);
      UFieldCellGroupType& localErrExtfldCell = localErrExtfld.getCellGroup<Line>(0);
      localErrEstfld = 0;
      localErrExtfld = 0;

      ElementUFieldClass localErrEstfldElem( localErrEstfldCell.basis() );
      ElementUFieldClass localErrExtfldElem( localErrExtfldCell.basis() );

      ElementXFieldClass  xfldElem( xfldCell.basis() );
      ElementUFieldClass  qfldElem( qfldCell.basis() );

      ElementUFieldClass  adjqfldElem( adjqfldCell.basis() );
      ElementQFieldClass  adjafldElem( adjafldCell.basis() );

      ElementUFieldClass  rsdqfldElemPp1( rsdqfldCellPp1.basis() );
      ElementQFieldClass  rsdafldElemPp1( rsdafldCellPp1.basis() );

      ElementUFieldClass  adjqfldElemPp1( adjqfldCellPp1.basis() );
      ElementQFieldClass  adjafldElemPp1( adjafldCellPp1.basis() );

      ElementUFieldClass  rsdqfldElem2Pp1( rsdqfldCell2Pp1.basis() );
      ElementQFieldClass  rsdafldElem2Pp1( rsdafldCell2Pp1.basis() );

      ElementUFieldClass  adjqfldElem2Pp1( adjqfldCell2Pp1.basis() );
      ElementQFieldClass  adjafldElem2Pp1( adjafldCell2Pp1.basis() );

      ElementUFieldClass  adjqfldElem2Pp1Exact( rsdqfldCell2Pp1.basis() );
      ElementQFieldClass  adjafldElem2Pp1Exact( rsdafldCell2Pp1.basis() );

      ArrayQ localSln=0;
      Real globalSln=0;

      Real globalErrEst=0;

      IntegrandClass fcnint( volumeFcn, {0} );

      for (int elem = 0; elem < xfldCell.nElem(); elem++)
      {
        xfldCell.getElement( xfldElem, elem );
        qfldCell.getElement( qfldElem, elem );

        FunctorClass fcn = fcnint.integrand(xfldElem,qfldElem);

        localSln = 0;
        ElemIntegral(fcn, xfldElem, localSln);

        globalSln += localSln;

        // local error estimate

        rsdqfldCellPp1.getElement( rsdqfldElemPp1, elem );
        rsdafldCellPp1.getElement( rsdafldElemPp1, elem );

        adjqfldCellPp1.getElement( adjqfldElemPp1, elem );
        adjafldCellPp1.getElement( adjafldElemPp1, elem );

        //cout << "elem = " << elem << endl;
        Real localErrEst = 0;
        for ( int n = 0; n < adjqfldElemPp1.nDOF(); n++ )
        {
          localErrEst += dot( rsdqfldElemPp1.DOF(n), adjqfldElemPp1.DOF(n) );
          localErrEst += dot( rsdafldElemPp1.DOF(n), adjafldElemPp1.DOF(n) );
        }

        // error in local error estimate

        // extract resid in 2p + 1
        rsdqfldCell2Pp1.getElement( rsdqfldElem2Pp1, elem );
        rsdafldCell2Pp1.getElement( rsdafldElem2Pp1, elem );

        // project adjoint from p to 2p+1
        adjqfldCell.getElement( adjqfldElem, elem );
        adjqfldElem.projectTo( adjqfldElem2Pp1 );
        adjafldCell.getElement( adjafldElem, elem );
        adjafldElem.projectTo( adjafldElem2Pp1 );

        // extract adjoint solved from 2p + 1
        adjqfldCell2Pp1.getElement( adjqfldElem2Pp1Exact, elem );
        adjafldCell2Pp1.getElement( adjafldElem2Pp1Exact, elem );

        Real localErrExt = 0;
        for ( int n = 0; n < adjqfldElem2Pp1Exact.nDOF(); n++ )
        {
          // not necessary given local galerkin orthogonality
          // but will give marginally better accuracy near machine precision
          //ArrayQ adjuDiff = adjqfldElem2Pp1Exact.DOF(n) - adjqfldElem2Pp1.DOF(n);
          //localErrExt += dot( rsdqfldElem2Pp1.DOF(n), adjuDiff );
          //rCount += dot( rsdqfldElem2Pp1.DOF(n), adjuDiff );

          // without (r, psi) as opposed to (r,psi-psi_hp)
          localErrExt += dot( rsdqfldElem2Pp1.DOF(n), adjqfldElem2Pp1Exact.DOF(n) );
        }

        for ( int n = 0; n < adjqfldElem2Pp1Exact.nDOF(); n++ )
        {
          //VectorArrayQ adjqDiff = adjafldElem2Pp1Exact.DOF(n) - adjafldElem2Pp1.DOF(n);
          //localErrExt += dot( rsdafldElem2Pp1.DOF(n), adjqDiff );

          localErrExt += dot( rsdafldElem2Pp1.DOF(n), adjafldElem2Pp1Exact.DOF(n) );
        }
        globalErrEst += localErrEst;

        localErrEstfldElem.DOF(0) = localErrEst;
        localErrEstfldCell.setElement( localErrEstfldElem, elem );

        localErrExtfldElem.DOF(0) = localErrExt;
        localErrExtfldCell.setElement( localErrExtfldElem, elem );

      }

#ifndef SANS_FULLTEST
      // Check if the Global Error Estimate from the Volume terms is equal to the sum of the Local Estimates of the Volume terms
      Real globalErrEst1 = 0;
      for (int k = 0; k < nDOFPDEPp1; k++) // loop over u
      {
        globalErrEst1 += dot( adjPp1[0][k], rsdPp1[0][k] ); // dot the u rsd with adjoint weighting
      }
      for (int k =0; k < nDOFAuPp1; k++) // loop over q
      {
        globalErrEst1 += dot( adjPp1[1][k], rsdPp1[1][k] ); // dot the q rsd with adjoint weighting
      }
      //std::cout << "Check 1: globalErrEst = " << globalErrEst << "  " << globalErrEst1 << endl;

      const Real close_tol = 1e-9;
      const Real small_tol = 1e-12;
      SANS_CHECK_CLOSE( globalErrEst1, globalErrEst, small_tol, close_tol );
#endif

      typedef typename Field<PhysD1, TopoD1, ArrayQ>::FieldTraceGroupType<Node> UFieldTraceGroupType;
      typedef typename XField<PhysD1, TopoD1>::FieldTraceGroupType<Node> XFieldTraceGroupType;
      typedef typename UFieldTraceGroupType::ElementType<> ElementUFieldTraceClass;
      typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;

      // loop over interior face groups
      for ( int group = 0; group <nInteriorTraceGroups; group++)
      {
        const XFieldTraceGroupType xfldInteriorTrace = xfld.getInteriorTraceGroup<Node>(group);

        UFieldTraceGroupType adjqIfldInteriorTrace = qIflda.getInteriorTraceGroup<Node>(group);

        UFieldTraceGroupType rsdqIfldInteriorTracePp1 = qIfldrPp1.getInteriorTraceGroup<Node>(group);
        UFieldTraceGroupType adjqIfldInteriorTracePp1 = qIfldaPp1.getInteriorTraceGroup<Node>(group);

        UFieldTraceGroupType rsdqIfldInteriorTrace2Pp1 = qIfldr2Pp1.getInteriorTraceGroup<Node>(group);
        UFieldTraceGroupType adjqIfldInteriorTrace2Pp1 = qIflda2Pp1.getInteriorTraceGroup<Node>(group);

        ElementXFieldTraceClass xfldElem( xfldInteriorTrace.basis() );

        // adjoint in p
        ElementUFieldTraceClass adjqIfldElem( adjqIfldInteriorTrace.basis() );

        // rsd projected from p to p+1, adjoint solved in p+1
        ElementUFieldTraceClass rsdqIfldElemPp1( rsdqIfldInteriorTracePp1.basis() );
        ElementUFieldTraceClass adjqIfldElemPp1( adjqIfldInteriorTracePp1.basis() );

        // rsd projected from p to 2p+1, adjoint solved in 2p+1
        ElementUFieldTraceClass rsdqIfldElem2Pp1( rsdqIfldInteriorTrace2Pp1.basis() );
        ElementUFieldTraceClass adjqIfldElem2Pp1( rsdqIfldInteriorTrace2Pp1.basis() );
        ElementUFieldTraceClass adjqIfldElem2Pp1Exact( rsdqIfldInteriorTrace2Pp1.basis() );

        for (int elem = 0; elem< xfldInteriorTrace.nElem(); elem++)
        {
          xfldInteriorTrace.getElement( xfldElem, elem );

          rsdqIfldInteriorTracePp1.getElement( rsdqIfldElemPp1, elem );
          adjqIfldInteriorTracePp1.getElement( adjqIfldElemPp1, elem );

          Real localErrEst = 0;
          for (int n=0; n< adjqIfldElemPp1.nDOF(); n++)
          {
            localErrEst += dot( rsdqIfldElemPp1.DOF(n), adjqIfldElemPp1.DOF(n) );
          }
          globalErrEst += localErrEst;

          // extract resid in 2p + 1
          rsdqIfldInteriorTrace2Pp1.getElement( rsdqIfldElem2Pp1, elem );

          // project adjoint from p to 2p +1
          adjqIfldInteriorTrace.getElement( adjqIfldElem, elem );
          adjqIfldElem.projectTo( adjqIfldElem2Pp1 );

          adjqIfldInteriorTrace2Pp1.getElement( adjqIfldElem2Pp1Exact, elem );

          Real localErrExt = 0;
          for ( int n = 0; n< adjqIfldElem2Pp1Exact.nDOF(); n++)
          {
            // not necessary given local galerkin orthogonality
            // but will give marginally better accuracy near machine precision
           //ArrayQ adjDiff = adjqIfldElem2Pp1Exact.DOF(n) - adjqIfldElem2Pp1.DOF(n);
           //localErrExt += dot( rsdqIfldElem2Pp1.DOF(n), adjDiff );

           localErrExt += dot( rsdqIfldElem2Pp1.DOF(n), adjqIfldElem2Pp1Exact.DOF(n) );
          }

          // Distribute the local error estimates to adjacent elements
          int elemL = xfldInteriorTrace.getElementLeft( elem );
          int elemR = xfldInteriorTrace.getElementRight( elem );

          localErrEstfldCell.getElement( localErrEstfldElem, elemL );
          localErrEstfldElem.DOF(0) += 0.5* localErrEst;
          localErrEstfldCell.setElement( localErrEstfldElem, elemL );

          localErrEstfldCell.getElement( localErrEstfldElem, elemR );
          localErrEstfldElem.DOF(0) += 0.5* localErrEst;
          localErrEstfldCell.setElement( localErrEstfldElem, elemR );

          localErrExtfldCell.getElement( localErrExtfldElem, elemL );
          localErrExtfldElem.DOF(0) += 0.5* localErrExt;
          localErrExtfldCell.setElement( localErrExtfldElem, elemL );

          localErrExtfldCell.getElement( localErrExtfldElem, elemR );
          localErrExtfldElem.DOF(0) += 0.5* localErrExt;
          localErrExtfldCell.setElement( localErrExtfldElem, elemR );
        } // loop over elements of trace group
      } // loop over interior trace groups


#ifndef SANS_FULLTEST // comment out for speed
      // Check if the Global Error Estimate from the Volume and interior terms
      // is equal to the sum of the Local Estimates thus far
      Real globalErrEst2 = 0;
      for (int k = 0; k < nDOFPDEPp1; k++) // loop over u
      {
        globalErrEst2 += dot( adjPp1[0][k], rsdPp1[0][k] ); // dot the u rsd with adjoint weighting
      }
      for (int k =0; k < nDOFAuPp1; k++) // loop over q
      {
        globalErrEst2 += dot( adjPp1[1][k], rsdPp1[1][k] ); // dot the q rsd with adjoint weighting
      }
      for (int k =0; k< nDOFInt; k++)
      {
        globalErrEst2 += dot( adjPp1[2][k], rsdPp1[2][k] ); // dot the u^ rsd with adjoint weighting
      }
      //std::cout << "Check 2: globalErrEst = " << globalErrEst << "  " << globalErrEst1 << endl;

      SANS_CHECK_CLOSE( globalErrEst2, globalErrEst, small_tol, close_tol );
#endif

      //boundary contributions

      for (int group = 0; group < nBoundaryTraceGroups; group++)
      {
        const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<Node>(group);

        UFieldTraceGroupType adjlgfldBoundaryTrace = lgflda.getBoundaryTraceGroup<Node>(group);

        UFieldTraceGroupType rsdlgfldBoundaryTracePp1 = lgfldrPp1.getBoundaryTraceGroup<Node>(group);
        UFieldTraceGroupType adjlgfldBoundaryTracePp1 = lgfldaPp1.getBoundaryTraceGroup<Node>(group);

        UFieldTraceGroupType rsdlgfldBoundaryTrace2Pp1 = lgfldr2Pp1.getBoundaryTraceGroup<Node>(group);
        UFieldTraceGroupType adjlgfldBoundaryTrace2Pp1 = lgflda2Pp1.getBoundaryTraceGroup<Node>(group);

        ElementXFieldTraceClass xfldElem( xfldBoundaryTrace.basis() );

        ElementUFieldTraceClass adjlgfldElem( adjlgfldBoundaryTrace.basis() );

        ElementUFieldTraceClass rsdlgfldElemPp1( rsdlgfldBoundaryTracePp1.basis() );
        ElementUFieldTraceClass adjlgfldElemPp1( adjlgfldBoundaryTracePp1.basis() );

        ElementUFieldTraceClass rsdlgfldElem2Pp1( rsdlgfldBoundaryTrace2Pp1.basis() );
        ElementUFieldTraceClass adjlgfldElem2Pp1( rsdlgfldBoundaryTrace2Pp1.basis() );
        ElementUFieldTraceClass adjlgfldElem2Pp1Exact( rsdlgfldBoundaryTrace2Pp1.basis() );

        for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
        {
          xfldBoundaryTrace.getElement( xfldElem, elem );

          rsdlgfldBoundaryTracePp1.getElement( rsdlgfldElemPp1, elem );
          adjlgfldBoundaryTracePp1.getElement( adjlgfldElemPp1, elem );

          Real localErrEst = 0;
          for (int n=0; n< adjlgfldElemPp1.nDOF(); n++)
          {
            localErrEst += dot( rsdlgfldElemPp1.DOF(n), adjlgfldElemPp1.DOF(n) );
          }
          globalErrEst += localErrEst;

          // extract resid in 2p +1
          rsdlgfldBoundaryTrace2Pp1.getElement( rsdlgfldElem2Pp1, elem );

          // project adjoint from p to 2p +1
          adjlgfldBoundaryTrace.getElement( adjlgfldElem, elem );
          adjlgfldElem.projectTo( adjlgfldElem2Pp1 );

          // extract adjoint solved from 2p+1
          adjlgfldBoundaryTrace2Pp1.getElement( adjlgfldElem2Pp1Exact, elem );

          Real localErrExt = 0;
          for ( int n = 0; n< adjlgfldElem2Pp1Exact.nDOF(); n++)
          {
            // not necessary given local galerkin orthogonality
            // but will give marginally better accuracy near machine precision
           //ArrayQ adjDiff = adjlgfldElem2Pp1Exact.DOF(n) - adjlgfldElem2Pp1.DOF(n);
           //localErrExt += dot( rsdlgfldElem2Pp1.DOF(n), adjDiff );
           //rCount += dot( rsdlgfldElem2Pp1.DOF(n), adjDiff );

           localErrExt += dot( rsdlgfldElem2Pp1.DOF(n), adjlgfldElem2Pp1Exact.DOF(n) );
          }

          // Distribute the local error estimates to adjacent elements
          int elemL = xfldBoundaryTrace.getElementLeft( elem );

          localErrEstfldCell.getElement( localErrEstfldElem, elemL );
          localErrEstfldElem.DOF(0) += localErrEst;
          localErrEstfldCell.setElement( localErrEstfldElem, elemL );

          localErrExtfldCell.getElement( localErrExtfldElem, elemL );
          localErrExtfldElem.DOF(0) += localErrExt;
          localErrExtfldCell.setElement( localErrExtfldElem, elemL );
        } // loop over elements of trace group
      } // loop over boundary trace groups

      Real avglocalErrEstError=0, avglocalErrEstEff=0;
      Real avgLocalErrExact=0,avgLocalErrEst=0;

#if 0 // actual average local estimate and effectivity
      for (int elem = 0; elem < xfldCell.nElem(); elem++)
      {
        localErrEstfldCell.getElement( localErrEstfldElem, elem );
        Real localErrEst = localErrEstfldElem.DOF(0)(0);

        localErrExtfldCell.getElement( localErrExtfldElem, elem );
        Real localErrExact = localErrExtfldElem.DOF(0)(0);

        avgLocalErrExact    += fabs(localErrExact);
        avgLocalErrEst      += fabs(localErrEst);
        avglocalErrEstError += fabs(localErrExact - localErrEst);
      }
      avgLocalErrExact    /= xfldCell.nElem();
      avgLocalErrEst      /= xfldCell.nElem();
      avglocalErrEstError /= xfldCell.nElem();
      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExact;
#else

      int elem = 0;

      localErrEstfldCell.getElement( localErrEstfldElem, elem );
      Real localErrEst = localErrEstfldElem.DOF(0);

      localErrExtfldCell.getElement( localErrExtfldElem, elem );
      Real localErrExact = localErrExtfldElem.DOF(0);

      avgLocalErrExact    = fabs(localErrExact);
      avgLocalErrEst      = fabs(localErrEst);
      avglocalErrEstError = fabs(localErrExact - localErrEst);

      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExact;
#endif

#ifndef SANS_FULLTEST //comment out for speed
      // Checks that the global Estimate evaluated all at once is the same as the sum of the locals
      Real globalErrEst3 = dot( adjPp1, rsdPp1 );
      //std::cout << "Check 3: globalErrEst = " << globalErrEst3 << "  " << globalErrEst << endl;
      SANS_CHECK_CLOSE( globalErrEst3, globalErrEst, small_tol, close_tol );
#endif

      Real globalError = globalSln - globalExt;

      //cout << " estimated error = " << globalErrEst << endl;

      hVec[indx] = 1./ii; //sqrt(2*ii*jj*(order+1)*(order+2)/2);

      globalErrorVec[indx] = abs(globalError);
      globalEstVec[indx] = abs(globalErrEst);
      globalEstErrVec[indx] = abs( globalError - globalErrEst );
      globalEffVec[indx] = abs( 1 - globalErrEst/globalError );

      localErrorVec[indx] = abs(avgLocalErrExact);
      localEstVec[indx] = abs(avgLocalErrEst);
      localEstErrVec[indx] = avglocalErrEstError;
      localEffVec[indx] = avglocalErrEstEff;

      if (indx > 0)
      {
        globalErrorRate[indx]  = (log(globalErrorVec[indx])  - log(globalErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstRate[indx]    = (log(globalEstVec[indx])    - log(globalEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstErrRate[indx] = (log(globalEstErrVec[indx]) - log(globalEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        globalEffRate[indx]    = (log(globalEffVec[indx])    - log(globalEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));

        localErrorRate[indx]  = (log(localErrorVec[indx])  - log(localErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstRate[indx]    = (log(localEstVec[indx])    - log(localEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstErrRate[indx] = (log(localEstErrVec[indx]) - log(localEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        localEffRate[indx]    = (log(localEffVec[indx])    - log(localEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
      }
      else
      {
        globalErrorRate[indx]  = 0;
        globalEstRate[indx]    = 0;
        globalEstErrRate[indx] = 0;
        globalEffRate[indx]    = 0;

        localErrorRate[indx]  = 0;
        localEstRate[indx]    = 0;
        localEstErrRate[indx] = 0;
        localEffRate[indx]    = 0;
      }

      if ( power == powermin )
      {
        cout << "P = " << order << endl;
        cout << "  \t";
        cout << " global               global               global               global              ";
        cout << " local                local                local                local" << endl;
        cout << "ii\t";
        cout << " exact                estimate             err in estimate      effectivity         ";
        cout << " exact                estimate             err in estimate      effectivity" << endl;

        resultFile << "ZONE T=\"HDG(" <<lRef<<") P=" << order << "\"" << endl;
        //pyriteFile << order << endl;
      }

      resultFile << std::setprecision(16) << std::scientific;
      resultFile << hVec[indx];
      resultFile << ", " << globalErrorVec[indx];
      resultFile << ", " << globalEstVec[indx];
      resultFile << ", " << globalEstErrVec[indx];
      resultFile << ", " << globalEffVec[indx];
      resultFile << ", " << globalErrorRate[indx];
      resultFile << ", " << globalEstRate[indx];
      resultFile << ", " << globalEstErrRate[indx];
      resultFile << ", " << globalEffRate[indx];

      resultFile << ", " << localErrorVec[indx];
      resultFile << ", " << localEstVec[indx];
      resultFile << ", " << localEstErrVec[indx];
      resultFile << ", " << localEffVec[indx];
      resultFile << ", " << localErrorRate[indx];
      resultFile << ", " << localEstRate[indx];
      resultFile << ", " << localEstErrRate[indx];
      resultFile << ", " << localEffRate[indx];
      resultFile << endl;

      //pyriteFile << hVec[indx]; // grid
      //pyriteFile << globalErrorVec[indx] << globalEstVec[indx]<< globalEstErrVec[indx]; // values
      //pyriteFile << globalErrorRate[indx]<< globalEstRate[indx]<< globalEstErrRate[indx]; // rates
      //pyriteFile << globalEffVec[indx]<< globalEffRate[indx]; //effectivities
      //pyriteFile << endl;

      cout << ii << "\t";
      cout << std::scientific;
      cout << std::scientific << std::setprecision(5) << globalErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEffRate[indx] << ")  ";

      cout << std::scientific << std::setprecision(5) << localErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEffRate[indx] << ")  ";
      cout << endl;

      indx++;

#if 0
      // Tecplot dump
      string filename = "tmp/adjDG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, adjfldAreaPp1, filename );

      filename = "tmp/qDG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, afldArea, filename );
#endif
#if 0
      string filename = "tmp/rsdPp1DG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, rsdfldAreaPp1, filename );
#endif
#if 0
      string filename = "tmp/locErrHDG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, localErrEstfldArea, filename );
#endif
//#endif // TEMP DEBUGGING
    }

    cout << endl;

  }
  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;
}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
