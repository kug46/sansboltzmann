// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateOrder_2D_HDG_Triangle_AD_btest
// testing error estimation of 2-D DG with Advection-Diffusion on triangles

//#define SANS_FULLTEST
#define GLOBAL_TAU // Undefine this to have Local Tau

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "tools/KahanSum.h"
#include "pyrite_fstream.h"

#include "Surreal/SurrealS.h"

//PDE
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"

#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

//Solve includes
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementProjection_Specialization_L2.h"

#include "Field/Function/WeightedFunctionIntegral.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/JacobianFunctionalCell_HDG.h"

#include "Discretization/IntegrateCellGroups.h"

//Linear algebra
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "Field/output_grm.h"
#include "tools/stringify.h"

// Estimation
#include "ErrorEstimate/HDG/ErrorEstimate_HDG.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEst_2D_HDG_Triangle_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEst2D_HDG_Triangle_AD )
{
  timer totaltime;
  typedef SurrealS<1> SurrealClass;

  // Primal PDE
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeFunctionLinearRobin_sansLG BCType;
  typedef BCAdvectionDiffusion<PhysD2,BCType> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  // AlgebraicEquationSet and ErrorEstimate
  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_HDG<NDPDEClass, BCNDConvertSpace, BCVector,XField<PhysD2,TopoD2>> ErrorEstimateClass;

  // Adjoint PDE
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdjoint2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdjoint2D> NDPDEAdjointClass;

  // Integrand Functors
  // call this class with something of Adjoint Exact Class
  typedef ScalarFunction2D_ForcingFunction<NDPDEAdjointClass> WeightFunctional;
  typedef SolnNDConvertSpace<PhysD2, WeightFunctional>        NDWeightFunctional;

  typedef OutputCell_WeightedSolution<PDEClass, NDWeightFunctional> WeightOutputClass;
  typedef OutputNDConvertSpace<PhysD2, WeightOutputClass>           NDWeightOutputClass;
  typedef IntegrandCell_HDG_Output<NDWeightOutputClass>             IntegrandClass;

#if 1
  // Laplacian w/ Exponential functions
  // case in Hugh's Thesis and pyrite
  typedef ScalarFunction2D_VarExp3 SolutionExact;
  typedef ScalarFunction2D_SineSine AdjointExact;

  // PDE
  Real u=0.,v=0.; // convective velocities
  Real nu = 1;
  Real nu_xx = nu, nu_xy = 0, nu_yx = 0, nu_yy = nu; // viscosity

  // Create a solution dictionary
  Real a  = 3;
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.VarExp3;
  solnArgs[SolutionExact::ParamsType::params.a] = a;

  a=1;
  PyDict adjArgs;
  adjArgs[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.SineSine;
  adjArgs[AdjointExact::ParamsType::params.a] = a;
  adjArgs[AdjointExact::ParamsType::params.b] = a;
  cout << "case: Laplacian w/ Exponentials" << endl;
#ifdef GLOBAL_TAU
  cout << "Using Global length scale for tau" << endl;
#else
  cout << "Using Local length scale for tau" << endl;
#endif

#endif

  // ND verions of solution and adjoint
  typedef SolnNDConvertSpace<PhysD2, SolutionExact>  NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD2, AdjointExact>   NDAdjointExact;

  // Primal PDE setup
  AdvectiveFlux2D_Uniform adv(u,v);
  ViscousFlux2D_Uniform visc( nu_xx, nu_xy, nu_yx, nu_yy );
  Source2D_UniformGrad source(0,0,0);

  NDSolutionExact solnExact( solnArgs ); // all solutions come from a dictionary
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );
  std::shared_ptr<SolutionExact> solutionptr( new SolutionExact(solnArgs) );
  NDPDEClass pde( adv, visc, source, forcingptr );

  // Solution BC
  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = solnArgs;

  PyDict PyBCList;
  PyBCList["SolutionBC"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["SolutionBC"] = {0, 1, 2, 3};

  NDBCClass bc( solutionptr, visc ); // using the shared_ptr interface

  // Adjoint solution
  NDAdjointExact adjExact(adjArgs); // ND version needed for calc later

  // Adjoint PDE
  AdvectiveFlux2D_Uniform adv_adj( -u, -v );

  typedef ForcingFunction2D_MMS<PDEAdjoint2D> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact));
  NDPDEAdjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  NDWeightFunctional volumeFcn( pdeadj );

  // volumeFcn is g in (g,u) g = - Del psi
  NDWeightOutputClass weightOutput( volumeFcn );
  IntegrandClass outputFcn( weightOutput, {0} );

  // linear system setup
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // See Line 11
#ifdef GLOBAL_TAU
  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );
#else
  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );
#endif


  // norm data
  const int vsize = 20;
  Real hVec[vsize];
  Real globalErrorVec[vsize], globalEstVec[vsize], globalEstErrVec[vsize], globalEffVec[vsize];
  Real localErrorVec[vsize], localEstVec[vsize], localEstErrVec[vsize], localEffVec[vsize];
  Real globalErrorRate[vsize], globalEstRate[vsize], globalEstErrRate[vsize], globalEffRate[vsize];
  Real localErrorRate[vsize], localEstRate[vsize], localEstErrRate[vsize], localEffRate[vsize];
  int indx;

  std::string file_string = "ErrorEst2D_HDG";
#ifdef GLOBAL_TAU
  file_string += "_L";
#else
  file_string += "_h";
#endif

#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/" + file_string + ".plt", std::ios::out);
  std::ofstream datFile("tmp/" + file_string + ".dat", std::ios::out);
#ifdef INTEL_MKL
  std::string pyritefilename = "IO/ErrorEstimates/" + file_string + "_AD_FullTest_Intel.txt";
#else
  std::string pyritefilename = "IO/ErrorEstimates/" + file_string + "_AD_FullTest.txt";
#endif
  pyrite_file_stream pyriteFile(pyritefilename, 1e-9, 1.4e-1, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  std::stringstream datFile;
  std::string pyritefilename = "IO/ErrorEstimates/" + file_string + "_AD_MinTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 1e-9, 1e-8, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"Global Error\"";
  resultFile << ", \"Global Error Estimate\"";
  resultFile << ", \"Global Error Estimate Error\"";
  resultFile << ", \"Global 1-Effectivity\"";
  resultFile << ", \"Global Error Rate\"";
  resultFile << ", \"Global Error Estimate Rate\"";
  resultFile << ", \"Global Error Estimate Error Rate\"";
  resultFile << ", \"Global 1-Effectivity Rate\"";
  resultFile << ", \"Local Error\"";
  resultFile << ", \"Local Error Estimate\"";
  resultFile << ", \"Local Error Estimate Error\"";
  resultFile << ", \"Local 1-Effectivity\"";
  resultFile << ", \"Local Error Rate\"";
  resultFile << ", \"Local Error Estimate Rate\"";
  resultFile << ", \"Local Error Estimate Error Rate\"";
  resultFile << ", \"Local 1-Effectivity Rate\"";
  resultFile << endl;

  // For Latex Output
  datFile << "order,"
      "h,"
      "globalError,"
      "globalErrorEstimate,"
      "globalErrorEstimateError,"
      "globalEffectivity,"
      "globalErrorRate,"
      "globalErrorEstimateRate,"
      "globalErrorEstimateErrorRate,"
      "globalEffectivityRate,"
      "localError,"
      "localErrorEstimate,"
      "localErrorEstimateError,"
      "localEffectivity,"
      "localErrorRate,"
      "localErrorEstimateRate,"
      "localErrorEstimateErrorRate,"
      "localEffectivityRate";
  datFile << endl;

  //---------------------------------------------------------------------------//
  // Computer the exact output on a fine grid and high-quadrature
  Real globalExt=0;

  {
    XField2D_Box_UnionJack_Triangle_X1 xfld( 50, 50 );

    for_each_CellGroup<TopoD2>::apply( WeightedFunctionIntegral(volumeFcn, solnExact, globalExt, {0}), xfld );

//    std::cout << "globalExt = " << std::scientific << std::setprecision(16) << globalExt << std::endl;
  }
  //---------------------------------------------------------------------------//

  // Pyrite Settings: orderinc =2, ordermax=2, ii = 4*power+4, powermax = 4;
  const int orderinc = 2;

  int ordermin = 1;
#ifndef SANS_FULLTEST
  // ordermax = 2 is used for the pyrite
  int ordermax = 2;
#else
  // ordermax = 3 is used for the pyrite
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 3;
    cout << "...running full test" << endl;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
#ifndef SANS_FULLTEST
      //ii = 2 is used for the pyrite
      ii = 2;
#else
      // ii = 8*power is used for the pyrite
      ii = 8*power;
#endif
      jj = ii;

      cout << std::scientific << std::setprecision(7);
      // grid: Hierarchical P1 ( aka X1)

      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );
      std::vector<int> InteriorTraceGroups = {0,1,2};
      std::vector<int> CellGroups = {0};
      //      int nInteriorTraceGroups = xfld.nInteriorTraceGroups();
//      int nBoundaryTraceGroups = xfld.nBoundaryTraceGroups();

      // Primal Solution in p
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);

      // interface solution
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);

      // Lagrange multiplier
      std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order,
                                                           BasisFunctionCategory_Legendre, LG_BGroup_list);

      qfld = 0; afld = 0; qIfld = 0; lgfld = 0;

      // quadrature rule
      QuadratureOrder quadratureOrder( xfld, - 1 );

      std::vector<Real> tol = {1e-12, 1e-12, 1e-12};
      // create the primal equations

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder,
                                         tol, CellGroups, InteriorTraceGroups, PyBCList, BCBoundaryGroups );

      // residual

      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

      PrimalEqSet.fillSystemVector(q);

      rsd = 0;
      PrimalEqSet.residual(q, rsd);

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());
      solver.solve(rsd, dq);

      // update solution
      q -= dq;

      // check that the residual is zero
      rsd = 0;
      PrimalEqSet.residual(q, rsd);
      std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

      bool converged = PrimalEqSet.convergedResidual(rsdNorm);

      BOOST_CHECK( converged );
      if (!converged) PrimalEqSet.printDecreaseResidualFailure( PrimalEqSet.residualNorm(rsd) );

      PrimalEqSet.setSolutionField(q);

#if 0
      // Tecplot dump
      string filename_pri = "tmp/slnHDG_P";
      filename_pri += to_string(order);
      filename_pri += "_";
      filename_pri += to_string(ii);
      filename_pri += ".plt";
      output_Tecplot( qfld, filename_pri );
#endif


#if 0
      fstream fout( "tmp/jac.dat", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

//      // Adjoint in p
//      // cell solution
//      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
//
//      // auxiliary variable
//      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld(xfld, order, BasisFunctionCategory_Legendre);
//
//      // interface solution
//      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld(xfld, order, BasisFunctionCategory_Legendre);
//
//      // Lagrange multiplier
//      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld(xfld, order,
//                                                           BasisFunctionCategory_Legendre, LG_BGroup_list);
//
//      // jacobian transpose
//
//      //Create a transposed non-zero pattern
//      SystemNonZeroPattern nzT(Transpose(nz));
//
//      SystemMatrixClass jacT(nzT);
//
//      PrimalEqSet.jacobianTranspose(q, jacT);
//
//      // adjoint solve
//      SystemVectorClass adj(q.size());
//      rsd = 0;
//
//      IntegrateCellGroups<TopoD2>::integrate(
//          JacobianFunctionalCell_HDG<SurrealClass>( outputFcn, rsd(PrimalEqSet.iPDE) ),
//          xfld, (qfld, afld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
//
//      // compute the adjoint in p
//      adj = solver.inverse(jacT)*rsd;
//
//      // set adjoint fields
//      PrimalEqSet.setAdjointField( adj, wfld, bfld, wIfld, mufld );

      // ---------------------
      // Adjoint error estimate
      // Adjoint in p+1
      // ---------------------

      // prolongated primal
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_Pp1(xfld, order + orderinc, BasisFunctionCategory_Legendre);

      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld_Pp1(xfld, order + orderinc, BasisFunctionCategory_Legendre);

      // interface solution
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld_Pp1(xfld, order + orderinc, BasisFunctionCategory_Legendre);

      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_Pp1(xfld, order + orderinc,
                                                               BasisFunctionCategory_Legendre, LG_BGroup_list);

      // prolongate the solution
      qfld.projectTo(qfld_Pp1);
      afld.projectTo(afld_Pp1);
      qIfld.projectTo(qIfld_Pp1);
      lgfld.projectTo(lgfld_Pp1);

      PrimalEquationSetClass PrimalEqSet_Pp1(xfld, qfld_Pp1, afld_Pp1, qIfld_Pp1, lgfld_Pp1, pde, disc, quadratureOrder,
                                            tol, CellGroups, InteriorTraceGroups, PyBCList, BCBoundaryGroups );

      // Adjoint in p+1 solution field
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld_Pp1(xfld, order + orderinc, BasisFunctionCategory_Legendre);

      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld_Pp1(xfld, order + orderinc, BasisFunctionCategory_Legendre);

      // interface solution
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld_Pp1(xfld, order + orderinc, BasisFunctionCategory_Legendre);

      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld_Pp1(xfld, order + orderinc,
                                                               BasisFunctionCategory_Legendre, LG_BGroup_list);
      wfld_Pp1 = 0; bfld_Pp1 = 0; wIfld_Pp1 = 0; mufld_Pp1 = 0;

      // Functional integral
      SystemVectorClass rhs_Pp1(PrimalEqSet_Pp1.vectorEqSize());
      SystemVectorClass adj_Pp1(PrimalEqSet_Pp1.vectorStateSize());
      rhs_Pp1 = 0;

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_HDG<SurrealClass>( outputFcn, rhs_Pp1(PrimalEqSet_Pp1.iPDE) ),
          xfld, (qfld_Pp1, afld_Pp1), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint
      SLA::UMFPACK<SystemMatrixClass> solverAdj_Pp1(PrimalEqSet_Pp1, SLA::TransposeSolve);
      solverAdj_Pp1.solve(rhs_Pp1, adj_Pp1);

      // update solution
      PrimalEqSet_Pp1.setAdjointField(adj_Pp1, wfld_Pp1, bfld_Pp1, wIfld_Pp1, mufld_Pp1);

#if 0
      // Tecplot dump
      string filename_adj = "tmp/adjHDG_P";
      filename_adj += to_string(order+orderinc);
      filename_adj += "_";
      filename_adj += to_string(ii);
      filename_adj += ".plt";
      output_Tecplot( wfld_Pp1, filename_adj );
#endif


      ErrorEstimateClass ErrorEstimate(xfld,
                                       qfld, afld, qIfld, lgfld, wfld_Pp1, bfld_Pp1, wIfld_Pp1, mufld_Pp1,
                                       pde, disc, quadratureOrder, {0}, {0,1,2},
                                       PyBCList, BCBoundaryGroups);

      const Field_DG_Cell<PhysD2,TopoD2,Real>& ifldEstimate = ErrorEstimate.getIField();

      // ---------------------
      // Adjoint error 'exact'
      // ---------------------
      // prolongate the primal to pmax and then evaluate residual
#ifdef SANS_FULLTEST
      int pmax = 7; BOOST_REQUIRE( 2*order+orderinc <= 7); //BasisFunctionArea_Triangle_HierarchicalPMax;;
#else
      int pmax = 2*order + orderinc;
#endif
      // Prolongated primal
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld_PMax(xfld, pmax, BasisFunctionCategory_Legendre);

      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld_PMax(xfld, pmax, BasisFunctionCategory_Legendre);

      // interface solution
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld_PMax(xfld, pmax, BasisFunctionCategory_Legendre);

      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_PMax(xfld, pmax,
                                                                BasisFunctionCategory_Legendre, LG_BGroup_list);

      // prolongate the solution
      qfld.projectTo(qfld_PMax);
      afld.projectTo(afld_PMax);
      qIfld.projectTo(qIfld_PMax);
      lgfld.projectTo(lgfld_PMax);

      PrimalEquationSetClass PrimalEqSet_PMax(xfld, qfld_PMax, afld_PMax, qIfld_PMax, lgfld_PMax, pde, disc, quadratureOrder,
                                            tol, CellGroups, InteriorTraceGroups, PyBCList, BCBoundaryGroups );

      // Adjoint in pmax solution field
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld_PMax(xfld, pmax, BasisFunctionCategory_Legendre);

      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> bfld_PMax(xfld, pmax, BasisFunctionCategory_Legendre);

      // interface solution
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> wIfld_PMax(xfld, pmax, BasisFunctionCategory_Legendre);

      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld_PMax(xfld, pmax,
                                                                BasisFunctionCategory_Legendre, LG_BGroup_list);

      // Functional integral
      SystemVectorClass rhs_PMax(PrimalEqSet_PMax.vectorEqSize());
      SystemVectorClass adj_PMax(PrimalEqSet_PMax.vectorStateSize());
      rhs_PMax = 0;

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_HDG<SurrealClass>( outputFcn, rhs_PMax(PrimalEqSet_PMax.iPDE) ),
          xfld, (qfld_PMax, afld_PMax), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint
      SLA::UMFPACK<SystemMatrixClass> solverAdj_Pmax(PrimalEqSet_PMax, SLA::TransposeSolve);
      solverAdj_Pmax.solve(rhs_PMax, adj_PMax);

      // update solution
      PrimalEqSet_PMax.setAdjointField(adj_PMax, wfld_PMax, bfld_PMax, wIfld_PMax, mufld_PMax);

      // Calculate 'Exact' errors
      ErrorEstimateClass ErrorExact(xfld,
                                    qfld, afld, qIfld, lgfld, wfld_PMax, bfld_PMax, wIfld_PMax, mufld_PMax,
                                    pde, disc, quadratureOrder, CellGroups, InteriorTraceGroups,
                                    PyBCList, BCBoundaryGroups);
      const Field_DG_Cell<PhysD2,TopoD2,Real>& ifldExact = ErrorExact.getIField();

      // global and local error
      Real globalSln=0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( outputFcn, globalSln ),
          xfld, (qfld, afld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real avglocalErrEstEff=0,globalErrEst=0;
      Real avgLocalErrExt=0, avgLocalErrEst=0,avglocalErrEstError=0;

      KahanSum<Real> sumErrEst=0;
      // Estimates
      ErrorEstimate.getErrorEstimate(globalErrEst);
      ErrorEstimate.getErrorIndicator(avgLocalErrEst);

      // 'Exact'
      ErrorExact.getErrorIndicator(avgLocalErrExt);

      // Error in local Estimate
      Field_DG_Cell<PhysD2, TopoD2, Real> ifldDiff(xfld, 0, BasisFunctionCategory_Legendre);
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldExact.nDOF() );
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldDiff.nDOF() );
      for (int i = 0; i < ifldExact.nDOF(); i++ )
      {
        ifldDiff.DOF(i) = ifldExact.DOF(i)-ifldEstimate.DOF(i);
        sumErrEst += fabs( ifldDiff.DOF(i) );
      }
      avglocalErrEstError = (Real)sumErrEst;

      const int nElem = xfld.getCellGroup<Triangle>(0).nElem();
      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExt;
      avgLocalErrExt      /= nElem;
      avgLocalErrEst      /= nElem;
      avglocalErrEstError /= nElem;

      const Real close_tol = 5e-7;
      const Real small_tol = 5e-7;
      if (power==1)
      {
        // residual field
        SystemVectorClass rsd_Pp1(PrimalEqSet_Pp1.vectorEqSize());
        rsd_Pp1 = 0;

        SystemVectorClass q_Pp1(PrimalEqSet_Pp1.vectorStateSize());
        PrimalEqSet_Pp1.fillSystemVector(q_Pp1);

        PrimalEqSet_Pp1.residual(q_Pp1, rsd_Pp1);

        Real globalErrEst2 = dot(rsd_Pp1, adj_Pp1);
        SANS_CHECK_CLOSE( globalErrEst2, globalErrEst, small_tol, close_tol );
      }

      Real globalError = globalSln - globalExt;

      hVec[indx] = 1./ii; //sqrt(nDOFPDE);

      globalErrorVec[indx] = abs(globalError);
      globalEstVec[indx] = abs(globalErrEst);
      globalEstErrVec[indx] = abs( globalError - globalErrEst );
      globalEffVec[indx] = globalEstErrVec[indx]/globalEstVec[indx];

      localErrorVec[indx] = abs(avgLocalErrExt);
      localEstVec[indx] = abs(avgLocalErrEst);
      localEstErrVec[indx] = avglocalErrEstError;
      localEffVec[indx] = avglocalErrEstEff;

      //std::cout << "Local Error Exact: " << std::scientific << std::setprecision(16) << abs(avgLocalErrExt) << std::endl;

      if (indx > 0)
      {
        globalErrorRate[indx]  = (log(globalErrorVec[indx])  - log(globalErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstRate[indx]    = (log(globalEstVec[indx])    - log(globalEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstErrRate[indx] = (log(globalEstErrVec[indx]) - log(globalEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        globalEffRate[indx]    = (log(globalEffVec[indx])    - log(globalEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));

        localErrorRate[indx]  = (log(localErrorVec[indx])  - log(localErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstRate[indx]    = (log(localEstVec[indx])    - log(localEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstErrRate[indx] = (log(localEstErrVec[indx]) - log(localEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        localEffRate[indx]    = (log(localEffVec[indx])    - log(localEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
      }
      else
      {
        globalErrorRate[indx]  = 0;
        globalEstRate[indx]    = 0;
        globalEstErrRate[indx] = 0;
        globalEffRate[indx]    = 0;

        localErrorRate[indx]  = 0;
        localEstRate[indx]    = 0;
        localEstErrRate[indx] = 0;
        localEffRate[indx]    = 0;
      }

      if ( power == powermin )
      {
        cout << "P = " << order << ", P^ = " << order + orderinc << ", P_ = " <<  orderinc << endl;
        cout << "  \t";
#ifdef GLOBAL_TAU
        cout << " global     (2P+1)    global     (2P+1)    global     (2P^+1)    global     (2P_)   ";
        cout << " local      (2P+3)    local      (2P+3)    local      (P+P^+3)   local      (P_) " << endl;
#else
        cout << " global     (2P)      global     (2P)      global     (2P^)      global     (2P_)   ";
        cout << " local      (2P+2)    local      (2P+2)    local      (P+P^+2)   local      (P_) " << endl;
#endif
        cout << "ii\t";
        cout << " exact                estimate             err in estimate       effectivity        ";
        cout << " exact                estimate             err in estimate       effectivity" << endl;

        resultFile << "ZONE T=\"DG P=" << order << "\"" << endl;
      }

      resultFile << std::setprecision(16) << std::scientific;
      resultFile << hVec[indx];
      resultFile << ", " << globalErrorVec[indx];
      resultFile << ", " << globalEstVec[indx];
      resultFile << ", " << globalEstErrVec[indx];
      resultFile << ", " << globalEffVec[indx];
      resultFile << ", " << globalErrorRate[indx];
      resultFile << ", " << globalEstRate[indx];
      resultFile << ", " << globalEstErrRate[indx];
      resultFile << ", " << globalEffRate[indx];

      resultFile << ", " << localErrorVec[indx];
      resultFile << ", " << localEstVec[indx];
      resultFile << ", " << localEstErrVec[indx];
      resultFile << ", " << localEffVec[indx];
      resultFile << ", " << localErrorRate[indx];
      resultFile << ", " << localEstRate[indx];
      resultFile << ", " << localEstErrRate[indx];
      resultFile << ", " << localEffRate[indx];
      resultFile << endl;

      // For Latex
      datFile << std::setprecision(16) << std::scientific;
      if (indx==0)
      {
        datFile << order;
      }
      else
      {
        datFile << " ";
      }
      datFile << "," << hVec[indx];
      datFile << "," << globalErrorVec[indx];
      datFile << "," << globalEstVec[indx];
      datFile << "," << globalEstErrVec[indx];
      datFile << "," << globalEffVec[indx];
      datFile << "," << globalErrorRate[indx];
      datFile << "," << globalEstRate[indx];
      datFile << "," << globalEstErrRate[indx];
      datFile << "," << globalEffRate[indx];
      datFile << "," << localErrorVec[indx];
      datFile << "," << localEstVec[indx];
      datFile << "," << localEstErrVec[indx];
      datFile << "," << localEffVec[indx];
      datFile << "," << localErrorRate[indx];
      datFile << "," << localEstRate[indx];
      datFile << "," << localEstErrRate[indx];
      datFile << "," << localEffRate[indx];
      datFile << endl;

      cout << ii << "\t";
      cout << std::scientific;
      cout << std::scientific << std::setprecision(5) << globalErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEffRate[indx] << ")  ";

      cout << std::scientific << std::setprecision(5) << localErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEffRate[indx] << ")  ";
      cout << endl;

      pyriteFile << globalErrorVec[indx];
      pyriteFile.close(1e-1) << globalErrorRate[indx];
      pyriteFile << globalEstVec[indx];
      pyriteFile.close(1e-1) << globalEstRate[indx];
      pyriteFile << globalEstErrVec[indx];
      pyriteFile.close(3) << globalEstErrRate[indx];
      pyriteFile.close(12) << globalEffVec[indx];
      pyriteFile.close(7) << globalEffRate[indx];

      pyriteFile << localErrorVec[indx];
      pyriteFile.close(1e-1) << localErrorRate[indx];
      pyriteFile << localEstVec[indx];
      pyriteFile.close(1e-1) << localEstRate[indx];
      pyriteFile << localEstErrVec[indx];
      pyriteFile.close(1e-1) << localEstErrRate[indx];
      pyriteFile << localEffVec[indx];
      pyriteFile.close(1e-1) << localEffRate[indx];
      pyriteFile << std::endl;

      indx++;

#if 0
      string filename = "tmp/locErrCG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, localErrEstfldArea, filename );
#endif
    }

    cout << endl;
  }
  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
