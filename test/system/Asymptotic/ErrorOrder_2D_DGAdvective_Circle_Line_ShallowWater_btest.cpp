// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGAdvective_Circle_Line_ShallowWater_btest
//   testing of 2-D DG Advective for shallow water equations on line grids of arc (part of circle)
////
//#define SANS_FULLTEST
//#define SANS_VERBOSE
//#define RESIDUAL_TEST  // test residual convergence for projections of the exact solution

//#define PDEShallowWater_sansgH2fluxAdvective2D 0  // does not incorporate grad H in advective flux
//#define ShallowWater_IsCircle_NotFlat 1
//// The following test case only runs when circle formulation is used in the PDEShallowWater2D class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/ShallowWater/PDEShallowWater2D.h"
#include "pde/ShallowWater/BCShallowWater2D.h"
#include "pde/ShallowWater/OutputShallowWater2D.h"
#include "pde/ShallowWater/ShallowWaterSolution2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"

#include "pde/OutputCell_FunctionalErrorSquared.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/output_Tecplot.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_Xq_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGAdvective_Circle_Line_ShallowWater_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGAdvective_Circle_Line_ShallowWater_test )
{
  // ---------- Define type/class names ---------- //
  typedef PhysD2 PhysDim;

  typedef VarTypeHVelocity2D VariableType;

  // Exact solution
//  typedef ShallowWaterSolutionFunction2D_CosineTheta SolutionClass;
  typedef ShallowWaterSolutionFunction2D_GeometricSeriesTheta SolutionClass;

  typedef SolnNDConvertSpace<PhysD2, SolutionClass> SolutionNDClass;

  // PDE class
  typedef PDEShallowWater<PhysDim,VariableType,SolutionClass> PDEClass;
  typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  // BC
  typedef BCShallowWater2DVector<VariableType,SolutionClass> BCVector;

  // Primal equation set
  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD1> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // Output error
  typedef OutputShallowWater2D_H<VariableType,PDEClass> OutputType; // solution error in H
//  typedef OutputShallowWater2D_V<VariableType,PDEClass> OutputType; // solution error in velocity magnitude
  typedef OutputCell_FunctionalErrorSquared<OutputType, SolutionClass> ErrorClass;
  typedef OutputNDConvertSpace<PhysDim, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  // ---------- Set problem parameters ---------- //
#define linearGridShallowWater 0
#define unstructuredGridShallowWater 0
  int gridOrder = 5; // grid geometric order Q. Q=1 is overridden later in the code

  const int p = 5;     // solution H polynomial degree
  const Real g = 9.81; // gravitational acceleration [m/s^2]
  const Real thetamin = 3;   // unit: degree angle
  const Real thetamax = 11.5;  // unit: degree angle； 11.5 degree ~ 0.2 radian
  const Real Fr = 2.;   // Froude number at inflow boundary

  const Real thetaL = thetamin * PI/180; // in radian

  Real HL = 1;
  for ( int k = 1; k < (p+1); k++ )
    HL += pow( thetaL, k );

  //  const Real HL = cos(thetaL); // if CosineTheta solution is used

  const Real VL = Fr * sqrt(g*HL);  // inflow speed
  const Real q0 = HL*VL; // inflow H*V

  const Real vxL = -sin(thetaL) * VL;
  const Real vyL =  cos(thetaL) * VL;

  const Real R = 3.; // circle radius

  // Exact solution
  PyDict solnArgs;
  solnArgs[SolutionClass::ParamsType::params.g] = g;
  solnArgs[SolutionClass::ParamsType::params.q0] = q0;
  solnArgs[SolutionClass::ParamsType::params.p] = p;

  SolutionNDClass solnExact( solnArgs );

  // PDE
  NDPDEClass pde( g, solnArgs, PDEClass::ShallowWater_ResidInterp_Raw );

  const int nSol = NDPDEClass::N;

  // BC

  // Create a BC dictionary
  PyDict BCInflowSupercriticalArgs;
  BCInflowSupercriticalArgs[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSupercritical;
  BCInflowSupercriticalArgs[BCShallowWater2DParams<BCTypeInflowSupercritical>::params.H] = HL;
  BCInflowSupercriticalArgs[BCShallowWater2DParams<BCTypeInflowSupercritical>::params.vx] = vxL;
  BCInflowSupercriticalArgs[BCShallowWater2DParams<BCTypeInflowSupercritical>::params.vy] = vyL;

  PyDict BCOutArgs;
  BCOutArgs[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCInflowSupercriticalArgs;
  PyBCList["BCOut"] = BCOutArgs;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCIn"] = {0};
  BCBoundaryGroups["BCOut"] = {1};

  // No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1};

  // ---------- Set solver parameters ---------- //

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NewtonSolverParam::checkInputs(NewtonSolverDict);


  // ---------- Set output error ---------- //
  OutputType output(pde);
  NDErrorClass fcnError(output, solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // norm data
  const int nrmDatSize = 10;  // size of norm data containers
  Real hVec[nrmDatSize];     // 1/nElem
  Real hDOFVec[nrmDatSize];  // 1/sqrt(DOF)
  Real normVec[nrmDatSize];  // L2 error
  int indx;                  // index of norm data entries

#ifdef RESIDUAL_TEST
  Real normVecRsd[nrmDatSize];  // PDE residual
#endif

  // Tecplot & pyrite output
#if 1
#ifdef SANS_FULLTEST
#if defined(SANS_VERBOSE) && 0
  // Tecplot
  string resultFilename = "tmp/L2_DGAdvective_Circle_Line_ShallowWater2D";
  resultFilename += "_Q";
#if linearGridShallowWater
  resultFilename += stringify(1);
#else
  resultFilename += stringify(gridOrder);
#endif
  resultFilename += ".plt";
  cout << "calling resultFilename = " << resultFilename << endl;
  std::ofstream resultFile(resultFilename, std::ios::out);
#else
  std::stringstream resultFile;
#endif
  // Pyrite
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_DGAdvective_Circle_Line_ShallowWater_FullTest.txt",
                                1e-10, 5e-4, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_DGAdvective_Circle_Line_ShallowWater_MinTest.txt",
                                1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
#ifdef RESIDUAL_TEST
  resultFile << ", \"PDE residual\"";
  resultFile << ", \"PDE residual rate\"";
#endif
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;
#endif

  // Perform grid convergence tests for various solution orders
  int ordermin = 0;  // minimum solution/polynomial order
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 4;
#else
  int ordermax = ordermin;
#endif
  // ---------- Run across solution orders & Grid refinements ---------- //
  // loop over solution order: p = order
  for ( int order = ordermin; order <= ordermax; order++ )
  {
    indx = 0;  // (re)initialized for each solution order loop

    // loop over grid resolution: 2^power
    int ii;  // number of elements
    int powermin = 2;
#ifdef SANS_FULLTEST
    int powermax = 5;
#else
    int powermax = powermin;
#endif
    // loop over grid refinement: 2^power
    for ( int power = powermin; power <= powermax; power++ )
    {
      ii = pow( 2, power );

      // grid:
      // Q1 linear grid
      std::vector<DLA::VectorS<2,Real>> coordinates_X1(ii+1);
      Real theta;
      for (int i = 0; i < ii+1; i++)
      {
        theta = PI/180.0 * ( thetamin + (thetamax - thetamin) * ( static_cast<Real>(i) / static_cast<Real>(ii) ) );
        coordinates_X1[i] = { R*cos(theta), R*sin(theta) };
      }

#if linearGridShallowWater

#if unstructuredGridShallowWater
      XField2D_Line_X1_1Group_unstructured xfld( coordinates_X1 );
#else
      XField2D_Line_X1_1Group xfld( coordinates_X1 );
#endif

#else
      // higher-order Q grid
      // Compute coordinates
      const int nDOF = ii*gridOrder+1;
      std::vector<DLA::VectorS<2,Real>> coordinates(nDOF);
      for (int iDOF = 0; iDOF < nDOF; iDOF++ )
      {
        theta = PI/180.0 * ( thetamin + (thetamax - thetamin) * ( static_cast<Real>(iDOF) / static_cast<Real>(nDOF-1) ) );
        coordinates[iDOF] = { R*cos(theta), R*sin(theta) };
      }

#if unstructuredGridShallowWater
      XField2D_Line_X1_1Group_unstructured xfld_X1( coordinates_X1 );
      XField2D_Line_Xq_1Group_unstructured xfld( xfld_X1, gridOrder, coordinates);
#else
      XField2D_Line_X1_1Group xfld_X1( coordinates_X1 );
      XField2D_Line_Xq_1Group xfld( xfld_X1, gridOrder, coordinates);
#endif

#endif

      // DG solution field
      Field_DG_Cell<PhysD2, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      // Set the initial condition
//      qfld = 0;
      // use the project of exact solution as initial solution
      for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
//      Field_DG_BoundaryTrace<PhysD2, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre );
      Field_DG_BoundaryTrace<PhysD2, TopoD1, ArrayQ>
        lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      // TODO:?
      lgfld = 0;
      const int nDOFBC = lgfld.nDOF();

      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-11, 1e-11};

      ////////////
      //SOLVE
      ////////////

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups );

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      SolveStatus status = Solver.solve(ini, sln);
      BOOST_CHECK( status.converged );
      PrimalEqSet.setSolutionField(sln);

      rsd = 0;
      PrimalEqSet.residual(sln, rsd);

#ifdef RESIDUAL_TEST
      Real rsdPDEnrmIS = 0;
      for (int n = 0; n < nDOFPDE; n++)
        for (int j = 0; j < nSol; j++)
          rsdPDEnrmIS += pow(rsd[0][n][j],2);

      normVecRsd[indx] = sqrt(rsdPDEnrmIS);
#endif

      // check that the residual is zero
      Real rsdPDEnrm[3] = { 0, 0, 0 };

      for (int n = 0; n < nDOFPDE; n++)
        for (int j = 0; j < nSol; j++)
          rsdPDEnrm[j] += pow(rsd[0][n][j],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[0]), 5e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[1]), 5e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[2]), 5e-12 );

      Real rsdBCnrm[3] = { 0, 0, 0 };
      for (int n = 0; n < nDOFBC; n++)
        for (int j = 0; j < nSol; j++)
          rsdBCnrm[j] += pow(rsd[1][n][j],2);

      BOOST_CHECK_SMALL( sqrt(rsdBCnrm[0]), 5e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdBCnrm[1]), 5e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdBCnrm[2]), 5e-12 );

      // Monitor Square Error in solution variable H (scalar)
      Real SolutionHSquareError = 0.0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorIntegrand, SolutionHSquareError ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real norm = SolutionHSquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

      // print L2 error & convergence rate
#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
        cout << "  (convergence rate = " << (log(normVec[indx-1])  - log(normVec[indx-2])) /(log(hVec[indx-1]) - log(hVec[indx-2]))
        << ")";
      cout << endl;
#endif
    } // end: grid refinement loop

    // Tecplot output
    resultFile << "ZONE T=\"DG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) / (log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
#ifdef RESIDUAL_TEST
      Real slopeRsd = 0;
      if (n > 0)
        slopeRsd = (log(normVecRsd[n])  - log(normVecRsd[n-1])) / (log(hVec[n]) - log(hVec[n-1]));
      resultFile << ", " << normVecRsd[n];
      resultFile << ", " << slopeRsd;
#endif
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n];
      pyriteFile.close(2) << slope << std::endl;
    }

  } // end: solution order loop

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
