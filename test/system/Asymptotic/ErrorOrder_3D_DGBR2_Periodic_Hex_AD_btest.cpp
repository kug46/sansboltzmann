// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_3D_DGBR2_Hex_Periodic_AD_btest
// testing of 3D DG with Advection-Diffusion on a box with tets

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "unit/UnitGrids/XField3D_Box_Hex_X1.h"

#include "tools/SANSnumerics.h"     // Real

#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/ForcingFunction3D_MMS.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#ifdef SANS_PETSC
#include "LinearAlgebra/SparseLinAlg/PETSc/PETScSolver.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_DGBR2_BoxPeriodic_Hex_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve3D_DGBR2_BoxPeriodic_Hex_AD )
{
  typedef ScalarFunction3D_SineSineSine SolutionExact;
  typedef SolnNDConvertSpace<PhysD3, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD3,  PDEClass > NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD3, TopoD3>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world; //.split(world.rank());

  // PDE

  Real a = 0.5;
  Real b = 0.5;
  Real c = 0.5;
  AdvectiveFlux3D_Uniform adv( a, b, c );

  Real nu = 1;
  ViscousFlux3D_Uniform visc( nu, 0, 0, 0, nu, 0, 0, 0, nu );

  // A small source term is needed to lock down the solution
  Source3D_UniformGrad source(0.1, 0, 0, 0);

  NDSolutionExact solnExact;

  typedef ForcingFunction3D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // All boundaries are periodic, so no BC information
  PyDict PyBCList;
  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // BR2 discretization
  Real viscousEtaParameter = Hex::NTrace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD3, ErrorClass> NDErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_3D_DGBR2_Periodic.dat", std::ios::out);
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_3D_DGBR2_Periodic_Hex_AD_FullTest.txt", 1e-9, 1e-9, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_3D_DGBR2_Periodic_Hex_AD_MinTest.txt", 1e-9, 1e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "Running full test..." << endl;
#endif
  int ordermax = 1;
#else
  int ordermax = 1;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj, kk;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 3;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );
      jj = ii;
      kk = ii;

      // grid:
      XField3D_Box_Hex_X1 xfld( comm, ii, jj, kk, {{true, true, true}} );

      // solution: Legendre, C0
      Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
      qfld = 0;
      int nDOFPDE = qfld.nDOFpossessed();
#ifdef SANS_MPI
      nDOFPDE = boost::mpi::all_reduce( *xfld.comm(), nDOFPDE, std::plus<Real>() );
#endif

      // lifting operators
      FieldLift_DG_Cell<PhysD3, TopoD3, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Hierarchical);
      rfld = 0;

      // Lagrange multiplier: Hierarchical, C0 (also at corners)

#if 0
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order );
#elif 0
      std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order );
#else
      std::vector<int> active_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld(xfld, order-1, BasisFunctionCategory_Legendre, active_BGroup_list);
#endif

      lgfld = 0;

      std::vector<int> interiorTraceGroups;
      for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
        interiorTraceGroups.push_back(i);

      // integrate product of test function and solution basis exactly
      QuadratureOrder quadratureOrder( xfld, 2*order );
      std::vector<Real> tol = {1e-11, 1e-11};
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, interiorTraceGroups, PyBCList, BCBoundaryGroups );

      // residual
      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      PrimalEqSet.fillSystemVector(q);

      rsd = 0;

      PrimalEqSet.residual(q, rsd);

#if 0 //Print residual vector
      cout <<endl << "Solution:" <<endl;
      for (int k = 0; k < nDOFPDE; k++) cout << k << "\t" << rsd[PrimalEqSet.iPDE][k] <<endl;

      cout <<endl << "BC:" <<endl;
      for (int k = 0; k < nDOFBC; k++) cout << k << "\t" << rsd[PrimalEqSet.iBC][k] << endl;
#endif

      // solve
#ifdef SANS_PETSC
      PyDict PETScDict;
      PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-16;
      PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-12;
      SLA::PETScSolverParam::checkInputs(PETScDict);
      SLA::PETScSolver<SystemMatrixClass> solver(PETScDict, PrimalEqSet);
#else
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
#endif
      SystemVectorClass dq(q.size());
      dq = 0; // set initial guess

      solver.solve(rsd, dq);

      // update solution
      q -= dq;

      // check that the residual is zero
      rsd = 0;
      PrimalEqSet.residual(q, rsd);
      std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

      bool converged = PrimalEqSet.convergedResidual(rsdNorm);
      BOOST_CHECK( converged );
      if (!converged) PrimalEqSet.printDecreaseResidualFailure( PrimalEqSet.residualNorm(rsd) );


      // L2 solution error
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD3>::integrate(
          FunctionalCell_DGBR2( errorIntegrand, SquareError ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
#ifdef SANS_MPI
      SquareError = boost::mpi::all_reduce( *xfld.comm(), SquareError, std::plus<Real>() );
#endif
      Real norm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
      {
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        Real slope = (log(normVec[indx-1]) - log(normVec[indx-2]))/(log(hVec[indx-1]) - log(hVec[indx-2]));
        cout << "  (slope = " << slope << ")";
      }
      cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/DGBR2_BoxPeriodic_Hex_P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += "x";
      filename += stringify(kk);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
#endif

    } //grid refinement loop

    // Tecplot output
    resultFile << "ZONE T=\"DG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
//    std::cout<<resultFile.str()<<std::endl;
  } //order loop
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
