// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGAdvective_Annulus_Triangle_btest
// testing of 2-D DG with Advection-Diffusion on a annulus (or part of annulus) of single-layered Triangle grid

//#define SANS_FULLTEST
//#define SANS_VERBOSE
//#define RESIDUAL_TEST  // test residual convergence for projections of the exact solution

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "unit/UnitGrids/XField2D_Annulus_Triangle_X1.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGAdvective_Annulus_Triangle_test_suite )

//----------------------------------------------------------------------------//
// solution: Legendre P0 thru P4
// Lagrange multiplier: Legendre P0 thru P4
// exact solution: sin(theta)
BOOST_AUTO_TEST_CASE( Solve2D_DGAdvective_Annulus_Triangle )
{
  // ---------- Define type/class names ---------- //
  // Exact solution
  typedef ScalarFunction2D_SineTheta SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> SolutionNDClass;
  // Fluxes
  typedef AdvectiveFlux2D_ConstRotation AdvectiveFluxClass;
  typedef ViscousFlux2D_None ViscousFluxClass;
  // Source
  typedef Source2D_None SourceClass;
  // PDE class
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFluxClass,
                                ViscousFluxClass,
                                SourceClass > PDEClass;
  // Forcing function
  typedef ForcingFunction2D_SineThetaTopo2D<PDEClass> ForcingFcnClass;

  typedef PDENDConvertSpace<PhysD2,PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  // BC
  typedef BCAdvectionDiffusion2DVector<AdvectiveFluxClass, ViscousFluxClass> BCVector;

  // Primal equation set
  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2,TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // Solution square error
  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  // ---------- Set problem parameters ---------- //
  // PDE
  const Real digitDhDR1 = 0;                // h/R_1 = 10^digitDhDR1
  const Real dhdR1 = pow( 10, digitDhDR1);  // h/R_1
  const Real digitR1 = 0;                   // R_1 = 10^digitR1
  const Real R1 = pow( 10, digitR1), R2 = R1 * (1 + dhdR1);
  const Real V = 1.0;  // uniform advection speed
  AdvectiveFluxClass adv( V );

  ViscousFluxClass visc;

  SourceClass source;

  std::shared_ptr<ForcingFcnClass> forcingptr( new ForcingFcnClass(V) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  Real thetamin = 0;    // unit: degree angle
  Real thetamax = 45;  // unit: degree angle

  // ---------- Create parameter dictionaries ---------- //
  // Exact solution
  PyDict solnArgs;
  solnArgs[SolutionExact::ParamsType::params.c] = 1.0;  // exact solution: sin(theta)
  SolutionNDClass solnExact( solnArgs );

  // BC
  PyDict SineTheta;
  SineTheta[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.Function.SineTheta;
  SineTheta[SolutionExact::ParamsType::params.c] = 1.0;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.Function] = SineTheta;

  PyDict BCNone;
  BCNone[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCSoln"] = BCSoln;
  PyBCList["BCNone"] = BCNone;

  // Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Define the BoundaryGroups for each boundary condition
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["BCSoln"] = {1,3};  // {outflow, inflow}
  BCBoundaryGroups["BCNone"] = {0,2};  // {outer arc, inner arc}

  // ---------- Miscellaneous setup before runs ---------- //
  // Solution square error class
  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // norm data
  const int nrmDatSize = 10;  // size of norm data containers
  Real hVec[nrmDatSize];      // 1/nElem
  Real hDOFVec[nrmDatSize];   // 1/sqrt(DOF)
  Real normVec[nrmDatSize];   // L2 error
  int indx;                   // index of norm data entries

#ifdef RESIDUAL_TEST
  Real normVecRsd[nrmDatSize];  // PDE residual
#endif

  // Tecplot & pyrite output
#ifdef SANS_FULLTEST
#if 0
  // Tecplot
  string resultFilename = "tmp/L2_2D_DGAdvective_Annulus_Triangle";
  resultFilename += "_R1_1e";
  resultFilename += stringify(digitR1);
  resultFilename += "_dR1Dh_1e";
  resultFilename += stringify(-digitDhDR1);
  resultFilename += ".plt";
  cout << "calling resultFilename = " << resultFilename << endl;
  std::ofstream resultFile(resultFilename, std::ios::out);
#endif
  std::stringstream resultFile;
  // Pyrite
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_DGAdvective_Annulus_Triangle_FullTest.txt",
                                1e-10, 1e-4, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_DGAdvective_Annulus_Triangle_MinTest.txt",
                                1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h=1/nElem\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
#ifdef RESIDUAL_TEST
  resultFile << ", \"PDE residual\"";
  resultFile << ", \"PDE residual rate\"";
#endif
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  // Perform grid convergence tests for various solution orders
  int ordermin = 0;  // minimum solution/polynomial order
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 4;  // maximum solution/polynomial order of a full test
#else
  int ordermax = ordermin;
#endif
  // ---------- Run across solution orders & Grid refinements ---------- //
  // loop over solution order: p = order
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;  // (re)initialized for each solution order loop

    BasisFunctionCategory BasisFunction;
    // choose basis function type
    BasisFunction = BasisFunctionCategory_Legendre;
//      BasisFunction = BasisFunctionCategory_Hierarchical;

    int ii;            // number of elements along theta
    int jj;            // number of elements along r
    int powermin = 2;
#ifdef SANS_FULLTEST
    int powermax = 5;
#else
    int powermax = powermin;
#endif
    // loop over grid resolution: 2^power
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );
      jj = pow( 2, power-2 );  // increasing jj is used for SANS pyrite tests

//      ii = pow( 2, power );
//      jj = 1;

//      ii = 16;
//      jj = pow( 2, power );

      // grid:
      XField2D_Annulus_Triangle_X1 xfld( ii, jj, R1, R2-R1, thetamin, thetamax );
#if 0
      output_Tecplot(xfld, "tmp/AnnulusTriangle_grid.dat");  // Tecplot dump grid
#endif

      // solution: Legendre, C0
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunction);
      // initial solution
//      qfld = 0;
//      // Or, use the projection of the exact solution as an initial solution
      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Legendre
#if 0
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
        lgfld( xfld, order, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#elif 0
      std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
        lgfld( xfld, order, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      // TODO: will this actually get used?
#else
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
        lgfld( xfld, order, BasisFunction, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
#endif
      lgfld = 0;
      const int nDOFBC = lgfld.nDOF();

      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-12, 1e-12};

      std::vector<int> InteriorTraceGroups;
      if ( jj==1 )
        InteriorTraceGroups = {0,1};
      else if ( jj>1 )
        InteriorTraceGroups = {0,1,2};
      else
        SANS_DEVELOPER_EXCEPTION("Invalid annulus triangle mesh because jj is unexpected");

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, InteriorTraceGroups, PyBCList, BCBoundaryGroups );

      // residual
      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      PrimalEqSet.fillSystemVector(q);
      rsd = 0;

      PrimalEqSet.residual(q, rsd);
#ifdef RESIDUAL_TEST
      Real rsdPDEnrmIS = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrmIS += pow(rsd[0][n],2);

      normVecRsd[indx] = sqrt(rsdPDEnrmIS);
#endif

      // Print residual vectors
#if 0
      std::cout <<endl << "Solution:" <<endl;
      for (int k = 0; k < nDOFPDE; k++) std::cout << k << "\t" << rsd[0][k] << std::endl;

      std::cout <<endl << "BC:" <<endl;
      for (int k = 0; k < nDOFBC; k++) std::cout << k << "\t" << rsd[1][k] << std::endl;
#endif

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());

      solver.solve(rsd, dq);

      // update solution
      q -= dq;
      PrimalEqSet.setSolutionField(q);

      // check that the residual is zero
      rsd = 0;
      PrimalEqSet.residual(q, rsd);

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdBCnrm),  1e-12 );

      //      cout << sqrt(rsdPDEnrm) << "\t" << sqrt(rsdBCnrm) <<endl;

      // L2 solution error
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_Galerkin( errorIntegrand, SquareError ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError;

      hVec[indx] = 1./(2*ii*jj);
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

      // print L2 error & convergence rate
#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << " jj = " << jj << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
      {
//        // If only ii is refined
//        cout << "  (convergence rate = " << (log(normVec[indx-1])  - log(normVec[indx-2])) /(log(hVec[indx-1]) - log(hVec[indx-2]))
//             << ")";
        // If both ii & jj are refined
        cout << "  (convergence rate = " << (log(normVec[indx-1])  - log(normVec[indx-2])) /(log(hDOFVec[indx-1]) - log(hDOFVec[indx-2]))
                     << ")";
      }
      cout << endl;
#endif

      // Tecplot dump: solution
#if 0
      string filename = "tmp/slnDG_AnnulusTriangle_P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
#endif

    } // end: grid refinement loop

    // Tecplot output: grid convergence data of solution
    resultFile << "ZONE T=\"DG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
      {
//        // If only ii is refined
//        slope = (log(normVec[n])  - log(normVec[n-1])) / (log(hVec[n]) - log(hVec[n-1]));
        // If both ii & jj are refined
        slope = (log(normVec[n])  - log(normVec[n-1])) / (log(hDOFVec[n]) - log(hDOFVec[n-1]));
      }
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
#ifdef RESIDUAL_TEST
      Real slopeRsd = 0;
      if (n > 0)
      {
//        // If only ii is refined
//        slopeRsd = (log(normVecRsd[n])  - log(normVecRsd[n-1])) / (log(hVec[n]) - log(hVec[n-1]));
        // If both ii & jj are refined
        slopeRsd = (log(normVecRsd[n])  - log(normVecRsd[n-1])) / (log(hDOFVec[n]) - log(hDOFVec[n-1]));
      }
      resultFile << ", " << normVecRsd[n];
      resultFile << ", " << slopeRsd;
#endif
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n];
      pyriteFile.close(1e-2) << slope << std::endl;
    }

    // Mathematica dump
#if 0
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
    // Mathematica dump (cut & paste)
#if 0
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  } // solution order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
