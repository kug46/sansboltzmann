// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateOrder_2D_DGAdvective_Triangle_AD_btest
// testing error estimation of 2-D Advective with Advection-Diffusion on triangles

//#define SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "tools/KahanSum.h"
#include "pyrite_fstream.h"

#include "Surreal/SurrealS.h"

//PDE
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

//Solve includes
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/Function/WeightedFunctionIntegral.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/IntegrateBoundaryTrace_Dispatch.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"


// Estimate includes
#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Galerkin_manifold.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"


#include "ErrorEstimate/DG/ErrorEstimate_DGAdvective.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/Tuple/FieldTuple.h"

//Linear algebra
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEstimateOrder_2D_DG_Triangle_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEstimateOrder_2D_DG_Triangle_AD )
{
  timer totaltime;
  // Case used in Hugh's Masters Thesis - ALSO PYRITE SETTINGS
  // Case 1 - primal [a=1;b=1;c=1;d=1;]       - adjoint [a=1;b=-2;c=-2;d=1;]
  typedef ScalarFunction2D_VarExp3 SolutionExact;
  typedef ScalarFunction2D_SineSine AdjointExact;

  // ND Exacts
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD2, AdjointExact>  NDAdjointExact;

  // Primal PDE
  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_None,
      Source2D_None > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef BCTypeFunction_mitStateParam BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_None> BCVector;

  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
      AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // Adjoint PDE
  typedef PDEAdvectionDiffusion<PhysD2,
      AdvectiveFlux2D_Uniform,
      ViscousFlux2D_None,
      Source2D_None > PDEAdjoint2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdjoint2D> PDEAjointClass;

  // Integrand Functors
  typedef ScalarFunction2D_ForcingFunction<PDEAjointClass> WeightFunctional;
  typedef SolnNDConvertSpace<PhysD2, WeightFunctional>    NDWeightFunctional;

  typedef OutputCell_WeightedSolution<PDEClass, WeightFunctional> WeightOutputClass;
  typedef OutputNDConvertSpace<PhysD2, WeightOutputClass>        NDWeightOutputClass;
  typedef IntegrandCell_Galerkin_Output<NDWeightOutputClass>      IntegrandClass;

  // Estimation
  typedef ErrorEstimate_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector, XField<PhysD2,TopoD2>> ErrorEstimateClass;

  // PDE
  Real u=2.;
  Real v=0.1;
  AdvectiveFlux2D_Uniform adv( u, v );

  ViscousFlux2D_None visc;

  Source2D_None source;

  Real a=3;

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;

  NDSolutionExact solnExact(solnArgs);

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );
  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC

  PyDict SolnBC;
  SolnBC[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.VarExp3;
  SolnBC[SolutionExact::ParamsType::params.a] = a;

  PyDict BC_Dirichlet_Soln;
  BC_Dirichlet_Soln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = SolnBC;
  BC_Dirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.SolutionBCType] = "Dirichlet";

  PyDict BC_None;
  BC_None[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCNone"] = BC_None;
  PyBCList["BCDirichlet_Soln"] = BC_Dirichlet_Soln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCNone"] = {XField2D_Box_UnionJack_Triangle_X1::iRight,
                                XField2D_Box_UnionJack_Triangle_X1::iTop};
  BCBoundaryGroups["BCDirichlet_Soln"] = {XField2D_Box_UnionJack_Triangle_X1::iBottom,
                                          XField2D_Box_UnionJack_Triangle_X1::iLeft};

  // Adjoint PDE
  AdvectiveFlux2D_Uniform adv_adj( -u, -v );

  a=1;
  NDAdjointExact adjExact(a,a);

  typedef ForcingFunction2D_MMS<PDEAdjoint2D> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact));
  PDEAjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  NDWeightFunctional volumeFcn( pdeadj );

  // volumeFcn is g in (g,u) g = - Del psi
  NDWeightOutputClass weightOutput( volumeFcn );
  IntegrandClass outputFcn( weightOutput, {0} );


  // linear system setup
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // norm data
  const int vsize = 10;
  Real hVec[vsize];
  Real globalErrorVec[vsize], globalEstVec[vsize], globalEstErrVec[vsize], globalEffVec[vsize];
  Real localErrorVec[vsize], localEstVec[vsize], localEstErrVec[vsize], localEffVec[vsize];
  Real globalErrorRate[vsize], globalEstRate[vsize], globalEstErrRate[vsize], globalEffRate[vsize];
  Real localErrorRate[vsize], localEstRate[vsize], localEstErrRate[vsize], localEffRate[vsize];
  int indx;

#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/ErrorEst2D_DGAdvective.plt", std::ios::out);
  std::ofstream datFile("tmp/ErrorEst2D_DGAdvective.dat", std::ios::out);
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_2D_DGAdvective_AD_FullTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-9, 8e-2, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  std::stringstream datFile;
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_2D_DGAdvective_AD_MinTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 5e-9, 5e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"Global Error\"";
  resultFile << ", \"Global Error Estimate\"";
  resultFile << ", \"Global Error Estimate Error\"";
  resultFile << ", \"Global 1-Effectivity\"";
  resultFile << ", \"Global Error Rate\"";
  resultFile << ", \"Global Error Estimate Rate\"";
  resultFile << ", \"Global Error Estimate Error Rate\"";
  resultFile << ", \"Global 1-Effectivity Rate\"";
  resultFile << ", \"Local Error\"";
  resultFile << ", \"Local Error Estimate\"";
  resultFile << ", \"Local Error Estimate Error\"";
  resultFile << ", \"Local 1-Effectivity\"";
  resultFile << ", \"Local Error Rate\"";
  resultFile << ", \"Local Error Estimate Rate\"";
  resultFile << ", \"Local Error Estimate Error Rate\"";
  resultFile << ", \"Local 1-Effectivity Rate\"";
  resultFile << endl;

  // For Latex Output
  datFile << "order,"
      "h,"
      "globalError,"
      "globalErrorEstimate,"
      "globalErrorEstimateError,"
      "globalEffectivity,"
      "globalErrorRate,"
      "globalErrorEstimateRate,"
      "globalErrorEstimateErrorRate,"
      "globalEffectivityRate,"
      "localError,"
      "localErrorEstimate,"
      "localErrorEstimateError,"
      "localEffectivity,"
      "localErrorRate,"
      "localErrorEstimateRate,"
      "localErrorEstimateErrorRate,"
      "localEffectivityRate";
  datFile << endl;

  //int quadorder=-1;
  //ElementIntegral <TopoD2, Triangle, ArrayQ> ElemIntegral(quadorder);

  //---------------------------------------------------------------------------//
  // Computer the exact output on a fine grid and high-quadrature
  Real globalExt=0;

  {
    XField2D_Box_UnionJack_Triangle_X1 xfld( 100, 100 );

    for_each_CellGroup<TopoD2>::apply( WeightedFunctionIntegral(volumeFcn, solnExact, globalExt, {0}), xfld );
    std::cout << "globalExt = " << std::scientific << std::setprecision(16) << globalExt << std::endl;
  }
  //---------------------------------------------------------------------------//

  const int orderinc = 2;


  int ordermin = 1;
#ifndef SANS_FULLTEST
  // ordermax = 3 is used for the pyrite
  int ordermax = 2;
#else
  // ordermax = 3 is used for the pyrite
  int ordermax = 2;
#endif

  int count = 0;
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 0;
#ifdef SANS_FULLTEST
    int powermax = 3;
    cout << "...running full test" << endl;
#else
    int powermax = 0;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      //ii = pow( 2, power );
#ifndef SANS_FULLTEST
      //ii = 2 is used for the pyrite
      ii = 2;
#else
      // ii = 4*power + 4 is used for the pyrite
      ii = 4*power+4;
#endif
      //ii = 2;
      jj = ii;

      cout << std::scientific << std::setprecision(7);
      // grid: Hierarchical P1 ( aka X1)

      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

      // Primal Solution in p
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
      std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order-1, BasisFunctionCategory_Legendre, LG_BGroup_list );

      qfld = 0; lgfld = 0;

      //      std::cout<< "LG_BGroup_list = ";
      //      for (auto it = LG_BGroup_list.begin(); it != LG_BGroup_list.end(); ++it)
      //        std::cout<< *it;
      //      std::cout<< std::endl;

      // quadrature rule
      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-11, 1e-11};

      // create the primal equations
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_L2, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      // residual

      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());


      PrimalEqSet.fillSystemVector(q);

      rsd = 0;
      PrimalEqSet.residual(q, rsd);

#if 0
      typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;
      // jacobian nonzero pattern
      SystemNonZeroPattern nz(PrimalEqSet.matrixSize());
      PrimalEqSet.jacobian(nz);

      // jacobian
      SystemMatrixClass jac(nz);
      jac = 0;
      PrimalEqSet.jacobian(jac);

      fstream fout( "tmp/jac" + std::to_string(count) + ".mtx", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());
      dq = 0;
      solver.solve(rsd, dq);

      // update solution
      q -= dq;

      PrimalEqSet.setSolutionField(q);


//      output_Tecplot( qfld, "tmp/qfld_tmp.plt");

      // Adjoint in p
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Hierarchical);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld( xfld, order-1, BasisFunctionCategory_Legendre, LG_BGroup_list );

      int nDOFPDE = wfld.nDOF(), nDOFBC = mufld.nDOF();

      // adjoint solve
      SystemVectorClass adj(q.size());
      rsd = 0;
      adj = 0;

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_Galerkin( outputFcn, rsd(0) ),
          xfld, wfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint in p
      SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);
      solverAdj.solve(rsd, adj);

      // update the adjoint solution
      for (int k = 0; k < nDOFPDE; k++)
        wfld.DOF(k) = adj[0][k];
      for (int k = 0; k < nDOFBC; k++)
        mufld.DOF(k) = adj[1][k];


      // ---------------------
      // Adjoint error estimate
      // Adjoint in p+1
      // ---------------------

      // GLOBAL ESTIMATE
      // This is currently the old hack, just to perform the Global Estimate for verification of the local sum
      // prolongated primal to p+1
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldPp1(xfld, order+orderinc, BasisFunctionCategory_Hierarchical);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfldPp1( xfld, order+orderinc-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
      const int nDOFPDEPp1 = qfldPp1.nDOF(), nDOFBCPp1 = lgfldPp1.nDOF();

      // prolongate the solution
      qfld.projectTo(qfldPp1);
      lgfld.projectTo(lgfldPp1);

      PrimalEquationSetClass PrimalEqSetPp1(xfld, qfldPp1, lgfldPp1, pde, quadratureOrder,
                                            ResidualNorm_L2, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      // residual field
      SystemVectorClass qPp1(PrimalEqSetPp1.vectorStateSize());
      SystemVectorClass rsdWeakPp1(PrimalEqSetPp1.vectorEqSize());
      rsdWeakPp1 =0;
      PrimalEqSetPp1.fillSystemVector(qPp1);

      // evaluate the weak form residual in p+1 - For validation of the sum of the local
      PrimalEqSetPp1.residual(qPp1, rsdWeakPp1);

      // LOCAL ESTIMATE

      // Adjoint in p+1 solution field
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfldPp1(xfld, order+orderinc, BasisFunctionCategory_Hierarchical);

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfldProlongateP(xfld, order+orderinc, BasisFunctionCategory_Hierarchical); // for the prolongated p
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufldPp1( xfld, order+orderinc-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufldProlongateP( xfld, order+orderinc-1, BasisFunctionCategory_Legendre, LG_BGroup_list );

      wfld.projectTo(wfldProlongateP); // prolongate the p adjoint to subtract from the p+1
      mufld.projectTo(mufldProlongateP);

      // Functional integral
      SystemVectorClass rhsPp1(qPp1.size());
      SystemVectorClass adjPp1(qPp1.size());
      rhsPp1 = 0;
      adjPp1 = 0;

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_Galerkin( outputFcn, rhsPp1(0) ),
          xfld, wfldPp1, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint
      SLA::UMFPACK<SystemMatrixClass> solverAdjPp1(PrimalEqSetPp1, SLA::TransposeSolve);
      solverAdjPp1.solve(rhsPp1, adjPp1);

      // update the solution  - subtract the p solution at the same time
      for (int k = 0; k < nDOFPDEPp1; k++)
        wfldPp1.DOF(k) = adjPp1[0][k] - wfldProlongateP.DOF(k);
      for (int k = 0; k < nDOFBCPp1; k++)
        mufldPp1.DOF(k) = adjPp1[1][k];// - mufldProlongateP.DOF(k);

      // Calling the Error Estimate functions
      ErrorEstimateClass ErrorEstimate( xfld,
                                        qfld, lgfld, wfldPp1, mufldPp1,
                                        pde, quadratureOrder, {0}, {0,1,2},
                                        PyBCList, BCBoundaryGroups);

      const Field_DG_Cell<PhysD2,TopoD2,Real>& ifldEstimate = ErrorEstimate.getIField();

      //      ErrorEstimate.outputFields( "tmp/efld.plt" );
      // ---------------------
      // Adjoint 'exact' error
      // ---------------------
      // prolongate the primal to pmax and then evaluate strong residual

#ifdef SANS_FULLTEST
      int pmax = 7; BOOST_REQUIRE( 2*order+orderinc <= 7); //BasisFunctionArea_Triangle_HierarchicalPMax;;
#else
      int pmax = 7; BOOST_REQUIRE( 2*order+1 <= 7);
#endif

      // GLOBAL ESTIMATE
      // This is currently the old hack, just to perform the Global Estimate for verification of the local sum
      // prolongated primal to p+1
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldPmax(xfld, pmax, BasisFunctionCategory_Hierarchical);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfldPmax( xfld, pmax-1, BasisFunctionCategory_Legendre, LG_BGroup_list );

      const int nDOFPDEPmax = qfldPmax.nDOF(), nDOFBCPmax = lgfldPmax.nDOF();

      // prolongate the solution
      qfld.projectTo(qfldPmax);
      lgfld.projectTo(lgfldPmax);

      PrimalEquationSetClass PrimalEqSetPmax(xfld, qfldPmax, lgfldPmax, pde,
                                             quadratureOrder, ResidualNorm_L2, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      // residual field
      SystemVectorClass qPmax(PrimalEqSetPmax.vectorStateSize());

      PrimalEqSetPmax.fillSystemVector(qPmax);

      // Have to use the p7 solve hack because CG L2 project is a global operation

      // Adjoint in pmax solution field
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfldPmax(xfld, pmax, BasisFunctionCategory_Hierarchical);
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfldProlongatePmax(xfld, pmax, BasisFunctionCategory_Hierarchical);

      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufldPmax( xfld, pmax-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufldProlongatePmax( xfld, pmax-1, BasisFunctionCategory_Legendre, LG_BGroup_list );


      wfld.projectTo(wfldProlongatePmax); // prolongate the p adjoint to subtract from the pmax
      mufld.projectTo(mufldProlongatePmax);

      // Functional integral
      SystemVectorClass rhsPmax(qPmax.size());
      SystemVectorClass adjPmax(qPmax.size());
      rhsPmax = 0;

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_Galerkin( outputFcn, rhsPmax(0) ),
          xfld, wfldPmax, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint
      SLA::UMFPACK<SystemMatrixClass> solverAdjPmax(PrimalEqSetPmax, SLA::TransposeSolve);
      solverAdjPmax.solve(rhsPmax, adjPmax);

      // update the solution - subtract the p solution at the same time
      for (int k = 0; k < nDOFPDEPmax; k++)
        wfldPmax.DOF(k) = adjPmax[0][k]- wfldProlongatePmax.DOF(k);
      for (int k = 0; k < nDOFBCPmax; k++)
        mufldPmax.DOF(k) = adjPmax[1][k];// - mufldProlongatePmax.DOF(k);

      // Calling the Error Estimate functions
      ErrorEstimateClass ErrorExact(xfld,
                                    qfld, lgfld, wfldPmax, mufldPmax,
                                    pde, quadratureOrder, {0}, {0,1,2},
                                    PyBCList,
                                    BCBoundaryGroups);
      const Field_DG_Cell<PhysD2,TopoD2,Real>& ifldExact = ErrorExact.getIField();


      // Field Cell Types
      typedef typename XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;

      const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Triangle>(0);

      Real globalSln=0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_Galerkin( outputFcn, globalSln ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real avglocalErrEstEff=0,globalErrEst=0;
      Real avgLocalErrExt=0, avgLocalErrEst=0,avglocalErrEstError=0;

      KahanSum<Real> sumErrEst=0;
      // Estimates
      ErrorEstimate.getErrorEstimate(globalErrEst);
      ErrorEstimate.getErrorIndicator(avgLocalErrEst);

      // 'Exact'
      ErrorExact.getErrorIndicator(avgLocalErrExt);

      // Error in local Estimate
      Field_DG_Cell<PhysD2, TopoD2, Real> ifldDiff(xfld, 0, BasisFunctionCategory_Legendre);
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldExact.nDOF() );
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldDiff.nDOF() );
      for (int i = 0; i < ifldExact.nDOF(); i++ )
      {
        ifldDiff.DOF(i) = ifldExact.DOF(i)-ifldEstimate.DOF(i);
        sumErrEst += fabs( ifldDiff.DOF(i) );
      }
      avglocalErrEstError = (Real)sumErrEst;

      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExt;
      avgLocalErrExt      /= xfldCell.nElem();
      avgLocalErrEst      /= xfldCell.nElem();
      avglocalErrEstError /= xfldCell.nElem();

      // Computing the estimate using the global weak form

      // residual field
      SystemVectorClass rsdPp1(PrimalEqSetPp1.vectorEqSize());
      rsdPp1 = 0;

      // Recompute lifting operators
      SystemVectorClass qPp1_post(PrimalEqSetPp1.vectorStateSize());
      PrimalEqSetPp1.fillSystemVector(qPp1_post);

      PrimalEqSetPp1.residual(qPp1_post, rsdPp1);

      Real globalErrEst2 = dot(rsdPp1, adjPp1);

      const Real close_tol = 5e-7;
      const Real small_tol = 5e-7;
      SANS_CHECK_CLOSE( globalErrEst2, globalErrEst, small_tol, close_tol );


      Real globalError = globalSln - globalExt;

      hVec[indx] = 1./ii; //sqrt(nDOFPDE);

      globalErrorVec[indx] = abs(globalError);
      globalEstVec[indx] = abs(globalErrEst);
      globalEstErrVec[indx] = abs( globalError - globalErrEst );
      globalEffVec[indx] = globalEstErrVec[indx]/globalEstVec[indx];

      localErrorVec[indx] = abs(avgLocalErrExt);
      localEstVec[indx] = abs(avgLocalErrEst);
      localEstErrVec[indx] = avglocalErrEstError;
      localEffVec[indx] = avglocalErrEstEff;

      if (indx > 0)
      {
        globalErrorRate[indx]  = (log(globalErrorVec[indx])  - log(globalErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstRate[indx]    = (log(globalEstVec[indx])    - log(globalEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstErrRate[indx] = (log(globalEstErrVec[indx]) - log(globalEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        globalEffRate[indx]    = (log(globalEffVec[indx])    - log(globalEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));

        localErrorRate[indx]  = (log(localErrorVec[indx])  - log(localErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstRate[indx]    = (log(localEstVec[indx])    - log(localEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstErrRate[indx] = (log(localEstErrVec[indx]) - log(localEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        localEffRate[indx]    = (log(localEffVec[indx])    - log(localEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
      }
      else
      {
        globalErrorRate[indx]  = 0;
        globalEstRate[indx]    = 0;
        globalEstErrRate[indx] = 0;
        globalEffRate[indx]    = 0;

        localErrorRate[indx]  = 0;
        localEstRate[indx]    = 0;
        localEstErrRate[indx] = 0;
        localEffRate[indx]    = 0;
      }

      if ( power == powermin )
      {
        cout << "P = " << order << ", P^ = " << order + orderinc << ", P_ = " <<  orderinc << endl;
        cout << "  \t";
        cout << " global     (2P+1)      global     (2P+1)      global     (2P^+1)     global     (2P_)    ";
        cout << " local      (2P+3)    local      (2P+3)    local      (P+P^+3)  local      (P_) " << endl;
        cout << "ii\t";
        cout << " exact                estimate             err in estimate      effectivity         ";
        cout << " exact                estimate             err in estimate      effectivity" << endl;

        resultFile << "ZONE T=\"CG P=" << order << "\"" << endl;
      }

      resultFile << std::setprecision(16) << std::scientific;
      resultFile << hVec[indx];
      resultFile << ", " << globalErrorVec[indx];
      resultFile << ", " << globalEstVec[indx];
      resultFile << ", " << globalEstErrVec[indx];
      resultFile << ", " << globalEffVec[indx];
      resultFile << ", " << globalErrorRate[indx];
      resultFile << ", " << globalEstRate[indx];
      resultFile << ", " << globalEstErrRate[indx];
      resultFile << ", " << globalEffRate[indx];

      resultFile << ", " << localErrorVec[indx];
      resultFile << ", " << localEstVec[indx];
      resultFile << ", " << localEstErrVec[indx];
      resultFile << ", " << localEffVec[indx];
      resultFile << ", " << localErrorRate[indx];
      resultFile << ", " << localEstRate[indx];
      resultFile << ", " << localEstErrRate[indx];
      resultFile << ", " << localEffRate[indx];
      resultFile << endl;

      // For Latex
      datFile << std::setprecision(16) << std::scientific;
      if (indx==0)
      {
        datFile << order;
      }
      else
      {
        datFile << " ";
      }
      datFile << "," << hVec[indx];
      datFile << "," << globalErrorVec[indx];
      datFile << "," << globalEstVec[indx];
      datFile << "," << globalEstErrVec[indx];
      datFile << "," << globalEffVec[indx];
      datFile << "," << globalErrorRate[indx];
      datFile << "," << globalEstRate[indx];
      datFile << "," << globalEstErrRate[indx];
      datFile << "," << globalEffRate[indx];
      datFile << "," << localErrorVec[indx];
      datFile << "," << localEstVec[indx];
      datFile << "," << localEstErrVec[indx];
      datFile << "," << localEffVec[indx];
      datFile << "," << localErrorRate[indx];
      datFile << "," << localEstRate[indx];
      datFile << "," << localEstErrRate[indx];
      datFile << "," << localEffRate[indx];
      datFile << endl;

      cout << ii << "\t";
      cout << std::scientific;
      cout << std::scientific << std::setprecision(5) << globalErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEffRate[indx] << ")  ";

      cout << std::scientific << std::setprecision(5) << localErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEffRate[indx] << ")  ";
      cout << endl;

      pyriteFile << globalErrorVec[indx];
      pyriteFile.close(5e-1) << globalErrorRate[indx];
      pyriteFile << globalEstVec[indx];
      pyriteFile.close(5e-1) << globalEstRate[indx];
      pyriteFile << globalEstErrVec[indx];
      pyriteFile.close(5e-1) << globalEstErrRate[indx];
      pyriteFile << globalEffVec[indx];
      pyriteFile.close(5.e-1) << globalEffRate[indx];

      pyriteFile << localErrorVec[indx];
      pyriteFile.close(5e-1) << localErrorRate[indx];
      pyriteFile << localEstVec[indx];
      pyriteFile.close(5e-1) << localEstRate[indx];
      pyriteFile << localEstErrVec[indx];
      pyriteFile.close(5e-1) << localEstErrRate[indx];
      pyriteFile << localEffVec[indx];
      pyriteFile.close(5e-1) << localEffRate[indx];
      pyriteFile << std::endl;

      indx++;
      count = count+1;
    }

    cout << endl;
  }
  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
