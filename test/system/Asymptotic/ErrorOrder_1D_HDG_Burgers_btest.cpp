// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_1D_HDG_Burgers_btest
// testing of 1-D HDG with Burgers

//#define SANS_FULLTEST
#define SANS_VERBOSE
//#define DUMP_TMP_FILES

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "pde/Burgers/PDEBurgers1D.h"
#include "pde/Burgers/BCBurgers1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/Burgers/BurgersConservative1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"
#include "pde/ForcingFunction1D_MMS.h"

#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve1D_HDG_Burgers_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_HDG_Burgers1D )
{
  typedef BurgersConservative1D QType;
  typedef PDEBurgers<PhysD1,
                     QType,
                     ViscousFlux1D_Uniform,
                     Source1D_Uniform > PDEBurgers1D;
  typedef PDENDConvertSpace<PhysD1, PDEBurgers1D> NDPDEClass;
  typedef PDEBurgers1D::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  typedef ScalarFunction1D_Tanh SolutionExact;
  typedef SolnNDConvertSpace<PhysD1, SolutionExact> NDSolutionExact;

  typedef BCTypeFunction_mitStateParam BCType;
  typedef BCBurgers1DVector<QType, ViscousFlux1D_Uniform, Source1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> AlgebraicEquationSet_HDGClass;
  typedef AlgebraicEquationSet_HDGClass::BCParams BCParams;

  Real nu = 0.1;
  Real b =  0.2;
  Real A = 2.0;
  Real B = 0.0;

  ScalarFunction1D_Tanh soln(nu, A, B);
  NDSolutionExact NDsolnExact(nu, A, B);

  // PDE
  ViscousFlux1D_Uniform visc(nu);
  ScalarFunction1D_Tanh MMS_soln(nu, A, B);
  typedef ForcingFunction1D_MMS<PDEBurgers1D> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(MMS_soln) );

  Source1D_Uniform source(b);

  NDPDEClass pde( visc, source, forcingptr ); // solution as argument means a forcing function is generated

  // BC
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(nu, A ,B) );

  // Create a Solution dictionary
  PyDict Tanh;
  Tanh[BCBurgersParams<PhysD1,BCType>::params.Function.Name] =
      BCBurgersParams<PhysD1,BCType>::params.Function.Tanh;
  Tanh[SolutionExact::ParamsType::params.nu] = nu;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCBurgersParams<PhysD1,BCType>::params.Function] = Tanh;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef AlgebraicEquationSet_HDGClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_HDGClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  typedef OutputCell_SolutionErrorSquared<NDPDEClass, SolutionExact> OutputSolutionErrorSquaredClass;
  typedef OutputNDConvertSpace<PhysD1, OutputSolutionErrorSquaredClass> NDOutputSolutionErrorSquaredClass;
  typedef IntegrandCell_HDG_Output<NDOutputSolutionErrorSquaredClass> IntegrandSquareErrorClass;

  const std::vector<int> BoundaryGroups = {0,1};

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  // integrands
  NDOutputSolutionErrorSquaredClass outErr( NDsolnExact );
  IntegrandSquareErrorClass fcnErr( outErr, {0} );

  // convergence flag
  bool converged = false;

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
#if defined(DUMP_TMP_FILES)
  std::ofstream resultFile("tmp/L2_1D_HDG_Burger.plt", std::ios::out);
#endif
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_HDG_Burgers_FullTest.txt", 1e-9, 1e-6, pyrite_file_stream::check);
#else
//  std::stringstream resultFile;
#if defined(DUMP_TMP_FILES)
  std::ofstream resultFile("tmp/L2_1D_HDG_Burger.plt", std::ios::out);
#endif
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_1D_HDG_Burgers_MinTest.txt", 1e-9, 1e-9, pyrite_file_stream::check);
#endif

#if defined(DUMP_TMP_FILES)
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
#endif

  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 4;
#else
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 15*factor
    int ii;
    int factormin = 1;
#ifdef SANS_FULLTEST
    int factormax = 4;
#else
    int factormax = 3;
#endif
    for (int factor = factormin; factor <= factormax; factor++)
    {
      ii = 15*factor;

      // grid:
      XField1D xfld( ii, -1, 1 );

      // HDG solution field
      // cell solution
      Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      qfld = 0;
      // auxiliary variable
      Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
      afld = 0;
      // interface solution
      Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
      qIfld = 0;
      // Lagrange multiplier
      std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, LG_BGroup_list);
      lgfld = 0;

      const int nDOFPDE = qfld.nDOF();

      QuadratureOrder quadratureOrder( xfld, -1 );
      std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

      AlgebraicEquationSet_HDGClass AlgEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                             {0}, {0}, PyBCList, BCBoundaryGroups );

      // Newton Solver set up
      PyDict NewtonSolverDict, UMFPACKDict, ScalarGoldenSearchLineUpdateDict;
      UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

      ScalarGoldenSearchLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] =
          LineUpdateParam::params.LineUpdate.ScalarGoldenSearch;

      NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
      NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = ScalarGoldenSearchLineUpdateDict;
      NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
      NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

      // Check inputs
      NewtonSolverParam::checkInputs(NewtonSolverDict);
      // Create the solver object
      NewtonSolver<SystemMatrixClass> Solver( AlgEqSet, NewtonSolverDict );

      // residual vectors
      SystemVectorClass q0(AlgEqSet.vectorStateSize());
      SystemVectorClass sln(AlgEqSet.vectorStateSize());
      SystemVectorClass rsd(sln.size());

      // set initial condition
      q0 = 0.0;
      sln = 0.0;

      // Solve
      converged = Solver.solve(q0, sln).converged;
      BOOST_CHECK( converged );

      rsd = 0;
      AlgEqSet.residual(sln, rsd);
      std::vector<std::vector<Real>> rsdNorm = AlgEqSet.residualNorm(rsd);

      BOOST_REQUIRE( AlgEqSet.convergedResidual(rsdNorm) );

      // L2 solution error

      Real SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_HDG( fcnErr, SquareError ), xfld, (qfld, afld),
          quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real errorNorm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( errorNorm );
      indx++;

#ifdef SANS_VERBOSE

      if (converged)
      {
        std::cout << "Converged   ";
      }
      else
      {
        std::cout << "Unconverged ";
      }
      std::cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( errorNorm );
      if (indx > 1)
      {
        std::cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        std::cout << "  (rate = " << (log(normVec[indx-1]) - log(normVec[indx-2])) /(log(hVec[indx-1]) - log(hVec[indx-2])) << ")";
      }
      std::cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/slnHDG_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += ".plt";
      output_Tecplot( qfld, filename );
#endif

    }

#if defined(DUMP_TMP_FILES)
    // Tecplot output
    resultFile << "ZONE T=\"HDG P=" << order << "\"" << std::endl;
#endif
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
#if defined(DUMP_TMP_FILES)

      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;
#endif
      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
