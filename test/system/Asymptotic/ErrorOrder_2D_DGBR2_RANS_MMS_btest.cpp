// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_2D_DGBR2_RANS_MMS_btest
// testing of 2-D DG BR2 with RANS-SA on triangles

//#define SANS_FULLTEST
#define SANS_VERBOSE
//#define SANS_TIMING

#include <iomanip> // set::setprecision
#include <type_traits> //std::is_same

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"

#include "pyrite_fstream.h"

#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/SolutionFunction_RANSSA2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/FunctionNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/DistanceFunction/DistanceFunction.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "NonLinearSolver/NewtonSolver.h"

//#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGBR2_RANSSA_MMS_test_suite )

// MMS solutions taken from AIAA-2017-3290
/*
  Motivations and methods of verification for high-order
  RANS solvers and solutions
 */

//----------------------------------------------------------------------------//
void RunMMS(PyDict slnArgs, const Real Reynolds, const std::string& MMS)
{
  typedef QTypePrimitiveRhoPressure QType;
  //typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass>::type BCVector;

  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef SolutionFunction_RANSSA2D_TrigMMS<TraitsSizeRANSSA, TraitsModelRANSSAClass> SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;

  typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // gas model
  const Real gamma = 1.4;
  const Real R     = 1;         // V^2/T (reference scale in lieu of velocity or temperature)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Prandtl = 0.7;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale
  const Real qRef = 1;                              // velocity scale

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity

  const Real L = 1.0;

  PyDict gasModel;
  gasModel[GasModelParams::params.gamma] = gamma;
  gasModel[GasModelParams::params.R] = R;

  typedef NDSolutionExact::ParamsType SlnParams;

  slnArgs[SlnParams::params.gasModel] = gasModel;
  slnArgs[SlnParams::params.L] = L;

  NDSolutionExact solnExact( slnArgs );

  typedef Function2DBase<ArrayQ> FunctionBase;
  std::shared_ptr<FunctionBase> solnptr = std::make_shared<NDSolutionExact>(solnExact);

  // PDE
  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  Real ntref = 1;
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw, RoeEntropyFix::eVanLeer, ntref, solnptr );

  // BC
  slnArgs[BCEuler2D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Function.Name] =
      BCEuler2D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Function.TrigMMS;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCEuler2D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Function] = slnArgs;
  BCSoln[BCEuler2D<BCTypeFunction_mitState,PDEClass>::ParamsType::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCSln"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSln"] = {0,1,2,3};

  // Newton Solver set up
  PyDict NewtonSolverDict, UMFPACKDict, LineUpdateDict;
  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
  LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-18;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 60;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

  // Check inputs
  NewtonSolverParam::checkInputs(NewtonSolverDict);


  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;

  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // BR2 discretization
  Real viscousEtaParameter = 2*Triangle::NTrace;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  ArrayQ normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  //std::ofstream resultFile("tmp/L2_2D_DGBR2_RANSSA_" + MMS + "_Quad_Cons.dat", std::ios::out);
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_DGBR2_RANSSA_" + MMS + "_FullTest.txt", 1e-9, 1e-4, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_DGBR2_RANSSA_" + MMS + "_MinTest.txt", 1e-9, 1e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  if (std::is_same<QType,QTypePrimitiveRhoPressure>::value)
  {
    resultFile << ", \"rho L2 error\"";
    resultFile << ", \"u L2 error\"";
    resultFile << ", \"v L2 error\"";
    resultFile << ", \"p L2 error\"";
    resultFile << ", \"nt L2 error\"";
    resultFile << ", \"rho L2 error rate\"";
    resultFile << ", \"u L2 error rate\"";
    resultFile << ", \"v L2 error rate\"";
    resultFile << ", \"p L2 error rate\"";
    resultFile << ", \"nt L2 error rate\"";
  }
  else if (std::is_same<QType,QTypeConservative>::value)
  {
    resultFile << ", \"rho L2 error\"";
    resultFile << ", \"rhou L2 error\"";
    resultFile << ", \"rhov L2 error\"";
    resultFile << ", \"rhoE L2 error\"";
    resultFile << ", \"rhont L2 error\"";
    resultFile << ", \"rho L2 error rate\"";
    resultFile << ", \"rhou L2 error rate\"";
    resultFile << ", \"rhov L2 error rate\"";
    resultFile << ", \"rhoE L2 error rate\"";
    resultFile << ", \"rhont L2 error rate\"";
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unkowng QType");

  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "Running full test..." << endl;
#endif
  int ordermax = 4;
#else
  int ordermax = 1;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 4;
    if ( order == 4 ) powermax = 3;
#else
    int powermax = 2;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );
      jj = ii;

      // grid:
//      XField2D_Box_Triangle_X1 xfld( ii, jj );
      //XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );
      XField2D_Box_Quad_X1 xfld( ii, jj );

      std::vector<int> interiorTraceGroups;
      for (int n = 0; n < xfld.nInteriorTraceGroups(); n++)
        interiorTraceGroups.push_back(n);

      // solution: Legendre, C0
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

      // L2 projection of exact solution as initial guess
      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}),
                                         (xfld, qfld) );

      const int nDOFPDE = qfld.nDOF();

      // lifting operators
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      rfld = 0;

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order-1, BasisFunctionCategory_Legendre,
                                                            BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
      lgfld = 0;

      // Distance function offset by 1 to reduce the size of the SA source term
      Field_CG_Cell<PhysD2, TopoD2, Real> distfld(xfld, 1, BasisFunctionCategory_Lagrange);
      DistanceFunction(distfld, {XField2D_Box_UnionJack_Triangle_X1::iBottom}, false);
      for (int n = 0; n < distfld.nDOF(); n++)
        distfld.DOF(n) += 1;

      ParamFieldTupleType paramfld = (distfld, xfld);

      QuadratureOrder quadratureOrder( xfld, 3*order + 1 );
      std::vector<Real> tol = {5e-9, 5e-9};
      PrimalEquationSetClass PrimalEqSet(paramfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, interiorTraceGroups, PyBCList, BCBoundaryGroups );

      // Create the solver object
      NewtonSolver<SystemMatrixClass> nonlinear_solver( PrimalEqSet, NewtonSolverDict );

      // residual
      SystemVectorClass q0(PrimalEqSet.vectorStateSize());

      PrimalEqSet.fillSystemVector(q0);

#ifdef SANS_TIMING
      std::cout << "P = " << order << " ii = " << ii << std::endl;
      timer rsd_time;
#endif

      // nonlinear solve
      SystemVectorClass q(q0.size());
      SolveStatus status = nonlinear_solver.solve(q0, q);
      BOOST_REQUIRE( status.converged );

      // L2 solution error

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( errorIntegrand, SquareError ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size()  );
      ArrayQ norm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      for (int n = 0; n < ArrayQ::M; n++)
        normVec[indx][n] = sqrt( norm[n] );
      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << std::setw(3) << ii << ": L2 solution error = ";
      for (int n = 0; n < ArrayQ::M; n++)
      {
        cout << std::setw(8) << std::setprecision(5) << std::scientific << sqrt( norm[n] );
        if (n < ArrayQ::M-1) cout << ", ";
      }

      if (indx > 1)
      {
        cout << "  (ratio = ";
        for (int n = 0; n < ArrayQ::M; n++)
        {
          cout << std::setw(7) << std::setprecision(5) << std::fixed << normVec[indx-1][n]/normVec[indx-2][n];
          if (n < ArrayQ::M-1) cout << ", ";
        }
        cout << ")";
        cout << "  (slope = ";
        for (int n = 0; n < ArrayQ::M; n++)
        {
          Real slope = (log(normVec[indx-1][n]) - log(normVec[indx-2][n]))/(log(hVec[indx-1]) - log(hVec[indx-2]));
          cout << std::setw(7) << std::setprecision(5) << std::fixed << slope;
          if (n < ArrayQ::M-1) cout << ", ";
        }
        cout << ")";
      }
      cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/ErrorOrder_2D_DGBR2_RANSSA_" + MMS;
      filename +=  + "_P" + to_string(order);
      filename += "_L" + to_string(power-1);
      filename += "_Quad_Cons.dat";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      FunctionNDConvertSpace<PhysD2, ArrayQ> fcnND( *solnptr );
      output_Tecplot(fcnND, qfld, filename, pde.variableInterpreter().stateNames() );
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
#endif

    } //grid refinement loop

    // Tecplot output
    resultFile << "ZONE T=\"DG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      for (int i = 0; i < ArrayQ::M; i++)
        resultFile << ", " << normVec[n][i];

      slope = 0;
      if (n > 0)
        for (int i = 0; i < ArrayQ::M; i++)
          resultFile << ", " << (log(normVec[n][i])  - log(normVec[n-1][i])) /(log(hVec[n]) - log(hVec[n-1]));
      else
        for (int i = 0; i < ArrayQ::M; i++)
          resultFile << ", " << slope;

      resultFile << endl;

      pyriteFile << hDOFVec[n];
      for (int i = 0; i < ArrayQ::M; i++)
        pyriteFile << normVec[n][i];

      if (n > 0)
        for (int i = 0; i < ArrayQ::M; i++)
        {
          slope = (log(normVec[n][i]) - log(normVec[n-1][i]))/(log(hVec[n]) - log(hVec[n-1]));
          pyriteFile.close(4) << slope;
        }
      pyriteFile << endl;
    }

  } //order loop
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_MMS1 )
{
  typedef SolutionFunction_RANSSA2D_TrigMMS_Params SlnParams;

  // Create the exact solution (MMS-1 from the paper)

  Real Reynolds = 1e3;

  PyDict slnArgs;

  slnArgs[SlnParams::params.r0]   = 1;
  slnArgs[SlnParams::params.rx]   = 0.1;
  slnArgs[SlnParams::params.ry]   = -0.2;
  slnArgs[SlnParams::params.rxy]  = 0.1;
  slnArgs[SlnParams::params.arx]  = 1;
  slnArgs[SlnParams::params.ary]  = 1;
  slnArgs[SlnParams::params.arxy] = 1;

  slnArgs[SlnParams::params.u0]   = 2;
  slnArgs[SlnParams::params.ux]   = 0.3;
  slnArgs[SlnParams::params.uy]   = 0.3;
  slnArgs[SlnParams::params.uxy]  = 0.3;
  slnArgs[SlnParams::params.aux]  = 3;
  slnArgs[SlnParams::params.auy]  = 1;
  slnArgs[SlnParams::params.auxy] = 1;

  slnArgs[SlnParams::params.v0]   = 2;
  slnArgs[SlnParams::params.vx]   = 0.3;
  slnArgs[SlnParams::params.vy]   = 0.3;
  slnArgs[SlnParams::params.vxy]  = 0.3;
  slnArgs[SlnParams::params.avx]  = 1;
  slnArgs[SlnParams::params.avy]  = 1;
  slnArgs[SlnParams::params.avxy] = 1;

  slnArgs[SlnParams::params.p0]   = 10;
  slnArgs[SlnParams::params.px]   = 1;
  slnArgs[SlnParams::params.py]   = 1;
  slnArgs[SlnParams::params.pxy]  = 0.5;
  slnArgs[SlnParams::params.apx]  = 2;
  slnArgs[SlnParams::params.apy]  = 1;
  slnArgs[SlnParams::params.apxy] = 1;

  slnArgs[SlnParams::params.nt0]   = 0.6;
  slnArgs[SlnParams::params.ntx]   = -0.03;
  slnArgs[SlnParams::params.nty]   = -0.02;
  slnArgs[SlnParams::params.ntxy]  = 0.02;
  slnArgs[SlnParams::params.antx]  = 2;
  slnArgs[SlnParams::params.anty]  = 1;
  slnArgs[SlnParams::params.antxy] = 3;

  RunMMS(slnArgs, Reynolds, "MMS1");
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve2D_DGBR2_MMS2 )
{
  typedef SolutionFunction_RANSSA2D_TrigMMS_Params SlnParams;

  // Create the exact solution (MMS-2 from the paper)

  Real Reynolds = 1e1;

  PyDict slnArgs;

  slnArgs[SlnParams::params.r0]   = 1;
  slnArgs[SlnParams::params.rx]   = 0.1;
  slnArgs[SlnParams::params.ry]   = -0.2;
  slnArgs[SlnParams::params.rxy]  = 0.1;
  slnArgs[SlnParams::params.arx]  = 1;
  slnArgs[SlnParams::params.ary]  = 1;
  slnArgs[SlnParams::params.arxy] = 1;

  slnArgs[SlnParams::params.u0]   = 2;
  slnArgs[SlnParams::params.ux]   = 0.3;
  slnArgs[SlnParams::params.uy]   = 0.3;
  slnArgs[SlnParams::params.uxy]  = 0.3;
  slnArgs[SlnParams::params.aux]  = 3;
  slnArgs[SlnParams::params.auy]  = 1;
  slnArgs[SlnParams::params.auxy] = 1;

  slnArgs[SlnParams::params.v0]   = 2;
  slnArgs[SlnParams::params.vx]   = 0.3;
  slnArgs[SlnParams::params.vy]   = 0.3;
  slnArgs[SlnParams::params.vxy]  = 0.3;
  slnArgs[SlnParams::params.avx]  = 1;
  slnArgs[SlnParams::params.avy]  = 1;
  slnArgs[SlnParams::params.avxy] = 1;

  slnArgs[SlnParams::params.p0]   = 10;
  slnArgs[SlnParams::params.px]   = 1;
  slnArgs[SlnParams::params.py]   = 1;
  slnArgs[SlnParams::params.pxy]  = 0.5;
  slnArgs[SlnParams::params.apx]  = 2;
  slnArgs[SlnParams::params.apy]  = 1;
  slnArgs[SlnParams::params.apxy] = 1;

  slnArgs[SlnParams::params.nt0]   = -6;
  slnArgs[SlnParams::params.ntx]   = -0.3;
  slnArgs[SlnParams::params.nty]   = -0.2;
  slnArgs[SlnParams::params.ntxy]  = 0.2;
  slnArgs[SlnParams::params.antx]  = 2;
  slnArgs[SlnParams::params.anty]  = 1;
  slnArgs[SlnParams::params.antxy] = 3;

  RunMMS(slnArgs, Reynolds, "MMS2");
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
