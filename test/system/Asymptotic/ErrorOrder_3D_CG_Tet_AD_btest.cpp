// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve3D_CG_Tet_AD_btest
// testing of 3-D CG with Advection-Diffusion on tetrahedra

//#define SANS_FULLTEST
#define SANS_VERBOSE
//#define TIMING

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"
#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"
#include "pde/NDConvert/OutputNDConvertSpace3D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"
#include "pde/ForcingFunction3D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"

#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/Direct/MKL_PARDISO/MKL_PARDISOSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve3D_CG_Tet_AD_test_suite )


//----------------------------------------------------------------------------//
// solution: Hierarchical P1 thru P3
// Lagrange multiplier: continuous Hierarchical P1 thru P3
// exact solution: double BL
BOOST_AUTO_TEST_CASE( Solve3D_CG_Tet_AD )
{
  typedef ScalarFunction3D_SineSineSine SolutionExact;

  typedef SolnNDConvertSpace<PhysD3,  SolutionExact > NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD3,
                                AdvectiveFlux3D_Uniform,
                                ViscousFlux3D_Uniform,
                                Source3D_UniformGrad > PDEClass;

  typedef ForcingFunction3D_MMS<PDEClass> ForcingFunction;
  typedef PDENDConvertSpace<PhysD3, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

  typedef AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvertSpace, BCVector,
                                        AlgEqSetTraits_Sparse, XField<PhysD3, TopoD3>> AlgebraicEquationSet_GalerkinClass;
  typedef AlgebraicEquationSet_GalerkinClass::BCParams BCParams;

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  // Exact solution
  NDSolutionExact solnExact;

  // PDE
  AdvectiveFlux3D_Uniform adv( 0, 0, 0 );

  Real nu = 1;
  ViscousFlux3D_Uniform visc( nu, 0, 0, 0, nu, 0, 0, 0, nu );

  Source3D_UniformGrad source(0,0,0,0);

  std::shared_ptr<ForcingFunction> forcingptr(new ForcingFunction(solnExact));

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC

  // Create a BC dictionary
    PyDict SineSineSine;
  SineSineSine[BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function.SineSineSine;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD3,BCType>::params.Function] = SineSineSine;

  PyDict PyBCList;
  PyBCList["TheBCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCName"] = {0, 1, 2, 3, 4, 5};

  typedef AlgebraicEquationSet_GalerkinClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_GalerkinClass::SystemVector SystemVectorClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD3, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  // integrands
  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/L2_3D_CG.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_3D_CG_Tet_AD_FullTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_3D_CG_Tet_AD_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 2;
#else
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj, kk;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
    if (order == 2 ) powermax = 3;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      timer totaltime;

      ii = pow( 2, power );
      jj = ii;
      kk = ii;

      // grid:

      XField3D_Box_Tet_X1 xfld( comm, ii, jj, kk );

      // solution: Hierarchical, C0
      Field_CG_Cell<PhysD3, TopoD3, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Hierarchical);
      qfld = 0;

      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)

#if 1
      Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#elif 0
      QField3D_CG_BoundaryEdge_Independent<NDPDEClass> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#else
      QField3D_DG_BoundaryEdge<NDPDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#endif

      lgfld = 0;

      const int nDOFBC = lgfld.nDOF();

      // quadrature rule
      int orderMax = 2*order;

      QuadratureOrder quadratureOrder( xfld, orderMax );
      std::vector<Real> tol = {1e-11, 1e-11};

      StabilizationNitsche stab(order);
      AlgebraicEquationSet_GalerkinClass AlgEqSet(xfld, qfld, lgfld, pde, stab,
                                                  quadratureOrder, ResidualNorm_L2, tol, {0}, PyBCList, BCBoundaryGroups );

      // residual vectors

      SystemVectorClass q(AlgEqSet.vectorStateSize());
      SystemVectorClass rsd(AlgEqSet.vectorEqSize());

      AlgEqSet.fillSystemVector(q);

      rsd = 0;

      AlgEqSet.residual(q, rsd);

      // solve

#ifdef INTEL_MKL
      SLA::MKL_PARDISO<SystemMatrixClass> solver(AlgEqSet);
#else
      SLA::UMFPACK<SystemMatrixClass> solver(AlgEqSet);
#endif

      SystemVectorClass dq(q.size());

      solver.solve(rsd, dq);

      // updated solution

      q -= dq;

      AlgEqSet.setSolutionField(q);

      // check that the residual is zero

      rsd = 0;
      AlgEqSet.residual(q, rsd);

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdBCnrm), 1e-12 );

      // L2 solution error

      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD3>::integrate(
          FunctionalCell_Galerkin( errorIntegrand, SquareError ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
      {
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << "  (rate = " << (log(normVec[indx-1]) - log(normVec[indx-2]))/(log(hVec[indx-1]) - log(hVec[indx-2])) << ")";
      }
      cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/slnCG_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += "x";
      filename += to_string(jj);
      filename += "x";
      filename += to_string(kk);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      //output_Tecplot( qfld, &solnExact, filename, order-1 );
      output_Tecplot( qfld, filename );
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
#endif

    }

    // Tecplot output
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n] << slope << std::endl;
    }

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
