// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorOrder_2D_HDG_Triangle_AD_btest
// testing of 2-D HDG with Advection-Diffusion on triangles

//#define SANS_FULLTEST
#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_HDG_Triangle_AD_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_HDG_Triangle_AD )
{
  typedef ScalarFunction2D_DoubleBL SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

//  typedef BCTypeFunction_mitStateParam BCType;
  typedef BCTypeFunctionLinearRobin_sansLG BCType;
  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> AlgebraicEquationSet_HDGClass;
  typedef AlgebraicEquationSet_HDGClass::BCParams BCParams;

  // PDE
  AdvectiveFlux2D_Uniform adv( 0.3, 0.5 );
  ViscousFlux2D_Uniform visc( 1.0, 0.2, 0.1, 1.5 );
  Source2D_UniformGrad source(0.5, 0.2, 0.04);

  Real a = 0.1;
  Real b = 0.3;
  Real nu = 0.1;

  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.a] = a;
  solnArgs[NDSolutionExact::ParamsType::params.b] = b;
  solnArgs[NDSolutionExact::ParamsType::params.nu] = nu;

  NDSolutionExact solnExact( solnArgs );

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict DoubleBL;
  DoubleBL[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.DoubleBL;
  DoubleBL[SolutionExact::ParamsType::params.a] = a;
  DoubleBL[SolutionExact::ParamsType::params.b] = b;
  DoubleBL[SolutionExact::ParamsType::params.nu] = nu;

#if 0 //mitState BCs
  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
         BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;

  PyDict PyBCList;
  PyBCList["BCDirichlet"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  BCBoundaryGroups["BCDirichlet"] = {0,1,2,3};
#else //sansLG BCs
  PyDict BCSoln_Bottom;
  BCSoln_Bottom[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCSoln_Bottom[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0.0;

  PyDict BCSoln_Right;
  BCSoln_Right[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 0.0;
  BCSoln_Right[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 1.0;

  PyDict BCSoln_Top;
  BCSoln_Top[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 0.65;
  BCSoln_Top[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = -0.24;

  PyDict BCSoln_Left;
  BCSoln_Left[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_sansLG;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = DoubleBL;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1.0;
  BCSoln_Left[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 1.0;

  PyDict PyBCList;
  PyBCList["BCBottom"] = BCSoln_Bottom;
  PyBCList["BCRight"] = BCSoln_Right;
  PyBCList["BCTop"] = BCSoln_Top;
  PyBCList["BCLeft"] = BCSoln_Left;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCBottom"] = {0};
  BCBoundaryGroups["BCRight"] = {1};
  BCBoundaryGroups["BCTop"] = {2};
  BCBoundaryGroups["BCLeft"] = {3};
#endif

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef AlgebraicEquationSet_HDGClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_HDGClass::SystemVector SystemVectorClass;

  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_HDG_Output<NDErrorClass> ErrorIntegrandClass;

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );

  const std::vector<int> InteriorTraceGroups = {0,1,2}; //UnionJack has 3 interior trace groups

  // output integrands
  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  //std::ofstream resultFile("tmp/L2_2D_HDG.plt", std::ios::out);
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_HDG_Triangle_AD_FullTest.txt", 1e-10, 1e-4, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_HDG_Triangle_AD_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 5;
#else
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
    if ( order == 5 ) powermax = 4;
#else
    int powermax = 2;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );
      jj = ii;

      // grid:
      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

      // HDG solution field
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      qfld = 0;

      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
      afld = 0;

      // interface solution
      Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
      qIfld = 0;

      // Lagrange multiplier
      std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, LG_BGroup_list);
      lgfld = 0;

      const int nDOFPDE = qfld.nDOF();

      // quadrature rule
      QuadratureOrder quadratureOrder( xfld, -1 );

      std::vector<Real> tol = {1e-10, 1e-10, 1e-10};

      AlgebraicEquationSet_HDGClass AlgEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                             {0}, InteriorTraceGroups, PyBCList, BCBoundaryGroups );

      // residual vectors

      SystemVectorClass q(AlgEqSet.vectorStateSize());
      SystemVectorClass rsd(AlgEqSet.vectorEqSize());

      AlgEqSet.fillSystemVector(q);

      rsd = 0;
      AlgEqSet.residual(q, rsd);

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(AlgEqSet);

      SystemVectorClass dq(q.size());
      solver.solve(rsd, dq);

      // updated solution
      q -= dq;

      // check that the residual is zero
      rsd = 0;
      AlgEqSet.residual(q, rsd);
      std::vector<std::vector<Real>> rsdNorm = AlgEqSet.residualNorm(rsd);

      bool converged = AlgEqSet.convergedResidual(rsdNorm);

      BOOST_CHECK( converged );
      if (!converged) AlgEqSet.printDecreaseResidualFailure( AlgEqSet.residualNorm(rsd) );

      // L2 solution error
      Real SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_HDG( errorIntegrand, SquareError ),
          xfld, (qfld, afld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

#ifdef SANS_VERBOSE
      std::cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
      {
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
        cout << "  (rate = " << (log(normVec[indx-1]) - log(normVec[indx-2])) /(log(hVec[indx-1]) - log(hVec[indx-2])) << ")";
      }
      std::cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/slnHDG_P";
      filename += to_string(order);
      filename += "_";
      filename += to_string(ii);
      filename += ".plt";
      output_Tecplot( qfld, filename );
#endif

    }

    // Tecplot output
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n]) - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n];
      pyriteFile.close(5e-1) << slope << std::endl;
    }

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
