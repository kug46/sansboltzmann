// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve2D_DGAdvective_Flat_Line_btest
// testing of 2-D DG Advection on a flat manifold

//#define SANS_FULLTEST
//#define SANS_VERBOSE
//#define RESIDUAL_TEST  // test residual convergence for projections of the exact solution

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_DGAdvective_Flat_Line_AD_test_suite )

//----------------------------------------------------------------------------//
// solution: Legendre P0 thru P4
// Lagrange multiplier: Legendre P0 thru P4
// exact solution: u(x,y) = x^6*y^0 where y!=0
BOOST_AUTO_TEST_CASE( Solve2D_DGAdvective_Flat_Line_AD_test )
{
  // ---------- Define type/class names ---------- //
  // Exact solution
  typedef ScalarFunction2D_Monomial SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> SolutionNDClass;
  // Fluxes
  typedef AdvectiveFlux2D_Uniform AdvectiveFluxClass;
  typedef ViscousFlux2D_None ViscousFluxClass;
  // Source
  typedef Source2D_None SourceClass;

  // PDE class
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFluxClass,
                                ViscousFluxClass,
                                SourceClass > PDEClass;
  // Forcing function
  typedef ForcingFunction2D_MMS<PDEClass> ForcingFcnClass;  // manufactured solution
  typedef PDENDConvertSpace< PhysD2, PDEClass > NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  // BC
  typedef BCAdvectionDiffusion2DVector<AdvectiveFluxClass, ViscousFluxClass> BCVector;

  // Primal equation set
  typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD1>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // Solution square error
  typedef OutputCell_SolutionErrorSquared<PDEClass, SolutionExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_Galerkin_Output<NDErrorClass> ErrorIntegrandClass;

  // ---------- Set problem parameters ---------- //
  // PDE
  const int xp = 6, yp = 0;     // exact solution u(x,y) = x^xp * y^yp
  SolutionExact MMS_soln( xp, yp );

  const Real u = 2.0, v = 0.0;  // uniform advection velocity
  AdvectiveFluxClass adv( u, v );

  ViscousFluxClass visc;    // no diffusion

  SourceClass source;

  std::shared_ptr<ForcingFcnClass> forcingptr( new ForcingFcnClass(MMS_soln) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // ---------- Create parameter dictionaries ---------- //
  // Exact solution
  PyDict solnArgs;
  solnArgs[SolutionNDClass::ParamsType::params.i] = xp;
  solnArgs[SolutionNDClass::ParamsType::params.j] = yp;
  SolutionNDClass solnExact( solnArgs );

  // BC
  PyDict Monomial;
  Monomial[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.Function.Monomial;
  Monomial[SolutionExact::ParamsType::params.i] = xp;
  Monomial[SolutionExact::ParamsType::params.j] = yp;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunctionLinearRobin_mitLG>::params.Function] = Monomial;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  // Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // Define the BoundaryGroups for each boundary condition
  std::map<std::string, std::vector<int>> BCBoundaryGroups;
  BCBoundaryGroups["BCName"] = {0,1};

  // ---------- Miscellaneous setup before runs ---------- //
  // Solution square error class
  NDErrorClass fcnError(solnExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // norm data
  const int nrmDatSize = 10;  // size of norm data containers
  Real hVec[nrmDatSize];     // 1/nElem
  Real hDOFVec[nrmDatSize];  // 1/sqrt(DOF)
  Real normVec[nrmDatSize];  // L2 error
  int indx;                  // index of norm data entries

#ifdef RESIDUAL_TEST
  Real normVecRsd[nrmDatSize];  // PDE residual
#endif

  // Tecplot & pyrite output
#ifdef SANS_FULLTEST
  // Tecplot
  std::ofstream resultFile("tmp/L2_2D_DGAdvective_Flat_Line.plt", std::ios::out);
  // Pyrite
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_DGAdvective_Flat_Line_AD_FullTest.txt",
                                1e-10, 1e-4, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/ErrorOrder_2D_DGAdvective_Flat_Line_AD_MinTest.txt", 1e-10, 1e-10,
                                pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
#ifdef RESIDUAL_TEST
  resultFile << ", \"PDE residual\"";
  resultFile << ", \"PDE residual rate\"";
#endif
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  // Perform grid convergence tests for various solution orders
  int ordermin = 0;  // minimum solution/polynomial order
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 4;  // maximum solution/polynomial order of a full test
#else
  int ordermax = ordermin;
#endif
  // ---------- Run across solution orders & Grid refinements ---------- //
  // loop over solution order: p = order
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;  // (re)initialized for each solution order loop

    int ii;  // number of elements
    int powermin = 2;
#ifdef SANS_FULLTEST
    int powermax = 7;
#else
    int powermax = powermin;
#endif
    // loop over grid refinement: 2^power
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );

      // grid: line grid on x = [0,1], y = 1.0 with ii elements
      std::vector<DLA::VectorS<2,Real>> coordinates(ii+1);
      for (int i = 0; i < ii + 1; i++)
        coordinates[i] = { static_cast<Real>(i) / static_cast<Real>(ii), 1.0 };  // Note that y cannot be zero because 0^0 gives NaN
      XField2D_Line_X1_1Group xfld( coordinates );
#if 0
      output_Tecplot(xfld, "tmp/FlatLine_grid.dat");  // Tecplot dump grid
#endif

      // solution: Legendre
      Field_DG_Cell<PhysD2, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      // use the project of exact solution as initial solution
      for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfld, qfld) );
      const int nDOFPDE = qfld.nDOF();

      // Lagrange multiplier: Legendre
#if 0
      Field_CG_BoundaryTrace<PhysD3, TopoD2, ArrayQ> lgfld( xfld, order );
#elif 0
      std::vector<std::vector<int>> boundaryGroupSets = {{0},{1},{2},{3}};
      Field_CG_BoundaryTrace<PhysD2, TopoD1, ArrayQ> lgfld( xfld, order );
#else
      Field_DG_BoundaryTrace<PhysD2, TopoD1, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#endif
      lgfld = 0;
      const int nDOFBC = lgfld.nDOF();

      QuadratureOrder quadratureOrder( xfld, - 1 );
      std::vector<Real> tol = {1e-12, 1e-12};

      // primal equation set
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0}, PyBCList, BCBoundaryGroups );

      // residual
      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      PrimalEqSet.fillSystemVector(q);
      rsd = 0;

      PrimalEqSet.residual(q, rsd);
#ifdef RESIDUAL_TEST
      Real rsdPDEnrmIS = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrmIS += pow(rsd[0][n],2);

      normVecRsd[indx] = sqrt(rsdPDEnrmIS);
#endif

      // Print residual vectors
#if 0
      std::cout <<endl << "Solution:" <<endl;
      for (int k = 0; k < nDOFPDE; k++) std::cout << k << "\t" << rsd[0][k] << std::endl;

      std::cout <<endl << "BC:" <<endl;
      for (int k = 0; k < nDOFBC; k++) std::cout << k << "\t" << rsd[1][k] << std::endl;
#endif

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());

      solver.solve(rsd, dq);

      // update solution
      q -= dq;
      PrimalEqSet.setSolutionField(q);

      // check that the residual is zero
      rsd = 0;
      PrimalEqSet.residual(q, rsd);

      Real rsdPDEnrm = 0;
      for (int n = 0; n < nDOFPDE; n++)
        rsdPDEnrm += pow(rsd[0][n],2);

      Real rsdBCnrm = 0;
      for (int n = 0; n < nDOFBC; n++)
        rsdBCnrm += pow(rsd[1][n],2);

      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );
      BOOST_CHECK_SMALL( sqrt(rsdBCnrm),  1e-12 );

//      cout << sqrt(rsdPDEnrm) << "\t" << sqrt(rsdBCnrm) <<endl;

      // L2 solution error
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD1>::integrate(
          FunctionalCell_Galerkin( errorIntegrand, SquareError ),
          xfld, qfld, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      Real norm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

      // print L2 error & convergence rate
#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
        cout << "  (convergence rate = " << -log2( normVec[indx-1]/normVec[indx-2] ) << ")";
      cout << endl;
#endif

      // Tecplot dump: solution
#if 0
      string filename = "tmp/dqDG_FlatLine_P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qfld, filename );
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
#endif

    } // end: grid refinement loop

    // Tecplot output
    resultFile << "ZONE T=\"DG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
#ifdef RESIDUAL_TEST
      Real slopeRsd = 0;
      if (n > 0)
        slopeRsd = (log(normVecRsd[n])  - log(normVecRsd[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << ", " << normVecRsd[n];
      resultFile << ", " << slopeRsd;
#endif
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n];
      pyriteFile.close(1e-1) << slope << std::endl;
    }

    // Mathematica dump
#if 0
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
    // Mathematica dump (cut & paste)
#if 0
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  } // end: solution order loop
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
