// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AWErrorEst_2D_HDG_Triangle_AD_btest
// testing error estimation of 2-D HDG with Advection-Diffusion on triangles
//
// case: double BL
#undef SANS_FULLTEST
//#define SANS_FULLTEST // Line 263 to change to HDG(h) or HDG(L)

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "Surreal/SurrealS.h"
#include "tools/KahanSum.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction_MMS.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/HDG/AWIntegrandFunctor_HDG.h"
#include "Discretization/HDG/AWIntegrandBoundaryFunctor_HDG.h"
#include "Discretization/HDG/AWIntegrandBCFunctor_HDG.h"

#include "Discretization/HDG/AWResidualCell_HDG.h"
#include "Discretization/HDG/AWResidualInteriorTrace_HDG.h"
#include "Discretization/HDG/AWResidualBoundaryTrace_HDG.h"
#include "Discretization/HDG/AWResidualBCTrace_HDG.h"

#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementIntegral.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

//#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( AWErrorEst_2D_HDG_Triangle_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AWErrorEst_2D_HDG_Triangle_AD )
{
  timer totaltime;
  typedef SurrealS<1> SurrealClass;

#if 1  // AD w/ double BL
  // Exacts
  typedef ScalarFunction2D_DoubleBL         SolutionExact;
  typedef ScalarFunction2D_AdjointADAverage AdjointExact;

  // ND Exacts
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD2, AdjointExact>  NDAdjointExact;

  // Integrand Functors
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Const>                 VolumeFcn;
  typedef IntegrandCell_WeightedSolution<PhysD2,VolumeFcn>                      IntegrandClass;
  typedef IntegrandClass::Functor<ArrayQ,TopoD2,Triangle>                       FunctorClass;
  typedef IntegrandCell_WeightedExactSolution<PhysD2,VolumeFcn,NDSolutionExact> ExactIntegrandClass;
  typedef ExactIntegrandClass::Functor<ArrayQ,TopoD2,Triangle>                  ExactFunctorClass;
  // PDE

  Real u = 1;
  Real v = 0.2;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real nu = 0.1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  PDEClass pde( adv, visc );

  Real a0=1.0;
  NDSolutionExact       solnExact( u, v, nu );
  VolumeFcn             volumeFcn( a0 ); //  the rhs for the adjoint
  NDAdjointExact        adjExact( u, v, nu );

  // BC
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(u,v,nu) );
  BCClass bc( solnExact, visc );

  cout << "case: double BL;  nu = " << nu << endl;
#elif 0      // Laplacian w/ vortex
  typedef ScalarFunction2D_Vortex                  SolutionExact;
  typedef ScalarFunction2D_LaplacianUnitForcing AdjointExact;

  // ND Exacts
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD2, AdjointExact>  NDAdjointExact;

  // Integrand Functors
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Const>                 VolumeFcn;
  typedef IntegrandCell_WeightedSolution<PhysD2,VolumeFcn>                      IntegrandClass;
  typedef IntegrandClass::Functor<ArrayQ,TopoD2,Triangle>                       FunctorClass;
  typedef IntegrandCell_WeightedExactSolution<PhysD2,VolumeFcn,NDSolutionExact> ExactIntegrandClass;
  typedef ExactIntegrandClass::Functor<ArrayQ,TopoD2,Triangle>                  ExactFunctorClass;
  // PDE
  Real u=0.;
  Real v=0.;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real nu = 1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  PDEClass pde( adv, visc );

  Real x0 = -1./sqrt(3.);
  Real y0 = 1./sqrt(7.);
  NDSolutionExact solnExact( x0, y0 );

  Real a0=1.0;
  VolumeFcn             volumeFcn( a0 );
  //  the rhs for the adjoint i.e. the output Functional
  NDAdjointExact          adjExact;

  // BC
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(x0,y0) );
  BCClass bc( solnExactPtr, visc );

  cout << "case: Laplacian w/ vortex" << endl;

#elif 1  // Laplacian w/ Exponential functions
  // Case used in Hugh's Masters Thesis - ALSO PYRITE SETTINGS
  // Case 1 - primal [a=1;b=1;c=1;d=1;]       - adjoint [a=1;b=-2;c=-2;d=1;]

  // Also the Case as used for the pyrite files
  typedef ScalarFunction2D_VarExp3  SolutionExact;
  typedef ScalarFunction2D_SineSine AdjointExact;

  // ND Exacts
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD2, AdjointExact>  NDAdjointExact;

  // Primal PDE
  typedef PDEAdvectionDiffusion<PhysD2,
                                 AdvectiveFlux2D_Uniform,
                                 ViscousFlux2D_Uniform,
                                 Source2D_UniformGrad > PDEAdvectionDiffusion2D;
   typedef PDENDConvertSpace<PhysD2, PDEAdvectionDiffusion2D> PDEClass;

   typedef PDEAdvectionDiffusion2D::template ArrayQ<Real> ArrayQ;
   typedef PDEAdvectionDiffusion2D::template MatrixQ<Real> MatrixQ;
   typedef DLA::VectorS<PhysD2::D, ArrayQ> VectorArrayQ;

   typedef BCTypeFunctionLinearRobin_mitLG BCType;
   typedef BCAdvectionDiffusion<PhysD2,BCType> BCClass;
   typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;

   // Adjoint PDE
   typedef PDEAdvectionDiffusion<PhysD2,
                                 AdvectiveFlux2D_Uniform,
                                 ViscousFlux2D_Uniform,
                                 Source2D_UniformGrad > PDEAdjoint2D;
   typedef PDENDConvertSpace<PhysD2, PDEAdjoint2D> PDEAjointClass;

  // Integrand Functors
  // call this class with something of Adjoint Exact Class
  typedef SolnNDConvertSpace<PhysD2,  ScalarFunction2D_ForcingFunction<PDEAjointClass> > VolumeFcn;

  typedef IntegrandCell_WeightedSolution<PDEClass,VolumeFcn>                                IntegrandClass;
  typedef IntegrandClass::Functor<Real,TopoD2,Triangle>                                     FunctorClass;
  typedef IntegrandCell_WeightedExactSolution<PDEClass,VolumeFcn,NDSolutionExact>           ExactIntegrandClass;
  typedef ExactIntegrandClass::Functor<TopoD2,Triangle>                                     ExactFunctorClass;

  // PDE
  Real u=0.;
  Real v=0.;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real nu = 1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0,0,0);

  Real a=3;
  NDSolutionExact solnExact(a);

  typedef ForcingFunction2D_MMS<PDEAdvectionDiffusion2D, NDSolutionExact> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact));
//  std::shared_ptr<NDSolutionExact> solnptr( new NDSolutionExact(a) );

  // Primal PDE
//  PDEClass pde( adv, visc, source, solnptr );
  PDEClass pde( adv, visc, source, forcingptr );


  // BC
  std::shared_ptr<SolutionExact> solnExactPtr( new SolutionExact(a) );

  NDBCClass bc( solnExactPtr, visc );

  // Adjoint
  AdvectiveFlux2D_Uniform adv_adj( -u, -v );

  a=1;
  NDAdjointExact  adjExact(a,a);

  typedef ForcingFunction2D_MMS<PDEAdjoint2D, NDAdjointExact> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact));

  // Adjoint PDE
  PDEAjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  VolumeFcn volumeFcn( pdeadj );

  cout << "case: Laplacian w/ Exponentials" << endl;

#endif

  typedef IntegrandCell_HDG<PDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<PDEClass> IntegrandTraceClass;
  typedef IntegrandBoundaryTrace<PDEClass, NDBCClass, NDBCClass::Category, HDG> IntegrandBoundClass;

  typedef AWIntegrandCell_HDG<PDEClass,NDAdjointExact> AWIntegrandCellClass;
  typedef AWIntegrandTrace_HDG<PDEClass,NDAdjointExact> AWIntegrandTraceClass;
  typedef AWIntegrandBoundary_HDG<PDEClass,NDAdjointExact> AWIntegrandBoundClass;
  typedef AWIntegrandBC_HDG<PDEClass,NDAdjointExact, NDBCClass> AWIntegrandBCClass;

  // HDG discretization
  string lRef;
  // Pyrite L = 1./10
  DiscretizationHDG<PDEClass> disc( pde, Global , AugGradient, 1./10 ); cout << "...using HDG-L" << endl; lRef = "L";
  //DiscretizationHDG<PDEClass> disc( pde, Local ); cout << "...using HDG-h" << endl; lRef ="h";

  // integrands
  const std::vector<int> BoundaryGroups = {0,1,2,3};

  // integrands
  IntegrandCellClass fcnCell( pde, disc, {0} );
  IntegrandTraceClass fcnInt( pde, disc, {0,1,2} ); //UnionJack has 3 interior trace groups
  IntegrandBoundClass fcnBC( pde, bc, BoundaryGroups, disc );

  // AW integrands
  AWIntegrandCellClass AWfcnCell( pde, adjExact,  disc );
  AWIntegrandTraceClass AWfcnInt( pde, adjExact, disc );
  AWIntegrandBoundClass AWfcnBound( pde, adjExact, disc, BoundaryGroups );
  AWIntegrandBCClass AWfcnBC( pde, adjExact, disc, bc, BoundaryGroups );

  // norm data
  const int vsize = 25;
  Real hVec[vsize];
  Real globalErrorVec[vsize], globalEstVec[vsize], globalEstErrVec[vsize], globalEffVec[vsize];
  Real localErrorVec[vsize], localEstVec[vsize], localEstErrVec[vsize], localEffVec[vsize];
  Real globalErrorRate[vsize], globalEstRate[vsize], globalEstErrRate[vsize], globalEffRate[vsize];
  Real localErrorRate[vsize], localEstRate[vsize], localEstErrRate[vsize], localEffRate[vsize];
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::string filename = "tmp/ErrorEst2D_HDG_" + lRef + ".plt";
  std::ofstream resultFile(filename, std::ios::out);
  std::string filename2 = "tmp/ErrorEst2D_HDG_" + lRef + ".dat";
  std::ofstream datFile(filename2, std::ios::out);
  std::string pyritefilename = "IO/ErrorEstimates/AWErrorEst_2D_HDG_" + lRef + "_AD_FullTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 1e-8, 1e-8, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  std::stringstream datFile;
  std::string pyritefilename = "IO/ErrorEstimates/AWErrorEst_2D_HDG_" + lRef + "_AD_MinTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 1e-8, 1e-8, pyrite_file_stream::check);
#endif
  // For Tecplot Output
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"Global Error\"";
  resultFile << ", \"Global Error Estimate\"";
  resultFile << ", \"Global Error Estimate Error\"";
  resultFile << ", \"Global 1-Effectivity\"";
  resultFile << ", \"Global Error Rate\"";
  resultFile << ", \"Global Error Estimate Rate\"";
  resultFile << ", \"Global Error Estimate Error Rate\"";
  resultFile << ", \"Global 1-Effectivity Rate\"";
  resultFile << ", \"Local Error\"";
  resultFile << ", \"Local Error Estimate\"";
  resultFile << ", \"Local Error Estimate Error\"";
  resultFile << ", \"Local 1-Effectivity\"";
  resultFile << ", \"Local Error Rate\"";
  resultFile << ", \"Local Error Estimate Rate\"";
  resultFile << ", \"Local Error Estimate Error Rate\"";
  resultFile << ", \"Local 1-Effectivity Rate\"";
  resultFile << endl;

  // For Latex Output
  datFile << "order,"
      "h,"
      "globalError,"
      "globalErrorEstimate,"
      "globalErrorEstimateError,"
      "globalEffectivity,"
      "globalErrorRate,"
      "globalErrorEstimateRate,"
      "globalErrorEstimateErrorRate,"
      "globalEffectivityRate,"
      "localError,"
      "localErrorEstimate,"
      "localErrorEstimateError,"
      "localEffectivity,"
      "localErrorRate,"
      "localErrorEstimateRate,"
      "localErrorEstimateErrorRate,"
      "localEffectivityRate";
  datFile << endl;

  int quadorder=-1;
  ElementIntegral <TopoD2, Triangle, ArrayQ> ElemIntegral(quadorder);

//---------------------------------------------------------------------------//
  // Computer the exact output on a fine grid and high-quadrature
  Real globalExt=0;

  {
    XField2D_Box_UnionJack_Triangle_X1 xfld( 50, 50 );

    // Field Cell Types
    typedef typename XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;

    ExactIntegrandClass exFcnint( volumeFcn, solnExact, {0} );

    ArrayQ localExt=0;

    const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Triangle>(0);

    ElementXFieldClass  xfldElem( xfldCell.basis() );

    // Loop over Volume Integrals
    for (int elem = 0; elem < xfldCell.nElem(); elem++)
    {
      xfldCell.getElement( xfldElem, elem );

      ExactFunctorClass exFcn = exFcnint.integrand(xfldElem);

      localExt = 0;
      ElemIntegral(exFcn, xfldElem, localExt);

      globalExt += localExt;
    }
    //std::cout << "globalExt = " << std::scientific << std::setprecision(16) << globalExt << std::endl;
  }
//---------------------------------------------------------------------------//

  const int orderinc = 2;

  int ordermin = 1;
  // ordermax = 3 is used for the pyrite
  int ordermax = 2;
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 4;
    cout << "...running full test" << endl;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
#ifndef SANS_FULLTEST
      //ii = 2 is used for the pyrite
      ii = 2;
#else
      // ii = 4*power + 4 is used for the pyrite
      ii = 4*power+4;
#endif
      jj = ii;

      cout << std::scientific << std::setprecision(7);
      // grid: Hierarchical P1 ( aka X1)

      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

      int nInteriorTraceGroups = xfld.nInteriorTraceGroups();
      int nBoundaryTraceGroups = xfld.nBoundaryTraceGroups();
      int nCellGroups = xfld.nCellGroups();

      BOOST_REQUIRE_EQUAL( 1, nCellGroups ); //unit test hard coded for single area group
      BOOST_REQUIRE_EQUAL( 3, nInteriorTraceGroups );
      BOOST_REQUIRE_EQUAL( 4, nBoundaryTraceGroups );

      // HDG solution field
      // solution in p: Legendre
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroups);

      int nDOFPDE = qfld.nDOF();
      int nDOFAu  = afld.nDOF()*PhysD2::D;
      int nDOFInt = qIfld.nDOF();
      int nDOFBC  = lgfld.nDOF();

      // quadrature rule
      int quadratureOrder[4] = {-1, -1, -1, -1};    // max
      int quadratureOrderMin[4] = {0, 0, 0, 0};     // min

      // linear system setup

      typedef SLA::SparseMatrix_CRS<MatrixQ>  SparseMatrixClass;
      typedef SLA::SparseVector<ArrayQ>       SparseVectorClass;
      typedef DLA::MatrixD<SparseMatrixClass> SystemMatrixClass;
      typedef DLA::VectorD<SparseVectorClass> SystemVectorClass;

      // residual

      SystemVectorClass rsd = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};
      rsd = 0;

      //timer residptime;

      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_HDG(fcnCell, rsd(0), rsd(1)),
                                              xfld, (qfld, afld), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsd(0), rsd(1), rsd(2)),
                                                              xfld, (qfld, afld), qIfld, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsd(0), rsd(1), rsd(3)),
                                                          xfld, (qfld, afld), lgfld, quadratureOrder, nBoundaryTraceGroups );

      //std::cout << "resid p Time: " << residptime.elapsed() << std::endl;

      // jacobian nonzero pattern
      //
      //        u   q  uI  lg
      //   PDE  X   X   X   X
      //   Au   X   X   X   X
      //   Int  X   X   X   0
      //   BC   X   X   0   0

      typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

      //fld.SystemMatrixSize();
      DLA::MatrixD<NonZeroPatternClass> nz =
          {{ {nDOFPDE, nDOFPDE}, {nDOFPDE, nDOFAu}, {nDOFPDE, nDOFInt}, {nDOFPDE, nDOFBC} },
           { {nDOFAu , nDOFPDE}, {nDOFAu , nDOFAu}, {nDOFAu , nDOFInt}, {nDOFAu , nDOFBC} },
           { {nDOFInt, nDOFPDE}, {nDOFInt, nDOFAu}, {nDOFInt, nDOFInt}, {nDOFInt, nDOFBC} },
           { {nDOFBC , nDOFPDE}, {nDOFBC , nDOFAu}, {nDOFBC , nDOFInt}, {nDOFBC , nDOFBC} }};

      NonZeroPatternClass& nzPDE_q  = nz(0,0);
      NonZeroPatternClass& nzPDE_a  = nz(0,1);
      NonZeroPatternClass& nzPDE_qI = nz(0,2);
      NonZeroPatternClass& nzPDE_lg = nz(0,3);

      NonZeroPatternClass& nzAu_q  = nz(1,0);
      NonZeroPatternClass& nzAu_a  = nz(1,1);
      NonZeroPatternClass& nzAu_qI = nz(1,2);
      NonZeroPatternClass& nzAu_lg = nz(1,3);

      NonZeroPatternClass& nzInt_q  = nz(2,0);
      NonZeroPatternClass& nzInt_a  = nz(2,1);
      NonZeroPatternClass& nzInt_qI = nz(2,2);
      //NonZeroPatternClass& nzInt_lg = nz(2,3);

      NonZeroPatternClass& nzBC_q  = nz(3,0);
      NonZeroPatternClass& nzBC_a  = nz(3,1);
      //NonZeroPatternClass& nzBC_qI = nz(3,2);
      NonZeroPatternClass& nzBC_lg = nz(3,3);

      //timer jacPatptime;

      IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nzPDE_q, nzPDE_a,
                                                                                      nzAu_q, nzAu_a),
                                              xfld, (qfld, afld), quadratureOrderMin, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, nzPDE_q, nzPDE_a, nzPDE_qI,
                                                          nzAu_q ,          nzAu_qI ,
                                                          nzInt_q, nzInt_a, nzInt_qI),
          xfld, (qfld, afld), qIfld, quadratureOrderMin, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, nzPDE_q, nzPDE_a, nzPDE_lg,
                                                               nzAu_q , nzAu_a , nzAu_lg ,
                                                               nzBC_q , nzBC_a , nzBC_lg ),
          xfld, (qfld, afld), lgfld, quadratureOrderMin, nBoundaryTraceGroups);



      //std::cout << "jac Pattern p Time: " << jacPatptime.elapsed() << std::endl;
      // jacobian
      SystemMatrixClass jac(nz);

      SparseMatrixClass& jacPDE_q  = jac(0,0);
      SparseMatrixClass& jacPDE_a  = jac(0,1);
      SparseMatrixClass& jacPDE_qI = jac(0,2);
      SparseMatrixClass& jacPDE_lg = jac(0,3);

      SparseMatrixClass& jacAu_q  = jac(1,0);
      SparseMatrixClass& jacAu_a  = jac(1,1);
      SparseMatrixClass& jacAu_qI = jac(1,2);
      SparseMatrixClass& jacAu_lg = jac(1,3);

      SparseMatrixClass& jacInt_q  = jac(2,0);
      SparseMatrixClass& jacInt_a  = jac(2,1);
      SparseMatrixClass& jacInt_qI = jac(2,2);
      //SparseMatrixClass& jacInt_lg = jac(2,3);

      SparseMatrixClass& jacBC_q  = jac(3,0);
      SparseMatrixClass& jacBC_a  = jac(3,1);
      //SparseMatrixClass& jacBC_qI = jac(3,2);
      SparseMatrixClass& jacBC_lg = jac(3,3);

      jac = 0;
      //timer jacptime;

      IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacPDE_q, jacPDE_a,
                                                                                      jacAu_q,  jacAu_a),
                                              xfld, (qfld, afld), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacPDE_q, jacPDE_a, jacPDE_qI,
                                                          jacAu_q ,           jacAu_qI ,
                                                          jacInt_q, jacInt_a, jacInt_qI),
          xfld, (qfld, afld), qIfld, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jacPDE_q, jacPDE_a, jacPDE_lg,
                                                               jacAu_q , jacAu_a , jacAu_lg ,
                                                               jacBC_q , jacBC_a , jacBC_lg ),
          xfld, (qfld, afld), lgfld, quadratureOrder, nBoundaryTraceGroups);

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver;
      SystemVectorClass sln = {{nDOFPDE},{nDOFAu},{nDOFInt},{nDOFBC}};
      sln = solver.inverse(jac)*rsd;

      // update solution
      for (int k = 0; k < nDOFPDE; k++)
        qfld.DOF(k) -= sln[0][k];
      for (int k = 0; k < nDOFPDE; k++)
        for (int d = 0; d < PhysD2::D; d++)
          afld.DOF(k)[d] -= sln[1][PhysD2::D*k+d];
      for (int k = 0; k < nDOFInt; k++)
        qIfld.DOF(k) -= sln[2][k];
      for (int k = 0; k < nDOFBC; k++)
        lgfld.DOF(k) -= sln[3][k];


      // Adjoint in p

      // HDG adjoint solution field
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qflda(xfld, order, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> aflda(xfld, order, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qIflda(xfld, order, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgflda(xfld, order, BasisFunctionCategory_Legendre, BoundaryGroups);

      nDOFPDE = qflda.nDOF();
      nDOFAu  = aflda.nDOF()*PhysD2::D;
      nDOFInt = qIflda.nDOF();
      nDOFBC  = lgflda.nDOF();

      // jacobian transpose
      // jacobian nonzero pattern
      //
      //        u   q  uI  lg
      //   PDE  X   X   X   X
      //   Au   X   X   X   X
      //   Int  X   X   X   0
      //   BC   X   X   0   0

      //Create a transposed non-zero pattern
      DLA::MatrixD<NonZeroPatternClass> nzT(Transpose(nz));

      SystemMatrixClass jacT(nzT);

      // ND version
      auto jacTPDE_q  = Transpose(jacT)(0,0);
      auto jacTPDE_a  = Transpose(jacT)(0,1);
      auto jacTPDE_qI = Transpose(jacT)(0,2);
      auto jacTPDE_lg = Transpose(jacT)(0,3);

      auto jacTAu_q  = Transpose(jacT)(1,0);
      auto jacTAu_a  = Transpose(jacT)(1,1);
      auto jacTAu_qI = Transpose(jacT)(1,2);
      auto jacTAu_lg = Transpose(jacT)(1,3);

      auto jacTInt_q  = Transpose(jacT)(2,0);
      auto jacTInt_a  = Transpose(jacT)(2,1);
      auto jacTInt_qI = Transpose(jacT)(2,2);
      //auto jacInt_lg  = Transpose(jacT)(2,3);

      auto jacTBC_q  = Transpose(jacT)(3,0);
      auto jacTBC_a  = Transpose(jacT)(3,1);
      //auto jacTBC_qI = Transpose(jacT)(3,2);
      auto jacTBC_lg = Transpose(jacT)(3,3);



      jac = 0;
      //timer jacTtime;
      IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacTPDE_q, jacTPDE_a,
                                                                                      jacTAu_q,  jacTAu_a),
                                              xfld, (qfld, afld), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacTPDE_q, jacTPDE_a, jacTPDE_qI,
                                                          jacTAu_q ,            jacTAu_qI ,
                                                          jacTInt_q, jacTInt_a, jacTInt_qI),
          xfld, (qflda, aflda), qIflda, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jacTPDE_q, jacTPDE_a, jacTPDE_lg,
                                                               jacTAu_q , jacTAu_a , jacTAu_lg ,
                                                               jacTBC_q , jacTBC_a , jacTBC_lg ),
          xfld, (qflda, aflda), lgflda, quadratureOrder, nBoundaryTraceGroups);

      // solve
      SystemVectorClass adj = {{nDOFPDE}, {nDOFAu}, {nDOFInt}, {nDOFBC}};

      rsd = 0; // reset residual
      IntegrandClass outputFcn( volumeFcn, {0} ); // declared back with the pde

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell<SurrealClass>( outputFcn, rsd(0) ), xfld, qflda, quadratureOrder, xfld.nCellGroups() );

      ////std::cout << std::endl << rsd(0) << std::endl;
      // compute the adjoint
      adj = solver.inverse(jacT)*rsd;

      // update solution
      for (int k = 0; k < nDOFPDE; k++)
        qflda.DOF(k) = adj[0][k];
      for (int k = 0; k < nDOFPDE; k++)
        for (int d = 0; d < PhysD2::D; d++)
          aflda.DOF(k)[d] = adj[1][PhysD2::D*k+d];
      for (int k = 0; k < nDOFInt; k++)
        qIflda.DOF(k) = adj[2][k];
      for (int k = 0; k < nDOFBC; k++)
        lgflda.DOF(k) = adj[3][k];

      // ---------------------
      // Adjoint error estimate
      // ---------------------
      // prolongate P+1 primal solution
      // HDG primal solution field
      // cell solution
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qIfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre, BoundaryGroups);

      qfld.projectTo(qfldPp1);
      afld.projectTo(afldPp1);
      qIfld.projectTo(qIfldPp1);
      lgfld.projectTo(lgfldPp1);

      const int nDOFPDEPp1 = qfldPp1.nDOF();
      const int nDOFAuPp1  = afldPp1.nDOF()*PhysD2::D;
      const int nDOFIntPp1 = qIfldPp1.nDOF();
      const int nDOFBCPp1  = lgfldPp1.nDOF();

      // residual
      SystemVectorClass rsdPp1 = {{nDOFPDEPp1},{nDOFAuPp1},{nDOFIntPp1},{nDOFBCPp1}};
      rsdPp1 = 0;

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qIfldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfldrPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre, BoundaryGroups);


      IntegrateCellGroups<TopoD2>::integrate( ResidualCell_HDG(fcnCell, rsdPp1(0), rsdPp1(1)),
                                              xfld, (qfldPp1, afldPp1), quadratureOrder, nCellGroups );
      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate( ResidualInteriorTrace_HDG(fcnInt, rsdPp1(0), rsdPp1(1), rsdPp1(2)),
                                                              xfld, (qfldPp1, afldPp1), qIfldPp1, quadratureOrder, nInteriorTraceGroups);
      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate( ResidualBoundaryTrace_FieldTrace_HDG(fcnBC, rsdPp1(0), rsdPp1(1), rsdPp1(3)),
                                                             xfld, (qfldPp1, afldPp1), lgfldPp1, quadratureOrder, nBoundaryTraceGroups );

      // Map residual to field
      for (int k = 0; k < nDOFPDEPp1; k++)
        qfldrPp1.DOF(k) = rsdPp1[0][k];
      for (int k = 0; k < nDOFPDEPp1; k++)
        for (int d = 0; d < PhysD2::D; d++)
          afldrPp1.DOF(k)[d] = rsdPp1[1][PhysD2::D*k+d];
      for (int k = 0; k < nDOFIntPp1; k++)
        qIfldrPp1.DOF(k) = rsdPp1[2][k];
      for (int k = 0; k < nDOFBCPp1; k++)
        lgfldrPp1.DOF(k) = rsdPp1[3][k];

      // jacobian nonzero pattern
      //
      //        u   q  uI  lg
      //   PDE  X   X   X   X
      //   Au   X   X   X   X
      //   Int  X   X   X   0
      //   BC   X   X   0   0

      //typedef SLA::SparseNonZeroPattern<MatrixQ> NonZeroPatternClass;

      //fld.SystemMatrixSize();
      DLA::MatrixD<NonZeroPatternClass> nzPp1 =
          {{ {nDOFPDEPp1, nDOFPDEPp1}, {nDOFPDEPp1, nDOFAuPp1}, {nDOFPDEPp1, nDOFIntPp1}, {nDOFPDEPp1, nDOFBCPp1} },
           { {nDOFAuPp1 , nDOFPDEPp1}, {nDOFAuPp1 , nDOFAuPp1}, {nDOFAuPp1 , nDOFIntPp1}, {nDOFAuPp1 , nDOFBCPp1} },
           { {nDOFIntPp1, nDOFPDEPp1}, {nDOFIntPp1, nDOFAuPp1}, {nDOFIntPp1, nDOFIntPp1}, {nDOFIntPp1, nDOFBCPp1} },
           { {nDOFBCPp1 , nDOFPDEPp1}, {nDOFBCPp1 , nDOFAuPp1}, {nDOFBCPp1 , nDOFIntPp1}, {nDOFBCPp1 , nDOFBCPp1} }};

      NonZeroPatternClass& nzPDEPp1_q  = nzPp1(0,0);
      NonZeroPatternClass& nzPDEPp1_a  = nzPp1(0,1);
      NonZeroPatternClass& nzPDEPp1_qI = nzPp1(0,2);
      NonZeroPatternClass& nzPDEPp1_lg = nzPp1(0,3);

      NonZeroPatternClass& nzAuPp1_q  = nzPp1(1,0);
      NonZeroPatternClass& nzAuPp1_a  = nzPp1(1,1);
      NonZeroPatternClass& nzAuPp1_qI = nzPp1(1,2);
      NonZeroPatternClass& nzAuPp1_lg = nzPp1(1,3);

      NonZeroPatternClass& nzIntPp1_q  = nzPp1(2,0);
      NonZeroPatternClass& nzIntPp1_a  = nzPp1(2,1);
      NonZeroPatternClass& nzIntPp1_qI = nzPp1(2,2);
      //NonZeroPatternClass& nzIntPp1_lg = nzPp1(2,3);

      NonZeroPatternClass& nzBCPp1_q  = nzPp1(3,0);
      NonZeroPatternClass& nzBCPp1_a  = nzPp1(3,1);
      //NonZeroPatternClass& nzBCPp1_qI = nzPp1(3,2);
      NonZeroPatternClass& nzBCPp1_lg = nzPp1(3,3);

      //timer jacPatPp1//timer;
      IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, nzPDEPp1_q, nzPDEPp1_a,
                                                                                      nzAuPp1_q, nzAuPp1_a),
                                              xfld, (qfldPp1, afldPp1), quadratureOrderMin, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, nzPDEPp1_q, nzPDEPp1_a, nzPDEPp1_qI,
                                                          nzAuPp1_q ,             nzAuPp1_qI ,
                                                          nzIntPp1_q, nzIntPp1_a, nzIntPp1_qI),
          xfld, (qfldPp1, afldPp1), qIfldPp1, quadratureOrderMin, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, nzPDEPp1_q, nzPDEPp1_a, nzPDEPp1_lg,
                                                               nzAuPp1_q , nzAuPp1_a , nzAuPp1_lg ,
                                                               nzBCPp1_q , nzBCPp1_a , nzBCPp1_lg ),
          xfld, (qfldPp1, afldPp1), lgfldPp1, quadratureOrderMin, nBoundaryTraceGroups);

      // HDG adjoint solution field
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qIfldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfldaPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre, BoundaryGroups);

      // jacobian transpose
      //Create a transposed non-zero pattern
      DLA::MatrixD<NonZeroPatternClass> nzTPp1(Transpose(nzPp1));

      SystemMatrixClass jacTPp1(nzTPp1);
      // ND version
      auto jacTPDEPp1_q  = Transpose(jacTPp1)(0,0);
      auto jacTPDEPp1_a  = Transpose(jacTPp1)(0,1);
      auto jacTPDEPp1_qI = Transpose(jacTPp1)(0,2);
      auto jacTPDEPp1_lg = Transpose(jacTPp1)(0,3);

      auto jacTAuPp1_q  = Transpose(jacTPp1)(1,0);
      auto jacTAuPp1_a  = Transpose(jacTPp1)(1,1);
      auto jacTAuPp1_qI = Transpose(jacTPp1)(1,2);
      auto jacTAuPp1_lg = Transpose(jacTPp1)(1,3);

      auto jacTIntPp1_q  = Transpose(jacTPp1)(2,0);
      auto jacTIntPp1_a  = Transpose(jacTPp1)(2,1);
      auto jacTIntPp1_qI = Transpose(jacTPp1)(2,2);
      //auto jacIntPp1_lg  = Transpose(jacTPp1)(2,3);

      auto jacTBCPp1_q  = Transpose(jacTPp1)(3,0);
      auto jacTBCPp1_a  = Transpose(jacTPp1)(3,1);
      //auto jacTBCPp1_qI = Transpose(jacTPp1)(3,2);
      auto jacTBCPp1_lg = Transpose(jacTPp1)(3,3);

      IntegrateCellGroups<TopoD2>::integrate( JacobianCell_HDG<SurrealClass>(fcnCell, jacTPDEPp1_q, jacTPDEPp1_a,
                                                                                      jacTAuPp1_q, jacTAuPp1_a),
                                              xfld, (qfldaPp1, afldaPp1), quadratureOrder, nCellGroups );

      IntegrateInteriorTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianInteriorTrace_HDG<SurrealClass>(fcnInt, jacTPDEPp1_q, jacTPDEPp1_a, jacTPDEPp1_qI,
                                                          jacTAuPp1_q ,               jacTAuPp1_qI ,
                                                          jacTIntPp1_q, jacTIntPp1_a, jacTIntPp1_qI),
          xfld, (qfldaPp1, afldaPp1), qIfldaPp1, quadratureOrder, nInteriorTraceGroups);

      IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
          JacobianBoundaryTrace_FieldTrace_HDG<SurrealClass>(fcnBC, jacTPDEPp1_q, jacTPDEPp1_a, jacTPDEPp1_lg,
                                                               jacTAuPp1_q , jacTAuPp1_a , jacTAuPp1_lg ,
                                                               jacTBCPp1_q , jacTBCPp1_a , jacTBCPp1_lg ),
          xfld, (qfldaPp1, afldaPp1), lgfldaPp1, quadratureOrder, nBoundaryTraceGroups);

      // Functional integral
      SystemVectorClass rhsPp1 = {{nDOFPDEPp1},  {nDOFAuPp1}, {nDOFIntPp1}, {nDOFBCPp1}};
      SystemVectorClass adjPp1 = {{nDOFPDEPp1},  {nDOFAuPp1}, {nDOFIntPp1}, {nDOFBCPp1}};

      rhsPp1 = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell<SurrealClass>( outputFcn, rhsPp1(0) ), xfld, qfldaPp1, quadratureOrder, xfld.nCellGroups() );

      // compute the adjoint
      adjPp1 = solver.inverse(jacTPp1)*rhsPp1;

      // update solution
      for (int k = 0; k < nDOFPDEPp1; k++)
        qfldaPp1.DOF(k) = adjPp1[0][k];
      for (int k = 0; k < nDOFPDEPp1; k++)
        for (int d = 0; d < PhysD2::D; d++)
          afldaPp1.DOF(k)[d] = adjPp1[1][PhysD2::D*k+d];
      for (int k = 0; k < nDOFIntPp1; k++)
        qIfldaPp1.DOF(k) = adjPp1[2][k];
      for (int k = 0; k < nDOFBCPp1; k++)
        lgfldaPp1.DOF(k) = adjPp1[3][k];

      // ---------------------
      // Adjoint error exact
      // ---------------------

      // HDG primal solution field
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> eqfld(xfld, 0, BasisFunctionCategory_Legendre);
      // auxiliary variable
      Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> eafld(xfld, 0, BasisFunctionCategory_Legendre);
      // interface solution
      Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> eqIfld(xfld, 0, BasisFunctionCategory_Legendre);
      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> elgfld(xfld, 0, BasisFunctionCategory_Legendre, BoundaryGroups);
      eqfld = 0;eafld = 0;eqIfld = 0;elgfld = 0;

      SystemVectorClass ersd = {{ eqfld.nDOF() },{ eafld.nDOF()*PhysD2::D },{ eqIfld.nDOF() },{ elgfld.nDOF()}};
      ersd = 0;
      //std::cout<< "eu:" << eqfld.nDOF() <<std::endl;
      //std::cout<< "eq:" << eafld.nDOF() <<std::endl;
      //std::cout<< "euI:" << eqIfld.nDOF() <<std::endl;
      //std::cout<< "elg:" << elgfld.nDOF() <<std::endl;

      AWResidualCell_HDG<TopoD2>::integrate( AWfcnCell, qfld, afld, quadratureOrder, nCellGroups, ersd(0), ersd(1) );
      AWResidualInteriorTrace_HDG<TopoD2>::integrate( AWfcnInt, qfld, afld, qIfld,
                                          quadratureOrder, nInteriorTraceGroups, ersd(0), ersd(1), ersd(2) );
      AWResidualBoundaryTrace_HDG<TopoD2>::integrate( AWfcnBound, qfld, afld,
                                          quadratureOrder, nBoundaryTraceGroups, ersd(0), ersd(1) );
      AWResidualBCTrace_HDG<TopoD2>::integrate( AWfcnBC, qfld, afld, lgfld,
                                          quadratureOrder, nBoundaryTraceGroups, ersd(0), ersd(1), ersd(3) );

      // storage field
      // populate the field
      for (int k = 0; k < eqfld.nDOF(); k++)
        eqfld.DOF(k) += ersd[0][k];
      for (int k = 0; k < eafld.nDOF(); k++)
        for (int d = 0; d < PhysD2::D; d++)
          eafld.DOF(k)[d] += ersd[1][PhysD2::D*k+d];
      for (int k = 0; k < eqIfld.nDOF(); k++)
        eqIfld.DOF(k) += ersd[2][k];
      for (int k = 0; k < elgfld.nDOF(); k++)
        elgfld.DOF(k) += ersd[3][k];

      // exact global and local error

      // Field Cell Types
      typedef typename XField<PhysD2, TopoD2            >::FieldCellGroupType<Triangle> XFieldCellGroupType;
      typedef typename Field< PhysD2, TopoD2, ArrayQ    >::FieldCellGroupType<Triangle> UFieldCellGroupType;
      typedef typename Field< PhysD2, TopoD2, VectorArrayQ>::FieldCellGroupType<Triangle> QFieldCellGroupType;

      typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;
      typedef typename UFieldCellGroupType::ElementType<> ElementUFieldClass;
      typedef typename QFieldCellGroupType::ElementType<> ElementQFieldClass;

      const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Triangle>(0);

      // solution - p
      UFieldCellGroupType&  qfldCell = qfld.getCellGroup<Triangle>(0);
      QFieldCellGroupType&  afldCell = afld.getCellGroup<Triangle>(0);

      // adjoint - p
      UFieldCellGroupType&  adjqfldCell = qflda.getCellGroup<Triangle>(0);
      QFieldCellGroupType&  adjafldCell = aflda.getCellGroup<Triangle>(0);

      // residual - p+1
      UFieldCellGroupType&  rsdqfldCellPp1 = qfldrPp1.getCellGroup<Triangle>(0);
      QFieldCellGroupType&  rsdafldCellPp1 = afldrPp1.getCellGroup<Triangle>(0);

      // adjoint - p+1
      UFieldCellGroupType&  adjqfldCellPp1 = qfldaPp1.getCellGroup<Triangle>(0);
      QFieldCellGroupType&  adjafldCellPp1 = afldaPp1.getCellGroup<Triangle>(0);

      // exact error
      UFieldCellGroupType&  eqfldCell = eqfld.getCellGroup<Triangle>(0);
      QFieldCellGroupType&  eafldCell = eafld.getCellGroup<Triangle>(0);

      // fields for element storage of local error estimates
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> localErrEstfld( xfld, 0, BasisFunctionCategory_Legendre );
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> localErrExtfld( xfld, 0, BasisFunctionCategory_Legendre );
      UFieldCellGroupType& efldEstimateCell = localErrEstfld.getCellGroup<Triangle>(0);
      UFieldCellGroupType& efldExactCell = localErrExtfld.getCellGroup<Triangle>(0);
      localErrEstfld = 0;
      localErrExtfld = 0;

      ElementUFieldClass efldEstimateElem( efldEstimateCell.basis() );
      ElementUFieldClass efldExactElem( efldExactCell.basis() );

      ElementXFieldClass  xfldElem( xfldCell.basis() );
      ElementUFieldClass  qfldElem( qfldCell.basis() );
      ElementQFieldClass  afldElem( afldCell.basis() );

      ElementUFieldClass  adjqfldElem( adjqfldCell.basis() );
      ElementQFieldClass  adjafldElem( adjafldCell.basis() );

      ElementUFieldClass  rsdqfldElemPp1( rsdqfldCellPp1.basis() );
      ElementQFieldClass  rsdafldElemPp1( rsdafldCellPp1.basis() );

      ElementUFieldClass  adjqfldElemPp1( adjqfldCellPp1.basis() );
      ElementQFieldClass  adjafldElemPp1( adjafldCellPp1.basis() );

      ElementUFieldClass  eqfldElem( eqfldCell.basis() );
      ElementQFieldClass  eafldElem( eafldCell.basis() );

      ArrayQ localSln=0;
      Real globalSln=0;

      IntegrandClass fcnint( volumeFcn, {0} ); // volumeFcn is g in (g,u) g = - Del psi

      for (int elem = 0; elem < xfldCell.nElem(); elem++)
      {
        xfldCell.getElement( xfldElem, elem );
        qfldCell.getElement( qfldElem, elem );

        FunctorClass fcn = fcnint.integrand(xfldElem,qfldElem);

        localSln = 0;
        ElemIntegral(fcn, xfldElem, localSln);
        globalSln += localSln;

        // local error estimate
        rsdqfldCellPp1.getElement( rsdqfldElemPp1, elem );
        rsdafldCellPp1.getElement( rsdafldElemPp1, elem );

        adjqfldCellPp1.getElement( adjqfldElemPp1, elem );
        adjafldCellPp1.getElement( adjafldElemPp1, elem );

        Real localErrEst = 0;
        for ( int n = 0; n < adjqfldElemPp1.nDOF(); n++ )
        {
          localErrEst += dot( rsdqfldElemPp1.DOF(n), adjqfldElemPp1.DOF(n) );
          localErrEst += dot( rsdafldElemPp1.DOF(n), adjafldElemPp1.DOF(n) );
        }

        // error in local error estimate
        // extract exact
        eqfldCell.getElement( eqfldElem, elem );
        eafldCell.getElement( eafldElem, elem );

        Real localErrExt = 0;
        localErrExt += eqfldElem.DOF(0);
        for (int d =0; d<PhysD2::D; d++)
        {
          localErrExt += eafldElem.DOF(0)[d];
        }

        efldEstimateElem.DOF(0) = localErrEst;
        efldEstimateCell.setElement( efldEstimateElem, elem );

        efldExactElem.DOF(0) = localErrExt;
        efldExactCell.setElement( efldExactElem, elem );

      }

      typedef typename Field<PhysD2, TopoD2, ArrayQ>::FieldTraceGroupType<Line> UFieldTraceGroupType;
      typedef typename XField<PhysD2, TopoD2>::FieldTraceGroupType<Line> XFieldTraceGroupType;
      typedef typename UFieldTraceGroupType::ElementType<> ElementUFieldTraceClass;
      typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;

      // loop over interior face groups
      for ( int group = 0; group <nInteriorTraceGroups; group++)
      {
        const XFieldTraceGroupType xfldInteriorTrace = xfld.getInteriorTraceGroup<Line>(group);

        UFieldTraceGroupType adjqIfldInteriorTrace = qIflda.getInteriorTraceGroup<Line>(group);

        UFieldTraceGroupType rsdqIfldInteriorTracePp1 = qIfldrPp1.getInteriorTraceGroup<Line>(group);
        UFieldTraceGroupType adjqIfldInteriorTracePp1 = qIfldrPp1.getInteriorTraceGroup<Line>(group);

        UFieldTraceGroupType eqIfldInteriorTrace = eqIfld.getInteriorTraceGroup<Line>(group);

        ElementXFieldTraceClass xfldElem( xfldInteriorTrace.basis() );

        // adjoint in p
        ElementUFieldTraceClass adjqIfldElem( adjqIfldInteriorTrace.basis() );

        // rsd projected from p to p+1, adjoint solved in p+1
        ElementUFieldTraceClass rsdqIfldElemPp1( rsdqIfldInteriorTracePp1.basis() );
        ElementUFieldTraceClass adjqIfldElemPp1( adjqIfldInteriorTracePp1.basis() );

        // ext field
        ElementUFieldTraceClass eqIfldElem( eqIfldInteriorTrace.basis() );

        for (int elem = 0; elem< xfldInteriorTrace.nElem(); elem++)
        {
          xfldInteriorTrace.getElement( xfldElem, elem );

          rsdqIfldInteriorTracePp1.getElement( rsdqIfldElemPp1, elem );
          adjqIfldInteriorTracePp1.getElement( adjqIfldElemPp1, elem );

          Real localErrEst = 0;
          for (int n=0; n< adjqIfldElemPp1.nDOF(); n++)
          {
            localErrEst += dot( rsdqIfldElemPp1.DOF(n), adjqIfldElemPp1.DOF(n) );
          }

          // extract exact
          eqIfldInteriorTrace.getElement( eqIfldElem, elem );

          Real localErrExt = 0;
          localErrExt += eqIfldElem.DOF(0);

          // Distribute the local error estimates to adjacent elements
          int elemL = xfldInteriorTrace.getElementLeft( elem );
          int elemR = xfldInteriorTrace.getElementRight( elem );

          efldEstimateCell.getElement( efldEstimateElem, elemL );
          efldEstimateElem.DOF(0) += 0.5*localErrEst;
          efldEstimateCell.setElement( efldEstimateElem, elemL );

          efldEstimateCell.getElement( efldEstimateElem, elemR );
          efldEstimateElem.DOF(0) += 0.5*localErrEst;
          efldEstimateCell.setElement( efldEstimateElem, elemR );

          efldExactCell.getElement( efldExactElem, elemL );
          efldExactElem.DOF(0) += 0.5*localErrExt;
          efldExactCell.setElement( efldExactElem, elemL );

          efldExactCell.getElement( efldExactElem, elemR );
          efldExactElem.DOF(0) += 0.5*localErrExt;
          efldExactCell.setElement( efldExactElem, elemR );
        } // loop over elements of trace group
      } // loop over interior trace groups

      //boundary contributions
      for (int group = 0; group < nBoundaryTraceGroups; group++)
      {
        const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<Line>(group);

        UFieldTraceGroupType adjlgfldBoundaryTrace = lgflda.getBoundaryTraceGroup<Line>(group);

        UFieldTraceGroupType rsdlgfldBoundaryTracePp1 = lgfldrPp1.getBoundaryTraceGroup<Line>(group);
        UFieldTraceGroupType adjlgfldBoundaryTracePp1 = lgfldaPp1.getBoundaryTraceGroup<Line>(group);

        UFieldTraceGroupType elgfldBoundaryTrace = elgfld.getBoundaryTraceGroup<Line>(group);

        ElementXFieldTraceClass xfldElem( xfldBoundaryTrace.basis() );

        ElementUFieldTraceClass adjlgfldElem( adjlgfldBoundaryTrace.basis() );

        ElementUFieldTraceClass rsdlgfldElemPp1( rsdlgfldBoundaryTracePp1.basis() );
        ElementUFieldTraceClass adjlgfldElemPp1( adjlgfldBoundaryTracePp1.basis() );

        ElementUFieldTraceClass elgfldElem( elgfldBoundaryTrace.basis() );

        for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
        {
          xfldBoundaryTrace.getElement( xfldElem, elem );

          rsdlgfldBoundaryTracePp1.getElement( rsdlgfldElemPp1, elem );
          adjlgfldBoundaryTracePp1.getElement( adjlgfldElemPp1, elem );

          Real localErrEst = 0;
          for (int n=0; n< adjlgfldElemPp1.nDOF(); n++)
          {
            localErrEst += dot( rsdlgfldElemPp1.DOF(n), adjlgfldElemPp1.DOF(n) );
          }

          // extract exact
          elgfldBoundaryTrace.getElement( elgfldElem, elem );

          Real localErrExt = 0;
          localErrExt += elgfldElem.DOF(0);

          // Distribute the local error estimates to adjacent elements
          int elemL = xfldBoundaryTrace.getElementLeft( elem );

          efldEstimateCell.getElement( efldEstimateElem, elemL );
          efldEstimateElem.DOF(0) += localErrEst;
          efldEstimateCell.setElement( efldEstimateElem, elemL );

          efldExactCell.getElement( efldExactElem, elemL );
          efldExactElem.DOF(0) += localErrExt;
          efldExactCell.setElement( efldExactElem, elemL );
        } // loop over elements of trace group
      } // loop over boundary trace groups

      Real avglocalErrEstEff=0,globalErrEst=0;

#if 1 // actual average local estimate and effectivity
      KahanSum<Real> avgLocalErrEst,avgLocalErrExt,avglocalErrEstError;
      KahanSum<Real> sumErrEst;
      for (int elem = 0; elem < xfldCell.nElem(); elem++)
      {
        efldEstimateCell.getElement( efldEstimateElem, elem );
        efldExactCell.getElement( efldExactElem, elem );

        // Estimate
        avgLocalErrEst += fabs(efldEstimateElem.DOF(0));
        sumErrEst += efldEstimateElem.DOF(0);

        // Exact
        avgLocalErrExt += fabs(efldExactElem.DOF(0));

        // Error in the Estimate
        avglocalErrEstError += fabs(efldExactElem.DOF(0)- efldEstimateElem.DOF(0));
      }
      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExt;
      avgLocalErrExt      /= xfldCell.nElem();
      avgLocalErrEst      /= xfldCell.nElem();
      avglocalErrEstError /= xfldCell.nElem();
      globalErrEst = sumErrEst;
#else // specific element - not really average at all
      /*
       *    \  |  /
       *     \ | /
       * =============
       *     / | \
       *    /  |  \
       *
       * top row l:r e,f,g,h
       * bot row l:r a,b,c,d
       * ((ii*jj) -  1) - ii  === b
       *
       * to add row, add (2*ii)
       * a = ((ii*jj) -  1) - ii - 1;
       * b = ((ii*jj) -  1) - ii;
       * c = ((ii*jj) -  1) - ii + 1;
       * b = ((ii*jj) -  1) - ii + 2;
       *
       * e = ((ii*jj) -  1) + ii - 1;
       * f = ((ii*jj) -  1) + ii;
       * g = ((ii*jj) -  1) + ii + 1;
       * h = ((ii*jj) -  1) + ii + 2;
       */

      int elem;
      //elem = 0; // 1st elem
      elem = ((ii*jj) -  1) - ii; // b

      efldEstimateCell.getElement( efldEstimateElem, elem );
      Real localErrEst = efldEstimateElem.DOF(0)(0);

      efldExactCell.getElement( efldExactElem, elem );
      Real localErrExact = efldExactElem.DOF(0)(0);

      avgLocalErrExact    = fabs(localErrExact);
      avgLocalErrEst      = fabs(localErrEst);
      avglocalErrEstError = fabs(localErrExact - localErrEst);

      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExact;
#endif

      Real globalError = globalSln - globalExt;

      hVec[indx] = 1./ii;

      globalErrorVec[indx] = abs(globalError);
      globalEstVec[indx] = abs(globalErrEst);
      globalEstErrVec[indx] = abs( globalError - globalErrEst );
      globalEffVec[indx] = abs( 1 - globalErrEst/globalError );

      localErrorVec[indx] = abs(avgLocalErrExt);
      localEstVec[indx] = abs(avgLocalErrEst);
      localEstErrVec[indx] = avglocalErrEstError;
      localEffVec[indx] = avglocalErrEstEff;

      if (indx > 0)
      {
        globalErrorRate[indx]  = (log(globalErrorVec[indx])  - log(globalErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstRate[indx]    = (log(globalEstVec[indx])    - log(globalEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstErrRate[indx] = (log(globalEstErrVec[indx]) - log(globalEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        globalEffRate[indx]    = (log(globalEffVec[indx])    - log(globalEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));

        localErrorRate[indx]  = (log(localErrorVec[indx])  - log(localErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstRate[indx]    = (log(localEstVec[indx])    - log(localEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstErrRate[indx] = (log(localEstErrVec[indx]) - log(localEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        localEffRate[indx]    = (log(localEffVec[indx])    - log(localEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
      }
      else
      {
        globalErrorRate[indx]  = 0;
        globalEstRate[indx]    = 0;
        globalEstErrRate[indx] = 0;
        globalEffRate[indx]    = 0;

        localErrorRate[indx]  = 0;
        localEstRate[indx]    = 0;
        localEstErrRate[indx] = 0;
        localEffRate[indx]    = 0;
      }

      if ( power == powermin )
      {
        cout << "P = " << order << endl;
        cout << "  \t";
        cout << " global               global               global               global              ";
        cout << " local                local                local                local" << endl;
        cout << "ii\t";
        cout << " exact                estimate             err in estimate      effectivity         ";
        cout << " exact                estimate             err in estimate      effectivity" << endl;

        resultFile << "ZONE T=\"HDG("<<lRef <<")P=" << order << "\"" << endl;

#ifndef SANS_FULLTEST
        pyriteFile << order << endl;
#endif
      }
      // For tecplot
      resultFile << std::setprecision(16) << std::scientific;
      resultFile << hVec[indx];
      resultFile << ", " << globalErrorVec[indx];
      resultFile << ", " << globalEstVec[indx];
      resultFile << ", " << globalEstErrVec[indx];
      resultFile << ", " << globalEffVec[indx];
      resultFile << ", " << globalErrorRate[indx];
      resultFile << ", " << globalEstRate[indx];
      resultFile << ", " << globalEstErrRate[indx];
      resultFile << ", " << globalEffRate[indx];

      resultFile << ", " << localErrorVec[indx];
      resultFile << ", " << localEstVec[indx];
      resultFile << ", " << localEstErrVec[indx];
      resultFile << ", " << localEffVec[indx];
      resultFile << ", " << localErrorRate[indx];
      resultFile << ", " << localEstRate[indx];
      resultFile << ", " << localEstErrRate[indx];
      resultFile << ", " << localEffRate[indx];
      resultFile << endl;

      // For Latex
      datFile << std::setprecision(16) << std::scientific;
      if (indx==0)
      {
        datFile << order;
      }
      else
      {
        datFile << " ";
      }
      datFile << "," << hVec[indx];
      datFile << "," << globalErrorVec[indx];
      datFile << "," << globalEstVec[indx];
      datFile << "," << globalEstErrVec[indx];
      datFile << "," << globalEffVec[indx];
      datFile << "," << globalErrorRate[indx];
      datFile << "," << globalEstRate[indx];
      datFile << "," << globalEstErrRate[indx];
      datFile << "," << globalEffRate[indx];
      datFile << "," << localErrorVec[indx];
      datFile << "," << localEstVec[indx];
      datFile << "," << localEstErrVec[indx];
      datFile << "," << localEffVec[indx];
      datFile << "," << localErrorRate[indx];
      datFile << "," << localEstRate[indx];
      datFile << "," << localEstErrRate[indx];
      datFile << "," << localEffRate[indx];
      datFile << endl;

#ifndef SANS_FULLTEST
      pyriteFile << hVec[indx]; // grid
      pyriteFile << globalErrorVec[indx] << globalEstVec[indx]<< globalEstErrVec[indx]; // values
      pyriteFile << globalErrorRate[indx]<< globalEstRate[indx]<< globalEstErrRate[indx]; // rates
      pyriteFile << globalEffVec[indx]<< globalEffRate[indx]; //effectivities
      pyriteFile << endl;
#endif

      cout << ii << "\t";
      cout << std::scientific;
      cout << std::scientific << std::setprecision(5) << globalErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEffRate[indx] << ")  ";

      cout << std::scientific << std::setprecision(5) << localErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEffRate[indx] << ")  ";
      cout << endl;

      indx++;


//#endif // TEMP DEBUGGING
    }
    cout << endl;
  }
  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;
}



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
