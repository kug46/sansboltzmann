// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp> // to automagically make the directories

//#define SANS_FULLTEST

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <fstream>

#include "pde/AnalyticFunction/ScalarFunction4D.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/NDConvert/OutputNDConvertSpace4D.h"
#include "pde/NDConvert/SolnNDConvertSpaceTime3D.h"

#include "Field/FieldSpacetime_DG_Cell.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/tools/for_each_CellGroup.h"

#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorOrder_4D_DG_Pentatope_test_suite )

#ifdef SANS_AVRO // only avro provides the KuhnFreudenthal triangulation

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorOrder_4D_DG_Pentatope )
{
#if 0
  typedef SolnNDConvertSpaceTime<PhysD3, ScalarFunction4D_SinFun4> SolutionExact;
  SolutionExact solnExact;
#elif 0
  typedef SolnNDConvertSpaceTime<PhysD3, ScalarFunction4D_Const> SolutionExact;
  SolutionExact solnExact(1);
#elif 0
  typedef SolnNDConvertSpaceTime<PhysD3, ScalarFunction4D_Linear> SolutionExact;
  SolutionExact solnExact(1,2,3,4,5);
#elif 1
  typedef SolnNDConvertSpaceTime<PhysD3, ScalarFunction4D_Monomial> SolutionExact;
  std::vector<Real> coeffs = {.1,.2,.3,.4};
  std::vector<int> E = {3,1,2,4};
  SolutionExact solnExact( 1. , coeffs, E );
#elif 0
  typedef SolnNDConvertSpaceTime<PhysD3,ScalarFunction4D_BoundaryLayer> SolutionExact;
  Real epsilon = 0.01;
  int order = 1;
  Real beta = pow(2.,order+1);
  SolutionExact solnExact(epsilon,beta,order);
#endif

  typedef PhysD4 PhysDim;
  typedef TopoD4 TopoDim;

  typedef OutputNDConvertSpace<PhysDim, OutputCell_SolutionErrorSquared<AdvectionDiffusionTraits<PhysDim>, SolutionExact>> L2ErrorClass;
  typedef IntegrandCell_Galerkin_Output<L2ErrorClass> L2ErrorIntegrandClass;

  mpi::communicator world;
  mpi::communicator comm = world.split(world.rank());

#ifdef SANS_FULLTEST
  int orderL = 1, orderH = 3;
  std::vector<int> sizes = {2,4,6,8};
#else
  int orderL = 1, orderH = 1;
  std::vector<int> sizes = {2,4,6};
#endif

  // create the set of meshes we will use for the convergence studies
  std::vector< std::shared_ptr<XField_KuhnFreudenthal<PhysD4,TopoD4>> > meshes(sizes.size());
  for (int k=0;k<(int)sizes.size();k++)
  {
    std::vector<int> dims(4,sizes[k]);
    meshes[k] = std::make_shared<XField_KuhnFreudenthal<PhysD4,TopoD4>>(comm,dims);
  }

  std::vector<int> cellGroups = {0};

  for (int order=orderL; order <= orderH; order++)
  {
    std::vector<Real> errors( meshes.size() );
    std::vector<Real> happrox( meshes.size() );
    for (int imesh=0;imesh<(int)meshes.size();imesh++)
    {
      // retrieve the mesh
      XField<PhysD4,TopoD4>& xfld = *meshes[imesh];

      // create the solution field
      Field_DG_Cell<PhysD4,TopoD4,Real> qfld(xfld, order, BasisFunctionCategory_Lagrange);

      const int quadOrder = 2*(order+1);

      QuadratureOrder quadratureOrder(xfld, quadOrder);

      //Perform L2 projection from solution on previous mesh
      qfld = 0;

      for_each_CellGroup<TopoD4>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}),
                                         (xfld, qfld) );

      // Compute L2 error
      L2ErrorClass L2ErrorOutput(solnExact);
      L2ErrorIntegrandClass L2ErrorIntegrand(L2ErrorOutput, {0});

      Real L2error = 0;
      IntegrateCellGroups<TopoDim>::integrate( FunctionalCell_Galerkin( L2ErrorIntegrand, L2error ),
                                               xfld, qfld,
                                               quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      if (L2error<0.0)
      {
        // The Grundmann-Moeller quadrature rules (used in 4d) can have negative weights.
        // While the sum of the weights is still 1, this can
        // cause roundoff problems if we are integrating a
        // very wiggly function on a coarse mesh.
        // The output can be negative so take abs before taking
        // sqrt...this will likely cause a wrong answer but
        // as we refine, hopefully the function is better
        // approximated so that there is less wiggliness over the pentatopes.
        SANS_DEVELOPER_EXCEPTION(
         "warning: quadrature causing numerical roundoff problems -> taking sqrt(abs(L2_Error)).\n"
         "         the answer is probably wrong so think about the wiggliness of your function\n"
         "         on a coarse mesh such as the one you are currently using.\n");
      }
      L2error = sqrt(L2error);

      printf("error for order %d (ndof = %d): %1.12e\n",order,qfld.nDOF(),L2error);
      errors[imesh]  = log( L2error );
      happrox[imesh] = (1./PhysD4::D)*log( qfld.nDOF() );
    } // mesh size loop

    // compute the slope
    int last = meshes.size()-1;
    Real slope = -( errors[last] - errors[last-1] ) / ( happrox[last] - happrox[last-1] );

    #if 0
    std::string filename = "errororder_p"+std::to_string(order)+".dat";
    FILE *fid = fopen(filename.c_str(),"w");
    for (size_t i=0;i<meshes.size();i++)
      fprintf(fid,"%1.16e %1.16e\n",happrox[i],errors[i]);
    fclose(fid);
    #endif

    BOOST_CHECK_CLOSE( slope , order+1 , 4 ); // very relaxed check (3%)

  } // order loop

}

#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
