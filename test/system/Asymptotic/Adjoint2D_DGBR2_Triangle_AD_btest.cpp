// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Adjoint2D_DGBR2_Triangle_AD_btest
// testing of 2-D DG adjoint with Advection-Diffusion on triangles

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "tools/stringify.h"
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"
#include "pde/OutputCell_SolutionErrorSquared.h"

#include "pde/BCParameters.h" // Needed because the BCVector is extended here

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( Adjoint2D_DGBR2_Triangle_AD_test_suite )

//----------------------------------------------------------------------------//
// solution: Hierarchical P1 thru P3
// Lagrange multiplier: continuous Hierarchical P1 thru P3
// exact solution: double BL
BOOST_AUTO_TEST_CASE( Adjoint2D_DGBR2_Triangle_AD )
{
  typedef SurrealS<1> SurrealClass;

   // Laplacian w/ vortex
  typedef ScalarFunction2D_Vortex SolutionExact;
  typedef SolnNDConvertSpace<PhysD2, SolutionExact> NDSolutionExact;
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef BCTypeFunctionLinearRobin_mitLG BCType;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE

  Real u = 0;
  Real v = 0;
  AdvectiveFlux2D_Uniform adv( u, v );

  Real nu = 1;
  ViscousFlux2D_Uniform visc( nu, 0, 0, nu );

  Source2D_UniformGrad source(0,0,0);

  Real xvort = -1./sqrt(3);
  Real yvort = 1./sqrt(7);

  // Create a solution dictionary
  PyDict solnArgs;
  solnArgs[NDSolutionExact::ParamsType::params.x0] = xvort;
  solnArgs[NDSolutionExact::ParamsType::params.y0] = yvort;

  NDSolutionExact solnExact( solnArgs );

  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );

  NDPDEClass pde( adv, visc, source, forcingptr );

  // BC
  PyDict Vortex;
  Vortex[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function.Vortex;
  Vortex[SolutionExact::ParamsType::params.x0] = xvort;
  Vortex[SolutionExact::ParamsType::params.y0] = yvort;

  PyDict BCSoln;
  BCSoln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  BCSoln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = Vortex;

  PyDict PyBCList;
  PyBCList["BCName"] = BCSoln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCName"] = {0, 1, 2, 3};


  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // Adjoint PDE
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_SineSine> AdjointExact;

  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdjoint2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdjoint2D> PDEAdjointClass;

  typedef ScalarFunction2D_ForcingFunction<PDEAdjointClass> WeightFcn; // Change this to change output weighting type
  typedef OutputCell_WeightedSolution<PDEClass, WeightFcn> OutputClass;
  typedef OutputNDConvertSpace<PhysD2, OutputClass> NDOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDOutputClass> OutputIntegrandClass;

  typedef OutputCell_SolutionErrorSquared<PDEAdjointClass, AdjointExact> ErrorClass;
  typedef OutputNDConvertSpace<PhysD2, ErrorClass> NDErrorClass;
  typedef IntegrandCell_DGBR2_Output<NDErrorClass> ErrorIntegrandClass;

  // Adjoint PDE
  AdvectiveFlux2D_Uniform adv_adj( -u, -v );

  AdjointExact adjExact;

  typedef ForcingFunction2D_MMS<PDEAdjoint2D> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact) );

  PDEAdjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  WeightFcn volumeFcn( pdeadj ); // This is 'g' in when integrating the solution as the output


  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  // integrands
  NDOutputClass fcnOutput(volumeFcn);
  OutputIntegrandClass outputIntegrand(fcnOutput, {0});

  NDErrorClass fcnError(adjExact);
  ErrorIntegrandClass errorIntegrand(fcnError, {0});

  // norm data
  Real hVec[10];
  Real hDOFVec[10];   // 1/sqrt(DOF)
  Real normVec[10];   // L2 error
  int indx;

  // Tecplot output
#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/adjL2_2D_DGBR2.plt", std::ios::out);
  pyrite_file_stream pyriteFile("IO/ErrorOrder/Adjoint2D_DGBR2_Triangle_AD_FullTest.txt", 1e-10, 5e-4, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  pyrite_file_stream pyriteFile("IO/ErrorOrder/Adjoint2D_DGBR2_Triangle_AD_MinTest.txt", 1e-10, 1e-10, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"1/sqrt(DOF)\"";
  resultFile << ", \"L2 error\"";
  resultFile << ", \"L2 error rate\"";
  resultFile << std::endl;

  resultFile << std::setprecision(16) << std::scientific;
  pyriteFile << std::setprecision(16) << std::scientific;

  int ordermin = 1;
#ifdef SANS_FULLTEST
#ifdef SANS_VERBOSE
  cout << "...running full test" << endl;
#endif
  int ordermax = 5;
#else
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 5;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
      ii = pow( 2, power );
      jj = ii;

      // grid:

//      XField2D_Box_Triangle_X1 xfld( ii, jj );
      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );

      // solution: Legendre, C0
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
      qfld = 0;

      const int nDOFPDE = qfld.nDOF();

      // lifting operators
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, order, BasisFunctionCategory_Legendre);
      rfld = 0;

//      const int nDOFLO = D*rfld.nDOF();

      // Lagrange multiplier: Hierarchical, C0 (also at corners)
#if 0
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
#elif 1
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order, BasisFunctionCategory_Hierarchical );
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld( xfld, order, BasisFunctionCategory_Hierarchical );
#else
      QField2D_DG_BoundaryEdge<NDPDEClass> lgfld( xfld, order, BasisFunctionCategory_Legendre );
#endif
      lgfld = 0;

      // quadrature rule
      QuadratureOrder quadratureOrder( xfld, - 1 );

      std::vector<Real> tol = {1e-12, 1e-12};
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder, ResidualNorm_Default, tol,
                                         {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      // residual
      SystemVectorClass q(PrimalEqSet.vectorStateSize());

      PrimalEqSet.fillSystemVector(q);

      // adjoint (right hand side)

      SystemVectorClass rhs(q.size());
      rhs = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_DGBR2<SurrealClass>( outputIntegrand, rhs(PrimalEqSet.iPDE) ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // solve

      SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);

      SystemVectorClass adj(q.size());

      solverAdj.solve(rhs, adj);

      PrimalEqSet.setAdjointField(adj, wfld, sfld, mufld);

      // L2 solution error
      ArrayQ SquareError = 0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( errorIntegrand, SquareError ),
          xfld, (wfld, sfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      //FunctionalCell<TopoD2>::integrate( fcnGradErr, sfld, quadratureOrder, 1, SquareError );
      Real norm = SquareError;

      hVec[indx] = 1./ii;
      hDOFVec[indx] = 1./sqrt(nDOFPDE);
      normVec[indx] = sqrt( norm );
      indx++;

#ifdef SANS_VERBOSE
      cout << "P = " << order << " ii = " << ii << ": L2 solution error = " << sqrt( norm );
      if (indx > 1)
        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;
#endif

#if 0
      // Tecplot dump
      string filename = "tmp/adjDG_P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( qflda, filename );
#endif
#if 0
      cout << "dumping lg:" << endl;
      lgfldBoundaryEdgeBase[0]->dump(2);
      lgfldBoundaryEdgeBase[1]->dump(2);
      lgfldBoundaryEdgeBase[2]->dump(2);
      lgfldBoundaryEdgeBase[3]->dump(2);
#endif

    } //grid refinement loop

    // Tecplot output
    resultFile << "ZONE T=\"CG P=" << order << "\"" << std::endl;
    for (int n = 0; n < indx; n++)
    {
      Real slope = 0;
      if (n > 0)
        slope = (log(normVec[n])  - log(normVec[n-1])) /(log(hVec[n]) - log(hVec[n-1]));
      resultFile << hVec[n];
      resultFile << ", " << hDOFVec[n];
      resultFile << ", " << normVec[n];
      resultFile << ", " << slope;
      resultFile << endl;

      pyriteFile << hDOFVec[n] << normVec[n];
      pyriteFile.close(5e-5) << slope << std::endl;
    }

#if 0
    // Mathematica dump
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      cout << "{" << hVec[n] << "," << normVec[n] << "}";
      if (n < indx-1)
        cout << ",";
    }
    cout << "}" << endl;
#endif
#if 0
    // Mathematica dump (cut & paste)
    Real z, base;
    int exponent;
    cout << "P = " << order << "  {";
    for (int n = 0; n < indx; n++)
    {
      z = hVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << "{" << base << " 10^" << exponent << ",";
      z = normVec[n];
      exponent = floor( log10( std::abs(z) ) );
      base     = z / pow(10, exponent);
      cout << base << " 10^" << exponent << "}";
      if (n < indx-1)
        cout << ", ";
    }
    cout << "}" << endl;
#endif
  } //order loop
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
