// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ErrorEstimateOrder_2D_DGBR2_Triangle_AD_btest
// testing error estimation of 2-D DG with Advection-Diffusion on triangles

// #define SANS_FULLTEST

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "tools/KahanSum.h"
#include "pyrite_fstream.h"

#include "Surreal/SurrealS.h"

//PDE
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"

#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/OutputCell_WeightedSolution.h"

#include "pde/BCParameters.h"

//Solve includes
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementProjection_Specialization_L2.h"

#include "Field/Function/WeightedFunctionIntegral.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"

#include "Discretization/IntegrateCellGroups.h"

//Linear algebra
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#include "Field/output_grm.h"
#include "tools/stringify.h"

// Estimation
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( ErrorEst_2D_DGBR2_Triangle_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ErrorEst2D_DGBR2_Triangle_AD )
{
  timer totaltime;
  typedef SurrealS<1> SurrealClass;

  // Primal PDE
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCTypeFunction_mitState<AdvectiveFlux2D_Uniform,ViscousFlux2D_Uniform> BCType;
  typedef BCAdvectionDiffusion<PhysD2,BCType> BCClass;
  typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;

  typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;

  // AlgebraicEquationSet and ErrorEstimate
  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef ErrorEstimate_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,XField<PhysD2,TopoD2>> ErrorEstimateClass;

  // Adjoint PDE
  typedef PDEAdvectionDiffusion<PhysD2,
                                AdvectiveFlux2D_Uniform,
                                ViscousFlux2D_Uniform,
                                Source2D_UniformGrad > PDEAdjoint2D;
  typedef PDENDConvertSpace<PhysD2, PDEAdjoint2D> NDPDEAdjointClass;

  // Integrand Functors
  // call this class with something of Adjoint Exact Class
  typedef ScalarFunction2D_ForcingFunction<NDPDEAdjointClass> WeightFunctional;
  typedef SolnNDConvertSpace<PhysD2, WeightFunctional>       NDWeightFunctional;

  typedef OutputCell_WeightedSolution<PDEClass, NDWeightFunctional> WeightOutputClass;
  typedef OutputNDConvertSpace<PhysD2, WeightOutputClass>           NDWeightOutputClass;
  typedef IntegrandCell_DGBR2_Output<NDWeightOutputClass>           IntegrandClass;

#if 1  // Laplacian w/ Exponential functions
  // case in Hugh's Thesis and pyrite
  typedef ScalarFunction2D_VarExp3 SolutionExact;
  typedef ScalarFunction2D_SineSine AdjointExact;

  // PDE
  Real u=0.,v=0.; // convective velocities
  Real nu = 1;
  Real nu_xx = nu, nu_xy = 0, nu_yx = 0, nu_yy = nu; // viscosity

  // Create a solution dictionary
  Real a  = 3;
  PyDict solnArgs;
  solnArgs[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.VarExp3;
  solnArgs[SolutionExact::ParamsType::params.a] = a;

  a=1;
  PyDict adjArgs;
  adjArgs[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.Name] =
      BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function.SineSine;
  adjArgs[AdjointExact::ParamsType::params.a] = a;
  adjArgs[AdjointExact::ParamsType::params.b] = a;
  cout << "case: Laplacian w/ Exponentials" << endl;

#endif

  // ND verions of solution and adjoint
  typedef SolnNDConvertSpace<PhysD2, SolutionExact>  NDSolutionExact;
  typedef SolnNDConvertSpace<PhysD2, AdjointExact>   NDAdjointExact;

  // Primal PDE setup
  AdvectiveFlux2D_Uniform adv(u,v);
  ViscousFlux2D_Uniform visc( nu_xx, nu_xy, nu_yx, nu_yy );
  Source2D_UniformGrad source(0,0,0);

  NDSolutionExact solnExact( solnArgs ); // all solutions come from a dictionary
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(solnExact) );
  std::shared_ptr<SolutionExact> solutionptr( new SolutionExact(solnArgs) );
  NDPDEClass pde( adv, visc, source, forcingptr );

  // Solution BC
  PyDict BCDirichlet_Soln;
  // BCDirichlet_Soln[BCParams::params.BC.BCType] = BCParams::params.BC.FunctionLinearRobin_mitLG;
  // BCDirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.Function] = solnArgs;
  // BCDirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.A] = 1;
  // BCDirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCType>::params.B] = 0;

  BCDirichlet_Soln[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCDirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Function] = solnArgs;
  BCDirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType] =
                    BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.SolutionBCType.Dirichlet;
  BCDirichlet_Soln[BCAdvectionDiffusionParams<PhysD2,BCTypeFunction_mitStateParam>::params.Upwind] = false;


  PyDict PyBCList;
  PyBCList["BCDirichlet_Soln"] = BCDirichlet_Soln;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCDirichlet_Soln"] = {0, 1, 2, 3};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);
  std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  // Function mit LG
  // NDBCClass bc( solutionptr, visc ); // using the shared_ptr interface
  // Function mitState
  NDBCClass bc( solutionptr, adv, visc, "Dirichlet", false );

  // Adjoint solution
  NDAdjointExact adjExact(adjArgs); // ND version needed for calc later

  // Adjoint PDE
  AdvectiveFlux2D_Uniform adv_adj( -u, -v );

  typedef ForcingFunction2D_MMS<PDEAdjoint2D> ForcingAdjType;
  std::shared_ptr<ForcingAdjType> forcingadjptr( new ForcingAdjType(adjExact));
  NDPDEAdjointClass pdeadj( adv_adj, visc, source, forcingadjptr );

  NDWeightFunctional volumeFcn( pdeadj );

  // volumeFcn is g in (g,u) g = - Del psi
  NDWeightOutputClass weightOutput( volumeFcn );
  IntegrandClass outputFcn( weightOutput, {0} );


  // linear system setup
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  // BR2 discretization
  Real viscousEtaParameter = 3;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  // norm data
  const int vsize = 20;
  Real hVec[vsize];
  Real globalErrorVec[vsize], globalEstVec[vsize], globalEstErrVec[vsize], globalEffVec[vsize];
  Real localErrorVec[vsize], localEstVec[vsize], localEstErrVec[vsize], localEffVec[vsize];
  Real globalErrorRate[vsize], globalEstRate[vsize], globalEstErrRate[vsize], globalEffRate[vsize];
  Real localErrorRate[vsize], localEstRate[vsize], localEstErrRate[vsize], localEffRate[vsize];
  int indx;

#ifdef SANS_FULLTEST
  std::ofstream resultFile("tmp/ErrorEst2D_DG.plt", std::ios::out);
  std::ofstream datFile("tmp/ErrorEst2D_DG.dat", std::ios::out);
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_2D_DG_AD_FullTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 1e-9, 5e-1, pyrite_file_stream::check);
#else
  std::stringstream resultFile;
  std::stringstream datFile;
  std::string pyritefilename = "IO/ErrorEstimates/ErrorEst_2D_DG_AD_MinTest.txt";
  pyrite_file_stream pyriteFile(pyritefilename, 1e-9, 5e-9, pyrite_file_stream::check);
#endif
  resultFile << "VARIABLES=";
  resultFile << "\"h\"";
  resultFile << ", \"Global Error\"";
  resultFile << ", \"Global Error Estimate\"";
  resultFile << ", \"Global Error Estimate Error\"";
  resultFile << ", \"Global 1-Effectivity\"";
  resultFile << ", \"Global Error Rate\"";
  resultFile << ", \"Global Error Estimate Rate\"";
  resultFile << ", \"Global Error Estimate Error Rate\"";
  resultFile << ", \"Global 1-Effectivity Rate\"";
  resultFile << ", \"Local Error\"";
  resultFile << ", \"Local Error Estimate\"";
  resultFile << ", \"Local Error Estimate Error\"";
  resultFile << ", \"Local 1-Effectivity\"";
  resultFile << ", \"Local Error Rate\"";
  resultFile << ", \"Local Error Estimate Rate\"";
  resultFile << ", \"Local Error Estimate Error Rate\"";
  resultFile << ", \"Local 1-Effectivity Rate\"";
  resultFile << endl;

  // For Latex Output
  datFile << "order,"
      "h,"
      "globalError,"
      "globalErrorEstimate,"
      "globalErrorEstimateError,"
      "globalEffectivity,"
      "globalErrorRate,"
      "globalErrorEstimateRate,"
      "globalErrorEstimateErrorRate,"
      "globalEffectivityRate,"
      "localError,"
      "localErrorEstimate,"
      "localErrorEstimateError,"
      "localEffectivity,"
      "localErrorRate,"
      "localErrorEstimateRate,"
      "localErrorEstimateErrorRate,"
      "localEffectivityRate";
  datFile << endl;

  //---------------------------------------------------------------------------//
  // Computer the exact output on a fine grid and high-quadrature
  Real globalExt=0;

  {
    XField2D_Box_UnionJack_Triangle_X1 xfld( 50, 50 );

    for_each_CellGroup<TopoD2>::apply( WeightedFunctionIntegral(volumeFcn, solnExact, globalExt, {0}), xfld );

//    std::cout << "globalExt = " << std::scientific << std::setprecision(16) << globalExt << std::endl;
  }
  //---------------------------------------------------------------------------//

  // Pyrite Settings: orderinc =2, ordermax=2, ii = 4*power+4, powermax = 4;
  const int orderinc = 2;

  // LIFT_EST needs to be off for Masa's estimates
  //#define MASA_1 // R_{h,p}(u_{h,p},\psi_{h,p^\prime})
  //#define MASA_2 // R_{h,p^\prime}(u_{h,p},\psi_{h,p^\prime})

#if defined(MASA_1) || defined(MASA_2)
  std::cout<< "Lifting Operator error not included in estimate" << std::endl;
#else
  std::cout<< "Lifting Operator error included in estimate" << std::endl;
#endif

  int ordermin = 1;
#ifndef SANS_FULLTEST
  // ordermax = 2 is used for the pyrite
  int ordermax = 2;
#else
  // ordermax = 3 is used for the pyrite
  int ordermax = 2;
#endif
  for (int order = ordermin; order <= ordermax; order++)
  {
    indx = 0;

    // loop over grid resolution: 2^power
    int ii, jj;
    int powermin = 1;
#ifdef SANS_FULLTEST
    int powermax = 4;
    cout << "...running full test" << endl;
#else
    int powermax = 1;
#endif
    for (int power = powermin; power <= powermax; power++)
    {
#ifndef SANS_FULLTEST
      //ii = 2 is used for the pyrite
      ii = 2;
#else
      // ii = 4*power + 4 is used for the pyrite
      ii = 4*power+4;
#endif
      jj = ii;

      cout << std::scientific << std::setprecision(7);
      // grid: Hierarchical P1 ( aka X1)

      XField2D_Box_UnionJack_Triangle_X1 xfld( ii, jj );
//      int nInteriorTraceGroups = xfld.nInteriorTraceGroups();
//      int nBoundaryTraceGroups = xfld.nBoundaryTraceGroups();

      // Primal Solution in p
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld( xfld, order-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
      qfld = 0; lgfld = 0; rfld = 0;


      // quadrature rule
      QuadratureOrder quadratureOrder( xfld, - 1 );

      std::vector<Real> tol = {1e-12, 1e-12};
      // create the primal equations
      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder,
                                         ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      // residual

      SystemVectorClass q(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

      PrimalEqSet.fillSystemVector(q);

      rsd = 0;
      PrimalEqSet.residual(q, rsd);

      // solve
      SLA::UMFPACK<SystemMatrixClass> solver(PrimalEqSet);
      SystemVectorClass dq(q.size());
      solver.solve(rsd, dq);

      // update solution
      q -= dq;

      PrimalEqSet.setSolutionField(q);

#if 0
      fstream fout( "tmp/jac.dat", fstream::out );
      cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

      // Adjoint in p
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufld( xfld, order-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
      wfld = 0; mufld = 0; sfld = 0;

      // jacobian transpose

      // adjoint solve
      SystemVectorClass adj(q.size());
      rsd = 0;

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_DGBR2<SurrealClass>( outputFcn, rsd(PrimalEqSet.iPDE) ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint in p
      SLA::UMFPACK<SystemMatrixClass> solverAdj(PrimalEqSet, SLA::TransposeSolve);

      solverAdj.solve(rsd, adj);

      // set adjoint fields
      PrimalEqSet.setAdjointField( adj, wfld, sfld, mufld );

      // ---------------------
      // Adjoint error estimate
      // Adjoint in p+1
      // ---------------------

      // prolongated primal
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfldPp1( xfld, order+orderinc-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
      qfldPp1 = 0; lgfldPp1 = 0; rfldPp1 = 0;

      // prolongate the solution
      qfld.projectTo(qfldPp1);
      rfld.projectTo(rfldPp1);
      lgfld.projectTo(lgfldPp1);

      PrimalEquationSetClass PrimalEqSetPp1(xfld, qfldPp1, rfldPp1, lgfldPp1, pde, disc, quadratureOrder,
                                            ResidualNorm_Default, tol, {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

#ifdef MASA_2 // evaluate the lifting operators in p+p_inc
      // Estimate 2 from Masa's Thesis, Estimate 2 from Hugh's thesis -- R_{h,p^\prime}(u_{h,p}, \psi_{h,p^\prime})
      SystemVectorClass qPp1(PrimalEqSetPp1.vectorStateSize());
      PrimalEqSetPp1.fillSystemVector(qPp1);
      PrimalEqSetPp1.setSolutionField(qPp1);
#endif

      // Adjoint in p+1 solution field
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfldPp1(xfld, order+orderinc, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufldPp1( xfld, order+orderinc-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
      wfldPp1 = 0; mufldPp1 = 0; sfldPp1 = 0;

      // P+1 jacobian transpose

      // Functional integral
      SystemVectorClass rhsPp1(PrimalEqSetPp1.vectorEqSize());
      SystemVectorClass adjPp1(PrimalEqSetPp1.vectorStateSize());
      rhsPp1 = 0;

      IntegrateCellGroups<TopoD2>::integrate(
          JacobianFunctionalCell_DGBR2<SurrealClass>( outputFcn, rhsPp1(PrimalEqSetPp1.iPDE) ),
          xfld, (qfldPp1, rfldPp1), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      // compute the adjoint
      SLA::UMFPACK<SystemMatrixClass> solverAdjPp1(PrimalEqSetPp1, SLA::TransposeSolve);

      solverAdjPp1.solve(rhsPp1, adjPp1);

      // update solution
      PrimalEqSetPp1.setAdjointField(adjPp1, wfldPp1, sfldPp1, mufldPp1);

#if defined(MASA_1) || defined(MASA_2)
      // Estimate 3 from Masa's Thesis, Estimate 1 from Hugh's thesis -- R_{h,p}(u_{h,p}, \psi_{h,p^\prime})
      // Do not account for errors in the lifting operator
      sfldPp1 = 0; // zero out Sigma
#endif

#ifdef MASA_2
      ErrorEstimateClass ErrorEstimate(xfld,
                                       qfldPp1, rfldPp1, lgfldPp1, wfldPp1, sfldPp1, mufldPp1,
                                       pde, disc, quadratureOrder, {0}, {0,1,2},
                                       PyBCList, BCBoundaryGroups);
#else
      ErrorEstimateClass ErrorEstimate(xfld,
                                       qfld, rfld, lgfld, wfldPp1, sfldPp1, mufldPp1,
                                       pde, disc, quadratureOrder, {0}, {0,1,2},
                                       PyBCList, BCBoundaryGroups);
#endif
      const Field<PhysD2,TopoD2,Real>& ifldEstimate = ErrorEstimate.getIField();

      // ---------------------
      // Adjoint error 'exact'
      // ---------------------
      // prolongate the primal to pmax and then evaluate residual
#ifdef SANS_FULLTEST
      int pmax = 7; BOOST_REQUIRE( 2*order+orderinc <= 7); //BasisFunctionArea_Triangle_HierarchicalPMax;;
#else
      int pmax = 2*order + orderinc;
#endif

      // Adjoint in pmax solution field
      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> wfldPmax(xfld, pmax, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> sfldPmax(xfld, pmax, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> mufldPmax( xfld, pmax-1, BasisFunctionCategory_Legendre, LG_BGroup_list );
      wfldPmax = 0; mufldPmax = 0; sfldPmax = 0;

      // Loop over elems and L2 project the exact solution onto the basis
      typedef typename XField<PhysD2, TopoD2                  >::FieldCellGroupType<Triangle> XFieldCellGroupType;
      typedef typename Field< PhysD2, TopoD2, ArrayQ          >::FieldCellGroupType<Triangle> QFieldCellGroupType;
      typedef typename XFieldCellGroupType::ElementType<> ElementXFieldClass;
      typedef typename QFieldCellGroupType::ElementType<> ElementQFieldClass;

      const XFieldCellGroupType&  xfldCell = xfld.getCellGroup<Triangle>(0);
      QFieldCellGroupType&  adjqfldCellPmax = wfldPmax.getCellGroup<Triangle>(0);

      ElementXFieldClass  xfldElem( xfldCell.basis() );
      ElementQFieldClass  adjqfldElemExact( adjqfldCellPmax.basis() );

      // class for computing the projection
      ElementProjectionSolution_L2<TopoD2, Triangle> projector(adjqfldElemExact.basis());

      // Exact adjoint
      for (int elem = 0; elem < xfldCell.nElem(); elem++)
      {
        xfldCell.getElement( xfldElem, elem );

        // L2 Project the exact adjoint onto the pmax basis
        projector.project( xfldElem, adjqfldElemExact, adjExact );
        adjqfldCellPmax.setElement(adjqfldElemExact,elem);

        // Lifting operator adjoint field is zero for little-R formulation without source with gradient
      }

      // // Boundary terms
      // typedef typename Field<PhysD2, TopoD2, ArrayQ>::FieldTraceGroupType<Line> QFieldTraceGroupType;
      // typedef typename XField<PhysD2, TopoD2       >::FieldTraceGroupType<Line> XFieldTraceGroupType;
      //
      // typedef typename QFieldTraceGroupType::ElementType<> ElementQFieldTraceClass;
      // typedef typename XFieldTraceGroupType::ElementType<> ElementXFieldTraceClass;
      //
      // for (int group = 0; group < mufldPmax.nBoundaryTraceGroups(); group++)
      // {
      //   const XFieldTraceGroupType xfldBoundaryTrace = xfld.getBoundaryTraceGroup<Line>(group);
      //   QFieldTraceGroupType adjlgfldBoundaryTracePmax = mufldPmax.getBoundaryTraceGroup<Line>(group);
      //
      //   ElementXFieldTraceClass xfldElem( xfldBoundaryTrace.basis() );
      //   ElementQFieldTraceClass adjlgfldElemExact( adjlgfldBoundaryTracePmax.basis() );
      //
      //   for (int elem = 0; elem< xfldBoundaryTrace.nElem(); elem++)
      //   {
      //     xfldBoundaryTrace.getElement( xfldElem, elem );
      //
      //     // L2 Project for the lagrange multipliers
      //     ElementProjection_LinearScalarLG_L2(xfldElem, adjlgfldElemExact, pde, bc, adjExact );
      //     adjlgfldBoundaryTracePmax.setElement(adjlgfldElemExact, elem);
      //   }
      // }

      // Calculate 'Exact' errors
      ErrorEstimateClass ErrorExact(xfld,
                                    qfld, rfld, lgfld, wfldPmax, sfldPmax, mufldPmax,
                                    pde, disc, quadratureOrder, {0}, {0,1,2},
                                    PyBCList, BCBoundaryGroups);
      const Field<PhysD2,TopoD2,Real>& ifldExact = ErrorExact.getIField();

      // global and local error
      Real globalSln=0;
      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( outputFcn, globalSln ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      Real avglocalErrEstEff=0,globalErrEst=0;
      Real avgLocalErrExt=0, avgLocalErrEst=0,avglocalErrEstError=0;

      KahanSum<Real> sumErrEst=0;
      // Estimates
      ErrorEstimate.getErrorEstimate(globalErrEst);
      ErrorEstimate.getErrorIndicator(avgLocalErrEst);

      // 'Exact'
      ErrorExact.getErrorIndicator(avgLocalErrExt);

      // Error in local Estimate
      Field_DG_Cell<PhysD2, TopoD2, Real> ifldDiff(xfld, 0, BasisFunctionCategory_Legendre);
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldExact.nDOF() );
      BOOST_CHECK_EQUAL( ifldEstimate.nDOF(), ifldDiff.nDOF() );
      for (int i = 0; i < ifldExact.nDOF(); i++ )
      {
        ifldDiff.DOF(i) = ifldExact.DOF(i)-ifldEstimate.DOF(i);
        sumErrEst += fabs( ifldDiff.DOF(i) );
      }
      avglocalErrEstError = (Real)sumErrEst;

      avglocalErrEstEff   = avglocalErrEstError/avgLocalErrExt;
      avgLocalErrExt      /= xfldCell.nElem();
      avgLocalErrEst      /= xfldCell.nElem();
      avglocalErrEstError /= xfldCell.nElem();

      const Real close_tol = 5e-7;
      const Real small_tol = 5e-7;
      if (power==1)
      {
        // residual field
        SystemVectorClass rsdPp1(PrimalEqSetPp1.vectorEqSize());
        rsdPp1 = 0;

        // Recompute lifting perators
        SystemVectorClass qPp1(PrimalEqSetPp1.vectorStateSize());
        PrimalEqSetPp1.fillSystemVector(qPp1);

        // recompute lifting operators and evaluate the residual in p+1
        // equivalent to Masa's Estimate 2 for the global, but gives bad local estimates
        PrimalEqSetPp1.residual(qPp1, rsdPp1);

        // This check is only valid if the lifting operator is recomputed in p+1
        // However, the local estimate is worse if the lifting operator is recomputed
        Real globalErrEst2 = dot(rsdPp1, adjPp1);
        SANS_CHECK_CLOSE( globalErrEst2, globalErrEst, small_tol, close_tol );
      }

      Real globalError = globalSln - globalExt;

      hVec[indx] = 1./ii; //sqrt(nDOFPDE);

      globalErrorVec[indx] = abs(globalError);
      globalEstVec[indx] = abs(globalErrEst);
      globalEstErrVec[indx] = abs( globalError - globalErrEst );
      globalEffVec[indx] = globalEstErrVec[indx]/globalEstVec[indx];

      localErrorVec[indx] = abs(avgLocalErrExt);
      localEstVec[indx] = abs(avgLocalErrEst);
      localEstErrVec[indx] = avglocalErrEstError;
      localEffVec[indx] = avglocalErrEstEff;

      //std::cout << "Local Error Exact: " << std::scientific << std::setprecision(16) << abs(avgLocalErrExt) << std::endl;

      if (indx > 0)
      {
        globalErrorRate[indx]  = (log(globalErrorVec[indx])  - log(globalErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstRate[indx]    = (log(globalEstVec[indx])    - log(globalEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        globalEstErrRate[indx] = (log(globalEstErrVec[indx]) - log(globalEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        globalEffRate[indx]    = (log(globalEffVec[indx])    - log(globalEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));

        localErrorRate[indx]  = (log(localErrorVec[indx])  - log(localErrorVec[indx-1])) /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstRate[indx]    = (log(localEstVec[indx])    - log(localEstVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
        localEstErrRate[indx] = (log(localEstErrVec[indx]) - log(localEstErrVec[indx-1]))/(log(hVec[indx]) - log(hVec[indx-1]));
        localEffRate[indx]    = (log(localEffVec[indx])    - log(localEffVec[indx-1]))   /(log(hVec[indx]) - log(hVec[indx-1]));
      }
      else
      {
        globalErrorRate[indx]  = 0;
        globalEstRate[indx]    = 0;
        globalEstErrRate[indx] = 0;
        globalEffRate[indx]    = 0;

        localErrorRate[indx]  = 0;
        localEstRate[indx]    = 0;
        localEstErrRate[indx] = 0;
        localEffRate[indx]    = 0;
      }

      if ( power == powermin )
      {
        cout << "P = " << order << ", P^ = " << order + orderinc << ", P_ = " <<  orderinc << endl;
        cout << "  \t";
        cout << " global     (2P)      global     (2P)      global     (2P^)      global     (2P_)   ";
        cout << " local      (2P+2)    local      (2P+2)    local      (P+P^+2)   local      (P_) " << endl;
        cout << "ii\t";
        cout << " exact                estimate             err in estimate       effectivity        ";
        cout << " exact                estimate             err in estimate       effectivity" << endl;

        resultFile << "ZONE T=\"DG P=" << order << "\"" << endl;
      }

      resultFile << std::setprecision(16) << std::scientific;
      resultFile << hVec[indx];
      resultFile << ", " << globalErrorVec[indx];
      resultFile << ", " << globalEstVec[indx];
      resultFile << ", " << globalEstErrVec[indx];
      resultFile << ", " << globalEffVec[indx];
      resultFile << ", " << globalErrorRate[indx];
      resultFile << ", " << globalEstRate[indx];
      resultFile << ", " << globalEstErrRate[indx];
      resultFile << ", " << globalEffRate[indx];

      resultFile << ", " << localErrorVec[indx];
      resultFile << ", " << localEstVec[indx];
      resultFile << ", " << localEstErrVec[indx];
      resultFile << ", " << localEffVec[indx];
      resultFile << ", " << localErrorRate[indx];
      resultFile << ", " << localEstRate[indx];
      resultFile << ", " << localEstErrRate[indx];
      resultFile << ", " << localEffRate[indx];
      resultFile << endl;

      // For Latex
      datFile << std::setprecision(16) << std::scientific;
      if (indx==0)
      {
        datFile << order;
      }
      else
      {
        datFile << " ";
      }
      datFile << "," << hVec[indx];
      datFile << "," << globalErrorVec[indx];
      datFile << "," << globalEstVec[indx];
      datFile << "," << globalEstErrVec[indx];
      datFile << "," << globalEffVec[indx];
      datFile << "," << globalErrorRate[indx];
      datFile << "," << globalEstRate[indx];
      datFile << "," << globalEstErrRate[indx];
      datFile << "," << globalEffRate[indx];
      datFile << "," << localErrorVec[indx];
      datFile << "," << localEstVec[indx];
      datFile << "," << localEstErrVec[indx];
      datFile << "," << localEffVec[indx];
      datFile << "," << localErrorRate[indx];
      datFile << "," << localEstRate[indx];
      datFile << "," << localEstErrRate[indx];
      datFile << "," << localEffRate[indx];
      datFile << endl;

      cout << ii << "\t";
      cout << std::scientific;
      cout << std::scientific << std::setprecision(5) << globalErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << globalEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << globalEffRate[indx] << ")  ";

      cout << std::scientific << std::setprecision(5) << localErrorVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localErrorRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEstErrVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEstErrRate[indx] << ")  ";
      cout << std::scientific << std::setprecision(5) << localEffVec[indx];
      cout << std::fixed      << std::setprecision(3) << " (" << localEffRate[indx] << ")  ";
      cout << endl;

      pyriteFile << globalErrorVec[indx];
      pyriteFile.close(5e-1) << globalErrorRate[indx];
      pyriteFile << globalEstVec[indx];
      pyriteFile.close(5e-1) << globalEstRate[indx];
      pyriteFile << globalEstErrVec[indx];
      pyriteFile.close(5e-1) << globalEstErrRate[indx];
      pyriteFile << globalEffVec[indx];
      pyriteFile.close(5.e-1) << globalEffRate[indx];

      pyriteFile << localErrorVec[indx];
      pyriteFile.close(5e-1) << localErrorRate[indx];
      pyriteFile << localEstVec[indx];
      pyriteFile.close(5e-1) << localEstRate[indx];
      pyriteFile << localEstErrVec[indx];
      pyriteFile.close(5e-1) << localEstErrRate[indx];
      pyriteFile << localEffVec[indx];
      pyriteFile.close(5e-1) << localEffRate[indx];
      pyriteFile << std::endl;

      indx++;

#if 0
      string filename = "tmp/locErrCG_";
      filename += "P";
      filename += stringify(order);
      filename += "_";
      filename += stringify(ii);
      filename += "x";
      filename += stringify(jj);
      filename += ".plt";
      cout << "calling output_Tecplot: filename = " << filename << endl;
      output_Tecplot( xfldArea, localErrEstfldArea, filename );
#endif
    }

    cout << endl;
  }
  std::cout << "Total Time: " << totaltime.elapsed() << std::endl;
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
