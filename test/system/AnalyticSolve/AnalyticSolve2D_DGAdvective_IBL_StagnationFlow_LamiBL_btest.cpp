// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing of 2-D DGAdvective for IBL on a line elements with stagnation

// Analytic solution: (Falker-Skan)
//   Linear edge velocity distribution results in
//   constant (displacement, momentum defect and kinetic energy defect) thicknesses
//   for laminar boundary layers.
//   This solution holds regardless of stagnation. This test introduces stagnation to examine
//   the capability of the code to handle stagnating flows.

//#define IBL2D_DUMP
//#define SANS_VERBOSE
//#define DISPLAY_FOR_DEBUG

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/IBL/PDEIBL2D.h"
#include "pde/IBL/BCIBL2D.h"
#include "pde/IBL/SetSolnDofCell_IBL2D.h"
#include "pde/IBL/SetVelDofCell_IBL2D.h"
#include "pde/IBL/SetIBLoutputCellGroup.h"
#include "pde/IBL/GetSolnDofCell_IBL2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_HubTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_WakeMatch_Galerkin_IBL.h"

using namespace std;
using namespace SANS;

namespace SANS
{
// ---------- Define type/class names ---------- //
typedef PhysD2 PhysDim;  // physical dimension
typedef TopoD1 TopoDim;  // grid element topological dimension

typedef VarTypeDANCt VarType;

// PDE class
typedef PDEIBL<PhysDim,VarType> PDEClass;
typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;

typedef typename PDEClass::ThicknessesCoefficientsType::ClassParamsType ThicknessesCoefficientsParamsType;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef PDEClass::template ArrayParam<Real> ArrayParam;
typedef NDPDEClass::VectorX VectorX;

// BC
typedef BCIBL2DVector<VarType> BCVector;

// parameter field
typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayParam>,
                                       XField<PhysDim, TopoDim> >::type FieldParamType;

// Primal equation set
typedef AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvertSpace, BCVector,
                                         AlgEqSetTraits_Sparse, DGAdv_manifold, FieldParamType> PrimalEquationSetClass;
typedef PrimalEquationSetClass::BCParams BCParams;
typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

} // namespace SANS

//############################################################################//
BOOST_AUTO_TEST_SUITE( AnalyticSolve2D_DGAdvective_IBL_StagnationFlow_LamiBL_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AnalyticSolve2D_DGAdvective_IBL_StagnationFlow_LamiBL_p1orHigher_test )
{
  const int nelem = 9;

  const std::vector<int> order_soln_array = {1}; // note that no even order since DOF can potential lies on stagnation point x=0
  // TODO: higher orders (p>3) are experiencing difficulty in solution convergence
  // probably because wrong use of hierarchical basis for high orders

  // ---------- Set problem parameters ---------- //

  const int order_param_grid = 1;  // order: parameter field and grid

  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelIBL gasModel(gasModelDict);

  const Real mue = 1.827e-5;
  typename PDEClass::ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  const typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  PyDict secVarDict;
  ThicknessesCoefficientsParamsType::checkInputs(secVarDict);

  // PDE
  NDPDEClass pde(gasModel, viscosityModel, transitionModel, secVarDict);

  // BC
  PyDict BCOutArgs;
  BCOutArgs[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCOut"] = BCOutArgs;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCOut"] = {0,1};

  // No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // ---------- Set solver parameters ---------- //

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 50;
#ifdef SANS_VERBOSE
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
#else
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // grid field
  std::vector<VectorX> grid_coordinates(nelem+1);
  for (size_t j = 0; j <= nelem; ++j)
  {
    Real x = 2*static_cast<Real>(j)/static_cast<Real>(nelem) - 1.0;
    Real y = 0;
#if defined(DISPLAY_FOR_DEBUG)
    cout << "x = " << x << endl;
#endif
    grid_coordinates.at(j) = {x,y};
  }

  XField2D_Line_X1_1Group xfld(grid_coordinates);

  // parameter: velocity field
  std::vector<Real> dataVel(nelem+1);
  for (size_t j = 0; j <= nelem; ++j)
  {
    dataVel.at(j) = grid_coordinates.at(j)[0]; // edge velocity ue = x;

#if defined(DISPLAY_FOR_DEBUG)
    cout << "ue = " << dataVel.at(j) << endl;
#endif
  }

  Field_DG_Cell<PhysDim, TopoDim, VectorX> velfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velxXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velzXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  for_each_CellGroup<TopoD1>::apply( SetVelocityDofCell_IBL2D(dataVel, {0}), (velfld, velxXfld, velzXfld, xfld) );

#if defined(DISPLAY_FOR_DEBUG)
  for (int j = 0; j < velfld.nDOF(); ++j)
    cout << "velfld = " << velfld.DOF(j) << endl;

  for (int j = 0; j < velxXfld.nDOF(); ++j)
    cout << "velxXfld = " << velxXfld.DOF(j) << endl;

  for (int j = 0; j < velzXfld.nDOF(); ++j)
    cout << "velzXfld = " << velzXfld.DOF(j) << endl;
#endif

  BOOST_REQUIRE_EQUAL( xfld.nElem(), velfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velxXfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velzXfld.nElem() );

  // parameter field
  Field_DG_Cell<PhysDim, TopoD1, ArrayParam> paramfld(xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( xfld.nElem(), paramfld.nElem() );

  // TODO: should not work directly with DOFs
  const auto& paramInterpret = pde.getParamInterpreter();
  for (int i = 0; i < paramfld.nDOF(); ++i)
  {
    paramfld.DOF(i) = paramInterpret.setDOFFrom(
        velfld.DOF(i),
        DLA::VectorS<PhysDim::D, VectorX>(
            {VectorX({velxXfld.DOF(i)[0], velzXfld.DOF(i)[0]}),
             VectorX({velxXfld.DOF(i)[1], velzXfld.DOF(i)[1]})} ),
        p0, T0);

#if defined(DISPLAY_FOR_DEBUG)
    cout << paramfld.DOF(i) << endl;
#endif
  }

  // loop over different solution order
  for (size_t jorder_soln = 0; jorder_soln < order_soln_array.size(); ++jorder_soln)
  {
    const int order_soln = order_soln_array.at(jorder_soln);
#ifdef SANS_VERBOSE
    std::cout << std::endl;
    std::cout << "solution polynomial order = " << order_soln << std::endl << std::endl;
#endif

    // DG solution field
    BasisFunctionCategory basisFcnCat_q = BasisFunctionCategory_Hierarchical;
    if (order_soln == 0) { basisFcnCat_q = BasisFunctionCategory_Legendre; }
    Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld( xfld, order_soln, basisFcnCat_q );

    // TODO: should not work directly with DOFs
    // arbitrary initial solution guess, constant through the field
    const Real deltaTurb_dummy_ = pde.getVarInterpreter().deltaTurb_dummy_;
    const Real ATurb_dummy_ = pde.getVarInterpreter().ATurb_dummy_;
    const Real ctauLami_dummy_ = pde.getVarInterpreter().ctauLami_dummy_;
    for (int j = 0; j < qfld.nDOF(); ++j)
    {
      qfld.DOF(j) = pde.setDOFFrom( VarData2DDANCt(0.012763710580117,3.949679046979493,
                                                   deltaTurb_dummy_, ATurb_dummy_,
                                                   1.6, ctauLami_dummy_) );

#if defined(DISPLAY_FOR_DEBUG)
      cout << "qfld = " << qfld.DOF(j) << endl;
#endif
    }

    BOOST_REQUIRE_EQUAL( xfld.nElem(), qfld.nElem() );

    // Lagrange multiplier: Hierarchical, C0 (also at corners)
    Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ>
    lgfld( xfld, order_soln, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
    lgfld = 0.0;

    ////////////
    //SOLVE
    ////////////

    FieldParamType tuplefld = (paramfld, xfld);

    QuadratureOrder quadratureOrder( xfld, -1 );
    const Real tol_eqn = 2.e-12;
    const std::vector<Real> tol(2,tol_eqn);
    const std::vector<int> cellGroups = {0};
    const std::vector<int> interiorTraceGroups = {0};
    PrimalEquationSetClass primalEqSet(tuplefld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                       cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups );

    NewtonSolver<SystemMatrixClass> solver(primalEqSet, NewtonSolverDict);

    SystemVectorClass sln(primalEqSet.vectorStateSize());
    SystemVectorClass rsd(primalEqSet.vectorEqSize());

    primalEqSet.fillSystemVector(sln); // initialize global solution vector

  #ifdef IBL2D_DUMP
    // ----- check initial residuals and Jacobian
    // residual
    SystemVectorClass rsdinit(primalEqSet.vectorEqSize());
    rsdinit = 0;
    primalEqSet.residual(sln, rsdinit);

    // jacobian nonzero pattern
    SystemNonZeroPattern nz(primalEqSet.matrixSize());
    primalEqSet.jacobian(sln, nz);

    // jacobian
    SystemMatrixClass jac(nz);
    primalEqSet.jacobian(sln, jac);

    string filetag = "StagnationFlow";

    fstream fout( "tmp/jac_init_" + filetag + ".mtx", fstream::out );
    cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
  #endif

    SolveStatus status = solver.solve(sln, sln);
    BOOST_CHECK( status.converged );

    primalEqSet.setSolutionField(sln);

#ifdef IBL2D_DUMP
    { // dump output for Tecplot
      const int order_output = std::max(1,order_soln);
      const string BLprofileName = transitionModelDict.get(TransitionModelParams::params.profileCatDefault);
      string outputfilename = "tmp/outputDGAdv_IBL2D_" + filetag + "_" + BLprofileName
          + "_p" + stringify(order_soln) + "_nelem" + stringify(nelem) + ".plt";
      typedef typename SetIBLoutputCellGroup_impl<VarType>::template ArrayOutput<Real> ArrayOutput;

      BasisFunctionCategory basisFcnCat = BasisFunctionCategory_Hierarchical;
      if (order_output > 2) { basisFcnCat = BasisFunctionCategory_Legendre; }
      Field_DG_Cell<PhysDim, TopoDim, ArrayOutput> outputfld(xfld, order_output, basisFcnCat);

      // set output field
      const Real qinf = 1.0; // [m/s] freestream speed
      for_each_CellGroup<TopoD1>::apply(SetIBLoutputCellGroup<VarType>(pde, qinf, qfld, paramfld, {0}, -1), (xfld, outputfld));

      auto output_names = SetIBLoutputCellGroup_impl<VarType>::ElementProjectionType::outputNames();
      output_Tecplot(outputfld, outputfilename, output_names);
    }
#endif

    { // check against reference solution
      const Real small_tol = 1e-13, close_tol = 3e-4;
      typedef typename GetSolnDofCell_IBL2D_impl<VarType,TopoDim>::RefCoordType RefCoordType;
      const std::vector<RefCoordType> sRefArr = {0.0, 0.6, 1.0};
      std::vector<std::vector<ArrayQ> > qdata;
      for_each_CellGroup<TopoD1>::apply(GetSolnDofCell_IBL2D<VarType,TopoDim>(sRefArr,cellGroups,qdata), (xfld, qfld));

      // reference solution obtained from MATLAB by numerically solving (with fsolve) analytical IBL residuals
      const Real deltaLamiTrue = 1.066604750537544e-02;
      const Real ALamiTrue = 3.593003890090205e+00;

      const auto& varInterpret =  pde.getVarInterpreter();

#ifdef SANS_VERBOSE
      std::cout << "...checking against reference solution..." << std::endl;
#endif
      for (int group=0; group<(int)qdata.size(); ++group)
      {
        for (int i=0; i<(int)qdata.at(0).size(); ++i)
        {
#ifdef SANS_VERBOSE
          std::cout << "\nPoint " << i << std::endl;
          std::cout << std::setprecision(16);
          std::cout << qdata.at(group).at(i) << std::endl;
#endif
          SANS_CHECK_CLOSE(deltaLamiTrue, qdata.at(group).at(i)[varInterpret.ideltaLami], small_tol, close_tol);
          SANS_CHECK_CLOSE(ALamiTrue,     qdata.at(group).at(i)[varInterpret.iALami], small_tol, close_tol);
        }
      }
    }
  } // end loop over order_soln
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
