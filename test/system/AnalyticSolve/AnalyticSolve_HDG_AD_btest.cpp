// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Solve1D_HDG_AD_btest
// testing of 1-D HDG with Advection-Diffusion

// #define SANS_FULLTEST
// #define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"
#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/OutputNDConvertSpace1D.h"

#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/XField1D/XField1D.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AnalyticSolve_HDG_AD_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Solve_HDG_AD_Uniform )
{

  typedef PDEAdvectionDiffusion<PhysD1,
                                AdvectiveFlux1D_Uniform,
                                ViscousFlux1D_Uniform,
                                Source1D_UniformGrad > PDEClass;
  typedef PDENDConvertSpace<PhysD1, PDEClass> NDPDEClass;
  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysD1::D, ArrayQ> VectorArrayQ;

  typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvertSpace, BCVector,
                                   AlgEqSetTraits_Sparse, XField<PhysD1, TopoD1>> AlgebraicEquationSet_HDGClass;
  typedef AlgebraicEquationSet_HDGClass::BCParams BCParams;

  // PDE
  AdvectiveFlux1D_Uniform adv( 1.1 );

  Real nu = 0.9;
  ViscousFlux1D_Uniform visc( nu );

  ScalarFunction1D_Sine MMS_soln;

  Source1D_UniformGrad source(0., 0.);

  NDPDEClass pde( adv, visc, source );

  // BC

  PyDict BCLeft;
  BCLeft[BCParams::params.BC.BCType] = BCParams::params.BC.LinearRobin_sansLG;
  BCLeft[BCAdvectionDiffusionParams<PhysD1,BCTypeLinearRobin_sansLG>::params.A] = 1;
  BCLeft[BCAdvectionDiffusionParams<PhysD1,BCTypeLinearRobin_sansLG>::params.B] = 0;
  BCLeft[BCAdvectionDiffusionParams<PhysD1,BCTypeLinearRobin_sansLG>::params.bcdata] = 1;

  PyDict BCRight;
  BCRight[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["TheBCLeft"] = BCLeft;
  PyBCList["TheBCRight"] = BCRight;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["TheBCLeft"]  = {0};
  BCBoundaryGroups["TheBCRight"] = {1};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef AlgebraicEquationSet_HDGClass::SystemMatrix SystemMatrixClass;
  typedef AlgebraicEquationSet_HDGClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  // grid:

  XField1D xfld( 4 );

  // wiggle the grid
  BOOST_REQUIRE(xfld.nDOF() >= 5);
  xfld.DOF(0) += -0.1;
  xfld.DOF(1) +=  0.2;
  xfld.DOF(3) += -0.05;
  xfld.DOF(4) +=  0.125;

  int ordermin = 1;
  int ordermax = BasisFunctionLine_LegendrePMax;
  for (int order = ordermin; order <= ordermax; order++)
  {

    // HDG solution field
    // cell solution
    Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
    // auxiliary variable
    Field_DG_Cell<PhysD1, TopoD1, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
    // interface solution
    Field_DG_Trace<PhysD1, TopoD1, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);

    // Lagrange multiplier
    std::vector<int> LG_BGroup_list = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);
    Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> lgfld(xfld, order, BasisFunctionCategory_Legendre, LG_BGroup_list );

    QuadratureOrder quadratureOrder( xfld, -1 );
    std::vector<Real> tol = {1e-10, 1e-10, 1e-10};
    AlgebraicEquationSet_HDGClass AlgEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                           {0}, {0}, PyBCList, BCBoundaryGroups );

    // not the solution
    qfld =  -1;
    afld =   2;
    qIfld = -3;
    lgfld =  4;

    // residual vectors

    SystemVectorClass q(AlgEqSet.vectorStateSize());
    SystemVectorClass rsd(AlgEqSet.vectorEqSize());

    AlgEqSet.fillSystemVector(q);

    rsd = 0;
    AlgEqSet.residual(q, rsd);

    // solve
    SLA::UMFPACK<SystemMatrixClass> solver(AlgEqSet);

    SystemVectorClass dq(q.size());

    solver.solve(rsd, dq);

    // updated solution
    q -= dq;

    // check that the residual is zero
    rsd = 0;
    AlgEqSet.residual(q, rsd);
    std::vector<std::vector<Real>> rsdNorm = AlgEqSet.residualNorm(rsd);

    bool converged = AlgEqSet.convergedResidual(rsdNorm);

    if (!converged)
    {
      std::vector<std::vector<Real>> rsdNorm = AlgEqSet.residualNorm(rsd);
      AlgEqSet.printDecreaseResidualFailure(rsdNorm, std::cout);
    }

    BOOST_REQUIRE(converged);

#if 0
    // Tecplot dump
    string filename = "tmp/slnHGD_P";
    filename += to_string(order);
    filename += "_";
    filename += to_string(ii);
    filename += "x";
    filename += to_string(jj);
    filename += ".plt";
    cout << "calling output_Tecplot: filename = " << filename << endl;
    output_Tecplot( xfldArea, afldArea, &solnExact, filename, order-1 );
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
