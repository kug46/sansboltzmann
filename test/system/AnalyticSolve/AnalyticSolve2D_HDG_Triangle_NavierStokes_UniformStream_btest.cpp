// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// HDG_Euler_UniformStream_btest
// testing of 2-D HDG for Euler on triangle rectangular Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"

#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( HDG_Euler_UniformStream_test_suite )


//----------------------------------------------------------------------------//
// check that residual = 0 for uniform flow initialized to exact initial conditions

BOOST_AUTO_TEST_CASE( HDG_Euler_Duct )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_HDG< NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE
  const Real gamma = 1.4;
  const Real R = 1.0;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const Real muRef = 0.0;
  const Real Prandtl = 0.72;

  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum);

  const Real tRef = 1.0;
  const Real pRef = 1.0;
  const Real rhoRef = 1.0;
  const Real Mach = 0.3;
  const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  const Real TtRef = tRef*Gamma;
  const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  const Real aSpec = 0.0;
  const Real uRef = Mach*sqrt(gamma*R*tRef)*cos(aSpec);
  const Real vRef = Mach*sqrt(gamma*R*tRef)*sin(aSpec);

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2};
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {3};


  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  int ii, jj;
  int order = 1;
  int power = 1;

  jj = pow( 2, power );
  ii = 3*jj; //small grid!
  Real tau = 0.0; //tau=0 is straight channel

  // grid: 2D Cubic Source Bump
  XField2D_CubicSourceBump_Xq xfld( ii, jj, tau );

  std::vector<int> cellGroups = {0};
  std::vector<int> interorTraceGroups = {0,1,2};

  // HDG solution field
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  //exact solution is (1, 0.1, 0, 1);
  ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
  lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

  // std::cout << "lgfld.nBoundaryTraceGroups()" << lgfld.nBoundaryTraceGroups() << std::endl;

  // Set the initial condition
  qfld = q0;
  afld = 0;
  qIfld = q0;
  lgfld = 0;

  ////////////
  //Check RSD = 0
  ////////////

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                     cellGroups, interorTraceGroups, PyBCList, BCBoundaryGroups );

  SystemVectorClass ini(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

  PrimalEqSet.fillSystemVector(ini);

  // is rsd zero?
  rsd = 0;
  PrimalEqSet.residual(ini, rsd);
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

  bool converged = PrimalEqSet.convergedResidual(rsdNorm);

  if (!converged)
  {
    std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);
    PrimalEqSet.printDecreaseResidualFailure(rsdNorm, std::cout);
  }

  BOOST_REQUIRE(converged);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( HDG_Euler_Diagonal )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_HDG< NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE
  const Real gamma = 1.4;
  const Real R = 1.0;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const Real muRef = 0.0;
  const Real Prandtl = 0.72;

  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum);

  const Real tRef = 1.0;
  const Real pRef = 1.0;
  const Real rhoRef = 1.0;
  const Real Mach = 0.3;
  const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  const Real TtRef = tRef*Gamma;
  const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  const Real aSpec = 0.1;
  const Real uRef = Mach*sqrt(gamma*R*tRef)*cos(aSpec);
  const Real vRef = Mach*sqrt(gamma*R*tRef)*sin(aSpec);

  PyDict BCIn1;
  BCIn1[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCIn1[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtRef;
  BCIn1[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtRef;
  BCIn1[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;

  PyDict BCIn2;
  BCIn2[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCIn2[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtRef;
  BCIn2[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtRef;
  BCIn2[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = 1.0;

  PyDict PyBCList;
  PyBCList["BCIn1"] = BCIn1;
  PyBCList["BCIn2"] = BCIn2;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCOut"] = {1,2};
  BCBoundaryGroups["BCIn1"] = {3};
  BCBoundaryGroups["BCIn2"] = {0};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  int ii, jj;
  int order = 1;
  int power = 1;
  jj = pow( 2, power );
  ii = 3*jj; //small grid!
  Real tau = 0.0; //tau=0 is straight channel

  // grid: 2D Cubic Source Bump
  XField2D_CubicSourceBump_Xq xfld( ii, jj, tau );

  std::vector<int> cellGroups = {0};
  std::vector<int> interorTraceGroups = {0,1,2};

  // HDG solution field
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient );

  //exact solution is (1, 0.095393920141695, 0.03, 1); velocity magnitude is 0.1
  ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
  lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

  // Set the initial condition
  qfld = q0;
  afld = 0;
  qIfld = 0;
  lgfld = 0;

  const int nDOFINT = qIfld.nDOF();

  //HACK INITIAL CONDITION FOR qIfld while Marshall figures it out
  for (int k = 0; k < nDOFINT; k++)
  {
    if (!(k % 2))
    {
      qIfld.DOF(k) = q0;
    }
  }

  ////////////
  //Check RSD = 0
  ////////////

  QuadratureOrder quadratureOrder( xfld, -1 );
  std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                     cellGroups, interorTraceGroups, PyBCList, BCBoundaryGroups );

  SystemVectorClass ini(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

  PrimalEqSet.fillSystemVector(ini);

  // is rsd zero?
  rsd = 0;
  PrimalEqSet.residual(ini, rsd);
  std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

  bool converged = PrimalEqSet.convergedResidual(rsdNorm);

  if (!converged)
  {
    std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);
    PrimalEqSet.printDecreaseResidualFailure(rsdNorm, std::cout);
  }

  BOOST_REQUIRE(converged);
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
