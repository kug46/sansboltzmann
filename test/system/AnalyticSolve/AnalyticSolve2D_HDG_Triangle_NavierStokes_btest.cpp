// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
//  Solve2D_HDG_Triangle_NavierStokes_Poiseuille_btest
// testing of 2-D HDG for NS on triangle Cubic Source Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "pde/BCParameters.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/OutputEuler2D.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/BCParameters.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/IntegrandCell_HDG_Output.h"

#include "Discretization/HDG/ProjectSoln_HDG.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectSolnTrace_Discontinuous.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( AnalyticSolve2D_HDG_Triangle_NavierStokes_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AnalyticSolve2D_HDG_Triangle)
{

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_HDG< NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef SolutionFunction_NavierStokes2D_QuadraticMMS<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolnType;
  typedef SolnNDConvertSpace<PhysD2,SolnType> NDSolnType;
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;

  typedef OutputEuler2D_V2<PDEClass> OutputClassV2;
  typedef OutputNDConvertSpace<PhysD2, OutputClassV2> NDOutputClassV2;
  typedef IntegrandCell_HDG_Output<NDOutputClassV2> IntegrandOutputClassV2;

  typedef OutputEuler2D_MomentumErrorSquare<PDEClass> OutputClassMomentumErrorSquare;
  typedef OutputNDConvertSpace<PhysD2, OutputClassMomentumErrorSquare> NDOutputClassMomentumErrorSquare;
  typedef IntegrandCell_HDG_Output<NDOutputClassMomentumErrorSquare> IntegrandOutputClassMomentumErrorSquare;

  typedef BCEuler2D<BCTypeFunction_mitState, PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QType, GasModel>>> BCSolnClass;

  typedef AlgebraicEquationSet_HDG< NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  std::vector<Real> tol = {1e-10, 1e-10, 1e-10};

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

// INFLOW CONDITIONS
  Real rho0 = 1.0;
  Real u0 = 0.3;
  Real v0 = 0.1;
  Real p0 = 1.0;
//  Real T0 = p0/(rho0*R);

//  Real E0 = Cv*T0 + 0.5*(u0*u0 + v0*v0);

  const Real Re = 1.0;
  const Real muRef = sqrt(u0*u0 + v0*v0) / Re; // kg/(m s) // FOR RE = 10
  const Real Prandtl = 0.72;

  // PDE
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = 1.4;
  gasModelDict[GasModelParams::params.R] = 0.4;
  GasModel gas(gasModelDict);

  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);


//  Real M02 = M0*M0;

  NDSolnType soln(gas);
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(soln) );

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw, eVanLeer, forcingptr );
  // HDG solution field at inlet
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rho0, u0, v0, p0) );

  // Momentum Error Output
  NDOutputClassMomentumErrorSquare fcnOutputMomErrorSquare(pde, soln);
  IntegrandOutputClassMomentumErrorSquare fcnOutMomErrorSquare( fcnOutputMomErrorSquare, {0} );

  // Velocity Squared Output
  NDOutputClassV2 fcnOutputV2(pde);
  IntegrandOutputClassV2 fcnOutV2( fcnOutputV2, {0} );

#if 0
  DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient);
#else
  Real refl = 0.1;
  // HDG discretization
  DiscretizationHDG<NDPDEClass> disc( pde, Global, Gradient, refl);
#endif

  PyDict Function;
  Function[BCSolnClass::ParamsType::params.Function.Name] = BCSolnClass::ParamsType::params.Function.QuadraticMMS;
  Function[SolnType::ParamsType::params.gasModel] = gasModelDict;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCIn[BCSolnClass::ParamsType::params.Function] = Function;
  BCIn[BCSolnClass::ParamsType::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
//  BCBoundaryGroups["BCOut"] = {1,2};
  BCBoundaryGroups["BCIn"] = {0, 1, 2, 3};

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1.0e-48;
#ifdef SANS_VERBOSE
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#else
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 3000;
#ifdef SANS_VERBOSE
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
#else
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

//   norm data
//  Real normVec[10];   // L2 error
//  int indx = 0;

//  fstream fout( "tmp/hdgL1em3_Ec0p3Re10.txt", fstream::out );

  int ii, jj;

  int ordermin = 2;
#ifdef SANS_FULLTEST
    int ordermax = 5;
#else
  int ordermax = 3;
#endif

  for (int order = ordermin; order <= ordermax; order++)
  {
    jj = 2;
    ii = jj; //small grid!

    // grid
    XField2D_Box_UnionJack_Triangle_X1 xfld(ii, jj);

    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
    Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> afld(xfld, order, BasisFunctionCategory_Legendre);
    Field_DG_Trace<PhysD2, TopoD2, ArrayQ> qIfld(xfld, order, BasisFunctionCategory_Legendre);
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
    lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

    qfld = q0;
    afld = 0.0;
    qIfld = q0;
    lgfld = 0;

    //L2 projection of exact solution
    ProjectSoln_HDG(qfld, afld, qIfld, soln);

    // Check machine precision error for L2 projection
    Real VelSquare = 0.0;
    Real MomError = 0.0;

    QuadratureOrder quadratureOrder( xfld, -1 );

    std::vector<int> cellGroups = {0};
    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < xfld.nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    PrimalEquationSetClass PrimalEqSet(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                                       cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups );
    SystemVectorClass ini(PrimalEqSet.vectorStateSize());
    SystemVectorClass sln(PrimalEqSet.vectorStateSize());
    SystemVectorClass slnchk(PrimalEqSet.vectorStateSize());
    SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

    //////////////////////
    // Set the uniform initial condition and resolve
    //////////////////////
    qfld = q0;
    afld = 0.0;
    qIfld = q0;
    lgfld = 0;

    NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

    ini = 0;
    PrimalEqSet.fillSystemVector(ini);
    sln = ini;

    bool solved = Solver.solve(ini,sln).converged;
    BOOST_REQUIRE(solved);

    rsd = 0;
    PrimalEqSet.residual(sln, rsd);
    std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);

    bool converged = PrimalEqSet.convergedResidual(rsdNorm);

    if (!converged)
    {
      std::vector<std::vector<Real>> rsdNorm = PrimalEqSet.residualNorm(rsd);
      PrimalEqSet.printDecreaseResidualFailure(rsdNorm, std::cout);
    }

    BOOST_REQUIRE(converged);

    // Monitor Velocity Error
    VelSquare = 0.0;
    MomError = 0.0;

    Real VelSquareTrue = 0.102891; //ANALYTIC INTEGRAL OF V2
    IntegrateCellGroups<TopoD2>::integrate(
        FunctionalCell_HDG( fcnOutMomErrorSquare, MomError ), xfld, (qfld, afld),
        quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
    MomError = sqrt(MomError);

    IntegrateCellGroups<TopoD2>::integrate(
        FunctionalCell_HDG( fcnOutV2, VelSquare ), xfld, (qfld, afld),
        quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

    BOOST_CHECK_SMALL(MomError, tol[0]);
    BOOST_CHECK_SMALL( (VelSquare- VelSquareTrue), tol[0]);


//#define SANS_VERBOSE
#ifdef SANS_VERBOSE
    //      fout << "P = " << order << " ii = " << ii <<
    //          ": L2 Mom error = " << std::setprecision(15) << sqrt(MomError) <<
    //          ": V2 Output = " << std::setprecision(15) << VelSquare;
    //      //      if (indx > 1)
    //      //        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
    //      fout << endl;
    cout << "P = " << order << " ii = " << ii <<
        ": Mom error = " << std::setprecision(15) << sqrt(MomError) <<
        ": V2 Output Error = " << std::setprecision(15) << (VelSquare - VelSquareTrue);
    //      if (indx > 1)
    //        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
    cout << endl;

#endif

#if 0
    // Tecplot dump grid
    string filename = "tmp/HDG_AnalyticMMS_P";
    filename += to_string(order);
    filename += "re10.plt";
    output_Tecplot( qfld, filename );
    std::cout << "Wrote Tecplot File\n";
#endif

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
