// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AnalyticSolve2D_DGBR2_Triangle_NavierStokes_btest
// testing of 2-D DGBR2 for NS on triangle Mesh - uniform stream test

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftingOperator.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( AnalyticSolve2D_DGBR2_Triangle_NavierStokes_UniformStream_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AnalyticSolve_DGBR2_Triangle_NavierStokes_UniformStream )
{

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;
  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  std::vector<Real> tol = {1e-10, 1e-10};

  // PDE
  const Real gamma = 1.4;
  const Real R = 1.0;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const Real muRef = 0.0;
  const Real Prandtl = 0.72;

  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Momentum);
//  int nSol = NDPDEClass::N;

  const Real tRef = 1.0;
  const Real pRef = 1.0;
  const Real rhoRef = 1.0;
  const Real Mach = 0.3;

  const Real aSpec = 0.1;
  const Real uRef = Mach*sqrt(gamma*R*tRef)*cos(aSpec);
  const Real vRef = Mach*sqrt(gamma*R*tRef)*sin(aSpec);

  // HDG solution field at inlet
  ArrayQ qRef;
  pde.setDOFFrom( qRef, DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

#if 0
  const Real Gamma = 1 + 0.5*(gamma - 1)*Mach*Mach;
  const Real TtRef = tRef*Gamma;
  const Real PtRef = pRef*pow( Gamma, gamma/(gamma - 1) );

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;
#else
  Real sRef = log( pRef / pow(rhoRef, gamma) );
  Real HRef = gas.enthalpy(rhoRef, tRef) + 0.5*(uRef*uRef + vRef*vRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = pRef;
#endif

  PyDict PyBCList;
//  PyBCList["BCSlip"] = BCSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCOut"] = {1,2};
  BCBoundaryGroups["BCIn"] = {0,3};

//  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
//  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1.0e-48;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 3000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] =  false;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

//   norm data
//  Real normVec[10];   // L2 error
//  int indx = 0;

//  fstream fout( "tmp/hdgL1em3_Ec0p3Re10.txt", fstream::out );

  int ii, jj;

  int ordermin = 1;
#ifdef SANS_FULLTEST
  int ordermax = 5;
#else
  int ordermax = 4;
#endif

  for (int order = ordermin; order <= ordermax; order++)
  {
    jj = 2;
    ii = jj; //small grid!

    // grid
    XField2D_Box_UnionJack_Triangle_X1 xfld(ii, jj);

    Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
    FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
    lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

    const int nDOFPDE = qfld.nDOF();

    QuadratureOrder quadratureOrder( xfld, -1 );

    PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder, ResidualNorm_Default, tol,
                                       {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

    //////////////////////
    // Set the uniform initial condition and resolve
    //////////////////////
    qfld = qRef;
    for (int i=0; i<rfld.nDOF(); i++) rfld.DOF(i) = 0.0;
    lgfld = 0;

    SystemVectorClass ini(PrimalEqSet.vectorStateSize());
    SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

    PrimalEqSet.fillSystemVector(ini);
    // is rsd zero?
    rsd = 0;
    PrimalEqSet.residual(ini, rsd);

    // check that the residual is zero
    Real rsdPDEnrm[4] = {0,0,0,0};

    int nSol = 4;
    for (int j = 0; j < nSol; j++)
      rsdPDEnrm[j] = 0;

    for (int n = 0; n < nDOFPDE; n++)
      for (int j = 0; j < nSol; j++)
        rsdPDEnrm[j] += pow(rsd[PrimalEqSet.iPDE][n][j],2);

    for (int j=0; j<nSol; j++)
      BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[j]), tol[0]);

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
