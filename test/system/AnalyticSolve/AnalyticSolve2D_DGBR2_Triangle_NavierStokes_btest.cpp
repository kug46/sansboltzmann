// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AnalyticSolve2D_DGBR2_Triangle_NavierStokes_btest
// testing of 2-D DGBR2 for NS on triangle Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "Topology/Dimension.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/OutputEuler2D.h"

#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"
#include "pde/BCParameters.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftingOperator.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_Output.h"
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( AnalyticSolve2D_DGBR2_Triangle_NavierStokes_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AnalyticSolve_DGBR2_Triangle )
{

  typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
  typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;
  typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;
  typedef SolutionFunction_NavierStokes2D_QuadraticMMS<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> SolnType;
  typedef SolnNDConvertSpace<PhysD2,SolnType> NDSolnType;
  typedef ForcingFunction2D_MMS<PDEClass> ForcingType;

  typedef OutputEuler2D_V2<PDEClass> OutputClassV2;
  typedef OutputNDConvertSpace<PhysD2, OutputClassV2> NDOutputClassV2;
  typedef IntegrandCell_DGBR2_Output<NDOutputClassV2> IntegrandOutputClassV2;

  typedef OutputEuler2D_MomentumErrorSquare<PDEClass> OutputClassMomentumErrorSquare;
  typedef OutputNDConvertSpace<PhysD2, OutputClassMomentumErrorSquare> NDOutputClassMomentumErrorSquare;
  typedef IntegrandCell_DGBR2_Output<NDOutputClassMomentumErrorSquare> IntegrandOutputClassMomentumErrorSquare;

  typedef BCEuler2D<BCTypeFunction_mitState, PDEEuler2D<TraitsSizeEuler, TraitsModelEuler<QType, GasModel>>> BCSolnClass;

  typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

  typedef AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvertSpace, BCVector,
                                     AlgEqSetTraits_Sparse, DGBR2, XField<PhysD2, TopoD2>> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  std::vector<Real> tol = {1e-10, 1e-10};

  // PDE
  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;
  // PDE
  PyDict gasModelDict;
  gasModelDict[GasModelParams::params.gamma] = gamma;
  gasModelDict[GasModelParams::params.R] = R;
  GasModel gas(gasModelDict);

// INFLOW CONDITIONS
  Real rho0 = 1.0;
  Real u0 = 0.3;
  Real v0 = 0.1;
  Real p0 = 1.0;
//  Real T0 = p0/(rho0*R);

//  Real E0 = Cv*T0 + 0.5*(u0*u0 + v0*v0);

  const Real Re = 1.0;
  const Real muRef = sqrt(u0*u0 + v0*v0) / Re; // kg/(m s) // FOR RE = 10
  const Real Prandtl = 0.72;
//  GasModel gas(gamma, R);
  ViscosityModelType visc(muRef); //single argument for constant viscosity
  ThermalConductivityModel tcond(Prandtl, Cp);

  NDSolnType soln(gas);
  std::shared_ptr<ForcingType> forcingptr( new ForcingType(soln) );

  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw, eVanLeer, forcingptr );
  // HDG solution field at inlet
  ArrayQ q0;
  pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rho0, u0, v0, p0) );

  // Momentum error
  NDOutputClassMomentumErrorSquare fcnOutputMomErrorSquare(pde, soln);
  IntegrandOutputClassMomentumErrorSquare fcnOutMomErrorSquare( fcnOutputMomErrorSquare, {0} );

  // Velocity squared output
  NDOutputClassV2 fcnOutputV2(pde);
  IntegrandOutputClassV2 fcnOutV2( fcnOutputV2, {0} );

  // DGBR2 discretization
  Real viscousEtaParameter = 6;
  DiscretizationDGBR2 disc(0, viscousEtaParameter);

  PyDict Function;
  Function[BCSolnClass::ParamsType::params.Function.Name] = BCSolnClass::ParamsType::params.Function.QuadraticMMS;
  Function[SolnType::ParamsType::params.gasModel] = gasModelDict;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.Function_mitState;
  BCIn[BCSolnClass::ParamsType::params.Function] = Function;
  BCIn[BCSolnClass::ParamsType::params.Characteristic] = true;

  PyDict PyBCList;
  PyBCList["BCIn"] = BCIn;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCIn"] = {0,1,2,3};

  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1.0e-48;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = false;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 3000;
  NewtonSolverDict[NewtonSolverParam::params.Verbose] =  false;

  NewtonSolverParam::checkInputs(NewtonSolverDict);

//   norm data
//  Real normVec[10];   // L2 error
//  int indx = 0;

//  fstream fout( "tmp/hdgL1em3_Ec0p3Re10.txt", fstream::out );

  int ii, jj;

  int ordermin = 2;
#ifdef SANS_FULLTEST
  int ordermax = 5;
#else
  int ordermax = 2;
#endif

  for (int order = ordermin; order <= ordermax; order++)
  {
    {
      jj = 2;
      ii = jj; //small grid!

      // grid
      XField2D_Box_UnionJack_Triangle_X1 xfld(ii, jj);

      Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
      FieldLift_DG_Cell<PhysD2, TopoD2, VectorArrayQ> rfld(xfld, order, BasisFunctionCategory_Legendre);
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
      lgfld(xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );

      const int nDOFPDE = qfld.nDOF();

      //L2 projection of exact solution

#if 0
      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(soln, {0}), (xfld, qfld) );

      // Compute the field of inverse mass matrices
      FieldDataInvMassMatrix_Cell mmfld(qfld);

      // quadrature rule (quadratic: basis & flux both linear)
//      int quadOrderLO = 4;

      // integrand
//      IntegrandInteriorTrace_LiftingOperator<NDPDEClass> fcn( pde, {0} );
//      IntegrateInteriorTraceGroups<TopoD2>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcn, mmfld),
//                                                      xfld, (qfld, rfld), &quadOrderLO, 1);
      lgfld = 0;
#endif

      QuadratureOrder quadratureOrder( xfld, - 1 );

      PrimalEquationSetClass PrimalEqSet(xfld, qfld, rfld, lgfld, pde, disc, quadratureOrder, ResidualNorm_Default, tol,
                                         {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

      //////////////////////
      // Set the uniform initial condition and resolve
      //////////////////////
      qfld = q0;
      rfld = 0.0;
      lgfld = 0;

      NewtonSolver<SystemMatrixClass> Solver( PrimalEqSet, NewtonSolverDict );

      SystemVectorClass ini(PrimalEqSet.vectorStateSize());
      SystemVectorClass sln(PrimalEqSet.vectorStateSize());
      SystemVectorClass rsd(PrimalEqSet.vectorEqSize());
      rsd = 0;

      PrimalEqSet.fillSystemVector(ini);
      sln = ini;

      SolveStatus status = Solver.solve(ini, sln);
      BOOST_CHECK( status.converged );
      PrimalEqSet.setSolutionField(sln);

      ini = 0;
      sln = 0;
      rsd = 0;
      PrimalEqSet.fillSystemVector(ini);
      PrimalEqSet.residual(ini, rsd);

      // check that the residual is zero
      Real rsdPDEnrm[4] = {0,0,0,0};

      int nSol = 4;
      for (int j = 0; j < nSol; j++)
        rsdPDEnrm[j] = 0;

      for (int n = 0; n < nDOFPDE; n++)
        for (int j = 0; j < nSol; j++)
          rsdPDEnrm[j] += pow(rsd[PrimalEqSet.iPDE][n][j],2);

      for (int j=0; j<nSol; j++)
        BOOST_CHECK_SMALL( sqrt(rsdPDEnrm[j]), tol[0]);


      // Monitor Velocity Error
      Real VelSquare = 0.0;
      Real MomError = 0.0;
      Real VelSquareTrue = 0.102891; //ANALYTIC INTEGRAL OF V2

      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( fcnOutMomErrorSquare, MomError ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      MomError = sqrt(MomError);

      IntegrateCellGroups<TopoD2>::integrate(
          FunctionalCell_DGBR2( fcnOutV2, VelSquare ),
          xfld, (qfld, rfld), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

      BOOST_CHECK_SMALL(MomError, tol[0]);
      BOOST_CHECK_SMALL( (VelSquare-VelSquareTrue), tol[0]);

//#define SANS_VERBOSE
#ifdef SANS_VERBOSE
//      fout << "P = " << order << " ii = " << ii <<
//          ": L2 Mom error = " << std::setprecision(15) << sqrt(MomError) <<
//          ": V2 Output = " << std::setprecision(15) << VelSquare;
//      //      if (indx > 1)
//      //        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
//      fout << endl;
      cout << "P = " << order << " ii = " << ii <<
          ": Mom error = " << std::setprecision(15) << sqrt(MomError) <<
          ": V2 Output Error = " << std::setprecision(15) << (VelSquare - VelSquareTrue);
      //      if (indx > 1)
      //        cout << "  (ratio = " << normVec[indx-1]/normVec[indx-2] << ")";
      cout << endl;
#endif
#if 0
      // Tecplot dump grid
      string filename = "tmp/DGBR2_AnalyticMMS";
      filename += "_P" + to_string(order);
      filename += "_ii" + to_string(ii);
      filename += "_Re" + to_string(int(Re)) + ".dat";
      output_Tecplot( qfld, filename );
      std::cout << "Wrote Tecplot File\n";
#endif
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
