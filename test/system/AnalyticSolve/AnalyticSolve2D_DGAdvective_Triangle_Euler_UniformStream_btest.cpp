// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// DGAdvective_Euler_UniformStream_btest
// testing of 2-D Advective DG for Euler on triangle rectangular Mesh

//#define SANS_FULLTEST
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "pyrite_fstream.h"
#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DConservative.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "pde/BCParameters.h"

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"

#include "SolutionTrek/TimeMarching/RungeKutta/RungeKutta.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/ProjectSoln/ProjectConstCell.h"
#include "Field/output_Tecplot.h"
//
//#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
//#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
//#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
//#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_CubicSourceBump_Xq.h"
#include "unit/UnitGrids/XField2D_BoxPeriodic_Triangle_X1.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( DGAdvective_Euler_UniformStream_test_suite )


//----------------------------------------------------------------------------//
// check that residual = 0 for uniform flow initialized to exact initial conditions

BOOST_AUTO_TEST_CASE( DGAdvective_Euler_Duct_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE
  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);
  int nSol = NDPDEClass::N;

  // BC
  // Create a BC dictionary
  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry;

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt;

  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.sSpec] = 0.0;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.HSpec] = 3.505;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.qtSpec] = 0;

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure>::params.pSpec] = 1.0;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {0,2};
  BCBoundaryGroups["BCOut"] = {1};
  BCBoundaryGroups["BCIn"] = {3};


  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};

  int ii, jj;
  int order = 1;

  int power = 1;
  jj = pow( 2, power );
  ii = 3*jj; //small grid!
  Real tau = 0.0; //tau=0 is straight channel

  // grid: 2D Cubic Source Bump
  XField2D_CubicSourceBump_Xq xfld( ii, jj, tau );

  // DG solution field
  // cell solution

  //exact solution is (1, 0.1, 0, 1);
  ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(1, 0.1, 0, 1) );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // Set the initial condition
  qfld = q0;

  const int nDOFPDE = qfld.nDOF();

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
  lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  ////////////
  //Check RSD = 0
  ////////////

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                     {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

  SystemVectorClass ini(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

  PrimalEqSet.fillSystemVector(ini);

  // is rsd zero?
  rsd = 0;
  PrimalEqSet.residual(ini, rsd);
  Real rsdPDEnrm = 0;
  for (int n = 0; n < nDOFPDE; n++)
    for (int j = 0; j < nSol; j++)
      rsdPDEnrm += pow(rsd[0][n][j],2);

  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGAdvective_Euler_Diagonal_test )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  // PDE
  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  NDPDEClass pde(gas, Euler_ResidInterp_Raw);
  int nSol = NDPDEClass::N;

  // BC
  // Create a BC dictionary - flow going from bottom left to top right
  PyDict BCIn1; //left boundary
  BCIn1[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt;

  BCIn1[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.sSpec] = 0.0;
  BCIn1[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.HSpec] = 3.505;
  BCIn1[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.qtSpec] = -0.03;

  PyDict BCIn2; //bottom boundary
  BCIn2[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt;

  BCIn2[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.sSpec] = 0.0;
  BCIn2[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.HSpec] = 3.505;
  BCIn2[BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params.qtSpec] = 0.095393920141695;

  PyDict BCOut; // upper-right boundaries
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure>::params.pSpec] = 1.0;

  PyDict PyBCList;
  PyBCList["BCIn1"] = BCIn1;
  PyBCList["BCIn2"] = BCIn2;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCOut"] = {1,2};
  BCBoundaryGroups["BCIn1"] = {3};
  BCBoundaryGroups["BCIn2"] = {0};

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {0,1,2,3};


  int ii, jj;
  int order = 1;

  int power = 1;
  jj = pow( 2, power );
  ii = 3*jj; //small grid!
  Real tau = 0.0; //tau=0 is straight channel

  // grid: 2D Cubic Source Bump
  XField2D_CubicSourceBump_Xq xfld( ii, jj, tau );

  // DG solution field
  // cell solution

  //exact solution is (1, 0.095393920141695, 0.03, 1); velocity magnitude is 0.1
  ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(1, 0.095393920141695, 0.03, 1) );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);

  // Set the initial condition
  qfld = q0;

  const int nDOFPDE = qfld.nDOF();

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
  lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  ////////////
  //Check RSD = 0
  ////////////

  PrimalEquationSetClass PrimalEqSet(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                     {0}, {0,1,2}, PyBCList, BCBoundaryGroups );

  SystemVectorClass ini(PrimalEqSet.vectorStateSize());
  SystemVectorClass rsd(PrimalEqSet.vectorEqSize());

  PrimalEqSet.fillSystemVector(ini);

  // is rsd zero?
  rsd = 0;
  PrimalEqSet.residual(ini, rsd);
  Real rsdPDEnrm = 0;
  for (int n = 0; n < nDOFPDE; n++)
    for (int j = 0; j < nSol; j++)
      rsdPDEnrm += pow(rsd[0][n][j],2);

  BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DGAdvective_Euler_Periodic_RK_test )
{
  typedef QTypeConservative QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
  typedef NDPDEClass::ArrayQ<Real> ArrayQ;

  typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

  typedef AlgebraicEquationSet_DGAdvective< NDPDEClass, BCNDConvertSpace, BCVector,
                                            AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;

  typedef RK<NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2>> RKClass;

  PyDict NonLinearSolverDict, NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NonLinearSolverParam::params.NonLinearSolver.Solver] = NonLinearSolverParam::params.NonLinearSolver.Newton;
  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;

  NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;
  NonLinearSolverParam::checkInputs(NonLinearSolverDict);

  // PDE
  GlobalTime time(0);
  const Real gamma = 1.4;
  const Real R = 0.4;

  // reference state (freestream)
  const Real Mach = 0.1;

  const Real rhoRef = 1;                            // density scale
  const Real tRef = 1;          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure
  const Real cRef = sqrt(gamma*R*tRef);

  const Real qRef = cRef*Mach;                      // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)
  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);


  GasModel gas(gamma, R);
  NDPDEClass pde(time, gas, Euler_ResidInterp_Raw);
  int nSol = NDPDEClass::N;

  // BC
  PyDict PyBCList;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;

  //No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  const std::vector<int> BoundaryGroups = {};


  int ii = 2, jj = 3;
  int order = 1;

  // grid: 2D Cubic Source Bump
  XField2D_BoxPeriodic_Triangle_X1 xfld( ii, jj );

  // DG solution field

  //exact solution
  ArrayQ q0 = pde.setDOFFrom( DensityVelocityPressure2D<Real>(rhoRef, uRef, vRef, pRef) );

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfld(xfld, order, BasisFunctionCategory_Legendre);
  qfld = q0;

  const int nDOFPDE = qfld.nDOF();

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>
  lgfld( xfld, order, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0;

  QuadratureOrder quadratureOrder( xfld, - 1 );
  std::vector<Real> tol = {1e-11, 1e-11};

  ////////////
  //Check RSD = 0
  ////////////

  PrimalEquationSetClass AlgEqSetSpace(xfld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_Default, tol,
                                       {0}, {0}, PyBCList, BCBoundaryGroups, time );

  SystemVectorClass rsd(AlgEqSetSpace.vectorEqSize());

  Real dt = 1.2;
  int RKtype = 0;

  for (int RKorder = 1; RKorder <= 4; RKorder++)
  {
    int stages = RKorder;

    // The RK class
    RKClass RK( RKorder, stages, RKtype, dt, time, xfld, qfld, NonLinearSolverDict, pde, quadratureOrder, {tol[0]}, {0}, AlgEqSetSpace );

    // Set the initial condition
    qfld = q0;

    // Advance the solution
    int nsteps = 5;
    RK.march(nsteps);

    // is rsd zero?
    rsd = 0;
    AlgEqSetSpace.residual(rsd);
    Real rsdPDEnrm = 0;
    for (int n = 0; n < nDOFPDE; n++)
      for (int j = 0; j < nSol; j++)
        rsdPDEnrm += pow(rsd[0][n][j],2);

    BOOST_CHECK_SMALL( sqrt(rsdPDEnrm), 1e-12 );
  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
