// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCEulermitAVSensor2D_Consistency_RANSSA_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/TraitsRANSSAArtificialViscosity.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};
}

namespace SANS
{

template <class T>
using SAnt2D_rhovT = SAnt2D<DensityVelocityTemperature2D<T>>;

}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( BCEulermitAVSensor2D_Consistency_RANSSA_btest )

typedef boost::mpl::list< QTypePrimitiveRhoPressure > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeWallNoSlipAdiabatic_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;

  typedef BCRANSSA2D< BCTypeWall_mitState, BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  typedef BCRANSSA2D< BCTypeWall_mitState, BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, nut = 4.2, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  PyDict d;

  BCClass bc(pde, d);
  AVBCClass avbc(avpde, d);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nut}) );
  AVVariable<SAnt2D_rhovT,Real> qdata({rho, u, v, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( dist, x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( dist, x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeWallNoSlipAdiabatic_mitState_nonZeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;

  typedef BCRANSSA2D< BCTypeWall_mitState, BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  typedef BCRANSSA2D< BCTypeWall_mitState, BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, nut = 4.2, s = 0.5;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  PyDict d;

  BCClass bc(pde, d);
  AVBCClass avbc(avpde, d);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nut}) );
  AVVariable<SAnt2D_rhovT,Real> qdata({rho, u, v, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.23};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.65};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( dist, x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( dist, x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeSymmetry_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;

  typedef BCRANSSA2D< BCTypeNeumann_mitState, BCNavierStokes2D< BCTypeSymmetry_mitState, PDEClass>> BCClass;
  typedef BCRANSSA2D< BCTypeNeumann_mitState, BCNavierStokes2D< BCTypeSymmetry_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, nut = 4.2, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  PyDict d;

  BCClass bc(pde, d);
  AVBCClass avbc(avpde, d);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nut}) );
  AVVariable<SAnt2D_rhovT,Real> qdata({rho, u, v, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( dist, x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( dist, x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeSymmetry_mitState_nonZeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;

  typedef BCRANSSA2D< BCTypeNeumann_mitState, BCNavierStokes2D< BCTypeSymmetry_mitState, PDEClass>> BCClass;
  typedef BCRANSSA2D< BCTypeNeumann_mitState, BCNavierStokes2D< BCTypeSymmetry_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, nut = 4.2, s = 0.5;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  PyDict d;

  BCClass bc(pde, d);
  AVBCClass avbc(avpde, d);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nut}) );
  AVVariable<SAnt2D_rhovT,Real> qdata({rho, u, v, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.75};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.09};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( dist, x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( dist, x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeFullState_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;

  typedef BCRANSSA2D< BCTypeFullState_mitState, BCEuler2D< BCTypeFullState_mitState, PDEClass>> BCClass;
  typedef BCRANSSA2D< BCTypeFullState_mitState, BCEuler2D< BCTypeFullState_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> PDEBaseClass;
  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;
  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;

  typedef BCParameters< BCRANSSAmitAVDiffusion2DVector<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, nut = 4.2, s = 0.0;
  Real p  = R*rho*t;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  typedef SAnt2D<DensityVelocityPressure2D<Real>>::ParamsType ParamsType;

  PyDict d;
  d[NSVariableType2DParams::params.StateVector.Variables] = SAVariableType2DParams::params.StateVector.PrimitivePressure;
  d[ParamsType::params.rho] = rho;
  d[ParamsType::params.u] = u;
  d[ParamsType::params.v] = v;
  d[ParamsType::params.p] = p;
  d[ParamsType::params.nt] = nut;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  BCClass bc(pde, BCFullState);
  AVBCClass avbc(avpde, BCFullState);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nut}) );
  AVVariable<SAnt2D_rhovT,Real> qdata({rho, u, v, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( dist, x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( dist, x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeInflowSubsonic_sHqt_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;

  typedef BCRANSSA2D< BCTypeNeumann_mitState, BCEuler2D< BCTypeInflowSubsonic_sHqt_mitState, PDEClass>> BCClass;
  typedef BCRANSSA2D< BCTypeNeumann_mitState, BCEuler2D< BCTypeInflowSubsonic_sHqt_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> PDEBaseClass;
  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;
  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;

  typedef BCParameters< BCRANSSAmitAVDiffusion2DVector<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, nut = 4.2, s = 0.0;
  Real p  = R*rho*t;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  Real sSpec = log(p / pow(rho,gamma));
  Real HSpec = gas.enthalpy(rho,t) + 0.5*(u*u+v*v);
  Real VxSpec = u;
  Real VySpec = v;

  //Pydict constructor
  PyDict BCsHqt;
  BCsHqt[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCsHqt[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.sSpec] = sSpec;
  BCsHqt[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.HSpec] = HSpec;
  BCsHqt[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VxSpec] = VxSpec;
  BCsHqt[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VySpec] = VySpec;

  BCClass bc(pde, BCsHqt);
  AVBCClass avbc(avpde, BCsHqt);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nut}) );
  AVVariable<SAnt2D_rhovT,Real> qdata({rho, u, v, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( dist, x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( dist, x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeOutflowSubsonic_Pressure_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;

  typedef BCRANSSA2D< BCTypeNeumann_mitState, BCEuler2D< BCTypeOutflowSubsonic_Pressure_mitState, PDEClass>> BCClass;
  typedef BCRANSSA2D< BCTypeNeumann_mitState, BCEuler2D< BCTypeOutflowSubsonic_Pressure_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> PDEBaseClass;
  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;
  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;

  typedef BCParameters< BCRANSSAmitAVDiffusion2DVector<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, nut = 4.2, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  typedef typename BCClass::ParamsType ParamsType;

  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCSubsonicOut[ParamsType::params.pSpec] = 101325.0;

  BCClass bc(pde, BCSubsonicOut);
  AVBCClass avbc(avpde, BCSubsonicOut);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nut}) );
  AVVariable<SAnt2D_rhovT,Real> qdata({rho, u, v, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( dist, x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( dist, x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
