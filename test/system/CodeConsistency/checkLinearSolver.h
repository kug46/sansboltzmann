// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TEST_SYSTEM_CODECONSISTENCY_CHECKLINEARSOLVER_H_
#define TEST_SYSTEM_CODECONSISTENCY_CHECKLINEARSOLVER_H_

#include "Python/PyDict.h"

#include "LinearAlgebra/SparseLinAlg/ScalarVector.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/WritePlainVector.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "NonLinearSolver/NewtonSolver.h"

namespace SANS
{

//===========================================================================//
namespace detail_IBLCaseSetup
{
static inline Real
getqinf(const Real& Reinf, const Real& p0, const Real& T0,
        const Real& gamma, const Real& R, const Real& mu, const Real M=0.0)
{
  const Real tmp = 1.0 + 0.5*(gamma-1)*pow(M, 2.0);
  const Real pinf = p0/pow(tmp, gamma/(gamma-1.0));
  const Real Tinf = T0/tmp;
  const Real rhoinf = pinf/(R*Tinf);
  const Real nu = mu / rhoinf;
  const Real qinf = Reinf * nu;
  std::cout << std::scientific << "Re = " << Reinf << ", Mach = " << M << ", qinf = " << qinf << std::endl;
  return qinf;
}
}

namespace detail_checkLinearSolver
{

template <class AESType>
void solve_and_set_solution_field(AESType& eqSet, const PyDict& newtonSolverDict)
{
  NewtonSolver<typename AESType::SystemMatrix> eqSolver(eqSet, newtonSolverDict);

  typename AESType::SystemVector sln(eqSet.vectorStateSize());

  // check residuals of initial solution
  eqSet.fillSystemVector(sln);

  // solve
  SolveStatus status = eqSolver.solve(sln, sln);
  BOOST_CHECK( status.converged );

  eqSet.setSolutionField(sln);
}

template <class AESType>
void check_linear_solver_block4x4(AESType& eqnSet)
{ // UMFPACK fails to solve correctly if pivot tolerance is too low (aka too little pivoting)
  PyDict UMFPACKDict;
  SANS::SLA::UMFPACKParam::checkInputs(UMFPACKDict);

  SANS::SLA::UMFPACK<typename AESType::SystemMatrix> linearSolver(UMFPACKDict, eqnSet);

  typename AESType::SystemVector r0(eqnSet.vectorEqSize());
  r0 = 0.0;
  SANS::DLA::index(r0.v0[0][0],0) = 1.0;

  typename AESType::SystemVector dq(eqnSet.vectorStateSize());
  dq = 0;

  linearSolver.solve(r0, dq);

  typename AESType::SystemVector b(eqnSet.vectorEqSize());
  b = linearSolver.A() * dq;

  SANS::SLA::ScalarVector b_plain(b), r0_plain(r0);
  const Real tol_small = 8e-12, tol_close = 5e-14;
  for (int j = 0; j < b_plain.m; ++j)
    SANS_CHECK_CLOSE(r0_plain.v[j], b_plain.v[j], tol_small, tol_close);

#if defined(IBL2D_DUMP) && 0
  cout << "writing r0 to tmp/r0.dat" << endl;
  SANS::SLA::WritePlainVector( r0, "tmp/r0.dat" );

  cout << "writing dq to tmp/dq.dat" << endl;
  SANS::SLA::WritePlainVector( dq, "tmp/dq.dat" );

  cout << "writing b to tmp/b.dat" << endl;
  SANS::SLA::WritePlainVector( b, "tmp/b.dat" );

  cout << "writing jacUMFPACK to tmp/jacUMFPACK.mtx" << endl;
  SANS::SLA::WriteMatrixMarketFile( linearSolver.A(), "tmp/jacUMFPACK.mtx" );
#endif
}

}

}

#endif /* TEST_SYSTEM_CODECONSISTENCY_CHECKLINEARSOLVER_H_ */
