// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Linear-vorticity panel method with constant source strength
// Similar to XFOIL formulation

//TODO: add comparison to nonzero angle of attack & asymmetric airfoils
//TODO: cp on naca0004 alpha=1 was manually compared and was fine

// #define PANEL_DEBUG_MODE_FiniteTE_Airfoil_NoSource
// #define PANEL_DEBUG_MODE_SharpTE_Airfoil_NoSource
// #define PANEL_DEBUG_MODE_FiniteTE_AirfoilWake_WithSource
// #define PANEL_DEBUG_MODE_SharpTE_Airfoil_WithSource
// #define PANEL_DEBUG_MODE_SharpTE_AirfoilWake_WithSource

// #define SANS_VERBOSE

#include "Python/PyDict.h"

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include "Discretization/IntegrateCellGroups.h"

#include "Field/Element/ElementProjection_L2.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "PanelMethod/XfoilPanel.h"
#include "PanelMethod/XfoilPanelEquationSet.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_X1_2Group_AirfoilWithWake.h"

//----------------------------------------------------------------------------//

using namespace std;

namespace SANS
{
typedef XfoilPanel PanelType;

typedef XfoilPanelEquationSet<PanelType,AlgEqSetTraits_Sparse> PanelEquationClass;

typedef PanelEquationClass::SystemMatrix SystemMatrixClass;
typedef PanelEquationClass::SystemVector SystemVectorClass;
typedef PanelEquationClass::SystemNonZeroPattern SystemNonZeroPattern;

typedef typename PanelEquationClass::VectorX VectorX;
typedef typename PanelEquationClass::GamFieldType<Real> GamFieldType;
typedef typename PanelEquationClass::LamFieldType<Real> LamFieldType;
}

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( SolveConsistency_XfoilPanel_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FiniteTE_NACA0004_Airfoil_NoSource_test )
{
#ifdef PANEL_DEBUG_MODE_FiniteTE_Airfoil_NoSource
  cout << "//--------------------------------------------------------------------//" << endl;
  cout << "// FiniteTE_NACA0004_Airfoil_NoSource" << endl;
  cout << "//--------------------------------------------------------------------//" << endl;
#endif

  // solver settings

  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
#ifdef PANEL_DEBUG_MODE_FiniteTE_Airfoil_NoSource
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
#else
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // set up grid
  string airfoilname = "naca0004";
  int nelem = 64;

  string filename_airfoil = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem) + ".txt";
  std::vector<VectorX> coordinates_a;
  XField2D_Line_X1_1Group::readXFoilGrid(filename_airfoil, coordinates_a);
  XField2D_Line_X1_1Group xfld(coordinates_a);

  // set up panel
  const int alpha = 0;
  const Real qinf = 1.573869150000000; // [m/s] freestream speed
  const VectorX Vinf = { qinf*cos(static_cast<Real>(alpha)*PI/180.0),
                         qinf*sin(static_cast<Real>(alpha)*PI/180.0) }; // freestream velocity
  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  PanelType panel(Vinf,p0,T0,xfld,{0});

  const int nnode_a = panel.nNodeAirfoil();
  const int nelem_a = panel.nPanelAirfoil();

  const int nDOFgam = nnode_a;
  const int nDOFlam = nelem_a;

  // initialize solution fields
  GamFieldType gamfld(xfld, PanelEquationClass::order_gam, BasisFunctionCategory_Hierarchical,{0});
  for (int i = 0; i < nDOFgam; ++i)
    gamfld.DOF(i) = 1;

  Real Psi0 = 1;

  LamFieldType lamfld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < nDOFlam; ++i)
    lamfld.DOF(i) = 0;

  BOOST_CHECK(nelem_a == lamfld.nDOF());

  // set up Qauxi field
  LamFieldType qauxifld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < nDOFlam; ++i)
    qauxifld.DOF(i) = lamfld.DOF(i);

  BOOST_CHECK(lamfld.nDOF() == qauxifld.nDOF());

  // set up panel equation
  const Real tol_eqn = 1e-12;
  PanelEquationClass Equation(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  NewtonSolver<SystemMatrixClass> Solver(Equation, NewtonSolverDict);

  SystemVectorClass ini(Equation.vectorStateSize()), sln(Equation.vectorStateSize());
  SystemVectorClass rsd(Equation.vectorEqSize());

  // check residuals of initial solution
  Equation.fillSystemVector(ini);
  Equation.residual(ini,rsd);

#ifdef PANEL_DEBUG_MODE_FiniteTE_Airfoil_NoSource
  for ( int i = 0; i < rsd(0).m(); ++i )
    cout << "rsd(0)[" << i << "] = " << setprecision(16) << rsd(0)[i] << endl;

  // initial jacobian
  SystemNonZeroPattern nz(Equation.matrixSize());
  Equation.jacobian(ini,nz);

  SystemMatrixClass jac(nz);
  Equation.jacobian(ini,jac);

  fstream fout("tmp/jac_XfoilPanel.mtx", fstream::out);
  cout << "Writing global Jacobian" << endl;  WriteMatrixMarketFile(jac,fout);
#endif

  // solve
  SolveStatus status = Solver.solve(ini, sln);
  BOOST_CHECK( status.converged );

  Equation.setSolutionField(sln);

  /////////////////////////////
  // check if residual has converged
  /////////////////////////////
  Equation.residual(sln,rsd);

  const Real res_tol = 1.e-12;

  for ( int i = 0; i < rsd.m(); ++i )
    for ( int j = 0; j < rsd(i).m(); ++j )
      BOOST_CHECK_SMALL( rsd(i)[j], res_tol );

  /////////////////////////////
  // check solution
  /////////////////////////////

  // print solution
#ifdef PANEL_DEBUG_MODE_FiniteTE_Airfoil_NoSource
  cout << "\n//-----print out solution-----//" << endl;
  for (int k=0; k<sln.m(); ++k)
  {
    for ( int i = 0; i < rsd(k).m(); ++i )
      cout << "sln("<<k<<")[" << i << "] = " << setprecision(16) << sln(k)[i] << endl;
  }

  cout << "\n//-----print out Ue-----//" << endl;
  for ( int i = 0; i < xfld.nDOF(); ++i )
  {
    Real Ue = 0;
    panel.Uecalc(gamfld,lamfld,xfld.DOF(i),i,Ue);

    cout << setprecision(16) << Ue << endl;
    if (i == (nDOFgam-1))
      cout << endl;
  }
#endif

  // Pyrite check
  // check with MATLAB prototype results
  {
    const Real pyrite_tol = 5.e-9;

    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_FiniteTE_NACA0004_Airfoil_NoSource_MATLAB_Test.txt";
    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < nDOFgam; ++i)
      pyriteFile << gamfld.DOF(i) << endl;

    pyriteFile << Psi0 << endl;

    for (int i = 0; i < nDOFlam; ++i)
      pyriteFile << lamfld.DOF(i) << endl;
  }
  // check with previous version of SANS panel code
  {
    const Real pyrite_tol = 5.e-9;

    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_FiniteTE_NACA0004_Airfoil_NoSource_Test.txt";
    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < nDOFgam; ++i)
      pyriteFile << gamfld.DOF(i) << endl;

    pyriteFile << Psi0 << endl;

    for (int i = 0; i < nDOFlam; ++i)
      pyriteFile << lamfld.DOF(i) << endl;
  }
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SharpTE_Joukowski_Airfoil_NoSource_test )
{
#ifdef PANEL_DEBUG_MODE_SharpTE_Airfoil_NoSource
  cout << "//--------------------------------------------------------------------//" << endl;
  cout << "// SharpTE_Joukowski_Airfoil_NoSource" << endl;
  cout << "//--------------------------------------------------------------------//" << endl;
#endif

  // solver settings

  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
#ifdef PANEL_DEBUG_MODE_SharpTE_Airfoil_NoSource
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
#else
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // set up grid
  string airfoilname = "joukowski";
  int nelem = 80;

  string filename_airfoil = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem) + ".txt";
  std::vector<VectorX> coordinates_a;
  XField2D_Line_X1_1Group::readXFoilGrid(filename_airfoil, coordinates_a);
  XField2D_Line_X1_1Group xfld(coordinates_a);

  // set up panel
  const int alpha = 0;
  const Real qinf = 1.573869150000000; // [m/s] freestream speed
  const VectorX Vinf = { qinf*cos(static_cast<Real>(alpha)*PI/180.0),
                         qinf*sin(static_cast<Real>(alpha)*PI/180.0) }; // freestream velocity
  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  PanelType panel(Vinf,p0,T0,xfld,{0});

  const int nnode_a = panel.nNodeAirfoil();
  const int nelem_a = panel.nPanelAirfoil();

  const int nDOFgam = nnode_a;
  const int nDOFlam = nelem_a;

  // initialize solution fields
  GamFieldType gamfld(xfld, PanelEquationClass::order_gam, BasisFunctionCategory_Hierarchical,{0});
  for (int i = 0; i < nDOFgam; ++i)
    gamfld.DOF(i) = 1;

  Real Psi0 = 1;

  LamFieldType lamfld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < nDOFlam; ++i)
    lamfld.DOF(i) = 0;

  BOOST_CHECK(nelem_a == lamfld.nDOF());

  // set up Qauxi field
  LamFieldType qauxifld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  for (int i = 0; i < nDOFlam; ++i)
    qauxifld.DOF(i) = lamfld.DOF(i);

  BOOST_CHECK(lamfld.nDOF() == qauxifld.nDOF());

  // set up panel equation
  const Real tol_eqn = 1e-12;
  PanelEquationClass Equation(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  NewtonSolver<SystemMatrixClass> Solver(Equation, NewtonSolverDict);

  SystemVectorClass ini(Equation.vectorStateSize()), sln(Equation.vectorStateSize());
  SystemVectorClass rsd(Equation.vectorEqSize());

  // check residuals of initial solution
  Equation.fillSystemVector(ini);
  Equation.residual(ini,rsd);

#ifdef PANEL_DEBUG_MODE_SharpTE_Airfoil_NoSource
  for ( int i = 0; i < rsd(0).m(); ++i )
    cout << "rsd(0)[" << i << "] = " << setprecision(16) << rsd(0)[i] << endl;

  // initial jacobian
  SystemNonZeroPattern nz(Equation.matrixSize());
  Equation.jacobian(ini,nz);

  SystemMatrixClass jac(nz);
  Equation.jacobian(ini,jac);

  fstream fout("tmp/jac_XfoilPanel.mtx", fstream::out);
  cout << "Writing global Jacobian" << endl;  WriteMatrixMarketFile(jac,fout);
#endif

  // solve
  SolveStatus status = Solver.solve(ini, sln);
  BOOST_CHECK( status.converged );

  Equation.setSolutionField(sln);

  /////////////////////////////
  // check if residual has converged
  /////////////////////////////
  Equation.residual(sln,rsd);

  const Real res_tol = 1.e-12;

  for ( int i = 0; i < rsd.m(); ++i )
    for ( int j = 0; j < rsd(i).m(); ++j )
      BOOST_CHECK_SMALL( rsd(i)[j], res_tol);

  /////////////////////////////
  // check solution
  /////////////////////////////

  // print solution
#ifdef PANEL_DEBUG_MODE_SharpTE_Airfoil_NoSource
  cout << "\n//-----print out solution-----//" << endl;
  for (int k=0; k<sln.m(); ++k)
  {
    for ( int i = 0; i < rsd(k).m(); ++i )
      cout << "sln("<<k<<")[" << i << "] = " << setprecision(16) << sln(k)[i] << endl;
  }

  cout << "\n//-----print out Ue-----//" << endl;
  for ( int i = 0; i < xfld.nDOF(); ++i )
  {
    Real Ue = 0;
    panel.Uecalc(gamfld,lamfld,xfld.DOF(i),i,Ue);

    cout << setprecision(16) << Ue <<  endl;
    if (i == (nDOFgam-1))
      cout << endl;
  }
#endif

  // Pyrite check
  // check with MATLAB prototype results
  {
    const Real pyrite_tol = 2.e-9;

  #if PanelSharpTETreatment_isXfoilPaper // 1989 XFOIL paper
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_Airfoil_NoSource_XfoilPaper_MATLAB_Test.txt";
  #else // XFOIL 6.99
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_Airfoil_NoSource_XfoilV699_MATLAB_Test.txt";
  #endif

    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < nDOFgam; ++i)
      pyriteFile << gamfld.DOF(i) << endl;

    pyriteFile << Psi0 << endl;

    for (int i = 0; i < lamfld.nDOF(); ++i)
      pyriteFile << lamfld.DOF(i) << endl;
  }
  // check with previous version of SANS panel code
  {
    const Real pyrite_tol = 2.e-9;
#if PanelSharpTETreatment_isXfoilPaper // 1989 XFOIL paper
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_Airfoil_NoSource_XfoilPaper_Test.txt";
#else // XFOIL 6.99
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_Airfoil_NoSource_XfoilV699_Test.txt";
#endif
    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < nDOFgam; ++i)
      pyriteFile << gamfld.DOF(i) << endl;

    pyriteFile << Psi0 << endl;

    for (int i = 0; i < lamfld.nDOF(); ++i)
      pyriteFile << lamfld.DOF(i) << endl;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FiniteTE_NACA0004_AirfoilAndWakePanel_WithSource_test )
{
#ifdef PANEL_DEBUG_MODE_FiniteTE_AirfoilWake_WithSource
  cout << "//--------------------------------------------------------------------//" << endl;
  cout << "// FiniteTE_NACA0004_AirfoilWake_WithSource" << endl;
  cout << "//--------------------------------------------------------------------//" << endl;
#endif

  // solver settings

  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
#ifdef PANEL_DEBUG_MODE_FiniteTE_AirfoilWake_WithSource
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
#else
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // set up grid
  string airfoilname = "naca0004";
  const int alpha = 0;
  int nelem = 64;

  string filename_airfoil = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem) + ".txt";
  string filename_wake = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem)
                       + "_wake_a" + stringify(alpha) + ".txt";

  std::vector<VectorX> coordinates_a, coordinates_w;

  XField2D_Line_X1_1Group::readXFoilGrid(filename_airfoil, coordinates_a);
  XField2D_Line_X1_1Group::readXFoilGrid(filename_wake, coordinates_w);

  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);

  // set up panel
  const Real qinf = 1.573869150000000; // [m/s] freestream speed
  const VectorX Vinf = { qinf*cos(static_cast<Real>(alpha)*PI/180.0),
                         qinf*sin(static_cast<Real>(alpha)*PI/180.0) }; // freestream velocity
  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  PanelType panel(Vinf,p0,T0,xfld,{0,1});

  const int nnode_a = panel.nNodeAirfoil();
  const int nelem_a = panel.nPanelAirfoil();
  const int nelem_w = panel.nPanelWake();

  const int nDOFgam = nnode_a;
  const int nDOFlam = nelem_a + nelem_w;

  // initialize solution fields
  std::vector<Real> gamData
  = { 1.5609788540532374e+00,
      1.5669515943085519e+00,
      1.5739877901845789e+00,
      1.5804751667217780e+00,
      1.5863939264165023e+00,
      1.5918310040777535e+00,
      1.5968833066139390e+00,
      1.6016367769793665e+00,
      1.6061675855735447e+00,
      1.6105398263774113e+00,
      1.6148036797841667e+00,
      1.6190034743564152e+00,
      1.6231669206308652e+00,
      1.6273278152749706e+00,
      1.6314941775821599e+00,
      1.6356882724945576e+00,
      1.6399085884976352e+00,
      1.6441617027050952e+00,
      1.6484429588251008e+00,
      1.6527477265301436e+00,
      1.6570597803468616e+00,
      1.6613638616887199e+00,
      1.6656435074020652e+00,
      1.6698753568980023e+00,
      1.6740352134717773e+00,
      1.6781042611693926e+00,
      1.6819945457086551e+00,
      1.6851200207242232e+00,
      1.6930075761811747e+00,
      1.6758274396214250e+00,
      1.7184789608741660e+00,
      1.6683613764241787e+00,
      1.2711890594379904e-06,
      -1.6683642442691113e+00,
      -1.7184748980054156e+00,
      -1.6758276635193743e+00,
      -1.6930076823018076e+00,
      -1.6851200517911757e+00,
      -1.6819947048914727e+00,
      -1.6781036639615765e+00,
      -1.6740356023046481e+00,
      -1.6698749960542330e+00,
      -1.6656433026177768e+00,
      -1.6613636873369289e+00,
      -1.6570595820583369e+00,
      -1.6527473642381447e+00,
      -1.6484432392891024e+00,
      -1.6441619803945151e+00,
      -1.6399082237372924e+00,
      -1.6356880491970411e+00,
      -1.6314938507685988e+00,
      -1.6273282672202254e+00,
      -1.6231666018317472e+00,
      -1.6190032890109052e+00,
      -1.6148034927297337e+00,
      -1.6105395022542701e+00,
      -1.6061680199858135e+00,
      -1.6016364350222352e+00,
      -1.5968830652391057e+00,
      -1.5918306338808248e+00,
      -1.5863942760506431e+00,
      -1.5804745853901385e+00,
      -1.5739880638302062e+00,
      -1.5669507310222861e+00,
      -1.5609788540532374e+00 };
  GamFieldType gamfld(xfld, PanelEquationClass::order_gam, BasisFunctionCategory_Hierarchical,{0});
  for (int i = 0; i < nDOFgam; ++i)
    gamfld.DOF(i) = gamData[i];

  Real Psi0 = 0;
  const Real Psi0true = -3.7853925369750979e-03;

  std::vector<Real> lamData
  = { 1.9925853255787680e-02,
      1.6866078013339867e-02,
      1.3538961018385027e-02,
      1.1450311391024088e-02,
      1.0006847954654525e-02,
      9.0126467141120273e-03,
      8.3350114659799020e-03,
      7.8809076403539534e-03,
      7.5891252567289302e-03,
      7.4185723091863623e-03,
      7.3383598093140229e-03,
      7.3290529999962160e-03,
      7.3698556976911443e-03,
      7.4604951823909601e-03,
      7.5813242881950273e-03,
      7.7390954696488004e-03,
      7.9264591083869265e-03,
      8.1418084560477832e-03,
      8.3919025691212047e-03,
      8.6825124283817657e-03,
      9.0223949111037371e-03,
      9.4247851467876061e-03,
      9.9196519189258189e-03,
      1.0545207365175112e-02,
      1.1377990599304303e-02,
      1.2570279966503641e-02,
      1.4432637334231795e-02,
      1.5282002786573304e-02,
      2.7823159884845034e-02,
      -2.8837896597530871e-03,
      1.3589482083233539e-01,
      5.1152285010037879e-02,
      5.1154676266718400e-02,
      1.3589007750574619e-01,
      -2.8820604459919232e-03,
      2.7823536216507922e-02,
      1.5282000441280244e-02,
      1.4432636407945209e-02,
      1.2569783585102374e-02,
      1.1378484035603560e-02,
      1.0544714312243863e-02,
      9.9196519189258189e-03,
      9.4247851467876061e-03,
      9.0223949111037371e-03,
      8.6825124283817657e-03,
      8.3923872333143360e-03,
      8.1418071976236857e-03,
      7.9259758617653449e-03,
      7.7390954696488004e-03,
      7.5813242881950273e-03,
      7.4609733280362224e-03,
      7.3693775896522861e-03,
      7.3290529999962160e-03,
      7.3383598093140229e-03,
      7.4185723091863623e-03,
      7.5895970651079355e-03,
      7.8804358372421817e-03,
      8.3350114659799020e-03,
      9.0126467141120273e-03,
      1.0007313959537384e-02,
      1.1449845372216187e-02,
      1.3539423493546542e-02,
      1.6865609652014956e-02,
      1.9926534713790790e-02,
      //
      -9.6582180676574425e-02,
      -8.7259795395324788e-02,
      -7.4481323665313917e-02,
      -5.9716892246788583e-02,
      -4.4740623567963239e-02,
      -3.1498295722374850e-02,
      -2.1104363335504490e-02,
      -1.3690072984931467e-02,
      -8.7251934837345654e-03,
      -5.5160644406839288e-03,
      -3.4605304526363805e-03,
      -2.1528262676120051e-03,
      -1.3320520469188469e-03,
      -8.5007476086140213e-04 };
  LamFieldType lamfld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  BOOST_CHECK( nDOFlam == lamfld.nDOF() );
  for ( int i = 0; i < nDOFlam; ++i )
  {
#if 1
    lamfld.DOF(i) = lamData[i];
#else
    lamfld.DOF(i) = 0.0;
#endif
  }

  // set up Qauxi field
  LamFieldType qauxifld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  BOOST_CHECK(lamfld.nDOF() == qauxifld.nDOF());
  for (int i = 0; i < nDOFlam; ++i)
    qauxifld.DOF(i) = lamData[i];

  // set up panel equation

  const Real tol_eqn = 1.e-12;
  PanelEquationClass Equation(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  NewtonSolver<SystemMatrixClass> Solver(Equation, NewtonSolverDict);

  SystemVectorClass ini(Equation.vectorStateSize()), sln(Equation.vectorStateSize());
  SystemVectorClass rsd(Equation.vectorEqSize());

  // check residuals of initial solution
  Equation.fillSystemVector(ini);
  Equation.residual(ini,rsd);

  const Real res_tol = 4e-10;

  for (int i = 0; i < nDOFgam; ++i)
    SANS_CHECK_CLOSE( Psi0true, rsd(PanelEquationClass::iPanel)[i], res_tol, res_tol );

  SANS_CHECK_CLOSE( 0.0, rsd(PanelEquationClass::iKutta)[0], res_tol, res_tol );

  for (int i = 0; i < nDOFlam; ++i)
    SANS_CHECK_CLOSE( 0.0, rsd(PanelEquationClass::iProj)[i], res_tol, res_tol );

#ifdef PANEL_DEBUG_MODE_FiniteTE_AirfoilWake_WithSource
  for (int k=0; k<rsd.m(); ++k)
  {
    for ( int i = 0; i < rsd(k).m(); ++i )
      cout << "rsd("<<k<<")[" << i << "] = " << setprecision(16) << rsd(k)[i] << endl;
  }

  // initial jacobian
  SystemNonZeroPattern nz(Equation.matrixSize());
  Equation.jacobian(ini, nz);

  SystemMatrixClass jac(nz);
  Equation.jacobian(ini, jac);

  fstream fout( "tmp/jac_XfoilPanel.mtx", fstream::out );
  cout << "Writing global Jacobian" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

  // solve
  SolveStatus status = Solver.solve(ini, sln);
  BOOST_CHECK( status.converged );

  Equation.setSolutionField(sln);

  /////////////////////////////
  // check if residual has converged
  /////////////////////////////
  Equation.residual(sln,rsd);

  for ( int i = 0; i < rsd.m(); ++i )
    for ( int j = 0; j < rsd(i).m(); ++j )
      BOOST_CHECK_SMALL( rsd(i)[j], res_tol );

  /////////////////////////////
  // check solution
  /////////////////////////////
  // print solution
#ifdef PANEL_DEBUG_MODE_FiniteTE_AirfoilWake_WithSource
  cout << "\n//-----print out solution-----//" << endl;
  for (int k=0; k<sln.m(); ++k)
  {
    for ( int i = 0; i < rsd(k).m(); ++i )
      cout << "sln("<<k<<")[" << i << "] = " << setprecision(16) << sln(k)[i] << endl;
  }

  cout << "\n//-----print out Ue-----//" << endl;
  for (int i = 0; i < xfld.nDOF(); ++i)
  {
    Real Ue = 0;
    panel.Uecalc(gamfld,lamfld,xfld.DOF(i),i,Ue);

    cout << setprecision(16) << Ue <<  endl;
    if (i == (nDOFgam-1))
      cout << endl;
  }
#endif

  // Pyrite check
  // check with MATLAB prototype results
  {
    const Real pyrite_tol = 3e-6;
    // This tolerance is loose because exact match may not be the case.
    // Though MATLAB prototype panel code uses prescribed velocity field, SANS panel code uses velocity field evaluated by itself.

    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_FiniteTE_NACA0004_AirfoilWake_WithSource_MATLAB_Test.txt";

    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < nDOFgam; ++i)
      pyriteFile << gamfld.DOF(i) << endl;

    pyriteFile << Psi0 << endl;

    for (int i = 0; i < lamfld.nDOF(); ++i)
      pyriteFile << lamfld.DOF(i) << endl;
  }
  // check with previous version of SANS panel code
  {
    const Real pyrite_tol = 6e-6;

    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_FiniteTE_NACA0004_AirfoilWake_WithSource_Test.txt";
    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < nDOFgam; ++i)
      pyriteFile << gamfld.DOF(i) << endl;

    pyriteFile << Psi0 << endl;

    for (int i = 0; i < lamfld.nDOF(); ++i)
      pyriteFile << lamfld.DOF(i) << endl;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SharpTE_Joukowski_Airfoil_WithSource_test )
{
#ifdef PANEL_DEBUG_MODE_SharpTE_Airfoil_WithSource
  cout << "//--------------------------------------------------------------------//" << endl;
  cout << "// SharpTE_Joukowski_Airfoil_WithSource" << endl;
  cout << "//--------------------------------------------------------------------//" << endl;
#endif

  // solver settings
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
#ifdef PANEL_DEBUG_MODE_SharpTE_Airfoil_WithSource
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
#else
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // set up grid
  string airfoilname = "joukowski";
  const int alpha = 0;
  const int nelem = 80;
  const Real qinf = 1.573869150000000; // [m/s] freestream speed

  string filename_airfoil = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem) + ".txt";
  std::vector<VectorX> coordinates_a;
  XField2D_Line_X1_1Group::readXFoilGrid(filename_airfoil, coordinates_a);
  XField2D_Line_X1_1Group xfld(coordinates_a);

  // set up panel
  const VectorX Vinf = { qinf*cos(static_cast<Real>(alpha)*PI/180.0),
                         qinf*sin(static_cast<Real>(alpha)*PI/180.0) }; // freestream velocity
  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  PanelType panel(Vinf,p0,T0,xfld,{0});

  const int nnode_a = panel.nNodeAirfoil();
  const int nelem_a = panel.nPanelAirfoil();

  const int nDOFgam = nnode_a;
  const int nDOFlam = nelem_a;

  // initialize solution fields
#if PanelSharpTETreatment_isXfoilPaper // 1989 XFOIL paper
  SANS_DEVELOPER_EXCEPTION("Not tested here");
#else // XFOIL 6.99
  std::vector<Real> gamData
  = { 1.5532594311170946e+00,
      1.5229206697114679e+00,
      1.5371578267919710e+00,
      1.5446926387292701e+00,
      1.5505500978953013e+00,
      1.5557383675498402e+00,
      1.5606197418644543e+00,
      1.5653533872826209e+00,
      1.5700229976954072e+00,
      1.5746580520712934e+00,
      1.5793024117661052e+00,
      1.5839485909598343e+00,
      1.5886211787315727e+00,
      1.5933123964920028e+00,
      1.5980350451684897e+00,
      1.6027788749312299e+00,
      1.6075485311037916e+00,
      1.6123443163845572e+00,
      1.6171631115764515e+00,
      1.6220021505636226e+00,
      1.6268661197627745e+00,
      1.6317464678212159e+00,
      1.6366425471216408e+00,
      1.6415534345463991e+00,
      1.6464720562413622e+00,
      1.6513998630408357e+00,
      1.6563311706109169e+00,
      1.6612568198154916e+00,
      1.6661728022795173e+00,
      1.6710687460145852e+00,
      1.6759266476674173e+00,
      1.6807294476511514e+00,
      1.6854401378655719e+00,
      1.6899884555131073e+00,
      1.6941268614027047e+00,
      1.6966717849248547e+00,
      1.7052960216035853e+00,
      1.6765921950593805e+00,
      1.6897928570996550e+00,
      1.5679977113165764e+00,
      -3.0925855340061121e-05,
      -1.5474510096918903e+00,
      -1.7106836297549848e+00,
      -1.6737404788921801e+00,
      -1.7031697025828427e+00,
      -1.6968436915349698e+00,
      -1.6940938497665194e+00,
      -1.6899457556866266e+00,
      -1.6854111816219817e+00,
      -1.6807131770629551e+00,
      -1.6759162968835821e+00,
      -1.6710628434046069e+00,
      -1.6661693098551378e+00,
      -1.6612570800220259e+00,
      -1.6563323357174184e+00,
      -1.6514013134725527e+00,
      -1.6464764354711854e+00,
      -1.6415555727642301e+00,
      -1.6366450241236918e+00,
      -1.6317494307871507e+00,
      -1.6268682955419695e+00,
      -1.6220086815380812e+00,
      -1.6171652109799297e+00,
      -1.6123466712603629e+00,
      -1.6075513019580290e+00,
      -1.6027816393291301e+00,
      -1.5980375988823843e+00,
      -1.5933143541969850e+00,
      -1.5886229194102475e+00,
      -1.5839504452611635e+00,
      -1.5793036755094803e+00,
      -1.5746589338704349e+00,
      -1.5700232486677603e+00,
      -1.5653544780983810e+00,
      -1.5606204340476140e+00,
      -1.5557381297600155e+00,
      -1.5505504979964631e+00,
      -1.5446931669414257e+00,
      -1.5371590063737897e+00,
      -1.5229237197579166e+00,
      -1.5532594311170946e+00 };
#endif
  GamFieldType gamfld(xfld, PanelEquationClass::order_gam, BasisFunctionCategory_Hierarchical,{0});
  for (int i = 0; i < nDOFgam; ++i)
    gamfld.DOF(i) = gamData[i];

  Real Psi0 = 0.0;
  const Real Psi0true = 2.4967369464696172e-06;

#if PanelSharpTETreatment_isXfoilPaper // 1989 XFOIL paper
  SANS_DEVELOPER_EXCEPTION("Not tested here");
#else // XFOIL 6.99
  std::vector<Real> lamData
  = { 1.5831961118898889e-02,
      4.5444459977781243e-03,
      5.5298868334201856e-03,
      5.9524439049989967e-03,
      6.4142481952655338e-03,
      6.8045807612932153e-03,
      7.1228331935443657e-03,
      7.3757287341913429e-03,
      7.5844008956213139e-03,
      7.7412595602691639e-03,
      7.8779724023407445e-03,
      7.9819725669056634e-03,
      8.0740699605804517e-03,
      8.1491802956872132e-03,
      8.2221433103891271e-03,
      8.2901463053885755e-03,
      8.3580585223619764e-03,
      8.4281260464778174e-03,
      8.5066690483013609e-03,
      8.5903208765670293e-03,
      8.6891017403391869e-03,
      8.8033683969721312e-03,
      8.9342337034905798e-03,
      9.0902839740492278e-03,
      9.2694255780511482e-03,
      9.4820991422426646e-03,
      9.7382039564654355e-03,
      1.0040343044545522e-02,
      1.0409697896079912e-02,
      1.0861237079205718e-02,
      1.1425615354533493e-02,
      1.2152195668517747e-02,
      1.3130965712379195e-02,
      1.4533822379055811e-02,
      1.6716578839779429e-02,
      1.6855759687682274e-02,
      3.3830902647732218e-02,
      6.3410331684628689e-03,
      1.2971324948992641e-01,
      5.6116035342138819e-02,
      4.5074336299415728e-02,
      1.5743182163359809e-01,
      -3.5032572710896725e-03,
      3.1851915749497549e-02,
      1.8113651952804226e-02,
      1.6761362654806797e-02,
      1.4587976720433443e-02,
      1.3173561303955170e-02,
      1.2190255068314691e-02,
      1.1454756957662836e-02,
      1.0885226773298754e-02,
      1.0429202301317365e-02,
      1.0057765507939253e-02,
      9.7514178092138835e-03,
      9.4927788654726904e-03,
      9.2796124310021953e-03,
      9.0957539414743514e-03,
      8.9398605332308805e-03,
      8.8083515371669089e-03,
      8.6928441070298519e-03,
      8.5959169801312647e-03,
      8.5060697857712881e-03,
      8.4293948045891156e-03,
      8.3593142360252146e-03,
      8.2907907699623015e-03,
      8.2221817003173998e-03,
      8.1486176672347782e-03,
      8.0734955805179445e-03,
      7.9813904621147888e-03,
      7.8767835856289724e-03,
      7.7400654776296597e-03,
      7.5831834954788301e-03,
      7.3754364924512525e-03,
      7.1218053026649870e-03,
      6.8032908091627499e-03,
      6.4140861611669412e-03,
      5.9522349609770634e-03,
      5.5300208065640821e-03,
      4.5444979180021610e-03,
      1.5828805280914199e-02 };
#endif
  LamFieldType lamfld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  BOOST_CHECK( nDOFlam == lamfld.nDOF() );
  for ( int i = 0; i < nDOFlam; ++i )
  {
#if 1
    lamfld.DOF(i) = lamData[i];
#else
    lamfld.DOF(i) = 0.0;
#endif
  }

  LamFieldType qauxifld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  BOOST_CHECK(lamfld.nDOF() == qauxifld.nDOF());
  for (int i = 0; i < nDOFlam; ++i)
    qauxifld.DOF(i) = lamData[i];

  // set up panel equation
  const Real tol_eqn = 1e-12;
  PanelEquationClass Equation(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  const int iProj = PanelEquationClass::iProj;
  const int iPanel = PanelEquationClass::iPanel;
  const int iKutta = PanelEquationClass::iKutta;

  NewtonSolver<SystemMatrixClass> Solver(Equation, NewtonSolverDict);

  SystemVectorClass ini(Equation.vectorStateSize()), sln(Equation.vectorStateSize());
  SystemVectorClass rsd(Equation.vectorEqSize());

  // check residuals of initial solution
  Equation.fillSystemVector(ini);
  Equation.residual(ini,rsd);

  const Real res_tol = 4.e-7;

  for (int i = 0; i < nDOFgam-1; ++i)
    SANS_CHECK_CLOSE( Psi0true, rsd(iPanel)[i], res_tol, res_tol );

  SANS_CHECK_CLOSE( 0, rsd(iPanel)[nDOFgam-1], res_tol, res_tol );

  SANS_CHECK_CLOSE( 0, rsd(iKutta)[0], res_tol, res_tol );

  for (int i = 0; i < nDOFlam; ++i)
    SANS_CHECK_CLOSE( 0, rsd(iProj)[i], res_tol, res_tol );

#ifdef PANEL_DEBUG_MODE_SharpTE_Airfoil_WithSource
  for (int k=0; k<rsd.m(); ++k)
  {
    for ( int i = 0; i < rsd(k).m(); ++i )
      cout << "rsd("<<k<<")[" << i << "] = " << setprecision(16) << rsd(k)[i] << endl;
  }

  // initial jacobian
  SystemNonZeroPattern nz(Equation.matrixSize());
  Equation.jacobian(ini, nz);

  SystemMatrixClass jac(nz);
  Equation.jacobian(ini, jac);

  fstream fout( "tmp/jac_XfoilPanel.mtx", fstream::out );
  cout << "Writing global Jacobian" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

  // solve
  SolveStatus status = Solver.solve(ini, sln);
  BOOST_CHECK( status.converged );

  Equation.setSolutionField(sln);

  /////////////////////////////
  // check if residual has converged
  /////////////////////////////
  Equation.residual(sln,rsd);

  for ( int i = 0; i < rsd.m(); ++i )
    for ( int j = 0; j < rsd(i).m(); ++j )
      BOOST_CHECK_SMALL( rsd(i)[j], res_tol );

  /////////////////////////////
  // check solution
  /////////////////////////////

  // print solution
#ifdef PANEL_DEBUG_MODE_SharpTE_Airfoil_WithSource
  cout << "\n//-----print out solution-----//" << endl;
  for (int k=0; k<sln.m(); ++k)
  {
    for ( int i = 0; i < rsd(k).m(); ++i )
      cout << "sln("<<k<<")[" << i << "] = " << setprecision(16) << sln(k)[i] << endl;
  }

  cout << "\n//-----print out Ue-----//" << endl;
  for (int i = 0; i < xfld.nDOF(); ++i)
  {
    Real Ue = 0;
    panel.Uecalc(gamfld,lamfld,xfld.DOF(i),i,Ue);

    cout << setprecision(16) << Ue <<  endl;
    if (i == (nDOFgam-1))
      cout << endl;
  }
#endif

  // Pyrite check
  // check with MATLAB prototype results
  {
    const Real pyrite_tol = 2e-6;
    // This tolerance is loose because exact match may not be the case.
    // Though MATLAB prototype panel code uses prescribed velocity field, SANS panel code uses velocity field evaluated by itself.

  #if PanelSharpTETreatment_isXfoilPaper
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_Airfoil_WithSource_XfoilPaper_MATLAB_Test.txt";
  #else
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_Airfoil_WithSource_XfoilV699_MATLAB_Test.txt";
  #endif

    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < nDOFgam; ++i)
      pyriteFile << gamfld.DOF(i) << endl;

    pyriteFile << Psi0 << endl;

    for (int i = 0; i < lamfld.nDOF(); ++i)
      pyriteFile << lamfld.DOF(i) << endl;
  }
  // check with previous version of SANS panel code
  {
    const Real pyrite_tol = 2e-6;

#if PanelSharpTETreatment_isXfoilPaper // 1989 XFOIL paper
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_Airfoil_WithSource_XfoilPaper_Test.txt";
#else // XFOIL 6.99
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_Airfoil_WithSource_XfoilV699_Test.txt";
#endif
    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < nDOFgam; ++i)
      pyriteFile << gamfld.DOF(i) << endl;

    pyriteFile << Psi0 << endl;

    for (int i = 0; i < lamfld.nDOF(); ++i)
      pyriteFile << lamfld.DOF(i) << endl;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( SharpTE_Joukowski_AirfoilWake_WithSource_test )
{
#ifdef PANEL_DEBUG_MODE_SharpTE_AirfoilWake_WithSource
  cout << "//--------------------------------------------------------------------//" << endl;
  cout << "// SharpTE_Joukowski_AirfoilWake_WithSource" << endl;
  cout << "//--------------------------------------------------------------------//" << endl;
#endif

  // solver settings
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
#ifdef PANEL_DEBUG_MODE_SharpTE_AirfoilWake_WithSource
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
#else
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // set up grid
  string airfoilname = "joukowski";
  int alpha = 0;
  int nelem = 80;
  const Real qinf = 1.573869150000000; // [m/s] freestream speed

  string filename_airfoil = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem) + ".txt";
  string filename_wake = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem)
                       + "_wake_a" + stringify(alpha) + ".txt";
  std::vector<VectorX> coordinates_a, coordinates_w;
  XField2D_Line_X1_1Group::readXFoilGrid(filename_airfoil, coordinates_a);
  XField2D_Line_X1_1Group::readXFoilGrid(filename_wake, coordinates_w);

  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);

  // set up panel
  const VectorX Vinf = { qinf*cos(static_cast<Real>(alpha)*PI/180.0),
                         qinf*sin(static_cast<Real>(alpha)*PI/180.0) }; // freestream velocity
  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  PanelType panel(Vinf,p0,T0,xfld,{0,1});

  const int nnode_a = panel.nNodeAirfoil();
  const int nelem_a = panel.nPanelAirfoil();
  const int nelem_w = panel.nPanelWake();

  const int nDOFgam = nnode_a;
  const int nDOFlam = nelem_a + nelem_w;

  // initialize solution fields
#if PanelSharpTETreatment_isXfoilPaper // 1989 XFOIL paper
  std::vector<Real> gamData
  = { 1.5501084084805878e+00,
      1.5529144830991988e+00,
      1.5557230179733432e+00,
      1.5588634161181438e+00,
      1.5621847660364443e+00,
      1.5656867833555990e+00,
      1.5693502354416116e+00,
      1.5731557997708989e+00,
      1.5770909653426171e+00,
      1.5811282178028148e+00,
      1.5852752566675610e+00,
      1.5895004104842425e+00,
      1.5938114326830271e+00,
      1.5981884347159683e+00,
      1.6026352702683393e+00,
      1.6071348638245107e+00,
      1.6116865968800351e+00,
      1.6162866391609207e+00,
      1.6209285604380568e+00,
      1.6256069385777199e+00,
      1.6303242789062926e+00,
      1.6350702280725236e+00,
      1.6398426410521758e+00,
      1.6446393293888046e+00,
      1.6494521526403958e+00,
      1.6542816457077620e+00,
      1.6591213395558020e+00,
      1.6639613983858255e+00,
      1.6687972155718980e+00,
      1.6736179085803329e+00,
      1.6784050045242722e+00,
      1.6831410382798013e+00,
      1.6877886252247745e+00,
      1.6922771525693172e+00,
      1.6963587266919482e+00,
      1.6988494033183958e+00,
      1.7074212461153231e+00,
      1.6786651262775178e+00,
      1.6917715086086196e+00,
      1.5699663313225438e+00,
      -3.0925855045029896e-05,
      -1.5494196296981801e+00,
      -1.7126622812637973e+00,
      -1.6758134101103896e+00,
      -1.7052949270945472e+00,
      -1.6990213099285274e+00,
      -1.6963257150557396e+00,
      -1.6922344527428375e+00,
      -1.6877596689811867e+00,
      -1.6831247676916141e+00,
      -1.6783946537404240e+00,
      -1.6736120059703545e+00,
      -1.6687937231475156e+00,
      -1.6639616585923620e+00,
      -1.6591225046622886e+00,
      -1.6542830961394868e+00,
      -1.6494565318702217e+00,
      -1.6446414676066388e+00,
      -1.6398451180542255e+00,
      -1.6350731910384608e+00,
      -1.6303264546854790e+00,
      -1.6256134695521736e+00,
      -1.6209306598415356e+00,
      -1.6162889940367247e+00,
      -1.6116893677342801e+00,
      -1.6071376282223906e+00,
      -1.6026378239822465e+00,
      -1.5981903924209460e+00,
      -1.5938131733617089e+00,
      -1.5895022647855597e+00,
      -1.5852765204109438e+00,
      -1.5811290996019525e+00,
      -1.5770912163149673e+00,
      -1.5731568905866549e+00,
      -1.5693509276247721e+00,
      -1.5656865455657698e+00,
      -1.5621851661376076e+00,
      -1.5588639443303023e+00,
      -1.5557241975551575e+00,
      -1.5529175331456393e+00,
      -1.5501084084805876e+00 };
#else // XFOIL 6.99
  std::vector<Real> gamData
  = { 1.5495467653289672e+00,
      1.5529196814807800e+00,
      1.5557229747651131e+00,
      1.5588634346573023e+00,
      1.5621847740489956e+00,
      1.5656867882449921e+00,
      1.5693502386951339e+00,
      1.5731558020919250e+00,
      1.5770909670807152e+00,
      1.5811282191526626e+00,
      1.5852752577459479e+00,
      1.5895004113654438e+00,
      1.5938114334165163e+00,
      1.5981884353359745e+00,
      1.6026352707992859e+00,
      1.6071348642842800e+00,
      1.6116865972820462e+00,
      1.6162866395154014e+00,
      1.6209285607529771e+00,
      1.6256069388593226e+00,
      1.6303242791596284e+00,
      1.6350702283016210e+00,
      1.6398426412603702e+00,
      1.6446393295788193e+00,
      1.6494521528145216e+00,
      1.6542816458679084e+00,
      1.6591213397035851e+00,
      1.6639613985226251e+00,
      1.6687972156988997e+00,
      1.6736179086985439e+00,
      1.6784050046345869e+00,
      1.6831410383829695e+00,
      1.6877886253214796e+00,
      1.6922771526601421e+00,
      1.6963587267774149e+00,
      1.6988494033989610e+00,
      1.7074212461913929e+00,
      1.6786651263495729e+00,
      1.6917715086763347e+00,
      1.5699663313894638e+00,
      -3.0925855062026276e-05,
      -1.5494196297650960e+00,
      -1.7126622813315147e+00,
      -1.6758134101824471e+00,
      -1.7052949271706173e+00,
      -1.6990213100090961e+00,
      -1.6963257151412048e+00,
      -1.6922344528336595e+00,
      -1.6877596690778980e+00,
      -1.6831247677947829e+00,
      -1.6783946538507382e+00,
      -1.6736120060885651e+00,
      -1.6687937232745211e+00,
      -1.6639616587291561e+00,
      -1.6591225048100795e+00,
      -1.6542830962996251e+00,
      -1.6494565320443590e+00,
      -1.6446414677966479e+00,
      -1.6398451182624223e+00,
      -1.6350731912675620e+00,
      -1.6303264549388097e+00,
      -1.6256134698337854e+00,
      -1.6209306601564495e+00,
      -1.6162889943912055e+00,
      -1.6116893681362923e+00,
      -1.6071376286821670e+00,
      -1.6026378245131869e+00,
      -1.5981903930409584e+00,
      -1.5938131740951935e+00,
      -1.5895022656667692e+00,
      -1.5852765214893250e+00,
      -1.5811291009517974e+00,
      -1.5770912180530650e+00,
      -1.5731568929076853e+00,
      -1.5693509308782925e+00,
      -1.5656865504551654e+00,
      -1.5621851741501656e+00,
      -1.5588639628694509e+00,
      -1.5557241543469216e+00,
      -1.5529227315272343e+00,
      -1.5495467653289672e+00 };
#endif
  GamFieldType gamfld(xfld, PanelEquationClass::order_gam, BasisFunctionCategory_Hierarchical,{0});
  for (int i = 0; i < nDOFgam; ++i)
    gamfld.DOF(i) = gamData[i];

  Real Psi0 = 0;
  const Real Psi0true = -3.4737813342387304e-03;

#if PanelSharpTETreatment_isXfoilPaper // 1989 XFOIL paper
  std::vector<Real> lamData
  = { 1.5831961118898889e-02,
      4.5444459977781243e-03,
      5.5298868334201856e-03,
      5.9524439049989967e-03,
      6.4142481952655338e-03,
      6.8045807612932153e-03,
      7.1228331935443657e-03,
      7.3757287341913429e-03,
      7.5844008956213139e-03,
      7.7412595602691639e-03,
      7.8779724023407445e-03,
      7.9819725669056634e-03,
      8.0740699605804517e-03,
      8.1491802956872132e-03,
      8.2221433103891271e-03,
      8.2901463053885755e-03,
      8.3580585223619764e-03,
      8.4281260464778174e-03,
      8.5066690483013609e-03,
      8.5903208765670293e-03,
      8.6891017403391869e-03,
      8.8033683969721312e-03,
      8.9342337034905798e-03,
      9.0902839740492278e-03,
      9.2694255780511482e-03,
      9.4820991422426646e-03,
      9.7382039564654355e-03,
      1.0040343044545522e-02,
      1.0409697896079912e-02,
      1.0861237079205718e-02,
      1.1425615354533493e-02,
      1.2152195668517747e-02,
      1.3130965712379195e-02,
      1.4533822379055811e-02,
      1.6716578839779429e-02,
      1.6855759687682274e-02,
      3.3830902647732218e-02,
      6.3410331684628689e-03,
      1.2971324948992641e-01,
      5.6116035342138819e-02,
      4.5074336299415728e-02,
      1.5743182163359809e-01,
      -3.5032572710896725e-03,
      3.1851915749497549e-02,
      1.8113651952804226e-02,
      1.6761362654806797e-02,
      1.4587976720433443e-02,
      1.3173561303955170e-02,
      1.2190255068314691e-02,
      1.1454756957662836e-02,
      1.0885226773298754e-02,
      1.0429202301317365e-02,
      1.0057765507939253e-02,
      9.7514178092138835e-03,
      9.4927788654726904e-03,
      9.2796124310021953e-03,
      9.0957539414743514e-03,
      8.9398605332308805e-03,
      8.8083515371669089e-03,
      8.6928441070298519e-03,
      8.5959169801312647e-03,
      8.5060697857712881e-03,
      8.4293948045891156e-03,
      8.3593142360252146e-03,
      8.2907907699623015e-03,
      8.2221817003173998e-03,
      8.1486176672347782e-03,
      8.0734955805179445e-03,
      7.9813904621147888e-03,
      7.8767835856289724e-03,
      7.7400654776296597e-03,
      7.5831834954788301e-03,
      7.3754364924512525e-03,
      7.1218053026649870e-03,
      6.8032908091627499e-03,
      6.4140861611669412e-03,
      5.9522349609770634e-03,
      5.5300208065640821e-03,
      4.5444979180021610e-03,
      1.5828805280914199e-02,
      //
      -1.3540612306000899e-01,
      -6.5493074402366042e-02,
      -5.3473332872447980e-02,
      -4.4786818691650264e-02,
      -3.7604879799832798e-02,
      -3.0735116691467758e-02,
      -2.4047280114131677e-02,
      -1.7883282060791802e-02,
      -1.2669910054149193e-02,
      -8.6390781233074629e-03,
      -5.7328964143327724e-03,
      -3.7290967143042116e-03,
      -2.3833019582951177e-03,
      -1.4999064777804578e-03,
      -9.6432365721973556e-04 };
#else // XFOIL 6.99
  std::vector<Real> lamData
  = { 1.5831961118898889e-02,
      4.5444459977781243e-03,
      5.5298868334201856e-03,
      5.9524439049989967e-03,
      6.4142481952655338e-03,
      6.8045807612932153e-03,
      7.1228331935443657e-03,
      7.3757287341913429e-03,
      7.5844008956213139e-03,
      7.7412595602691639e-03,
      7.8779724023407445e-03,
      7.9819725669056634e-03,
      8.0740699605804517e-03,
      8.1491802956872132e-03,
      8.2221433103891271e-03,
      8.2901463053885755e-03,
      8.3580585223619764e-03,
      8.4281260464778174e-03,
      8.5066690483013609e-03,
      8.5903208765670293e-03,
      8.6891017403391869e-03,
      8.8033683969721312e-03,
      8.9342337034905798e-03,
      9.0902839740492278e-03,
      9.2694255780511482e-03,
      9.4820991422426646e-03,
      9.7382039564654355e-03,
      1.0040343044545522e-02,
      1.0409697896079912e-02,
      1.0861237079205718e-02,
      1.1425615354533493e-02,
      1.2152195668517747e-02,
      1.3130965712379195e-02,
      1.4533822379055811e-02,
      1.6716578839779429e-02,
      1.6855759687682274e-02,
      3.3830902647732218e-02,
      6.3410331684628689e-03,
      1.2971324948992641e-01,
      5.6116035342138819e-02,
      4.5074336299415728e-02,
      1.5743182163359809e-01,
      -3.5032572710896725e-03,
      3.1851915749497549e-02,
      1.8113651952804226e-02,
      1.6761362654806797e-02,
      1.4587976720433443e-02,
      1.3173561303955170e-02,
      1.2190255068314691e-02,
      1.1454756957662836e-02,
      1.0885226773298754e-02,
      1.0429202301317365e-02,
      1.0057765507939253e-02,
      9.7514178092138835e-03,
      9.4927788654726904e-03,
      9.2796124310021953e-03,
      9.0957539414743514e-03,
      8.9398605332308805e-03,
      8.8083515371669089e-03,
      8.6928441070298519e-03,
      8.5959169801312647e-03,
      8.5060697857712881e-03,
      8.4293948045891156e-03,
      8.3593142360252146e-03,
      8.2907907699623015e-03,
      8.2221817003173998e-03,
      8.1486176672347782e-03,
      8.0734955805179445e-03,
      7.9813904621147888e-03,
      7.8767835856289724e-03,
      7.7400654776296597e-03,
      7.5831834954788301e-03,
      7.3754364924512525e-03,
      7.1218053026649870e-03,
      6.8032908091627499e-03,
      6.4140861611669412e-03,
      5.9522349609770634e-03,
      5.5300208065640821e-03,
      4.5444979180021610e-03,
      1.5828805280914199e-02,
      //
      -1.3540612306000899e-01,
      -6.5493074402366042e-02,
      -5.3473332872447980e-02,
      -4.4786818691650264e-02,
      -3.7604879799832798e-02,
      -3.0735116691467758e-02,
      -2.4047280114131677e-02,
      -1.7883282060791802e-02,
      -1.2669910054149193e-02,
      -8.6390781233074629e-03,
      -5.7328964143327724e-03,
      -3.7290967143042116e-03,
      -2.3833019582951177e-03,
      -1.4999064777804578e-03,
      -9.6432365721973556e-04 };
#endif
  LamFieldType lamfld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  BOOST_CHECK(nDOFlam == lamfld.nDOF());
  for ( int i = 0; i < nDOFlam; ++i )
  {
#if 1
    lamfld.DOF(i) = lamData[i];
#else
    lamfld.DOF(i) = 0.0;
#endif
  }

  LamFieldType qauxifld(xfld, PanelEquationClass::order_lam, BasisFunctionCategory_Legendre);
  BOOST_CHECK(lamfld.nDOF() == qauxifld.nDOF());
  for (int i = 0; i < nDOFlam; ++i)
    qauxifld.DOF(i) = lamData[i];

  // set up panel equation
  const Real tol_eqn = 1e-12;

  PanelEquationClass Equation(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  NewtonSolver<SystemMatrixClass> Solver(Equation, NewtonSolverDict);

  SystemVectorClass ini(Equation.vectorStateSize()), sln(Equation.vectorStateSize());
  SystemVectorClass rsd(Equation.vectorEqSize());

  // check residuals of initial solution
  Equation.fillSystemVector(ini);
  Equation.residual(ini,rsd);

  const Real res_tol = 3e-10;

  for (int i = 0; i < nDOFgam-1; ++i)
    SANS_CHECK_CLOSE( Psi0true, rsd(PanelEquationClass::iPanel)[i], res_tol, res_tol );

  SANS_CHECK_CLOSE( 0.0, rsd(PanelEquationClass::iPanel)[nDOFgam-1], res_tol, res_tol );

  SANS_CHECK_CLOSE( 0.0, rsd(PanelEquationClass::iKutta)[0], res_tol, res_tol );

  for (int i = 0; i < nDOFlam; ++i)
    SANS_CHECK_CLOSE( 0.0, rsd(PanelEquationClass::iProj)[i], res_tol, res_tol );

#ifdef PANEL_DEBUG_MODE_SharpTE_AirfoilWake_WithSource
  for (int k=0; k<rsd.m(); ++k)
  {
    for ( int i = 0; i < rsd(k).m(); ++i )
      cout << "rsd("<<k<<")[" << i << "] = " << setprecision(16) << rsd(k)[i] << endl;
  }

  // initial jacobian
  SystemNonZeroPattern nz(Equation.matrixSize());
  Equation.jacobian(ini, nz);

  SystemMatrixClass jac(nz);
  Equation.jacobian(ini, jac);

  fstream fout( "tmp/jac_XfoilPanel.mtx", fstream::out );
  cout << "Writing global Jacobian" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

  // solve
  SolveStatus status = Solver.solve(ini, sln);
  BOOST_CHECK( status.converged );

  Equation.setSolutionField(sln);

  /////////////////////////////
  // check if residual has converged
  /////////////////////////////
  Equation.residual(sln,rsd);

  for ( int i = 0; i < rsd.m(); ++i )
    for ( int j = 0; j < rsd(i).m(); ++j )
      BOOST_CHECK_SMALL( rsd(i)[j], res_tol );

  /////////////////////////////
  // check solution
  /////////////////////////////

  // print solution
#ifdef PANEL_DEBUG_MODE_SharpTE_AirfoilWake_WithSource
  cout << "\n//-----print out solution-----//" << endl;
  for (int k=0; k<sln.m(); ++k)
  {
    for ( int i = 0; i < rsd(k).m(); ++i )
      cout << "sln("<<k<<")[" << i << "] = " << setprecision(16) << sln(k)[i] << endl;
  }

  cout << "\n//-----print out Ue-----//" << endl;
  for (int i = 0; i < xfld.nDOF(); ++i)
  {
    Real Ue = 0;
    panel.Uecalc(gamfld,lamfld,xfld.DOF(i),i,Ue);

    cout << setprecision(16) << Ue <<  endl;
    if (i == (nDOFgam-1))
      cout << endl;
  }
#endif

  // Pyrite check
  // check with MATLAB prototype results
  {
    const Real pyrite_tol = 3e-6;
    // This tolerance is loose because exact match may not be the case.
    // Though MATLAB prototype panel code uses prescribed velocity field, SANS panel code uses velocity field evaluated by itself.

  #if PanelSharpTETreatment_isXfoilPaper
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_AirfoilWake_WithSource_XfoilPaper_MATLAB_Test.txt";
  #else
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_AirfoilWake_WithSource_XfoilV699_MATLAB_Test.txt";
  #endif

    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < nDOFgam; ++i)
      pyriteFile << gamfld.DOF(i) << endl;

    pyriteFile << Psi0 << endl;

    for (int i = 0; i < lamfld.nDOF(); ++i)
      pyriteFile << lamfld.DOF(i) << endl;
  }
  // check with previous version of SANS panel code
  {
    const Real pyrite_tol = 2e-6;

#if PanelSharpTETreatment_isXfoilPaper // 1989 XFOIL paper
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_AirfoilWake_WithSource_XfoilPaper_Test.txt";
#else // XFOIL 6.99
    string pyriteFilename = "IO/CodeConsistency/Solve2D_XfoilLikePanelMethod_SharpTE_Joukowski_AirfoilWake_WithSource_XfoilV699_Test.txt";
#endif
    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < nDOFgam; ++i)
      pyriteFile << gamfld.DOF(i) << endl;

    pyriteFile << Psi0 << endl;

    for (int i = 0; i < lamfld.nDOF(); ++i)
      pyriteFile << lamfld.DOF(i) << endl;
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
