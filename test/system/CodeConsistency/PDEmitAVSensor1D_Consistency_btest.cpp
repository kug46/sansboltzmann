// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEmitAVSensor1D_Consistency_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEmitAVSensor1D_Consistency_btest )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> ParamType;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 4 );

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, AVPDEClass::Euler_ResidInterp_Raw);
  PDEBaseClass pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEBaseClass::Euler_ResidInterp_Raw);

  // function tests

  Real x, time;
  Real rho, u, t;

  x = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;

  ParamType H = {{0.5}};
  ParamType paramL = log(H);

  Real s = 0.35;

  // set
  ArrayQ q = 0;
  AVVariable<DensityVelocityTemperature1D, Real> var( DensityVelocityTemperature1D<Real>(rho, u, t), s );
  avpde.setDOFFrom( q, var );

  // master state
  ArrayQ uTrue = 0;
  ArrayQ uCons = 0;
  avpde.masterState( paramL, x, time, q, uCons );
  pde.masterState( paramL, x, time, q, uTrue );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol );

  // conservative flux
  ArrayQ ftTrue = 0;
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( paramL, x, time, q, ft );
  pde.fluxAdvectiveTime( paramL, x, time, q, ftTrue );
  SANS_CHECK_CLOSE( ftTrue(0), ft(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(1), ft(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( ftTrue(2), ft(2), small_tol, close_tol );

  // advective flux
  ArrayQ fTrue = 0;
  ArrayQ f = 0;

  avpde.fluxAdvective( paramL, x, time, q, f );
  pde.fluxAdvective( paramL, x, time, q, fTrue );

  SANS_CHECK_CLOSE( fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(2), f(2), small_tol, close_tol );

  ArrayQ qx = {0.31, -0.21, 0.67, 0.14};

  //Make H really small so that the artificial viscosity terms in Euler go away.
  //We only want to test the sensor equation here. Artificial viscosity is tested elsewhere.
  H = {{1e-20}};
  paramL = log(H);
  ParamType paramR = 0.5*log(H);

  MatrixQ kxx = 0;
  BOOST_CHECK_THROW( avpde.diffusionViscous( paramL, x, time, q, qx, kxx ) , DeveloperException);


  f = 0;
  fTrue = 0;
  avpde.fluxViscous( paramL, x, time, q, qx, f );
  pde.fluxViscous( paramL, x, time, q, qx, fTrue );

  SANS_CHECK_CLOSE( fTrue(0), f(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(1), f(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(2), f(2), small_tol, close_tol );

  Real nx = 0.3674;
  ArrayQ fn = 0.0;
  fTrue = 0;
  avpde.fluxViscous( paramL, paramR, x, time, q, qx, q, qx, nx, fn );
  pde.fluxViscous( paramL, paramR, x, time, q, qx, q, qx, nx, fTrue );

  SANS_CHECK_CLOSE( fTrue(0), fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(1), fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( fTrue(2), fn(2), small_tol, close_tol );

  ArrayQ src = 0;
  ArrayQ srcTrue = 0;
  avpde.source( paramL, x, time, q, qx, src );
  pde.source( paramL, x, time, q, qx, srcTrue );

  SANS_CHECK_CLOSE( src(0), srcTrue(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( src(1), srcTrue(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( src(2), srcTrue(2), small_tol, close_tol );

  MatrixQ dsdu = 0;
  MatrixQ dsduTrue = 0;
  avpde.jacobianSource( paramL, x, time, q, qx, dsdu );
  pde.jacobianSource( paramL, x, time, q, qx, dsduTrue );

  for (int i = 0; i < N-1; i++)
    for (int j = 0; j < N-1; j++)
      SANS_CHECK_CLOSE( dsdu(i,j), dsduTrue(i,j), small_tol, close_tol );

  ArrayQ srcL = 0, srcR = 0;
  avpde.sourceTrace( paramL, x, paramL, x, time,
                     q, qx, q, qx, srcL, srcR );

  SANS_CHECK_CLOSE( srcL(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcL(3), as*s, small_tol, close_tol );

  SANS_CHECK_CLOSE( srcR(0), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(1), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(2), 0.0, small_tol, close_tol );
  SANS_CHECK_CLOSE( srcR(3), as*s, small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( AVEuler1D_PrimitiveStrongFlux )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> ParamType;
  const int N = AVPDEClass::N;


  const Real tol = 1.e-12;
  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, AVPDEClass::Euler_ResidInterp_Raw);
  PDEBaseClass pde(order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEBaseClass::Euler_ResidInterp_Raw);

  // function tests

  Real x, time;
  Real rho, u, t;

  x = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;

  ParamType Htensor = {{0.5}};
  ParamType paramL = log(Htensor);

  Real s = 0.35;

  // set
  ArrayQ q = 0;
  DensityVelocityTemperature1D<Real> var(rho, u, t);
  AVVariable<DensityVelocityTemperature1D, Real> qdata( var, s );
  avpde.setDOFFrom( q, qdata );


  // Strong Conservative Flux
  Real rhot = -0.9;
  Real ut = -0.25;
  Real pt = 0.23;
  Real st = 0.49;

  ArrayQ qt = {rhot, ut, pt, st};
  ArrayQ utCons, utConsTrue;
  utCons = 0;
  utConsTrue = 0;
  avpde.strongFluxAdvectiveTime( paramL, x, time, q, qt, utCons );
  pde.strongFluxAdvectiveTime( paramL, x, time, q, qt, utConsTrue );
  BOOST_CHECK_CLOSE( utConsTrue(0), utCons(0), tol );
  BOOST_CHECK_CLOSE( utConsTrue(1), utCons(1), tol );
  BOOST_CHECK_CLOSE( utConsTrue(2), utCons(2), tol );

  // Strong Advective Flux
  Real rhox = -0.02;
  Real ux = 0.072;
  Real px = 10.2;
  Real sx = 2.37;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, px, sx};

  ArrayQ strongAdv = 0;
  ArrayQ strongAdvTrue = 0;

  avpde.strongFluxAdvective( paramL, x, time, q, qx, strongAdv );
  pde.strongFluxAdvective( paramL, x, time, q, qx, strongAdvTrue );
  BOOST_CHECK_CLOSE( strongAdvTrue(0), strongAdv(0), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(1), strongAdv(1), tol );
  BOOST_CHECK_CLOSE( strongAdvTrue(2), strongAdv(2), tol );


  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;
  avpde.jacobianMasterState(paramL, x, time, q, dudq);
  pde.jacobianMasterState(paramL, x, time, q, dudqTrue);

  for (int i = 0; i < N-1; i++)
    for (int j = 0; j < N-1; j++)
      SANS_CHECK_CLOSE( dudq(i,j), dudqTrue(i,j), small_tol, close_tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> ParamType;

  const int N = AVPDEClass::N;
  BOOST_REQUIRE( N == 4 );

  const Real tol = 2.e-11;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion,
                   EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Raw);
  PDEBaseClass pde(order, hasSpaceTimeDiffusion,
                   EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, time;
  Real nx;
  Real rhoL, uL, tL;
  Real rhoR, uR, tR;

  x = time = 0;   // not actually used in functions
  nx = 1.0;
  rhoL = 1.034; uL = 3.26; tL = 5.78;
  rhoR = 0.973; uR = 1.79; tR = 6.13;

  ParamType H = {{0.5}};
  ParamType paramL = log(H);

  Real sL = 0.35;
  Real sR = 0.41;

  // set
  AVVariable<DensityVelocityTemperature1D, Real> qdataL = {{rhoL, uL, tL}, sL};
  AVVariable<DensityVelocityTemperature1D, Real> qdataR = {{rhoR, uR, tR}, sR};
  ArrayQ qL = avpde.setDOFFrom( qdataL );
  ArrayQ qR = avpde.setDOFFrom( qdataR );

  ArrayQ fn = 0;
  ArrayQ fnTrue = 0;
  avpde.fluxAdvectiveUpwind( paramL, x, time, qL, qR, nx, fn );
  pde.fluxAdvectiveUpwind( paramL, x, time, qL, qR, nx, fnTrue );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );

  MatrixQ mtx;
  mtx = 0;
  BOOST_CHECK_THROW( avpde.jacobianFluxAdvectiveAbsoluteValue(paramL, x, time, qL, nx, mtx ), DeveloperException);

  Real rhox = -0.02;
  Real ux = 0.072;
  Real px = 10.2;
  Real sx = 2.37;

  //PRIMITIVE SPECIFIC
  ArrayQ qx = {rhox, ux, px, sx};

  ArrayQ srcL = 0, srcR = 0;
  ArrayQ srcLTrue = 0, srcRTrue = 0;
  avpde.sourceTrace( paramL, x, paramL, x, time,
                     qL, qx, qR, qx, srcL, srcR );
  pde.sourceTrace( paramL, x, paramL, x, time,
                     qL, qx, qR, qx, srcLTrue, srcRTrue );

  BOOST_CHECK_CLOSE( srcL(0), srcLTrue(0), tol );
  BOOST_CHECK_CLOSE( srcL(1), srcLTrue(1), tol );
  BOOST_CHECK_CLOSE( srcL(2), srcLTrue(2), tol );

  BOOST_CHECK_CLOSE( srcR(0), srcRTrue(0), tol );
  BOOST_CHECK_CLOSE( srcR(1), srcRTrue(1), tol );
  BOOST_CHECK_CLOSE( srcR(2), srcRTrue(2), tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD1::D,Real> ParamType;

  const Real tol = 1.e-13;

  Real us = 1.0;
  Real ksxx = 0.8;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us);
  SensorViscousFlux visc(ksxx);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  AVPDEClass avpde(adv, visc, source, isSteady, order, hasSpaceTimeDiffusion,
                   EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Raw);
  PDEBaseClass pde(order, hasSpaceTimeDiffusion,
                   EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, AVPDEClass::Euler_ResidInterp_Raw);

  Real x, time, dx;
  Real rho, u, t;
  Real speed, speedTrue;

  x = time = 0;   // not actually used in functions
  dx = 0.57;
  rho = 1.137; u = 0.784; t = 0.987;

  ParamType H = {{0.5}};
  ParamType paramL = log(H);

  Real s = 0.35;

  AVVariable<DensityVelocityTemperature1D, Real> qdata = {{rho, u, t}, s};
  ArrayQ q = avpde.setDOFFrom( qdata );

  avpde.speedCharacteristic( paramL, x, time, dx, q, speed );
  pde.speedCharacteristic( paramL, x, time, dx, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( paramL, x, time, q, speed );
  pde.speedCharacteristic( paramL, x, time, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
