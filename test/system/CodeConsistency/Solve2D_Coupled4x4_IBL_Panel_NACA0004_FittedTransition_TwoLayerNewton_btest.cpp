// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//#define IBL2D_DUMP
//#define SANS_VERBOSE

//----------------------------------------------------------------------------//
// testing coupled solution of block 4x4 system of IBL and Xfoil-like panel method on airfoil and wake
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include <limits>

#include <boost/mpl/vector_c.hpp>

#define JACOBIANPARAM_INSTANTIATE
#include "Discretization/JacobianParam_impl.h"

#define ALGEBRAICEQUATIONSET_BLOCK4X4_INSTANTIATE // HACK : need proper instantiations
#include "Discretization/Block/AlgebraicEquationSet_Block4x4_impl.h"

#include "Discretization/Block/JacobianParam_Decoupled.h"

#include "Discretization/DG/AlgebraicEquationSet_TransitionIBL.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_HubTrace_Galerkin_cutCellTransitionIBL.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_DG_HubTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_InteriorTraceGroup_Cell.h"
#include "Field/output_Tecplot.h"

#define SCALARVECTOR_INSTANTIATE
#include "LinearAlgebra/SparseLinAlg/ScalarVector_impl.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/WritePlainVector.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "PanelMethod/AlgebraicEquationSet_ProjectToQauxi.h"
#include "PanelMethod/JacobianParam_PanelEqn_Qauxi.h"
#include "PanelMethod/ProjectionToQauxi_IBL.h"

#include "PanelMethod/XfoilPanel.h"
#include "PanelMethod/XfoilPanelEquationSet.h"

#include "pde/IBL/PDEIBL2D.h"
#include "pde/IBL/BCIBL2D.h"
#include "pde/IBL/SetSolnDofCell_IBL2D.h"
#include "pde/IBL/SetVelDofCell_IBL2D.h"
#include "pde/IBL/SetIBLoutputCellGroup.h"
#include "pde/IBL/SetIBLtransitionFrontCellGroup.h"
#include "pde/IBL/SetTransitionFrontIBL2D_InteriorTraceGroup.h"

#include "pde/IBL/AlgebraicEquationSet_ProjectToQauxv_PanelMethod.h"
#include "pde/IBL/JacobianParam_ProjectToQauxv_Qinv.h"
#include "pde/IBL/JacobianParam_ProjectToQauxi_QIBL.h"
#include "pde/IBL/JacobianParam_ProjectToQauxi_Qauxv.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "tools/timer.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_X1_2Group_AirfoilWithWake.h"

using namespace std;
using namespace SANS::DLA;
using namespace SANS::SLA;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef TopoD1 TopoDim;

typedef AlgEqSetTraits_Sparse AESTraitsTag;

/////////////////////////////////////////////////////////////////////
// Panel method
typedef XfoilPanel PanelMethodType;
typedef XfoilPanelEquationSet<PanelMethodType,AESTraitsTag> PanelMethodEquationType;

typedef typename PanelMethodEquationType::GamFieldType<Real> GamFieldType;
typedef typename PanelMethodEquationType::LamFieldType<Real> LamFieldType;

/////////////////////////////////////////////////////////////////////
// IBL
typedef VarTypeDANCt VarType;
typedef VarData2DDANCt VarDataType;

typedef BCIBL2DVector<VarType> BCVectorClass;

typedef PDEIBL<PhysDim,VarType> PDEClassIBL;
typedef PDENDConvertSpace<PhysDim, PDEClassIBL> NDPDEClassIBL;

typedef typename PDEClassIBL::ThicknessesCoefficientsType::ClassParamsType ThicknessesCoefficientsParamsType;

typedef NDPDEClassIBL::template ArrayQ<Real> ArrayQIBL;
typedef PDEClassIBL::template ArrayParam<Real> ArrayQauxv;
typedef NDPDEClassIBL::VectorX VectorX;

typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQauxv>,
                                       XField<PhysDim, TopoDim> >::type TupleFieldIBLParamType;

typedef AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<
          AlgebraicEquationSet_TransitionIBL<NDPDEClassIBL,BCNDConvertSpace,BCVectorClass,
                                             AESTraitsTag,DGAdv_manifold,TupleFieldIBLParamType> > IBLEquationType;

typedef IBLEquationType::BCParams BCParamsIBL;
typedef BCIBL2D<BCTypeWakeMatch,VarType> BCWakeMatchClass;

/////////////////////////////////////////////////////////////////////
// Auxiliary equations
typedef FunctionEvalQauxv<PanelMethodType,Real,Real> FcnEvalQauxvType;
typedef IntegrandCell_ProjectFunction<FcnEvalQauxvType,IntegrandCell_ProjFcn_detail::FcnSref> IntegrandCellAuxvType;
typedef AlgebraicEquationSet_ProjectToQauxv_PanelMethod<
          XField<PhysDim,TopoDim>, IntegrandCellAuxvType, TopoDim, AESTraitsTag, PanelMethodEquationType> AuxvEquationType;

// TODO: note that the ordering of QIBL and Qauxv need to be consistent with that defined in ProjectionToQauxi
typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQIBL>,
                                       Field<PhysDim, TopoDim, ArrayQauxv>,
                                       XField<PhysDim, TopoDim> >::type TupleFieldAuxiParamType;

typedef typename AESProjToQauxi_detail::IntegrandCellProjNull IntegrandAuxiNullCellType;
typedef AlgebraicEquationSet_ProjectToQauxi<TupleFieldAuxiParamType, IntegrandAuxiNullCellType, TopoDim, AESTraitsTag,
                                            IBLEquationType, AuxvEquationType> AuxiEquationType;

/////////////////////////////////////////////////////////////////////
// Block 4x4
// diagonal block equation sets
typedef AuxvEquationType::TraitsType TraitsTypeBlock0;
typedef AuxiEquationType::TraitsType TraitsTypeBlock1;
typedef IBLEquationType::TraitsType TraitsTypeBlock2;
typedef PanelMethodEquationType::TraitsType TraitsTypeBlock3;

// primary unknown size
static const int MQ0 = DLA::VectorSize<typename TraitsTypeBlock0::ArrayQ>::M;
static const int MQ1 = DLA::VectorSize<typename TraitsTypeBlock1::ArrayQ>::M;
static const int MQ2 = DLA::VectorSize<typename TraitsTypeBlock2::ArrayQ>::M;
static const int MQ3 = DLA::VectorSize<typename TraitsTypeBlock3::ArrayQ>::M;

typedef typename TraitsTypeBlock0::MatrixQ        MatrixQ00;
typedef typename MatrixS_or_T<MQ0,MQ1,Real>::type MatrixQ01;
typedef typename MatrixS_or_T<MQ0,MQ2,Real>::type MatrixQ02;
typedef typename MatrixS_or_T<MQ0,MQ3,Real>::type MatrixQ03;

typedef typename MatrixS_or_T<MQ1,MQ0,Real>::type MatrixQ10;
typedef typename TraitsTypeBlock1::MatrixQ        MatrixQ11;
typedef typename MatrixS_or_T<MQ1,MQ2,Real>::type MatrixQ12;
typedef typename MatrixS_or_T<MQ1,MQ3,Real>::type MatrixQ13;

typedef typename MatrixS_or_T<MQ2,MQ0,Real>::type MatrixQ20;
typedef typename MatrixS_or_T<MQ2,MQ1,Real>::type MatrixQ21;
typedef typename TraitsTypeBlock2::MatrixQ        MatrixQ22;
typedef typename MatrixS_or_T<MQ2,MQ3,Real>::type MatrixQ23;

typedef typename MatrixS_or_T<MQ3,MQ0,Real>::type MatrixQ30;
typedef typename MatrixS_or_T<MQ3,MQ1,Real>::type MatrixQ31;
typedef typename MatrixS_or_T<MQ3,MQ2,Real>::type MatrixQ32;
typedef typename TraitsTypeBlock3::MatrixQ        MatrixQ33;

// block system matrices
typedef TraitsTypeBlock0::SystemMatrix        BlockSM00;
typedef MatrixD<SparseMatrix_CRS<MatrixQ01> > BlockSM01;
typedef MatrixD<SparseMatrix_CRS<MatrixQ02> > BlockSM02;
typedef MatrixD<SparseMatrix_CRS<MatrixQ03> > BlockSM03;

typedef MatrixD<SparseMatrix_CRS<MatrixQ10> > BlockSM10;
typedef TraitsTypeBlock1::SystemMatrix        BlockSM11;
typedef MatrixD<SparseMatrix_CRS<MatrixQ12> > BlockSM12;
typedef MatrixD<SparseMatrix_CRS<MatrixQ13> > BlockSM13;

typedef MatrixD<SparseMatrix_CRS<MatrixQ20> > BlockSM20;
typedef MatrixD<SparseMatrix_CRS<MatrixQ21> > BlockSM21;
typedef TraitsTypeBlock2::SystemMatrix        BlockSM22;
typedef MatrixD<SparseMatrix_CRS<MatrixQ23> > BlockSM23;

typedef MatrixD<SparseMatrix_CRS<MatrixQ30> > BlockSM30;
typedef MatrixD<SparseMatrix_CRS<MatrixQ31> > BlockSM31;
typedef MatrixD<SparseMatrix_CRS<MatrixQ32> > BlockSM32;
typedef TraitsTypeBlock3::SystemMatrix        BlockSM33;

typedef AlgebraicEquationSet_Block4x4<BlockSM00, BlockSM01, BlockSM02, BlockSM03,
                                      BlockSM10, BlockSM11, BlockSM12, BlockSM13,
                                      BlockSM20, BlockSM21, BlockSM22, BlockSM23,
                                      BlockSM30, BlockSM31, BlockSM32, BlockSM33,
                                      AlgebraicEquationSetBase> Block4x4AlgebraicEquationSetType;

typedef Block4x4AlgebraicEquationSetType::SystemMatrix BlockSystemMatrixType;
typedef Block4x4AlgebraicEquationSetType::SystemNonZeroPattern BlockSystemMatrixNZType;
typedef Block4x4AlgebraicEquationSetType::SystemVector BlockSystemVectorType;

/////////////////////////////////////////////////////////////////////
// Coupling jacobians
typedef JacobianParam_Decoupled<BlockSM01,TraitsTypeBlock0> DecoupledJacobianType01;
typedef JacobianParam_Decoupled<BlockSM02,TraitsTypeBlock0> DecoupledJacobianType02;
typedef JacobianParam_ProjectToQauxv_Qinv<AuxvEquationType> CouplingJacobianType03;

typedef JacobianParam_ProjectToQauxi_Qauxv<AuxiEquationType> CouplingJacobianType10;
typedef JacobianParam_ProjectToQauxi_QIBL<AuxiEquationType> CouplingJacobianType12;
typedef JacobianParam_Decoupled<BlockSM13,TraitsTypeBlock1> DecoupledJacobianType13;

typedef JacobianParam<boost::mpl::vector1_c<int,0>,IBLEquationType> CouplingJacobianType20_IBLeqn_Qauxv;
typedef JacobianParam_Decoupled<BlockSM21,TraitsTypeBlock2> DecoupledJacobianType21;
typedef JacobianParam_Decoupled<BlockSM23,TraitsTypeBlock2> DecoupledJacobianType23;

typedef JacobianParam_Decoupled<BlockSM30,TraitsTypeBlock3> DecoupledJacobianType30;
typedef JacobianParam_PanelEqn_Qauxi<PanelMethodEquationType> CouplingJacobianType31;
typedef JacobianParam_Decoupled<BlockSM32,TraitsTypeBlock3> DecoupledJacobianType32;

template <class AESType>
void solve_and_set_solution_field(AESType& eqSet, const PyDict& newtonSolverDict)
{
  NewtonSolver<typename AESType::SystemMatrix> eqSolver(eqSet, newtonSolverDict);

  typename AESType::SystemVector sln(eqSet.vectorStateSize());

  // check residuals of initial solution
  eqSet.fillSystemVector(sln);

  // solve
  SolveStatus status = eqSolver.solve(sln, sln);
  BOOST_CHECK( status.converged );

  eqSet.setSolutionField(sln);
}

template <class AESType>
void check_linear_solver(AESType& eqSet)
{ // UMFPACK fails to solve correctly if pivot tolerance is too low (aka too little pivoting)
  PyDict UMFPACKDict;
  UMFPACKParam::checkInputs(UMFPACKDict);

  UMFPACK<typename AESType::SystemMatrix> linearSolver(UMFPACKDict, eqSet);

  BlockSystemVectorType r0(eqSet.vectorEqSize());
  r0 = 0;
  DLA::index(r0.v0[0][0],0) = 1;

  typename AESType::SystemVector dq(eqSet.vectorStateSize());
  dq = 0;

  linearSolver.solve(r0, dq);

  typename AESType::SystemVector b(eqSet.vectorEqSize());
  b = linearSolver.A() * dq;

  ScalarVector b_plain(b), r0_plain(r0);
  const Real tol_small = 1e-12, tol_close = 5e-14;
  BOOST_REQUIRE_EQUAL(b_plain.m, r0_plain.m);
  for (int j = 0; j < b_plain.m; ++j)
    SANS_CHECK_CLOSE(r0_plain.v[j], b_plain.v[j], tol_small, tol_close);

#if defined(IBL2D_DUMP) && 0
  cout << "writing r0 to tmp/r0.dat" << endl;
  WritePlainVector( r0, "tmp/r0.dat" );

  cout << "writing dq to tmp/dq.dat" << endl;
  WritePlainVector( dq, "tmp/dq.dat" );

  cout << "writing b to tmp/b.dat" << endl;
  WritePlainVector( b, "tmp/b.dat" );

  cout << "writing jacUMFPACK to tmp/jacUMFPACK.mtx" << endl;
  WriteMatrixMarketFile( linearSolver.A(), "tmp/jacUMFPACK.mtx" );
#endif
}

void search_transition(const NDPDEClassIBL& pdeIBLWall,
                       const XField<PhysDim,TopoDim>& xfld,
                       const Field_DG_Cell<PhysDim, TopoDim, ArrayQIBL>& qIBLfld,
                       const Field_DG_Cell<PhysDim, TopoDim, ArrayQauxv>& qauxvfld,
                       Real& xtr,
                       bool& isTrFoundInCell)
{
  for_each_CellGroup<TopoDim>::apply(
      SetTransitionFrontIBL2D<VarType>(pdeIBLWall, qIBLfld, qauxvfld, {0}, xtr, isTrFoundInCell), xfld );
  std::cout << std::endl;

  // search element interfaces for transition
  if (!isTrFoundInCell)
  {
    typedef typename MakeTuple<FieldTuple, XField<PhysDim, TopoDim>,
        Field<PhysDim, TopoDim, ArrayQIBL>,
        Field<PhysDim, TopoDim, ArrayQauxv> >::type TupleFieldType;

    for_each_InteriorTraceGroup_Cell<TopoDim>::apply(
        SetTransitionFrontIBL2D_interiorTrace<VarType, TupleFieldType>(pdeIBLWall, {0}, (xfld, qIBLfld, qauxvfld), xtr),
        (xfld, qIBLfld, qauxvfld) );

    std::cout << "Element interface xtr = " << xtr << std::endl;
  }
  else
  {
    std::cout << "Element interior xtr = " << xtr << std::endl;
  }
}

Real getqinf(const Real& Reinf, const Real& p0, const Real& T0,
             const Real& gamma, const Real& R, const Real& mu, const Real M=0.0)
{

  const Real tmp = 1.0 + 0.5*(gamma-1)*pow(M, 2.0);
  const Real pinf = p0/pow(tmp, gamma/(gamma-1.0));
  const Real Tinf = T0/tmp;
  const Real rhoinf = pinf/(R*Tinf);
  const Real nu = mu / rhoinf;
  return Reinf * nu;
}

} // namespace SANS

using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_Coupled4x4_IBL_Panel_FreeTransition_TwoLayerNewton_test_suite )

//----------------------------------------------------------------------------//
void runCase(const int nelemAirfoil)
{
  timer timer_setup;

  // ---------- Set problem parameters ---------- //
  const string airfoilname = "naca0004";
  const int nelem_a = nelemAirfoil; // number of panels on airfoil

  const int alpha = 0; // [degree] angle of attack
  const Real Reinf = 1.e5;

  // ---------- Set up IBL ---------- //
  const int order_soln = 1; // solution order
  const int order_param_grid = 1;  // order: parameter field and grid

  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  // ---------- Set up problem ---------- //
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelIBL gasModel(gasModelDict);

  const Real mue = 1.827e-5;
  typename PDEClassIBL::ViscosityModelType viscosityModel(mue);

  const Real qinf = getqinf(Reinf, p0, T0, gasModel.gamma(), gasModel.R(), viscosityModel.dynamicViscosity()); // [m/s] freestream speed
  std::cout << "qinf = " << qinf << std::endl;

  std::vector<int> ntcrit_thousand_array;

  ntcrit_thousand_array.push_back(6000);

//  for (int j = 4000; j <= 6000; j+=25)
//    ntcrit_thousand_array.push_back(j);
//  for (int j = 4275; j <= 4425; j+=5)
//    ntcrit_thousand_array.push_back(j);

//  ntcrit_thousand_array.push_back(6000);
//  for (int j = 5100; j <= 5400; j+=20)
//    ntcrit_thousand_array.push_back(j);

  PyDict transitionModelDict_wall;
  transitionModelDict_wall[TransitionModelParams::params.xtr] = 10.0;
  transitionModelDict_wall[TransitionModelParams::params.isTransitionActive] = true;
  transitionModelDict_wall[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict_wall); // necessary for checking input parameter validity
  const typename PDEClassIBL::TransitionModelType transitionModel_wall(transitionModelDict_wall);

  PyDict transitionModelDict_wake(transitionModelDict_wall);
#if 0 // laminar wake
  transitionModelDict_wake[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict_wake[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarWake;
#else // turbulent wake
  transitionModelDict_wake[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict_wake[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.turbulentWake;
#endif
  TransitionModelParams::checkInputs(transitionModelDict_wake); // necessary for checking input parameter validity
  const typename PDEClassIBL::TransitionModelType transitionModel_wake(transitionModelDict_wake);

  PyDict secVarDict;
  secVarDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
             = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;
  ThicknessesCoefficientsParamsType::checkInputs(secVarDict);

  // PDE
  NDPDEClassIBL pdeIBLWall(gasModel, viscosityModel, transitionModel_wall, secVarDict);
  NDPDEClassIBL pdeIBLWake(gasModel, viscosityModel, transitionModel_wake, secVarDict);

  // BC
  // Create a BC dictionaries
  PyDict BCNoneTEArgs;
  BCNoneTEArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.Matching;
  BCNoneTEArgs[BCWakeMatchClass::ParamsType::params.matchingType] =
      BCWakeMatchClass::ParamsType::params.matchingType.trailingEdge;

  PyDict BCWakeInArgs;
  BCWakeInArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.Matching;
  BCWakeInArgs[BCWakeMatchClass::ParamsType::params.matchingType] =
      BCWakeMatchClass::ParamsType::params.matchingType.wakeInflow;

  PyDict BCWakeOutArgs;
  BCWakeOutArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.None;

  PyDict PyBCList_a;
  PyBCList_a["_BCNoneTE_"] = BCNoneTEArgs;

  PyDict PyBCList_w;
  PyBCList_w["_BCWakeIn_"] = BCWakeInArgs;
  PyBCList_w["BCOut"] = BCWakeOutArgs;

  // No exceptions should be thrown
  BCParamsIBL::checkInputs(PyBCList_a);
  BCParamsIBL::checkInputs(PyBCList_w);

  std::map<std::string, std::vector<int> > BCBoundaryGroups_a;
  std::map<std::string, std::vector<int> > BCBoundaryGroups_w;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_a["_BCNoneTE_"] = {0};
  BCBoundaryGroups_w["_BCWakeIn_"] = {1};
  BCBoundaryGroups_w["BCOut"] = {2};

  // ---------- Set up grid ---------- //

  string filename_airfoil = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem_a) + ".txt";
  string filename_wake = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem_a)
                               + "_wake_a" + stringify(alpha) + ".txt";

  std::vector<VectorX> coordinates_a, coordinates_w;
  XField2D_Line_X1_1Group::readXFoilGrid(filename_airfoil, coordinates_a);
  XField2D_Line_X1_1Group::readXFoilGrid(filename_wake, coordinates_w);
#if defined(SANS_VERBOSE)
  cout << "Airfoil grid: " << filename_airfoil << endl;
  cout << "Wake grid:    " << filename_wake << endl;
#endif

  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);

  std::cout << "Completed setting up grid.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

  // ---------- Set up Panel ---------- //

  const VectorX Vinf = { qinf*cos(static_cast<Real>(alpha)*PI/180.0),
                         qinf*sin(static_cast<Real>(alpha)*PI/180.0) }; // freestream velocity

  const std::vector<int> panelCellGroups = {0,1};
  PanelMethodType panel(Vinf,p0,T0,xfld,panelCellGroups);

  BOOST_CHECK_EQUAL(nelem_a, panel.nPanelAirfoil());
  const int nelem_w = panel.nPanelWake();

  // ---------- Initialize solution/parameter fields ---------- //
  // Auxiliary viscous solution field
  Field_DG_Cell<PhysDim, TopoDim, ArrayQauxv> qauxvfld(xfld, order_param_grid, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( xfld.nElem(), qauxvfld.nElem() );
  qauxvfld = 0.0;

  // IBL solution field
  Field_DG_Cell<PhysDim, TopoDim, ArrayQIBL> matchIBLfld( xfld, 0, BasisFunctionCategory_Legendre );
//  for (int i = 0;  i < matchIBLfld.nDOF(); ++i)
//    matchIBLfld = pdeIBLWall.setDOFFrom( VarDataType(pdeIBLWall.deltaLami_dummy_, pdeIBLWall.ALami_dummy_,
//                                                     pdeIBLWall.deltaTurb_dummy_, pdeIBLWall.ATurb_dummy_,
//                                                     pdeIBLWall.varMatchDummy_, pdeIBLWall.ctauLami_dummy_) );
  for (int i = 0;  i < matchIBLfld.nDOF(); ++i)
    matchIBLfld = pdeIBLWall.setDOFFrom( VarDataType(pdeIBLWall.varMatchDummy_, pdeIBLWall.varMatchDummy_,
                                                     1.304878480604027e-02, 1.281371168605929e+00,
                                                     pdeIBLWall.varMatchDummy_, 4.095399703953297e-04) );
  //TODO: strictly speaking, should not work with DOF directly

  BasisFunctionCategory basisCat_qIBL = BasisFunctionCategory_None;
  if (order_soln == 0)
    basisCat_qIBL = BasisFunctionCategory_Legendre;
  else if (order_soln == 1)
    basisCat_qIBL = BasisFunctionCategory_Hierarchical;

  Field_DG_Cell<PhysDim, TopoDim, ArrayQIBL> qIBLfld( xfld, order_soln, basisCat_qIBL );

  string QinitNameBL = "xfoil";
  string Qfilename_a = "IO/CodeConsistency/IBL/" + airfoilname + "_qinit_ne" + stringify(nelem_a)
                          + "_a" + stringify(alpha) + "_transition.txt"; // TODO: hard-coded initial sol here
  std::vector<ArrayQIBL> dataQinit_a =
    QIBLDataReader<VarDataType>::getqIBLvec(pdeIBLWall.getVarInterpreter(), Qfilename_a);

  ArrayQIBL varIBLinitial_custom4_wake = pdeIBLWall.setDOFFrom( VarDataType(0.01, 1.0, 0.05, 4.0, 0.8, 0.02) );
  string QinitNameWake = "custom4";
  std::vector<ArrayQIBL> dataQinit_w;
  dataQinit_w.assign(nelem_w+1, varIBLinitial_custom4_wake);

  for_each_CellGroup<TopoDim>::apply( SetSolnDofCell_IBL2D<VarType>(dataQinit_a, {0}, order_soln), (xfld, qIBLfld) );
  for_each_CellGroup<TopoDim>::apply( SetSolnDofCell_IBL2D<VarType>(dataQinit_w, {1}, order_soln), (xfld, qIBLfld) );

  BOOST_REQUIRE_EQUAL( xfld.nElem(), qIBLfld.nElem() );

#if defined(SANS_VERBOSE)
  cout << "Initial QIBL airfoil: " << QinitNameBL << ": " << Qfilename_a << endl;
  cout << "Initial QIBL wake:    " << QinitNameWake << " = " << varIBLinitial_custom4_wake << endl;
#endif

  // get forced transition location
  {
    Real xtr_natural = 10.0; //TODO
    bool isTrFoundInCell = true;
    for_each_CellGroup<TopoDim>::apply( SetTransitionFrontIBL2D<VarType>(pdeIBLWall, qIBLfld, qauxvfld, {0}, xtr_natural, isTrFoundInCell), xfld );

    typedef typename MakeTuple<FieldTuple, XField<PhysDim, TopoDim>,
                                           Field<PhysDim, TopoDim, ArrayQIBL>,
                                           Field<PhysDim, TopoDim, ArrayQauxv> >::type TupleFieldType;

    Real xtr_interiorTrace = 10.0;
    for_each_InteriorTraceGroup_Cell<TopoDim>::apply(
        SetTransitionFrontIBL2D_interiorTrace<VarType, TupleFieldType>(pdeIBLWall, {0}, (xfld, qIBLfld, qauxvfld), xtr_interiorTrace),
        (xfld, qIBLfld, qauxvfld) );
  }

  // Lagrange multiplier field
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQIBL>
    lgfld_a( xfld, order_soln, BasisFunctionCategory_Legendre, BCParamsIBL::getLGBoundaryGroups(PyBCList_a, BCBoundaryGroups_a) );
  lgfld_a = 0.0;

  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQIBL>
    lgfld_w( xfld, order_soln, BasisFunctionCategory_Legendre, BCParamsIBL::getLGBoundaryGroups(PyBCList_w, BCBoundaryGroups_w) );
  lgfld_w = 0.0;

  // Hub trace field
  Field_DG_HubTrace<PhysDim,TopoDim,ArrayQIBL> hbfld(xfld, 0, BasisFunctionCategory_Legendre);

  hbfld = {0.0, 0.0, -1e-02, -1e-02, 0.0, 0.0}; // representative values

#if defined(SANS_VERBOSE)
  cout << "Initial QIBL hubtrace: " << hbfld.DOF(0) << endl;
#endif

  std::cout << "Completed initializing IBL solution.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

  // Panel solution field
  GamFieldType gamfld(xfld, PanelMethodEquationType::order_gam, BasisFunctionCategory_Hierarchical,{0});
  gamfld = 0.0;
  Real Psi0 = 0.0;
  LamFieldType lamfld(xfld, PanelMethodEquationType::order_lam, BasisFunctionCategory_Legendre);
  lamfld = 0.0;

  // Auxiliary inviscid solution field
  LamFieldType qauxifld(xfld, PanelMethodEquationType::order_lam, BasisFunctionCategory_Legendre);
  qauxifld = 0.0;

  // Tuple fields
  TupleFieldIBLParamType tupleIBLfld = (qauxvfld, xfld);

  TupleFieldAuxiParamType tupleAuxifld = (qIBLfld, qauxvfld, xfld);

  // ------------------------------------ //
  // SET UP EQUATION SETS
  // ------------------------------------ //
  std::vector<int> cellGroup_a = {0};
  std::vector<int> cellGroup_w = {1};

  std::vector<int> interiorTraceGroups_a = {0};
  std::vector<int> interiorTraceGroups_w = {1};

  std::vector<int> boundaryTraceGroups_a = {0};
  std::vector<int> boundaryTraceGroups_w = {1,2};

  QuadratureOrder quadratureOrder_GalerkinWeightedIntegral(xfld, 4);

  const Real tol_eqn = 2.e-12;

  // IBL
  std::vector<Real> tol_IBL(IBLEquationType::nEqnSet, tol_eqn);
  IBLEquationType iblEqSetFreeTr(hbfld, tupleIBLfld, matchIBLfld, qIBLfld, lgfld_a, pdeIBLWall,
                                 quadratureOrder_GalerkinWeightedIntegral, ResidualNorm_L2, tol_IBL,
                                 cellGroup_a, interiorTraceGroups_a, PyBCList_a, BCBoundaryGroups_a);
  auto iblEqSetFreeTr_w_ptr
    = std::make_shared<IBLEquationType>(hbfld, tupleIBLfld, matchIBLfld, qIBLfld, lgfld_w, pdeIBLWake,
                                        quadratureOrder_GalerkinWeightedIntegral, ResidualNorm_L2, tol_IBL,
                                        cellGroup_w, interiorTraceGroups_w, PyBCList_w, BCBoundaryGroups_w);
  iblEqSetFreeTr.addAlgebraicEquationSet(iblEqSetFreeTr_w_ptr);

  std::cout << "Completed setting up IBL AES.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

  // Panel
  PanelMethodEquationType panelEqSet(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  std::cout << "Completed setting up panel AES.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

  // Auxiliary viscous equation
  ElementXField<PhysDim,TopoDim,Line> xfldElemDummy(FcnEvalQauxvType::order_qauxv, BasisFunctionCategory_Hierarchical);
  xfldElemDummy.DOF(0) = {0.0, 0.0};
  xfldElemDummy.DOF(1) = {1.0, 0.0};
  FcnEvalQauxvType fcnEvalQauxvDummy(panel,xfldElemDummy,gamfld,lamfld,0,0); //TODO: dummy here
  IntegrandCellAuxvType integrandCellAuxv(fcnEvalQauxvDummy, panelCellGroups);
  AuxvEquationType auxvEqSet(xfld, qauxvfld, integrandCellAuxv, quadratureOrder_GalerkinWeightedIntegral, {{tol_eqn}}, panelEqSet, panelCellGroups);

  std::cout << "Completed setting up auxv AES.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

  // Auxiliary inviscid equation
  IntegrandAuxiNullCellType integrandCellAuxiNull(panelCellGroups);

  typedef typename AuxiEquationType::IntegrandInteriorTraceType integrandAuxiInteriorTraceType;
  typedef typename AuxiEquationType::IntegrandBoundaryTraceType integrandAuxiBoundaryTraceType;

  PDENDConvertSpace<PhysDim, ProjectionToQauxi<PanelMethodEquationType, NDPDEClassIBL> > projectorToQauxi_a(pdeIBLWall);
  PDENDConvertSpace<PhysDim, ProjectionToQauxi<PanelMethodEquationType, NDPDEClassIBL> > projectorToQauxi_w(pdeIBLWake);

  std::vector<std::shared_ptr<integrandAuxiInteriorTraceType> > integrandAuxiInteriorTraceArray;
  integrandAuxiInteriorTraceArray.push_back( std::make_shared<integrandAuxiInteriorTraceType>(projectorToQauxi_a, interiorTraceGroups_a) );
  integrandAuxiInteriorTraceArray.push_back( std::make_shared<integrandAuxiInteriorTraceType>(projectorToQauxi_w, interiorTraceGroups_w) );

  std::vector<std::shared_ptr<integrandAuxiBoundaryTraceType> > integrandAuxiBoundaryTraceArray;
  integrandAuxiBoundaryTraceArray.push_back( std::make_shared<integrandAuxiBoundaryTraceType>(projectorToQauxi_a, boundaryTraceGroups_a) );
  integrandAuxiBoundaryTraceArray.push_back( std::make_shared<integrandAuxiBoundaryTraceType>(projectorToQauxi_w, boundaryTraceGroups_w) );

  AuxiEquationType auxiEqSet(tupleAuxifld, qauxifld, integrandCellAuxiNull, quadratureOrder_GalerkinWeightedIntegral, {{tol_eqn}},
                             integrandAuxiInteriorTraceArray, integrandAuxiBoundaryTraceArray);

  std::cout << "Completed setting up auxi AES.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

  // Coupling jacobians
  DecoupledJacobianType01 JP01(auxvEqSet);
  DecoupledJacobianType02 JP02(auxvEqSet);
  CouplingJacobianType03 JP03(auxvEqSet);

  CouplingJacobianType10 JP10(auxiEqSet);
  CouplingJacobianType12 JP12(auxiEqSet);
  DecoupledJacobianType13 JP13(auxiEqSet);

  std::map<int,int> columnMap;
  columnMap[0] = 0;
  CouplingJacobianType20_IBLeqn_Qauxv JP20(iblEqSetFreeTr, columnMap);
  auto JP20_w_ptr = std::make_shared<CouplingJacobianType20_IBLeqn_Qauxv>(*iblEqSetFreeTr_w_ptr, columnMap);
  JP20.addJacobianParamSet(JP20_w_ptr);

  DecoupledJacobianType21 JP21(iblEqSetFreeTr);
  DecoupledJacobianType23 JP23(iblEqSetFreeTr);

  DecoupledJacobianType30 JP30(panelEqSet);
  CouplingJacobianType31  JP31(panelEqSet);
  DecoupledJacobianType32 JP32(panelEqSet);

  // block system
  Block4x4AlgebraicEquationSetType blockAES(auxvEqSet, JP01,      JP02,     JP03,
                                            JP10,      auxiEqSet, JP12,     JP13,
                                            JP20,      JP21,      iblEqSetFreeTr, JP23,
                                            JP30,      JP31,      JP32,     panelEqSet);

  std::cout << "Completed setting up global AES.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

  // ------------------------------------ //
  // SOLVE
  // ------------------------------------ //

  // ---------- Set up Newton solver ---------- //
  PyDict NewtonSolverDict, LinearSolverDict, NewtonLineUpdateDict;

#if defined(INTEL_MKL) && 0 // MKL was only once used for debugging
  std::cout << "Using MKL\n";
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
#else
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
#endif

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 5.0;
#ifdef SANS_VERBOSE
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinearSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 50;
#ifdef SANS_VERBOSE
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = true;
#endif

#if defined(IBL2D_DUMP) && 0 // even more verbose for debugging
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = true;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  std::cout << "Starting solution.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

  // --------- Carry out solution ---------- //
#if defined(SANS_VERBOSE)
  std::cout << "\nInitialize the inviscid solution by solving the panel method alone:" << endl;
#endif
  solve_and_set_solution_field(panelEqSet, NewtonSolverDict);  // solve panel method

  std::cout << "Completed pre-solving panel.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

#if defined(SANS_VERBOSE)
  std::cout << "\nGiven the panel solution, initialize the auxiliary viscous solution by solving the auxiliary viscous equation alone:" << endl;
#endif

  solve_and_set_solution_field(auxvEqSet, NewtonSolverDict);  // solve for qauxvfld

  std::cout << "Completed pre-solving auxv eqns.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

#if 1 // don't solve uncoupled IBL for initialization

#if defined(SANS_VERBOSE)
  std::cout << "\nViscous solution is set to the initial input data. \n" << endl;
#endif

#else // solve uncoupled IBL for initialization

#if defined(SANS_VERBOSE)
  std::cout << "\nViscous solution is initialized by solving uncoupled IBL, given initialized Qauxv. \n" << endl;
#endif
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5;
  solve_and_set_solution_field(iblEqSetFreeTr, NewtonSolverDict); // solve for Q_IBL
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 50;

  std::cout << "Completed pre-solving ibl eqns.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

#endif

#if 1

#if defined(SANS_VERBOSE)
  cout << "\nAuxiliary inviscid solution to initialized to zero. \n" << endl;
#endif

#else

#if defined(SANS_VERBOSE)
  std::cout << "\nAuxiliary inviscid solution to initialized based on initial QIBL and Qauxv:" << endl;
#endif

  solve_and_set_solution_field(auxiEqSet, NewtonSolverDict); // solve for qauxifld
#endif

  // --------- Linear solver pre-check ---------- //
  std::cout << "Starting test-solving linear system.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;
  check_linear_solver(blockAES);
  std::cout << "Completed pre-checking linear solver.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

  // --------- Solve global system using nonlinear solver ---------- //
  std::cout << "Start solving global nonlinear system.  Elapsed time : " << timer_setup.elapsed() << " s" << std::endl;

  BlockSystemVectorType sln(blockAES.vectorStateSize());
  blockAES.fillSystemVector(sln);

#if defined(IBL2D_DUMP) && 0
  BlockSystemMatrixNZType nz(blockAES.matrixSize());
  blockAES.jacobian(sln,nz);
  BlockSystemMatrixType jac(nz);
  blockAES.jacobian(sln, jac);
  cout << "writing initial jacobian tmp/jac_initial.mtx" << endl;
  WriteMatrixMarketFile( jac, "tmp/jac_initial.mtx" );
#endif

  // Newton solution
  const int niterNewtonFreeTr = 50;

  PyDict NewtonSolverDictFreeTr(NewtonSolverDict), NewtonLineUpdateDictFreeTr(NewtonLineUpdateDict);
#if 0 // line-search Newton update
  NewtonLineUpdateDictFreeTr[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
#ifdef SANS_VERBOSE
  NewtonLineUpdateDictFreeTr[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif
#else // undex-relaxed Newton update
  NewtonLineUpdateDictFreeTr[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
  NewtonSolverDictFreeTr[NewtonSolverParam::params.MaxChangeFraction] = 100.0;
#endif

  NewtonSolverDictFreeTr[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDictFreeTr;
  NewtonSolverDictFreeTr[NewtonSolverParam::params.MaxIterations] = 1;

  NewtonSolver<BlockSystemMatrixType> solverFreeTr(blockAES, NewtonSolverDictFreeTr);

  PyDict NewtonSolverDictForcedTr(NewtonSolverDict);
  //    NewtonSolverDictForcedTr[NewtonSolverParam::params.MaxIterations] = 3;
  NewtonSolver<BlockSystemMatrixType> solverForcedTr(blockAES, NewtonSolverDictForcedTr);

  for (int i_nt = 0; i_nt < (int) ntcrit_thousand_array.size(); ++i_nt)
  {
    const int ntcrit_thousand = ntcrit_thousand_array.at(i_nt);
    const Real ntcrit_freeTr = static_cast<Real>(ntcrit_thousand)*1e-4;
    transitionModel_wall.set_ntcrit(ntcrit_freeTr);
#ifdef SANS_VERBOSE
    std::cout << std::endl;
    std::cout << "Run the case with ntcrit = " << transitionModel_wall.ntcrit() << std::endl << std::endl;
#endif

    bool isTrFoundInCell = true;
    Real xtr = transitionModel_wall.xtr();
    const Real xtr_freeTr = transitionModel_wall.xtr();

    // two-layer Newton iterations
    for (int iter = 0; iter < niterNewtonFreeTr; ++iter)
    {
      // First, solve the free transition problem
      std::cout << std::endl << "Free-transition Newton iteration " << iter << std::endl << std::endl;
      SolveStatus statusFree = solverFreeTr.solve(sln, sln);
      blockAES.setSolutionField(sln);

      // terminate for loop if solution has converged
      if (statusFree.converged) { break; }

      // Otherwise, re-condition solution using forced-transition solution
      //
      // search for current transition location
      std::cout << std::endl;
      search_transition(pdeIBLWall, xfld, qIBLfld, qauxvfld, xtr, isTrFoundInCell);
      const Real xtrPrevIter = xtr; // record for later use

      std::cout << std::endl << "***** Nested forced-forced solution starts *****" << std::endl << std::endl;
      transitionModel_wall.set_ntcrit(1.e10);
      transitionModel_wall.set_xtr(xtr);

      SolveStatus statusForced = solverForcedTr.solve(sln, sln);
      blockAES.setSolutionField(sln);

      BOOST_CHECK( statusForced.converged );

      transitionModel_wall.set_ntcrit(ntcrit_freeTr);
      transitionModel_wall.set_xtr(xtr_freeTr);
      std::cout << std::endl << "***** Nested forced-transition solution completes *****" << std::endl << std::endl;

      // search for transition location again
      search_transition(pdeIBLWall, xfld, qIBLfld, qauxvfld, xtr, isTrFoundInCell);

#if 1 // TODO: this is a current hack to account for the case where transition happens at an element interface;
      // free and forced transition does have the same residual in this case but this issue is pretty minor
      if (!isTrFoundInCell && std::fabs(xtr-xtrPrevIter) < 10.*std::numeric_limits<Real>::epsilon())
      {
        std::cout << std::endl
            << "***** Forced and free transition both identifies the same transition location at element interface *****" << std::endl;
#if 0
        break; // TODO: this does not guarantee that forced and free transition gets the same solution
#else
        // TODO: the following treatment is super hacky
        const Real dx = (iter % 2 == 0) ? +1.e-6 : -1.e-6;
        std::cout << "To avoid being trapped here, move xtrPrevIter = " << xtrPrevIter << " by dx = " << dx << std::endl << std::endl;
        xtr += dx;
#endif
      }
#endif
    }

    std::cout << std::endl << "Free-transition Newton iteration terminated " << std::endl << std::endl;
    std::cout << "Transition location xtr = " << std::setprecision(16) << xtr << std::endl;
    SolveStatus status = solverFreeTr.solve(sln, sln);
    BOOST_CHECK( status.converged );
    if (status.converged)
      std::cout << "Coupled IBL solution converged!" << std::endl;

    // ------------------------------------ //
    // Pyrite check
    // ------------------------------------ //
    const std::string caseTagName = "ntcrit0p" + stringify(ntcrit_thousand);
    const std::string blProfileType = transitionModelDict_wall.get(TransitionModelParams::params.profileCatDefault)
                  + caseTagName;
    const std::string wakeProfileType = transitionModelDict_wake.get(TransitionModelParams::params.profileCatDefault)
                      + secVarDict.get(ThicknessesCoefficientsParamsType::params.nameCDclosure);

    if (ntcrit_thousand == 6000)
    { // Pyrite check
      const Real pyrite_tol = 2.4e-7;

#if ISIBLLFFLUX_PDEIBL
      string pyriteFilename
      = "IO/CodeConsistency/Solve2D_Coupled_4x4_DG_IBL_Panel_" + caseTagName + "_"
      + airfoilname + "_ne" + stringify(nelem_a) + "_p" + stringify(order_soln)
      + "_Test_FittedTransition_LFflux.txt";
#else
      string pyriteFilename
      = "IO/CodeConsistency/Solve2D_Coupled_4x4_DG_IBL_Panel_" + caseTagName + "_"
      + airfoilname + "_ne" + stringify(nelem_a) + "_p" + stringify(order_soln)
      + "_Test_FittedTransition_FullUpwind.txt";
#endif

      const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
      const bool isPyriteVerbose = true;
#else
      const bool isPyriteVerbose = false;
#endif
      pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

      pyriteFile << std::setprecision(16) << std::scientific;

      for (int i = 0; i < qIBLfld.nDOF(); ++i)
      {
        for (int n = 0; n < PDEClassIBL::N; ++n)
          pyriteFile << qIBLfld.DOF(i)[n];
        pyriteFile << std::endl;
      }

      for (int n = 0; n < PDEClassIBL::N; ++n)
        pyriteFile << hbfld.DOF(0)[n];
      pyriteFile << std::endl;

      for (int i = 0; i < gamfld.nDOF(); ++i)
      {
        pyriteFile << gamfld.DOF(i) << std::endl;
      }

      pyriteFile << Psi0 << std::endl;

      for (int i = 0; i < lamfld.nDOF(); ++i)
      {
        pyriteFile << lamfld.DOF(i) << std::endl;
      }
    }

    // ------------------------------------ //
    // Dump results
    // ------------------------------------ //
#ifdef IBL2D_DUMP
    { // IBL solution & output
      const int order_output = 1;
      string outputfilename = "tmp/outputCoupled4x4_DGAdv_IBL2D_" + airfoilname + "_ne" + stringify(nelem_a)
                                      + "_p" + stringify(order_soln) + "_airfoilANDwake_"
                                      + blProfileType + "_Qinit" + QinitNameBL + "_"
                                      + wakeProfileType + "_Qinit" + QinitNameWake + ".plt";
      typedef typename SetIBLoutputCellGroup_impl<VarType>::template ArrayOutput<Real> ArrayOutput;
      Field_DG_Cell<PhysDim, TopoDim, ArrayOutput> outputfld(xfld, order_output, BasisFunctionCategory_Hierarchical);

      // set output field
      for_each_CellGroup<TopoD1>::apply( SetIBLoutputCellGroup<VarType>(pdeIBLWall, qinf, qIBLfld, qauxvfld, {0}, -1), (xfld, outputfld) );
      for_each_CellGroup<TopoD1>::apply( SetIBLoutputCellGroup<VarType>(pdeIBLWake, qinf, qIBLfld, qauxvfld, {1}, -1), (xfld, outputfld) );

      auto output_names = SetIBLoutputCellGroup_impl<VarType>::ElementProjectionType::outputNames();
      output_Tecplot(outputfld, outputfilename, output_names);

      std::cout << "Hub trace variable: " << setprecision(16) << hbfld.DOF(0) << std::endl;

      //    string slnfilename_match = "tmp/matchCoupled4x4_DGAdv_" + airfoilname + "_ne" + stringify(nelem_a)
      //                             + "_p" + stringify(order_soln) + "_airfoilANDwake_"
      //                             + blProfileType + "_Qinit" + QinitNameBL + "_"
      //                             + wakeProfileType + "_Qinit" + QinitNameWake + ".plt";
      //    output_Tecplot( matchIBLfld, slnfilename_match );
    }

    //  {
    //    string slnfilename_gam = "tmp/gamCoupled4x4_DGAdv_" + airfoilname + "_ne" + stringify(nelem_a)
    //                             + "_p" + stringify(order_soln) + "_airfoilANDwake_"
    //                             + blProfileType + "_Qinit" + QinitNameBL + "_"
    //                             + wakeProfileType + "_Qinit" + QinitNameWake + ".plt";
    //    output_Tecplot( gamfld, slnfilename_gam, {"gamma"} );
    //
    //    string slnfilename_lam = "tmp/lamCoupled4x4_DGAdv_" + airfoilname + "_ne" + stringify(nelem_a)
    //                             + "_p" + stringify(order_soln) + "_airfoilANDwake_"
    //                             + blProfileType + "_Qinit" + QinitNameBL + "_"
    //                             + wakeProfileType + "_Qinit" + QinitNameWake + ".plt";
    //    output_Tecplot( lamfld, slnfilename_lam, {"lambda"} );
    //
    //    std::cout << "Psi_0 = " << setprecision(16) << Psi0 << std::endl;
    //  }
#endif

#if defined(IBL2D_DUMP) && 0
    // final jacobian
    blockAES.jacobian(sln, jac);
    cout << "writing final jacobian tmp/jac_final.mtx" << endl;
    WriteMatrixMarketFile( jac, "tmp/jac_final.mtx" );
#endif

    std::cout << "Total runtime for current ntcrit: " << timer_setup.elapsed() << " s" << std::endl;
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Coupled4x4_IBL_Panel_IntraCellNaturalTransitionBL_NewtonUR_TurbWakeXfoilLagClosure_test )
{
//  const std::vector<int> nelemArr = {64, 96, 128, 192, 256, 360, 512, 640, 800, 1024, 2048};
  const std::vector<int> nelemArr = {64};
  for (int i=0; i < int(nelemArr.size()); ++i)
    runCase(nelemArr.at(i));
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
