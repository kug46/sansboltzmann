// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEulermitAVDiffusion1D_Consistency_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/PDEEuler1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

//Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD1>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
template class TraitsModelEuler<QTypeEntropy, GasModel>;
template class PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>>;
template class PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel>>;
template class PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion1D_Consistency_btest )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 3 );

  // function tests

  Real x, time;
  Real rho, u, t;

  x = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  // conservative flux
  ArrayQ uTrue = 0;
  ArrayQ uCons = 0;
  avpde.masterState( param, x, time, q, uCons );
  pde.masterState( param, x, time, q, uTrue );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // conservative flux
  ArrayQ ftTrue = 0;
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( param, x, time, q, ft );
  pde.fluxAdvectiveTime( param, x, time, q, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );

  // advective flux
  ArrayQ fTrue = 0;
  ArrayQ f = 0;
  avpde.fluxAdvective( param, x, time, q, f );
  pde.fluxAdvective( x, time, q, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );

  //Call these simply to make sure coverage is high
  ArrayQ qL, qR, qx, qy, qxL, qxR, qyL, qyR, source;
  Real dummy = 0;

  MatrixQ kxx;
  avpde.fluxViscous( param, dummy, dummy, q, qx, f );
  avpde.diffusionViscous( param, dummy, dummy, q, qx, kxx );
  avpde.source( param, dummy, dummy, q, qx, source );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( flux_areaChange, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw, area);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw, area);

  // static tests
  BOOST_REQUIRE( avpde.N == 3 );

  // function tests

  Real x, time;
  Real rho, u, t;

  x = time = 1.0;   // not actually used in functions
  rho = 1.137; u = 0.784; t = 0.987;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  // conservative flux
  ArrayQ uTrue = 0;
  ArrayQ uCons = 0;
  avpde.masterState( param, x, time, q, uCons );
  pde.masterState( x, time, q, uTrue );
  BOOST_CHECK_CLOSE( uTrue(0), uCons(0), tol );
  BOOST_CHECK_CLOSE( uTrue(1), uCons(1), tol );
  BOOST_CHECK_CLOSE( uTrue(2), uCons(2), tol );

  // advective flux
  ArrayQ fTrue = 0;
  ArrayQ f = 0;
  avpde.fluxAdvective( param, x, time, q, f );
  pde.fluxAdvective( x, time, q, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( jacobianMasterState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 3 );

  // function tests

  Real rho, u, t;

  rho = 1.137; u = 0.784; t = 0.987;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real x = 0, time = 0;

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature1D<Real>(rho, u, t) );

  // conservative flux
  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;
  avpde.jacobianMasterState( param, x, time, q, dudq );
  pde.jacobianMasterState( x, time, q, dudqTrue );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  SANS_CHECK_CLOSE( dudqTrue(0,0), dudq(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( dudqTrue(0,1), dudq(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( dudqTrue(0,2), dudq(0,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( dudqTrue(1,0), dudq(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( dudqTrue(1,1), dudq(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( dudqTrue(1,2), dudq(1,2), small_tol, close_tol )

  SANS_CHECK_CLOSE( dudqTrue(2,0), dudq(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( dudqTrue(2,1), dudq(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( dudqTrue(2,2), dudq(2,2), small_tol, close_tol )
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, time;
  Real nx;
  Real rhoL, uL, tL;
  Real rhoR, uR, tR;

  x = time = 0;   // not actually used in functions
  nx = 1.0;
  rhoL = 1.034; uL = 3.26; tL = 5.78;
  rhoR = 0.973; uR = 1.79; tR = 6.13;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  // set
  ArrayQ qL = avpde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoL, uL, tL) );
  ArrayQ qR = avpde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoR, uR, tR) );

  ArrayQ fn = 0;
  ArrayQ fnTrue = 0;
  avpde.fluxAdvectiveUpwind( param, x, time, qL, qR, nx, fn );
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fnTrue );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxRoe_areaChange, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;
  std::shared_ptr<ScalarFunction1D_OffsetLog> area( new ScalarFunction1D_OffsetLog() );

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw, area);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw, area);

  // Roe flux function test

  Real x, time;
  Real nx;
  Real rhoL, uL, tL;
  Real rhoR, uR, tR;

  x = time = 1.0;
  nx = 1.0;
  rhoL = 1.034; uL = 3.26; tL = 5.78;
  rhoR = 0.973; uR = 1.79; tR = 6.13;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  // set
  ArrayQ qL = avpde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoL, uL, tL) );
  ArrayQ qR = avpde.setDOFFrom( DensityVelocityTemperature1D<Real>(rhoR, uR, tR) );

  ArrayQ fn = 0;
  ArrayQ fnTrue = 0;
  avpde.fluxAdvectiveUpwind( param, x, time, qL, qR, nx, fn );
  pde.fluxAdvectiveUpwind( x, time, qL, qR, nx, fnTrue );
  BOOST_CHECK_CLOSE( fnTrue(0), fn(0), tol );
  BOOST_CHECK_CLOSE( fnTrue(1), fn(1), tol );
  BOOST_CHECK_CLOSE( fnTrue(2), fn(2), tol );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion1D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef MakeTuple<ParamTuple, Real, Real>::type ParamType;

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, PDEClass::Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, PDEClass::Euler_ResidInterp_Raw);

  Real x, time, dx;
  Real rho, u, t;
  Real speed, speedTrue;

  x = time = 0;   // not actually used in functions
  dx = 0.57;
  rho = 1.137; u = 0.784; t = 0.987;
  ParamType param(0.1, 0.0); // grid spacing and jump values

  Real qDataPrim[3] = {rho, u, t};
  string qNamePrim[3] = {"Density", "VelocityX",  "Temperature"};
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 3 );

  avpde.speedCharacteristic( param, x, time, dx, q, speed );
  pde.speedCharacteristic( x, time, dx, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( param, x, time, q, speed );
  pde.speedCharacteristic( x, time, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
