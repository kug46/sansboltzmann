// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing of 2-D HDGAdvective for IBL on airfoil

//#define IBL2D_DUMP
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include "pde/IBL/PDEIBL2D_impl.h"
#include "pde/IBL/BCIBL2D.h"
#include "pde/IBL/SetSolnDofCell_IBL2D.h"
#include "pde/IBL/SetVelDofCell_IBL2D.h"
#include "pde/IBL/SetIBLoutputCellGroup.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

#define ALGEBRAICEQUATIONSET_HDGADVECTIVE_INSTANTIATE
#include "Discretization/HDG/AlgebraicEquationSet_HDGAdvective_impl.h"

using namespace std;
using namespace SANS;

namespace SANS
{
typedef PhysD2 PhysDim; // physical dimension
typedef TopoD1 TopoDim; // grid cell topological dimension

// PDE class
typedef VarTypeDANCt VarType;
typedef PDEIBL<PhysDim,VarType> PDEClass;
typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef PDEClass::template ArrayParam<Real> ArrayParam;
typedef NDPDEClass::VectorX VectorX;

// BC
typedef BCIBL2DVector_NoWake<VarType> BCVectorClass;

// parameter field
typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayParam>,
                                       XField<PhysDim, TopoDim> >::type FieldParamType;

// Primal equation set
typedef AlgebraicEquationSet_HDGAdvective<NDPDEClass,BCNDConvertSpace,BCVectorClass,
                                         AlgEqSetTraits_Sparse,HDGAdv_manifold,FieldParamType> PrimalEquationSetClass;
typedef PrimalEquationSetClass::BCParams BCParams;
typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( CodeConsistencySolve2D_HDGAdvective_IBL_Line_Airfoil_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CodeConsistencySolve2D_HDGAdvective_IBL_Line_Airfoil_test )
{
  // ---------- Set problem parameters ---------- //
  const string airfoilname = "naca0004";

  const int nelem = 64;

  const int order_param_grid = 1;  // order: parameter field and grid
  const int order_soln = 1; // solution order

  const int alpha = 0; // [degree] angle of attack
#ifdef IBL2D_DUMP
  const Real qinf = 1.573869150000000; // [m/s] freestream speed
#endif
  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  GasModelIBL gasModel(gasModelDict);

  const Real mue = 1.827e-5;
  typename PDEClass::ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL; // This is dummy; not actually checking profileCatDefault_
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // PDE
  NDPDEClass pde(gasModel, viscosityModel, transitionModel);
  const int nSol = NDPDEClass::N;

  // BC
  // Create a BC dictionary
  PyDict BCOutArgs;
  BCOutArgs[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCOut"] = BCOutArgs;

  // No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCOut"] = {0,1};

  // ---------- Set solver parameters ---------- //

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 50;
#ifdef SANS_VERBOSE
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
//  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = true;
#else
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // ---------- Set up fields ---------- //

  string filename_airfoil = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem) + ".txt";
  std::vector<VectorX> coordinates_a;
  XField2D_Line_X1_1Group::readXFoilGrid(filename_airfoil, coordinates_a);
  XField2D_Line_X1_1Group xfld(coordinates_a);

#if defined(SANS_VERBOSE)
  cout << "Airfoil grid: " << filename_airfoil << endl;
#endif

  // parameter field: velocity and gradients
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velxXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velzXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  string velfilename_a = "IO/CodeConsistency/IBL/" + airfoilname + "_vel_ne" + stringify(nelem)
                       + "_a" + stringify(alpha) + ".txt";
  std::vector<Real> dataVel_a;
  SetVelocityDofCell_IBL2D::readVelocity( velfilename_a, dataVel_a );
  for_each_CellGroup<TopoDim>::apply( SetVelocityDofCell_IBL2D(dataVel_a, {0}), (velfld, velxXfld, velzXfld, xfld) );

#if defined(SANS_VERBOSE)
  cout << "Velocity field initialized from: " << velfilename_a << endl;
#endif

  BOOST_REQUIRE_EQUAL( xfld.nElem(), velfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velxXfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velzXfld.nElem() );

  // parameter field
  Field_DG_Cell<PhysDim, TopoDim, ArrayParam> paramfld(xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( xfld.nElem(), paramfld.nElem() );

  // TODO: should not work directly with DOFs
  const auto& paramInterpret = pde.getParamInterpreter();
  for (int i = 0; i < paramfld.nDOF(); ++i)
  {
    paramfld.DOF(i) = paramInterpret.setDOFFrom(
        velfld.DOF(i),
        DLA::VectorS<PhysDim::D, VectorX>(
            {VectorX({velxXfld.DOF(i)[0], velzXfld.DOF(i)[0]}),
             VectorX({velxXfld.DOF(i)[1], velzXfld.DOF(i)[1]})} ),
        p0, T0);
  }

  // DG solution field
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld( xfld, order_soln, BasisFunctionCategory_Hierarchical );
  Field_DG_Trace<PhysDim, TopoDim, ArrayQ> qIfld(xfld, order_soln, BasisFunctionCategory_Legendre); // interior trace solution

  // Set the initial condition
  string Qfilename_a = "IO/CodeConsistency/IBL/" + airfoilname + "_qinit_ne" + stringify(nelem)
                     + "_a" + stringify(alpha) + ".txt";
  std::vector<ArrayQ> dataQinit_a
    = QIBLDataReader<VarData2DDANCt>::getqIBLvec(pde.getVarInterpreter(), Qfilename_a);
  for_each_CellGroup<TopoDim>::apply( SetSolnDofCell_IBL2D<VarType>(dataQinit_a, {0}, order_soln), (xfld, qfld) );

  const int nnode = (int) dataQinit_a.size();
  BOOST_CHECK(nnode == qIfld.nDOF());
  for (int j = 0; j < nnode-2; ++j)
    qIfld.DOF(j) = 1.01*dataQinit_a.at(j+1);

  qIfld.DOF(nnode-2) = 1.01*dataQinit_a.at(0);
  qIfld.DOF(nnode-1) = 1.01*dataQinit_a.at(nnode-1);

#if defined(SANS_VERBOSE)
  cout << "Initial QIBL airfoil: " << Qfilename_a << endl;
  cout << "xfld.nElem() = " << xfld.nElem() << endl;
#endif

  BOOST_REQUIRE_EQUAL( xfld.nElem(), qfld.nElem() );

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ>
    lgfld( xfld, order_soln, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0.0;

  // ---------- Solve ---------- //

  FieldParamType tuplefld = (paramfld, xfld);

  QuadratureOrder quadratureOrder(xfld, -1);
  const Real tol_eqn = 1e-12;
  const std::vector<Real> tol(PrimalEquationSetClass::nEqnSet, tol_eqn);
  const std::vector<int> cellGroups = {0};
  const std::vector<int> interiorTraceGroups = {0};
  PrimalEquationSetClass iblEqnSet(tuplefld, qfld, qIfld, lgfld, pde, quadratureOrder, ResidualNorm_L2,
                                   tol, cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups);

  NewtonSolver<SystemMatrixClass> solver(iblEqnSet, NewtonSolverDict);

  SystemVectorClass sln(iblEqnSet.vectorStateSize());
  SystemVectorClass rsd(iblEqnSet.vectorEqSize());
  rsd = 0;

  iblEqnSet.fillSystemVector(sln);

#ifdef IBL2D_DUMP
  // ----- check initial residuals and Jacobian
  // residual
  SystemVectorClass rsdinit(iblEqnSet.vectorEqSize());
  rsdinit = 0;
  iblEqnSet.residual(sln, rsdinit);

  // jacobian nonzero pattern
  SystemNonZeroPattern nz(iblEqnSet.matrixSize());
  iblEqnSet.jacobian(sln, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  iblEqnSet.jacobian(sln, jac);

  fstream fout( "tmp/jac_init.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

  SolveStatus status = solver.solve(sln, sln);
  BOOST_CHECK( status.converged );
  iblEqnSet.setSolutionField(sln);

  // ------------------------------------ //
  // Check restart
  // ------------------------------------ //
  if ( status.converged )
  {
#ifdef SANS_VERBOSE
    std::cout << "restart with solution obtained just now: should also solve!" << endl;
#endif
    PrimalEquationSetClass PrimalEqSet_restart(tuplefld, qfld, qIfld, lgfld, pde, quadratureOrder, ResidualNorm_L2,
                                               tol, cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups );

    NewtonSolver<SystemMatrixClass> Solver_restart(PrimalEqSet_restart, NewtonSolverDict);

    SystemVectorClass sln_restart(PrimalEqSet_restart.vectorStateSize());
    PrimalEqSet_restart.fillSystemVector(sln_restart);

    SolveStatus status_restart = Solver_restart.solve(sln_restart, sln_restart);
    BOOST_CHECK( status_restart.converged );
    PrimalEqSet_restart.setSolutionField(sln_restart);

    const Real tol_restart = 1e-13;
    const int iq = 0;
    const int nDOFPDE = qfld.nDOF();
    for (int i=0; i<nDOFPDE; ++i)
      for (int j=0; j<nSol; ++j)
        SANS_CHECK_CLOSE(sln[iq][i][j],sln_restart[iq][i][j],tol_restart,tol_restart);
  }

  // ------------------------------------ //
  // POST-PROCESSING
  // ------------------------------------ //
#ifdef IBL2D_DUMP
  { // ----- check terminal jacobian
    iblEqnSet.jacobian(sln, jac);

    fstream fout_final("tmp/jac_final.mtx", fstream::out);
    cout << "btest: global jac" << endl;  WriteMatrixMarketFile(jac, fout_final);
  }

  { // dump IBL solution & output
    const int order_output = 1;
    string outputfilename = "tmp/outputUncoupled_HDGAdv_IBL2D_" + airfoilname + "_ne" + stringify(nelem)
                          + "_p" + stringify(order_soln) + "_airfoil.plt";
    typedef typename SetIBLoutputCellGroup_impl<VarType>::template ArrayOutput<Real> ArrayOutput;
    Field_DG_Cell<PhysDim, TopoDim, ArrayOutput> outputfld(xfld, order_output, BasisFunctionCategory_Hierarchical);

    // set output field
    for_each_CellGroup<TopoD1>::apply(SetIBLoutputCellGroup<VarType>(pde, qinf, qfld, paramfld, {0}, -1), (xfld, outputfld));

    auto output_names = SetIBLoutputCellGroup_impl<VarType>::ElementProjectionType::outputNames();
    output_Tecplot(outputfld, outputfilename, output_names);
  }
#endif

  { // Pyrite check
    const Real pyrite_tol = 1e-10;

#if IsContinousInterface_HDG
    string pyriteFilename = "IO/CodeConsistency/Solve2D_Uncoupled_HDGAdvectiveIBL_" + airfoilname + "_ne" + stringify(nelem) +
                            "_p" + stringify(order_soln) + "_airfoil_Test_continuous.txt";
#else
    // In the case of full upwinding, HDG is equivalent to DG for advection-reaction equation
    string pyriteFilename = "IO/CodeConsistency/Solve2D_Uncoupled_DGAdvectiveIBL_" + airfoilname + "_ne" + stringify(nelem) +
                            "_p" + stringify(order_soln) + "_airfoil_Test_FullUpwind.txt";
#endif

    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      // Currently, only check two primary unknowns; other unknowns do not matter in this test
      pyriteFile << qfld.DOF(i)[PDEClass::VarInterpType::ideltaLami];
      pyriteFile << qfld.DOF(i)[PDEClass::VarInterpType::iALami];
      pyriteFile << std::endl;
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
