// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing of 2-D DGAdvective for IBL on airfoil and wake

//#define IBL2D_DUMP
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include "pde/IBL/PDEIBLUniField.h"
#include "pde/IBL/BCIBLUniField.h"
#include "pde/IBL/SetSolnDofCell_IBL2D.h"
#include "pde/IBL/SetVelDofCell_IBL2D.h"
#include "pde/IBL/SetIBLoutputCellGroup.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_WakeMatch_Galerkin_IBL.h" // this is here just for the code to compile

using namespace std;
using namespace SANS;

namespace SANS
{
typedef PhysD2 PhysDim; // physical dimension
typedef TopoD1 TopoDim; // grid cell topological dimension

// PDE class
typedef VarTypeDsThG VarType;
typedef VarData2DDsThG VarDataType;
typedef PDEIBLUniField<PhysDim,VarType> PDEClass;
typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef PDEClass::template ArrayParam<Real> ArrayParam;
typedef NDPDEClass::VectorX VectorX;

// BC
typedef BCIBLUniField2DVector<VarType> BCVectorClass;

// parameter field
typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayParam>,
                                       XField<PhysDim, TopoDim> >::type FieldParamType;

// Primal equation set
typedef AlgebraicEquationSet_DGAdvective<NDPDEClass,BCNDConvertSpace,BCVectorClass,
                                         AlgEqSetTraits_Sparse,DGAdv_manifold,FieldParamType> PrimalEquationSetClass;
typedef PrimalEquationSetClass::BCParams BCParams;
typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( CodeConsistencySolve2D_DGAdvective_IBLUniField_Line_Airfoil_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CodeConsistencySolve2D_DGAdvective_IBLUniField_Line_Airfoil_test )
{
  // ---------- Set problem parameters ---------- //
  const string airfoilname = "naca0004";
  const int nelem = 64;

  const int order_sln = 1; // solution order
  const int order_param_grid = 1;  // order: parameter field and grid

  const int alpha = 0; // [degree] angle of attack
#ifdef IBL2D_DUMP
  const Real qinf = 1.573869150000000; // [m/s] freestream speed
#endif
  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  GasModelIBL gasModel(gasModelDict);

  const Real mue = 1.827e-5;
  typename PDEClass::ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL; // This is dummy; not actually checking profileCatDefault_
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // PDE
  NDPDEClass pde(gasModel, viscosityModel, transitionModel);

  // BC
  // Create a BC dictionary
  PyDict BCOutArgs;
  BCOutArgs[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict pyBCList;
  pyBCList["BCOut"] = BCOutArgs;

  // No exceptions should be thrown
  BCParams::checkInputs(pyBCList);

  std::map<std::string, std::vector<int>> bcBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  bcBoundaryGroups["BCOut"] = {0,1};

  // ---------- Set solver parameters ---------- //

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 2.0;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 50;
#ifdef SANS_VERBOSE
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
#else
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // ---------- Set up fields ---------- //

  string filename_airfoil = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem) + ".txt";
  std::vector<VectorX> coordinates_a;
  XField2D_Line_X1_1Group::readXFoilGrid(filename_airfoil, coordinates_a);
  XField2D_Line_X1_1Group xfld(coordinates_a);

#if defined(SANS_VERBOSE)
  cout << "Airfoil grid: " << filename_airfoil << endl;
#endif

  // parameter field: velocity and gradients
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velxXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velzXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  string velfilename_a = "IO/CodeConsistency/IBL/" + airfoilname + "_vel_ne" + stringify(nelem)
                       + "_a" + stringify(alpha) + ".txt";
  std::vector<Real> dataVel_a;
  SetVelocityDofCell_IBL2D::readVelocity( velfilename_a, dataVel_a );
  for_each_CellGroup<TopoDim>::apply( SetVelocityDofCell_IBL2D(dataVel_a, {0}), (velfld, velxXfld, velzXfld, xfld) );

#if defined(SANS_VERBOSE)
  cout << "Velocity field initialized from: " << velfilename_a << endl;
#endif

  BOOST_REQUIRE_EQUAL( xfld.nElem(), velfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velxXfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velzXfld.nElem() );

  // parameter field
  Field_DG_Cell<PhysDim, TopoDim, ArrayParam> paramfld(xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( xfld.nElem(), paramfld.nElem() );

  // TODO: should not work directly with DOFs
  const auto& paramInterpret = pde.getParamInterpreter();
  for (int i = 0; i < paramfld.nDOF(); ++i)
  {
    paramfld.DOF(i) = paramInterpret.setDOFFrom(
        velfld.DOF(i),
        DLA::VectorS<PhysDim::D, VectorX>(
            {VectorX({velxXfld.DOF(i)[0], velzXfld.DOF(i)[0]}),
             VectorX({velxXfld.DOF(i)[1], velzXfld.DOF(i)[1]})} ),
        p0, T0);
  }

  // DG solution field
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld( xfld, order_sln, BasisFunctionCategory_Hierarchical );

  // Set the initial condition

  const ArrayQ varIBLinitial_BL = pde.setDOFFrom( VarDataType(0.004, 0.0015, -12.5) );

  std::vector<ArrayQ> dataQinit_a;
  dataQinit_a.assign(nelem+1, varIBLinitial_BL);

  for_each_CellGroup<TopoDim>::apply( SetSolnDofCell_IBL2D<VarType>(dataQinit_a, {0}, order_sln), (xfld, qfld) );

#if defined(SANS_VERBOSE)
  cout << "Initial QIBL airfoil: " << varIBLinitial_BL << endl;
#endif

  BOOST_REQUIRE_EQUAL( xfld.nElem(), qfld.nElem() );

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ>
    lgfld( xfld, order_sln, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(pyBCList, bcBoundaryGroups) );
  lgfld = 0.0;

  // ---------- Solve ---------- //

  FieldParamType tuplefld = (paramfld, xfld);

  QuadratureOrder quadratureOrder(xfld, -1);
  const Real tol_eqn = 1e-12;
  const std::vector<Real> tol(2,tol_eqn);
  const std::vector<int> cellGroups = {0};
  const std::vector<int> interiorTraceGroups = {0};
  PrimalEquationSetClass iblEqnSet(tuplefld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_L2, tol,
                                   cellGroups, interiorTraceGroups, pyBCList, bcBoundaryGroups);

  NewtonSolver<SystemMatrixClass> solver(iblEqnSet, NewtonSolverDict);

  SystemVectorClass sln(iblEqnSet.vectorStateSize());
  SystemVectorClass rsd(iblEqnSet.vectorEqSize());
  rsd = 0;

  iblEqnSet.fillSystemVector(sln);

#ifdef IBL2D_DUMP
  // ----- check initial residuals and Jacobian
  // residual
  SystemVectorClass rsdinit(iblEqnSet.vectorEqSize());
  rsdinit = 0;
  iblEqnSet.residual(sln, rsdinit);

  // jacobian nonzero pattern
  SystemNonZeroPattern nz(iblEqnSet.matrixSize());
  iblEqnSet.jacobian(sln, nz);

  // jacobian
  SystemMatrixClass jac(nz);
  iblEqnSet.jacobian(sln, jac);

  fstream fout( "tmp/jac_init.mtx", fstream::out );
  cout << "btest: global jac" << endl;  WriteMatrixMarketFile( jac, fout );
#endif

  SolveStatus status = solver.solve(sln, sln);
  BOOST_CHECK( status.converged );
  iblEqnSet.setSolutionField(sln);

  // ------------------------------------ //
  // Check restart
  // ------------------------------------ //
  if ( status.converged )
  {
#ifdef SANS_VERBOSE
    std::cout << "restart with solution obtained just now: should also solve!" << endl;
#endif
    PrimalEquationSetClass PrimalEqSet_restart(tuplefld, qfld, lgfld, pde, quadratureOrder,
                                               ResidualNorm_L2, tol,
                                               cellGroups, interiorTraceGroups, pyBCList, bcBoundaryGroups );

    NewtonSolver<SystemMatrixClass> Solver_restart(PrimalEqSet_restart, NewtonSolverDict);

    SystemVectorClass sln_restart(PrimalEqSet_restart.vectorStateSize());
    PrimalEqSet_restart.fillSystemVector(sln_restart);

    SolveStatus status_restart = Solver_restart.solve(sln_restart, sln_restart);
    BOOST_CHECK( status_restart.converged );
    PrimalEqSet_restart.setSolutionField(sln_restart);

    const Real tol_restart = 1e-13;
    const int iq = 0;
    const int nDOFPDE = qfld.nDOF();
    for (int i=0; i<nDOFPDE; ++i)
      for (int j=0; j<NDPDEClass::N; ++j)
        SANS_CHECK_CLOSE(sln[iq][i][j],sln_restart[iq][i][j],tol_restart,tol_restart);
  }

  // ------------------------------------ //
  // POST-PROCESSING
  // ------------------------------------ //
#ifdef IBL2D_DUMP
  if (false)
  { // ----- check terminal jacobian
    iblEqnSet.jacobian(sln, jac);

    fstream fout_final("tmp/jac_final.mtx", fstream::out);
    cout << "btest: global jac" << endl;  WriteMatrixMarketFile(jac, fout_final);
  }

  { // dump IBL solution & output
    const int order_output = 1;

    typedef typename SetIBLoutputCellGroup_impl<VarType>::template ArrayOutput<Real> ArrayOutput;
    Field_DG_Cell<PhysDim, TopoDim, ArrayOutput> outputfld(xfld, order_output, BasisFunctionCategory_Hierarchical);
    for_each_CellGroup<TopoD1>::apply(SetIBLoutputCellGroup<VarType>(pde, qinf, qfld, paramfld, {0}, -1), (xfld, outputfld));

    const std::string caseTag = "uncoupledIBLUniField2D_" + airfoilname + "_ne" + stringify(nelem)
        + "_p" + stringify(order_sln) + "_airfoil";
    const string outputfilename = "tmp/" + caseTag + "_output.plt";

    auto output_names = SetIBLoutputCellGroup_impl<VarType>::ElementProjectionType::outputNames();
    output_Tecplot(outputfld, outputfilename, output_names);
  }
#endif

  { // Pyrite check
    const Real pyrite_tol = 6.0e-10;

#if ISIBLLFFLUX_PDEIBLUniField
    string pyriteFilename = "IO/CodeConsistency/Solve2D_Uncoupled_DGAdvectiveIBL_CorrelationClosureXFOIL_" +
        airfoilname + "_ne" + std::to_string(nelem) + "_p" + std::to_string(order_sln) + "_airfoil_Test_LFFlux.txt";
#else
#if ISUSEXFOIL_CORRELATIONCLOSURE
    string pyriteFilename = "IO/CodeConsistency/Solve2D_Uncoupled_DGAdvectiveIBL_CorrelationClosureXFOIL_" +
        airfoilname + "_ne" + std::to_string(nelem) + "_p" + std::to_string(order_sln) + "_airfoil_Test_FullUpwind.txt";
#else
    string pyriteFilename = "IO/CodeConsistency/Solve2D_Uncoupled_DGAdvectiveIBL_CorrelationClosureMSES_" +
        airfoilname + "_ne" + std::to_string(nelem) + "_p" + std::to_string(order_sln) + "_airfoil_Test_FullUpwind.txt";
#endif
#endif

    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      for (int n=0; n<PDEClass::N; ++n)
        pyriteFile << qfld.DOF(i)[n];
      pyriteFile << std::endl;
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
