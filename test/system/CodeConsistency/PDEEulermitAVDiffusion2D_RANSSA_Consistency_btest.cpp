// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEulermitAVDiffusion2D__RANSSA_Consistency_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
//class QTypePrimitiveSurrogate {};
//class QTypeConservative {};
//class QTypeEntropy {};

//Explicitly instantiate the class so coverage information is correct
template class TraitsSizeRANSSA<PhysD2>;
template class TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel>;
//template class TraitsModelRANSSA<QTypePrimitiveSurrogate, GasModel>;
//template class TraitsModelRANSSA<QTypeConservative, GasModel>;
//template class TraitsModelRANSSA<QTypeEntropy, GasModel>;
template class PDERANSSA2D< TraitsSizeRANSSA,
                            TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
//template class PDERANSSA2D< TraitsSizeRANSSA,
//                            TraitsModelRANSSA<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
//template class PDERANSSA2D< TraitsSizeRANSSA,
//                            TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
//template class PDERANSSA2D< TraitsSizeRANSSA,
//                            TraitsModelRANSSA<QTypeEntropy, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template class PDEEulermitAVDiffusion2D<TraitsSizeRANSSA,
                                        TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel>,
                                        PDERANSSA2D>;
//template class PDEEulermitAVDiffusion2D<TraitsSizeRANSSA,
//                                        TraitsModelRANSSA<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel>,
//                                        PDERANSSA2D>;
//template class PDEEulermitAVDiffusion2D<TraitsSizeRANSSA,
//                                        TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel>,
//                                        PDERANSSA2D>;
//template class PDEEulermitAVDiffusion2D<TraitsSizeRANSSA,
//                                        TraitsModelRANSSA<QTypeEntropy, GasModel, ViscosityModel_Const, ThermalConductivityModel>,
//                                        PDERANSSA2D>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion2D_RANSSA_Consistency_btest )

typedef boost::mpl::list< QTypePrimitiveRhoPressure > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  BOOST_REQUIRE( AVPDEClass::D == PDEClass::D );
  BOOST_REQUIRE( AVPDEClass::N == PDEClass::N );
  BOOST_REQUIRE( AVArrayQ::M == ArrayQ::M );
  BOOST_REQUIRE( AVMatrixQ::M == MatrixQ::M );
  BOOST_REQUIRE( AVMatrixQ::N == MatrixQ::N );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == pde.D );
  BOOST_REQUIRE( avpde.N == pde.N );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == pde.hasFluxAdvectiveTime() );
  BOOST_CHECK( avpde.hasFluxAdvective() == pde.hasFluxAdvective() );
  BOOST_CHECK( avpde.hasFluxViscous() == pde.hasFluxViscous() );
  BOOST_CHECK( avpde.hasSource() == pde.hasSource() );
  BOOST_CHECK( avpde.hasSourceTrace() == pde.hasSourceTrace() );
  BOOST_CHECK( avpde.hasForcingFunction() == pde.hasSourceTrace() );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == pde.fluxViscousLinearInGradient() );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == pde.needsSolutionGradientforSource() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxAdvective, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 5 );

  // function tests

  Real x, y, time;
  Real rho, u, v, t, nut;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = 0.821; t = 0.987, nut = 4.2;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nut}) );

  // flux in time direction
  ArrayQ ftTrue = 0;
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( param, x, y, time, q, ft );
  pde.fluxAdvectiveTime( dist, x, y, time, q, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( ftTrue(4), ft(4), tol );

  // Jacobian of flux in time direction
  MatrixQ Jt = 0;
  MatrixQ JtTrue = 0;
  avpde.jacobianFluxAdvectiveTime( param, x, y, time, q, Jt );
  pde.jacobianFluxAdvectiveTime( dist, x, y, time, q, JtTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtTrue(i,j), Jt(i,j), small_tol, close_tol )
    }
  }

  // advective flux
  ArrayQ fTrue = 0, gTrue = 0;
  ArrayQ f = 0, g = 0;
  avpde.fluxAdvective( param, x, y, time, q, f, g );
  pde.fluxAdvective( dist, x, y, time, q, fTrue, gTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );

  // Jacobian of advective flux
  MatrixQ Jxadv = 0, Jyadv = 0;
  MatrixQ JxadvTrue = 0, JyadvTrue = 0;
  avpde.jacobianFluxAdvective( param, x, y, time, q, Jxadv, Jyadv );
  pde.jacobianFluxAdvective( dist, x, y, time, q, JxadvTrue, JyadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JxadvTrue(i,j), Jxadv(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( JyadvTrue(i,j), Jyadv(i,j), small_tol, close_tol )
    }
  }

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 0.34};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, -1.23};
  ArrayQ qt = {0.21, 0.31, -0.14, 0.26, 0.01};

  // strong advective flux
  ArrayQ fadvStrongTrue = 0;
  ArrayQ fadvStrong = 0;
  avpde.strongFluxAdvective( param, x, y, time, q, qx, qy, fadvStrong );
  pde.strongFluxAdvective( dist, x, y, time, q, qx, qy, fadvStrongTrue );
  BOOST_CHECK_CLOSE( fadvStrongTrue(0), fadvStrong(0), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(1), fadvStrong(1), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(2), fadvStrong(2), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(3), fadvStrong(3), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(4), fadvStrong(4), tol );

  // strong advective flux
  ArrayQ ftadvStrongTrue = 0;
  ArrayQ ftadvStrong = 0;
  avpde.strongFluxAdvectiveTime( param, x, y, time, q, qt, ftadvStrong );
  pde.strongFluxAdvectiveTime( dist, x, y, time, q, qt, ftadvStrongTrue );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(0), ftadvStrong(0), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(1), ftadvStrong(1), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(2), ftadvStrong(2), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(3), ftadvStrong(3), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(4), ftadvStrong(4), tol );

  Real rhoL = 1.137, uL = 0.784, vL = -0.231, tL = 0.987, nutL = 4.2;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, tR = 0.865, nutR = 5.7;
  Real nx = 2.0/7.0; Real ny = sqrt(1.0-nx*nx);
  Real nt = 1.0;

  ArrayQ qL = 0, qR = 0;
  avpde.setDOFFrom( qL, SAnt2D<DensityVelocityTemperature2D<Real>>({rhoL, uL, vL, tL, nutL}) );
  avpde.setDOFFrom( qR, SAnt2D<DensityVelocityTemperature2D<Real>>({rhoR, uR, vR, tR, nutR}) );

  // upwinded advective flux
  f = 0;
  fTrue = 0;
  avpde.fluxAdvectiveUpwind( param, x, y, time, qL, qR, nx, ny, f );
  pde.fluxAdvectiveUpwind( dist, x, y, time, qL, qR, nx, ny, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );

  // Right now fluxAdvectiveUpwindSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // upwinded space-time advective flux
  ft = 0;
  ftTrue = 0;
  avpde.fluxAdvectiveUpwindSpaceTime( param, x, y, time, qL, qR, nx, ny, nt, ft );
  pde.fluxAdvectiveUpwindSpaceTime( x, y, time, qL, qR, nx, ny, nt, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( ftTrue(4), ft(4), tol );
#else
  // absolute value of Jacobian of advective flux
  ft = 0;
  ftTrue = 0;
  BOOST_CHECK_THROW(avpde.fluxAdvectiveUpwindSpaceTime( param, x, y, time, qL, qR, nx, ny, nt, ft ), DeveloperException);
  BOOST_CHECK_THROW(pde.fluxAdvectiveUpwindSpaceTime( x, y, time, qL, qR, nx, ny, nt, ftTrue ), DeveloperException);
#endif

  // absolute value of Jacobian of advective flux
  MatrixQ Jadv = 0;
  MatrixQ JadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValue( param, x, y, time, q, nx, ny, Jadv );
  pde.jacobianFluxAdvectiveAbsoluteValue( dist, x, y, time, q, nx, ny, JadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JadvTrue(i,j), Jadv(i,j), small_tol, close_tol )
    }
  }

  // Right now jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // absolute value of Jacobian of advective flux
  MatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, time, q, nx, ny, nt, Jtadv );
  pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( dist, x, y, time, q, nx, ny, nt, JtadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtadvTrue(i,j), Jtadv(i,j), small_tol, close_tol )
    }
  }
#else
  // absolute value of Jacobian of advective flux
  MatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  BOOST_CHECK_THROW(avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, time, q, nx, ny, nt, Jtadv ), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( dist, x, y, time, q, nx, ny, nt, JtadvTrue ), DeveloperException);
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( masterState, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 5 );

  // function tests

  Real rho, u, v, t, nut;

  rho = 1.137; u = 0.784; v = 0.821; t = 0.987; nut = 4.2;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, time = 0;

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, SAnt2D<DensityVelocityTemperature2D<Real>>({rho, u, v, t, nut}) );

  // conservative flux
  ArrayQ uTrue = 0;
  ArrayQ uCons = 0;
  avpde.masterState( param, x, y, time, q, uCons );
  pde.masterState( dist, x, y, time, q, uTrue );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(4), uCons(4), small_tol, close_tol )

  // conservative flux
  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;
  avpde.jacobianMasterState( param, x, y, time, q, dudq );
  pde.jacobianMasterState( dist, x, y, time, q, dudqTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), small_tol, close_tol )
    }
  }
}

// Cannot do viscous flux checks here... should probably refactor things to be able to...

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, y, time;
  Real rhoL, uL, vL,tL, nutL;
  Real rhoR, uR, vR, tR, nutR;

  x = y = time = 0;   // not actually used in functions
  rhoL = 1.034; uL = 3.26; vL = 1.23; tL = 5.78, nutL = 4.2;
  rhoR = 0.973; uR = 1.79; vR = 0.34; tR = 6.13, nutR = 5.7;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  // set
  ArrayQ qL = 0, qR = 0;
  avpde.setDOFFrom( qL, SAnt2D<DensityVelocityTemperature2D<Real>>({rhoL, uL, vL, tL, nutL}) );
  avpde.setDOFFrom( qR, SAnt2D<DensityVelocityTemperature2D<Real>>({rhoR, uR, vR, tR, nutR}) );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19, 1.32};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21, 0.32};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26, -1.65};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15, 0.43};

  Real lifted_quantity = 1.23;

  // vanilla source term
  ArrayQ s = 0;
  ArrayQ sTrue = 0;
  avpde.source( param, x, y, time, qL, qxL, qyL, s );
  pde.source( dist, x, y, time, qL, qxL, qyL, sTrue );
  SANS_CHECK_CLOSE( sTrue(0), s(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(1), s(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(2), s(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(3), s(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(4), s(4), small_tol, close_tol )

  // source term with lifted quantity
  s = 0;
  sTrue = 0;
  avpde.source( param, x, y, time, lifted_quantity, qL, qxL, qyL, s );
  pde.source( dist, x, y, time, lifted_quantity, qL, qxL, qyL, sTrue );
  SANS_CHECK_CLOSE( sTrue(0), s(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(1), s(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(2), s(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(3), s(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(4), s(4), small_tol, close_tol )

  // Right now sourceTrace is not implemented for RANSSA (which is probably a good thing),
  // if  it is, this is the test we want
#if 1
  // trace source term with lifted quantity
  ArrayQ sL = 0, sR = 0;
  ArrayQ sLTrue = 0, sRTrue = 0;
  avpde.sourceTrace( param, x, y, param, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sL, sR );
  pde.sourceTrace( dist, x, y, dist, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sLTrue, sRTrue );
  SANS_CHECK_CLOSE( sLTrue(0), sL(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(1), sL(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(2), sL(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(3), sL(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(4), sL(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(0), sR(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(1), sR(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(2), sR(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(3), sR(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(4), sR(4), small_tol, close_tol )
#else
  // trace source term with lifted quantity
  ArrayQ sL = 0, sR = 0;
  ArrayQ sLTrue = 0, sRTrue = 0;
  BOOST_CHECK_THROW(avpde.sourceTrace( param, x, y, param, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sL, sR ), DeveloperException);
  BOOST_CHECK_THROW(pde.sourceTrace( dist, x, y, dist, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sLTrue, sRTrue ), DeveloperException);
#endif

  // source for lifted quantity
  s = 0;
  sTrue = 0;
  avpde.sourceLiftedQuantity( param, x, y, param, x, y, time, qL, qR, s );
  pde.sourceLiftedQuantity( dist, x, y, dist, x, y, time, qL, qR, sTrue );
  SANS_CHECK_CLOSE( sTrue(0), s(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(1), s(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(2), s(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(3), s(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(4), s(4), small_tol, close_tol )

  // source Jacobian
  MatrixQ Js = 0;
  MatrixQ JsTrue = 0;
  avpde.jacobianSource( param, x, y, time, qL, qxL, qyL, Js );
  pde.jacobianSource( dist, x, y, time, qL, qxL, qyL, JsTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JsTrue(i,j), Js(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSA, TraitsModelRANSSAClass, PDERANSSA2D> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, visc, tcond, Euler_ResidInterp_Raw);

  Real x, y, time, dx, dy;
  Real rho, u, v, t, nut;
  Real speed, speedTrue;

  x = y = time = 0;   // not actually used in functions
  dx = 0.57; dy = 1.23;
  rho = 1.137; u = 0.784; v = 2.31; t = 0.987; nut = 5.254;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real qDataPrim[5] = {rho, u, v, t, nut};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "Temperature", "SANutilde"};
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  avpde.speedCharacteristic( param, x, y, time, dx, dy, q, speed );
  pde.speedCharacteristic( dist, x, y, time, dx, dy, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( param, x, y, time, q, speed );
  pde.speedCharacteristic( dist, x, y, time, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
