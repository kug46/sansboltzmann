// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEmitAVSensor3D_Consistency_RANSSA_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsRANSSAArtificialViscosity.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DConservative.h"
//#include "pde/NS/Q3DEntropy.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/PDERANSSA3D.h"
#include "pde/NS/PDEEulermitAVDiffusion3D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
//class QTypeEntropy {};
}

namespace SANS
{

template <class T>
using SAnt3D_rhovT = SAnt3D<DensityVelocityTemperature3D<T>>;

}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEmitAVSensor3D_Consistency_RANSSA_btest )

typedef boost::mpl::list< QTypePrimitiveRhoPressure > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  BOOST_REQUIRE( AVPDEClass::D == PDEClass::D );
  BOOST_REQUIRE( AVPDEClass::N == PDEClass::N+1 );
  BOOST_REQUIRE( AVArrayQ::M == ArrayQ::M+1 );
  BOOST_REQUIRE( AVMatrixQ::M == MatrixQ::M+1 );
  BOOST_REQUIRE( AVMatrixQ::N == MatrixQ::N+1 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == pde.D );
  BOOST_REQUIRE( avpde.N == pde.N+1 );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == pde.hasFluxAdvectiveTime() );
  BOOST_CHECK( avpde.hasFluxAdvective() == pde.hasFluxAdvective() );
  BOOST_CHECK( avpde.hasFluxViscous() == (pde.hasFluxViscous() or true) );
  BOOST_CHECK( avpde.hasSource() == (pde.hasSource() or true) );
  BOOST_CHECK( avpde.hasSourceTrace() == pde.hasSourceTrace() );
  BOOST_CHECK( avpde.hasForcingFunction() == pde.hasSourceTrace() );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == pde.fluxViscousLinearInGradient() );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == pde.needsSolutionGradientforSource() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxAdvective_zeroSensor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-11;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, nut, s;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = 0.821; w = 0.423; t = 0.987; nut = 4.2; s = 0;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nut}) );
  AVVariable<SAnt3D_rhovT,Real> qdata({rho, u, v, w, t, nut}, s);
  AVArrayQ avq = 0;
  avpde.setDOFFrom( avq, qdata );

  // flux in time direction
  ArrayQ ftTrue = 0;
  AVArrayQ ft = 0;
  avpde.fluxAdvectiveTime( param, x, y, z, time, avq, ft );
  pde.fluxAdvectiveTime( dist, x, y, z, time, q, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( ftTrue(4), ft(4), tol );
  BOOST_CHECK_CLOSE( ftTrue(5), ft(5), tol );

  // Jacobian of flux in time direction
  AVMatrixQ Jt = 0;
  MatrixQ JtTrue = 0;
  avpde.jacobianFluxAdvectiveTime( param, x, y, z, time, avq, Jt );
  pde.jacobianFluxAdvectiveTime( dist, x, y, z, time, q, JtTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtTrue(i,j), Jt(i,j), small_tol, close_tol )
    }
  }

  // advective flux
  ArrayQ fTrue = 0, gTrue = 0, hTrue = 0;
  AVArrayQ f = 0, g = 0, h = 0;
  avpde.fluxAdvective( param, x, y, z, time, avq, f, g, h );
  pde.fluxAdvective( dist, x, y, z, time, q, fTrue, gTrue, hTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );
  BOOST_CHECK_CLOSE( fTrue(5), f(5), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );
  BOOST_CHECK_CLOSE( gTrue(4), g(4), tol );
  BOOST_CHECK_CLOSE( gTrue(5), g(5), tol );
  BOOST_CHECK_CLOSE( hTrue(0), h(0), tol );
  BOOST_CHECK_CLOSE( hTrue(1), h(1), tol );
  BOOST_CHECK_CLOSE( hTrue(2), h(2), tol );
  BOOST_CHECK_CLOSE( hTrue(3), h(3), tol );
  BOOST_CHECK_CLOSE( hTrue(4), h(4), tol );
  BOOST_CHECK_CLOSE( hTrue(5), h(5), tol );

  // Jacobian of advective flux
  AVMatrixQ Jxadv = 0, Jyadv = 0, Jzadv = 0;
  MatrixQ JxadvTrue = 0, JyadvTrue = 0, JzadvTrue = 0;
  avpde.jacobianFluxAdvective( param, x, y, z, time, avq, Jxadv, Jyadv, Jzadv );
  pde.jacobianFluxAdvective( dist, x, y, z, time, q, JxadvTrue, JyadvTrue, JzadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JxadvTrue(i,j), Jxadv(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( JyadvTrue(i,j), Jyadv(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( JzadvTrue(i,j), Jzadv(i,j), small_tol, close_tol )
    }
  }

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 0.43, 3.12};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 0.85, 7.12};
  ArrayQ qz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86};
  ArrayQ qt = {0.21, 0.31, -0.14, 0.26, 0.63, -0.12};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.43, 3.12, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.85, 7.12, 0.0};
  AVArrayQ avqz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86, 0.0};
  AVArrayQ avqt = {0.21, 0.31, -0.14, 0.26, 0.63, -0.12, 0.0};

  // strong advective flux
  ArrayQ fadvStrongTrue = 0;
  AVArrayQ fadvStrong = 0;
  avpde.strongFluxAdvective( param, x, y, z, time, avq, avqx, avqy, avqz, fadvStrong );
  pde.strongFluxAdvective( dist, x, y, z, time, q, qx, qy, qz, fadvStrongTrue );
  BOOST_CHECK_CLOSE( fadvStrongTrue(0), fadvStrong(0), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(1), fadvStrong(1), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(2), fadvStrong(2), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(3), fadvStrong(3), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(4), fadvStrong(4), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(5), fadvStrong(5), tol );

  // strong advective flux
  ArrayQ ftadvStrongTrue = 0;
  AVArrayQ ftadvStrong = 0;
  avpde.strongFluxAdvectiveTime( param, x, y, z, time, avq, avqt, ftadvStrong );
  pde.strongFluxAdvectiveTime( dist, x, y, z, time, q, qt, ftadvStrongTrue );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(0), ftadvStrong(0), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(1), ftadvStrong(1), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(2), ftadvStrong(2), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(3), ftadvStrong(3), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(4), ftadvStrong(4), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(5), ftadvStrong(5), tol );

  Real rhoL = 1.137, uL = 0.784, vL = -0.231, wL = 0.322, tL = 0.987, nutL = 4.2, sL = 0.0;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, wR = 0.932, tR = 0.865, nutR = 5.7, sR = 0.0;
  Real nx = 2.0/7.0; Real ny = 0.3; Real nz = sqrt(1.0-nx*nx-ny*ny);
  Real nt = 1.0;

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoL, uL, vL, wL, tL, nutL}) );
  pde.setDOFFrom( qR, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoR, uR, vR, wR, tR, nutR}) );
  AVArrayQ avqL = 0, avqR = 0;
  AVVariable<SAnt3D_rhovT,Real> qdataL({rhoL, uL, vL, wL, tL, nutL}, sL);
  AVVariable<SAnt3D_rhovT,Real> qdataR({rhoR, uR, vR, wR, tR, nutR}, sR);
  avpde.setDOFFrom( avqL, qdataL );
  avpde.setDOFFrom( avqR, qdataR );

  // upwinded advective flux
  f = 0;
  fTrue = 0;
  avpde.fluxAdvectiveUpwind( param, x, y, z, time, avqL, avqR, nx, ny, nz, f );
  pde.fluxAdvectiveUpwind( dist, x, y, z, time, qL, qR, nx, ny, nz, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );
  BOOST_CHECK_CLOSE( fTrue(5), f(5), tol );

  // Right now fluxAdvectiveUpwindSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // upwinded space-time advective flux
  ft = 0;
  ftTrue = 0;
  avpde.fluxAdvectiveUpwindSpaceTime( param, x, y, z, time, avqL, avqR, nx, ny, nz, nt, ft );
  pde.fluxAdvectiveUpwindSpaceTime( dist, x, y, z, time, qL, qR, nx, ny, nz, nt, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( ftTrue(4), ft(4), tol );
  BOOST_CHECK_CLOSE( ftTrue(5), ft(5), tol );
#else
  // upwinded space-time advective flux
  ft = 0;
  ftTrue = 0;
  BOOST_CHECK_THROW(avpde.fluxAdvectiveUpwindSpaceTime( param, x, y, z, time, avqL, avqR, nx, ny, nz, nt, ft ), DeveloperException);
  BOOST_CHECK_THROW(pde.fluxAdvectiveUpwindSpaceTime( dist, x, y, z, time, qL, qR, nx, ny, nz, nt, ftTrue ), DeveloperException);
#endif

  // absolute value of Jacobian of advective flux
  AVMatrixQ Jadv = 0;
  MatrixQ JadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValue( param, x, y, z, time, avq, nx, ny, nz, Jadv );
  pde.jacobianFluxAdvectiveAbsoluteValue( dist, x, y, z, time, q, nx, ny, nz, JadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JadvTrue(i,j), Jadv(i,j), small_tol, close_tol )
    }
  }

  // Right now jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // absolute value of Jacobian of advective flux
  AVMatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, z, time, avq, nx, ny, nz, nt, Jtadv );
  pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( dist, x, y, z, time, q, nx, ny, nz, nt, JtadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtadvTrue(i,j), Jtadv(i,j), small_tol, close_tol )
    }
  }
#else
  // absolute value of Jacobian of advective flux
  AVMatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  BOOST_CHECK_THROW(avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, z, time, avq, nx, ny, nz, nt, Jtadv ), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( dist, x, y, z, time, q, nx, ny, nz, nt, JtadvTrue ), DeveloperException);
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxAdvective_nonZeroSensor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-13;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, z, time;
  Real rho, u, v, w, t, nut, s;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = 0.821; w = 0.432; t = 0.987; nut = 4.2; s = 0.23;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nut}) );
  AVVariable<SAnt3D_rhovT,Real> qdata({rho, u, v, w, t, nut}, s);
  AVArrayQ avq = 0;
  avpde.setDOFFrom( avq, qdata );

  // flux in time direction
  ArrayQ ftTrue = 0;
  AVArrayQ ft = 0;
  avpde.fluxAdvectiveTime( param, x, y, z, time, avq, ft );
  pde.fluxAdvectiveTime( dist, x, y, z, time, q, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( ftTrue(4), ft(4), tol );
  BOOST_CHECK_CLOSE( ftTrue(5), ft(5), tol );

  // Jacobian of flux in time direction
  AVMatrixQ Jt = 0;
  MatrixQ JtTrue = 0;
  avpde.jacobianFluxAdvectiveTime( param, x, y, z, time, avq, Jt );
  pde.jacobianFluxAdvectiveTime( dist, x, y, z, time, q, JtTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtTrue(i,j), Jt(i,j), small_tol, close_tol )
    }
  }

  // advective flux
  ArrayQ fTrue = 0, gTrue = 0, hTrue = 0;
  AVArrayQ f = 0, g = 0, h = 0;
  avpde.fluxAdvective( param, x, y, z, time, avq, f, g, h );
  pde.fluxAdvective( dist, x, y, z, time, q, fTrue, gTrue, hTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );
  BOOST_CHECK_CLOSE( fTrue(5), f(5), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );
  BOOST_CHECK_CLOSE( gTrue(4), g(4), tol );
  BOOST_CHECK_CLOSE( gTrue(5), g(5), tol );
  BOOST_CHECK_CLOSE( hTrue(0), h(0), tol );
  BOOST_CHECK_CLOSE( hTrue(1), h(1), tol );
  BOOST_CHECK_CLOSE( hTrue(2), h(2), tol );
  BOOST_CHECK_CLOSE( hTrue(3), h(3), tol );
  BOOST_CHECK_CLOSE( hTrue(4), h(4), tol );
  BOOST_CHECK_CLOSE( hTrue(5), h(5), tol );

  // Jacobian of advective flux
  AVMatrixQ Jxadv = 0, Jyadv = 0, Jzadv = 0;
  MatrixQ JxadvTrue = 0, JyadvTrue = 0, JzadvTrue = 0;
  avpde.jacobianFluxAdvective( param, x, y, z, time, avq, Jxadv, Jyadv, Jzadv );
  pde.jacobianFluxAdvective( dist, x, y, z, time, q, JxadvTrue, JyadvTrue, JzadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JxadvTrue(i,j), Jxadv(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( JyadvTrue(i,j), Jyadv(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( JzadvTrue(i,j), Jzadv(i,j), small_tol, close_tol )
    }
  }

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 0.43, 3.12};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 0.85, 7.12};
  ArrayQ qz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86};
  ArrayQ qt = {0.21, 0.31, -0.14, 0.26, 0.63, -0.12};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.43, 3.12, 0.35};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.85, 7.12, 0.54};
  AVArrayQ avqz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86, 0.63};
  AVArrayQ avqt = {0.21, 0.31, -0.14, 0.26, 0.63, -0.12, 0.23};

  // strong advective flux
  ArrayQ fadvStrongTrue = 0;
  AVArrayQ fadvStrong = 0;
  avpde.strongFluxAdvective( param, x, y, z, time, avq, avqx, avqy, avqz, fadvStrong );
  pde.strongFluxAdvective( dist, x, y, z, time, q, qx, qy, qz, fadvStrongTrue );
  BOOST_CHECK_CLOSE( fadvStrongTrue(0), fadvStrong(0), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(1), fadvStrong(1), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(2), fadvStrong(2), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(3), fadvStrong(3), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(4), fadvStrong(4), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(5), fadvStrong(5), tol );

  // strong advective flux
  ArrayQ ftadvStrongTrue = 0;
  AVArrayQ ftadvStrong = 0;
  avpde.strongFluxAdvectiveTime( param, x, y, z, time, avq, avqt, ftadvStrong );
  pde.strongFluxAdvectiveTime( dist, x, y, z, time, q, qt, ftadvStrongTrue );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(0), ftadvStrong(0), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(1), ftadvStrong(1), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(2), ftadvStrong(2), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(3), ftadvStrong(3), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(4), ftadvStrong(4), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(5), ftadvStrong(5), tol );

  Real rhoL = 1.137, uL = 0.784, vL = -0.231, wL = 0.322, tL = 0.987, nutL = 4.2, sL = 0.3;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, wR = 0.932, tR = 0.865, nutR = 5.7, sR = 0.5;
  Real nx = 2.0/7.0; Real ny = 0.3; Real nz = sqrt(1.0-nx*nx-ny*ny);
  Real nt = 1.0;

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoL, uL, vL, wL, tL, nutL}) );
  pde.setDOFFrom( qR, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoR, uR, vR, wR, tR, nutR}) );
  AVArrayQ avqL = 0, avqR = 0;
  AVVariable<SAnt3D_rhovT,Real> qdataL({rhoL, uL, vL, wL, tL, nutL}, sL);
  AVVariable<SAnt3D_rhovT,Real> qdataR({rhoR, uR, vR, wR, tR, nutR}, sR);
  avpde.setDOFFrom( avqL, qdataL );
  avpde.setDOFFrom( avqR, qdataR );

  // upwinded advective flux
  f = 0;
  fTrue = 0;
  avpde.fluxAdvectiveUpwind( param, x, y, z, time, avqL, avqR, nx, ny, nz, f );
  pde.fluxAdvectiveUpwind( dist, x, y, z, time, qL, qR, nx, ny, nz, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );
  BOOST_CHECK_CLOSE( fTrue(5), f(5), tol );

  // Right now fluxAdvectiveUpwindSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // upwinded space-time advective flux
  ft = 0;
  ftTrue = 0;
  avpde.fluxAdvectiveUpwindSpaceTime( param, x, y, z, time, avqL, avqR, nx, ny, nz, nt, ft );
  pde.fluxAdvectiveUpwindSpaceTime( dist, x, y, z, time, qL, qR, nx, ny, nz, nt, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( ftTrue(4), ft(4), tol );
  BOOST_CHECK_CLOSE( ftTrue(5), ft(5), tol );
#else
  // upwinded space-time advective flux
  ft = 0;
  ftTrue = 0;
  BOOST_CHECK_THROW(avpde.fluxAdvectiveUpwindSpaceTime( param, x, y, z, time, avqL, avqR, nx, ny, nz, nt, ft ), DeveloperException);
  BOOST_CHECK_THROW(pde.fluxAdvectiveUpwindSpaceTime( dist, x, y, z, time, qL, qR, nx, ny, nz, nt, ftTrue ), DeveloperException);
#endif

  // absolute value of Jacobian of advective flux
  AVMatrixQ Jadv = 0;
  MatrixQ JadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValue( param, x, y, z, time, avq, nx, ny, nz, Jadv );
  pde.jacobianFluxAdvectiveAbsoluteValue( dist, x, y, z, time, q, nx, ny, nz, JadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JadvTrue(i,j), Jadv(i,j), small_tol, close_tol )
    }
  }

  // Right now jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // absolute value of Jacobian of advective flux
  AVMatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, z, time, avq, nx, ny, nz, nt, Jtadv );
  pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( dist, x, y, z, time, q, nx, ny, nz, nt, JtadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtadvTrue(i,j), Jtadv(i,j), small_tol, close_tol )
    }
  }
#else
  // absolute value of Jacobian of advective flux
  AVMatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  BOOST_CHECK_THROW(avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, z, time, avq, nx, ny, nz, nt, Jtadv ), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( dist, x, y, z, time, q, nx, ny, nz, nt, JtadvTrue ), DeveloperException);
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxViscous_zeroSensor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-13;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real nx = 2.0/7.0; Real ny = 0.3; Real nz = sqrt(1.0-nx*nx-ny*ny);

  x = y = z = time = 0;   // not actually used in functions
  Real rhoL = 1.137, uL = 0.784, vL = -0.231, wL = 0.322, tL = 0.987, nutL = 4.2, sL = 0.0;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, wR = 0.932, tR = 0.865, nutR = 5.7, sR = 0.0;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoL, uL, vL, wL, tL, nutL}) );
  pde.setDOFFrom( qR, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoR, uR, vR, wR, tR, nutR}) );
  AVArrayQ avqL = 0, avqR = 0;
  AVVariable<SAnt3D_rhovT,Real> qdataL({rhoL, uL, vL, wL, tL, nutL}, sL);
  AVVariable<SAnt3D_rhovT,Real> qdataR({rhoR, uR, vR, wR, tR, nutR}, sR);
  avpde.setDOFFrom( avqL, qdataL );
  avpde.setDOFFrom( avqR, qdataR );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19, 0.41, 0.32};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21, 0.52, 0.86};
  ArrayQ qzL = {1.43, 0.23, 0.34, -0.41, 1.24, 0.34};
  AVArrayQ avqxL = {0.17, 0.21, -0.34, 0.19, 0.41, 0.32, 0.0};
  AVArrayQ avqyL = {-0.05, 0.61, 0.12, -0.21, 0.52, 0.86, 0.0};
  AVArrayQ avqzL = {1.43, 0.23, 0.34, -0.41, 1.24, 0.34, 0.0};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26, 0.43, 0.63};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15, 0.91, 0.98};
  ArrayQ qzR = {-0.15, 0.52, 0.73, -0.23, 0.14, 0.45};
  AVArrayQ avqxR = {0.21, 0.31, -0.14, 0.26, 0.43, 0.63, 0.0};
  AVArrayQ avqyR = {0.11, 0.48, 0.27, -0.15, 0.91, 0.98, 0.0};
  AVArrayQ avqzR = {-0.15, 0.52, 0.73, -0.23, 0.14, 0.45, 0.0};

  ArrayQ qxx = {1.25, 5.42, 0.43, 8.45, 0.32, 8.62};
  ArrayQ qxy = {0.35, 3.62, 5.43, 3.23, 5.32, 8.25};
  ArrayQ qyy = {4.23, 3.35, 7.43, 0.32, 4.65, 0.47};
  ArrayQ qxz = {0.34, -1.12, 0.45, 0.23, 9.89, 1.95};
  ArrayQ qyz = {0.55, 3.54, 0.85, 0.17, 1.32, 2.84};
  ArrayQ qzz = {0.72, 0.49, 0.03, 0.82, 0.91, 5.26};
  AVArrayQ avqxx = {1.25, 5.42, 0.43, 8.45, 0.32, 8.62, 0.0};
  AVArrayQ avqxy = {0.35, 3.62, 5.43, 3.23, 5.32, 8.25, 0.0};
  AVArrayQ avqyy = {4.23, 3.35, 7.43, 0.32, 4.65, 0.47, 0.0};
  AVArrayQ avqxz = {0.34, -1.12, 0.45, 0.23, 9.89, 1.95, 0.0};
  AVArrayQ avqyz = {0.55, 3.54, 0.85, 0.17, 1.32, 2.84, 0.0};
  AVArrayQ avqzz = {0.72, 0.49, 0.03, 0.82, 0.91, 5.26, 0.0};

  // spacetime viscous flux
  ArrayQ fTrue = 0, gTrue = 0, hTrue = 0;
  AVArrayQ f = 0, g = 0, h = 0;
  avpde.fluxViscous( param, x, y, z, time, avqR, avqxR, avqyR, avqzR, f, g, h );
  pde.fluxViscous( dist, x, y, z, time, qR, qxR, qyR, qzR, fTrue, gTrue, hTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );
  BOOST_CHECK_CLOSE( fTrue(5), f(5), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );
  BOOST_CHECK_CLOSE( gTrue(4), g(4), tol );
  BOOST_CHECK_CLOSE( gTrue(5), g(5), tol );
  BOOST_CHECK_CLOSE( hTrue(0), h(0), tol );
  BOOST_CHECK_CLOSE( hTrue(1), h(1), tol );
  BOOST_CHECK_CLOSE( hTrue(2), h(2), tol );
  BOOST_CHECK_CLOSE( hTrue(3), h(3), tol );
  BOOST_CHECK_CLOSE( hTrue(4), h(4), tol );
  BOOST_CHECK_CLOSE( hTrue(5), h(5), tol );

  // central viscous flux
  fTrue = 0;
  f = 0;
  avpde.fluxViscous( param, param, x, y, z, time, avqL, avqxL, avqyL, avqzL, avqR, avqxR, avqyR, avqzR, nx, ny, nz, f );
  pde.fluxViscous( dist, dist, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fTrue );

  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );
  BOOST_CHECK_CLOSE( fTrue(5), f(5), tol );

  // diffusion coeficients
  AVMatrixQ kxx = 0, kxy = 0, kxz = 0;
  AVMatrixQ kyx = 0, kyy = 0, kyz = 0;
  AVMatrixQ kzx = 0, kzy = 0, kzz = 0;
  MatrixQ kxxTrue = 0, kxyTrue = 0, kxzTrue = 0;
  MatrixQ kyxTrue = 0, kyyTrue = 0, kyzTrue = 0;
  MatrixQ kzxTrue = 0, kzyTrue = 0, kzzTrue = 0;
  avpde.diffusionViscous( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, kxx, kxy, kxy, kyx, kyy, kyz, kzx, kzy, kzz );
  pde.diffusionViscous( dist, x, y, z, time, qL, qxL, qyL, qzL, kxxTrue, kxyTrue, kxyTrue, kyxTrue, kyyTrue, kyzTrue, kzxTrue, kzyTrue, kzzTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx(i,j), kxxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy(i,j), kxyTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxz(i,j), kxzTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx(i,j), kyxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyy(i,j), kyyTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyz(i,j), kyzTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzx(i,j), kzxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzy(i,j), kzyTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzz(i,j), kzzTrue(i,j), small_tol, close_tol )
    }
  }

  // diffusion coefficients
  AVMatrixQ dfdu = 0, dgdu = 0, dhdu = 0;
  MatrixQ dfduTrue = 0, dgduTrue = 0, dhduTrue = 0;
  avpde.jacobianFluxViscous( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, dfdu, dgdu, dhdu );
  pde.jacobianFluxViscous( dist, x, y, z, time, qL, qxL, qyL, qzL, dfduTrue, dgduTrue, dhduTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dfdu(i,j), dfduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dgdu(i,j), dgduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dhdu(i,j), dhduTrue(i,j), small_tol, close_tol )
    }
  }

  // strong viscous flux
  ArrayQ fviscStrongTrue = 0;
  AVArrayQ fviscStrong = 0;
  avpde.strongFluxViscous( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, avqxx, avqxy, avqyy, avqxz, avqyz, avqzz, fviscStrong );
  pde.strongFluxViscous( dist, x, y, z, time, qL, qxL, qyL, qzL, qxx, qxy, qyy, qxz, qyz, qzz, fviscStrongTrue );
  BOOST_CHECK_CLOSE( fviscStrongTrue(0), fviscStrong(0), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(1), fviscStrong(1), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(2), fviscStrong(2), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(3), fviscStrong(3), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(4), fviscStrong(4), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(5), fviscStrong(5), tol );

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  AVMatrixQ kxx_x = 0, kxy_x = 0, kxz_x = 0, kyx_x = 0, kzx_x = 0;
  AVMatrixQ kxy_y = 0, kyx_y = 0, kyy_y = 0, kyz_y = 0, kzy_y = 0;
  AVMatrixQ kxz_z = 0, kyz_z = 0, kzx_z = 0, kzy_z = 0, kzz_z = 0;
  MatrixQ kxx_xTrue = 0, kxy_xTrue = 0, kxz_xTrue = 0, kyx_xTrue = 0, kzx_xTrue = 0;
  MatrixQ kxy_yTrue = 0, kyx_yTrue = 0, kyy_yTrue = 0, kyz_yTrue = 0, kzy_yTrue = 0;
  MatrixQ kxz_zTrue = 0, kyz_zTrue = 0, kzx_zTrue = 0, kzy_zTrue = 0, kzz_zTrue = 0;
  avpde.diffusionViscousGradient( param, x, y, z, time, avqL, avqxL, avqyL, avqzL,
                                  avqxx, avqxy, avqyy, avqxz, avqyz, avqzz,
                                  kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                  kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                  kxz_z, kyz_z, kzx_z, kzy_z, kzz_z );
  pde.diffusionViscousGradient( dist, x, y, z, time, qL, qxL, qyL, qzL,
                                qxx, qxy, qyy, qxz, qyz, qzz,
                                kxx_xTrue, kxy_xTrue, kxz_xTrue, kyx_xTrue, kzx_xTrue,
                                kxy_yTrue, kyx_yTrue, kyy_yTrue, kyz_yTrue, kzy_yTrue,
                                kxz_zTrue, kyz_zTrue, kzx_zTrue, kzy_zTrue, kzz_zTrue);

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxz_x(i,j), kxz_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzx_x(i,j), kzx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyz_y(i,j), kyz_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzy_y(i,j), kzy_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxz_z(i,j), kxz_zTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyz_z(i,j), kyz_zTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzx_z(i,j), kzx_zTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzy_z(i,j), kzy_zTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzz_z(i,j), kzz_zTrue(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxViscous_nonZeroSensor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 2.e-13;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real nx = 2.0/7.0; Real ny = 0.3; Real nz = sqrt(1.0-nx*nx-ny*ny);

  x = y = z = time = 0;   // not actually used in functions
  Real rhoL = 1.137, uL = 0.784, vL = -0.231, wL = 0.322, tL = 0.987, nutL = 4.2, sL = 0.3;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, wR = 0.932, tR = 0.865, nutR = 5.7, sR = 0.5;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoL, uL, vL, wL, tL, nutL}) );
  pde.setDOFFrom( qR, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoR, uR, vR, wR, tR, nutR}) );
  AVArrayQ avqL = 0, avqR = 0;
  AVVariable<SAnt3D_rhovT,Real> qdataL({rhoL, uL, vL, wL, tL, nutL}, sL);
  AVVariable<SAnt3D_rhovT,Real> qdataR({rhoR, uR, vR, wR, tR, nutR}, sR);
  avpde.setDOFFrom( avqL, qdataL );
  avpde.setDOFFrom( avqR, qdataR );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19, 0.41, 0.32};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21, 0.52, 0.86};
  ArrayQ qzL = {1.43, 0.23, 0.34, -0.41, 1.24, 0.34};
  AVArrayQ avqxL = {0.17, 0.21, -0.34, 0.19, 0.41, 0.32, 0.56};
  AVArrayQ avqyL = {-0.05, 0.61, 0.12, -0.21, 0.52, 0.86, 0.32};
  AVArrayQ avqzL = {1.43, 0.23, 0.34, -0.41, 1.24, 0.34, 0.79};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26, 0.43, 0.63};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15, 0.91, 0.98};
  ArrayQ qzR = {-0.15, 0.52, 0.73, -0.23, 0.14, 0.45};
  AVArrayQ avqxR = {0.21, 0.31, -0.14, 0.26, 0.43, 0.63, 0.45};
  AVArrayQ avqyR = {0.11, 0.48, 0.27, -0.15, 0.91, 0.98, 0.36};
  AVArrayQ avqzR = {-0.15, 0.52, 0.73, -0.23, 0.14, 0.45, 0.84};

  ArrayQ qxx = {1.25, 5.42, 0.43, 8.45, 0.32, 8.62};
  ArrayQ qxy = {0.35, 3.62, 5.43, 3.23, 5.32, 8.25};
  ArrayQ qyy = {4.23, 3.35, 7.43, 0.32, 4.65, 0.47};
  ArrayQ qxz = {0.34, -1.12, 0.45, 0.23, 9.89, 1.95};
  ArrayQ qyz = {0.55, 3.54, 0.85, 0.17, 1.32, 2.84};
  ArrayQ qzz = {0.72, 0.49, 0.03, 0.82, 0.91, 5.26};
  AVArrayQ avqxx = {1.25, 5.42, 0.43, 8.45, 0.32, 8.62, 0.32};
  AVArrayQ avqxy = {0.35, 3.62, 5.43, 3.23, 5.32, 8.25, 0.87};
  AVArrayQ avqyy = {4.23, 3.35, 7.43, 0.32, 4.65, 0.47, 0.35};
  AVArrayQ avqxz = {0.34, -1.12, 0.45, 0.23, 9.89, 1.95, 0.65};
  AVArrayQ avqyz = {0.55, 3.54, 0.85, 0.17, 1.32, 2.84, 0.87};
  AVArrayQ avqzz = {0.72, 0.49, 0.03, 0.82, 0.91, 5.26, 0.89};

  MatrixQ kxxLT = 0, kxyLT = 0, kxzLT = 0;
  MatrixQ kyxLT = 0, kyyLT = 0, kyzLT = 0;
  MatrixQ kzxLT = 0, kzyLT = 0, kzzLT = 0;
  MatrixQ kxxRT = 0, kxyRT = 0, kxzRT = 0;
  MatrixQ kyxRT = 0, kyyRT = 0, kyzRT = 0;
  MatrixQ kzxRT = 0, kzyRT = 0, kzzRT = 0;

  MatrixQ kxxL_AV = 0, kxyL_AV = 0, kxzL_AV = 0;
  MatrixQ kyxL_AV = 0, kyyL_AV = 0, kyzL_AV = 0;
  MatrixQ kzxL_AV = 0, kzyL_AV = 0, kzzL_AV = 0;
  MatrixQ kxxR_AV = 0, kxyR_AV = 0, kxzR_AV = 0;
  MatrixQ kyxR_AV = 0, kyyR_AV = 0, kyzR_AV = 0;
  MatrixQ kzxR_AV = 0, kzyR_AV = 0, kzzR_AV = 0;

  Real lambdaL = 0, lambdaR = 0;
  pde.speedCharacteristic( x, y, z, time, qL, lambdaL );
  pde.speedCharacteristic( x, y, z, time, qR, lambdaR );

#if 0
  Real theta_L = 0.001;
  Real theta_H = 1.0;
  sensor = smoothActivation_sine(sensor, theta_L, theta_H);
#else
//  Real alpha = 500;
//  Real eps_exp = 1e-10;
//
//  sensor = smoothActivation_exp(sensor, alpha, eps_exp);


  Real zz = 0.0;
  Real sensorL = std::max(sL, zz);
  Real sensorR = std::max(sR, zz);
#endif

  Real factorL = 2.0/(Real(order)) * lambdaL * sensorL; //smoothabs0(sensorL, 1.0e-5);
  Real factorR = 2.0/(Real(order)) * lambdaR * sensorR; //smoothabs0(sensorL, 1.0e-5);

  MatrixQ dutduL = DLA::Identity();
  dutduL(4, 0) = 0.5*(gamma - 1.0)*(uL*uL + vL*vL + wL*wL);
  dutduL(4, 1) = -(gamma - 1.0)*uL;
  dutduL(4, 2) = -(gamma - 1.0)*vL;
  dutduL(4, 3) = -(gamma - 1.0)*wL;
  dutduL(4, 4) = gamma;

  MatrixQ dutduR = DLA::Identity();
  dutduR(4, 0) = 0.5*(gamma - 1.0)*(uR*uR + vR*vR + wR*wR);
  dutduR(4, 1) = -(gamma - 1.0)*uR;
  dutduR(4, 2) = -(gamma - 1.0)*vR;
  dutduR(4, 3) = -(gamma - 1.0)*wR;
  dutduR(4, 4) = gamma;

  for (int i = 0; i <= PDEClass::iEngy; i++)
    for (int j = 0; j <= PDEClass::iEngy; j++)
    {
      if (i == j)
      {
        kxxLT(i,j) = factorL*H(0,0);
        kxyLT(i,j) = factorL*H(0,1);
        kxzLT(i,j) = factorL*H(0,2);
        kyxLT(i,j) = factorL*H(1,0);
        kyyLT(i,j) = factorL*H(1,1);
        kyzLT(i,j) = factorL*H(1,2);
        kzxLT(i,j) = factorL*H(2,0);
        kzyLT(i,j) = factorL*H(2,1);
        kzzLT(i,j) = factorL*H(2,2);

        kxxRT(i,j) = factorR*H(0,0);
        kxyRT(i,j) = factorR*H(0,1);
        kxzRT(i,j) = factorR*H(0,2);
        kyxRT(i,j) = factorR*H(1,0);
        kyyRT(i,j) = factorR*H(1,1);
        kyzRT(i,j) = factorR*H(1,2);
        kzxRT(i,j) = factorR*H(2,0);
        kzyRT(i,j) = factorR*H(2,1);
        kzzRT(i,j) = factorR*H(2,2);
      }
      else
      {
        kxxLT(i,j) = 0.0;
        kxyLT(i,j) = 0.0;
        kxzLT(i,j) = 0.0;
        kyxLT(i,j) = 0.0;
        kyyLT(i,j) = 0.0;
        kyzLT(i,j) = 0.0;
        kzxLT(i,j) = 0.0;
        kzyLT(i,j) = 0.0;
        kzzLT(i,j) = 0.0;

        kxxRT(i,j) = 0.0;
        kxyRT(i,j) = 0.0;
        kxzRT(i,j) = 0.0;
        kyxRT(i,j) = 0.0;
        kyyRT(i,j) = 0.0;
        kyzRT(i,j) = 0.0;
        kzxRT(i,j) = 0.0;
        kzyRT(i,j) = 0.0;
        kzzRT(i,j) = 0.0;
      }
    }

  kxxL_AV = kxxLT*dutduL;
  kxyL_AV = kxyLT*dutduL;
  kxzL_AV = kxzLT*dutduL;
  kyxL_AV = kyxLT*dutduL;
  kyyL_AV = kyyLT*dutduL;
  kyzL_AV = kyzLT*dutduL;
  kzxL_AV = kzxLT*dutduL;
  kzyL_AV = kzyLT*dutduL;
  kzzL_AV = kzzLT*dutduL;

  kxxR_AV = kxxRT*dutduR;
  kxyR_AV = kxyRT*dutduR;
  kxzR_AV = kxzRT*dutduR;
  kyxR_AV = kyxRT*dutduR;
  kyyR_AV = kyyRT*dutduR;
  kyzR_AV = kyzRT*dutduR;
  kzxR_AV = kzxRT*dutduR;
  kzyR_AV = kzyRT*dutduR;
  kzzR_AV = kzzRT*dutduR;

  MatrixQ dudqL = 0, dudqR = 0;
  pde.jacobianMasterState(x, y, z, time, qL, dudqL);
  pde.jacobianMasterState(x, y, z, time, qR, dudqR);

  ArrayQ fL_AV = -kxxL_AV*dudqL*qxL - kxyL_AV*dudqL*qyL - kxzL_AV*dudqL*qzL;
  ArrayQ gL_AV = -kyxL_AV*dudqL*qxL - kyyL_AV*dudqL*qyL - kyzL_AV*dudqL*qzL;
  ArrayQ hL_AV = -kzxL_AV*dudqL*qxL - kzyL_AV*dudqL*qyL - kzzL_AV*dudqL*qzL;

  ArrayQ fR_AV = -kxxR_AV*dudqR*qxR - kxyR_AV*dudqR*qyR - kxzR_AV*dudqR*qzR;
  ArrayQ gR_AV = -kyxR_AV*dudqR*qxR - kyyR_AV*dudqR*qyR - kyzR_AV*dudqR*qzR;
  ArrayQ hR_AV = -kzxR_AV*dudqR*qxR - kzyR_AV*dudqR*qyR - kzzR_AV*dudqR*qzR;

  // viscous flux
  ArrayQ fBase = 0, gBase = 0, hBase = 0;
  AVArrayQ f = 0, g = 0, h = 0;
  avpde.fluxViscous( param, x, y, z, time, avqR, avqxR, avqyR, avqzR, f, g, h );
  pde.fluxViscous( dist, x, y, z, time, qR, qxR, qyR, qzR, fBase, gBase, hBase );

  ArrayQ fTrue = 0, gTrue = 0, hTrue = 0;
  fTrue = fBase + fR_AV;
  gTrue = gBase + gR_AV;
  hTrue = hBase + hR_AV;

  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );
  BOOST_CHECK_CLOSE( fTrue(5), f(5), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );
  BOOST_CHECK_CLOSE( gTrue(4), g(4), tol );
  BOOST_CHECK_CLOSE( gTrue(5), g(5), tol );
  BOOST_CHECK_CLOSE( hTrue(0), h(0), tol );
  BOOST_CHECK_CLOSE( hTrue(1), h(1), tol );
  BOOST_CHECK_CLOSE( hTrue(2), h(2), tol );
  BOOST_CHECK_CLOSE( hTrue(3), h(3), tol );
  BOOST_CHECK_CLOSE( hTrue(4), h(4), tol );
  BOOST_CHECK_CLOSE( hTrue(5), h(5), tol );

  // central viscous flux
  fTrue = 0;
  fBase = 0;
  f = 0;
  avpde.fluxViscous( param, param, x, y, z, time, avqL, avqxL, avqyL, avqzL, avqR, avqxR, avqyR, avqzR, nx, ny, nz, f );
  pde.fluxViscous( dist, dist, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fBase );

  fTrue = fBase + 0.5*(fL_AV + fR_AV)*nx + 0.5*(gL_AV + gR_AV)*ny + 0.5*(hL_AV + hR_AV)*nz;

  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );
  BOOST_CHECK_CLOSE( fTrue(5), f(5), tol );

  // diffusion coeficients
  AVMatrixQ kxx = 0, kxy = 0, kxz = 0;
  AVMatrixQ kyx = 0, kyy = 0, kyz = 0;
  AVMatrixQ kzx = 0, kzy = 0, kzz = 0;
  MatrixQ kxxBase = 0, kxyBase = 0, kxzBase = 0;
  MatrixQ kyxBase = 0, kyyBase = 0, kyzBase = 0;
  MatrixQ kzxBase = 0, kzyBase = 0, kzzBase = 0;
  avpde.diffusionViscous( param, x, y, z, time, avqR, avqxR, avqyR, avqzR, kxx, kxy, kxz, kyx, kyy, kyz, kzx, kzy, kzz );
  pde.diffusionViscous( x, y, z, time, qR, qxR, qyR, qzR, kxxBase, kxyBase, kxzBase, kyxBase, kyyBase, kyzBase, kzxBase, kzyBase, kzzBase );

  MatrixQ kxxTrue = 0, kxyTrue = 0, kxzTrue = 0;
  MatrixQ kyxTrue = 0, kyyTrue = 0, kyzTrue = 0;
  MatrixQ kzxTrue = 0, kzyTrue = 0, kzzTrue = 0;
  kxxTrue = kxxBase + kxxR_AV;
  kxyTrue = kxyBase + kxyR_AV;
  kxzTrue = kxzBase + kxzR_AV;
  kyxTrue = kyxBase + kyxR_AV;
  kyyTrue = kyyBase + kyyR_AV;
  kyzTrue = kyzBase + kyzR_AV;
  kzxTrue = kzxBase + kzxR_AV;
  kzyTrue = kzyBase + kzyR_AV;
  kzzTrue = kzzBase + kzzR_AV;

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx(i,j), kxxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy(i,j), kxyTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxz(i,j), kxzTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx(i,j), kyxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyy(i,j), kyyTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyz(i,j), kyzTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzx(i,j), kzxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzy(i,j), kzyTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzz(i,j), kzzTrue(i,j), small_tol, close_tol )
    }
  }

  // diffusion coefficients
  AVMatrixQ dfdu = 0, dgdu = 0, dhdu = 0;
  MatrixQ dfduTrue = 0, dgduTrue = 0, dhduTrue = 0;
  avpde.jacobianFluxViscous( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, dfdu, dgdu, dhdu );
  pde.jacobianFluxViscous( dist, x, y, z, time, qL, qxL, qyL, qzL, dfduTrue, dgduTrue, dhduTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dfdu(i,j), dfduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dgdu(i,j), dgduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dhdu(i,j), dhduTrue(i,j), small_tol, close_tol )
    }
  }

  // strong viscous flux
  ArrayQ fviscStrongTrue = 0;
  AVArrayQ fviscStrong = 0;
  avpde.strongFluxViscous( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, avqxx, avqxy, avqyy, avqxz, avqyz, avqzz, fviscStrong );
  pde.strongFluxViscous( dist, x, y, z, time, qL, qxL, qyL, qzL, qxx, qxy, qyy, qxz, qyz, qzz, fviscStrongTrue );
  BOOST_CHECK_CLOSE( fviscStrongTrue(0), fviscStrong(0), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(1), fviscStrong(1), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(2), fviscStrong(2), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(3), fviscStrong(3), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(4), fviscStrong(4), tol );

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  AVMatrixQ kxx_x = 0, kxy_x = 0, kxz_x = 0, kyx_x = 0, kzx_x = 0;
  AVMatrixQ kxy_y = 0, kyx_y = 0, kyy_y = 0, kyz_y = 0, kzy_y = 0;
  AVMatrixQ kxz_z = 0, kyz_z = 0, kzx_z = 0, kzy_z = 0, kzz_z = 0;
  MatrixQ kxx_xTrue = 0, kxy_xTrue = 0, kxz_xTrue = 0, kyx_xTrue = 0, kzx_xTrue = 0;
  MatrixQ kxy_yTrue = 0, kyx_yTrue = 0, kyy_yTrue = 0, kyz_yTrue = 0, kzy_yTrue = 0;
  MatrixQ kxz_zTrue = 0, kyz_zTrue = 0, kzx_zTrue = 0, kzy_zTrue = 0, kzz_zTrue = 0;
  avpde.diffusionViscousGradient( param, x, y, z, time, avqL, avqxL, avqyL, avqzL,
                                  avqxx, avqxy, avqyy, avqxz, avqyz, avqzz,
                                  kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                  kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                  kxz_z, kyz_z, kzx_z, kzy_z, kzz_z );
  pde.diffusionViscousGradient( dist, x, y, z, time, qL, qxL, qyL, qzL,
                                qxx, qxy, qyy, qxz, qyz, qzz,
                                kxx_xTrue, kxy_xTrue, kxz_xTrue, kyx_xTrue, kzx_xTrue,
                                kxy_yTrue, kyx_yTrue, kyy_yTrue, kyz_yTrue, kzy_yTrue,
                                kxz_zTrue, kyz_zTrue, kzx_zTrue, kzy_zTrue, kzz_zTrue);

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxz_x(i,j), kxz_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzx_x(i,j), kzx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyz_y(i,j), kyz_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzy_y(i,j), kzy_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxz_z(i,j), kxz_zTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyz_z(i,j), kyz_zTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzx_z(i,j), kzx_zTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzy_z(i,j), kzy_zTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kzz_z(i,j), kzz_zTrue(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( masterState_zeroSensor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  // function tests
  Real rho, u, v, w, t, nut, s;

  rho = 1.137; u = 0.784; v = 0.821; w = 0.326; t = 0.987; nut = 4.7; s = 0;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing

  Real x = 0, y = 0, z = 0, time = 0;

  // set
  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nut}) );
  AVVariable<SAnt3D_rhovT,Real> qdata({rho, u, v, w, t, nut}, s);
  AVArrayQ avq = 0;
  avpde.setDOFFrom( avq, qdata );

  // conservative flux
  ArrayQ uTrue = 0;
  AVArrayQ uCons = 0;
  avpde.masterState( param, x, y, z, time, avq, uCons );
  pde.masterState( dist, x, y, z, time, q, uTrue );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(4), uCons(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(5), uCons(5), small_tol, close_tol )

  // conservative flux
  AVMatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;
  avpde.jacobianMasterState( param, x, y, z, time, avq, dudq );
  pde.jacobianMasterState( dist, x, y, z, time, q, dudqTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( masterState_nonZeroSensor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  // function tests
  Real rho, u, v, w, t, nut, s;

  rho = 1.137; u = 0.784; v = 0.821; w = 0.432; t = 0.987; nut = 4.7; s = 0;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing

  Real x = 0, y = 0, z = 0, time = 0;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nut}) );
  AVVariable<SAnt3D_rhovT,Real> qdata({rho, u, v, w, t, nut}, s);
  AVArrayQ avq = 0;
  avpde.setDOFFrom( avq, qdata );

  // conservative flux
  ArrayQ uTrue = 0;
  AVArrayQ uCons = 0;
  avpde.masterState( param, x, y, z, time, avq, uCons );
  pde.masterState( dist, x, y, z, time, q, uTrue );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(4), uCons(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(5), uCons(5), small_tol, close_tol )

  // conservative flux
  AVMatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;
  avpde.jacobianMasterState( param, x, y, z, time, avq, dudq );
  pde.jacobianMasterState( dist, x, y, z, time, q, dudqTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source_zeroSensor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real rhoL, uL, vL, wL, tL, nutL, sL;
  Real rhoR, uR, vR, wR, tR, nutR, sR;

  x = y = time = 0;   // not actually used in functions
  rhoL = 1.034; uL = 3.26; vL = 1.23; wL = 0.432; tL = 5.78; nutL = 4.2; sL = 0.0;
  rhoR = 0.973; uR = 1.79; vR = 0.34; wR = 0.533; tR = 6.13; nutR = 5.7; sR = 0.0;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoL, uL, vL, wL, tL, nutL}) );
  pde.setDOFFrom( qR, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoR, uR, vR, wR, tR, nutR}) );
  AVArrayQ avqL = 0, avqR = 0;
  AVVariable<SAnt3D_rhovT,Real> qdataL({rhoL, uL, vL, wL, tL, nutL}, sL);
  AVVariable<SAnt3D_rhovT,Real> qdataR({rhoR, uR, vR, wR, tR, nutR}, sR);
  avpde.setDOFFrom( avqL, qdataL );
  avpde.setDOFFrom( avqR, qdataR );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19, 0.41, 0.32};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21, 0.52, 0.86};
  ArrayQ qzL = {1.43, 0.23, 0.34, -0.41, 1.24, 0.34};
  AVArrayQ avqxL = {0.17, 0.21, -0.34, 0.19, 0.41, 0.32, 0.0};
  AVArrayQ avqyL = {-0.05, 0.61, 0.12, -0.21, 0.52, 0.86, 0.0};
  AVArrayQ avqzL = {1.43, 0.23, 0.34, -0.41, 1.24, 0.34, 0.0};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26, 0.43, 0.63};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15, 0.91, 0.98};
  ArrayQ qzR = {-0.15, 0.52, 0.73, -0.23, 0.14, 0.45};
  AVArrayQ avqxR = {0.21, 0.31, -0.14, 0.26, 0.43, 0.63, 0.0};
  AVArrayQ avqyR = {0.11, 0.48, 0.27, -0.15, 0.91, 0.98, 0.0};
  AVArrayQ avqzR = {-0.15, 0.52, 0.73, -0.23, 0.14, 0.45, 0.0};

  Real lifted_quantity = 1.23;

  // vanilla source term
  AVArrayQ src = 0;
  ArrayQ srcTrue = 0;
  avpde.source( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, src );
  pde.source( dist, x, y, z, time, qL, qxL, qyL, qzL, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(4), src(4), small_tol, close_tol )

  // source term with lifted quantity
  src = 0;
  srcTrue = 0;
  avpde.source( param, x, y, z, time, lifted_quantity, avqL, avqxL, avqyL, avqzL, src );
  pde.source( dist, x, y, z, time, lifted_quantity, qL, qxL, qyL, qzL, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(4), src(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(5), src(5), small_tol, close_tol )

  // Right now sourceTrace is not implemented,
  // once it is, this is the test we want
#if 0
  // trace source term with lifted quantity
  AVArrayQ srcL = 0, srcR = 0;
  ArrayQ srcLTrue = 0, srcRTrue = 0;
  avpde.sourceTrace( param, x, y, z, param, x, y, z, time, avqL, avqxL, avqyL, avqzL, avqR, avqxR, avqyR, avqzR, srcL, srcR );
  pde.sourceTrace( dist, x, y, z, dist, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzL, srcLTrue, srcRTrue );
  SANS_CHECK_CLOSE( srcLTrue(0), srcL(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(1), srcL(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(2), srcL(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(3), srcL(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(4), srcL(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(5), srcL(5), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(0), srcR(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(1), srcR(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(2), srcR(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(3), srcR(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(4), srcR(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(5), srcR(5), small_tol, close_tol )
#else
  // trace source term with lifted quantity
  AVArrayQ srcL = 0, srcR = 0;
  ArrayQ srcLTrue = 0, srcRTrue = 0;
  BOOST_CHECK_THROW(
      avpde.sourceTrace( param, x, y, z, param, x, y, z, time, avqL, avqxL, avqyL, avqzL, avqR, avqxR, avqyR, avqzR, srcL, srcR ),
      DeveloperException);
  BOOST_CHECK_THROW(
      pde.sourceTrace( dist, x, y, z, dist, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, srcLTrue, srcRTrue ),
      DeveloperException);
#endif

  // source for lifted quantity
  src = 0;
  srcTrue = 0;
  avpde.sourceLiftedQuantity( param, x, y, z, param, x, y, z, time, avqL, avqR, src );
  pde.sourceLiftedQuantity( dist, x, y, z, dist, x, y, z, time, qL, qR, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(4), src(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(5), src(5), small_tol, close_tol )


  // source Jacobian
  AVMatrixQ Js = 0;
  MatrixQ JsTrue = 0;
  avpde.jacobianSource( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, Js );
  pde.jacobianSource( dist, x, y, z, time, qL, qxL, qyL, qzL, JsTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JsTrue(i,j), Js(i,j), small_tol, close_tol )
    }
  }

  // source Jacobian
  AVMatrixQ dsdux = 0, dsduy = 0, dsduz = 0;
  MatrixQ dsduxTrue = 0, dsduyTrue = 0, dsduzTrue = 0;
  avpde.jacobianGradientSource( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, dsdux, dsduy, dsduz );
  pde.jacobianGradientSource( dist, x, y, z, time, qL, qxL, qyL, qzL, dsduxTrue, dsduyTrue, dsduzTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dsduxTrue(i,j), dsdux(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dsduyTrue(i,j), dsduy(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dsduzTrue(i,j), dsduz(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source_nonZeroSensor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  Real x, y, z, time;
  Real rhoL, uL, vL, wL, tL, nutL, sL;
  Real rhoR, uR, vR, wR, tR, nutR, sR;

  x = y = time = 0;   // not actually used in functions
  rhoL = 1.034; uL = 3.26; vL = 1.23; wL = 0.432; tL = 5.78; nutL = 4.2; sL = 0.3;
  rhoR = 0.973; uR = 1.79; vR = 0.34; wR = 0.533; tR = 6.13; nutR = 5.7; sR = 0.5;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoL, uL, vL, wL, tL, nutL}) );
  pde.setDOFFrom( qR, SAnt3D<DensityVelocityTemperature3D<Real>>({rhoR, uR, vR, wR, tR, nutR}) );
  AVArrayQ avqL = 0, avqR = 0;
  AVVariable<SAnt3D_rhovT,Real> qdataL({rhoL, uL, vL, wL, tL, nutL}, sL);
  AVVariable<SAnt3D_rhovT,Real> qdataR({rhoR, uR, vR, wR, tR, nutR}, sR);
  avpde.setDOFFrom( avqL, qdataL );
  avpde.setDOFFrom( avqR, qdataR );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19, 0.41, 0.32};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21, 0.52, 0.86};
  ArrayQ qzL = {1.43, 0.23, 0.34, -0.41, 1.24, 0.34};
  AVArrayQ avqxL = {0.17, 0.21, -0.34, 0.19, 0.41, 0.32, 0.54};
  AVArrayQ avqyL = {-0.05, 0.61, 0.12, -0.21, 0.52, 0.86, 0.85};
  AVArrayQ avqzL = {1.43, 0.23, 0.34, -0.41, 1.24, 0.34, 0.75};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26, 0.43, 0.63};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15, 0.91, 0.98};
  ArrayQ qzR = {-0.15, 0.52, 0.73, -0.23, 0.14, 0.45};
  AVArrayQ avqxR = {0.21, 0.31, -0.14, 0.26, 0.43, 0.63, 0.78};
  AVArrayQ avqyR = {0.11, 0.48, 0.27, -0.15, 0.91, 0.98, 0.42};
  AVArrayQ avqzR = {-0.15, 0.52, 0.73, -0.23, 0.14, 0.45, 0.95};

  Real lifted_quantity = 1.23;

  // vanilla source term
  AVArrayQ src = 0;
  ArrayQ srcTrue = 0;
  avpde.source( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, src );
  pde.source( dist, x, y, z, time, qL, qxL, qyL, qzL, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(4), src(4), small_tol, close_tol )

  // source term with lifted quantity
  src = 0;
  srcTrue = 0;
  avpde.source( param, x, y, z, time, lifted_quantity, avqL, avqxL, avqyL, avqzL, src );
  pde.source( dist, x, y, z, time, lifted_quantity, qL, qxL, qyL, qzL, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(4), src(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(5), src(5), small_tol, close_tol )

  // Right now sourceTrace is not implemented,
  // once it is, this is the test we want
#if 0
  // trace source term with lifted quantity
  AVArrayQ srcL = 0, srcR = 0;
  ArrayQ srcLTrue = 0, srcRTrue = 0;
  avpde.sourceTrace( param, x, y, z, param, x, y, z, time, avqL, avqxL, avqyL, avqzL, avqR, avqxR, avqyR, avqzR, srcL, srcR );
  pde.sourceTrace( dist, x, y, z, dist, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzL, srcLTrue, srcRTrue );
  SANS_CHECK_CLOSE( srcLTrue(0), srcL(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(1), srcL(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(2), srcL(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(3), srcL(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(4), srcL(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(5), srcL(5), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(0), srcR(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(1), srcR(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(2), srcR(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(3), srcR(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(4), srcR(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(5), srcR(5), small_tol, close_tol )
#else
  // trace source term with lifted quantity
  AVArrayQ srcL = 0, srcR = 0;
  ArrayQ srcLTrue = 0, srcRTrue = 0;
  BOOST_CHECK_THROW(
      avpde.sourceTrace( param, x, y, z, param, x, y, z, time, avqL, avqxL, avqyL, avqzL, avqR, avqxR, avqyR, avqzR, srcL, srcR ),
      DeveloperException);
  BOOST_CHECK_THROW(
      pde.sourceTrace( dist, x, y, z, dist, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, srcLTrue, srcRTrue ),
      DeveloperException);
#endif

  // source for lifted quantity
  src = 0;
  srcTrue = 0;
  avpde.sourceLiftedQuantity( param, x, y, z, param, x, y, z, time, avqL, avqR, src );
  pde.sourceLiftedQuantity( dist, x, y, z, dist, x, y, z, time, qL, qR, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(4), src(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(5), src(5), small_tol, close_tol )


  // source Jacobian
  AVMatrixQ Js = 0;
  MatrixQ JsTrue = 0;
  avpde.jacobianSource( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, Js );
  pde.jacobianSource( dist, x, y, z, time, qL, qxL, qyL, qzL, JsTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JsTrue(i,j), Js(i,j), small_tol, close_tol )
    }
  }

  // source Jacobian
  AVMatrixQ dsdux = 0, dsduy = 0, dsduz = 0;
  MatrixQ dsduxTrue = 0, dsduyTrue = 0, dsduzTrue = 0;
  avpde.jacobianGradientSource( param, x, y, z, time, avqL, avqxL, avqyL, avqzL, dsdux, dsduy, dsduz );
  pde.jacobianGradientSource( dist, x, y, z, time, qL, qxL, qyL, qzL, dsduxTrue, dsduyTrue, dsduzTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dsduxTrue(i,j), dsdux(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dsduyTrue(i,j), dsduy(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dsduzTrue(i,j), dsduz(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic_zeroSensor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  const Real tol = 1e-12;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  Real x, y, z, time, dx, dy, dz;
  Real rho, u, v, w, t, nut, s;
  Real speed, speedTrue;

  x = y = z = time = 0;   // not actually used in functions
  dx = 0.57; dy = 1.23, dz = 1.4;
  rho = 1.137; u = 0.784; v = 2.31; w= 0.432; t = 0.987; nut = 4.2; s = 0.0;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nut}) );
  AVVariable<SAnt3D_rhovT,Real> qdata({rho, u, v, w, t, nut}, s);
  AVArrayQ avq = 0;
  avpde.setDOFFrom( avq, qdata );

  avpde.speedCharacteristic( param, x, y, z, time, dx, dy, dz, avq, speed );
  pde.speedCharacteristic( dist, x, y, z, time, dx, dy, dz, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( param, x, y, z, time, avq, speed );
  pde.speedCharacteristic( dist, x, y, z, time, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic_nonZeroSensor, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  const Real tol = 1e-12;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  const int order = 1;
  bool isSteady = false;

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  Real x, y, z, time, dx, dy, dz;
  Real rho, u, v, w, t, nut, s;
  Real speed, speedTrue;

  x = y = z = time = 0;   // not actually used in functions
  dx = 0.57; dy = 1.23, dz = 1.4;
  rho = 1.137; u = 0.784; v = 2.31; w= 0.432; t = 0.987; nut = 4.2; s = 0.5;
  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nut}) );
  AVVariable<SAnt3D_rhovT,Real> qdata({rho, u, v, w, t, nut}, s);
  AVArrayQ avq = 0;
  avpde.setDOFFrom( avq, qdata );

  avpde.speedCharacteristic( param, x, y, z, time, dx, dy, dz, avq, speed );
  pde.speedCharacteristic( dist, x, y, z, time, dx, dy, dz, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( param, x, y, z, time, avq, speed );
  pde.speedCharacteristic( dist, x, y, z, time, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
