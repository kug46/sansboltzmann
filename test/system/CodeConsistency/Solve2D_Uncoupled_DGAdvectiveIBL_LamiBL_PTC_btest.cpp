// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// testing of 2-D DGAdvective for IBL on airfoil and wake

//#define IBL2D_DUMP
//#define SANS_VERBOSE

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include "pde/IBL/PDEIBL2D.h"
#include "pde/IBL/BCIBL2D.h"
#include "pde/IBL/SetSolnDofCell_IBL2D.h"
#include "pde/IBL/SetVelDofCell_IBL2D.h"
#include "pde/IBL/SetIBLoutputCellGroup.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_WakeMatch_Galerkin_IBL.h"

using namespace std;
using namespace SANS;

namespace SANS
{
typedef PhysD2 PhysDim; // physical dimension
typedef TopoD1 TopoDim; // grid cell topological dimension

// PDE class
typedef VarTypeDANCt VarType;
typedef PDEIBL<PhysDim,VarType> PDEClass;
typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;

typedef NDPDEClass::template ArrayQ<Real> ArrayQ;
typedef PDEClass::template ArrayParam<Real> ArrayParam;
typedef NDPDEClass::VectorX VectorX;

// BC
typedef BCIBL2DVector<VarType> BCVectorClass;

// parameter field
typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayParam>,
                                       XField<PhysDim, TopoDim> >::type FieldParamType;

// Primal equation set
typedef AlgebraicEquationSet_DGAdvective<NDPDEClass,BCNDConvertSpace,BCVectorClass,
                                         AlgEqSetTraits_Sparse,DGAdv_manifold,FieldParamType> PrimalEquationSetClass;
typedef PrimalEquationSetClass::BCParams BCParams;
typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

// Pseudo time continuation wrapper equation set
typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC_manifold, FieldParamType> AlgebraicEquationSet_PTCClass;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( CodeConsistencySolve2D_DGAdvective_IBL_Line_Airfoil_PTC_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CodeConsistencySolve2D_DGAdvective_IBL_Line_Airfoil_test )
{
  // ---------- Set problem parameters ---------- //
  const string airfoilname = "naca0004";
  const int nelem = 64;

  const int order_soln = 1; // solution order
  const int order_param_grid = 1;  // order: parameter field and grid

  const int alpha = 0; // [degree] angle of attack
  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300; // [K] stagnation temperature

  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  GasModelIBL gasModel(gasModelDict);

  const Real mue = 1.827e-5;
  typename PDEClass::ViscosityModelType viscosityModel(mue);

  PyDict transitionModelDict;
  transitionModelDict[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict[TransitionModelParams::params.profileCatDefault]
                      = TransitionModelParams::params.profileCatDefault.laminarBL; // This is dummy; not actually checking profileCatDefault_
  TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity
  typename PDEClass::TransitionModelType transitionModel(transitionModelDict);

  // PDE
  NDPDEClass pde(gasModel, viscosityModel, transitionModel);

  // BC
  // Create a BC dictionary
  PyDict BCOutArgs;
  BCOutArgs[BCParams::params.BC.BCType] = BCParams::params.BC.None;

  PyDict PyBCList;
  PyBCList["BCOut"] = BCOutArgs;

  // No exceptions should be thrown
  BCParams::checkInputs(PyBCList);

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCOut"] = {0,1};

  // ---------- Set solver parameters ---------- //

  // Set up Newton Solver
  PyDict NewtonSolverDict, UMFPACKDict, NewtonLineUpdateDict;

  UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 50;
#ifdef SANS_VERBOSE
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
#else
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = false;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  // Set up pseudo time continuation
  PyDict PTCDict;
  PTCDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
  PTCDict[PseudoTimeParam::params.invCFL] = 0.001;
//  PTCDict[PseudoTimeParam::params.invCFL] = 0.1;
  PTCDict[PseudoTimeParam::params.invCFL_min] = 0.0;
  PTCDict[PseudoTimeParam::params.invCFL_max] = 1000.0;
  PTCDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.9;
  PTCDict[PseudoTimeParam::params.CFLIncreaseFactor] = 1.1;
#ifdef SANS_VERBOSE
  PTCDict[PseudoTimeParam::params.Verbose] = true;
#else
  PTCDict[PseudoTimeParam::params.Verbose] = false;
#endif

  PseudoTimeParam::checkInputs(PTCDict);

  // ---------- Set up fields ---------- //

  string filename_airfoil = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem) + ".txt";
  std::vector<VectorX> coordinates_a;
  XField2D_Line_X1_1Group::readXFoilGrid(filename_airfoil, coordinates_a);
  XField2D_Line_X1_1Group xfld(coordinates_a);

#if defined(SANS_VERBOSE)
  cout << "Airfoil grid: " << filename_airfoil << endl;
#endif

  // parameter field: velocity and gradients
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velxXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );
  Field_DG_Cell<PhysDim, TopoDim, VectorX> velzXfld( xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  string velfilename_a = "IO/CodeConsistency/IBL/" + airfoilname + "_vel_ne" + stringify(nelem)
                       + "_a" + stringify(alpha) + ".txt";
  std::vector<Real> dataVel_a;
  SetVelocityDofCell_IBL2D::readVelocity( velfilename_a, dataVel_a );
  for_each_CellGroup<TopoDim>::apply( SetVelocityDofCell_IBL2D(dataVel_a, {0}), (velfld, velxXfld, velzXfld, xfld) );

#if defined(SANS_VERBOSE)
  cout << "Velocity field initialized from: " << velfilename_a << endl;
#endif

  BOOST_REQUIRE_EQUAL( xfld.nElem(), velfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velxXfld.nElem() );
  BOOST_REQUIRE_EQUAL( xfld.nElem(), velzXfld.nElem() );

  // parameter field
  Field_DG_Cell<PhysDim, TopoDim, ArrayParam> paramfld(xfld, order_param_grid, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( xfld.nElem(), paramfld.nElem() );

  // TODO: should not work directly with DOFs
  const auto& paramInterpret = pde.getParamInterpreter();
  for (int i = 0; i < paramfld.nDOF(); ++i)
  {
    paramfld.DOF(i) = paramInterpret.setDOFFrom(
        velfld.DOF(i),
        DLA::VectorS<PhysDim::D, VectorX>(
            {VectorX({velxXfld.DOF(i)[0], velzXfld.DOF(i)[0]}),
             VectorX({velxXfld.DOF(i)[1], velzXfld.DOF(i)[1]})} ),
        p0, T0);
  }

  // DG solution field
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfld( xfld, order_soln, BasisFunctionCategory_Hierarchical );

  // Set the initial condition
  string Qfilename_a = "IO/CodeConsistency/IBL/" + airfoilname + "_qinit_ne" + stringify(nelem)
                     + "_a" + stringify(alpha) + ".txt";
  std::vector<ArrayQ> dataQinit_a
    = QIBLDataReader<VarData2DDANCt>::getqIBLvec(pde.getVarInterpreter(), Qfilename_a);
  for_each_CellGroup<TopoDim>::apply( SetSolnDofCell_IBL2D<VarType>(dataQinit_a, {0}, order_soln), (xfld, qfld) );

#if defined(SANS_VERBOSE)
  cout << "Initial QIBL airfoil: " << Qfilename_a << endl;
#endif

  BOOST_REQUIRE_EQUAL( xfld.nElem(), qfld.nElem() );

  // Lagrange multiplier: Hierarchical, C0 (also at corners)
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ>
    lgfld( xfld, order_soln, BasisFunctionCategory_Legendre, BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups) );
  lgfld = 0.0;

  // ---------- Solve ---------- //

  FieldParamType tuplefld = (paramfld, xfld);

  QuadratureOrder quadratureOrder(xfld, -1);
  const Real tol_eqn = 1e-12;
  const std::vector<Real> tol(2,tol_eqn);
  const std::vector<int> cellGroups = {0};
  const std::vector<int> interiorTraceGroups = {0};
  PrimalEquationSetClass iblEqnSet(tuplefld, qfld, lgfld, pde, quadratureOrder, ResidualNorm_L2, tol,
                                     cellGroups, interiorTraceGroups, PyBCList, BCBoundaryGroups);

  // Pseudo time continuation equation set object
  AlgebraicEquationSet_PTCClass iblEqSetPTC(tuplefld, qfld, pde, quadratureOrder, cellGroups, iblEqnSet);

  PseudoTime<SystemMatrixClass> PTC(PTCDict, iblEqSetPTC);

  SystemVectorClass sln(iblEqSetPTC.vectorStateSize());

  iblEqSetPTC.fillSystemVector(sln);

  BOOST_REQUIRE( PTC.iterate(100) );

  // ------------------------------------ //
  // POST-PROCESSING
  // ------------------------------------ //
  { // Pyrite check
    const Real pyrite_tol = 5.e-9;

#if ISIBLLFFLUX_PDEIBL
    string pyriteFilename = "IO/CodeConsistency/Solve2D_Uncoupled_DGAdvectiveIBL_" + airfoilname + "_ne" + stringify(nelem) +
                            "_p" + stringify(order_soln) + "_airfoil_Test_LFflux.txt";
#else
    string pyriteFilename = "IO/CodeConsistency/Solve2D_Uncoupled_DGAdvectiveIBL_" + airfoilname + "_ne" + stringify(nelem) +
                            "_p" + stringify(order_soln) + "_airfoil_Test_FullUpwind.txt";
#endif

    const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
    const bool isPyriteVerbose = true;
#else
    const bool isPyriteVerbose = false;
#endif
    pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      // Currently, only check two primary unknowns; other unknowns do not matter in this test
      pyriteFile << qfld.DOF(i)[PDEClass::VarInterpType::ideltaLami];
      pyriteFile << qfld.DOF(i)[PDEClass::VarInterpType::iALami];
      pyriteFile << std::endl;
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
