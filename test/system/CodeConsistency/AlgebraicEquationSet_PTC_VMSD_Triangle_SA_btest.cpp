// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// AlgebraicEquationSet_DGBR2_Triangle_AD_btest
// testing AlgebraicEquationSet_DGBR2

//#define DISPLAY_FOR_DEBUGGING

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <string>
#include <fstream>

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/QRANSSA2D.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/OutputNDConvertSpace2D.h"
#include "pde/ForcingFunction2D_MMS.h"

#include "pde/BCParameters.h"

#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"

#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"

#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"
#include "Field/DistanceFunction/DistanceFunction.h"
#include "Field/ProjectSoln/ProjectSolnCell_Lagrange.h"
#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#ifdef SANS_PETSC
#include "LinearAlgebra/SparseLinAlg/PETSc/PETScSolver.h"
#endif

#include "SolutionTrek/Continuation/Continuation.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( AlgebraicEquationSet_PTC_VMSD_Triangle_SA_test_suite )

#ifdef SANS_PETSC
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PTC_Serial_Parallel_Static_Condense_Solve_Equivalency )
{
  typedef QTypePrimitiveRhoPressure QType;
  typedef ViscosityModel_Const ViscosityModelType;
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;

  typedef BCRANSSA2DVector< TraitsSizeRANSSA, TraitsModelRANSSAClass > BCVector;

  typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

  typedef AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvertSpace, BCVector,
                                    AlgEqSetTraits_Sparse, ParamFieldTupleType> PrimalEquationSet;
  typedef PrimalEquationSet::BCParams BCParams;

  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamFieldTupleType> AlgebraicEquationSet_PTCClass;

  typedef NDPDEClass::template ArrayQ<Real> ArrayQ;

  // Size of the state vector
  const int N = ArrayQ::M;

  typedef PrimalEquationSet::SystemMatrix SparseSystemMatrixClass;
  typedef PrimalEquationSet::SystemVector SparseSystemVectorClass;

  const Real small_tol = 1e-11;
//  const Real close_tol = 1e-6;

  // parallel and serial communicator
  mpi::communicator world;
  mpi::communicator comm_global = world.split(world.rank());

  // PDE

  const Real gamma = 1.4;
  const Real R     = 1;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  // reference state (freestream)
  const Real Mach = 0.2;
  const Real Reynolds = 1e4;
  const Real Prandtl = 0.72;

  const Real lRef = 1;                              // length scale
  const Real rhoRef = 1;                            // density scale

  const Real qRef = 1;                              // velocity scale
  const Real aoaRef = 0;                            // angle of attack (radians)

  const Real uRef = qRef * cos(aoaRef);             // velocity
  const Real vRef = qRef * sin(aoaRef);
  const Real cRef = qRef/Mach;                      // speed of sound
  const Real tRef = (cRef*cRef)/(gamma*R);          // temperature
  const Real pRef = R*rhoRef*tRef;                  // pressure

  const Real muRef = rhoRef*qRef*lRef/Reynolds;     // molecular viscosity
  const Real chiRef = 3;
  const Real ntRef = chiRef*muRef/rhoRef;           // SA solution

  GasModel gas(gamma, R);
  ViscosityModel_Const visc(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);
  NDPDEClass pde(gas, visc, tcond, Euler_ResidInterp_Raw );

  // BC
  const Real HRef = Cp*tRef + 0.5*(uRef*uRef + vRef*vRef);
  const Real sRef = log( pRef / pow(rhoRef,gamma) );
  const Real aSpec = atan(vRef/uRef);

  PyDict BCIn;
  BCIn[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_sHqt_BN;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.sSpec] = sRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.HSpec] = HRef;
  BCIn[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params.aSpec] = aSpec;
  BCIn["nt"] = ntRef; // TODO: not happy about this

  PyDict BCOut;
  BCOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_BN;
  BCOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params.pSpec] = pRef;

  PyDict BCSymmetry;
  BCSymmetry[BCParams::params.BC.BCType] = BCParams::params.BC.Symmetry_mitState;

  PyDict BCNoSlip;
  BCNoSlip[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

  PyDict PyBCList;
  PyBCList["BCSymmetry"] = BCSymmetry;
  PyBCList["BCNoSlip"] = BCNoSlip;
  PyBCList["BCIn"] = BCIn;
  PyBCList["BCOut"] = BCOut;

  std::map<std::string, std::vector<int>> BCBoundaryGroups;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups["BCSymmetry"] = {XField2D_Box_Triangle_Lagrange_X1::iTop};
  BCBoundaryGroups["BCNoSlip"] = {XField2D_Box_Triangle_Lagrange_X1::iBottom};
  BCBoundaryGroups["BCOut"] = {XField2D_Box_Triangle_Lagrange_X1::iRight};
  BCBoundaryGroups["BCIn"] = {XField2D_Box_Triangle_Lagrange_X1::iLeft};

  //Check the BC dictionary
  BCParams::checkInputs(PyBCList);

  std::vector<int> mitLG_bcgroups = BCParams::getLGBoundaryGroups(PyBCList, BCBoundaryGroups);

  SolnNDConvertSpace<PhysD2, SolutionFunction_RANSSA2D_Wake<TraitsSizeRANSSA, TraitsModelRANSSAClass>>
    solnWake( gas, 0.9, rhoRef, uRef, pRef, ntRef );

  // compute the global jacobian

  // grid

  int ii = 4;
  int jj = 4;

  XField2D_Box_Triangle_Lagrange_X1 xfld_global( comm_global, ii, jj ); // complete system on all processors

  // distance function
  Field_CG_Cell<PhysD2, TopoD2, Real> distfld_global(xfld_global, 1, BasisFunctionCategory_Lagrange);
  DistanceFunction(distfld_global, BCBoundaryGroups.at("BCNoSlip"), false);

  int porder = 1;
  for (int order = 1; order <= 3; order++)
  {
    // solution:
    Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld_global(xfld_global, order, BasisFunctionCategory_Lagrange, EmbeddedCGField);

    // This avoids jacobians about 0 == fabs(uy - vx)
    for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Lagrange(solnWake, {0}), (xfld_global, qfld_global) );

    // pertubation
    Field_EG_Cell<PhysD2, TopoD2, ArrayQ> qpfld_global(qfld_global, porder, BasisFunctionCategory_Lagrange);
    qpfld_global = 0;

    // Lagrange multiplier
    Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_global( xfld_global, order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
    lgfld_global = 0;

    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < xfld_global.nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    ParamFieldTupleType paramfld_global = (distfld_global, xfld_global);

    DiscretizationVMSD stab(VMSDp, true, order);
    QuadratureOrder quadratureOrder( xfld_global, 2*order + 1 );
    std::vector<Real> tol = {1e-12, 1e-12, 1e-12};

    PrimalEquationSet PrimalEqSet_global(paramfld_global, qfld_global, qpfld_global, lgfld_global, pde, stab, quadratureOrder,
                                         ResidualNorm_L2, tol, {0}, interiorTraceGroups, PyBCList, BCBoundaryGroups);

    AlgebraicEquationSet_PTCClass AlgEqSetPTC_global(paramfld_global,
                                              qfld_global, pde,
                                              quadratureOrder,
                                              {0}, PrimalEqSet_global);

    // residual

    SparseSystemVectorClass q_global(PrimalEqSet_global.vectorStateSize());

    SparseSystemVectorClass rsd_global(PrimalEqSet_global.vectorEqSize());
    SparseSystemVectorClass rsd_global_saved(PrimalEqSet_global.vectorEqSize());

    SparseSystemVectorClass dq_global(PrimalEqSet_global.vectorStateSize());

    rsd_global = 0;
    rsd_global_saved = 0;
    AlgEqSetPTC_global.residual(rsd_global);
    AlgEqSetPTC_global.residual(rsd_global_saved);

    PyDict PreconditionerLU;
    PreconditionerLU[SLA::PreconditionerASMParam::params.SubPreconditioner.Name] = SLA::PreconditionerASMParam::params.SubPreconditioner.DirectLU;
    PreconditionerLU[SLA::PreconditionerILUParam::params.PreconditionerSide] = SLA::PreconditionerILUParam::params.PreconditionerSide.Right;

    PyDict PreconditionerDict;
    PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ASM;
    PreconditionerDict[SLA::PreconditionerASMParam::params.SubPreconditioner] = PreconditionerLU;

    PyDict PETScDict;
    PETScDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.PETSc;
    PETScDict[SLA::PETScSolverParam::params.Verbose] = false;
    PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-12;
    PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;

    PyDict LineUpdateDict, NewtonSolverDict, NonlinearSolverDict, SolverContinuationDict;
    LineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
    LineUpdateDict[HalvingSearchLineUpdateParam::params.minStepSize] = 1e-10;
    LineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;

    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = PETScDict;
    NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = LineUpdateDict;
    NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 2;
    NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
    NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = false;

   // Easier PTC for subsequent solves
    NonlinearSolverDict[SolverContinuationParams<TemporalMarch>::params.Continuation.Type]
                        = SolverContinuationParams<TemporalMarch>::params.Continuation.PseudoTime;
    NonlinearSolverDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict;
    NonlinearSolverDict[PseudoTimeParam::params.Verbose] = true;
    NonlinearSolverDict[PseudoTimeParam::params.invCFL] = 100;
    NonlinearSolverDict[PseudoTimeParam::params.invCFL_min] = 0;
    NonlinearSolverDict[PseudoTimeParam::params.MaxIterations] = 2;
    NonlinearSolverDict[PseudoTimeParam::params.invCFL_max] = 1e5;
    NonlinearSolverDict[PseudoTimeParam::params.CFLPartialStepDecreaseFactor] = 0.9;

    PseudoTime<SparseSystemMatrixClass> PTC_global( NonlinearSolverDict, AlgEqSetPTC_global);

#if 0
    if (world.rank() == 0)
    {
      Solver_global.factorize(rsd_global, false);

      PetscViewer view_global;
      PETSc_STATUS( PetscViewerASCIIOpen(comm_global, "tmp/VMSD_global.mat",&view_global) );
      PETSc_STATUS( PetscViewerPushFormat(view_global, PETSC_VIEWER_ASCII_MATLAB) );

      Solver_global.view(view_global);

      PETSc_STATUS( PetscViewerDestroy(&view_global) );
    }
#endif

    PTC_global.solve();
    PrimalEqSet_global.fillSystemVector(q_global);

    for (int comm_size = 1; comm_size <= world.size(); comm_size++)
    {

      int color = world.rank() < comm_size ? 0 : 1;
      mpi::communicator comm_local = world.split(color);

      if (color == 1) continue;

      // grid
      XField2D_Box_Triangle_Lagrange_X1 xfld_local( comm_local, ii, jj ); // partitioned system

      // distance function
      Field_CG_Cell<PhysD2, TopoD2, Real> distfld_local(xfld_local, 1, BasisFunctionCategory_Lagrange);
      DistanceFunction(distfld_local , BCBoundaryGroups.at("BCNoSlip"), false);

      // solution
      Field_CG_Cell<PhysD2, TopoD2, ArrayQ> qfld_local(xfld_local, order, BasisFunctionCategory_Lagrange, EmbeddedCGField);

      // This avoids jacobians about 0 == fabs(uy - vx)
      for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Lagrange(solnWake, {0}), (xfld_local, qfld_local ) );

      // pertubation
      Field_EG_Cell<PhysD2, TopoD2, ArrayQ> qpfld_local(qfld_local, porder, BasisFunctionCategory_Lagrange);
      qpfld_local = 0;

      // Lagrange multiplier
      Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> lgfld_local( xfld_local, order, BasisFunctionCategory_Legendre, mitLG_bcgroups );
      lgfld_local  = 0;

      ParamFieldTupleType paramfld_local = (distfld_local , xfld_local );

      PrimalEquationSet PrimalEqSet_local (paramfld_local, qfld_local, qpfld_local, lgfld_local, pde, stab, quadratureOrder,
                                           ResidualNorm_L2, tol, {0}, interiorTraceGroups, PyBCList, BCBoundaryGroups);



      AlgebraicEquationSet_PTCClass AlgEqSetPTC_local(paramfld_local,
                                                      qfld_local, pde,
                                                quadratureOrder,
                                                {0}, PrimalEqSet_local);

      SparseSystemVectorClass q_local(PrimalEqSet_local.vectorStateSize());

      SparseSystemVectorClass rsd_local(PrimalEqSet_local.vectorEqSize());

      PrimalEqSet_local.fillSystemVector(q_local);

      rsd_local = 0;
      AlgEqSetPTC_local.residual(rsd_local);

      BOOST_REQUIRE_EQUAL( rsd_local[0].m(), qpfld_local.nDOFpossessed() + qpfld_local.nDOFghost() );
      BOOST_REQUIRE_EQUAL( rsd_local[1].m(), qfld_local.nDOFpossessed() );
      BOOST_REQUIRE_EQUAL( rsd_local[2].m(), lgfld_local.nDOFpossessed() );

      BOOST_REQUIRE_EQUAL(rsd_global.m(), 3);
      BOOST_REQUIRE_EQUAL(rsd_local.m(), 3);

      for ( int i = 0; i < rsd_local[0].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          BOOST_CHECK_SMALL( rsd_global_saved[0][qpfld_local.local2nativeDOFmap(i)][ib] - rsd_local[0][i][ib], small_tol );

      for ( int i = 0; i < rsd_local[1].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          BOOST_CHECK_SMALL( rsd_global_saved[1][qfld_local.local2nativeDOFmap(i)][ib] - rsd_local[1][i][ib], small_tol );

      for ( int i = 0; i < rsd_local[2].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          BOOST_CHECK_SMALL( rsd_global_saved[2][lgfld_local.local2nativeDOFmap(i)][ib] - rsd_local[2][i][ib], small_tol );

#if 0
      {
        Solver_local.factorize(rsd_local, false);

        PetscViewer view_local;
        PETSc_STATUS( PetscViewerASCIIOpen(comm_local, "tmp/VMSD_local.mat",&view_local) );
        PETSc_STATUS( PetscViewerPushFormat(view_local, PETSC_VIEWER_ASCII_MATLAB) );

        Solver_local.view(view_local);

        PETSc_STATUS( PetscViewerDestroy(&view_local) );
      }
#endif

      PseudoTime<SparseSystemMatrixClass> PTC_local( NonlinearSolverDict, AlgEqSetPTC_local);

      PTC_local.solve();
      PrimalEqSet_local.fillSystemVector(q_local);

      BOOST_REQUIRE_EQUAL(q_global.m(), 3);
      BOOST_REQUIRE_EQUAL(q_local.m(), 3);

      for ( int i = 0; i < q_local[0].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          BOOST_CHECK_SMALL( q_global[0][qpfld_local.local2nativeDOFmap(i)][ib] - q_local[0][i][ib], small_tol);

      for ( int i = 0; i < q_local[1].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          BOOST_CHECK_SMALL( q_global[1][qfld_local.local2nativeDOFmap(i)][ib] - q_local[1][i][ib], small_tol );

      for ( int i = 0; i < q_local[2].m(); i++ )
        for ( int ib = 0; ib < N; ib++ )
          BOOST_CHECK_SMALL( q_global[2][lgfld_local.local2nativeDOFmap(i)][ib] - q_local[2][i][ib], small_tol );
    }
  }
}
#endif //SANS_PETSC


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
