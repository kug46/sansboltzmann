// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEmitAVSensor2D_Consistency_btest
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEmitAVSensor2D_Consistency_btest )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  BOOST_REQUIRE( AVPDEClass::D == PDEClass::D );
  BOOST_REQUIRE( AVPDEClass::N == PDEClass::N+1 );
  BOOST_REQUIRE( AVArrayQ::M == ArrayQ::M+1 );
  BOOST_REQUIRE( AVMatrixQ::M == MatrixQ::M+1 );
  BOOST_REQUIRE( AVMatrixQ::N == MatrixQ::N+1 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == pde.D );
  BOOST_REQUIRE( avpde.N == pde.N+1 );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == pde.hasFluxAdvectiveTime() );
  BOOST_CHECK( avpde.hasFluxAdvective() == pde.hasFluxAdvective() );
  BOOST_CHECK( avpde.hasFluxViscous() == (pde.hasFluxViscous() or true) );
  BOOST_CHECK( avpde.hasSource() == (pde.hasSource() or true) );
  BOOST_CHECK( avpde.hasSourceTrace() == pde.hasSourceTrace() );
  BOOST_CHECK( avpde.hasForcingFunction() == pde.hasSourceTrace() );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == pde.fluxViscousLinearInGradient() );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == pde.needsSolutionGradientforSource() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxAdvective_zeroSensor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-13;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, time;
  Real rho, u, v, t, s;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = 0.821; t = 0.987, s = 0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  AVArrayQ avq = 0;
  avpde.setDOFFrom( avq, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rho, u, v, t), s ) );

  // flux in time direction
  ArrayQ ftTrue = 0;
AVArrayQ ft = 0;
  avpde.fluxAdvectiveTime( param, x, y, time, avq, ft );
  pde.fluxAdvectiveTime( x, y, time, q, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );

  // Jacobian of flux in time direction
  AVMatrixQ Jt = 0;
  MatrixQ JtTrue = 0;
  avpde.jacobianFluxAdvectiveTime( param, x, y, time, avq, Jt );
  pde.jacobianFluxAdvectiveTime( x, y, time, q, JtTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtTrue(i,j), Jt(i,j), small_tol, close_tol )
    }
  }

  // advective flux
  ArrayQ fTrue = 0, gTrue = 0;
  AVArrayQ f = 0, g = 0;
  avpde.fluxAdvective( param, x, y, time, avq, f, g );
  pde.fluxAdvective( x, y, time, q, fTrue, gTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );

  // Jacobian of advective flux
  AVMatrixQ Jxadv = 0, Jyadv = 0;
  MatrixQ JxadvTrue = 0, JyadvTrue = 0;
  avpde.jacobianFluxAdvective( param, x, y, time, avq, Jxadv, Jyadv );
  pde.jacobianFluxAdvective( x, y, time, q, JxadvTrue, JyadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JxadvTrue(i,j), Jxadv(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( JyadvTrue(i,j), Jyadv(i,j), small_tol, close_tol )
    }
  }

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21};
  ArrayQ qt = {0.21, 0.31, -0.14, 0.26};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.0};
  AVArrayQ avqt = {0.21, 0.31, -0.14, 0.26, 0.0};

  // strong advective flux
  ArrayQ fadvStrongTrue = 0;
  AVArrayQ fadvStrong = 0;
  avpde.strongFluxAdvective( param, x, y, time, avq, avqx, avqy, fadvStrong );
  pde.strongFluxAdvective( x, y, time, q, qx, qy, fadvStrongTrue );
  BOOST_CHECK_CLOSE( fadvStrongTrue(0), fadvStrong(0), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(1), fadvStrong(1), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(2), fadvStrong(2), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(3), fadvStrong(3), tol );

  // strong advective flux
  ArrayQ ftadvStrongTrue = 0;
  AVArrayQ ftadvStrong = 0;
  avpde.strongFluxAdvectiveTime( param, x, y, time, avq, avqt, ftadvStrong );
  pde.strongFluxAdvectiveTime( x, y, time, q, qt, ftadvStrongTrue );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(0), ftadvStrong(0), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(1), ftadvStrong(1), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(2), ftadvStrong(2), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(3), ftadvStrong(3), tol );

  Real rhoL = 1.137, uL = 0.784, vL = -0.231, tL = 0.987, sL = 0.0;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, tR = 0.865, sR = 0.0;
  Real nx = 2.0/7.0; Real ny = sqrt(1.0-nx*nx);
  Real nt = 1.0;

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  pde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );
  AVArrayQ avqL = 0, avqR = 0;
  avpde.setDOFFrom( avqL, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL), sL ) );
  avpde.setDOFFrom( avqR, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR), sR ) );

  // upwinded advective flux
  f = 0;
  fTrue = 0;
  avpde.fluxAdvectiveUpwind( param, x, y, time, avqL, avqR, nx, ny, f );
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );

  // upwinded space-time advective flux
  ft = 0;
  ftTrue = 0;
  avpde.fluxAdvectiveUpwindSpaceTime( param, x, y, time, avqL, avqR, nx, ny, nt, ft );
  pde.fluxAdvectiveUpwindSpaceTime( x, y, time, qL, qR, nx, ny, nt, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );

  // absolute value of Jacobian of advective flux
  AVMatrixQ Jadv = 0;
  MatrixQ JadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValue( param, x, y, time, avq, nx, ny, Jadv );
  pde.jacobianFluxAdvectiveAbsoluteValue( x, y, time, q, nx, ny, JadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JadvTrue(i,j), Jadv(i,j), small_tol, close_tol )
    }
  }

  // Right now jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // absolute value of Jacobian of advective flux
  AVMatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, time, avq, nx, ny, nt, Jtadv );
  pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( x, y, time, q, nx, ny, nt, JtadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtadvTrue(i,j), Jtadv(i,j), small_tol, close_tol )
    }
  }
#else
  // absolute value of Jacobian of advective flux
  AVMatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  BOOST_CHECK_THROW(avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, time, avq, nx, ny, nt, Jtadv ), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( x, y, time, q, nx, ny, nt, JtadvTrue ), DeveloperException);
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxAdvective_nonZeroSensor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-13;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  // function tests

  Real x, y, time;
  Real rho, u, v, t, s;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = 0.821; t = 0.987, s = 0.3;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  AVArrayQ avq = 0;
  avpde.setDOFFrom( avq, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rho, u, v, t), s ) );

  // flux in time direction
  ArrayQ ftTrue = 0;
AVArrayQ ft = 0;
  avpde.fluxAdvectiveTime( param, x, y, time, avq, ft );
  pde.fluxAdvectiveTime( x, y, time, q, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );

  // Jacobian of flux in time direction
  AVMatrixQ Jt = 0;
  MatrixQ JtTrue = 0;
  avpde.jacobianFluxAdvectiveTime( param, x, y, time, avq, Jt );
  pde.jacobianFluxAdvectiveTime( x, y, time, q, JtTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtTrue(i,j), Jt(i,j), small_tol, close_tol )
    }
  }

  // advective flux
  ArrayQ fTrue = 0, gTrue = 0;
  AVArrayQ f = 0, g = 0;
  avpde.fluxAdvective( param, x, y, time, avq, f, g );
  pde.fluxAdvective( x, y, time, q, fTrue, gTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );

  // Jacobian of advective flux
  AVMatrixQ Jxadv = 0, Jyadv = 0;
  MatrixQ JxadvTrue = 0, JyadvTrue = 0;
  avpde.jacobianFluxAdvective( param, x, y, time, avq, Jxadv, Jyadv );
  pde.jacobianFluxAdvective( x, y, time, q, JxadvTrue, JyadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JxadvTrue(i,j), Jxadv(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( JyadvTrue(i,j), Jyadv(i,j), small_tol, close_tol )
    }
  }

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21};
  ArrayQ qt = {0.21, 0.31, -0.14, 0.26};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.12};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, -0.23};
  AVArrayQ avqt = {0.21, 0.31, -0.14, 0.26, 0.01};

  // strong advective flux
  ArrayQ fadvStrongTrue = 0;
  AVArrayQ fadvStrong = 0;
  avpde.strongFluxAdvective( param, x, y, time, avq, avqx, avqy, fadvStrong );
  pde.strongFluxAdvective( x, y, time, q, qx, qy, fadvStrongTrue );
  BOOST_CHECK_CLOSE( fadvStrongTrue(0), fadvStrong(0), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(1), fadvStrong(1), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(2), fadvStrong(2), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(3), fadvStrong(3), tol );

  // strong advective flux
  ArrayQ ftadvStrongTrue = 0;
  AVArrayQ ftadvStrong = 0;
  avpde.strongFluxAdvectiveTime( param, x, y, time, avq, avqt, ftadvStrong );
  pde.strongFluxAdvectiveTime( x, y, time, q, qt, ftadvStrongTrue );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(0), ftadvStrong(0), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(1), ftadvStrong(1), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(2), ftadvStrong(2), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(3), ftadvStrong(3), tol );

  Real rhoL = 1.137, uL = 0.784, vL = -0.231, tL = 0.987, sL = 0.3;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, tR = 0.865, sR = 0.4;
  Real nx = 2.0/7.0; Real ny = sqrt(1.0-nx*nx);
  Real nt = 1.0;

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  pde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );
  AVArrayQ avqL = 0, avqR = 0;
  avpde.setDOFFrom( avqL, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL), sL ) );
  avpde.setDOFFrom( avqR, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR), sR ) );

  // upwinded advective flux
  f = 0;
  fTrue = 0;
  avpde.fluxAdvectiveUpwind( param, x, y, time, avqL, avqR, nx, ny, f );
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );

  // upwinded space-time advective flux
  ft = 0;
  ftTrue = 0;
  avpde.fluxAdvectiveUpwindSpaceTime( param, x, y, time, avqL, avqR, nx, ny, nt, ft );
  pde.fluxAdvectiveUpwindSpaceTime( x, y, time, qL, qR, nx, ny, nt, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );

  // absolute value of Jacobian of advective flux
  AVMatrixQ Jadv = 0;
  MatrixQ JadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValue( param, x, y, time, avq, nx, ny, Jadv );
  pde.jacobianFluxAdvectiveAbsoluteValue( x, y, time, q, nx, ny, JadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JadvTrue(i,j), Jadv(i,j), small_tol, close_tol )
    }
  }

  // Right now jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // absolute value of Jacobian of advective flux
  AVMatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, time, avq, nx, ny, nt, Jtadv );
  pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( x, y, time, q, nx, ny, nt, JtadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtadvTrue(i,j), Jtadv(i,j), small_tol, close_tol )
    }
  }
#else
  // absolute value of Jacobian of advective flux
  AVMatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  BOOST_CHECK_THROW(avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, time, avq, nx, ny, nt, Jtadv ), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( x, y, time, q, nx, ny, nt, JtadvTrue ), DeveloperException);
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxViscous_zeroSensor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-13;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length
  typedef DLA::MatrixSymS<PhysD2::D+1,Real> HTypeT;
  typedef HTypeT ParamTypeT; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real rhoL, uL, vL,tL, sL;
  Real rhoR, uR, vR, tR, sR;
  Real nx = 2.0/7.0; Real ny = sqrt(1.0-nx*nx); Real nt = 1.0;

  x = y = time = 0;   // not actually used in functions
  rhoL = 1.034; uL = 3.26; vL = 1.23; tL = 5.78; sL = 0.0;
  rhoR = 0.973; uR = 1.79; vR = 0.34; tR = 6.13; sR = 0.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing
  HTypeT HT = {{0.5},{0.1, 0.3}, {0.0, 0.4, 1.0}};
  HTypeT logHT = log(HT);
  ParamTypeT paramT(logHT); // grid spacing

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  pde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );
  AVArrayQ avqL = 0, avqR = 0;
  avpde.setDOFFrom( avqL, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL), sL ) );
  avpde.setDOFFrom( avqR, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR), sR ) );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21};
  ArrayQ qtL = {1.43, 0.23, 0.34, -0.41};
  AVArrayQ avqxL = {0.17, 0.21, -0.34, 0.19, 0.0};
  AVArrayQ avqyL = {-0.05, 0.61, 0.12, -0.21, 0.0};
  AVArrayQ avqtL = {1.43, 0.23, 0.34, -0.41, 0.0};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15};
  ArrayQ qtR = {0.34, 0.45, -0.12, -0.43};
  AVArrayQ avqxR = {0.21, 0.31, -0.14, 0.2, 0.0};
  AVArrayQ avqyR = {0.11, 0.48, 0.27, -0.15, 0.0};
  AVArrayQ avqtR = {0.34, 0.45, -0.12, -0.43, 0.0};

  ArrayQ qxx = {1.25, 5.42, 0.43, 8.45};
  ArrayQ qxy = {0.35, 3.62, 5.43, 3.23};
  ArrayQ qyy = {4.23, 3.35, 7.43, 0.32};
  ArrayQ qxt = {0.34, -1.12, 0.45, 0.23};
  ArrayQ qyt = {0.55, 3.54, 0.85, 0.17};
  ArrayQ qtt = {0.72, 0.49, 0.03, 0.82};
  AVArrayQ avqxx = {1.25, 5.42, 0.43, 8.45, 0.0};
  AVArrayQ avqxy = {0.35, 3.62, 5.43, 3.23, 0.0};
  AVArrayQ avqyy = {4.23, 3.35, 7.43, 0.32, 0.0};
  AVArrayQ avqxt = {0.34, -1.12, 0.45, 0.23, 0.0};
  AVArrayQ avqyt = {0.55, 3.54, 0.85, 0.17, 0.0};
  AVArrayQ avqtt = {0.72, 0.49, 0.03, 0.82, 0.0};

  // spacetime viscous flux
  ArrayQ fTrue = 0, gTrue = 0;
  AVArrayQ f = 0, g = 0;
  avpde.fluxViscous( param, x, y, time, avqL, avqxL, avqyL, f, g );
  pde.fluxViscous( x, y, time, qL, qxL, qyL, fTrue, gTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );

  // viscous flux
  ArrayQ ftTrue = 0, gtTrue = 0, htTrue = 0;
  AVArrayQ ft = 0, gt = 0, ht = 0;
  avpde.fluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, ft, gt, ht );
  pde.fluxViscousSpaceTime( x, y, time, qL, qxL, qyL, qtL, ftTrue, gtTrue, htTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( gtTrue(0), gt(0), tol );
  BOOST_CHECK_CLOSE( gtTrue(1), gt(1), tol );
  BOOST_CHECK_CLOSE( gtTrue(2), gt(2), tol );
  BOOST_CHECK_CLOSE( gtTrue(3), gt(3), tol );
  BOOST_CHECK_CLOSE( htTrue(0), ht(0), tol );
  BOOST_CHECK_CLOSE( htTrue(1), ht(1), tol );
  BOOST_CHECK_CLOSE( htTrue(2), ht(2), tol );
  BOOST_CHECK_CLOSE( htTrue(3), ht(3), tol );

  // central viscous flux
  fTrue = 0;
  f = 0;
  avpde.fluxViscous( param, param, x, y, time, avqL, avqxL, avqyL, avqR, avqxR, avqyR, nx, ny, f );
  pde.fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );

  // spacetime central viscous flux
  fTrue = 0;
  f = 0;
  avpde.fluxViscousSpaceTime( paramT, paramT, x, y, time, avqL, avqxL, avqyL, avqtL, avqR, avqxR, avqyR, avqtR, nx, ny, nt, f );
  pde.fluxViscousSpaceTime( x, y, time, qL, qxL, qyL, qtL, qR, qxR, qyR, qtR, nx, ny, nt, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );

  // diffusion coeficients
  AVMatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  MatrixQ kxxTrue = 0, kxyTrue = 0, kyxTrue = 0, kyyTrue = 0;
  avpde.diffusionViscous( param, x, y, time, avqL, avqxL, avqyL, kxx, kxy, kyx, kyy );
  pde.diffusionViscous(  x, y, time, qL, qxL, qyL, kxxTrue, kxyTrue, kyxTrue, kyyTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx(i,j), kxxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy(i,j), kxyTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx(i,j), kyxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyy(i,j), kyyTrue(i,j), small_tol, close_tol )
    }
  }

  // diffusion coefficients
  AVMatrixQ dfdu = 0, dgdu = 0;
  MatrixQ dfduTrue = 0, dgduTrue = 0;
  avpde.jacobianFluxViscous( param, x, y, time, avqL, avqxL, avqyL, dfdu, dgdu );
  pde.jacobianFluxViscous(  x, y, time, qL, qxL, qyL, dfduTrue, dgduTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dfdu(i,j), dfduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dgdu(i,j), dgduTrue(i,j), small_tol, close_tol )
    }
  }

  // Right now jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // spacetime diffusion coefficients
  AVMatrixQ dftdu = 0, dgtdu = 0, dhtdu = 0;
  MatrixQ dftduTrue = 0, dgtduTrue = 0, dhtduTrue = 0;
  avpde.jacobianFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, dftdu, dgtdu, dhtdu );
  pde.jacobianFluxViscousSpaceTime(  x, y, time, qL, qxL, qyL, qtL, dftduTrue, dgtduTrue, dhtduTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dftdu(i,j), dftduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dgtdu(i,j), dgtduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dhtdu(i,j), dhtduTrue(i,j), small_tol, close_tol )
    }
  }
#else
  // spacetime diffusion coefficients
  AVMatrixQ dftdu = 0, dgtdu = 0, dhtdu = 0;
  MatrixQ dftduTrue = 0, dgtduTrue = 0, dhtduTrue = 0;
  BOOST_CHECK_THROW(avpde.jacobianFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, dftdu, dgtdu, dhtdu ), DeveloperException);
//  BOOST_CHECK_THROW(pde.jacobianFluxViscousSpaceTime(  x, y, time, qL, qxL, qyL, qtL, dftduTrue, dgtduTrue, dhtduTrue ), DeveloperException);
#endif

  // strong viscous flux
  ArrayQ fviscStrongTrue = 0;
  AVArrayQ fviscStrong = 0;
  avpde.strongFluxViscous( param, x, y, time, avqL, avqxL, avqyL, avqxx, avqxy, avqyy, fviscStrong );
  pde.strongFluxViscous( x, y, time, qL, qxL, qyL, qxx, qxy, qyy, fviscStrongTrue );
  BOOST_CHECK_CLOSE( fviscStrongTrue(0), fviscStrong(0), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(1), fviscStrong(1), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(2), fviscStrong(2), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(3), fviscStrong(3), tol );

  // Right now strongFluxViscousSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // strong spacetime viscous flux
  ArrayQ ftviscStrongTrue = 0;
  AVArrayQ ftviscStrong = 0;
  avpde.strongFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, avqxx, avqxy, avqyy, avqxt, avqyt, avqtt, ftviscStrong );
  pde.strongFluxViscousSpaceTime( x, y, time, qL, qxL, qyL, qtL, qxx, qxy, qyy, qxt, qyt, qtt, ftviscStrongTrue );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(0), ftviscStrong(0), tol );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(1), ftviscStrong(1), tol );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(2), ftviscStrong(2), tol );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(3), ftviscStrong(3), tol );
#else
  // strong spacetime viscous flux
  ArrayQ ftviscStrongTrue = 0;
  AVArrayQ ftviscStrong = 0;
  BOOST_CHECK_THROW(
      avpde.strongFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, avqxx, avqxy, avqyy, avqxt, avqyt, avqtt, ftviscStrong ),
      DeveloperException);
//  BOOST_CHECK_THROW(
//      pde.strongFluxViscousSpaceTime( x, y, time, qL, qxL, qyL, qtL, qxx, qxy, qyy, qxt, qyt, qtt, ftviscStrongTrue ),
//      DeveloperException);
#endif

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  AVMatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
  MatrixQ kxx_xTrue = 0, kxy_xTrue = 0, kyx_xTrue =0, kxy_yTrue = 0, kyx_yTrue = 0, kyy_yTrue = 0;
  avpde.diffusionViscousGradient( param, x, y, time, avqL, avqxL, avqyL, avqxx, avqxy, avqyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y );
  pde.diffusionViscousGradient( x, y, time, qL, qxL, qyL, qxx, qxy, qyy, kxx_xTrue, kxy_xTrue, kyx_xTrue, kxy_yTrue, kyx_yTrue, kyy_yTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxViscous_nonZeroSensor_EnthalpyLaplacian, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-12;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length
  typedef DLA::MatrixSymS<PhysD2::D+1,Real> HTypeT;
  typedef HTypeT ParamTypeT; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real rhoL, uL, vL,tL, sL;
  Real rhoR, uR, vR, tR, sR;
  Real nx = 2.0/7.0; Real ny = sqrt(1.0-nx*nx);

  x = y = time = 0;   // not actually used in functions
  rhoL = 1.034; uL = 3.26; vL = 1.23; tL = 5.78; sL = 0.3;
  rhoR = 0.973; uR = 1.79; vR = 0.34; tR = 6.13; sR = 0.5;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing
  HTypeT HT = {{0.5},{0.1, 0.3}, {0.0, 0.4, 1.0}};
  HTypeT logHT = log(HT);
  ParamTypeT paramT(logHT); // grid spacing

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  pde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );
  AVArrayQ avqL = 0, avqR = 0;
  avpde.setDOFFrom( avqL, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL), sL ) );
  avpde.setDOFFrom( avqR, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR), sR ) );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21};
  ArrayQ qtL = {1.43, 0.23, 0.34, -0.41};
  AVArrayQ avqxL = {0.17, 0.21, -0.34, 0.19, 0.34};
  AVArrayQ avqyL = {-0.05, 0.61, 0.12, -0.21, 0.15};
  AVArrayQ avqtL = {1.43, 0.23, 0.34, -0.41, 0.61};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15};
  ArrayQ qtR = {0.34, 0.45, -0.12, -0.43};
  AVArrayQ avqxR = {0.21, 0.31, -0.14, 0.26, 0.12};
  AVArrayQ avqyR = {0.11, 0.48, 0.27, -0.15, 0.81};
  AVArrayQ avqtR = {0.34, 0.45, -0.12, -0.43, 0.46};

  ArrayQ qxx = {1.25, 5.42, 0.43, 8.45};
  ArrayQ qxy = {0.35, 3.62, 5.43, 3.23};
  ArrayQ qyy = {4.23, 3.35, 7.43, 0.32};
  ArrayQ qxt = {0.34, -1.12, 0.45, 0.23};
  ArrayQ qyt = {0.55, 3.54, 0.85, 0.17};
  ArrayQ qtt = {0.72, 0.49, 0.03, 0.82};
  AVArrayQ avqxx = {1.25, 5.42, 0.43, 8.45, 0.54};
  AVArrayQ avqxy = {0.35, 3.62, 5.43, 3.23, 0.29};
  AVArrayQ avqyy = {4.23, 3.35, 7.43, 0.32, 0.93};
  AVArrayQ avqxt = {0.34, -1.12, 0.45, 0.23, 0.43};
  AVArrayQ avqyt = {0.55, 3.54, 0.85, 0.17, 0.72};
  AVArrayQ avqtt = {0.72, 0.49, 0.03, 0.82, 0.23};

  MatrixQ kxxLT = 0, kxyLT = 0;
  MatrixQ kyxLT = 0, kyyLT = 0;
  MatrixQ kxxRT = 0, kxyRT = 0;
  MatrixQ kyxRT = 0, kyyRT = 0;

  MatrixQ kxxL_AV = 0, kxyL_AV = 0;
  MatrixQ kyxL_AV = 0, kyyL_AV = 0;
  MatrixQ kxxR_AV = 0, kxyR_AV = 0;
  MatrixQ kyxR_AV = 0, kyyR_AV = 0;

  Real lambdaL = 0, lambdaR = 0;
  pde.speedCharacteristic( x, y, time, qL, lambdaL );
  pde.speedCharacteristic( x, y, time, qR, lambdaR );

#if 0
  Real theta_L = 0.001;
  Real theta_H = 1.0;
  sensor = smoothActivation_sine(sensor, theta_L, theta_H);
#else
//  Real alpha = 500;
//  Real eps_exp = 1e-10;
//
//  sensor = smoothActivation_exp(sensor, alpha, eps_exp);


  Real zz = 0.0;
  Real sensorL = std::max(sL, zz);
  Real sensorR = std::max(sR, zz);
#endif

  Real factorL = 2.0/(Real(order)) * lambdaL * sensorL; //smoothabs0(sensorL, 1.0e-5);
  Real factorR = 2.0/(Real(order)) * lambdaR * sensorR; //smoothabs0(sensorL, 1.0e-5);

  MatrixQ dutduL = DLA::Identity();
  dutduL(3, 0) = 0.5*(gamma - 1.0)*(uL*uL + vL*vL);
  dutduL(3, 1) = -(gamma - 1.0)*uL;
  dutduL(3, 2) = -(gamma - 1.0)*vL;
  dutduL(3, 3) = gamma;

  MatrixQ dutduR = DLA::Identity();
  dutduR(3, 0) = 0.5*(gamma - 1.0)*(uR*uR + vR*vR);
  dutduR(3, 1) = -(gamma - 1.0)*uR;
  dutduR(3, 2) = -(gamma - 1.0)*vR;
  dutduR(3, 3) = gamma;

  for (int i = 0; i < PDEClass::N; i++)
    for (int j = 0; j < PDEClass::N; j++)
    {
      if (i == j)
      {
        kxxLT(i,j) = factorL*H(0,0);
        kxyLT(i,j) = factorL*H(0,1);
        kyxLT(i,j) = factorL*H(1,0);
        kyyLT(i,j) = factorL*H(1,1);

        kxxRT(i,j) = factorR*H(0,0);
        kxyRT(i,j) = factorR*H(0,1);
        kyxRT(i,j) = factorR*H(1,0);
        kyyRT(i,j) = factorR*H(1,1);
      }
      else
      {
        kxxLT(i,j) = 0.0;
        kxyLT(i,j) = 0.0;
        kyxLT(i,j) = 0.0;
        kyyLT(i,j) = 0.0;

        kxxRT(i,j) = 0.0;
        kxyRT(i,j) = 0.0;
        kyxRT(i,j) = 0.0;
        kyyRT(i,j) = 0.0;
      }
    }

  kxxL_AV = kxxLT*dutduL;
  kxyL_AV = kxyLT*dutduL;
  kyxL_AV = kyxLT*dutduL;
  kyyL_AV = kyyLT*dutduL;

  kxxR_AV = kxxRT*dutduR;
  kxyR_AV = kxyRT*dutduR;
  kyxR_AV = kyxRT*dutduR;
  kyyR_AV = kyyRT*dutduR;

  MatrixQ dudqL = 0, dudqR = 0;
  pde.jacobianMasterState(x, y, time, qL, dudqL);
  pde.jacobianMasterState(x, y, time, qR, dudqR);

  ArrayQ fL_AV = -kxxL_AV*dudqL*qxL - kxyL_AV*dudqL*qyL;
  ArrayQ gL_AV = -kyxL_AV*dudqL*qxL - kyyL_AV*dudqL*qyL;

  ArrayQ fR_AV = -kxxR_AV*dudqR*qxR - kxyR_AV*dudqR*qyR;
  ArrayQ gR_AV = -kyxR_AV*dudqR*qxR - kyyR_AV*dudqR*qyR;

  // spacetime viscous flux
  ArrayQ fBase = 0, gBase = 0;
  AVArrayQ f = 0, g = 0;
  avpde.fluxViscous( param, x, y, time, avqR, avqxR, avqyR, f, g );
  pde.fluxViscous( x, y, time, qR, qxR, qyR, fBase, gBase );

  ArrayQ fTrue = 0, gTrue = 0;
  fTrue = fBase + fR_AV;
  gTrue = gBase + gR_AV;

  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );


  // central viscous flux
  fTrue = 0;
  fBase = 0;
  f = 0;
  avpde.fluxViscous( param, param, x, y, time, avqL, avqxL, avqyL, avqR, avqxR, avqyR, nx, ny, f );
  pde.fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fBase );

  fTrue = fBase + 0.5*(fL_AV + fR_AV)*nx + 0.5*(gL_AV + gR_AV)*ny;

  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );

  // diffusion coeficients
  AVMatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  MatrixQ kxxBase = 0, kxyBase = 0, kyxBase = 0, kyyBase = 0;
  avpde.diffusionViscous( param, x, y, time, avqL, avqxL, avqyL, kxx, kxy, kyx, kyy );
  pde.diffusionViscous(  x, y, time, qL, qxL, qyL, kxxBase, kxyBase, kyxBase, kyyBase );

  MatrixQ kxxTrue = 0, kxyTrue = 0, kyxTrue = 0, kyyTrue = 0;
  kxxTrue = kxxBase + kxxL_AV;
  kxyTrue = kxyBase + kxyL_AV;
  kyxTrue = kyxBase + kyxL_AV;
  kyyTrue = kyyBase + kyyL_AV;

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx(i,j), kxxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy(i,j), kxyTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx(i,j), kyxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyy(i,j), kyyTrue(i,j), small_tol, close_tol )
    }
  }

  // diffusion coefficients
  AVMatrixQ dfdu = 0, dgdu = 0;
  MatrixQ dfduTrue = 0, dgduTrue = 0;
  avpde.jacobianFluxViscous( param, x, y, time, avqL, avqxL, avqyL, dfdu, dgdu );
  pde.jacobianFluxViscous(  x, y, time, qL, qxL, qyL, dfduTrue, dgduTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dfdu(i,j), dfduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dgdu(i,j), dgduTrue(i,j), small_tol, close_tol )
    }
  }

  // Right now jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // spacetime diffusion coefficients
  AVMatrixQ dftdu = 0, dgtdu = 0, dhtdu = 0;
  MatrixQ dftduTrue = 0, dgtduTrue = 0, dhtduTrue = 0;
  avpde.jacobianFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, dftdu, dgtdu, dhtdu );
  pde.jacobianFluxViscousSpaceTime(  x, y, time, qL, qxL, qyL, qtL, dftduTrue, dgtduTrue, dhtduTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dftdu(i,j), dftduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dgtdu(i,j), dgtduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dhtdu(i,j), dhtduTrue(i,j), small_tol, close_tol )
    }
  }
#else
  // spacetime diffusion coefficients
  AVMatrixQ dftdu = 0, dgtdu = 0, dhtdu = 0;
  MatrixQ dftduTrue = 0, dgtduTrue = 0, dhtduTrue = 0;
  BOOST_CHECK_THROW(avpde.jacobianFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, dftdu, dgtdu, dhtdu ), DeveloperException);
//  BOOST_CHECK_THROW(pde.jacobianFluxViscousSpaceTime(  x, y, time, qL, qxL, qyL, qtL, dftduTrue, dgtduTrue, dhtduTrue ), DeveloperException);
#endif

  // strong viscous flux
  ArrayQ fviscStrongTrue = 0;
  AVArrayQ fviscStrong = 0;
  avpde.strongFluxViscous( param, x, y, time, avqL, avqxL, avqyL, avqxx, avqxy, avqyy, fviscStrong );
  pde.strongFluxViscous( x, y, time, qL, qxL, qyL, qxx, qxy, qyy, fviscStrongTrue );
  BOOST_CHECK_CLOSE( fviscStrongTrue(0), fviscStrong(0), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(1), fviscStrong(1), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(2), fviscStrong(2), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(3), fviscStrong(3), tol );

  // Right now strongFluxViscousSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // strong spacetime viscous flux
  ArrayQ ftviscStrongTrue = 0;
  AVArrayQ ftviscStrong = 0;
  avpde.strongFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, avqxx, avqxy, avqyy, avqxt, avqyt, avqtt, ftviscStrong );
  pde.strongFluxViscousSpaceTime( x, y, time, qL, qxL, qyL, qtL, qxx, qxy, qyy, qxt, qyt, qtt, ftviscStrongTrue );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(0), ftviscStrong(0), tol );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(1), ftviscStrong(1), tol );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(2), ftviscStrong(2), tol );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(3), ftviscStrong(3), tol );
#else
  // strong spacetime viscous flux
  ArrayQ ftviscStrongTrue = 0;
  AVArrayQ ftviscStrong = 0;
  BOOST_CHECK_THROW(
      avpde.strongFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, avqxx, avqxy, avqyy, avqxt, avqyt, avqtt, ftviscStrong ),
      DeveloperException);
//  BOOST_CHECK_THROW(
//      pde.strongFluxViscousSpaceTime( x, y, time, qL, qxL, qyL, qtL, qxx, qxy, qyy, qxt, qyt, qtt, ftviscStrongTrue ),
//      DeveloperException);
#endif

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  AVMatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
  MatrixQ kxx_xTrue = 0, kxy_xTrue = 0, kyx_xTrue =0, kxy_yTrue = 0, kyx_yTrue = 0, kyy_yTrue = 0;
  avpde.diffusionViscousGradient( param, x, y, time, avqL, avqxL, avqyL, avqxx, avqxy, avqyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y );
  pde.diffusionViscousGradient( x, y, time, qL, qxL, qyL, qxx, qxy, qyy, kxx_xTrue, kxy_xTrue, kyx_xTrue, kxy_yTrue, kyx_yTrue, kyy_yTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxViscous_nonZeroSensor_EnergyLaplacian, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-12;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length
  typedef DLA::MatrixSymS<PhysD2::D+1,Real> HTypeT;
  typedef HTypeT ParamTypeT; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnergy,
                   gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real rhoL, uL, vL,tL, sL;
  Real rhoR, uR, vR, tR, sR;
  Real nx = 2.0/7.0; Real ny = sqrt(1.0-nx*nx);

  x = y = time = 0;   // not actually used in functions
  rhoL = 1.034; uL = 3.26; vL = 1.23; tL = 5.78; sL = 0.3;
  rhoR = 0.973; uR = 1.79; vR = 0.34; tR = 6.13; sR = 0.5;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing
  HTypeT HT = {{0.5},{0.1, 0.3}, {0.0, 0.4, 1.0}};
  HTypeT logHT = log(HT);
  ParamTypeT paramT(logHT); // grid spacing

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  pde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );
  AVArrayQ avqL = 0, avqR = 0;
  avpde.setDOFFrom( avqL, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL), sL ) );
  avpde.setDOFFrom( avqR, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR), sR ) );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21};
  ArrayQ qtL = {1.43, 0.23, 0.34, -0.41};
  AVArrayQ avqxL = {0.17, 0.21, -0.34, 0.19, 0.34};
  AVArrayQ avqyL = {-0.05, 0.61, 0.12, -0.21, 0.15};
  AVArrayQ avqtL = {1.43, 0.23, 0.34, -0.41, 0.61};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15};
  ArrayQ qtR = {0.34, 0.45, -0.12, -0.43};
  AVArrayQ avqxR = {0.21, 0.31, -0.14, 0.26, 0.12};
  AVArrayQ avqyR = {0.11, 0.48, 0.27, -0.15, 0.81};
  AVArrayQ avqtR = {0.34, 0.45, -0.12, -0.43, 0.46};

  ArrayQ qxx = {1.25, 5.42, 0.43, 8.45};
  ArrayQ qxy = {0.35, 3.62, 5.43, 3.23};
  ArrayQ qyy = {4.23, 3.35, 7.43, 0.32};
  ArrayQ qxt = {0.34, -1.12, 0.45, 0.23};
  ArrayQ qyt = {0.55, 3.54, 0.85, 0.17};
  ArrayQ qtt = {0.72, 0.49, 0.03, 0.82};
  AVArrayQ avqxx = {1.25, 5.42, 0.43, 8.45, 0.54};
  AVArrayQ avqxy = {0.35, 3.62, 5.43, 3.23, 0.29};
  AVArrayQ avqyy = {4.23, 3.35, 7.43, 0.32, 0.93};
  AVArrayQ avqxt = {0.34, -1.12, 0.45, 0.23, 0.43};
  AVArrayQ avqyt = {0.55, 3.54, 0.85, 0.17, 0.72};
  AVArrayQ avqtt = {0.72, 0.49, 0.03, 0.82, 0.23};

  MatrixQ kxxL_AV = 0, kxyL_AV = 0;
  MatrixQ kyxL_AV = 0, kyyL_AV = 0;
  MatrixQ kxxR_AV = 0, kxyR_AV = 0;
  MatrixQ kyxR_AV = 0, kyyR_AV = 0;

  Real lambdaL = 0, lambdaR = 0;
  pde.speedCharacteristic( x, y, time, qL, lambdaL );
  pde.speedCharacteristic( x, y, time, qR, lambdaR );

#if 0
  Real theta_L = 0.001;
  Real theta_H = 1.0;
  sensor = smoothActivation_sine(sensor, theta_L, theta_H);
#else
//  Real alpha = 500;
//  Real eps_exp = 1e-10;
//
//  sensor = smoothActivation_exp(sensor, alpha, eps_exp);


  Real zz = 0.0;
  Real sensorL = std::max(sL, zz);
  Real sensorR = std::max(sR, zz);
#endif

  Real factorL = 2.0/(Real(order)) * lambdaL * sensorL; //smoothabs0(sensorL, 1.0e-5);
  Real factorR = 2.0/(Real(order)) * lambdaR * sensorR; //smoothabs0(sensorL, 1.0e-5);

  for (int i = 0; i < PDEClass::N; i++)
    for (int j = 0; j < PDEClass::N; j++)
    {
      if (i == j)
      {
        kxxL_AV(i,j) = factorL*H(0,0);
        kxyL_AV(i,j) = factorL*H(0,1);
        kyxL_AV(i,j) = factorL*H(1,0);
        kyyL_AV(i,j) = factorL*H(1,1);

        kxxR_AV(i,j) = factorR*H(0,0);
        kxyR_AV(i,j) = factorR*H(0,1);
        kyxR_AV(i,j) = factorR*H(1,0);
        kyyR_AV(i,j) = factorR*H(1,1);
      }
      else
      {
        kxxL_AV(i,j) = 0.0;
        kxyL_AV(i,j) = 0.0;
        kyxL_AV(i,j) = 0.0;
        kyyL_AV(i,j) = 0.0;

        kxxR_AV(i,j) = 0.0;
        kxyR_AV(i,j) = 0.0;
        kyxR_AV(i,j) = 0.0;
        kyyR_AV(i,j) = 0.0;
      }
    }

  MatrixQ dudqL = 0, dudqR = 0;
  pde.jacobianMasterState(x, y, time, qL, dudqL);
  pde.jacobianMasterState(x, y, time, qR, dudqR);

  ArrayQ fL_AV = -kxxL_AV*dudqL*qxL - kxyL_AV*dudqL*qyL;
  ArrayQ gL_AV = -kyxL_AV*dudqL*qxL - kyyL_AV*dudqL*qyL;

  ArrayQ fR_AV = -kxxR_AV*dudqR*qxR - kxyR_AV*dudqR*qyR;
  ArrayQ gR_AV = -kyxR_AV*dudqR*qxR - kyyR_AV*dudqR*qyR;

  // spacetime viscous flux
  ArrayQ fBase = 0, gBase = 0;
  AVArrayQ f = 0, g = 0;
  avpde.fluxViscous( param, x, y, time, avqR, avqxR, avqyR, f, g );
  pde.fluxViscous( x, y, time, qR, qxR, qyR, fBase, gBase );

  ArrayQ fTrue = 0, gTrue = 0;
  fTrue = fBase + fR_AV;
  gTrue = gBase + gR_AV;

  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );


  // central viscous flux
  fTrue = 0;
  fBase = 0;
  f = 0;
  avpde.fluxViscous( param, param, x, y, time, avqL, avqxL, avqyL, avqR, avqxR, avqyR, nx, ny, f );
  pde.fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fBase );

  fTrue = fBase + 0.5*(fL_AV + fR_AV)*nx + 0.5*(gL_AV + gR_AV)*ny;

  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );

  // diffusion coeficients
  AVMatrixQ kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  MatrixQ kxxBase = 0, kxyBase = 0, kyxBase = 0, kyyBase = 0;
  avpde.diffusionViscous( param, x, y, time, avqL, avqxL, avqyL, kxx, kxy, kyx, kyy );
  pde.diffusionViscous(  x, y, time, qL, qxL, qyL, kxxBase, kxyBase, kyxBase, kyyBase );

  MatrixQ kxxTrue = 0, kxyTrue = 0, kyxTrue = 0, kyyTrue = 0;
  kxxTrue = kxxBase + kxxL_AV;
  kxyTrue = kxyBase + kxyL_AV;
  kyxTrue = kyxBase + kyxL_AV;
  kyyTrue = kyyBase + kyyL_AV;

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx(i,j), kxxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy(i,j), kxyTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx(i,j), kyxTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyy(i,j), kyyTrue(i,j), small_tol, close_tol )
    }
  }

  // diffusion coefficients
  AVMatrixQ dfdu = 0, dgdu = 0;
  MatrixQ dfduTrue = 0, dgduTrue = 0;
  avpde.jacobianFluxViscous( param, x, y, time, avqL, avqxL, avqyL, dfdu, dgdu );
  pde.jacobianFluxViscous(  x, y, time, qL, qxL, qyL, dfduTrue, dgduTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dfdu(i,j), dfduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dgdu(i,j), dgduTrue(i,j), small_tol, close_tol )
    }
  }

  // Right now jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // spacetime diffusion coefficients
  AVMatrixQ dftdu = 0, dgtdu = 0, dhtdu = 0;
  MatrixQ dftduTrue = 0, dgtduTrue = 0, dhtduTrue = 0;
  avpde.jacobianFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, dftdu, dgtdu, dhtdu );
  pde.jacobianFluxViscousSpaceTime(  x, y, time, qL, qxL, qyL, qtL, dftduTrue, dgtduTrue, dhtduTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dftdu(i,j), dftduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dgtdu(i,j), dgtduTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dhtdu(i,j), dhtduTrue(i,j), small_tol, close_tol )
    }
  }
#else
  // spacetime diffusion coefficients
  AVMatrixQ dftdu = 0, dgtdu = 0, dhtdu = 0;
  MatrixQ dftduTrue = 0, dgtduTrue = 0, dhtduTrue = 0;
  BOOST_CHECK_THROW(avpde.jacobianFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, dftdu, dgtdu, dhtdu ), DeveloperException);
//  BOOST_CHECK_THROW(pde.jacobianFluxViscousSpaceTime(  x, y, time, qL, qxL, qyL, qtL, dftduTrue, dgtduTrue, dhtduTrue ), DeveloperException);
#endif

  // strong viscous flux
  ArrayQ fviscStrongTrue = 0;
  AVArrayQ fviscStrong = 0;
  avpde.strongFluxViscous( param, x, y, time, avqL, avqxL, avqyL, avqxx, avqxy, avqyy, fviscStrong );
  pde.strongFluxViscous( x, y, time, qL, qxL, qyL, qxx, qxy, qyy, fviscStrongTrue );
  BOOST_CHECK_CLOSE( fviscStrongTrue(0), fviscStrong(0), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(1), fviscStrong(1), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(2), fviscStrong(2), tol );
  BOOST_CHECK_CLOSE( fviscStrongTrue(3), fviscStrong(3), tol );

  // Right now strongFluxViscousSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // strong spacetime viscous flux
  ArrayQ ftviscStrongTrue = 0;
  AVArrayQ ftviscStrong = 0;
  avpde.strongFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, avqxx, avqxy, avqyy, avqxt, avqyt, avqtt, ftviscStrong );
  pde.strongFluxViscousSpaceTime( x, y, time, qL, qxL, qyL, qtL, qxx, qxy, qyy, qxt, qyt, qtt, ftviscStrongTrue );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(0), ftviscStrong(0), tol );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(1), ftviscStrong(1), tol );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(2), ftviscStrong(2), tol );
  BOOST_CHECK_CLOSE( ftviscStrongTrue(3), ftviscStrong(3), tol );
#else
  // strong spacetime viscous flux
  ArrayQ ftviscStrongTrue = 0;
  AVArrayQ ftviscStrong = 0;
  BOOST_CHECK_THROW(
      avpde.strongFluxViscousSpaceTime( paramT, x, y, time, avqL, avqxL, avqyL, avqtL, avqxx, avqxy, avqyy, avqxt, avqyt, avqtt, ftviscStrong ),
      DeveloperException);
//  BOOST_CHECK_THROW(
//      pde.strongFluxViscousSpaceTime( x, y, time, qL, qxL, qyL, qtL, qxx, qxy, qyy, qxt, qyt, qtt, ftviscStrongTrue ),
//      DeveloperException);
#endif

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  AVMatrixQ kxx_x = 0, kxy_x = 0, kyx_x = 0, kxy_y = 0, kyx_y = 0, kyy_y = 0;
  MatrixQ kxx_xTrue = 0, kxy_xTrue = 0, kyx_xTrue =0, kxy_yTrue = 0, kyx_yTrue = 0, kyy_yTrue = 0;
  avpde.diffusionViscousGradient( param, x, y, time, avqL, avqxL, avqyL, avqxx, avqxy, avqyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y );
  pde.diffusionViscousGradient( x, y, time, qL, qxL, qyL, qxx, qxy, qyy, kxx_xTrue, kxy_xTrue, kyx_xTrue, kxy_yTrue, kyx_yTrue, kyy_yTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( kxx_x(i,j), kxx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy_x(i,j), kxy_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx_x(i,j), kyx_xTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kxy_y(i,j), kxy_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyx_y(i,j), kyx_yTrue(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( kyy_y(i,j), kyy_yTrue(i,j), small_tol, close_tol )
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( masterState_zeroSensor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  // function tests
  Real rho, u, v, t, s;

  rho = 1.137; u = 0.784; v = 0.821; t = 0.987; s = 0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  AVArrayQ avq = 0;
  avpde.setDOFFrom( avq, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rho, u, v, t), s ) );

  // conservative flux
  ArrayQ uTrue = 0;
  AVArrayQ uCons = 0;
  avpde.masterState( param, x, y, time, avq, uCons );
  pde.masterState( x, y, time, q, uTrue );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol )

  // conservative flux
  AVMatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;
  avpde.jacobianMasterState( param, x, y, time, avq, dudq );
  pde.jacobianMasterState( x, y, time, q, dudqTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), small_tol, close_tol )
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( masterState_nonZeroSensor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  // function tests
  Real rho, u, v, t, s;

  rho = 1.137; u = 0.784; v = 0.821; t = 0.987; s = 0.5;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;

  // set
  ArrayQ q = 0;
  pde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );
  AVArrayQ avq = 0;
  avpde.setDOFFrom( avq, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rho, u, v, t), s ) );

  // conservative flux
  ArrayQ uTrue = 0;
  AVArrayQ uCons = 0;
  avpde.masterState( param, x, y, time, avq, uCons );
  pde.masterState( x, y, time, q, uTrue );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol )

  // conservative flux
  AVMatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;
  avpde.jacobianMasterState( param, x, y, time, avq, dudq );
  pde.jacobianMasterState( x, y, time, q, dudqTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source_zeroSensor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real rhoL, uL, vL,tL, sL;
  Real rhoR, uR, vR, tR, sR;

  x = y = time = 0;   // not actually used in functions
  rhoL = 1.034; uL = 3.26; vL = 1.23; tL = 5.78; sL = 0.0;
  rhoR = 0.973; uR = 1.79; vR = 0.34; tR = 6.13; sR = 0.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  // set

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  pde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );
  AVArrayQ avqL = 0, avqR = 0;
  avpde.setDOFFrom( avqL, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL), sL ) );
  avpde.setDOFFrom( avqR, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR), sR ) );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21};
  AVArrayQ avqxL = {0.17, 0.21, -0.34, 0.19, 0.0};
  AVArrayQ avqyL = {-0.05, 0.61, 0.12, -0.21, 0.0};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15};
  AVArrayQ avqxR = {0.21, 0.31, -0.14, 0.2, 0.0};
  AVArrayQ avqyR = {0.11, 0.48, 0.27, -0.15, 0.0};

  Real lifted_quantity = 1.23;

  // vanilla source term
  AVArrayQ src = 0;
  ArrayQ srcTrue = 0;
  avpde.source( param, x, y, time, avqL, avqxL, avqyL, src );
  pde.source( x, y, time, qL, qxL, qyL, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )

  // source term with lifted quantity
  src = 0;
  srcTrue = 0;
  avpde.source( param, x, y, time, lifted_quantity, avqL, avqxL, avqyL, src );
  pde.source( x, y, time, lifted_quantity, qL, qxL, qyL, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )

  // trace source term with lifted quantity
  AVArrayQ srcL = 0, srcR = 0;
  ArrayQ srcLTrue = 0, srcRTrue = 0;
  avpde.sourceTrace( param, x, y, param, x, y, time, avqL, avqxL, avqyL, avqR, avqxR, avqyR, srcL, srcR );
  pde.sourceTrace( x, y, x, y, time, qL, qxL, qyL, qR, qxR, qyR, srcLTrue, srcRTrue );
  SANS_CHECK_CLOSE( srcLTrue(0), srcL(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(1), srcL(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(2), srcL(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(3), srcL(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(0), srcR(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(1), srcR(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(2), srcR(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(3), srcR(3), small_tol, close_tol )

  // source for lifted quantity
  src = 0;
  srcTrue = 0;
  avpde.sourceLiftedQuantity( param, x, y, param, x, y, time, avqL, avqR, src );
  pde.sourceLiftedQuantity( x, y, x, y, time, qL, qR, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )


  // source Jacobian
  AVMatrixQ Js = 0;
  MatrixQ JsTrue = 0;
  avpde.jacobianSource( param, x, y, time, avqL, avqxL, avqyL, Js );
  pde.jacobianSource( x, y, time, qL, qxL, qyL, JsTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JsTrue(i,j), Js(i,j), small_tol, close_tol )
    }
  }

  // source Jacobian
  AVMatrixQ dsdux = 0, dsduy = 0;
  MatrixQ dsduxTrue = 0, dsduyTrue = 0;
  avpde.jacobianGradientSource( param, x, y, time, avqL, avqxL, avqyL, dsdux, dsduy );
  pde.jacobianGradientSource( x, y, time, qL, qxL, qyL, dsduxTrue, dsduxTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dsduxTrue(i,j), dsdux(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dsduxTrue(i,j), dsduy(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source_nonZeroSensor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  Real x, y, time;
  Real rhoL, uL, vL,tL, sL;
  Real rhoR, uR, vR, tR, sR;

  x = y = time = 0;   // not actually used in functions
  rhoL = 1.034; uL = 3.26; vL = 1.23; tL = 5.78; sL = 0.5;
  rhoR = 0.973; uR = 1.79; vR = 0.34; tR = 6.13; sR = 0.2;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  // set

  ArrayQ qL = 0, qR = 0;
  pde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  pde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );
  AVArrayQ avqL = 0, avqR = 0;
  avpde.setDOFFrom( avqL, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL), sL ) );
  avpde.setDOFFrom( avqR, AVVariable<DensityVelocityTemperature2D, Real>( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR), sR ) );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21};
  AVArrayQ avqxL = {0.17, 0.21, -0.34, 0.19, 0.12};
  AVArrayQ avqyL = {-0.05, 0.61, 0.12, -0.21, 0.15};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15};
  AVArrayQ avqxR = {0.21, 0.31, -0.14, 0.2, 0.27};
  AVArrayQ avqyR = {0.11, 0.48, 0.27, -0.15, 0.63};

  Real lifted_quantity = 1.23;

  // vanilla source term
  AVArrayQ src = 0;
  ArrayQ srcTrue = 0;
  avpde.source( param, x, y, time, avqL, avqxL, avqyL, src );
  pde.source( x, y, time, qL, qxL, qyL, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )

  // source term with lifted quantity
  src = 0;
  srcTrue = 0;
  avpde.source( param, x, y, time, lifted_quantity, avqL, avqxL, avqyL, src );
  pde.source( x, y, time, lifted_quantity, qL, qxL, qyL, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )

  // trace source term with lifted quantity
  AVArrayQ srcL = 0, srcR = 0;
  ArrayQ srcLTrue = 0, srcRTrue = 0;
  avpde.sourceTrace( param, x, y, param, x, y, time, avqL, avqxL, avqyL, avqR, avqxR, avqyR, srcL, srcR );
  pde.sourceTrace( x, y, x, y, time, qL, qxL, qyL, qR, qxR, qyR, srcLTrue, srcRTrue );
  SANS_CHECK_CLOSE( srcLTrue(0), srcL(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(1), srcL(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(2), srcL(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcLTrue(3), srcL(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(0), srcR(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(1), srcR(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(2), srcR(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcRTrue(3), srcR(3), small_tol, close_tol )

  // source for lifted quantity
  src = 0;
  srcTrue = 0;
  avpde.sourceLiftedQuantity( param, x, y, param, x, y, time, avqL, avqR, src );
  pde.sourceLiftedQuantity( x, y, x, y, time, qL, qR, srcTrue );
  SANS_CHECK_CLOSE( srcTrue(0), src(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(1), src(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(2), src(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( srcTrue(3), src(3), small_tol, close_tol )


  // source Jacobian
  AVMatrixQ Js = 0;
  MatrixQ JsTrue = 0;
  avpde.jacobianSource( param, x, y, time, avqL, avqxL, avqyL, Js );
  pde.jacobianSource( x, y, time, qL, qxL, qyL, JsTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JsTrue(i,j), Js(i,j), small_tol, close_tol )
    }
  }

  // source Jacobian
  AVMatrixQ dsdux = 0, dsduy = 0;
  MatrixQ dsduxTrue = 0, dsduyTrue = 0;
  avpde.jacobianGradientSource( param, x, y, time, avqL, avqxL, avqyL, dsdux, dsduy );
  pde.jacobianGradientSource( x, y, time, qL, qxL, qyL, dsduxTrue, dsduxTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dsduxTrue(i,j), dsdux(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( dsduxTrue(i,j), dsduy(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic_zeroSensor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  const Real tol = 1e-13;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  Real x, y, time, dx, dy;
  Real rho, u, v, t, s;
  Real speed, speedTrue;

  x = y = time = 0;   // not actually used in functions
  dx = 0.57; dy = 1.23;
  rho = 1.137; u = 0.784; v = 2.31; t = 0.987; s = 0.0;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY",  "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  AVVariable<DensityVelocityTemperature2D, Real> qdata = {{rho, u, v, t}, s};
  AVArrayQ avq = avpde.setDOFFrom( qdata );

  avpde.speedCharacteristic( param, x, y, time, dx, dy, avq, speed );
  pde.speedCharacteristic( x, y, time, dx, dy, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( param, x, y, time, avq, speed );
  pde.speedCharacteristic( x, y, time, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic_nonZeroSensor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor2D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  const Real tol = 1e-13;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  Real us = 1.0, vs = 0.0;
  Real ksxx = 0.8, ksxy = 0.0, ksyx = 0.0, ksyy = 1.0;;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs );
  SensorViscousFlux visc(ksxx, ksxy, ksyx, ksyy);
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy,
                   gas, Euler_ResidInterp_Raw);

  Real x, y, time, dx, dy;
  Real rho, u, v, t, s;
  Real speed, speedTrue;

  x = y = time = 0;   // not actually used in functions
  dx = 0.57; dy = 1.23;
  rho = 1.137; u = 0.784; v = 2.31; t = 0.987; s = 0.23;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY",  "Temperature"};
  ArrayQ q;
  pde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  AVVariable<DensityVelocityTemperature2D, Real> qdata = {{rho, u, v, t}, s};
  AVArrayQ avq = avpde.setDOFFrom( qdata );

  avpde.speedCharacteristic( param, x, y, time, dx, dy, avq, speed );
  pde.speedCharacteristic( x, y, time, dx, dy, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( param, x, y, time, avq, speed );
  pde.speedCharacteristic( x, y, time, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
