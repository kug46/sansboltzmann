// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCEulermitAVSensor2D_Consistency_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/BCEulermitAVSensor2D.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Fluids2D_Sensor.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

//Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD2>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
template class TraitsModelEuler<QTypeEntropy, GasModel>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion2D_Consistency_btest )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCSymmetry_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> AVPDEClass;

  typedef BCEuler2D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCEuler2D<BCTypeSymmetry_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeSymmetry_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  BCClass bc(pde);
  AVBCClass avbc(avpde);

  ArrayQ qI;
  AVArrayQ avqI;
  DensityVelocityTemperature2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Temperature  = t;
  pde.setDOFFrom( qI, qdata );
  avpde.setDOFFrom( avqI, qdata );
  avqI(4) = s;

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCSymmetry_nonZeroSensorTest, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> AVPDEClass;

  typedef BCEuler2D<BCTypeSymmetry_mitState, PDEClass> BCClass;
  typedef BCEuler2D<BCTypeSymmetry_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeSymmetry_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, s = 0.5;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  BCClass bc(pde);
  AVBCClass avbc(avpde);

  ArrayQ qI;
  AVArrayQ avqI;
  DensityVelocityTemperature2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Temperature  = t;
  pde.setDOFFrom( qI, qdata );
  avpde.setDOFFrom( avqI, qdata );
  avqI(4) = s;

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCFullState_BzeroSensorTest, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> AVPDEClass;

  typedef BCEuler2D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCEuler2D<BCTypeFullState_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;
  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;
  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;

  typedef BCParameters< BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, p = 0.987, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  PyDict d;
  d[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
  d[DensityVelocityPressure2DParams::params.rho] = rho;
  d[DensityVelocityPressure2DParams::params.u] = u;
  d[DensityVelocityPressure2DParams::params.v] = v;
  d[DensityVelocityPressure2DParams::params.p] = p;

  //Pydict constructor
  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  BCClass bc(pde, BCFullState);
  AVBCClass avbc(avpde, BCFullState);

  ArrayQ qI;
  AVArrayQ avqI;
  DensityVelocityPressure2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Pressure  = p;
  pde.setDOFFrom( qI, qdata );
  avpde.setDOFFrom( avqI, qdata );
  avqI(4) = s;

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeInflowSubsonic_PtTta_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> AVPDEClass;

  typedef BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> BCClass;
  typedef BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;
  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;
  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;

  typedef BCParameters< BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, s = 0.0;
  Real p  = R*rho*t;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  Real TtSpec = t*(1+0.2*0.1*0.1);
  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  Real aSpec = 0.1;
  //Real sSpec = 0.265;

  //Pydict constructor
  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
  BCInflowSubsonic[BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;

  BCClass bc(pde, BCInflowSubsonic);
  AVBCClass avbc(avpde, BCInflowSubsonic);

  ArrayQ qI;
  AVArrayQ avqI;
  DensityVelocityTemperature2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Temperature  = t;
  pde.setDOFFrom( qI, qdata );
  avpde.setDOFFrom( avqI, qdata );
  avqI(4) = s;

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeOutflowSubsonic_Pressure_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> AVPDEClass;

  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;
  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;
  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;

  typedef BCParameters< BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCSubsonicOut[BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = 101325.0;

  BCClass bc(pde, BCSubsonicOut);
  AVBCClass avbc(avpde, BCSubsonicOut);

  ArrayQ qI;
  AVArrayQ avqI;
  DensityVelocityTemperature2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Temperature  = t;
  pde.setDOFFrom( qI, qdata );
  avpde.setDOFFrom( avqI, qdata );
  avqI(4) = s;

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeOutflowSupersonic_Pressure_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> AVPDEClass;

  typedef BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;
  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;
  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;

  typedef BCParameters< BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCSubsonicOut[BCEuler2DParams<BCTypeOutflowSupersonic_Pressure_mitState>::params.pSpec] = 101325.0;

  BCClass bc(pde, BCSubsonicOut);
  AVBCClass avbc(avpde, BCSubsonicOut);

  ArrayQ qI;
  AVArrayQ avqI;
  DensityVelocityTemperature2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Temperature  = t;
  pde.setDOFFrom( qI, qdata );
  avpde.setDOFFrom( avqI, qdata );
  avqI(4) = s;

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
}
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeReflect_mitState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> AVPDEClass;

  typedef BCEuler2D<BCTypeReflect_mitState, PDEClass> BCClass;
  typedef BCEuler2D<BCTypeReflect_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  BCClass bc(pde);
  AVBCClass avbc(avpde);

  ArrayQ qI;
  AVArrayQ avqI;
  DensityVelocityTemperature2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Temperature  = t;
  pde.setDOFFrom( qI, qdata );
  avpde.setDOFFrom( avqI, qdata );
  avqI(4) = s;

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
}
#endif
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeInflowSubsonic_sHqt_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> AVPDEClass;

  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDEClass> BCClass;
  typedef BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor2D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;
  typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef Fluids_Sensor<PhysD2, PDEBaseClass> Sensor;
  typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;
  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;

  typedef BCParameters< BCEulermitAVSensor2DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.352;   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, t = 0.987, s = 0.0;
  Real p  = R*rho*t;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  Real sSpec = log(p / pow(rho,gamma));
  Real HSpec = gas.enthalpy(rho,t) + 0.5*(u*u+v*v);
  Real VxSpec = u;
  Real VySpec = v;

  //Pydict constructor
  PyDict BCsHqt;
  BCsHqt[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCsHqt[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.sSpec] = sSpec;
  BCsHqt[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.HSpec] = HSpec;
  BCsHqt[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VxSpec] = VxSpec;
  BCsHqt[BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params.VySpec] = VySpec;

  BCClass bc(pde, BCsHqt);
  AVBCClass avbc(avpde, BCsHqt);

  ArrayQ qI;
  AVArrayQ avqI;
  DensityVelocityTemperature2D<Real> qdata;
  qdata.Density   = rho;
  qdata.VelocityX = u;
  qdata.VelocityY = v;
  qdata.Temperature  = t;
  pde.setDOFFrom( qI, qdata );
  avpde.setDOFFrom( avqI, qdata );
  avqI(4) = s;

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, time, nx, ny, avqI, qB );
  bc.state( x, y, time, nx, ny, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, time, nx, ny, avqI, avqx, avqy, qB, Fn );
  bc.fluxNormal( x, y, time, nx, ny, qI, qx, qy, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
