// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//#define IBL2D_DUMP
//#define SANS_VERBOSE

//----------------------------------------------------------------------------//
// testing coupled solution of block 4x4 system of IBL and Xfoil-like panel method on airfoil and wake
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"
#include "pyrite_fstream.h"

#include <boost/mpl/vector_c.hpp>

#define JACOBIANPARAM_INSTANTIATE
#include "Discretization/JacobianParam_impl.h"

#define ALGEBRAICEQUATIONSET_BLOCK4X4_INSTANTIATE // HACK : need proper instantiations
#include "Discretization/Block/AlgebraicEquationSet_Block4x4_impl.h"

#include "Discretization/Block/JacobianParam_Decoupled.h"

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_HubTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_WakeMatch_Galerkin_IBL.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_DG_HubTrace.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/output_Tecplot.h"

#define SCALARVECTOR_INSTANTIATE
#include "LinearAlgebra/SparseLinAlg/ScalarVector_impl.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/WritePlainVector.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"

#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_Block4x4_PTC_IBL.h"

#include "PanelMethod/AlgebraicEquationSet_ProjectToQauxi.h"
#include "PanelMethod/JacobianParam_PanelEqn_Qauxi.h"
#include "PanelMethod/ProjectionToQauxi_IBL.h"

#include "PanelMethod/XfoilPanel.h"
#include "PanelMethod/XfoilPanelEquationSet.h"

#include "pde/IBL/PDEIBLSplitAmpLag2D.h"
#include "pde/IBL/BCIBLSplitAmpLag2D.h"
#include "pde/IBL/SetSolnDofCell_IBL2D.h"
#include "pde/IBL/SetVelDofCell_IBL2D.h"
#include "pde/IBL/SetIBLoutputCellGroup.h"

#include "pde/IBL/AlgebraicEquationSet_ProjectToQauxv_PanelMethod.h"
#include "pde/IBL/JacobianParam_ProjectToQauxv_Qinv.h"
#include "pde/IBL/JacobianParam_ProjectToQauxi_QIBL.h"
#include "pde/IBL/JacobianParam_ProjectToQauxi_Qauxv.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"
#include "pde/BCParameters.h"

#include "tools/timer.h"

#include "unit/UnitGrids/XField2D_Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Line_X1_2Group_AirfoilWithWake.h"

#include "checkLinearSolver.h"

using namespace std;
using namespace SANS::DLA;
using namespace SANS::SLA;

namespace SANS
{
typedef PhysD2 PhysDim;
typedef TopoD1 TopoDim;

typedef AlgEqSetTraits_Sparse AESTraitsTag;

/////////////////////////////////////////////////////////////////////
// Panel method
typedef XfoilPanel PanelMethodType;
typedef XfoilPanelEquationSet<PanelMethodType,AESTraitsTag> PanelMethodEquationType;

typedef typename PanelMethodEquationType::GamFieldType<Real> GamFieldType;
typedef typename PanelMethodEquationType::LamFieldType<Real> LamFieldType;

/////////////////////////////////////////////////////////////////////
// IBL
typedef VarTypeDsThNGsplit VarType;
typedef VarData2DDsThNGsplit VarDataType;

typedef BCIBLSplitAmpLag2DVector<VarType> BCVectorClass;

typedef PDEIBLSplitAmpLag<PhysDim,VarType> PDEClassIBL;
typedef PDENDConvertSpace<PhysDim, PDEClassIBL> NDPDEClassIBL;

typedef typename PDEClassIBL::ThicknessesCoefficientsType::ClassParamsType ThicknessesCoefficientsParamsType;

typedef NDPDEClassIBL::template ArrayQ<Real> ArrayQIBL;
typedef PDEClassIBL::template ArrayParam<Real> ArrayQauxv;
typedef NDPDEClassIBL::VectorX VectorX;

typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQauxv>,
                                       XField<PhysDim, TopoDim> >::type TupleFieldIBLParamType;

typedef AlgebraicEquationSet_HubTrace_Galerkin<
          AlgebraicEquationSet_DGAdvective<NDPDEClassIBL,BCNDConvertSpace,BCVectorClass,
                                           AESTraitsTag,DGAdv_manifold,TupleFieldIBLParamType> > IBLEquationType;

typedef IBLEquationType::BCParams BCParamsIBL;
typedef BCIBLSplitAmpLag2D<BCTypeWakeMatchSplitAmpLag,VarType> BCWakeMatchClass;

// Pseudo time continuation wrapper equation set
typedef AlgebraicEquationSet_PTC<NDPDEClassIBL, AESTraitsTag, PTC_manifold, TupleFieldIBLParamType> IBLEquationType_PTC;

/////////////////////////////////////////////////////////////////////
// Auxiliary equations
typedef FunctionEvalQauxv<PanelMethodType,Real,Real> FcnEvalQauxvType;
typedef IntegrandCell_ProjectFunction<FcnEvalQauxvType,IntegrandCell_ProjFcn_detail::FcnSref> IntegrandCellAuxvType;
typedef AlgebraicEquationSet_ProjectToQauxv_PanelMethod<
          XField<PhysDim,TopoDim>, IntegrandCellAuxvType, TopoDim, AESTraitsTag, PanelMethodEquationType> AuxvEquationType;

// TODO: note that the ordering of QIBL and Qauxv need to be consistent with that defined in ProjectionToQauxi
typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQIBL>,
                                       Field<PhysDim, TopoDim, ArrayQauxv>,
                                       XField<PhysDim, TopoDim> >::type TupleFieldAuxiParamType;

typedef typename AESProjToQauxi_detail::IntegrandCellProjNull IntegrandAuxiNullCellType;
typedef AlgebraicEquationSet_ProjectToQauxi<TupleFieldAuxiParamType, IntegrandAuxiNullCellType, TopoDim, AESTraitsTag,
                                            IBLEquationType, AuxvEquationType> AuxiEquationType;

/////////////////////////////////////////////////////////////////////
// Block 4x4
// diagonal block equation sets
typedef AuxvEquationType::TraitsType TraitsTypeBlock0;
typedef AuxiEquationType::TraitsType TraitsTypeBlock1;
typedef IBLEquationType::TraitsType TraitsTypeBlock2;
typedef PanelMethodEquationType::TraitsType TraitsTypeBlock3;

// primary unknown size
static const int MQ0 = DLA::VectorSize<typename TraitsTypeBlock0::ArrayQ>::M;
static const int MQ1 = DLA::VectorSize<typename TraitsTypeBlock1::ArrayQ>::M;
static const int MQ2 = DLA::VectorSize<typename TraitsTypeBlock2::ArrayQ>::M;
static const int MQ3 = DLA::VectorSize<typename TraitsTypeBlock3::ArrayQ>::M;

typedef typename TraitsTypeBlock0::MatrixQ        MatrixQ00;
typedef typename MatrixS_or_T<MQ0,MQ1,Real>::type MatrixQ01;
typedef typename MatrixS_or_T<MQ0,MQ2,Real>::type MatrixQ02;
typedef typename MatrixS_or_T<MQ0,MQ3,Real>::type MatrixQ03;

typedef typename MatrixS_or_T<MQ1,MQ0,Real>::type MatrixQ10;
typedef typename TraitsTypeBlock1::MatrixQ        MatrixQ11;
typedef typename MatrixS_or_T<MQ1,MQ2,Real>::type MatrixQ12;
typedef typename MatrixS_or_T<MQ1,MQ3,Real>::type MatrixQ13;

typedef typename MatrixS_or_T<MQ2,MQ0,Real>::type MatrixQ20;
typedef typename MatrixS_or_T<MQ2,MQ1,Real>::type MatrixQ21;
typedef typename TraitsTypeBlock2::MatrixQ        MatrixQ22;
typedef typename MatrixS_or_T<MQ2,MQ3,Real>::type MatrixQ23;

typedef typename MatrixS_or_T<MQ3,MQ0,Real>::type MatrixQ30;
typedef typename MatrixS_or_T<MQ3,MQ1,Real>::type MatrixQ31;
typedef typename MatrixS_or_T<MQ3,MQ2,Real>::type MatrixQ32;
typedef typename TraitsTypeBlock3::MatrixQ        MatrixQ33;

// block system matrices
typedef TraitsTypeBlock0::SystemMatrix        BlockSM00;
typedef MatrixD<SparseMatrix_CRS<MatrixQ01> > BlockSM01;
typedef MatrixD<SparseMatrix_CRS<MatrixQ02> > BlockSM02;
typedef MatrixD<SparseMatrix_CRS<MatrixQ03> > BlockSM03;

typedef MatrixD<SparseMatrix_CRS<MatrixQ10> > BlockSM10;
typedef TraitsTypeBlock1::SystemMatrix        BlockSM11;
typedef MatrixD<SparseMatrix_CRS<MatrixQ12> > BlockSM12;
typedef MatrixD<SparseMatrix_CRS<MatrixQ13> > BlockSM13;

typedef MatrixD<SparseMatrix_CRS<MatrixQ20> > BlockSM20;
typedef MatrixD<SparseMatrix_CRS<MatrixQ21> > BlockSM21;
typedef TraitsTypeBlock2::SystemMatrix        BlockSM22;
typedef MatrixD<SparseMatrix_CRS<MatrixQ23> > BlockSM23;

typedef MatrixD<SparseMatrix_CRS<MatrixQ30> > BlockSM30;
typedef MatrixD<SparseMatrix_CRS<MatrixQ31> > BlockSM31;
typedef MatrixD<SparseMatrix_CRS<MatrixQ32> > BlockSM32;
typedef TraitsTypeBlock3::SystemMatrix        BlockSM33;

typedef AlgebraicEquationSet_Block4x4<BlockSM00, BlockSM01, BlockSM02, BlockSM03,
                                      BlockSM10, BlockSM11, BlockSM12, BlockSM13,
                                      BlockSM20, BlockSM21, BlockSM22, BlockSM23,
                                      BlockSM30, BlockSM31, BlockSM32, BlockSM33,
                                      AlgebraicEquationSetBase> Block4x4AlgebraicEquationSetType;

typedef Block4x4AlgebraicEquationSetType::SystemMatrix BlockSystemMatrixType;
typedef Block4x4AlgebraicEquationSetType::SystemNonZeroPattern BlockSystemMatrixNZType;
typedef Block4x4AlgebraicEquationSetType::SystemVector BlockSystemVectorType;

typedef AlgebraicEquationSet_Block4x4_PTC_IBL<
    BlockSM00, BlockSM01, BlockSM02, BlockSM03,
    BlockSM10, BlockSM11, BlockSM12, BlockSM13,
    BlockSM20, BlockSM21, BlockSM22, BlockSM23,
    BlockSM30, BlockSM31, BlockSM32, BlockSM33> Block4x4AlgebraicEquationSetType_PTC;

/////////////////////////////////////////////////////////////////////
// Coupling jacobians
typedef JacobianParam_Decoupled<BlockSM01,TraitsTypeBlock0> DecoupledJacobianType01;
typedef JacobianParam_Decoupled<BlockSM02,TraitsTypeBlock0> DecoupledJacobianType02;
typedef JacobianParam_ProjectToQauxv_Qinv<AuxvEquationType> CouplingJacobianType03;

typedef JacobianParam_ProjectToQauxi_Qauxv<AuxiEquationType> CouplingJacobianType10;
typedef JacobianParam_ProjectToQauxi_QIBL<AuxiEquationType> CouplingJacobianType12;
typedef JacobianParam_Decoupled<BlockSM13,TraitsTypeBlock1> DecoupledJacobianType13;

typedef JacobianParam<boost::mpl::vector1_c<int,0>,IBLEquationType> CouplingJacobianType20_IBLeqn_Qauxv;
typedef JacobianParam_Decoupled<BlockSM21,TraitsTypeBlock2> DecoupledJacobianType21;
typedef JacobianParam_Decoupled<BlockSM23,TraitsTypeBlock2> DecoupledJacobianType23;

typedef JacobianParam_Decoupled<BlockSM30,TraitsTypeBlock3> DecoupledJacobianType30;
typedef JacobianParam_PanelEqn_Qauxi<PanelMethodEquationType> CouplingJacobianType31;
typedef JacobianParam_Decoupled<BlockSM32,TraitsTypeBlock3> DecoupledJacobianType32;

} // namespace SANS

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( Solve2D_Coupled4x4_IBLSplitAmpLag_CorrelationClosure_CapturedTransition_toy_test_suite )

//----------------------------------------------------------------------------//
void runCase(const int nelemAirfoil)
{
  timer timer_total;

  // ---------- Set problem parameters ---------- //
  const string airfoilname = "e387";
  const int alpha = 4; // [degree] angle of attack
  const Real Reinf = 1.e5;

  const int nelem_a = nelemAirfoil; // number of panels on airfoil

  // ---------- Set up IBL ---------- //
//  const int order_soln = 0; // solution order
  const int order_soln = 1; // solution order

  const int order_param_grid = 1;  // order: parameter field and grid

  const Real p0 = 1.e5; // [Pa] stagnation pressure
  const Real T0 = 300.0; // [K] stagnation temperature

  // ---------- Set up problem ---------- //
  // models
  PyDict gasModelDict;
  gasModelDict[GasModelParamsIBL::params.gamma] = 1.4;
  gasModelDict[GasModelParamsIBL::params.R] = 287.15;
  GasModelParamsIBL::checkInputs(gasModelDict);
  const GasModelIBL gasModel(gasModelDict);

  const Real mue = 1.827e-5;
  typename PDEClassIBL::ViscosityModelType viscosityModel(mue);

  const Real qinf = detail_IBLCaseSetup::
      getqinf(Reinf, p0, T0, gasModel.gamma(), gasModel.R(), viscosityModel.dynamicViscosity()); // [m/s] freestream speed

  PyDict transitionModelDict_wall;

  std::vector<int> ntcrit_thousand_array;
  ntcrit_thousand_array.push_back(90000);
  transitionModelDict_wall[TransitionModelParams::params.ntcrit] = 1e-4 * static_cast<Real>(ntcrit_thousand_array.at(0));
  transitionModelDict_wall[TransitionModelParams::params.xtr] = 10.0;
  const std::string caseTagName = "xtr10p0_ntcrit9p0";

  transitionModelDict_wall[TransitionModelParams::params.intermittencyLength] = 0.07 * Real(64)/Real(nelem_a);
  transitionModelDict_wall[TransitionModelParams::params.isTransitionActive] = true;
//  transitionModelDict_wall[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict_wall[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.laminarBL;
  TransitionModelParams::checkInputs(transitionModelDict_wall); // necessary for checking input parameter validity
  const typename PDEClassIBL::TransitionModelType transitionModel_wall(transitionModelDict_wall);

  PyDict transitionModelDict_wake(transitionModelDict_wall);
  transitionModelDict_wake[TransitionModelParams::params.isTransitionActive] = false;
  transitionModelDict_wake[TransitionModelParams::params.profileCatDefault]
                           = TransitionModelParams::params.profileCatDefault.turbulentWake;
  TransitionModelParams::checkInputs(transitionModelDict_wake); // necessary for checking input parameter validity
  const typename PDEClassIBL::TransitionModelType transitionModel_wake(transitionModelDict_wake);

  PyDict secVarDict;
  secVarDict[ThicknessesCoefficientsParamsType::params.nameCDclosure]
             = ThicknessesCoefficientsParamsType::params.nameCDclosure.xfoillag;

  // PDE
  NDPDEClassIBL pdeIBLWall(gasModel, viscosityModel, transitionModel_wall, secVarDict);
  NDPDEClassIBL pdeIBLWake(gasModel, viscosityModel, transitionModel_wake, secVarDict);

  // BC
  // Create a BC dictionaries
  PyDict BCNoneTEArgs;
  BCNoneTEArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.MatchingUniField;
  BCNoneTEArgs[BCWakeMatchClass::ParamsType::params.matchingType] =
      BCWakeMatchClass::ParamsType::params.matchingType.trailingEdge;

  PyDict BCWakeInArgs;
  BCWakeInArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.MatchingUniField;
  BCWakeInArgs[BCWakeMatchClass::ParamsType::params.matchingType] =
      BCWakeMatchClass::ParamsType::params.matchingType.wakeInflow;

  PyDict BCWakeOutArgs;
  BCWakeOutArgs[BCParamsIBL::params.BC.BCType] = BCParamsIBL::params.BC.None;

  PyDict PyBCList_a;
  PyBCList_a["_BCNoneTE_"] = BCNoneTEArgs;

  PyDict PyBCList_w;
  PyBCList_w["_BCWakeIn_"] = BCWakeInArgs;
  PyBCList_w["BCOut"] = BCWakeOutArgs;

  // No exceptions should be thrown
  BCParamsIBL::checkInputs(PyBCList_a);
  BCParamsIBL::checkInputs(PyBCList_w);

  std::map<std::string, std::vector<int> > BCBoundaryGroups_a;
  std::map<std::string, std::vector<int> > BCBoundaryGroups_w;

  // Define the BoundaryGroups for each boundary condition
  BCBoundaryGroups_a["_BCNoneTE_"] = {0};
  BCBoundaryGroups_w["_BCWakeIn_"] = {1};
  BCBoundaryGroups_w["BCOut"] = {2};

  // ---------- Set up grid ---------- //
  string filename_airfoil = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem_a) + "_airfoil.txt";
  string filename_wake = "IO/CodeConsistency/IBL/" + airfoilname + "_ne" + stringify(nelem_a)
                               + "_wake.txt";

  std::vector<VectorX> coordinates_a, coordinates_w;
  XField2D_Line_X1_1Group::readXFoilGrid(filename_airfoil, coordinates_a);
  XField2D_Line_X1_1Group::readXFoilGrid(filename_wake, coordinates_w);
#if defined(SANS_VERBOSE)
  cout << "Airfoil grid: " << filename_airfoil << endl;
  cout << "Wake grid:    " << filename_wake << endl;
#endif

  XField2D_Line_X1_2Group_AirfoilWithWake xfld(coordinates_a,coordinates_w);

  std::cout << "Completed setting up grid.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

  // ---------- Set up Panel ---------- //

  const VectorX Vinf = { qinf*cos(static_cast<Real>(alpha)*PI/180.0),
                         qinf*sin(static_cast<Real>(alpha)*PI/180.0) }; // freestream velocity

  const std::vector<int> panelCellGroups = {0,1};
  PanelMethodType panel(Vinf,p0,T0,xfld,panelCellGroups);

  BOOST_CHECK_EQUAL(nelem_a, panel.nPanelAirfoil());

  // ---------- Initialize solution/parameter fields ---------- //

  // Auxiliary viscous solution field
  Field_DG_Cell<PhysDim, TopoDim, ArrayQauxv> qauxvfld(xfld, order_param_grid, BasisFunctionCategory_Hierarchical);
  BOOST_CHECK_EQUAL( xfld.nElem(), qauxvfld.nElem() );

  qauxvfld = 0.0;

  // IBL solution field
  string Qfilename_a = "IO/CodeConsistency/IBL/" + airfoilname + "_qinit_ne" + stringify(nelem_a) + "_airfoil_splitAmpLagXfoil.txt";
  string Qfilename_w = "IO/CodeConsistency/IBL/" + airfoilname + "_qinit_ne" + stringify(nelem_a) + "_wake_splitAmpLagXfoil.txt";

#if defined(SANS_VERBOSE)
  cout << "Initial QIBL airfoil: " << Qfilename_a << endl;
  cout << "Initial QIBL wake:    " << Qfilename_w << endl;
#endif

  BasisFunctionCategory basisCat_qIBL = BasisFunctionCategory_None;
  if (order_soln == 0)
    basisCat_qIBL = BasisFunctionCategory_Legendre;
  else if (order_soln == 1)
    basisCat_qIBL = BasisFunctionCategory_Hierarchical;

  Field_DG_Cell<PhysDim, TopoDim, ArrayQIBL> qIBLfld( xfld, order_soln, basisCat_qIBL );

  const auto& varInterpret = pdeIBLWall.getVarInterpreter();
  const std::vector<ArrayQIBL> qIBLinit_vec_a
    = QIBLDataReader<VarDataType>::getqIBLvec(varInterpret, Qfilename_a);
  const std::vector<ArrayQIBL> qIBLinit_vec_w
    = QIBLDataReader<VarDataType>::getqIBLvec(varInterpret, Qfilename_w);

  for_each_CellGroup<TopoDim>::apply( SetSolnDofCell_IBL2D<VarType>(qIBLinit_vec_a, {0}, order_soln), (xfld, qIBLfld) );
  for_each_CellGroup<TopoDim>::apply( SetSolnDofCell_IBL2D<VarType>(qIBLinit_vec_w, {1}, order_soln), (xfld, qIBLfld) );

  BOOST_REQUIRE_EQUAL( xfld.nElem(), qIBLfld.nElem() );

  // Lagrange multiplier field
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQIBL>
    lgfld_a( xfld, order_soln, BasisFunctionCategory_Legendre, BCParamsIBL::getLGBoundaryGroups(PyBCList_a, BCBoundaryGroups_a) );
  lgfld_a = 0.0;

  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQIBL>
    lgfld_w( xfld, order_soln, BasisFunctionCategory_Legendre, BCParamsIBL::getLGBoundaryGroups(PyBCList_w, BCBoundaryGroups_w) );
  lgfld_w = 0.0;

  // Hub trace field
  Field_DG_HubTrace<PhysDim,TopoDim,ArrayQIBL> hbfld(xfld, 0, BasisFunctionCategory_Legendre);

  hbfld = {-0.03236750047792614, -0.03600241753532338, 0, 3.802340901981209};

#if defined(SANS_VERBOSE)
  cout << "Initial QIBL hubtrace: " << hbfld.DOF(0) << endl;
#endif

  std::cout << "Completed initializing IBL solution.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

  // Panel solution field
  GamFieldType gamfld(xfld, PanelMethodEquationType::order_gam, BasisFunctionCategory_Hierarchical,{0});
  gamfld = 0.0;
  Real Psi0 = 0.0;
  LamFieldType lamfld(xfld, PanelMethodEquationType::order_lam, BasisFunctionCategory_Legendre);
  lamfld = 0.0;

  // Auxiliary inviscid solution field
  LamFieldType qauxifld(xfld, PanelMethodEquationType::order_lam, BasisFunctionCategory_Legendre);
  qauxifld = 0.0;

  // Tuple fields
  TupleFieldIBLParamType tupleIBLfld = (qauxvfld, xfld);

  TupleFieldAuxiParamType tupleAuxifld = (qIBLfld, qauxvfld, xfld);

  // ------------------------------------ //
  // SET UP EQUATION SETS
  // ------------------------------------ //
  std::vector<int> cellGroup_a = {0};
  std::vector<int> cellGroup_w = {1};

  std::vector<int> interiorTraceGroups_a = {0};
  std::vector<int> interiorTraceGroups_w = {1};

  std::vector<int> boundaryTraceGroups_a = {0};
  std::vector<int> boundaryTraceGroups_w = {1,2};

  QuadratureOrder quadratureOrder_GalerkinWeightedIntegral(xfld, 4);
//  QuadratureOrder quadratureOrder_GalerkinWeightedIntegral(xfld, 13);
//  QuadratureOrder quadratureOrder_GalerkinWeightedIntegral(xfld, 39);

//  const Real tol_eqn = 2.e-12;
  const Real tol_eqn = 1.e-10;

  // IBL
  std::vector<Real> tol_IBL(IBLEquationType::nEqnSet, tol_eqn);
  IBLEquationType iblEqSet(hbfld, tupleIBLfld, qIBLfld, lgfld_a, pdeIBLWall,
                           quadratureOrder_GalerkinWeightedIntegral, ResidualNorm_L2, tol_IBL,
                           cellGroup_a,interiorTraceGroups_a, PyBCList_a, BCBoundaryGroups_a);
  auto iblEqSet_w_ptr
    = std::make_shared<IBLEquationType>(hbfld, tupleIBLfld, qIBLfld, lgfld_w, pdeIBLWake,
                                        quadratureOrder_GalerkinWeightedIntegral, ResidualNorm_L2, tol_IBL,
                                        cellGroup_w, interiorTraceGroups_w, PyBCList_w, BCBoundaryGroups_w);
  iblEqSet.addAlgebraicEquationSet(iblEqSet_w_ptr);

  // Pseudo time continuation equation set object
  IBLEquationType_PTC iblEqSetPTC(tupleIBLfld, qIBLfld, pdeIBLWall,
                                  quadratureOrder_GalerkinWeightedIntegral, cellGroup_a, iblEqSet);
  auto iblEqSetPTC_w_ptr
    = std::make_shared<IBLEquationType_PTC>(tupleIBLfld, qIBLfld, pdeIBLWake,
                                            quadratureOrder_GalerkinWeightedIntegral, cellGroup_w, *iblEqSet_w_ptr);
  iblEqSetPTC.addAlgebraicEquationSetPTC(iblEqSetPTC_w_ptr);

  std::cout << "Completed setting up IBL AES.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

  // Panel
  PanelMethodEquationType panelEqSet(panel,gamfld,Psi0,lamfld,qauxifld,tol_eqn);

  std::cout << "Completed setting up panel AES.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

  // Auxiliary viscous equation
  ElementXField<PhysDim,TopoDim,Line> xfldElemDummy(FcnEvalQauxvType::order_qauxv, BasisFunctionCategory_Hierarchical);
  xfldElemDummy.DOF(0) = {0.0, 0.0};
  xfldElemDummy.DOF(1) = {1.0, 0.0};
  FcnEvalQauxvType fcnEvalQauxvDummy(panel,xfldElemDummy,gamfld,lamfld,0,0); //TODO: dummy here
  IntegrandCellAuxvType integrandCellAuxv(fcnEvalQauxvDummy, panelCellGroups);
  AuxvEquationType auxvEqSet(xfld, qauxvfld, integrandCellAuxv, quadratureOrder_GalerkinWeightedIntegral, {{tol_eqn}}, panelEqSet, panelCellGroups);

  std::cout << "Completed setting up auxv AES.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

  // Auxiliary inviscid equation
  IntegrandAuxiNullCellType integrandCellAuxiNull(panelCellGroups);

  typedef typename AuxiEquationType::IntegrandInteriorTraceType integrandAuxiInteriorTraceType;
  typedef typename AuxiEquationType::IntegrandBoundaryTraceType integrandAuxiBoundaryTraceType;

  PDENDConvertSpace<PhysDim, ProjectionToQauxi<PanelMethodEquationType, NDPDEClassIBL> > projectorToQauxi_a(pdeIBLWall);
  PDENDConvertSpace<PhysDim, ProjectionToQauxi<PanelMethodEquationType, NDPDEClassIBL> > projectorToQauxi_w(pdeIBLWake);

  std::vector<std::shared_ptr<integrandAuxiInteriorTraceType> > integrandAuxiInteriorTraceArray;
  integrandAuxiInteriorTraceArray.push_back( std::make_shared<integrandAuxiInteriorTraceType>(projectorToQauxi_a, interiorTraceGroups_a) );
  integrandAuxiInteriorTraceArray.push_back( std::make_shared<integrandAuxiInteriorTraceType>(projectorToQauxi_w, interiorTraceGroups_w) );

  std::vector<std::shared_ptr<integrandAuxiBoundaryTraceType> > integrandAuxiBoundaryTraceArray;
  integrandAuxiBoundaryTraceArray.push_back( std::make_shared<integrandAuxiBoundaryTraceType>(projectorToQauxi_a, boundaryTraceGroups_a) );
  integrandAuxiBoundaryTraceArray.push_back( std::make_shared<integrandAuxiBoundaryTraceType>(projectorToQauxi_w, boundaryTraceGroups_w) );

  AuxiEquationType auxiEqSet(tupleAuxifld, qauxifld, integrandCellAuxiNull, quadratureOrder_GalerkinWeightedIntegral, {{tol_eqn}},
                             integrandAuxiInteriorTraceArray, integrandAuxiBoundaryTraceArray);

  std::cout << "Completed setting up auxi AES.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

  // Coupling jacobians
  DecoupledJacobianType01 JP01(auxvEqSet);
  DecoupledJacobianType02 JP02(auxvEqSet);
  CouplingJacobianType03 JP03(auxvEqSet);

  CouplingJacobianType10 JP10(auxiEqSet);
  CouplingJacobianType12 JP12(auxiEqSet);
  DecoupledJacobianType13 JP13(auxiEqSet);

  std::map<int,int> columnMap;
  columnMap[0] = 0;
  CouplingJacobianType20_IBLeqn_Qauxv JP20(iblEqSet, columnMap);
  auto JP20_w_ptr = std::make_shared<CouplingJacobianType20_IBLeqn_Qauxv>(*iblEqSet_w_ptr, columnMap);
  JP20.addJacobianParamSet(JP20_w_ptr);

  DecoupledJacobianType21 JP21(iblEqSet);
  DecoupledJacobianType23 JP23(iblEqSet);

  DecoupledJacobianType30 JP30(panelEqSet);
  CouplingJacobianType31  JP31(panelEqSet);
  DecoupledJacobianType32 JP32(panelEqSet);

  // block system
  Block4x4AlgebraicEquationSetType blockAES(auxvEqSet, JP01,      JP02,     JP03,
                                            JP10,      auxiEqSet, JP12,     JP13,
                                            JP20,      JP21,      iblEqSet, JP23,
                                            JP30,      JP31,      JP32,     panelEqSet);

  Block4x4AlgebraicEquationSetType_PTC blockAES_PTC(
      auxvEqSet, JP01,      JP02,     JP03,
      JP10,      auxiEqSet, JP12,     JP13,
      JP20,      JP21,      iblEqSetPTC, JP23,
      JP30,      JP31,      JP32,     panelEqSet);

  std::cout << "Completed setting up global AES.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

  // ------------------------------------ //
  // SOLVE
  // ------------------------------------ //

  // ---------- Set up Newton solver ---------- //
  PyDict NewtonSolverDict, LinearSolverDict, NewtonLineUpdateDict;

#if defined(INTEL_MKL) && 0 // MKL was only once used for debugging
  std::cout << "Using MKL\n";
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.MKL_PARDISO;
#else
  LinearSolverDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;
  LinearSolverDict[SLA::UMFPACKParam::params.Timing] = true;
#endif

  NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 5.0;
  // Allowing the residual to grow moderately in the beginning is observed to accelerate convergence
#ifdef SANS_VERBOSE
  NewtonLineUpdateDict[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

  NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = LinearSolverDict;
  NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
  NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
  NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
#ifdef SANS_VERBOSE
  NewtonSolverDict[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict[NewtonSolverParam::params.Timing] = true;
#endif

#if defined(IBL2D_DUMP) && 0 // even more verbose for debugging
  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = true;
#endif

  NewtonSolverParam::checkInputs(NewtonSolverDict);

  std::cout << "Starting solution.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

  // --------- Carry out solution ---------- //
#if defined(SANS_VERBOSE)
  std::cout << "\nInitialize the inviscid solution by solving the panel method alone:" << endl;
#endif
  detail_checkLinearSolver::solve_and_set_solution_field(panelEqSet, NewtonSolverDict);  // solve panel method
  std::cout << "Completed pre-solving panel.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

#if defined(SANS_VERBOSE)
  std::cout << "\nGiven the panel solution, initialize the auxiliary viscous solution by solving the auxiliary viscous equation alone:" << endl;
#endif
  detail_checkLinearSolver::solve_and_set_solution_field(auxvEqSet, NewtonSolverDict);  // solve for qauxvfld
  std::cout << "Completed pre-solving auxv eqns.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

#if 1 // don't solve uncoupled IBL for initialization

#if defined(SANS_VERBOSE)
  std::cout << "\nViscous solution is set to the initial input data. \n" << endl;
#endif

#else // solve uncoupled IBL for initialization

#if defined(SANS_VERBOSE)
  std::cout << "\nViscous solution is initialized by solving uncoupled IBL, given initialized Qauxv. \n" << endl;
#endif
  {
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 5;
    detail_checkLinearSolver::solve_and_set_solution_field(iblEqSet, NewtonSolverDict); // solve for Q_IBL
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
  }
  std::cout << "Completed pre-solving ibl eqns.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

#endif

#if 0

#if defined(SANS_VERBOSE)
  cout << "\nAuxiliary inviscid solution to initialized to zero. \n" << endl;
#endif

#else

#if defined(SANS_VERBOSE)
  std::cout << "\nAuxiliary inviscid solution to initialized based on initial QIBL and Qauxv:" << endl;
#endif
  detail_checkLinearSolver::solve_and_set_solution_field(auxiEqSet, NewtonSolverDict); // solve for qauxifld
  std::cout << "Completed pre-solving auxi eqns.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

#if defined(SANS_VERBOSE)
  std::cout << "\nRe-initialize the inviscid solution by solving the panel method alone:" << endl;
#endif
  detail_checkLinearSolver::solve_and_set_solution_field(panelEqSet, NewtonSolverDict);  // solve panel method
  std::cout << "Completed pre-solving panel.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

#if defined(SANS_VERBOSE)
  std::cout << "\nGiven the panel solution, initialize the auxiliary viscous solution by solving the auxiliary viscous equation alone:" << endl;
#endif
  detail_checkLinearSolver::solve_and_set_solution_field(auxvEqSet, NewtonSolverDict);  // solve for qauxvfld
  std::cout << "Completed pre-solving auxv eqns.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

#endif

#if defined(IBL2D_DUMP)
  {
    const int order_output = 1;

    typedef typename SetIBLoutputCellGroup_impl<VarType>::template ArrayOutput<Real> ArrayOutput;
    Field_DG_Cell<PhysDim, TopoDim, ArrayOutput> outputfld(xfld, order_output, BasisFunctionCategory_Hierarchical);
    for_each_CellGroup<TopoD1>::apply( SetIBLoutputCellGroup<VarType>(pdeIBLWall, qinf, qIBLfld, qauxvfld, {0}, -1), (xfld, outputfld) );
    for_each_CellGroup<TopoD1>::apply( SetIBLoutputCellGroup<VarType>(pdeIBLWake, qinf, qIBLfld, qauxvfld, {1}, -1), (xfld, outputfld) );

    const std::string caseTag = "coupledIBLSplitAmpLag2D_CapturedTransition_"
        + airfoilname + "_ne" + std::to_string(nelem_a) + "_p" + std::to_string(order_soln);
    const std::string outputfilename = "tmp/" + caseTag + "_init_output.plt";

    auto output_names = SetIBLoutputCellGroup_impl<VarType>::ElementProjectionType::outputNames();
    output_Tecplot(outputfld, outputfilename, output_names);

    // dump hub trace results
    std::cout << "Hub trace variable initial: " << setprecision(16) << hbfld.DOF(0) << std::endl;

    const std::string slnfilename_gam = "tmp/" + caseTag + "_init_gamma.plt";
    output_Tecplot(gamfld, slnfilename_gam, {"gamma"});

    const std::string slnfilename_lam = "tmp/" + caseTag + "_init_lambda.plt";
    output_Tecplot(lamfld, slnfilename_lam, {"lambda"});
    std::cout << "Psi_0 initial = " << setprecision(16) << Psi0 << std::endl;

    output_Tecplot(qauxifld, "tmp/" + caseTag + "_init_qauxifld.plt", {"qauxi"});
    output_Tecplot(qauxvfld, "tmp/" + caseTag + "_init_qauxvfld.plt", {"qx", "qz", "qx_x", "qx_z", "qz_x", "qz_z", "p0", "T0"});
  }
#endif

#if 0 //TODO: turn off for speed
  // --------- Linear solver pre-check ---------- //
  std::cout << "Starting test-solving linear system.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;
  detail_checkLinearSolver::check_linear_solver_block4x4(blockAES);
  std::cout << "Completed pre-checking linear solver.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;
#endif

  // --------- Nonlinear solution ---------- //
  std::cout << "Start solving global nonlinear system.  Elapsed time : " << timer_total.elapsed() << " s" << std::endl;

  BlockSystemVectorType sln(blockAES.vectorStateSize());
  blockAES.fillSystemVector(sln);

#if defined(IBL2D_DUMP) && 0
  BlockSystemMatrixNZType nz(blockAES.matrixSize());
  blockAES.jacobian(sln,nz);
  BlockSystemMatrixType jac(nz);
  blockAES.jacobian(sln, jac);
  cout << "writing initial jacobian tmp/jac_initial.mtx" << endl;
  WriteMatrixMarketFile( jac, "tmp/jac_initial.mtx" );
#endif

  // Newton solver setup
  PyDict NewtonSolverDict_global;
  NewtonSolverDict_global[NewtonSolverParam::params.LinearSolver] = LinearSolverDict;
  NewtonSolverDict_global[NewtonSolverParam::params.MaxIterations] = 30;
//  NewtonSolverDict_global[NewtonSolverParam::params.MaxIterations] = 50;
//  NewtonSolverDict_global[NewtonSolverParam::params.MaxChangeFraction] = 100.0; // turn on relaxation
#ifdef SANS_VERBOSE
  NewtonSolverDict_global[NewtonSolverParam::params.Verbose] = true;
  NewtonSolverDict_global[NewtonSolverParam::params.Timing] = true;
//  NewtonSolverDict[NewtonSolverParam::params.DumpJacobian] = true;
#endif

  PyDict NewtonLineUpdateDict_global;
#if 1 // line-search Newton
  NewtonLineUpdateDict_global[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;
  NewtonLineUpdateDict_global[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 5.0;
//  NewtonLineUpdateDict_global[HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor] = 2.0;
  // Allowing the residual to grow moderately in the beginning is observed to accelerate convergence
#ifdef SANS_VERBOSE
//  NewtonLineUpdateDict_global[HalvingSearchLineUpdateParam::params.verbose] = true;
#endif

#else // plain Newton
  NewtonLineUpdateDict_global[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.Newton;
#endif
  NewtonSolverDict_global[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict_global;

  NewtonSolverParam::checkInputs(NewtonSolverDict_global);

  NewtonSolver<BlockSystemMatrixType> blockAESsolver(blockAES, NewtonSolverDict_global);

  // Set up pseudo time continuation
  PyDict PTCDict;
  PTCDict[PseudoTimeParam::params.NonLinearSolver] = NewtonSolverDict_global;
  PTCDict[PseudoTimeParam::params.invCFL] = 1.0;
  PTCDict[PseudoTimeParam::params.invCFL_min] = 0.0;
  PTCDict[PseudoTimeParam::params.invCFL_max] = 1000.0;
  PTCDict[PseudoTimeParam::params.CFLDecreaseFactor] = 0.9;
  PTCDict[PseudoTimeParam::params.CFLIncreaseFactor] = 1.1;
  PTCDict[PseudoTimeParam::params.MaxIterations] = 100;
//  PTCDict[PseudoTimeParam::params.MaxIterations] = 20;
#ifdef SANS_VERBOSE
  PTCDict[PseudoTimeParam::params.Verbose] = true;
#else
  PTCDict[PseudoTimeParam::params.Verbose] = false;
#endif
  PseudoTimeParam::checkInputs(PTCDict);

  PseudoTime<BlockSystemMatrixType> blockAESsolverPTC(PTCDict, blockAES_PTC);

  // ------------------
  for (int i_nt = 0; i_nt < (int) ntcrit_thousand_array.size(); ++i_nt)
  {
    const int ntcrit_thousand = ntcrit_thousand_array.at(i_nt);
    transitionModel_wall.set_ntcrit(static_cast<Real>(ntcrit_thousand)*1e-4);
#ifdef SANS_VERBOSE
    std::cout << std::endl;
    std::cout << "Run the case with ntcrit = " << transitionModel_wall.ntcrit() << std::endl;
    std::cout << "                     xtr = " << transitionModel_wall.xtr() << std::endl;
    std::cout << "    intermittency length = " << transitionModel_wall.getIntermittencyLength() << std::endl << std::endl;
#endif

#if 1 // Newton solver
    SolveStatus status = blockAESsolver.solve(sln, sln);
    const bool isConverged = status.converged;
    blockAES.setSolutionField(sln);
#else // PTC solver
    const bool isConverged = blockAESsolverPTC.solve();
#endif

    std::cout << "Completed coupled IBL solution: " << timer_total.elapsed() << " s" << std::endl;

    BOOST_CHECK( isConverged );
    if (!isConverged)
    {
      BlockSystemVectorType rsd_spatial(blockAES.vectorStateSize());
      blockAES.fillSystemVector(sln);
      rsd_spatial = 0.0;
      blockAES.residual(sln, rsd_spatial);

      //dump vectors: require overloaded insertion operator "<<" for printing out vectors
      const std::string res_filename = "tmp/rsd_spatial_nonconverged_final.dat";
      std::fstream fout_res( res_filename, std::fstream::out );
      fout_res << rsd_spatial << std::endl;
      std::cout << "...Wrote final residual to file: " << res_filename << std::endl;

      const std::string sln_filename = "tmp/sln_spatial_nonconverged_final.dat";
      std::fstream fout_sol( sln_filename, std::fstream::out );
      fout_sol << sln << std::endl;
      std::cout << "...Wrote final solution to file: " << sln_filename << std::endl;

    }

    // ------------------------------------ //
    // Pyrite check
    // ------------------------------------ //
    const std::string blProfileType = transitionModelDict_wall.get(TransitionModelParams::params.profileCatDefault)
                                                + caseTagName;
    const std::string wakeProfileType = transitionModelDict_wake.get(TransitionModelParams::params.profileCatDefault)
                                                + secVarDict.get(ThicknessesCoefficientsParamsType::params.nameCDclosure);

    if (true)
    { // Pyrite check
      const Real pyrite_tol = 2.0e-7;

#if ISIBLLFFLUX_PDEIBLSplitAmpLag
      std::string pyriteFilename = "IO/CodeConsistency/Solve2D_Coupled_4x4_IBLSplitAmpLag_CorrelationClosure_"
        + airfoilname + "_Re1e5_a4_LFflux_ne" + std::to_string(nelem_a) + "_p" + std::to_string(order_soln) + "_Test.txt";
#else
      std::string pyriteFilename = "IO/CodeConsistency/Solve2D_Coupled_4x4_IBLSplitAmpLag_CorrelationClosure_"
        + airfoilname + "_Re1e5_a4_FullUpwind_ne" + std::to_string(nelem_a) + "_p" + std::to_string(order_soln) + "_Test.txt";
#endif

      const pyrite_file_stream::testmode pyrite_testmode = pyrite_file_stream::check;
#ifdef SANS_VERBOSE
      const bool isPyriteVerbose = true;
#else
      const bool isPyriteVerbose = false;
#endif
      pyrite_file_stream pyriteFile(pyriteFilename, pyrite_tol, pyrite_tol, pyrite_testmode, isPyriteVerbose);

      pyriteFile << std::setprecision(16) << std::scientific;

      for (int i = 0; i < qIBLfld.nDOF(); ++i)
      {
        for (int n = 0; n < PDEClassIBL::N; ++n)
          pyriteFile << qIBLfld.DOF(i)[n];
        pyriteFile << std::endl;
      }

      for (int n = 0; n < PDEClassIBL::N; ++n)
        pyriteFile << hbfld.DOF(0)[n];
      pyriteFile << std::endl;

      for (int i = 0; i < gamfld.nDOF(); ++i)
      {
        pyriteFile << gamfld.DOF(i) << std::endl;
      }

      pyriteFile << Psi0 << std::endl;

      for (int i = 0; i < lamfld.nDOF(); ++i)
      {
        pyriteFile << lamfld.DOF(i) << std::endl;
      }

      for (int i = 0; i < qauxvfld.nDOF(); ++i)
      {
        for (int n = 0; n < ArrayQauxv::N; ++n)
          pyriteFile << qauxvfld.DOF(i)[n];
        pyriteFile << std::endl;
      }

      for (int i = 0; i < qauxifld.nDOF(); ++i)
      {
        pyriteFile << qauxifld.DOF(i) << std::endl;
      }
    }

    // ------------------------------------ //
    // Dump results
    // ------------------------------------ //
#if defined(IBL2D_DUMP)
  {
    const int order_output = 1;

    typedef typename SetIBLoutputCellGroup_impl<VarType>::template ArrayOutput<Real> ArrayOutput;
    Field_DG_Cell<PhysDim, TopoDim, ArrayOutput> outputfld(xfld, order_output, BasisFunctionCategory_Hierarchical);
    for_each_CellGroup<TopoD1>::apply( SetIBLoutputCellGroup<VarType>(pdeIBLWall, qinf, qIBLfld, qauxvfld, {0}, -1), (xfld, outputfld) );
    for_each_CellGroup<TopoD1>::apply( SetIBLoutputCellGroup<VarType>(pdeIBLWake, qinf, qIBLfld, qauxvfld, {1}, -1), (xfld, outputfld) );

    const std::string caseTag = "coupledIBLSplitAmpLag2D_CapturedTransition_"
        + airfoilname + "_ne" + std::to_string(nelem_a) + "_p" + std::to_string(order_soln);
    const std::string outputfilename = "tmp/" + caseTag + "_output.plt";

    auto output_names = SetIBLoutputCellGroup_impl<VarType>::ElementProjectionType::outputNames();
    output_Tecplot(outputfld, outputfilename, output_names);

    // dump hub trace results
    std::cout << "Hub trace variable: " << setprecision(16) << hbfld.DOF(0) << std::endl;

    const std::string slnfilename_gam = "tmp/" + caseTag + "_gamma.plt";
    output_Tecplot(gamfld, slnfilename_gam, {"gamma"});

    const std::string slnfilename_lam = "tmp/" + caseTag + "_lambda.plt";
    output_Tecplot(lamfld, slnfilename_lam, {"lambda"});
    std::cout << "Psi_0 = " << setprecision(16) << Psi0 << std::endl;

    output_Tecplot(qauxifld, "tmp/" + caseTag + "_qauxifld.plt", {"qauxi"});
    output_Tecplot(qauxvfld, "tmp/" + caseTag + "_qauxvfld.plt", {"qx", "qz", "qx_x", "qx_z", "qz_x", "qz_z", "p0", "T0"});
  }
#endif

#if defined(IBL2D_DUMP) && 0
    // final jacobian
    blockAES.jacobian(sln, jac);
    cout << "writing final jacobian tmp/jac_final_restart.mtx" << endl;
    WriteMatrixMarketFile( jac, "tmp/jac_final_restart.mtx" );
#endif

    std::cout << "Total runtime for current case: " << timer_total.elapsed() << " s" << std::endl;
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Coupled4x4_IBLSplitAmpLag_CorrelationClosure_CapturedTransition_test )
{
  const std::vector<int> nelemArr = {64};

  for (int i=0; i < int(nelemArr.size()); ++i)
    runCase(nelemArr.at(i));
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
