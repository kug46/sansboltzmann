// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEulermitAVDiffusion3D_Consistency_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DConservative.h"
//#include "pde/NS/Q3DEntropy.h"
#include "pde/NS/PDEEulermitAVDiffusion3D.h"
#include "pde/NS/PDEEuler3D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
//class QTypeEntropy {};

//Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD3>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
//template class TraitsModelEuler<QTypeEntropy, GasModel>;
template class PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>>;
template class PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel>>;
//template class PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion3D_Consistency_btest )

//typedef boost::mpl::list< QTypePrimitiveRhoPressure,
//                          QTypeConservative,
//                          QTypePrimitiveSurrogate,
//                          QTypeEntropy > QTypes;
typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  BOOST_REQUIRE( AVPDEClass::D == PDEClass::D );
  BOOST_REQUIRE( AVPDEClass::N == PDEClass::N );
  BOOST_REQUIRE( AVArrayQ::M == ArrayQ::M );
  BOOST_REQUIRE( AVMatrixQ::M == MatrixQ::M );
  BOOST_REQUIRE( AVMatrixQ::N == MatrixQ::N );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == pde.D );
  BOOST_REQUIRE( avpde.N == pde.N );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == pde.hasFluxAdvectiveTime() );
  BOOST_CHECK( avpde.hasFluxAdvective() == pde.hasFluxAdvective() );
  BOOST_CHECK( avpde.hasFluxViscous() != pde.hasFluxViscous() );
  BOOST_CHECK( avpde.hasSource() == pde.hasSource() );
  BOOST_CHECK( avpde.hasSourceTrace() == pde.hasSourceTrace() );
  BOOST_CHECK( avpde.hasForcingFunction() == pde.hasSourceTrace() );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == pde.fluxViscousLinearInGradient() );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == pde.needsSolutionGradientforSource() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxAdvective, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 5 );

  // function tests

  Real x, y, time;
  Real rho, u, v, w, z, t;

  x = y = z = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = 0.821; w = 0.543; t = 0.987;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  // flux in time direction
  ArrayQ ftTrue = 0;
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( param, x, y, z, time, q, ft );
  pde.fluxAdvectiveTime( x, y, z, time, q, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( ftTrue(4), ft(4), tol );

  // Jacobian of flux in time direction
  MatrixQ Jt = 0;
  MatrixQ JtTrue = 0;
  avpde.jacobianFluxAdvectiveTime( param, x, y, z, time, q, Jt );
  pde.jacobianFluxAdvectiveTime( x, y, z, time, q, JtTrue );

  SANS_CHECK_CLOSE( JtTrue(0,0), Jt(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(0,1), Jt(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(0,2), Jt(0,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(0,3), Jt(0,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(0,4), Jt(0,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( JtTrue(1,0), Jt(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(1,1), Jt(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(1,2), Jt(1,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(1,3), Jt(1,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(1,4), Jt(1,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( JtTrue(2,0), Jt(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(2,1), Jt(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(2,2), Jt(2,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(2,3), Jt(2,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(2,4), Jt(2,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( JtTrue(3,0), Jt(3,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(3,1), Jt(3,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(3,2), Jt(3,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(3,3), Jt(3,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(3,4), Jt(3,4), small_tol, close_tol )

  SANS_CHECK_CLOSE( JtTrue(4,0), Jt(4,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(4,1), Jt(4,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(4,2), Jt(4,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(4,3), Jt(4,3), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(4,4), Jt(4,4), small_tol, close_tol )

  // advective flux
  ArrayQ fTrue = 0, gTrue = 0, hTrue = 0;
  ArrayQ f = 0, g = 0, h = 0;
  avpde.fluxAdvective( param, x, y, z, time, q, f, g, h );
  pde.fluxAdvective( x, y, z, time, q, fTrue, gTrue, hTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );
  BOOST_CHECK_CLOSE( gTrue(4), g(4), tol );

  // Jacobian of advective flux
  MatrixQ Jxadv = 0, Jyadv = 0, Jzadv = 0;
  MatrixQ JxadvTrue = 0, JyadvTrue = 0, JzadvTrue = 0;
  avpde.jacobianFluxAdvective( param, x, y, z, time, q, Jxadv, Jyadv, Jzadv );
  pde.jacobianFluxAdvective( x, y, z, time, q, JxadvTrue, JyadvTrue, JzadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JxadvTrue(i,j), Jxadv(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( JyadvTrue(i,j), Jyadv(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( JzadvTrue(i,j), Jzadv(i,j), small_tol, close_tol )
    }
  }

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 0.23};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 0.21};
  ArrayQ qz = {-6.32, 1.43, 0.65, -7.32, 0.01};
  ArrayQ qt = {0.21, 0.31, -0.14, 0.26, -0.31};

  // strong advective flux
  ArrayQ fadvStrongTrue = 0;
  ArrayQ fadvStrong = 0;
  avpde.strongFluxAdvective( param, x, y, z, time, q, qx, qy, qz, fadvStrong );
  pde.strongFluxAdvective( x, y, z, time, q, qx, qy, qz, fadvStrongTrue );
  BOOST_CHECK_CLOSE( fadvStrongTrue(0), fadvStrong(0), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(1), fadvStrong(1), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(2), fadvStrong(2), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(3), fadvStrong(3), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(4), fadvStrong(4), tol );

  // strong advective flux
  ArrayQ ftadvStrongTrue = 0;
  ArrayQ ftadvStrong = 0;
  avpde.strongFluxAdvectiveTime( param, x, y, z, time, q, qt, ftadvStrong );
  pde.strongFluxAdvectiveTime( x, y, z, time, q, qt, ftadvStrongTrue );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(0), ftadvStrong(0), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(1), ftadvStrong(1), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(2), ftadvStrong(2), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(3), ftadvStrong(3), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(4), ftadvStrong(4), tol );

  Real rhoL = 1.137, uL = 0.784, vL = -0.231, wL = 0.431, tL = 0.987;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, wR = 1.520, tR = 0.865;
  Real nx = 2.0/7.0; Real ny = 0.3; Real nz = sqrt(1.0-nx*nx-ny*ny);
  Real nt = 1.0;

  ArrayQ qL = 0, qR = 0;
  avpde.setDOFFrom( qL, DensityVelocityTemperature3D<Real>(rhoL, uL, vL, wL, tL) );
  avpde.setDOFFrom( qR, DensityVelocityTemperature3D<Real>(rhoR, uR, vR, wR, tR) );

  // upwinded advective flux
  f = 0;
  fTrue = 0;
  avpde.fluxAdvectiveUpwind( param, x, y, z, time, qL, qR, nx, ny, nz, f );
  pde.fluxAdvectiveUpwind( x, y, z, time, qL, qR, nx, ny, nz, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( fTrue(4), f(4), tol );

  // upwinded space-time advective flux
  ft = 0;
  ftTrue = 0;
  avpde.fluxAdvectiveUpwindSpaceTime( param, x, y, z, time, qL, qR, nx, ny, nz, nt, ft );
  pde.fluxAdvectiveUpwindSpaceTime( x, y, z, time, qL, qR, nx, ny, nz, nt, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );
  BOOST_CHECK_CLOSE( ftTrue(4), ft(4), tol );

  // absolute value of Jacobian of advective flux
  MatrixQ Jadv = 0;
  MatrixQ JadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValue( param, x, y, z, time, q, nx, ny, nz, Jadv );
  pde.jacobianFluxAdvectiveAbsoluteValue( x, y, z, time, q, nx, ny, nz, JadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JadvTrue(i,j), Jadv(i,j), small_tol, close_tol )
    }
  }

  // Right now jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // absolute value of Jacobian of advective flux
  MatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, z, time, q, nx, ny, nz, nt, Jtadv );
  pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( x, y, z, time, q, nx, ny, nz, nt, JtadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtadvTrue(i,j), Jtadv(i,j), small_tol, close_tol )
    }
  }
#else
  // absolute value of Jacobian of advective flux
  MatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  BOOST_CHECK_THROW(avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, z, time, q, nx, ny, nz, nt, Jtadv ), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( x, y, z, time, q, nx, ny, nz, nt, JtadvTrue ), DeveloperException);
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( masterState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 5 );

  // function tests

  Real rho, u, v, w, t;

  rho = 1.137; u = 0.784; v = 0.821; w = 0.213; t = 0.987;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, z = 0, time = 0;

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );

  // conservative flux
  ArrayQ uTrue = 0;
  ArrayQ uCons = 0;
  avpde.masterState( param, x, y, z, time, q, uCons );
  pde.masterState( x, y, z, time, q, uTrue );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(4), uCons(4), small_tol, close_tol )

  // conservative flux
  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;
  avpde.jacobianMasterState( param, x, y, z, time, q, dudq );
  pde.jacobianMasterState( x, y, z, time, q, dudqTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), small_tol, close_tol )
    }
  }
}

// Cannot do viscous flux checks here... should probably refactor things to be able to...

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, gas, Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, y, z, time;
  Real rhoL, uL, vL, wL, tL;
  Real rhoR, uR, vR, wR, tR;

  x = y = z = time = 0;   // not actually used in functions
  rhoL = 1.034; uL = 3.26; vL = 1.23; wL = 0.13; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR = 0.34; wR = 3.21; tR = 6.13;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  // set
  ArrayQ qL = avpde.setDOFFrom( DensityVelocityTemperature3D<Real>(rhoL, uL, vL, wL, tL) );
  ArrayQ qR = avpde.setDOFFrom( DensityVelocityTemperature3D<Real>(rhoR, uR, vR, wR, tR) );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19, 0.41};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21, 0.75};
  ArrayQ qzL = {-6.32, 1.43, 0.65, -7.32, 0.01};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26, -0.32};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15, 0.74};
  ArrayQ qzR = {0.56, 0.75, 0.94, -0.43, 0.83};

  Real lifted_quantity = 1.23;

  // vanilla source term
  ArrayQ s = 0;
  ArrayQ sTrue = 0;
  avpde.source( param, x, y, z, time, qL, qxL, qyL, qzL, s );
  pde.source( x, y, z, time, qL, qxL, qyL, qzL, sTrue );
  SANS_CHECK_CLOSE( sTrue(0), s(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(1), s(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(2), s(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(3), s(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(4), s(4), small_tol, close_tol )

  // source term with lifted quantity
  s = 0;
  sTrue = 0;
  avpde.source( param, x, y, z, time, lifted_quantity, qL, qxL, qyL, qzL, s );
  pde.source( x, y, z, time, lifted_quantity, qL, qxL, qyL, qzL, sTrue );
  SANS_CHECK_CLOSE( sTrue(0), s(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(1), s(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(2), s(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(3), s(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(4), s(4), small_tol, close_tol )

  // trace source term with lifted quantity
  ArrayQ sL = 0, sR = 0;
  ArrayQ sLTrue = 0, sRTrue = 0;
  avpde.sourceTrace( param, x, y, z, param, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sL, sR );
  pde.sourceTrace( x, y, z, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sLTrue, sRTrue );
  SANS_CHECK_CLOSE( sLTrue(0), sL(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(1), sL(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(2), sL(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(3), sL(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(4), sL(4), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(0), sR(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(1), sR(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(2), sR(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(3), sR(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(4), sR(4), small_tol, close_tol )

  // source for lifted quantity
  s = 0;
  sTrue = 0;
  avpde.sourceLiftedQuantity( param, x, y, z, param, x, y, z, time, qL, qR, s );
  pde.sourceLiftedQuantity( x, y, z, x, y, z, time, qL, qR, sTrue );
  SANS_CHECK_CLOSE( sTrue(0), s(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(1), s(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(2), s(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(3), s(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(4), s(4), small_tol, close_tol )

  // source Jacobian
  MatrixQ Js = 0;
  MatrixQ JsTrue = 0;
  avpde.jacobianSource( param, x, y, z, time, qL, qxL, qyL, qzL, Js );
  pde.jacobianSource( x, y, z, time, qL, qxL, qyL, qzL, JsTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JsTrue(i,j), Js(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, gas, Euler_ResidInterp_Raw);

  Real x, y, z, time, dx, dy, dz;
  Real rho, u, v, w, t;
  Real speed, speedTrue;

  x = y = z = time = 0;   // not actually used in functions
  dx = 0.57; dy = 1.23, dz = 0.43;
  rho = 1.137; u = 0.784; v = 2.31; w = 4.21; t = 0.987;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real qDataPrim[5] = {rho, u, v, w, t};
  string qNamePrim[5] = {"Density", "VelocityX", "VelocityY", "VelocityZ",  "Temperature"};
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 5 );

  avpde.speedCharacteristic( param, x, y, z, time, dx, dy, dz, q, speed );
  pde.speedCharacteristic( x, y, z, time, dx, dy, dz, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( param, x, y, z, time, q, speed );
  pde.speedCharacteristic( x, y, z, time, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
