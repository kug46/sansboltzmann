// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCEulermitAVSensor3D_Consistency_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DConservative.h"
//#include "pde/NS/Q3DEntropy.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/BCEulermitAVSensor3D.h"
#include "pde/NS/PDERANSSA3D.h"
#include "pde/NS/TraitsRANSSAArtificialViscosity.h"
#include "pde/NS/Fluids3D_Sensor.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor3D.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
//class QTypeEntropy {};
}

namespace SANS
{

template <class T>
using SAnt3D_rhovT = SAnt3D<DensityVelocityTemperature3D<T>>;

}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion3D_Consistency_btest )

typedef boost::mpl::list< QTypePrimitiveRhoPressure > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeWallNoSlipAdiabatic_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef BCRANSSA3D< BCTypeWall_mitState, BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  typedef BCRANSSA3D< BCTypeWall_mitState, BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor3D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const int order = 1;
  bool isSteady = false;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, z = 0, time = 0;   // not actually used in functions
  Real nx = 2.0/7.0; Real ny = 0.3; Real nz = sqrt(1.0-nx*nx-ny*ny);
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.431, t = 0.987, nut = 4.2, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  PyDict d;

  BCClass bc(pde, d);
  AVBCClass avbc(avpde, d);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nut}) );
  AVVariable<SAnt3D_rhovT,Real> qdata({rho, u, v, w, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.41};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.94};
  ArrayQ qz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.41, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.94, 0.0};
  AVArrayQ avqz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, z, time, nx, ny, nz, avqI, qB );
  bc.state( dist, x, y, z, time, nx, ny, nz, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(5), qB(5), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, z, time, nx, ny, nz, avqI, avqx, avqy, avqz, qB, Fn );
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, qI, qx, qy, qz, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(5), Fn(5), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeWallNoSlipAdiabatic_mitState_nonZeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef BCRANSSA3D< BCTypeWall_mitState, BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState, PDEClass>> BCClass;
  typedef BCRANSSA3D< BCTypeWall_mitState, BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor3D<BCTypeWall_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const int order = 1;
  bool isSteady = false;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, z = 0, time = 0;   // not actually used in functions
  Real nx = 2.0/7.0; Real ny = 0.3; Real nz = sqrt(1.0-nx*nx-ny*ny);
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.431, t = 0.987, nut = 4.2, s = 0.5;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  PyDict d;

  BCClass bc(pde, d);
  AVBCClass avbc(avpde, d);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nut}) );
  AVVariable<SAnt3D_rhovT,Real> qdata({rho, u, v, w, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.41};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.94};
  ArrayQ qz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.41, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.94, 0.0};
  AVArrayQ avqz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, z, time, nx, ny, nz, avqI, qB );
  bc.state( dist, x, y, z, time, nx, ny, nz, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(5), qB(5), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, z, time, nx, ny, nz, avqI, avqx, avqy, avqz, qB, Fn );
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, qI, qx, qy, qz, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(5), Fn(5), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeSymmetry_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef BCRANSSA3D< BCTypeNeumann_mitState, BCNavierStokes3D< BCTypeSymmetry_mitState, PDEClass>> BCClass;
  typedef BCRANSSA3D< BCTypeNeumann_mitState, BCNavierStokes3D< BCTypeSymmetry_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor3D<BCTypeSymmetry_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const int order = 1;
  bool isSteady = false;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, z = 0, time = 0;   // not actually used in functions
  Real nx = 2.0/7.0; Real ny = 0.3; Real nz = sqrt(1.0-nx*nx-ny*ny);
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.431, t = 0.987, nut = 4.2, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  PyDict d;

  BCClass bc(pde, d);
  AVBCClass avbc(avpde, d);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nut}) );
  AVVariable<SAnt3D_rhovT,Real> qdata({rho, u, v, w, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.41};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.94};
  ArrayQ qz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.41, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.94, 0.0};
  AVArrayQ avqz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, z, time, nx, ny, nz, avqI, qB );
  bc.state( dist, x, y, z, time, nx, ny, nz, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(5), qB(5), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, z, time, nx, ny, nz, avqI, avqx, avqy, avqz, qB, Fn );
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, qI, qx, qy, qz, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(5), Fn(5), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeFullState_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelRANSSA<QType, GasModel, ViscosityModel_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
  typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef BCRANSSA3D< BCTypeFullState_mitState, BCEuler3D< BCTypeFullState_mitState, PDEClass>> BCClass;
  typedef BCRANSSA3D< BCTypeFullState_mitState, BCEuler3D< BCTypeFullState_mitState, AVPDEClass>> BCBaseClass;
  typedef BCmitAVSensor3D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef BCParameters< BCRANSSAmitAVDiffusion3DVector<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef MakeTuple<ParamTuple, HType, Real>::type ParamType; //grid length + distance

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  const Real muRef = 1.789e-5;      // kg/(m s)
  const Real Prandtl = 0.72;
  const Real Cv = R/(gamma - 1);
  const Real Cp = gamma*Cv;

  const int order = 1;
  bool isSteady = false;

  GasModel gas(gamma, R);
  ViscosityModel_Const viscSA(muRef);
  ThermalConductivityModel tcond(Prandtl, Cp);

  PDEClass pde(gas, viscSA, tcond, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, viscSA, tcond, Euler_ResidInterp_Raw);

  Real dist = 60.0;
  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH, dist); // grid spacing + distance

  Real x = 0, y = 0, z = 0, time = 0;   // not actually used in functions
  Real nx = 2.0/7.0; Real ny = 0.3; Real nz = sqrt(1.0-nx*nx-ny*ny);
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.431, t = 0.987, nut = 4.2, s = 0.0;
  Real p  = R*rho*t;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  typedef SAnt3D<DensityVelocityPressure3D<Real>>::ParamsType ParamsType;

  PyDict d;
  d[NSVariableType3DParams::params.StateVector.Variables] = SAVariableType3DParams::params.StateVector.PrimitivePressure;
  d[ParamsType::params.rho] = rho;
  d[ParamsType::params.u] = u;
  d[ParamsType::params.v] = v;
  d[ParamsType::params.w] = w;
  d[ParamsType::params.p] = p;
  d[ParamsType::params.nt] = nut;

  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  BCClass bc(pde, BCFullState);
  AVBCClass avbc(avpde, BCFullState);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, SAnt3D<DensityVelocityTemperature3D<Real>>({rho, u, v, w, t, nut}) );
  AVVariable<SAnt3D_rhovT,Real> qdata({rho, u, v, w, t, nut}, s);
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, qdata );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.41};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.94};
  ArrayQ qz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 3.12, 0.41, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 7.12, 0.94, 0.0};
  AVArrayQ avqz = {-0.64, 0.89, 0.12, -0.73, 0.64, 4.86, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, z, time, nx, ny, nz, avqI, qB );
  bc.state( dist, x, y, z, time, nx, ny, nz, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(5), qB(5), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, z, time, nx, ny, nz, avqI, avqx, avqy, avqz, qB, Fn );
  bc.fluxNormal( dist, x, y, z, time, nx, ny, nz, qI, qx, qy, qz, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(5), Fn(5), small_tol, close_tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
