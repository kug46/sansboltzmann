// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// BCEulermitAVSensor3D_Consistency_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/Q3DPrimitiveSurrogate.h"
#include "pde/NS/Q3DConservative.h"
//#include "pde/NS/Q3DEntropy.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/BCEulermitAVSensor3D.h"
#include "pde/NS/PDEEuler3D.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Fluids3D_Sensor.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor3D.h"

#include "pde/AnalyticFunction/ScalarFunction3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"

#define BCPARAMETERS_INSTANTIATE // included because testing without NDConvert
#include "pde/BCParameters_impl.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
//class QTypeEntropy {};

//Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD3>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
//template class TraitsModelEuler<QTypeEntropy, GasModel>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion3D_Consistency_btest )

//typedef boost::mpl::list< QTypePrimitiveRhoPressure,
//                          QTypeConservative,
//                          QTypePrimitiveSurrogate,
//                          QTypeEntropy > QTypes;
typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCFullState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef BCEuler3D<BCTypeFullState_mitState, PDEClass> BCClass;
  typedef BCEuler3D<BCTypeFullState_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor3D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef BCParameters< BCEulermitAVSensor3DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, z = 0,time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.3, nz = sqrt(1-nx*nx-ny*ny);   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.211, t = 0.987, s = 0.0;
  Real p  = R*rho*t;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  PyDict d;
  d[NSVariableType3DParams::params.StateVector.Variables] = NSVariableType3DParams::params.StateVector.PrimitivePressure;
  d[DensityVelocityPressure3DParams::params.rho] = rho;
  d[DensityVelocityPressure3DParams::params.u] = u;
  d[DensityVelocityPressure3DParams::params.v] = v;
  d[DensityVelocityPressure3DParams::params.w] = w;
  d[DensityVelocityPressure3DParams::params.p] = p;

  //Pydict constructor
  PyDict BCFullState;
  BCFullState[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
  BCFullState[BCClass::ParamsType::params.Characteristic] = false;
  BCFullState[BCClass::ParamsType::params.StateVector] = d;

  BCClass bc(pde, BCFullState);
  AVBCClass avbc(avpde, BCFullState);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, AVVariable<DensityVelocityTemperature3D, Real>( DensityVelocityTemperature3D<Real>(rho, u, v, w, t), s ) );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 0.31};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 0.43};
  ArrayQ qz = {0.65, 4.62, 0.96, -0.25, 0.78};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.31, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.43, 0.0};
  AVArrayQ avqz = {0.65, 4.62, 0.96, -0.25, 0.78, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, z, time, nx, ny, nz, avqI, qB );
  bc.state( x, y, z, time, nx, ny, nz, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, z, time, nx, ny, nz, avqI, avqx, avqy, avqz, qB, Fn );
  bc.fluxNormal( x, y, z, time, nx, ny, nz, qI, qx, qy, qz, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeInflowSubsonic_PtTta_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDEClass> BCClass;
  typedef BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor3D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef BCParameters< BCEulermitAVSensor3DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, z = 0,time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.3, nz = sqrt(1-nx*nx-ny*ny);   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.211, t = 0.987, s = 0.0;
  Real p  = R*rho*t;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  Real TtSpec = t*(1+0.2*0.1*0.1);
  Real PtSpec = p*pow( (1+0.2*0.1*0.1), 3.5 );
  Real aSpec = 0.1;
  Real bSpec = 0.3;
  //Real sSpec = 0.265;

  //Pydict constructor
  PyDict BCInflowSubsonic;
  BCInflowSubsonic[BCParams::params.BC.BCType] = BCParams::params.BC.InflowSubsonic_PtTta_mitState;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.TtSpec] = TtSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.PtSpec] = PtSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.aSpec] = aSpec;
  BCInflowSubsonic[BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params.bSpec] = bSpec;

  BCClass bc(pde, BCInflowSubsonic);
  AVBCClass avbc(avpde, BCInflowSubsonic);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, AVVariable<DensityVelocityTemperature3D, Real>( DensityVelocityTemperature3D<Real>(rho, u, v, w, t), s ) );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 0.31};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 0.43};
  ArrayQ qz = {0.65, 4.62, 0.96, -0.25, 0.78};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.31, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.43, 0.0};
  AVArrayQ avqz = {0.65, 4.62, 0.96, -0.25, 0.78, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, z, time, nx, ny, nz, avqI, qB );
  bc.state( x, y, z, time, nx, ny, nz, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, z, time, nx, ny, nz, avqI, avqx, avqy, avqz, qB, Fn );
  bc.fluxNormal( x, y, z, time, nx, ny, nz, qI, qx, qy, qz, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeOutflowSubsonic_Pressure_mitState_zeroSensorTest, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDEClass> BCClass;
  typedef BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor3D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef BCParameters< BCEulermitAVSensor3DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> > BCParams;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, z = 0,time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.3, nz = sqrt(1-nx*nx-ny*ny);   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.211, t = 0.987, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  //Pydict constructor
  PyDict BCSubsonicOut;
  BCSubsonicOut[BCParams::params.BC.BCType] = BCParams::params.BC.OutflowSubsonic_Pressure_mitState;
  BCSubsonicOut[BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params.pSpec] = 101325.0;

  BCClass bc(pde, BCSubsonicOut);
  AVBCClass avbc(avpde, BCSubsonicOut);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, AVVariable<DensityVelocityTemperature3D, Real>( DensityVelocityTemperature3D<Real>(rho, u, v, w, t), s ) );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 0.31};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 0.43};
  ArrayQ qz = {0.65, 4.62, 0.96, -0.25, 0.78};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.31, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.43, 0.0};
  AVArrayQ avqz = {0.65, 4.62, 0.96, -0.25, 0.78, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, z, time, nx, ny, nz, avqI, qB );
  bc.state( x, y, z, time, nx, ny, nz, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, z, time, nx, ny, nz, avqI, avqx, avqy, avqz, qB, Fn );
  bc.fluxNormal( x, y, z, time, nx, ny, nz, qI, qx, qy, qz, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( BCTypeReflect_mitState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

  typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
  typedef AVSensor_ViscousFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
  typedef AVSensor_Source3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorSource;

  typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
  typedef PDEmitAVSensor3D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> AVPDEClass;

  typedef BCEuler3D<BCTypeReflect_mitState, PDEClass> BCClass;
  typedef BCEuler3D<BCTypeReflect_mitState, AVPDEClass> BCBaseClass;
  typedef BCmitAVSensor3D<BCTypeFlux_mitState, BCBaseClass> AVBCClass;

  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;

  typedef DLA::MatrixSymS<PhysD3::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1.0e-12;
  const Real close_tol = 1.0e-12;

  Real us = 1.0, vs = 0.0, ws = 0.0;;
  Real ksxx = 2.123, ksxy = 0.553, ksxz = 0.643;
  Real ksyx = 0.378, ksyy = 1.007, ksyz = 0.765;
  Real kszx = 1.642, kszy = 1.299, kszz = 1.234;
  Real as = 0.8;

  SensorAdvectiveFlux adv(us, vs, ws );
  SensorViscousFlux visc( ksxx, ksxy, ksxz,
                           ksyx, ksyy, ksyz,
                           kszx, kszy, kszz );
  SensorSource source(as);

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);

  const int order = 1;
  bool isSteady = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(adv, visc, source, isSteady,
                   order,
                   gas, Euler_ResidInterp_Raw);

  HType H = {{0.5},{0.1, 0.3}, {0.2, 0.5, 1.0}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, z = 0,time = 0;   // not actually used in functions
  Real nx = -0.936, ny = 0.3, nz = sqrt(1-nx*nx-ny*ny);   // Note: magnitude = 1
  Real rho = 1.137, u = 0.784, v = -0.231, w = 0.211, t = 0.987, s = 0.0;

  BOOST_CHECK( BCClass::D == AVBCClass::D );
  BOOST_CHECK( BCClass::N+1 == AVBCClass::N );
//  BOOST_CHECK( BCClass::NBC+1 == AVBCClass::NBC );

  BCClass bc(pde);
  AVBCClass avbc(avpde);

  ArrayQ qI = 0;
  pde.setDOFFrom( qI, DensityVelocityTemperature3D<Real>(rho, u, v, w, t) );
  AVArrayQ avqI = 0;
  avpde.setDOFFrom( avqI, AVVariable<DensityVelocityTemperature3D, Real>( DensityVelocityTemperature3D<Real>(rho, u, v, w, t), s ) );

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19, 0.31};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21, 0.43};
  ArrayQ qz = {0.65, 4.62, 0.96, -0.25, 0.78};
  AVArrayQ avqx = {0.17, 0.21, -0.34, 0.19, 0.31, 0.0};
  AVArrayQ avqy = {-0.05, 0.61, 0.12, -0.21, 0.43, 0.0};
  AVArrayQ avqz = {0.65, 4.62, 0.96, -0.25, 0.78, 0.0};

  AVArrayQ qB = 0;
  ArrayQ qBTrue = 0.0;
  avbc.state( param, x, y, z, time, nx, ny, nz, avqI, qB );
  bc.state( x, y, z, time, nx, ny, nz, qI, qBTrue );

  SANS_CHECK_CLOSE( qBTrue(0), qB(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(1), qB(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(2), qB(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(3), qB(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( qBTrue(4), qB(4), small_tol, close_tol );

  AVArrayQ Fn = 0;
  ArrayQ FnTrue = 0.0;
  avbc.fluxNormal( param, x, y, z, time, nx, ny, nz, avqI, avqx, avqy, avqz, qB, Fn );
  bc.fluxNormal( x, y, z, time, nx, ny, nz, qI, qx, qy, qz, qBTrue, FnTrue );

  SANS_CHECK_CLOSE( FnTrue(0), Fn(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(1), Fn(1), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(2), Fn(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(3), Fn(3), small_tol, close_tol );
  SANS_CHECK_CLOSE( FnTrue(4), Fn(4), small_tol, close_tol );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
