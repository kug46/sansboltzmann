// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// PDEEulermitAVDiffusion2D_Consistency_btest
//
// test of quasi 1-D compressible Euler PDE class

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include <boost/mpl/list.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Topology/Dimension.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"

namespace SANS
{
// Needed for Boost Test
class QTypePrimitiveRhoPressure {};
class QTypePrimitiveSurrogate {};
class QTypeConservative {};
class QTypeEntropy {};

//Explicitly instantiate the class so coverage information is correct
template class TraitsSizeEuler<PhysD2>;
template class TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>;
template class TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>;
template class TraitsModelEuler<QTypeConservative, GasModel>;
template class TraitsModelEuler<QTypeEntropy, GasModel>;
template class PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>>;
template class PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>>;
template class PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel>>;
template class PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel>>;
}

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( PDEEulermitAVDiffusion2D_Consistency_btest )

typedef boost::mpl::list< QTypePrimitiveRhoPressure,
                          QTypeConservative,
                          QTypePrimitiveSurrogate,
                          QTypeEntropy > QTypes;

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( static_test, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename PDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename PDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename AVPDEClass::template ArrayQ<Real> AVArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> AVMatrixQ;

  BOOST_REQUIRE( AVPDEClass::D == PDEClass::D );
  BOOST_REQUIRE( AVPDEClass::N == PDEClass::N );
  BOOST_REQUIRE( AVArrayQ::M == ArrayQ::M );
  BOOST_REQUIRE( AVMatrixQ::M == MatrixQ::M );
  BOOST_REQUIRE( AVMatrixQ::N == MatrixQ::N );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( ctor, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;

  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.D == pde.D );
  BOOST_REQUIRE( avpde.N == pde.N );

  // flux components
  BOOST_CHECK( avpde.hasFluxAdvectiveTime() == pde.hasFluxAdvectiveTime() );
  BOOST_CHECK( avpde.hasFluxAdvective() == pde.hasFluxAdvective() );
  BOOST_CHECK( avpde.hasFluxViscous() != pde.hasFluxViscous() );
  BOOST_CHECK( avpde.hasSource() == pde.hasSource() );
  BOOST_CHECK( avpde.hasSourceTrace() == pde.hasSourceTrace() );
  BOOST_CHECK( avpde.hasForcingFunction() == pde.hasSourceTrace() );

  BOOST_CHECK( avpde.fluxViscousLinearInGradient() == pde.fluxViscousLinearInGradient() );
  BOOST_CHECK( avpde.needsSolutionGradientforSource() == pde.needsSolutionGradientforSource() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( fluxAdvective, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;
  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 4 );

  // function tests

  Real x, y, time;
  Real rho, u, v, t;

  x = y = time = 0;   // not actually used in functions
  rho = 1.137; u = 0.784; v = 0.821; t = 0.987;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  // flux in time direction
  ArrayQ ftTrue = 0;
  ArrayQ ft = 0;
  avpde.fluxAdvectiveTime( param, x, y, time, q, ft );
  pde.fluxAdvectiveTime( x, y, time, q, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );

  // Jacobian of flux in time direction
  MatrixQ Jt = 0;
  MatrixQ JtTrue = 0;
  avpde.jacobianFluxAdvectiveTime( param, x, y, time, q, Jt );
  pde.jacobianFluxAdvectiveTime( x, y, time, q, JtTrue );

  SANS_CHECK_CLOSE( JtTrue(0,0), Jt(0,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(0,1), Jt(0,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(0,2), Jt(0,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(0,3), Jt(0,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( JtTrue(1,0), Jt(1,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(1,1), Jt(1,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(1,2), Jt(1,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(1,3), Jt(1,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( JtTrue(2,0), Jt(2,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(2,1), Jt(2,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(2,2), Jt(2,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(2,3), Jt(2,3), small_tol, close_tol )

  SANS_CHECK_CLOSE( JtTrue(3,0), Jt(3,0), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(3,1), Jt(3,1), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(3,2), Jt(3,2), small_tol, close_tol )
  SANS_CHECK_CLOSE( JtTrue(3,3), Jt(3,3), small_tol, close_tol )

  // advective flux
  ArrayQ fTrue = 0, gTrue = 0;
  ArrayQ f = 0, g = 0;
  avpde.fluxAdvective( param, x, y, time, q, f, g );
  pde.fluxAdvective( x, y, time, q, fTrue, gTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );
  BOOST_CHECK_CLOSE( gTrue(0), g(0), tol );
  BOOST_CHECK_CLOSE( gTrue(1), g(1), tol );
  BOOST_CHECK_CLOSE( gTrue(2), g(2), tol );
  BOOST_CHECK_CLOSE( gTrue(3), g(3), tol );

  // Jacobian of advective flux
  MatrixQ Jxadv = 0, Jyadv = 0;
  MatrixQ JxadvTrue = 0, JyadvTrue = 0;
  avpde.jacobianFluxAdvective( param, x, y, time, q, Jxadv, Jyadv );
  pde.jacobianFluxAdvective( x, y, time, q, JxadvTrue, JyadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JxadvTrue(i,j), Jxadv(i,j), small_tol, close_tol )
      SANS_CHECK_CLOSE( JyadvTrue(i,j), Jyadv(i,j), small_tol, close_tol )
    }
  }

  ArrayQ qx = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qy = {-0.05, 0.61, 0.12, -0.21};
  ArrayQ qt = {0.21, 0.31, -0.14, 0.26};

  // strong advective flux
  ArrayQ fadvStrongTrue = 0;
  ArrayQ fadvStrong = 0;
  avpde.strongFluxAdvective( param, x, y, time, q, qx, qy, fadvStrong );
  pde.strongFluxAdvective( x, y, time, q, qx, qy, fadvStrongTrue );
  BOOST_CHECK_CLOSE( fadvStrongTrue(0), fadvStrong(0), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(1), fadvStrong(1), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(2), fadvStrong(2), tol );
  BOOST_CHECK_CLOSE( fadvStrongTrue(3), fadvStrong(3), tol );

  // strong advective flux
  ArrayQ ftadvStrongTrue = 0;
  ArrayQ ftadvStrong = 0;
  avpde.strongFluxAdvectiveTime( param, x, y, time, q, qt, ftadvStrong );
  pde.strongFluxAdvectiveTime( x, y, time, q, qt, ftadvStrongTrue );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(0), ftadvStrong(0), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(1), ftadvStrong(1), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(2), ftadvStrong(2), tol );
  BOOST_CHECK_CLOSE( ftadvStrongTrue(3), ftadvStrong(3), tol );

  Real rhoL = 1.137, uL = 0.784, vL = -0.231, tL = 0.987;
  Real rhoR = 1.240, uR = 0.831, vR =  0.142, tR = 0.865;
  Real nx = 2.0/7.0; Real ny = sqrt(1.0-nx*nx);
  Real nt = 1.0;

  ArrayQ qL = 0, qR = 0;
  avpde.setDOFFrom( qL, DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  avpde.setDOFFrom( qR, DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  // upwinded advective flux
  f = 0;
  fTrue = 0;
  avpde.fluxAdvectiveUpwind( param, x, y, time, qL, qR, nx, ny, f );
  pde.fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fTrue );
  BOOST_CHECK_CLOSE( fTrue(0), f(0), tol );
  BOOST_CHECK_CLOSE( fTrue(1), f(1), tol );
  BOOST_CHECK_CLOSE( fTrue(2), f(2), tol );
  BOOST_CHECK_CLOSE( fTrue(3), f(3), tol );

  // upwinded space-time advective flux
  ft = 0;
  ftTrue = 0;
  avpde.fluxAdvectiveUpwindSpaceTime( param, x, y, time, qL, qR, nx, ny, nt, ft );
  pde.fluxAdvectiveUpwindSpaceTime( x, y, time, qL, qR, nx, ny, nt, ftTrue );
  BOOST_CHECK_CLOSE( ftTrue(0), ft(0), tol );
  BOOST_CHECK_CLOSE( ftTrue(1), ft(1), tol );
  BOOST_CHECK_CLOSE( ftTrue(2), ft(2), tol );
  BOOST_CHECK_CLOSE( ftTrue(3), ft(3), tol );

  // absolute value of Jacobian of advective flux
  MatrixQ Jadv = 0;
  MatrixQ JadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValue( param, x, y, time, q, nx, ny, Jadv );
  pde.jacobianFluxAdvectiveAbsoluteValue( x, y, time, q, nx, ny, JadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JadvTrue(i,j), Jadv(i,j), small_tol, close_tol )
    }
  }

  // Right now jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented,
  // once it is, this is the test we want
#if 0
  // absolute value of Jacobian of advective flux
  MatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, time, q, nx, ny, nt, Jtadv );
  pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( x, y, time, q, nx, ny, nt, JtadvTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JtadvTrue(i,j), Jtadv(i,j), small_tol, close_tol )
    }
  }
#else
  // absolute value of Jacobian of advective flux
  MatrixQ Jtadv = 0;
  MatrixQ JtadvTrue = 0;
  BOOST_CHECK_THROW(avpde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( param, x, y, time, q, nx, ny, nt, Jtadv ), DeveloperException);
  BOOST_CHECK_THROW(pde.jacobianFluxAdvectiveAbsoluteValueSpaceTime( x, y, time, q, nx, ny, nt, JtadvTrue ), DeveloperException);
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( masterState, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  // static tests
  BOOST_REQUIRE( avpde.N == 4 );

  // function tests

  Real rho, u, v, t;

  rho = 1.137; u = 0.784; v = 0.821; t = 0.987;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real x = 0, y = 0, time = 0;

  // set
  ArrayQ q = 0;
  avpde.setDOFFrom( q, DensityVelocityTemperature2D<Real>(rho, u, v, t) );

  // conservative flux
  ArrayQ uTrue = 0;
  ArrayQ uCons = 0;
  avpde.masterState( param, x, y, time, q, uCons );
  pde.masterState( x, y, time, q, uTrue );
  SANS_CHECK_CLOSE( uTrue(0), uCons(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(1), uCons(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(2), uCons(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( uTrue(3), uCons(3), small_tol, close_tol )

  // conservative flux
  MatrixQ dudq = 0;
  MatrixQ dudqTrue = 0;
  avpde.jacobianMasterState( param, x, y, time, q, dudq );
  pde.jacobianMasterState( x, y, time, q, dudqTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( dudqTrue(i,j), dudq(i,j), small_tol, close_tol )
    }
  }
}

// Cannot do viscous flux checks here... should probably refactor things to be able to...

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( source, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename AVPDEClass::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-12;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  // Roe flux function test

  Real x, y, time;
  Real rhoL, uL, vL,tL;
  Real rhoR, uR, vR, tR;

  x = y = time = 0;   // not actually used in functions
  rhoL = 1.034; uL = 3.26; vL = 1.23; tL = 5.78;
  rhoR = 0.973; uR = 1.79; vR = 0.34; tR = 6.13;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  // set
  ArrayQ qL = avpde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoL, uL, vL, tL) );
  ArrayQ qR = avpde.setDOFFrom( DensityVelocityTemperature2D<Real>(rhoR, uR, vR, tR) );

  ArrayQ qxL = {0.17, 0.21, -0.34, 0.19};
  ArrayQ qyL = {-0.05, 0.61, 0.12, -0.21};

  ArrayQ qxR = {0.21, 0.31, -0.14, 0.26};
  ArrayQ qyR = {0.11, 0.48, 0.27, -0.15};

  Real lifted_quantity = 1.23;

  // vanilla source term
  ArrayQ s = 0;
  ArrayQ sTrue = 0;
  avpde.source( param, x, y, time, qL, qxL, qyL, s );
  pde.source( x, y, time, qL, qxL, qyL, sTrue );
  SANS_CHECK_CLOSE( sTrue(0), s(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(1), s(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(2), s(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(3), s(3), small_tol, close_tol )

  // source term with lifted quantity
  s = 0;
  sTrue = 0;
  avpde.source( param, x, y, time, lifted_quantity, qL, qxL, qyL, s );
  pde.source( x, y, time, lifted_quantity, qL, qxL, qyL, sTrue );
  SANS_CHECK_CLOSE( sTrue(0), s(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(1), s(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(2), s(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(3), s(3), small_tol, close_tol )

  // trace source term with lifted quantity
  ArrayQ sL = 0, sR = 0;
  ArrayQ sLTrue = 0, sRTrue = 0;
  avpde.sourceTrace( param, x, y, param, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sL, sR );
  pde.sourceTrace( x, y, x, y, time, qL, qxL, qyL, qR, qxR, qyR, sLTrue, sRTrue );
  SANS_CHECK_CLOSE( sLTrue(0), sL(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(1), sL(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(2), sL(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sLTrue(3), sL(3), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(0), sR(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(1), sR(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(2), sR(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sRTrue(3), sR(3), small_tol, close_tol )

  // source for lifted quantity
  s = 0;
  sTrue = 0;
  avpde.sourceLiftedQuantity( param, x, y, param, x, y, time, qL, qR, s );
  pde.sourceLiftedQuantity( x, y, x, y, time, qL, qR, sTrue );
  SANS_CHECK_CLOSE( sTrue(0), s(0), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(1), s(1), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(2), s(2), small_tol, close_tol )
  SANS_CHECK_CLOSE( sTrue(3), s(3), small_tol, close_tol )

  // source Jacobian
  MatrixQ Js = 0;
  MatrixQ JsTrue = 0;
  avpde.jacobianSource( param, x, y, time, qL, qxL, qyL, Js );
  pde.jacobianSource( x, y, time, qL, qxL, qyL, JsTrue );

  for (int i = 0; i < PDEClass::N; i++)
  {
    for (int j = 0; j < PDEClass::N; j++)
    {
      SANS_CHECK_CLOSE( JsTrue(i,j), Js(i,j), small_tol, close_tol )
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE_TEMPLATE( speedCharacteristic, QType, QTypes )
{
  typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
  typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
  typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> AVPDEClass;
  typedef typename AVPDEClass::template ArrayQ<Real> ArrayQ;

  typedef DLA::MatrixSymS<PhysD2::D,Real> HType;
  typedef HType ParamType; //grid length

  const Real tol = 1.e-13;

  const Real gamma = 1.4;
  const Real R = 0.4;
  GasModel gas(gamma, R);
  bool hasSpaceTimeDiffusion = false;
  PDEClass pde(gas, Euler_ResidInterp_Raw);
  AVPDEClass avpde(1, hasSpaceTimeDiffusion, EulerArtViscosity::eLaplaceViscosityEnthalpy, gas, Euler_ResidInterp_Raw);

  Real dx, dy;
  Real rho, u, v, t;
  Real speed, speedTrue;

  Real x = 0, y = 0, time = 0;
  dx = 0.57; dy = 1.23;
  rho = 1.137; u = 0.784; v = 2.31; t = 0.987;
  HType H = {{0.5},{0.1, 0.3}};
  HType logH = log(H);
  ParamType param(logH); // grid spacing

  Real qDataPrim[4] = {rho, u, v, t};
  string qNamePrim[4] = {"Density", "VelocityX", "VelocityY",  "Temperature"};
  ArrayQ q;
  avpde.setDOFFrom( q, qDataPrim, qNamePrim, 4 );

  avpde.speedCharacteristic( param, x, y, time, dx, dy, q, speed );
  pde.speedCharacteristic( x, y, time, dx, dy, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );

  avpde.speedCharacteristic( param, x, y, time, q, speed );
  pde.speedCharacteristic( x, y, time, q, speedTrue );
  BOOST_CHECK_CLOSE( speedTrue, speed, tol );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
